angular.module('app')
    .controller('VehicleTypeColorController', function($scope, $http, CurrentUser, ProductModel, SuffixType, VehicleTypeColor, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVehicleTypeColor = null; //Model
    $scope.cVehicleTypeColor = null; //Collection
    $scope.xVehicleTypeColor = {};
    $scope.xVehicleTypeColor.selected=[];

    $scope.filter = {vehicleModelId:null, vehicleTypeId:null, colorId:null};

    $scope.enbElement = true;
    $scope.disEl = false;

    // var dateFilter='date:"dd/MM/yyyy HH:mm"';
    var dateFilter='date:"dd/MM/yyyy"';
    
    $scope.onShowDetail = function(xrow, mode){
        $scope.mVehicleTypeColor.productModel = _.find($scope.productModelData,function(o){
            return o.VehicleModelId == $scope.mVehicleTypeColor.VehicleModelId;
        });
        $scope.mVehicleTypeColor.productType = _.find($scope.mVehicleTypeColor.productModel.listVehicleType,function(o){
            return o.VehicleTypeId == $scope.mVehicleTypeColor.VehicleTypeId;
        });

        var noFndColor = _.find($scope.mVehicleTypeColor.productType.listColor,function(o){
            return o.ColorId == $scope.mVehicleTypeColor.ColorId;
        });
        if (noFndColor==undefined || noFndColor==null) {
            $scope.mVehicleTypeColor.productType.listColor.push({
                ColorId : $scope.mVehicleTypeColor.ColorId,
                ColorCode : xrow.ColorCode,
                ColorName : xrow.ColorName
            });
        }

        // var katObj = _.find($scope.productModelData,function(o){
        //     return o.KatashikiCode == $scope.mVehicleTypeColor.KatashikiCode;
        // });
        // var sufObj = _.find($scope.productModelData,function(o){
        //     return o.SuffixCode == $scope.mVehicleTypeColor.SuffixCode;
        // });
        $scope.katashikiData = [];
        $scope.suffixData = [];
        $scope.assemblyTypeData = [];
        $scope.formulaData = [];

        $scope.katashikiData.push({KatashikiCode:$scope.mVehicleTypeColor.KatashikiCode});
        $scope.suffixData.push({SuffixCode:$scope.mVehicleTypeColor.SuffixCode});
        $scope.assemblyTypeData.push({AssemblyType:$scope.mVehicleTypeColor.AssemblyType});
        $scope.formulaData.push({FormulaHitung:$scope.mVehicleTypeColor.FormulaHitung});
        console.log($scope.mVehicleTypeColor);

        if (mode=='edit') {
            console.log('edit mode..');
            $scope.enbElement = true;
            $scope.disEl = false;
        } else {
            $scope.enbElement = false;
            $scope.disEl = true;
            // prepareData();
        }
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        console.log($scope.filter);
        // if ($scope.filter.vehicleModelId==null || $scope.filter.vehicleTypeId==null) {
        //     bsNotify.show({
        //         size: 'big',
        //         type: 'danger',
        //         title: "Mohon Input Filter",
        //         // content: error.join('<br>'),
        //         // number: error.length
        //     });
        // }else{            
            var temp = angular.copy($scope.filter);
            VehicleTypeColor.getData(temp).then(
            // VehicleTypeColor.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data.Result;                
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        // }
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    // $scope.filproductModel = function(selected){
    //     $scope.filter.productModel = selected;    
    //     $scope.filter.vehicleTypeId = null;
    //     $scope.filter.colorId = null;
    // }
    
    $scope.filproductType = function(selected){        
        if(selected){
            $scope.filter.productType = selected;
        }else{
            $scope.filter.vehicleTypeId = null;
            $scope.filter.productType = null;
        }
        $scope.filter.colorId = null;
    };

    $scope.startDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        //disableWeekend: 1
    };
    $scope.endDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        //disableWeekend: 1
    };

    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;            
            return res.data.Result;
        }
    );
    SuffixType.getData().then(
        function(res){
            // $scope.suffixTypeData = res.data.Result;
            // $scope.suffixTypeDataEdit = res.data.Result;
            $scope.suffixTypeData = res.data.Result;
            $scope.suffixTypeDataEdit = [];
            for (var i in res.data.Result) {
                if (res.data.Result[i].StatusCode == 1)
                    $scope.suffixTypeDataEdit.push(res.data.Result[i]);
            }
            $scope.suffixTypeData.unshift({
                SuffixTypeId : -1,
                SuffixTypeName : 'Belum diset Suffix Type', 
                SuffixTypeCode : '', 
                SuffixTypeDescription : ''
            });
            $scope.loading=false;
            return res.data;
        },
        function(err){
            console.log("err=>",err);
        }
    );

    //----------------------------------
    // Grid Setup
    //----------------------------------    

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'Model Id',    field:'VehicleModelId', width:'7%' , visible: false},
            { name:'Model Name', field:'VehicleModelName' },
            { name:'Type', field:'Type' },
            { name:'Color Code',  field: 'ColorCode' },
            { name:'Color Name', field:'ColorName' },
            { name:'Start Order',  field: 'StartValidDate', cellFilter: dateFilter },
            { name:'End Order', field:'EndValidDate', cellFilter: dateFilter },
            { name:'Start SPK',  field: 'StartSPKDate', cellFilter: dateFilter },
            { name:'End SPK', field:'EndSPKDate', cellFilter: dateFilter },
            { name:'Suffix Type Code', field:'SuffixTypeCode' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
