angular.module('app')
  .factory('VehicleTypeColor', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    function fixDate(date) {
      if (date != null || date != undefined) {
          var fix = date.getFullYear() + '-' +
              ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
              ('0' + date.getDate()).slice(-2) + 'T' +
              ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
              ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
              ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
          return fix;
      } else {
          return null;
      }
  };

    return {
      getData: function(filter) {
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }
        if (filter.productModel) {
          delete filter.productModel;
        }
        if (filter.productType) {
          delete filter.productType;
        }
        var res = $http.get('/api/param/VehicleTransactions?start=1&limit=10000'+(Object.keys(filter).length>0?'&':'')+$httpParamSerializer(filter));
        // console.log('res=>','/api/param/VehicleTransactions?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;
      },
      update: function(obj){
        return $http.put('/api/param/VehicleTransactions/Multiple', [{
                                                                VehicleTransactionId: obj.VehicleTransactionId,
                                                                StartValidDate: fixDate(obj.StartValidDate),
                                                                EndValidDate: fixDate(obj.EndValidDate),
                                                                StartSPKDate: fixDate(obj.StartSPKDate),
                                                                EndSPKDate: fixDate(obj.EndSPKDate),
                                                                SuffixTypeId: obj.SuffixTypeId}]);
      },
    }
  });
