angular.module('app')
  .factory('SuffixType', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/master/vehiclemodel');
        var res = $http.get('/api/param/SuffixTypes?start=1&limit=1000');
        //console.log('res=>',res);
        return res;
      },

      create: function(suffix) {
        return $http.post('/api/param/SuffixTypes/Multiple', [{
                                            SuffixTypeCode: suffix.SuffixTypeCode,
                                            SuffixTypeName: suffix.SuffixTypeName,
                                            SuffixTypeDescription: suffix.SuffixTypeDescription,
                                            StatusCode: suffix.StatusCode}]);
      },
      update: function(suffix){
        return $http.put('/api/param/SuffixTypes/Multiple', [{
                                                                SuffixTypeId: suffix.SuffixTypeId,
                                                                SuffixTypeCode: suffix.SuffixTypeCode,
                                                                SuffixTypeName: suffix.SuffixTypeName,
                                                                SuffixTypeDescription: suffix.SuffixTypeDescription,
                                                                StatusCode: suffix.StatusCode}]);
      },
      delete: function(id) {
        return $http.post('/api/param/SuffixTypes/delete',id);
      },
    }
  });
