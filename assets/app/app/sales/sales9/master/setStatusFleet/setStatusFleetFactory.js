angular.module('app')
  .factory('SetStatusFleetFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      
        getData: function(filter) {
            for(key in filter){
              if(filter[key]==null || filter[key]==undefined)
                delete filter[key];
            }
            var res=$http.get('/api/sales/FleetStatusCustomer/?start=1&limit=1000&'+$httpParamSerializer(filter));
            return res;
        },
		getCustomerCategory: function() {
            var res=$http.get('/api/sales/CCustomerCategory');
            return res;
        },
		GetAfcoOptions: function () {
        
        var da_json = [
          { SearchOptionId: true, SearchOptionName: "AFCO" },
		  { SearchOptionId: false, SearchOptionName: "Non-AFCO" },

        ];

        var res=da_json

        return res;
      },

        getDetail: function(param) {
            var res=$http.get('/api/sales/FleetStatusCustomer/TOP' + param);
            return res;
        },

        update: function(TOP){
            return $http.put('/api/sales/FleetStatusCustomer', [TOP]);
        },

        delete: function(id) {
          return $http.delete('/api/sales/FleetStatusCustomer',{data:id,headers: {'Content-Type': 'application/json'}});
        },
    }
  });