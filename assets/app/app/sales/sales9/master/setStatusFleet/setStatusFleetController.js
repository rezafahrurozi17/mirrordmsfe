angular.module('app')
    .controller('SetStatusFleetController', function ($scope, $httpParamSerializer, $http, CurrentUser, SetStatusFleetFactory, uiGridConstants, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
        });

        $scope.mainForm = true;
        $scope.secondForm = false;

        var listData = [{
            FleetId: 1,
            CategoryFleetName: "Fleet"
        },
        {
            FleetId: 2,
            CategoryFleetName: "Non Fleet"
        }
        ];

        $scope.selectionItem = listData;

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.dateOptions = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.user = CurrentUser.user();
        $scope.mSetStatusFleet = null; //Model
        $scope.xRole = { selected: [] };
        //$scope.filter = {timeFrameYearStart:null, timeFrameYearEnd:null, unitCount:null};
        $scope.filter = { timeFrameYear: null, unitCount: null };
        $scope.optionsAfcoBuatSetStatusFleet = SetStatusFleetFactory.GetAfcoOptions();
        $scope.disabledBtnSimpan = false;
        //----------------------------------
        // Get Data test
        //----------------------------------
        var gridData = [];
        $scope.filter = {};
        $scope.filter.timeFrameYear = 1;
        $scope.getData = function () {
            console.log('$scope.filter', $scope.filter);
            if ($scope.filter.timeFrameYear == null) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Data mandatory harus diisi",
                        type: 'warning'
                    }
                );
            }
            // else  if($scope.filter.timeFrameYearEnd < $scope.filter.timeFrameYearStart){
            //     bsNotify.show(
            //         {
            //             title: "Peringatan",
            //             content: "Tanggal End lebih besar dari Tanggal Start",
            //             type: 'warning'
            //         }
            //     );
            // }
            else {
                //         try {
                //             $scope.filter.timeFrameYearStart = $scope.filter.timeFrameYearStart.getFullYear() + '-' +
                //             ('0' + ($scope.filter.timeFrameYearStart.getMonth() + 1)).slice(-2) + '-' +
                //             ('0' + $scope.filter.timeFrameYearStart.getDate()).slice(-2) + 'T' +
                //             (($scope.filter.timeFrameYearStart.getHours() < '10' ? '0' : '') + $scope.filter.timeFrameYearStart.getHours()) + ':' +
                //             (($scope.filter.timeFrameYearStart.getMinutes() < '10' ? '0' : '') + $scope.filter.timeFrameYearStart.getMinutes()) + ':' +
                //             (($scope.filter.timeFrameYearStart.getSeconds() < '10' ? '0' : '') + $scope.filter.timeFrameYearStart.getSeconds());

                //             $scope.filter.timeFrameYearStart = $scope.filter.timeFrameYearStart.slice(0, 10);
                //     } catch (e1) {
                //         $scope.filter.timeFrameYearStart = null;
                //     }

                //     try {
                //         $scope.filter.timeFrameYearEnd = $scope.filter.timeFrameYearEnd.getFullYear() + '-' +
                //         ('0' + ($scope.filter.timeFrameYearEnd.getMonth() + 1)).slice(-2) + '-' +
                //         ('0' + $scope.filter.timeFrameYearEnd.getDate()).slice(-2) + 'T' +
                //         (($scope.filter.timeFrameYearEnd.getHours() < '10' ? '0' : '') + $scope.filter.timeFrameYearEnd.getHours()) + ':' +
                //         (($scope.filter.timeFrameYearEnd.getMinutes() < '10' ? '0' : '') + $scope.filter.timeFrameYearEnd.getMinutes()) + ':' +
                //         (($scope.filter.timeFrameYearEnd.getSeconds() < '10' ? '0' : '') + $scope.filter.timeFrameYearEnd.getSeconds());

                //         $scope.filter.timeFrameYearEnd = $scope.filter.timeFrameYearEnd.slice(0, 10);
                // } catch (e1) {
                //     $scope.filter.timeFrameYearEnd = null;
                // }

                SetStatusFleetFactory.getData($scope.filter)
                    .then(
                    function (res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                    },
                    function (err) {
                        bsNotify.show(
                            {
                                title: "Gagal",
                                content: "Data tidak ditemukan.",
                                type: 'danger'
                            }
                        );
                    }
                    );
            }
        }

        SetStatusFleetFactory.getCustomerCategory().then(
            function (res) {
                var tempCategory = res.data.Result;
                $scope.dataCusCategory = tempCategory.filter(function (type) {
                    return (type.ParentId == 2 || type.CustomerTypeId == 3);
                })
            }
        );
		
		$scope.SetStatusFleetJagaPersen = function () {
			var da_persen=document.getElementById("DaStatusFleetPersen").value;
			if(da_persen>100)
			{
				document.getElementById("DaStatusFleetPersen").value=100;
			}
		}

        $scope.clickUbah = function (selected) {
            $scope.selectRow = selected;
            var getId = "?CustomerId=" + $scope.selectRow.CustomerId;
            SetStatusFleetFactory.getDetail(getId).then(
                function (res) {
                    $scope.ubahTOPFleet = res.data.Result[0];
                    $scope.gridEditTOP.data = $scope.ubahTOPFleet.ListOfCustomerTOPDetail;
                }
            );
            $scope.mainForm = false;
            $scope.secondForm = true;
			//$scope.SetStatusFleet_DaTopKe=1;
        }

        $scope.btnTambah = function () {
            $scope.disabledBtnSimpan = false;
            angular.element('.ui.modal.tambahTOP').modal('show');
            
			//var daDefaultTopKe=$scope.gridEditTOP.data.length+1;
			//$scope.ubahTOPFleet.TOPKe = daDefaultTopKe;
			if($scope.gridEditTOP.data.length<=0)
			{
				$scope.ubahTOPFleet.TOPKe =1;
			}
			else if($scope.gridEditTOP.data.length>=1)
			{
				$scope.ubahTOPFleet.TOPKe =(Math.max.apply(Math,$scope.gridEditTOP.data.map(function(item){return item.TOPKe;})))+1;
			}
			
			
            $scope.ubahTOPFleet.Persentase = 0;
            $scope.ubahTOPFleet.Durasi = 0;
        }

        $scope.btnSimpan = function () { 
            // $scope.disabledBtnSimpan = true;          
            // try {
            //     $scope.ubahTOPFleet.Durasi = $scope.ubahTOPFleet.Durasi.getFullYear() + '-' +
            //         ('0' + ($scope.ubahTOPFleet.Durasi.getMonth() + 1)).slice(-2) + '-' +
            //         ('0' + $scope.ubahTOPFleet.Durasi.getDate()).slice(-2) + 'T' +
            //         (($scope.ubahTOPFleet.Durasi.getHours() < '10' ? '0' : '') + $scope.ubahTOPFleet.Durasi.getHours()) + ':' +
            //         (($scope.ubahTOPFleet.Durasi.getMinutes() < '10' ? '0' : '') + $scope.ubahTOPFleet.Durasi.getMinutes()) + ':' +
            //         (($scope.ubahTOPFleet.Durasi.getSeconds() < '10' ? '0' : '') + $scope.ubahTOPFleet.Durasi.getSeconds());

            //         $scope.ubahTOPFleet.Durasi.slice(0, 10);
            // } catch (e1) {
            //     $scope.ubahTOPFleet.Durasi = null;
            // }
            console.log('setan1', $scope.ubahTOPFleet);
            $scope.gridEditTOP.data.push({
                TOPKe: $scope.ubahTOPFleet.TOPKe,
                Percentage: $scope.ubahTOPFleet.Persentase,
                Durasi: $scope.ubahTOPFleet.Durasi
            });
            $scope.disabledBtnSimpan = true;
			console.log('setan2', $scope.gridEditTOP.data);
			//$scope.SetStatusFleet_DaTopKe=$scope.gridEditTOP.data.length+1;
            angular.element('.ui.modal.tambahTOP').modal('hide');           
        }

        $scope.btnBatal = function () {
            angular.element('.ui.modal.tambahTOP').modal('hide');
        }

        $scope.btnubahTerm = function () {
            var TotalPresentase = 0;
            if ($scope.ubahTOPFleet.ListOfCustomerTOPDetail.length > 0) {
                for (var i in $scope.gridEditTOP.data) {
                    
                    TotalPresentase = TotalPresentase + $scope.gridEditTOP.data[i].Percentage;
                
                }

                if (TotalPresentase != 100){
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Data Percentage Harus 100%",
                        type: 'warning'
                    });
                }else{
                    SetStatusFleetFactory.update($scope.ubahTOPFleet).then(
                        function (res) {
                            SetStatusFleetFactory.getData($scope.filter)
                                .then(
                                function (res) {
                                    $scope.grid.data = res.data.Result;
                                    $scope.loading = false;
                                },
                                function (err) {
                                    bsNotify.show(
                                        {
                                            title: "Gagal",
                                            content: "Data tidak ditemukan.",
                                            type: 'danger'
                                        }
                                    );
                                }
                                );
                            bsNotify.show({
                                title: "Sukses",
                                content: "Data berhasil disimpan",
                                type: 'success'
                            });
                        },
                        function (err) {
                            bsNotify.show({
                                title: "Gagal",
                                content: "Data gagal disimpan",
                                type: 'danger'
                            });
                        }
                    );
                    $scope.mainForm = true;
                    $scope.secondForm = false;

                }
                
            }

            else {
                SetStatusFleetFactory.update($scope.ubahTOPFleet).then(
                    function (res) {
                        SetStatusFleetFactory.getData($scope.filter)
                            .then(
                            function (res) {
                                $scope.grid.data = res.data.Result;
                                $scope.loading = false;
                            },
                            function (err) {
                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: "Data tidak ditemukan.",
                                        type: 'danger'
                                    }
                                );
                            }
                            );
                        bsNotify.show({
                            title: "Sukses",
                            content: "Data berhasil disimpan",
                            type: 'success'
                        });
                    },
                    function (err) {
                        bsNotify.show({
                            title: "Gagal",
                            content: "Data gagal disimpan",
                            type: 'danger'
                        });
                    }
                );
                $scope.mainForm = true;
                $scope.secondForm = false;
            }
            
        }

        $scope.clickhapus = function (row) {
            var index = $scope.gridEditTOP.data.indexOf(row.entity);
            $scope.gridEditTOP.data.splice(index, 1);
			
			//kode di bawah ini tadinya ga ada kalo bikin gara gara apus aja
			for(var i = 0; i < $scope.gridEditTOP.data.length; ++i)
			{
				var NomorinUlangTopKe=angular.copy(i)+1;
				$scope.gridEditTOP.data[i].TOPKe=angular.copy(NomorinUlangTopKe);
			}
        }

        $scope.btnKembaliToMainForm = function () {
            $scope.mainForm = true;
            $scope.secondForm = false;
            $scope.filter = [];
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function (rows) {

            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function (rows) {

        }

        var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u uib-tooltip="Setting TOP" tooltip-placement="right" ng-click="grid.appScope.$parent.clickUbah(row.entity)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></u></span>'
        var btnDel = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.clickhapus(row)" ></i>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'CustomerId', width: '7%', visible: false },
                { name: 'Status Fleet', field: 'CategoryFleetName' },
                { name: 'group Pelanggan', field: 'AFCOGroup' },
                { name: 'Sejumlah', field: 'BuyCount' },
                { name: 'Tahun', field: 'Tahun' },
                { name: 'Toyota ID/ Customer Code', cellTemplate: '<p style="margin:5px 0 0 5px">{{row.entity.ToyotaId}} - {{row.entity.CustomerCode}}</p>', width: '25%' },
                { name: 'Nama Pelanggan', field: 'CustomerName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '8%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };

        $scope.gridEditTOP = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 15,
            columnDefs: [
                { name: 'T O P Id-', field: 'TOPId', visible: false },
                { name: 'T O P ke -', field: 'TOPKe' },
                { name: 'Persentase', field: 'Percentage', width: '20%',cellTemplate:'<div style="padding:5px 0 0 5px">{{row.entity.Percentage}} %</div>'  },
                { name: 'Due Date (Hari)', field: 'Durasi', cellFilter: 'number:0' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '15%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnDel
                }
            ]
        };

        $scope.gridEditTOP.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };
    });