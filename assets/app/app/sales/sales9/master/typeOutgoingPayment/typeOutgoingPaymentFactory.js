angular.module('app')
  .factory('TypeOutgoingPayment', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTypeOutgoingPayment');
        //console.log('res=>',res);
// var da_json=[
//         {OutgoingPaymentId: "1",  OutgoingPaymentName: "name 1"},
//         {OutgoingPaymentId: "2",  OutgoingPaymentName: "name 2"},
//         {OutgoingPaymentId: "3",  OutgoingPaymentName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(typeOutgoingPayment) {
        return $http.post('/api/sales/PCategoryTypeOutgoingPayment', [{
                                            //AppId: 1,
                                            OutgoingPaymentName: typeOutgoingPayment.OutgoingPaymentName}]);
      },
      update: function(typeOutgoingPayment){
        return $http.put('/api/sales/PCategoryTypeOutgoingPayment', [{
                                            OutletId : typeOutgoingPayment.OutletId,
                                            OutgoingPaymentId: typeOutgoingPayment.OutgoingPaymentId,
                                            OutgoingPaymentName: typeOutgoingPayment.OutgoingPaymentName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTypeOutgoingPayment',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd