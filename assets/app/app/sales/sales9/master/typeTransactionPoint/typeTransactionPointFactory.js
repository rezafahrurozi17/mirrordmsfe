angular.module('app')
  .factory('TypeTransactionPoint', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTypeTransactionPoint');
        //console.log('res=>',res);
// var da_json=[
//         {TypeTransactionPointId: "1",  TypeTransactionPointName: "name 1"},
//         {TypeTransactionPointId: "2",  TypeTransactionPointName: "name 2"},
//         {TypeTransactionPointId: "3",  TypeTransactionPointName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(typeTransactionPoint) {
        return $http.post('/api/sales/PCategoryTypeTransactionPoint', [{
                                            //AppId: 1,
                                            TypeTransactionPointName: typeTransactionPoint.TypeTransactionPointName}]);
      },
      update: function(typeTransactionPoint){
        return $http.put('/api/sales/PCategoryTypeTransactionPoint', [{
                                            OutletId : typeTransactionPoint.OutletId,
                                            TypeTransactionPointId: typeTransactionPoint.TypeTransactionPointId,
                                            TypeTransactionPointName: typeTransactionPoint.TypeTransactionPointName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTypeTransactionPoint',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd