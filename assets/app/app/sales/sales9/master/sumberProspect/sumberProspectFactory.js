angular.module('app')
  .factory('SumberProspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryProspectSource');
        return res;
      },
      create: function(SumberProspect) {
        return $http.post('/api/sales/PCategoryProspectSource', [{
                                            //AppId: 1,
                                            ProspectSourceName: SumberProspect.ProspectSourceName}]);
      },
      update: function(SumberProspect){
        return $http.put('/api/sales/PCategoryProspectSource', [{
                                            ProspectSourceId: SumberProspect.ProspectSourceId,
                                            ProspectSourceName: SumberProspect.ProspectSourceName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryProspectSource',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd