angular.module('app')
    .controller('SumberProspectController', function($scope, $http, CurrentUser, SumberProspectFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mSumberProspect = null; //Model
    $scope.xRole={selected:[]};
	console.log("setan",$scope.user);
	
	var OperationSumberProspect="";
	$scope.ShowSimpanSumberProspect=false;
	//$scope.mSumberProspect_Enable=false;
	//$scope.SumberProspectTable=true;
	//$scope.SumberProspectDetail=false;

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
        SumberProspectFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){

        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){

    }
	
	// $scope.TambahSumberProspect = function() {
			// OperationSumberProspect="Insert";
			// $scope.ShowSumberProspectDetail=true;

			// $scope.SumberProspectTable=false;
			// $scope.ShowSimpanSumberProspect=true;
            // $scope.mSumberProspect={};
			// $scope.mSumberProspect_Enable=true;
			
        // }
		
	// $scope.KembaliDariSumberProspect = function() {
			// $scope.ShowSumberProspectDetail=false;
			// $scope.SumberProspectTable=true;
        // }

	// $scope.lihatSumberProspect = function(SelectedData) {
			// OperationSumberProspect="Look";
			// $scope.ShowSumberProspectDetail=true;
			// $scope.SumberProspectTable=false;
			// $scope.ShowSimpanSumberProspect=false;
			// $scope.mSumberProspect=angular.copy(SelectedData);
			// $scope.mSumberProspect_Enable=false;
			
			
        // }
		
	// $scope.editSumberProspect = function(SelectedData) {
			
			// OperationSumberProspect="Update";
			// $scope.ShowSumberProspectDetail=true;
			// $scope.SumberProspectTable=false;
			
			// $scope.ShowSimpanSumberProspect=true;
			// $scope.mSumberProspect=angular.copy(SelectedData);

			// $scope.mSumberProspect_Enable=true;
			
        // }
		
	$scope.doCustomSave = function (mdl,mode) {

		
		if(mode=="create")
		{
			SumberProspectFactory.create($scope.mSumberProspect).then(
				function(res) {
					SumberProspectFactory.getData().then(
						function(res){
							$scope.grid.data = res.data.Result;
							$scope.formApi.setMode("grid");
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							
							return res.data;
						},
						function(err){
							if(err.data.Message=="Role Anda Tidak Berhak Merubah")
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Role Anda Tidak Berhak Merubah",
										type: 'danger'
									}
								);
							}
							else
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			SumberProspectFactory.update($scope.mSumberProspect).then(
				function(res) {
					SumberProspectFactory.getData().then(
						function(res){
							$scope.grid.data = res.data.Result;
							$scope.formApi.setMode("grid");
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							if(err.data.Message="Role Anda Tidak Berhak Merubah")
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Role Anda Tidak Berhak Merubah",
										type: 'danger'
									}
								);
							}
							else
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
							
						}
			)
		}
		
        
        
    }
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Kode Sumber Prospect',    field:'ProspectSourceId', width:'7%', visible:false},
            { name:'Sumber Prospect', field:'ProspectSourceName' },
			
        ]
    };
});


