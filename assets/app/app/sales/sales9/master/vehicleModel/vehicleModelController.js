angular.module('app')
    .controller('VehicleModelController', function($scope, $http, $httpParamSerializer, CurrentUser, VehicleModel,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVehicleModel = null; //Model
    $scope.cVehicleModel = null; //Collection
    $scope.xVehicleModel = {};
    $scope.xVehicleModel.selected=[];
    $scope.optionsOutlet_OutletRaw = {};
    $scope.filter={GroupDealerId:null,OutletId:null};

    VehicleModel.getDealerDropdown().then(function (res) {
        $scope.optionsOutlet_Dealer = res.data.Result;
        $scope.MasterOutletDisableFilterDealer=false;
        $scope.MasterOutletDisableFilterOutlet=false;
        //return $scope.optionsOutlet_Dealer;

        if($scope.user.RoleName == "Admin HO")
        {
            $scope.filter.GroupDealerId=$scope.user.GroupDealerId;
            //$scope.MasterOutletSearchDealerBrubah();
            $scope.MasterOutletDisableFilterDealer=true;
            VehicleModel.getOutletDropdown().then(function (res) {
                $scope.optionsOutlet_OutletRaw = res.data.Result;
                
                var Filtered_Outlet1 = [{ OutletId: null, OutletName: null }];
                Filtered_Outlet1.splice(0, 1);
                for (var i = 0; i < $scope.optionsOutlet_OutletRaw.length; ++i) {
                    if ($scope.optionsOutlet_OutletRaw[i].DealerId == $scope.filter.GroupDealerId) {
                        Filtered_Outlet1.push({ OutletId: $scope.optionsOutlet_OutletRaw[i].OutletId,OutletName: $scope.optionsOutlet_OutletRaw[i].OutletName });
                    }
                }
                $scope.optionsOutlet_Outlet = Filtered_Outlet1;
                $scope.filter.OutletId=$scope.user.OutletId;
            });
        } else if($scope.user.RoleName == "KACAB" || $scope.user.RoleName == "ADH" || $scope.user.RoleName == "Admin Unit" || $scope.user.RoleName == "SUPERVISOR SALES"){
            $scope.filter.GroupDealerId = $scope.user.GroupDealerId;
            $scope.MasterOutletDisableFilterDealer=true;
            $scope.MasterOutletDisableFilterOutlet=true;
            VehicleModel.getOutletDropdown().then(
                function(res){
                    $scope.optionsOutlet_OutletRaw=res.data.Result;

                    var Filtered_Outlet1 = [{
                        OutletId: null,
                        OutletName: null
                    }];
                    Filtered_Outlet1.splice(0, 1);
                    for(var i=0; i<$scope.optionsOutlet_OutletRaw.length; i++){
                        if($scope.optionsOutlet_OutletRaw[i].DealerId == $scope.filter.GroupDealerId){
                            Filtered_Outlet1.push({
                                OutletId: $scope.optionsOutlet_OutletRaw[i].OutletId, OutletName: $scope.optionsOutlet_OutletRaw[i].OutletName
                            });
                        }
                    }
                    $scope.optionsOutlet_Outlet=Filtered_Outlet1;
                    $scope.filter.OutletId=$scope.user.OutletId;
                }
            )
        }
    });
    
    VehicleModel.getOutletDropdown().then(function (res) {
        $scope.optionsOutlet_OutletRaw = res.data.Result;
            //return $scope.optionsOutlet_Outlet; 
    });

    $scope.MasterOutletSearchDealerBrubah = function() {
        var Filtered_Outlet1 = [{ OutletId: null, OutletName: null }];
        Filtered_Outlet1.splice(0, 1);
        for (var i = 0; i < $scope.optionsOutlet_OutletRaw.length; ++i) {
            if ($scope.optionsOutlet_OutletRaw[i].DealerId == $scope.filter.GroupDealerId) {
                Filtered_Outlet1.push({ OutletId: $scope.optionsOutlet_OutletRaw[i].OutletId,OutletName: $scope.optionsOutlet_OutletRaw[i].OutletName });
            }
        }
        $scope.optionsOutlet_Outlet = Filtered_Outlet1;
        $scope.filter.OutletId = undefined;
        // $scope.grid.data = [];
    };

    // ----------------------------------
    // Get Data
    // ----------------------------------
    // $scope.getData = function() {
    //     VehicleModel.getData().then(
    //         function(res){
    //             $scope.grid.data = res.data.Result;
    //             console.log("data=>",res.data);
    //             $scope.VehicleModelData = res.data.Result;
    //             $scope.loading=false;
    //             return res.data;
    //         },
    //         function(err){
    //             console.log("err=>",err);
    //         }
    //     );
    // }

    $scope.getData = function() {
        // if($scope.filter.GroupDealerId == undefined || $scope.filter.GroupDealerId == null || $scope.filter.GroupDealerId == ""){
        //     bsNotify.show({
        //         title: "Peringatan",
        //         content: "Harap isi data mandatory terlebih dahulu",
        //         type: 'warning'
        //     });
        //     $scope.filter.GroupDealerId = undefined;
        // } else {
            var param=$httpParamSerializer($scope.filter);
            VehicleModel.getData(param).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    bsNotify.show(
                        {
                            title: "gagal",
                            content: "Data tidak ditemukan.",
                            type: 'danger'
                        }
                    );
                }
            );
        // }
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    // var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
    //                                 ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 ' onclick="this.blur()"'+
    //                                 '>'+
    //                                 '<i ng-class="'+
    //                                                   '{ \'fa fa-fw fa-lg fa-toggle-on\' : (row.entity.SpotOrder),'+
    //                                                   '  \'fa fa-fw fa-lg fa-toggle-off\': (!row.entity.SpotOrder),'+
    //                                                   '}'+
    //                                 '">'+
    //                                 '</i>'+
    //                             '</button>';



    var btnActionEditTemplate = '<bscheckbox ng-model="row.entity.SpotOrder"'+
                                            ' ng-disabled="true"'+
                                            ' true-value = "true"'+
                                            ' false-value = "false"'+
                                            ' styles="margin-top:-10px;margin-left:8px;">'+
                                '</bscheckbox>';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Model Id',    field:'VehicleModelId', width:'7%' , visible: false},
            { name:'Kode Model', field:'VehicleModelCode', width: '15%' },
            { name:'Model', field:'VehicleModelName' },
            // { name:'Brand Name',  field: 'BrandName' },
            { name: 'Tipe', field: 'VehicleTypeName' },
            { name:'Katashiki', field:'KatashikiCode' },
            { name:'Suffix', field:'SuffixCode' },
            { name:'Assembly Type', field:'AssemblyType' },
            { name:'Spot Order',  field: 'SpotOrder' },
            { name:'Spot Order', allowCellFocus: false,
                                  cellTemplate: btnActionEditTemplate,enableFiltering: false, visible: true
            },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
