angular.module('app')
  .factory('VehicleModel', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        //var res=$http.get('/master/vehiclemodel');
        if(param == undefined || param == null || param == ""){
          param="";
        } else {
          param="&"+param;
        }
        var res = $http.get('/api/sales/MasterVehicleModel?start=1&limit=10000&Id=0'+param);
        //console.log('res=>',res);
        return res;
      },
      getDataforDDL: function() {
        //var res=$http.get('/master/vehiclemodel');
        var res = $http.get('/api/as/VehicleModel');
        //console.log('res=>',res);
        return res;
      },

      getDealerDropdown: function(){
        var res=$http.get('/api/sales/MsitesGroupDealer');
        return res;
      },

      getOutletDropdown: function(){
        var res=$http.get('/api/sales/GetMSitesOutlet');
        return res;
      },

      create: function(fleet) {
        return $http.post('/master/vehiclemodel/create', {
          VehicleModelCode: fleet.VehicleModelCode,
          VehicleModelName: fleet.VehicleModelName,
          BrandName: fleet.BrandName,
          SpotOrder: fleet.spotOrder
        });
      },
      update: function(fleet) {
        console.log("=>update:", JSON.stringify([{
          VehicleModelId: fleet.VehicleModelId,
          SpotOrder: fleet.SpotOrder
        }]));
        //'/master/vehiclemodel/update'
        //console.log("update=>",fleet);

        return $http.put('/api/param/VehicleModels/Multiple', [{
          VehicleModelId: fleet.VehicleModelId,
          SpotOrder: fleet.SpotOrder
        }]);
      },
      delete: function(id) {
        return $http.post('/master/vehiclemodel/delete', {
          VehicleModelId: VehicleModelId
        });
      },
    }
  });
