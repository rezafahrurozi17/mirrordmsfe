angular.module('app')
    .controller('CustomerListUnsatifiedController', function($scope, $http, CurrentUser, CustomerListUnsatifiedFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCustomerListUnsatified = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=CustomerListUnsatifiedFactory.getData();         
        $scope.loading=false;
    },
    function(err){
       bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'CustomerDetailUnsatifiedId',    field:'CustomerDetailUnsatifiedId', width:'7%', visible:false },
            { name:'Customer Id', field:'CustomerId' },
            { name:'Date', field:'Date' },
            { name:'CSL', field:'CSL' },
            { name:'IDI', field:'IDI' },
            { name:'CustomerComplaint', field:'CustomerComplaint' },
            { name:'CustomerSurvey', field:'CustomerSurvey' }
        ]
    };
});








