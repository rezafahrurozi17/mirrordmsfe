angular.module('app')
  .factory('CustomerListUnsatifiedFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
            "CustomerDetailUnsatifiedId" : 1,
            "CustomerId" : "TC2",
            "Date" : "Ganti Ban",
            "CSL" : "PDC 2",
            "IDI" : "PDC 2",
            "CustomerComplaint" : "PDC 2",
            "CustomerSurvey" : "PDC 2"
          },
          {
            "CustomerDetailUnsatifiedId" : 2,
            "CustomerId" : "TC2",
            "Date" : "Ganti Ban",
            "CSL" : "PDC 2",
            "IDI" : "PDC 2",
            "CustomerComplaint" : "PDC 2",
            "CustomerSurvey" : "PDC 2"
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 







