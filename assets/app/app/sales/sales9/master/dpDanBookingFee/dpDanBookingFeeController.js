angular.module('app')
    .controller('DpDanBookingFeeController', function($scope,$filter, $http, CurrentUser, DpDanBookingFeeFactoryMaster,$timeout,bsNotify,uiGridConstants, uiGridGroupingConstants) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.Excel_AfterGet_DpDanBookingFee = {};
	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	$scope.DpDanBookingFeeTable=true;
	$scope.DpDanBookingFeeDetail=false;
	var OperationDpDanBookingFee="";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mDpDanBookingFee = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={VehicleModelId:null};
	
	$scope.dateOptionsDpBookingFeeValidOn = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
	$scope.dateOptionsDpBookingFeeValidUntil = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

    //----------------------------------
    // Get Data test
	//----------------------------------
	
	$scope.returnVal = function(val){
		if(val > 10000000000){
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal tidak boleh lebih dari 10 Milyar.",
					type: 'warning'
				}
			);
			$scope.mDpDanBookingFee.NominalBookingFee = 10000000000
		}else{
			$scope.mDpDanBookingFee.NominalBookingFee = val
		}
		
	}

	$scope.returnVal1 = function(val){
		if(val > 10000000000){
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal tidak boleh lebih dari 10 Milyar.",
					type: 'warning'
				}
			);
			$scope.mDpDanBookingFee.NominalDP = 10000000000
		}else{
			$scope.mDpDanBookingFee.NominalDP = val
		}
		
	}
	
	DpDanBookingFeeFactoryMaster.getDataLeasing().then(function (res) {
			$scope.optionsLeasing = res.data.Result;
			$scope.DaValidOnDpBookingFeeChanged();
			console.log('get data leasing ===>', $scope.optionsLeasing);
			return $scope.optionsLeasing;

		});
	
	//buat model	
	DpDanBookingFeeFactoryMaster.getDataVehicleModel().then(function(res) {
            $scope.optionsModelDpDanBookingFee = res.data.Result;
            return $scope.optionsModelDpDanBookingFee;
        });
	//buat tipe
	$scope.RawTipe_DpDanBookingFee = "";
        DpDanBookingFeeFactoryMaster.getDataVehicleType().then(function(res) {
            $scope.RawTipe_DpDanBookingFee = res.data.Result;
            return $scope.RawTipe_DpDanBookingFee;
        });
	
	$scope.ModelKendaraan_DpDanBookingFeeChanged = function() {
            var Filtered_Tipe1 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe1.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe_DpDanBookingFee.length; ++i) {
                if ($scope.RawTipe_DpDanBookingFee[i].VehicleModelId == $scope.mDpDanBookingFee.VehicleModelId) {
                    Filtered_Tipe1.push({ VehicleTypeId: $scope.RawTipe_DpDanBookingFee[i].VehicleTypeId, VehicleModelId: $scope.RawTipe_DpDanBookingFee[i].VehicleModelId, Description: $scope.RawTipe_DpDanBookingFee[i].Description, KatashikiCode: $scope.RawTipe_DpDanBookingFee[i].KatashikiCode, SuffixCode: $scope.RawTipe_DpDanBookingFee[i].SuffixCode });
                }
            }
            $scope.optionsTipe_DpDanBookingFeeChanged = Filtered_Tipe1;
        };

	$scope.DpBookingFeeNominalBookingFeeChanged = function () {
		if($scope.mDpDanBookingFee.NominalBookingFee>$scope.mDpDanBookingFee.NominalDP)
		{
			$scope.mDpDanBookingFee.NominalDP=$scope.mDpDanBookingFee.NominalBookingFee;
			bsNotify.show(
						{
							title: "Peringatan",
							content: "DP tidak boleh lebih kecil dr booking fee.",
							type: 'warning'
						}
					);
		}
	}
	
	$scope.DaValidOnDpBookingFeeChanged = function () {
		$scope.dateOptionsDpBookingFeeValidOn.minDate=new Date();
		
		if($scope.mDpDanBookingFee.ValidOn<=$scope.dateOptionsDpBookingFeeValidOn.minDate)
		{
			$scope.mDpDanBookingFee.ValidOn=$scope.dateOptionsDpBookingFeeValidOn.minDate;
		}
		
		$scope.dateOptionsDpBookingFeeValidUntil.minDate=$scope.mDpDanBookingFee.ValidOn;
		if($scope.mDpDanBookingFee.ValidOn>=$scope.mDpDanBookingFee.ValidUntil)
		{
				$scope.mDpDanBookingFee.ValidUntil=$scope.mDpDanBookingFee.ValidOn;
		}
	}


	$scope.onSelectedLeasingChange= function(selectedLeasing){
		console.log('selectedLeasing ===>',selectedLeasing);
	}
	
	$scope.KembaliDariDetailDpDanBookingFee = function() {
			$scope.DpDanBookingFeeDetail=false;
			$scope.DpDanBookingFeeTable=true;
 }
	
	$scope.TambahDpDanBookingFee = function() {
			OperationDpDanBookingFee="Insert";
			$scope.DpDanBookingFeeDetail=true;
			$scope.DpDanBookingFeeTable=false;
			$scope.ShowSimpanDpDanBookingFee=true;
   $scope.mDpDanBookingFee={};
			$scope.mDpDanBookingFee_Enable=true;

			DpDanBookingFeeFactoryMaster.getDataLeasing().then(function (res) {
				$scope.optionsLeasing = res.data.Result;
				console.log('get data leasing ===>', $scope.optionsLeasing);
				return $scope.optionsLeasing;
			});
 }
		
	$scope.lihatDpDanBookingFee = function(SelectedData) {
			OperationDpDanBookingFee="Look";
			$scope.DpDanBookingFeeDetail=true;
			$scope.DpDanBookingFeeTable=false;
			$scope.ShowSimpanDpDanBookingFee=false;
			$scope.mDpDanBookingFee=angular.copy(SelectedData);
			$scope.mDpDanBookingFee_Enable=false;
			
			
        }
		
	$scope.editDpDanBookingFee = function(SelectedData) {
			
			OperationDpDanBookingFee="Update";
			$scope.DpDanBookingFeeDetail=true;
			$scope.DpDanBookingFeeTable=false;
			$scope.ShowSimpanDpDanBookingFee=true;
			$scope.mDpDanBookingFee=angular.copy(SelectedData);

			$scope.mDpDanBookingFee_Enable=true;
			
        }	
	
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
	
	$scope.DpDanBookingFee_UploadExcelTemplate = function() {
		//VirtualAccount_ExcelData
		var DpDanBookingFeeUploadAman=true;
		for(var i = 0; i < $scope.VirtualAccount_ExcelData.length; ++i)
		{	
			$scope.VirtualAccount_ExcelData[i].VehicleTypeId=Number ($scope.VirtualAccount_ExcelData[i].VehicleTypeId);
			$scope.VirtualAccount_ExcelData[i].LeasingId=null;
			$scope.VirtualAccount_ExcelData[i].LeasingName=null;
			//new Date(yyyy,mm,dd) in mm parameter got adjusted by 1 always so must be -1 from excel source
			
			var RawValidOn = $scope.VirtualAccount_ExcelData[i].ValidOn.toString();
			var ProcessedValidOn = {    
				value: $filter('date')(new Date(parseInt(RawValidOn.substring(0, 4)), parseInt(RawValidOn.substring(4, 6))-1, parseInt(RawValidOn.substring(6, 8))), 'yyyy-MM-dd')
				};

			var RawValidUntil = $scope.VirtualAccount_ExcelData[i].ValidUntil.toString();
			var ProcessedValidUntil = {    
				value: $filter('date')(new Date(parseInt(RawValidUntil.substring(0, 4)), parseInt(RawValidUntil.substring(4, 6))-1, parseInt(RawValidUntil.substring(6, 8))), 'yyyy-MM-dd')
				};	

			$scope.VirtualAccount_ExcelData[i].ValidOn=ProcessedValidOn.value;
			$scope.VirtualAccount_ExcelData[i].ValidUntil=ProcessedValidUntil.value;
			
			// $scope.VirtualAccount_ExcelData[i].ValidOn=ProcessedValidOn.getFullYear()+'-'
											// +('0' + (ProcessedValidOn.getMonth()+1)).slice(-2)+'-'
											// +('0' + ProcessedValidOn.getDate()).slice(-2)+'T'
											// +((ProcessedValidOn.getHours()<'10'?'0':'')+ ProcessedValidOn.getHours())+':'
											// +((ProcessedValidOn.getMinutes()<'10'?'0':'')+ ProcessedValidOn.getMinutes())+':'
											// +((ProcessedValidOn.getSeconds()<'10'?'0':'')+ ProcessedValidOn.getSeconds());
											
			// $scope.VirtualAccount_ExcelData[i].ValidUntil=ProcessedValidUntil.getFullYear()+'-'
											// +('0' + (ProcessedValidUntil.getMonth()+1)).slice(-2)+'-'
											// +('0' + ProcessedValidUntil.getDate()).slice(-2)+'T'
											// +((ProcessedValidUntil.getHours()<'10'?'0':'')+ ProcessedValidUntil.getHours())+':'
											// +((ProcessedValidUntil.getMinutes()<'10'?'0':'')+ ProcessedValidUntil.getMinutes())+':'
											// +((ProcessedValidUntil.getSeconds()<'10'?'0':'')+ ProcessedValidUntil.getSeconds());								
			
			for(var y = 0; y < $scope.RawTipe_DpDanBookingFee.length; ++y)
			{
				if($scope.VirtualAccount_ExcelData[i].VehicleTypeId.toString()==$scope.RawTipe_DpDanBookingFee[y].VehicleTypeId.toString())
				{
					$scope.VirtualAccount_ExcelData[i].VehicleModelId=$scope.RawTipe_DpDanBookingFee[y].VehicleModelId;
				}
			}

			if($scope.VirtualAccount_ExcelData[i].VehicleModelName==""||$scope.VirtualAccount_ExcelData[i].VehicleTypeName==""||$scope.VirtualAccount_ExcelData[i].VehicleTypeId==""||$scope.VirtualAccount_ExcelData[i].VehicleTypeId<=0||$scope.VirtualAccount_ExcelData[i].NominalBookingFee==""||$scope.VirtualAccount_ExcelData[i].NominalBookingFee<=0||$scope.VirtualAccount_ExcelData[i].NominalDP==""||$scope.VirtualAccount_ExcelData[i].NominalDP<=0||$scope.VirtualAccount_ExcelData[i].ValidOn==""||$scope.VirtualAccount_ExcelData[i].ValidUntil=="")
			{
				DpDanBookingFeeUploadAman=false;
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Field Tidak Boleh Kosong.",
							type: 'warning'
						}
					);
				break;	
			}
		}
		if(DpDanBookingFeeUploadAman==true)
		{
			
			DpDanBookingFeeFactoryMaster.createUploadExcel($scope.VirtualAccount_ExcelData).then(
					function(res) {
						bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
						$scope.VirtualAccount_ExcelData=[];
						$scope.loading=false;
						document.getElementById('uploadDpDanBookingFee').value=null;
					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
		}
	}
	
	$scope.DpDanBookingFee_DownloadExcelTemplate = function() {
            var excelData = [];
            var fileName = "";

            excelData = [];
            fileName = "DpDanBookinFee_Template";
			var DaPaksaan="";
			DpDanBookingFeeFactoryMaster.getDataBuatPaksaanDownload().then(
							function(res){
								DaPaksaan= res.data.Result;
								// for(var i = 0; i < $scope.optionsModelDpDanBookingFee.length; ++i)
								// {
								// 	for(var x = 0; x < $scope.RawTipe_DpDanBookingFee.length; ++x)
								// 	{
										
								// 		var da_shitty_tipe="";
								// 		if($scope.RawTipe_DpDanBookingFee[x].VehicleModelId==$scope.optionsModelDpDanBookingFee[i].VehicleModelId)
								// 		{
								// 			var Da_Shitty_Akurin_Nominal_Dp=0;
								// 			var Da_Shitty_Akurin_Nominal_Booking_Fee=0;
								// 			var Da_Shitty_Akurin_Tanggal_ValidOn= new Date();
								// 			var Da_Shitty_Akurin_Tanggal_ValidUntil= new Date();
								// 			for(var y = 0; y < DaPaksaan.length; ++y)
								// 			{
								// 				if($scope.RawTipe_DpDanBookingFee[x].VehicleTypeId==DaPaksaan[y].VehicleTypeId)
								// 				{
								// 					Da_Shitty_Akurin_Nominal_Dp=DaPaksaan[y].NominalDP;
								// 					Da_Shitty_Akurin_Nominal_Booking_Fee=DaPaksaan[y].NominalBookingFee;
								// 					Da_Shitty_Akurin_Tanggal_ValidOn=DaPaksaan[y].ValidOn;
								// 					Da_Shitty_Akurin_Tanggal_ValidUntil=DaPaksaan[y].ValidUntil;
								// 					break;
								// 				}
								// 			}
											
								// 			da_shitty_tipe=$scope.RawTipe_DpDanBookingFee[x].Description+" "+$scope.RawTipe_DpDanBookingFee[x].KatashikiCode+" "+$scope.RawTipe_DpDanBookingFee[x].SuffixCode;
								// 			Da_Shitty_Akurin_Tanggal_ValidOn = formatDate(Da_Shitty_Akurin_Tanggal_ValidOn);
								// 			Da_Shitty_Akurin_Tanggal_ValidUntil = formatDate(Da_Shitty_Akurin_Tanggal_ValidUntil);
								// 			excelData.push({ VehicleModelName:$scope.optionsModelDpDanBookingFee[i].VehicleModelName,VehicleTypeName:da_shitty_tipe,VehicleTypeId:$scope.RawTipe_DpDanBookingFee[x].VehicleTypeId,NominalBookingFee:Da_Shitty_Akurin_Nominal_Booking_Fee,NominalDP:Da_Shitty_Akurin_Nominal_Dp,ValidOn:""+Da_Shitty_Akurin_Tanggal_ValidOn.toString().slice(0,10).replace(/-/g,""),ValidUntil:""+Da_Shitty_Akurin_Tanggal_ValidUntil.toString().slice(0,10).replace(/-/g,"")});
								// 		}
										
										
										
								// 	}
								// }
								for(var i=0; i< DaPaksaan.length; i++){
									excelData.push({ 
										VehicleModelName:DaPaksaan[i].VehicleModelName,
										VehicleTypeName:DaPaksaan[i].VehicleTypeName,									
										VehicleTypeId:DaPaksaan[i].VehicleTypeId,
										NominalBookingFee:DaPaksaan[i].NominalBookingFee,
										NominalDP:DaPaksaan[i].NominalDP,
										ValidOn:$filter('date')(DaPaksaan[i].ValidOn,'yyyyMMdd'),
										ValidUntil:$filter('date')(DaPaksaan[i].ValidUntil, 'yyyyMMdd')
									});
								}


								XLSXInterface.writeToXLSX(excelData, fileName);
							}
						);
			
        }
	
	$scope.SimpanDpDanBookingFee = function () {


		if($scope.mDpDanBookingFee.NominalBookingFee>$scope.mDpDanBookingFee.NominalDP)
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Simpan data gagal DP tidak boleh lebih kecil dr booking fee.",
							type: 'warning'
						}
					);
		}
		else if($scope.mDpDanBookingFee.NominalBookingFee<=$scope.mDpDanBookingFee.NominalDP)
		{
			
			for(var i = 0; i < $scope.optionsLeasing.length; ++i)
			{	
				if($scope.optionsLeasing[i].LeasingId == $scope.mDpDanBookingFee.LeasingId)
				{
					$scope.mDpDanBookingFee.LeasingName=$scope.optionsLeasing[i].LeasingName;
					break;
				}
			}
			var da_mDpDanBookingFee=angular.copy($scope.mDpDanBookingFee);
			
			
			if(OperationDpDanBookingFee=="Insert")
			{
				
				DpDanBookingFeeFactoryMaster.create(da_mDpDanBookingFee).then(
					function(res) {
						DpDanBookingFeeFactoryMaster.getData($scope.filter).then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.DaValidOnDpBookingFeeChanged();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			else if(OperationDpDanBookingFee=="Update")
			{
				DpDanBookingFeeFactoryMaster.update(da_mDpDanBookingFee).then(
					function(res) {
						DpDanBookingFeeFactoryMaster.getData($scope.filter).then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.DaValidOnDpBookingFeeChanged();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			//$scope.formApi.setMode("grid");
			$scope.KembaliDariDetailDpDanBookingFee();
		}
		
		
    }
	
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------

	
	$scope.getData = function() {
        DpDanBookingFeeFactoryMaster.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                $scope.DaValidOnDpBookingFeeChanged();
                
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
	
	
	
	$scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',    field:'MasterVehicleBookingFeeDPId', width:'7%', visible:false},
            { name:'Model', field:'VehicleModelName' },
			{ name:'Tipe', field:'Description' },
			// { name:'Leasing', field:'LeasingName' },
			{ name:'Nominal Booking Fee', field:'NominalBookingFee' ,cellFilter: 'currency:"Rp."',
					treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }},
			{ name:'Nominal DP', field:'NominalDP' ,cellFilter: 'currency:"Rp."',
					treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }},
			{ name:'Berlaku Dari', field:'ValidOn', cellFilter: 'date:\'dd-MM-yyyy\'' },
			{ name:'Berlaku Sampai', field:'ValidUntil', cellFilter: 'date:\'dd-MM-yyyy\'' },
			{ name:'Action', cellTemplate: '<i title="Lihat" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.lihatDpDanBookingFee(row.entity)" ></i>	<i title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.editDpDanBookingFee(row.entity)" ></i>' },
        ]
    };
	
	$scope.loadXLSDpDanBookingFee = function(ExcelFile) {
            var ExcelData = [];
            var myEl = angular.element(document.querySelector('#uploadDpDanBookingFee')); //ambil elemen dari dokumen yang di-upload 

            XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
                ExcelData = json[0];
				$scope.VirtualAccount_ExcelData = json;
            });
		}
		
		function formatDate(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
		
			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;
		
			return [year, month, day].join('-');
		}
		
		// $scope.ExecuteDpDanBookingFeeUtilityAFterUpload = function() {
            // //ini biar kalo dicancel datanya jadi kosong
            // $scope.Excel_AfterGet_DpDanBookingFee = {};
			// document.getElementById('DpDanBookingFeeFileUploadName').value=null;
            // // $scope.NumberOfValidData = 0;
        // }

        // $scope.$watch("Excel_AfterGet_DpDanBookingFee", function(newValue, oldValue) {
            // if ($scope.Excel_AfterGet_DpDanBookingFee.length > 0) {
                // $scope.CekValidity_DpDanBookingFee();
            // }
        // });
	
		
});


// angular.module('app').directive("fileread", [function() {
		// return {
			// scope: {
				// fileread: "=",
				// UtilityCallFromDirective: "&utilityAfterUploadFn"

			// },
			// link: function(scope, element, attributes) {
				// element.bind("change", function(changeEvent) {
					// var reader = new FileReader();
					// reader.onload = function(loadEvent) {
						// scope.$apply(function() {
							// scope.fileread = loadEvent.target.result;
							// //Base64.decode(scope.fileread);

							// var data = new Uint8Array(data64toBin(scope.fileread));
							// var arr = new Array();
							// for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
							// var bstr = arr.join("");

							// /* Call XLSX */
							// var workbook = XLSX.read(bstr, { type: "binary" });

							// /* DO SOMETHING WITH workbook HERE */
							// var first_sheet_name = workbook.SheetNames[0];
							// /* Get worksheet */
							// var worksheet = workbook.Sheets[first_sheet_name];

							// //console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
							// var ToShowAtController = XLSX.utils.sheet_to_json(worksheet, { raw: true });
							// scope.fileread = ToShowAtController;

						// });




					// }
					// try {
						// reader.readAsDataURL(changeEvent.target.files[0]);
					// } catch (kalo_di_cancel) {
						// //sementara pake ini dulu

						// scope.UtilityCallFromDirective();
					// }

				// });
			// }
		// }
	// }]);


