angular.module('app')
  .factory('DpDanBookingFeeFactoryMaster', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		//var Da_Start_From=(20*(DaPage-1))+1;
		var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/FFinanceMasterVehicleBookingFeeDPHO/filter/?'+param);
        return res;
      },
	  getDataBuatPaksaanDownload: function() {
        var res=$http.get('/api/sales/FFinanceMasterVehicleBookingFeeDPHO/NextStart');
        return res;
      },
	  getDataSearch: function(DaPage,da_value) {
		var Da_Start_From=(20*(DaPage-1))+1;
        var res=$http.get('/api/sales/FFinanceMasterVehicleBookingFeeDPHO?start='+Da_Start_From+'&limit=20&filterData=VehicleModelId|'+da_value);
        return res;
      },
	  getDataVehicleModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas'); 
        return res;
      },
	  getDataVehicleType: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');	  
        return res;
      },
	  getDataLeasing: function() {
        var res=$http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000');
        return res;
      },
      create: function(DpDanBookingFee) {
        return $http.post('/api/sales/FFinanceMasterVehicleBookingFeeDPHO', [{
                                            //AppId: 1,
											VehicleModelId: DpDanBookingFee.VehicleModelId,
											VehicleTypeId: DpDanBookingFee.VehicleTypeId,
											LeasingId: DpDanBookingFee.LeasingId,
											LeasingName: DpDanBookingFee.LeasingName,
											NominalBookingFee: DpDanBookingFee.NominalBookingFee,
											NominalDP: DpDanBookingFee.NominalDP,
											ValidOn: DpDanBookingFee.ValidOn.getFullYear()+'-'
											+('0' + (DpDanBookingFee.ValidOn.getMonth()+1)).slice(-2)+'-'
											+('0' + DpDanBookingFee.ValidOn.getDate()).slice(-2)+'T'
											+((DpDanBookingFee.ValidOn.getHours()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getHours())+':'
											+((DpDanBookingFee.ValidOn.getMinutes()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getMinutes())+':'
											+((DpDanBookingFee.ValidOn.getSeconds()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getSeconds()),
											
											ActiveBit: true,
											ValidUntil: DpDanBookingFee.ValidUntil.getFullYear()+'-'
											+('0' + (DpDanBookingFee.ValidUntil.getMonth()+1)).slice(-2)+'-'
											+('0' + DpDanBookingFee.ValidUntil.getDate()).slice(-2)+'T'
											+((DpDanBookingFee.ValidUntil.getHours()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getHours())+':'
											+((DpDanBookingFee.ValidUntil.getMinutes()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getMinutes())+':'
											+((DpDanBookingFee.ValidUntil.getSeconds()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getSeconds())}]);
      },
	  createUploadExcel: function(DpDanBookingFee) {
        return $http.post('/api/sales/FFinanceMasterVehicleBookingFeeDPHO', DpDanBookingFee);
      },
      update: function(DpDanBookingFee){
        return $http.put('/api/sales/FFinanceMasterVehicleBookingFeeDPHO', [{
                                            MasterVehicleBookingFeeDPId: DpDanBookingFee.MasterVehicleBookingFeeDPId,
											VehicleModelId: DpDanBookingFee.VehicleModelId,
											VehicleTypeId: DpDanBookingFee.VehicleTypeId,
											LeasingId: DpDanBookingFee.LeasingId,
											LeasingName: DpDanBookingFee.LeasingName,
											NominalBookingFee: DpDanBookingFee.NominalBookingFee,
											NominalDP: DpDanBookingFee.NominalDP,
											ValidOn: DpDanBookingFee.ValidOn.getFullYear()+'-'
											+('0' + (DpDanBookingFee.ValidOn.getMonth()+1)).slice(-2)+'-'
											+('0' + DpDanBookingFee.ValidOn.getDate()).slice(-2)+'T'
											+((DpDanBookingFee.ValidOn.getHours()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getHours())+':'
											+((DpDanBookingFee.ValidOn.getMinutes()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getMinutes())+':'
											+((DpDanBookingFee.ValidOn.getSeconds()<'10'?'0':'')+ DpDanBookingFee.ValidOn.getSeconds()),
											
											ActiveBit: true,
											ValidUntil: DpDanBookingFee.ValidUntil.getFullYear()+'-'
											+('0' + (DpDanBookingFee.ValidUntil.getMonth()+1)).slice(-2)+'-'
											+('0' + DpDanBookingFee.ValidUntil.getDate()).slice(-2)+'T'
											+((DpDanBookingFee.ValidUntil.getHours()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getHours())+':'
											+((DpDanBookingFee.ValidUntil.getMinutes()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getMinutes())+':'
											+((DpDanBookingFee.ValidUntil.getSeconds()<'10'?'0':'')+ DpDanBookingFee.ValidUntil.getSeconds())}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FFinanceMasterVehicleBookingFeeDPHO',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd