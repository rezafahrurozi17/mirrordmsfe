angular.module('app')
    .controller('PelanggaranWilayahController', function($scope, $http, CurrentUser, PelanggaranWilayahFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPelanggaranWilayah = null; //Model
    $scope.xRole={selected:[]};
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').remove();
		  angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').remove();
		  angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').remove();
		  angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').remove();
		  
		  angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Wilayah').remove();
		  angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').remove();
		  angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kota').remove();
		  angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kecamatan').remove();
		});
	
	$scope.PelanggaranWilayah_WilayahModal = function () {
		setTimeout(function() {     
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').modal('refresh'); 
		}, 0);
				
		setTimeout(function() {
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').not(':first').remove();
			
			PelanggaranWilayahFactory.getDataWilayah().then(function (res) {
					$scope.Modal_PelanggaranWilayah_Wilayah_Grid.data = res.data.Result;
					
					for(var i = 0; i < $scope.GridPelanggaranWilayah_Wilayah.data.length; ++i)
					{	
						for(var x = 0; x < $scope.Modal_PelanggaranWilayah_Wilayah_Grid.data.length; ++x)
						{	
							$scope.Modal_PelanggaranWilayah_Wilayah_Grid.data = $scope.Modal_PelanggaranWilayah_Wilayah_Grid.data.filter(function (PelanggaranWilayahArea) 
							{ 	//OrgId
								return (PelanggaranWilayahArea.AreaId!=$scope.GridPelanggaranWilayah_Wilayah.data[i].AreaId);
							})	
						}
					}
					
					
					
				});
				
		}, 1);
	}
	
	$scope.PelanggaranWilayah_ProvinsiModal = function () {
		setTimeout(function() {     
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').modal('refresh'); 
		}, 0);
				
		setTimeout(function() {
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').not(':first').remove();
			
			PelanggaranWilayahFactory.getDataProvinsi().then(function (res) {
					$scope.Modal_PelanggaranWilayah_Provinsi_Grid.data = res.data.Result;
					
					for(var i = 0; i < $scope.GridPelanggaranWilayah_Provinsi.data.length; ++i)
					{	
						for(var x = 0; x < $scope.Modal_PelanggaranWilayah_Provinsi_Grid.data.length; ++x)
						{	
							$scope.Modal_PelanggaranWilayah_Provinsi_Grid.data = $scope.Modal_PelanggaranWilayah_Provinsi_Grid.data.filter(function (PelanggaranWilayahProvinsi) 
							{ 	//OrgId
								return (PelanggaranWilayahProvinsi.ProvinceId!=$scope.GridPelanggaranWilayah_Provinsi.data[i].ProvinceId);
							})	
						}
					}
					
				});
				
		}, 1);
	}
	
	$scope.PelanggaranWilayah_KotaModal = function () {
		setTimeout(function() {     
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').modal('refresh'); 
		}, 0);
				
		setTimeout(function() {
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').not(':first').remove();
			
			PelanggaranWilayahFactory.getDataProvinsi().then(function (res) {
					$scope.Modal_PelanggaranWilayah_Kota_OptionSelectProvinsi = res.data.Result;
					$scope.Modal_PelanggaranWilayah_Kota_Grid.data=[];
					
					
				});
				
		}, 1);
	}
	
	$scope.PelanggaranWilayah_KecamatanModal = function () {
		setTimeout(function() {     
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').modal('refresh'); 
		}, 0);
				
		setTimeout(function() {
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').not(':first').remove();
			
			PelanggaranWilayahFactory.getDataProvinsi().then(function (res) {
					$scope.Modal_PelanggaranWilayah_Kecamatan_OptionSelectProvinsi = res.data.Result;
					$scope.Modal_PelanggaranWilayah_Kecamatan_Grid.data=[];
				});
				
		}, 1);
	}
	
	$scope.Modal_PelanggaranWilayah_Kota_SelectProvinsiClicked = function () {
		if($scope.Modal_PelanggaranWilayah_Kota_SelectProvinsi!=null && $scope.Modal_PelanggaranWilayah_Kota_SelectProvinsi!="" && (typeof $scope.Modal_PelanggaranWilayah_Kota_SelectProvinsi!="undefined"))
		{
			PelanggaranWilayahFactory.getDataKota('?start=1&limit=100&filterData=ProvinceId|' + $scope.Modal_PelanggaranWilayah_Kota_SelectProvinsi).then(function(res) {
                    $scope.Modal_PelanggaranWilayah_Kota_Grid.data = res.data.Result;
					
					for(var i = 0; i < $scope.GridPelanggaranWilayah_Kota.data.length; ++i)
					{	
						for(var x = 0; x < $scope.Modal_PelanggaranWilayah_Kota_Grid.data.length; ++x)
						{	
							$scope.Modal_PelanggaranWilayah_Kota_Grid.data = $scope.Modal_PelanggaranWilayah_Kota_Grid.data.filter(function (PelanggaranWilayahKota) 
							{ 	//OrgId
								return (PelanggaranWilayahKota.CityRegencyId!=$scope.GridPelanggaranWilayah_Kota.data[i].CityRegencyId);
							})	
						}
					}
                });
		}
		else
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Mohon input filter.",
							type: 'warning'
						}
					);
		}
		
	}
	
	$scope.Modal_PelanggaranWilayah_Kecamatan_SelectProvinsiChanged = function () {
		PelanggaranWilayahFactory.getDataKota('?start=1&limit=100&filterData=ProvinceId|' + $scope.Modal_PelanggaranWilayah_Kecamatan_SelectProvinsi).then(function(res) {
                    $scope.Modal_PelanggaranWilayah_Kecamatan_OptionSelectKota = res.data.Result;
                });
	}
	
	$scope.Modal_PelanggaranWilayah_Kecamatan_SelectKotaClicked = function () {
		if($scope.Modal_PelanggaranWilayah_Kecamatan_SelectProvinsi!=null && $scope.Modal_PelanggaranWilayah_Kecamatan_SelectProvinsi!="" && (typeof $scope.Modal_PelanggaranWilayah_Kecamatan_SelectProvinsi!="undefined") && $scope.Modal_PelanggaranWilayah_Kecamatan_SelectKota!=null && $scope.Modal_PelanggaranWilayah_Kecamatan_SelectKota!="" && (typeof $scope.Modal_PelanggaranWilayah_Kecamatan_SelectKota!="undefined"))
		{
			PelanggaranWilayahFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.Modal_PelanggaranWilayah_Kecamatan_SelectKota).then(function(res) {
                    $scope.Modal_PelanggaranWilayah_Kecamatan_Grid.data = res.data.Result;
					
					for(var i = 0; i < $scope.GridPelanggaranWilayah_Kecamatan.data.length; ++i)
					{	
						for(var x = 0; x < $scope.Modal_PelanggaranWilayah_Kecamatan_Grid.data.length; ++x)
						{	
							$scope.Modal_PelanggaranWilayah_Kecamatan_Grid.data = $scope.Modal_PelanggaranWilayah_Kecamatan_Grid.data.filter(function (PelanggaranWilayahKecamatan) 
							{ 	//OrgId
								return (PelanggaranWilayahKecamatan.DistrictId!=$scope.GridPelanggaranWilayah_Kecamatan.data[i].DistrictId);
							})	
						}
					}
					
                });
		}
		else
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Mohon input filter.",
							type: 'warning'
						}
					);
		}
		
	}

	
	$scope.Modal_PelanggaranWilayah_Wilayah_Simpan = function (selected) {

			for(var i = 0; i < $scope.Modal_PelanggaranWilayah_Wilayah_Checked.length; ++i)
			{
				$scope.GridPelanggaranWilayah_Wilayah.data.push({'AreaId':$scope.Modal_PelanggaranWilayah_Wilayah_Checked[i].AreaId,'AreaName':$scope.Modal_PelanggaranWilayah_Wilayah_Checked[i].AreaName});
			}
			$scope.Modal_PelanggaranWilayah_Wilayah_Checked=[];
			

		$scope.mPelanggaranWilayah[0].AreaRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Wilayah.data);
		PelanggaranWilayahFactory.createArea($scope.mPelanggaranWilayah).then(function(res){
				
				PelanggaranWilayahFactory.getData().then(function(res){
					$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
					$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
					$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
					$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
					$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
					//return $scope.mGlobalParameterAdminHo;
				});
				
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
			},
			function(err){
                bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
            });
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').modal('hide');
			
			
			
        }
	
	$scope.Modal_PelanggaranWilayah_Wilayah_Batal = function () {
		angular.element('.ui.modal.Modal_PelanggaranWilayah_Wilayah').modal('hide');
	}
	
	$scope.Modal_PelanggaranWilayah_Provinsi_Simpan = function (selected) {

			for(var i = 0; i < $scope.Modal_PelanggaranWilayah_Provinsi_Checked.length; ++i)
			{
				$scope.GridPelanggaranWilayah_Provinsi.data.push({'ProvinceId':$scope.Modal_PelanggaranWilayah_Provinsi_Checked[i].ProvinceId,'ProvinceName':$scope.Modal_PelanggaranWilayah_Provinsi_Checked[i].ProvinceName});
			}
			$scope.Modal_PelanggaranWilayah_Provinsi_Checked=[];
			$scope.mPelanggaranWilayah[0].ProvinceRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Provinsi.data);

			
			PelanggaranWilayahFactory.createProvinsi($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').modal('hide');
			
        }
		
	$scope.Modal_PelanggaranWilayah_Kota_Simpan = function (selected) {

			for(var i = 0; i < $scope.Modal_PelanggaranWilayah_Kota_Checked.length; ++i)
			{
				$scope.GridPelanggaranWilayah_Kota.data.push({'CityRegencyId':$scope.Modal_PelanggaranWilayah_Kota_Checked[i].CityRegencyId,'CityRegencyName':$scope.Modal_PelanggaranWilayah_Kota_Checked[i].CityRegencyName});
			}
			$scope.Modal_PelanggaranWilayah_Kota_Checked=[];

			$scope.mPelanggaranWilayah[0].CityRegencyRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kota.data);
			
			PelanggaranWilayahFactory.createKota($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').modal('hide');
        }

	$scope.Modal_PelanggaranWilayah_Kecamatan_Simpan = function (selected) {
			
			
			
			for(var i = 0; i < $scope.Modal_PelanggaranWilayah_Kecamatan_Checked.length; ++i)
			{
				$scope.GridPelanggaranWilayah_Kecamatan.data.push({'DistrictId':$scope.Modal_PelanggaranWilayah_Kecamatan_Checked[i].DistrictId,'DistrictName':$scope.Modal_PelanggaranWilayah_Kecamatan_Checked[i].DistrictName});
			}
			$scope.Modal_PelanggaranWilayah_Kecamatan_Checked=[];

			$scope.mPelanggaranWilayah[0].DistrictRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kecamatan.data);
			
			PelanggaranWilayahFactory.createKecamatan($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').modal('hide');
        }
	
	$scope.Modal_PelanggaranWilayah_Provinsi_Batal = function () {
		angular.element('.ui.modal.Modal_PelanggaranWilayah_Provinsi').modal('hide');
	}
	
	$scope.Modal_PelanggaranWilayah_Kota_Batal = function () {
		angular.element('.ui.modal.Modal_PelanggaranWilayah_Kota').modal('hide');
	}
	
	$scope.Modal_PelanggaranWilayah_Kecamatan_Batal = function () {
		angular.element('.ui.modal.Modal_PelanggaranWilayah_Kecamatan').modal('hide');
	}
	
	PelanggaranWilayahFactory.getData().then(function(res){
				$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
				$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
				$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
				$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
				$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
				//return $scope.mGlobalParameterAdminHo;
			});
			
	$scope.PelanggaranWilayah_Hapus_Wilayah = function (row) {
		$scope.PelanggaranWilayah_Wilayah_ToDelete=null;
		$scope.PelanggaranWilayah_Wilayah_ToDelete=angular.copy(row);
		
		setTimeout(function() {
              angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Wilayah').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Wilayah').not(':first').remove();  
            }, 1);
		
    }
	
	$scope.Modal_ApusPelanggaranWilayah_Wilayah_Simpan = function () {
		for (var i = 0; i < $scope.GridPelanggaranWilayah_Wilayah.data.length; ++i) {
                if ($scope.GridPelanggaranWilayah_Wilayah.data[i].AreaId == $scope.PelanggaranWilayah_Wilayah_ToDelete.AreaId) {
                    $scope.GridPelanggaranWilayah_Wilayah.data.splice(i, 1);
                }
            }
			
		$scope.mPelanggaranWilayah[0].AreaRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Wilayah.data);
		PelanggaranWilayahFactory.createArea($scope.mPelanggaranWilayah).then(function(res){
				
				PelanggaranWilayahFactory.getData().then(function(res){
					$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
					$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
					$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
					$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
					$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
					//return $scope.mGlobalParameterAdminHo;
				});
				
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil dihapus.",
						type: 'success'
					}
				);
			},
			function(err){
                bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal hapus data.",
						type: 'danger'
					}
				);
            });
			angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Wilayah').modal('hide');	

    }

	$scope.PelanggaranWilayah_Hapus_Provinsi = function (row) {
		$scope.PelanggaranWilayah_Provinsi_ToDelete=null;
		$scope.PelanggaranWilayah_Provinsi_ToDelete=angular.copy(row);
		
		setTimeout(function() {
              angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').not(':first').remove();  
            }, 1);
    }
	
	$scope.Modal_ApusPelanggaranWilayah_Provinsi_Simpan = function () {
		
		for (var i = 0; i < $scope.GridPelanggaranWilayah_Provinsi.data.length; ++i) {
                if ($scope.GridPelanggaranWilayah_Provinsi.data[i].ProvinceId == $scope.PelanggaranWilayah_Provinsi_ToDelete.ProvinceId) {
                    $scope.GridPelanggaranWilayah_Provinsi.data.splice(i, 1);
                }
            }
		
		$scope.mPelanggaranWilayah[0].ProvinceRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Provinsi.data);

			
			PelanggaranWilayahFactory.createProvinsi($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal hapus data.",
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').modal('hide');
			
			

    }
	
	$scope.PelanggaranWilayah_Hapus_Kota = function (row) {
		$scope.PelanggaranWilayah_Kota_ToDelete=null;
		$scope.PelanggaranWilayah_Kota_ToDelete=angular.copy(row);
		
		setTimeout(function() {
              angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kota').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kota').not(':first').remove();  
            }, 1);
    }
	
	$scope.Modal_ApusPelanggaranWilayah_Kota_Simpan = function () {

		for (var i = 0; i < $scope.GridPelanggaranWilayah_Kota.data.length; ++i) {
                if ($scope.GridPelanggaranWilayah_Kota.data[i].CityRegencyId == $scope.PelanggaranWilayah_Kota_ToDelete.CityRegencyId) {
                    $scope.GridPelanggaranWilayah_Kota.data.splice(i, 1);
                }
            }
			
		$scope.mPelanggaranWilayah[0].CityRegencyRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kota.data);
			
			PelanggaranWilayahFactory.createKota($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal hapus data.",
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kota').modal('hide');	

    }
	
	$scope.PelanggaranWilayah_Hapus_Kecamatan = function (row) {
		$scope.PelanggaranWilayah_Kecamatan_ToDelete=null;
		$scope.PelanggaranWilayah_Kecamatan_ToDelete=angular.copy(row);
		
		setTimeout(function() {
              angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kecamatan').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kecamatan').not(':first').remove();  
            }, 1);

    }
	
	$scope.Modal_ApusPelanggaranWilayah_Kecamatan_Simpan = function () {

		for (var i = 0; i < $scope.GridPelanggaranWilayah_Kecamatan.data.length; ++i) {
                if ($scope.GridPelanggaranWilayah_Kecamatan.data[i].DistrictId == $scope.PelanggaranWilayah_Kecamatan_ToDelete.DistrictId) {
                    $scope.GridPelanggaranWilayah_Kecamatan.data.splice(i, 1);
                }
            }
			
		$scope.mPelanggaranWilayah[0].DistrictRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kecamatan.data);
			
			PelanggaranWilayahFactory.createKecamatan($scope.mPelanggaranWilayah).then(function(res){
					
					PelanggaranWilayahFactory.getData().then(function(res){
						$scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
						$scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
						$scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
						$scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
						$scope.mPelanggaranWilayah=angular.copy(res.data.Result);
						//return $scope.mGlobalParameterAdminHo;
					});
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal hapus data."+err,
							type: 'danger'
						}
					);
				});
			angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kecamatan').modal('hide');	

    }
	
	$scope.PelanggaranWilayahSimpan = function () {
		// console.log("setan",$scope.mPelanggaranWilayah);
		// $scope.mPelanggaranWilayah[0].AreaRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Wilayah.data);
		// $scope.mPelanggaranWilayah[0].ProvinceRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Provinsi.data);
		// $scope.mPelanggaranWilayah[0].CityRegencyRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kota.data);
		// $scope.mPelanggaranWilayah[0].DistrictRestrictionView=angular.copy($scope.GridPelanggaranWilayah_Kecamatan.data);
		
		// PelanggaranWilayahFactory.create($scope.mPelanggaranWilayah).then(function(res){
				
				// PelanggaranWilayahFactory.getData().then(function(res){
					// $scope.GridPelanggaranWilayah_Wilayah.data = res.data.Result[0].AreaRestrictionView;
					// $scope.GridPelanggaranWilayah_Provinsi.data = res.data.Result[0].ProvinceRestrictionView;
					// $scope.GridPelanggaranWilayah_Kota.data = res.data.Result[0].CityRegencyRestrictionView;
					// $scope.GridPelanggaranWilayah_Kecamatan.data = res.data.Result[0].DistrictRestrictionView;
					// $scope.mPelanggaranWilayah=angular.copy(res.data.Result);
					// //return $scope.mGlobalParameterAdminHo;
				// });
				
				// bsNotify.show(
					// {
						// title: "Berhasil",
						// content: "Data berhasil disimpan.",
						// type: 'success'
					// }
				// );
			// },
			// function(err){
                // bsNotify.show(
					// {
						// title: "Gagal",
						// content: "Gagal simpan data.",
						// type: 'danger'
					// }
				// );
            // });
	}
	
	$scope.Modal_ApusPelanggaranWilayah_Wilayah_Batal = function () {
		angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Wilayah').modal('hide');
	}
	
	$scope.Modal_ApusPelanggaranWilayah_Provinsi_Batal = function () {
		angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').modal('hide');
	}
	
	$scope.Modal_ApusPelanggaranWilayah_Provinsi_Batal = function () {
		angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Provinsi').modal('hide');
	}
	
	$scope.Modal_ApusPelanggaranWilayah_Kota_Batal = function () {
		angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kota').modal('hide');
	}
	
	$scope.Modal_ApusPelanggaranWilayah_Kecamatan_Batal = function () {
		angular.element('.ui.modal.Modal_ApusPelanggaranWilayah_Kecamatan').modal('hide');
	}
	
	$scope.GridPelanggaranWilayah_Wilayah = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
				{ name:'Area Id', field: 'AreaId', visible: false },
                { name:'Nama Area', field: 'AreaName' },
				{ name: 'Action', field: '',width:'20%', cellTemplate:'<i class="fa fa-fw fa-lg fa-times" style="margin-left:8px;padding:8px 8px 8px 0px;" ng-click="grid.appScope.PelanggaranWilayah_Hapus_Wilayah(row.entity)" ></i>'},
            ]
        };
		
	$scope.GridPelanggaranWilayah_Provinsi = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Id Provinsi', field: 'ProvinceId', visible: false},
				{ name: 'Provinsi', field: 'ProvinceName'},
				{ name: 'Action', field: '',width:'20%', cellTemplate:'<i class="fa fa-fw fa-lg fa-times" style="margin-left:8px;padding:8px 8px 8px 0px;" ng-click="grid.appScope.PelanggaranWilayah_Hapus_Provinsi(row.entity)" ></i>'},
            ]
        };

	$scope.GridPelanggaranWilayah_Kota = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Id Kota', field: 'CityRegencyId', visible: false},
				{ name: 'Kota', field: 'CityRegencyName'},
				{ name: 'Action', field: '',width:'20%', cellTemplate:'<i class="fa fa-fw fa-lg fa-times" style="margin-left:8px;padding:8px 8px 8px 0px;" ng-click="grid.appScope.PelanggaranWilayah_Hapus_Kota(row.entity)" ></i>'},

            ]
        };
		
	$scope.GridPelanggaranWilayah_Kecamatan = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Id', field: 'DistrictId', visible: false},
				{ name: 'Kecamatan', field: 'DistrictName'},
				{ name: 'Action', field: '',width:'20%', cellTemplate:'<i class="fa fa-fw fa-lg fa-times" style="margin-left:8px;padding:8px 8px 8px 0px;" ng-click="grid.appScope.PelanggaranWilayah_Hapus_Kecamatan(row.entity)" ></i>'},

            ]
        };

	$scope.Modal_PelanggaranWilayah_Wilayah_Grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Nama Area', field: 'AreaName' },


            ]
        };
		
	$scope.Modal_PelanggaranWilayah_Wilayah_Grid.onRegisterApi = function(gridApi) {
            $scope.gridApiModal_PelanggaranWilayah_Wilayah_Grid = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.Modal_PelanggaranWilayah_Wilayah_Checked = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.Modal_PelanggaranWilayah_Wilayah_Checked = gridApi.selection.getSelectedRows();
            });
        };	
		
	$scope.Modal_PelanggaranWilayah_Provinsi_Grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Provinsi', field: 'ProvinceName' },
                

            ]
        };
		
	$scope.Modal_PelanggaranWilayah_Provinsi_Grid.onRegisterApi = function(gridApi) {
            $scope.gridApiModal_PelanggaranWilayah_Provinsi_Grid = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.Modal_PelanggaranWilayah_Provinsi_Checked = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.Modal_PelanggaranWilayah_Provinsi_Checked = gridApi.selection.getSelectedRows();
            });
        };	

	$scope.Modal_PelanggaranWilayah_Kota_Grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Kota', field: 'CityRegencyName' },

            ]
        };
		
	$scope.Modal_PelanggaranWilayah_Kota_Grid.onRegisterApi = function(gridApi) {
            $scope.gridApiModal_PelanggaranWilayah_Kota_Grid = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.Modal_PelanggaranWilayah_Kota_Checked = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.Modal_PelanggaranWilayah_Kota_Checked = gridApi.selection.getSelectedRows();
            });
        };	
		
	$scope.Modal_PelanggaranWilayah_Kecamatan_Grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Kecamatan', field: 'DistrictName' },

            ]
        };
		
	$scope.Modal_PelanggaranWilayah_Kecamatan_Grid.onRegisterApi = function(gridApi) {
            $scope.gridApiModal_PelanggaranWilayah_Kecamatan_Grid = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.Modal_PelanggaranWilayah_Kecamatan_Checked = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.Modal_PelanggaranWilayah_Kecamatan_Checked = gridApi.selection.getSelectedRows();
            });
        };	

    
});


