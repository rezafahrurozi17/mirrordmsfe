angular.module('app')
  .factory('PelanggaranWilayahFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryAreaRuleRestriction');
        return res;
      },
	  
	  getDataWilayah: function() {
        var res=$http.get('/api/sales/MAreaDealer');
        return res;
      },
	  
	  getDataProvinsi: function() {
        var res=$http.get('/api/sales/MLocationProvince');
        return res;
      },
	  
	  getDataKota: function(param) {
        var res=$http.get('/api/sales/MLocationCityRegency'+param);
        return res;
      },
	  
	  getDataKecamatan: function(param) {
        var res=$http.get('/api/sales/MLocationKecamatan'+param);
        return res;
      },
      create: function(PelanggaranWilayah) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction/Insert',PelanggaranWilayah );
		// return $http.post('/api/sales/PCategoryAreaRuleRestriction/Insert', [{
                                            // CustomerReligionId: PelanggaranWilayah.CustomerReligionId,
											// CustomerReligionId: PelanggaranWilayah.CustomerReligionId,
											// CustomerReligionId: PelanggaranWilayah.CustomerReligionId,
											// CustomerReligionId: PelanggaranWilayah.CustomerReligionId,
                                            // CustomerReligionName: PelanggaranWilayah.CustomerReligionName}]);
      },
	  createArea: function(PelanggaranWilayah) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction/Area/Insert',PelanggaranWilayah );
      },
	  createProvinsi: function(PelanggaranWilayah) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction/Province/Insert',PelanggaranWilayah );
      },
	  createKota: function(PelanggaranWilayah) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction/City/Insert',PelanggaranWilayah );
      },
	  createKecamatan: function(PelanggaranWilayah) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction/District/Insert',PelanggaranWilayah );
      },
      update: function(PelanggaranWilayah){
        return $http.put('/api/sales/CCustomerReligion', [{
                                            CustomerReligionId: PelanggaranWilayah.CustomerReligionId,
                                            CustomerReligionName: PelanggaranWilayah.CustomerReligionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd