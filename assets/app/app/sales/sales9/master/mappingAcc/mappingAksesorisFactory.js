angular.module('app')
	.factory('MappingAksesorisFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

	
		return {
			VerifyData: function (KlasifikasiPE, IsiGrid, TypePE) {
				//var url = '/api/fe/IncomingInstruction';
				var inputData = IsiGrid;
				// var url = '/api/fe/MasterPEPriceAccessories/Verify/';
				var url = '/api/sales/AdminHandlingPOMaintenance/verify/'; 
				var param = JSON.stringify(inputData);

				if (debugMode) { console.log('Masuk ke submitData') };
				if (debugMode) { console.log('url :' + url); };
				if (debugMode) { console.log('Parameter POST :' + param) };
				var res = $http.post(url, param);

				return res;
			},

			getData: function (VehicleModelId, PartsName) {
	// var url = '/api/fe/MasterPEPriceAccessories/SelectData/start/' + start + '/limit/' + limit + '/filterData/' + filterData;
				// var url = '/api/fe/MasterPEPriceAccessories/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
				if (PartsName == null || PartsName == '' || PartsName == undefined){
					var url = '/api/sales/AdminHandlingPOMaintenance/MappingAccessories?VehicleModelId=' + VehicleModelId + '&aksesoris=';					
				} else {
					var url = '/api/sales/AdminHandlingPOMaintenance/MappingAccessories?VehicleModelId=' + VehicleModelId + '&aksesoris=' + PartsName;				
				}
				var res = $http.get(url);
				return res;
			},

			Submit : function(IsiGrid) {
				// var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
				var url = '/api/sales/AdminHandlingPOMaintenance/simpandata/';
				// var param = JSON.stringify(inputData);

				if (debugMode) { console.log('Masuk ke submitData') };
				if (debugMode) { console.log('url :' + url); };
				// if (debugMode) { console.log('Parameter POST :' + inputData) };
				var res = $http.post(url, IsiGrid);
				return res;
			},

			getDaVehicleType: function (param) {
				var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
				return res;
			},

			getDataModel: function () {
				var res = $http.get('/api/sales/MUnitVehicleModelTomas');
				return res;
			},



		}
	});
