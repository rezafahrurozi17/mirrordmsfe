var app = angular.module('app');
app.controller('MappingAksesorisController', function ($scope, $http, $filter, bsNotify, bsAlert, CurrentUser,MappingAksesorisFactory, $timeout) {
	$scope.optionsKolomFilterSatu_PriceAccessories = [
		// {name: "Dealer Name", value:"DealerName"}, 
		// {name: "Province", value:"Province"} ,	 
		{ name: "Model", value: "Model" },
		{ name: "Tipe", value: "NamaModel" },
		{ name: "Katashiki", value: "Katashiki" },
		{ name: "Suffix", value: "Suffix" },
		{ name: "Kode Aksesoris", value: "AccessoriesCode" },
		{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
		// {name: "Nama Model / Grade",value:"NamaModel"}
	];

	$scope.optionsKolomFilterDua_PriceAccessories = [
		// {name: "Dealer Name", value:"DealerName"}, 
		// {name: "Province", value:"Province"} ,	 
		{ name: "Model", value: "Model" },
		{ name: "Tipe", value: "NamaModel" },
		{ name: "Katashiki", value: "Katashiki" },
		{ name: "Suffix", value: "Suffix" },
		{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
		// {name: "Nama Model / Grade",value:"NamaModel"}, 
	];

	$scope.optionsComboBulkAction_PriceAccessories = [{ name: "Delete", value: "Delete" }];

	$scope.optionsComboFilterGrid_PriceAccessories = [
		{ name: "Model", value: "Model" },
		// {name:"NamaModel", value:"NamaModel"},
		{ name: "Tipe", value: "NamaModel" },
		{ name: "Katashiki", value: "Katashiki" },
		{ name: "Suffix", value: "Suffix" },
		{ name: "Kode Aksesoris", value: "AccessoriesCode" },
		{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
		{ name: "Sales Price", value: "SalesPrice" },
		{ name: "Effective Date From", value: "EffectiveDateFrom" },
		{ name: "Effective Date To", value: "EffectiveDateTo" },
		{ name: "Remarks", value: "Remarks" }
	];
	$scope.btnUpload = "hide";
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'PriceAccessories';
	angular.element('#PriceAccessoriesModal').modal('hide');
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.cekKolom = '';
	$scope.ShowFieldGenerate_PriceAccessories = true;

	$scope.disabledSimpanUpload = true;

	$scope.MasterSellingPriceFU_PriceAccessories_grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'Model', displayName: 'Model', field: 'VehicleModelName' },
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'Kode Aksesoris', displayName: 'Kode Aksesoris', field: 'PartsCode', },
			{ width: '13%', enableCellEdit: false, enableHiding: false, name: 'Nama Aksesoris', displayName: 'Nama Aksesoris', field: 'PartsName' },
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'Effective Date From', displayName: 'Effective Date From', field: 'EffectiveDateFrom', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'Effective Date To', displayName: 'Effective Date To', field: 'EffectiveDateTo', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '25%', enableCellEdit: false, enableHiding: false, name: 'Remark', displayName: 'Remark', field: 'Remarks' },
		],

		onRegisterApi: function (gridApi) {
			$scope.MasterSellingPriceFU_PriceAccessories_gridAPI = gridApi;
		}
	};

	$scope.filterModel = [];
	MappingAksesorisFactory.getDataModel().then(function (res) {
		$scope.optionsModel = res.data.Result;
		console.log('$scope.optionsModel ==>', $scope.optionsModel)
		return $scope.optionsModel;
	});

	$scope.onSelectModelChanged = function (ModelSelected) {
		console.log('on model change ===>', ModelSelected);
		if (ModelSelected == undefined || ModelSelected == null) {
			$scope.filterModel.VehicleModelName = "";
			$scope.filterModel.VehicleModelId = "";
			$scope.optionsTipe = [];
			$scope.filterModel.VehicleTypelName = "";
			$scope.filterModel.VehicleTypeId = "";

		} else {
			$scope.filterModel.VehicleModelName = ModelSelected.VehicleModelName;
			$scope.filterModel.VehicleModelId = ModelSelected.VehicleModelId;
			MappingAksesorisFactory.getDaVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
				$scope.optionsTipe = [];
				$scope.filterModel.VehicleTypelName = "";
				$scope.filterModel.VehicleTypeId = "";
				$scope.optionsTipe = res.data.Result;
				return $scope.optionsTipe;
			});
		}

	}

	$scope.changeFormatDate = function(item) {
		console.log("tmpParam ===>",tmpParam);
		var tmpParam = item;
		tmpParam = new Date(tmpParam);
		var finalDate
		var yyyy = tmpParam.getFullYear().toString();
		var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpParam.getDate().toString();
		finalDate = yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
		
		return finalDate;
	}

	$scope.changeFormatDateGrid = function(item) {
		console.log("tmpParam ===>",tmpParam);
		var tmpParam = item;
		tmpParam = new Date(tmpParam);
		var finalDate
		var yyyy = tmpParam.getFullYear().toString();
		var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpParam.getDate().toString();
		finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
		
		return finalDate;
	}
	// $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate)
	// if($scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate != null || $scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate != undefined){
	// 	$scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate = $scope.changeFormatDate($scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate) 
	// }

	$scope.onSelectTipeChanged = function (TipeSelected) {
		console.log('TipeSelected ===>', TipeSelected);
		if (TipeSelected == undefined || TipeSelected == null) {
			$scope.filterModel.VehicleTypelName = "";
			$scope.filterModel.VehicleTypeId = "";

		} else {
			$scope.filterModel.VehicleTypelName = TipeSelected.Description;
			$scope.filterModel.VehicleTypeId = TipeSelected.VehicleTypeId;
		}
	}

	$scope.FilterGridMasterPriceAccessories = function () {
		var inputfilter = $scope.textFilterMasterPriceAccessories;
		console.log('$scope.textFilterMasterPriceAccessories ===>', $scope.textFilterMasterPriceAccessories);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_PriceAccessories_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterPriceAccessories + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_PriceAccessories_grid.data = $scope.MasterSellingPriceFU_PriceAccessories_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data if ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data)

		} else {
			$scope.MasterSellingPriceFU_PriceAccessories_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data else ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data)
		}
	};

	$scope.fixDate = function (date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() + "-" +
				('0' + (date.getMonth() + 1)).slice(-2) + "-" +
				('0' + date.getDate()).slice(-2)
			return fix;
		} else {
			return null;
		}
	};

	$scope.btnSearch_MappingAcc = function () {

		var tglStart;
		var tglEnd;

		if($scope.filterModel.VehicleModelName == "" || $scope.filterModel.VehicleModelName == undefined || $scope.filterModel.VehicleModelName == null){
			bsNotify.show({
				title: "Warning",
				content: "Model Harus Dipilih.",
				type: "warning"
			});
			return;
		}
		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined || $scope.filterModel.TanggalFilterStart == null) {
			tglStart = "";
		} else {
			tglStart = $scope.fixDate($scope.filterModel.TanggalFilterStart)
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined || $scope.filterModel.TanggalFilterEnd == null) {
			tglEnd = "";
		} else {
			tglEnd = $scope.fixDate($scope.filterModel.TanggalFilterEnd)
		}

		console.log('model ===>', $scope.filterModel.VehicleModelId);
		console.log('tipe ===>', $scope.filterModel.VehicleTypeId);
		console.log('date_from ===>', $scope.filterModel.TanggalFilterStart);
		console.log('date_to ===>', $scope.filterModel.TanggalFilterEnd);
		console.log('filterModel ===>', $scope.filterModel);

		var filter = [{
			StartDate: tglStart,
			EndDate: tglEnd,
			VehicleModelId: $scope.filterModel.VehicleModelId,
			VehicleTypeId: $scope.filterModel.VehicleTypeId
		}];

		console.log("filter",$scope.filter);
		MappingAksesorisFactory.getData($scope.filterModel.VehicleModelId, $scope.filterModel.PartsName)
			.then(
				function (res) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data = res.data.Result;            //Data hasil dari WebAPI
					$scope.MasterSellingPriceFU_PriceAccessories_grid.totalItems = res.data.Total;
					$scope.MasterSellingPriceFU_PriceAccessories_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_PriceAccessories_grid.data);

				},
				
				function (err) {
					bsNotify.show(
						{
							type: 'danger',
							title: "Data gagal disimpan!",
							content: err.data.Message.split('-')[1],
						}
					);
				}
			);
			console.log("$scope.MasterSellingPriceFU_PriceAccessories_grid.data",$scope.MasterSellingPriceFU_PriceAccessories_grid.data);
	}










	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function (ExcelFile) {
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element(document.querySelector('#uploadMappingAcc')); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			console.log('data asli yang di upload ===>', json.EffectiveDateFrom, json.EffectiveDateTo);
			console.log("json==>",json);

			var flag = 0;
			for (var i in json){
				console.log("EffectiveDateFrom.length",json[i].EffectiveDateFrom.length);
				console.log("EffectiveDateTo.length",json[i].EffectiveDateTo.length);
				if(json[i].EffectiveDateFrom.length != 8 || json[i].EffectiveDateTo.length != 8){
					flag++
				}
				console.log("json[i].EffectiveDateFrom.",json[i].EffectiveDateFrom);
				console.log("json[i].EffectiveDateTo.",json[i].EffectiveDateTo);
			}
			console.log("flag==>",flag);
			if(flag > 0){
				bsNotify.show({
					title: "Gagal",
					content: "Format tanggal salah, mohon untuk menggunakan format YYYYMMDD",
					type: 'danger'
				});
			}else{
				for (i = 0; i < json.length; i++) {
					// 	//perlu dimatikan jika kolom di grid sama dengan kolom yang di header xcel
					json[i].AccessoriesCode = json[i].KodeAksesoris;
	
					var tglUpload = json[i].EffectiveDateFrom //format 20191231
					var thn = tglUpload.substring(0, 4);
					var bln = tglUpload.substring(4, 6);
					var tgl = tglUpload.substring(6, 8);
					var tgl_jadi = tgl + "-" + bln + "-" + thn;
					json[i].EffectiveDateFrom = tgl_jadi;
	
					var tglUploadTo = json[i].EffectiveDateTo //format 20191231
					var thnTo = tglUploadTo.substring(0, 4);
					var blnTo = tglUploadTo.substring(4, 6);
					var tglTo = tglUploadTo.substring(6, 8);
	
					var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
					json[i].EffectiveDateTo = tgl_jadiTo;
				}
	
				$scope.btnUpload = "hide";
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				$scope.MasterSelling_PriceAccessories_Upload_Clicked();
				myEl.val('');
			}			
			
		});
	}

	$scope.validateColumn = function (inputExcelData) {
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.Model)) {
			$scope.cekKolom = $scope.cekKolom + ' Model \n';
		}

		if (angular.isUndefined(inputExcelData.KodeAksesoris)) {
			$scope.cekKolom = $scope.cekKolom + ' KodeAksesoris \n';
		}

		if (angular.isUndefined(inputExcelData.NamaAksesoris)) {
			$scope.cekKolom = $scope.cekKolom + ' NamaAksesoris \n';
		}

		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}

		if (
			angular.isUndefined(inputExcelData.Model) || 
			angular.isUndefined(inputExcelData.KodeAksesoris) ||
			angular.isUndefined(inputExcelData.NamaAksesoris) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) ||
			angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			return (false);
		}

		return (true);
	}

	$scope.ComboBulkAction_PriceAccessories_Changed = function () {
		if ($scope.ComboBulkAction_PriceAccessories == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_PriceAccessories_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_PriceAccessories_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_PriceAccessories_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_PriceAccessories_grid.data.splice($scope.MasterSellingPriceFU_PriceAccessories_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
				function() {
					$scope.OK_Button_Clicked();
				},
				function() {
		
				// $scope.PGA_BGTGLAccount_UIGrid.data.push(row_data);
				}
			)
			}
		}
	}

	$scope.OK_Button_Clicked = function () {
		angular.forEach($scope.MasterSellingPriceFU_PriceAccessories_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_PriceAccessories_grid.data.splice($scope.MasterSellingPriceFU_PriceAccessories_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_PriceAccessories = "";
		
	}
	$scope.DeleteCancel_Button_Clicked = function () {
		$scope.ComboBulkAction_PriceAccessories = "";
		angular.element('#PriceAccessoriesModal').modal('hide');
	}

	$scope.MasterSelling_PriceAccessories_Download_Clicked = function () {
		var excelData = [];
		var fileName = "";
		fileName = "MasterMappingAksesorisTemplate";

		// for (i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length; i++) {
		// 	var setan = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom.split('-');
		// 	var to_submit = "" + setan[2] + "-" + setan[1] + "-" + setan[0];
		// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom = to_submit;
		// 	console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom);

		// 	var setan2 = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo.split('-');
		// 	var to_submit2 = "" + setan2[2] + "-" + setan2[1] + "-" + setan2[0];
		// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo = to_submit2;
		// 	console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo);
		// }

		if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data.length == 0) {
			// for (i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length;) {
			// 	// 	//perlu dimatikan jika kolom di grid sama dengan kolom yang di header xcel
			// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].AccessoriesCode = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].KodeAksesoris;

			// 	var tglUpload = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom //format 20191231
			// 	var thn = tglUpload.substring(0, 4);
			// 	var bln = tglUpload.substring(4, 6);
			// 	var tgl = tglUpload.substring(6, 8);
			// 	var tgl_jadi = tgl + "-" + bln + "-" + thn;
			// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom = tgl_jadi;

			// 	var tglUploadTo = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo //format 20191231
			// 	var thnTo = tglUploadTo.substring(0, 4);
			// 	var blnTo = tglUploadTo.substring(4, 6);
			// 	var tglTo = tglUploadTo.substring(6, 8);

			// 	var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
			// 	$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo = tgl_jadiTo;
			// }
			var currentdate = new Date();
			excelData.push({
				Model: "[Model]" ,
				KodeAksesoris: "[Kode_Aksesoris]",
				NamaAksesoris: "[Nama_Aksesoris]",
				EffectiveDateFrom: $scope.changeFormatDate(currentdate),
				EffectiveDateTo: "99991231",
				Remarks: "Berikut contoh format pengisian data"
				// Model: " ",
				// KodeAksesoris: " ",
				// NamaAksesoris: " ",
				// EffectiveDateFrom : EffectiveDateFrom == " " ? EffectiveDateFrom:$scope.changeFormatDate(EffectiveDateFrom),
				// EffectiveDateTo : EffectiveDateFrom == " " ? EffectiveDateTo:$scope.changeFormatDate(EffectiveDateTo),
				// Remarks: ""
			});
			fileName = "MasterMappingAksesorisTemplate";
		}
		else {

			for (var i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length; i++) {

				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].VehicleModelName == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].VehicleModelName = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].PartsName == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].PartsName = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].PartsCode == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].PartsCode = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].Remarks == null) {
					$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].Remarks = " ";
				}
			}

			$scope.MasterSellingPriceFU_PriceAccessories_grid.data.forEach(function (row) {
				excelData.push({
					Model: row.VehicleModelName,
					KodeAksesoris: row.PartsCode,
					NamaAksesoris: row.PartsName,
					EffectiveDateFrom: $scope.changeFormatDate(row.EffectiveDateFrom),
					EffectiveDateTo: $scope.changeFormatDate(row.EffectiveDateTo),
					// EffectiveDateFrom : row.EffectiveDateFrom !== " " ? row.EffectiveDateFrom:$scope.changeFormatDate(row.EffectiveDateFrom),
					// EffectiveDateTo : row.EffectiveDateTo !== " " ? row.EffectiveDateTo:$scope.changeFormatDate(row.EffectiveDateTo),
					Remarks: row.Remarks
				});
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.MasterSelling_PriceAccessories_Upload_Clicked = function () {
		if ($scope.MasterSelling_ExcelData.length == 0) {
			alert("file excel kosong !");
			return;
		}

		if (!$scope.validateColumn($scope.MasterSelling_ExcelData[0])) {
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}



		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		
		for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
			var setan = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[2] + "-" + setan[1] + "-" + setan[0];
			$scope.MasterSelling_ExcelData[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSelling_ExcelData[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[2] + "-" + setan2[1] + "-" + setan2[0];
			$scope.MasterSelling_ExcelData[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_PriceAccessories_gridAPI.grid.clearAllFilters();
		$scope.KirimVerify = [];
		var DataFinal = JSON.parse(Grid);
		
			for (i = 0; i < DataFinal.length; i++ ){
				$scope.KirimVerify.push({
					VehicleModelName:DataFinal[i].Model, 
					PartsCode:DataFinal[i].KodeAksesoris,
					PartsName:DataFinal[i].NamaAksesoris,
					EffectiveDateFrom:DataFinal[i].EffectiveDateFrom,
					EffectiveDateTo:DataFinal[i].EffectiveDateTo,
					Remarks:DataFinal[i].Remarks
				
				});
			}

		MappingAksesorisFactory.VerifyData($scope.JenisPE, $scope.KirimVerify, $scope.TypePE).then(
			function (res) {
				$scope.MasterSellingPriceFU_PriceAccessories_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.dataSimpan = res.data.Result;
				console.log("$scope.MasterSellingPriceFU_PriceAccessories_grid.data",$scope.MasterSellingPriceFU_PriceAccessories_grid.data)
				var dataValid = undefined;
				for (i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length; i++) {
					if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].Remarks == "" || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].Remarks == null || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].Remarks == undefined) {
						dataValid = true;
						$scope.disabledSimpanUpload = false;
					} else {
						dataValid = false;
						$scope.disabledSimpanUpload = true;
					}
				}

				if (dataValid == false) {
					bsNotify.show(
						{
							type: 'warning',
							title: "Data tidak valid!",
							content: "Cek detail kesalahan di kolom Remarks pada tabel",
						}
					);
				}


				$scope.MasterSellingPriceFU_PriceAccessories_grid.totalItems = res.data.Total;
			},
			function (err) {
				bsNotify.show({
					title: "Error Message",
					content: "Gagal Upload Data",
					type: 'danger'
				});
			}		
		);
		
		console.log("$scope.Grid", Grid);
		console.log("DataFinal",DataFinal);
// 		[{"MappingAccessoriesview":[{"VehicleModelName":"AVANZA","PartsCode":"81270-55040","PartsName":"LAMP AS, LICENSE PLATE","EffectiveDateFrom":"2021-01-01T00:00:00","EffectiveDateTo":"2020-12-30T00:00:00","Remarks":null},{"VehicleModelName":"rush","PartsCode":"81270-55040","PartsName":"LAMP AS, LICENSE PLATE","EffectiveDateFrom":"2021-01-01T00:00:00","EffectiveDateTo":"2020-12-30T00:00:00","Remarks":null}]
// }]
			
	}

	$scope.MasterSelling_Simpan_Clicked = function () {

		// for (i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length; i++) {
		// 	if ($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom.indexOf('-') !== -1) {
		// 		var setan = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom.split('-');
		// 		var to_submit = "" + setan[2] + "/" + setan[1] + "/" + setan[0];
		// 		$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom = to_submit;
		// 		console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom);

		// 		var setan2 = $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo.split('-');
		// 		var to_submit2 = "" + setan2[2] + "/" + setan2[1] + "/" + setan2[0];
		// 		$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo = to_submit2;
		// 		console.log('$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo);
		// 	}
		// }
		if($scope.dataSimpan.EffectiveDateFrom != null || $scope.dataSimpan.EffectiveDateFrom != undefined || $scope.dataSimpan.EffectiveDateFrom != " "){
			$scope.dataSimpan.EffectiveDateFrom = $scope.changeFormatDate($scope.dataSimpan.EffectiveDateFrom) 
		}
		if($scope.dataSimpan.EffectiveDateTo != null || $scope.dataSimpan.EffectiveDateTo != undefined || $scope.dataSimpan.EffectiveDateTo != " "){
			$scope.dataSimpan.EffectiveDateTo = $scope.changeFormatDate($scope.dataSimpan.EffectiveDateTo) 
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_PriceAccessories_grid.data);
		console.log("$scope.dataSimpan",$scope.dataSimpan);
		for (var i = 0; i < $scope.MasterSellingPriceFU_PriceAccessories_grid.data.length; i++) {
			if($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom != null || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom != undefined || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom != " "){
				$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom = $scope.changeFormatDateGrid($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateFrom) 
			}
			if($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo != null || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo != undefined || $scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo != " "){
				$scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo = $scope.changeFormatDateGrid($scope.MasterSellingPriceFU_PriceAccessories_grid.data[i].EffectiveDateTo) 
			}
		}
		// for (i = 0; i < $scope.dataSimpan.length; i++){
		// 	if($scope.dataSimpan[i].EffectiveDateFrom.length != 8 || $scope.dataSimpan[i].EffectiveDateTo.length != 8){
		// 		bsNotify.show({
		// 						title: "Warning",
		// 						content: "zzz",
		// 						type: 'error'
		// 					});
		// 	}
		// }
		MappingAksesorisFactory.Submit($scope.dataSimpan).then(
			function (res) {
				bsNotify.show({
					title: "Sukses",
					content: "Data berhasil di simpan",
					type: 'success'
				});
				$scope.MasterSellingPriceFU_PriceAccessories_grid.data = [];
				$scope.disabledSimpanUpload = true;

				// if($scope.filterModel.VehicleModelName == "" || $scope.filterModel.VehicleModelName == undefined || $scope.filterModel.VehicleModelName == null){
				// 	$scope.btnSearch_MappingAcc();
				// }else{
				// 	$scope.btnSearch_MappingAcc();					
				// }
			},
			function (err) {
				
				bsNotify.show(
					{
						type: 'danger',
						title: "Data gagal disimpan!",
						content: err.data.Message.split('-')[1],
					}
				);
			}
		);


	}

	$scope.MasterSelling_Batal_Clicked = function () {
		$scope.MasterSellingPriceFU_PriceAccessories_grid.data = [];
		$scope.MasterSellingPriceFU_PriceAccessories_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_PriceAccessories = "";
		$scope.TextFilterSatu_PriceAccessories = "";

		var myEl = angular.element(document.querySelector('#uploadMappingAcc'));
		myEl.val('');
	}



	$scope.MasterSelling_PriceAccessories_Cari_Clicked = function () {
		var value = $scope.TextFilterGrid_PriceAccessories;
		$scope.MasterSellingPriceFU_PriceAccessories_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_PriceAccessories != "") {
			$scope.MasterSellingPriceFU_PriceAccessories_gridAPI.grid.getColumn($scope.ComboFilterGrid_PriceAccessories).filters[0].term = value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_PriceAccessories_gridAPI.grid.clearAllFilters();

	}

});