angular.module('app')
  .factory('ProfileAsuransiPerluasanJaminan', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileInsuranceExtension');
        //console.log('res=>',res);
		
        return res;
      },
      create: function(profileAsuransiPerluasanJaminan) {
        return $http.post('/api/sales/MProfileInsuranceExtension', [{
                                            InsuranceExtensionName: profileAsuransiPerluasanJaminan.InsuranceExtensionName}]);
      },
      update: function(profileAsuransiPerluasanJaminan){
        return $http.put('/api/sales/MProfileInsuranceExtension', [{
                                            InsuranceExtensionId: profileAsuransiPerluasanJaminan.InsuranceExtensionId,
											OutletId: profileAsuransiPerluasanJaminan.OutletId,
                                            InsuranceExtensionName: profileAsuransiPerluasanJaminan.InsuranceExtensionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileInsuranceExtension',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd