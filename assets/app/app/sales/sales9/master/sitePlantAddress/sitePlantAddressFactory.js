angular.module('app')
    .factory('SitePlantAddressFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MSitesPlantAddress');
                return res;
            },
            getDataKelurahan: function() {
                var res = $http.get('/api/sales/MLocationKelurahan');
                return res;
            },
            getDataCityRegency: function (){
                var res = $http.get('/api/sales/MLocationCityRegency');
                return res;
            },
            create: function(SitePlantAddress) {
                return $http.post('/api/sales/MSitesPlantAddress', [{
                    PlantName : SitePlantAddress.PlantName,
                    PlantAddress: SitePlantAddress.PlantAddress,
                    CityRegencyId: SitePlantAddress.CityRegencyId,
                    PostalCodeId: SitePlantAddress.PostalCodeId,
                }]);
            },
            update: function(SitePlantAddress) {
                return $http.put('/api/sales/MSitesPlantAddress', [{
                    OutletId: SitePlantAddress.OutletId,
                    PlantId : SitePlantAddress.PlantId,
                    PlantName : SitePlantAddress.PlantName,
                    PlantAddress: SitePlantAddress.PlantAddress,
                    CityRegencyId: SitePlantAddress.CityRegencyId,
                    PostalCodeId: SitePlantAddress.PostalCodeId,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MSitesPlantAddress', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });