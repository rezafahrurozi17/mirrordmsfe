angular.module('app')
    .controller('SitePlantAddressController', function($scope, $http, CurrentUser, SitePlantAddressFactory, $timeout,bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mSitePlantAddressController = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data test
        //----------------------------------

        SitePlantAddressFactory.getDataCityRegency().then(
            function(res){
                $scope.cityRegency = res.data.Result;
            }
        );

         SitePlantAddressFactory.getDataKelurahan().then(function (res){
            $scope.kelurahan = res.data.Result;
        })

        var gridData = [];
        $scope.getData = function() {
            SitePlantAddressFactory.getData()
                 .then(
                     function(res) {
                        $scope.grid.data = res.data.Result;
                 },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
             );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'PlantId', width: '7%', visible: false },
                { name: 'Nama Plant', field: 'PlantName' },
                { name: 'Alamat', field: 'PlantAddress' },
                { name: 'Kota / Kabupaten', field: 'CityRegencyName'},
                { name: 'Kode Pos', field: 'PostalCode'},
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                }
            ]
        };
    });