angular.module('app')
  .factory('TypeRefund', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTypeRefund');
        //console.log('res=>',res);
// var da_json=[
//         {TypeRefundId: "1",  TypeRefundName: "name 1"},
//         {TypeRefundId: "2",  TypeRefundName: "name 2"},
//         {TypeRefundId: "3",  TypeRefundName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(typeRefund) {
        return $http.post('/api/sales/PCategoryTypeRefund', [{
                                            //AppId: 1,
                                            TypeRefundName: typeRefund.TypeRefundName}]);
      },
      update: function(typeRefund){
        return $http.put('/api/sales/PCategoryTypeRefund', [{
                                            OutletId : typeRefund.OutletId,
                                            TypeRefundId: typeRefund.TypeRefundId,
                                            TypeRefundName: typeRefund.TypeRefundName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTypeRefund',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd