angular.module('app')
    .factory('AssetBrosur', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        return {
            getData: function() {
                    var res=$http.get('/api/sales/MAssetBrochure');
                    //console.log('res=>',res);
                return res;
            },
            // getDataVehicleModel: function() {
            //         var res=$http.get('/api/sales/MUnitVehicleModel');
            //         //console.log('res=>',res);
            //     return res;
            // },
            create: function(AssetBrosur) {
                return $http.post('/api/sales/MAssetBrochure', [{
                            // VehicleModelId: AssetBrosur.VehicleModelId,
                            BrochureTypeId: AssetBrosur.BrochureTypeId,
                            BrochureName: AssetBrosur.BrochureName,
                            UploadDataBrochure: AssetBrosur.UploadDataBrochure,
                            ValidOn: AssetBrosur.ValidOn,
                            ValidUntil: AssetBrosur.ValidUntil,
                            ShowBit: AssetBrosur.ShowBit,
                            UploadDate: AssetBrosur.LastModifiedDate,
                            UploadById: currentUser.UserId,
                            InputByOrgTypeId: currentUser.OrgId

                }]);
            },
            update: function(AssetBrosur) {
                return $http.put('/api/sales/MAssetBrochure', [{
                            OutletId: AssetBrosur.OutletId,
                            BrochureId: AssetBrosur.BrochureId,
                            // VehicleModelId: AssetBrosur.VehicleModelId,
                            BrochureTypeId: AssetBrosur.BrochureTypeId,
                            BrochureName: AssetBrosur.BrochureName,
                            BrochureUrl: AssetBrosur.BrochureUrl,
                            ValidOn: AssetBrosur.ValidOn,
                            ValidUntil: AssetBrosur.ValidUntil,
                            ShowBit: AssetBrosur.ShowBit,
                            UploadDate: AssetBrosur.LastModifiedDate,
                            UploadById: currentUser.UserId,
                            InputByOrgTypeId: currentUser.OrgId
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MAssetBrochure', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd