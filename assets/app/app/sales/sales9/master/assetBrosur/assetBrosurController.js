angular.module('app')
    .controller('AssetBrosurController', function($scope, $http, CurrentUser,AssetBrochureTypeFactory, AssetBrosur, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            //$scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mAssetBrosur = {}; //Model
        $scope.mAssetBrosur.UploadDataBrochure = {};
        $scope.uploadFiles = [];
        //$scope.uploadFiles.push({Branch:"",DocumentTypeId:"",FileName:"",NameTable:"",TableRefId:"",strBase64:""});
        $scope.xRole = { selected: [] };
        
        //$scope.mAssetBrosur.ShowBit = false;

        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';

        $scope.dateOption = { startingDay: 1, format: dateFormat };
        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };
        $scope.dateOptionLastDate = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

         $scope.tanggalmin = function (dt){
            console.log("1",dt)
            $scope.dateOptionLastDate.minDate = dt;
            console.log("2",$scope.mintgl)
        }

        $scope.cekObject = function() {
            console.log($scope.mAssetBrosur);
        }

        // AssetBrosur.getDataVehicleModel().then(function (res){
        //     $scope.VehicleModel = res.data.Result;
        // })

        AssetBrochureTypeFactory.getData().then(function (res){
            $scope.BrochureType = res.data.Result;
        })
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            
            // for (var i = 0; i < $scope.grid.data.length; ++i)  {
            //     var validOn = new Date($scope.grid.data[i].ValidOn); 
            //     var validUntil = new Date($scope.grid.data[i].ValidUntil);          
            //     validOn.setDate(validOn.getDate() +1);
            //     validUntil.setDate(validUntil.getDate() +1);               
            //     $scope.grid.data[i].ValidUntil = new Date(validUntil);
            //     $scope.grid.data[i].ValidOn = new Date(validOn);                        
            // }
            AssetBrosur.getData()
                .then(
                    function(res){
                        $scope.grid.data = res.data.Result;
                        $scope.uploadFiles = [];
            //         gridData = [];
            //         //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
            //          = res.data.Result;
            //         console.log("AssetBrosur=>",res.data.Result);
            //         //console.log("grid data=>",$scope.grid.data);
            //         //$scope.roleData = res.data;
            //         $scope.loading=false;
            //     },
            //     function(err){
            //         console.log("err=>",err);
                 }
            );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
                console.log("onSelectRows=>", rows);
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------

        var viewDocument = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.viewDocuments(row.entity)">{{row.entity.BrochureName}}</u></p></a>';
        
        $scope.viewDocuments = function(row){
             $scope.dataStream = null;
            angular.element('.ui.modal.mAssetBrosururMaster').modal('show');
            $scope.BrocureNames = row.BrochureName;
            $scope.dataStream = row.BrochureData;

        }


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'id', field: 'BrochureId', visible: false },
                //{ name: 'vehicle model', field: 'VehicleModelName' },
                { name: 'brochure type', field: 'BrochureTypeName' },
                { name: 'brochure name', field: 'BrochureName' },
                { name: 'file modul', cellTemplate: viewDocument },
                { name: 'valid on', field: 'ValidOn', cellFilter: dateFilter },
                { name: 'valid until', field: 'ValidUntil', cellFilter: dateFilter }
            ]
        };
    });


angular.module('app').directive('myDirective', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      element.bind('change', function() {
        var formData = new FormData();
        formData.append('file', element[0].files[0]);

        // optional front-end logging 
        var fileObject = element[0].files[0];
        console.log("test", fileObject);
        // scope.fileLog = {
        //   'lastModified': fileObject.lastModified,
        //   'lastModifiedDate': fileObject.lastModifiedDate,
        //   'name': fileObject.name,
        //   'size': fileObject.size,
        //   'type': fileObject.type
        // };
        
        // scope.mAssetBrosur.BrochureUrl = "/imgs/content/Master/"+scope.fileLog.name;
        scope.$apply();

        /*  ---> post request to your php file and use $_FILES in your php file   < ----
        httpPostFactory('your_upload_image_php_file.php', formData, function (callback) {
            console.log(callback);
        });
        */
      });

    }
  };
});