angular.module('app')
    .factory('CategoryPointsourceFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/PCategoryPointSource');
                //console.log('res=>',res);
                // var res = [{
                //     "CategoryPointsourceId": 1,
                //     "CategoryPointsourceName": "Home"
                // }, {
                //     "CategoryPointsourceId": 2,
                //     "CategoryPointsourceName": "Phone"
                // }, {
                //     "CategoryPointsourceId": 3,
                //     "CategoryPointsourceName": "Office"
                // }];
                return res;
            },
            create: function(CategoryPointsource) {
                 return $http.post('/api/sales/PCategoryPointSource', [{
                    PointSourceName: CategoryPointsource.PointSourceName
                }]);
            },
            update: function(CategoryPointsource) {
                return $http.put('/api/sales/PCategoryPointSource', [{
                    OutletId : CategoryPointsource.OutletId,
                    PointSourceId: CategoryPointsource.PointSourceId,
                    PointSourceName: CategoryPointsource.PointSourceName
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/PCategoryPointSource', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd