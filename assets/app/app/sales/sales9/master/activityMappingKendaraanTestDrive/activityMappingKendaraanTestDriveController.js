angular.module('app')
    .controller('ActivityMappingKendaraanTestDriveController', function($scope, $http, CurrentUser, ActivityMappingKendaraanTestDrive,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
		$scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mActivityMappingKendaraanTestDrive = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
   $scope.getData = function() {
        ActivityMappingKendaraanTestDrive.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            //{ name:'Vehicle Mapping', field:'VehicleMappingId', width:'7%', visible:false },
            { name:'Vehicle Mapping', field:'VehicleMappingId'},
            { name:'TestDriveUnit', field:'TestDriveUnitId' },
            //{ name:'BranchOwnerId',  field: 'BranchOwnerId' },
			{ name:'Cabang Pemohon',  field: 'OutletRequestorId' },
			{ name:'Waktu Maksimal(Jam)', field: 'LimitTime' },
			{ name:'Batas Booking/hari', field: 'LimitBookingInADay' }
        ]
    };
});
