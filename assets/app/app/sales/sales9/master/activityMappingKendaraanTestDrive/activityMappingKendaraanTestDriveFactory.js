angular.module('app')
  .factory('ActivityMappingKendaraanTestDrive', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MAssetMappingVehicleTestDrive');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityMappingKendaraanTestDrive) {
        return $http.post('/api/sales/MAssetMappingVehicleTestDrive', [{
              VehicleMappingId: ActivityMappingKendaraanTestDrive.VehicleMappingId,
              TestDriveUnitId: ActivityMappingKendaraanTestDrive.TestDriveUnitId,
              OutletRequestorId: ActivityMappingKendaraanTestDrive.OutletRequestorId
            }]);
      },
      update: function(ActivityMappingKendaraanTestDrive){
        return $http.put('/api/sales/MAssetMappingVehicleTestDrive', [{
              OutletId: ActivityMappingKendaraanTestDrive.OutletId,
              VehicleMappingId: ActivityMappingKendaraanTestDrive.VehicleMappingId,
              TestDriveUnitId: ActivityMappingKendaraanTestDrive.TestDriveUnitId,
              OutletRequestorId: ActivityMappingKendaraanTestDrive.OutletRequestorId
            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAssetMappingVehicleTestDrive',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });