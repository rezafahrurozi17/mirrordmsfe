angular.module('app')
  .factory('AccountGLGroupFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "gl_group_id" : "1",
              "gl_group_code" : "001",
              "gl_group_name" : "Asset",
			  "AccountingCoa": [
				{"GLAccountId": "1",  "GLAccountCode": "GL01", "GLAccountName": "GL Name 01"},
				{"GLAccountId": "3",  "GLAccountCode": "GL03", "GLAccountName": "GL Name 03"},
			  ]
          },
          {
              "gl_group_id" : "2",
              "gl_group_code" : "002",
              "gl_group_name" : "Liability",
			  "AccountingCoa": [
				{"GLAccountId": "1",  "GLAccountCode": "GL01", "GLAccountName": "GL Name 01"},
				{"GLAccountId": "3",  "GLAccountCode": "GL03", "GLAccountName": "GL Name 03"},
			  ]
          },
          {
              "gl_group_id" : "3",
              "gl_group_code" : "003",
              "gl_group_name" : "Equity",
			  "AccountingCoa": [
				{"GLAccountId": "1",  "GLAccountCode": "GL01", "GLAccountName": "GL Name 01"},
				{"GLAccountId": "3",  "GLAccountCode": "GL03", "GLAccountName": "GL Name 03"},
			  ]
          },
          {
              "gl_group_id" : "4",
              "gl_group_code" : "004",
              "gl_group_name" : "Revenue",
			  "AccountingCoa": [
				{"GLAccountId": "1",  "GLAccountCode": "GL01", "GLAccountName": "GL Name 01"},
				{"GLAccountId": "2",  "GLAccountCode": "GL02", "GLAccountName": "GL Name 02"},
			  ]
          },
          {
              "gl_group_id" : "5",
              "gl_group_code" : "005",
              "gl_group_name" : "Expense",
			  "AccountingCoa": [
				{"GLAccountId": "1",  "GLAccountCode": "GL01", "GLAccountName": "GL Name 01"},
				{"GLAccountId": "3",  "GLAccountCode": "GL03", "GLAccountName": "GL Name 03"},
			  ]
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });








