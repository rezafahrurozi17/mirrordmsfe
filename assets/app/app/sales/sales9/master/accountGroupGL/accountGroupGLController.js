angular.module('app')
    .controller('AccountGLGroupController', function($scope, $http, CurrentUser, AccountGLGroupFactory,AccountingCOAFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAccountGLGroup = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=AccountGLGroupFactory.getData();
		$scope.optionsAccountingCoa =  AccountingCOAFactory.getData();
		
        $scope.loading=false;
    },
    function(err){
        bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){

        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){

    }
	
	$scope.RemoveAccountingCoa = function (index) {
		$scope.mAccountGLGroup.AccountingCoa.splice(index, 1);

    }
	
	$scope.AddAccountingCoa = function () {
		try
		{
			$scope.mAccountGLGroup.AccountingCoa.push($scope.The_Coa);
		}
		catch(err)
		{
			$scope.mAccountGLGroup.AccountingCoa=[{ GLAccountId: null,GLAccountCode:null,GLAccountName:null}];
			$scope.mAccountGLGroup.AccountingCoa.splice(0, 1);
			$scope.mAccountGLGroup.AccountingCoa.push($scope.The_Coa);
		}
    }
	
	$scope.SetSelectedGLAccount = function (selectedcoa) {
		$scope.The_Coa=selectedcoa;

    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'gl_group_id', field:'gl_group_id', width:'7%', visible:false },
            { name:'gl Group Code', field:'gl_group_code'},
            { name:'GL Group Name', field:'gl_group_name'},
			{ name:'Accounting Coa', field:'AccountingCoa', visible:false },
        ]
    };
});







