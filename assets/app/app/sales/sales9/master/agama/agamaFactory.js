angular.module('app')
  .factory('AgamaFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerReligion');
        return res;
      },
      create: function(Agama) {
        return $http.post('/api/sales/CCustomerReligion', [{
                                            //AppId: 1,
                                            CustomerReligionName: Agama.CustomerReligionName}]);
      },
      update: function(Agama){
        return $http.put('/api/sales/CCustomerReligion', [{
                                            CustomerReligionId: Agama.CustomerReligionId,
                                            CustomerReligionName: Agama.CustomerReligionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd