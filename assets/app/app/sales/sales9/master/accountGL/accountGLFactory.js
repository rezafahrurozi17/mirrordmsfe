angular.module('app')
  .factory('AccountGLFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "gl_id" : "1",
              "gl_group_id" : "2",
              "GL_name" : "Kas",
              "Value" : "1"
          },
          {
              "gl_id" : "2",
              "gl_group_id" : "1",
              "GL_name" : "Piutang",
              "Value" : "2"
          },
          {
              "gl_id" : "3",
              "gl_group_id" : "1",
              "GL_name" : "Perlengkapan",
              "Value" : "2"
          },
          {
              "gl_id" : "4",
              "gl_group_id" : "1",
              "GL_name" : "Piutang sewa",
              "Value" : "3"
          },
          {
              "gl_id" : "5",
              "gl_group_id" : "1",
              "GL_name" : "Hutang",
              "Value" : "4"
          },
          {
              "gl_id" : "6",
              "gl_group_id" : "1",
              "GL_name" : "Hutang gaji",
              "Value" : "4"
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });







