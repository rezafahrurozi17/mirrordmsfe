angular.module('app')
    .controller('VendorJasaController', function($scope, $http, CurrentUser, VendorJasaFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVendorJasa = null; //Modelnya
	$scope.xRole={selected:[]};
	console.log('$scope.user',$scope.user);	
	$scope.ShowVendorJasaMain=true;
	$scope.ShowVendorJasaDetail=false;
	$scope.VendorJasaOPeration="";
	$scope.disabledSimpanVendorJasa = false;
	
	$scope.$on('$destroy', function() {
	
		  angular.element('.ui.modal.ModalVendorJasa_Outlet').remove();
		  angular.element('.ui.modal.VendorJasaFormModalUpload').remove();

		});

    //----------------------------------
    // Get Data test
	//----------------------------------

	//Hotfix tombol generate

	$scope.VendorJasa_GenerateHargaBeli = function() {

		console.log('mVendorJasa.VendorTypeId',$scope.mVendorJasa.VendorTypeId);
		console.log('mVendorJasa.PriceArea',$scope.mVendorJasa.PriceArea);
		console.log('mVendorJasa.VehicleModelId',$scope.mVendorJasa.VehicleModelId);
		console.log('mVendorJasa.JasaId',$scope.mVendorJasa.JasaId);
		
		$scope.filterUrl = {};
		$scope.filterUrl.BusinessUnitId = angular.copy($scope.mVendorJasa.BusinessUnitId);
		$scope.filterUrl.VendorJasaId = angular.copy($scope.pilihVendorJasaId);
		$scope.filterUrl.PriceArea = angular.copy($scope.mVendorJasa.PriceArea);
		$scope.filterUrl.VehicleModelId = angular.copy($scope.mVendorJasa.VehicleModelId);
		$scope.filterUrl.JasaId = angular.copy($scope.mVendorJasa.JasaId);
		VendorJasaFactory.getDataHargaBeli($scope.filterUrl).then(
			function(res){
				$scope.gridVendorJasa_HargaBeli.data = res.data.Result[0].ListHargaBeli;
				//$scope.loading=false;
				//return res.data;
			},
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
		);
	}
	//end Hotfix tombol generate
	
	$scope.filter={VendorJasaCode:null, VendorJasaName:null, BusinessUnitId:null};
	VendorJasaFactory.getDataModel().then(
		function(res){
			$scope.OptionVendorJasaVehicleModel = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	
	VendorJasaFactory.getDataBusinessUnit().then(
		function(res){
			$scope.OptionsVendorJasaBusinessUnit = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	
	VendorJasaFactory.getDataBank($scope.user.OutletId).then(
		function(res){
			$scope.OptionsVendorJasaBank = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	
	VendorJasaFactory.getDataTipeVendor().then(
		function(res){
			$scope.TipeVendorOption = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	VendorJasaFactory.getDataProvinsi().then(
		function(res){
			$scope.ProvinsiOption = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	VendorJasaFactory.getDataPricingArea().then(
		function(res){
			$scope.OptionsVendorJasaPricingArea = res.data.Result;
			//$scope.loading=false;
			return res.data;
		}
	);
	// VendorJasaFactory.getDataJasa().then(
	// 	function(res){
	// 		$scope.OptionsVendorJasa_Jasa = res.data.Result;
	// 		//$scope.loading=false;
	// 		return res.data;
	// 	}
	// );
	

     $scope.getData = function() {
        VendorJasaFactory.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
	}
	

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.VendorJasa_Git_Dari_Grid_HargaBeli = function(){
		
		var da_donlot=[];
		var fileName = "VendorJasa_Download";
		
		if($scope.selectedVendorJasa.BusinessUnitName=='Biro Jasa')
		{
			for(var i = 0; i < $scope.gridVendorJasa_HargaBeli.data.length; ++i)
			{
				da_donlot.push({"PricingAreaCluster": $scope.gridVendorJasa_HargaBeli.data[i].PriceArea,
								"KodeJasa":$scope.gridVendorJasa_HargaBeli.data[i].JasaCode,
								"NamaJasa":$scope.gridVendorJasa_HargaBeli.data[i].JasaName,
								"Model":$scope.gridVendorJasa_HargaBeli.data[i].Description,
								"Katashiki":$scope.gridVendorJasa_HargaBeli.data[i].KatashikiCode,
								"Suffix":$scope.gridVendorJasa_HargaBeli.data[i].SuffixCode,
								"HargaNotice":$scope.gridVendorJasa_HargaBeli.data[i].PurchasePrice ? $scope.gridVendorJasa_HargaBeli.data[i].PurchasePrice : '',
								"HargaUnnotice":$scope.gridVendorJasa_HargaBeli.data[i].ServicePrice ? $scope.gridVendorJasa_HargaBeli.data[i].ServicePrice :'',
								"TanggalBerlakuDari":$scope.gridVendorJasa_HargaBeli.data[i].EffectiveDateFrom,
								"TanggalBerlakuSampai":$scope.gridVendorJasa_HargaBeli.data[i].EffectiveDateTo});				
			}
		}
		else if($scope.selectedVendorJasa.BusinessUnitName=='OPD' || $scope.selectedVendorJasa.BusinessUnitName=='Ekspedisi')
		{
			for(var i = 0; i < $scope.gridVendorJasa_HargaBeli.data.length; ++i)
			{
				da_donlot.push({"PricingAreaCluster": $scope.gridVendorJasa_HargaBeli.data[i].PriceArea,
								"KodeJasa":$scope.gridVendorJasa_HargaBeli.data[i].JasaCode,
								"NamaJasa":$scope.gridVendorJasa_HargaBeli.data[i].JasaName,
								"Model":$scope.gridVendorJasa_HargaBeli.data[i].Description,
								"Katashiki":$scope.gridVendorJasa_HargaBeli.data[i].KatashikiCode,
								"Suffix":$scope.gridVendorJasa_HargaBeli.data[i].SuffixCode,
								"HargaBeli":$scope.gridVendorJasa_HargaBeli.data[i].PurchasePrice,
								"TanggalBerlakuDari":$scope.gridVendorJasa_HargaBeli.data[i].EffectiveDateFrom,
								"TanggalBerlakuSampai":$scope.gridVendorJasa_HargaBeli.data[i].EffectiveDateTo});				
			}
		}
		
		
		console.log("isinyaa==>,", da_donlot);
		XLSXInterface.writeToXLSX(da_donlot, fileName);
	}
	
	$scope.SimpanVendorJasa = function(){
		$scope.disabledSimpanVendorJasa = true;
		$scope.mVendorJasa.ListHargaBeli=angular.copy($scope.gridVendorJasa_HargaBeli.data);
		$scope.mVendorJasa.ListInformasiBank=angular.copy($scope.gridVendorJasa_InformasiBank.data);
		$scope.mVendorJasa.ListOutlet=angular.copy($scope.gridVendorJasa_Outlet.data);

		
		if($scope.VendorJasaOPeration=="Insert")
		{
			VendorJasaFactory.create($scope.mVendorJasa).then(
				function(res) {
					VendorJasaFactory.getData($scope.filter).then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							$scope.KembaliDariVendorJasaDetail();
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
						}
					);
				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content:err.data.Message,
									type: 'danger'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
						}
			)
		}
		else if($scope.VendorJasaOPeration=="Update")
		{
			VendorJasaFactory.update($scope.mVendorJasa).then(
				function(res) {
					VendorJasaFactory.getData($scope.filter).then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							$scope.KembaliDariVendorJasaDetail();
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
						}
					);
				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content:err.data.Message,
									type: 'danger'
								}
							);
							$scope.disabledSimpanVendorJasa = false;
						}
			)
		}
	}
	
	$scope.TambahVendorJasa = function(){
		
		
		$scope.MasterVendorJasaDisableUpload=true;
		$scope.ShowVendorJasaMain=false;
		$scope.ShowVendorJasaDetail=true;
		$scope.VendorJasaOPeration="Insert";
		$scope.VendorJasaGridHargaRefresh();
		$scope.mVendorJasa={};
		
		$scope.gridVendorJasa_InformasiBank.data=[];
		$scope.gridVendorJasa_HargaBeli.data=[];
		$scope.gridVendorJasa_Outlet.data=[];
		
		}
		
	$scope.VendorJasaGridHargaRefresh = function(){
			$scope.VendorJasa_ExcelData=[];
			$scope.fileNameVendorJasa=null;
			document.getElementById('UploadvendorJasa').value=null;
			$scope.gridVendorJasa_HargaBeli.data=[];
			$scope.VendorJasaHargaValid=0;
			$scope.VendorJasaHargaTidakValid=0;
			$scope.VendorJasaHargaDaTotal=0;
			angular.element('.ui.modal.VendorJasaFormModalUpload').modal('hide');
		}
	
	$scope.LihatVendorJasa = function(rows){
		$scope.MasterVendorJasaDisableUpload=true;
		$scope.ShowVendorJasaMain=false;
		$scope.ShowVendorJasaDetail=true;
		$scope.VendorJasaOPeration="Lihat";
		$scope.VendorJasaGridHargaRefresh();
		$scope.mVendorJasa={};
		$scope.pilihVendorJasaId = rows.VendorJasaId;
		
		VendorJasaFactory.getVendorJasaDetail(rows.VendorJasaId).then(
			function(res){
				$scope.mVendorJasa=angular.copy(res.data.Result[0]);
				$scope.VendorJasaPaksaGridHarga();
				$scope.VendorJasaProvinsiSelected($scope.mVendorJasa);
				
				$scope.gridVendorJasa_HargaBeli.data=$scope.mVendorJasa.ListHargaBeli;
				$scope.gridVendorJasa_InformasiBank.data=$scope.mVendorJasa.ListInformasiBank;
				$scope.gridVendorJasa_Outlet.data=$scope.mVendorJasa.ListOutlet;
				
				$scope.VendorJasaHargaValid=$scope.mVendorJasa.ListHargaBeli[0].ValidQty;
				$scope.VendorJasaHargaTidakValid=$scope.mVendorJasa.ListHargaBeli[0].NonValidQty;
				$scope.VendorJasaHargaDaTotal=$scope.mVendorJasa.ListHargaBeli[0].TotalData;
				
			}
		);
	
		
		
		}
	
		$scope.VendorJasaPaksaGridHarga = function(){
			for(var i = 0; i < $scope.OptionsVendorJasaBusinessUnit.length; ++i)
			{	
				if($scope.OptionsVendorJasaBusinessUnit[i].BusinessUnitId == $scope.mVendorJasa.BusinessUnitId)
				{
					$scope.VendorJasaVendorJasaSelected($scope.OptionsVendorJasaBusinessUnit[i]);
					break;
				}
			}
		}
		

	$scope.UpdateVendorJasa = function(rows){
		$scope.MasterVendorJasaDisableUpload=true;
		$scope.ShowVendorJasaMain=false;
		$scope.ShowVendorJasaDetail=true;
		$scope.VendorJasaOPeration="Update";
		$scope.VendorJasaGridHargaRefresh();
		$scope.mVendorJasa={};
		
		VendorJasaFactory.getVendorJasaDetail(rows.VendorJasaId).then(
				function(res){
					$scope.mVendorJasa=angular.copy(res.data.Result[0]);
					$scope.VendorJasaPaksaGridHarga();
					$scope.VendorJasaProvinsiSelected($scope.mVendorJasa);
					
					$scope.gridVendorJasa_HargaBeli.data=$scope.mVendorJasa.ListHargaBeli;
					$scope.gridVendorJasa_InformasiBank.data=$scope.mVendorJasa.ListInformasiBank;
					$scope.gridVendorJasa_Outlet.data=$scope.mVendorJasa.ListOutlet;
					
					$scope.VendorJasaHargaValid=$scope.mVendorJasa.ListHargaBeli[0].ValidQty;
					$scope.VendorJasaHargaTidakValid=$scope.mVendorJasa.ListHargaBeli[0].NonValidQty;
					$scope.VendorJasaHargaDaTotal=$scope.mVendorJasa.ListHargaBeli[0].TotalData;
					
				}
			);
		
		
		}
		
	$scope.KembaliDariVendorJasaDetail = function(){
		$scope.ShowVendorJasaMain=true;
		$scope.ShowVendorJasaDetail=false;
		$scope.VendorJasaOPeration="";
		$scope.mVendorJasa={};
		}	
	
	$scope.DeleteVendorJasa_InformationBank = function (row) {
		$scope.gridVendorJasa_InformasiBank.data.splice($scope.gridVendorJasa_InformasiBank.data.indexOf(row),1);
		}
		
	$scope.DeleteVendorJasa_Outlet = function (row) {
		$scope.gridVendorJasa_Outlet.data.splice($scope.gridVendorJasa_Outlet.data.indexOf(row),1);
		}	
	
	$scope.VendorJasaProvinsiSelected = function (selected) {
		VendorJasaFactory.getDataKota('?start=1&limit=100&filterData=ProvinceId|' + selected.ProvinceId).then(function(res) {
                    $scope.VendorJasaKota = res.data.Result;
                });
	}

	$scope.ModalVendorJasa_Outlet_Hilang = function(){
		angular.element('.ui.modal.ModalVendorJasa_Outlet').modal('hide');
		$scope.selctedRowVendorJasaOutletModal=[];

		}
	
	$scope.VendorJasaOutletPilih = function(){
		var setan=angular.copy($scope.selctedRowVendorJasaOutletModal);
		
		for(var i = 0; i < setan.length; ++i)
		{
			setan[i].OutletName=setan[i].Name;
		}
		
		for(var x = 0; x < setan.length; ++x)
		{
			$scope.gridVendorJasa_Outlet.data.push(setan[x]);
		}

		console.log("setan",$scope.gridVendorJasa_Outlet.data);
		$scope.ModalVendorJasa_Outlet_Hilang();
		
		}

	$scope.VendorJasaVendorJasaSelected = function(selected){
			$scope.ColumngridVendorJasa_HargaBeli[6].visible = false;
			$scope.ColumngridVendorJasa_HargaBeli[7].visible = false;
			$scope.ColumngridVendorJasa_HargaBeli[8].visible = false;
			
			$scope.selectedVendorJasa =angular.copy(selected); 

			VendorJasaFactory.getDataKodeJasaFilter(selected.BusinessUnitId).then(
				function(res){
					$scope.OptionsVendorJasa_Jasa = res.data.Result;
					//$scope.loading=false;
					//return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);

			
			if(selected.BusinessUnitName=='Biro Jasa')
			{
				$scope.ColumngridVendorJasa_HargaBeli[6].visible = true;
				$scope.ColumngridVendorJasa_HargaBeli[7].visible = true;
			}
			else if(selected.BusinessUnitName=='OPD' || selected.BusinessUnitName=='Ekspedisi')
			{
				$scope.ColumngridVendorJasa_HargaBeli[8].visible = true;
			}
			$scope.gridApiVendorJasa_HargaBeli.grid.refresh();
		
		}
	
	
	$scope.TambahVendorJasa_Outlet = function () {
	setTimeout(function() {
		angular.element('.ui.modal.ModalVendorJasa_Outlet').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalVendorJasa_Outlet').not(':first').remove(); 
		}, 1);
		VendorJasaFactory.getDataOutlet().then(
			function(res){
				$scope.Modal_gridVendorJasa_Outlet.data = res.data.Result;
				return res.data;
			}
		);
	}

	$scope.TambahVendorJasa_InformationBank = function () {
		var da_bank_name="";
		for(var i = 0; i < $scope.OptionsVendorJasaBank.length; ++i)
		{	
			if($scope.OptionsVendorJasaBank[i].BankId == $scope.mVendorJasa.BankId)
			{
				da_bank_name=$scope.OptionsVendorJasaBank[i].BankName;
				break;
			}
		}
		
		$scope.gridVendorJasa_InformasiBank.data.push({"BankId":$scope.mVendorJasa.BankId,"BankName":da_bank_name,"AccountNumber":$scope.mVendorJasa.AccountNumber,"AccountName":$scope.mVendorJasa.AccountName});
	
	}
		
	$scope.VendorJasa_Git_Da_Template = function() {

			if((typeof $scope.mVendorJasa.BusinessUnitId=="undefined"))
			{
				$scope.mVendorJasa.BusinessUnitId=null;
			}
			
			if((typeof $scope.mVendorJasa.VehicleModelId=="undefined"))
			{
				$scope.mVendorJasa.VehicleModelId=null;
			}
			
			if((typeof $scope.mVendorJasa.JasaId=="undefined"))
			{
				$scope.mVendorJasa.JasaId=null;
			}
			
			if((typeof $scope.mVendorJasa.PriceArea=="undefined"))
			{
				$scope.mVendorJasa.PriceArea=null;
			}
			
			var da_search={"BusinessUnitId":$scope.mVendorJasa.BusinessUnitId,"VehicleModelId":$scope.mVendorJasa.VehicleModelId,"JasaId":$scope.mVendorJasa.JasaId,"PriceArea":$scope.mVendorJasa.PriceArea};

			VendorJasaFactory.getVendorJasaDaExcel(da_search).then(
				function(res){
					var da_get_thingy = res.data.Result;
					console.log('da_get_thingy',da_get_thingy);
					var excelData = [];
					var fileName = "";
					$scope.gridVendorJasa_HargaBeli.data=[];
					console.log('vendorjasa',$scope.selectedVendorJasa);

					if($scope.selectedVendorJasa.BusinessUnitName=='Biro Jasa')
					{
						//console.log('vendorjasa',$scope.selectedVendorJasa);
						for(var i = 0; i < da_get_thingy.length; ++i)
						{
							excelData.push({"PricingAreaCluster": da_get_thingy[i].PriceArea,
											"KodeJasa":da_get_thingy[i].JasaCode,
											"NamaJasa":da_get_thingy[i].JasaName,
											"Model":da_get_thingy[i].Description,
											"Katashiki":da_get_thingy[i].KatashikiCode,
											"Suffix":da_get_thingy[i].SuffixCode,
											"HargaNotice":"",
											"HargaUnnotice":"",
											"TanggalBerlakuDari":da_get_thingy[i].EffectiveDateFrom,
											"TanggalBerlakuSampai":da_get_thingy[i].EffectiveDateTo});
						}
					}
					else if($scope.selectedVendorJasa.BusinessUnitName=='OPD' || $scope.selectedVendorJasa.BusinessUnitName=='Ekspedisi')
					{
						//console.log('vendorjasa',$scope.selectedVendorJasa);
						
						for(var i = 0; i < da_get_thingy.length; ++i)
						{	
							excelData.push({"PricingAreaCluster": da_get_thingy[i].PriceArea,
											"KodeJasa": da_get_thingy[i].JasaCode,
											"NamaJasa":da_get_thingy[i].JasaName,
											"Model":da_get_thingy[i].Description,
											"Katashiki":da_get_thingy[i].KatashikiCode,
											"Suffix":da_get_thingy[i].SuffixCode,
											"HargaBeli":"",
											"TanggalBerlakuDari":da_get_thingy[i].EffectiveDateFrom,
											"TanggalBerlakuSampai":da_get_thingy[i].EffectiveDateTo});
						}
					}
					fileName = "VendorJasa_Template";

					XLSXInterface.writeToXLSX(excelData, fileName);
				}
			);

			
        }
		
	$scope.openDaVendorJasa = function () {
            document.getElementById("UploadvendorJasa").select();//tadi ga ada
        }	

	$scope.VendorJasaloadXLS = function(ExcelFile) {
			angular.element('.ui.modal.VendorJasaFormModalUpload').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.VendorJasaFormModalUpload').not(':first').remove();
             
			
			$scope.MasterVendorJasaDisableUpload=true;
			var ExcelData = [];
            var myEl = angular.element(document.querySelector('#UploadvendorJasa')); //ambil elemen dari dokumen yang di-upload 
			$scope.fileNameVendorJasa = myEl.context.files[0].name;//tadi ga ada
			var Cek_Da_Extention = myEl.context.value;
			var DapetPanjangBenda = Cek_Da_Extention.split(".");
			if(DapetPanjangBenda.length>=3)
			{
				
				
				$scope.MasterVendorJasaDisableUpload=true;
				angular.element('.ui.modal.VendorJasaFormModalUpload').modal('hide');
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Nama file tidak boleh menggunakan '.'",
						type: 'warning'
					}
				);
			}
			else if(DapetPanjangBenda.length<3)
			{
				XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
					if(json.length>0)
					{
						$scope.MasterVendorJasaDisableUpload=false;
						ExcelData = json[0];
						$scope.VendorJasa_ExcelData = json;
						angular.element('.ui.modal.VendorJasaFormModalUpload').modal('hide');
					}
					else if(json.length<=0)
					{
						$scope.MasterVendorJasaDisableUpload=true;
						angular.element('.ui.modal.VendorJasaFormModalUpload').modal('hide');
						ExcelData = json[0];
						$scope.VendorJasa_ExcelData = json;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "File yang anda masukkan kosong",
								type: 'warning'
							}
						);
					}
				});
			}
			
            
        }
		
	$scope.VendorJasa_Upload_Clicked = function() {
        var inputData = [];
        angular.forEach($scope.VendorJasa_ExcelData, function(value, key) {
			var da_PurchasePrice="";
			if($scope.selectedVendorJasa.BusinessUnitName=='Biro Jasa')
			{//if($scope.mVendorJasa.BusinessUnitId==17)
				if(value.HargaNotice!=null && value.HargaNotice!="" && (typeof value.HargaNotice!="undefined"))
				{
					da_PurchasePrice=value.HargaNotice;
				}
				
			}
			else if($scope.selectedVendorJasa.BusinessUnitName=='OPD' || $scope.selectedVendorJasa.BusinessUnitName=='Ekspedisi')
			{//$scope.mVendorJasa.BusinessUnitId==7 || $scope.mVendorJasa.BusinessUnitId==5
				if(value.HargaBeli!=null && value.HargaBeli!="" && (typeof value.HargaBeli!="undefined"))
				{
					da_PurchasePrice=value.HargaBeli;
				}
			}
			
			var da_ServicePrice="";
			if(value.HargaUnnotice!=null && value.HargaUnnotice!="" || (typeof value.HargaUnnotice!="undefined"))
			{
				da_ServicePrice=value.HargaUnnotice;
			}
			
			var da_BusinessUnitName="";
			for(var x = 0; x < $scope.OptionsVendorJasaBusinessUnit.length; ++x)
			{	
				if($scope.OptionsVendorJasaBusinessUnit[x].BusinessUnitId == $scope.mVendorJasa.BusinessUnitId)
				{
					da_BusinessUnitName=$scope.OptionsVendorJasaBusinessUnit[x].BusinessUnitName;
					break;
				}
			}
			
			inputData.push({ "VendorJasaPriceId" : "",
							"VendorJasaId" : "",
							"VendorCode" : $scope.mVendorJasa.VendorCode,
							"Name" : $scope.mVendorJasa.Name,
							"BusinessUnitId" : $scope.mVendorJasa.BusinessUnitId,
							"BusinessUnitName" : da_BusinessUnitName,
							"PriceArea" : value.PricingAreaCluster,
							"JasaVehicleTypeId" : "",
							"JasaId" : "",
							"JasaCode" : value.KodeJasa,
							"JasaName" : value.NamaJasa,
							"VehicleTypeId" : "",
							"Description" : value.Model,
							"KatashikiCode" : value.Katashiki,
							"SuffixCode" : value.Suffix,
							"PurchasePrice" : da_PurchasePrice,
							"ServicePrice" : da_ServicePrice,
							"EffectiveDateFrom" : value.TanggalBerlakuDari,
							"EffectiveDateTo" : value.TanggalBerlakuSampai,
							"Remarks" : ""});
							
        });

		VendorJasaFactory.ValidasiVendorJasa(inputData).then(
			function(res){
				var MasukinKeTabelVendorJasaHarga = res.data.Result;
				
				$scope.VendorJasaHargaValid=MasukinKeTabelVendorJasaHarga[0].ValidQty;
				$scope.VendorJasaHargaTidakValid=MasukinKeTabelVendorJasaHarga[0].NonValidQty;
				$scope.VendorJasaHargaDaTotal=MasukinKeTabelVendorJasaHarga[0].TotalData;
				
				//$scope.gridApiVendorJasa_HargaBeli.grid.refresh();
				
				$scope.gridVendorJasa_HargaBeli.data=MasukinKeTabelVendorJasaHarga;
				console.log("setan",$scope.gridVendorJasa_HargaBeli.data);
				//$scope.gridApiVendorJasa_HargaBeli.grid.refresh();
			}
		);
        //angular.element(document.querySelector('#UploadvendorJasa')).val(null);
	}	
	
	
	
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
		enableColumnResizing: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Kode Vendor',field:'VendorCode'},
            { name:'Nama Vendor', field:'Name' },
			{ name:'Bisnis Unit', field:'BusinessUnitName' },
			{ name: 'Action', visible: true, field: '', width: '12%', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatVendorJasa(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateVendorJasa(row.entity)" ></i>' },
        ]
    };
	
	$scope.gridVendorJasa_InformasiBank = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
		enableColumnResizing: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        columnDefs: [
            { name:'Nama Bank',field:'BankName'},
            { name:'No Rekening', field:'AccountNumber' },
			{ name:'Atas Nama Bank', field:'AccountName' },
			{ name:'Action', field:'' ,cellTemplate:'<i style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.VendorJasaOPeration==\'Lihat\'" ng-click="grid.appScope.DeleteVendorJasa_InformationBank(row.entity)" class="fa fa-times" aria-hidden="true"></i>'}
        ]
	};
	
	$scope.gridVendorJasa_InformasiBank.onRegisterApi = function(gridApi) {
		$scope.gridApi = gridApi;
		gridApi.selection.on.rowSelectionChanged($scope, function(row) {
			$scope.selctedRowVendorJasaInformasiBank = gridApi.selection.getSelectedRows();
		});
		gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
			$scope.selctedRowVendorJasaInformasiBank = gridApi.selection.getSelectedRows();
		});
	};
	
	$scope.ColumngridVendorJasa_HargaBeli=[
            { name:'Pricing Area Cluster',field:'PriceArea'},
            { name:'Kode Jasa', field:'JasaCode' },
			{ name:'Nama Jasa', field:'JasaName' },
			{ name:'Model', field:'Description' },
			{ name:'Katashiki', field:'KatashikiCode' },
			{ name:'Suffix', field:'SuffixCode' },
			{ name:'Harga Notice', field:'PurchasePrice' , visible: false },
			{ name:'Harga Unnotice', field:'ServicePrice', visible: false  },
			{ name:'Harga Beli', field:'PurchasePrice' , visible: false },
			{ name:'Tanggal Berlaku Dari', field:'EffectiveDateFrom' },
			{ name:'Tanggal Berlaku Sampai', field:'EffectiveDateTo' },
			{ name:'Keterangan', field:'Remarks' },
        ];
	
	$scope.gridVendorJasa_HargaBeli = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
		enableColumnResizing: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        columnDefs: $scope.ColumngridVendorJasa_HargaBeli
    };
	
	$scope.gridVendorJasa_HargaBeli.onRegisterApi = function(gridApi) {
		$scope.gridApiVendorJasa_HargaBeli = gridApi;
		gridApi.selection.on.rowSelectionChanged($scope, function(row) {
			$scope.selctedRowVendorJasaHargaBeli = gridApi.selection.getSelectedRows();
		});
		gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
			$scope.selctedRowVendorJasaHargaBeli = gridApi.selection.getSelectedRows();
		});
	};
	
	$scope.gridVendorJasa_Outlet = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
		enableColumnResizing: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        columnDefs: [
            { name:'Kode Outlet',field:'OutletCode'},
            { name:'Nama Outlet', field:'OutletName' },
			{ name:'Action', field:'' ,cellTemplate:'<i style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.VendorJasaOPeration==\'Lihat\'" ng-click="grid.appScope.DeleteVendorJasa_Outlet(row.entity)" class="fa fa-times" aria-hidden="true"></i>'}
        ]
    };
	
	$scope.Modal_gridVendorJasa_Outlet = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
		enableColumnResizing: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        columnDefs: [
            { name:'Kode Outlet',field:'OutletCode'},
            { name:'Nama Outlet', field:'Name' }
        ]
    };
	
	$scope.Modal_gridVendorJasa_Outlet.onRegisterApi = function(gridApi) {
		$scope.gridApi = gridApi;
		gridApi.selection.on.rowSelectionChanged($scope, function(row) {
			$scope.selctedRowVendorJasaOutletModal = gridApi.selection.getSelectedRows();
		});
		gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
			$scope.selctedRowVendorJasaOutletModal = gridApi.selection.getSelectedRows();
		});
	};
	
});


