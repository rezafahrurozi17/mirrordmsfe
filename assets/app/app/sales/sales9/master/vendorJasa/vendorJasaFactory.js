angular.module('app')
  .factory('VendorJasaFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var ulala="";
		if(filter.VendorJasaCode!=null)
		{
			
			if(filter.VendorJasaName==null && filter.BusinessUnitId==null)
			{
				ulala+="&VendorJasaCode="+filter.VendorJasaCode;
			}
			else
			{
				ulala+="&VendorJasaCode="+filter.VendorJasaCode;
			}
		}
		
		if(filter.VendorJasaName!=null)
		{
			
			if(filter.VendorJasaCode==null && filter.BusinessUnitId==null)
			{
				ulala+="&VendorJasaName="+filter.VendorJasaName;
			}
			else
			{
				ulala+="&VendorJasaName="+filter.VendorJasaName;
			}
		}
		
		if(filter.BusinessUnitId!=null)
		{
			
			if(filter.VendorJasaCode==null && filter.VendorJasaName==null)
			{
				ulala+="&BusinessUnitId="+filter.BusinessUnitId;
			}
			else
			{
				ulala+="&BusinessUnitId="+filter.BusinessUnitId;
			}
		}
		
		var res=$http.get('/api/sales/VendorJasaMaster/?start=1&limit=100000'+ulala);
        return res;
      },
	  
	  getVendorJasaDaExcel: function(filter) {
		var ulala="";
		if(filter.BusinessUnitId!=null)
		{
			ulala+="BusinessUnitId="+filter.BusinessUnitId;
		}
		
		if(filter.VehicleModelId!=null)
		{
			ulala+="&VehicleModelId="+filter.VehicleModelId;
		}
		
		if(filter.JasaId!=null)
		{
			ulala+="&JasaId="+filter.JasaId;
		}
		
		if(filter.PriceArea!=null)
		{
			ulala+="&PriceArea="+filter.PriceArea;
		}
		
		var res=$http.get('/api/sales/DownloadTemplateHargaBeliVendorJasa/?'+ulala);
        return res;
			},
			
			getDataKodeJasaFilter: function(selectedbisinisid) {
        var res=$http.get('/api/sales/AdminHandlingJasa/?start=1&limit=100000&BusinessUnitId='+selectedbisinisid);
        return res;
			},
			
		
	  getDataJasa: function() {
        var res=$http.get('/api/sales/AdminHandlingJasa');
        return res;
      },
		
	  getVendorJasaDetail: function(VendorJasaId) {
        var res=$http.get('/api/sales/VendorJasaDetail/?VendorJasaId='+VendorJasaId);
        return res;
	  },	  
	   
	  getDataPricingArea: function() {
        var res=$http.get('/api/sales/PESellingPriceAreaPricing');
        return res;
      },
	  getDataBusinessUnit: function() {
        var res=$http.get('/api/sales/MVendorBusinessUnit');
        return res;
      },
	  getDataOutlet: function() {
        var res=$http.get('/api/sales/MSitesOutlet');
        return res;
      },
	  getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
        return res;
      },
      getDataTipeVendor: function() {
        var res=$http.get('/api/sales/MVendorType');
        return res;
      },
      
      getDataProvinsi: function() {
        var res=$http.get('/api/sales/MLocationProvince');
        return res;
      },
	  getDataKota: function(param) {
        var res=$http.get('/api/sales/MLocationCityRegency'+param);
        return res;
      },
	  
	  ValidasiVendorJasa: function(validasi) {
        var res=$http.post('/api/sales/VendorJasa/Validasi',validasi);
        return res;
      },
	  
	  getDataBank: function(setan) {
            var res=$http.get('/api/sales/AdminHandlingBank/?start=1&limit=1000000&filterData=OutletId|'+setan);
            return res;
        },
	  
      create: function(VendorJasa) {
        return $http.post('/api/sales/VendorJasa/Insert', [VendorJasa]);
      },
      update: function(VendorJasa){
        return $http.put('/api/sales/VendorJasa/Update',[VendorJasa]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
	  },

	  //HotFix VendorJasa
	  
	  getDataHargaBeli: function(filter) {
		
		var param = $httpParamSerializer(filter);
			
        var res=$http.get('/api/sales/HargaBeliVendorJasa/?'+param);
        return res;
	  },

	  //End HotFix VendorJasa
    }
  });
 //ddd