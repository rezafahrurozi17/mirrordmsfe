angular.module('app')
.controller('SampleUITemplateController', function($scope, $http, CurrentUser, SampleUITemplateFactory,$timeout) {
    
	var listData = [{
                Id: "1",
                Name: "Option 1"
            },
            {
                Id: "2",
                Name: "Option 2"
            },
            {
                Id: "3",
                Name: "Option 3"
            },

        ];

    $scope.selectionItem = listData;
		
	$scope.intended_operation="None";
	$scope.LookSelected = function (SampleUITemplateFactory) {

	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.ChangeSelected = function (SalesProgramId,SalesProgramName) {
	   $scope.intended_operation="Update_Ops";
	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Batal_Button = true;
	   $scope.Submit_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Batal_Clicked = function () {

	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.intended_operation="None";
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Add_Clicked = function () {
			
		   $scope.mProfileSalesProgram_SalesProgramName=null;
		   $scope.intended_operation="Insert_Ops";
		   
		   $scope.ShowParameter = !$scope.ShowParameter;
		   $scope.ShowTable = !$scope.ShowTable;
		   $scope.Submit_Button = true;
		   $scope.Batal_Button = true;
		   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Simpan_Clicked = function () {
		//put httppost,delete,cll here
		if($scope.intended_operation=="Update_Ops")
		{
			alert("update");
		}
		else if($scope.intended_operation="Insert_Ops")
		{
			alert("insert");
		}
		
		//clear all input stuff first 
	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	   $scope.intended_operation="None";
	};
		
});



 
// angular.module('app').factory('SampleUITemplateFactory', function ($http) {
      // return {
          // getData: function () {
              // //var res = $http.get('file:///C:/Git/DMS-Sales-Prototype-Mike/DMS.Website/assets/app/app/sales/master/profileSalesProgram/the_thing.json');
				// var res = null;
			  // //uda bisa sampe sini ternyata
              // return res;
          // }
      // }
  // });
