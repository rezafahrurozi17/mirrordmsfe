angular.module('app')
  .factory('FinanceFundSourceFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SNegotiationBookingUnitFundSource');
        //console.log('res=>',res);
        var da_json=[
				{FundSourceId: "1", FundSourceName: "Name 01", FundSourceValue: "Value 01"},
        {FundSourceId: "2", FundSourceName: "Name 02", FundSourceValue: "Value 01"},
        {FundSourceId: "3", FundSourceName: "Name 03", FundSourceValue: "Value 01"},
				
			  ];
        //res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/SNegotiationBookingUnitFundSource', [{
                                            FundSourceName:ActivityChecklistItemAdministrasiPembayaran.FundSourceName,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/SNegotiationBookingUnitFundSource', [{
                                            FundSourceId:ActivityChecklistItemAdministrasiPembayaran.FundSourceId,
											OutletId:ActivityChecklistItemAdministrasiPembayaran.OutletId,
											FundSourceName:ActivityChecklistItemAdministrasiPembayaran.FundSourceName,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnitFundSource',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd