angular.module('app')
    .factory('UnitAksesorisFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                //var res=$http.get('/api/fw/Role');
                //console.log('res=>',res);
                var res = [
                    { AksesorisId: "1", NamaAksesoris: "Okto Van Roy", Stock: "200", Price: "2000000", UomId: 1, UomName: "UOM A" },
                    { AksesorisId: "2", NamaAksesoris: "Jhon Snow", Stock: "300", Price: "3000000", UomId: 2, UomName: "UOM B" },
                    { AksesorisId: "3", NamaAksesoris: "Rey Renaldi", Stock: "100", Price: "1000000", UomId: 1, UomName: "UOM A" }
                ];
                return res;
            },
            create: function(UnitAksesoris) {
                return $http.post('/api/fw/Role', [{

                    NamaAksesoris: UnitAksesoris.NamaAksesoris
                }]);
            },
            update: function(UnitAksesoris) {
                return $http.put('/api/fw/Role', [{
                    AksesorisId: UnitAksesoris.AksesorisId,
                    //pid: negotiation.pid,
                    NamaAksesoris: UnitAksesoris.NamaAksesoris
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });