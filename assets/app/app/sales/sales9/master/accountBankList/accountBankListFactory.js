angular.module('app')
  .factory('AccountBankListFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FFinanceListBank');
        //console.log('res=>',res);
        var data = [
          {
              "bank_id" : "1",
              "bank_code" : "002",
              "bank_name" : "BANK BCA",
              "active_bit" : "1"
          },
          {
              "bank_id" : "2",
              "bank_code" : "009",
              "bank_name" : "BANK BNI",
              "active_bit" : "1"
          },
           {
              "bank_id" : "3",
              "bank_code" : "003",
              "bank_name" : "BANK MANDIRI",
              "active_bit" : "1"
          },
        ];
        //res = data;
        return res;
      },
      create: function(AccountBankList) {
        return $http.post('/api/sales/FFinanceListBank', [{
                                            BankName : AccountBankList.BankName,
                                            }]);
      },
      update: function(AccountBankList){
        return $http.put('/api/sales/FFinanceListBank', [{
                                            BankId : AccountBankList.BankId,
											OutletId : AccountBankList.OutletId,
                                            BankName : AccountBankList.BankName,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FFinanceListBank',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });





