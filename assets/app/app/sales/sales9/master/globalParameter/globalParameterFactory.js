angular.module('app')
  .factory('GlobalParameterFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getDataAdh: function() {
        var res=$http.get('/api/sales/GetSGlobalParam?ParamType=3');
        return res;
      },
	  getDataAdminTam: function() {
        var res=$http.get('/api/sales/GetSGlobalParam?ParamType=1');
        return res;
      },
	  UbahDataGlobalParameterEmangSamaSemua: function(ToSimpan) {
		return $http.post('/api/sales/SGlobalParam',ToSimpan );
      },
	  getDataAdminHO: function() {
        var res=$http.get('/api/sales/GetSGlobalParam?ParamType=2');
        return res;
      },
	  GetGlobalParameter1Sama2Options: function () {
		var da_json = [
          { SearchOptionId: 1, SearchOptionName: 1 },
		  { SearchOptionId: 2, SearchOptionName: 2 },
        ];
        var res=da_json
        return res;
      },
      create: function(GlobalParameter) {
        return $http.post('/api/sales/GlobalParameter', [{
                                            ParameterName: GlobalParameter.ParameterName,
                                            ParameterValue: GlobalParameter.ParameterValue
                                            }]);
      },
      updateAdh: function(GlobalParameter){
        return $http.put('/api/sales/GlobalParameter', [{
                                            OutletId: GlobalParameter.OutletId,
                                            ParameterId: GlobalParameter.ParameterId,
                                            ParameterName: GlobalParameter.ParameterName,
                                            ParameterValue: GlobalParameter.ParameterValue
                                            }]);
      },
	  updateAdminTam: function(GlobalParameter){
        return $http.put('/api/sales/GlobalParameter', [{
                                            OutletId: GlobalParameter.OutletId,
                                            ParameterId: GlobalParameter.ParameterId,
                                            ParameterName: GlobalParameter.ParameterName,
                                            ParameterValue: GlobalParameter.ParameterValue
                                            }]);
      },updateAdminHO: function(GlobalParameter){
        return $http.put('/api/sales/GlobalParameter', [{
                                            OutletId: GlobalParameter.OutletId,
                                            ParameterId: GlobalParameter.ParameterId,
                                            ParameterName: GlobalParameter.ParameterName,
                                            ParameterValue: GlobalParameter.ParameterValue
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/GlobalParameter',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd