angular.module('app')
    .controller('GlobalParameterController', function($scope, $http, CurrentUser, GlobalParameterFactory,$timeout,bsNotify) {
    
	$scope.user = CurrentUser.user();
	
	$scope.mGlobalParameterAdhGet="";
	$scope.mGlobalParameterAdminTamGet="";
	$scope.mGlobalParameterAdminHoGet="";
	
	$scope.optionGlobalParameter1Sama2 = GlobalParameterFactory.GetGlobalParameter1Sama2Options();

	//----------------------------------
    // Initialization
    //----------------------------------
	$scope.disabledSimpanGlobalParameter = false;

	function GlobalParameterDaGitAll()
	{
		if($scope.user.RoleName == 'ADH')
		{
			GlobalParameterFactory.getDataAdh().then(function(res){
				$scope.mGlobalParameterAdhGet = angular.copy(res.data.Result);
				$scope.mGlobalParameterAdh={};
				
				for(var i = 0; i < $scope.mGlobalParameterAdhGet.length; ++i)
				{
					if($scope.mGlobalParameterAdhGet[i].ParamKey=="AH_BLAFI")
					{
						if($scope.mGlobalParameterAdhGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdh.UrutanAfi=2;
							$scope.mGlobalParameterAdh.UrutanBilling=1;
							$scope.mGlobalParameterAdh.AfiAuto=true;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==2)
						{
							$scope.mGlobalParameterAdh.UrutanAfi=2;
							$scope.mGlobalParameterAdh.UrutanBilling=1;
							$scope.mGlobalParameterAdh.AfiAuto=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==3)
						{
							$scope.mGlobalParameterAdh.UrutanAfi=1;
							$scope.mGlobalParameterAdh.UrutanBilling=2;
							$scope.mGlobalParameterAdh.AfiAuto=false;
							//kalo afi 1 billing harus 2 sama auto false
						}
						else
						{
							$scope.mGlobalParameterAdh.UrutanAfi=2;
							$scope.mGlobalParameterAdh.UrutanBilling=1;
							$scope.mGlobalParameterAdh.AfiAuto=true;
						}
					}
					
					if($scope.mGlobalParameterAdhGet[i].ParamKey=="AH_CTKFKB")
					{
						if($scope.mGlobalParameterAdhGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdh.ShowBbn=false;
							$scope.mGlobalParameterAdh.ShowDiskon=false;
							$scope.mGlobalParameterAdh.ShowPph22=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdh.ShowBbn=true;
							$scope.mGlobalParameterAdh.ShowDiskon=false;
							$scope.mGlobalParameterAdh.ShowPph22=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==2)
						{
							$scope.mGlobalParameterAdh.ShowBbn=false;
							$scope.mGlobalParameterAdh.ShowDiskon=true;
							$scope.mGlobalParameterAdh.ShowPph22=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==3)
						{
							$scope.mGlobalParameterAdh.ShowBbn=true;
							$scope.mGlobalParameterAdh.ShowDiskon=true;
							$scope.mGlobalParameterAdh.ShowPph22=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==4)
						{
							$scope.mGlobalParameterAdh.ShowBbn=false;
							$scope.mGlobalParameterAdh.ShowDiskon=false;
							$scope.mGlobalParameterAdh.ShowPph22=true;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==5)
						{
							$scope.mGlobalParameterAdh.ShowBbn=true;
							$scope.mGlobalParameterAdh.ShowDiskon=false;
							$scope.mGlobalParameterAdh.ShowPph22=true;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==6)
						{
							$scope.mGlobalParameterAdh.ShowBbn=false;
							$scope.mGlobalParameterAdh.ShowDiskon=true;
							$scope.mGlobalParameterAdh.ShowPph22=true;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==7)
						{
							$scope.mGlobalParameterAdh.ShowBbn=true;
							$scope.mGlobalParameterAdh.ShowDiskon=true;
							$scope.mGlobalParameterAdh.ShowPph22=true;
						}

						//Show Karoseri
						if($scope.mGlobalParameterAdhGet[i].NumVal2==1)
						{
							$scope.mGlobalParameterAdh.ShowKaroseri=true;
						}
						else
						{
							$scope.mGlobalParameterAdh.ShowKaroseri=false;
						}
						
					}
					
					if($scope.mGlobalParameterAdhGet[i].ParamKey=="SPK_GSN")
					{
						if($scope.mGlobalParameterAdhGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdh.GenerateNumberSpkBySystem=false;
						}
						else if($scope.mGlobalParameterAdhGet[i].NumVal1==1)
						{							
							$scope.mGlobalParameterAdh.GenerateNumberSpkBySystem=true;
						}
						
					}
				}
				
				
				return $scope.mGlobalParameterAdh;
			});
		}
		else if($scope.user.RoleName == 'Admin TAM')
		{
			GlobalParameterFactory.getDataAdminTam().then(function(res){
				$scope.mGlobalParameterAdminTamGet =angular.copy(res.data.Result);
				$scope.mGlobalParameterAdminTam={}
				
				for(var i = 0; i < $scope.mGlobalParameterAdminTamGet.length; ++i)
				{
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="AreaViolation")
					{
						if($scope.mGlobalParameterAdminTamGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminTam.AreaViolation=false;
						}
						else if($scope.mGlobalParameterAdminTamGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminTam.AreaViolation=true;
						}
						else
						{
							$scope.mGlobalParameterAdminTam.AreaViolation=false;
						}
					}
					
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="SPK_ESIGN")
					{
						if($scope.mGlobalParameterAdminTamGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminTam.TandaTanganSpk=false;
						}
						else if($scope.mGlobalParameterAdminTamGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminTam.TandaTanganSpk=true;
						}
						else
						{
							$scope.mGlobalParameterAdminTam.TandaTanganSpk=false;
						}
					}
					
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="SPK_DIOACC")
					{
						if($scope.mGlobalParameterAdminTamGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminTam.PaketDioAksesoris=false;
						}
						else if($scope.mGlobalParameterAdminTamGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminTam.PaketDioAksesoris=true;
						}
						else
						{
							$scope.mGlobalParameterAdminTam.PaketDioAksesoris=false;
						}
					}
					
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="DS_RSVSTOCK")
					{
						if($scope.mGlobalParameterAdminTamGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminTam.ReversedStock=false;
						}
						else if($scope.mGlobalParameterAdminTamGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminTam.ReversedStock=true;
						}
						else
						{
							$scope.mGlobalParameterAdminTam.ReversedStock=false;
						}
					}
					
				}
				
				return $scope.mGlobalParameterAdminTam;
			});
		}
		else if($scope.user.RoleName == 'Admin HO')
		{
			GlobalParameterFactory.getDataAdminHO().then(function(res){
				$scope.mGlobalParameterAdminHoGet = angular.copy(res.data.Result);
				$scope.mGlobalParameterAdminHo={};
				
				for(var i = 0; i < $scope.mGlobalParameterAdminHoGet.length; ++i)
				{
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="SPK_INCPKB")
					{
						if($scope.mGlobalParameterAdminHoGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminHo.IncludePkb=false;
						}
						else if($scope.mGlobalParameterAdminHoGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminHo.IncludePkb=true;
						}
						else
						{
							$scope.mGlobalParameterAdminHo.IncludePkb=false;
						}

					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="SPK_MED")
					{
						if($scope.mGlobalParameterAdminHoGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminHo.Mediator=false;
						}
						else if($scope.mGlobalParameterAdminHoGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminHo.Mediator=true;
						}
						else
						{
							$scope.mGlobalParameterAdminHo.Mediator=false;
						}

					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="PDD_QRPDD")
					{
						$scope.mGlobalParameterAdminHo.KuotaRevisiPddKali=$scope.mGlobalParameterAdminHoGet[i].NumVal2;
						if($scope.mGlobalParameterAdminHoGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminHo.KuotaRevisiPddAktif=false;
						}
						else if($scope.mGlobalParameterAdminHoGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminHo.KuotaRevisiPddAktif=true;
						}
						else
						{
							$scope.mGlobalParameterAdminHo.KuotaRevisiPddAktif=false;
						}

					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="DR_QUM")
					{
						$scope.mGlobalParameterAdminHo.KuotaUrgentMemoKali=$scope.mGlobalParameterAdminHoGet[i].NumVal2;
						
						if($scope.mGlobalParameterAdminHoGet[i].NumVal1==0)
						{
							$scope.mGlobalParameterAdminHo.KuotaUrgentMemoAktif=false;
						}
						else if($scope.mGlobalParameterAdminHoGet[i].NumVal1==1)
						{
							$scope.mGlobalParameterAdminHo.KuotaUrgentMemoAktif=true;
						}
						else
						{
							$scope.mGlobalParameterAdminHo.KuotaUrgentMemoAktif=false;
						}

					}
				}
				
				return $scope.mGlobalParameterAdminHo;
			});
		}
	}
	
	GlobalParameterDaGitAll();
	
	$scope.GlobalParameterJagaSwitch = function () {
		if($scope.mGlobalParameterAdh.AfiAuto==true)
		{
			$scope.mGlobalParameterAdh.UrutanAfi=2;
			$scope.mGlobalParameterAdh.UrutanBilling=1;
		}
		else if($scope.mGlobalParameterAdh.AfiAuto==false)
		{
			if($scope.mGlobalParameterAdh.UrutanAfi==1)
			{
				$scope.mGlobalParameterAdh.UrutanBilling=2;
			}
			else if($scope.mGlobalParameterAdh.UrutanAfi==2)
			{
				$scope.mGlobalParameterAdh.UrutanBilling=1;
			}
		}
	}
	
	$scope.GlobalParameterJagaSwitch2 = function () {
		if($scope.mGlobalParameterAdh.AfiAuto==true)
		{
			$scope.mGlobalParameterAdh.UrutanAfi=2;
			$scope.mGlobalParameterAdh.UrutanBilling=1;
		}
		else if($scope.mGlobalParameterAdh.AfiAuto==false)
		{
			if($scope.mGlobalParameterAdh.UrutanBilling==1)
			{
				$scope.mGlobalParameterAdh.UrutanAfi=2;
			}
			else if($scope.mGlobalParameterAdh.UrutanBilling==2)
			{
				$scope.mGlobalParameterAdh.UrutanAfi=1;
			}
		}
	}
	
	$scope.SimpanGlobalParameter = function () {
		$scope.disabledSimpanGlobalParameter = true;
		if($scope.user.RoleName == 'ADH')
		{
			for(var i = 0; i < $scope.mGlobalParameterAdhGet.length; ++i)
			{
				if($scope.mGlobalParameterAdhGet[i].ParamKey=="AH_BLAFI")
				{
					if($scope.mGlobalParameterAdh.UrutanAfi==2 && $scope.mGlobalParameterAdh.UrutanBilling==1 && $scope.mGlobalParameterAdh.AfiAuto==true)
					{
						$scope.mGlobalParameterAdhGet[i].NumVal1=1;
					}
					else if($scope.mGlobalParameterAdh.UrutanAfi==2 && $scope.mGlobalParameterAdh.UrutanBilling==1 && $scope.mGlobalParameterAdh.AfiAuto==false)
					{
						$scope.mGlobalParameterAdhGet[i].NumVal1=2;
					}
					else if($scope.mGlobalParameterAdh.UrutanAfi==1 && $scope.mGlobalParameterAdh.UrutanBilling==2 && $scope.mGlobalParameterAdh.AfiAuto==false)
					{
						$scope.mGlobalParameterAdhGet[i].NumVal1=3;
					}
				}
				if($scope.mGlobalParameterAdhGet[i].ParamKey=="AH_CTKFKB")
				{
					var Da_3_Centang=0;
					var isShowKaroseri=0;
					if($scope.mGlobalParameterAdh.ShowBbn==false)
					{
						Da_3_Centang+=0;
					}
					else if($scope.mGlobalParameterAdh.ShowBbn==true)
					{
						Da_3_Centang+=1;
					}
					
					if($scope.mGlobalParameterAdh.ShowDiskon==false)
					{
						Da_3_Centang+=0;
					}
					else if($scope.mGlobalParameterAdh.ShowDiskon==true)
					{
						Da_3_Centang+=2;
					}
					
					if($scope.mGlobalParameterAdh.ShowPph22==false)
					{
						Da_3_Centang+=0;
					}
					else if($scope.mGlobalParameterAdh.ShowPph22==true)
					{
						Da_3_Centang+=4;
					}
					
					if($scope.mGlobalParameterAdh.ShowKaroseri==false)
					{
						isShowKaroseri=0;
					}
					else if($scope.mGlobalParameterAdh.ShowKaroseri==true)
					{
						isShowKaroseri=1;
					}

					$scope.mGlobalParameterAdhGet[i].NumVal1=Da_3_Centang;
					$scope.mGlobalParameterAdhGet[i].NumVal2=isShowKaroseri;
				}
				
				if($scope.mGlobalParameterAdhGet[i].ParamKey=="SPK_GSN")
				{
					if($scope.mGlobalParameterAdh.GenerateNumberSpkBySystem==false)
					{
						$scope.mGlobalParameterAdhGet[i].NumVal1=0;
					}
					else if($scope.mGlobalParameterAdh.GenerateNumberSpkBySystem==true)
					{							
						$scope.mGlobalParameterAdhGet[i].NumVal1=1;
					}
				}
			}	

			GlobalParameterFactory.UbahDataGlobalParameterEmangSamaSemua($scope.mGlobalParameterAdhGet).then(function(res){
				GlobalParameterDaGitAll();
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
			},
            function(err){
                bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
            });
		}
		else if($scope.user.RoleName == 'Admin TAM')
		{
			
			for(var i = 0; i < $scope.mGlobalParameterAdminTamGet.length; ++i)
			{
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="AreaViolation")
					{
						if($scope.mGlobalParameterAdminTam.AreaViolation==true)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=1;
						}
						else if($scope.mGlobalParameterAdminTam.AreaViolation==false)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=0;
						}
						
					}
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="SPK_ESIGN")
					{
						if($scope.mGlobalParameterAdminTam.TandaTanganSpk==true)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=1;
						}
						else if($scope.mGlobalParameterAdminTam.TandaTanganSpk==false)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=0;
						}
					}
					
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="SPK_DIOACC")
					{
						if($scope.mGlobalParameterAdminTam.PaketDioAksesoris==true)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=1;
						}
						else if($scope.mGlobalParameterAdminTam.PaketDioAksesoris==false)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=0;
						}
					}
					
					if($scope.mGlobalParameterAdminTamGet[i].ParamKey=="DS_RSVSTOCK")
					{
						if($scope.mGlobalParameterAdminTam.ReversedStock==true)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=1;
						}
						else if($scope.mGlobalParameterAdminTam.ReversedStock==false)
						{
							$scope.mGlobalParameterAdminTamGet[i].NumVal1=0;
						}
					}
					
			}

			GlobalParameterFactory.UbahDataGlobalParameterEmangSamaSemua($scope.mGlobalParameterAdminTamGet).then(function(res){
				GlobalParameterDaGitAll();
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
			},
            function(err){
                bsNotify.show(
					{
						title: "Gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            });
		}
		else if($scope.user.RoleName == 'Admin HO')
		{
			for(var i = 0; i < $scope.mGlobalParameterAdminHoGet.length; ++i)
				{
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="SPK_INCPKB")
					{
						if($scope.mGlobalParameterAdminHo.IncludePkb==false)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=0;
						}
						else if($scope.mGlobalParameterAdminHo.IncludePkb==true)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=1;
						}
					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="SPK_MED")
					{
						if($scope.mGlobalParameterAdminHo.Mediator==false)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=0;
						}
						else if($scope.mGlobalParameterAdminHo.Mediator==true)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=1;
						}
					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="PDD_QRPDD")
					{
						$scope.mGlobalParameterAdminHoGet[i].NumVal2=$scope.mGlobalParameterAdminHo.KuotaRevisiPddKali;
						if($scope.mGlobalParameterAdminHo.KuotaRevisiPddAktif==false)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=0;
						}
						else if($scope.mGlobalParameterAdminHo.KuotaRevisiPddAktif==true)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=1;
						}
					}
					if($scope.mGlobalParameterAdminHoGet[i].ParamKey=="DR_QUM")
					{
						$scope.mGlobalParameterAdminHoGet[i].NumVal2=$scope.mGlobalParameterAdminHo.KuotaUrgentMemoKali;
						
						if($scope.mGlobalParameterAdminHo.KuotaUrgentMemoAktif==false)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=0;
						}
						else if($scope.mGlobalParameterAdminHo.KuotaUrgentMemoAktif==true)
						{
							$scope.mGlobalParameterAdminHoGet[i].NumVal1=1;
						}
					}
				}
			GlobalParameterFactory.UbahDataGlobalParameterEmangSamaSemua($scope.mGlobalParameterAdminHoGet).then(function(res){
				GlobalParameterDaGitAll();
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				$scope.disabledSimpanGlobalParameter = false;
			},
            function(err){
                bsNotify.show(
					{
						title: "Gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
				$scope.disabledSimpanGlobalParameter = false;
            });
		}
	}
});