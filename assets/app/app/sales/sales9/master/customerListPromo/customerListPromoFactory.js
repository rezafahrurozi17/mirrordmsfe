angular.module('app')
  .factory('CustomerListPromoFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryPromo');
        // console.log("data dari factory",res);
        return res;
      },

      create: function(customerListPromo) {
        return $http.post('/api/sales/PCategoryPromo', [{
            PromoName : customerListPromo.PromoName,
						StartValidDate : customerListPromo.StartValidDate,
						EndValidDate : customerListPromo.EndValidDate,
						EnableBit : customerListPromo.EnableBit,
        }]);
      },

      update: function(customerListPromo){
        return $http.put('/api/sales/PCategoryPromo', [{
            PromoId : customerListPromo.PromoId,
            PromoName : customerListPromo.PromoName,
						StartValidDate : customerListPromo.StartValidDate,
						EndValidDate : customerListPromo.EndValidDate,
						EnableBit : customerListPromo.EnableBit
        }]);
      },
      
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryPromo',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 

