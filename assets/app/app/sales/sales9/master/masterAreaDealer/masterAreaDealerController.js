angular.module('app')
    .controller('MasterAreaDealerController', function($scope, $http, CurrentUser, MasterAreaDealerFactoryMaster,$timeout,bsNotify,$httpParamSerializer) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
	console.log("setan",$scope.user);
    $scope.mMasterAreaDealer = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={GroupDealerId:null,AreaDealerId:null};


    //----------------------------------
    // Get Data test
    //----------------------------------
	
	
	MasterAreaDealerFactoryMaster.getDealerNameDropdown().then(function (res) {
			$scope.DaMasterAreaDealerDealerName = res.data.Result[0].GroupDealerName;
			//return $scope.optionsMasterAreaDealerCAbang;
			MasterAreaDealerFactoryMaster.getOutletDropdown("GroupDealerId="+res.data.Result[0].GroupDealerId).then(function (res) {
				$scope.filter.GroupDealerId=res.data.Result[0].GroupDealerId;
				$scope.optionsMasterAreaDealerCAbang = res.data.Result;
				//return $scope.optionsMasterAreaDealerCAbang;
			});
		});
	
	
		
	

	
	
     $scope.getData = function() {

		MasterAreaDealerFactoryMaster.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }
	
	$scope.RemoveListDetailOutlet = function (index) {
		$scope.mMasterAreaDealer.ListDetailOutlet.splice(index, 1);

    }
	
	$scope.MasterAreaDealerAddDaCabang = function (Selected) {
		console.log("setan",Selected);
		$scope.The_MasterAreaDealerCabang=Selected;

    }
	
	$scope.MasterAreaDealerAddNamaCabang = function () {
		var MasterAreaDealerAddNamaCabangAmanGaDuplikat=true;
		
		try
		{
			if($scope.mMasterAreaDealer.ListDetailOutlet.length>=1)
			{
				for(var i = 0; i < $scope.mMasterAreaDealer.ListDetailOutlet.length; ++i)
				{	
					if($scope.mMasterAreaDealer.ListDetailOutlet[i].OutletId ==$scope.The_MasterAreaDealerCabang.OutletId)
					{
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Cabang yang anda pilih duplikat",
								type: 'warning'
							}
						);
						MasterAreaDealerAddNamaCabangAmanGaDuplikat=false;
						break;
					}
				}
			}
		}
		catch(ulala)
		{
			MasterAreaDealerAddNamaCabangAmanGaDuplikat=true;
		}
		
		
		
		if((typeof $scope.The_MasterAreaDealerCabang=="undefined"))
		{
			bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda harus memilih paling tidak satu cabang.",
						type: 'warning'
					}
				);
		}
		else if((typeof $scope.The_MasterAreaDealerCabang!="undefined") && MasterAreaDealerAddNamaCabangAmanGaDuplikat==true)
		{
			try
			{
				// for( var i in $scope.mMasterAreaDealer.ListDetailOutlet){
				//     if(){

				//     }
				// }
				$scope.mMasterAreaDealer.ListDetailOutlet.push({ OutletId: $scope.The_MasterAreaDealerCabang.OutletId,OutletName:$scope.The_MasterAreaDealerCabang.OutletName});
			}
			catch(err)
			{
				$scope.mMasterAreaDealer.ListDetailOutlet=[{ OutletId: null,OutletName:null}];
				$scope.mMasterAreaDealer.ListDetailOutlet.splice(0, 1);
				$scope.mMasterAreaDealer.ListDetailOutlet.push({ OutletId: $scope.The_MasterAreaDealerCabang.OutletId,OutletName:$scope.The_MasterAreaDealerCabang.OutletName});
			}
		}
		
		
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			$scope.mMasterAreaDealer.HOOutletId=$scope.user.OutletId;
			MasterAreaDealerFactoryMaster.create($scope.mMasterAreaDealer).then(
				function(res) {
					MasterAreaDealerFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			$scope.mMasterAreaDealer.HOOutletId=$scope.user.OutletId;
			MasterAreaDealerFactoryMaster.update($scope.mMasterAreaDealer).then(
				function(res) {
					MasterAreaDealerFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'AreaDealerId',    field:'AreaDealerId', width:'7%', visible:false},
            { name:'Area Dealer', field:'AreaDealerName' },
			
        ]
    };
	
	
	
});


