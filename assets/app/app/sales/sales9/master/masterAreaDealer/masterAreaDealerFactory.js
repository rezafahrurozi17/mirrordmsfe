angular.module('app')
  .factory('MasterAreaDealerFactoryMaster', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/MSitesAreaDealer?'+param);
        return res;
      },
	  getDealerDropdown: function() {
        var res=$http.get('/api/sales/MsitesGroupDealer');
        return res;
      },
	  getDealerNameDropdown: function() {
        var res=$http.get('/api/sales/GetMSitesGroupDealerByOrg');
        return res;
      },
	  getOutletDropdown: function(Param) {
        var res=$http.get('/api/sales/GetMSitesOutlet?'+Param);
        return res;
      },
      create: function(MasterAreaDealer) {
        return $http.post('/api/sales/MSitesAreaDealer', [{
                                            AreaDealerName: MasterAreaDealer.AreaDealerName,
											HOOutletId: MasterAreaDealer.HOOutletId,
                                            ListDetailOutlet: MasterAreaDealer.ListDetailOutlet}]);
      },
      update: function(MasterAreaDealer){
        return $http.put('/api/sales/MSitesAreaDealer', [{
                                            AreaDealerId: MasterAreaDealer.AreaDealerId,
											AreaDealerName: MasterAreaDealer.AreaDealerName,
											HOOutletId: MasterAreaDealer.HOOutletId,
                                            ListDetailOutlet: MasterAreaDealer.ListDetailOutlet}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MSitesAreaDealer',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd