angular.module('app')
    .factory('MasterCAOFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            getData: function(start, limit, param) {
                if (param == undefined || param == null || param == "") {
                    param = "";
                } else {
                    param = "&" + param;
                }
                var da_start=(limit*(start-1))+1;
               // if(GDid == null && OId == null){
                    var url = '/api/sales/ListCAO/?start=' + da_start + '&limit=' + limit + param ;
                // }else if(GDid != null && OId == null){
                //     var url = '/api/sales/ListCAO/?start=' + da_start + '&limit=' + limit +'&GroupDealerId=' +GDid;
                // }else if (GDid != null && OId != null){
                //     var url = '/api/sales/ListCAO/?start=' + da_start + '&limit=' + limit +'&GroupDealerId=' +GDid+ '&OutletId=' +OId;
                // }else if(GDid == null && OId != null){
                //     var url = '/api/sales/ListCAO/?start=' + da_start + '&limit=' + limit+ '&OutletId=' +OId;
                //}
               // console.log("url MasterStockBasket : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataDealerList: function() {
                var url = '/api/sales/SalesMasterCAO/GetDataDealerList';
                console.log("url Dealer List : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataListCabang: function(Id) {
                var url = '/api/sales/ListCabang/?GroupDealerId=' + Id;
                var res = $http.get(url);
                return res;
            },
			
			getDataListCabang2: function(Id) {
                var url = '/api/sales/ListCabangMasterCAO/?GroupDealerId=' + Id;
                var res = $http.get(url);
                return res;
            },

            getDataListCabang3: function(Id) {
                var url = '/api/sales/GetMSitesOutlet/?GroupDealerId=' + Id;
                var res = $http.get(url);
                return res;
            },

            getDataListCabangEx: function(filter) {
                var url = '/api/sales/ListCabang/?' + filter; //GroupDealerId=' + Id + '&OutletId=' + OutletId;
                var res = $http.get(url);
                return res;
            },
			
			getDataListCabangEx2: function(filter) {
                var url = '/api/sales/ListCabangMasterCAO/?' + filter; //GroupDealerId=' + Id + '&OutletId=' + OutletId;
                var res = $http.get(url);
                return res;
            },

            getCAOdepan: function(Id){
                var url = '/api/sales/ListCabangCAO?start=1&limit=100&OutletId='+Id;
                var res = $http.get(url);
                return res;
            },
            //api/sales/ListCAO/?start=1&limit=1000

            getDataOutletList: function(groupDealerId) {
                var url = '/api/sales/SalesMasterCAO/GetDataOutletList/?start=1&limit=100&GroupDealerId=' + groupDealerId;
                console.log("url Outlet List : ", url);
                var res = $http.get(url);
                return res;
            },



            create: function(inputData) {
                var url = '/api/sales/ListCAO/Insert';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            delete: function(id) {
                var url = '/api/as/AfterSalesFIRAnswers/deletedata/?id=' + id;
                console.log("url delete Data : ", url);
                var res = $http.delete(url);
                return res;
            }
        }
    });