var app = angular.module('app');
app.controller('MasterCAOController', function ($scope, $http, $filter, $httpParamSerializer, CurrentUser, MasterCAOFactory, uiGridConstants, $timeout, bsNotify) {
    var user = CurrentUser.user();
    $scope.user = CurrentUser.user();
    $scope.uiGridPageSize = 10;
    $scope.mMaintainCAO = {};
    $scope.showAdvsearch = true;
    $scope.MainMasterCAO_Show = true;
    $scope.OutletIdCAO = null;
    $scope.status = "";
    var cellEdit = false;

    $scope.mMaintainCAOfilter = {GroupDealerId:null, OutletId:null};

    $scope.MasterCAOOutletListEdit = function () { return cellEdit; };

    $scope.textFilter = '';
    $scope.filterData = {
        defaultFilter: [
            { name: 'Group Dealer', value: 'GroupDealerName' },
            { name: 'CAO/HO', value: 'OutletNameCAO' },
            { name: 'Area', value: 'AreaName' },
            { name: 'Provinsi', value: 'ProvinceName' },
            { name: 'Kota', value: 'CityRegencyName' },
            // { name: 'Start Valid', value: 'DateFrom' },
            // { name: 'End Valid', value: 'DateTo' }
        ],
        // advancedFilter: [
        // 	{ name: 'Model', value: 'VehicleModelName', typeMandatory: 0 },
        // 	{ name: 'Tipe', value: 'VehicleTypeName', typeMandatory: 0 },
        // 	{ name: 'Tahun Produksi', value: 'VehicleModelYear', typeMandatory: 0 },
        // 	{ name: 'Start Valid', value: 'DateFrom', typeMandatory: 0 },
        // 	{ name: 'End Valid', value: 'DateTo', typeMandatory: 0 }
        // ]
    };

    $scope.filterSearch = function () {
        //$scope.ControlLoadMore = 'Search';
        if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '')) {
            bsNotify.show({
                //size: 'big',
                type: 'warning',
                //timeout: 2000,
                title: "Peringatan",
                content: "Harap pilih kategori"
            });
        } else {
            var inputfilter = $scope.textFilter;
            var tempGrid = angular.copy($scope.CAO_UIGrid.data);
            var objct = '{"' + $scope.selectedFilter + '":"' + inputfilter + '"}'
            if (inputfilter == "" || inputfilter == null) {
                $scope.CAO_UIGrid.data = angular.copy($scope.TampungGrid);
            } else {
                tempGrid = angular.copy($scope.TampungGrid);
                $scope.CAO_UIGrid.data = $filter('filter')(tempGrid, JSON.parse(objct));
            }
        }
    }

    $scope.TambahMasterCAO_InitiateDealerList = function () {
        MasterCAOFactory.getDataDealerList()
            .then(
                function (res) {
                    var DealerList = [];
                    angular.forEach(res.data.Result, function (value, key) {
                        DealerList.push({ "name": value.GroupDealerName, "value": value.GroupDealerId });
                    });

                    $scope.optionsTambahMasterCAO_Dealer = DealerList;
                }
            );
    }

    $scope.TambahMasterCAO_InitiateDealerList();

    $scope.TambahMasterCAO_Dealer_Changed = function (Id) {
        if (!angular.isUndefined(Id) || !Id == null) {
            if ($scope.status == "Insert") {
                MasterCAOFactory.getDataListCabang2(Id).then(function (res) {
                    $scope.listCabang = res.data.Result;
                    if ($scope.status == "Insert") {
                        $scope.mMaintainCAO.OutletId = null;
                    }

                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

                }).then(function () {
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
                });
            }
            else {
                MasterCAOFactory.getDataListCabang2(Id).then(function (res) {
                    $scope.listCabang = res.data.Result;
                    $scope.listCabang.push({ 'OutletId': $scope.mMaintainCAO.OutletIdCAO, 'OutletName': $scope.mMaintainCAO.OutletNameCAO });
                    if ($scope.status == "Insert") {
                        $scope.mMaintainCAO.OutletId = null;
                    }

                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

                }).then(function () {
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
                });
            }

        }


    }

    $scope.TambahMasterCAO_Dealer_ChangedAdv = function (Id) {
        // if (!angular.isUndefined(Id) || !Id == null) {
        //     if ($scope.status == "Insert") {
                MasterCAOFactory.getDataListCabang3(Id).then(function (res) {
                    $scope.listCabangadv = res.data.Result;
                    // if ($scope.status == "Insert") {
                    //     $scope.mMaintainCAO.OutletId = null;
                    // }

                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

                }).then(function () {
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
                });
            // }
            // else {
            //     MasterCAOFactory.getDataListCabang3(Id).then(function (res) {
            //         $scope.listCabangadv = res.data.Result;
            //         $scope.listCabang.push({ 'OutletId': $scope.mMaintainCAO.OutletIdCAO, 'OutletName': $scope.mMaintainCAO.OutletNameCAO });
            //         if ($scope.status == "Insert") {
            //             $scope.mMaintainCAO.OutletId = null;
            //         }

            //         //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

            //     }).then(function () {
            //         //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
            //     });
            // }

        //}


    }

    $scope.Semprul_Dealer_Changed = function (Id) {
        if (!angular.isUndefined(Id) || !Id == null) {
            if ($scope.status == "Insert") {
                MasterCAOFactory.getDataListCabang2(Id).then(function (res) {
                    $scope.listCabang = res.data.Result;
                    for (var i = 0; i < $scope.CAOOutletList_UIGrid.data.length; ++i) {
                        $scope.listCabang = $scope.listCabang.filter(function (daoutletCabang) { 	//OrgId
                            return (daoutletCabang.OutletId != $scope.CAOOutletList_UIGrid.data[i].OutletId);
                        })
                    }
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

                }).then(function () {
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
                });
            }
            else {
                MasterCAOFactory.getDataListCabang2(Id).then(function (res) {
                    $scope.listCabang = res.data.Result;
                    for (var i = 0; i < $scope.CAOOutletList_UIGrid.data.length; ++i) {
                        $scope.listCabang = $scope.listCabang.filter(function (daoutletCabang) { 	//OrgId
                            return (daoutletCabang.OutletId != $scope.CAOOutletList_UIGrid.data[i].OutletId);
                        })
                    }
                    $scope.listCabang.push({ 'OutletId': $scope.mMaintainCAO.OutletIdCAO, 'OutletName': $scope.mMaintainCAO.OutletNameCAO });
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);

                }).then(function () {
                    //setTimeout(function() { $scope.mMaintainCAO.OutletId = $scope.tempOutletIdCAO; }, 5000);
                });
            }

        }


    }

    $scope.pilihSebagaiCAO = function (Id) {
        // if (!angular.isUndefined(Id) || !Id == null) {
        // $scope.filter = { GroupDealerId: $scope.mMaintainCAO.GroupDealerId, OutletId: Id };
        // var param = $httpParamSerializer($scope.filter);
        // MasterCAOFactory.getDataListCabangEx(param).then(function(res) {
        // $scope.listCabang = res.data.Result;
        // //setTimeout(function() { $scope.OutletIdCAO = $scope.tempOutletIdCAO; }, 5000);

        // }).then(function() {
        // //setTimeout(function() { $scope.OutletIdCAO = $scope.tempOutletIdCAO; }, 5000);
        // });
        // }
    }

    $scope.MainMasterCAO_Tambah_Clicked = function () {
        $scope.MainMasterCAO_Show = false;
        $scope.TambahMasterCAO_Show = true;
        $scope.TambahmasterCAO_Simpan_Show = true;
        cellEdit = true;
        $scope.status = "Insert";
        $scope.mMaintainCAO = {};
        $scope.listCabang = [];
    }

    $scope.MainMasterCAO_Refresh_Clicked = function () {
        $scope.mMaintainCAOfilter = {GroupDealerId:null, OutletId:null};
        var param = $httpParamSerializer($scope.mMaintainCAOfilter);
        $scope.CAO_UIGrid_Paging(1, $scope.uiGridPageSize);
    }

    $scope.MainMasterCAO_ADV_Clicked = function () {
        console.log("test", $scope.mMaintainCAOfilter);
        var param = $httpParamSerializer($scope.mMaintainCAOfilter);
        $scope.CAO_UIGrid_Paging(1, $scope.uiGridPageSize);
    }

    $scope.TambahMasterCAO_Kembali_Clicked = function () {
        $scope.mMaintainCAOfilter = {};
        $scope.CAO_UIGrid_Paging(1, $scope.uiGridPageSize);
        $scope.MainMasterCAO_Show = true;
        $scope.TambahMasterCAO_Show = false;
        $scope.TambahMasterCAO_DealerId = "";
        $scope.CAOOutletList_UIGrid.data = [];

            if($scope.GridApiCAO.pagination.getPage() > 1){
                $scope.GridApiCAO.pagination.seek(1);
            };

    }

    $scope.TambahMasterCAO_Kembali_Clicked_lihat = function (){
        $scope.MainMasterCAO_Show = true;
        $scope.TambahMasterCAO_Show = false;
    }

    $scope.TambahMasterCAO_Simpan_Clicked = function () {

        $scope.mMaintainCAOfilter = {GroupDealerId:null, OutletId:null};
        var DaThing = angular.toJson($scope.CAOOutletList_UIGrid.data);
        var inputData = [{
            "OutletIdCAO": $scope.mMaintainCAO.OutletId,
            "listDetail": angular.fromJson(DaThing)
        }];
        MasterCAOFactory.create(inputData)
            .then(
                function (res) {
                    //var param = $httpParamSerializer($scope.mMaintainCAOfilter);
                    if($scope.GridApiCAO.pagination.getPage() > 1){
                        $scope.GridApiCAO.pagination.seek(1);
                    };
                    $scope.CAO_UIGrid_Paging(1, $scope.uiGridPageSize);
                    bsNotify.show(
                        {
                            title: "berhasil",
                            content: "Data berhasil disimpan.",
                            type: 'success'
                        }
                    );
                    $scope.TambahMasterCAO_Kembali_Clicked();
                    $scope.TambahMasterCAO_InitiateDealerList();
                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "gagal",
                            content: "err.data.ExceptionMessage.",
                            type: 'danger'
                        }
                    );
                }
            );
    }

    $scope.tambahOutlet = function () {
        $scope.filter = { GroupDealerId: $scope.mMaintainCAO.GroupDealerId, OutletId: $scope.mMaintainCAO.OutletIdCAO };
        console.log("setan", $scope.filter);
        var param = $httpParamSerializer($scope.filter);
        MasterCAOFactory.getDataListCabangEx2(param).then(function (res) {
            $scope.outletCabang = res.data.Result;

            for (var i = 0; i < $scope.CAOOutletList_UIGrid.data.length; ++i) {
                $scope.outletCabang = $scope.outletCabang.filter(function (daoutletCabang) { 	//OrgId
                    return (daoutletCabang.OutletId != $scope.CAOOutletList_UIGrid.data[i].OutletId);
                })
            }

            var numItems = angular.element('.ui.modal.OutletCabang').length;
            setTimeout(function () {
                angular.element('.ui.modal.OutletCabang').modal('refresh');
            }, 0);
            if (numItems > 1) {
                angular.element('.ui.modal.OutletCabang').not(':first').remove();
            }
            angular.element('.ui.modal.OutletCabang').modal('show');
        });

    }

    $scope.LihatMasterCAO = function (row) {
        $scope.mMaintainCAOfilter = {GroupDealerId:null, OutletId:null};
        $scope.MainMasterCAO_Show = false;
        $scope.TambahMasterCAO_Show = true;
        $scope.TambahmasterCAO_Simpan_Show = false;
        cellEdit = false;
        $scope.status = 'lihat';
        $scope.mMaintainCAO = angular.copy(row.entity);
        $scope.CAOOutletList_UIGrid.data = $scope.mMaintainCAO.listDetail;
        $scope.CAOOutletList_UIGrid.columnDefs[6].visible = false;
        $scope.mMaintainCAO.OutletId = row.entity.OutletIdCAO;
        //$scope.Semprul_Dealer_Changed($scope.mMaintainCAO.GroupDealerId);
    }

    $scope.UbahMasterCAO = function (row) {
        if ($scope.user.RoleName == "Admin TAM") {
            $scope.MainMasterCAO_Show = false;
            $scope.TambahMasterCAO_Show = true;
            $scope.TambahmasterCAO_Simpan_Show = true;
            $scope.status = "Update";

            $scope.mMaintainCAO = angular.copy(row.entity);
            $scope.CAOOutletList_UIGrid.data = $scope.mMaintainCAO.listDetail;
            $scope.CAOOutletList_UIGrid.columnDefs[6].visible = true;
            $scope.mMaintainCAO.OutletId = row.entity.OutletIdCAO;
            //$scope.Semprul_Dealer_Changed($scope.mMaintainCAO.GroupDealerId);
        }
        else if ($scope.user.RoleName != "Admin TAM") {
            bsNotify.show(
                {
                    title: "peringatan",
                    content: "Hanya Admin TAM yang dapat mengubah data.",
                    type: 'warning'
                }
            );
        }
    }

    $scope.selectedItems = [];
    $scope.toggleChecked = function (data) {
        if (data.checked) {
            data.checked = false;
            var index = $scope.selectedItems.indexOf(data);
            $scope.selectedItems.splice(index, 1);
        } else {
            data.checked = true;
            $scope.selectedItems.push(data);
        }
    };

    $scope.PilihKategori = function () {
        angular.element('.ui.modal.OutletCabang').modal('hide');
        if ($scope.CAOOutletList_UIGrid.data.length == 0) {
            $scope.CAOOutletList_UIGrid.data = angular.copy($scope.selectedItems);
        } else {
            for (var i in $scope.CAOOutletList_UIGrid.data) {
                for (var j in $scope.selectedItems) {
                    if ($scope.selectedItems[j].OutletId == $scope.CAOOutletList_UIGrid.data[i].OutletId) {
                        $scope.selectedItems.splice(j, 1);
                    }
                }
            }
            for (var i in $scope.selectedItems) {
                $scope.CAOOutletList_UIGrid.data.push($scope.selectedItems[i]);
            }
        }

        for (var i = 0; i < $scope.selectedItems.length; i++) {
            $scope.selectedItems[i].ApprovalFlag = 1;
            delete $scope.selectedItems[i].checked;
        }

        $scope.selectedItems = [];
        $scope.Semprul_Dealer_Changed($scope.mMaintainCAO.GroupDealerId);

    };

    $scope.BatalKategori = function () {
        for (var i = 0; i < $scope.selectedItems.length; i++) {
            $scope.selectedItems[i].ApprovalFlag = 0;
            delete $scope.selectedItems[i].checked;
        }

        $scope.selectedItems = [];
        angular.element('.ui.modal.OutletCabang').modal('hide');
    };

    $scope.CAO_UIGrid = {
        useCustomPagination: true,
        useExternalPagination: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "GroupDealerId", field: "GroupDealerId", visible: false },
            { name: "Dealer", field: "GroupDealerName" },
            { name: "CAO/HO", field: "OutletNameCAO" },
            { name: "Area", field: "AreaName" },
            { name: "Provinsi", field: "ProvinceName" },
            { name: "Kota", field: "CityRegencyName" },
            {
                name: "Action",
                width:'10%',
                cellTemplate: '<p style="padding:5px 0 0 5px"><a uib-tooltip="Lihat" tooltip-placement="bottom" ng-click="grid.appScope.LihatMasterCAO(row)"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> &nbsp&nbsp <a uib-tooltip="Ubah" tooltip-placement="bottom" ng-click="grid.appScope.UbahMasterCAO(row)"><i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a></p>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiCAO = gridApi;

            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.CAO_UIGrid_Paging(pageNumber, pageSize);
            });

           
        }
    };


    $scope.CAO_UIGrid_Paging = function (pageNumber, pageSize) {
        var param = $httpParamSerializer($scope.mMaintainCAOfilter)
        MasterCAOFactory.getData(pageNumber, pageSize, param)
            .then(
                function (res) {
                    $scope.CAO_UIGrid.data = res.data.Result;
                    $scope.TampungGrid = angular.copy(res.data.Result);
                    $scope.CAO_UIGrid.totalItems = res.data.Total;
                    $scope.CAO_UIGrid.paginationPageSize = pageSize;
                    $scope.CAO_UIGrid.paginationPageSizes = [10, 25, 50];
					
					var inputfilter = $scope.textFilter;
					var tempGrid = angular.copy($scope.CAO_UIGrid.data);
					var objct = '{"' + $scope.selectedFilter + '":"' + inputfilter + '"}'
					if (inputfilter == "" || inputfilter == null) {
						$scope.CAO_UIGrid.data = angular.copy($scope.TampungGrid);
					} else {
						tempGrid = angular.copy($scope.TampungGrid);
						$scope.CAO_UIGrid.data = $filter('filter')(tempGrid, JSON.parse(objct));
					}
					
                }

            );
    };

    $scope.CAO_UIGrid_Paging(1, $scope.uiGridPageSize);

    $scope.CAOOutletList_UIGrid = {
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        useCustomPagination: true,
        useExternalPagination: true,
        rowSelection: false,
        multiSelect: false,
        columnDefs: [
            { name: "OutletId", field: "OutletId", visible: false },
            { name: "Kode Outlet", field: "OutletCode",  width:'10%', enableCellEdit: false },
            { name: "Nama Outlet", field: "OutletName", width:'25%',  enableCellEdit: false },
            { name: "Alamat", field: "Address",width:'25%', enableCellEdit: false },
            { name: "Nomor Telpon", field: "PhoneNumber",width:'15%', enableCellEdit: false },
            { name: "Fax", field: "FaxNumber", width:'15%', enableCellEdit: false },
            {
                name: "Action",
                cellTemplate: '<a uib-tooltip="Hapus" tooltip-placement="bottom" ng-click="grid.appScope.Hapus(row.entity)"> ' +
                '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a>',
                width:'10%',
                enableCellEdit: false
            },

        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiCAOOutletList = gridApi;
        }
    };

    $scope.Hapus = function (entity) {
        for (var i in $scope.CAOOutletList_UIGrid.data) {
            if ($scope.CAOOutletList_UIGrid.data[i].OutletId == entity.OutletId) {
                $scope.CAOOutletList_UIGrid.data.splice(i, 1);
            }
        }
        $scope.Semprul_Dealer_Changed($scope.mMaintainCAO.GroupDealerId);

    }

    $scope.CAOOutletList_UIGrid_GetData = function () {
        MasterCAOFactory.getDataOutletList($scope.TambahMasterCAO_DealerId)
            .then(
                function (res) {
                    $scope.CAOOutletList_UIGrid.data = res.data.Result;
                    $scope.CAOOutletList_UIGrid.totalItems = res.data.Total;
                    $scope.CAOOutletList_UIGrid.paginationPageSize = 100;
                    $scope.CAOOutletList_UIGrid.paginationPageSizes = [100];
                }

            );
    }

    $scope.$watch("mMaintainCAOfilter.GroupDealerId", function (newValue, oldValue) {
       if (newValue == null || newValue == undefined || newValue == ''){
        $scope.mMaintainCAOfilter = {};
       }
       
    });
//terima bpkb dari biro jasa, bisa input huruf, strip , garing, max length 20 character. login CAO/HO
    
});