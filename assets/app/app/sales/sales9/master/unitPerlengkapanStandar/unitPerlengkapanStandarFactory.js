angular.module('app')
    .factory('UnitPerlengkapanStandarFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/sales/MUnitEquipmentStandard');
                return res;
            },
            create: function(perlengkapan) {
                return $http.post('/api/sales/MUnitEquipmentStandard', [{
                    EquipmentStandardId: perlengkapan.EquipmentStandardId,
                    EquipmentStandardName: perlengkapan.EquipmentStandardName,
					SerialNumber: perlengkapan.SerialNumber,
                }]);
            },
            update: function(perlengkapan) {
                return $http.put('/api/sales/MUnitEquipmentStandard', [{
                    OutletId: perlengkapan.OutletId,
                    EquipmentStandardId: perlengkapan.EquipmentStandardId,
                    EquipmentStandardName: perlengkapan.EquipmentStandardName,
					SerialNumber: perlengkapan.SerialNumber,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitEquipmentStandard', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd