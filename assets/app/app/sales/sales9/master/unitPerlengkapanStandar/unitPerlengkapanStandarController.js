angular.module('app')
    .controller('UnitPerlengkapanStandarController', function($scope, $http, CurrentUser, UnitPerlengkapanStandarFactory, $timeout,bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.munitPerlengkapanStandar = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            $scope.loading = true;
            UnitPerlengkapanStandarFactory.getData()
                .then(
                    function(res){
                        var temp = res.data.Result;
                        $scope.grid.data = temp;
                        $scope.loading = false;
                        //return $scope.grid.data;
                    }
                    
                );

                },
                function(err){
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }

        

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
                
            }
			
		$scope.doCustomSaveUnitPerlengkapanStandar = function (mdl,mode) {

			console.log('doCustomSave3',mode);//create update
			
			if(mode=="create")
			{
					UnitPerlengkapanStandarFactory.create($scope.munitPerlengkapanStandar).then(
						function(res) {
							UnitPerlengkapanStandarFactory.getData().then(
								function(res){
									gridData = [];
									$scope.grid.data = res.data.Result;
									$scope.loading=false;
									bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
									return res.data;
								},
								function(err){
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
									);
								}
							);


						},            
								function(err){
									
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
									);
								}
					)
				}
				else if(mode=="update")
				{
					UnitPerlengkapanStandarFactory.update($scope.munitPerlengkapanStandar).then(
						function(res) {
							UnitPerlengkapanStandarFactory.getData().then(
								function(res){
									gridData = [];
									$scope.grid.data = res.data.Result;
									$scope.loading=false;
									bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
									return res.data;
								},
								function(err){
									bsNotify.show(
										{
											title: "Gagal",
											content: "Data tidak ditemukan.",
											type: 'danger'
										}
									);
								}
							);


						},            
								function(err){
									
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
									);
								}
					)
				}
				
				
				$scope.formApi.setMode("grid");
		}

		$scope.HapusUnitPerlengkapanStandar = function(){
			var length = $scope.selectedRows.length;
			var setan =[];
			for(var i = 0; i < $scope.selectedRows.length; ++i)
			{
				setan.push($scope.selectedRows[i].EquipmentStandardId);
			}
			UnitPerlengkapanStandarFactory.delete(setan).then(
				function (res) {
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
					$scope.selectedRows = [];
					$scope.getData();
				
			})
		}

      
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'unit id', field: 'PerlengkapanStandarId', width: '7%', visible: false },
                { name: 'nama unit', field: 'EquipmentStandardName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                }
            ]
        };
    });