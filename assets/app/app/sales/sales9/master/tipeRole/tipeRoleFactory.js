angular.module('app')
  .factory('TipeRoleFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MRoleApproval');
        return res;
      },
	  getDaRole: function() {
        var res=$http.get('/api/sales/Lookup/FrameworkRoleTM');
        return res;
      },
	  getDaUser: function() {
        var res=$http.get('/api/sales/Lookup/FrameworkUserTM');
        return res;
      },
      create: function(TipeRole) {
        return $http.post('/api/sales/MRoleApproval', TipeRole);
      },
      update: function(TipeRole){
        return $http.put('/api/sales/MRoleApproval', TipeRole);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MRoleApproval',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd