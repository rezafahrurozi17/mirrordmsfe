angular.module('app')
    .controller('TipeRoleController', function($scope, $http, CurrentUser, TipeRoleFactory,$timeout,bsNotify) {
	$scope.mTipeRole=null;
	$scope.OperationTipeRole="";
	
	$scope.TipeRole=true;
	
	
	TipeRoleFactory.getDaUser().then(function(res) {
            $scope.OptionsTipeRoleNama = res.data.Result;
            return $scope.OptionsTipeRoleNama;
        });
	
	TipeRoleFactory.getDaRole().then(function(res) {
            $scope.OptionsRoleJabatan = res.data.Result;
            return $scope.OptionsRoleJabatan;
        });

			  

	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	
	

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTipeRole = null; //Model
    $scope.xRole={selected:[]};


    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
      $scope.getData = function() {
				$scope.grid.data=TipeRoleFactory.getData();
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
			
		
	$scope.KembaliDariTipeRole = function() {
			$scope.ShowTipeRoleDetail=false;
			$scope.TipeRole=true;
        }	
		
	$scope.LihatTipeRole = function(SelectedData) {
			$scope.OperationTipeRole="Look";
			$scope.ShowTipeRoleDetail=true;
			$scope.TipeRole=false;
			$scope.ShowSimpanTipeRole=false;
			$scope.mTipeRole_DataCompletenessProspectId=SelectedData.DataCompletenessProspectId;
            $scope.mTipeRole_Field=SelectedData.Type;
			$scope.mTipeRole_ApprovalCategoryName=SelectedData.ApprovalCategoryName;
			$scope.TipeRoleChildData=angular.copy(SelectedData.ListSGlobalApprovalDetailConfigView);
			$scope.mTipeRole_Field_EnableDisable=false;

			
        }
		
	$scope.DeleteTipeRoleChild = function(index) {
			$scope.TipeRoleChildData.splice(index, 1);
        }
	
	$scope.TambahTipeRoleChild = function () {

		var da_RoleName="";
		
		for(var i = 0; i < $scope.OptionsRoleJabatan.length; ++i)
		{	
			if($scope.OptionsRoleJabatan[i].Id == $scope.mTipeRole_Child_RoleId)
			{
				da_RoleName=$scope.OptionsRoleJabatan[i].Name;
			}
		}
		
		try
		{
			var TipeRoleAmanGaAdaDuplicate=false;
			for(var i = 0; i < $scope.TipeRoleChildData.length; ++i)
			{	
				if(da_RoleName!=$scope.TipeRoleChildData[i].RoleName)
				{
					
					TipeRoleAmanGaAdaDuplicate=true;
				}
				else if(da_RoleName==$scope.TipeRoleChildData[i].RoleName)
				{
					TipeRoleAmanGaAdaDuplicate=false;
					break;
				}
			}
			if(TipeRoleAmanGaAdaDuplicate==true && $scope.TipeRoleChildData.length<1)
			{
				$scope.TipeRoleChildData.push({ 
											OutletId:$scope.SelectedTipeRole.OutletId,
											DetailConfigId:null,
											ConfigId:$scope.SelectedTipeRole.ConfigId,
											RoleId:$scope.mTipeRole_Child_RoleId,
											
											RoleName:da_RoleName,
											Seq:null,
											ApprovalCategoryCode:$scope.SelectedTipeRole.ApprovalCategoryCode});
			}
			if(TipeRoleAmanGaAdaDuplicate==true && $scope.TipeRoleChildData.length>=1)
			{
				//ini tadinya ga ada sama diatas yg length <1 ga ada
				bsNotify.show(
								{
									title: "Peringatan",
									content: "Anda Hanya dapat meng assign 1 role saja",
									type: 'warning'
								}
							);
			}
			else if(TipeRoleAmanGaAdaDuplicate==false && $scope.TipeRoleChildData.length!=0)
			{
				bsNotify.show(
								{
									title: "Peringatan",
									content: "Sudah ada role dengan nama yang sama.",
									type: 'warning'
								}
							);
			}
			console.log("setan",$scope.TipeRoleChildData);
			if($scope.TipeRoleChildData.length==0)
			{
				$scope.TipeRoleChildData=[{
										OutletId:null,
										DetailConfigId:null,
										ConfigId:null,
										RoleId:null,
										
										RoleName:null,
										Seq:null,
										ApprovalCategoryCode:null}];
			$scope.TipeRoleChildData.splice(0, 1);
			$scope.TipeRoleChildData.push({ 
											OutletId:$scope.SelectedTipeRole.OutletId,
											DetailConfigId:null,
											ConfigId:$scope.SelectedTipeRole.ConfigId,
											RoleId:$scope.mTipeRole_Child_RoleId,
											
											RoleName:da_RoleName,
											Seq:null,
											ApprovalCategoryCode:$scope.SelectedTipeRole.ApprovalCategoryCode});
			}
			
		//UserId: $scope.mTipeRole_Child_UserId,
		//EmployeeName:da_EmployeeName,
		
		}
		catch(err)
		{

			$scope.TipeRoleChildData=[{
										OutletId:null,
										DetailConfigId:null,
										ConfigId:null,
										RoleId:null,
										
										RoleName:null,
										Seq:null,
										ApprovalCategoryCode:null}];
			$scope.TipeRoleChildData.splice(0, 1);
			$scope.TipeRoleChildData.push({ 
											OutletId:$scope.SelectedTipeRole.OutletId,
											DetailConfigId:null,
											ConfigId:$scope.SelectedTipeRole.ConfigId,
											RoleId:$scope.mTipeRole_Child_RoleId,
											
											RoleName:da_RoleName,
											Seq:null,
											ApprovalCategoryCode:$scope.SelectedTipeRole.ApprovalCategoryCode});
		}
    }	

	$scope.UpdateTipeRole = function(SelectedData) {
			$scope.SelectedTipeRole=SelectedData;
			$scope.OperationTipeRole="Update";
			$scope.ShowTipeRoleDetail=true;
			$scope.TipeRole=false;
			$scope.ShowSimpanTipeRole=true;
			$scope.mTipeRole_DataCompletenessProspectId=SelectedData.DataCompletenessProspectId;
            $scope.mTipeRole_Field=SelectedData.Type;
			$scope.mTipeRole_ApprovalCategoryName=SelectedData.ApprovalCategoryName;
			$scope.TipeRoleChildData=angular.copy(SelectedData.ListSGlobalApprovalDetailConfigView);
			$scope.mTipeRole_Field_EnableDisable=true;
        }

	$scope.SimpanTipeRole = function() {
			if($scope.OperationTipeRole=="Update")
			{	
				
				$scope.SelectedTipeRole.ListSGlobalApprovalDetailConfigView=$scope.TipeRoleChildData;
				TipeRoleFactory.update([$scope.SelectedTipeRole]).then(function () {
					TipeRoleFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid3.data = res.data.Result;
							 $scope.loading=false;
						 },
						 function(err){
							 bsNotify.show(
								{
									title: "gagal",
									content: "Data gagal di update.",
									type: 'danger'
								}
							);
						 }
					)
				});
				$scope.KembaliDariTipeRole();
			}
			
        }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------

		
	TipeRoleFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid3.data = res.data.Result; //nanti balikin
			 $scope.loading=false;
         },
         function(err){
             bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)	
	
	$scope.grid3 = {
            enableSorting: true,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
			//data:TipeRoleFactory.getData(),
            columnDefs: [
                { name: 'Id', field: 'DataCompletenessProspectId' , visible: false},
                { name: 'Permohonan Persetujuan', field: 'ApprovalCategoryName' },
                { name: 'Action', field: '', cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.LihatTipeRole(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.UpdateTipeRole(row.entity)" ></i>'},

            ]
        };
		
	
		
	$scope.grid3.onRegisterApi = function(gridApi){
                //set gridApi on scope
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    $scope.selctedRow = gridApi.selection.getSelectedRows();

                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                   $scope.selctedRow = gridApi.selection.getSelectedRows();
                });
        };


	
});
