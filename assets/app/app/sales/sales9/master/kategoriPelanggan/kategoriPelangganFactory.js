angular.module('app')
  .factory('MasterKategoriPelangganFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerCategory?start=1&limit=10000&FilterData=ParentId|2');
        return res;
      },
      create: function(KategoriPelanggan) {
        return $http.post('/api/sales/CCustomerCategory', [{
                                            //AppId: 1,
											ParentId: 2,
                                            CustomerTypeDesc: KategoriPelanggan.CustomerTypeDesc}]);
      },
      update: function(KategoriPelanggan){
        return $http.put('/api/sales/CCustomerCategory', [{
                                            CustomerTypeId: KategoriPelanggan.CustomerTypeId,
											ParentId: 2,
                                            CustomerTypeDesc: KategoriPelanggan.CustomerTypeDesc}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerCategory',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd