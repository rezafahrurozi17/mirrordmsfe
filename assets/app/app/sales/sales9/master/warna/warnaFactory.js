angular.module('app')
  .factory('WarnaFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityReasonDropProspect');
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/MActivityReasonDropProspect', [{
                ReasonDropProspectName: Warna.ReasonDropProspectName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/MActivityReasonDropProspect', [{
                OutletId: Warna.OutletId,
                ReasonDropProspectId: Warna.ReasonDropProspectId,
                ReasonDropProspectName: Warna.ReasonDropProspectName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityReasonDropProspect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });