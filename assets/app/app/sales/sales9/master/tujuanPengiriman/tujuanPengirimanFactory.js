angular.module('app')
  .factory('TujuanPengirimanFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTujuanPengiriman');
        return res;
      },
      create: function(TujuanPengiriman) {
        return $http.post('/api/sales/PCategoryTujuanPengiriman', [{
                                            //AppId: 1,
											TujuanPengirimanName: TujuanPengiriman.TujuanPengirimanName,
                                            KeteranganTujuanPengiriman: TujuanPengiriman.KeteranganTujuanPengiriman}]);
      },
      update: function(TujuanPengiriman){
        return $http.put('/api/sales/PCategoryTujuanPengiriman', [{
                                            TujuanPengirimanId: TujuanPengiriman.TujuanPengirimanId,
											TujuanPengirimanName: TujuanPengiriman.TujuanPengirimanName,
                                            KeteranganTujuanPengiriman: TujuanPengiriman.KeteranganTujuanPengiriman}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTujuanPengiriman',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd