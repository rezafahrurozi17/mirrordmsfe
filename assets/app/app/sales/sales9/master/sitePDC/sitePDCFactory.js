angular.module('app')
    .factory('SitePDCFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/fw/Role');
                //console.log('res=>',res);
                return res;
            },
            create: function(pdc) {
                return $http.post('/api/fw/Role', [{
                    NamaSitePDC: pdc.NamaSitePDC,
                }]);
            },
            update: function(pdc) {
                return $http.put('/api/fw/Role', [{
                    SitePDCId: pdc.SitePDCId,
                    NamaSitePDC: pdc.NamaSitePDC,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd