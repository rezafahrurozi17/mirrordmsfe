angular.module('app')
  .factory('ProfileCustomerFleet', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var res=[
        {CustomerFleetId: "1",  CustomerCode: "name 1",  CustomerName: "d1"},
        {CustomerFleetId: "2",  CustomerCode: "name 2",  CustomerName: "d2"},
        {CustomerFleetId: "3",  CustomerCode: "name 3",  CustomerName: "d3"},
      ];
        return res;
      },
      create: function(ProfileCustomerFleet) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            CustomerCode: ProfileCustomerFleet.CustomerCode,
                                            CustomerName: ProfileCustomerFleet.CustomerName}]);
      },
      update: function(ProfileCustomerFleet){
        return $http.put('/api/fw/Role', [{
                                            CustomerFleetId: ProfileCustomerFleet.CustomerFleetId,
                                            CustomerCode: ProfileCustomerFleet.CustomerCode,
                                            CustomerName: ProfileCustomerFleet.CustomerName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd