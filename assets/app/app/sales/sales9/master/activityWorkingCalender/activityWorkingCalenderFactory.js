angular.module('app')
  .factory('ActivityWorkingCalender', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityWorkingCalender');
        return res;
      },
      
      create: function(ActivityWorkingCalender) {
        return $http.post('/api/sales/MActivityWorkingCalender', [{
              WorkingCalenderName: ActivityWorkingCalender.WorkingCalenderName,
              WorkingCalenderDate: ActivityWorkingCalender.WorkingCalenderDate,
              WorkingCalenderDesc: ActivityWorkingCalender.WorkingCalenderDesc}]);
      },

      update: function(ActivityWorkingCalender){
        return $http.put('/api/sales/MActivityWorkingCalender', [{
              OutletId: ActivityWorkingCalender.OutletId,
              WorkingCalenderId: ActivityWorkingCalender.WorkingCalenderId,
              WorkingCalenderName: ActivityWorkingCalender.WorkingCalenderName,
              WorkingCalenderDate: ActivityWorkingCalender.WorkingCalenderDate,
              WorkingCalenderDesc: ActivityWorkingCalender.WorkingCalenderDesc}]);
      },

      delete: function(id) {
        return $http.delete('/api/sales/MActivityWorkingCalender',{data:id,headers: {'Content-Type': 'application/json'}});
      },


    }
});