angular.module('app')
  .factory('AccountingCOAFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FAccountingCOA');
        //console.log('res=>',res);
        var da_json=[
				{GLAccountId: "1",  GLAccountCode: "GL0001", GLAccountName: "GL Name 01"},
				{GLAccountId: "2",  GLAccountCode: "GL0002", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL0003", GLAccountName: "GL Name 03"},
				{GLAccountId: "4",  GLAccountCode: "GL0004", GLAccountName: "GL Name 04"},
				{GLAccountId: "5",  GLAccountCode: "GL0005", GLAccountName: "GL Name 05"},
				{GLAccountId: "6",  GLAccountCode: "GL0006", GLAccountName: "GL Name 06"},
				{GLAccountId: "7",  GLAccountCode: "GL0007", GLAccountName: "GL Name 07"},
			  ];
        //res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/FAccountingCOA', [{
                                            GLAccountCode:ActivityChecklistItemAdministrasiPembayaran.GLAccountCode,
											GLAccountName:ActivityChecklistItemAdministrasiPembayaran.GLAccountName,
                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/FAccountingCOA', [{
                                            GLAccountId:ActivityChecklistItemAdministrasiPembayaran.GLAccountId,
											OutletId:ActivityChecklistItemAdministrasiPembayaran.OutletId,
											GLAccountCode:ActivityChecklistItemAdministrasiPembayaran.GLAccountCode,
											GLAccountName:ActivityChecklistItemAdministrasiPembayaran.GLAccountName,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FAccountingCOA',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd