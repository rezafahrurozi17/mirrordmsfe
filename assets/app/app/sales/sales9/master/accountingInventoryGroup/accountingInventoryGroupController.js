angular.module('app')
    .controller('AccountingInventoryGroupController', function($scope, $http, CurrentUser, AccountingInventoryGroupFactory,AccountingInventoryTypeFactory,AccountingCOAFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAccountingInventoryGroup = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];

	$scope.getData = function() {
		AccountingInventoryTypeFactory.getData()
        .then(
            function(res){
                gridData = [];
                $scope.optionsInventoryTypeId= res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
		AccountingCOAFactory.getData()
        .then(
            function(res){
                gridData = [];
                $scope.optionsInventoryGLAccountId = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
        AccountingInventoryGroupFactory.getData()
        .then(
            function(res){
                gridData = [];
                $scope.grid.data = res.data.Result;

                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
		
    }
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Inventory Group Id',field:'InventoryGroupId', width:'7%', visible:false },
            { name:'Inventory Type Id', field:'InventoryTypeId' , visible:false},
			{ name:'InventoryTypeName', field:'InventoryTypeName' },
			{ name:'Inventory GL Account Id',  field: 'InventoryGLAccountId' , visible:false},
			{ name:'Inventory GL Account Name',  field: 'InventoryGLAccountName' },
            { name:'Purchase GL Account Id',  field: 'PurchaseGLAccountId' , visible:false},
			{ name:'Purchase GL Account Name',  field: 'PurchaseGLAccountName' },
            { name:'Selling GL Account Id',  field: 'SellingGLAccountId' , visible:false},
			{ name:'Selling GL Account Name',  field: 'SellingGLAccountName' },
            { name:'Adjustment Defect GL Account Id',  field: 'AdjustmentDefectGLAccountId', visible:false },
			{ name:'Adjustment Defect GL Account Name',  field: 'AdjustmentDefectGLAccountName' },
            { name:'Adjustment Lost GL Account Id',  field: 'AdjustmentLostGLAccountId', visible:false },
			{ name:'Adjustment Lost GL Account Name',  field: 'AdjustmentLostGLAccountName' },
            { name:'Adjustment Found GL Account Id',  field: 'AdjustmentFountGLAccountId' , visible:false },
			{ name:'Adjustment Found GL Account Name',  field: 'AdjustmentFoundGLAccountName' }
        ]
    };
});
