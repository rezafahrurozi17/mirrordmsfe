angular.module('app')
  .factory('AccountingInventoryGroupFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FAccountingInventoryGroup');
        //console.log('res=>',res);
        var da_json=[
				{InventoryGroupId: "1",  InventoryTypeId: "1",  InventoryTypeName: "Name 01", InvemtoryGLAccountId: "1", InvemtoryGLAccountName: "GL Name 01", PurchaseGLAccountId: "3", PurchaseGLAccountName: "GL Name 03", SellingGLAccountId: "1", SellingGLAccountName: "GL Name 01", AdjustmentDefectGLAccountId: "1", AdjustmentDefectGLAccountName: "GL Name 01", AdjustmentLostGLAccountId: "1", AdjustmentLostGLAccountName: "GL Name 01", AdjustmentFountGLAccountId: "1", AdjustmentFoundGLAccountName: "GL Name 01"},
				{InventoryGroupId: "2",  InventoryTypeId: "2",  InventoryTypeName: "Name 02", InvemtoryGLAccountId: "2", InvemtoryGLAccountName: "GL Name 02", PurchaseGLAccountId: "2", PurchaseGLAccountName: "GL Name 02", SellingGLAccountId: "2", SellingGLAccountName: "GL Name 02", AdjustmentDefectGLAccountId: "2", AdjustmentDefectGLAccountName: "GL Name 02", AdjustmentLostGLAccountId: "2", AdjustmentLostGLAccountName: "GL Name 02", AdjustmentFountGLAccountId: "2", AdjustmentFoundGLAccountName: "GL Name 02"},
				{InventoryGroupId: "3",  InventoryTypeId: "3",  InventoryTypeName: "Name 03", InvemtoryGLAccountId: "3", InvemtoryGLAccountName: "GL Name 03", PurchaseGLAccountId: "3", PurchaseGLAccountName: "GL Name 03", SellingGLAccountId: "3", SellingGLAccountName: "GL Name 03", AdjustmentDefectGLAccountId: "3", AdjustmentDefectGLAccountName: "GL Name 03", AdjustmentLostGLAccountId: "3", AdjustmentLostGLAccountName: "GL Name 03", AdjustmentFountGLAccountId: "3", AdjustmentFoundGLAccountName: "GL Name 03"},
				
			  ];
        //res=da_json;
        return res;
      },
      create: function(AccountingInventoryGroup) {
        return $http.post('/api/sales/FAccountingInventoryGroup', [{
                                            InventoryTypeId:AccountingInventoryGroup.InventoryTypeId,
											InventoryGLAccountId:AccountingInventoryGroup.InventoryGLAccountId,
											PurchaseGLAccountId:AccountingInventoryGroup.PurchaseGLAccountId,
											SellingGLAccountId:AccountingInventoryGroup.SellingGLAccountId,
											AdjustmentDefectGLAccountId:AccountingInventoryGroup.AdjustmentDefectGLAccountId,
											AdjustmentLostGLAccountId:AccountingInventoryGroup.AdjustmentLostGLAccountId,
											AdjustmentFountGLAccountId:AccountingInventoryGroup.AdjustmentFountGLAccountId,
                                            }]);
      },
      update: function(AccountingInventoryGroup){
        return $http.put('/api/sales/FAccountingInventoryGroup', [{
											InventoryGroupId:AccountingInventoryGroup.InventoryGroupId,
											OutletId:AccountingInventoryGroup.OutletId,
											InventoryTypeId:AccountingInventoryGroup.InventoryTypeId,
                                            InventoryGLAccountId:AccountingInventoryGroup.InventoryGLAccountId,
											PurchaseGLAccountId:AccountingInventoryGroup.PurchaseGLAccountId,
											SellingGLAccountId:AccountingInventoryGroup.SellingGLAccountId,
											AdjustmentDefectGLAccountId:AccountingInventoryGroup.AdjustmentDefectGLAccountId,
											AdjustmentLostGLAccountId:AccountingInventoryGroup.AdjustmentLostGLAccountId,
											AdjustmentFountGLAccountId:AccountingInventoryGroup.AdjustmentFountGLAccountId,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FAccountingInventoryGroup',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd