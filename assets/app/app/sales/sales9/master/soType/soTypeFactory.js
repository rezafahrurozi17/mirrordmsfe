angular.module('app')
  .factory('SoType', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/AdminHandlingSOType');
        //console.log('res=>',res);
// var da_json=[
//         {SOTypeId: "1",  SOTypeName: "name 1"},
//         {SOTypeId: "2",  SOTypeName: "name 2"},
//         {SOTypeId: "3",  SOTypeName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(soType) {
        return $http.post('/api/sales/AdminHandlingSOType', [{
                                            //AppId: 1,
                                            SOTypeName: soType.SOTypeName}]);
      },
      update: function(soType){
        return $http.put('/api/sales/AdminHandlingSOType', [{
                                            OutletId : soType.OutletId,
                                            SOTypeId: soType.SOTypeId,
                                            SOTypeName: soType.SOTypeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingSOType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd