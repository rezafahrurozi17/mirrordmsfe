angular.module('app')
  .factory('ActivityChecklistItemDecFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
         //var filter = $httpParamSerializer(param);
        // var res=$http.get('/api/sales/MActivityChecklistItemDEC?'+filter);
        var res=$http.get('/api/sales/MActivityChecklistItemDEC?start=1&limit=1000');
        return res;
      },

      getDataCombo: function(param) {
        var filter = $httpParamSerializer(param);
        //var filter = $httpParamSerializer(param);
        var res=$http.get('/api/sales/MActivityChecklistItemDEC/?'+filter);
		//var res=$http.get('/api/sales/MUnitAccessoriesPackageHODetail?'+filter);
        return res;
      },

      getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
          
        return res;
      },

      getDataTipe: function(param) {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas/?VehicleModelId='+param);
          
        return res;
      },

      create: function(ActivityChecklistItemDec) {
        return $http.post('/api/sales/MActivityChecklistItemDEC', [{
                // OutletId: ActivityChecklistItemDec.OutletId,
                GroupChecklistDECId: ActivityChecklistItemDec.GroupChecklistDECId,
                ChecklistItemDECName: ActivityChecklistItemDec.ChecklistItemDECName
        }]);
      },
      update: function(ActivityChecklistItemDec){
        return $http.put('/api/sales/MActivityChecklistItemDEC', [{
                OutletId: ActivityChecklistItemDec.OutletId,
                ChecklistItemDECId: ActivityChecklistItemDec.ChecklistItemDECId,
                GroupChecklistDECId: ActivityChecklistItemDec.GroupChecklistDECId,
                ChecklistItemDECName: ActivityChecklistItemDec.ChecklistItemDECName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityChecklistItemDEC',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });