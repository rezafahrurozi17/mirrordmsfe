angular.module('app')
    .controller('ActivityChecklistItemDecController', function($scope, $http, CurrentUser, ActivityChecklistItemDecFactory, NewActivityGroupChecklistDECFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mactivityChecklistItemDec = null; //Model
        $scope.xRole = { selected: [] };
        $scope.filterItemDEC = { VehicleModelId: null, VehicleTypeId: null };

        //----------------------------------
        // Get Data
        //----------------------------------

        NewActivityGroupChecklistDECFactory.getData().then(
				function(res){
					$scope.GroupDECData = res.data.Result;
					$scope.loading=false;
					return res.data;
				}
			);

        $scope.getDEC = function (){
			
            // $scope.GroupDECData.filter(function (type) {
                // return (type.VehicleTypeId == $scope.mactivityChecklistItemDec.VehicleTypeId);
            // })
			
			
			
			$scope.GroupDECData = $scope.GroupDECData.filter(function(daoutletCabang) { //OrgId
                    return (daoutletCabang.VehicleTypeId==$scope.mactivityChecklistItemDec.VehicleTypeId);
                })
			console.log("setan",$scope.GroupDECData);	
			
        	// ActivityChecklistItemDecFactory.getData($scope.mactivityChecklistItemDec).then(
        		// function(res){
        			 // gridData = [];
        			 // $scope.grid.data = res.data.Result; //nanti balikin
        			 // $scope.loading=false;
        		 // },
        		 // function(err){
        			 // bsNotify.show(
        						// {
        							// title: "gagal",
        							// content: "Data tidak ditemukan.",
        							// type: 'danger'
        						// }
        					// );
        		 // }
        	// );
        }


        // ActivityChecklistItemDecFactory.getDataModel().then(function(res) {
        //     $scope.optionsModel = res.data.Result;
        //     return $scope.optionsModel;
        // });



        // ActivityChecklistItemDecFactory.getDataTipe().then(function(res) {
        //     $scope.optionsTipe = res.data.Result;
        //     return $scope.optionsTipe;
        // });

        $scope.SelectedModelSearchDEC = function(DataModel) {
            $scope.optionsTypeSearchBuatAccessoryPackage = $scope.optionsTipe.filter(function(type) { return (type.VehicleModelId == DataModel.VehicleModelId); });
        }

        $scope.doCustomSave = function(mdl, mode) {


            if (mode == "create") {
                ActivityChecklistItemDecFactory.create($scope.mactivityChecklistItemDec).then(
                    function(res) {
                        ActivityChecklistItemDecFactory.getData().then(
                            function(res) {
                                gridData = [];
                                $scope.grid.data = res.data.Result;
                                $scope.formApi.setMode("grid");
                                $scope.loading = false;
                                bsNotify.show({
                                    title: "Sukses",
                                    content: "Data berhasil disimpan",
                                    type: 'success'
                                });
                                return res.data;
                            },
                            function(err) {
                                bsNotify.show({
                                    title: "Gagal",
                                    content: "Data tidak ditemukan.",
                                    type: 'danger'
                                });
                            }
                        );


                    },
                    function(err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                )
            } else if (mode == "update") {
                ActivityChecklistItemDecFactory.update($scope.mactivityChecklistItemDec).then(
                    function(res) {
                        ActivityChecklistItemDecFactory.getData().then(
                            function(res) {
                                gridData = [];
                                $scope.grid.data = res.data.Result;
                                $scope.formApi.setMode("grid");
                                $scope.loading = false;
                                bsNotify.show({
                                    title: "Sukses",
                                    content: "Data berhasil disimpan",
                                    type: 'success'
                                });
                                return res.data;
                            },
                            function(err) {
                                bsNotify.show({
                                    title: "Gagal",
                                    content: "Data tidak ditemukan.",
                                    type: 'danger'
                                });
                            }
                        );


                    },
                    function(err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                )
            }



        }

        var gridData = [];
        $scope.getData = function() {
            ActivityChecklistItemDecFactory.getData().then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    bsNotify.show(
                        {
                            title: "gagal",
                            content: "Data tidak ditemukan.",
                            type: 'danger'
                        }
                    );
                }
            );
        }
        //  $scope.getData = function() {
        //     ActivityChecklistItemDecFactory.getData().then(
        //         function(res){
        //             $scope.grid.data = res.data.Result;
        //             $scope.loading=false;
        //             return res.data;
        //         },
        //         function(err){
        //             bsNotify.show(
        // 				{
        // 					title: "gagal",
        // 					content: "Data tidak ditemukan.",
        // 					type: 'danger'
        // 				}
        // 			);
        //         }
        //     );
        // }

        $scope.filtertipe = function(id) {
            ActivityChecklistItemDecFactory.getDataTipe(id).then(function(res) {
                $scope.optionTipe = res.data.Result;
            })
        }


        // $scope.getData = function() {

        //     if ($scope.filterItemDEC.VehicleModelId != null && (typeof $scope.filterItemDEC.VehicleModelId != "undefined") && $scope.filterItemDEC.VehicleTypeId != null && (typeof $scope.filterItemDEC.VehicleTypeId != "undefined")) {
        //         ActivityChecklistItemDecFactory.getData($scope.filterItemDEC).then(
        //             function(res) {
        //                 gridData = [];
        //                 $scope.grid.data = res.data.Result; //nanti balikin
        //                 $scope.loading = false;
        //             },
        //             function(err) {
        //                 bsNotify.show({
        //                     title: "gagal",
        //                     content: "Data tidak ditemukan.",
        //                     type: 'danger'
        //                 });
        //             }
        //         );
        //     } else if ($scope.filterItemDEC.VehicleModelId == null || (typeof $scope.filterItemDEC.VehicleModelId == "undefined") || $scope.filterItemDEC.VehicleTypeId == null || (typeof $scope.filterItemDEC.VehicleTypeId == "undefined")) {
        //         // bsNotify.show({
        //         //     title: "peringatan",
        //         //     content: "Data mandatory wajib diisi.",
        //         //     type: 'warning'
        //         // });
        //     }
        // };

        // function roleFlattenAndSetLevel(node,lvl){
        //     for(var i=0;i<node.length;i++){
        //         node[i].$$treeLevel = lvl;
        //         gridData.push(node[i]);
        //         if(node[i].child.length>0){
        //             roleFlattenAndSetLevel(node[i].child,lvl+1)
        //         }else{

        //         }
        //     }
        //     return gridData;
        // }
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {}
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'checklist dec id', field: 'ChecklistItemDECId', visible: false },
                { name: 'Group checklist dec id', field: 'GroupChecklistDECId', visible: false },
                { name: 'Nama Group checklist item dec', field: 'GroupChecklistDECName' },
                { name: 'Nama checklist item dec', field: 'ChecklistItemDECName' }
            ]
        };
    });