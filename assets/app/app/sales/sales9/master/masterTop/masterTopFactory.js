angular.module('app')
  .factory('MasterTopFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SAH_MasterTOP');
        return res;
      },
	  
	  getCustomerCategory: function() {
            var res=$http.get('/api/sales/CCustomerCategory');
            return res;
        },
	  
      create: function(MasterTop) {
        return $http.post('/api/sales/SAH_MasterTOP', [{
                                            CustomerTypeId: MasterTop.CustomerTypeId,
                                            TopDayCKD: MasterTop.TopDayCKD,
                                            TopDayCBU: MasterTop.TopDayCBU}]);
      },
      update: function(MasterTop){
        return $http.put('/api/sales/SAH_MasterTOP', [{
                                            TopId: MasterTop.TopId,
                                            CustomerTypeId: MasterTop.CustomerTypeId,
                                            TopDayCKD: MasterTop.TopDayCKD,
                                            TopDayCBU: MasterTop.TopDayCBU}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SAH_MasterTOP',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd