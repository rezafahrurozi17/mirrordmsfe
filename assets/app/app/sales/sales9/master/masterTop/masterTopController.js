angular.module('app')
    .controller('MasterTopController', function($scope, $http, CurrentUser, MasterTopFactoryMaster,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMasterTop = null; //Model
	$scope.xRole={selected:[]};
	console.log('$scope.user',$scope.user);
	
	MasterTopFactoryMaster.getCustomerCategory().then(
            function(res) {
                var tempCategory = res.data.Result;
                $scope.MasterTopGetCustomerCategory = tempCategory.filter(function(type) {
                    return (type.ParentId == 2 || type.CustomerTypeId == 3);
                })
            }
        );

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
        MasterTopFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			MasterTopFactoryMaster.create($scope.mMasterTop).then(
				function(res) {
					MasterTopFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			console.log('$scope.mMasterTop',$scope.mMasterTop);
			MasterTopFactoryMaster.update($scope.mMasterTop).then(
				function(res) {
					MasterTopFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'TopId',    field:'TopId', width:'7%', visible:false},
            { name:'Kategori Pelanggan', field:'CustomerTypeDesc' },
			{ name:'TopCKD', field:'TopDayCKD', displayName:'CKD Cash (hari)' },
			{ name:'TopCBU', field:'TopDayCBU', displayName:'CBU Cash (hari)' }
        ]
    };
});


