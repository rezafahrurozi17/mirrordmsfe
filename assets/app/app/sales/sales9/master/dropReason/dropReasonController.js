angular.module('app')
.controller('DropReasonController', function($scope, $http, CurrentUser, DropReasonFactory,$timeout) {
    
		
	$scope.intended_operation="None";
	$scope.LookSelected = function (DropReasonFactory) {

	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.ChangeSelected = function (SalesProgramId,SalesProgramName) {
	   $scope.intended_operation="Update_Ops";
	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Batal_Button = true;
	   $scope.Submit_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Batal_Clicked = function () {

	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.intended_operation="None";
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Add_Clicked = function () {
			
		   $scope.mProfileSalesProgram_SalesProgramName=null;
		   $scope.intended_operation="Insert_Ops";
		   
		   $scope.ShowParameter = !$scope.ShowParameter;
		   $scope.ShowTable = !$scope.ShowTable;
		   $scope.Submit_Button = true;
		   $scope.Batal_Button = true;
		   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Simpan_Clicked = function () {
		if($scope.intended_operation=="Update_Ops")
		{
			alert("update");
		}
		else if($scope.intended_operation="Insert_Ops")
		{
			alert("insert");
		}
		
	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	   $scope.intended_operation="None";
	};
		
});

