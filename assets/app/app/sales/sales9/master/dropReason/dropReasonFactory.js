angular.module('app')
  .factory('DropReasonFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(SampleUITemplateFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
      },
      update: function(SampleUITemplateFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: SampleUITemplateFactory.SalesProgramId,
                                            SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd