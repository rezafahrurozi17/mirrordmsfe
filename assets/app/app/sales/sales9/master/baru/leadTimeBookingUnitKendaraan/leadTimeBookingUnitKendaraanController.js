angular.module('app')
    .controller('LeadTimeBookingUnitKendaraanController', function($scope, $http, CurrentUser, LeadTimeBookingUnitKendaraanFactory,$timeout,bsNotify) {
	$scope.mLeadTimeBookingUnitKendaraan=null;
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	
	

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mLeadTimeBookingUnitKendaraan = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
	
	LeadTimeBookingUnitKendaraanFactory.getDataModelKendaraan().then(function(res){
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });
	
    var gridData = [];
      

    $scope.getData = function() {
        LeadTimeBookingUnitKendaraanFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid.data = res.data.Result;
			 $scope.loading=false;
         },
         function(err){
            bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)};
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'PDDBufferId', width:'7%', visible:false },
			{ name:'Model',  field: 'VehicleModelName' },
			{ name:'Length Time Of Booking',  field: 'LengthTimeOfBooking' },
			{ name:'Length Time Booking Fee',  field: 'LengthTimeBookingFee' },
			{ name:'Lead Time Stock',  field: 'LeadTimeStock' },
        ]
    };
});
