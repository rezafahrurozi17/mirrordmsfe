angular.module('app')
  .factory('LeadTimeBookingUnitKendaraanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryPDDBuffer');
        return res;
      },
	  getDataModelKendaraan: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');	  
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/PCategoryPDDBuffer', [{
                                            VehicleModelId:ActivityChecklistItemAdministrasiPembayaran.VehicleModelId,
											LengthTimeOfBooking:ActivityChecklistItemAdministrasiPembayaran.LengthTimeOfBooking,
											LengthTimeBookingFee:ActivityChecklistItemAdministrasiPembayaran.LengthTimeBookingFee,
											LeadTimeStock:ActivityChecklistItemAdministrasiPembayaran.LeadTimeStock
                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/PCategoryPDDBuffer', [{
                                            PDDBufferId:ActivityChecklistItemAdministrasiPembayaran.PDDBufferId,
                                            VehicleModelId:ActivityChecklistItemAdministrasiPembayaran.VehicleModelId,
											LengthTimeOfBooking:ActivityChecklistItemAdministrasiPembayaran.LengthTimeOfBooking,
											LengthTimeBookingFee:ActivityChecklistItemAdministrasiPembayaran.LengthTimeBookingFee,
											LeadTimeStock:ActivityChecklistItemAdministrasiPembayaran.LeadTimeStock
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryPDDBuffer',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd