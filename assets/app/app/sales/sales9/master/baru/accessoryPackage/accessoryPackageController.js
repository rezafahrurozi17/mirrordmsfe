angular.module('app')
    .controller('AccessoryPackageController', function($scope, $http, CurrentUser, AccessoryPackageFactory,$timeout,bsNotify) {
	$scope.mAccessoryPackage=null;
	$scope.filterAccessoryPackage={VehicleModelId: null,VehicleTypeId: null};
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	$scope.PaketAksesorisOperation="Not create/update";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAccessoryPackage = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalAccessoryPackageDaAksesoris').remove();
		});
	
	AccessoryPackageFactory.getOutlet().then(function(res){
            $scope.optionsOutlet = res.data.Result;
            return $scope.optionsOutlet;
        });

	    
        AccessoryPackageFactory.getDataModel().then(function(res){
			$scope.mAccessoryPackage.VehicleTypeId ='';
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });

        AccessoryPackageFactory.getDataTipe().then(function(res){
            $scope.optionsTipe = res.data.Result;
            return $scope.optionsTipe;
        });

        $scope.optionsTGA = [{ TGAId: 1, TGAName: "TGA", StatusTGA: true }, { TGAId: 1, TGAName: "Non TGA", StatusTGA: false }];

        $scope.SelectVehicle = {};
        $scope.SelectedModel = function (DataModel) {
			var tipe = null;
			if ($scope.AccessoryPackageDaMode == 'view' || $scope.AccessoryPackageDaMode == 'edit'){
				tipe = angular.copy($scope.mAccessoryPackage.VehicleTypeId)
			}else{
				$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail = [];
				$scope.mAccessoryPackage.Price = null;
			}
			$scope.mAccessoryPackage.VehicleTypeId = null;
			$scope.optionsTipeFiltered = $scope.optionsTipe.filter(function(type) { return (type.VehicleModelId == DataModel.VehicleModelId); });
			console.log("$scope.optionsTipeFiltered-->", $scope.optionsTipeFiltered)
			if($scope.optionsTipeFiltered.length<=0)
			{
				$scope.optionsTipeFiltered = $scope.optionsTipe.filter(function(type) { return (type.VehicleModelId == DataModel); });
				if ($scope.AccessoryPackageDaMode == 'view' || $scope.AccessoryPackageDaMode == 'edit'){
					$scope.mAccessoryPackage.VehicleTypeId = tipe
				}
			}	
			console.log("iniiii-->", $scope.optionsTipeFiltered);		
            //$scope.SelectVehicle.VehicleModelId = DataModel.VehicleModelId; // ini engga ngerti salah dimananya perasaan dah bener wkwk

        }
		
		$scope.SelectedModelSearchAccessoryPackage = function (DataModel) {
            $scope.optionsTypeSearchBuatAccessoryPackage = $scope.optionsTipe.filter(function(type) { return (type.VehicleModelId == DataModel.VehicleModelId); });
			setTimeout(function() {
					if($scope.PaketAksesorisOperation=="Not create/update")
					{
						$scope.filterAccessoryPackage.VehicleTypeId=null;
					}	
					
                }, 100);
			
        }


        $scope.SelectedTipe = function (DataTipe) {
			if ($scope.AccessoryPackageDaMode == 'view' || $scope.AccessoryPackageDaMode == 'edit'){
			}else{
				$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail = [];
				$scope.mAccessoryPackage.Price = null;
			}
            //$scope.SelectVehicle.VehicleTypeId = DataTipe.VehicleTypeId;
			//$scope.mAccessoryPackage.VehicleTypeId= DataTipe.VehicleTypeId;
        }
        //$scope.DisabledAcc = true;
        $scope.SelectedTGA = function(DataTGA) {
            
            // $scope.SelectedDataAccesories = null;
            if(DataTGA.TGAId != null){
				//$scope.DisabledAcc = false;
                AccessoryPackageFactory.getDataAcessories('start=1&Limit=100000&VehicleTypeId=' + $scope.mAccessoryPackage.VehicleTypeId + '&Classification=' + DataTGA.StatusTGA).then(function(res){
					$scope.gridAccessoryPackageDaAksesoris.data= res.data.Result;
                    return $scope.optionsAccessories;
                });

            }else{
				//$scope.DisabledAcc = true;
				
			}
			console.log("disini",$scope.SelectedDataAccesories,DataTGA);
        }
	
    var gridData = [];
      $scope.getData = function() {
		console.log("setan",$scope.filterAccessoryPackage);
		if($scope.filterAccessoryPackage.VehicleModelId!=null && (typeof $scope.filterAccessoryPackage.VehicleModelId!="undefined"))
		{
			AccessoryPackageFactory.getData($scope.filterAccessoryPackage).then(
				function(res){
					 gridData = [];
					 $scope.grid.data = res.data.Result; //nanti balikin
					 $scope.loading=false;
				 },
				 function(err){
					 bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
				 }
			);
		}
		else if($scope.filterAccessoryPackage.VehicleModelId==null || (typeof $scope.filterAccessoryPackage.VehicleModelId=="undefined") || $scope.filterAccessoryPackage.VehicleTypeId==null || (typeof $scope.filterAccessoryPackage.VehicleTypeId=="undefined"))
		{
			bsNotify.show(
						{
							title: "peringatan",
							content: "Data mandatory wajib diisi.",
							type: 'warning'
						}
					);
		}
    };

	$scope.doCustomSave = function (mdl,mode) {
		$scope.PaketAksesorisOperation="create/update";
		$scope.filterAccessoryPackage.VehicleModelId=angular.copy($scope.mAccessoryPackage.VehicleModelId);
		$scope.filterAccessoryPackage.VehicleTypeId=angular.copy($scope.mAccessoryPackage.VehicleTypeId);
		console.log("cek1-->",$scope.filterAccessoryPackage.VehicleModelId)
		console.log("cek2-->",$scope.filterAccessoryPackage.VehicleTypeId)
		console.log("cek3-->",$scope.mAccessoryPackage)

		if(mode=="create")
		{
			AccessoryPackageFactory.create($scope.mAccessoryPackage).then(
				function(res) {	
					if($scope.filterAccessoryPackage.VehicleModelId!=null && (typeof $scope.filterAccessoryPackage.VehicleModelId!="undefined") &&$scope.filterAccessoryPackage.VehicleTypeId!=null && (typeof $scope.filterAccessoryPackage.VehicleTypeId!="undefined"))
					{
						AccessoryPackageFactory.getData($scope.filterAccessoryPackage).then(
							function(res){
								 gridData = [];
								 $scope.grid.data = res.data.Result; //nanti balikin
								 
								 bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								$scope.PaketAksesorisOperation="Not create/update";
								$scope.formApi.setMode("grid");
								 $scope.loading=false;
								 $scope.filterAccessoryPackage.VehicleModelId = {};
							 },
							 function(err){
								 $scope.PaketAksesorisOperation="Not create/update";
								 bsNotify.show(
											{
												title: "gagal",
												content: "Data tidak ditemukan.",
												type: 'danger'
											}
										);
										$scope.filterAccessoryPackage.VehicleModelId = {};
							 }
						);
					}
					else if($scope.filterAccessoryPackage.VehicleModelId==null || (typeof $scope.filterAccessoryPackage.VehicleModelId=="undefined") || $scope.filterAccessoryPackage.VehicleTypeId==null || (typeof $scope.filterAccessoryPackage.VehicleTypeId=="undefined"))
					{
						
						bsNotify.show(
									{
										title: "peringatan",
										content: "Data mandatory wajib diisi.",
										type: 'warning'
									}
								);
					}

				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			AccessoryPackageFactory.update($scope.mAccessoryPackage).then(
				function(res) {			
					if($scope.filterAccessoryPackage.VehicleModelId!=null && (typeof $scope.filterAccessoryPackage.VehicleModelId!="undefined") &&$scope.filterAccessoryPackage.VehicleTypeId!=null && (typeof $scope.filterAccessoryPackage.VehicleTypeId!="undefined"))
					{
						AccessoryPackageFactory.getData($scope.filterAccessoryPackage).then(
							function(res){
								 gridData = [];
								 $scope.grid.data = res.data.Result; //nanti balikin
								 
								 bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								$scope.PaketAksesorisOperation="Not create/update";
								$scope.formApi.setMode("grid");
								 $scope.loading=false;
							 },
							 function(err){
								 $scope.PaketAksesorisOperation="Not create/update";
								 bsNotify.show(
											{
												title: "gagal",
												content: "Data tidak ditemukan.",
												type: 'danger'
											}
										);
							 }
						);
					}
					else if($scope.filterAccessoryPackage.VehicleModelId==null || (typeof $scope.filterAccessoryPackage.VehicleModelId=="undefined") || $scope.filterAccessoryPackage.VehicleTypeId==null || (typeof $scope.filterAccessoryPackage.VehicleTypeId=="undefined"))
					{
						bsNotify.show(
									{
										title: "peringatan",
										content: "Data mandatory wajib diisi.",
										type: 'warning'
									}
								);
					}

				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        
    }
	$scope.PaketAksesorisWarnainTabel = function(setan) {
		var ulala=setan+1;
		if(ulala%2!=0)
		{
			return "PaketAksesorisWarnainYgGanjil";
		}
	}
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.RemoveAccessories = function (index) {
		$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.splice(index, 1);
		var DaAccessoryPackagePrice=0;
		for(var i = 0; i < $scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.length; ++i)
		{
			DaAccessoryPackagePrice+=$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail[i].RetailPrice;
		}
		$scope.mAccessoryPackage.Price=DaAccessoryPackagePrice;
    }
    
	$scope.SelectedDataAccesories = {};
    $scope.mAccessoryPackage = {};
    $scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail = [];
    $scope.SelectMultiAcc = function (DataAcc) {
        $scope.SelectedDataAccesories = DataAcc;
    }
	
	$scope.BatalLookupDaAksesoris = function () {
		angular.element('.ui.modal.ModalAccessoryPackageDaAksesoris').modal('hide');
		$scope.Rows_Selected_DaAccessories=null;
		$scope.gridApiAccessoryPackageDaAksesoris.selection.clearSelectedRows();
		//$scope.gridApiAccessoryPackageDaAksesoris.clearSelectedRows()
	}
	
	$scope.AddAccessories = function () {			
		angular.element('.ui.modal.ModalAccessoryPackageDaAksesoris').modal('setting',{closable:false}).modal('show');
		angular.element('.ui.modal.ModalAccessoryPackageDaAksesoris').not(':first').remove();
		console.log('filter popup', $scope.gridApiAccessoryPackageDaAksesoris.grid.columns, $scope.gridApiAccessoryPackageDaAksesoris.grid.columns[4]);
		for(var i = 0; i < $scope.gridApiAccessoryPackageDaAksesoris.grid.columns.length; i++){
			$scope.gridApiAccessoryPackageDaAksesoris.grid.columns[i].filters[0].term = null;
		}
		$scope.gridAccessoryPackageDaAksesoris.data = [];
		$scope.SelectedDataAccesories = {};
		console.log("tess",$scope.SelectedDataAccesories);
    }
	
	$scope.AddDaAccessoriesToTable = function () {
		angular.element('.ui.modal.ModalAccessoryPackageDaAksesoris').modal('hide');
		
		try
		{//$scope.Rows_Selected_DaAccessories
			for(var i = 0; i < $scope.Rows_Selected_DaAccessories.length; ++i)
			{
				$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.push({AccessoriesId: $scope.Rows_Selected_DaAccessories[i].PartsId,AccessoriesName: $scope.Rows_Selected_DaAccessories[i].PartsName,AccessoriesCode: $scope.Rows_Selected_DaAccessories[i].PartsCode,RetailPrice:$scope.Rows_Selected_DaAccessories[i].RetailPrice});
			}
			
			var DaAccessoryPackagePrice=0;
			for(var i = 0; i < $scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.length; ++i)
			{
				DaAccessoryPackagePrice+=$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail[i].RetailPrice;
			}
			$scope.mAccessoryPackage.Price=DaAccessoryPackagePrice;
		}
		catch(err)
		{

			$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail=[{ 
          AccessoriesId: null,
          AccessoriesName: null,
          AccessoriesCode: null,
		  RetailPrice:null}];
			
			$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.splice(0, 1);
			
			for(var i = 0; i < $scope.Rows_Selected_DaAccessories.length; ++i)
			{
				$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.push({AccessoriesId: $scope.Rows_Selected_DaAccessories[i].PartsId,AccessoriesName: $scope.Rows_Selected_DaAccessories[i].PartsName,AccessoriesCode: $scope.Rows_Selected_DaAccessories[i].PartsCode,RetailPrice:$scope.Rows_Selected_DaAccessories[i].RetailPrice});
			}
			var DaAccessoryPackagePrice=0;
			for(var i = 0; i < $scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail.length; ++i)
			{
				DaAccessoryPackagePrice+=$scope.mAccessoryPackage.ListMUnitAccessoriesPackageHODetail[i].RetailPrice;
			}
			$scope.mAccessoryPackage.Price=DaAccessoryPackagePrice;
        }
        $scope.mAccessoryPackage.ActiveBit = true;
		$scope.Rows_Selected_DaAccessories=null;
		$scope.gridApiAccessoryPackageDaAksesoris.selection.clearSelectedRows();
    }
	
	$scope.SetSelectedAccessories = function (selectedaccessories) {

		$scope.The_Accessories=[{ 
          AccessoriesId: null,
          AccessoriesName: null,
          AccessoriesCode: null}];
		  

		  $scope.The_Accessories['AccessoriesId']=selectedaccessories.AccessoriesId;
		  $scope.The_Accessories['AccessoriesName']=selectedaccessories.AccessoriesName;
		  $scope.The_Accessories['AccessoriesCode']=selectedaccessories.AccessoriesCode;
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------owner tu TAMBit kalo Dealer tu OutletId
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Model',field:'VehicleModelName'},
			{ name:'Tipe',field:'Barang3'},
            { name:'Nama Paket',  field: 'AccessoriesPackageName' },
			{ name:'Status',  field: 'ActiveBit' ,cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-show="row.entity.ActiveBit==true">Aktif</label><label ng-show="row.entity.ActiveBit==false">Non Aktif</label></p>'},
        ]
    };

    $scope.debug = function() {
        console.log('TickBox --> ', $scope.mAccessoryPackage.ActiveBit);
    }
	
	$scope.gridAccessoryPackageDaAksesoris = {
			enableSorting: true,
			enableFiltering: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
			enableColumnMenus: false,
            paginationPageSizes: [10,20,50,100],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Kode Part', field: 'PartsId',enableHiding : false},
                { name: 'Klasifikasi', field: 'Classification' ,enableHiding : false},
                { name: 'Nama Part', field: 'PartsName',enableHiding : false },
                { name: 'Harga Retail', field: 'RetailPrice',enableHiding : false,
                cellFilter: 'currency:"Rp."',
                type: 'number',
                footerCellFilter: 'currency:"Rp."',
                cellClass: 'text-right'
            }
            ]
        };
		
	$scope.gridAccessoryPackageDaAksesoris.onRegisterApi = function(gridApi){
			$scope.gridApiAccessoryPackageDaAksesoris = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope,function(row){
				$scope.Rows_Selected_DaAccessories = gridApi.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
			   $scope.Rows_Selected_DaAccessories = gridApi.selection.getSelectedRows();
			});
		};
		// =======Adding Custom Filter==============
		$scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
		$scope.gridApiAppointment = {};
		$scope.filterColIdx = 1;
		var x = -1;
		for (var i = 0; i < $scope.gridAccessoryPackageDaAksesoris.columnDefs.length; i++) {
			if ($scope.gridAccessoryPackageDaAksesoris.columnDefs[i].visible == undefined && $scope.gridAccessoryPackageDaAksesoris.columnDefs[i].name !== 'Action') {
				x++;
				$scope.gridCols.push($scope.gridAccessoryPackageDaAksesoris.columnDefs[i]);
				$scope.gridCols[x].idx = i;
			}
			console.log("$scope.gridCols", $scope.gridCols);
			console.log("$scope.grid.columnDefs[i]", $scope.gridAccessoryPackageDaAksesoris.columnDefs[i]);
		}
		$scope.filterBtnLabel = $scope.gridCols[0].name;
		$scope.filterBtnChange = function (col) {
			console.log("col", col);
			$scope.filterBtnLabel = col.name;
			$scope.filterColIdx = col.idx + 1;
		};
});
