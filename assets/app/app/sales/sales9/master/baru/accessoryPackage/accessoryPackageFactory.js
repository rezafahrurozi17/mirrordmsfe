angular.module('app')
  .factory('AccessoryPackageFactory', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var filter = $httpParamSerializer(param);
		var res=$http.get('/api/sales/MUnitAccessoriesPackageHODetail?'+filter);
        return res;
      },
	  getOutlet: function() {
        var res=$http.get('/api/sales/MSitesOutlet');
        	  
        return res;
      },
	  getAksesoris: function() {
        var res=$http.get('/api/sales/MUnitAccessories');
        	  
        return res;
      },

      getDataAcessories: function(param) {
        var res = $http.get('/api/sales/ListAccMasterPaketAcc/?'+param);
        return res;
    },

      getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
          
        return res;
      },

      getDataTipe: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
          
        return res;
      },

      create: function(AccessoryPackage) {
        return $http.post('/api/sales/MUnitAccessoriesPackageHODetail', [{
											OutletId:AccessoryPackage.OutletId,
											AccessoriesPackageCode:AccessoryPackage.AccessoriesPackageCode,
											AccessoriesPackageName:AccessoryPackage.AccessoriesPackageName,
											VehicleModelId:AccessoryPackage.VehicleModelId,
											VehicleTypeId:AccessoryPackage.VehicleTypeId,
											StatusCode :1 ,
											ListMUnitAccessoriesPackageHODetail:AccessoryPackage.ListMUnitAccessoriesPackageHODetail,
											ActiveBit:AccessoryPackage.ActiveBit,
											Price:AccessoryPackage.Price,
											LeadTimeAccessoriesInstallation:AccessoryPackage.LeadTimeAccessoriesInstallation,
                                            }]);
      },
      update: function(AccessoryPackage){
        return $http.put('/api/sales/MUnitAccessoriesPackageHODetail', [{
                                            AccessoriesPackageCode:AccessoryPackage.AccessoriesPackageCode,
											OutletId:AccessoryPackage.OutletId,
											AccessoriesPackageName:AccessoryPackage.AccessoriesPackageName,
											VehicleModelId:AccessoryPackage.VehicleModelId,
											VehicleTypeId:AccessoryPackage.VehicleTypeId,
											StatusCode :1 ,
											ListMUnitAccessoriesPackageHODetail:AccessoryPackage.ListMUnitAccessoriesPackageHODetail,
                                            ActiveBit:AccessoryPackage.ActiveBit,
											Price:AccessoryPackage.Price,
											LeadTimeAccessoriesInstallation:AccessoryPackage.LeadTimeAccessoriesInstallation,
											TAMBit:AccessoryPackage.TAMBit, AccessoriesPackageId:AccessoryPackage.AccessoriesPackageId,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MUnitAccessoriesPackageHODetail/Delete',{data:id,headers: {'Content-Type': 'application/json'}});
		
      },
    }
  });
 //ddd