angular.module('app')
  .factory('PddBufferFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryPDDBufferDetail');
        //console.log('res=>',res);
        return res;
      },
	  
	  getAssemblyType: function() {
        var res=$http.get('/api/sales/CVehicleAssemblyType');
        //console.log('res=>',res);
        return res;
      },
            
	getStatusUnit: function() {
        var res=$http.get('/api/sales/PStatusUnit');
        //console.log('res=>',res);
        return res;
      },
			
      create: function(PddBffer) {
        return $http.post('/api/sales/PCategoryPDDBuffer', [{
                                            AssemblyTypeId:PddBffer.AssemblyTypeId,
											VehicleModelId:PddBffer.VehicleModelId,
											StatusUnitId:PddBffer.StatusUnitId,
											BufferTime:PddBffer.BufferTime
                                            }]);
      },
      update: function(PddBffer){
        return $http.put('/api/sales/PCategoryPDDBuffer', [{
											PDDBufferId:PddBffer.PDDBufferId,
                                            AssemblyTypeId:PddBffer.AssemblyTypeId,
											VehicleModelId:PddBffer.VehicleModelId,
											StatusUnitId:PddBffer.StatusUnitId,
											BufferTime:PddBffer.BufferTime
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryPDDBuffer',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd