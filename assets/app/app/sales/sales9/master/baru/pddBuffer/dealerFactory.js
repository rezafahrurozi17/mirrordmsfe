angular.module('app')
  .factory('DealerFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{name: "A",value: 1},
				{name: "B",value: 2},
			  ];
        res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/fw/Role', [{
                                            NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/fw/Role', [{
                                            ChecklistItemAdministrasiPembayaranId:ActivityChecklistItemAdministrasiPembayaran.ChecklistItemAdministrasiPembayaranId,
											NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd