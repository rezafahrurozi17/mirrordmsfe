angular.module('app')
    .controller('PddBufferController', function($scope, $http, CurrentUser, PddBufferFactory,ModelFactory,StockPositionFactory,AssemblyTypeFactory,DealerFactory,$timeout,bsNotify) {
	
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	PddBufferFactory.getAssemblyType().then(function(res){
            $scope.optionsAssemblyType = res.data.Result;
            return $scope.optionsAssemblyType;
        });
	
	ModelFactory.getData().then(function(res){
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });
		
	PddBufferFactory.getStatusUnit().then(function(res){
            $scope.optionsStockPosition = res.data.Result;
            return $scope.optionsStockPosition;
        });	
	
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPddBuffer = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------

	var gridData = [];
      $scope.getData = function() {
        PddBufferFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid.data = res.data.Result; //nanti balikin
			 $scope.loading=false;
         },
         function(err){
            bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)};		
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'PDDBufferId', width:'7%', visible:false },
            { name:'Assembly Type',  field: 'AssemblyTypeName' },
			{ name:'Model',  field: 'VehicleModelName' },
			{ name:'Stock Position',  field: 'StatusUnitName' },
			{ name:'Buffer Time',  field: 'BufferTime' },
        ]
    };
});
