angular.module('app')
  .factory('AssemblyTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{name: "CKD",value: "CKD"},
				{name: "CBU",value: "CBU"},
			  ];
        res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/fw/Role', [{
                                            NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/fw/Role', [{
                                            ChecklistItemAdministrasiPembayaranId:ActivityChecklistItemAdministrasiPembayaran.ChecklistItemAdministrasiPembayaranId,
											NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd