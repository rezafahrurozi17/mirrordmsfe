angular.module('app')
  .factory('CategoryProspectVariableFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryDataCompletenessProspect');
        //console.log('res=>',res);
        var da_json=[
				{Id: 1, Field:"Nama" , Category:"Hot" },
				{Id: 2, Field:"Nama" , Category:"Medium" },
				{Id: 3, Field:"Nama" , Category:"Low" },
			  ];
        //var res=da_json;
        return res;
      },
	  getCategoryProspect: function() {
        var res=$http.get('/api/sales/PCategoryProspect/?start=1&limit=100&filter=null&filterData=!CategoryProspectName|Drop');
        	  
        return res;
      },
      create: function(CategoryProspectVariable) {
        return $http.post('/api/sales/PCategoryDataCompletenessProspect', [{
                                            Type:CategoryProspectVariable.Type,
											CategoryProspectId:CategoryProspectVariable.CategoryProspectId,
                                            }]);
      },
      update: function(CategoryProspectVariable){
        return $http.put('/api/sales/PCategoryDataCompletenessProspect', [{
                                            DataCompletenessProspectId:CategoryProspectVariable.DataCompletenessProspectId,
											Type:CategoryProspectVariable.Type,
											CategoryProspectId:CategoryProspectVariable.CategoryProspectId,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryDataCompletenessProspect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd