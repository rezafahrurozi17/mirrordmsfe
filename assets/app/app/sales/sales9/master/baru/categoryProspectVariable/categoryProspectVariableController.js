angular.module('app')
    .controller('CategoryProspectVariableController', function($scope, $http, CurrentUser, CategoryProspectVariableFactory,TingkatKebutuhanMasterFactory,$timeout,bsNotify) {
	$scope.mCategoryProspectVariable=null;
	var OperationTingkatKebutuhan="";
	var OperationCategoryProspectVariable="";
	
	$scope.CategoryProspectVariable=true;
	$scope.ShowTingkatKebutuhan=true;
	
	CategoryProspectVariableFactory.getCategoryProspect().then(function(res){
            $scope.optionsCategory = res.data.Result;
            return $scope.optionsCategory;
        });
	
	TingkatKebutuhanMasterFactory.getData().then(function(res){
            $scope.optionsTingkatKebutuhan = res.data.Result;
            return $scope.optionsTingkatKebutuhan;
        });
			  

	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	
	

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCategoryProspectVariable = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
      $scope.getData = function() {

				$scope.grid.data=CategoryProspectVariableFactory.getData();
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.TambahTingkatKebutuhan = function() {
			OperationTingkatKebutuhan="Insert";
			$scope.ShowTingkatKebutuhanDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanTingkatKebutuhan=true;
            $scope.mCategoryProspectVariable_TingkatKebutuhan="";
			$scope.mCategoryProspectVariable_TingkatKebutuhan_EnableDisable=true;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_Category_EnableDisable=true;
        }
		
	$scope.TambahCategoryProspectVariable = function() {
			OperationCategoryProspectVariable="Insert";
			$scope.ShowCategoryProspectVariableDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanCategoryProspectVariable=true;
            $scope.mCategoryProspectVariable_Field="";
			$scope.mCategoryProspectVariable_Field_EnableDisable=true;
			$scope.mCategoryProspectVariable_Category_EnableDisable=true;
        }
		
	$scope.DeleteCategoryProspectVariable = function() {
			

			$scope.IdToDelete=[{ DataCompletenessProspectId: null}];
			$scope.IdToDelete.splice(0, 1);
			
			for(var i = 0; i < $scope.selctedRow.length; ++i)
			{
				$scope.IdToDelete.push($scope.selctedRow[i].DataCompletenessProspectId);
					
			}
			
			CategoryProspectVariableFactory.delete($scope.IdToDelete).then(function () {
					CategoryProspectVariableFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid3.data = res.data.Result; //nanti balikin
							 $scope.loading=false;
						 },
						 function(err){
							 console.log("err=>",err);
						 }
					)
				});
        }	
	
	$scope.KembaliDariTingkatKebutuhan = function() {
			$scope.ShowTingkatKebutuhanDetail=false;
			$scope.ShowCategoryProspectVariableDetail=false;
			$scope.ShowTingkatKebutuhan=true;
			$scope.CategoryProspectVariable=true;
            $scope.mCategoryProspectVariable_TingkatKebutuhan="";
			
        }
		
	$scope.KembaliDariCategoryProspectVariable = function() {
			$scope.ShowTingkatKebutuhanDetail=false;
			$scope.ShowCategoryProspectVariableDetail=false;
			$scope.ShowTingkatKebutuhan=true;
			$scope.CategoryProspectVariable=true;
            $scope.mCategoryProspectVariable_TingkatKebutuhan="";
			
        }	
	
	$scope.LihatTingkatKebutuhan = function(SelectedData) {
			OperationTingkatKebutuhan="Look";
			$scope.ShowTingkatKebutuhanDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanTingkatKebutuhan=false;
            $scope.mCategoryProspectVariable_TingkatKebutuhan=SelectedData.PurchaseTimeId;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_Category=SelectedData.CategoryProspectId;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_EnableDisable=false;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_Category_EnableDisable=false;
			
        }
		
	$scope.LihatCategoryProspectVariable = function(SelectedData) {
			OperationCategoryProspectVariable="Look";
			$scope.ShowCategoryProspectVariableDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanCategoryProspectVariable=false;
			$scope.mCategoryProspectVariable_DataCompletenessProspectId=SelectedData.DataCompletenessProspectId;
            $scope.mCategoryProspectVariable_Field=SelectedData.Type;
			$scope.mCategoryProspectVariable_Category=SelectedData.CategoryProspectId;
			$scope.mCategoryProspectVariable_Field_EnableDisable=false;
			$scope.mCategoryProspectVariable_Category_EnableDisable=false;
			
        }	
		
	$scope.UpdateTingkatKebutuhan = function(SelectedData) {

			$scope.SelectedTingkatKebutuhan=SelectedData;
			OperationTingkatKebutuhan="Update";
			$scope.ShowTingkatKebutuhanDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanTingkatKebutuhan=true;

            $scope.mCategoryProspectVariable_TingkatKebutuhan=SelectedData.PurchaseTimeId;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_Category=SelectedData.CategoryProspectId;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_EnableDisable=false;
			$scope.mCategoryProspectVariable_TingkatKebutuhan_Category_EnableDisable=true;
			
        }

	$scope.UpdateCategoryProspectVariable = function(SelectedData) {
			$SelectedCategoryProspectVariable=SelectedData;
			OperationCategoryProspectVariable="Update";
			$scope.ShowCategoryProspectVariableDetail=true;
			$scope.ShowTingkatKebutuhan=false;
			$scope.CategoryProspectVariable=false;
			$scope.ShowSimpanCategoryProspectVariable=true;
			$scope.mCategoryProspectVariable_DataCompletenessProspectId=SelectedData.DataCompletenessProspectId;
            $scope.mCategoryProspectVariable_Field=SelectedData.Type;
			$scope.mCategoryProspectVariable_Category=SelectedData.CategoryProspectId;
			$scope.mCategoryProspectVariable_Field_EnableDisable=true;
			$scope.mCategoryProspectVariable_Category_EnableDisable=true;
			
        }
	
	
		
	$scope.SimpanTingkatKebutuhan = function() {
			if(OperationTingkatKebutuhan=="Insert")
			{

				$scope.TingkatKebutuhanToInsert={ 'Type':$scope.mCategoryProspectVariable_TingkatKebutuhan,'CategoryProspectId':$scope.mCategoryProspectVariable_TingkatKebutuhan_Category };
				TingkatKebutuhanMasterFactory.create($scope.TingkatKebutuhanToInsert).then(function () {
					//nanti di get
					TingkatKebutuhanMasterFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid2.data = res.data.Result; //nanti balikin
							 $scope.loading=false;
						 },
						 function(err){
							 console.log("err=>",err);
						 }
					)
				});
				$scope.TingkatKebutuhanToInsert={};
				$scope.KembaliDariTingkatKebutuhan();
			}
			else if(OperationTingkatKebutuhan=="Update")
			{	

				$scope.TingkatKebutuhanToUpdate={ 'PurchaseTimeId':$scope.mCategoryProspectVariable_TingkatKebutuhan,'CategoryProspectId':$scope.mCategoryProspectVariable_TingkatKebutuhan_Category };
				TingkatKebutuhanMasterFactory.update($scope.TingkatKebutuhanToUpdate).then(function () {
					TingkatKebutuhanMasterFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid2.data = res.data.Result; //nanti balikin
							 $scope.loading=false;
						 },
						 function(err){
							 console.log("err=>",err);
						 }
					)
				});
				$scope.KembaliDariTingkatKebutuhan();
			}
			
        }

	$scope.SimpanCategoryProspectVariable = function() {
			if(OperationCategoryProspectVariable=="Insert")
			{
				
				$scope.CategoryProspectVariableToInsert={ 'Type':$scope.mCategoryProspectVariable_Field,'CategoryProspectId':$scope.mCategoryProspectVariable_Category };
				CategoryProspectVariableFactory.create($scope.CategoryProspectVariableToInsert).then(function () {
					CategoryProspectVariableFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid3.data = res.data.Result;
							 $scope.loading=false;
						 },
						 function(err){
							 console.log("err=>",err);
						 }
					)
				});
				$scope.CategoryProspectVariableToInsert={};
				$scope.KembaliDariCategoryProspectVariable();
			}
			else if(OperationCategoryProspectVariable=="Update")
			{	

				$scope.CategoryProspectVariableToUpdate={ 'DataCompletenessProspectId':$scope.mCategoryProspectVariable_DataCompletenessProspectId,'Type':$scope.mCategoryProspectVariable_Field,'CategoryProspectId':$scope.mCategoryProspectVariable_Category };
				CategoryProspectVariableFactory.update($scope.CategoryProspectVariableToUpdate).then(function () {
					CategoryProspectVariableFactory.getData().then(
						function(res){
							 gridData = [];
							 $scope.grid3.data = res.data.Result;
							 $scope.loading=false;
						 },
						 function(err){
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						 }
					)
				});
				$scope.KembaliDariCategoryProspectVariable();
			}
			
        }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'Id', width:'7%', visible:false },
            { name:'Field',  field: 'Field' },
			{ name:'Category',  field: 'Category' },
			

        ]
    };
	
	TingkatKebutuhanMasterFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid2.data = res.data.Result; //nanti balikin
			 $scope.loading=false;
         },
         function(err){
            bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)
	
	$scope.grid2 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
			//data:TingkatKebutuhanMasterFactory.getData(),
            columnDefs: [
                { name: 'Id', field: 'PurchaseTimeId', width: '25%' , visible: false},
                { name: 'Tingkat Kebutuhan', field: 'PurchaseTimeName', width: '25%' },
                { name: 'Category', field: 'CategoryProspectName', width: '25%' },
                { name: 'Action', field: '', width: '20%' , cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.LihatTingkatKebutuhan(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.UpdateTingkatKebutuhan(row.entity)" ></i>'},

            ]
        };
		
	CategoryProspectVariableFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid3.data = res.data.Result; //nanti balikin
			 $scope.loading=false;
         },
         function(err){
            bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)	
	
	$scope.grid3 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
			//data:CategoryProspectVariableFactory.getData(),
            columnDefs: [
                { name: 'Id', field: 'DataCompletenessProspectId', width: '25%' , visible: false},
                { name: 'Field', field: 'Type', width: '25%' },
                { name: 'Category', field: 'CategoryProspectName', width: '25%' },
                { name: 'Action', field: '', width: '20%' , cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.LihatCategoryProspectVariable(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.UpdateCategoryProspectVariable(row.entity)" ></i>'},

            ]
        };
		
	$scope.grid3.onRegisterApi = function(gridApi){
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    $scope.selctedRow = gridApi.selection.getSelectedRows();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                   $scope.selctedRow = gridApi.selection.getSelectedRows();
                });
        };	
	
});
