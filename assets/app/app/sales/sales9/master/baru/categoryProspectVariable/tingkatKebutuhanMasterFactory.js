angular.module('app')
  .factory('TingkatKebutuhanMasterFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryListPurchaseTime');
        return res;
      },
      create: function(TingkatKebutuhanMaster) {
        return $http.post('/api/sales/PCategoryListPurchaseTime', [{
                                            PurchaseTimeCode:TingkatKebutuhanMaster.PurchaseTimeCode,
											PurchaseTimeName:TingkatKebutuhanMaster.PurchaseTimeName,
											CategoryProspectId:TingkatKebutuhanMaster.CategoryProspectId,
                                            }]);
      },
      update: function(TingkatKebutuhanMaster){
        return $http.put('/api/sales/PCategoryListPurchaseTime', [{
                                            PurchaseTimeId:TingkatKebutuhanMaster.PurchaseTimeId,

											CategoryProspectId:TingkatKebutuhanMaster.CategoryProspectId,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryListPurchaseTime',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd