angular.module('app')
  .factory('LeadTimeProspectFuFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
		//var res=$http.get('/api/sales/PCategoryFollowUpProspectLeadTime/?start=1&limit=100&filterData=!CategoryProspectName|Drop');
        var res=$http.get('/api/sales/PCategoryFollowUpProspectLeadTime');
        //console.log('res=>',res);
        
        return res;
      },
	  getCategoryProspect: function() {
        var res=$http.get('/api/sales/PCategoryProspect');
        	  
        return res;
      },
	  getOutlet: function() {
        var res=$http.get('/api/sales/MSitesOutlet');
        	  
        return res;
      },
      create: function(LeadTimeProspectFu) {
        return $http.post('/api/sales/PCategoryFollowUpProspectLeadTime', [{
                                            OutletId:LeadTimeProspectFu.OutletId,
											CategoryProspectId:LeadTimeProspectFu.CategoryProspectId,
											LeadTime:LeadTimeProspectFu.LeadTime,
											
                                            }]);
      },
      update: function(LeadTimeProspectFu){
        return $http.put('/api/sales/PCategoryFollowUpProspectLeadTime', [{
                                            FollowUpProspectLeadTimeId:LeadTimeProspectFu.FollowUpProspectLeadTimeId,
											OutletId:LeadTimeProspectFu.OutletId,
											CategoryProspectId:LeadTimeProspectFu.CategoryProspectId,
											LeadTime:LeadTimeProspectFu.LeadTime,
											
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryFollowUpProspectLeadTime',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd