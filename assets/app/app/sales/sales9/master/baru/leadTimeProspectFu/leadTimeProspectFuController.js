angular.module('app')
    .controller('LeadTimeProspectFuController', function($scope, $http, CurrentUser, LeadTimeProspectFuFactory,$timeout,bsNotify) {
	$scope.mLeadTimeProspectFu=null;
	var OperationLeadTimeProspectFu="";
	$scope.ShowSimpanLeadTimeProspectFu=false;
	$scope.mLeadTimeProspectFu_Enable=false;
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	$scope.LeadTimeProspectFuTable=true;
	$scope.LeadTimeProspectFuDetail=false;
	
	
	

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mLeadTimeProspectFu = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
	
	LeadTimeProspectFuFactory.getCategoryProspect().then(function(res){
            $scope.optionsCategoryProspect = res.data.Result;
			for(var ee = 0; ee < $scope.optionsCategoryProspect.length; ee++)
			{
				if($scope.optionsCategoryProspect[ee].CategoryProspectName=="Drop")
				{
					$scope.optionsCategoryProspect.splice(ee, 1);
				}
			}
            return $scope.optionsCategoryProspect;
        });

	LeadTimeProspectFuFactory.getOutlet().then(function(res){
            $scope.optionsOutlet = res.data.Result;
            return $scope.optionsOutlet;
        });
	
    var gridData = [];

	$scope.getData = function() {
        LeadTimeProspectFuFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

	$scope.SimpanLeadTimeProspectFu = function () {

		
		if(OperationLeadTimeProspectFu=="Insert")
		{
			LeadTimeProspectFuFactory.create($scope.mLeadTimeProspectFu).then(
				function(res) {
					LeadTimeProspectFuFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.KembaliDariLeadTimeProspectFu();
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(OperationLeadTimeProspectFu=="Update")
		{
			LeadTimeProspectFuFactory.update($scope.mLeadTimeProspectFu).then(
				function(res) {
					LeadTimeProspectFuFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.KembaliDariLeadTimeProspectFu();
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        
    }
	
	$scope.TambahLeadTimeProspectFu = function() {
			OperationLeadTimeProspectFu="Insert";
			$scope.ShowLeadTimeProspectFuDetail=true;

			$scope.LeadTimeProspectFuTable=false;
			$scope.ShowSimpanLeadTimeProspectFu=true;
            $scope.mLeadTimeProspectFu={};
			$scope.mLeadTimeProspectFu_Enable=true;
			
        }
		
	$scope.KembaliDariLeadTimeProspectFu = function() {
			$scope.ShowLeadTimeProspectFuDetail=false;
			$scope.LeadTimeProspectFuTable=true;
        }

	$scope.lihatLeadTimeProspectFu = function(SelectedData) {
			OperationLeadTimeProspectFu="Look";
			$scope.ShowLeadTimeProspectFuDetail=true;
			$scope.LeadTimeProspectFuTable=false;
			$scope.ShowSimpanLeadTimeProspectFu=false;
			$scope.mLeadTimeProspectFu=angular.copy(SelectedData);
			$scope.mLeadTimeProspectFu_Enable=false;
			
			
        }
		
	$scope.editLeadTimeProspectFu = function(SelectedData) {
			
			OperationLeadTimeProspectFu="Update";
			$scope.ShowLeadTimeProspectFuDetail=true;
			$scope.LeadTimeProspectFuTable=false;
			
			$scope.ShowSimpanLeadTimeProspectFu=true;
			$scope.mLeadTimeProspectFu=angular.copy(SelectedData);

			$scope.mLeadTimeProspectFu_Enable=true;
			
        }	
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.actButtonCaption = {
                new:'',cancel: 'Kembali'
            }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'FollowUpProspectLeadTimeId', width:'7%', visible:false },
			{ name:'Cabang',  field: 'Name' },
			{ name:'Kategori Prospek',  field: 'CategoryProspectName' },
			{ name:'Waktu Standart Follow Up',  field: 'LeadTime' },
			{ name:'Action', enablePinning: true,pinRight : true, pinned: true, width:'8%',visible:true, cellTemplate: '<i title="Lihat" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.lihatLeadTimeProspectFu(row.entity)" ></i>	<i title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.editLeadTimeProspectFu(row.entity)" ></i>' },
        ]
    };
});
