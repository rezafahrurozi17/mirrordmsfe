angular.module('app')
    .controller('AreaRestrictionController', function($scope, $http, CurrentUser, AreaRestrictionFactory,$timeout,bsNotify) {
	$scope.mAreaRestriction=null;
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	AreaRestrictionFactory.getArea().then(function(res){
            $scope.optionsArea = res.data.Result;
            return $scope.optionsArea;
        });
		
	AreaRestrictionFactory.getRestriction().then(function(res){
            $scope.optionsRegionLevelRestriction = res.data.Result;
            return $scope.optionsRegionLevelRestriction;
        });	
	
	
	
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAreaRestriction = {}; //Model
    $scope.xRole={selected:[]};
	
			AreaRestrictionFactory.getDataProvince().then(function(res){
				$scope.province = res.data.Result;
			});

			AreaRestrictionFactory.getDataKabupaten().then(function(res){
				$scope.tempKabupaten = res.data.Result;
			})

			 AreaRestrictionFactory.getDataKecamatan().then(function(res){
				$scope.tempKecamatan = res.data.Result;
			})
	
			$scope.filterKabupaten = function(selected){
                $scope.mAreaRestriction.CityRegencyId = "";
                $scope.mAreaRestriction.DistrictId = "";
                $scope.kecamatan = null;
                // $scope.kabupaten = $scope.tempKabupaten.filter(function(type){
				// 	return (type.ProvinceId == selected);
				// })
                AreaRestrictionFactory.getDataKabupatenNew('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function (res) {
                    $scope.tempKabupaten = res.data.Result;
                    $scope.kabupaten = $scope.tempKabupaten;
                    if($scope.tmpMode == 0 ){
                        $scope.mAreaRestriction.CityRegencyId = $scope.DataSelected.CityRegencyId;
                        // $scope.tmpMode = 1;
                        $scope.filterKecamatan($scope.mAreaRestriction.CityRegencyId);
                    }
                });   

			}

			$scope.filterKecamatan = function(selected){
                    $scope.mAreaRestriction.DistrictId = "";
				// $scope.kecamatan = $scope.tempKecamatan.filter(function(type){
				// 	return (type.CityRegencyId == selected);
				// }) 
                AreaRestrictionFactory.getDataKecamatanNew('?start=1&limit=100&filterData=CityRegencyId|' + selected).then(function (res) {
                    $scope.tempKecamatan = res.data.Result;
                    $scope.kecamatan = $scope.tempKecamatan;
                    if($scope.tmpMode == 0 ){
                        $scope.mAreaRestriction.DistrictId = $scope.DataSelected.DistrictId;
                        $scope.tmpMode = 1;
                    }
    
                });  
			}
			
			

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
      
    $scope.getData = function() {
        AreaRestrictionFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
               bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }		
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'AreaRuleRestrictionId', width:'7%', visible:false },
            { name:'Area',  field: 'AreaName' },
			{ name:'Provinsi',  field: 'ProvinceName' },
			{ name:'Kabupaten/Kota',  field: 'CityRegencyName' },
			{ name:'Kecamatan',  field: 'DistrictName' },
			{ name:'SPK Restriction Name',  field: 'SPKRestrictionName' },
			{ name:'Swapping Restriction Name',  field: 'SwappingRestrictionName' },
        ]
    };

    $scope.onShowDetail = function (data,mode) {
      console.log('show detail', mode);
      console.log('show data', data);
      $scope.DataSelected = null;
      $scope.tmpMode = 0;
      if(mode == 'new'){
        $scope.tmpMode = 1; 
      }else{
        $scope.DataSelected = angular.copy(data);
        $scope.mAreaRestriction.AreaId = data.AreaName;
        $scope.mAreaRestriction.ProvinceId = data.ProvinceId
        $scope.mAreaRestriction.CityRegencyId = data.CityRegencyId;
        $scope.mAreaRestriction.DistrictId = data.DistrictId;
        $scope.mAreaRestriction.SPKRestrictionId = data.SPKRestrictionName;
        $scope.mAreaRestriction.SwappingRestrictionId = data.SwappingRestrictionName;
        $scope.filterKabupaten($scope.mAreaRestriction.ProvinceId);
      } 
    }
});
