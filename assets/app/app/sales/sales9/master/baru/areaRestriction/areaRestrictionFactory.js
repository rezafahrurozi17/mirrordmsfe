angular.module('app')
  .factory('AreaRestrictionFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryAreaRuleRestriction');
        //console.log('res=>',res);
        
        return res;
      },
	  getDataProvince: function() {
                var res = $http.get('/api/sales/MLocationProvince');
                return res;
            },
            getDataKabupaten: function() {
                var res = $http.get('/api/sales/MLocationCityRegency');
                return res;
            },
            getDataKabupatenNew: function(param) {
                var res = $http.get('/api/sales/MLocationCityRegency' + param);
                return res;
            },
            getDataKecamatan: function() {
                var res = $http.get('/api/sales/MLocationKecamatan');
                return res;
            },
            getDataKecamatanNew: function(param) {
                var res = $http.get('/api/sales/MLocationKecamatan' + param);
                return res;
            },
			getArea: function() {
                var res = $http.get('/api/sales/MsitesArea');
                return res;
            },
			getRestriction: function() {
                var res = $http.get('/api/sales/PCategoryAreaRestriction');
                return res;
            },
      create: function(AreaRestriction) {
        return $http.post('/api/sales/PCategoryAreaRuleRestriction', [{
                                            AreaId:AreaRestriction.AreaId,
											ProvinceId:AreaRestriction.ProvinceId,
											CityRegencyId:AreaRestriction.CityRegencyId,
											DistrictId:AreaRestriction.DistrictId,
											SPKRestrictionId:AreaRestriction.SPKRestrictionId,
											SwappingRestrictionId:AreaRestriction.SwappingRestrictionId
                                            }]);
      },
      update: function(AreaRestriction){
        return $http.put('/api/sales/PCategoryAreaRuleRestriction', [{
                                            AreaRuleRestrictionId:AreaRestriction.AreaRuleRestrictionId,
											AreaId:AreaRestriction.AreaId,
											ProvinceId:AreaRestriction.ProvinceId,
											CityRegencyId:AreaRestriction.CityRegencyId,
											DistrictId:AreaRestriction.DistrictId,
											SPKRestrictionId:AreaRestriction.SPKRestrictionId,
											SwappingRestrictionId:AreaRestriction.SwappingRestrictionId
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryAreaRuleRestriction',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd