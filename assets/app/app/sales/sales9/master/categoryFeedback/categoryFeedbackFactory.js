angular.module('app')
    .factory('CategoryFeedbackFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/PCategoryFeedback');
                //console.log('res=>',res);
                // var res = [{
                //     "CategoryFeedbackId": 1,
                //     "FeedBack": "Feed Back ABC"
                // }, {
                //     "CategoryFeedbackId": 2,
                //     "FeedBack": "Feed Back MNO"
                // }, {
                //     "CategoryFeedbackId": 1,
                //     "FeedBack": "Feed Back XYZ"
                // }];
                return res;
            },
            create: function(CategoryFeedback) {
                return $http.post('/api/sales/PCategoryFeedback', [{

                    Description: CategoryFeedback.Description
                }]);
            },
            update: function(CategoryFeedback) {
                return $http.put('/api/sales/PCategoryFeedback', [{
                    OutletId : CategoryFeedback.OutletId,
                    FeedbackCategoryId: CategoryFeedback.FeedbackCategoryId,
                    Description: CategoryFeedback.Description
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/PCategoryFeedback', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });