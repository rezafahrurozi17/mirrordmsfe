angular.module('app')
  .factory('GroupChecklistFinalInspectionFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityGroupCheckListInspection');
        return res;
      },
      getDataDetail: function(Id) {
        var res=$http.get('/api/sales/MActivityGroupCheckListInspection/GetById?GroupChecklistInspectionId='+Id);
        return res;
      },
	    getDataVehicleModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas'); 
        return res;
      },
	    getDataVehicleType: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');	  
        return res;
      },
      create: function(GroupChecklistFinalInspection) {
        return $http.post('/api/sales/MActivityGroupCheckListInspection', [{
                                            GroupChecklistInspectionName: GroupChecklistFinalInspection.GroupChecklistInspectionName,
                                            VehicleModelId: GroupChecklistFinalInspection.VehicleModelId,
                                            GroupCheckListImage: GroupChecklistFinalInspection.GroupCheckListImage
                                            }]);
      },
      update: function(GroupChecklistFinalInspection){
        return $http.put('/api/sales/MActivityGroupCheckListInspection', [{
                                            OutletId: GroupChecklistFinalInspection.OutletId,
                                            GroupChecklistInspectionId: GroupChecklistFinalInspection.GroupChecklistInspectionId,
                                            GroupChecklistInspectionName: GroupChecklistFinalInspection.GroupChecklistInspectionName,
                                            VehicleModelId: GroupChecklistFinalInspection.VehicleModelId,
                                            GroupCheckListImage: GroupChecklistFinalInspection.GroupCheckListImage
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityGroupCheckListInspection',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd