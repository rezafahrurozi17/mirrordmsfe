angular.module('app')
    .controller('GroupChecklistFinalInspectionController', function($scope, $http, CurrentUser, GroupChecklistFinalInspectionFactoryMaster,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mGroupChecklistFinalInspection = null; //Model
    $scope.xRole={selected:[]};
    $scope.Main = true;
    $scope.status = 'buat';

    //----------------------------------
    // Get Data test
    //----------------------------------
	
	//buat model	
	GroupChecklistFinalInspectionFactoryMaster.getDataVehicleModel().then(function(res) {
            $scope.optionsModelGroupChecklistFinalInspection = res.data.Result;
            return $scope.optionsModelGroupChecklistFinalInspection;
        });
	//buat tipe
	$scope.RawTipeGroupChecklistFinalInspection = "";
        GroupChecklistFinalInspectionFactoryMaster.getDataVehicleType().then(function(res) {
            $scope.RawTipeGroupChecklistFinalInspection = res.data.Result;
            return $scope.RawTipeGroupChecklistFinalInspection;
        });

    $scope.tambah = function (){
        $scope.Main = false;
        $scope.status = 'buat';
    }
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.grupChecklist').remove();

		});

    $scope.lihat = function (row){
        GroupChecklistFinalInspectionFactoryMaster.getDataDetail(row.GroupChecklistInspectionId).then(
            function(res){
                $scope.mGroupChecklistFinalInspection = res.data.Result[0];
                $scope.Main = false;
                $scope.status = 'lihat';
            },
            function(err){
            }
        );
    }

    $scope.revisi = function (row){
        GroupChecklistFinalInspectionFactoryMaster.getDataDetail(row.GroupChecklistInspectionId).then(
            function(res){
                $scope.mGroupChecklistFinalInspection = res.data.Result[0];
                $scope.Main = false;
                $scope.status = 'ubah';
            },
            function(err){
            }
        );
    }

    $scope.Simpan = function (){
        
		$scope.mGroupChecklistFinalInspection.GroupCheckListImage.UpDocObj = btoa($scope.mGroupChecklistFinalInspection.GroupCheckListImage.UpDocObj);
        if(angular.isDefined($scope.mGroupChecklistFinalInspection.GroupCheckListImage.PrefixHtmlData) == true){
            delete $scope.mGroupChecklistFinalInspection.GroupCheckListImage.PrefixHtmlData;
        } 

       if ($scope.status == 'buat'){
        GroupChecklistFinalInspectionFactoryMaster.create($scope.mGroupChecklistFinalInspection).then(function (res){
            GroupChecklistFinalInspectionFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				$scope.Main = true;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
			
        });
       }else{
		GroupChecklistFinalInspectionFactoryMaster.update($scope.mGroupChecklistFinalInspection).then(function (res){
            GroupChecklistFinalInspectionFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil diubah.",
						type: 'success'
					}
				);
				$scope.Main = true;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
			
        });
           
       }
        
    }

    $scope.Kembali = function(){
        $scope.Main = true;
        $scope.status = 'buat';
        $scope.mGroupChecklistFinalInspection = {};
    };

    $scope.lihatImage = function (row){
        $scope.temp = row;
        if($scope.temp.GroupCheckListImage != null){
            $scope.image = row.GroupCheckListImage.UpDocObj;
            angular.element('.ui.modal.grupChecklist').modal('show');
			angular.element('.ui.modal.grupChecklist').not(':first').remove();
        }
    }

    $scope.keluarImage = function(){
        angular.element('.ui.modal.grupChecklist').modal('hide');
    };

    $scope.test = function(){
        console.log("asdsad", $scope.mGroupChecklistFinalInspection);
    }

    // $scope.hapusGambar = function (image){
    //     image = {};
    // }
		
     $scope.getData = function() {
        GroupChecklistFinalInspectionFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    $scope.onSelectRows = function(rows){
    }

    var actionbutton = '<div>' +
    '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.lihat(row.entity)" tabindex="0"> ' +
    '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
    '</a>' +
    '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.revisi(row.entity)" tabindex="0"> ' +
    '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
    '</a>' +
    '</div>';
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'GroupChecklistInspectionId', field:'GroupChecklistInspectionId', width:'7%', visible:false},
            { name:'Group CheckList', field:'GroupChecklistInspectionName' },
            // { name:'Model', field:'VehicleModelName' },
            { name:"Action", width:'10%',cellTemplate:actionbutton,enableFiltering: false, visible: true}
        ]
    };
});


