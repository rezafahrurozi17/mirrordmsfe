angular.module('app')
    .controller('ActivityGroupChecklistInspeksiController', function($scope, $http, CurrentUser, ActivityGroupChecklistInspeksi, ModelFactory, TipeFactory, $timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mActivityGroupChecklistInspeksi = null; //Model
    $scope.xRole={selected:[]};
    $scope.TempTipe = [];

    //----------------------------------
    // Get Data
    //----------------------------------
    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    var listTipe = [];
    TipeFactory.getData().then(
        function(res){
            listTipe = res.data.Result;
            return res.data;
        }
    );

    $scope.filterModel = function() {
        $scope.getTipe = listTipe.filter(function(test)
        {
            return (test.VehicleModelId == $scope.mActivityGroupChecklistInspeksi.VehicleModelId);
        })

        var filterModel = $scope.listTipe.filter(function(obj)
        {
            return obj.VehicleTypeId == $scope.mActivityGroupChecklistInspeksi.VehicleTypeId;
        })

        if(filterModel.length == 0) 
            $scope.mActivityGroupChecklistInspeksi.VehicleTypeId = null;

    }

    var gridData = [];
    $scope.getData = function() {
        ActivityGroupChecklistInspeksi.getData()
        .then(
            function(res){
                gridData = [];
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }
        
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }

    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'GroupChecklistInspectionId',    field:'GroupChecklistInspectionId', width:'7%', visible:false },
            { name:'Group Checklist Inspection Name', field:'GroupChecklistInspectionName' },
            { name:'Model Kendaraan', field:'VehicleModelName' },
            { name:'Type Kendaraan', field:'VehicleTypeName' },
            { name:'Description',  field: 'GroupChecklistInspectionDescription' }
        ]
    };
});
