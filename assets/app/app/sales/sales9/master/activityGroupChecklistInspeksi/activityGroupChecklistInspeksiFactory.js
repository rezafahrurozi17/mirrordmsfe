angular.module('app')
  .factory('ActivityGroupChecklistInspeksi', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityGroupCheckListInspection');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityGroupChecklistInspeksi) {
        return $http.post('/api/sales/MActivityGroupCheckListInspection', [{
              GroupChecklistInspectionName:ActivityGroupChecklistInspeksi.GroupChecklistInspectionName,
              VehicleModelId:ActivityGroupChecklistInspeksi.VehicleModelId,
              VehicleTypeId:ActivityGroupChecklistInspeksi.VehicleTypeId,
              GroupChecklistInspectionDescription:ActivityGroupChecklistInspeksi.GroupChecklistInspectionDescription


              }]);
      },
      update: function(ActivityGroupChecklistInspeksi){
        return $http.put('/api/sales/MActivityGroupCheckListInspection', [{
              OutletId:ActivityGroupChecklistInspeksi.OutletId,
              GroupChecklistInspectionId:ActivityGroupChecklistInspeksi.GroupChecklistInspectionId,
							GroupChecklistInspectionName:ActivityGroupChecklistInspeksi.GroupChecklistInspectionName,
              VehicleModelId:ActivityGroupChecklistInspeksi.VehicleModelId,
              VehicleTypeId:ActivityGroupChecklistInspeksi.VehicleTypeId,
              GroupChecklistInspectionDescription:ActivityGroupChecklistInspeksi.GroupChecklistInspectionDescription
            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityGroupCheckListInspection',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });