angular.module('app')
  .factory('AccountGroupFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "account_group_id" : "1",
              "account_group_code" : "001",
              "account_group_name" : "Asset",
			  "account_type_id" : "1",
			  "account_type_name" : "type1",
              "account_format" : "Liability",
              "allow_sub_gl_bit" : "002",
              "active_bit" : "3",
              "group_system_bit" : "Equity",
          },
          {
              "account_group_id" : "2",
              "account_group_code" : "002",
              "account_group_name" : "Equity",
			  "account_type_id" : "2",
			  "account_type_name" : "type2",
              "account_format" : "Liability",
              "allow_sub_gl_bit" : "002",
              "active_bit" : "3",
              "group_system_bit" : "",
          }          
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
















