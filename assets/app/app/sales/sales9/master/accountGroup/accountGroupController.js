angular.module('app')
    .controller('AccountGroupController', function($scope, $http, CurrentUser, AccountGroupFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAccountGroup = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
	$scope.optionsAccountType = [{ account_type_name: "type1", account_type_id: "1" },{ account_type_name: "type2", account_type_id: "2" }];
        $scope.grid.data=AccountGroupFactory.getData();         
        $scope.loading=false;
    },
    function(err){
        bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Account Group Id', field:'account_group_id', width:'7%', visible:false },
            { name:'Account Group Code', field:'account_group_code'},
            { name:'Account Group Name', field:'account_group_name'},
			{ name:'Account Type id', field:'account_type_id', visible:false},
			{ name:'Account Type Name', field:'account_type_name'},
            { name:'Account Format', field:'account_format'},
            { name:'Allow Sub GL Bit', field:'allow_sub_gl_bit', visible:false},
            { name:'Active Bit', field:'active_bit', visible:false},
            { name:'Group System Bit', field:'group_system_bit', visible:false}
        ]
    };
});













