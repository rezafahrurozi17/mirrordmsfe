angular.module('app')
  .factory('ProfileInsuranceUserTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{InsuranceUserTypeId: "1", InsuranceUserTypeName: "Name 01", InsuranceUserTypeValue: "Value 01"},
				{InsuranceUserTypeId: "2", InsuranceUserTypeName: "Name 02", InsuranceUserTypeValue: "Value 02"},
        {InsuranceUserTypeId: "3", InsuranceUserTypeName: "Name 03", InsuranceUserTypeValue: "Value 03"},

			  ];
        res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/fw/Role', [{
                                            NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/fw/Role', [{
                                            ChecklistItemAdministrasiPembayaranId:ActivityChecklistItemAdministrasiPembayaran.ChecklistItemAdministrasiPembayaranId,
											NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd