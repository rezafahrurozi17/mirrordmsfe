angular.module('app')
    .controller('ProfileSalesProgramController', function($scope, $http, CurrentUser, ProfileSalesProgram,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
	
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mProfileSalesProgram = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        ProfileSalesProgram.getData()
        .then(
            function(res){
                gridData = [];
				$scope.dateOptionsStart.minDate=new Date();
				$scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
		
    }

    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 0
    };

    $scope.dateOptionsEnd = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 0,
    }

    $scope.tanggalmin = function (dt){
        
		$scope.dateOptionsEnd.minDate = dt;
		if($scope.mProfileSalesProgram.StartValidDate>=$scope.mProfileSalesProgram.EndValidDate)
		{
			$scope.mProfileSalesProgram.EndValidDate=$scope.mProfileSalesProgram.StartValidDate;
		}
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
	}
	
	$scope.hapusProfilSales = function(){
		//console.log("data alasan", $scope.selectedRows);
		var length = $scope.selectedRows.length;
		var id = [];
		for(var i in $scope.selectedRows){
			id.push($scope.selectedRows[i].SalesProgramId);
		};
		//console.log("asaS", id)
		ProfileSalesProgram.delete(id).then(
			function (res) {
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil dihapus.",
						type: 'success'
					}
				);
				$scope.selectedRows = [];
				$scope.getData();
			
		},
		function(err){
			bsNotify.show(
				{
					title: "Gagal",
					content: err.Message,
					type: 'danger'
				}
			);
		})
	}
	
	$scope.onSelectRows = function(row){
		$scope.selectedRow = angular.copy(row);
	}

	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			ProfileSalesProgram.create($scope.mProfileSalesProgram).then(
				function(res) {
					ProfileSalesProgram.getData().then(
						function(res){
							gridData = [];
							$scope.dateOptionsStart.minDate=new Date();
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			ProfileSalesProgram.update($scope.mProfileSalesProgram).then(
				function(res) {
					ProfileSalesProgram.getData().then(
						function(res){
							gridData = [];
							$scope.dateOptionsStart.minDate=new Date();
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content:  err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'SalesProgramId',field:'SalesProgramId', width:'7%', visible:false },
   			{ name:'Nama Sales Program',  field: 'SalesProgramName' },
			{ name:'Status',  field: 'StatusActive',cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-hide="row.entity.StatusActive">Non Aktif</label><label ng-show="row.entity.StatusActive">Aktif</label></p>' },
			{ name:'Tanggal Mulai',  field: 'StartValidDate',cellFilter:'date:\'dd-MM-yyyy\'' },
			{ name:'Tanggal Berakhir',  field: 'EndValidDate' ,cellFilter:'date:\'dd-MM-yyyy\''}
        ]
    };
});
