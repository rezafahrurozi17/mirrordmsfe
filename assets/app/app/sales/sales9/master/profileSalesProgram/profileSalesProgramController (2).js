angular.module('app')
    .controller('ProfileSalesProgramController2', function($scope, $http, CurrentUser, ProfileSalesProgram,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        //$scope.loading=true;
		$scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mProfileSalesProgram = null; //Model
    $scope.xRole={selected:[]};
	var datefilter='date:"dd/MM/yyyy"';

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
    //    ProfileSalesProgram.getData()
    //    .then(
    //       function(res){
    //            gridData = [];
    //            //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
    //            //$scope.grid.data = res.data.Result;nanti balikin
    //    $scope.grid.data=da_json;
	//			
    //            //console.log("ProfileSalesProgram=>",res.data.Result);nanti balikin
    //            //console.log("grid data=>",$scope.grid.data);
    //            //$scope.roleData = res.data;
    //            $scope.loading=false;
    //        },
    //        function(err){
    //            console.log("err=>",err);
    //        }
    //    );
		$scope.loading=false;
		$scope.grid.data=ProfileSalesProgram.getData();
		
		for (var i = 0; i < $scope.grid.data.length; ++i)  {
                var TanggalStartPRogram = new Date($scope.grid.data[i].TanggalStartPRogram); 
                var TanggalAkhirPRogram = new Date($scope.grid.data[i].TanggalAkhirPRogram);          
                TanggalStartPRogram.setDate(TanggalStartPRogram.getDate() +1);
                TanggalAkhirPRogram.setDate(TanggalAkhirPRogram.getDate() +1);               
                $scope.grid.data[i].TanggalAkhirPRogram = new Date(TanggalAkhirPRogram);
                $scope.grid.data[i].TanggalStartPRogram = new Date(TanggalStartPRogram);                        
            }
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'SalesProgramId', width:'7%', visible:false },
            { name:'SalesProgramName',  field: 'SalesProgramName' },
			{ name:'SalesProgramValue',  field: 'SalesProgramValue' },
			{ name:'TanggalStartPRogram',  field: 'TanggalStartPRogram' ,cellFilter:datefilter},
			{ name:'TanggalAkhirPRogram',  field: 'TanggalAkhirPRogram' ,cellFilter:datefilter},
        ]
    };
});
