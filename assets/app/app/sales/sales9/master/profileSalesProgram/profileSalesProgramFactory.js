angular.module('app')
  .factory('ProfileSalesProgram', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileSalesProgramForMaster');
        return res;
      },
      
      create: function(ProfileSalesProgram) {
        return $http.post('/api/sales/MProfileSalesProgram', [{
                                            //AppId: 1,
											SalesProgramName: ProfileSalesProgram.SalesProgramName,
											StatusActive:ProfileSalesProgram.StatusActive,
											StartValidDate: ProfileSalesProgram.StartValidDate.getFullYear()+'-'
											+('0' + (ProfileSalesProgram.StartValidDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ProfileSalesProgram.StartValidDate.getDate()).slice(-2)+'T'
											+((ProfileSalesProgram.StartValidDate.getHours()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getHours())+':'
											+((ProfileSalesProgram.StartValidDate.getMinutes()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getMinutes())+':'
											+((ProfileSalesProgram.StartValidDate.getSeconds()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getSeconds()),
											
											EndValidDate: ProfileSalesProgram.EndValidDate.getFullYear()+'-'
											+('0' + (ProfileSalesProgram.EndValidDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ProfileSalesProgram.EndValidDate.getDate()).slice(-2)+'T'
											+((ProfileSalesProgram.EndValidDate.getHours()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getHours())+':'
											+((ProfileSalesProgram.EndValidDate.getMinutes()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getMinutes())+':'
											+((ProfileSalesProgram.EndValidDate.getSeconds()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getSeconds())
											
											}]);
      },
      update: function(ProfileSalesProgram){
        return $http.put('/api/sales/MProfileSalesProgram', [{
                                            SalesProgramId: ProfileSalesProgram.SalesProgramId,
											OutletId: ProfileSalesProgram.OutletId,
											SalesProgramName: ProfileSalesProgram.SalesProgramName,
											StatusActive:ProfileSalesProgram.StatusActive,
											StartValidDate: ProfileSalesProgram.StartValidDate.getFullYear()+'-'
											+('0' + (ProfileSalesProgram.StartValidDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ProfileSalesProgram.StartValidDate.getDate()).slice(-2)+'T'
											+((ProfileSalesProgram.StartValidDate.getHours()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getHours())+':'
											+((ProfileSalesProgram.StartValidDate.getMinutes()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getMinutes())+':'
											+((ProfileSalesProgram.StartValidDate.getSeconds()<'10'?'0':'')+ ProfileSalesProgram.StartValidDate.getSeconds()),
											EndValidDate: ProfileSalesProgram.EndValidDate.getFullYear()+'-'
											+('0' + (ProfileSalesProgram.EndValidDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ProfileSalesProgram.EndValidDate.getDate()).slice(-2)+'T'
											+((ProfileSalesProgram.EndValidDate.getHours()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getHours())+':'
											+((ProfileSalesProgram.EndValidDate.getMinutes()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getMinutes())+':'
											+((ProfileSalesProgram.EndValidDate.getSeconds()<'10'?'0':'')+ ProfileSalesProgram.EndValidDate.getSeconds())}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileSalesProgram',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd