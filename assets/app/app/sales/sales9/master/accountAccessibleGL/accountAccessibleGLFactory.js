angular.module('app')
  .factory('AccountAccessibleGLFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "glId" : "1",
              "Name" : "GL0011",
              "Value" : "1"
          },
          {
              "glId" : "2",
              "Name" : "GL0012",
              "Value" : "2"
          }
        ];
        res = data;
        return res;
      },
      create: function(accountAccessibleGLFactory) {
        return $http.post('/api/fw/Role', [{
											Name : accountAccessibleGLFactory.Name,
                                            Value : accountAccessibleGLFactory.Value,
                                            }]);
      },
      update: function(accountAccessibleGLFactory){
        return $http.put('/api/fw/Role', [{
                                            glId : accountAccessibleGLFactory.glId,
											Name : accountAccessibleGLFactory.Name,
                                            Value : accountAccessibleGLFactory.Value,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });