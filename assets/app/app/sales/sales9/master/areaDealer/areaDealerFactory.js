angular.module('app')
  .factory('AreaDealerFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MAreaDealer');
        return res;
      },
      create: function(AreaDealer) {
        return $http.post('/api/sales/MAreaDealer', [{
                                            //AppId: 1,
											AreaName: AreaDealer.AreaName,
                                            Outlets: AreaDealer.Outlets}]);
      },
      update: function(AreaDealer){
        return $http.put('/api/sales/MAreaDealer', [{
                                            AreaId: AreaDealer.AreaId,
											AreaName: AreaDealer.AreaName,
                                            Outlets: AreaDealer.Outlets}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAreaDealer',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd