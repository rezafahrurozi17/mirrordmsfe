angular.module('app')
    .controller('OutletController', function($scope, $http, CurrentUser, OutletFactoryMaster,$timeout,bsNotify,$httpParamSerializer) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mOutlet = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={GroupDealerId:null,OutletId:null};

    //----------------------------------
    // Get Data test
    //----------------------------------
	
	OutletFactoryMaster.getDealerDropdown().then(function (res) {
        $scope.optionsOutlet_Dealer = res.data.Result;
        $scope.MasterOutletDisableFilterDealer=false;
        $scope.MasterOutletDisableFilterOutlet=false;
			//return $scope.optionsOutlet_Dealer;

			if($scope.user.RoleName == "Admin HO"||$scope.user.RoleName == "Admin Unit")
			{
				$scope.filter.GroupDealerId=$scope.user.GroupDealerId;
				//$scope.MasterOutletSearchDealerBrubah();
				$scope.MasterOutletDisableFilterDealer=true;
				OutletFactoryMaster.getOutletDropdown().then(function (res) {
					$scope.optionsOutlet_OutletRaw = res.data.Result;
					
					var Filtered_Outlet1 = [{ OutletId: null, OutletName: null }];
					Filtered_Outlet1.splice(0, 1);
					for (var i = 0; i < $scope.optionsOutlet_OutletRaw.length; ++i) {
						if ($scope.optionsOutlet_OutletRaw[i].DealerId == $scope.filter.GroupDealerId) {
							Filtered_Outlet1.push({ OutletId: $scope.optionsOutlet_OutletRaw[i].OutletId,OutletName: $scope.optionsOutlet_OutletRaw[i].OutletName });
						}
					}
					$scope.optionsOutlet_Outlet = Filtered_Outlet1;
					$scope.filter.OutletId=$scope.user.OutletId;
					if($scope.user.RoleName == "Admin Unit")
					{
						$scope.MasterOutletDisableFilterOutlet=true;
					}
					
				});
				
			}
		});
		
	OutletFactoryMaster.getOutletDropdown().then(function (res) {
			$scope.optionsOutlet_OutletRaw = res.data.Result;
			//return $scope.optionsOutlet_Outlet;
			
		});

	$scope.MasterOutletSearchDealerBrubah = function() {
            var Filtered_Outlet1 = [{ OutletId: null, OutletName: null }];
            Filtered_Outlet1.splice(0, 1);
            for (var i = 0; i < $scope.optionsOutlet_OutletRaw.length; ++i) {
                if ($scope.optionsOutlet_OutletRaw[i].DealerId == $scope.filter.GroupDealerId) {
                    Filtered_Outlet1.push({ OutletId: $scope.optionsOutlet_OutletRaw[i].OutletId,OutletName: $scope.optionsOutlet_OutletRaw[i].OutletName });
                }
            }
            $scope.optionsOutlet_Outlet = Filtered_Outlet1;
			$scope.filter.OutletId=null;
			
        };
	
     $scope.getData = function() {

		OutletFactoryMaster.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Outlet Id',    field:'OutletId', width:'7%', visible:false},
            { name:'Dealer', field:'DealerName' },
			{ name:'Kode Cabang', field:'OutletCode' },
			{ name:'Outlet', field:'OutletName' }
			
        ]
    };
});


