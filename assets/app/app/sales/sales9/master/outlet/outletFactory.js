angular.module('app')
  .factory('OutletFactoryMaster', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/GetMSitesOutlet?'+param);
        return res;
      },
	  getDealerDropdown: function() {
        var res=$http.get('/api/sales/MsitesGroupDealer');
        return res;
      },
	  getOutletDropdown: function() {
        var res=$http.get('/api/sales/GetMSitesOutlet');
        return res;
      },
      create: function(Outlet) {
        return $http.post('/api/sales/CCustomerReligion', [{
                                            //AppId: 1,
                                            CustomerReligionName: Outlet.CustomerReligionName}]);
      },
      update: function(Outlet){
        return $http.put('/api/sales/CCustomerReligion', [{
                                            CustomerReligionId: Outlet.CustomerReligionId,
                                            CustomerReligionName: Outlet.CustomerReligionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd