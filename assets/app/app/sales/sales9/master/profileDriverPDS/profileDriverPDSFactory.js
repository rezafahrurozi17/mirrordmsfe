angular.module('app')
  .factory('ProfileDriverPDSFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileDriverPDS');
        //console.log('res=>',res)
        return res;
      },
	  getDataPdc: function() {
                var res=$http.get('/api/sales/MSitesPDC');
                
                return res;
            },
      create: function(profileDriverPDS) {
        return $http.post('/api/sales/MProfileDriverPDS', [{
                                            DriverPDSName:profileDriverPDS.DriverPDSName,
											DriverPDSPhoneNo:profileDriverPDS.DriverPDSPhoneNo,
											DriverOutsource:profileDriverPDS.DriverOutsource,
											ListMSitePDC:profileDriverPDS.MSitesPDCDriverPDS,
                                            }]);
      },
      update: function(profileDriverPDS){
        return $http.put('/api/sales/MProfileDriverPDS', [{
                                            DriverPDSId:profileDriverPDS.DriverPDSId,
											DriverPDSName: profileDriverPDS.DriverPDSName,
											DriverPDSPhoneNo:profileDriverPDS.DriverPDSPhoneNo,
											DriverOutsource:profileDriverPDS.DriverOutsource,
											ListMSitePDC:profileDriverPDS.MSitesPDCDriverPDS,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileDriverPDS',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd