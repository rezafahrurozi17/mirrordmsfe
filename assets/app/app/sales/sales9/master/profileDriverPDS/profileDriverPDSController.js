angular.module('app')
    .controller('ProfileDriverPDSController', function($scope, $http, CurrentUser, ProfileDriverPDSFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
		$scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mProfileDriverPDS = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
	
	ProfileDriverPDSFactory.getDataPdc().then(function(res){
        $scope.optionsSitePdc = res.data.Result;
        return $scope.optionsSitePdc;
    });
	
    var gridData = [];
    $scope.getData = function() {
        ProfileDriverPDSFactory.getData()
        .then(
            function(res){
                gridData = [];
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
           },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
		$scope.loading=false;

    }
	
	
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){

    }
	
	$scope.RemoveSitePds = function (index) {
		$scope.mProfileDriverPDS.MSitesPDCDriverPDS.splice(index, 1);

    }
	
	$scope.AddSitePds = function () {
		if((typeof $scope.The_SitePds=="undefined"))
		{
			bsNotify.show(
					{
						title: "peringatan",
						content: "Anda harus memilih paling tidak satu site.",
						type: 'warning'
					}
				);
		}
		else if((typeof $scope.The_SitePds!="undefined"))
		{
			if ($scope.mProfileDriverPDS.MSitesPDCDriverPDS)
			{
				console.log('01',$scope.mProfileDriverPDS.MSitesPDCDriverPDS);

				if($scope.mProfileDriverPDS.MSitesPDCDriverPDS.length==0){
					$scope.mProfileDriverPDS.MSitesPDCDriverPDS.push($scope.The_SitePds);
				}else{
					var Tambah_SitePds_Aman=true;
					for (var i in $scope.mProfileDriverPDS.MSitesPDCDriverPDS) 
					{
						if($scope.mProfileDriverPDS.MSitesPDCDriverPDS[i].PDCId == $scope.The_SitePds.PDCId)
						{
							Tambah_SitePds_Aman=false;
							break;
						}
					}
					if(Tambah_SitePds_Aman==true)
					{
						$scope.mProfileDriverPDS.MSitesPDCDriverPDS.push($scope.The_SitePds);
					}
					else if(Tambah_SitePds_Aman==false)
					{
						bsNotify.show(
								{
									title: "peringatan",
									content: "Site PDS tidak boleh sama.",
									type: 'warning'
								}
							);
					}						
				}
				
				
				// for( var i in $scope.mProfileDriverPDS.MSitesPDCDriverPDS){
				//     if(){

				//     }
				// }
				
			}
			else
			{
				console.log('02');
				$scope.mProfileDriverPDS.MSitesPDCDriverPDS=[{ PDCId: null,PDCName:null}];
				$scope.mProfileDriverPDS.MSitesPDCDriverPDS.splice(0, 1);
				$scope.mProfileDriverPDS.MSitesPDCDriverPDS.push($scope.The_SitePds);
			}
		}
		
		
    }
	
	$scope.SetSelectedSitePds = function (SelectedSitePds) {
		console.log("setan",SelectedSitePds);
		$scope.The_SitePds=SelectedSitePds;

    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if($scope.mProfileDriverPDS.MSitesPDCDriverPDS.length<=0)
		{
			bsNotify.show(
					{
						title: "peringatan",
						content: "Site PDS tidak boleh kosong.",
						type: 'warning'
					}
				);
		}
		else if($scope.mProfileDriverPDS.MSitesPDCDriverPDS.length>=0)
		{
			if(mode=="create")
			{
				ProfileDriverPDSFactory.create($scope.mProfileDriverPDS).then(
					function(res) {
						ProfileDriverPDSFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			else if(mode=="update")
			{
				ProfileDriverPDSFactory.update($scope.mProfileDriverPDS).then(
					function(res) {
						ProfileDriverPDSFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			
			
			$scope.formApi.setMode("grid");
		}
		
		
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Driver PDS Id',    field:'DriverPDSId', width:'7%', visible:false },
            { name:'Nama DriverPDS', field:'DriverPDSName' },
            { name:'No Tel DriverPDS',  field: 'DriverPDSPhoneNo' },
			{ name:'Driver Outsource',  field: 'DriverOutsource' ,cellTemplate:'<p style="padding:5px 0 0 5px"><input type="checkbox" ng-checked="row.entity.DriverOutsource" disabled readonly /></p>'},
			{ name:'Site Pds',  field: 'MSitesPDCDriverPDS' , visible:false},
        ]
    };
});
