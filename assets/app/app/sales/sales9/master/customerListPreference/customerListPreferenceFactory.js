angular.module('app')
  .factory('CustomerListPreferenceFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
            "CustomerDetailPreferenceId" : "342",
            "CustomerId" : "234",
            "PreferenceId" : "03847283",
            "Description" : "XXXXX",
            "Value":""
          },
          {
            "CustomerDetailPreferenceId" : "231",
            "CustomerId" : "123",
            "PreferenceId" : "4234",
            "Description" : "ZZZZZZZZ",
            "Value":""
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });

