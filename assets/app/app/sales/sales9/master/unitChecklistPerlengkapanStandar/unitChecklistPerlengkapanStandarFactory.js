angular.module('app')
    .factory('UnitChecklistPerlengkapanStandarFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                // var res=$http.get('/api/sales/MUnitVehicleEquipmentStandard');
                var res=$http.get('/api/sales/MUnitVehicleEquipmentStandard');
                return res;
            },
            getDataUnit: function() {
                var res = $http.get('/api/sales/MUnitEquipmentStandard');
                return res;
            },
            getDataVehicleModelUC : function (){
                var res=$http.get('/api/sales/MUnitVehicleModelTomas');
                return res;
            },

            getDataVehiclePrintoutType : function (){
                var res=$http.get('/api/sales/PCategoryPrintOutType');
                return res;
            },

            getDataVehicleTypeUC : function (id){
                var res=$http.get('/api/sales/MUnitVehicleTypeTomas?VehicleModelId='+id);
                return res;
            },
            create: function(perlengkapaninsert) {
                console.log("baru",perlengkapaninsert);
                return $http.post('/api/sales/MUnitVehicleEquipmentStandard/Insert', [perlengkapaninsert]);
            },
            update: function(perlengkapanupdate) {
                console.log("update",perlengkapanupdate);
                return $http.put('/api/sales/MUnitVehicleEquipmentStandard/Update', [perlengkapanupdate]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitVehicleEquipmentStandard', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd