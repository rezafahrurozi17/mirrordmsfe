angular.module('app')
    .controller('UnitChecklistPerlengkapanStandarController', function($scope,bsNotify, $http, CurrentUser, UnitPerlengkapanStandarFactory,UnitChecklistPerlengkapanStandarFactory, $timeout) {
        //---------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            //$scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        //$scope.munitChecklistPerlengkapanStandar = null; //Model
        $scope.xRole = { selected: [] };
        $scope.gridData = true;
        $scope.formContent = false;
        $scope.ubah = false;


        $scope.datas = "";
        $scope.munitChecklistPerlengkapanStandar = {};
       // $scope.data.splice(0, 1);
        var VehicleType = [];
        $scope.vehicleTypefilter = [];
        var sequence = 0 ;
       $scope.munitChecklistPerlengkapanStandar.listDetail = [];
        //$scope.munitChecklistPerlengkapanStandar.listDetail.SequenceNumber = 0;
        //console.log("Unit", $scope.munitChecklistPerlengkapanStandar.NamaPerlengkapanStandar);

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            UnitChecklistPerlengkapanStandarFactory.getData().then(function(res){
                 $scope.grid.data = res.data.Result;
            })
        }

        
            UnitChecklistPerlengkapanStandarFactory.getDataUnit().then(function(res){
                $scope.perlenkapan = res.data.Result;
            });

            UnitChecklistPerlengkapanStandarFactory.getDataVehicleModelUC().then(function (res){
                $scope.VehicleModel = res.data.Result;
            });

            UnitChecklistPerlengkapanStandarFactory.getDataVehiclePrintoutType().then(function (res){
                $scope.PrintOutType = res.data.Result;
            });
            
           
        

        $scope.tambahChecklist = function(){
            $scope.gridData = false, $scope.formContent = true, $scope.btnSave=true, $scope.viewMode=false, $scope.ubah = false;
             $scope.munitChecklistPerlengkapanStandar = {};
        }

        $scope.viewDetail = function(row){
            $scope.gridData = false, $scope.formContent = true, $scope.viewMode=true;
            $scope.munitChecklistPerlengkapanStandar = angular.copy(row);
        }

        $scope.DisabledComboBox = false;
         $scope.update = function(row){
            $scope.gridData = false, $scope.formContent = true, $scope.btnUpdate=true, $scope.viewMode=false, $scope.ubah = true;
            $scope.munitChecklistPerlengkapanStandar = row;
            $scope.DisabledComboBox = true;
        }

        $scope.updateSave = function(){
            $scope.loading = true;
            UnitChecklistPerlengkapanStandarFactory.update($scope.munitChecklistPerlengkapanStandar).then(function(res){
                $scope.getData();
				bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
                $scope.loading = false;
                $scope.gridData = true, $scope.formContent = false, $scope.btnUpdate=false;
                $scope.DisabledComboBox = false;
            })
        }

         $scope.Save = function(){
            $scope.loading = true;
            UnitChecklistPerlengkapanStandarFactory.create($scope.munitChecklistPerlengkapanStandar).then(function(res){
                $scope.getData();
				bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
                $scope.loading = false, $scope.gridData = true, $scope.formContent = false, $scope.btnSave=false;
                $scope.DisabledComboBox = false;
            })
        }

        $scope.batal = function (){
             $scope.gridData = true, $scope.formContent=false, $scope.btnSave=false, $scope.btnUpdate=false;
             $scope.DisabledComboBox = false;
        }
  
        $scope.filtertipe = function(id) {
            UnitChecklistPerlengkapanStandarFactory.getDataVehicleTypeUC(id).then(function (res){
                $scope.vehicleTypefilter = res.data.Result;
            });

            //  $scope.vehicleTypefilter = VehicleType.filter(function(type){
            //     return (type.VehicleModelId == $scope.munitChecklistPerlengkapanStandar.VehicleModelId);
            // })

            // var filtertipe = $scope.vehicleTypefilter.filter(function(obj){
            //     return obj.VehicleTypeId == $scope.munitChecklistPerlengkapanStandar.VehicleTypeId;
            // })

            // if(filtertipe.length == 0) 
            //     $scope.munitChecklistPerlengkapanStandar.VehicleTypeId = null;

        }

        
    $scope.customDelete = function(selected) {
        $scope.array = [];
        for (var i in selected){
            for (var j in selected[i].listDetail){
                $scope.array.push(selected[i].listDetail[j].VehicleEquipmentStandardId);
            }
        }
        UnitChecklistPerlengkapanStandarFactory.delete($scope.array).then(function(res){
            $scope.getData();
        });
    };


        $scope.RemoveChildUnitChecklistPerlengkapanStandar = function(index) {
            // var total = $scope.munitChecklistPerlengkapanStandar.listDetail.length;
            // $scope.data = [];
            // for (var i = index + 1; i <= total - 1; i++) {
                // $scope.data.push($scope.munitChecklistPerlengkapanStandar.listDetail[i]);
            // }
            // console.log("test",$scope.data);
            // $scope.munitChecklistPerlengkapanStandar.listDetail.splice(index, total);
            // sequence = total - 1;
            // for (var i = 1; i <=  $scope.data.length; i++) {
               // $scope.munitChecklistPerlengkapanStandar.listDetail.push({ SequenceNumber: $scope.data[i].SequenceNumber - 1, 
                                                                        // EquipmentStandardId: $scope.data[i].EquipmentStandardId,
                                                                        // EquipmentStandardName: $scope.data[i].EquipmentStandardName });
            // }
			$scope.munitChecklistPerlengkapanStandar.listDetail.splice($scope.munitChecklistPerlengkapanStandar.listDetail.indexOf(index),1);
        }

        $scope.TambahChildUnitChecklistPerlengkapanStandar = function() {
            // try
            // {
				
				if((typeof $scope.munitChecklistPerlengkapanStandar.listDetail=="undefined")||$scope.munitChecklistPerlengkapanStandar.listDetail.length<=0)
				{
					$scope.munitChecklistPerlengkapanStandar.listDetail=[{ EquipmentStandardId: null,SequenceNumber:0,EquipmentStandardName:null}];
					var top = $scope.munitChecklistPerlengkapanStandar.listDetail.length - 1;
					sequence = $scope.munitChecklistPerlengkapanStandar.listDetail[top].SequenceNumber + 1;
					$scope.munitChecklistPerlengkapanStandar.listDetail.splice(0, 1);
					$scope.munitChecklistPerlengkapanStandar.listDetail.push({ EquipmentStandardId: $scope.perlengkapanStandar.EquipmentStandardId,
																				SequenceNumber: sequence,
																				EquipmentStandardName: $scope.perlengkapanStandar.EquipmentStandardName});
				}
				else if($scope.munitChecklistPerlengkapanStandar.listDetail.length>0 && (typeof $scope.munitChecklistPerlengkapanStandar.listDetail!="undefined"))
				{
					var top = $scope.munitChecklistPerlengkapanStandar.listDetail.length - 1;
					sequence = $scope.munitChecklistPerlengkapanStandar.listDetail[top].SequenceNumber + 1;
					
					var aman=true;
					
					if($scope.munitChecklistPerlengkapanStandar.listDetail!=null && $scope.munitChecklistPerlengkapanStandar.listDetail.length>=1 && (typeof $scope.munitChecklistPerlengkapanStandar.listDetail!="undefined"))
					{
						for(var x = 0; x < $scope.munitChecklistPerlengkapanStandar.listDetail.length; ++x)
						{
							if($scope.munitChecklistPerlengkapanStandar.listDetail[x].EquipmentStandardId==$scope.perlengkapanStandar.EquipmentStandardId)
							{
									aman=false;
									break;
							}
						}
					}
					if(aman==true)
					{
						$scope.munitChecklistPerlengkapanStandar.listDetail.push({EquipmentStandardId: $scope.perlengkapanStandar.EquipmentStandardId,
																				SequenceNumber: sequence,
																			   EquipmentStandardName: $scope.perlengkapanStandar.EquipmentStandardName});
					}
					else if(aman==false)
					{
						bsNotify.show(
								{
									title: "Peringatan",
									content: "Tolong pastikan data tidak duplikat.",
									type: 'warning'
								}
							);
					}
				}
				
                
				
            // }
            // catch(err)
            // {
				
                
            // }                                                        
            
        }


        $scope.perlengkapan= function(selected){
            $scope.perlengkapanStandar = selected;
        }

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
                console.log("onSelectRows=>", rows);
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------

        var customAction = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="View" tooltip-placement="bottom"' + 
                                    'onclick="this.blur()" ng-click="grid.appScope.$parent.viewDetail(row.entity)" tabindex="0"> \
                                    <i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"> </i> \
                                </a> \
                                <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Edit" tooltip-placement="bottom"' +
                                    'onclick="this.blur()" ng-click="grid.appScope.$parent.update(row.entity)" tabindex="0"> \
                                    <i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"> </i> \
                                </a>';
        var type = '<p class="" style="margin:0 0 0 0; padding:5px 0 0 5px">{{row.entity.Description}} {{row.entity.KatashikiCode}} {{row.entity.SuffixCode}} </p>';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            enableGroupHeaderSelection: true,
            showGroupPanel: true,
            //multiSelect: true,
            //enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            //groupable:true,
             //groups:'Model',
             //selectedItems: UnitChecklistPerlengkapanStandarFactory.getData(),
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'unit id', field: 'PrintOutTypeId', width: '7%', visible: false },
                { name: 'Tipe Cetakan', field: 'PrintOutTypeName' },
                // {
                //     name: 'model',
                //     field: 'VehicleModelName',
                //     width: '15%',
                //                     },
                // { name: 'type', 
                // cellTemplate: type,
                //           },
                 {
                    name: 'action',
                    allowCellFocus: false,
                    width: '10%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: customAction
                }
            ]
        };
    });