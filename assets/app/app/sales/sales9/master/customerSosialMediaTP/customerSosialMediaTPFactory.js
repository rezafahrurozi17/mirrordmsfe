angular.module('app')
  .factory('CustomerSosialMediaTPFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerSocialMedia');
        //console.log('res=>',res);
        
        return res;
      },
      create: function(customerSosialMediaTP) {
        return $http.post('/api/sales/CCustomerSocialMedia', [{
                                            SocialMediaName : customerSosialMediaTP.SocialMediaName,
                                            }]);
      },
      update: function(customerSosialMediaTP){
        return $http.put('/api/sales/CCustomerSocialMedia', [{
                                            SocialMediaId : customerSosialMediaTP.SocialMediaId,
                                            SocialMediaName : customerSosialMediaTP.SocialMediaName,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerSocialMedia',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
