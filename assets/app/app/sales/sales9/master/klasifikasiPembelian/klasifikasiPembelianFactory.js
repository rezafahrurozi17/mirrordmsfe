angular.module('app')
  .factory('KlasifikasiPembelianFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res = $http.get('/api/sales/MPurchasingClassification');
        return res;
      },
      create: function(KlasifikasiPembelian) {
        return $http.post('/api/sales/MPurchasingClassification', [{
                                            //AppId: 1,
          PurchasingClassificationName: KlasifikasiPembelian.PurchasingClassificationName}]);
      },
      update: function(KlasifikasiPembelian){
        return $http.put('/api/sales/MPurchasingClassification', [{
          PurchasingClassificationId: KlasifikasiPembelian.PurchasingClassificationId,
          PurchasingClassificationName: KlasifikasiPembelian.PurchasingClassificationName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MPurchasingClassification',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd