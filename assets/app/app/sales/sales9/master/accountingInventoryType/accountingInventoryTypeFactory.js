angular.module('app')
  .factory('AccountingInventoryTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FAccountingInventoryType');
        //console.log('res=>',res);
        var da_json=[
				{InventoryTypeId: "1", InventoryTypeName: "Name 01"},
        {InventoryTypeId: "2", InventoryTypeName: "Name 02"},
        {InventoryTypeId: "3", InventoryTypeName: "Name 03"},
				
			  ];
        //res=da_json;
        return res;
      },
      create: function(accountingInventoryType) {
        return $http.post('/api/sales/FAccountingInventoryType', [{
                                            InventoryTypeName:accountingInventoryType.InventoryTypeName,

                                            }]);
      },
      update: function(accountingInventoryType){
        return $http.put('/api/sales/FAccountingInventoryType', [{
                                            InventoryTypeId:accountingInventoryType.InventoryTypeId,
											OutletId:accountingInventoryType.OutletId,
											InventoryTypeName:accountingInventoryType.InventoryTypeName,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FAccountingInventoryType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd