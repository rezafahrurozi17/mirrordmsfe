angular.module('app')
    .factory('CategoryPrePrinterdTTUServicesFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                //var res=$http.get('/api/fw/Role');
                //console.log('res=>',res);
                var res = [{
                    "BussinessTypeId": 1,
                    "BussinessTypeName": "Home",
                    "ShowBit": true
                }, {
                    "BussinessTypeId": 2,
                    "BussinessTypeName": "Phone",
                    "ShowBit": true
                }, {
                    "BussinessTypeId": 3,
                    "BussinessTypeName": "Office",
                    "ShowBit": false
                }];
                return res;
            },
            create: function(CategoryPointsource) {
                // return res.post({
                //     'CategoryPointsourceName': CategoryPointsource.CategoryPointsourceName,
                //     'CategoryPointsourceValue': CategoryPointsource.CategoryPointsourceValue
                // });

                return $http.post('/api/fw/Role', [{
                    CategoryPointsourceName: CategoryPointsource.CategoryPointsourceName,
                    CategoryPointsourceValue: CategoryPointsource.CategoryPointsourceValue
                }]);
            },
            update: function(CategoryPointsource) {
                return $http.put('/api/fw/Role', [{
                    CategoryPointsourceId: CategoryPointsource.CategoryPointsourceId,
                    CategoryPointsourceName: CategoryPointsource.CategoryPointsourceName,
                    CategoryPointsourceValue: CategoryPointsource.CategoryPointsourceValue
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd