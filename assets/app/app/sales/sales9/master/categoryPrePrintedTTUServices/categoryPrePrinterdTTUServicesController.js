angular.module('app')
    .controller('CategoryPrePrinterdTTUServicesController', function($scope, $http, CurrentUser, CategoryPrePrinterdTTUServicesFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mCategoryPrePrinterdTTUServicesFactory = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            gridData = [];
            $scope.loading = true;
            $scope.grid.data = CategoryPrePrinterdTTUServicesFactory.getData();

            $scope.loading = false;

        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'id', field: 'BussinessTypeId', visible: false },
                { name: 'bussiness type id', field: 'BussinessTypeId' },
                { name: 'showbit', field: 'ShowBit' }
            ]
        };
    });