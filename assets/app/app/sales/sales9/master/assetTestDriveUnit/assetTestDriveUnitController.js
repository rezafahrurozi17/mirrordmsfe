angular.module('app')
	.controller('AssetTestDriveUnitController', function ($scope, $http, CurrentUser, AssetTestDriveUnit, $timeout, bsNotify) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = true;
			$scope.gridData = [];
		});

		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.mAssetTestDriveUnit = {}; //Model

		$scope.dateOptionsFrom = {
			startingDay: 1,
			format: "dd/MM/yyyy",
			disableWeekend: 1
		};


		$scope.dateOptionLastDate = {
			startingDay: 1,
			format: "dd/MM/yyyy",
			disableWeekend: 1,
		};

		//------------------------------
		// Get Master 
		//-------------------------------
		var VehicleType = [], VehicleColor = [];

		AssetTestDriveUnit.getDataVehicleModel().then(function (res) {
			$scope.VehicleModel = res.data.Result;
		});

		AssetTestDriveUnit.getDataVehicleType().then(function (res) {
			VehicleType = res.data.Result;
		});

		AssetTestDriveUnit.getDataVehicleTypeColors().then(function (res) {
			VehicleColor = res.data.Result;
		});

		AssetTestDriveUnit.getOutlet().then(function (res) {
			$scope.Outlet = res.data.Result;
		})

		//---------------------------------------
		//Combobox Filter
		//---------------------------------------


		$scope.emptyFromModel = function () {
			$scope.vehicleTypefilter = [], $scope.vehicleTypecolorfilter = [];
			$scope.mAssetTestDriveUnit.VehicleTypeId = null, $scope.mAssetTestDriveUnit.ColorId = null;
		}

		$scope.OutletReq = function (selected) {
			$scope.outletRequester = selected;
		}

		$scope.$watch("AssetTestDriveDaMode", function (newValue, oldValue) {
			if (newValue != 'view') {
				$scope.dateOptionsFrom.minDate = new Date();
			}
		});

		$scope.tambah = function () {
			var ga_duplikat = true;
			try {
				for (var i = 0; i < $scope.mAssetTestDriveUnit.listDetail.length; ++i) {
					if ($scope.mAssetTestDriveUnit.listDetail[i].OutletRequestorId == $scope.outletRequester.OutletId) {
						ga_duplikat = false;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Data yang anda pilih duplikat.",
								type: 'warning'
							}
						);
						break;
					}
				}
			}catch (ulala) {
				ga_duplikat = true;
			}

			if (ga_duplikat == true) {
				try {
					$scope.mAssetTestDriveUnit.listDetail.push({
						OutletRequestorId: $scope.outletRequester.OutletId,
						OutletRequestorName: $scope.outletRequester.Name,
						VehicleMappingId: $scope.VehicleMappingId
					});
				} catch (err) {
					$scope.mAssetTestDriveUnit.listDetail = [{ OutletRequestorId: null, OutletRequestorName: null, VehicleMappingId: null }];
					$scope.mAssetTestDriveUnit.listDetail.splice(0, 1);
					$scope.mAssetTestDriveUnit.listDetail.push({
						OutletRequestorId: $scope.outletRequester.OutletId,
						OutletRequestorName: $scope.outletRequester.Name,
						VehicleMappingId: $scope.VehicleMappingId
					});
				}
			}
		}

		$scope.remove = function (index) {
			$scope.mAssetTestDriveUnit.listDetail.splice(index, 1);
		}

		$scope.filtertipe = function (selected) {
			AssetTestDriveUnit.getDataVehicleType(selected).then(function (res) {
				$scope.vehicleTypefilter = res.data.Result
			})
		}

		$scope.filterColor = function (selected) {
			AssetTestDriveUnit.getDataVehicleTypeColors(selected).then(function (res) {
				$scope.vehicleTypecolorfilter = res.data.Result
			})
		}


		//----------------------------------
		// Get Data
		//----------------------------------

		$scope.getData = function () {
			$scope.loading = true;
			AssetTestDriveUnit.getData().then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$scope.loading = false;
				},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}

		$scope.selectRole = function (rows) {
			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}

		$scope.onSelectRows = function (rows) {
		}

		$scope.doCustomSave = function (mdl, mode) {
			var da_mAssetTestDriveUnit = angular.copy($scope.mAssetTestDriveUnit);

			if (mode == "create") {
				AssetTestDriveUnit.create(da_mAssetTestDriveUnit).then(
					function (res) {
						AssetTestDriveUnit.getData().then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								$scope.formApi.setMode("grid");
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					},
					function (err) {
						if (err.data.Message == "Tanggal Mulai lebih kecil dari hari ini") {
							bsNotify.show(
								{
									title: "Gagal",
									content: "Tanggal Mulai lebih kecil dari hari ini",
									type: 'danger'
								}
							);
						}else if (err.data.Message == "Tanggal Berakhir lebih kecil dari Tanggal Mulai"){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Tanggal Berakhir lebih kecil dari Tanggal Mulai",
									type: 'danger'
								}
							);
						} else {
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}

					}
				)
			}
			else if (mode == "update") {
				if (da_mAssetTestDriveUnit.OutletId == $scope.user.OutletId) {
					AssetTestDriveUnit.update(da_mAssetTestDriveUnit).then(
						function (res) {
							AssetTestDriveUnit.getData().then(
								function (res) {
									gridData = [];
									$scope.grid.data = res.data.Result;
									$scope.loading = false;
									bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
									$scope.formApi.setMode("grid");
									return res.data;
								},
								function (err) {
									bsNotify.show(
										{
											title: "Gagal",
											content: "Data tidak ditemukan.",
											type: 'danger'
										}
									);
								}
							);
						},function (err) {
							if (err.data.Message == "Tanggal Mulai lebih kecil dari hari ini") {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Tanggal Mulai lebih kecil dari hari ini",
										type: 'danger'
									}
								);
							}else if (err.data.Message == "Tanggal Berakhir lebih kecil dari Tanggal Mulai"){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Tanggal Berakhir lebih kecil dari Tanggal Mulai",
										type: 'danger'
									}
								);
							}else {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
						}
					)
				}else {
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Anda tidak dapat mengubah data milik cabang lain",
							type: 'warning'
						}
					);
				}

			}
		}

		$scope.tanggalmin = function (dt) {
			$scope.dateOptionLastDate.minDate = dt;
		}
		//----------------------------------
		// Grid Setup
		//----------------------------------
		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,
			columnDefs: [
				{ name: 'id', field: 'TestDriveUnitId', width: '7%', visible: false },
				{ name: 'nomor polisi', field: 'PoliceNumber' },
				{ name: 'model', field: 'VehicleModelName' },
				{ name: 'tipe', field: 'Description' },
				{ name: 'warna', field: 'ColorName' },
				{ name: 'no rangka', field: 'FrameNo' },
				{ name: 'waktu maksimal', field: 'TimeMaxTestDrive' },
				{ name: 'batas booking per hari', field: 'MaxOrderPerdayPerSales' },
				{ name: 'jarak terakhir', field: 'LastMetter' },
				{ name: 'outlet pemilik', field: 'OutletOwnerName' },
				{ name: 'Tanggal Mulai', field: 'LastStartDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
				{ name: 'Tanggal Berakhir', field: 'LastEndDate', cellFilter: 'date:\'dd-MM-yyyy\'' }
			]
		};
	});