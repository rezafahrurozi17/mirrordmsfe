angular.module('app')
    .factory('AssetTestDriveUnit', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        console.log("user", currentUser);
        return {
            getData: function() {
                var res = $http.get('/api/sales/SNegotiationTestDriveUnit');
                return res;
            },
            getOutlet: function() {
                var res = $http.get('/api/sales/MSitesOutletRequestor');
                return res;
            },
            getDataVehicleModel: function() {
                var res = $http.get('/api/sales/MUnitVehicleModelTomas');
                return res;
            },
            getDataVehicleType: function(param) {
                var res = $http.get('/api/sales/MUnitVehicleTypeTomas/?VehicleModelId='+param);
                return res;
            },
            getDataVehicleTypeColors: function(param) {
                var res = $http.get('/api/sales/MUnitVehicleTypeColorJoinTomas/?VehicleTypeId='+param);
                return res;
            },
            create: function(data) {
                var AssetTestDriveUnit = angular.copy(data);
                AssetTestDriveUnit.LastStartDate = new Date(AssetTestDriveUnit.LastStartDate).getFullYear() + '-' +
                    ('0' + (new Date(AssetTestDriveUnit.LastStartDate).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date(AssetTestDriveUnit.LastStartDate).getDate()).slice(-2) + 'T' +
                    ((new Date(AssetTestDriveUnit.LastStartDate).getHours() < '10' ? '0' : '') + new Date(AssetTestDriveUnit.LastStartDate).getHours()) + ':' +
                    '00:' + '00';
                AssetTestDriveUnit.LastEndDate = new Date(AssetTestDriveUnit.LastEndDate).getFullYear() + '-' +
                    ('0' + (new Date(AssetTestDriveUnit.LastEndDate).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date(AssetTestDriveUnit.LastEndDate).getDate()).slice(-2) + 'T' +
                    ((new Date(AssetTestDriveUnit.LastEndDate).getHours() < '10' ? '0' : '') + new Date(AssetTestDriveUnit.LastEndDate).getHours()) + ':' +
                    '00:' + '00';
                try {
                    AssetTestDriveUnit.listDetail.push({ OutletRequestorId: currentUser.OutletId });
                    return $http.post('/api/sales/SNegotiationTestDriveUnit/Insert', [{
                        PoliceNumber: AssetTestDriveUnit.PoliceNumber,
                        VehicleModelId: AssetTestDriveUnit.VehicleModelId,
                        VehicleTypeId: AssetTestDriveUnit.VehicleTypeId,
                        ColorId: AssetTestDriveUnit.ColorId,
                        FrameNo: AssetTestDriveUnit.FrameNo,
                        TimeMaxTestDrive: AssetTestDriveUnit.TimeMaxTestDrive,
                        MaxOrderPerdayPerSales: AssetTestDriveUnit.MaxOrderPerdayPerSales,
                        LastMetter: AssetTestDriveUnit.LastMetter,
                        LastStartDate: AssetTestDriveUnit.LastStartDate,
                        LastEndDate: AssetTestDriveUnit.LastEndDate,
                        OutletOwnerId: AssetTestDriveUnit.OutletId,
                        listDetail: AssetTestDriveUnit.listDetail
                    }]);
                } catch (wew) {
                    AssetTestDriveUnit.listDetail = [];
                    AssetTestDriveUnit.listDetail.push({ OutletRequestorId: currentUser.OutletId });
                    return $http.post('/api/sales/SNegotiationTestDriveUnit/Insert', [{
                        PoliceNumber: AssetTestDriveUnit.PoliceNumber,
                        VehicleModelId: AssetTestDriveUnit.VehicleModelId,
                        VehicleTypeId: AssetTestDriveUnit.VehicleTypeId,
                        ColorId: AssetTestDriveUnit.ColorId,
                        FrameNo: AssetTestDriveUnit.FrameNo,
                        TimeMaxTestDrive: AssetTestDriveUnit.TimeMaxTestDrive,
                        MaxOrderPerdayPerSales: AssetTestDriveUnit.MaxOrderPerdayPerSales,
                        LastMetter: AssetTestDriveUnit.LastMetter,
                        LastStartDate: AssetTestDriveUnit.LastStartDate,
                        LastEndDate: AssetTestDriveUnit.LastEndDate,
                        OutletOwnerId: AssetTestDriveUnit.OutletId,
                        listDetail: AssetTestDriveUnit.listDetail
                    }]);
                }
            },
            update: function(AssetTestDriveUnit) {
                AssetTestDriveUnit.LastStartDate = new Date(AssetTestDriveUnit.LastStartDate).getFullYear() + '-' +
                    ('0' + (new Date(AssetTestDriveUnit.LastStartDate).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date(AssetTestDriveUnit.LastStartDate).getDate()).slice(-2) + 'T' +
                    ((new Date(AssetTestDriveUnit.LastStartDate).getHours() < '10' ? '0' : '') + new Date(AssetTestDriveUnit.LastStartDate).getHours()) + ':' +
                    '00:' + '00';
                AssetTestDriveUnit.LastEndDate = new Date(AssetTestDriveUnit.LastEndDate).getFullYear() + '-' +
                    ('0' + (new Date(AssetTestDriveUnit.LastEndDate).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date(AssetTestDriveUnit.LastEndDate).getDate()).slice(-2) + 'T' +
                    ((new Date(AssetTestDriveUnit.LastEndDate).getHours() < '10' ? '0' : '') + new Date(AssetTestDriveUnit.LastEndDate).getHours()) + ':' +
                    '00:' + '00';
                return $http.put('/api/sales/SNegotiationTestDriveUnit/Update', [{
                    OutletId: AssetTestDriveUnit.OutletId,
                    TestDriveUnitId: AssetTestDriveUnit.TestDriveUnitId,
                    PoliceNumber: AssetTestDriveUnit.PoliceNumber,
                    VehicleModelId: AssetTestDriveUnit.VehicleModelId,
                    VehicleTypeId: AssetTestDriveUnit.VehicleTypeId,
                    ColorId: AssetTestDriveUnit.ColorId,
                    FrameNo: AssetTestDriveUnit.FrameNo,
                    TimeMaxTestDrive: AssetTestDriveUnit.TimeMaxTestDrive,
                    MaxOrderPerdayPerSales: AssetTestDriveUnit.MaxOrderPerdayPerSales,
                    LastMetter: AssetTestDriveUnit.LastMetter,
                    LastStartDate: AssetTestDriveUnit.LastStartDate,
                    LastEndDate: AssetTestDriveUnit.LastEndDate,
                    OutletOwnerId: AssetTestDriveUnit.OutletId,
                    listDetail: AssetTestDriveUnit.listDetail
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/SNegotiationTestDriveUnit', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });