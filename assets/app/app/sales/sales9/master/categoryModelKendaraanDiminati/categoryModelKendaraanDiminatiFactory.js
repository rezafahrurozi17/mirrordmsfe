angular.module('app')
    .factory('CategoryModelKendaraanDiminatiFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                //var res=$http.get('/api/fw/Role');
                //console.log('res=>',res);
                var res = [{
                    "ModelKendaraanDiminatiId": 1,
                    "ModelKendaraanDiminatiName": "Model ABC",
                    "ModelKendaraanDiminatiValue": "ABC"
                }, {
                    "ModelKendaraanDiminatiId": 2,
                    "ModelKendaraanDiminatiName": "Model MNO",
                    "ModelKendaraanDiminatiValue": "MNO"
                }, {
                    "ModelKendaraanDiminatiId": 3,
                    "ModelKendaraanDiminatiName": "Model XYZ",
                    "ModelKendaraanDiminatiValue": "XYZ"
                }];
                return res;
            },
            create: function(SiteAreaStock) {
                return $http.post('/api/fw/Role', [{

                    NamaAreaStock: SiteAreaStock.NamaAreaStock
                }]);
            },
            update: function(SiteAreaStock) {
                return $http.put('/api/fw/Role', [{
                    AreaStockId: SiteAreaStock.AreaStockId,
                    //pid: negotiation.pid,
                    NamaAreaStock: SiteAreaStock.NamaAreaStock
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });