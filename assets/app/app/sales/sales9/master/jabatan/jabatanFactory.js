angular.module('app')
  .factory('MasterJabatanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerPosition');
        return res;
      },
      create: function(Jabatan) {
        return $http.post('/api/sales/CCustomerPosition', [{
                                            //AppId: 1,
                                            CustomerPositionName: Jabatan.CustomerPositionName}]);
      },
      update: function(Jabatan){
        return $http.put('/api/sales/CCustomerPosition', [{
                                            CustomerPositionId: Jabatan.CustomerPositionId,
                                            CustomerPositionName: Jabatan.CustomerPositionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerPosition',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd