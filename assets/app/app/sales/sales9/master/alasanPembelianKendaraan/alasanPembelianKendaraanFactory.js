angular.module('app')
  .factory('AlasanPembelianKendaraanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityReasonBuying');
        //console.log('res=>',res);
        return res;
      },
      create: function(alasan) {
        return $http.post('/api/sales/MActivityReasonBuying', [{
                ReasonBuyingName: alasan.ReasonBuyingName,
                }]);
      },
      update: function(alasan){
        return $http.put('/api/sales/MActivityReasonBuying', [{
                ReasonBuyingId:alasan.ReasonBuyingId,
				OutletId: alasan.OutletId,
                ReasonBuyingName:alasan.ReasonBuyingName,
            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityReasonBuying',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd