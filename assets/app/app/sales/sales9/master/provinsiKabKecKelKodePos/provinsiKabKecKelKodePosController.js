angular.module('app')
    .controller('ProvinsiKabKecKelKodePosController', function($scope, $http, CurrentUser, ProvinsiKabKecKelKodePosFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mProvinsiKabKecKelKodePos = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={IslandId:null,ProvinceId:null, CityRegencyId:null, DistrictId:null, VillageId:null};

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
		if($scope.filter.ProvinceId!=null)
		{
			ProvinsiKabKecKelKodePosFactory.getData($scope.filter).then(
				function(res){
					$scope.grid.data = res.data.Result;
					$scope.loading=false;
					return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Gagal ambil data.",
							type: 'danger'
						}
					);
				}
			);
		}
		else if($scope.filter.ProvinceId==null||$scope.filter.IslandId==null)
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Field pulau dan provinsi perlu diisi.",
							type: 'warning'
						}
					);
		}
        
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
	
	ProvinsiKabKecKelKodePosFactory.getIsland().then(function (res) {
		$scope.ProvinsiKabKecKelKodePos_DaIslandOptions = res.data.Result;
		return $scope.ProvinsiKabKecKelKodePos_DaIslandOptions;
	});
	
	// ProvinsiKabKecKelKodePosFactory.getProvince().then(function (res) {
		// $scope.ProvinsiKabKecKelKodePos_DaProvinceOptions = res.data.Result;
		// return $scope.ProvinsiKabKecKelKodePos_DaProvinceOptions;
	// });
	
	$scope.ProvinsiKabKecKelKodePosSelectedChanged_Island = function(selected) {   
		// ProvinsiKabKecKelKodePosFactory.getCityRegency('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function(res) {
            // $scope.ProvinsiKabKecKelKodePos_DaKabupatenOptions = res.data.Result;
            // return $scope.ProvinsiKabKecKelKodePos_DaKabupatenOptions;
        // });
		ProvinsiKabKecKelKodePosFactory.getProvince('?start=1&limit=100&filterData=IslandId|' + selected).then(function (res) {
			$scope.ProvinsiKabKecKelKodePos_DaProvinceOptions = res.data.Result;
			return $scope.ProvinsiKabKecKelKodePos_DaProvinceOptions;
		});
		$scope.filter.CityRegencyId=null;
		$scope.filter.ProvinceId=null;
		$scope.filter.DistrictId=null;
		$scope.filter.VillageId=null;
		$scope.ProvinsiKabKecKelKodePos_DaKabupatenOptions=[];
		$scope.ProvinsiKabKecKelKodePos_DaKecamatanOptions=[];
		$scope.ProvinsiKabKecKelKodePos_DaKelurahanOptions=[];
    }
	
	$scope.ProvinsiKabKecKelKodePosSelectedChanged_Provinsi = function(selected) {   
		ProvinsiKabKecKelKodePosFactory.getCityRegency('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function(res) {
            $scope.ProvinsiKabKecKelKodePos_DaKabupatenOptions = res.data.Result;
            return $scope.ProvinsiKabKecKelKodePos_DaKabupatenOptions;
        });

		$scope.filter.CityRegencyId=null;
		$scope.filter.DistrictId=null;
		$scope.filter.VillageId=null;
		$scope.ProvinsiKabKecKelKodePos_DaKecamatanOptions=[];
		$scope.ProvinsiKabKecKelKodePos_DaKelurahanOptions=[];
    }
	
	$scope.ProvinsiKabKecKelKodePosSelectedChanged_Kabupaten = function(selected) {   
		ProvinsiKabKecKelKodePosFactory.getDistrict('?start=1&limit=100&filterData=CityRegencyId|' + selected).then(function(res) {
            $scope.ProvinsiKabKecKelKodePos_DaKecamatanOptions = res.data.Result;
            return $scope.ProvinsiKabKecKelKodePos_DaKecamatanOptions;
        });


		$scope.filter.DistrictId=null;
		$scope.filter.VillageId=null;
		$scope.ProvinsiKabKecKelKodePos_DaKelurahanOptions=[];
    }
	
	$scope.ProvinsiKabKecKelKodePosSelectedChanged_Kecamatan = function(selected) {   
		ProvinsiKabKecKelKodePosFactory.getVillage('?start=1&limit=100&filterData=DistrictId|' + selected).then(function(res) {
            $scope.ProvinsiKabKecKelKodePos_DaKelurahanOptions = res.data.Result;
            return $scope.ProvinsiKabKecKelKodePos_DaKelurahanOptions;
        });


		$scope.filter.VillageId=null;

    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'LocationId',    field:'LocationId', width:'7%', visible:false},
            { name:'Pulau', field:'IslandName' },
			{ name:'Provinsi', field:'ProvinceName' },
			{ name:'Kabupaten/Kota', field:'CityRegencyName' },
			{ name:'Kecamatan', field:'DistrictName' },
			{ name:'Kelurahan', field:'VillageName' },
			{ name:'Kode Pos', field:'PostalCode' }
        ]
    };
});


