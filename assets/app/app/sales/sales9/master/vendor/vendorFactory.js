angular.module('app')
  .factory('VendorFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MVendor');
        //console.log('res=>',res);
        var data = [
          {
              "VendorId" : 1,           
              "Name" : "PT Indomarco Prismatama",
			  "SPLD":true,
			  "VendorType":"t1"
          },
          {
              "VendorId" : 2,           
              "Name" : "PT Surya Madistrindo",
			  "SPLD":false,
			  "VendorType":"t2"
          }
        ];
        //res = data;
        return res;
      },
      create: function(Vendor) {
        return $http.post('/api/sales/MVendor', [{
                                            Name : Vendor.Name,
											SPLDBit : Vendor.SPLDBit,
											VendorTypeId : Vendor.VendorTypeId,
                                            }]);
      },
      update: function(Vendor){
        return $http.put('/api/sales/MVendor', [{
                                            VendorId : Vendor.VendorId,
											OutletId : Vendor.OutletId,
											Name : Vendor.Name,
											SPLDBit : Vendor.SPLDBit,
											VendorTypeId : Vendor.VendorTypeId,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MVendor',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });