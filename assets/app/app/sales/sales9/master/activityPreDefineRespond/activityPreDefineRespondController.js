angular.module('app')
    .controller('ActivityPreDefineRespondController', function($scope, $http, CurrentUser, ActivityPreDefineRespond,$timeout,bsNotify, $filter) {
    //----------------------------------
    // Start-Up
    //----------------------------------
	
    $scope.$on('$viewContentLoaded', function() {
		$scope.loading=false;
        $scope.gridData=[];
    });
	$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
    $scope.ActivityPredefineRespondCurrentLevel="";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mActivityPreDefineRespond = null; //Model
    $scope.xRole={selected:[]};
	
	$scope.ActivityPreDefineRespond=true;
	//$scope.ActivityPredefineRespondEnableTheDetails=false;
	$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=false;
	$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=false;
	$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
	$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=false;
	$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=false;
	$scope.OperationActivityPredefineRespond="";
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').remove();
		  angular.element('.ui.modal.ModalActivityPredefineRespondPeringatanAnak').remove();
		});

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    
	$scope.optionsActivityPredefineRespondEndRespond = ActivityPreDefineRespond.GetActivityPredefineRespondEndRespondOptions();
	
	ActivityPreDefineRespond.getActivityTypeData().then(function (res) {
			$scope.optionsActivityTypeRaw = res.data.Result;
		}).then(function () {

			$scope.optionsActivityType=angular.copy($scope.optionsActivityTypeRaw);
			$scope.optionsActivityType.splice(0,$scope.optionsActivityTypeRaw.length);

			for(var i = 0; i < $scope.optionsActivityTypeRaw.length; ++i)
			{	
				if($scope.optionsActivityTypeRaw[i].ActivityTypeName == "Customer/Prospect" || $scope.optionsActivityTypeRaw[i].ActivityTypeName == "Others")
				{
					$scope.optionsActivityType.push($scope.optionsActivityTypeRaw[i]);
				}
			}
		});

	ActivityPreDefineRespond.getActivityTaskTypeData().then(function (res) {
			$scope.optionsActivityTaskTypeRaw = res.data.Result;
		}).then(function () {

			$scope.optionsActivityTaskType=angular.copy($scope.optionsActivityTaskTypeRaw);
			$scope.optionsActivityTaskType.splice(0,$scope.optionsActivityTaskTypeRaw.length);

			for(var i = 0; i < $scope.optionsActivityTaskTypeRaw.length; ++i)
			{	
				if($scope.optionsActivityTaskTypeRaw[i].TaskTypeName != "Other")
				{
					$scope.optionsActivityTaskType.push($scope.optionsActivityTaskTypeRaw[i]);
				}
			}
		});
	
	
	$scope.DeleteActivityPredefineRespond = function() {
			
			
			$scope.ActivityPredefineRespondHapusAdaAnak="ga ada anak";
			//ActivityPreDefineRespondSelctedRow
			for(var i = 0; i < $scope.ActivityPreDefineRespondSelctedRow.length; ++i)
			{
				if($scope.ActivityPreDefineRespondSelctedRow[i].ParentId==0)
				{
					$scope.ActivityPredefineRespondCurrentLevel="Level 1";
				}
				else if($scope.ActivityPreDefineRespondSelctedRow[i].ParentId!=0 && $scope.ActivityPreDefineRespondSelctedRow[i].EndRespond==null)
				{
					$scope.ActivityPredefineRespondCurrentLevel="Level 2";
				}
				else if($scope.ActivityPreDefineRespondSelctedRow[i].ParentId!=0&&$scope.ActivityPreDefineRespondSelctedRow[i].EndRespond!=null&&$scope.ActivityPreDefineRespondSelctedRow[i].ParentNameAnak!=null&&($scope.ActivityPreDefineRespondSelctedRow[i].ParentNameAnak!=$scope.ActivityPreDefineRespondSelctedRow[i].ParentName))
				{
					$scope.ActivityPredefineRespondCurrentLevel="Level 2";
				}
				else if($scope.ActivityPreDefineRespondSelctedRow[i].ParentNameEngkong==null && $scope.ActivityPreDefineRespondSelctedRow[i].ParentNameAnak==$scope.ActivityPreDefineRespondSelctedRow[i].ParentName && $scope.ActivityPreDefineRespondSelctedRow[i].ParentNameAnak!=null && $scope.ActivityPreDefineRespondSelctedRow[i].ParentId!=0 && $scope.ActivityPreDefineRespondSelctedRow[i].EndRespond!=null)
				{
					$scope.ActivityPredefineRespondCurrentLevel="Level 2";
				}
				
				//level 1 cek yang laen ada ga yang punya parentid sama kaya id
				//level 2 cek yang punya parent id dan row.entity.ParentId!=0 && row.entity.EndRespond!=null && row.entity.ParentNameEngkong!=null sama id asli
				var tampung=$scope.grid.data;
				if($scope.ActivityPredefineRespondCurrentLevel=="Level 1")
				{
					for(var x = 0; x < tampung.length; ++x)
					{
						if(tampung[x].ParentId==$scope.ActivityPreDefineRespondSelctedRow[i].ActivityPreDefineRespondId)
						{
							$scope.ActivityPredefineRespondHapusAdaAnak="level 1 punya anak";
							break;
						}
					}
				}
				else if($scope.ActivityPredefineRespondCurrentLevel=="Level 2")
				{
					for(var x = 0; x < tampung.length; ++x)
					{
						if(tampung[x].ParentId==$scope.ActivityPreDefineRespondSelctedRow[i].ActivityPreDefineRespondId && $scope.ActivityPreDefineRespondSelctedRow[i].ParentId!=0 && $scope.ActivityPreDefineRespondSelctedRow[i].EndRespond==null)
						{
							$scope.ActivityPredefineRespondHapusAdaAnak="level 2 punya anak";
							break;
						}
					}
				}
			}
			
			if($scope.ActivityPredefineRespondHapusAdaAnak=="ga ada anak")
			{
				setTimeout(function() {
				  angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').not(':first').remove();  
				}, 1);
			}
			else
			{
				setTimeout(function() {
				  angular.element('.ui.modal.ModalActivityPredefineRespondPeringatanAnak').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalActivityPredefineRespondPeringatanAnak').not(':first').remove();  
				}, 1);
			}
        }
		
	$scope.ModalActivityPredefineRespondPeringatanAnakBatal = function() {
		angular.element('.ui.modal.ModalActivityPredefineRespondPeringatanAnak').modal('hide');
	}

	$scope.ModalActivityPredefineRespondPeringatanAnakLanjut = function() {
			angular.element('.ui.modal.ModalActivityPredefineRespondPeringatanAnak').modal('hide');
			
			setTimeout(function() {
				  angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').not(':first').remove();  
				}, 1);
		}	
		
	$scope.HapusActivityPreDefineRespondCancel = function() {
		angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').modal('hide');
	}

	$scope.HapusActivityPreDefineRespondOk = function() {
		$scope.IdToDelete=[{ ActivityPreDefineRespondId: null}];
			$scope.IdToDelete.splice(0, 1);
			
			for(var i = 0; i < $scope.ActivityPreDefineRespondSelctedRow.length; ++i)
			{
				$scope.IdToDelete.push($scope.ActivityPreDefineRespondSelctedRow[i].ActivityPreDefineRespondId);
					
			}
			
			ActivityPreDefineRespond.delete($scope.IdToDelete).then(function () {
					ActivityPreDefineRespond.getData().then(
						function(res){
							 gridData = [];
							 $scope.ActivityPreDefineRespondSelctedRow= [];
							 angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').modal('hide');
							 //$scope.grid.data = res.data.Result; //nanti balikin
							 
							 var tampung=res.data.Result;
							 for(var i = 0; i < tampung.length; ++i)
							{	
								var ParentIdNameThingy=null;
								var ParentIdNameAnakThingy=null;
								if(tampung[i].ParentId!=0 && tampung[i].EndRespond!=null)
								{
									var ParentIdAnak=tampung[i].ParentId;
									ParentIdNameAnakThingy=tampung[i].ParentName;
									
									for(var x = 0; x < tampung.length; ++x)
									{
										if(tampung[x].ActivityPreDefineRespondId==ParentIdAnak )
										{
											for(var y = 0; y < tampung.length; ++y)
											{
												if(tampung[y].ActivityPreDefineRespondId==tampung[x].ParentId)
												{
													ParentIdNameThingy=tampung[y].ActivityPreDefineRespondName;
													break;
												}
												
											}
										}
									}
								}
								else
								{
									ParentIdNameAnakThingy=null;
									ParentIdNameThingy=null;
								}
								tampung[i].ParentNameAnak=ParentIdNameAnakThingy;
								tampung[i].ParentNameEngkong=ParentIdNameThingy;
							}
							 
							 $scope.grid.data = tampung; //nanti balikin
							 
							 $scope.loading=false;
						 },
						 function(err){
							 $scope.ActivityPreDefineRespondSelctedRow= [];
							 angular.element('.ui.modal.ModalActivityPreDefineRespondHapus').modal('hide');
							 bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						 }
					)
				});
	}
	
	$scope.KembaliDariActivityPredefineRespond = function() {
			$scope.ShowActivityPreDefineRespondDetail=false;
			$scope.ActivityPreDefineRespond=true;
            $scope.mActivityPreDefineRespond="";
			
        }
		
	$scope.LihatActivityPreDefineRespond = function(SelectedData) {
			$scope.OperationActivityPredefineRespond="Look";
			$scope.ShowActivityPreDefineRespondDetail=true;
			$scope.ActivityPreDefineRespond=false;
			$scope.ShowSimpanActivityPredefineRespond=false;
			
			// ActivityPreDefineRespond.getData().then(function(res){
				// //$scope.optionsParentRespond = res.data.Result;
				// $scope.optionsParentRespond = res.data.Result.filter(function (type) 
				// { return (type.ActivityTypeId == selected.ActivityTypeId && type.TaskTypeId == selected.TaskTypeId); })
				
				 // return res.data;
			// });
			
			$scope.mActivityPreDefineRespond=angular.copy(SelectedData);
			$scope.ActivityPredefineDaFilterThing(SelectedData.ActivityTypeId);
				
			$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
			$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=false;
			$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=false;
			
			if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
			{
				$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
			}
			else
			{
				$scope.ActivityPredefineRespondDaTipeTugasIlang=true;
			}
			
			if($scope.mActivityPreDefineRespond.ParentId==0)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 1";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond==null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0&&$scope.mActivityPreDefineRespond.EndRespond!=null&&$scope.mActivityPreDefineRespond.ParentNameAnak!=null&&($scope.mActivityPreDefineRespond.ParentNameAnak!=$scope.mActivityPreDefineRespond.ParentName))
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentNameEngkong==null && $scope.mActivityPreDefineRespond.ParentNameAnak==$scope.mActivityPreDefineRespond.ParentName && $scope.mActivityPreDefineRespond.ParentNameAnak!=null && $scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond!=null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond!=null && $scope.mActivityPreDefineRespond.ParentNameEngkong!=null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 3";
			}
        }

	$scope.UpdateActivityPreDefineRespond = function(SelectedData) {


			$scope.OperationActivityPredefineRespond="Update";
			$scope.ShowActivityPreDefineRespondDetail=true;
			$scope.ActivityPreDefineRespond=false;
			$scope.ShowSimpanActivityPredefineRespond=true;
			// ActivityPreDefineRespond.getDataExceptSelf(SelectedData.ActivityPreDefineRespondId).then(function(res){
				// //$scope.optionsParentRespond = res.data.Result;
				// $scope.optionsParentRespond = res.data.Result.filter(function (type) 
				// { return (type.ActivityTypeId == selected.ActivityTypeId && type.TaskTypeId == selected.TaskTypeId); })
				
				 // return res.data;
			// });

            $scope.mActivityPreDefineRespond=angular.copy(SelectedData);
			$scope.ActivityPredefineDaFilterThing(SelectedData.ActivityTypeId);
			//$scope.ActivityPredefineRespondEnableTheDetails=true;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
			$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=true;
			$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=true;
			if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
			{
				$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
			}
			else
			{
				$scope.ActivityPredefineRespondDaTipeTugasIlang=true;
			}
			
			if($scope.mActivityPreDefineRespond.ParentId==0)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 1";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond==null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0&&$scope.mActivityPreDefineRespond.EndRespond!=null&&$scope.mActivityPreDefineRespond.ParentNameAnak!=null&&($scope.mActivityPreDefineRespond.ParentNameAnak!=$scope.mActivityPreDefineRespond.ParentName))
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentNameEngkong==null && $scope.mActivityPreDefineRespond.ParentNameAnak==$scope.mActivityPreDefineRespond.ParentName && $scope.mActivityPreDefineRespond.ParentNameAnak!=null && $scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond!=null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 2";
			}
			else if($scope.mActivityPreDefineRespond.ParentId!=0 && $scope.mActivityPreDefineRespond.EndRespond!=null && $scope.mActivityPreDefineRespond.ParentNameEngkong!=null)
			{
				$scope.ActivityPredefineRespondCurrentLevel="Level 3";
			}
			
        }
	
	$scope.TambahActivityPredefineRespond = function() {
			$scope.ActivityPredefineRespondEndRespondNoNull=false;
			$scope.OperationActivityPredefineRespond="Insert";
			$scope.ShowActivityPreDefineRespondDetail=true;
			$scope.ActivityPreDefineRespond=false;

			$scope.ShowSimpanActivityPredefineRespond=true;
			// ActivityPreDefineRespond.getData().then(function(res){
				// //$scope.optionsParentRespond = res.data.Result;
				// $scope.optionsParentRespond = res.data.Result.filter(function (type) 
				// { return (type.ActivityTypeId == selected.ActivityTypeId && type.TaskTypeId == selected.TaskTypeId); })
				
				 // return res.data;
			// });

			
            $scope.mActivityPreDefineRespond="";
			$scope.ActivityPredefineDaFilterThing("");
			//$scope.ActivityPredefineRespondEnableTheDetails=true;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=true;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=true;
			$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
			$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=true;
			$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=true;
        }
		
	$scope.TambahActivityPreDefineRespond_DiLevel1 = function(selected) {
			$scope.ActivityPredefineRespondEndRespondNoNull=false;
			$scope.OperationActivityPredefineRespond="Insert";
			$scope.ShowActivityPreDefineRespondDetail=true;
			$scope.ActivityPreDefineRespond=false;

			$scope.ShowSimpanActivityPredefineRespond=true;
			

			
            $scope.mActivityPreDefineRespond="";
			$scope.ActivityPredefineDaFilterThing("");
			
			$scope.mActivityPreDefineRespond={};
			$scope.mActivityPreDefineRespond.ActivityTypeId=selected.ActivityTypeId;
			$scope.mActivityPreDefineRespond.TaskTypeId=selected.TaskTypeId;
			$scope.mActivityPreDefineRespond.ParentId=selected.ActivityPreDefineRespondId;
			
			if(selected.ActivityTypeName=="Others")
			{
				//$scope.mActivityPreDefineRespond.TaskTypeId=5;
				for(var i = 0; i < $scope.optionsActivityTaskTypeRaw.length; ++i)
				{	
					if($scope.optionsActivityTaskTypeRaw[i].ActivityTypeName == "Others"&& $scope.optionsActivityTaskTypeRaw[i].ActivityTypeId==2)
					{
						$scope.mActivityPreDefineRespond.TaskTypeId=angular.copy($scope.optionsActivityTaskTypeRaw[i].TaskTypeId);
						break;
					}
				}
				$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
			}
			
			console.log("setan1",selected);
			console.log("setan2",$scope.mActivityPreDefineRespond);
			//$scope.ActivityPredefineRespondEnableTheDetails=true;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
			$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=true;
			$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=true;
			
			
        }

	$scope.TambahActivityPreDefineRespond_DiLevel2 = function(selected) {
			$scope.ActivityPredefineRespondEndRespondNoNull=true;
			$scope.OperationActivityPredefineRespond="Insert";
			$scope.ShowActivityPreDefineRespondDetail=true;
			$scope.ActivityPreDefineRespond=false;

			$scope.ShowSimpanActivityPredefineRespond=true;
			

			
            $scope.mActivityPreDefineRespond="";
			$scope.ActivityPredefineDaFilterThing("");
			
			$scope.mActivityPreDefineRespond={};
			$scope.mActivityPreDefineRespond.ActivityTypeId=selected.ActivityTypeId;
			$scope.mActivityPreDefineRespond.TaskTypeId=selected.TaskTypeId;
			$scope.mActivityPreDefineRespond.ParentId=selected.ActivityPreDefineRespondId;
			
			if(selected.ActivityTypeName=="Others")
			{
				//$scope.mActivityPreDefineRespond.TaskTypeId=5;
				for(var i = 0; i < $scope.optionsActivityTaskTypeRaw.length; ++i)
				{	
					if($scope.optionsActivityTaskTypeRaw[i].ActivityTypeName == "Others" && $scope.optionsActivityTaskTypeRaw[i].ActivityTypeId==2)
					{
						$scope.mActivityPreDefineRespond.TaskTypeId=angular.copy($scope.optionsActivityTaskTypeRaw[i].TaskTypeId);
						break;
					}
				}
				$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
			}
			
			console.log("setan1",selected);
			console.log("setan2",$scope.mActivityPreDefineRespond);
			//$scope.ActivityPredefineRespondEnableTheDetails=true;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeAktivitas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_TipeTugas=false;
			$scope.ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond=false;
			$scope.ActivityPredefineRespondEnableTheDetails_PredefineRespondName=true;
			$scope.ActivityPredefineRespondEnableTheDetails_EndRespond=true;
			
			
        }
		
	$scope.ActivityPredefineDaFilterThing = function(DaSelected) {
		if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
		{
			$scope.mActivityPreDefineRespond.TaskTypeId=null;
			//$scope.mActivityPreDefineRespond.TaskTypeId=5;
			for(var i = 0; i < $scope.optionsActivityTaskTypeRaw.length; ++i)
			{	
				if($scope.optionsActivityTaskTypeRaw[i].ActivityTypeName == "Others"&& $scope.optionsActivityTaskTypeRaw[i].ActivityTypeId==2)
				{
					$scope.mActivityPreDefineRespond.TaskTypeId=angular.copy($scope.optionsActivityTaskTypeRaw[i].TaskTypeId);
					break;
				}
			}
		}
				
		if($scope.mActivityPreDefineRespond=="")
		{
			ActivityPreDefineRespond.getData("").then(function(res){
				//$scope.optionsParentRespond = res.data.Result;

				$scope.optionsParentRespond = res.data.Result.filter(function (type) 
				{ 
					if($scope.mActivityPreDefineRespond.ActivityTypeId!=2)
					{
						return (type.ActivityTypeId == $scope.mActivityPreDefineRespond.ActivityTypeId && type.TaskTypeId == $scope.mActivityPreDefineRespond.TaskTypeId);
					}
					else if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
					{
						return (type.ActivityTypeId == $scope.mActivityPreDefineRespond.ActivityTypeId);
					} 
				})
				if($scope.optionsParentRespond.length<=0 && $scope.mActivityPreDefineRespond.ActivityTypeId!=2)
				{
					$scope.mActivityPreDefineRespond.ParentId=null;
				}
				
				 return res.data;
			});
		}
		else
		{
			ActivityPreDefineRespond.getDataExceptSelf($scope.mActivityPreDefineRespond.ActivityPreDefineRespondId).then(function(res){
				//$scope.optionsParentRespond = res.data.Result;
				$scope.optionsParentRespond = res.data.Result.filter(function (type) 
				{ 
					if($scope.mActivityPreDefineRespond.ActivityTypeId!=2)
					{
						return (type.ActivityTypeId == $scope.mActivityPreDefineRespond.ActivityTypeId && type.TaskTypeId == $scope.mActivityPreDefineRespond.TaskTypeId);
					}
					else if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
					{
						return (type.ActivityTypeId == $scope.mActivityPreDefineRespond.ActivityTypeId);
					}
				})
				
				if($scope.optionsParentRespond.length<=0 && $scope.mActivityPreDefineRespond.ActivityTypeId!=2)
				{
					$scope.mActivityPreDefineRespond.ParentId=null;
				}
				
				 return res.data;
			});
		}
		
		if($scope.mActivityPreDefineRespond.ActivityTypeId==2)
		{
			$scope.ActivityPredefineRespondDaTipeTugasIlang=false;
		}
		else
		{
			$scope.ActivityPredefineRespondDaTipeTugasIlang=true;
		}
	
		
	}	
		
	$scope.SimpanActivityPredefineRespond = function() {
			if((typeof $scope.mActivityPreDefineRespond.EndRespond=="undefined"))
			{
				$scope.mActivityPreDefineRespond.EndRespond=null;
			}
			
			if($scope.OperationActivityPredefineRespond=="Insert")
			{
				var ActivityPredefineRespondToInsertParent=0;
				if((typeof $scope.mActivityPreDefineRespond.ParentId!="undefined") && $scope.mActivityPreDefineRespond.ParentId!=null)
				{
					ActivityPredefineRespondToInsertParent=$scope.mActivityPreDefineRespond.ParentId;
				}
				else if((typeof $scope.mActivityPreDefineRespond.ParentId=="undefined")||$scope.mActivityPreDefineRespond.ParentId==null)
				{
					ActivityPredefineRespondToInsertParent=0;
				}
				
				$scope.ActivityPredefineRespondToInsert={ 'ActivityTypeId':$scope.mActivityPreDefineRespond.ActivityTypeId,
															'TaskTypeId':$scope.mActivityPreDefineRespond.TaskTypeId,
															'ActivityPreDefineRespondName':$scope.mActivityPreDefineRespond.ActivityPreDefineRespondName,
															'ParentId':ActivityPredefineRespondToInsertParent,
															'EndRespond':$scope.mActivityPreDefineRespond.EndRespond };
				ActivityPreDefineRespond.create($scope.ActivityPredefineRespondToInsert).then(function () {
					//nanti di get
					ActivityPreDefineRespond.getData().then(
						function(res){
							 gridData = [];
							 //$scope.grid.data = res.data.Result; //nanti balikin
							 var tampung=res.data.Result;
							 for(var i = 0; i < tampung.length; ++i)
							{	
								var ParentIdNameThingy=null;
								var ParentIdNameAnakThingy=null;
								if(tampung[i].ParentId!=0 && tampung[i].EndRespond!=null)
								{
									var ParentIdAnak=tampung[i].ParentId;
									ParentIdNameAnakThingy=tampung[i].ParentName;
									
									for(var x = 0; x < tampung.length; ++x)
									{
										if(tampung[x].ActivityPreDefineRespondId==ParentIdAnak )
										{
											for(var y = 0; y < tampung.length; ++y)
											{
												if(tampung[y].ActivityPreDefineRespondId==tampung[x].ParentId)
												{
													ParentIdNameThingy=tampung[y].ActivityPreDefineRespondName;
													break;
												}
												
											}
										}
									}
								}
								else
								{
									ParentIdNameAnakThingy=null;
									ParentIdNameThingy=null;
								}
								tampung[i].ParentNameAnak=ParentIdNameAnakThingy;
								tampung[i].ParentNameEngkong=ParentIdNameThingy;
							}
							 
							 $scope.grid.data = tampung; //nanti balikin
							 
							 bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							 $scope.loading=false;
						 },
						 function(err){
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						 }
					)
				});
				$scope.ActivityPredefineRespondToInsert={};
				$scope.KembaliDariActivityPredefineRespond();
			}
			else if($scope.OperationActivityPredefineRespond=="Update")
			{	
				var ActivityPredefineRespondToUpdateParent=0;
				if((typeof $scope.mActivityPreDefineRespond.ParentId!="undefined") && $scope.mActivityPreDefineRespond.ParentId!=null)
				{
					ActivityPredefineRespondToUpdateParent=$scope.mActivityPreDefineRespond.ParentId;
				}
				else if((typeof $scope.mActivityPreDefineRespond.ParentId=="undefined")||$scope.mActivityPreDefineRespond.ParentId==null)
				{
					ActivityPredefineRespondToUpdateParent=0;
				}
				
				$scope.CategoryProspectVariableToUpdate={ 'ActivityPreDefineRespondId':$scope.mActivityPreDefineRespond.ActivityPreDefineRespondId,
														  'ActivityTypeId':$scope.mActivityPreDefineRespond.ActivityTypeId,
														  'TaskTypeId':$scope.mActivityPreDefineRespond.TaskTypeId,
														  'ActivityPreDefineRespondName':$scope.mActivityPreDefineRespond.ActivityPreDefineRespondName,
														  'ParentId':ActivityPredefineRespondToUpdateParent,
														  'EndRespond':$scope.mActivityPreDefineRespond.EndRespond };
				console.log("setan",$scope.CategoryProspectVariableToUpdate);
				ActivityPreDefineRespond.update($scope.CategoryProspectVariableToUpdate).then(function () {
					ActivityPreDefineRespond.getData().then(
						function(res){
							 gridData = [];
							 //$scope.grid.data = res.data.Result;
							 
							 var tampung=res.data.Result;
							 for(var i = 0; i < tampung.length; ++i)
							{	
								var ParentIdNameThingy=null;
								var ParentIdNameAnakThingy=null;
								if(tampung[i].ParentId!=0 && tampung[i].EndRespond!=null)
								{
									var ParentIdAnak=tampung[i].ParentId;
									ParentIdNameAnakThingy=tampung[i].ParentName;
									
									for(var x = 0; x < tampung.length; ++x)
									{
										if(tampung[x].ActivityPreDefineRespondId==ParentIdAnak )
										{
											for(var y = 0; y < tampung.length; ++y)
											{
												if(tampung[y].ActivityPreDefineRespondId==tampung[x].ParentId)
												{
													ParentIdNameThingy=tampung[y].ActivityPreDefineRespondName;
													break;
												}
												
											}
										}
									}
								}
								else
								{
									ParentIdNameAnakThingy=null;
									ParentIdNameThingy=null;
								}
								tampung[i].ParentNameAnak=ParentIdNameAnakThingy;
								tampung[i].ParentNameEngkong=ParentIdNameThingy;
							}
							 
							 $scope.grid.data = tampung; //nanti balikin
							 
							 bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							 $scope.loading=false;
						 },
						 function(err){
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						 }
					)
				});
				$scope.KembaliDariActivityPredefineRespond();
			}
			
        }	

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    ActivityPreDefineRespond.getData().then(
        function(res){
             gridData = [];
			 var tampung=res.data.Result;
			 for(var i = 0; i < tampung.length; ++i)
			{	
				var ParentIdNameThingy=null;
				var ParentIdNameAnakThingy=null;
				if(tampung[i].ParentId!=0 && tampung[i].EndRespond!=null)
				{
					var ParentIdAnak=tampung[i].ParentId;
					ParentIdNameAnakThingy=tampung[i].ParentName;
					
					for(var x = 0; x < tampung.length; ++x)
					{
						if(tampung[x].ActivityPreDefineRespondId==ParentIdAnak )
						{
							for(var y = 0; y < tampung.length; ++y)
							{
								if(tampung[y].ActivityPreDefineRespondId==tampung[x].ParentId)
								{
									ParentIdNameThingy=tampung[y].ActivityPreDefineRespondName;
									break;
								}
								
							}
						}
					}
				}
				else
				{
					ParentIdNameAnakThingy=null;
					ParentIdNameThingy=null;
				}
				tampung[i].ParentNameAnak=ParentIdNameAnakThingy;
				tampung[i].ParentNameEngkong=ParentIdNameThingy;
			}
			 
             $scope.grid.data = tampung; //nanti balikin
			 console.log("setan",$scope.grid.data);
			 $scope.loading=false;
         },
         function(err){
             bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)
	
	// $scope.grid = {
            // enableSorting: true,
			// enableColumnResizing: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            // //showTreeExpandNoChildren: true,
            // paginationPageSizes: [10,25,30],
            // paginationPageSize: 10,
			// //data:ActivityPreDefineRespond.getData(),
            // columnDefs: [
                // { name:'Activity PreDefine Respond Id', field:'ActivityPreDefineRespondId', width:'7%', visible:false },
				// { name:'Tipe Aktivitas',  field: 'ActivityTypeName', width: '14%' },
				// { name:'Tipe Tugas',  field: 'TaskTypeName', width: '14%' },
				// { name:'Parent Id',  field: 'ParentId' , visible:false},
				// { name:'Level 1', field:'ActivityPreDefineRespondName' , cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-if="row.entity.ParentId==0">{{row.entity.ActivityPreDefineRespondName}}</label><label ng-if="row.entity.ParentId!=0 && row.entity.EndRespond==null">{{row.entity.ParentName}}</label><label>{{row.entity.ParentNameEngkong}}</label><label ng-if="row.entity.ParentId!=0 && row.entity.EndRespond!=null && row.entity.ParentNameEngkong==null">{{row.entity.ParentName}}</label></p>'},
				// { name:'Level 2',  field: 'ParentAnak1', cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-if="row.entity.ParentId!=0 && row.entity.EndRespond==null">{{row.entity.ActivityPreDefineRespondName}}</label><label ng-if="row.entity.ParentNameAnak!=row.entity.ParentName && row.entity.ParentNameAnak!=null && row.entity.ParentId!=0 && row.entity.EndRespond!=null">{{row.entity.ActivityPreDefineRespondName}}</label><label ng-if="row.entity.ParentNameEngkong!=null && row.entity.ParentNameAnak==row.entity.ParentName && row.entity.ParentNameAnak!=null && row.entity.ParentId!=0 && row.entity.EndRespond!=null">{{row.entity.ParentName}}</label><label ng-if="row.entity.ParentNameEngkong==null && row.entity.ParentNameAnak==row.entity.ParentName && row.entity.ParentNameAnak!=null && row.entity.ParentId!=0 && row.entity.EndRespond!=null">{{row.entity.ActivityPreDefineRespondName}}</label></p>'},
				// { name:'Level 3',  field: 'ParentAnak2',  cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-if="row.entity.ParentId!=0 && row.entity.EndRespond!=null && row.entity.ParentNameEngkong!=null">{{row.entity.ActivityPreDefineRespondName}}</label></p>'},
				// { name: 'Action', field: '', width: '10%', cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-click="grid.appScope.LihatActivityPreDefineRespond(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" aria-hidden="true" ng-click="grid.appScope.UpdateActivityPreDefineRespond(row.entity)" ></i> <i title="Tambah Level 2" class="fa fa-fw fa-lg fa-plus" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-if="row.entity.ParentId==0 && row.entity.EndRespond==null" ng-click="grid.appScope.TambahActivityPreDefineRespond_DiLevel1(row.entity)" ></i> <i title="Tambah Level 3" class="fa fa-fw fa-lg fa-plus" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-if="row.entity.ParentId!=0 && row.entity.EndRespond==null " ng-click="grid.appScope.TambahActivityPreDefineRespond_DiLevel2(row.entity)" ></i>'},
            // ]
        // };
		
	$scope.grid = {
            enableSorting: true,
			enableColumnResizing: true,
            enableRowSelection: true,
            multiSelect: true,
            //enableSelectAll: true,
			//noTabInterference: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
			//data:ActivityPreDefineRespond.getData(),
            columnDefs: [
                { name:'Activity PreDefine Respond Id', field:'ActivityPreDefineRespondId', width:'7%', visible:false },
				{ name:'Tipe Aktivitas',  field: 'ActivityTypeName', width: '14%' },
				{ name:'Tipe Tugas',  field: 'TaskTypeName', width: '14%' },
				{ name:'Parent Id',  field: 'ParentId' , visible:false},
				{ name:'Level 1', field:'ActivityPreDefineRespondNameLevel1'},
				{ name:'Level 2', field:'ActivityPreDefineRespondNameLevel2'},
				{ name:'Level 3', field:'ActivityPreDefineRespondNameLevel3'},
				{ name: 'Action', field: '', width: '10%', cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-click="grid.appScope.LihatActivityPreDefineRespond(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" aria-hidden="true" ng-click="grid.appScope.UpdateActivityPreDefineRespond(row.entity)" ></i> <i title="Tambah Level 2" class="fa fa-fw fa-lg fa-plus" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-if="row.entity.ParentId==0 && row.entity.EndRespond==null" ng-click="grid.appScope.TambahActivityPreDefineRespond_DiLevel1(row.entity)" ></i> <i title="Tambah Level 3" class="fa fa-fw fa-lg fa-plus" style="padding:8px 8px 8px 0px;margin-left:8px; aria-hidden="true" ng-if="row.entity.ParentId!=0 && row.entity.EndRespond==null " ng-click="grid.appScope.TambahActivityPreDefineRespond_DiLevel2(row.entity)" ></i>'},
            ]
        };	
		
	$scope.grid.onRegisterApi = function(gridApi){
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    $scope.ActivityPreDefineRespondSelctedRow = gridApi.selection.getSelectedRows();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                   $scope.ActivityPreDefineRespondSelctedRow = gridApi.selection.getSelectedRows();
                });
				
        };	
		
});
