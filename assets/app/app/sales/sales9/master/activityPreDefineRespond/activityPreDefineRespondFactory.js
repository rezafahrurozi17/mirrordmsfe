angular.module('app')
  .factory('ActivityPreDefineRespond', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/ActivityPreDefineRespondNew/WithLevel')
        return res;//WithLevel
      },
	  getDataExceptSelf: function(ActivityPreDefineRespondId) {
        var res=$http.get('/api/sales/ActivityPreDefineRespondNew/?start=1&limit=10000&filterData=!ActivityPreDefineRespondId|'+ActivityPreDefineRespondId)
        return res;
      },
	  getActivityTypeData: function() {
        var res=$http.get('/api/sales/SDSActivityType')
        return res;
      },
	  getActivityTaskTypeData: function() {
        var res=$http.get('/api/sales/SDSActivityTaskType')
        return res;
      },
	  GetActivityPredefineRespondEndRespondOptions: function () {
        
        var da_json = [
		  { SearchOptionId: "Data berhasil tersimpan!", SearchOptionName: "Data berhasil tersimpan!" },
		  { SearchOptionId: "Input reason (End Task)", SearchOptionName: "Input reason (End Task)" },
        ];
        var res=da_json
        return res;
      },

      create: function(ActivityPreDefineRespond) {
        return $http.post('/api/sales/SDSActivityPreDefineRespondNew', [{
							ActivityTypeId: ActivityPreDefineRespond.ActivityTypeId,
    						TaskTypeId: ActivityPreDefineRespond.TaskTypeId,
    						ActivityPreDefineRespondName: ActivityPreDefineRespond.ActivityPreDefineRespondName,
							ParentId: ActivityPreDefineRespond.ParentId,
							EndRespond: ActivityPreDefineRespond.EndRespond
              }]);
      },

      update: function(ActivityPreDefineRespond){
        return $http.put('/api/sales/SDSActivityPreDefineRespondNew', [{

                ActivityPreDefineRespondId: ActivityPreDefineRespond.ActivityPreDefineRespondId,
                ActivityTypeId: ActivityPreDefineRespond.ActivityTypeId,
                TaskTypeId: ActivityPreDefineRespond.TaskTypeId,
                ActivityPreDefineRespondName: ActivityPreDefineRespond.ActivityPreDefineRespondName,
                ParentId: ActivityPreDefineRespond.ParentId,
                EndRespond: ActivityPreDefineRespond.EndRespond
              }]);
      },

      delete: function(id) {
        return $http.delete('/api/sales/SDSActivityPreDefineRespondNew',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });