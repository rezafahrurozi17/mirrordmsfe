angular.module('app')
    .controller('ProfileBankController', function($scope, $http, CurrentUser, ProfileBank,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        //$scope.loading=true;
		$scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mprofileBank = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        
		$scope.loading=false;
		$scope.optionsAccountType = [{ name: "type1", value: "type1" },{ name: "type2", value: "type2" }];
		$scope.grid.data = ProfileBank.getData();

		
		
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Account Id',    field:'AccountId', width:'7%', visible:false },
            { name:'Account Code', field:'AccountCode' },
            { name:'Account Name',  field: 'AccountName' },
			{ name:'Account User', field:'AccountUser' },
            { name:'Account Type',  field: 'AccountType' },
            { name:'GL Account Code',  field: 'GLAccountCode' },
        ]
    };
});
