angular.module('app')
  .factory('ProfileBank', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var res=[
        {AccountId: "1",  AccountCode: "AccountCode 1",  AccountName: "name1",  AccountUser: "user1",  AccountType: "type1",  GLAccountCode: "cd1"},
        {AccountId: "2",  AccountCode: "AccountCode 2",  AccountName: "name2",  AccountUser: "user2",  AccountType: "type1",  GLAccountCode: "cd2"},
        {AccountId: "3",  AccountCode: "AccountCode 3",  AccountName: "name3",  AccountUser: "user3",  AccountType: "type2",  GLAccountCode: "cd3"},
      ];
        return res;
      },
      create: function(profileBank) {
        return $http.post('/api/fw/Role', [{
                                            NamaBank:profileBank.NamaBank,
                                            }]);
      },
      update: function(profileBank){
        return $http.put('/api/fw/Role', [{
                                            BankId:profileBank.BankId,
											NamaBank: profileBank.NamaBank
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd