angular.module('app')
  .factory('CustomerPreferenceTPFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerPreference');
        //console.log('res=>',res);
        return res;
      },
      create: function(customerPreference) {
        return $http.post('/api/sales/CCustomerPreference', [{
                                            PreferenceName : customerPreference.PreferenceName
                                            }]);
      },
      update: function(customerPreference){
        return $http.put('/api/sales/CCustomerPreference', [{
                                            OutletId : customerPreference.OutletId,
                                            PreferenceId : customerPreference.PreferenceId,
                                            PreferenceName : customerPreference.PreferenceName
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerPreference',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
