angular.module('app')
  .factory('ProfileLeasingSimulationFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileLeasingSimulation');
        //console.log('res=>',res);
        var da_json=[
				{LeasingSimulationId: "1", LeasingSimulationName: "Name 01", LeasingSimulationValue: "Value 01"},
				{LeasingSimulationId: "2", LeasingSimulationName: "Name 02", LeasingSimulationValue: "Value 02"},
        {LeasingSimulationId: "3", LeasingSimulationName: "Name 03", LeasingSimulationValue: "Value 03"},

			  ];
        //res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/MProfileLeasingSimulation', [{
                                            LeasingSimulationName:ActivityChecklistItemAdministrasiPembayaran.LeasingSimulationName,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/MProfileLeasingSimulation', [{
                                            LeasingSimulationId:ActivityChecklistItemAdministrasiPembayaran.LeasingSimulationId,
											OutletId:ActivityChecklistItemAdministrasiPembayaran.OutletId,
											LeasingSimulationName:ActivityChecklistItemAdministrasiPembayaran.LeasingSimulationName,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileLeasingSimulation',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd