angular.module('app')
    .controller('KonfigurasiRegistrationController', function($scope, $http, CurrentUser, KonfigurasiRegistrationFactory,$timeout,bsNotify) {
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        //$scope.gridData=[];
    });
	$scope.ShowKonfigurasiRegistrationMain=true;
	$scope.ShowKonfigurasiRegistrationDetail=false;
	$scope.KonfigurasiRegistrationShowAdvSearch=true;
	$scope.KonfigurasiRegistrationOperation="";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mKonfigurasiRegistration = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={GroupDealerId:null,OutletId:null};
	$scope.disabledKonfigurasiRegistrationSimpan = false;
    //----------------------------------
    // Get Data
    //----------------------------------
		
		KonfigurasiRegistrationFactory.getDataDealerList()
            .then(
                function (res) {
                    var DealerList = [];
                    angular.forEach(res.data.Result, function (value, key) {
                        DealerList.push({ "name": value.GroupDealerName, "value": value.GroupDealerId });
                    });

                    $scope.optionsTambahMasterCAO_Dealer = DealerList;
					$scope.filter.GroupDealerId=$scope.user.GroupDealerId;
					
					KonfigurasiRegistrationFactory.getDataListCabang3($scope.user.GroupDealerId).then(function (rus) {
						$scope.OptionsKonfigurasiRegistrationOutlet = rus.data.Result;
						
					});
					
			$scope.KonfigurasiRegistrationSearchData();
                }
            );
		
		$scope.KonfigurasiRegistrationSearchToggle = function () {
			$scope.KonfigurasiRegistrationShowAdvSearch=!$scope.KonfigurasiRegistrationShowAdvSearch;
		}
		
		$scope.UpdateKonfigurasiRegistration = function (selected) {
			$scope.ShowKonfigurasiRegistrationMain=false;
			$scope.ShowKonfigurasiRegistrationDetail=true;
			$scope.KonfigurasiRegistrationOperation="Update";
			$scope.mKonfigurasiRegistration = {};
			$scope.mKonfigurasiRegistration = angular.copy(selected);
			$scope.mKonfigurasiRegistration.GroupDealerId=$scope.user.GroupDealerId;
			$scope.gridMainKonfigurasiRegistrationChild.data=angular.copy(selected.listDetail);
		}
		
		$scope.LihatKonfigurasiRegistration = function (selected) {
			$scope.ShowKonfigurasiRegistrationMain=false;
			$scope.ShowKonfigurasiRegistrationDetail=true;
			$scope.KonfigurasiRegistrationOperation="Lihat";
			$scope.mKonfigurasiRegistration = {};
			$scope.mKonfigurasiRegistration = angular.copy(selected);
			$scope.mKonfigurasiRegistration.GroupDealerId=$scope.user.GroupDealerId;
			$scope.gridMainKonfigurasiRegistrationChild.data=angular.copy(selected.listDetail);
		}
		
		$scope.KonfigurasiRegistrationValueChanged = function(barang) {
			if(barang.ConfigRegistrationValue==false)
			{
				$scope.gridMainKonfigurasiRegistrationChild.data[$scope.gridMainKonfigurasiRegistrationChild.data.indexOf(barang)].ConfigRegistrationValue = true;
			}
			else if(barang.ConfigRegistrationValue==true)
			{
				$scope.gridMainKonfigurasiRegistrationChild.data[$scope.gridMainKonfigurasiRegistrationChild.data.indexOf(barang)].ConfigRegistrationValue = false;
			}
			console.log("iblis",$scope.gridMainKonfigurasiRegistrationChild.data);
        }
		
		$scope.KembaliKeKonfigurasiRegistrationMasterMain = function () {
			$scope.ShowKonfigurasiRegistrationMain=true;
			$scope.ShowKonfigurasiRegistrationDetail=false;
			$scope.mKonfigurasiRegistration = {};
			
		}
		
		$scope.KonfigurasiRegistrationSimpan = function () {
			$scope.disabledKonfigurasiRegistrationSimpan = true;
			$scope.mKonfigurasiRegistration.listDetail=angular.copy($scope.gridMainKonfigurasiRegistrationChild.data);
			KonfigurasiRegistrationFactory.update($scope.mKonfigurasiRegistration).then(
				function(res) {
					KonfigurasiRegistrationFactory.getData($scope.filter).then(
						function(res){
							gridData = [];
							$scope.gridMainKonfigurasiRegistration.data = res.data.Result;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							$scope.disabledKonfigurasiRegistrationSimpan = false;
							$scope.KembaliKeKonfigurasiRegistrationMasterMain();
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
							$scope.disabledKonfigurasiRegistrationSimpan = false;
						}
					);
				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
							$scope.disabledKonfigurasiRegistrationSimpan = false;
						}
			)
			
		}
		
		$scope.KonfigurasiRegistrationSearchData = function () {
			KonfigurasiRegistrationFactory.getData($scope.filter).then(
				function(res){
					$scope.gridMainKonfigurasiRegistration.data = res.data.Result;
					return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		
	
    var gridData = [];
      // $scope.getData = function() {
		// KonfigurasiRegistrationFactory.getData().then(
			// function(res){
				 // //gridData = [];
				 // $scope.grid.data = res.data.Result; //nanti balikin
				 // $scope.loading=false;
			 // },
			// function(err){
			// bsNotify.show(
						// {
							// title: "gagal",
							// content: "Data tidak ditemukan.",
							// type: 'danger'
						// }
					// );
			// }
		// );
    // };

    //----------------------------------
    // Grid Setup
    //----------------------------------owner tu TAMBit kalo Dealer tu OutletId
    $scope.gridMainKonfigurasiRegistration = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10,25,50],
        paginationPageSize: 10,
        columnDefs: [
            { name:'Cabang',field:'OutletName'},
            { name:'CAO/HO',  field: 'OutletNameCAO' },
			{ name:'Area',field:'AreaName'},
            { name:'Provinsi',  field: 'ProvinceName' },
			{ name:'Kota',field:'CityRegencyName'},
			{ name: 'Action', visible: true, field: '', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.LihatKonfigurasiRegistration(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.UpdateKonfigurasiRegistration(row.entity)" ></i>' },
        ]
    };
	
	$scope.gridMainKonfigurasiRegistrationChild = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
		paginationPageSizes: [10,25,50],
        paginationPageSize: 10,
        //showTreeExpandNoChildren: true,
        
        columnDefs: [
            { name:'Proses',field:'ConfigRegistrationName'},
            { name:'Role',  field: 'RoleName' },
			{ name:'Switch',field:'ConfigRegistrationValue',cellTemplate:'<label class="switch"><input ng-disabled="grid.appScope.KonfigurasiRegistrationOperation==\'Lihat\'" ng-checked="row.entity.ConfigRegistrationValue == true" ng-click="grid.appScope.KonfigurasiRegistrationValueChanged(row.entity)" type="checkbox"><span class="slider round"></span></label>'},
            { displayName:'Role' ,name:'Role 2',  field: 'RoleName2' },
        ]
    };
	

		
	
});
