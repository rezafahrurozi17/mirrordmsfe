angular.module('app')
  .factory('KonfigurasiRegistrationFactory', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var filter = $httpParamSerializer(param);
		var res="";
		if(param.GroupDealerId==null && param.OutletId==null)
		{
			res=$http.get('/api/sales/ConfigRegistration?start=1&limit=10000');
		}
		else
		{
			res=$http.get('/api/sales/ConfigRegistration?start=1&limit=10000&'+filter);
		}
		
        return res;
      },

      getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
          
        return res;
      },

      getDataTipe: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
          
        return res;
      },
	  
	  getDataDealerList: function() {
                var url = '/api/sales/SalesMasterCAO/GetDataDealerList';
                console.log("url Dealer List : ", url);
                var res = $http.get(url);
                return res;
            },
			
	   getDataListCabang3: function(Id) {
                var url = '/api/sales/GetMSitesOutlet/?GroupDealerId=' + Id;
                var res = $http.get(url);
                return res;
            },		

      create: function(KonfigurasiRegistration) {
        return $http.post('/api/sales/PCategoryConfigRegistration', [{
											OutletId:KonfigurasiRegistration.OutletId,
											AccessoriesPackageName:KonfigurasiRegistration.AccessoriesPackageName,
											VehicleModelId:KonfigurasiRegistration.VehicleModelId,
											VehicleTypeId:KonfigurasiRegistration.VehicleTypeId,
											StatusCode :1 ,
											ListMUnitAccessoriesPackageHODetail:KonfigurasiRegistration.ListMUnitAccessoriesPackageHODetail,
											ActiveBit:KonfigurasiRegistration.ActiveBit,
											Price:KonfigurasiRegistration.Price,
											LeadTimeAccessoriesInstallation:KonfigurasiRegistration.LeadTimeAccessoriesInstallation,
                                            }]);
      },
      update: function(KonfigurasiRegistration){
        return $http.put('/api/sales/PCategoryConfigRegistration', [{
                                            AreaId:KonfigurasiRegistration.AreaId,
											AreaName:KonfigurasiRegistration.AreaName,
											CityRegencyId:KonfigurasiRegistration.CityRegencyId,
											CityRegencyName:KonfigurasiRegistration.CityRegencyName,
											LastModifiedDate:KonfigurasiRegistration.LastModifiedDate,
											LastModifiedUserId:KonfigurasiRegistration.LastModifiedUserId,
											OutletId:KonfigurasiRegistration.OutletId,
											OutletIdCAO:KonfigurasiRegistration.OutletIdCAO,
											OutletName:KonfigurasiRegistration.OutletName,
											OutletNameCAO:KonfigurasiRegistration.OutletNameCAO,
											ProvinceId:KonfigurasiRegistration.ProvinceId,
											ProvinceName:KonfigurasiRegistration.ProvinceName,
											StatusCode:KonfigurasiRegistration.StatusCode,
											TotalData:KonfigurasiRegistration.TotalData,
											listDetail:KonfigurasiRegistration.listDetail,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MUnitAccessoriesPackageHODetail/Delete',{data:id,headers: {'Content-Type': 'application/json'}});
		
      },
    }
  });
 //ddd