angular.module('app')
  .factory('ActivityChecklistItemAdministrasiPembayaran', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityChecklistAdminPayment');
        //console.log('res=>',res);
        return res;
      },

      create: function(activityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/MActivityChecklistAdminPayment', [{
            ChecklistAdminPaymentName:activityChecklistItemAdministrasiPembayaran.ChecklistAdminPaymentName
          }]);
      },

      update: function(activityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/MActivityChecklistAdminPayment', [{
            OutletId:activityChecklistItemAdministrasiPembayaran.OutletId,
            ChecklistAdminPaymentId:activityChecklistItemAdministrasiPembayaran.ChecklistAdminPaymentId,
						ChecklistAdminPaymentName:activityChecklistItemAdministrasiPembayaran.ChecklistAdminPaymentName
          }]);
      },

      delete: function(id) {
        return $http.delete('/api/sales/MActivityChecklistAdminPayment',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });