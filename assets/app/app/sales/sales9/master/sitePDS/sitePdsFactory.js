angular.module('app')
    .factory('SitePdsFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MSitesPDS');
                
                return res;
            },
             getDataKelurahan: function() {
                var res = $http.get('/api/sales/MLocationKelurahan');
                return res;
            },
            // getDataPostalCode: function (){
            //     var res = $http.get('/api/sales/MLocationPostalCode');
            //     return res;
            // },
            // getDataCityRegency: function (){
            //     var res = $http.get('/api/sales/MLocationCityRegency');
            //     return res;
            // },
            create: function(sitePds) {
                return $http.post('/api/sales/MSitesPDS', [{
                    PDSName: sitePds.PDSName,
                    PDSAddress: sitePds.PDSAddress,
                    CityRegencyId: sitePds.CityRegencyId,
                    PostalCodeId: sitePds.PostalCodeId,
                }]);
            },
            update: function(sitePds) {
                return $http.put('/api/sales/MSitesPDS', [{
                            OutletId: sitePds.OutletId,
                            PDSId :sitePds.PDSId,
                            PDSName : sitePds.PDSName,
                            PDSAddress : sitePds.PDSAddress,
                            CityRegencyId : sitePds.CityRegencyId,
                            PostalCodeId : sitePds.PostalCodeId,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MSitesPDS', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd