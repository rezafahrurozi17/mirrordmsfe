angular.module('app')
    .controller('SitePdsController', function($scope, $http, CurrentUser, SitePlantAddressFactory, SitePdsFactory, $timeout,bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.msitePds = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];

        SitePdsFactory.getDataKelurahan().then(function (res){
            $scope.kelurahan = res.data.Result;
        })

         SitePlantAddressFactory.getDataCityRegency().then(
            function(res){
                $scope.cityRegency = res.data.Result;
            }
        );

        SitePdsFactory.getDataKelurahan().then(
            function(res){
                $scope.postalCode = res.data.Result;
            }
        );

        $scope.getData = function() {
            SitePdsFactory.getData()
             .then(
                 function(res){
					$scope.grid.data = res.data.Result;

                },
                 function(err){
                    bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
                 }
             );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
		
		$scope.doCustomSave = function (mdl,mode) {

			console.log('doCustomSave3',mode);//create update
			
			if(mode=="create")
			{
				SitePdsFactory.create($scope.msitePds).then(
					function(res) {
						SitePdsFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			else if(mode=="update")
			{
				SitePdsFactory.update($scope.msitePds).then(
					function(res) {
						SitePdsFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
				)
			}
			
			
			$scope.formApi.setMode("grid");
		}
		
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'site pds id', field: 'SitePdsId', width: '7%', visible: false },
                { name: 'Nama Pds', field: 'PDSName' },
                { name: 'Alamat Pds', field: 'PDSAddress' },
                { name: 'Kota / Kabupaten', field: 'CityRegencyName' },
                { name: 'Kode Pos', field: 'PostalCodeName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                }
            ]
        };
    });