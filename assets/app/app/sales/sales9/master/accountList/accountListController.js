angular.module('app')
    .controller('AccountListController', function($scope, $http, CurrentUser, AccountListFactory,AccountGroupFactory,AccountBankListFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAccountList = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
		$scope.optionsaccount_group_id=AccountGroupFactory.getData();
		$scope.optionsbank_id=AccountBankListFactory.getData();
        $scope.grid.data=AccountListFactory.getData();         
        $scope.loading=false;
    },
    function(err){
        bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'account_id', field:'account_id', visible:false },
            { name:'Account Code', field:'account_code'},
            { name:'Account Name', field:'account_name'},
            { name:'coa_ide', field:'coa_id', visible:false },
            { name:'policy_id', field:'policy_id', visible:false },
            { name:'currency_id', field:'currency_id', visible:false },
            { name:'Account Owner', field:'account_owner'},
            { name:'account_group_id', field:'account_group_id', visible:false },
			{ name:'account_group_name', field:'account_group_name' },
            { name:'bank_id', field:'bank_id', visible:false },
			{ name:'bank_name', field:'bank_name' },
            { name:'bank_checking_account_bit', field:'bank_checking_account_bit', visible:false },
            { name:'Bank Checking Account Plafond', field:'bank_checking_account_plafond'},
            { name:'Bank Address', field:'bank_address'},
            { name:'Bank Contact Person Name', field:'bank_contact_person_name'},
            { name:'Bank Contact Person Phone', field:'bank_contact_person_phone'},
            { name:'Bank Contact Person Fax', field:'bank_contact_person_fax'},
            { name:'Bank Contact Person HP', field:'bank_contact_person_hp'},
            { name:'Bank Contact Person Email', field:'bank_contact_person_email'},
            { name:'Description', field:'description'},
            { name:'active_bit', field:'active_bit', visible:false },
            { name:'account_system_bit', field:'account_system_bit', visible:false },
            { name:'operator_id', field:'operator_id', visible:false },
            { name:'input_date', field:'input_date', visible:false }
        ]
    };
});





















