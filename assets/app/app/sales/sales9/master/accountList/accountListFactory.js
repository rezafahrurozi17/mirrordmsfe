angular.module('app')
  .factory('AccountListFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "account_id":"1",
              "account_code":"42",
              "account_name":"Asset",
              "coa_id:":"12",
              "policy_id":"4312",
              "currency_id":"123",
              "account_owner":"",
              "account_group_id":"1",
			  "account_group_name":"Asset",
              "bank_id":"1",
			  "bank_name":"BANK BCA",
              "bank_checking_account_bit":"",
              "bank_checking_account_plafond":"",
              "bank_address":"",
              "bank_contact_person_name":"",
              "bank_contact_person_phone":"",
              "bank_contact_person_fax":"",
              "bank_contact_person_hp":"",
              "bank_contact_person_email":"",
              "description":"",
              "active_bit":"",
              "account_system_bit":"",
              "operator_id":"",
              "input_date":""
          },
          {
             "account_id":"",
              "account_code":"",
              "account_name":"Liability",
              "coa_id:":"",
              "policy_id":"",
              "currency_id":"",
              "account_owner":"",
              "account_group_id":"2",
			  "account_group_name":"Equity",
              "bank_id":"2",
			  "bank_name":"BANK BNI",
              "bank_checking_account_bit":"",
              "bank_checking_account_plafond":"",
              "bank_address":"",
              "bank_contact_person_name":"",
              "bank_contact_person_phone":"",
              "bank_contact_person_fax":"",
              "bank_contact_person_hp":"",
              "bank_contact_person_email":"",
              "description":"",
              "active_bit":"",
              "account_system_bit":"",
              "operator_id":"",
              "input_date":""
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });









