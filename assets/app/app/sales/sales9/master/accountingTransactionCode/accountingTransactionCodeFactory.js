angular.module('app')
  .factory('AccountingTransactionCodeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{TransactionId: "1", TransactionCode: "Code 01", GLTypeDebetId: "d1", GLTypeCreditId: "c1"},
        {TransactionId: "2", TransactionCode: "Code 02", GLTypeDebetId: "d2", GLTypeCreditId: "c2"},
        {TransactionId: "3", TransactionCode: "Code 03", GLTypeDebetId: "d3", GLTypeCreditId: "c1"},
		{TransactionId: "4", TransactionCode: "Code 04", GLTypeDebetId: "d2", GLTypeCreditId: "c2"},
        {TransactionId: "5", TransactionCode: "Code 05", GLTypeDebetId: "d3", GLTypeCreditId: "c2"}		
			  ];
        res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/fw/Role', [{
                                            NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/fw/Role', [{
                                            ChecklistItemAdministrasiPembayaranId:ActivityChecklistItemAdministrasiPembayaran.ChecklistItemAdministrasiPembayaranId,
											NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd