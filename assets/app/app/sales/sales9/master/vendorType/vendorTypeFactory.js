angular.module('app')
  .factory('VendorTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTypeVendor');
        return res;
      },
      create: function(VendorType) {
        return $http.post('/api/sales/PCategoryTypeVendor', [{
                                            VendorTypeName : VendorType.VendorTypeName,
                                            }]);
      },
      update: function(VendorType){
        return $http.put('/api/sales/PCategoryTypeVendor', [{
                                            VendorTypeId : VendorType.VendorTypeId,
											OutletId : VendorType.OutletId,
											VendorTypeName : VendorType.VendorTypeName,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTypeVendor',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });