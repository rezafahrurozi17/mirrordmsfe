angular.module('app')
  .factory('StampCost', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryStampCost');
        //console.log('res=>',res);
// var da_json=[
//         {StampCostId: "1",  StampValue: "1",  MinimumTransaction: "1000",  MaximumTransaction: "1000",  ValidDate: new Date("2/24/2017")},
//         {StampCostId: "2",  StampValue: "2",  MinimumTransaction: "1000",  MaximumTransaction: "1000",  ValidDate: new Date("2/24/2017")},
//         {StampCostId: "3",  StampValue: "3",  MinimumTransaction: "1000",  MaximumTransaction: "1000",  ValidDate: new Date("2/24/2017")},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(stampCost) {
        return $http.post('/api/sales/PCategoryStampCost', [{
                                            //AppId: 1,
                                            StampValue: stampCost.StampValue,
											                      MinimumTransaction: stampCost.MinimumTransaction,
										                      	MaximumTransaction: stampCost.MaximumTransaction,
                                            ValidDate: stampCost.ValidDate}]);
      },
      update: function(stampCost){
        return $http.put('/api/sales/PCategoryStampCost', [{
                                            OutletId : stampCost.OutletId, 
                                            StampCostId: stampCost.StampCostId,
                                            StampValue: stampCost.StampValue,
											                      MinimumTransaction: stampCost.MinimumTransaction,
                                            MaximumTransaction: stampCost.MaximumTransaction,
                                            ValidDate: stampCost.ValidDate}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryStampCost',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd