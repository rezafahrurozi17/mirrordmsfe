angular.module('app')
  .factory('ActivityAlasanBatalPembelianKendaraanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
       var res=$http.get('/api/sales/MActivityReasonCancelBuy');
        return res;
      },

      create: function(ActivityAlasanBatalPembelianKendaraan) {
        return $http.post('/api/sales/MActivityReasonCancelBuy', [{
            ReasonCancelBuyName: ActivityAlasanBatalPembelianKendaraan.ReasonCancelBuyName}]);
      },
      
      update: function(ActivityAlasanBatalPembelianKendaraan){
        return $http.put('/api/sales/MActivityReasonCancelBuy', [{
                                            OutletId: ActivityAlasanBatalPembelianKendaraan.OutletId,
                                            ReasonCancelBuyId: ActivityAlasanBatalPembelianKendaraan.ReasonCancelBuyId,
                                            ReasonCancelBuyName: ActivityAlasanBatalPembelianKendaraan.ReasonCancelBuyName}]);
      },

       delete: function(id) {
        return $http.delete('/api/sales/MActivityReasonCancelBuy',{data:id,headers: {'Content-Type': 'application/json'}});
      },      
    }
  });