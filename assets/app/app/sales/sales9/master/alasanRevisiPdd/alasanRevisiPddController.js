angular.module('app')
    .controller('AlasanRevisiPddController', function($scope, $http, CurrentUser, AlasanRevisiPddFactoryMaster,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAlasanRevisiPdd = null; //Model
    $scope.xRole={selected:[]};
    $scope.countClick = 0;

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
        AlasanRevisiPddFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    $scope.onShowDetail = function(){
        $scope.countClick = 0;
    }
    $scope.onValidateSave = function(data){
        if($scope.countClick == 0){
            $scope.countClick++;
            return data
        }

    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Alasan Revisi Id',    field:'ReasonRevisionId', width:'7%', visible:false},
            { name:'Alasan Revisi', field:'ReasonRevisionName' }
        ]
    };
});


