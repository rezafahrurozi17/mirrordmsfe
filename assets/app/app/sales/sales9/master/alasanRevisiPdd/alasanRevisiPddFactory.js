angular.module('app')
  .factory('AlasanRevisiPddFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryReasonRevision');
        return res;
      },
      create: function(AlasanRevisiPdd) {
        return $http.post('/api/sales/PCategoryReasonRevision', [{
                                            //AppId: 1,
											ReasonRevisionName: AlasanRevisiPdd.ReasonRevisionName,
                                            LimitRevision: AlasanRevisiPdd.LimitRevision}]);
      },
      update: function(AlasanRevisiPdd){
        return $http.put('/api/sales/PCategoryReasonRevision', [{
                                            ReasonRevisionId: AlasanRevisiPdd.ReasonRevisionId,
											ReasonRevisionName: AlasanRevisiPdd.ReasonRevisionName,
                                            LimitRevision: AlasanRevisiPdd.LimitRevision}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryReasonRevision',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd