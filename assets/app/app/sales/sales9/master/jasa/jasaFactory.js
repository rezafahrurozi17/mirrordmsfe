angular.module('app')
  .factory('JasaFactory', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        
		var res=$http.get('/api/sales/AdminHandlingJasa');
        return res;
      },
	  
	  getDataTipeJasa: function() {
        var res=$http.get('/api/sales/PCategoryMaterialType');
          
        return res;
      },
    
      getDataModel: function() {
        // var res=$http.get('/api/sales/MUnitVehicleModelTomas');
        var res=$http.get('/api/sales/MUnitVehicleModelMasterJasa');
        return res;
      },
      
      getDataTipe: function(VehicleModelId,JasaId) {
        // var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
        var res=$http.get('/api/sales/MUnitVehicleTypeMasterJasa/?VehicleModelId='+VehicleModelId +'&JasaId='+JasaId); 
        return res;
      },
	  
	  getDataChild: function(JasaId) {
        var res=$http.get('/api/sales/AdminHandlingJasa/Lihat?JasaId='+JasaId);
          
        return res;
      },

      create: function(Jasa) {
        return $http.post('/api/sales/AdminHandlingJasa', [Jasa]);
      },
      update: function(Jasa){
        return $http.put('/api/sales/AdminHandlingJasa', [Jasa]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingJasa',{data:id,headers: {'Content-Type': 'application/json'}});
		
      },
    }
  });
 //ddd