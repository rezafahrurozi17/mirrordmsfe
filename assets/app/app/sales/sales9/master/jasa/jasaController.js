
angular.module('app')
    .controller('JasaController', function($scope, $http, CurrentUser, JasaFactory,$timeout,bsNotify) {
	$scope.mJasa=null;
	
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        //$scope.gridData=[];
    });
	$scope.ShowMasterJasaMain=true;
	$scope.ShowMasterJasaDetail=false;
	
	$scope.MasterJasaOperation="";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mJasa = null; //Model
	$scope.xRole={selected:[]};
	$scope.disabledMasterJasaSimpan = false;

    //----------------------------------
    // Get Data
    //----------------------------------
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalJasaVehicleTypeChild').remove();
		});
		
		JasaFactory.getDataTipeJasa().then(function(res){
			var MasterJasaRawTipeJasa=res.data.Result
			var MasterJasaFilteredTipeJasa=[{MaterialTypeId:null,MaterialTypeName: null}];
			MasterJasaFilteredTipeJasa.splice(0,1);
			
			for(var i = 0; i < MasterJasaRawTipeJasa.length; ++i)
			{	
				if(MasterJasaRawTipeJasa[i].MaterialTypeName=="Jasa" || MasterJasaRawTipeJasa[i].MaterialTypeName=="Ekspedisi" || MasterJasaRawTipeJasa[i].MaterialTypeName=="Biro Jasa" || MasterJasaRawTipeJasa[i].MaterialTypeName=="OPD")
				{
					MasterJasaFilteredTipeJasa.push({MaterialTypeId:MasterJasaRawTipeJasa[i].MaterialTypeId,MaterialTypeName: MasterJasaRawTipeJasa[i].MaterialTypeName});
				}
			}
			
			$scope.optionsMasterJasaTipeJasa = angular.copy(MasterJasaFilteredTipeJasa);
			//return $scope.optionsMasterJasaTipeJasa;
		});
	    
        JasaFactory.getDataModel().then(function(res){
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });

        // JasaFactory.getDataTipe().then(function(res){
        //     $scope.Jasa_Semua_VehicleType = res.data.Result;
        // });
		
		$scope.HapusJasa = function(){
			var length = $scope.selectedRows.length;
			JasaFactory.delete($scope.selectedRows).then(
				function (res) {
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
					$scope.selectedRows = [];
					$scope.getData();
				
			})
		}

        $scope.SelectedModel = function (DataModel) {
			var JasaId = null;
			JasaId = $scope.copyJasaId;
			JasaFactory.getDataTipe(DataModel.VehicleModelId,JasaId).then(function(res){
				$scope.Jasa_Semua_VehicleType = res.data.Result;
				$scope.TipeFilteredBuatGridModalJasa = $scope.Jasa_Semua_VehicleType.filter(function(type) { return (type.VehicleModelId == DataModel.VehicleModelId); });
			
				if($scope.TipeFilteredBuatGridModalJasa.length<=0)
				{
					$scope.TipeFilteredBuatGridModalJasa = $scope.Jasa_Semua_VehicleType.filter(function(type) { return (type.VehicleModelId == DataModel); });
				}
				for(var i = 0; i < $scope.TipeFilteredBuatGridModalJasa.length; ++i)
				{
					$scope.TipeFilteredBuatGridModalJasa[i].VehicleModelName=DataModel.VehicleModelName;
				}
				
				$scope.gridModalJasaDaAksesoris.data=$scope.TipeFilteredBuatGridModalJasa;
			});
        }
		
		$scope.SelectJasaVehicleTypeChild = function () {
			//console.log("iblis",$scope.Rows_Selected_gridModalJasaDaAksesoris);
			//$scope.gridJasaDaAksesoris.data=[];
			//$scope.gridJasaDaAksesoris.data=angular.copy($scope.Rows_Selected_gridModalJasaDaAksesoris);
			
			var aman=true;
			
			if($scope.gridJasaDaAksesoris.data!=null && $scope.gridJasaDaAksesoris.data.length>=1 && (typeof $scope.gridJasaDaAksesoris.data!="undefined"))
			{
				for(var x = 0; x < $scope.Rows_Selected_gridModalJasaDaAksesoris.length; ++x)
				{
					for(var y = 0; y < $scope.gridJasaDaAksesoris.data.length; ++y)
					{
						if($scope.Rows_Selected_gridModalJasaDaAksesoris[x].VehicleTypeId==$scope.gridJasaDaAksesoris.data[y].VehicleTypeId)
						{
							aman=false;
						}
					}
				}
			}
			
			if(aman==true)
			{
				for(var z = 0; z < $scope.Rows_Selected_gridModalJasaDaAksesoris.length; ++z)
				{	
					$scope.gridJasaDaAksesoris.data.push($scope.Rows_Selected_gridModalJasaDaAksesoris[z]);
				}
			}
			if(aman==false)
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Tolong pastikan data tidak duplikat.",
							type: 'warning'
						}
					);
			}
			angular.element('.ui.modal.ModalJasaVehicleTypeChild').modal('hide');
		}
		
		$scope.AddJasaMaster = function () {
			$scope.copyJasaId = null;
			try
			{
				$scope.gridJasaDaAksesoris.data=[];
			}
			catch(ExceptionComment)
			{
			}
			//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
			//$scope.gridJasaDaAksesoris.data.splice(0,1);
			$scope.ShowMasterJasaMain=false;
			$scope.ShowMasterJasaDetail=true;
			$scope.MasterJasaOperation="Insert";
			$scope.mJasa = {};
		}
		
		$scope.LihatMasterJasa = function (rows) {
			try
			{
				$scope.gridJasaDaAksesoris.data=[];
			}
			catch(ExceptionComment)
			{
			}
			//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
			//$scope.gridJasaDaAksesoris.data.splice(0,1);
			$scope.ShowMasterJasaMain=false;
			$scope.ShowMasterJasaDetail=true;
			$scope.MasterJasaOperation="Lihat";
			//$scope.mJasa=angular.copy(rows);
			
			JasaFactory.getDataChild(rows.JasaId).then(function(res){
				$scope.mJasa=angular.copy(res.data.Result[0]);
				$scope.gridJasaDaAksesoris.data = $scope.mJasa.JasaVehicleTypeView;
			});

		}
		
		$scope.UpdateMasterJasa = function (rows) {
			try
			{
				$scope.gridJasaDaAksesoris.data=[];
			}
			catch(ExceptionComment)
			{
			}
			//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
			//$scope.gridJasaDaAksesoris.data.splice(0,1);
			$scope.ShowMasterJasaMain=false;
			$scope.ShowMasterJasaDetail=true;
			$scope.MasterJasaOperation="Update";
			
			console.log("Edit ?")
			$scope.copyJasaId = rows.JasaId;
			JasaFactory.getDataChild(rows.JasaId).then(function(res){
				$scope.mJasa=angular.copy(res.data.Result[0]);
				$scope.gridJasaDaAksesoris.data = $scope.mJasa.JasaVehicleTypeView;
			});

		}
		
		$scope.KembaliKeJasaMasterMain = function () {
			$scope.ShowMasterJasaMain=true;
			$scope.ShowMasterJasaDetail=false;
			$scope.mJasa = {};
		}

		$scope.DeleteMasterJasaChild = function (row) {
			$scope.gridJasaDaAksesoris.data.splice($scope.gridJasaDaAksesoris.data.indexOf(row),1);
			}
        
	
    var gridData = [];
      $scope.getData = function() {
		JasaFactory.getData().then(
			function(res){
				 //gridData = [];
				 $scope.grid.data = res.data.Result; //nanti balikin
				 $scope.loading=false;
			 },
			function(err){
			bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
			}
		);
			
		
		
    };
		
		$scope.MasterJasaSimpan = function () {
			$scope.tampung_tabelNew = [];
			$scope.disabledMasterJasaSimpan = true;
			var tampung_tabel=angular.copy($scope.gridJasaDaAksesoris.data);
			for (var i in $scope.gridJasaDaAksesoris.data){
				if($scope.MasterJasaOperation=="Insert"){
					$scope.tampung_tabelNew.push({
						VehicleTypeId : $scope.gridJasaDaAksesoris.data[i].VehicleTypeId
					});
				}else{
					if($scope.gridJasaDaAksesoris.data[i].JasaVehicleTypeId != undefined){
						$scope.tampung_tabelNew.push($scope.gridJasaDaAksesoris.data[i]);
					}else{
						$scope.tampung_tabelNew.push({
							VehicleTypeId : $scope.gridJasaDaAksesoris.data[i].VehicleTypeId
						});
					}
				}
			}
			
			console.log("cek data", $scope.tampung_tabelNew)
			$scope.mJasa.JasaVehicleTypeView=angular.copy($scope.tampung_tabelNew);
			delete $scope.mJasa.VehicleModelId; //untuk hapus object
			console.log("iblis",$scope.mJasa);
			
			if($scope.MasterJasaOperation=="Insert")
			{
				JasaFactory.create($scope.mJasa).then(
					function(res) {
						JasaFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.KembaliKeJasaMasterMain();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
							}
				)
			}
			else if($scope.MasterJasaOperation=="Update")
			{
				JasaFactory.update($scope.mJasa).then(
					function(res) {
						JasaFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.KembaliKeJasaMasterMain();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledMasterJasaSimpan = false;
							}
				)
			}

		}
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    
    
	$scope.AddJasaVehicleTypeChild = function () {
		$scope.Rows_Selected_gridModalJasaDaAksesoris=[];
		$scope.gridApiJasaDaAksesorisModal.selection.clearSelectedRows();
		$scope.mJasa.VehicleModelId = null;
		$scope.gridModalJasaDaAksesoris.data = [];
		angular.element('.ui.modal.ModalJasaVehicleTypeChild').modal('setting',{closable:false}).modal('show');
		angular.element('.ui.modal.ModalJasaVehicleTypeChild').not(':first').remove();
    }
	
	$scope.BatalLookupModalJasaVehicleTypeChild = function () {
		angular.element('.ui.modal.ModalJasaVehicleTypeChild').modal('hide');
    }

	
    //----------------------------------
    // Grid Setup
    //----------------------------------owner tu TAMBit kalo Dealer tu OutletId
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Kode Jasa',field:'JasaCode'},
            { name:'Nama Jasa',  field: 'JasaName' },
			{ name:'Tipe Jasa',  field: 'MaterialTypeName' },
			{ name: 'Action', visible: true, field: '', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatMasterJasa(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateMasterJasa(row.entity)" ></i>' },
        ]
    };
	
	$scope.gridModalJasaDaAksesoris = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
			enableColumnMenus: false,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName',enableHiding : false},
                { name: 'Tipe', field: 'Description' ,enableHiding : false,cellTemplate:'<p style="font-size:14px;margin:5px 0 0 5px"><label>{{row.entity.Description}} {{row.entity.KatashikiCode}} {{row.entity.SuffixCode}}</label></p>'},
				
                
            ]
        };
		
	$scope.gridModalJasaDaAksesoris.onRegisterApi = function(gridApi){
			$scope.gridApiJasaDaAksesorisModal = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope,function(row){
				$scope.Rows_Selected_gridModalJasaDaAksesoris = gridApi.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
				$scope.Rows_Selected_gridModalJasaDaAksesoris = gridApi.selection.getSelectedRows();
			});
		};
		
	$scope.gridJasaDaAksesoris = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
			enableColumnMenus: false,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName',enableHiding : false},
				{ name: 'Tipe', field: 'Description', width: '53%' ,enableHiding : false,cellTemplate:'<p style="font-size:14px;margin:5px 0 0 5px"><label>{{row.entity.Description}} {{row.entity.KatashikiCode}} {{row.entity.SuffixCode}}</label></p>'},
				{ name: 'Action', visible: true, field: '', width: '15%', cellTemplate:'<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.MasterJasaOperation==\'Lihat\'" ng-click="grid.appScope.DeleteMasterJasaChild(row.entity)"/i>'}
                
            ]
        };
		
	$scope.gridJasaDaAksesoris.onRegisterApi = function(gridApi){
			$scope.gridApiJasaDaAksesoris = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope,function(row){
				$scope.Rows_Selected_gridJasaDaAksesoris = gridApi.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
				$scope.Rows_Selected_gridJasaDaAksesoris = gridApi.selection.getSelectedRows();
			});
		};
});
