angular.module('app')
  .factory('KaroseriFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MPartsKaroseriForAdminHO');
        return res;
      },
      getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
          
        return res;
      },
      getDataTipe: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
          
        return res;
      },
      getDataChild: function(KaroseriId) {
        var res=$http.get('/api/sales/MPartsKaroseriForAdminHO/Detail?KaroseriId='+KaroseriId);
          
        return res;
      },
      create: function(karoseri) {
        return $http.post('/api/sales/MPartsKaroseriForAdminHO', [{
                                            //AppId: 1,
                                            KaroseriCode: karoseri.KaroseriCode,
                                            KaroseriName: karoseri.KaroseriName,
                                            OutletId: karoseri.OutletId,
                                            VehicleModelId: karoseri.VehicleModelId,
                                            ListTypeModels: karoseri.ListTypeModels}]);
      },
      update: function(karoseri){
        return $http.put('/api/sales/MPartsKaroseriForAdminHO', [{
                                            KaroseriCode: karoseri.KaroseriCode,
                                            KaroseriName: karoseri.KaroseriName,
                                            OutletId: karoseri.OutletId,
                                            KaroseriId: karoseri.KaroseriId,
                                            VehicleModelId: karoseri.VehicleModelId,
                                            ListTypeModels: karoseri.ListTypeModels}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MPartsKaroseriForAdminHO/Delete',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });