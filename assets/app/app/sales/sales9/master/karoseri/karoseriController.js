angular.module('app')
    .controller('karoseriController', function($scope, $http, CurrentUser, KaroseriFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
	});
	$scope.MasterKaroseriMain=true;
	$scope.MasterKaroseriDetail=false;
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mKaroseri = null; //Model
	$scope.xRole={selected:[]};
	console.log('$scope.user',$scope.user);


    //----------------------------------
    // Get Data test
	//----------------------------------
     $scope.getData = function() {
        KaroseriFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
		);
	}
	KaroseriFactory.getDataModel().then(function(res){
		$scope.optionsModel = res.data.Result;
		return $scope.optionsModel;
	});
	KaroseriFactory.getDataTipe().then(function(res){
		$scope.Karoseri_Semua_VehicleType = res.data.Result;
	});
	$scope.SelectedModelMasterKaroseri = function (DataModel) {
		$scope.TipeFilteredBuatGridModalKaroseri = $scope.Karoseri_Semua_VehicleType.filter(function(type) { return (type.VehicleModelId == DataModel.VehicleModelId); });
		
		if($scope.TipeFilteredBuatGridModalKaroseri.length<=0)
		{
			$scope.TipeFilteredBuatGridModalKaroseri = $scope.Karoseri_Semua_VehicleType.filter(function(type) { return (type.VehicleModelId == DataModel); });
		}
		for(var i = 0; i < $scope.TipeFilteredBuatGridModalKaroseri.length; ++i)
		{
			$scope.TipeFilteredBuatGridModalKaroseri[i].VehicleModelName=DataModel.VehicleModelName;
		}
		
		$scope.gridModalKaroseri.data=$scope.TipeFilteredBuatGridModalKaroseri;

	}
	$scope.AddMasterKaroseri = function () {
		try
		{
			$scope.gridModelKaroseri.data=[];
			$scope.gridModalKaroseri.data=[];
		}
		catch(ExceptionComment)
		{
		}
		//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
		//$scope.gridJasaDaAksesoris.data.splice(0,1);
		$scope.MasterKaroseriMain=false;
		$scope.MasterKaroseriDetail=true;
		$scope.MasterKaroseriOperation="Insert";
		$scope.mKaroseri = {};
	}
	$scope.BatalLookupModalKaroseriVehicleTypeChild = function () {
		angular.element('.ui.modal.gridModalKaroseriVehicle').modal('hide');
	}
	$scope.SelectKaroseriVehicleTypeChild = function () {
		
		var aman=true;
		
		if($scope.gridModelKaroseri.data!=null && $scope.gridModelKaroseri.data.length>=1 && (typeof $scope.gridModelKaroseri.data!="undefined"))
		{
			for(var x = 0; x < $scope.Rows_Selected_gridModalKaroseri.length; ++x)
			{
				for(var y = 0; y < $scope.gridModelKaroseri.data.length; ++y)
				{
					if($scope.Rows_Selected_gridModalKaroseri[x].VehicleTypeId==$scope.gridModelKaroseri.data[y].VehicleTypeId)
					{
						aman=false;
					}
				}
			}
		}
		
		if(aman==true)
		{
			for(var z = 0; z < $scope.Rows_Selected_gridModalKaroseri.length; ++z)
			{	
				$scope.gridModelKaroseri.data.push($scope.Rows_Selected_gridModalKaroseri[z]);
			}
		}
		if(aman==false)
		{
			bsNotify.show(
					{
						title: "Peringatan",
						
						content: "Tolong pastikan data tidak duplikat.",
						type: 'warning'
					}
				);
		}
		angular.element('.ui.modal.gridModalKaroseriVehicle').modal('hide');
	}

	$scope.LihatMasterKaroseri = function (rows) {
		try
		{
			$scope.gridModelKaroseri.data=[];
			$scope.gridModalKaroseri.data=[];
		}
		catch(ExceptionComment)
		{
		}
		//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
		//$scope.gridJasaDaAksesoris.data.splice(0,1);
		$scope.MasterKaroseriMain=false;
		$scope.MasterKaroseriDetail=true;
		$scope.MasterKaroseriOperation="Lihat";
		$scope.mKaroseri=angular.copy(rows);
		
		KaroseriFactory.getDataChild(rows.KaroseriId).then(function(res){
			$scope.mKaroseri=angular.copy(res.data.Result[0]);
			$scope.gridModelKaroseri.data = $scope.mKaroseri.ListTypeModels;
		});

	}
	$scope.KembaliKeKaroseriMasterMain = function () {
		$scope.MasterKaroseriMain=true;
		$scope.MasterKaroseriDetail=false;
		$scope.mKaroseri = {};
	}
	$scope.DeleteMasterKaroseriChild = function (row) {
		$scope.gridModelKaroseri.data.splice($scope.gridModelKaroseri.data.indexOf(row),1);
		}

	$scope.MasterKaroseriSimpan = function () {
			
			var tampung_tabel=angular.copy($scope.gridModelKaroseri.data);
			$scope.mKaroseri.ListTypeModels=angular.copy(tampung_tabel);
			console.log("iblis",$scope.mKaroseri);
			
			if($scope.MasterKaroseriOperation=="Insert")
			{
				KaroseriFactory.create($scope.mKaroseri).then(
					function(res) {
						KaroseriFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.KembaliKeKaroseriMasterMain();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
				)
			}
			else if($scope.MasterKaroseriOperation=="Update")
			{
				KaroseriFactory.update($scope.mKaroseri).then(
					function(res) {
						KaroseriFactory.getData().then(
							function(res){
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.KembaliKeKaroseriMasterMain();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},            
							function(err){
								
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
				)
			}

		}
		$scope.UpdateMasterKaroseri = function (rows) {
			try
			{
				$scope.gridModelKaroseri.data=[];
				$scope.gridModalKaroseri.data=[];
				
			}
			catch(ExceptionComment)
			{
			}
			//$scope.gridJasaDaAksesoris.data=[{"VehicleModelName":null,"Description":null}];
			//$scope.gridJasaDaAksesoris.data.splice(0,1);
			$scope.MasterKaroseriMain=false;
			$scope.MasterKaroseriDetail=true;
			$scope.MasterKaroseriOperation="Update";
			$scope.mKaroseri = {};
			
			KaroseriFactory.getDataChild(rows.KaroseriId).then(function(res){
				$scope.mKaroseri=angular.copy(res.data.Result[0]);

				
				$scope.gridModelKaroseri.data = $scope.mKaroseri.ListTypeModels;

				for(var i = 0; i < $scope.optionsModel.length; ++i)
				{
					if($scope.optionsModel[i].VehicleModelId==$scope.mKaroseri.VehicleModelId)
					{
						$scope.SelectedModelMasterKaroseri($scope.optionsModel[i]);
						break;
					}
				}
			});

		}

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			KaroseriFactory.create($scope.mKaroseri).then(
				function(res) {
					KaroseriFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			KaroseriFactory.update($scope.mKaroseri).then(
				function(res) {
					KaroseriFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	$scope.AddKaroseriVehicleTypeChild = function () {
		$scope.Rows_Selected_gridModalKaroseri=[];
		$scope.gridApiModalKaroseri.selection.clearSelectedRows();
		angular.element('.ui.modal.gridModalKaroseriVehicle').modal('setting',{closable:false}).modal('show');
		angular.element('.ui.modal.gridModalKaroseriVehicle').not(':first').remove();
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Kode Karoseri', field:'KaroseriCode' },
			{ name:'Nama Karoseri', field:'KaroseriName'},
			{ name: 'Action', visible: true, field: '', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatMasterKaroseri(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateMasterKaroseri(row.entity)" ></i>' },
        ]
	};
	$scope.gridModelKaroseri = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableColumnMenus: false,
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		columnDefs: [
			{ name: 'Model', field: 'VehicleModelName',enableHiding : false},
			{ name: 'Tipe', field: 'Description' ,enableHiding : false},
			{ name: 'Action', visible: true, field: '', cellTemplate:'<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.MasterKaroseriOperation==\'Lihat\'" ng-click="grid.appScope.DeleteMasterKaroseriChild(row.entity)"/i>'}
			
		]
	};

		
	$scope.gridModalKaroseri = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableColumnMenus: false,
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		columnDefs: [
			{ name: 'Model', field: 'VehicleModelName',enableHiding : false},
			{ name: 'Tipe', field: 'Description' ,enableHiding : false},
			
			
		]
	};
	
$scope.gridModalKaroseri.onRegisterApi = function(gridApi){
		$scope.gridApiModalKaroseri = gridApi;
		gridApi.selection.on.rowSelectionChanged($scope,function(row){
			$scope.Rows_Selected_gridModalKaroseri = gridApi.selection.getSelectedRows();
		});
		gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
			$scope.Rows_Selected_gridModalKaroseri = gridApi.selection.getSelectedRows();
		});
	};
});