angular.module('app')
  .factory('ChecklistItemAdministrasiFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityChecklistAdminPayment');
        return res;
      },
      create: function(ChecklistItemAdministrasi) {
        return $http.post('/api/sales/MActivityChecklistAdminPayment', [{
                                            //AppId: 1,
                                            ChecklistAdminPaymentName: ChecklistItemAdministrasi.ChecklistAdminPaymentName}]);
      },
      update: function(ChecklistItemAdministrasi){
        return $http.put('/api/sales/MActivityChecklistAdminPayment', [{
											OutletId: ChecklistItemAdministrasi.OutletId,
                                            ChecklistAdminPaymentId: ChecklistItemAdministrasi.ChecklistAdminPaymentId,
                                            ChecklistAdminPaymentName: ChecklistItemAdministrasi.ChecklistAdminPaymentName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityChecklistAdminPayment',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd