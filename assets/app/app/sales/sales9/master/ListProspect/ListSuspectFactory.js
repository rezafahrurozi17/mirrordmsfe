angular.module('app')
  .factory('ListSuspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityChecklistItemDecFactory) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: ActivityChecklistItemDecFactory.Name,
                                            Description: ActivityChecklistItemDecFactory.Description}]);
      },
      update: function(ActivityGroupChecklistItemDecFactory){
        return $http.put('/api/fw/Role', [{
                                            Id: ActivityChecklistItemDecFactory.Id,
                                            //pid: role.pid,
                                            Name: ActivityChecklistItemDecFactory.Name,
                                            Description: ActivityChecklistItemDecFactory.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });