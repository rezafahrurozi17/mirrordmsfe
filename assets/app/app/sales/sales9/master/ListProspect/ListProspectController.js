angular.module('app')
    .controller('ListProspectController', function($scope, $http, CurrentUser, ListProspectFactory,$timeout) {
    //----------------------------------
    // JSON Data
    //---------------------------------
    var data=[
              {
                Id: "1",
                Nama: "Michael E",
				Rate: "Hot",
                NoTlp: "085673991448",
                Type: "Fortuner VRZ 4x2w (Diesel) - Black Mica"
              },
              {
                Id: "2",
                Nama: "Sam Mitchell",
				Rate: "Medium",
                NoTlp: "085673991448",
                Type: "Avanza G 15L - Silver Metallic"
              },
                {
                Id: "3",
                Nama: "Barney Ross",
				Rate: "Low",
                NoTlp: "085673991448",
                Type: "Sienta Q - Super White 2"
              },

    ];
    $scope.list=data;
    
    //----------------------------------
    // Start-Up
    //----------------------------------

    //----------------------------------
    // Initialization
    //---------------------------------

    $scope.user = CurrentUser.user();
    $scope.mListProspect = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
   
    //----------------------------------
    // Grid Setup
    //----------------------------------
    	$scope.intended_operation="None";
	$scope.LookSelected = function (ListProspectFactory) {

	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.ChangeSelected = function (SalesProgramId,SalesProgramName) {
	   $scope.intended_operation="Update_Ops";
	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Batal_Button = true;
	   $scope.Submit_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Batal_Clicked = function () {

	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.intended_operation="None";
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Add_Clicked = function () {
			
		   $scope.mProfileSalesProgram_SalesProgramName=null;
		   $scope.intended_operation="Insert_Ops";
		   
		   $scope.ShowParameter = !$scope.ShowParameter;
		   $scope.ShowTable = !$scope.ShowTable;
		   $scope.Submit_Button = true;
		   $scope.Batal_Button = true;
		   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Simpan_Clicked = function () {
		if($scope.intended_operation=="Update_Ops")
		{
			alert("update");
		}
		else if($scope.intended_operation="Insert_Ops")
		{
			alert("insert");
		}
		
		//clear all input stuff first 
	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	   $scope.intended_operation="None";
	};
		
});

