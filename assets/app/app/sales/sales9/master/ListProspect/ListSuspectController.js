angular.module('app')
    .controller('ListSuspectController', function($scope, $http, CurrentUser, ListSuspectFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
		 $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //---------------------------------

    $scope.user = CurrentUser.user();
    $scope.mListSuspect = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        ListSuspectFactory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                //$scope.grid.data = res.data.Result;
                var data = [
					{
						"ChecklistItemDECId" : 23,
						"GroupChecklistDECId" : 323,  
						"NamaChecklistItemDEC" : "Checklist Item",
						"InputByTAM" : true
					},
					{
						"ChecklistItemDECId" : 323,
						"GroupChecklistDECId" : 2323,  
						"NamaChecklistItemDEC" : "Checklist Items",
						"InputByTAM" : true
					}
				];
				
				$scope.grid.data = data;
				
				//console.log("role=>",res.data.Result);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',                      field:'ChecklistItemDECId', width:'7%', visible:false },
            { name:'group checklist dec id',  field:'GroupChecklistDECId' },
            { name:'nama checklist item dec', field:'NamaChecklistItemDEC' },
			{ name:'input by tam',            field:'InputByTAM' },
        ]
    };
});
