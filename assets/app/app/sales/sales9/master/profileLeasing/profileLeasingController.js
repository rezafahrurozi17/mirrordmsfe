angular.module('app')
	.controller('ProfileLeasingController', function ($scope, $http, CurrentUser, ProfileLeasingFactory, ProfileLeasingFactory, ProfileLeasingTenorFactory, $timeout, bsNotify) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = true;
			$scope.gridData = [];
		});
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.mProfileLeasing = " "; //Model
		
		$scope.xRole = { selected: [] };
		$scope.DropdownProfileLeasingTenor = null;
		$scope.uimode = null;

		$scope.ShowProfileLeasingMain = true;
		$scope.ShowProfileLeasingDetail = false;
		$scope.ProfileLeasingOPeration = "";
		$scope.DuplikatGrid = [];
		// $scope.mProfileLeasing.BisnisUnit = "Leasing";

		$scope.MasterOutlet = [];
		$scope.mProfileLeasingJoin = [];

		// $scope.gridProfileLeasing_TOP = [];
		// $scope.gridProfileLeasing_TOP.data = [];

		//----------------------------------	
		// Get Data
		//----------------------------------
		// $scope.gridProfileLeasing_TOP.data = [
		// 	{ CustomerTypeDesc: "Individu", TopCkd: 12, TopCbu: 24 },
		// 	{ CustomerTypeDesc: "Pemerintah", TopCkd: 12, TopCbu: 24 },
		// 	{ CustomerTypeDesc: "Yayasan", TopCkd: 12, TopCbu: 24 },
		// 	{ CustomerTypeDesc: "Perusahaan", TopCkd: 12, TopCbu: 24 }
		// ];

		var gridData = [];
		$scope.getData = function () {
			ProfileLeasingTenorFactory.getData().then(function (res) {
				gridData = [];
				$scope.OptionsProfileLeasingTenor = res.data.Result;
				$scope.loading = false;
			},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);



			ProfileLeasingFactory.getData().then(function (res) {
				gridData = [];
				$scope.grid.data = res.data.Result;
				$scope.DuplikatGrid = angular.copy($scope.grid.data);
				console.log('duplikat dariii main ===>', $scope.DuplikatGrid);

				$scope.mProfileLeasing.BisnisUnit = "Leasing";
				$scope.loading = false;
			},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}

//--------------------------------------------
// ----------------GET DATA TAMBAHAN----------
// -------------------------------------------
		// $scope.filter = { LeasingCode: null, LeasingName: null, BusinessUnit: null };
		// ProfileLeasingFactory.getDataModel().then(
		// 	function (res) {
		// 		$scope.OptionProfileLeasingVehicleModel = res.data.Result;
		// 		//$scope.loading=false;
		// 		return res.data;
		// 	}
		// );

		$scope.allowPattern = function (event, type, item) {
			var patternRegex
			if (type == 1) {
				patternRegex = /\d/i; //NUMERIC ONLY
			} else if (type == 2) {
				patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
			};
			console.log("event", event);
			var keyCode = event.which || event.keyCode;
			var keyCodeChar = String.fromCharCode(keyCode);
			if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
				event.preventDefault();
				return false;
			};
		};

		ProfileLeasingFactory.getDataBusinessUnit().then(
			function (res) {
				$scope.OptionsProfileLeasingBusinessUnit = res.data.Result;
				console.log('	$scope.OptionsProfileLeasingBusinessUnit ===>', $scope.OptionsProfileLeasingBusinessUnit);
				
				//$scope.loading=false;
				return res.data;
			}
		);

		ProfileLeasingFactory.getDataBank($scope.user.OutletId).then(
			function (res) {
				$scope.OptionsProfileLeasingBank = res.data.Result;
				//$scope.loading=false;
				return res.data;
			}
		);

		ProfileLeasingFactory.getDataTipeVendor().then(
			function (res) {
				$scope.TipeVendorOption = res.data.Result;
				//$scope.loading=false;
				return res.data;
			}
		);
		ProfileLeasingFactory.getDataProvinsi().then(
			function (res) {
				$scope.ProvinsiOption = res.data.Result;
				//$scope.loading=false;
				return res.data;
			}
		);
		ProfileLeasingFactory.getDataPricingArea().then(
			function (res) {
				$scope.OptionsProfileLeasingPricingArea = res.data.Result;
				//$scope.loading=false;
				return res.data;
			}
		);
		ProfileLeasingFactory.getDataJasa().then(
			function (res) {
				$scope.OptionsProfileLeasing_Jasa = res.data.Result;
				//$scope.loading=false;
				return res.data;
			}
		);
		// ----------------------------------------------



		$scope.selectRole = function (rows) {
			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}
		$scope.onSelectRows = function (rows) {
		}

		$scope.RemoveProfileLeasingTenor = function (index) {
			$scope.mProfileLeasing.ListLeasingTenor.splice(index, 1);

		}

		$scope.AddProfileLeasingTenor = function () {
				var AddProfileLeasingTenorChildAman = true;
				if ((typeof $scope.The_Leasing == "undefined")) {
					AddProfileLeasingTenorChildAman = false;
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Anda harus memilih tenor.",
							type: 'warning'
						}
					);
				}

				try {
					for (var i = 0; i < $scope.gridProfileLeasing_Tenor.data.length; ++i) {
						// if ($scope.mProfileLeasing.ListLeasingTenor[i].LeasingTenorId == $scope.The_Leasing.LeasingTenorId) {
						if ($scope.gridProfileLeasing_Tenor.data[i].LeasingTenorTime == $scope.The_Leasing) {
							AddProfileLeasingTenorChildAman = false;
							break;
						}
					}
					if (AddProfileLeasingTenorChildAman == true) {
						// $scope.mProfileLeasing.ListLeasingTenor.push($scope.The_Leasing);

						var setan = { "LeasingTenorTime": $scope.The_Leasing};
						$scope.gridProfileLeasing_Tenor.data.push(setan);
					}
					else {
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Tenor sudah dipilih.",
								type: 'warning'
							}
						);
					}

				}
				catch (err) {
					if (AddProfileLeasingTenorChildAman == true) {
						$scope.gridProfileLeasing_Tenor.data = [{ LeasingTenorTime: null }];
						$scope.gridProfileLeasing_Tenor.data.splice(0, 1);
						var demit = { "LeasingTenorTime": $scope.The_Leasing };
						$scope.gridProfileLeasing_Tenor.data.push(demit);

					}
				}

				$scope.DropdownProfileLeasingTenor = undefined;
				$scope.The_Leasing= null;
			
		}



		$scope.SetSelectedLeasing = function (SelectedLeasing) {
			$scope.The_Leasing = SelectedLeasing;
		}
		
		$scope.ProfileLeasingWarnainTabel = function(setan) {
			var ulala=setan+1;
			if(ulala%2!=0)
			{
				return "ProfileLeasingWarnainYgGanjil";
			}
		}


		//----------------------------------
		// Tambahan
		//----------------------------------


		$scope.SimpanProfileLeasing = function () {

			$scope.mProfileLeasing.ListLeasingTenor = angular.copy($scope.gridProfileLeasing_Tenor.data);
			console.log('$scope.mProfileLeasing.ListLeasingTenor ===>', $scope.mProfileLeasing.ListLeasingTenor);
			$scope.mProfileLeasing.ListInformasiBank = angular.copy($scope.gridProfileLeasing_InformasiBank.data);
			console.log('$scope.mProfileLeasing.ListInformasiBank ===>', $scope.mProfileLeasing.ListInformasiBank);
			$scope.mProfileLeasing.ListOutlet = angular.copy($scope.gridProfileLeasing_Outlet.data);
			console.log('$scope.mProfileLeasing.ListOutlet ===>', $scope.mProfileLeasing.ListOutlet);
			$scope.mProfileLeasing.ListLeasingTOP = angular.copy($scope.gridProfileLeasing_TOP.data);
			console.log('$scope.mProfileLeasing.ListLeasingTOP ===>', $scope.mProfileLeasing.ListLeasingTOP);


			if ($scope.ProfileLeasingOPeration == "Insert") {
				ProfileLeasingFactory.create($scope.mProfileLeasing).then(
					function (res) {
						ProfileLeasingFactory.getData($scope.filter).then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								// $scope.DuplikatGrid = angular.copy($scope.grid.data);
								// console.log('duplikat dariii insert ===>', $scope.DuplikatGrid);
								$scope.loading = false;
								$scope.KembaliDariProfileLeasingDetail();
								$scope.getData();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					},
					function (err) {

						bsNotify.show(
							{
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							}
						);
					}
				)
			}
			else if ($scope.ProfileLeasingOPeration == "Update") {
				// if ($scope.mProfileLeasing.StatusActive == true) {
				// 			$scope.mProfileLeasing.StatusActive = 1;
				// }else{
				// 	$scope.mProfileLeasing.StatusActive = 0;
				// }
				ProfileLeasingFactory.update($scope.mProfileLeasing).then(
					function (res) {
						ProfileLeasingFactory.getData($scope.filter).then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								// $scope.DuplikatGrid = angular.copy($scope.grid.data);
								// console.log('duplikat dariii update ===>', $scope.DuplikatGrid);
								$scope.loading = false;
								$scope.KembaliDariProfileLeasingDetail();
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					},
					function (err) {

						bsNotify.show(
							{
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							}
						);
					}
				)
			}
		}

		
		
		$scope.KembaliDariProfileLeasingDetail = function () {
			$scope.ShowProfileLeasingMain = true;
				$scope.ShowProfileLeasingDetail = false;
				$scope.ProfileLeasingOPeration = "";
				$scope.mProfileLeasing = {};

		}	
		
		$scope.LihatProfileLeasing = function (rows) {


			 
			$scope.gridProfileLeasing_TOP.columnDefs[1].enableCellEdit=false;
			$scope.gridProfileLeasing_TOP.columnDefs[2].enableCellEdit=false;

			console.log('ini isi row selected Lihat==>',rows);
			$scope.mProfileLeasing = angular.copy(rows);
			$scope.mProfileLeasingAwal = angular.copy(rows);

			$scope.gridProfileLeasing_TOP.data = angular.copy(rows.ListLeasingTOP);

			$scope.MasterProfileLeasingDisableUpload = true;
			$scope.ShowProfileLeasingMain = false;
			$scope.ShowProfileLeasingDetail = true;
			$scope.ProfileLeasingOPeration = "Lihat";
			console.log('ProfileLeasingOperation ===>', $scope.ProfileLeasingOPeration);


			ProfileLeasingFactory.getDataOutlet().then(
				function (res) {
					$scope.MasterOutlet = res.data.Result;
					for (var k in $scope.MasterOutlet) {
						$scope.MasterOutlet[k].StatusCode = 0;
					}
					$scope.mProfileLeasing.ListOutlet = $scope.MasterOutlet;

					console.log('mProfileLeasingAwal ====>', $scope.mProfileLeasingAwal);


					for (var i = 0; i < $scope.mProfileLeasing.ListOutlet.length; i++) {
						for (var j = 0; j < $scope.mProfileLeasingAwal.ListOutlet.length; j++) {
							if ($scope.mProfileLeasing.ListOutlet[i].OutletId === $scope.mProfileLeasingAwal.ListOutlet[j].OutletId) {
								$scope.mProfileLeasing.ListOutlet[i].StatusCode = 1;
								console.log('hmm');
							}
						}
					}

					console.log('$scope.mProfileLeasing.ListOutlet ===>', $scope.mProfileLeasing.ListOutlet);

					$scope.gridProfileLeasing_Tenor.data = $scope.mProfileLeasing.ListLeasingTenor;
					$scope.gridProfileLeasing_InformasiBank.data = $scope.mProfileLeasing.ListInformasiBank;
					$scope.gridProfileLeasing_Outlet.data = $scope.mProfileLeasing.ListOutlet;

					return res.data;
				}
			);
		}
			
			$scope.TambahProfileLeasing = function () {
				$scope.MasterProfileLeasingDisableUpload = true;
				$scope.ShowProfileLeasingMain = false;
				$scope.ShowProfileLeasingDetail = true;
				$scope.ProfileLeasingOPeration = "Insert";
	
				console.log('ProfileLeasingOperation ===>', $scope.ProfileLeasingOPeration);
				console.log('mProfileLeasing ===>', $scope.mProfileLeasing);
				$scope.mProfileLeasing = {};
				$scope.mProfileLeasing.BisnisUnit = "Leasing";
				$scope.gridProfileLeasing_InformasiBank.data = [];
				$scope.gridProfileLeasing_Tenor.data = [];
				$scope.gridProfileLeasing_Outlet.data = [];
				$scope.gridProfileLeasing_TOP.data = [];
	
				ProfileLeasingFactory.getDataOutlet().then(
					function (res) {
						$scope.mProfileLeasing.ListOutlet = res.data.Result;

						for (var k in $scope.mProfileLeasing.ListOutlet) {
							$scope.mProfileLeasing.ListOutlet[k].StatusCode = 0;
						}
						$scope.gridProfileLeasing_Outlet.data = $scope.mProfileLeasing.ListOutlet;
						console.log('gridProfileLeasing_Outlet.data ====>',$scope.gridProfileLeasing_Outlet.data);
						return res.data;
					}
				);


				ProfileLeasingFactory.getDataTOP().then(
					function (res) {

						var tempCategory = res.data.Result;
						$scope.MasterTopGetCustomerCategory = tempCategory.filter(function (type)   { return (type.ParentId == 2 || type.CustomerTypeId == 3)});
						$scope.mProfileLeasing.ListTOP_pasTambah = $scope.MasterTopGetCustomerCategory;
						for (var i = 0; i < $scope.mProfileLeasing.ListTOP_pasTambah.length; i++) {
							$scope.mProfileLeasing.ListTOP_pasTambah[i].TOPDayCKD  = 0;
							$scope.mProfileLeasing.ListTOP_pasTambah[i].TOPDayCBU  = 0;
						}
						console.log('$scope.mProfileLeasing.ListTOP;===>', $scope.mProfileLeasing.ListTOP_pasTambah)
						$scope.gridProfileLeasing_TOP.data = $scope.mProfileLeasing.ListTOP_pasTambah;


					}
				);
			}

		$scope.UpdateProfileLeasing = function (rows) {

			$scope.gridProfileLeasing_TOP.columnDefs[1].enableCellEdit = true;
			$scope.gridProfileLeasing_TOP.columnDefs[2].enableCellEdit = true;

			$scope.mProfileLeasingAwal = angular.copy(rows);
			$scope.mProfileLeasing = angular.copy(rows);
			// console.log('ini isi row selected Update==>', $scope.mProfileLeasingAwal);
			$scope.MasterProfileLeasingDisableUpload = true;
			$scope.ShowProfileLeasingMain = false;
			$scope.ShowProfileLeasingDetail = true;
			$scope.ProfileLeasingOPeration = "Update";
			// console.log('ProfileLeasingOperation ===>', $scope.ProfileLeasingOPeration);
			$scope.gridProfileLeasing_TOP.data = angular.copy(rows.ListLeasingTOP);

			$scope.ProfileLeasingProvinsiSelected($scope.mProfileLeasingAwal);

				ProfileLeasingFactory.getDataOutlet().then(
					function (res) {
						for (var i = 0; i < res.data.Result.length; i++){
							res.data.Result[i].StatusCode= 0;
							console.log('didalam looping ===>', res.data.Result[i])
						}

						var MasterOutlet2 = angular.copy(res.data.Result);
						
						console.log('ini yang List Outlet yang sudah 0 dari MasterOutlet ===>', MasterOutlet2);
						
						$scope.mProfileLeasing.ListOutlet = MasterOutlet2;


						
						
						for (var i = 0; i < $scope.mProfileLeasing.ListOutlet.length; i++) {
							for (var j = 0; j < $scope.mProfileLeasingAwal.ListOutlet.length; j++) {
								if ($scope.mProfileLeasing.ListOutlet[i].OutletId === $scope.mProfileLeasingAwal.ListOutlet[j].OutletId) {
									$scope.mProfileLeasing.ListOutlet[i].StatusCode = 1;
								}
							}
						}

						console.log('$scope.mProfileLeasing yang sudah di rubah jd 1 sebagian ===>',$scope.mProfileLeasing);
						
						$scope.gridProfileLeasing_Tenor.data = $scope.mProfileLeasing.ListLeasingTenor;
						$scope.gridProfileLeasing_InformasiBank.data = $scope.mProfileLeasing.ListInformasiBank;
						$scope.gridProfileLeasing_Outlet.data = $scope.mProfileLeasing.ListOutlet;

						return res.data;
						}
				);
				
		}

		$scope.ProfileLeasingProvinsiSelected = function (selected) {
			ProfileLeasingFactory.getDataKota('?start=1&limit=100&filterData=ProvinceId|' + selected.ProvinceId).then(function (res) {
				$scope.ProfileLeasingKota = res.data.Result;
			});
		}


		$scope.DeleteProfileLeasing = function (rows) {
			console.log('rows.LeasingId ====>', rows.LeasingId);
			ProfileLeasingFactory.delete(rows.LeasingId).then(function (res) {
				bsNotify.show(
					{
						title: "Sukses",
						content: "Data berhasil dihapus",
						type: 'success'
					}
				);

			})
		}
		

		$scope.DeleteProfileLeasing_InformationBank = function (row) {
			$scope.gridProfileLeasing_InformasiBank.data.splice($scope.gridProfileLeasing_InformasiBank.data.indexOf(row), 1);
		}

		// =================================GRID SETUP VENDOR JASA================================

		$scope.DeleteProfileLeasing_Outlet = function (row) {
			$scope.gridProfileLeasing_Outlet.data.splice($scope.gridProfileLeasing_Outlet.data.indexOf(row), 1);
		}

		$scope.DeleteProfileLeasing_Tenor = function (row) {
			$scope.gridProfileLeasing_Tenor.data.splice($scope.gridProfileLeasing_Tenor.data.indexOf(row), 1);
		}


		$scope.ModalProfileLeasing_Outlet_Hilang = function () {
			angular.element('.ui.modal.ModalProfileLeasing_Outlet').modal('hide');
			$scope.selctedRowProfileLeasingOutletModal = [];

		}

		$scope.ProfileLeasingOutletPilih = function () {
			var setan = angular.copy($scope.selctedRowProfileLeasingOutletModal);

			for (var i = 0; i < setan.length; ++i) {
				setan[i].OutletName = setan[i].Name;
			}

			for (var x = 0; x < setan.length; ++x) {
				$scope.gridProfileLeasing_Outlet.data.push(setan[x]);
			}

			console.log("setan", $scope.gridProfileLeasing_Outlet.data);
			$scope.ModalProfileLeasing_Outlet_Hilang();

		}

		$scope.TambahProfileLeasing_Outlet = function () {
			setTimeout(function () {
				angular.element('.ui.modal.ModalProfileLeasing_Outlet').modal('setting', { closable: false }).modal('show');
				angular.element('.ui.modal.ModalProfileLeasing_Outlet').not(':first').remove();
			}, 1);
			ProfileLeasingFactory.getDataOutlet().then(
				function (res) {
					$scope.Modal_gridProfileLeasing_Outlet.data = res.data.Result;
					$scope.gridProfileLeasing_Outlet.data = res.data.Result;
					console.log('Modal_gridProfileLeasing_Outlet.data ====>', Modal_gridProfileLeasing_Outlet.data);
					return res.data;
				}
			);
		}



		$scope.TambahProfileLeasing_InformationBank = function () {
			var da_bank_name = "";
			for (var i = 0; i < $scope.OptionsProfileLeasingBank.length; ++i) {
				if ($scope.OptionsProfileLeasingBank[i].BankId == $scope.mProfileLeasing.BankId) {
					da_bank_name = $scope.OptionsProfileLeasingBank[i].BankName;
					break;
				}
			}

			$scope.gridProfileLeasing_InformasiBank.data.push({ "BankId": $scope.mProfileLeasing.BankId, "BankName": da_bank_name, "AccountNumber": $scope.mProfileLeasing.AccountNumber, "AccountName": $scope.mProfileLeasing.AccountName });

			$scope.mProfileLeasing.BankId = "";
			$scope.mProfileLeasing.AccountNumber = "";
			da_bank_name = "";
			$scope.mProfileLeasing.AccountName = "";
		}

		$scope.selectedOutletList = function (selectedRow){
			var index = $scope.gridProfileLeasing_Outlet.data.indexOf(selectedRow);
				// if ($scope.mProfileLeasingOperation == 'update') {

					if (selectedRow.StatusCode == 1) {
						$scope.mProfileLeasing.ListOutlet[index].StatusCode = 0;
						console.log('status codenya dirubah menjadi 0 ===>', index, $scope.mProfileLeasing.ListOutlet[index].StatusCode);
					} else if (selectedRow.StatusCode == 0) {
						$scope.mProfileLeasing.ListOutlet[index].StatusCode = 1;
						console.log('status codenya dirubah menjadi 1 ===>', index, $scope.mProfileLeasing.ListOutlet[index].StatusCode);
					} else {
						console.log('Status code anda tidak valid ===>', selectedRow.StatusCode);		
			 	}
			// }else{
				
			// }
		}


		

		//----------------------------------

		//----------------------------------
		// Grid Setup
		//----------------------------------

	


		$scope.gridProfileLeasing_TOP = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableColumnResizing: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 10,
			columnDefs: [
				{ name: 'Keterangan Pelanggan', field: 'CustomerTypeDesc' },
				{ name: 'TOP CKD', field: 'TOPDayCKD',  cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {return 'canEdit';}, attributes: 'style:"text-align:center"', width:'30%', enableCellEdit: true,enableCellEditOnFocus:true },
				{ name: 'TOP CBU', field: 'TOPDayCBU',  cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {return 'canEdit';}, attributes: 'style:"text-align:center"', width:'30%', enableCellEdit: true,enableCellEditOnFocus:true },
			]
		};


		// $scope.gridProfileLeasing_TOP.onRegisterApi = function (gridApi) {
		// 	$scope.gridApiTOP = gridApi;
		// 	gridApi.selection.on.rowSelectionChanged($scope, function (row) {
		// 		$scope.selectedRowTOP = gridApi.selection.getSelectedRows();
		// 	});
		// 	gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
		// 		$scope.selectedRowTOP = gridApi.selection.getSelectedRows();
		// 	});
		// }



		$scope.gridProfileLeasing_Outlet = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableColumnResizing: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 10,
			columnDefs: [
				{ name: 'Kode Outlet', field: 'OutletCode' },
				{ name: 'Nama Outlet', field: 'Name' },

				{ name: 'Action', field: 'StatusCode', cellTemplate: '<input class="ui-grid-cell-contents" ng-click="grid.appScope.selectedOutletList(row.entity)" ng-disabled="grid.appScope.ProfileLeasingOPeration ==\'Lihat\'" type="checkbox" name="StatusCode"  ng-checked="row.entity.StatusCode == 1">'  },



				
				// { name: 'Action', field: '', cellTemplate: '<i style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.ProfileLeasingOPeration==\'Lihat\'" ng-click="grid.appScope.DeleteProfileLeasing_Outlet(row.entity)" class="fa fa-times" aria-hidden="true"></i>' }
			]
		};

		$scope.gridProfileLeasing_Tenor = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableColumnResizing: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 10,
			columnDefs: [
				// { name: 'ID Tenor', field: 'LeasingTenorId' },
				{ name: 'Waktu Tenor', field: 'LeasingTenorTime' },
				{ name: 'Action', field: '', cellTemplate: '<i style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.ProfileLeasingOPeration==\'Lihat\'" ng-click="grid.appScope.DeleteProfileLeasing_Tenor(row.entity)" class="fa fa-times" aria-hidden="true"></i>' }
			]
		};



		$scope.gridProfileLeasing_InformasiBank = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableColumnResizing: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 10,
			columnDefs: [
				{ name: 'Nama Bank', field: 'BankName' },
				{ name: 'No Rekening', field: 'AccountNumber' },
				{ name: 'Atas Nama Bank', field: 'AccountName' },
				{ name: 'Action', field: '', cellTemplate: '<i style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.ProfileLeasingOPeration==\'Lihat\'" ng-click="grid.appScope.DeleteProfileLeasing_InformationBank(row.entity)" class="fa fa-times" aria-hidden="true"></i>' }
			]
		};

		$scope.gridProfileLeasing_InformasiBank.onRegisterApi = function (gridApi) {
			$scope.gridApiProfileLeasing_InformasiBank = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.selctedRowProfileLeasingInformasiBank = gridApi.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				$scope.selctedRowProfileLeasingInformasiBank = gridApi.selection.getSelectedRows();
			});
		};


		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,
			columnDefs: [
				{ name: 'Leasing Id', field: 'LeasingId', width: '7%', visible: false },
				{ name: 'Nama Leasing', field: 'LeasingName' },
				{ name: 'Status', field: 'StatusActive', enableSorting : false, visible: false, cellTemplate: '<p style="padding:5px 0 0 5px"><label ng-if="!row.entity.StatusActive">Non Aktif</label><label ng-if="row.entity.StatusActive">Aktif</label></p>' },
				{ name: 'List Leasing Tenor', field: 'ListLeasingTenor', visible: false },

				//with button delete master
				// { name: 'Action', visible: true, field: '', width: '12%', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatProfileLeasing(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateProfileLeasing(row.entity)" ></i> <i class="fa fa-fw fa-lg fa-trash" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.DeleteProfileLeasing(row.entity)" ></i>'  },

				{ name: 'Action', visible: true, field: '', width: '12%', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatProfileLeasing(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateProfileLeasing(row.entity)" ></i>' },

				
			]
		};


		$scope.Modal_gridProfileLeasing_Outlet = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableColumnResizing: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 10,
			columnDefs: [
				{ name: 'Kode Outlet', field: 'OutletCode' },
				{ name: 'Nama Outlet', field: 'Name' }
			]
		};



		$scope.Modal_gridProfileLeasing_Outlet.onRegisterApi = function (gridApi) {
			$scope.gridApiProfileLeasing_Outlet = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.selctedRowProfileLeasingOutletModal = gridApi.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				$scope.selctedRowProfileLeasingOutletModal = gridApi.selection.getSelectedRows();
			});
		};


	});



// for (var i in $scope.MasterOutlet) {
// 		for (var k in $scope.mProfileLeasing.ListOutlet) {
// 			if ($scope.mProfileLeasing.ListOutlet[k].LeasingOutletId == $scope.MasterOutlet[i].OutletId) {

// 							$scope.MasterChecklist[i].ListItemInspection[j].checked = true;
// 			}
// 		}

// 	}
// }	