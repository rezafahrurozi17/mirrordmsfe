angular.module('app')
  .factory('ProfileLeasingFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000');
        //console.log('res=>',res);
        return res;
      },
     
      update: function(ProfileLeasingFactory){
        return $http.put('/api/sales/MProfileLeasing', [ProfileLeasingFactory]);
      },

      // update: function (ProfileLeasingFactory) {
      //   return $http.put('/api/sales/MProfileLeasing', [{
      //     LeasingId: ProfileLeasingFactory.LeasingId,
      //     OutletId: ProfileLeasingFactory.OutletId,
      //     LeasingName: ProfileLeasingFactory.LeasingName,
      //     StatusActive: ProfileLeasingFactory.StatusActive,
      //     OfficeAddress: ProfileLeasingFactory.OfficeAddress,
      //     ListLeasingTenor: ProfileLeasingFactory.ListLeasingTenor
      //     //pid: role.pid,
      //   }]);
      // },

      // delete: function(id) {
      //   return $http.delete('/api/sales/MProfileLeasing',{data:id,headers: {'Content-Type': 'application/json'}});
      // },


      create: function (ProfileLeasing) {
        return $http.post('/api/sales/MProfileLeasingForMaster/Insert', [ProfileLeasing]);
      },

      update: function (ProfileLeasing) {
        return $http.put('/api/sales/MProfileLeasingForMaster/Update', [ProfileLeasing]);
      },

      delete: function (LeasingId) {
        // return $http.delete('/api/sales/MProfileLeasingForMaster/Remove',LeasingId);
        return $http.delete('/api/sales/MProfileLeasingForMaster', { data: LeasingId, headers: { 'Content-Type': 'application/json' } });

      },



      //---------------------------------TAMBAHAN DARI VENDOR JASA------------------------------------------
      getDataKodeJasaFilter: function (selectedbisinisid) {
        var res = $http.get('/api/sales/AdminHandlingJasa/?start=1&limit=100000&BusinessUnitId=' + selectedbisinisid);
        return res;
      },


      getDataJasa: function () {
        var res = $http.get('/api/sales/AdminHandlingJasa');
        return res;
      },




      getProfileLeasingDetail: function (LeasingId) {
        var res = $http.get('/api/sales/MProfileLeasingForMaster/?LeasingId=' + LeasingId);
        return res;
      },



      getDataPricingArea: function () {
        var res = $http.get('/api/sales/PESellingPriceAreaPricing');
        return res;
      },
      getDataBusinessUnit: function () {
        var res = $http.get('/api/sales/MVendorBusinessUnit');
        return res;
      },
      getDataOutlet: function () {
        var res = $http.get('/api/sales/MSitesOutlet');
        return res;
      },

      getDataTOP: function () {
        var res = $http.get('/api/sales/CCustomerCategory');
        return res;
      },
      // getDataModel: function () {
      //   var res = $http.get('/api/sales/MUnitVehicleModelTomas');
      //   return res;
      // },
      getDataTipeVendor: function () {
        var res = $http.get('/api/sales/MVendorType');
        return res;
      },

      getDataProvinsi: function () {
        var res = $http.get('/api/sales/MLocationProvince');
        return res;
      },
      getDataKota: function (param) {
        var res = $http.get('/api/sales/MLocationCityRegency' + param);
        return res;
      },

      ValidasiVendorJasa: function (validasi) {
        var res = $http.post('/api/sales/VendorJasa/Validasi', validasi);
        return res;
      },

      getDataBank: function (setan) {
        var res = $http.get('/api/sales/AdminHandlingBank/?start=1&limit=1000000&filterData=OutletId|' + setan);
        return res;
      },

      

     
    }
  });
