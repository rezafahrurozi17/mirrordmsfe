angular.module('app')
  .factory('CustomerListContactFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
            "CustomerDetailId" : 2,
            "CustomerId" : "TC2",
            "Phone" : "089656782319",
            "Address" : "JAKARTA",
            "RTRW" : "002/001",
            "ContactCategoryId" : "TC2",
            "ProvinceId" : "Ganti Ban",
            "CityRegencyId" : "PDC 2",
            "KecamatanId" : "PDC 2",
            "KelurahanId" : "PDC 2",
            "PostalCodeId" : "PDC 2",
            "DefaultBit" : "PDC 2"
          },
          {
            "CustomerDetailId" : 2,
            "CustomerId" : "TC2",
            "Phone" : "089652226615",
            "Address" : "JAKARTA",
            "RTRW" : "002/011",
            "ContactCategoryId" : "TC2",
            "ProvinceId" : "Ganti Ban",
            "CityRegencyId" : "PDC 2",
            "KecamatanId" : "PDC 2",
            "KelurahanId" : "PDC 2",
            "PostalCodeId" : "PDC 2",
            "DefaultBit" : "PDC 2"
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 











