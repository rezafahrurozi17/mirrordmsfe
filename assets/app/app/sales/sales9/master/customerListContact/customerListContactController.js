angular.module('app')
    .controller('CustomerListContactController', function($scope, $http, CurrentUser, CustomerListContactFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCustomerListContact = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=CustomerListContactFactory.getData();         
        $scope.loading=false;
    },
    function(err){
       bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'CustomerDetailId',    field:'CustomerDetailId', width:'7%', visible:false },
            { name:'CustomerId', field:'CustomerId', visible:false },
            { name:'Phone', field:'Phone' },
            { name:'RT/RW', field:'RTRW' },
            { name:'Address', field:'Address' },
            { name:'ContactCategoryId', field:'ContactCategoryId', visible:false },
            { name:'ProvinceId', field:'ProvinceId', visible:false },
            { name:'CityRegencyId', field:'CityRegencyId', visible:false },
            { name:'KecamatanId', field:'KecamatanId', visible:false },
            { name:'KelurahanId', field:'KelurahanId', visible:false },
            { name:'PostalCodeId', field:'PostalCodeId', visible:false },
            { name:'DefaultBit', field:'DefaultBit', visible:false }
        ]
    };
});















