angular.module('app')
  .factory('TipeTugasFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SDSActivityTaskType');
        return res;
      },
      create: function(TipeTugas) {
        return $http.post('/api/sales/SDSActivityTaskType', [{
				ActivityTypeId: 1,
                TaskTypeName: TipeTugas.TaskTypeName}]);
      },
      update: function(TipeTugas){
        return $http.put('/api/sales/SDSActivityTaskType', [{
                TaskTypeId: TipeTugas.TaskTypeId,
				ActivityTypeId: 1,
                TaskTypeName: TipeTugas.TaskTypeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SDSActivityTaskType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });