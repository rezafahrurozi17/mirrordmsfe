angular.module('app')
	.controller('TipeTugasController', function ($scope, $http, CurrentUser, TipeTugasFactory, $timeout, bsNotify) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = true;
			$scope.gridData = [];
		});
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.isEdit = false;
		$scope.mTipeTugas = null; //Model
		$scope.xRole = { selected: [] };
		$scope.mode = null;
		//----------------------------------
		// Get Data test
		//----------------------------------
		$scope.getData = function () {
			TipeTugasFactory.getData().then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$scope.loading = false;

					for (var i = 0; i < $scope.grid.data.length; ++i) {
						if ($scope.grid.data[i].ActivityTypeId == 2) {
							$scope.grid.data.splice(i, 1);
							break;
						}
					}

					//return res.data;
				},
				function (err) {
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		
		$scope.onBulkDeleteTipeTugas = function (ToBeDeleted) {
			//console.log("setan",ToBeDeleted);array ambil TaskTypeId
			var AmanDelete=false;
			for(var i = 0; i < ToBeDeleted.length; ++i)
			{	
				if(ToBeDeleted[i].TaskTypeId<= 5 || ToBeDeleted[i].ActivityTypeName=="Others")
				{
					AmanDelete=false;
					break;
				}
				else
				{
					AmanDelete=true;
				}
			}
			
			if(AmanDelete==true)
			{
				var Da_array=[];
				for(var i = 0; i < ToBeDeleted.length; ++i)
				{
					Da_array.push(ToBeDeleted[i].TaskTypeId);
				}
				
				
				
				TipeTugasFactory.delete(Da_array).then(function () {
					TipeTugasFactory.getData().then(
						function(res){
							$scope.grid.data = res.data.Result;
							for (var i = 0; i < $scope.grid.data.length; ++i) {
									if ($scope.grid.data[i].ActivityTypeId == 2) {
										$scope.grid.data.splice(i, 1);
										break;
									}
								}
							$scope.loading=false;
							//return res.data;
						},
						function (err) {
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);
				});
			}
			else
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Satu/lebih data anda tidak dapat diubah, karena berhubungan dengan fitur lain.",
							type: 'warning'
						}
					);
			}
		}

		$scope.Tambah = function () {
			$scope.isEdit = true;
			$scope.mode = "create";
		}

		$scope.update = function (data) {
			$scope.isEdit = true;
			$scope.mode = "update"
			$scope.mTipeTugas = angular.copy(data);
		}

		$scope.view = function (data) {
			$scope.isEdit = true;
			$scope.mode = "view"
			$scope.mTipeTugas = angular.copy(data);
		}

		$scope.back = function () {
			$scope.isEdit = false;
			$scope.mTipeTugas.TaskTypeName = null;
		}

		function roleFlattenAndSetLevel(node, lvl) {
			for (var i = 0; i < node.length; i++) {
				node[i].$$treeLevel = lvl;
				gridData.push(node[i]);
				if (node[i].child.length > 0) {
					roleFlattenAndSetLevel(node[i].child, lvl + 1)
				} else {

				}
			}
			return gridData;
		}

		$scope.SimpanMasterTipeTugas = function () {
			$scope.loading = false;
			//console.log('SimpanMasterTipeTugas3', mode);//create update

			if ($scope.mode == "create") {
				TipeTugasFactory.create($scope.mTipeTugas).then(
					function (res) {
						TipeTugasFactory.getData().then(
							function (res) {
								gridData = [];
								$scope.formApi.setMode("grid");
								$scope.isEdit = false;
								$scope.grid.data = res.data.Result;
								for (var i = 0; i < $scope.grid.data.length; ++i) {
									if ($scope.grid.data[i].ActivityTypeId == 2) {
										$scope.grid.data.splice(i, 1);
										break;
									}
								}
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								//return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
						$scope.mode = null;
						$scope.mTipeTugas.TaskTypeName = null;
					},
					function (err) {

						if(err.data.Message=="Tipe tugas sudah ada")
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
							else
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data gagal disimpan",
										type: 'danger'
									}
								);
							}
					}
				)
			}
			else if ($scope.mode == "update") {
				if ($scope.mTipeTugas.TaskTypeId <= 5 || $scope.mTipeTugas.ActivityTypeName=="Others") {
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data ini tidak dapat diubah, karena berhubungan dengan fitur lain.",
							type: 'warning'
						}
					);
				}
				else {
					TipeTugasFactory.update($scope.mTipeTugas).then(
						function (res) {
							TipeTugasFactory.getData().then(
								function (res) {
									gridData = [];
									$scope.formApi.setMode("grid");
									$scope.grid.data = res.data.Result;
									for (var i = 0; i < $scope.grid.data.length; ++i) {
										if ($scope.grid.data[i].ActivityTypeId == 2) {
											$scope.grid.data.splice(i, 1);
											break;
										}
									}
									$scope.isEdit = false;
									$scope.loading = false;
									bsNotify.show(
										{
											title: "Sukses",
											content: "Data berhasil disimpan",
											type: 'success'
										}
									);
									//return res.data;
								},
								function (err) {
									bsNotify.show(
										{
											title: "Gagal",
											content: "Data tidak ditemukan.",
											type: 'danger'
										}
									);
								}
							);
							$scope.mode = null;
							$scope.mTipeTugas.TaskTypeName = null;
						},
						function (err) {
							if(err.data.Message=="Tipe tugas sudah ada")
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Sudah ada item dengan nama yang sama",
										type: 'danger'
									}
								);
							}
							else
							{
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data gagal disimpan",
										type: 'danger'
									}
								);
							}
							
						}
					)
				}

			}



		}

		$scope.selectRole = function (rows) {

			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}
		$scope.onSelectRows = function (rows) {
			$scope.DaSelectedTipeTugas = rows;
		}

		var celltemplate = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.view(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
			'<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.update(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'

		$scope.actButtonCaption = {
			new: 'Baru sekali', cancel: 'Kembali dari jakarta'
		}
		//----------------------------------
		// Grid Setup
		//----------------------------------
		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,
			columnDefs: [
				{ name: 'ID Tipe Aktivitas', field: 'ActivityTypeId', width: '7%', visible: false },
				{ name: 'Tipe Aktifitas', field: 'ActivityTypeName' },
				{ name: 'ID Tipe Tugas', field: 'TaskTypeId', visible: false },
				{ name: 'Tipe Tugas', field: 'TaskTypeName' },
				{
					name: 'action',
					allowCellFocus: false,
					visible:true,
					enableFiltering: false,
					width: '10%',
					pinnedRight: true,
					enableColumnMenu: false,
					enableSorting: false,
					enableColumnResizing: true,
					cellTemplate: celltemplate
				}
			]
		};
	});


