angular.module('app')
    .controller('CustomerListController', function($scope, $http, CurrentUser, CustomerListFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCustomerList = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=CustomerListFactory.getData();         
        $scope.loading=false;
    },
    function(err){
       bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'CustomerId', field:'CustomerId', width:'7%', visible:false },
            { name:'CustomerCode', field:'CustomerCode' },
            { name:'CustomerName', field:'CustomerName' },
            { name:'KTPKITAS', field:'KTPKITAS' },
            { name:'SIUP', field:'SIUP' },
            { name:'TDP', field:'TDP' },
            { name:'NPWP', field:'NPWP' },
            { name:'BirthPlace', field:'BirthPlace' },
            { name:'BirthDate', field:'BirthDate' },
            { name:'Handphone1', field:'Handphone1' },
            { name:'Handphone2', field:'Handphone2' },
            { name:'Email', field:'Email' },
            { name:'Job', field:'Job' },
            { name:'Company', field:'Company' },
            { name:'PICName', field:'PICName' },
            { name:'PICHp', field:'PICHp' },
            { name:'CustomerPositionId', field:'CustomerPositionId', visible:false },
            { name:'Department', field:'Department' },
            { name:'MaritalDate', field:'MaritalDate' },
            { name:'TotalPoint', field:'TotalPoint' },
            { name:'CustomerCorrespondentId', field:'CustomerCorrespondentId', visible:false },
            { name:'CustomerGenderId', field:'CustomerGenderId', visible:false },
            { name:'CustomerReligionId', field:'CustomerReligionId', visible:false },
            { name:'CustomerSukuId', field:'CustomerSukuId', visible:false },
            { name:'CustomerLanguageId', field:'CustomerLanguageId', visible:false },
            { name:'CustomerCategoryId', field:'CustomerCategoryId', visible:false },
            { name:'CustomerTypeId', field:'CustomerTypeId', visible:false },
            { name:'RangeGrossIncomeId', field:'RangeGrossIncomeId', visible:false },
            { name:'SectorBusinessId', field:'SectorBusinessId', visible:false },
            { name:'MaritalStatusId', field:'MaritalStatusId', visible:false },
            { name:'InputUserId', field:'InputUserId', visible:false },
            { name:'InputDate', field:'InputDate', visible:false },
            { name:'ModifyUserId', field:'ModifyUserId', visible:false },
            { name:'ModifyDate', field:'ModifyDate', visible:false }
        ]
    };
});