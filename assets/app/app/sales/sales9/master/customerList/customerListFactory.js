angular.module('app')
  .factory('CustomerListFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
            "CustomerId" : 1,
            "CustomerCode" : "C0032",
            "CustomerName" : "Jonny",
            "OriginCode" : "",
            "KTPKITAS" : "10092984928787",
            "SIUP" : "988932",
            "TDP" : "393",
            "NPWP" : "9284273",
            "BirthPlace" : "Jakarta",
            "BirthDate" : "21/02/1980",
            "Handphone1" : "085210109872",
            "Handphone2" : "081234568712",
            "Email" : "xxx@yahoo.co.id",
            "Job" : "Engineer",
            "Company" : "PT XXXX XXXX",
            "PICName" : "Sumarni",
            "PICHp" : "085798120374",
            "CustomerPositionId" : "123",
            "Department" : "Sales",
            "MaritalDate" : "20/03/2010",
            "TotalPoint" : "1092",
            "CustomerCorrespondentId" : "2",
            "CustomerGenderId" : "22",
            "CustomerReligionId" : "12",
            "CustomerSukuId" : "PDC 2",
            "CustomerLanguageId" : "12",
            "CustomerCategoryId" : "12",
            "CustomerTypeId" : "1",
            "RangeGrossIncomeId" : "12",
            "SectorBusinessId" : "32",
            "MaritalStatusId" : "22",
            "InputUserId" : "52",
            "InputDate" : "20/02",
            "ModifyUserId" : "2",
            "ModifyDate" : "20/10/2010",
          },
          {
            "CustomerId" : 2,
            "CustomerCode" : "C0032",
            "CustomerName" : "Gery",
            "OriginCode" : "",
            "KTPKITAS" : "039939393020",
            "SIUP" : "988932",
            "TDP" : "393",
            "NPWP" : "9284273",
            "BirthPlace" : "Bekasi",
            "BirthDate" : "21/02/1980",
            "Handphone1" : "085210109872",
            "Handphone2" : "081234568712",
            "Email" : "xxx@yahoo.co.id",
            "Job" : "Consultant",
            "Company" : "PT XXXX XXXX",
            "PICName" : "Sumarni",
            "PICHp" : "085798120374",
            "CustomerPositionId" : "123",
            "Department" : "Sales",
            "MaritalDate" : "20/03/2010",
            "TotalPoint" : "1092",
            "CustomerCorrespondentId" : "2",
            "CustomerGenderId" : "22",
            "CustomerReligionId" : "12",
            "CustomerSukuId" : "PDC 2",
            "CustomerLanguageId" : "12",
            "CustomerCategoryId" : "12",
            "CustomerTypeId" : "1",
            "RangeGrossIncomeId" : "12",
            "SectorBusinessId" : "32",
            "MaritalStatusId" : "22",
            "InputUserId" : "52",
            "InputDate" : "20/02",
            "ModifyUserId" : "2",
            "ModifyDate" : "20/10/2010",
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 





































