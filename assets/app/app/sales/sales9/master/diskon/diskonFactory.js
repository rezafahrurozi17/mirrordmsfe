angular.module('app')
  .factory('DiskonFactoryMaster', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    var factory = {};
    var debugMode = true;

    function fixDate(date) {
      if (date != null || date != undefined) {
        var fix = date.getFullYear() + '-' +
          ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
          ('0' + date.getDate()).slice(-2) + 'T' +
          ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
          ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
          ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
        return fix;
      } else {
        return null;
      }
    };

    
    return{
      VerifyData: function (KlasifikasiPE, IsiGrid, TypePE) {
        //var url = '/api/fe/IncomingInstruction';
        var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
        var url = '/api/sales/sales9/MasterDiskon/Verify/';
        var param = JSON.stringify(inputData);

        if (debugMode) { console.log('Masuk ke submitData') };
        if (debugMode) { console.log('url :' + url); };
        if (debugMode) { console.log('Parameter POST :' + param) };
        var res = $http.post(url, param);

        return res;
      },

      getData : function (start, limit, filterData) {
        //var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
        // var url = '/api/fe/MasterPEDiskon/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
        if (filterData.TanggalFilterStart !== ''){
          filterData.TanggalFilterStart = fixDate(filterData.TanggalFilterStart);
        }
        if (filterData.TanggalFilterEnd !== ''){
          filterData.TanggalFilterEnd = fixDate(filterData.TanggalFilterEnd);
        }

        var UrlParameter =  "&model=" + filterData.VehicleModelName + 
                            "&tipe=" + filterData.VehicleTypeId + 
                            "&tahun=" + filterData.TahunProduksi + 
                            "&date_from=" + filterData.TanggalFilterStart + 
                            "&date_to=" + filterData.TanggalFilterEnd;

        var url = '/api/sales/sales9/MasterDiskon/SelectData?start=' + start + '&limit=' + limit + UrlParameter;
        var res = $http.get(url);
        return res;
      },

      Submit : function (KlasifikasiPE, IsiGrid, TypePE) {
        //var url = '/api/fe/IncomingInstructionCancel';
        var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
        var url = '/api/sales/sales9/MasterDiskon/Save/';
        var param = JSON.stringify(inputData);

        if (debugMode) { console.log('Masuk ke submitData') };
        if (debugMode) { console.log('url :' + url); };
        if (debugMode) { console.log('Parameter POST :' + inputData) };
        var res = $http.post(url, inputData);

        return res;
      },

      getDaVehicleType: function (param) {
        var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
        return res;
      },

      getYearProspect: function () {
        var res = $http.get('/api/sales/DemandSupplyVehicleYearStock?IncludeCurrentYear=True');
        return res;
      },

      getDataModel: function () {
        var res = $http.get('/api/sales/MUnitVehicleModelTomas');
        //console.log('res=>',res);
        var optionsModel_Json = [{ name: "Avanza", value: "Avanza" }, { name: "Harrier", value: "Harrier" }, { name: "Innova", value: "Innova" }, { name: "Mark X", value: "Mark X" }, { name: "Alphard", value: "Alphard" }, { name: "FT86", value: "FT86" }];
        //var res=optionsModel_Json;	  
        return res;
      },









    }
  });
