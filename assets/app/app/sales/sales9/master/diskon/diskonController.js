var app = angular.module('app');
app.controller('DiskonController', function ($scope, $http, $filter, CurrentUser, bsNotify, DiskonFactoryMaster,$timeout) {
	$scope.optionsKolomFilterSatu_Diskon = [
																																									// {name: "Dealer Name", value:"DealerName"}, 
																																									// {name: "Province", value:"Province"} ,	 
																																									{ name: "Model", value: "VehicleModelName" },
																																									{ name: "Tipe", value: "VehicleTypeName" },
																																									{ name: "Katashiki", value: "KatashikiCode" },
																																									{ name: "Suffix", value: "SuffixCode" },
																																									{ name: "Role", value: "ApproverRoleId" },
																																									{ name: "Pricing Cluster Code", value: "PricingClusterCode" }
																																									// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																								];

	$scope.optionsKolomFilterDua_Diskon = [
																																								// {name: "Dealer Name", value:"DealerName"}, 
																																								// {name: "Province", value:"Province"} ,	 
																																								{ name: "Model", value: "VehicleModelName" },
																																								{ name: "Tipe", value: "VehicleTypeName" },
																																								{ name: "Katashiki", value: "KatashikiCode" },
																																								{ name: "Suffix", value: "SuffixCode" },
																																								{ name: "Role", value: "ApproverRoleId" },
																																								{ name: "Pricing Cluster Code", value: "PricingClusterCode" }
																																								// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																							];

	$scope.optionsComboBulkAction_Diskon = [{ name: "Delete", value: "Delete" }];

	$scope.optionsComboFilterGrid_Diskon = [
																																									{ name: "Model", value: "VehicleModelName" },
																																									{ name: "Tipe", value: "VehicleTypeName" },
																																									{ name: "Katashiki", value: "KatashikiCode" },
																																									{ name: "Suffix", value: "SuffixCode" },
																																									{ name: "Role", value: "ApproverRoleId" },
																																									{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
																																									// { name: "Diskon", value: "Diskon" },
																																									// { name: "Diskon Service Fee", value: "DiskonServiceFee" },
																																									{ name: "Effective Date From", value: "EffectiveDateFrom" },
																																									{ name: "Effective Date To", value: "EffectiveDateTo" },
																																									{ name: "Remarks", value: "Remarks" }
																																								];
																																								
	$scope.btnUpload = "hide";
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'Diskon';
	$scope.cekKolom = '';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_Diskon = false;
	angular.element('#DiskonPriceModal').modal('hide');
	$scope.MasterSellingPriceFU_Diskon_grid = [];
	$scope.MasterSellingPriceFU_Diskon_grid.dataTemp= [];
																																								

	$scope.filterModel = [];
	$scope.filterModel.TahunProduksi = '';

	DiskonFactoryMaster.getDataModel().then(function (res) {
		$scope.optionsModel = res.data.Result;
		console.log('$scope.optionsModel ==>', $scope.optionsModel)
		return $scope.optionsModel;
	});

	$scope.ListTahunKendaraan = [];
		DiskonFactoryMaster.getYearProspect().then(function (res) {
			$scope.ListTahunKendaraan = res.data.Result;
			console.log('$scope.ListTahunKendaraan ==>', $scope.ListTahunKendaraan)
			return $scope.ListTahunKendaraan;
	});


	$scope.onSelectModelChanged = function (ModelSelected) {
		console.log('on model change ===>', ModelSelected);
		$scope.optionsTipe = [];
		$scope.filterModel.VehicleTypelName = "";
		$scope.filterModel.VehicleTypeId = "";
		if (ModelSelected == undefined || ModelSelected == null) {
			$scope.filterModel.VehicleModelName = "";
		} else {
			$scope.filterModel.VehicleModelName = ModelSelected.VehicleModelName;
			DiskonFactoryMaster.getDaVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
				$scope.optionsTipe = res.data.Result;
				return $scope.optionsTipe;
			});
		}
	}

	$scope.onSelectTipeChanged = function (TipeSelected) {
		console.log('TipeSelected ===>', TipeSelected);
		if (TipeSelected == undefined || TipeSelected == null) {
			$scope.filterModel.VehicleTypelName = "";
		} else {
			$scope.filterModel.VehicleTypelName = TipeSelected.Description;
			$scope.filterModel.VehicleTypeId = TipeSelected.VehicleTypeId;
		}
	}


	$scope.onSelectTahunChanged = function (TahunSelected) {
		console.log('Tahun Selected ===>', TahunSelected)
		$scope.filterModel.TahunProduksi = TahunSelected.YearName;

	}

	







	$scope.MasterSellingPriceFU_Diskon_grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'Model', displayName: 'Model', field: 'VehicleModelName', enableCellEdit: false, enableHiding: false },
			{ name: 'Tipe', displayName: 'Tipe', field: 'VehicleTypeName', enableCellEdit: false, enableHiding: false },
			{ name: 'Katashiki', displayName: 'Katashiki', field: 'KatashikiCode', enableCellEdit: false, enableHiding: false },
			{ name: 'Suffix', displayName: 'Suffix', field: 'SuffixCode', enableCellEdit: false, enableHiding: false },
			{ name: 'Role', displayName: 'Role', field: 'ApproverRoleId', enableCellEdit: false, enableHiding: false },
			{ name: 'Nominal Diskon Dari', displayName: 'Nominal Diskon Dari', field: 'NominalDiskonDari', enableCellEdit: false, enableHiding: false,cellFilter: 'currency:"Rp."'},
			{ name: 'Nominal Diskon Sampai', displayName: 'Nominal Diskon Sampai', field: 'NominalDiskonSampai', enableCellEdit: false, enableHiding: false,cellFilter: 'currency:"Rp."'},
			// { name: 'NominalDiskonSampai', displayName: 'Nominal Diskon Dari', field: 'NominalDiskonDari', enableCellEdit: true, enableHiding: false, cellFilter: 'rupiahC', type: "number" },
			// { name: 'NominalDiskonSampai', displayName: 'Nominal Diskon Sampai', field: 'NominalDiskonSampai', enableCellEdit: true, enableHiding: false, cellFilter: 'rupiahC', type: "number" },
			{ name: 'Pricing Cluster Code', displayName: 'Pricing Cluster Code', field: 'PricingClusterCode', enableCellEdit: true, enableHiding: false },
			{ name: 'Effective Date From', displayName: 'Effective Date From', field: 'EffectiveDateFrom', enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},
			{ name: 'Effective Date To', displayName: 'Effective Date To', field: 'EffectiveDateTo', enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Tahun', displayName: 'Tahun', field: 'VehicleModelYear', enableCellEdit: false, enableHiding: false },  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Sequence', displayName: 'Seq', field: 'Seq', enableCellEdit: false, enableHiding: false },  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks', enableCellEdit: true, enableHiding: false },
		],

		onRegisterApi: function (gridApi) {
			$scope.MasterSellingPriceFU_Diskon_gridAPI = gridApi;
		}
	};


	



	$scope.GetDiskon = function () {

		if ($scope.filterModel.VehicleModelName == null || $scope.filterModel.VehicleModelName == undefined) {
			$scope.filterModel.VehicleModelName = '';
		}

		if ($scope.filterModel.VehicleTypeName == null || $scope.filterModel.VehicleTypeName == undefined) {
			$scope.filterModel.VehicleTypeName = '';
		}

		if ($scope.filterModel.VehicleTypeId == null || $scope.filterModel.VehicleTypeId == undefined) {
			$scope.filterModel.VehicleTypeId = '';
		}

		if ($scope.filterModel.TahunProduksi == null || $scope.filterModel.TahunProduksi == undefined) {
			$scope.filterModel.TahunProduksi = ''
		}
		
		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined) {
			$scope.filterModel.TanggalFilterStart = '';
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined) {
			$scope.filterModel.TanggalFilterEnd = '';
		}

		console.log('model ===>', $scope.filterModel.VehicleModelId);
		console.log('tipe ===>', $scope.filterModel.VehicleTypeId);
		console.log('tahun ===>', $scope.filterModel.TahunProduksi);
		console.log('date_from ===>', $scope.filterModel.TanggalFilterStart);
		console.log('date_to ===>', $scope.filterModel.TanggalFilterEnd);
		console.log('filterModel ===>', $scope.filterModel);


		
		DiskonFactoryMaster.getData(1, 10000, $scope.filterModel) 
			.then
			(
				function (res) {
					$scope.loading = false;
					$scope.MasterSellingPriceFU_Diskon_grid.data = res.data.Result;
					$scope.MasterSellingPriceFU_Diskon_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_Diskon_grid.data);
					// $scope.filterModel = [];
					// $scope.filterModel.VehicleTypeId = '';
					// $scope.filterModel.TahunProduksi = {};
					// $scope.filterModel.TahunProduksi.YearName = '';	
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}





	$scope.FilterGridMasterDiskon = function () {
		var inputfilter = $scope.textFilterMasterDiskon;
		console.log('$scope.textFilterMasterDiskon ===>', $scope.textFilterMasterDiskon);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_Diskon_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterDiskon + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_Diskon_grid.data = $scope.MasterSellingPriceFU_Diskon_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_Diskon_grid.data if ===>', $scope.MasterSellingPriceFU_Diskon_grid.data)

		} else {
			$scope.MasterSellingPriceFU_Diskon_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_Diskon_grid.data else ===>', $scope.MasterSellingPriceFU_Diskon_grid.data)
		}
	};


	


	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};


	$scope.formatDate2 = function (dateSimpanConvert) {
		var d = new Date(dateSimpanConvert);
		month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [month, day, year].join('/');
		
	}

	$scope.loadXLS = function (ExcelFile) {
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element(document.querySelector('#uploadSellingFile_Diskon')); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			for (i = 0; i < json.length; i++) {
				var tglUpload = json[i].EffectiveDateFrom //format 20191231
				var thn = tglUpload.substring(0, 4);
				var bln = tglUpload.substring(4, 6);
				var tgl = tglUpload.substring(6, 8);
				var tgl_jadi = tgl + "-" + bln + "-" + thn;
				json[i].EffectiveDateFrom = tgl_jadi;

				var tglUploadTo = json[i].EffectiveDateTo //format 20191231
				var thnTo = tglUploadTo.substring(0, 4);
				var blnTo = tglUploadTo.substring(4, 6);
				var tglTo = tglUploadTo.substring(6, 8);

				var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
				json[i].EffectiveDateTo = tgl_jadiTo;
			}

			$scope.btnUpload = "hide";
			console.log("myEl : ", myEl);
			console.log("json : ", json);
			$scope.MasterSelling_ExcelData = json;
			$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

			$scope.MasterSelling_Diskon_Upload_Clicked();
			myEl.val('');
		});
	}

	$scope.validateColumn = function (inputExcelData) {
		// ini harus di rubah
		
		console.log('apa lagi coba',inputExcelData)

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.Model)) {
			$scope.cekKolom = $scope.cekKolom + ' Model \n';
		}

		if (angular.isUndefined(inputExcelData.Tipe)) {
			$scope.cekKolom = $scope.cekKolom + ' Tipe \n';
		}

		if (angular.isUndefined(inputExcelData.Katashiki)) {
			$scope.cekKolom = $scope.cekKolom + ' Katashiki \n';
		}
		if (angular.isUndefined(inputExcelData.Suffix)) {
			$scope.cekKolom = $scope.cekKolom + ' Suffix \n';
		}
		if (angular.isUndefined(inputExcelData.Role)) {
			$scope.cekKolom = $scope.cekKolom + ' Role \n';
		}
		if (angular.isUndefined(inputExcelData.NominalDiskonDari)) {
			$scope.cekKolom = $scope.cekKolom + ' NominalDiskonDari \n';
		}
		if (angular.isUndefined(inputExcelData.NominalDiskonSampai)) {
			$scope.cekKolom = $scope.cekKolom + ' NominalDiskonSampai \n';
		}
		if (angular.isUndefined(inputExcelData.PricingClusterCode)) {
			$scope.cekKolom = $scope.cekKolom + ' PricingClusterCode\n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		} 
		if (angular.isUndefined(inputExcelData.Tahun)) {
			$scope.cekKolom = $scope.cekKolom + ' Tahun \n';
		} 
		if (angular.isUndefined(inputExcelData.Seq)) {
			$scope.cekKolom = $scope.cekKolom + ' Seq \n';
		}
		if (angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}

		if (
			angular.isUndefined(inputExcelData.Model) ||
			angular.isUndefined(inputExcelData.Tipe) ||
			angular.isUndefined(inputExcelData.Katashiki) ||
			angular.isUndefined(inputExcelData.Suffix) ||
			angular.isUndefined(inputExcelData.Role) ||
			angular.isUndefined(inputExcelData.NominalDiskonDari) ||
			angular.isUndefined(inputExcelData.NominalDiskonSampai) ||
			angular.isUndefined(inputExcelData.PricingClusterCode) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) ||
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Tahun) ||
			angular.isUndefined(inputExcelData.Seq) ||
			angular.isUndefined(inputExcelData.Remarks)) {
			alert('Template file tidak sesuai !');
			return (false);
		}

		return (true);
	}

	$scope.ComboBulkAction_Diskon_Changed = function () {
		if ($scope.ComboBulkAction_Diskon == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_Diskon_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_Diskon_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_Diskon_grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_Diskon_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_Diskon_grid.data.splice($scope.MasterSellingPriceFU_Diskon_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				angular.element('#DiskonPriceModal').modal('show');
			}
		}
	}

	$scope.OK_Button_Clicked = function () {
		angular.forEach($scope.MasterSellingPriceFU_Diskon_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_Diskon_grid.data.splice($scope.MasterSellingPriceFU_Diskon_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_Diskon = "";
		angular.element('#DiskonPriceModal').modal('hide');
	}
	$scope.DeleteCancel_Button_Clicked = function () {
		$scope.ComboBulkAction_Diskon = "";
		angular.element('#DiskonPriceModal').modal('hide');
	}

	$scope.MasterSelling_Diskon_Download_Clicked = function () {
		var excelData = [];
		var fileName = "";
		fileName = "MasterSellingDiskon";

		if ($scope.MasterSellingPriceFU_Diskon_grid.data.length == 0) {
			excelData.push({
				Model: "",
				Tipe: "",
				Katashiki: "",
				Suffix: "",
				Role: "",
				NominalDiskonDari: "",
				NominalDiskonSampai: "",
				PricingClusterCode: "",
				EffectiveDateFrom: "",
				EffectiveDateTo: "",
				Tahun: "",
				Seq: "",
				Remarks: ""
			});
			fileName = "MasterSellingDiskonTemplate";
		}

		else {

			for (var i = 0; i < $scope.MasterSellingPriceFU_Diskon_grid.data.length; i++) {

				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelName == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelName = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleTypeName == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleTypeName = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].KatashikiCode == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].KatashikiCode = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].SuffixCode == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].SuffixCode = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].ApproverRoleId == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].ApproverRoleId = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].NominalDiskonDari == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].NominalDiskonDari = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].NominalDiskonSampai == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].NominalDiskonSampai = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].PricingClusterCode == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].PricingClusterCode = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom = " ";
				} 
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelYear == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelYear = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].Seq == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].Seq = " ";
				}
				if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].Remarks == null) {
					$scope.MasterSellingPriceFU_Diskon_grid.data[i].Remarks = " ";
				}

			}
			console.log('Data grid ==>', $scope.MasterSellingPriceFU_Diskon_grid.data);
			//$scope.MasterSellingPriceFU_Diskon_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_Diskon_grid.data.forEach(function (row) {
				excelData.push({
					Model: row.VehicleModelName,
					Tipe: row.VehicleTypeName,
					Katashiki: row.KatashikiCode,
					Suffix: row.SuffixCode,
					Role: row.ApproverRoleId,
					NominalDiskonDari: row.NominalDiskonDari,
					NominalDiskonSampai: row.NominalDiskonSampai,
					PricingClusterCode: row.PricingClusterCode,
					EffectiveDateFrom: $scope.changeFormatDate(row.EffectiveDateFrom),
        			EffectiveDateTo: $scope.changeFormatDate(row.EffectiveDateTo),
					Tahun: row.VehicleModelYear,
					Seq: row.Seq,
					Remarks: row.Remarks
				});
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);
	}
		$scope.changeFormatDate = function(item) {
			console.log("Tes ==>", item);
			if(item !== null && item !== ' '){
				var tmpParam = item;
				// tmpParam = new Date(tmpParam);
				// tmpParam.setUTCHours(0,0,0,0);
				console.log("Test2 ==>", tmpParam);
				var finalDate
				var yyyy = tmpParam.getFullYear().toString();
				var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
				var dd = tmpParam.getDate().toString();
				finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
				
				return finalDate;
			
			}
		}
		
	$scope.MasterSelling_Diskon_Upload_Clicked = function () {
		if ($scope.MasterSelling_ExcelData.length == 0) {
			alert("file excel kosong !");
			return;
		}

		for (var j=0; j<$scope.MasterSelling_ExcelData.length; j ++) {
			if ($scope.MasterSelling_ExcelData[j].Remarks == undefined || $scope.MasterSelling_ExcelData[j].Remarks == null) {
				$scope.MasterSelling_ExcelData[j].Remarks = '';
			}
		}

		if (!$scope.validateColumn($scope.MasterSelling_ExcelData[0])) {
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);


		//penjagaan kalo tanggal yang di upload kosong
		for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
			// if ($scope.MasterSelling_ExcelData[i].EffectiveDateFrom == '-' || 
			// 				$scope.MasterSelling_ExcelData[i].EffectiveDateFrom == undefined ||
			// 				$scope.MasterSelling_ExcelData[i].EffectiveDateFrom == null||
			// 	   $scope.MasterSelling_ExcelData[i].EffectiveDateFrom == '' ) {
			// 	$scope.MasterSelling_ExcelData[i].EffectiveDateFrom= '00-00-00';
			// }
			if ($scope.MasterSelling_ExcelData[i].EffectiveDateTo == '-' ||
							$scope.MasterSelling_ExcelData[i].EffectiveDateTo == undefined ||
							$scope.MasterSelling_ExcelData[i].EffectiveDateTo == null ||
							$scope.MasterSelling_ExcelData[i].EffectiveDateTo == '') {
								$scope.MasterSelling_ExcelData[i].EffectiveDateTo = '00-00-00';
			}

			if ($scope.MasterSelling_ExcelData[i].NominalDiskonDari == '-' ||
							$scope.MasterSelling_ExcelData[i].NominalDiskonDari == undefined ||
							$scope.MasterSelling_ExcelData[i].NominalDiskonDari == null ||
							$scope.MasterSelling_ExcelData[i].NominalDiskonDari == '' ) {
							$scope.MasterSelling_ExcelData[i].NominalDiskonDari = ' ';
			}

			
			$scope.MasterSelling_ExcelData[i].ApproverRoleId = angular.copy($scope.MasterSelling_ExcelData[i].Role);
			$scope.MasterSelling_ExcelData[i].SuffixCode = angular.copy($scope.MasterSelling_ExcelData[i].Suffix);
			$scope.MasterSelling_ExcelData[i].KatashikiCode = angular.copy($scope.MasterSelling_ExcelData[i].Katashiki);
			$scope.MasterSelling_ExcelData[i].VehicleModelYear = angular.copy($scope.MasterSelling_ExcelData[i].Tahun);
		
			




			//rubah format dari grid kee ====> format yang di minta backend 
			var setan = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSelling_ExcelData[i].DateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSelling_ExcelData[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSelling_ExcelData[i].DateTo ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);


			delete $scope.MasterSelling_ExcelData[i]['Role'];
			delete $scope.MasterSelling_ExcelData[i]['Suffix'];
			delete $scope.MasterSelling_ExcelData[i]['Katashiki'];
			delete $scope.MasterSelling_ExcelData[i]['Tahun'];
			delete $scope.MasterSelling_ExcelData[i]['Model'];
			delete $scope.MasterSelling_ExcelData[i]['Tipe'];

			
			// delete $scope.MasterSelling_ExcelData[i]['EffectiveDateTo'];
			// delete $scope.MasterSelling_ExcelData[i]['EffectiveDateFrom'];


		}

		



		var Grid;
		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_Diskon_gridAPI.grid.clearAllFilters();
		DiskonFactoryMaster.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				//				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_Diskon_grid.data = res.data.Result;			//Data hasil dari WebAPI

				var dataValid = undefined;
				for (i = 0; i < $scope.MasterSellingPriceFU_Diskon_grid.data.length; i++) {
					if ($scope.MasterSellingPriceFU_Diskon_grid.data[i].Remarks != "") {
						dataValid = false;
					}
				}

				if (dataValid == false) {
					bsNotify.show(
						{
							type: 'warning',
							title: "Data tidak valid!",
							content: "Cek detail kesalahan di kolom Remarks pada tabel",
						}
					);
				}

				$scope.MasterSellingPriceFU_Diskon_grid.totalItems = res.data.Total;
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked = function () {



		console.log('$scope.MasterSellingPriceFU_Diskon_grid sebelum ===>', $scope.MasterSellingPriceFU_Diskon_grid.data);

		for (i = 0; i < $scope.MasterSellingPriceFU_Diskon_grid.data.length; i++) {
			$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom = $scope.formatDate2($scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom);
			$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo = $scope.formatDate2($scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo);
			// $scope.MasterSellingPriceFU_Diskon_grid.data[i].ApproverRoleId = angular.copy($scope.MasterSellingPriceFU_Diskon_grid.data[i].Role);
			// $scope.MasterSellingPriceFU_Diskon_grid.data[i].SuffixCode = angular.copy($scope.MasterSelling_ExcelData[i].Suffix);
			// $scope.MasterSellingPriceFU_Diskon_grid.data[i].KatashikiCode = angular.copy($scope.MasterSelling_ExcelData[i].Katashiki);

			$scope.MasterSellingPriceFU_Diskon_grid.data[i].Tahun = $scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelYear;
			$scope.MasterSellingPriceFU_Diskon_grid.data[i].Model = $scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleModelName;
			$scope.MasterSellingPriceFU_Diskon_grid.data[i].Tipe  = $scope.MasterSellingPriceFU_Diskon_grid.data[i].VehicleTypeName;
			
			// delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['VehicleModelYear'];
			delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['VehicleModelName'];
			delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['VehicleTypeName'];
			// delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['Tahun'];
			// delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['Model'];
			// delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['Tipe'];
			delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['role'];
			delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['DateFrom'];
			delete $scope.MasterSellingPriceFU_Diskon_grid.data[i]['DateTo'];


		}

		console.log('$scope.MasterSellingPriceFU_Diskon_grid sesudah ===>', $scope.MasterSellingPriceFU_Diskon_grid.data);


		var Grid;

		// Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		Grid = JSON.stringify($scope.MasterSellingPriceFU_Diskon_grid.data);
		console.log('gridd sesudah ===>', Grid);

		
		DiskonFactoryMaster.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				$scope.GetDiskon();
				bsNotify.show({
					title: "Customer Data",
					content: "Data berhasil di simpan",
					type: 'success'
				});
			},
			function (err) {
				bsNotify.show(
					{
						title: 'Data Gagal Disimpan',
						type: 'danger',
						content: "Ditemukan beberapa data yang salah",
						// content: error.join('<br>'),
						// number: error.length
					}
				);
			}
		);


		//balikan dari simpan ===> yang muncul di Grid setelah Simpan
		//                        12/31/2019   ===> 0/1/2
		// for (i = 0; i < $scope.MasterSellingPriceFU_Diskon_grid.data.length; i++) {
		// 	var setan = $scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom.split('/');
		// 	var to_submit = "" + setan[1] + "-" + setan[0] + "-" + setan[2];
		// 	$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom = to_submit;
		// 	console.log('$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom);

		// 	var setan2 = $scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo.split('/');
		// 	var to_submit2 = "" + setan2[1] + "-" + setan2[0] + "-" + setan2[2];
		// 	$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo = to_submit2;
		// 	console.log('$scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_Diskon_grid.data[i].EffectiveDateTo);
		// }



	}

	$scope.MasterSelling_Batal_Clicked = function () {
		$scope.MasterSellingPriceFU_Diskon_grid.data = [];
		$scope.MasterSellingPriceFU_Diskon_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_Diskon = "";
		$scope.TextFilterSatu_Diskon = "";
		var myEl = angular.element(document.querySelector('#uploadSellingFile_Diskon'));
		myEl.val('');
	}

	$scope.MasterSelling_Diskon_Generate_Clicked = function () {
		var tglStart;
		var tglEnd;

		if ((isNaN($scope.MasterSelling_Diskon_TanggalFilterStart) != true) &&
			(angular.isUndefined($scope.MasterSelling_Diskon_TanggalFilterStart) != true)) {
			tglStart = $scope.formatDate($scope.MasterSelling_Diskon_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if ((isNaN($scope.MasterSelling_Diskon_TanggalFilterEnd) != true) &&
			(angular.isUndefined($scope.MasterSelling_Diskon_TanggalFilterEnd) != true))
		//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd = $scope.formatDate($scope.MasterSelling_Diskon_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{
			PEClassification: $scope.JenisPE,
			Filter1: $scope.KolomFilterSatu_Diskon,
			Text1: $scope.TextFilterSatu_Diskon,
			AndOr: $scope.Filter_Diskon_AndOr,
			Filter2: $scope.KolomFilterDua_Diskon,
			Text2: $scope.TextFilterDua_Diskon,
			StartDate: tglStart,
			EndDate: tglEnd,
			PEType: $scope.TypePE
		}];
		DiskonFactoryMaster.getData(1, 25, JSON.stringify(filter))
			.then(
				function (res) {
					$scope.MasterSellingPriceFU_Diskon_grid.data = res.data.Result;			//Data hasil dari WebAPI
					$scope.MasterSellingPriceFU_Diskon_grid.totalItems = res.data.Total;
				},
				function (err) {
					bsNotify.show(
						{
							type: 'danger',
							title: "Data gagal dimuat!",
							content: err.data.Message.split('-')[1],
						}
					);
				}
			);
	}

	$scope.MasterSelling_Diskon_Cari_Clicked = function () {
		var value = $scope.TextFilterGrid_Diskon;
		$scope.MasterSellingPriceFU_Diskon_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_Diskon != "") {
			$scope.MasterSellingPriceFU_Diskon_gridAPI.grid.getColumn($scope.ComboFilterGrid_Diskon).filters[0].term = value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_Diskon_gridAPI.grid.clearAllFilters();

	}

});
