angular.module('app')
  .factory('LeadtimeAdministrasiPddFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/LeadTimePDDOther');
        return res;
      },
      create: function(LeadtimeAdministrasiPdd) {
        return $http.post('/api/sales/LeadTimePDDOther', [{
                                            //AppId: 1,
											ApprovalLeasingLeadTimeYes: LeadtimeAdministrasiPdd.ApprovalLeasingLeadTimeYes,
											STNKDoneLeadTimeYes: LeadtimeAdministrasiPdd.STNKDoneLeadTimeYes,
											ChoosePoliceNoLeadTimeYes: LeadtimeAdministrasiPdd.ChoosePoliceNoLeadTimeYes,
											AccessoriesInstallationLeadTimeYes: LeadtimeAdministrasiPdd.AccessoriesInstallationLeadTimeYes,
											KaroseriLeadTimeYes: LeadtimeAdministrasiPdd.KaroseriLeadTimeYes,
											DECPreparationLeadTimeYes: LeadtimeAdministrasiPdd.DECPreparationLeadTimeYes,
											DirectDeliveryToCustomerLeadTimeYes: LeadtimeAdministrasiPdd.DirectDeliveryToCustomerLeadTimeYes,
											FormALeadTimeYes: LeadtimeAdministrasiPdd.FormALeadTimeYes,
											InvoiceDoneLeadTimeYes: LeadtimeAdministrasiPdd.InvoiceDoneLeadTimeYes,
                                            LeadTimes: LeadtimeAdministrasiPdd.LeadTimes}]);
      },
      update: function(LeadtimeAdministrasiPdd){
        return $http.put('/api/sales/LeadTimePDDOther', [{
                                            ReasonId: LeadtimeAdministrasiPdd.ReasonId,
											ReasonCategoryCode: LeadtimeAdministrasiPdd.ReasonCategoryCode,
                                            ReasonName: LeadtimeAdministrasiPdd.ReasonName}]);
      },
      delete: function(LeadtimeAdministrasiPdd) {
        return $http.delete('/api/sales/LeadTimePDDOther',{data:LeadtimeAdministrasiPdd,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd