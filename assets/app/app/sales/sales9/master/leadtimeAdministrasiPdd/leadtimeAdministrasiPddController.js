angular.module('app')
    .controller('LeadtimeAdministrasiPddController', function($scope, $http, CurrentUser, LeadtimeAdministrasiPddFactory,$timeout,bsNotify) {
	
	$scope.mLeadtimeAdministrasiPdd=null;
	$scope.LeadtimeAdministrasiPdd_DisableAtas=true;
	$scope.LeadtimeAdministrasiPdd_DisableBawah=true;
	$scope.LeadtimeAdministrasiPdd_ModelBawah = {};
	$scope.user = CurrentUser.user();
	
	LeadtimeAdministrasiPddFactory.getData().then(function(res) {
            $scope.mLeadtimeAdministrasiPdd = res.data.Result[0];
            return $scope.mLeadtimeAdministrasiPdd;
        });
		
	$scope.LeadtimeAdministrasiPdd_Ubah = function() {
			$scope.LeadtimeAdministrasiPdd_DisableAtas=false;
			$scope.LeadtimeAdministrasiPdd_DisableBawah=false;
        }

	$scope.LeadtimeAdministrasiPdd_BatalUbah = function() {
			$scope.LeadtimeAdministrasiPdd_DisableAtas=true;
			$scope.LeadtimeAdministrasiPdd_DisableBawah=true;
        }
		
	$scope.LeadtimeAdministrasiPdd_Simpan = function() {

			LeadtimeAdministrasiPddFactory.create($scope.mLeadtimeAdministrasiPdd).then(function () {
					LeadtimeAdministrasiPddFactory.getData().then(function(res) {
						$scope.mLeadtimeAdministrasiPdd = res.data.Result[0];
						bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
						$scope.LeadtimeAdministrasiPdd_BatalUbah();
						return $scope.mLeadtimeAdministrasiPdd;
					});
				});
			
			
        }	

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	
	

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mLeadtimeAdministrasiPdd = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    
	
    //----------------------------------
    // Grid Setup
    //----------------------------------

});
