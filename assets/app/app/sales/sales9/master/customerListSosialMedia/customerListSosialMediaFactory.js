angular.module('app')
  .factory('CustomerListSosialMediaFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
              "CustomerDetailSocialMediaId" : "1",
              "CustomerId" : "TC1",
              "SocialMediaId" : "Masuk Part",
              "SocialMediaAddress" : "https://accounts.google.com/ServiceLogin?",
              "FrequentlyUsed" : "22323"
          },
          {
              "CustomerDetailSocialMediaId" : "1",
              "CustomerId" : "TC1",
              "SocialMediaId" : "Masuk Part",
              "SocialMediaAddress" : "https://www.facebook.com/?stype=lo&jlou",
              "FrequentlyUsed" : "123423"
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });