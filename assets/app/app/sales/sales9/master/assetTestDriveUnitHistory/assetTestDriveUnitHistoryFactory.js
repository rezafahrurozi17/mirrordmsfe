angular.module('app')
    .factory('AssetTestDriveUnitHistory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                //var res=$http.get('/api/fw/Role');
                //console.log('res=>',res);
                var res = [
                    { TestDriveUnitHistoryId: "1", TestDriveUnitId: "1", NamaTestDriveUnit: "nama 01", NamaTestDriveUnitHistory: "d1", TanggalTestDriveUnitHistory: "12/22/2016" },
                    { TestDriveUnitHistoryId: "2", TestDriveUnitId: "2", NamaTestDriveUnit: "nama 02", NamaTestDriveUnitHistory: "d2", TanggalTestDriveUnitHistory: "12/22/2016" },
                    { TestDriveUnitHistoryId: "3", TestDriveUnitId: "3", NamaTestDriveUnit: "nama 03", NamaTestDriveUnitHistory: "d3", TanggalTestDriveUnitHistory: "12/22/2016" },
                ];
                return res;
            },
            getType: function() {
                return $http.get('/api/ds/Variabels');
            },
            create: function(AssetTestDriveUnitHistory) {
                return $http.post('/api/fw/Role', [{
                    //AppId: 1,
                    TestDriveUnitId: AssetTestDriveUnitHistory.TestDriveUnitId,
                    NamaTestDriveUnitHistory: AssetTestDriveUnitHistory.NamaTestDriveUnitHistory,
                    TanggalTestDriveUnitHistory: AssetTestDriveUnitHistory.TanggalTestDriveUnitHistory
                }]);
            },
            update: function(AssetTestDriveUnitHistory) {
                return $http.put('/api/fw/Role', [{
                    TestDriveUnitHistoryId: AssetTestDriveUnitHistory.TestDriveUnitHistoryId,
                    TestDriveUnitId: AssetTestDriveUnitHistory.TestDriveUnitId,
                    NamaTestDriveUnitHistory: AssetTestDriveUnitHistory.NamaTestDriveUnitHistory,
                    TanggalTestDriveUnitHistory: AssetTestDriveUnitHistory.TanggalTestDriveUnitHistory
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd