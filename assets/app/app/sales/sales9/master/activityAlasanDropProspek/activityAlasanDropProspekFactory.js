angular.module('app')
  .factory('ActivityAlasanDropProspekFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityReasonDropProspect');
        return res;
      },
      create: function(ActivityAlasanDropProspek) {
        return $http.post('/api/sales/MActivityReasonDropProspect', [{
                ReasonDropProspectName: ActivityAlasanDropProspek.ReasonDropProspectName}]);
      },
      update: function(ActivityAlasanDropProspek){
        return $http.put('/api/sales/MActivityReasonDropProspect', [{
                OutletId: ActivityAlasanDropProspek.OutletId,
                ReasonDropProspectId: ActivityAlasanDropProspek.ReasonDropProspectId,
                ReasonDropProspectName: ActivityAlasanDropProspek.ReasonDropProspectName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityReasonDropProspect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });