angular.module('app')
  .factory('ActivityChecklistItemInspeksi', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityChecklistItemInspection');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityChecklistItemInspeksi) {
        return $http.post('/api/sales/MActivityChecklistItemInspection', [{
                GroupChecklistInspectionId:ActivityChecklistItemInspeksi.GroupChecklistInspectionId,
                ChecklistItemInspectionName:ActivityChecklistItemInspeksi.ChecklistItemInspectionName
            }]);
      },
      update: function(ActivityChecklistItemInspeksi){
        return $http.put('/api/sales/MActivityChecklistItemInspection', [{
                OutletId:ActivityChecklistItemInspeksi.OutletId,
                ChecklistItemInspectionId:ActivityChecklistItemInspeksi.ChecklistItemInspectionId,
                GroupChecklistInspectionId:ActivityChecklistItemInspeksi.GroupChecklistInspectionId,
                ChecklistItemInspectionName:ActivityChecklistItemInspeksi.ChecklistItemInspectionName
            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityChecklistItemInspection',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });