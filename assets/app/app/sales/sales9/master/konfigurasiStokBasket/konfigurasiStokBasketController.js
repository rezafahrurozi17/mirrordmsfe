
var app = angular.module('app');
app.controller('KonfigurasiStokBasketController', function ($scope, $http, $filter, CurrentUser, KonfigurasiStokBasketFactory,bsNotify, uiGridConstants, $timeout) {
	var user = CurrentUser.user();
	$scope.uiGridPageSize = 10;
	var cellEdit = false;
	//----------------------------------
    // Initialization
    //----------------------------------
	$scope.MasterStokBasket_Edit_Show = true;	
	$scope.StockBasket = {};
	$scope.StockBasket.StockBasketBit = false;	
	$scope.disabledMasterStokBasket_Simpan_Clicked = false;
	//----------------------------------
	
	$scope.MasterStokBasket_Simpan_Clicked = function(){
		$scope.disabledMasterStokBasket_Simpan_Clicked = true;
		for(var i in $scope.StockBasket_UIGrid.data){
			$scope.StockBasket_UIGrid.data[i].StockBasketBit = $scope.StockBasket.StockBasketBit;
		}
		KonfigurasiStokBasketFactory.update($scope.StockBasket_UIGrid.data)
		.then(
			function(res){
				bsNotify.show(
					{
						title: "berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				$scope.disabledMasterStokBasket_Simpan_Clicked = false;
				$scope.MasterStokBasket_Batal_Clicked();
				//$scope.StockBasket_UIGrid_Paging();
			}
		);
	}
	
	$scope.MasterStokBasketEdit = function() { return cellEdit; };
	
	$scope.MasterStokBasket_Edit_Clicked = function(){
		cellEdit = true;
		$scope.MasterStokBasket_Edit_Show = false;
	}
	
	$scope.MasterStokBasket_Batal_Clicked = function(){
		cellEdit = false;
		$scope.MasterStokBasket_Edit_Show = true;
		$scope.StockBasket_UIGrid_Paging();
		//$scope.StockBasket_UIGrid_Paging(1);
	}
	
	$scope.StockBasket_UIGrid = {
		//paginationPageSizes: null,
		//useCustomPagination: true,
		//useExternalPagination : true,
		paginationPageSizes: [10,25,50],
        paginationPageSize: 10,
		rowSelection : true,
		multiSelect : false,
		columnDefs:[
					{name:"ConfigStockBasketId",field:"ConfigStockBasketId",visible:false},
					{name:"VehicleModelId",field:"VehicleModelId", visible:false},
					{name:"Model", field:"VehicleModelName", enableCellEdit:false},
					{name:"Lead Time (days)",type: 'number', field:"LeadTime", cellEditableCondition:$scope.MasterStokBasketEdit},
				],
		onRegisterApi: function(gridApi) {
			$scope.GridApiStockBasket = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.StockBasket_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.StockBasket_UIGrid_Paging = function(){
		  KonfigurasiStokBasketFactory.getData()
		  .then(
			function(res)
			{
				$scope.StockBasket_UIGrid.data = res.data.Result;
				if ($scope.StockBasket_UIGrid.data[0].StockBasketBit == true){
					$scope.StockBasket.StockBasketBit = true;
				}else{
					$scope.StockBasket.StockBasketBit = false;
				}
				// $scope.StockBasket_UIGrid.totalItems = res.data.Total;
				// $scope.StockBasket_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.StockBasket_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			
		);
	}

	$scope.StockBasket_UIGrid.onRegisterApi = function(gridApi) {
		$scope.grid = gridApi;

		gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
			if(!newValue){
				rowEntity[colDef.field] = 0;
			}
			if (newValue < 0) {
				newValue = 0;
				rowEntity[colDef.field] = newValue;   
				bsNotify.show({
					title: "Peringatan",
					content: "Nilai target tidak boleh kurang dari 0.",
					type: 'warning'
				});           
			} else if (newValue > 999) {
				newValue = 0;
				rowEntity[colDef.field] = newValue;   
				bsNotify.show({
					title: "Peringatan",
					content: "Nilai target tidak boleh lebih dari 999.",
					type: 'warning'
				}); 
			}

		});
	};

	// $scope.StockBasket_UIGrid = {
	// 	paginationPageSizes: null,
	// 	useCustomPagination: true,
	// 	useExternalPagination : true,
	// 	// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //         // paginationPageSize: 15,
	// 	rowSelection : true,
	// 	multiSelect : false,
	// 	columnDefs:[
	// 				{name:"ConfigStockBasketId",field:"ConfigStockBasketId",visible:false},
	// 				{name:"VehicleModelId",field:"VehicleModelId", visible:false},
	// 				{name:"Model", field:"VehicleModelName", enableCellEdit:false},
	// 				{name:"Lead Time (days)", field:"LeadTime", cellEditableCondition:$scope.MasterStokBasketEdit},
	// 			],
	// 	onRegisterApi: function(gridApi) {
	// 		$scope.GridApiStockBasket = gridApi;
	// 		gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	// 			$scope.StockBasket_UIGrid_Paging(pageNumber);
	// 		});
	// 	}
	// };
	
	// $scope.StockBasket_UIGrid_Paging = function(pageNumber){
	// 	  KonfigurasiStokBasketFactory.getData(pageNumber, $scope.uiGridPageSize)
	// 	  .then(
	// 		function(res)
	// 		{
	// 			$scope.StockBasket_UIGrid.data = res.data.Result;
	// 			$scope.StockBasket_UIGrid.totalItems = res.data.Total;
	// 			$scope.StockBasket_UIGrid.paginationPageSize = $scope.uiGridPageSize;
	// 			$scope.StockBasket_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
	// 		}
			
	// 	);
	// }
	
	$scope.StockBasket_UIGrid_Paging();
});