angular.module('app')
	.factory('KonfigurasiStokBasketFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getData: function(){
			var url = '/api/sales/SalesMasterStockBasket/GetData/?start=1&limit=100000000';
			console.log("url MasterStockBasket : ", url);
			var res=$http.get(url);
			return res;
		},

		// getData: function(start, limit){
		// 	var url = '/api/sales/SalesMasterStockBasket/GetData/?start=' + start + '&limit=' + limit;
		// 	console.log("url MasterStockBasket : ", url);
		// 	var res=$http.get(url);
		// 	return res;
		// },
		
		update:function(inputData){
			var url = '/api/sales/MActivityConfigStockBasket/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/as/AfterSalesFIRAnswers/deletedata/?id=' + id;
			console.log("url delete Data : ", url);
			var res=$http.delete(url);
			return res;
		}
	}
});