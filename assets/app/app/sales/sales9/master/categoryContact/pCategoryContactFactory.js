angular.module('app')
    .factory('ContactCategoryFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
        var res=$http.get('/api/sales/PCategoryContact');
        //console.log('res=>',res);
// var da_json=[
//         {SpkCancelId: "1",  SpkCancelName: "name 1"},
//         {SpkCancelId: "2",  SpkCancelName: "name 2"},
//         {SpkCancelId: "3",  SpkCancelName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(categoryContact) {
        return $http.post('/api/sales/PCategoryContact', [{
                                            //AppId: 1,
                                            ContactCategoryName: categoryContact.ContactCategoryName}]);
      },
      update: function(categoryContact){
        return $http.put('/api/sales/PCategoryContact', [{
                                            OutletId : categoryContact.OutletId,
                                            ContactCategoryId: categoryContact.ContactCategoryId,
                                            ContactCategoryName: categoryContact.ContactCategoryName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryContact',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
//ddd