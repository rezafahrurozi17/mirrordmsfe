angular.module('app')
    .factory('MasterTaskListFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/ATaskList');
                //console.log('res=>',res);
                return res;
            },
            create: function(tasklist) {
                return $http.post('/api/sales/ATaskList', [{
                    TaskCode: tasklist.TaskCode,
                    TaskName: tasklist.TaskName,
                    FR: tasklist.FR
                }]);
            },
            update: function(tasklist) {
                return $http.put('/api/sales/ATaskList', [{
                    OutletId: tasklist.OutletId,
                    TaskId: tasklist.TaskId,
                    TaskCode: tasklist.TaskCode,
                    TaskName: tasklist.TaskName,
                    FR: tasklist.FR
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/ATaskList', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });