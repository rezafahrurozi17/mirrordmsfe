angular.module('app')
  .factory('ActivityAlasanPengecualianPengiriman', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityDeliveryExcReason');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityAlasanPengecualianPengiriman) {
        return $http.post('/api/sales/MActivityDeliveryExcReason', [{
                                            //AppId: 1,
                      DeliveryExcReasonName: ActivityAlasanPengecualianPengiriman.DeliveryExcReasonName,
											InputByAdminBit: ActivityAlasanPengecualianPengiriman.InputByAdminBit,
                      InputBySalesmanBit: ActivityAlasanPengecualianPengiriman.InputBySalesmanBit}]);
      },
      update: function(ActivityAlasanPengecualianPengiriman){
        return $http.put('/api/sales/MActivityDeliveryExcReason', [{
                      OutletId: ActivityAlasanPengecualianPengiriman.OutletId,
                      DeliveryExcReasonId: ActivityAlasanPengecualianPengiriman.DeliveryExcReasonId,
											DeliveryExcReasonName: ActivityAlasanPengecualianPengiriman.DeliveryExcReasonName,
                      InputByAdminBit: ActivityAlasanPengecualianPengiriman.InputByAdminBit,
                      InputBySalesmanBit: ActivityAlasanPengecualianPengiriman.InputBySalesmanBit}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityDeliveryExcReason',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });