angular.module('app')
    .controller('TestDriveUnitController', function($scope, $http, CurrentUser, TestDriveUnit,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        //$scope.loading=true; tadinya true
		$scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTestDriveUnit = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
		$scope.loading=false;
		$scope.grid.data =TestDriveUnit.getData();
		
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){

        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){

    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Test Drive Unit Id',    field:'TestDriveUnitId', width:'7%', visible:false },
            { name:'Police Number', field:'PoliceNumber' },
            { name:'Model Type Id',  field: 'ModelTypeId' },
			{ name:'Last Meter', field:'LastMeter' },
            { name:'Last Start Date',  field: 'LastStartDate' },
			{ name:'Last End Date', field:'LastEndDate' },
            { name:'Input User Id',  field: 'InputUserId' },
			{ name:'Input Date', field:'InputDate' },
        ]
    };
});
