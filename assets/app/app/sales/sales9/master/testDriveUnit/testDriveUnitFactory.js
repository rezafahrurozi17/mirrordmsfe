angular.module('app')
  .factory('TestDriveUnit', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
var da_json=[
        {TestDriveUnitId: "1",  PoliceNumber: "PoliceNumber1",  ModelTypeId: "ModelTypeId1",  LastMeter: "LastMeter1",  LastStartDate: new Date("2/24/2017"),  LastEndDate: new Date("2/24/2017"),  InputUserId: "InputUserId1",  InputDate: new Date("2/24/2017")},
        {TestDriveUnitId: "2",  PoliceNumber: "PoliceNumber2",  ModelTypeId: "ModelTypeId2",  LastMeter: "LastMeter2",  LastStartDate: new Date("2/24/2017"),  LastEndDate: new Date("2/24/2017"),  InputUserId: "InputUserId2",  InputDate: new Date("2/24/2017")},
        {TestDriveUnitId: "3",  PoliceNumber: "PoliceNumber3",  ModelTypeId: "ModelTypeId3",  LastMeter: "LastMeter3",  LastStartDate: new Date("2/24/2017"),  LastEndDate: new Date("2/24/2017"),  InputUserId: "InputUserId3",  InputDate: new Date("2/24/2017")},
      ];
	  var res=da_json;
		
        return res;
      },
      create: function(testDriveUnit) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            PoliceNumber: testDriveUnit.PoliceNumber,
											ModelTypeId: testDriveUnit.ModelTypeId,
											LastMeter: testDriveUnit.LastMeter,
											LastStartDate: testDriveUnit.LastStartDate,
											LastEndDate: testDriveUnit.LastEndDate,
											InputUserId: testDriveUnit.InputUserId,
                                            InputDate: testDriveUnit.InputDate}]);
      },
      update: function(testDriveUnit){
        return $http.put('/api/fw/Role', [{
                                            TestDriveUnitId: testDriveUnit.TestDriveUnitId,
                                            PoliceNumber: testDriveUnit.PoliceNumber,
											ModelTypeId: testDriveUnit.ModelTypeId,
											LastMeter: testDriveUnit.LastMeter,
											LastStartDate: testDriveUnit.LastStartDate,
											LastEndDate: testDriveUnit.LastEndDate,
											InputUserId: testDriveUnit.InputUserId,
                                            InputDate: testDriveUnit.InputDate}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd