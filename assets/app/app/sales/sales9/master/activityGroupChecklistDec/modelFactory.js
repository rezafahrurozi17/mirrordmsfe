angular.module('app')
  .factory('ModelFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
        //console.log('res=>',res);
		var optionsModel_Json = [{ name: "Avanza", value: "Avanza" }, { name: "Harrier", value: "Harrier" }, { name: "Innova", value: "Innova" }, { name: "Mark X", value: "Mark X" }, { name: "Alphard", value: "Alphard" }, { name: "FT86", value: "FT86" }];
		//var res=optionsModel_Json;	  
        return res;
      },
      create: function(Model) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: Model.SalesProgramName}]);
      },
      update: function(Model){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: Model.SalesProgramId,
                                            SalesProgramName: Model.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd