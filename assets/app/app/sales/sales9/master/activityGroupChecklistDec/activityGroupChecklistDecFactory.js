angular.module('app')
  .factory('NewActivityGroupChecklistDECFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityGroupChecklistDEC');
        return res;
      },
      // getDataModel: function() {
      //   var res=$http.get('/api/sales/MUnitVehicleModel');
      //  return res;
      // },
      // getDataTipe: function() {
      //     var res=$http.get('/api/sales/MUnitVehicleType');
      //  return res;
      // },

      getDataModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
          
        return res;
      },

      getDataTipe: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
          
        return res;
      },
      
      create: function(ActivityGroupChecklistDEC) {
        return $http.post('/api/sales/MActivityGroupChecklistDEC', [{
          GroupChecklistDECName:ActivityGroupChecklistDEC.GroupChecklistDECName,
          VehicleModelId:ActivityGroupChecklistDEC.VehicleModelId,
		  VehicleTypeId:ActivityGroupChecklistDEC.VehicleTypeId
        }]);
      },

      update: function(ActivityGroupChecklistDEC){
        return $http.put('/api/sales/MActivityGroupChecklistDEC', [{
                      OutletId:ActivityGroupChecklistDEC.OutletId,
                      GroupChecklistDECId:ActivityGroupChecklistDEC.GroupChecklistDECId,
                      VehicleModelId:ActivityGroupChecklistDEC.VehicleModelId,
					  VehicleTypeId:ActivityGroupChecklistDEC.VehicleTypeId,
											GroupChecklistDECName:ActivityGroupChecklistDEC.GroupChecklistDECName
        }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityGroupChecklistDEC',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });