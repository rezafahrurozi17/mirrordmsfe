angular.module('app')
  .factory('TipeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
        //console.log('res=>',res);
		var optionsTipe_Json = [{ name: "1.5 G M/T", value: "1.5 G M/T" }, { name: "1.5 E A/T", value: "1.5 E A/T" }, { name: "2.5 S A/T", value: "2.5 S A/T" }];
		//var res=optionsTipe_Json;	  
        return res;
      },
      create: function(Tipe) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: Tipe.SalesProgramName}]);
      },
      update: function(Tipe){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: Tipe.SalesProgramId,
                                            SalesProgramName: Tipe.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd