angular.module('app')
    .controller('ActivityGroupChecklistDECController', function($scope, $http, CurrentUser, ModelFactory, TipeFactory, NewActivityGroupChecklistDECFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mNewActivityGroupChecklistDEC = null; //Model
    $scope.xRole={selected:[]};
    $scope.TempTipe = [];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        NewActivityGroupChecklistDECFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    var listTipe = [];
    TipeFactory.getData().then(
        function(res){
            listTipe = res.data.Result;
            return res.data;
        }
    );

    $scope.filterModel = function() {
        $scope.getTipe = listTipe.filter(function(test)
        {
            return (test.VehicleModelId == $scope.mNewActivityGroupChecklistDEC.VehicleModelId);
        })

        var filterModel = $scope.listTipe.filter(function(obj)
        {
            return obj.VehicleTypeId == $scope.mNewActivityGroupChecklistDEC.VehicleTypeId;
        })

        if(filterModel.length == 0) 
            $scope.mNewActivityGroupChecklistDEC.VehicleTypeId = null;

    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
    $scope.selectRole = function(rows) {
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    
    $scope.onSelectRows = function(rows) {
    }
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Group Checklist DEC Id',field:'GroupChecklistDECId', width:'7%', visible:false },
            { name:'Nama', field:'GroupChecklistDECName' }
            // { name:'Model Kendaraan', field:'VehicleModelName' },
            // { name:'Type Kendaraan', field:'VehicleTypeName' }
        ]
    };
});
