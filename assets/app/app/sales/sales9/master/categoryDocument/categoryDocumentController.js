angular.module('app')
	.controller('CategoryDocumentController', function ($scope, $http, CurrentUser, CategoryDocumentFactory, $timeout, bsNotify) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.gridData = [];
		});
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.mCategoryDocument = null; //Model
		$scope.xRole = { selected: [] };

		//----------------------------------
		// Get Data test
		//----------------------------------
		var gridData = [];
		$scope.getData = function () {
			CategoryDocumentFactory.getData().then(
				function (res) {
					gridData = [];
					$scope.grid.data = res.data.Result; //nanti balikin
					$scope.loading = false;
					console.log("grid",$scope.grid.data);
				},
				function (err) {
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			)
		};



		function roleFlattenAndSetLevel(node, lvl) {
			for (var i = 0; i < node.length; i++) {
				node[i].$$treeLevel = lvl;
				gridData.push(node[i]);
				if (node[i].child.length > 0) {
					roleFlattenAndSetLevel(node[i].child, lvl + 1)
				} else {

				}
			}
			return gridData;
		}
		$scope.selectRole = function (rows) {
			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}

		$scope.doCustomSave = function (mdl, mode) {

			console.log('doCustomSave3', mode);//create update

			if (mode == "create") {
				CategoryDocumentFactory.create($scope.mCategoryDocument).then(
					function (res) {
						CategoryDocumentFactory.getData().then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					},
					function (err) {

						console.log("create", err);
						var respond = err.data.Message;
						var lengthstring = err.data.Message.length;
						var count = (respond.match(/#/g) || []).length;
						var index = respond.indexOf('#');
	
						if (count >= 1) {
							bsNotify.show({
								title: "Gagal",
								content: respond.slice(index + 1, lengthstring),
								type: 'danger'
							});
						}
						else{
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data gagal disimpan",
									type: 'danger'
								}
							);
						}

						// if(err.data.Message="Sudah ada item dengan nama yang sama")
						// 	{
						// 		bsNotify.show(
						// 			{
						// 				title: "Gagal",
						// 				content: "Sudah ada item dengan nama yang sama",
						// 				type: 'danger'
						// 			}
						// 		);
						// 	}
						// 	else
						// 	{
						// 		bsNotify.show(
						// 			{
						// 				title: "Gagal",
						// 				content: "Data gagal disimpan",
						// 				type: 'danger'
						// 			}
						// 		);
						// 	}
					}
				)
			}
			else if (mode == "update") {
				var test = angular.copy($scope.mCategoryDocument);
				console.log("update klik",test);
				CategoryDocumentFactory.update($scope.mCategoryDocument).then(
					function (res) {
						CategoryDocumentFactory.getData().then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},
					function (err) {
						var respond = err.data.Message;
						var lengthstring = err.data.Message.length;
						var count = (respond.match(/#/g) || []).length;
						var index = respond.indexOf('#');
	
						if (count >= 1) {
							bsNotify.show({
								title: "Gagal",
								content: respond.slice(index + 1, lengthstring),
								type: 'danger'
							});
						}
						else{
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data gagal disimpan",
									type: 'danger'
								}
							);
						}

						// if(err.data.Message="Sudah ada item dengan nama yang sama")
						// 	{
						// 		bsNotify.show(
						// 			{
						// 				title: "Gagal",
						// 				content: "Sudah ada item dengan nama yang sama",
						// 				type: 'danger'
						// 			}
						// 		);
						// 	}
						// 	else
						// 	{
						// 		bsNotify.show(
						// 			{
						// 				title: "Gagal",
						// 				content: "Data gagal disimpan",
						// 				type: 'danger'
						// 			}
						// 		);
						// 	}
						
					}
				)
			}


			$scope.formApi.setMode("grid");
		}

		$scope.DeleteManual = function (id) {
			console.log('iddelete',id);
			var arrayDelete = [];
			for(var i in id) {
				arrayDelete.push(id[i].DocumentId)
			}
			console.log('arraydel',arrayDelete);
			CategoryDocumentFactory.delete(arrayDelete).then(
				function (res) {
					CategoryDocumentFactory.getData().then(
						function (res) {
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading = false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil dihapus",
									type: 'success'
								}
							);
							return res.data;
						},
						function (err) {
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan",
									type: 'danger'
								}
							);
						}
					);
				},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak berhasil dihapus",
							type: 'danger'
						}
					);
				}
			)
		}


		$scope.onSelectRows = function (rows) {
		}
		//----------------------------------
		// Grid Setup
		//----------------------------------
		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,
			columnDefs: [
				{ name: 'DocumentId', field: 'DocumentId', width: '7%', visible: false },
				{ name: 'Kategori Dokumen', field: 'DocumentType' },
				{ name: 'Nama Dokumen', field: 'DocumentName' },
			]
		};
	});