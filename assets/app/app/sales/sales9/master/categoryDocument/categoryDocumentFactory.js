angular.module('app')
    .factory('CategoryDocumentFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/PCategoryDocumentForMaster');
                console.log("factory getdata",res);
                //console.log('res=>',res);
                // var res = [{
                //     "DocumentId": 1,
                //     "DocumentTypeId": "1",
                //     "DocumentType": "Type A",
                //     "DocumentName": "KTP"
                // }, {
                //     "DocumentId": 2,
                //     "DocumentTypeId": "2",
                //     "DocumentType": "Type B",
                //     "DocumentName": "KK"
                // }, {
                //     "DocumentId": 3,
                //     "DocumentTypeId": "3",
                //     "DocumentType": "Type C",
                //     "DocumentName": "SIM"
                // }];
                return res;
            },
            create: function(CategoryDocument) {
                return $http.post('/api/sales/PCategoryDocument', [{
                    DocumentType: CategoryDocument.DocumentType,
                    DocumentName: CategoryDocument.DocumentName,
                    StatusActive : CategoryDocument.StatusActive
                    //model dari back : html ,,case
                }]);
            },
            update: function(CategoryDocument) {
                return $http.put('/api/sales/PCategoryDocument', [{
                    OutletId : CategoryDocument.OutletId,
                    DocumentId: CategoryDocument.DocumentId,
                    DocumentType: CategoryDocument.DocumentType,
                    DocumentName: CategoryDocument.DocumentName,
                    StatusActive : CategoryDocument.StatusActive
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/PCategoryDocument', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });