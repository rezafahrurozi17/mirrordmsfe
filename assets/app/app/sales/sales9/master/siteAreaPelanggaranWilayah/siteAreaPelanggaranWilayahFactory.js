angular.module('app')
    .factory('SiteAreaPelanggaranWilayahFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                //var res = $http.get('/api/fw/Role');
                var res = [{
                        "AreaPelanggaranWilayahId": 1,
                        "NamaAreaPelanggaranWilayah": "Kelapa Gading"
                    },
                    {
                        "AreaPelanggaranWilayahId": 2,
                        "NamaAreaPelanggaranWilayah": "Sunter"
                    }
                ];
                //console.log('res=>',res);
                return res;
            },
            create: function(pelanggaranwilayah) {
                return $http.post('/api/fw/Role', [{
                    NamaAreaPelanggaranWilayah: pelanggaranwilayah.NamaAreaPelanggaranWilayah
                }]);
            },
            update: function(sitePds) {
                return $http.put('/api/fw/Role', [{
                    AreaPelanggaranWilayahId: pelanggaranwilayah.AreaPelanggaranWilayahId,
                    NamaAreaPelanggaranWilayah: pelanggaranwilayah.NamaAreaPelanggaranWilayah
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd