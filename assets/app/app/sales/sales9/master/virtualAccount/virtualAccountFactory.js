angular.module('app')
    .factory('VirtualAccountFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            getData: function() {
                var url = '/api/sales/SalesMasterVirtualAccount/GetDataAll/?start=1&limit=1000000'; //+ '&outletid=' + user.OutletId;
                console.log("url MasterVirtualAccount : ", url);
                var res = $http.get(url);
                return res;
            },

            create: function(inputData) {
                var url = '/api/sales/SalesMasterVirtualAccount/Create/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },
			delete: function(id) {
				return $http.delete('/api/sales/SalesMasterVirtualAccount2',{data:id,headers: {'Content-Type': 'application/json'}});
			}
        }
    });