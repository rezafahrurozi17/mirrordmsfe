var app = angular.module('app');
app.controller('VirtualAccountController', function($scope, $http, $filter, CurrentUser, VirtualAccountFactory, $timeout,bsNotify) {
    var user = CurrentUser.user();

	$scope.mVirtualAccount = null; //Model
	$scope.MasterVirtualAccountDisableUpload=false;

    //Main Master Virtual Account
    $scope.MainMasterVirtualAccount_Download_Clicked = function() {
        $scope.VirtualAccount_DownloadExcelTemplate();
    }
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalVirtualAccountHapus').remove();
		});

    $scope.MainMasterVirtualAccount_Upload_Clicked = function() {
        var inputData = [];
		var VirtualAccount_Keisi_NomorDoang_Aman=true;
        angular.forEach($scope.VirtualAccount_ExcelData, function(value, key) {
			if(value.VirtualAccountNo==""||(typeof value.VirtualAccountNo=="undefined")||value.VirtualAccountNo==null)
			{
				VirtualAccount_Keisi_NomorDoang_Aman=false;
			}
			inputData.push({ "VirtualAccountNo": value.VirtualAccountNo, "OutletId": user.OutletId });
        });
		
		if(VirtualAccount_Keisi_NomorDoang_Aman==true)
		{
			VirtualAccountFactory.create(inputData)
            .then(
                function(res) {
                    bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: 'success'
						}
					);
                    //$scope.VirtualAccount_UIGrid_Paging(1);
					$scope.getData();
                },
                function(err) {
                    bsNotify.show(
						{
							title: "Gagal",
							content: "Data tak berhasil disimpan.",
							type: 'danger'
						}
					);
                }
            );
		}
		else if(VirtualAccount_Keisi_NomorDoang_Aman==false)
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Data tak berhasil disimpan. Pastikan Data Anda Valid",
							type: 'warning'
						}
					);
		}
        

        angular.element(document.querySelector('#uploadVirtualAccount')).val(null);
	}
	
	
	$scope.DeleteManual = function (id) {
		console.log('iddelete',id);
		var arrayDelete = [];
		for(var i in id) {
			arrayDelete.push(id[i].VirtualAccountId)
		}
		console.log('arraydel',arrayDelete);
		VirtualAccountFactory.delete(arrayDelete).then(
			function (res) {
				VirtualAccountFactory.getData().then(
					function (res) {
						gridData = [];
						$scope.grid.data = res.data.Result;
						$scope.loading = false;
						bsNotify.show(
							{
								title: "Sukses",
								content: "Data berhasil dihapus",
								type: 'success'
							}
						);
						return res.data;
					},
					function (err) {
						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak ditemukan",
								type: 'danger'
							}
						);
					}
				);
			},
			function (err) {
				bsNotify.show(
					{
						title: "Gagal",
						content: "Data tidak berhasil dihapus",
						type: 'danger'
					}
				);
			}
		)
	}

    
        //==========================================================================//
        // 							Excel Upload Begin								//
        //==========================================================================//
    $scope.loadXLS = function(ExcelFile) {
            var ExcelData = [];
            var myEl = angular.element(document.querySelector('#uploadVirtualAccount')); //ambil elemen dari dokumen yang di-upload 
			
			var Cek_Da_Extention = myEl.context.value;
			var DapetPanjangBenda = Cek_Da_Extention.split(".");
			$scope.MasterVirtualAccountDisableUpload=true;
			
			if(DapetPanjangBenda.length>=3)
			{
				$scope.MasterVirtualAccountDisableUpload=true;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Nama file tidak boleh menggunakan '.'",
						type: 'warning'
					}
				);
			}
			else if(DapetPanjangBenda.length<3)
			{
				XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
					$scope.MasterVirtualAccountDisableUpload=false;
					ExcelData = json[0];
					$scope.VirtualAccount_ExcelData = json;
				});
			}
            
        }
        //==========================================================================//
        // 							Excel Upload End								//
        //==========================================================================//

    //==========================================================================//
    // 							Excel Download Begin							//
    //==========================================================================//
    $scope.getData = function() {
        VirtualAccountFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }
	
	$scope.VirtualAccount_DownloadExcelTemplate = function() {
            var excelData = [];
            var fileName = "";

            excelData = [{ "VirtualAccountNo": "" }];
            fileName = "VirtualAccount_Template";

            XLSXInterface.writeToXLSX(excelData, fileName);
        }
        //==========================================================================//
        // 							Excel Download End								//
        //==========================================================================//

    //==========================================================================//
    // 							Lihat Section Begin								//
    //==========================================================================//

    

    

    
	
	$scope.HapusVirtualAccount = function(Selected_Data) {
        setTimeout(function() {
              angular.element('.ui.modal.ModalVirtualAccountHapus').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalVirtualAccountHapus').not(':first').remove();  
            }, 1);
		$scope.VirtualAccountHapusSelected_Data=angular.copy(Selected_Data);	
		// if(Selected_Data.VAStatus=='Belum Terpakai')
		// {
			// VirtualAccountFactory.delete(Selected_Data.VirtualAccountId).then(function () {
					// $scope.VirtualAccount_UIGrid_Paging(1);
					//$scope.getData();
					// bsNotify.show(
						// {
							// title: "Berhasil",
							// content: "Data berhasil dihapus.",
							// type: 'success'
						// }
					// );
					// //refresh balik ke page 1
				// });
		// }
		// else
		// {
			// bsNotify.show(
						// {
							// title: "Peringatan",
							// content: "Hanya data yang belum dipakai yang bisa dihapus.",
							// type: 'warning'
						// }
					// );
		// }
		
    }
	
	$scope.HapusVirtualAccountOk = function() {
		if($scope.VirtualAccountHapusSelected_Data.VAStatus=='Belum Terpakai')
		{
			VirtualAccountFactory.delete($scope.VirtualAccountHapusSelected_Data.VirtualAccountId).then(function () {
					//$scope.VirtualAccount_UIGrid_Paging(1);
					$scope.getData();
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
					angular.element('.ui.modal.ModalVirtualAccountHapus').modal('hide');
					//refresh balik ke page 1
				});
		}
		else
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Hanya data yang belum dipakai yang bisa dihapus.",
							type: 'warning'
						}
					);
		}
		
	}
	
	$scope.HapusVirtualAccountCancel = function() {
		angular.element('.ui.modal.ModalVirtualAccountHapus').modal('hide');
	}

    $scope.doCustomSave = function (mdl,mode) {
			if(mode=="create")
			{    
				var inputData = [];

				inputData.push({ "VirtualAccountNo": $scope.mVirtualAccount.VirtualAccountNo, "OutletId": user.OutletId });

				VirtualAccountFactory.create(inputData)
					.then(
						function(res) {
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
							$scope.formApi.setMode("grid");
						},
						function(err) {
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak berhasil disimpan.",
									type: 'danger'
								}
							);
						}
					);
			}	
        }
        //==========================================================================//
        // 							Lihat Section End								//
        //==========================================================================//


    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        rowSelection: true,
        columnDefs: [
            { name: "VirtualAccountId", field: "VirtualAccountId", visible: false },
            { name: "Virtual Account", field: "VirtualAccountNo" },
            { name: "Status Transaksi", field: "VAStatus" },
            { name: "SPKNo", field: "SPKNo", visible: false },
            
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiVirtualAccount = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
                $scope.VirtualAccount_UIGrid_Paging(pageNumber);
            });
        }
    };

    


});