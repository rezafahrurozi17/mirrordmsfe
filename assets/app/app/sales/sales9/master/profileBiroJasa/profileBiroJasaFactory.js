angular.module('app')
  .factory('ProfileBiroJasaFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileBiroJasa');
        //console.log('res=>',res);
        return res;
      },
	  getDataKabupatenKota: function(param) {
        var res=$http.get('/api/sales/MLocationCityRegency'+param);	  
        return res;
      },
      create: function(ProfileBiroJasa) {
        return $http.post('/api/sales/MProfileBiroJasa', [{
                                            ServiceBureauCode:ProfileBiroJasa.ServiceBureauCode,
											ServiceBureauName:ProfileBiroJasa.ServiceBureauName,
											ServiceBureauAddress:ProfileBiroJasa.ServiceBureauAddress,
											ServiceBureauPhone:ProfileBiroJasa.ServiceBureauPhone,
											ServiceBureauPicName:ProfileBiroJasa.ServiceBureauPicName,
											ServiceBureauDesc:ProfileBiroJasa.ServiceBureauDesc,
											ServiceBureauPrice:ProfileBiroJasa.ServiceBureauPrice,
											ServiceBureauCityId:ProfileBiroJasa.ServiceBureauCityId,
                                            }]);
      },
      update: function(ProfileBiroJasa){
        return $http.put('/api/sales/MProfileBiroJasa', [{
											ServiceBureauId:ProfileBiroJasa.ServiceBureauId,
											OutletId:ProfileBiroJasa.OutletId,
                                            ServiceBureauCode:ProfileBiroJasa.ServiceBureauCode,
											ServiceBureauName:ProfileBiroJasa.ServiceBureauName,
											ServiceBureauAddress:ProfileBiroJasa.ServiceBureauAddress,
											ServiceBureauPhone:ProfileBiroJasa.ServiceBureauPhone,
											ServiceBureauPicName:ProfileBiroJasa.ServiceBureauPicName,
											ServiceBureauDesc:ProfileBiroJasa.ServiceBureauDesc,
											ServiceBureauPrice:ProfileBiroJasa.ServiceBureauPrice,
											ServiceBureauCityId:ProfileBiroJasa.ServiceBureauCityId,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileBiroJasa',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd