angular.module('app')
  .factory('CategoryReasonBookingUnitRevisionFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryReasonRevision');
        //console.log('res=>',res);
        // var da_json=[
				// {ReasonBookingUnitRevisionId: "1",  ReasonBookingUnitRevisionName: "Name 01",  FundSourceId: "01", FundSourceName: "Fund 01"},
				// {ReasonBookingUnitRevisionId: "2",  ReasonBookingUnitRevisionName: "Name 02",  FundSourceId: "02", FundSourceName: "Fund 02"},
        // {ReasonBookingUnitRevisionId: "3",  ReasonBookingUnitRevisionName: "Name 03",  FundSourceId: "03", FundSourceName: "Fund 03"},

			  // ];
        // res=da_json;
        return res;
      },
      getDataFundSource: function() {
        var res=$http.get('/api/sales/FFinanceFundSource');
       
        return res;
      },
      create: function(CategoryReasonBookingUnitRevision) {
        return $http.post('/api/sales/PCategoryReasonRevision', [{
                                            ReasonRevisionName:CategoryReasonBookingUnitRevision.ReasonRevisionName,
                                            FundSourceId:CategoryReasonBookingUnitRevision.FundSourceId

                                            }]);
      },
      update: function(CategoryReasonBookingUnitRevision){
        return $http.put('/api/sales/PCategoryReasonRevision', [{
                                            OutletId : CategoryReasonBookingUnitRevision.OutletId,
                                            ReasonRevisionId:CategoryReasonBookingUnitRevision.ReasonRevisionId,
                                            ReasonRevisionName:CategoryReasonBookingUnitRevision.ReasonRevisionName,
                                            FundSourceId:CategoryReasonBookingUnitRevision.FundSourceId
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryReasonRevision',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd