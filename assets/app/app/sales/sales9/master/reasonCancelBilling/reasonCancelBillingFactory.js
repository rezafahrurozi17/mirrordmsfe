angular.module('app')
  .factory('ReasonCancelBilling', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryReasonCancelBilling');
        //console.log('res=>',res);
// var da_json=[
//         {ReasonCancelId: "1",  ReasonCancelName: "name 1"},
//         {ReasonCancelId: "2",  ReasonCancelName: "name 2"},
//         {ReasonCancelId: "3",  ReasonCancelName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(reasonCancelBilling) {
        return $http.post('/api/sales/PCategoryReasonCancelBilling', [{
                                            //AppId: 1,
                                            ReasonCancelName: reasonCancelBilling.ReasonCancelName}]);
      },
      update: function(reasonCancelBilling){
        return $http.put('/api/sales/PCategoryReasonCancelBilling', [{
                                            OutletId : reasonCancelBilling.OutletId,
                                            ReasonCancelId: reasonCancelBilling.ReasonCancelId,
                                            ReasonCancelName: reasonCancelBilling.ReasonCancelName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryReasonCancelBilling',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd