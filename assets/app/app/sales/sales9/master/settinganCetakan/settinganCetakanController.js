angular.module('app')
    .controller('SettinganCetakanController', function($scope, $http, CurrentUser, SettinganCetakanFactoryMaster,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
	
	
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalEmployeeBuatSettinganCetakan').remove();
		});
	
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mSettinganCetakan = null; //Model
    $scope.xRole={selected:[]};
	

    //----------------------------------
    // Get Data test
    //----------------------------------
	
     $scope.getData = function() {
        SettinganCetakanFactoryMaster.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.RemoveSettinganCetakanTandaTanganChild = function(index) {
			$scope.mSettinganCetakan.ListPrintSignature.splice(index, 1);
        }
	
	$scope.TambahSettinganCetakanTandaTanganChild = function () {
		
		SettinganCetakanFactoryMaster.getDataRole().then(function (res) {
			$scope.optionSettinganCetakanRole = res.data.Result;
			
			
			$scope.Da_ModalSettinganCetakanEmployeeName=null;
			//$scope.Da_ModalSettinganCetakanRoleId=null;
			document.getElementById('SettinganCetakanEmployeeNameTxtbx').value="";
			//document.getElementById('SettinganCetakanRoleIdDropDown').value=null;
			angular.element('.ui.modal.ModalEmployeeBuatSettinganCetakan').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalEmployeeBuatSettinganCetakan').not(':first').remove();
		});
		
		
    }
	
	$scope.BatalSettinganCetakanTandaTanganChild = function () {
			angular.element('.ui.modal.ModalEmployeeBuatSettinganCetakan').modal('hide');

        }
		
	$scope.Da_ModalSettinganCetakanEmployeeRole_Selected = function (selected) {
		try
		{
			$scope.Da_ModalSettinganCetakanRoleId=selected.LastManPowerPositionTypeId;
			$scope.Da_ModalSettinganCetakanRoleName=selected.LastManPowerPositionTypeName;
		}
		catch(elele)
		{
			$scope.Da_ModalSettinganCetakanRoleId="";
			$scope.Da_ModalSettinganCetakanRoleName="";
		}
		
	}
	
	// $scope.SearchEmployeeSettinganCetakan = function (da_value) {
			// try
			// {
				// if($scope.SelectedEmployeeSearchCriteria=="EmployeeName" && da_value!="" &&(typeof da_value!="undefined"))
				// {
					// SettinganCetakanFactoryMaster.getDataEmployeeSearchName(da_value).then(function (res) {
						// $scope.gridSettinganCetakanEmployee.data = res.data.Result;
					// });
				// }
				// else if($scope.SelectedEmployeeSearchCriteria=="RoleName" && da_value!="" &&(typeof da_value!="undefined"))
				// {
					// SettinganCetakanFactoryMaster.getDataEmployeeSearchJabatan(da_value).then(function (res) {
						// $scope.gridSettinganCetakanEmployee.data = res.data.Result;
					// });
				// }
				// else
				// {
					// SettinganCetakanFactoryMaster.getDataEmployee("").then(function (res) {
						// $scope.gridSettinganCetakanEmployee.data = res.data.Result;
					// });
				// }
			// }
			// catch(www)
			// {
				// SettinganCetakanFactoryMaster.getDataEmployee("").then(function (res) {
						// $scope.gridSettinganCetakanEmployee.data = res.data.Result;
					// });
			// }
			
        // }
	
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
	
	$scope.pilihSettignanCetakanEmployee = function () {
            var da_nama=document.getElementById('SettinganCetakanEmployeeNameTxtbx').value;

			if((typeof $scope.Da_ModalSettinganCetakanRoleId!="undefined") && $scope.Da_ModalSettinganCetakanRoleId!=null && $scope.Da_ModalSettinganCetakanRoleId!="" && (typeof da_nama!="undefined") && da_nama!=null && da_nama!="")
			{
				try
				{
					var SettinganCetakanAmanDitambah=true;
					
					for(var i = 0; i < $scope.mSettinganCetakan.ListPrintSignature.length; ++i)
					{	
						//if($scope.mSettinganCetakan.ListPrintSignature[i].EmployeeName==document.getElementById('SettinganCetakanEmployeeNameTxtbx').value && $scope.mSettinganCetakan.ListPrintSignature[i].LastManPowerPositionTypeName==$scope.Da_ModalSettinganCetakanRoleName)
						if($scope.mSettinganCetakan.ListPrintSignature[i].LastManPowerPositionTypeName==$scope.Da_ModalSettinganCetakanRoleName)
						{
							SettinganCetakanAmanDitambah=false;
							bsNotify.show(
								{
									title: "Peringatan",
									content: "Jabatan sudah terdaftar, silahkan mengubah data kembali",
									type: 'warning'
								}
							);
							break;
						}
					}
					
					if(SettinganCetakanAmanDitambah==true)
					{
						$scope.mSettinganCetakan.ListPrintSignature.push({ OutletId: $scope.user.OutletId,
																PrintSettingId: $scope.mSettinganCetakan.PrintSettingId,
																EmployeeName: document.getElementById('SettinganCetakanEmployeeNameTxtbx').value,
																LastManPowerPositionTypeName:$scope.Da_ModalSettinganCetakanRoleName,
																LastManPowerPositionTypeId: $scope.Da_ModalSettinganCetakanRoleId});
					}
					
				}
				catch(err)
				{
					$scope.mSettinganCetakan.ListPrintSignature=[{ OutletId: null,
															PrintSettingId: null,
															EmployeeName: null,
															LastManPowerPositionTypeName: null,
															LastManPowerPositionTypeId: null}];
															
					$scope.mSettinganCetakan.ListPrintSignature.splice(0, 1);
					$scope.mSettinganCetakan.ListPrintSignature.push({ OutletId: $scope.user.OutletId,
																PrintSettingId: $scope.mSettinganCetakan.PrintSettingId,
																EmployeeName: document.getElementById('SettinganCetakanEmployeeNameTxtbx').value,
																LastManPowerPositionTypeName:$scope.Da_ModalSettinganCetakanRoleName,
																LastManPowerPositionTypeId: $scope.Da_ModalSettinganCetakanRoleId});
				}
				angular.element('.ui.modal.ModalEmployeeBuatSettinganCetakan').modal('hide');
			}
			else
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Harap isi data mandatory.",
						type: 'warning'
					}
				);
			}
			
        }
		
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			SettinganCetakanFactoryMaster.create($scope.mSettinganCetakan).then(
				function(res) {
					SettinganCetakanFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			SettinganCetakanFactoryMaster.update($scope.mSettinganCetakan).then(
				function(res) {
					SettinganCetakanFactoryMaster.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Cetakan',    field:'PrintSettingName'},
            { name:'Tipe Transaksi', field:'PrintTransactionTypeName' }
        ]
    };
	
	// var btnActionPilihEmployeeSettinganCetakan = '<a style="color:blue;"  ng-click=""> \
                                    // <p style="padding:5px 0 0 5px" ng-click="grid.appScope.pilihSettignanCetakanEmployee(row.entity)"> \
                                        // <u>Pilih</u> \
                                    // </p> \
                                  // </a>';
	
	// $scope.gridSettinganCetakanEmployee = {
            // enableSorting: true,
            // enableFiltering: true,
            // // enableRowSelection: true,
            // // multiSelect: true,
            // // enableSelectAll: true,
            // //showTreeExpandNoChildren: true,
            // // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // // paginationPageSize: 15,
			// //ada kemungkinan ganti prospect code
            // columnDefs: [
                // { name: 'EmployeeName', field: 'EmployeeName'},
				// { name: 'RoleName', field: 'LastManPowerPositionTypeName' },
                // {
                    // name: 'action',
                    // allowCellFocus: false,
                    // width: '25%',
                    // pinnedRight: true,
                    // enableColumnMenu: false,
                    // enableSorting: false,
                    // enableColumnResizing: true,
                    // cellTemplate: btnActionPilihEmployeeSettinganCetakan
                // }

            // ]
        // };
});


