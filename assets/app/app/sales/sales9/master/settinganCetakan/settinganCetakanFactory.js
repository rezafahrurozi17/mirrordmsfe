angular.module('app')
  .factory('SettinganCetakanFactoryMaster', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MPrintSetting');
        return res;
      },
	  getDataEmployee: function(DaSearch) {
        var res=$http.get('/api/sales/MProfileEmployee'+DaSearch);
        return res;
      },
	  getDataRole: function() {
        var res=$http.get('/api/sales/MProfileLastManPowerPositionType');
        return res;
      },
	  getDataEmployeeSearchName: function(DaSearch) {
        var res=$http.get('/api/sales/MProfileEmployee/?start=1&limit=10000&filterData=EmployeeName|'+DaSearch);
        return res;
      },
	  getDataEmployeeSearchJabatan: function(DaSearch) {
        var res=$http.get('/api/sales/MProfileEmployee/?start=1&limit=10000&filterData=LastManPowerPositionTypeName|'+DaSearch);
        return res;
      },
	  GetEmployeeSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "EmployeeName", SearchOptionName: "Nama" },
          { SearchOptionId: "RoleName", SearchOptionName: "LastManPowerPositionTypeName" }
        ];

        var res=da_json

        return res;
      },
      create: function(SettinganCetakan) {
        return $http.post('/api/sales/MPrintSetting', [{
                                            //AppId: 1,
											PrintSettingName: SettinganCetakan.PrintSettingName,
											PrintTransactionTypeId: SettinganCetakan.PrintTransactionTypeId,
											Distribution: SettinganCetakan.Distribution,
											ListPrintSignature: SettinganCetakan.ListPrintSignature,
                                            Notation: SettinganCetakan.Notation}]);
      },
      update: function(SettinganCetakan){
        return $http.put('/api/sales/MPrintSetting', [{
                                            PrintSettingId: SettinganCetakan.PrintSettingId,
											PrintSettingName: SettinganCetakan.PrintSettingName,
											PrintTransactionTypeId: SettinganCetakan.PrintTransactionTypeId,
											Distribution: SettinganCetakan.Distribution,
											ListPrintSignature: SettinganCetakan.ListPrintSignature,
                                            Notation: SettinganCetakan.Notation,
                                            ActiveNonActive: SettinganCetakan.ActiveNonActive}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MPrintSetting',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd