angular.module('app')
  .factory('SiteAreaStockFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MSitesAreaStock');
        //console.log('res=>',res);
        return res;
      },
      create: function(SiteAreaStock) {
        return $http.post('/api/sales/MSitesAreaStock', [{
                            AreaStockName: SiteAreaStock.AreaStockName}]);
      },
      update: function(SiteAreaStock){
        return $http.put('/api/sales/MSitesAreaStock', [{
                                            OutletId: SiteAreaStock.OutletId,
                                            AreaStockId: SiteAreaStock.AreaStockId,
                                            AreaStockName: SiteAreaStock.AreaStockName,}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MSitesAreaStock',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });