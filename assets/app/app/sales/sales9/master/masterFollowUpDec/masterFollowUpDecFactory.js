angular.module('app')
  .factory('MasterFollowUpDecFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerReligion');
        return res;
      },
      create: function(MasterFollowUpDec) {
        return $http.post('/api/sales/CCustomerReligion', [{
                                            //AppId: 1,
                                            CustomerReligionName: MasterFollowUpDec.CustomerReligionName}]);
      },
      update: function(MasterFollowUpDec){
        return $http.put('/api/sales/CCustomerReligion', [{
                                            CustomerReligionId: MasterFollowUpDec.CustomerReligionId,
                                            CustomerReligionName: MasterFollowUpDec.CustomerReligionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd