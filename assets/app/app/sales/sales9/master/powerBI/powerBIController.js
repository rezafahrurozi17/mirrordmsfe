angular.module('app')
    .controller('PowerBIController', function($scope, $http, CurrentUser, PowerBIFactory,$timeout,bsNotify , $sce) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
		$scope.gridData=[];
		
		$scope.setProject(1);
		//angular.element(".ui.modal.loadingPowerBI").modal("refresh").modal("show");

	});
	
	// setTimeout(function() {
	// 	angular.element('.ui.modal.loadingPowerBI').modal('refresh').modal("hide");
	// }, 10000);
    //----------------------------------
    // Initialization
	//----------------------------------
	//alert('Sebaiknya menu ini dibuka via Dekstop/PC');
	bsNotify.show(
		{
			title: "Peringatan",
			content: 'Sebaiknya menu ini dibuka via Dekstop/PC',
			type: 'warning',
			timeout: 6000,
		}
	);
	$scope.user = CurrentUser.user();
    $scope.mAgama = null; //Model
	$scope.xRole={selected:[]};
	//console.log('$scope.user',$scope.user);

	 $scope.Dealer_ID = $scope.user.OrgCode.slice(0,3);
	// console.log('Dealer_ID',$scope.Dealer_ID);
	 $scope.Org_Code = $scope.user.OrgCode;
	// console.log('Org_Code',$scope.Org_Code);
	 $scope.Salesman_ID = $scope.user.EmployeeId;
	// console.log('Salesman_ID',$scope.Salesman_ID);

	var DateBI = new Date();
	
	try {
		DateBI = DateBI.getFullYear() + '-'
			+ ('0' + (DateBI.getMonth() + 1)).slice(-2) + '-'
			+ ('0' + DateBI.getDate()).slice(-2) + 'T'
			+ ((DateBI.getHours() < '10' ? '0' : '') + DateBI.getHours()) + ':'
			+ ((DateBI.getMinutes() < '10' ? '0' : '') + DateBI.getMinutes()) + ':'
			+ ((DateBI.getSeconds() < '10' ? '0' : '') + DateBI.getSeconds());
	}
	catch (e1) {
		DateBI = null;
	}

	//console.log('DateBI',DateBI);
	var TempYearBI = angular.copy(DateBI);
	var YearBI = TempYearBI.slice(0,4);
	var TempMonthBI = angular.copy(DateBI);
	var MonthBI = TempMonthBI.slice(5,7);
	var NewMonthBI = MonthBI.replace(/0/g,'');

	//$scope.MY = DateBI.slice(0, 10);
	//console.log('YearBI',YearBI);
	//console.log('MonthBI',MonthBI);
	//console.log('NewMonthBI',NewMonthBI);
	$scope.MY = YearBI + NewMonthBI;
	//console.log('$scope.MY',$scope.MY);


	if($scope.user.RoleName == 'SALES'){
		$scope.urlbi = 'https://dms.toyota.astra.co.id:7001/reports/powerbi/DMS_Report_Salesman?rs:embed=true&filter=STG_DMS_SUSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_PROSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_TARGET_SALESMAN/GroupDealer eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_SUSPECT/Outlet_ID eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_PROSPECT/Outlet_Code eq '+"'" + $scope.Org_Code+"'" + ' and STG_DMS_TARGET_SALESMAN/OutletCode eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_SUSPECT/Salesman_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_PROSPECT/Salesman_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_TARGET_SALESMAN/Salesman_ID eq '+"'"+$scope.Salesman_ID +"'"  +'' ;
	//console.log('$scope.urlbi',$scope.urlbi);
	}else
	if($scope.user.RoleName == 'SUPERVISOR SALES'){
		// $scope.urlbi = 'https://dms.toyota.astra.co.id:7001/reports/powerbi/DMS_Report_Supervisor?rs:embed=true&filter=STG_DMS_SUSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_PROSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_TARGET_SALESMAN/GroupDealer eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_SUSPECT/Outlet_ID eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_PROSPECT/Outlet_Code eq '+"'" + $scope.Org_Code+"'" + ' and STG_DMS_TARGET_SALESMAN/OutletCode eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_SUSPECT/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_PROSPECT/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_TARGET_SALESMAN/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" + ' and STG_DMS_TARGET_SPV/OutletCode eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_TARGET_ SPV/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +'' ;
		$scope.urlbi = 'https://dms.toyota.astra.co.id:7001/reports/powerbi/DMS_Report_Supervisor?rs:embed=true&filter=STG_DMS_SUSPECT/Outlet_ID eq '+ "'" + $scope.Org_Code+"'" +' and STG_DMS_PROSPECT/Outlet_Code eq '+"'" + $scope.Org_Code+"'" + ' and STG_DMS_TARGET_SALESMAN/OutletCode eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_SUSPECT/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_PROSPECT/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +' and STG_DMS_TARGET_SALESMAN/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" + ' and STG_DMS_TARGET_SPV/OutletCode eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_TARGET_SPV/SPV_ID eq '+"'"+$scope.Salesman_ID +"'" +'' ;
	//console.log('$scope.urlbi',$scope.urlbi);
	}else
	if($scope.user.RoleName == 'KACAB' || $scope.user.RoleName == 'Sales Manager'){
		$scope.urlbi = 'https://dms.toyota.astra.co.id:7001/reports/powerbi/DMS_Report_Kacab?rs:embed=true&filter=STG_DMS_SUSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_PROSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_TARGET_SALESMAN/GroupDealer eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_SUSPECT/Outlet_ID eq '+"'" + $scope.Org_Code+"'" +' and STG_DMS_PROSPECT/Outlet_Code eq '+"'" + $scope.Org_Code+"'" + ' and STG_DMS_TARGET_SALESMAN/OutletCode eq '+"'" + $scope.Org_Code+"'" + ' and STG_DMS_TARGET_SPV/OutletCode eq '+"'" + $scope.Org_Code+"'" + ' and RS_Salesman/Outlet eq '+"'" + $scope.Org_Code+"'" +'' ;
	//console.log('$scope.urlbi',$scope.urlbi);
	}else
	if($scope.user.RoleName == 'Admin HO'){
		$scope.urlbi = 'https://dms.toyota.astra.co.id:7001/reports/powerbi/DMS_Report_HeadOffice?rs:embed=true&filter=STG_DMS_SUSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_PROSPECT/Dealer_ID eq '+ "'" +$scope.Dealer_ID+"'" +' and STG_DMS_TARGET_SALESMAN/GroupDealer eq '+ "'" +$scope.Dealer_ID+"'" +'' ;
	//console.log('$scope.urlbi',$scope.urlbi);
	}

	// $scope.urlbi = 'https://vsv-c003-016105/reports/powerbi/DMS_Report_Salesman_tes?rs:embed=true&filter=STG_DMS_SUSPECT/Dealer_ID eq '+ $scope.Dealer_ID +' and STG_DMS_PROSPECT/Dealer_ID eq '+ $scope.Dealer_ID +' and STG_DMS_TARGET_SALESMAN/GroupDealer eq '+ $scope.Dealer_ID +' and STG_DMS_SUSPECT/Outlet_ID eq '+ $scope.Org_Code+' and STG_DMS_PROSPECT/Outlet_Code eq '+$scope.Org_Code + ' and STG_DMS_TARGET_SALESMAN/OutletCode eq '+ $scope.Org_Code +' and STG_DMS_SUSPECT/Salesman_ID eq '+$scope.Salesman_ID +' and STG_DMS_PROSPECT/Salesman_ID eq '+$scope.Salesman_ID +' and STG_DMS_TARGET_SALESMAN/Salesman_ID eq '+$scope.Salesman_ID +'' ;
	 console.log('$scope.urlbi',$scope.urlbi);



	$scope.projects = {

        1 : {
            "id" : 1,
            "name" : "Power BI",
            "url" : $scope.urlbi,
            "description" : "Power BI"
        }
    };
	
	$scope.setProject = function (id) {
		$scope.currentProject = $scope.projects[id];
		$scope.currentProjectUrl = $sce.trustAsResourceUrl($scope.currentProject.url);
	  }
	
    //----------------------------------
    // Get Data test
    //----------------------------------
    //  $scope.getData = function() {
    //     PowerBIFactory.getData().then(
    //         function(res){
    //             //$scope.grid.data = res.data.Result;
	// 			$scope.loading=false;
	// 			//console.log('sukses')
    //             //return res.data;
    //         },
    //         function(err){
    //             bsNotify.show(
	// 				{
	// 					title: "gagal",
	// 					content: "Data tidak ditemukan.",
	// 					type: 'danger'
	// 				}
	// 			);
    //         }
    //     );
    // }

    // function roleFlattenAndSetLevel(node,lvl){
    //     for(var i=0;i<node.length;i++){
    //         node[i].$$treeLevel = lvl;
    //         gridData.push(node[i]);
    //         if(node[i].child.length>0){
    //             roleFlattenAndSetLevel(node[i].child,lvl+1)
    //         }else{

    //         }
    //     }
    //     return gridData;
    // }
	
	// $scope.doCustomSave = function (mdl,mode) {

	// 	console.log('doCustomSave3',mode);//create update
		
	// 	if(mode=="create")
	// 	{
	// 		AgamaFactoryMaster.create($scope.mAgama).then(
	// 			function(res) {
	// 				AgamaFactoryMaster.getData().then(
	// 					function(res){
	// 						gridData = [];
	// 						$scope.grid.data = res.data.Result;
	// 						$scope.loading=false;
	// 						bsNotify.show(
	// 							{
	// 								title: "Sukses",
	// 								content: "Data berhasil disimpan",
	// 								type: 'success'
	// 							}
	// 						);
	// 						return res.data;
	// 					},
	// 					function(err){
	// 						bsNotify.show(
	// 							{
	// 								title: "Gagal",
	// 								content: "Data tidak ditemukan.",
	// 								type: 'danger'
	// 							}
	// 						);
	// 					}
	// 				);


	// 			},            
	// 					function(err){
							
	// 						bsNotify.show(
	// 							{
	// 								title: "Gagal",
	// 								content: "Sudah ada item dengan nama yang sama",
	// 								type: 'danger'
	// 							}
	// 						);
	// 					}
	// 		)
	// 	}
	// 	else if(mode=="update")
	// 	{
	// 		AgamaFactoryMaster.update($scope.mAgama).then(
	// 			function(res) {
	// 				AgamaFactoryMaster.getData().then(
	// 					function(res){
	// 						gridData = [];
	// 						$scope.grid.data = res.data.Result;
	// 						$scope.loading=false;
	// 						bsNotify.show(
	// 							{
	// 								title: "Sukses",
	// 								content: "Data berhasil disimpan",
	// 								type: 'success'
	// 							}
	// 						);
	// 						return res.data;
	// 					},
	// 					function(err){
	// 						bsNotify.show(
	// 							{
	// 								title: "Gagal",
	// 								content: "Data tidak ditemukan.",
	// 								type: 'danger'
	// 							}
	// 						);
	// 					}
	// 				);


	// 			},            
	// 					function(err){
							
	// 						bsNotify.show(
	// 							{
	// 								title: "Gagal",
	// 								content: "Sudah ada item dengan nama yang sama",
	// 								type: 'danger'
	// 							}
	// 						);
	// 					}
	// 		)
	// 	}
		
        
    //     $scope.formApi.setMode("grid");
    // }
	
    // $scope.selectRole = function(rows){
    //     $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    // }
    // $scope.onSelectRows = function(rows){
    // }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // $scope.grid = {
    //     enableSorting: true,
    //     enableRowSelection: true,
    //     multiSelect: true,
    //     enableSelectAll: true,
    //     //showTreeExpandNoChildren: true,
    //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //     // paginationPageSize: 15,
    //     columnDefs: [
    //         { name:'Customer Religion Id',    field:'CustomerReligionId', width:'7%', visible:false},
    //         { name:'Nama Agama', field:'CustomerReligionName' }
    //     ]
    // };
});


