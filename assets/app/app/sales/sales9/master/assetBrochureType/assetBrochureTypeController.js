angular.module('app')
    .controller('AssetBrochureTypeController', function($scope, $http, CurrentUser, AssetBrochureTypeFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAssetBrochureType = null;


    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
      $scope.getData = function() {
        $scope.loading = true;
        AssetBrochureTypeFactory.getData()
            .then(
                function(res){
                    $scope.grid.data = res.data.Result;
                }
            )
                $scope.loading=false;
                return $scope.grid.data;
        },
        function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
        };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			AssetBrochureTypeFactory.create($scope.mAssetBrochureType).then(
				function(res) {
					AssetBrochureTypeFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			AssetBrochureTypeFactory.update($scope.mAssetBrochureType).then(
				function(res) {
					AssetBrochureTypeFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }

    $scope.selectRole = function(rows) {

            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {

            }
	
	
			$scope.onBulkDeleteTipeBrosur = function (id) {
				console.log('iddelete',id);
				var arrayDelete = [];
				for(var i in id) {
					arrayDelete.push(id[i].BrochureTypeId)
				}
				console.log('arraydel',arrayDelete);
				AssetBrochureTypeFactory.delete(arrayDelete).then(
					function (res) {
						AssetBrochureTypeFactory.getData().then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil dihapus",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan",
										type: 'danger'
									}
								);
							}
						);
					},
					function (err) {
						bsNotify.show(
							{
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							}
						);
					}
				)
			}
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Brochure Type Id',field:'BrochureTypeId', width:'7%', visible:false },
   			{ name:'Nama Tipe Brosur',  field: 'BrochureTypeName' },
            
        ]
    };
});
