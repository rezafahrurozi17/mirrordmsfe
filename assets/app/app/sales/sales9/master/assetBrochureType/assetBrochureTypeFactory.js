angular.module('app')
  .factory('AssetBrochureTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    console.log("user",currentUser);
    return {
      getData: function() {
        var res = $http.get('/api/sales/MAssetBrochureType');
        return res;
      },
      create: function(AssetBrochure) {
        return $http.post('/api/sales/MAssetBrochureType', [{
                                            BrochureTypeName:AssetBrochure.BrochureTypeName,
                                            }]);
      },
      update: function(AssetBrochure){
        return $http.put('/api/sales/MAssetBrochureType', [{
                                            OutletId: AssetBrochure.OutletId,
                                            BrochureTypeId: AssetBrochure.BrochureTypeId,
                                            BrochureTypeName:AssetBrochure.BrochureTypeName,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAssetBrochureType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd.