angular.module('app')
  .factory('PenggunaanKendaraanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/VehicleUsage');
        return res;
      },
      create: function(PenggunaanKendaraan) {
        return $http.post('/api/sales/VehicleUsage', [{
                                            //AppId: 1,
											//PurchasePurposeCode: PenggunaanKendaraan.PurchasePurposeCode,
                                            PurchasePurposeName: PenggunaanKendaraan.PurchasePurposeName}]);
      },
      update: function(PenggunaanKendaraan){
        return $http.put('/api/sales/VehicleUsage', [{
                                            PurchasePurposeId: PenggunaanKendaraan.PurchasePurposeId,
                                            PurchasePurposeName: PenggunaanKendaraan.PurchasePurposeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/VehicleUsage',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd