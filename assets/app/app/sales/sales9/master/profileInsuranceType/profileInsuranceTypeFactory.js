angular.module('app')
  .factory('ProfileInsuranceTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{InsuranceTypeId: "1", InsuranceTypeName: "Name 01", InsuranceTypeValue: "Value 01"},
				{InsuranceTypeId: "2", InsuranceTypeName: "Name 02", InsuranceTypeValue: "Value 02"},
        {InsuranceTypeId: "3", InsuranceTypeName: "Name 03", InsuranceTypeValue: "Value 03"},

			  ];
        res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/fw/Role', [{
                                            NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/fw/Role', [{
                                            ChecklistItemAdministrasiPembayaranId:ActivityChecklistItemAdministrasiPembayaran.ChecklistItemAdministrasiPembayaranId,
											NamaChecklistItemAdministrasiPembayaran:ActivityChecklistItemAdministrasiPembayaran.NamaChecklistItemAdministrasiPembayaran,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd