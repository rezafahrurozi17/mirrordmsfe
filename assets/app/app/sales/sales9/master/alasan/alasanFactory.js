angular.module('app')
  .factory('AlasanFactory', function($http, CurrentUser, $httpParamSerializer, bsNotify) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(da_value) {
        if (da_value == undefined || da_value == null){
          var res=$http.get('/api/sales/Reason');
        }else if(da_value != undefined || da_value != null){
          var res=$http.get('/api/sales/Reason/?start=1&limit=100000&filterData=ReasonCategoryCode|'+da_value);
        }
		    return res;
      },
      getDataSearchBy: function(da_value) {
      var res=$http.get('/api/sales/Reason/?start=1&limit=100000&filterData=ReasonCategoryCode|'+da_value);
          return res;
      },
      getDataCategory: function() {
          var res=$http.get('/api/sales/Lookup/MReasonCategoryTM');
          return res;
      },
      create: function(Alasan) {
        return $http.post('/api/sales/Reason', [{
                                            //AppId: 1,
											ReasonCategoryCode: Alasan.ReasonCategoryCode,
                                            ReasonName: Alasan.ReasonName}]);
      },
      update: function(Alasan){
        return $http.put('/api/sales/Reason', [{
                                            ReasonId: Alasan.ReasonId,
											ReasonCategoryCode: Alasan.ReasonCategoryCode,
                                            ReasonName: Alasan.ReasonName}]);
      },
      delete: function(Alasan) {
        //console.log(Alasan);
        var result =  $http.delete('/api/sales/Reason/Delete',{data:Alasan,headers: {'Content-Type': 'application/json'}});
        return result;
      },
    }
  });
