angular.module('app')
	.controller('AlasanController', function ($scope, $http, CurrentUser, AlasanFactory, $timeout, bsNotify, $httpParamSerializer) {
		$scope.mAlasan = null; 
		var OperationAlasan = "";

		$scope.Alasan = true;
		$scope.ShowAlasanSearch = false;
		$scope.filter = {};
		$scope.filter.AlasanFilterByReasonCode = null;

		AlasanFactory.getDataCategory().then(function (res) {
			$scope.optionsAlasanCategory = res.data.Result;
			return $scope.optionsAlasanCategory;
		});


		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = false;
			$scope.gridData = [];
		});

		$scope.$on('$destroy', function () {
			angular.element('.ui.modal.ModalAlasanHapus').remove();
		});


		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.mAlasan = null; //Model
		$scope.mainmenu = true;
		$scope.xRole = { selected: [] };
		//$scope.mode = {};
		//$scope.RefreshAlasan();
		//----------------------------------
		// Get Data
		//----------------------------------
		var gridData = [];
		$scope.getData = function () {
			//var filter = $httpParamSerializer($scope.filter.AlasanFilterByReasonCode);

			console.log("test id", $scope.filter.AlasanFilterByReasonCode);
			AlasanFactory.getData($scope.filter.AlasanFilterByReasonCode).then(
				function (res) {
					gridData = [];
					$scope.grid.data = res.data.Result; //nanti balikin
					$scope.loading = false;
				},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			)
		};
		
		$scope.KategoriAlasanChanged = function (selected) {
			var da_maksimum=30;
			//console.log("setan",selected.ReasonCode);"003"
			if(selected.ReasonCode=="001")
			{
				da_maksimum=50;
			}
			else if(selected.ReasonCode=="002")
			{
				da_maksimum=50;
			}
			else if(selected.ReasonCode=="003")
			{
				da_maksimum=50;
			}
			else if(selected.ReasonCode=="004")
			{
				da_maksimum=30;
			}
			else if(selected.ReasonCode=="005")
			{
				da_maksimum=30;
			}
			else if(selected.ReasonCode=="006")
			{
				da_maksimum=100;
			}
			else if(selected.ReasonCode=="007")
			{
				da_maksimum=50;
			}
			else if(selected.ReasonCode=="008")
			{
				da_maksimum=30;
			}
			else if(selected.ReasonCode=="009")
			{
				da_maksimum=30;
			}
			else if(selected.ReasonCode=="010")
			{
				da_maksimum=250;
			}
			else if(selected.ReasonCode=="012")
			{
				da_maksimum=30;
			}
			else if(selected.ReasonCode=="013")
			{
				da_maksimum=200;
			}
			else
			{
				da_maksimum=30;
			}
			document.getElementById("AlasanDaAlasan").maxLength = da_maksimum;
			
			var isi_alasan=document.getElementById("AlasanDaAlasan").value;
			if(isi_alasan.length>da_maksimum)
			{
				var potong=isi_alasan.substring(0, da_maksimum);
				document.getElementById("AlasanDaAlasan").value=potong;
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Untuk kategori yang anda pilih maksimum "+da_maksimum+" karakter",
							type: 'warning'
						}
					);
				//$scope.mAlasan_ReasonName=$scope.mAlasan_ReasonName.substring(0, da_maksimum);
			}
		}
		
		$scope.MasterAlasanPaksaanReasonName = function () {
			$scope.mAlasan_ReasonName=angular.copy(document.getElementById("AlasanDaAlasan").value);
		}
		
		

		$scope.TambahAlasan = function () {
			OperationAlasan = "Insert";
			$scope.ShowAlasanDetail = true;
			$scope.mainmenu = false;
			$scope.Alasan = false;
			$scope.ShowSimpanAlasan = true;
			$scope.mAlasan_Category = "";
			$scope.mAlasan_ReasonName = "";
			$scope.mAlasan_ReasonName_EnableDisable = true;
			$scope.mAlasan_Category_EnableDisable = true;
		}

		$scope.CariAlasan = function () {
			AlasanFactory.getDataSearchBy($scope.filter.AlasanFilterByReasonCode).then(
				function (res) {
					gridData = [];
					$scope.grid.data = res.data.Result;
					$scope.loading = false;
				},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			)
		}

		$scope.RefreshAlasan = function () {
			AlasanFactory.getData().then(
				function (res) {
					gridData = [];
					$scope.grid.data = res.data.Result;
					$scope.loading = false;
				},
				function (err) {
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			)
		}

		$scope.ToggleAlasanSearch = function () {
			$scope.ShowAlasanSearch = !$scope.ShowAlasanSearch;
		}

		$scope.hapusAlasan = function(){
			console.log("data alasan", $scope.selectedRows);
			var length = $scope.selectedRows.length;
			AlasanFactory.delete($scope.selectedRows).then(
				function (res) {
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil dihapus.",
							type: 'success'
						}
					);
					$scope.selectedRows = [];
					$scope.getData();
				
			})
		}

		$scope.HapusAlasanIndividu = function (da_Selected_row) {

			setTimeout(function () {
				angular.element('.ui.small.modal.ModalAlasanHapus').modal('setting', { closable: false }).modal('show');
				angular.element('.ui.small.modal.ModalAlasanHapus').not(':first').remove();
			}, 1);

			$scope.IdToDeleteInividu = [{ ReasonId: da_Selected_row.ReasonId, ReasonCategoryCode: da_Selected_row.ReasonCategoryCode }];

		}

		$scope.DeleteAlasan = function () {


			// $scope.IdToDelete=[{ ReasonId: null,ReasonCategoryCode: null}];
			// $scope.IdToDelete.splice(0, 1);

			// for(var i = 0; i < $scope.selctedRow.length; ++i)
			// {
			// $scope.IdToDelete.push({ ReasonId: $scope.selctedRow[i].ReasonId,ReasonCategoryCode: $scope.selctedRow[i].ReasonCategoryCode});	
			// }
			// AlasanFactory.delete($scope.IdToDelete).then(function () {
			// AlasanFactory.getData().then(
			// function(res){
			// gridData = [];
			// $scope.grid3.data = res.data.Result;
			// $scope.loading=false;
			// },
			// function(err){
			// bsNotify.show(
			// {
			// title: "gagal",
			// content: "Data tidak ditemukan.",
			// type: 'danger'
			// }
			// );
			// }
			// )
			// });
		}

		$scope.HapusAlasanCancel = function () {
			angular.element('.ui.small.modal.ModalAlasanHapus').modal('hide');
		}

		$scope.HapusAlasanOk = function () {
			//$scope.IdToDelete.splice(0, 1);
			// for(var i = 0; i < $scope.selctedRow.length; ++i)
			// {
			// $scope.IdToDelete.push({ ReasonId: $scope.selctedRow[i].ReasonId,ReasonCategoryCode: $scope.selctedRow[i].ReasonCategoryCode});	
			// }
			AlasanFactory.delete($scope.IdToDeleteInividu).then(function () {
				AlasanFactory.getData().then(
					function (res) {
						gridData = [];
						$scope.grid.data = res.data.Result;
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Data berhasil dihapus.",
								type: 'success'
							}
						);
						angular.element('.ui.modal.ModalAlasanHapus').modal('hide');
						$scope.loading = false;
						$scope.mainmenu = true;
					},
					function (err) {
						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				)
			});

		}

		$scope.KembaliDariAlasan = function () {
			$scope.ShowAlasanDetail = false;
			$scope.mainmenu = true;
			$scope.Alasan = true;
		}

		$scope.LihatAlasan = function (SelectedData) {
			OperationAlasan = "Look";
			$scope.ShowAlasanDetail = true;
			$scope.mainmenu = false;
			$scope.Alasan = false;
			$scope.ShowSimpanAlasan = false;
			$scope.mAlasan_ReasonId = SelectedData.ReasonId;
			$scope.mAlasan_ReasonName = SelectedData.ReasonName;
			$scope.mAlasan_Category = SelectedData.ReasonCategoryCode;
			$scope.mAlasan_ReasonName_EnableDisable = false;
			$scope.mAlasan_Category_EnableDisable = false;
		}

		$scope.UpdateAlasan = function (SelectedData) {
			console.log("test", $scope.mode);
			$SelectedAlasan = SelectedData;
			OperationAlasan = "Update";
			$scope.ShowAlasanDetail = true;
			$scope.mainmenu = false;
			$scope.Alasan = false;
			$scope.ShowSimpanAlasan = true;
			$scope.mAlasan_ReasonId={};
			$scope.mAlasan_ReasonName={};
			$scope.mAlasan_Category={};
			$scope.mAlasan_ReasonId = angular.copy(SelectedData.ReasonId);
			$scope.mAlasan_ReasonName = angular.copy(SelectedData.ReasonName);
			$scope.mAlasan_Category = angular.copy(SelectedData.ReasonCategoryCode);
			$scope.mAlasan_ReasonName_EnableDisable = true;
			$scope.mAlasan_Category_EnableDisable = false;
			
			var da_maksimum=30;
			if($scope.mAlasan_Category=="001")
			{
				da_maksimum=50;
			}
			else if($scope.mAlasan_Category=="002")
			{
				da_maksimum=50;
			}
			else if($scope.mAlasan_Category=="003")
			{
				da_maksimum=50;
			}
			else if($scope.mAlasan_Category=="004")
			{
				da_maksimum=30;
			}
			else if($scope.mAlasan_Category=="005")
			{
				da_maksimum=30;
			}
			else if($scope.mAlasan_Category=="006")
			{
				da_maksimum=100;
			}
			else if($scope.mAlasan_Category=="007")
			{
				da_maksimum=50;
			}
			else if($scope.mAlasan_Category=="008")
			{
				da_maksimum=30;
			}
			else if($scope.mAlasan_Category=="009")
			{
				da_maksimum=30;
			}
			else if($scope.mAlasan_Category=="010")
			{
				da_maksimum=250;
			}
			else if($scope.mAlasan_Category=="012")
			{
				da_maksimum=30;
			}
			else if($scope.mAlasan_Category=="013")
			{
				da_maksimum=200;
			}
			else
			{
				da_maksimum=30;
			}
			document.getElementById("AlasanDaAlasan").maxLength = da_maksimum;

		}

		$scope.SimpanAlasan = function () {
			$scope.mAlasan_ReasonName=angular.copy(document.getElementById("AlasanDaAlasan").value);
			if (OperationAlasan == "Insert") {
				$scope.AlasanToInsert = { 'ReasonName': $scope.mAlasan_ReasonName, 'ReasonCategoryCode': $scope.mAlasan_Category };
				AlasanFactory.create($scope.AlasanToInsert).then(function () {
					if ($scope.filter.AlasanFilterByReasonCode != null && (typeof $scope.filter.AlasanFilterByReasonCode != "undefined")) {
						AlasanFactory.getDataSearchBy($scope.filter.AlasanFilterByReasonCode).then(
							function (res) {
								gridData = [];
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						)
					}
					else {
						AlasanFactory.getData().then(
							function (res) {
								gridData = [];
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						)
					}
				$scope.AlasanToInsert = {};
				$scope.KembaliDariAlasan();
				}, function (err) {
					console.log("setan", err);
					bsNotify.show(
						{
							title: "gagal",
							content: err.data.Message,
							type: 'danger'
						}
					);
				});
				//alert('simpan');
				
			}
			else if (OperationAlasan == "Update") {
				$scope.AlasanToUpdate = { 'ReasonId': $scope.mAlasan_ReasonId, 'ReasonName': $scope.mAlasan_ReasonName, 'ReasonCategoryCode': $scope.mAlasan_Category };
				AlasanFactory.update($scope.AlasanToUpdate).then(function () {
					if ($scope.filter.AlasanFilterByReasonCode != null && (typeof $scope.filter.AlasanFilterByReasonCode != "undefined")) {
						AlasanFactory.getDataSearchBy($scope.filter.AlasanFilterByReasonCode).then(
							function (res) {
								gridData = [];
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						)
					}
					else {
						AlasanFactory.getData().then(
							function (res) {
								gridData = [];
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						)
					}
					$scope.KembaliDariAlasan();
				}, function (err) {
					bsNotify.show(
						{
							title: "gagal",
							content: err.data.Message,
							type: 'danger'
						}
					);
				});
				
			}

		}
		
		$scope.onSelectRows = function(row){
			$scope.selectedRow = angular.copy(row);
		}

		//----------------------------------
		// Grid Setup
		//----------------------------------

		AlasanFactory.getData().then(
			function (res) {
				gridData = [];
				$scope.grid.data = res.data.Result; //nanti balikin
				$scope.loading = false;
			},
			function (err) {
				bsNotify.show(
					{
						title: "Gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
			}
		)

		$scope.onSelectRows = function(rows) {
			$scope.selectedRows = angular.copy(rows)
        }

		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			enableColumnResizing: true,
			//showTreeExpandNoChildren: true,
			//paginationPageSizes: [10],
			//paginationPageSize: 10,
			//data:AlasanFactory.getData(),
			columnDefs: [
				{ name: 'AlasanId', field: 'AlasanId', visible: false },
				{ name: 'Kode Alasan', width: '12%', field: 'ReasonCategoryCode' },
				{ name: 'Kategori Alasan', field: 'ReasonCategory' },
				{ name: 'Alasan', field: 'ReasonName' },
				{ name: 'Action', visible: true, field: '', width: '12%', cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatAlasan(row.entity)" ></i>	<i class="fa fa-fw fa-lg fa-pencil" uib-tooltip="Ubah" tooltip-placement="bottom" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateAlasan(row.entity)" ></i> <i ng-show="false" class="fa fa-fw fa-lg fa-times" uib-tooltip="Hapus" tooltip-placement="left" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.HapusAlasanIndividu(row.entity)" ></i>' },

			]
		};

		$scope.grid.onRegisterApi = function (gridApi) {
			$scope.gridApi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.selctedRow = gridApi.selection.getSelectedRows();
			});

			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				$scope.selctedRow = gridApi.selection.getSelectedRows();
			});
		};

	});
