angular.module('app')
  .factory('CustomerListVehicleFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var data = [
          {
            "CustomerDetailVehicleId" : 2,
            "CustomerId" : "2",
            "VehicleTypeColorId" : "1234",
            "PoliceNumber" : "B 5672 LFK",
            "YearBuy" : "2016",
            "BrandId" : "2",
            "VehicleCategoryId" : "2",
            "YearId" : "PDC 2",
            "BuyPrice" : "200000000"
          },
          {
            "CustomerDetailVehicleId" : 2,
            "CustomerId" : "2",
            "VehicleTypeColorId" : "34",
            "PoliceNumber" : "B 7738 YTS",
            "YearBuy" : "2014",
            "BrandId" : "2",
            "VehicleCategoryId" : "2",
            "YearId" : "2",
            "BuyPrice" : "185000000"
          }
        ];
        res = data;
        return res;
      },
      create: function(pdc) {
        return $http.post('/api/fw/Role', [{
                                            NamaSitePDC : pdc.NamaSitePDC,
                                            }]);
      },
      update: function(pdc){
        return $http.put('/api/fw/Role', [{
                                            SitePDCId : pdc.SitePDCId,
                                            NamaSitePDC : pdc.NamaSitePDC,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 









