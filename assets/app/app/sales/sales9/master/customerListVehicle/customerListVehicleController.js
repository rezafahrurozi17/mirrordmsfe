angular.module('app')
    .controller('CustomerListVehicleController', function($scope, $http, CurrentUser, CustomerListVehicleFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCustomerListVehicle = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=CustomerListVehicleFactory.getData();         
        $scope.loading=false;
    },
    function(err){
       bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
    };
    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }
            else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'CustomerDetailVehicleId',    field:'CustomerDetailVehicleId', width:'7%', visible:false },
            { name:'CustomerId', field:'CustomerId', visible:false },
            { name:'VehicleTypeColorId', field:'VehicleTypeColorId', visible:false },
            { name:'Police Number', field:'PoliceNumber' },
            { name:'Year Buy', field:'YearBuy' },
            { name:'BrandId', field:'BrandId', visible:false },
            { name:'VehicleCategoryId', field:'VehicleCategoryId', visible:false },
            { name:'YearId', field:'YearId', visible:false },
            { name:'Buy Price', field:'BuyPrice' }
        ]
    };
});


    







