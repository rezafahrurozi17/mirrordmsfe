angular.module('app')
    .controller('ChecklistInspeksiController', function($scope, $http, CurrentUser, ChecklistInspeksiFactoryMaster,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
	
	var OperationChecklistInspeksi="";
	$scope.ChecklistInspeksiMain=true;
	$scope.ChecklistInspeksiDetail=false;

	
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mChecklistInspeksi = null; //Model
    $scope.xRole={selected:[]};
	$scope.filter={ VehicleModelId:null, GroupChecklistInspectionName:null};
    

    //----------------------------------
    // Get Data test
    //----------------------------------
	
	ChecklistInspeksiFactoryMaster.getDataVehicleModel().then(function(res) {
            $scope.optionsModelChecklistInspeksi = res.data.Result;
            return $scope.optionsModelChecklistInspeksi;
        });
	
	ChecklistInspeksiFactoryMaster.getDataChecklist().then(function (res) {
			$scope.optionsGrupChecklistInspeksi = res.data.Result;
			return $scope.optionsGrupChecklistInspeksi;
		});

	
	$scope.getData = function () {
			ChecklistInspeksiFactoryMaster.getData($scope.filter).then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$scope.loading = false;
				},
				function (err) {
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
	
	$scope.RemoveChecklistInspeksi = function (index) {
		$scope.mChecklistInspeksi.ListItemInspection.splice(index, 1);
		
		
    }
	
	$scope.KembaliChecklistInspeksi = function() {
			$scope.ChecklistInspeksiDetail=false;
			$scope.ChecklistInspeksiMain=true;
			$scope.mChecklistInspeksiDaName="";
        }
	
	$scope.AddChecklistInspeksiChild = function (da_shitty_id,da_child_name,da_outlet_id) {
		
		try
		{
			var aman=true;
			for(var i = 0; i < $scope.mChecklistInspeksi.ListItemInspection.length; ++i)
			{	
				if($scope.mChecklistInspeksi.ListItemInspection[i].ChecklistItemInspectionName == da_child_name)
				{
					aman=false;
					break;
				}
			}
			
			
			if(aman==true)
			{
				$scope.mChecklistInspeksi.ListItemInspection.push({ GroupChecklistInspectionId: da_shitty_id,GroupChecklistInspectionName:$scope.mChecklistInspeksi.GroupChecklistInspectionName,ChecklistItemInspectionName:da_child_name,OutletId:da_outlet_id});
			}
			else if(aman==false)
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Sudah ada detail dengan nama yang sama.",
						type: 'warning'
					}
				);
			}
			
		}
		catch(err)
		{
			$scope.mChecklistInspeksi.ListItemInspection=[{ GroupChecklistInspectionId: null,GroupChecklistInspectionName:null,ChecklistItemInspectionName:null,OutletId:null}];
			$scope.mChecklistInspeksi.ListItemInspection.splice(0, 1);
			$scope.mChecklistInspeksi.ListItemInspection.push({ GroupChecklistInspectionId: da_shitty_id,GroupChecklistInspectionName:$scope.mChecklistInspeksi.GroupChecklistInspectionName,ChecklistItemInspectionName:da_child_name,OutletId:da_outlet_id});
		}								
							
		
    }
	
	$scope.SimpanChecklistInspeksi = function () {	
		ChecklistInspeksiFactoryMaster.create($scope.mChecklistInspeksi).then(function () {
					ChecklistInspeksiFactoryMaster.getData($scope.filter).then(
						function(res){
							 gridData = [];
							 $scope.grid.data = res.data.Result; //nanti balikin
							 $scope.loading=false;
							 bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							 $scope.KembaliChecklistInspeksi();
						 },
						 function(err){
							 bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						 }
					)
				});
    }
	$scope.ShowBtnSimpan = true;
	$scope.ShowBtnKembali = false;
	$scope.DisabledEdit = true;
	$scope.LihatChecklistInspeksi = function(SelectedData) {
			OperationChecklistInspeksi="Look";
			$scope.ChecklistInspeksiDetail=true;
			$scope.ChecklistInspeksiMain=false;
			$scope.mChecklistInspeksi=angular.copy(SelectedData);
			$scope.mChecklistInspeksi_Field_EnableDisable=true;

			$scope.ShowBtnSimpan = false;
			$scope.ShowBtnKembali = true;
			$scope.DisabledEdit = true;

			
        }
		
		
		
	$scope.UpdateChecklistInspeksi = function(SelectedData) {
			$scope.SelectedTipeRole=SelectedData;
			OperationChecklistInspeksi="Update";
			$scope.ChecklistInspeksiDetail=true;
			$scope.ChecklistInspeksiMain=false;
			$scope.mChecklistInspeksi=angular.copy(SelectedData);
			$scope.mChecklistInspeksi_Field_EnableDisable=false;

			$scope.ShowBtnSimpan = true;
			$scope.ShowBtnKembali = false;
			$scope.DisabledEdit = false;
        }	
	
    // ----------------------------------
    // Grid Setup
    // ----------------------------------
	
	$scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
			//data:TipeRoleFactory.getData(),
            columnDefs: [
				{ name:'Id',    field:'ChecklistItemInspectionId', width:'7%', visible:false},
				// { name:'Vehicle Model', field:'VehicleModelName' },
				{ name:'Nama Grup Checklist Inspeksi', field:'GroupChecklistInspectionName' },
				{ name:'Deskripsi', field:'GroupChecklistInspectionDescription' },
                { name:'Action', field: '', width: '20%',enableFiltering: false,visible:true , cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatChecklistInspeksi(row.entity)" title="Lihat" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateChecklistInspeksi(row.entity)" title="Ubah"></i>'},

            ]
        };
		
	
		
	// $scope.grid.onRegisterApi = function(gridApi){
                // $scope.gridApi = gridApi;
                // gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    // $scope.selectedRows = gridApi.selection.getSelectedRows();
                // });

                // gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                   // $scope.selectedRows = gridApi.selection.getSelectedRows();
                // });
        // };
});


