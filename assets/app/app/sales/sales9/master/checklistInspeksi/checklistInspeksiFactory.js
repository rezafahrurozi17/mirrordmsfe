angular.module('app')
  .factory('ChecklistInspeksiFactoryMaster', function($http, CurrentUser,$httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/MActivityCheckListItemMasterDetail/filter/?'+param);
        return res;
      },
	  getDataVehicleModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas'); 
        return res;
      },
	  
	  getDataChecklist: function() {
        var res=$http.get('/api/sales/MActivityGroupCheckListInspection');
		//aslinya var res=$http.get('/api/sales/MActivityChecklistItemMasterDetail');
        return res;
      },
	  
      create: function(ChecklistInspeksi) {
        return $http.post('/api/sales/MActivityCheckListItemMasterDetail', [{
                                            //AppId: 1,
											OutletId: ChecklistInspeksi.OutletId,
											GroupChecklistInspectionId: ChecklistInspeksi.GroupChecklistInspectionId,
											GroupChecklistInspectionName: ChecklistInspeksi.GroupChecklistInspectionName,
											GroupChecklistInspectionDescription: ChecklistInspeksi.GroupChecklistInspectionDescription,
											VehicleModelId: ChecklistInspeksi.VehicleModelId,
											VehicleModelName: ChecklistInspeksi.VehicleModelName,
											VehicleTypeId: ChecklistInspeksi.VehicleTypeId,
											VehicleTypeName: ChecklistInspeksi.VehicleTypeName,
                                            ListItemInspection: ChecklistInspeksi.ListItemInspection}]);
      },
      update: function(ChecklistInspeksi){
        return $http.put('/api/sales/MActivityChecklistItemInspection', [{
											InputByTAMBit: false,
											GroupChecklistInspectionId: ChecklistInspeksi.GroupChecklistInspectionId,
                                            ListItemInspection: ChecklistInspeksi.ListItemInspection}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityGroupCheckListInspection',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd