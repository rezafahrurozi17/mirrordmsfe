angular.module('app')
	.factory('RatioSalesFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getData: function(start, limit){
			var url = '/api/sales/SalesMasterRatioSales/GetData/?start=' + start + '&limit=' + limit + '&outletid=' + user.OutletId;
			console.log("url Master Ratio Sales : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(inputData){
			var url = '/api/sales/SalesMasterRatioSales/Create/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});