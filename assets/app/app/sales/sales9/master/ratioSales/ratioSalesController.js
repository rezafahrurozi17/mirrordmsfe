var app = angular.module('app');
app.controller('RatioSalesController', function ($scope, $http, $filter, CurrentUser, RatioSalesFactory, uiGridConstants, bsNotify, $timeout) {
	var user = CurrentUser.user();
	$scope.uiGridPageSize = 10;
	var cellEdit = false;
	$scope.MasterRatioSales_Edit_Show = true;
	
	$scope.MasterRatioSales_Simpan_Clicked = function(){
		
		RatioSalesFactory.create($scope.RatioSales_UIGrid.data)
		.then(
			function(res){
				bsNotify.show(
					{
						title: "Error Message",
						content: "Data Berhasil Disimpan",
						type: 'success'
					}
				);
				$scope.MasterRatioSales_Batal_Clicked();
			}
		);
	}
	
	$scope.MasterRatioSalesEdit = function() { return cellEdit; };
	
	$scope.MasterRatioSales_Edit_Clicked = function(){
		cellEdit = true;
		$scope.MasterRatioSales_Edit_Show = false;
	}
	
	$scope.MasterRatioSales_Batal_Clicked = function(){
		cellEdit = false;
		$scope.MasterRatioSales_Edit_Show = true;
		$scope.RatioSales_UIGrid_Paging(1);
	}
	
	$scope.RatioSales_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination : true,
		rowSelection : true,
		multiSelect : false,
		columnDefs:[
					{name:"RatioSalesId",field:"RatioSalesId",visible:false},
					{name:"VehicleModelId",field:"VehicleModelId", visible:false},
					{name:"Model", field:"VehicleModelName", enableCellEdit:false},
					{name:"RS to SPK (%)", field:"RStoSPK", type: 'number', min: 0, cellEditableCondition:$scope.MasterRatioSalesEdit},
					{name:"SPK to Hot Prospect (%)", field:"SPKtoHotProspect", type: 'number', min: 0, cellEditableCondition:$scope.MasterRatioSalesEdit},
					{name:"Hot Prospect to Prospect (%)", field:"HotProspecttoProspect", type: 'number', min: 0, cellEditableCondition:$scope.MasterRatioSalesEdit},
					{name:"Prospect to Suspect (%)", field:"ProspecttoSuspect", type: 'number', min: 0, cellEditableCondition:$scope.MasterRatioSalesEdit},
				],
		onRegisterApi: function(gridApi) {
			$scope.GridApiRatioSales = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.RatioSales_UIGrid_Paging(pageNumber);
			});
		}
	};
	
	$scope.RatioSales_UIGrid_Paging = function(pageNumber){
		  RatioSalesFactory.getData(pageNumber, $scope.uiGridPageSize)
		  .then(
			function(res)
			{
				$scope.RatioSales_UIGrid.data = res.data.Result;
				$scope.RatioSales_UIGrid.totalItems = res.data.Total;
				$scope.RatioSales_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.RatioSales_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			
		);
	}
	
	$scope.RatioSales_UIGrid_Paging(1);
});