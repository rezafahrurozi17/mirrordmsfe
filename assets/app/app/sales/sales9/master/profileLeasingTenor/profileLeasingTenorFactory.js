angular.module('app')
  .factory('ProfileLeasingTenorFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileLeasingTenor');
        //console.log('res=>',res);
        var da_json=[
				{LeasingTenorId: "1", LeasingTenorTime: 1},
				{LeasingTenorId: "2", LeasingTenorTime: 2},
				{LeasingTenorId: "3", LeasingTenorTime: 3},

			  ];
        //res=da_json;
        return res;
      },
      create: function(ActivityChecklistItemAdministrasiPembayaran) {
        return $http.post('/api/sales/MProfileLeasingTenor', [{
                                            LeasingTenorTime:ActivityChecklistItemAdministrasiPembayaran.LeasingTenorTime,
				
                                            }]);
      },
      update: function(ActivityChecklistItemAdministrasiPembayaran){
        return $http.put('/api/sales/MProfileLeasingTenor', [{
                                            LeasingTenorId:ActivityChecklistItemAdministrasiPembayaran.LeasingTenorId,
											OutletId:ActivityChecklistItemAdministrasiPembayaran.OutletId,
											LeasingTenorTime:ActivityChecklistItemAdministrasiPembayaran.LeasingTenorTime,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileLeasingTenor',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd