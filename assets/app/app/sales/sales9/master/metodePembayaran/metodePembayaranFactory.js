angular.module('app')
  .factory('MasterMetodePembayaranFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FFinancePaymentType');
        return res;
      },
      create: function(MetodePembayaran) {
        return $http.post('/api/sales/FFinancePaymentType', [{
                                            //AppId: 1,
                                            PaymentTypeName: MetodePembayaran.PaymentTypeName}]);
      },
      update: function(MetodePembayaran){
        return $http.put('/api/sales/FFinancePaymentType', [{
                                            OutletId: MetodePembayaran.OutletId,
											PaymentTypeId: MetodePembayaran.PaymentTypeId,
                                            PaymentTypeName: MetodePembayaran.PaymentTypeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FFinancePaymentType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd