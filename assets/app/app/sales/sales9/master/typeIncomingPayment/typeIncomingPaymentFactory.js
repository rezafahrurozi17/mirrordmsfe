angular.module('app')
  .factory('TypeIncomingPayment', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryTypeIncomingPayment');
        //console.log('res=>',res);
// var da_json=[
//         {IncomingPaymentId: "1",  IncomingPaymentName: "name 1"},
//         {IncomingPaymentId: "2",  IncomingPaymentName: "name 2"},
//         {IncomingPaymentId: "3",  IncomingPaymentName: "name 3"},
//       ];
	  // var res=da_json;
		
        return res;
      },
      create: function(typeIncomingPayment) {
        return $http.post('/api/sales/PCategoryTypeIncomingPayment', [{
                                            //AppId: 1,
                                            IncomingPaymentTypeName: typeIncomingPayment.IncomingPaymentTypeName}]);
      },
      update: function(typeIncomingPayment){
        return $http.put('/api/sales/PCategoryTypeIncomingPayment', [{
                                            OutletId : typeIncomingPayment.OutletId,
                                            IncomingPaymentTypeId: typeIncomingPayment.IncomingPaymentTypeId,
                                            IncomingPaymentTypeName: typeIncomingPayment.IncomingPaymentTypeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryTypeIncomingPayment',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd