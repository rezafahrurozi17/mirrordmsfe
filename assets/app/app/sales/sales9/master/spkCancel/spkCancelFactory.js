angular.module('app')
  .factory('SpkCancel', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/AdminHandlingSPKCancel');
        //console.log('res=>',res);
// var da_json=[
//         {SpkCancelId: "1",  SpkCancelName: "name 1"},
//         {SpkCancelId: "2",  SpkCancelName: "name 2"},
//         {SpkCancelId: "3",  SpkCancelName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(spkCancel) {
        return $http.post('/api/sales/AdminHandlingSPKCancel', [{
                                            //AppId: 1,
                                            SpkCancelName: spkCancel.SpkCancelName}]);
      },
      update: function(spkCancel){
        return $http.put('/api/sales/AdminHandlingSPKCancel', [{
                                            OutletId : spkCancel.OutletId,
                                            SpkCancelId: spkCancel.SpkCancelId,
                                            SpkCancelName: spkCancel.SpkCancelName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingSPKCancel',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd