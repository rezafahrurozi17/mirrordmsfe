angular.module('app')
  .factory('MasterSukuFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerSuku');
        return res;
      },
      create: function(Suku) {
        return $http.post('/api/sales/CCustomerSuku', [{
                                            //AppId: 1,
                                            CustomerEthnicName: Suku.CustomerEthnicName}]);
      },
      update: function(Suku){
        return $http.put('/api/sales/CCustomerSuku', [{
                                            CustomerEthnicId: Suku.CustomerEthnicId,
                                            CustomerEthnicName: Suku.CustomerEthnicName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerSuku',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd