angular.module('app')
    .controller('EmployeeController', function($scope, $http, CurrentUser, EmployeeFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        //$scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mEmployee = null; //Model
    $scope.xRole={selected:[]};
    $scope.filter={GroupDealerId:null,OutletId: null};

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
		if($scope.filter.GroupDealerId!=null && $scope.filter.GroupDealerId!="" ||(typeof $scope.filter.GroupDealerId!="undefined"))
		{
			EmployeeFactory.getData($scope.filter).then(
				function(res){
					$scope.grid.data = res.data.Result;
					if(res.data.Result.length<=0)
					{
						bsNotify.show(
								{
									title: "Information",
									content: "Tidak ada data ditemukan.",
									type: 'info'
								}
							);
					}
					$scope.loading=false;
					return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		else
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Pastikan field mandatory terisi.",
							type: 'warning'
						}
					);
		}
        
    }
	
	
	
	EmployeeFactory.getDealerDropdown().then(function (res) {
			$scope.optionsEmployee_Dealer = res.data.Result;
			
			$scope.filter.GroupDealerId=$scope.user.GroupDealerId;
			EmployeeFactory.getOutlet().then(function(res){
				$scope.EmployeeOutletRaw = res.data.Result;
				$scope.MasterEmployeeSearchDealerBrubah();
			});
			
		});
		
	$scope.MasterEmployeeSearchDealerBrubah = function() {
            var Filtered_Outlet1 = [{ OutletId: null, OutletName: null }];
            Filtered_Outlet1.splice(0, 1);
            for (var i = 0; i < $scope.EmployeeOutletRaw.length; ++i) {
                if ($scope.EmployeeOutletRaw[i].DealerId == $scope.filter.GroupDealerId) {
                    Filtered_Outlet1.push({ OutletId: $scope.EmployeeOutletRaw[i].OutletId,OutletName: $scope.EmployeeOutletRaw[i].OutletName });
                }
            }
            $scope.outlet = Filtered_Outlet1;
			$scope.filter.OutletId=$scope.user.OutletId;
			
        };	

    

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'EmployeeId', width:'7%', visible:false },
            { name:'Nama', field:'EmployeeName' },
			{ name:'Role', field:'LastManPowerPositionTypeName' },
			{ name:'Nama Cabang', field:'OutletName' }
        ]
    };
});


