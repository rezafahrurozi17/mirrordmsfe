angular.module('app')
  .factory('EmployeeFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var filter = $httpParamSerializer(param);
        //http://localhost:38006/api/sales/Employee?OutletId=280
        var res=$http.get('/api/sales/Employee/?start=1&limit=1000000&Id=0&'+filter);
        return res;//KTPNo
      },
	  getDealerDropdown: function() {
        var res=$http.get('/api/sales/MsitesGroupDealer');
        return res;
      },
      getOutlet: function(){
        var res=$http.get('/api/sales/GetMSitesOutlet');//MSitesOutlet
        return res;
      },
      create: function(Employee) {
        return $http.post('/api/sales/Employee', [{
                ReasonDropProspectName: Employee.ReasonDropProspectName}]);
      },
      update: function(Employee){
        return $http.put('/api/sales/Employee', [{
                OutletId: Employee.OutletId,
                ReasonDropProspectId: Employee.ReasonDropProspectId,
                ReasonDropProspectName: Employee.ReasonDropProspectName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/Employee',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });