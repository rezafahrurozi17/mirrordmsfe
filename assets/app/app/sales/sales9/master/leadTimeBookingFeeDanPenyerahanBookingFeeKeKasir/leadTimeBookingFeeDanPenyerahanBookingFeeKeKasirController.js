angular.module('app')
	.controller('LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirController', function ($scope, $http, CurrentUser, LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory, $timeout, bsNotify) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = false;
			$scope.gridData = [];
		});
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir = null; //Model
		$scope.xRole = { selected: [] };
		$scope.filter = { VehicleModelId: null };

		$scope.noFirstZero1 = function (value) {

			if (isNaN(parseFloat("" + value)) == true) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking = null;
			}
			if (value == 0) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking = null;
			}
			else if (value < 0) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking = null;
			}
			else if (value > 999) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking = 999;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Lead Time terlalu lama.",
						type: 'warning'
					}
				);
			}
		}

		$scope.noFirstZero2 = function (value) {
			if (isNaN(parseFloat("" + value)) == true) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive = null;
			}
			else if (value == 0) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive = null;
			}
			else if (value < 0) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive = null;
			}
			else if (value > 999) {
				$scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive = 999;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Lead Time terlalu lama.",
						type: 'warning'
					}
				);
			}
		}
		//----------------------------------
		// Get Data test
		//----------------------------------

		LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.getModel().then(function (res) {
			$scope.optionsModel = res.data.Result;
			return $scope.optionsModel;
		});

		$scope.getData = function () {
			if ($scope.filter.VehicleModelId != null) {
				LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.getData($scope.filter.VehicleModelId).then(
					function (res) {
						$scope.grid.data = res.data.Result;
						$scope.loading = false;
						return res.data;
					},
					function (err) {
						bsNotify.show(
							{
								title: "gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);
			}
			else if ($scope.filter.VehicleModelId == null) {
				LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.getDataAll().then(
					function (res) {
						$scope.grid.data = res.data.Result;
						$scope.loading = false;
						return res.data;
					},
					function (err) {
						bsNotify.show(
							{
								title: "gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);
			}


		}

		function roleFlattenAndSetLevel(node, lvl) {
			for (var i = 0; i < node.length; i++) {
				node[i].$$treeLevel = lvl;
				gridData.push(node[i]);
				if (node[i].child.length > 0) {
					roleFlattenAndSetLevel(node[i].child, lvl + 1)
				} else {

				}
			}
			return gridData;
		}
		$scope.selectRole = function (rows) {
			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}

		$scope.doCustomSave = function (mdl, mode) {

			console.log('doCustomSave3', mode);//create update
			$scope.filter.VehicleModelId = angular.copy($scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.VehicleModelId);
			if (mode == "create") {
				LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.create($scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir).then(
					function (res) {

						LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.getDataAll().then(
							function (res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.formApi.setMode("grid");
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
						);


					},
					function (err) {

						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak berhasil disimpan",
								type: 'danger'
							}
						);
					}
				)
			}
			else if (mode == "update") {
				LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.update($scope.mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir).then(
					function (res) {

						LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory.getDataAll().then(
							function (res) {
								gridData = [];
								$scope.formApi.setMode("grid");
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data berhasil disimpan",
										type: 'success'
									}
								);
								return res.data;
							},
							function (err) {
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);


					},
					function (err) {

						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak berhasil disimpan",
								type: 'danger'
							}
						);
					}
				)
			}



		}

		$scope.onSelectRows = function (rows) {
		}
		//----------------------------------
		// Grid Setup
		//----------------------------------
		$scope.grid = {
			enableSorting: true,
			enableRowSelection: true,
			multiSelect: true,
			enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,
			columnDefs: [
				{ name: 'Id Penyerahan Ke Kasir', field: 'LeadtimeBookingFeeId', width: '7%', visible: false },
				{ name: 'Model', field: 'VehicleModelName' },
				{ name: ' Batas Waktu Booking Unit (jam)', field: 'LeadTimeBooking' },
				{ name: 'Batas Waktu Penerimaan Booking Fee (jam)', field: 'LeadTimeBookingReceive' }
			]
		};

		$scope.getData();
	});


