angular.module('app')
  .factory('LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var res=$http.get('/api/sales/GetLeadTimeBookingFee?VehicleModelId='+filter);
        return res;
      },

      getDataAll: function() {
        var res=$http.get('/api/sales/GetLeadTimeBookingFee?');
            return res;
          },
	  getModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas');
        return res;
      },
      create: function(LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir) {
        return $http.post('/api/sales/LeadTimeBookingFee', [{
                                            //AppId: 1,
											VehicleModelId: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.VehicleModelId,
											LeadTimeBooking: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking,
											LeadTimeBookingReceive: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive,
                                            }]);
      },
      update: function(LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir){
        return $http.put('/api/sales/LeadTimeBookingFee', [{
                                            LeadtimeBookingFeeId: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadtimeBookingFeeId,
											VehicleModelId: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.VehicleModelId,
											LeadTimeBooking: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking,
											LeadTimeBookingReceive: LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/LeadTimeBookingFee',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd