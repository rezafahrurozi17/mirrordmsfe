angular.module('app')
    .factory('UnitYearFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MUnitYear');
                //console.log('res=>',res);
                return res;
            },
            create: function(year) {
                return $http.post('/api/sales/MUnitYear', [{
                    YearName: year.YearName,
                }]);
            },
            update: function(year) {
                return $http.put('/api/sales/MUnitYear', [{
                    OutletId: year.OutletId,
                    YearId: year.YearId,
                    YearName: year.YearName,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitYear', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd