angular.module('app')
    .controller('UnitPriceController', function($scope, $http, CurrentUser, UnitPriceFactory, UnitYearFactory, AssetTestDriveUnit,$timeout,bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mUnitPrice = null; //Model
        $scope.xRole = { selected: [] };
        $scope.vehicleTypecolorfilter = [], $scope.vehicleTypefilter = [];
        $scope.VehicleType = [], $scope.VehicleColor = [];

        //----------------------------------
        // Get Data
        //----------------------------------


        //------------------------------
        // Get Master 
        //-------------------------------
        AssetTestDriveUnit.getDataVehicleModel().then(function (res){
            $scope.VehicleModel = res.data.Result;
        });
         
        AssetTestDriveUnit.getDataVehicleType().then(function (res){
            $scope.VehicleType = res.data.Result;
        });

        AssetTestDriveUnit.getDataVehicleTypeColors().then(function (res){
            $scope.VehicleColor = res.data.Result;
        });

        //---------------------------------------
        //Combobox Filter
        //---------------------------------------
        function emptyFromModel() {
            $scope.vehicleTypefilter = [], $scope.vehicleTypecolorfilter = [];
            $scope.mUnitPrice.VehicleTypeId = null, $scope.mUnitPrice.VehicleTypeColorId = null;
        }

        function emptyFromType() {
            $scope.vehicleTypecolorfilter = [], $scope.mUnitPrice.VehicleTypeColorId = null;
        }
        
        $scope.filterType = function(selected){
            if (selected){
                emptyFromModel();
                for(var i = 0 ; i < $scope.VehicleType.length; i++){
                    if ($scope.VehicleType[i].VehicleModelId == $scope.mUnitPrice.VehicleModelId){
                        $scope.vehicleTypefilter.push($scope.VehicleType[i]);
                    }
                }
            }else{
                emptyFromModel();
            }
        }     

         $scope.filterColor = function(selected){
            if (angular.isUndefined($scope.mUnitPrice.VehicleTypeId) || $scope.mUnitPrice.VehicleTypeId == null){
                emptyFromType();
            }else{
                emptyFromType();
                for(var i = 0 ; i < $scope.VehicleColor.length; i++){
                    if ($scope.VehicleColor[i].VehicleTypeId == $scope.mUnitPrice.VehicleTypeId){
                        $scope.vehicleTypecolorfilter.push($scope.VehicleColor[i]);
                    }
                }
            }
        }       


        var gridData = [];
            
        $scope.getData = function() {
            $scope.loading = true;
                UnitPriceFactory.getData().then(function (res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;  
                    return res.data;  
                })
            },
            function(err) {
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            };

         $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'Unit Price Id', field: 'UnitPriceId', width: '7%', visible: false },
                { name: 'outlet', field: 'OutletName' },
                { name: 'tipe model', field: 'Description' },
                { name: 'warna', field: 'ColorName' },
                { name: 'Year', field: 'Year' },
                { name: 'Price', field: 'Price' }
            ]
        };
    });