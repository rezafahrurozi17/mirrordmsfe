angular.module('app')
    .factory('UnitPriceFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MUnitPrice');
                return res;
            },
            create: function(UnitPrice) {
                return $http.post('/api/sales/MUnitPrice', [{
                    VehicleTypeColorId: UnitPrice.VehicleTypeColorId,
                    Year: UnitPrice.Year,
                    Price: UnitPrice.Price

                }]);
            },
            update: function(UnitPrice) {
                return $http.put('/api/sales/MUnitPrice', [{
                    OutletId: UnitPrice.OutletId,
                    UnitPriceId: UnitPrice.UnitPriceId,
                    VehicleTypeColorId: UnitPrice.VehicleTypeColorId,
                    Year: UnitPrice.Year,
                    Price: UnitPrice.Price
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitPrice', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
