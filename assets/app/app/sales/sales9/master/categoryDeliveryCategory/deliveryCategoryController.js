angular.module('app')
    .controller('DeliveryCategoryController', function($scope, $http, CurrentUser, DeliveryCategoryFactory, $timeout,bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mDeliveryCategory = null;
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data test
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
        DeliveryCategoryFactory.getData().then(
        function(res){
             gridData = [];
             $scope.grid.data = res.data.Result; //nanti balikin
			 $scope.loading=false;
         },
         function(err){
            bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
         }
	)};



        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
		
		$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			DeliveryCategoryFactory.create($scope.mDeliveryCategory).then(
				function(res) {
					DeliveryCategoryFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			DeliveryCategoryFactory.update($scope.mDeliveryCategory).then(
				function(res) {
					DeliveryCategoryFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
		
        $scope.onSelectRows = function(rows) {
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'DeliveryCategoryId', width: '7%', visible: false },
                { name: 'Delivery Category', field: 'DeliveryCategoryName' }
            ]
        };
    });