angular.module('app')
    .factory('DeliveryCategoryFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/PCategoryDelivery');
                //console.log('res=>',res);
                // var res = [{
                //     "DeliveryCategoryId": 1,
                //     "DeliveryCategoryName": "Home"
                // }, {
                //     "DeliveryCategoryId": 2,
                //     "DeliveryCategoryName": "Phone"
                // }, {
                //     "DeliveryCategoryId": 3,
                //     "DeliveryCategoryName": "Office"
                // }];
                return res;
            },
            create: function(DeliveryCategory) {
                return $http.post('/api/sales/PCategoryDelivery', [{

                    DeliveryCategoryName: DeliveryCategory.DeliveryCategoryName
                }]);
            },
            update: function(DeliveryCategory) {
                return $http.put('/api/sales/PCategoryDelivery', [{
                    OutletId : DeliveryCategory.OutletId,
                    DeliveryCategoryId: DeliveryCategory.DeliveryCategoryId,
                    DeliveryCategoryName: DeliveryCategory.DeliveryCategoryName
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/PCategoryDelivery', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });