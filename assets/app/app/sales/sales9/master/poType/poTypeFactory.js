angular.module('app')
  .factory('PoType', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/AdminHandlingPOType');
        //console.log('res=>',res);
// var da_json=[
//         {POTypeId: "1",  POTypeName: "name 1"},
//         {POTypeId: "2",  POTypeName: "name 2"},
//         {POTypeId: "3",  POTypeName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(poType) {
        return $http.post('/api/sales/AdminHandlingPOType', [{
                                            //AppId: 1,
                                            POTypeName: poType.POTypeName}]);
      },
      update: function(poType){
        return $http.put('/api/sales/AdminHandlingPOType', [{
                                            OutletId : poType.OutletId,
                                            POTypeId: poType.POTypeId,
                                            POTypeName: poType.POTypeName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingPOType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd