angular.module('app')
  .factory('SpkLost', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/AdminHandlingSPKLost');
        //console.log('res=>',res);
// var da_json=[
//         {SpkLostId: "1",  SpkLostName: "name 1"},
//         {SpkLostId: "2",  SpkLostName: "name 2"},
//         {SpkLostId: "3",  SpkLostName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(spkLost) {
        return $http.post('/api/sales/AdminHandlingSPKLost', [{
                                            //AppId: 1,
                                            SpkLostName: spkLost.SpkLostName}]);
      },
      update: function(spkLost){
        return $http.put('/api/sales/AdminHandlingSPKLost', [{
                                            OutletId : spkLost.OutletId,
                                            SpkLostId: spkLost.SpkLostId,
                                            SpkLostName: spkLost.SpkLostName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingSPKLost',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd