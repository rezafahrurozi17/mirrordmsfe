angular.module('app')
  .factory('SoCancelReason', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/AdminHandlingSOCancelReason');
        //console.log('res=>',res);
// var da_json=[
//         {SOCancelReasonId: "1",  SOCancelReasonName: "name 1"},
//         {SOCancelReasonId: "2",  SOCancelReasonName: "name 2"},
//         {SOCancelReasonId: "3",  SOCancelReasonName: "name 3"},
//       ];
// 	  var res=da_json;
		
        return res;
      },
      create: function(soCancelReason) {
        return $http.post('/api/sales/AdminHandlingSOCancelReason', [{
                                            //AppId: 1,
                                            SOCancelReasonName: soCancelReason.SOCancelReasonName}]);
      },
      update: function(soCancelReason){
        return $http.put('/api/sales/AdminHandlingSOCancelReason', [{
                                            OutletId : soCancelReason.OutletId,
                                            SOCancelReasonId: soCancelReason.SOCancelReasonId,
                                            SOCancelReasonName: soCancelReason.SOCancelReasonName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/AdminHandlingSOCancelReason',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd