angular.module('app')
  .factory('AccountingGlTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{GLTypeId: "d1", GLTypeName: "debit 01", AccountingCoa: [
				{GLAccountId: "1",  GLAccountCode: "GL01", GLAccountName: "GL Name 01"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "d2", GLTypeName: "debit 02", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "d3", GLTypeName: "debit 03", AccountingCoa: [
				{GLAccountId: "1",  GLAccountCode: "GL01", GLAccountName: "GL Name 01"},
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "c1", GLTypeName: "credit 01", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "c2", GLTypeName: "credit 02", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				
			  ];
        res=da_json;
        return res;
      },
	  getAllDebit: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{GLTypeId: "d1", GLTypeName: "debit 01", AccountingCoa: [
				{GLAccountId: "1",  GLAccountCode: "GL01", GLAccountName: "GL Name 01"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "d2", GLTypeName: "debit 02", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "d3", GLTypeName: "debit 03", AccountingCoa: [
				{GLAccountId: "1",  GLAccountCode: "GL01", GLAccountName: "GL Name 01"},
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]}
				
			  ];
        res=da_json;
        return res;
      },
	  getAllCredit: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        var da_json=[
				{GLTypeId: "c1", GLTypeName: "credit 01", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				{GLTypeId: "c2", GLTypeName: "credit 02", AccountingCoa: [
				{GLAccountId: "2",  GLAccountCode: "GL02", GLAccountName: "GL Name 02"},
				{GLAccountId: "3",  GLAccountCode: "GL03", GLAccountName: "GL Name 03"},
			  ]},
				
			  ];
        res=da_json;
        return res;
      },
      create: function(accountingGlType) {
        return $http.post('/api/fw/Role', [{
                                            GLTypeName:accountingGlType.GLTypeName,

                                            }]);
      },
      update: function(accountingGlType){
        return $http.put('/api/fw/Role', [{
                                            GLTypeId:accountingGlType.GLTypeId,
											GLTypeName:accountingGlType.GLTypeName,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd