angular.module('app')
    .factory('UnitBrandFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MUnitBrand');
                return res;
            },
            create: function(brand) {
                return $http.post('/api/sales/MUnitBrand', [{
                    BrandName: brand.BrandName,

                }]);
            },
            update: function(brand) {
                return $http.put('/api/sales/MUnitBrand', [{
                    OutletId: brand.OutletId,
                    BrandId: brand.BrandId,
                    BrandName: brand.BrandName,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitBrand', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd