angular.module('app')
  .factory('FinanceDaftarCashFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var res=[
                {DaftarCashId: "1", NamaDaftarCash: "Okto Van Roy", ValueDaftarCash: "1000"},
                {DaftarCashId: "2", NamaDaftarCash: "Jhon Snow", ValueDaftarCash: "1000"},
                {DaftarCashId: "3", NamaDaftarCash: "Rey Renaldi", ValueDaftarCash: "1000"}
		];
        return res;
      },
      create: function(FinanceDaftarCash) {
        return $http.post('/api/fw/Role', [{
											NamaDaftarCash: FinanceDaftarCash.NamaDaftarCash,
                                            ValueDaftarCash: FinanceDaftarCash.ValueDaftarCash}]);
      },
      update: function(FinanceDaftarCash){
        return $http.put('/api/fw/Role', [{
                                            DaftarCashId: FinanceDaftarCash.DaftarCashId,
                                            NamaDaftarCash: FinanceDaftarCash.NamaDaftarCash,
                                            ValueDaftarCash: FinanceDaftarCash.ValueDaftarCash}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });