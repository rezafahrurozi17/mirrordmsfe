angular.module('app')
  .factory('SektorBisnisFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerSectorBusiness');
        return res;
      },
      create: function(SektorBisnis) {
        return $http.post('/api/sales/CCustomerSectorBusiness', [{
                                            //AppId: 1,
                                            SectorBusinessName: SektorBisnis.SectorBusinessName}]);
      },
      update: function(SektorBisnis){
        return $http.put('/api/sales/CCustomerSectorBusiness', [{
                                            SectorBusinessId: SektorBisnis.SectorBusinessId,
                                            SectorBusinessName: SektorBisnis.SectorBusinessName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerSectorBusiness',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd