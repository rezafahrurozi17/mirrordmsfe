angular.module('app')
    .factory('UnitUOMFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res=$http.get('/api/sales/MUnitUOM');
                //console.log('res=>',res);
                return res;
            },
            create: function(uom) {
                return $http.post('/api/sales/MUnitUOM', [{
                    UomName: uom.UomName
                }]);
            },
            update: function(uom) {
                return $http.put('/api/sales/MUnitUOM', [{
                    OutletId: uom.OutletId,
                    UomId: uom.UomId,
                    UomName: uom.UomName,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/MUnitUOM', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd