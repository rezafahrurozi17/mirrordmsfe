angular.module('app')
  .factory('MobileHandling',['$http','LocalService','AccessLevels','transformRequestAsFormPost','ngDialog', function($http, LocalService, AccessLevels,transformRequestAsFormPost,ngDialog) {
    return {
      browserVer: function(){
		  console.log("masuk1");
          
          // Chrome 1+
          //var isChrome = !!window.chrome && !!window.chrome.webstore;
		  var isChrome = !!window.chrome;
			
			console.log("masuk2");
			
			
          console.log("isChrome=>",isChrome);
          return bv =
              isChrome ? 'Chrome' :
              "Unknown";
      },
      getBrowserVer: function() {
        var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
        if(/trident/i.test(M[1])){
            tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
            return (tem[1]||'');
        }   
        if(M[1]==='Chrome'){
            tem=ua.match(/\bOPR|Edge\/(\d+)/)
            if(tem!=null)   {return tem[1];}
        }   
        M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
        return M[1];
      },
      authorize: function(access) {
        // if (access === AccessLevels.user) {
        //   //console.log('auth user');
        //   return this.isAuthenticated();
        // } else {
        //   //console.log('auth anon');
        //   return true;
        // }
        return true;
      },
      isAuthenticated: function() {
        // if (LocalService.get('auth_token')){
        //   //console.log('isauth true');
        //   return LocalService.get('auth_token');
        // }else{
        //   //console.log('isauth false');
        //   return null;
        // }
        return true;
        //return LocalService.get('auth_token');

      },
      login: function(credentials) {
        //console.log("credentials=>",credentials);
        var login = $http.post('/sys/auth/signin', credentials,{timeout:10000});
        login.success(function(result) {
          //alert(JSON.stringify(result));
          LocalService.set('auth_token', JSON.stringify(result));
        });
        return login;
      },
      logout: function() {
        // The backend doesn't care about logouts, delete the token and you're good to go.
        LocalService.unset('auth_token');
        window.location = '/sys/auth/logout';
        return true;
      },
      chgPasswd: function(mPwd) {
        var user=angular.fromJson(LocalService.get('auth_token')).user;
        //console.log("mPwd=>",mPwd,user);
        mPwd.userId = user.id;
        var chgPwd = $http.post('/sys/user/chgPasswd', mPwd,{timeout:10000});
        return chgPwd;
      },
      getUserInfo: function(mPwd) {
        var userInfo = $http.post('/sys/user/getUserInfo',{},{timeout:10000});
        return userInfo;
      },
      // register: function(formData) {
      //   LocalService.unset('auth_token');
      //   var register = $http.post('/sys/auth/register', formData);
      //   register.success(function(result) {
      //     LocalService.set('auth_token', JSON.stringify(result));
      //   });
      //   return register;
      // },
      CR: function(){
        return 'Copyright ©2014-17 BeSmart Global Indonesia. All Rights Reserved.'
      },
      chkAM: function(url){
        //console.log('access:',url);
        if(url=='' || url=='/' || url=='/home' || url=='/login' || url=='/logout' || url=='/register') return {key:true}
        else{
          var am=$http.post('/sys/roleRights/get',{roleid:1,act:url});
          am.success(function(result){
            res=(result==null?true:false);
            console.log("am=>",result,res);
          })
          return am;
        }
      }
    }
  }])

  .factory("transformRequestAsFormPost",function() {
                // I prepare the request data for the form post.
                function transformRequest( data, getHeaders ) {
                    var headers = getHeaders();
                    headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";
                    return( serializeData( data ) );
                }
                return( transformRequest );

                function serializeData( data ) {
                    if ( ! angular.isObject( data ) ) {
                        return( ( data == null ) ? "" : data.toString() );
                    }
                    var buffer = [];
                    for ( var name in data ) {
                        if ( ! data.hasOwnProperty( name ) ) {
                            continue;
                        }
                        var value = data[ name ];
                        buffer.push(
                            encodeURIComponent( name ) +
                            "=" +
                            encodeURIComponent( ( value == null ) ? "" : value )
                        );
                    }
                    var source = buffer
                        .join( "&" )
                        .replace( /%20/g, "+" )
                    ;
                    return( source );
                }
  })

  .factory('AuthInterceptor', function($rootScope, $q, $injector,LocalService) {
    return {
      request: function(config) {
        //console.log('intercept request config:',config);
        //var baseUrl = 'https://dms-training.toyota.astra.co.id';
        var baseUrl = 'https://dms.toyota.astra.co.id';
        var apiKey1="ApiKey A23#qeJdho6X1ieKhjc8mZyxQ9+x5fsLHdoRBWz09RiY0832MIhgDIBz2fN4HCF9wHeyeM8BNCnN6k/6gp5NBHSvu7J88+ducD5EkX9Pz09KeFe7xSEZkNhRwZRU5gyKCTIjbsF3S9nKgJ6+vDCsS5yAbv6jwumajug9756Dw3ZC79g=";
        //var apiKey2="ApiKey MDMProd#O/QWMsbODydfwQEHc+s+mDiCN4Hrk/exoagJBcifwpi9LNsBdauB+38HpIn5Wkemen6+fAclPA0mynwKELvCRYB8/ILHY9g30mh1DEgtfVFOrlhxChJoJB8pdd9EkQ4eQT/OpTunMozOsqSnoq+fmfURS/gbu/+wE5Cgy2eD5ZU=";
        var port='';var apikey='';
        // var usePort=false;
        var usePort=true;
        var xurl = config.url.split('/');
        console.log('xurl:',xurl);
        if(xurl[1]=='api'){
          switch(xurl[2]){
              case 'fw':    port='38001'; apikey=apiKey1; break;
              case 'param': port='38002'; apikey=apiKey1; break;
              case 'ds':    port='38003'; apikey=apiKey1; break;
              case 'rpt':   port='38004'; apikey=apiKey1; break;
          }
          config.url = baseUrl + (usePort ? ':' + port : '') + config.url;
          config.headers.Authorization = apikey;
          config.headers['Content-Type'] = 'application/json';
        }else{
          var token;
          if (LocalService.get('auth_token')) {
            token = angular.fromJson(LocalService.get('auth_token')).token;
            //token = angular.fromJson(LocalService.get('auth_token')).access_token;
          }
          //console.log("token->",token);
          if (token) {
            config.headers.Authorization = 'JWT ' + token;
          }
        }
        //console.log('intercept request config url:',config);
        return config;
      },
      responseError: function(response) {
        //console.log("factory responseError=>",response);
        if(response.status <= 0){ // Request timeout
          $rootScope.ngDialog.open({ template: '_sys/auth/dialog_err_default.html',
                                      controller: ['$scope', '$timeout', function ($scope) {
                                                      console.log("timeout response=>",response);
                                                      var msg='';var code='';
                                                      if(response.data!=null){
                                                        msg=(response.data.Message==undefined?"":response.data.Message);
                                                        code=(response.data==""?"":" ("+response.data.code+")");
                                                      }
                                                      $scope.status = response.status+" ("+response.statusText+")";
                                                      $scope.message =  msg;
                                                      $scope.sql = msg;
                                                      $scope.data = JSON.stringify(response.config.data);
                                                      $scope.url = response.config.url;
                                                      $scope.method = response.config.method;
                                                      $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                                                  }]
          });
        }else  if(response.status == 401 && response.data.Message=="jwt expired"){ // token expired
                  LocalService.unset('auth_token');
                  window.location = '/sys/auth/logout';
        }else if(response.status == 400){
                $rootScope.ngDialog.open({ template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function ($scope) {
                                console.log("response=>",response);
                                // var msg=(msg==undefined?"":response.Message);
                                // var code=(response.data==""?"":" ("+response.data.code+")");
                                $scope.status = response.status+" ("+response.statusText+")";
                                var msg;
                                if(response.data.Message){
                                 msg=response.data.Message;
                                }else if(response.data.message){
                                 msg=JSON.parse(response.data.message);
                                }
                                //var msg=JSON.parse(response.data.Message);
                                console.log("msg,config=>",msg,response.config);
                                $scope.message = msg;
                                if(msg.Response){
                                  $scope.message =  msg.Response.ResponseMessage;
                                }
                                $scope.sql = msg['Object'];
                                $scope.data = JSON.stringify(response.config.data);
                                $scope.url = response.config.url;
                                $scope.method = response.config.method;
                                $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                            }],
                });
        }else if(response.status == 303){
                $rootScope.ngDialog.open({ template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function ($scope) {
                                console.log("response=>",response);
                                // var msg=(msg==undefined?"":response.Message);
                                // var code=(response.data==""?"":" ("+response.data.code+")");
                                $scope.status = response.status+" ("+response.statusText+")";
                                var msg=JSON.parse(response.data.Message);
                                console.log("msg,config=>",msg,response.config);
                                $scope.message =  msg.Response.ResponseMessage;
                                $scope.sql = msg['Object'];
                                $scope.data = JSON.stringify(response.config.data);
                                $scope.url = response.config.url;
                                $scope.method = response.config.method;
                                $scope.debugMode = false;
                            }],
                });
        }else{
                try{
                  $rootScope.ngDialog.open({ template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function ($scope) {
                                $scope.status = response.status+" ("+response.statusText+")";
                                var msg;
                                if(response.data.message){
                                  msg=response.data.name+' '+response.data.message;
                                  $scope.sql = response.data.sql;
                                  $scope.data = '';
                                }else if(response.data.Message){
                                  msg=JSON.parse(response.data.Message);
                                }
                                //var msg=JSON.parse(response.data.Message);
                                console.log("msg,config=>",msg,response.config);
                                $scope.message = msg;
                                if(msg.Response){
                                  $scope.message =  msg.Response.ResponseMessage;
                                  $scope.sql = msg['Object'];
                                  $scope.data = JSON.stringify(response.config.data);
                                }

                                $scope.url = response.config.url;
                                $scope.method = response.config.method;
                                $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                            }],
                   });
                }catch(err){

                }finally{
                  console.log("ResponseError=>",response);
                  if (response.status === 401) {
                    if(LocalService.get('auth_token')){
                      // window.location = '/sys/auth/logout';
                    }else{
                      LocalService.unset('auth_token');
                    }
                    //$injector.get('$state').go('login');
                  }
                }
        }
        return $q.reject(response);
      },
    }
  })
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });