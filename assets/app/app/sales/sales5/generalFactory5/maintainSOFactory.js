angular.module('app')
  .factory('MaintainSOFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
        getData: function(filter) {
            for(key in filter){
              if(filter[key]==null || filter[key]==undefined)
                delete filter[key];
            }
            var res=$http.get('/api/sales/AdminHandlingSOMaintain/?start=1&limit=100&'+$httpParamSerializer(filter));
            return res;
        },
      
        getSOType: function() {
            var res=$http.get('/api/sales/AdminHandlingSOType');
            return res;
        },

        getJPembayaran: function() {
            var res=$http.get('/api/sales/FFinancePaymentType');
            return res;
        },
		
		getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },

        getCustomerCategory: function() {
            var res=$http.get('/api/sales/CCustomerCategory');
            return res;
        },

        getPeranPelanggan: function() {
            var res=$http.get('/api/sales/CCustomerCorrespondent');
            return res;
        },

        getSOStatus: function() {
            var res=$http.get('/api/sales/AdminHandlingSOStatus');
            return res;
        },

        getCancelreason: function() {
            var res=$http.get('/api/sales/AdminHandlingSOCancelReason');
            return res;
        },

        getOutlet: function() {
            var res=$http.get('/api/sales/MSitesOutlet?start=1&limit=1000');
            return res;
        },

        getDataJasaDokumen: function() {
            var res=$http.get('/api/sales/MProfileService?start=1&limit=100');
            return res;
        },

        getDataDokumen: function() {
            var res=$http.get('/api/sales/PCategoryDocument?start=1&limit=100');
            return res;
        },

        getDataAksesoris: function(param) {
            var res=$http.get('/api/sales/AccessoriesSatuan?start=1&limit=100' + param);
            return res;
        },

        getDataKaroseri: function(param) {
            var res=$http.get('/api/sales/MPartsKaroseri?start=1&limit=100' + param);
            return res;
        },

        getDataBank: function() {
            var res=$http.get('/api/sales/AdminHandlingBank');
            return res;
        },

        getDataSalesman: function() {
            var res=$http.get('/api/sales/MProfileEmployee?start=1&limit=100');
            return res;
        },

        getSODetail: function (param){
            var res = $http.get('/api/sales/AdminHandlingSOMaintain?SOId='+param);
            return res;
        },

        getNoRangka: function (param){
            var res = $http.get('/api/sales/AdminHandlingStockPurnaJual?start=1&limit=1000' + param);
            return res;
        },

        getFakturPajak: function (){
            var res = $http.get('/api/sales/FFinanceCodeInvoiceTransactionTax');
            return res;
        },

        getStatusBilling: function (param){
            var res = $http.get('/api/sales/AdminHandlingCheckBillingStatus' + param);
            return res;
        },

//-------------------------------------------------------------------------------------------------\\
        createSO: function(MaintainSO) {
            console.log("MaintainSO",MaintainSO)
            return $http.post('/api/sales/AdminHandlingSOMaintain', [{
            ProspectId: MaintainSO.ProspectId,
            SpkId: MaintainSO.SpkId,
            SpkDetailInfoUnitId: MaintainSO.SpkDetailInfoUnitId,
            SoCode: MaintainSO.SoCode,
            SoDate: MaintainSO.SoDate,
            ToyotaId: MaintainSO.ToyotaId,
            EmployeeId: MaintainSO.EmployeeId,
            Name: MaintainSO.Name,
            Handphone:MaintainSO.Handphone,
            KTP:MaintainSO.KTP,
            SIUP:MaintainSO.SIUP,
            NPWP:MaintainSO.NPWP,
            Address:MaintainSO.Address,
            CInvoiceTypeId:MaintainSO.CInvoiceTypeId,
            ReceiverName:MaintainSO.ReceiverName,
            ReceiverAddress:MaintainSO.ReceiverAddress,
            ReceiverHP:MaintainSO.ReceiverHP,
            SentDate:MaintainSO.SentDate,
            Discount:MaintainSO.Discount,
            AFIValidDate:MaintainSO.AFIValidDate,
            NoteSOCancelReason:MaintainSO.NoteSOCancelReason,
            RRNNo:MaintainSO.RRNNo,
            FrameNo:MaintainSO.FrameNo,
            NamaPO:MaintainSO.NamaPO,
            VehicleTypeColorId:MaintainSO.VehicleTypeColorId,
            SOStatusId:MaintainSO.SOStatusId,
            SOTypeId:MaintainSO.SOTypeId,
            SOCancelReasonId:MaintainSO.SOCancelReasonId,
            CustomerCategoryId:MaintainSO.CustomerCategoryId,
            PaymentTypeId:MaintainSO.PaymentTypeId,
            DPP:MaintainSO.DPP,
            PPN:MaintainSO.PPN,
            PPH22:MaintainSO.PPH22,
            TotalVAT:MaintainSO.TotalVAT,
            GrandTotal:MaintainSO.GrandTotal,
            MediatorName:MaintainSO.MediatorName,
            MediatorPhoneNo:MaintainSO.MediatorPhoneNo,
            MediatorKTPNo:MaintainSO.MediatorKTPNo,
            ListOfSODocumentCareDetailService:MaintainSO.ListOfSODocumentCareDetailService,
            ListOfSODocumentView:MaintainSO.ListOfSODocumentView,
            ListOfSODetailAccesoriesView:MaintainSO.ListOfSODetailAccesoriesView,
            ServiceBureauId:MaintainSO.ServiceBureauId,
            ServiceBureauCode:MaintainSO.ServiceBureauCode,
            ServiceBureauName:MaintainSO.ServiceBureauName,
            DocumentId:MaintainSO.DocumentId,
            DocumentType:MaintainSO.DocumentType,
            DocumentName:MaintainSO.DocumentName,
            AccessoriesId:MaintainSO.AccessoriesId,
            PostingDate:MaintainSO.PostingDate,
            SOTypeName:MaintainSO.SOTypeName,
            SOStatusName:MaintainSO.SOStatusName,
            ProspectName:MaintainSO.ProspectName,
            FormSPKNo:MaintainSO.FormSPKNo,
            spkCustomerId:MaintainSO.spkCustomerId,
            CustomerTypeId:MaintainSO.CustomerTypeId,
            CustomerTypeDesc:MaintainSO.CustomerTypeDesc,
            DeliveryCategoryId:MaintainSO.DeliveryCategoryId,
            DeliveryCategoryName:MaintainSO.DeliveryCategoryName,
            OnOffTheRoadId:MaintainSO.OnOffTheRoadId,
            OnOffTheRoadName:MaintainSO.OnOffTheRoadName,
            PromiseDeliveryMonthId:MaintainSO.PromiseDeliveryMonthId,
            PromiseDeliveryMonthName:MaintainSO.PromiseDeliveryMonthName,
            EstimateDate:MaintainSO.EstimateDate,
            SentUnitDate:MaintainSO.SentUnitDate,
            StatusPDDId:MaintainSO.StatusPDDId,
            StatusPDDName:MaintainSO.StatusPDDName,
            StatusMatchId:MaintainSO.StatusMatchId,
            StatusMatchName:MaintainSO.StatusMatchName,
            Description:MaintainSO.Description,
            colorName:MaintainSO.colorName,
            NumberPlateId:MaintainSO.NumberPlateId,
            NumberPlateName:MaintainSO.NumberPlateName,
            StatusStockName:MaintainSO.StatusStockName,
            ProductionYear:MaintainSO.ProductionYear,
            InsuranceId:MaintainSO.InsuranceId,
            NotePayment:MaintainSO.NotePayment,
            BankId:MaintainSO.BankId,
            BankName:MaintainSO.BankName,
            InsuranceProductId:MaintainSO.InsuranceProductId,
            ProductName:MaintainSO.ProductName,
            InsuranceUserTypeId:MaintainSO.InsuranceUserTypeId,
            InsuranceUserTypeName:MaintainSO.InsuranceUserTypeName,
            PolisName:MaintainSO.PolisName,
            spkPaymentTypeId:MaintainSO.spkPaymentTypeId,
            PaymentTypeName:MaintainSO.PaymentTypeName,
            EstimasiBiayaAsuransi:MaintainSO.EstimasiBiayaAsuransi,
            WaktuPertanggungan:MaintainSO.WaktuPertanggungan,
            Karoseri:MaintainSO.Karoseri,
            KaroseriPrice:MaintainSO.KaroseriPrice,
            MaterialId:MaintainSO.MaterialId,
            MaterialTypeId:MaintainSO.MaterialTypeId,
            MaterialNo:MaintainSO.MaterialNo,
            MaterialName:MaintainSO.MaterialName,
            KaroseriStatus:MaintainSO.KaroseriStatus      
            }]);
        },

        update: function(MaintainSO){
            return $http.put('/api/sales/AdminHandlingSOMaintain', [MaintainSO]);
        },

        batalSO:function(MaintainSO)
        {
            return $http.put('/api/sales/AdminHandlingSOMaintain/Cancel', [MaintainSO]);
        },

        delete: function(id) {
            return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
        },
    }
  });