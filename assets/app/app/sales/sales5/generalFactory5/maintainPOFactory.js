angular.module('app')
  .factory('MaintainPOFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          for(key in filter){
            if(filter[key]==null || filter[key]==undefined)
              delete filter[key];
          }
          var res=$http.get('/api/sales/AdminHandlingPOMaintenance/?start=1&limit=100&'+$httpParamSerializer(filter));
          return res;
      },
      
      getTypePO:function(){
        var tipe = $http.get('/api/sales/AdminHandlingPOType'); 
        return tipe;
      },
	  
	  getPOReference:function(DaParam){
        var tipe = $http.get('/api/sales/AdminHandlingPOReference'+DaParam);
		//var tipe = $http.get('/api/sales/AdminHandlingPOReference/?POTypeId='+DaParam);
        return tipe;
      },
	  
	  getPCategoryMaterialType:function(DaParam){
        var MaterialType = $http.get('/api/sales/PCategoryMaterialType'+DaParam); 
        return MaterialType;
      },
	  
	  getDaNoDokumen:function(){
        var NoDokumen = $http.get('/api/sales/SDRDeliveryNoteNoRangka'); 
        return NoDokumen;
      },
	  
	  getDataAksesoris: function(param) {
        //var res=$http.get('/api/sales/POInternalDetailAccessories?start=1&limit=100' + param);
        var res=$http.get('/api/sales/AdminHandlingPOInternalDetailAccessories?' + param);
		return res;
      },

      getDataKaroseri: function(param) {
        var res=$http.get('/api/sales/MPartsKaroseri?start=1&limit=1000'+ param);
        return res;
      },
	  
	  getDataJasaOpdDanEkspedisi: function(param) {
        //var res=$http.get('/api/sales/AdminHandlingPOInternalDetailJasa?start=1&limit=100'+ param);
        var res=$http.get('/api/sales/AdminHandlingPOInternalDetailJasa?'+ param);
		return res;
      },
      
      getStatusPO:function(){
        var status = $http.get('/api/sales/AdminHandlingPOStatus'); 
        return status;
      },

      getRequestPO:function(){
        var status = $http.get('/api/sales/AdminHandlingPOPRList'); 
        return status;
      },

      getPaymentType:function(){
        var res = $http.get('/api/sales/FFinancePaymentType'); 
        return res;
      },

      getNoRangka:function(){
        var res = $http.get('/api/sales/AdminHandlingPOInternalVehicle'); 
        return res;
      },

      getMaterialInternal:function(param){
        var res = $http.get('/api/sales/AdminHandlingPOInternalDetailAccessories' + param); 
        return res;
      },

      getVendor: function(param) {
        var res=$http.get('/api/sales/MVendorList' + param);
        return res;
      },
      
      create: function(maintainPO) {
        return $http.post('/api/sales/AdminHandlingPOMaintenance', [maintainPO]);
      },

      update: function(maintainPO){
        return $http.put('/api/sales/AdminHandlingPOMaintenance', [maintainPO]);
      },
	  
	  getDataDetailForUbah: function(filter) {

          var res=$http.get('/api/sales/AdminHandlingPOMaintenance?POId='+filter);
          return res;
      },

      closePO: function(param){
        return $http.put('/api/sales/AdminHandlingPOMaintenance/Close' + param);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });




