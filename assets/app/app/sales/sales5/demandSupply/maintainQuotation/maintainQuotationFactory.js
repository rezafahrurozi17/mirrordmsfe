angular.module('app')
    .factory('MaintainQuotationFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                var param = '?' + $httpParamSerializer(filter);
                var res = $http.get('/api/sales/DemandSupplyQuotation' + param);
                return res;
            },

            getDataCancel: function() {
                var res = $http.get('/api/sales/PCategoryQuotationCancelReason');
                return res;
            },

            getDataQuotation: function() {
                var res = $http.get('/api/sales/PStatusQuotation');
                return res;
            },

            getTOP: function() {
                var res = $http.get('/api/sales/PCategoryTermOfPayment');
                return res;
            },

            create: function(Quotation) {
                Quotation.SentDate = fixDate(Quotation.SentDate);
                return $http.post('/api/fw/Role', [Quotation]);
            },

            update: function(Quotation) {
                Quotation.SentDate = fixDate(Quotation.SentDate);
                return $http.put('/api/sales/DemandSupplyQuotation/Cancel', [Quotation]);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });