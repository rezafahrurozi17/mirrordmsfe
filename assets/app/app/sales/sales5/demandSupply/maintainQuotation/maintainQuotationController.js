angular.module('app')
    .controller('MaintainQuotationController', function($scope, $http, bsNotify, CurrentUser, ModelFactory, FindStockFactory, MaintainQuotationFactory, MaintainRequestSwappingFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MaintainQuotationShow = true;
        $scope.BuatQuotationShow = false;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainQuotation = null; //Model
        $scope.xRole = { selected: [] };
        $scope.filter = { VehicleTypeId: null, QuotationTYpeId: null };
        $scope.tempfilter = { VehicleModelId: null };
        $scope.dateOptions ={
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        }
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalMaintainQuotation_AlasanBatalQuotation').remove();
		  
		});

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            // if(($scope.filter.QuotationStatusId == null || $scope.filter.QuotationStatusId == undefined) || 
                // ($scope.filter.VehicleModelId == null || $scope.filter.VehicleModelId == undefined) ){
			if(($scope.filter.QuotationStatusId == null || $scope.filter.QuotationStatusId == undefined) 
                 ){
                bsNotify.show({
                    title: "Gagal",
                    content: "Mandatori harus diisi.",
                    type: 'danger'
                });
            }else{
                MaintainQuotationFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
           
        }

        MaintainRequestSwappingFactory.getType().then(
            function(res) {
                $scope.dataType = res.data.Result;
                return res.data;
            }
        );

        MaintainQuotationFactory.getDataCancel().then(
            function(res) {
                $scope.dataCancel = res.data.Result;
                return res.data;
            }
        );

        MaintainQuotationFactory.getDataQuotation().then(
            function(res) {
                $scope.dataQuotation = res.data.Result;
				$scope.filter.QuotationStatusId=$scope.dataQuotation[0].QuotationStatusId
                $scope.getData();
				return res.data;
            }
        );

        MaintainQuotationFactory.getTOP().then(function(res) {
            $scope.termOfPayment = res.data.Result;
            return res.data;
        });

        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        $scope.filterModel = function(selected) {
            $scope.Model = selected;
            getId = "?VehicleModelId=" + $scope.Model.VehicleModelId;
            FindStockFactory.getTipeModel(getId).then(
                function(res) {
                    $scope.listTipe = res.data.Result;
                    return res.data;
                }
            );
        }

        $scope.QuotationClick = function(selected) {
            $scope.Selectedrow = selected;
            $scope.MaintainQuotationShow = false;
            $scope.BuatQuotationShow = true;
        }

        $scope.btnkembaliQuotation = function() {
            $scope.BuatQuotationShow = false;
            $scope.MaintainQuotationShow = true;
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        // $scope.BatalModal = function(selected) {
            
            // $scope.Selectedrow = {};
            // $scope.QuotationIdTemp = selected.QuotationId;
            // $scope.show_modal.show = !$scope.show_modal.show;
        // }
		//$scope.KembaliModalMaintainQuotation_AlasanBatalQuotation
		$scope.BatalModal = function(selected) {
            $scope.Selectedrow = {};
			$scope.QuotationIdTemp = selected.QuotationId;
			$scope.Selectedrow.SentDate=selected.SentDate;
            setTimeout(function() {
              angular.element('.ui.modal.ModalMaintainQuotation_AlasanBatalQuotation').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalMaintainQuotation_AlasanBatalQuotation').not(':first').remove();  
            }, 1);
        }

        $scope.SimpanModalMaintainQuotation_AlasanBatalQuotation = function() {
            if(angular.isDefined($scope.Selectedrow.$$hashKey) == true){
                delete $scope.Selectedrow.$$hashKey;
            } 

            $scope.objQut = { QuotationId : $scope.QuotationIdTemp } ;
            _.extend($scope.Selectedrow, $scope.objQut);

            MaintainQuotationFactory.update($scope.Selectedrow).then(
                function(res) {
                    bsNotify.show({
                        title: "Success Message",
                        content: "Berhasil Batal Quotation",
                        type: 'success'
                    });
                    $scope.MaintainQuotationShow = true;
                    angular.element('.ui.modal.ModalMaintainQuotation_AlasanBatalQuotation').modal('hide');
                    $scope.getData();
                    $scope.Selectedrow = {};
                    // console.log("Succ=>", Success);
                },
                function(err) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Gagal Batal Quotation",
                        type: 'danger'
                    });
                    //console.log("err=>", err);
                }
            );
        }

        $scope.KembaliModalMaintainQuotation_AlasanBatalQuotation = function() {
            $scope.Selectedrow = {};
            angular.element('.ui.modal.ModalMaintainQuotation_AlasanBatalQuotation').modal('hide');
        }


        $scope.onSelectRows = function(rows) {
            $scope.Select = rows;
            console.log("onSelectRows=>", rows);
        }



        var btnAction = '<a  href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Batal" tooltip-placement="bottom" onclick="this.blur()" ng-if="row.entity.QuotationStatusName == \'Dibuat\'" ng-click="grid.appScope.$parent.BatalModal(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';
        
        //'<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-if="row.entity.QuotationStatusName == \'Dibuat\'" ng-click="grid.appScope.$parent.BatalModal(row.entity)">Batal</u></span>'
        var NoQuotation = '<span style="color:blue"><p style="padding:5px 0 0 5px" ng-click="grid.appScope.$parent.QuotationClick(row.entity)"><u>{{row.entity.QuotationNo}}</u></p></span>';
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                //{ name:'StockViewId',    field:'StockViewId', width:'7%', visible:false },
                { name: 'No. quotation', field: 'QuotationNo', width: '15%', cellTemplate: NoQuotation},
                { name: 'Model', field: 'VehicleModelName', width: '15%' },
                { name: 'Tipe', field: 'VehicleTypeDescription', width: '18%' },
                { name: 'warna', field: 'ColorName', width: '15%' },
                { name: 'No. rangka', field: 'FrameNo', width: '15%' },
                { name: 'Cabang yang meminta', field: 'OutletRequestName', width: '21%' },
                { name: 'Status', field: 'QuotationStatusName', width: '8%' },
                {                    
                    name: 'Action',
                                        allowCellFocus: false,
                                        width: '7%',
                                        visible: true,
                                        pinnedRight: true,
                                        enableColumnMenu: false,
                                        enableSorting: false,
                                        enableColumnResizing: true,
                                        cellTemplate: btnAction
                }
            ]
        };

        $scope.gridrincianHarga = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName' },
                { name: 'Keterangan', field: 'Description' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };

        $scope.gridEkspedisi = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName' },
                { name: 'Keterangan', field: 'Description' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };
    });