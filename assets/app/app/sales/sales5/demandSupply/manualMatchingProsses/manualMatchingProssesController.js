angular.module('app')
    .controller('ManualMatchingProssesController', function($scope, $http, CurrentUser, ManualMatchingProssesFactory, MaintainSOFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mmanualMatchingProsses = null; //Model

        //----------------------------------
        // Get Data
        //----------------------------------
        MaintainSOFactory.getOutlet().then(
            function(res){
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        var gridData = [];
        $scope.getData = function() {
            ManualMatchingProssesFactory.getData().then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    //$scope.xxx = res.data.Result;
                    $scope.loading=false;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        // var btnActionEditTemplate = '<button class="ui icon inverted grey button"' +
        //     ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
        //     ' onclick="this.blur()"' +
        //     ' ng-click="grid.appScope.gridClickToggleStatus(row.entity)">' +
        //     '<i ng-class="' +
        //     '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.OpenState,' +
        //     '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.OpenState,' +
        //     '}' +
        //     '">' +
        //     '</i>' +
        //     '</button>';

        //var btnActionEditTemplate = '<a style="color:blue;" onclick=""; ng-click=""><p style="padding:5px 0 0 5px"><u>Lihat</u></p></a>';
        var btnActionCekValid2 = '<a style="color:blue;"  ng-click=""><p style="padding:5px 0 0 5px" onclick="MatchRRN()";><u>...</u></p></a>';
        // var noSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u>{{row.entity.NoSPK}}</u></p></span>';
        // var statusSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px" onclick="ModalConfirmation()"><u>{{row.entity.StatusSPK}}</u></p></span>';;
        
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'id', field: 'SOId', visible: false },
                { name: 'tipe model', field: 'VehicleTypeColorDescription', width: '15%' },
                { name: 'warna', field: 'Warna', width: '12%' },
                { name: 'no rangka \ rrn', field:'FrameNo', width: '12%' },
                { name: 'no spk', field: 'FormSPKNo', width: '12%' },
                { name: 'tanggal spk', field: 'SpkDate', width: '10%' },
                { name: 'so no', field: 'SoCode', width: '12%' },
                { name: 'so date', field: 'SoDate', width: '10%' },
                { name: 'tanggal pelunasan', field: 'TanggalPelunasan', width: '10%' },
                { name: 'nama prospect/Pelanggan', field: 'NamaProspect', width: '20%' },
                { name: 'status matching', field: 'StatusMatching', width: '12%' }
            ]
        };
    });