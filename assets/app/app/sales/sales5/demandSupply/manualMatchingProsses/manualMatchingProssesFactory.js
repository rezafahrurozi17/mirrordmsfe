angular.module('app')
    .factory('ManualMatchingProssesFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/sales/DemandSupplyMaintainMatching');
                //console.log('res=>',res);
                return res;
            },
            create: function(SpkTaking) {
                return $http.post('/api/sales/DemandSupplyMaintainMatching', [{
                    NamaSales: SpkTaking.NamaSales

                }]);
            },
            update: function(SpkTaking) {
                return $http.put('/api/sales/Role', [{
                    NoSPK: SpkTaking.NoSPK,
                    NamaSPK: SpkTaking.NamaSales,

                    //pid: role.pid,
                }]);
            },
            // delete: function(id) {
            //     return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            // },
        }
    });
//ddd