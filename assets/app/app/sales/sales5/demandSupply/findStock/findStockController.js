angular.module('app')
    .controller('FindStockController', function($scope, $http, $httpParamSerializer, CurrentUser, bsNotify, FindStockFactory, ComboBoxFactory, StockViewMobileFactory, ModelFactory, TipeFactory, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = true;
        $scope.gridData = [];
    });
    
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.xRole = {selected:[]};
    enable_advsearch: '@enableAdvancedsearch',
    $scope.filter = {VehicleModelId:null, VehicleTypeId:null, ColorId:null, AreaId:null, ProvinceId:null};
    $scope.filterOther = {VehicleModelId:null, VehicleTypeId:null, ColorId:null, AreaId:null, ProvinceId:null};
    $scope.isEmpty = false, $scope.isOtherEmpty = false;
    $scope.QtyOther = 0, $scope.Qty = 0;
    $scope.flag = 'stock';

    $scope.falseToggle = function() {
        $scope.advSearch = false;
        $scope.toggle = false;
    }

    $scope.flag = function (flag){
        if(flag == 'stock'){
            $scope.flag = 'stock';
        }else{
            $scope.flag = 'otherStock';
        }
    }

    $scope.refresh = function (){
        if ($scope.flag == 'stock'){
            $scope.stockGroupDealer = [];
            $scope.filter = {VehicleModelId:null, VehicleTypeId:null, ColorId:null, AreaId:null, ProvinceId:null};
        }else{
            $scope.stockGroupDealer = [];
            $scope.filterOther = {VehicleModelId:null, VehicleTypeId:null, ColorId:null, AreaId:null, ProvinceId:null};
        }
    }

    //----------------------------------
    //Filter
    //----------------------------------

    $scope.$watch('filter.VehicleModelId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filter.VehicleTypeId = null;
            $scope.filter.ColorId = null;
        }
    });

    $scope.$watch('filter.VehicleTypeId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filter.ColorId = null;
        }
    });

    $scope.$watch('filter.AreaId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filter.ProvinceId = null;
        }
    });

    $scope.$watch('filterOther.VehicleModelId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filterOther.VehicleTypeId = null;
            $scope.filterOther.ColorId = null;
        }
    });

    $scope.$watch('filterOther.VehicleTypeId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filterOther.ColorId = null;
        }
    });

    $scope.$watch('filterOther.AreaId', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || (newValue != oldValue)){
            $scope.filterOther.ProvinceId = null;
        }
    });

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.advancedSearch = function(){
        if($scope.filter.VehicleModelId == null || $scope.filter.AreaId == null){
            bsNotify.show({
                title: "Peringatan",
                content: "Mandatory harus diisi.",
                type: 'warning'
            });
        }
        else{
            FindStockFactory.getDataGroupDealer($scope.filter).then(
                function(res){
                    $scope.stockGroupDealer = res.data.Result;
                    if ($scope.stockGroupDealer.length == 0){
                        $scope.isEmpty = true;
                        $scope.Qty = 0;
                    }else{
                        $scope.isEmpty = false;
                        $scope.Qty = $scope.stockGroupDealer.length
                    }
                    return res.data;
                }
            );
        }
    }

    $scope.otherDealerSearch = function(){
        if($scope.filterOther.VehicleModelId == null || $scope.filterOther.AreaId == null){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Mandatory harus diisi.",
                    type: 'warning'
                });
        }
        else{
            FindStockFactory.getDataOthDealer($scope.filterOther).then(
                function(res){
                    $scope.stockOtherDealer = res.data.Result;
                    if ($scope.stockOtherDealer.length == 0){
                        $scope.isOtherEmpty = true;
                        $scope.QtyOther = 0;
                    }else{
                        $scope.isOtherEmpty = false;
                        $scope.QtyOther = $scope.stockOtherDealer.length;
                    }
                    return res.data;
                }
            );
        }
    }

    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    $scope.filterModel = function(selected) {
        $scope.Model = selected;
        getId = "?VehicleModelId=" + $scope.Model.VehicleModelId;
        FindStockFactory.getTipeModel(getId).then(
            function(res){
                $scope.listTipe = res.data.Result;
                return res.data;
            }
        );
    }

    $scope.pilihType = function (selected) {
        ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected).then(
            function (res) {
            $scope.listWarna = res.data.Result;
        })
    };

    $scope.getProvince = function(Id) {
        FindStockFactory.getDataProvincebyArea(Id).then(
            function(res) {
                $scope.getDataProvince = res.data.Result;
                return res.data;
            }
        );
    }

    FindStockFactory.getDataArea().then(
        function(res){
            var da_area_ilangin_tam=res.data.Result;
			for(var i = 0; i < da_area_ilangin_tam.length; ++i)
			{	
				if(da_area_ilangin_tam[i].AreaName=="TAM")
				{
					da_area_ilangin_tam.splice(i, 1);
					break;
				}
			}
			$scope.getDataArea = da_area_ilangin_tam;
            return res.data;
        }
    );

    FindStockFactory.getDataProvince().then(
        function(res){
            $scope.getDataProvince = res.data.Result;
            return res.data;
        }
    );

});