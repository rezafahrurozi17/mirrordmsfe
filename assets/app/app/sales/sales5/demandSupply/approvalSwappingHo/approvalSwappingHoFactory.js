angular.module('app')
  .factory('ApprovalSwappingHoFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		var res=$http.get('/api/sales/CProspectListApproval');
		return res;
        
      },
	  
	  
	  
      create: function(ApprovalSwappingHo) {
        return $http.post('/api/sales/CProspectListApproval', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalSwappingHo.SalesProgramName}]);
      },
      update: function(ApprovalSwappingHo){
        return $http.put('/api/sales/CProspectListApproval', [{
                                            SalesProgramId: ApprovalSwappingHo.SalesProgramId,
                                            SalesProgramName: ApprovalSwappingHo.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CProspectListApproval',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd