angular.module('app')
  .factory('SalesCompositionHistoryFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
         var res=$http.get('/api/sales/DemandSupplyTargetAssignKacabViewFx' + param);         
          return res;
      },

      getDataKacabdetail: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignKacabViewDetailFx' + param);         
          return res;
      },

      getDataSPV: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignSPVViewFx' + param);         
          return res;
      },

      getDataSPVdetail: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignSPVViewDetailFx' + param);         
          return res;
      },

      getDataPeriod: function() {
        var res=$http.get('/api/sales/DemandSupplyOAPRAPMonth');         
          return res;
      },

      createKacab: function(SalesComposition) {
        return $http.post('/api/sales/DemandSupplyTargetAssignKacabViewDetailFx', SalesComposition);
      },

      createSPV: function(SalesComposition) {
        return $http.post('/api/sales/DemandSupplyTargetAssignSPVViewDetailFx', SalesComposition);
      },
      
      update: function(SalesComposition){
        return $http.put('/api/fw/Role', [{
          SalesProgramId: SalesComposition.SalesProgramId,
          SalesProgramName: SalesComposition.SalesProgramName}]);
      },
      
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });