angular.module('app')
    .controller('SalesCompositionHistoryController', function($scope, $http, CurrentUser, SalesCompositionHistoryFactory, MaintainSOFactory, ModelFactory, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.Role = $scope.user;
    console.log("user", $scope.Role);
    $scope.mComposition = null; //Model
    $scope.xRole={selected:[]};

    if($scope.Role.RoleName == "KACAB"){
        $scope.KacabView = true;
        $scope.showgridKacab = true;
        $scope.targetAssignment = false;
        $scope.showModel = false;
        $scope.disabledPeriod = false;
        $scope.showSimpanKacab = false;
        console.log("KECAB", $scope.Role.RoleId);
    }
    else if($scope.Role.RoleName == "SUPERVISOR SALES"){
        $scope.SupervisorView = true;
        $scope.showgridSPV = true;
        $scope.targetAssignment = false;
        $scope.showModel = false;
        $scope.disabledPeriod = false;
        $scope.showSimpanSPV = false;
        console.log("SPV", $scope.Role.RoleId);
    }

    //----------------------------------
    // Get Data test
    //----------------------------------
    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    SalesCompositionHistoryFactory.getDataPeriod().then(
        function(res){
            $scope.getPeriod = res.data.Result;
            console.log("SDF", $scope.getPeriod);
            return res.data;
        }
    );

    MaintainSOFactory.getOutlet().then(
        function(res){
            $scope.getOutlet = res.data.Result;
            return res.data;
        }
    );

    MaintainSOFactory.getDataSalesman().then(
        function(res){
            $scope.getDataSalesman = res.data.Result;
            return res.data;
        }
    );
    
    $scope.selectPeriod = function(selected){
        $scope.getMonth = selected;
        console.log("Month", $scope.getMonth);
        var getPeriod = "?Period=" + $scope.getMonth.moon;
        if($scope.Role.RoleName == "KACAB"){
            SalesCompositionHistoryFactory.getData(getPeriod).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    console.log("KECABDATA", $scope.grid.data);
                    return res.data;
                }
            );
            console.log("KECAB");
        }
        else{
            SalesCompositionHistoryFactory.getDataSPV(getPeriod).then(
                function(res){
                    $scope.gridSPV.data = res.data.Result;
                    return res.data;
                }
            );
            console.log("SPV");
        }
    }

    $scope.editKacab = function (selectedRow){
        $scope.kacabDetail = selectedRow;
        console.log("ViewDetail", $scope.kacabDetail);

        var getSales = "?Period=" + $scope.getMonth.Period + "&VehicleModelId=" + $scope.kacabDetail.VehicleModelId;
        SalesCompositionHistoryFactory.getDataKacabdetail(getSales).then(
            function(res){
                $scope.gridKacabedit.data = res.data.Result;
                return res.data;
            }
        );
        $scope.showgridKacabDetail = true;
        $scope.showSimpanKacab = true;
        $scope.disabledPeriod = true;
        $scope.showModel = true;
        $scope.showgridKacab = false;
        $scope.DisabledbtnSave = false;
    }

    $scope.DetailKacab = function (selectedRow) {
        console.log("Detail", selectedRow);
        $scope.kacabDetail = selectedRow;
        console.log("ViewDetail", $scope.kacabDetail);

        var getSales = "?Period=" + $scope.getMonth.Period + "&VehicleModelId=" + $scope.kacabDetail.VehicleModelId;
        SalesCompositionHistoryFactory.getDataKacabdetail(getSales).then(
            function(res){
                $scope.gridKacabedit.data = res.data.Result;
                return res.data;
            }
        );
        $scope.showgridKacabDetail = true;
        $scope.showSimpanKacab = true;
        $scope.disabledPeriod = true;
        $scope.showModel = true;
        $scope.showgridKacab = false;
        $scope.DisabledbtnSave = true;
    }

    $scope.btnKacabSimpan = function(){

        $scope.KacabSave = $scope.gridKacabedit.data;
        console.log("SAVE", $scope.KacabSave);
        SalesCompositionHistoryFactory.createKacab($scope.KacabSave).then(
            function(res){
                // SalesCompositionHistoryFactory.getData().then(
                //     function(res){
                //         $scope.grid.data = res.data.Result;
                //     });
                console.log("err=>",success);
            },
            function(err)
                {console.log("err=>",failed);
            }
        );
        $scope.showgridKacab = true;
        $scope.showModel = false;
        $scope.disabledPeriod = false;
        $scope.showSimpanKacab = false;
        $scope.showgridKacabDetail = false;
    }

    $scope.editSPV = function (selectedRowSPV){
        $scope.spvDetail = selectedRowSPV;
        console.log("ViewDetail", $scope.spvDetail);

        var getSales = "?Period=" + $scope.getMonth.Period + "&VehicleModelId=" + $scope.spvDetail.VehicleModelId;
        SalesCompositionHistoryFactory.getDataSPVdetail(getSales).then(
            function(res){
                $scope.gridSPVedit.data = res.data.Result;
                return res.data;
            }
        );
        $scope.showgridSPVDetail = true;
        $scope.showSimpanSPV = true;
        $scope.disabledPeriod = true;
        $scope.showModel = true;
        $scope.showgridSPV = false;
        $scope.DisabledbtnSave = false;
    }

    $scope.DetailSPV = function (selectedRowSPV){
        $scope.spvDetail = selectedRowSPV;
        console.log("ViewDetail", $scope.spvDetail);

        var getSales = "?Period=" + $scope.getMonth.Period + "&VehicleModelId=" + $scope.spvDetail.VehicleModelId;
        SalesCompositionHistoryFactory.getDataSPVdetail(getSales).then(
            function(res){
                $scope.gridSPVedit.data = res.data.Result;
                return res.data;
            }
        );
        $scope.showgridSPVDetail = true;
        $scope.showSimpanSPV = true;
        $scope.disabledPeriod = true;
        $scope.showModel = true;
        $scope.showgridSPV = false;
        $scope.DisabledbtnSave = true;
    }

    $scope.btnSPVSimpan = function(){
        $scope.SPVSave = $scope.gridSPVedit.data;
        console.log("SelectData",  $scope.SPVSave);
        SalesCompositionHistoryFactory.createSPV($scope.SPVSave).then(
            function(res){
                // SalesCompositionHistoryFactory.getData().then(
                //     function(res){
                //         $scope.grid.data = res.data.Result;
                //     });
                console.log("Result", Success);
            },
            function(err)
                {console.log("err=>", failed);
            }
        );

        $scope.showgridSPV = true;
        $scope.showgridSPVDetail = false;
        $scope.showModel = false;
        $scope.disabledPeriod = false;
        $scope.showSimpanSPV = false;
    }

    $scope.btnKacabKembali = function(){
        $scope.showgridKacabDetail = false;
        $scope.showSimpanKacab = false;
        $scope.showgridSPVDetail = false;
        $scope.showSimpanSPV = false;
        $scope.disabledPeriod = false;
        $scope.showModel = false;
        $scope.showgridKacab = true;
        $scope.showgridSPV = true;
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    
    var btnActionKacab = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.editKacab(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.DetailKacab(row.entity)">Detail</u></span>'
    var btnActionSPV = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.editSPV(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.DetailSPV(row.entity)">Detail</u></span>'
    
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Sales Id', field:'VehicleModelId', visible:false },
            { name:'Model', field:'VehicleModelName'}, 
            { name:'Total Target', field:'SalesTarget3Month'}, 
            { name:'Persentase model', field:'SalesTargetToTarget3MonthPercentage'},
            {
                name:'Action',  
                allowCellFocus: false,
                width:'15%',                    
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: btnActionKacab
            }
        ]
    };

    $scope.grid.onRegisterApi = function(gridApi){
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            $scope.selectedRow = gridApi.selection.getSelectedRows();
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
           $scope.selectedRow = gridApi.selection.getSelectedRows();
        });
    };

    $scope.gridKacabedit = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Sales Id', field:'TargetAssignmentSupervisorId', visible:false },
            { name:'Supervisor', field:'EmployeeName'}, 
            { name:'Average achievement 3 months', field:'AvgSalesAchievement'}, 
            { name:'Average percentage 3 months', field:'AvgAchievementPercentage'},
            { name:'Target', field:'SalesTarget'}, 
            { name:'percentage Composition', field:'SalesTargetPercentage'}
        ]
    };

    $scope.gridSPV = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Sales Id', field:'VehicleModelId', visible:false },
            { name:'Model', field:'VehicleModelName'}, 
            { name:'Total Target', field:'SalesTarget3Month'}, 
            { name:'Persentase model', field:'SalesTargetToTarget3MonthPercentage'},
            {
                name:'Action',  
                allowCellFocus: false,
                width:'15%',                    
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: btnActionSPV
            }
        ]
    };

    $scope.gridSPV.onRegisterApi = function(gridApi){
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            $scope.selectedRowSPV = gridApi.selection.getSelectedRows();
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
           $scope.selectedRowSPV = gridApi.selection.getSelectedRows();
        });
    };

    $scope.gridSPVedit = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Sales Id', field:'TargetAssignmentSalesmanId', visible:false },
            { name:'Salesman', field:'EmployeeName'}, 
            { name:'Kategori', field:'EmployeeCategorySales'}, 
            { name:'Average achievement 3 months', field:'AvgSalesAchievement'}, 
            { name:'Average percentage 3 months', field:'AvgSalesAchievementPercentage'},
            { name:'Target', field:'SalesTarget'},
                // editableCellTemplate: '<div><form name="inputForm"><input type="number" ng-class="\'colt\' + col.uid" \
                // ui-grid-editor ng-model="MODEL_COL_FIELD"></form></div>' }, 
            { name:'percentage Composition', field:'SalesTargetPercentage'}
        ]
    };
});
