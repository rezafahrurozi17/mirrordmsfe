angular.module('app')
.controller('StockViewMobileController', function($filter, $scope, $http, CurrentUser, StockViewMobileFactory, ModelFactory, TipeFactory, $timeout,bsNotify) {
   	//----------------------------------
    // Start-Up
    //----------------------------------
    $scope.MainForm = true;
    $scope.showDetail = false;

 	//----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();    
    $scope.xRole = {selected:[]};
    $scope.filter = {VehicleModelId:null, dataYear:null};
    $scope.VehicleColor = [];

    //----------------------------------
    // Get Data
    //----------------------------------
    StockViewMobileFactory.getVehicleModel().then(function(res){
        $scope.optionsModelStockViewMobile = res.data.Result;
		$scope.filter.VehicleModelId=$scope.optionsModelStockViewMobile[0].VehicleModelId;
        $scope.StockViewMobileDaFilterModelChanged();
		$scope.ShowListGroup=false;
		$scope.ShowListGroup2=false;
		
		//return res.data;
    });
	
	$scope.StockViewMobileDaFilterModelChanged = function(){
		StockViewMobileFactory.getYear($scope.filter.VehicleModelId).then(function(res){;
			$scope.dataYear = res.data.Result;
			$scope.filter.dataYear=$scope.dataYear[0].Year;
		});
	}

    

	$scope.filterModelStockViewMobile = function() {
		$scope.StockViewMobilePaksaYearLihatDetail=angular.copy($scope.filter.dataYear);
        
		if((typeof $scope.filter.VehicleModelId !="undefined")&&(typeof $scope.filter.dataYear!="undefined") && $scope.filter.VehicleModelId !=null && $scope.filter.dataYear!=null)
		{
			var Search = "&VehicleModelId=" + $scope.filter.VehicleModelId + "&Year=" + $scope.filter.dataYear;
			console.log("Filter",Search);
			StockViewMobileFactory.getData(Search).then(
				function(res){
					//$scope.Vehicle = {};
					$scope.ownStock = res.data.Result;
					console.log("CAR", $scope.ownStock);
				},
				function(err){
					console.log("err=>",err);
				}
			);

			StockViewMobileFactory.getStockBasket(Search).then(
				function(res){
					//$scope.Vehicle = {};
					$scope.sBasket = res.data.Result;
					console.log("sBasket", $scope.sBasket);
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		else
		{
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Model dan tahun tidak boleh kosong.",
					type: 'warning'
				}
			);
		}
        
    }

    $scope.btnKembali = function(){
		$scope.showDetail = false;
		$scope.MainForm = true;
    }

    $scope.openDetail = function(item){
        $scope.lihatDetail = item;
        console.log("Detail", $scope.lihatDetail);
    	angular.element('.ui.small.modal.Detail').modal('show');
    }
	$scope.changeFormatDate = function(item) {
		var tmpParam = item;
		tmpParam = new Date(tmpParam);
		var finalDate
		var yyyy = tmpParam.getFullYear().toString();
		var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpParam.getDate().toString();
		finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
		
		return finalDate;
	}
    $scope.ViewDetailListData =function(PaksaTaon){
        console.log("setan",PaksaTaon);
		var fType = "VehicleTypeColorId=" + $scope.lihatDetail.VehicleTypeColorId+"&StatusStockId=1&Year="+PaksaTaon;
        console.log("Filter",fType);
        StockViewMobileFactory.getDetailStock(fType).then(
            function(res){
                $scope.detailTypeColor = res.data.Result;
				for(var i in $scope.detailTypeColor){
					$scope.detailTypeColor[i].PLOD = $scope.changeFormatDate($scope.detailTypeColor[i].PLOD); 
				}
                console.log("CAR", $scope.detailTypeColor);
            },
            function(err){
                console.log("err=>",err);
            }
        );
		console.log("fType",fType);
    	angular.element('.ui.small.modal.Detail').modal('hide');
    	$scope.MainForm = false;
    	$scope.showDetail = true;
		$scope.ShowListGroup3=true;
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }	
});