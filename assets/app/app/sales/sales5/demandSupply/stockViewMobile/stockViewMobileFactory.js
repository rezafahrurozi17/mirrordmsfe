angular.module('app')
  .factory('StockViewMobileFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/DemandSupplyStockViewGroup/?start=1&limit=1000000' + param);
        return res;
      },

      getVehicleModel: function(){
          var res=$http.get('/api/sales/DemandSupplyVehicleModelStock?StatusStockId=1');
          return res;
      },

      getStockBasket: function(param){
          var res=$http.get('/api/sales/DemandSupplyStockBasketGroup/?start=1&limit=1000000' + param);
          return res;
      },
      
      getYear: function(Da_VehicleModelId) {
          var res=$http.get('/api/sales/DemandSupplyVehicleYearStock/GroupDealer?VehicleModelId='+Da_VehicleModelId+'&StatusStockId=1');
          return res;
      },

      getDetailStock: function(param) {
          var res=$http.get('/api/sales/DemandSupplyStockView/MobileDetail/?' + param);
          return res;
      },

      update: function(SampleUITemplateFactory){
          return $http.put('/api/fw/Role', [{
                SalesProgramId: SampleUITemplateFactory.SalesProgramId,
                SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
      },

      delete: function(id) {
          return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });