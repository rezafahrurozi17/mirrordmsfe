angular.module('app')
    .controller('TargetAssignmentController', function(MobileHandling, uiGridConstants, uiGridGroupingConstants, $scope, $http, CurrentUser, TargetAssignmentFactory, MaintainSOFactory, ModelFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
            //----------Begin Check Display---------
                      
            $scope.bv = MobileHandling.browserVer();      
            $scope.bver = MobileHandling.getBrowserVer();       //console.log("browserVer=>",$scope.bv);
                   //console.log("browVersion=>",$scope.bver);
                   //console.log("tab: ",$scope.tab);
                  
            if ($scope.bv !== "Chrome") {       if ($scope.bv == "Unknown") {         if (!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))         {           $scope.removeTab($scope.tab.sref);         }       }               }         //----------End Check Display-------
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.Role = $scope.user;
        console.log("user", $scope.Role);
        $scope.mComposition = null; //Model
        $scope.xRole = { selected: [] };
        $scope.isEdit = false;
        $scope.Role.Period = null;
        $scope.Role.moon = null;
        $scope.isNotValid = false;
        $scope.tagetAdjusmentKacab = 0;
        $scope.tagetAdjusmentSPV = 0;
        $scope.tagetSPV = 0;
        $scope.targetkacabview = false;
        $scope.targetspvview = false;

        var sekarang = new Date();
        $scope.now = new Date(sekarang.setDate("01"));
        $scope.now = new Date(sekarang.setHours("00"));
        $scope.now = new Date(sekarang.setMinutes("00"));
        $scope.now = new Date(sekarang.setSeconds("00"));

        function period(date) {
            var fixed = null;
            var year = date.getFullYear();
            var month = ('0' + (date.getMonth() + 1)).slice(-2);
            var date = ('0' + date.getDate()).slice(-2);

            fixed = parseInt((year + month + date));
            return fixed;
        };


        // console.log("tanggal",  $scope.nowInt);

        $scope.$watch("Role.Period", function(newValue, oldValue) {
            if (newValue == null || newValue == undefined) {
                $scope.gridSPV.data = [];
                $scope.gridSPVedit.data = [];
                $scope.targetspvview = false;
            } else {
                $scope.selectPeriod(newValue);
            }

            if (period(newValue) < period($scope.now)) {
                $scope.isNotValid = false;
            } else {
                $scope.isNotValid = true;
            }

        });

        $scope.tst = function() {
            console.log('test', $scope.gridview);
            // $scope.gridview.grid.treeBase.expandAll = false;
        }
        $scope.$watch("Role.moon", function(newValue, oldValue) {
            if (newValue == null || newValue == undefined) {
                $scope.grid.data = [];
                $scope.gridview.data = [];
                $scope.targetkacabview = false;
            } else {
                $scope.selectPeriod(newValue);
            }

            if (period(newValue) < period($scope.now)) {
                $scope.isNotValid = false;
            } else {
                $scope.isNotValid = true;
            }

        });

        if ($scope.Role.RoleName == "KACAB") {
            $scope.KacabView = true;
            $scope.showgridKacab = true;
            $scope.targetAssignment = false;
            $scope.showModel = true;
            $scope.disabledPeriod = false;
            $scope.showSimpanKacab = false;
            console.log("KECAB", $scope.Role.RoleId);
        } else if ($scope.Role.RoleName == "SUPERVISOR SALES") {
            $scope.SupervisorView = true;
            $scope.showgridSPV = true;
            $scope.targetAssignment = false;
            $scope.showModel = false;
            $scope.disabledPeriod = false;
            $scope.showSimpanSPV = false;
            console.log("SPV", $scope.Role.RoleId);
        }

        //----------------------------------
        // Get Data test
        //----------------------------------
        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        TargetAssignmentFactory.getDataPeriod().then(
            function(res) {
                $scope.getPeriod = res.data.Result;
                console.log("SDF", $scope.getPeriod);
                $scope.Role.Period = $scope.getPeriod[0].PeriodDate;
                $scope.Role.moon = $scope.getPeriod[0].PeriodDate;
                return res.data;
            }
        );

        MaintainSOFactory.getOutlet().then(
            function(res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataSalesman().then(
            function(res) {
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2);
                return fix;
            } else {
                return null;
            }
        };

        $scope.tempPeriod = null;

        $scope.selectPeriod = function(selected) {
            if(selected!=null && (typeof selected!="undefined"))
			{
				$scope.tempPeriod = angular.copy(selected);
				$scope.getMonth = angular.copy(selected);
				console.log("Month", selected);
				var getPeriod = "?Period=" + fixDate($scope.getMonth);
				if ($scope.Role.RoleName == "KACAB") {

					TargetAssignmentFactory.getData(getPeriod).then(
						function(res) {
							build(res.data.Result);
							$scope.grid.data = build(res.data.Result);
							$scope.gridview.data = build(res.data.Result);
							$scope.tagetAdjusmentKacab = sumCalculate(res.data.Result);
							$scope.targetkacabview = true;
							return res.data;
						}
					);

				} else {
					TargetAssignmentFactory.getDataSPV(getPeriod).then(
						function(res) {
							$scope.gridSPV.data = build(res.data.Result);
							$scope.gridSPVedit.data = build(res.data.Result);
							$scope.tagetAdjusmentSPV = res.data.Result[0].TotalTargetSPV;
							$scope.tagetSPV = sumCalculate(res.data.Result);
							$scope.targetspvview = true;
							return res.data;
						}
					);
				}
			}
			
        }


        $scope.btnKacabSimpan = function() {
            console.log("SAVE", $scope.KacabSave);
            // var total = 0;
            // for(var i in $scope.grid.data){
            //     total = total + $scope.grid.data[i].Target;
            // };
            // console.log('total', total, $scope.tagetAdjusmentKacab)
            // if(total < $scope.tagetAdjusmentKacab){
            //     bsNotify.show(
            //         {
            //             title: "Peringatan",
            //             content: "Total target tidak boleh lebih kecil dari Target Adjustmen.",
            //             type: 'warning'
            //         }
            //     );
            // }else{
            TargetAssignmentFactory.createKacab($scope.grid.data).then(
                function(res) {
                    $scope.selectPeriod($scope.tempPeriod);
                    $scope.isEdit = false;
                    $scope.showgridKacab = true;
                    $scope.showModel = false;
                    $scope.disabledPeriod = false;
                    $scope.showSimpanKacab = false;
                    $scope.showgridKacabEdit = false;
                    $scope.gridview.treeBase.collapseAllRows();
                    $scope.grid.treeBase.collapseAllRows();
                    // for(var i = 0; i<1; i++){
                    //     $scope.gridview.treeBase.collapseAllRows();
                    //     $scope.grid.treeBase.collapseAllRows();
                    // }
					$scope.selectPeriod($scope.Role.moon);
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Target Supervisor berhasil disimpan.",
                        type: 'success'
                    });
                },
                function(err) {
                    //console.log("err=>", err);
                    bsNotify.show({
                        title: "Error Message",
                        content: err.data.Message,
                        type: 'danger'
                    });
                }
            );
            //}

        }


        $scope.btnSPVSimpan = function() {
            var total = 0;
            for (var i in $scope.gridSPVedit.data) {
                total = total + $scope.gridSPVedit.data[i].Target;
            }
            //console.log('total', total, $scope.tagetSPV)
            if (total < $scope.tagetAdjusmentSPV) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Total target tidak boleh lebih kecil dari Target SPV.",
                    type: 'warning'
                });
            } else {
                TargetAssignmentFactory.createSPV($scope.gridSPVedit.data).then(
                    function(res) {
                        $scope.selectPeriod($scope.tempPeriod);
                        $scope.isEdit = false;
                        $scope.showgridKacab = true;
                        $scope.showModel = false;
                        $scope.disabledPeriod = false;
                        $scope.showSimpanKacab = false;
                        $scope.showgridKacabEdit = false;
                        $scope.gridSPV.treeBase.collapseAllRows();
                        $scope.gridSPVedit.treeBase.collapseAllRows();
                        // $scope.gridSPV.grid.treeBase.expandAll = false;
                        // $scope.gridSPVedit.grid.treeBase.expandAll = false;
						
						$scope.selectPeriod($scope.Role.Period);
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Target Sales berhasil disimpan.",
                            type: 'success'
                        });
                    },
                    function(err) {
                        //console.log("err=>", failed);
                    }
                );
                $scope.showgridSPV = true;
                $scope.showgridSPVDetail = false;
                $scope.showModel = false;
                $scope.disabledPeriod = false;
                $scope.showSimpanSPV = false;
            }

        }
		
		$scope.TargetAssignmentRefresh = function() {
			if ($scope.Role.RoleName == "KACAB")
			{
				$scope.selectPeriod($scope.Role.moon);
			}
			else if ($scope.Role.RoleName == "SUPERVISOR SALES")
			{
				$scope.selectPeriod($scope.Role.Period);
			}
		}

        $scope.edit = function() {
            // if($scope.Role.moon < new Date()){
            //     $scope.grid.enableCellEdit = false;
            // }else{
            //     $scope.grid.enableCellEdit = true;
            // }

            $scope.showgridKacabEdit = true;
            $scope.showgridKacab = false;
            $scope.isEdit = true;
        }

        $scope.batal = function() {
            $scope.selectPeriod($scope.Role.moon);
            $scope.showgridKacabEdit = false;
            $scope.showgridKacab = true;
            $scope.isEdit = false;
            $scope.gridview.treeBase.collapseAllRows();
            $scope.grid.treeBase.collapseAllRows();
        }

        $scope.editspv = function() {
            // if($scope.Role.Period < new Date()){
            //     $scope.gridSPVedit.enableCellEdit = false;
            // }else{
            //     $scope.gridSPVedit.enableCellEdit = true;
            // }
            $scope.showgridSPVDetail = true;
            $scope.showgridSPV = false;
            $scope.isEdit = true;
        }

        $scope.batalspv = function() {
            $scope.selectPeriod($scope.Role.Period);
            $scope.showgridSPVDetail = false;
            $scope.showgridSPV = true;
            $scope.isEdit = false;
            $scope.gridSPV.treeBase.collapseAllRows();
            $scope.gridSPVedit.treeBase.collapseAllRows();
        }


        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        var btnActionKacab = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.editKacab(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.DetailKacab(row.entity)">Detail</u></span>'
        var btnActionSPV = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.editSPV(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.DetailSPV(row.entity)">Detail</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------

        $scope.gridview = {
            enableSorting: false,
            enableRowSelection: false,
            enableRowHeaderSelection: false,
            multiSelect: false,
            enableColumnResizing: true,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [{
                    name: 'Supervisor',
                    field: 'SPVEmployeeName',
                    enableCellEdit: false,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'Model', field: 'VehicleModelName', enableCellEdit: false },
                {
                    name: 'Rata-rata pencapaian 3 bulan',
                    field: 'Ach3Month',
                    aggLabelFilter: "persen",
                    enableCellEdit: false,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'Average percentage 3 months (%)', field:'PercentAch3Month', aggLabelFilter:"persen",enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value;
                //   }},
                {
                    name: 'Target*',
                    field: 'Target',
                    enableCellEdit: false,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'percentage Composition (%)', field:'SalesTargetPercentage', enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM,  customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value;
                //   }},
            ]
        };

        $scope.grid = {
            enableSorting: false,
            enableRowSelection: false,
            enableRowHeaderSelection: false,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [{
                    name: 'Supervisor',
                    field: 'SPVEmployeeName',
                    enableCellEdit: false,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'Model', field: 'VehicleModelName', enableCellEdit: false },
                {
                    name: 'Rata-rata pencapaian 3 bulan',
                    field: 'Ach3Month',
                    aggLabelFilter: "persen",
                    enableCellEdit: false,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'Average percentage 3 months (%)', field:'PercentAch3Month', aggLabelFilter:"persen",enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM,  customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value +' %';
                //   }},
                {
                    name: 'Target*',
                    field: 'Target',
                    enableCellEdit: true,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'percentage Composition (%)', field:'SalesTargetPercentage', enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM,  customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = '100%';
                //   }},
            ]
        };


        $scope.gridSPV = {
            enableSorting: true,
            enableRowSelection: false,
            enableRowHeaderSelection: false,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [{
                    name: 'Sales',
                    width: '30%',
                    field: 'SalesmanEmployeeName',
                    enableCellEdit: false,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'Kategori Sales', enableCellEdit: false, field: 'GolonganSalesmanName' },
                { name: 'Model', enableCellEdit: false, field: 'VehicleModelName' },
                {
                    name: 'Rata-rata pencapaian 3 bulan',
                    field: 'Ach3Month',
                    aggLabelFilter: "persen",
                    enableCellEdit: false,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'Average percentage 3 months (%)', field:'PercentAch3Month', aggLabelFilter:"persen",enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value+ ' %';
                //   } },
                {
                    name: 'Target*',
                    field: 'Target',
                    enableCellEdit: false,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'percentage Composition (%)', field:'SalesTargetPercentage', enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM ,treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = "100 %";
                //   }},

            ]
        };

        $scope.gridSPVedit = {
            enableSorting: true,
            enableRowSelection: false,
            enableColumnResizing: true,
            multiSelect: false,
            enableRowHeaderSelection: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [{
                    name: 'Sales',
                    width: '30%',
                    field: 'SalesmanEmployeeName',
                    enableCellEdit: false,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'Kategori Sales', enableCellEdit: false, field: 'GolonganSalesmanName' },
                { name: 'Model', enableCellEdit: false, field: 'VehicleModelName' },
                {
                    name: 'Rata-rata pencapaian 3 bulan',
                    field: 'Ach3Month',
                    aggLabelFilter: "persen",
                    enableCellEdit: false,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'Average percentage 3 months (%)', field:'PercentAch3Month', aggLabelFilter:"persen",enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value+ ' %';
                //   } },
                {
                    name: 'Target*',
                    field: 'Target',
                    type: 'number',
                    enableCellEdit: true,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    //treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                // { name:'percentage Composition (%)', field:'SalesTargetPercentage', enableCellEdit: false, treeAggregationType: uiGridGroupingConstants.aggregation.SUM , treeAggregationType: uiGridGroupingConstants.aggregation.SUM, treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value+ ' %';
                //   }},

            ]
        };


        $scope.gridSPV.onRegisterApi = function(gridApi) {
            $scope.gridSPV = gridApi;
        };

        $scope.gridview.onRegisterApi = function(gridApi) {
            $scope.gridview = gridApi;
        };

        $scope.gridSPVedit.onRegisterApi = function(gridApi) {
            $scope.gridSPVedit = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
                console.log('select');
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
            });


            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                if (newValue < 0) {
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;
                    var tempcalulate = 0;
                    for (var i in $scope.gridSPVedit.data) {
                        tempcalulate = tempcalulate + $scope.gridSPVedit.data[i].Target;
                    }

                    $scope.tagetSPV = tempcalulate;
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai target tidak boleh kurang dari 0.",
                        type: 'warning'
                    });          
                } else {
                    if(newValue > 9999){
                        rowEntity[colDef.field] = 9999;
                        var tempcalulate = 0;
                        for (var i in $scope.gridSPVedit.data) {
                            tempcalulate = tempcalulate + $scope.gridSPVedit.data[i].Target;
                        }

                        $scope.tagetSPV = tempcalulate;
                                bsNotify.show({
                                    title: "Peringatan",
                                    content: "Nilai target tidak boleh lebih dari 9.999.",
                                    type: 'warning'
                        });
                    }else{
                        var tempcalulate = 0;
                        for (var i in $scope.gridSPVedit.data) {
                            tempcalulate = tempcalulate + $scope.gridSPVedit.data[i].Target;
                        }

                        $scope.tagetSPV = tempcalulate;

                        var sum = 0;
                        for (var i in $scope.gridSPVedit.data) {
    
                            if ($scope.gridSPVedit.data[i].SPVEmployeeId == rowEntity.SPVEmployeeId) {
                                sum = sum + $scope.gridSPVedit.data[i].Target;
                                //$scope.temp.push($scope.grid.data[i]);
                            }
                        }
    
                        for (var i in $scope.gridSPVedit.data) {
                            if ($scope.gridSPVedit.data[i].SPVEmployeeId == rowEntity.SPVEmployeeId) {
                                var SalesTarget = Number.parseFloat((($scope.gridSPVedit.data[i].Target / sum) * 100.0)).toFixed(2);
                                //var SalesTarget = Math.floor((($scope.grid.data[i].Target/sum) * 100.0));
                                if (isFinite(SalesTarget) == false) {
                                    $scope.gridSPVedit.data[i]['SalesTargetPercentage'] = 0
                                } else {
                                    $scope.gridSPVedit.data[i]['SalesTargetPercentage'] = SalesTarget;
                                }
                            }
                        }
                    }

                }

            });
        };

        $scope.grid.onRegisterApi = function(gridApi) {
            $scope.grid = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
                console.log('select');
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {

                

                if (newValue < 0) {
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;  
                    var tempcalulate = 0;
                    for (var i in $scope.grid.data) {
                        tempcalulate = tempcalulate + $scope.grid.data[i].Target;
                    }

                    $scope.tagetAdjusmentKacab = tempcalulate; 
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai target tidak boleh kurang dari 0.",
                        type: 'warning'
                    });           
                } else {
                    if(newValue > 9999){
                        rowEntity[colDef.field] = 9999;
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Nilai target tidak boleh lebih dari 9.999.",
                            type: 'warning'
                        });
                        var tempcalulate = 0;
                        for (var i in $scope.grid.data) {
                            tempcalulate = tempcalulate + $scope.grid.data[i].Target;
                        }

                        $scope.tagetAdjusmentKacab = tempcalulate;
                        //console.log("test", rowEntity);
                    }else{
                        var tempcalulate = 0;
                        for (var i in $scope.grid.data) {
                            tempcalulate = tempcalulate + $scope.grid.data[i].Target;
                        }

                        $scope.tagetAdjusmentKacab = tempcalulate;
                        //console.log("test", rowEntity);
                        
                        var sum = 0;
                        for (var i in $scope.grid.data) {

                            if ($scope.grid.data[i].SPVEmployeeId == rowEntity.SPVEmployeeId) {
                                sum = sum + $scope.grid.data[i].Target;
                                //$scope.temp.push($scope.grid.data[i]);
                            }
                        }

                        for (var i in $scope.grid.data) {
                            if ($scope.grid.data[i].SPVEmployeeId == rowEntity.SPVEmployeeId) {
                                var SalesTarget = Number((($scope.grid.data[i].Target / sum) * 100.0)).toFixed(2);
                                //var SalesTarget = Math.floor((($scope.grid.data[i].Target/sum) * 100.0));
                                if (isFinite(SalesTarget) == false) {
                                    $scope.grid.data[i]['SalesTargetPercentage'] = 0
                                } else {
                                    $scope.grid.data[i]['SalesTargetPercentage'] = SalesTarget;
                                }

                            }
                        }
                    }

                }

            });
        };

        function sumCalculate(tempdata) {
            var data = angular.copy(tempdata);
            var total = 0;
            for (var i in data) {
                total = total + data[i].Target;
            }
            return total;
        }

        function build(tempdata) {
            var data = angular.copy(tempdata);
            var tempModel = [];
            for (var i in data) {
                if (tempModel.length == 0) {
                    tempModel.push({
                        VehicleModelName: data[i].VehicleModelName,
                        TotalTarget: data[i].Target
                    });
                } else {
                    var _this = false;

                    for (var j = 0; j < tempModel.length; j++) {
                        if (tempModel[j].VehicleModelName == data[i].VehicleModelName) {
                            _this = true;
                            tempModel[j].TotalTarget = tempModel[j].TotalTarget + data[i].Target;
                        }
                    }

                    if (_this == false) {
                        tempModel.push({
                            VehicleModelName: data[i].VehicleModelName,
                            TotalTarget: data[i].Target
                        });
                    }
                }
            }

            for (var i in data) {
                var obj = { SalesTargetPercentage: 0 };
                _.extend(data[i], obj);

                for (var j in tempModel) {
                    if (data[i].VehicleModelName == tempModel[j].VehicleModelName) {
                        var SalesTarget = Number.parseFloat(((data[i].Target / tempModel[j].TotalTarget) * 100.0)).toFixed(2);
                        if (isFinite(SalesTarget) == false) {
                            data[i].SalesTargetPercentage = 0
                        } else {
                            data[i].SalesTargetPercentage = SalesTarget;
                        }
                    }
                }
            }

            return data;
        }

        function buildMOdel(tempdata) {
            var data = angular.copy(tempdata);
            var tempModel = [];
            for (var i in data) {
                if (tempModel.length == 0) {
                    tempModel.push({
                        SPVEmployeeId: data[i].SPVEmployeeId,
                        TotalTarget: data[i].Target
                    });
                } else {
                    var _this = false;

                    for (var j = 0; j < tempModel.length; j++) {
                        if (tempModel[j].SPVEmployeeId == data[i].SPVEmployeeId) {
                            _this = true;
                            tempModel[j].TotalTarget = tempModel[j].TotalTarget + data[i].Target;
                        }
                    }

                    if (_this == false) {
                        tempModel.push({
                            SPVEmployeeId: data[i].SPVEmployeeId,
                            TotalTarget: data[i].Target
                        });
                    }
                }
            }

            for (var i in data) {
                var obj = { SalesTargetPercentage: 0 };
                _.extend(data[i], obj);

                for (var j in tempModel) {
                    if (data[i].SPVEmployeeId == tempModel[j].SPVEmployeeId) {
                        var SalesTarget = Number.parseFloat(((data[i].Target / tempModel[j].TotalTarget) * 100.0)).toFixed(2);
                        if (isFinite(SalesTarget) == false) {
                            data[i].SalesTargetPercentage = 0
                        } else {
                            data[i].SalesTargetPercentage = SalesTarget;
                        }
                    }
                }
            }

            return data;
        }

    });