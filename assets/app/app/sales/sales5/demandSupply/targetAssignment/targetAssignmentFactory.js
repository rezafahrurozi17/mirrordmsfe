angular.module('app')
  .factory('TargetAssignmentFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;

    function fixDate(date) {
      if (date != null || date != undefined) {
          var fix = date.getFullYear() + '-' +
              ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
              '01T' +
              ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
              ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
              ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
          return fix;
      } else {
          return null;
      }
  };

    return {
      getData: function(param) {
         var res=$http.get('/api/sales/DemandSupplyNewTargetAssignmentSPVView/' + param);         
          return res;
      },

      getDataKacabdetail: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignKacabViewDetailFx' + param);         
          return res;
      },

      getDataSPV: function(param) {
        var res=$http.get('/api/sales/DemandSupplyNewTargetAssignmentSalesmanView' + param);         
          return res;
      },

      getDataSPVdetail: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignSPVViewDetailFx' + param);         
          return res;
      },

      getDataPeriod: function() {
        var res=$http.get('/api/sales/DemandSupplyOAPRAPMonth?monthMin=3&monthMax=0');         
          return res;
      },

      createKacab: function(targetAssign) {
        console.log("adasd",targetAssign);
        for(var i in targetAssign){
          targetAssign[i].Period = fixDate(targetAssign[i].Period);
        }
        return $http.post('/api/sales/DemandSupplyNewTargetAssignmentSPVView', targetAssign);
      },

      createSPV: function(targetAssign) {
        for(var i in targetAssign){
          targetAssign[i].Period = fixDate(targetAssign[i].Period);
        }
        return $http.post('/api/sales/DemandSupplyNewTargetAssignmentSalesmanView', targetAssign);
      }
    }
});