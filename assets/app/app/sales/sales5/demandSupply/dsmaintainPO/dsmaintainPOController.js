angular.module('app')
    .controller('DsMaintainPOController', function($scope,$filter, $http, CurrentUser, DsMaintainPOFactory, MaintainPOFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.LihatForm = false;
        $scope.MaintainForm = true;

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
		
		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mDsMaintainPO = null; //Model
        $scope.xRole = { selected: [] };
		$scope.filter={ PODateStart:null, PODateEnd:null};
		
		var da_today_date=new Date();
		$scope.filter.PODateEnd=new Date();
		$scope.filter.PODateStart=new Date(da_today_date.setDate(1));

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            DsMaintainPOFactory.getData("filter",$scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        MaintainPOFactory.getTypePO().then(
            function(tipe) {
                $scope.getTypePO = tipe.data.Result;
                return tipe.data;
            }
        );

        $scope.ModalClosePO = function() {
            angular.element('#ModalClosePO').modal();
        }

        $scope.btnBatalModal = function() {
            angular.element('#ModalClosePO').modal('hide');
        }
		
		$scope.DsMaintainPoJagaFilterTanggal = function() {
            if($scope.filter.PODateEnd<$scope.filter.PODateStart)
			{
				$scope.filter.PODateEnd=$scope.filter.PODateStart;
			}
        }

        $scope.btnKembaliFormMaintain = function() {
            $scope.LihatForm = false;
            $scope.MaintainForm = true;
        }

        $scope.PONOClick = function(row) {
            $scope.LihatForm = true;
            $scope.MaintainForm = false;
            DsMaintainPOFactory.getData("no_filter",row.POSwappingReceiveId).then(
                function(res) {
                    $scope.detailPOSwapping = res.data.Result[0];
					
					$scope.detailPOSwapping.Price=$filter('currency')($scope.detailPOSwapping.Price, "");
					$scope.detailPOSwapping.Discount=$filter('currency')($scope.detailPOSwapping.Discount, "");
					$scope.DsMaintainPoTabel_AfterDiskon=$scope.detailPOSwapping.Price - $scope.detailPOSwapping.Discount;
					$scope.DsMaintainPoTabel_AfterDiskon=$filter('currency')($scope.DsMaintainPoTabel_AfterDiskon, "");
					$scope.detailPOSwapping.DPP=$filter('currency')($scope.detailPOSwapping.DPP, "");
					$scope.detailPOSwapping.PPN=$filter('currency')($scope.detailPOSwapping.PPN, "");
					$scope.detailPOSwapping.PPH22=$filter('currency')($scope.detailPOSwapping.PPH22, "");
					
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnUbahPO = function() {
            $scope.LihatForm = true;
            $scope.MaintainForm = false;
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
		
		$scope.getData();

        var ClickNoPO = '<a style="color:blue;"  ng-click="grid.appScope.$parent.PONOClick(row.entity)"><p style="padding:5px 0 0 5px"><u>{{row.entity.POCode}}</u></p></a>';
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'POSwappingId', visible: false },
                { name: 'Tipe Warna', field: 'Dekatsuco' },
                //{ name: 'warna', field: 'ColorName' },
                { name: 'Tanggal PO', field: 'PODate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                {
                    name: 'No PO',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: ClickNoPO
                },
                { name: 'Status PO', field: 'POStatusName' },
				{ name: 'Cabang Yang Meminta', field: 'OutletRequestorName' }
            ]
        };
    });