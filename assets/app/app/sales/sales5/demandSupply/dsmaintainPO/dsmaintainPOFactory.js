angular.module('app')
    .factory('DsMaintainPOFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(maunya,filter) {
                var param ="";
				var res="";
				if (maunya=="no_filter") 
				{
                    param = '?POSwappingReceiveId=' + filter;
					res = $http.get('/api/sales/DemandSupplyPOSwapping' + param);
                } 
				else if(maunya=="filter")
				{
                    param = $httpParamSerializer(filter);
					res = $http.get('/api/sales/DemandSupplyPOSwapping/?' + param);
                }

                
                return res;
            },

            create: function(maintainSO) {
                return $http.post('/api/fw/Role', [{
                    Id: maintainSO.SOId,
                    SODate: maintainSO.SODate,
                    Status: maintainSO.StatusSO,
                    NOSPK: maintainSO.NOSPK,
                    ProspectId: maintainSO.ProspectId,
                    ProspectName: maintainSO.ProspectName
                }]);
            },

            update: function(maintainSO) {
                return $http.put('/api/fw/Role', [{
                    SOId: maintainSO.SOId,
                    SODate: maintainSO.SODate,
                    Type: maintainSO.SOType,
                    Status: maintainSO.StatusSO,
                    NOSPK: maintainSO.NOSPK,
                    ProspectId: maintainSO.ProspectId,
                    ProspectName: maintainSO.ProspectName
                }]);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });