angular.module('app')
  .factory('FindLocationFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          for(key in filter){
              if(filter[key]==null || filter[key]==undefined)
                delete filter[key];
            }
            var res=$http.get('/api/sales/DemandSupplyMonitoringUnit/?start=1&limit=10000&'+$httpParamSerializer(filter));
            return res;
      },

      create: function(FindLocation) {
        return $http.post('/api/fw/Role', [{
              SalesProgramName: FindLocation.SalesProgramName}]);
      },

      searchLocation: function(SearchLocation) {
        return $http.post('/api/sales/TLSFindVehicleLocation/array', SearchLocation);
      },

      updateLocation: function(UpdateLocation) {
        return $http.put('/api/sales/DemandSupply_FindLocation', UpdateLocation);
      },
      
      update: function(FindLocation){
        return $http.put('/api/sales/TLSFindVehicleLocation', [{
              SalesProgramId: FindLocation.SalesProgramId,
              SalesProgramName: FindLocation.SalesProgramName}]);
      },
      
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });