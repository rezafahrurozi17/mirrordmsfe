angular.module('app')
    .controller('FindLocationController', function($scope, $http, $httpParamSerializer, CurrentUser, FindLocationFactory, MaintainSOFactory, ModelFactory, TipeFactory, ComboBoxFactory, StockViewMobileFactory, $timeout, $rootScope, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mFindLocation = null; //Model
    $scope.filter = {OutletId:null, VehicleModelId:null, VehicleTypeId:null, ColorId:null, Year:null, PlanUnitinCabang:null, PlanUnitatPDC:null, PlanDelivery:null};
    $scope.xRole={selected:[]};

    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEnd = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    }

    $scope.tanggalmin = function (dt){
        $scope.dateOptionsEnd.minDate = dt;
    }

    //----------------------------------
    // Get Data test
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        if($scope.filter.OutletId==null || $scope.filter.VehicleModelId==null)
		{
			bsNotify.show(
								{
									title: "peringatan",
									content: "Filter mandatory tidak boleh kosong.",
									type: 'warning'
								}
							);
		}
		else
		{
			FindLocationFactory.getData($scope.filter).then(
				function(res){
					$scope.grid.data = res.data.Result;
					$scope.loading = false;
					return res.data.Result;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
    }

    MaintainSOFactory.getOutlet().then(
        function(res){
            $scope.getOutlet = res.data.Result;
			$scope.filter.OutletId=$scope.user.OutletId;
            return res.data;
        }
    );

    

    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    var listTipe = [];
    TipeFactory.getData().then(
        function(res){
            listTipe = res.data.Result;
            return res.data;
        }
    );

    $scope.pilihType = function (selected) {
        $scope.filter.ColorId=null;
		ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected).then(
            function (res) {
            $scope.listWarna = res.data.Result;
        })
    };

    $scope.filterModel = function(Selectmodel) {
       
		StockViewMobileFactory.getYear(Selectmodel.VehicleModelId).then(function(res){;
            $scope.dataYear = res.data.Result;
            return res.data;
        });
        
        $scope.getTipe = listTipe.filter(function(test){
            return (test.VehicleModelId == Selectmodel.VehicleModelId);
        })
		
		//$scope.filter.VehicleTypeId=$scope.getTipe[0].VehicleTypeId;
		//$scope.pilihType($scope.getTipe[0].VehicleTypeId);
		$scope.filter.VehicleTypeId=null;
		$scope.filter.ColorId=null;
		$scope.filter.Year=null;
		$scope.pilihType(null);
		
        var filterModel = $scope.listTipe.filter(function(obj){
            return obj.VehicleTypeId == $scope.mFindLocation.VehicleTypeId;
        })
        if(filterModel.length == 0)
		{
			$scope.mFindLocation.VehicleTypeId = null;
		}
		
            
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }

    $scope.UpdateLocation = function () {
        //console.log('SelectedData',SelectedData);
        //$scope.selctedRowFindLocation;
		//$scope.PostData = {};
		$scope.PostData = [];
		console.log("setan",$scope.selctedRowFindLocation);
        //$scope.PostData.FrameNumber = SelectedData.FrameNo;
		for(var i = 0; i < $scope.selctedRowFindLocation.length; ++i)
		{	
			//$scope.PostData[i].FrameNumber = $scope.selctedRowFindLocation[i].FrameNo;
			$scope.PostData.push({ FrameNumber:$scope.selctedRowFindLocation[i].FrameNo});
		}

        FindLocationFactory.searchLocation($scope.PostData).then(
            function(res){
                $scope.UpdateDataLocation = res.data;
                $scope.loading = false;
                console.log(' $scope.UpdateDataLocation', $scope.UpdateDataLocation);
                FindLocationFactory.updateLocation($scope.UpdateDataLocation).then(
                    function(res){
                        //$scope.UpdateDataLocation = res.data;
                        $scope.loading = false;
                        //console.log(' $scope.UpdateDataLocation', $scope.UpdateDataLocation);
                        bsNotify.show(
                            {
                                title: "Message",
                                content: "Data Berhasil diubah!",
                                type: 'success'
                            }
                        );
                        //$rootScope.$emit("RefreshDirective", {});
						$scope.getData();
						$scope.selctedRowFindLocation=[];
                    },
                    function(err){
                        console.log("err=>",err);
                        bsNotify.show(
                            {
                                title: "Error Message",
                                content: err.data.Message,
                                type: 'danger'
                            }
                        );
						$scope.selctedRowFindLocation=[];
                    }
                );

            },
            function(err){
                console.log("err=>",err);
                bsNotify.show(
                    {
                        title: "Error Message",
                        content: err.data.Message,
                        type: 'danger'
                    }
                );
            }
        );
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
		$scope.selctedRowFindLocation = rows;
    }
	
    
    //var updateLocationClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px" ng-click="grid.appScope.$parent.UpdateLocation(row.entity)"><u><i title="Refresh Location" style="transform: rotate(90deg);" class="fa fa-refresh" aria-hidden="true"></i></u></p></span>';
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: true,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'No rangka', field:'FrameNo', width:'14%'},
            { name:'Model', field:'VehicleModelName', width:'15%'}, 
			{ name:'Tipe', field:'VehicleTypeDescription', width:'11%'}, 
			{ name:'Warna', field:'ColorCodeName', width:'10%'},
			{ displayName:'PLOD',name:'PLOD', field:'PLOD', width:'10%', cellFilter: 'date:\'dd-MM-yyyy\''},
			{ displayName:'DO TAM',name:'Do TAM', field:'DOTAMDate', width:'10%', cellFilter: 'date:\'dd-MM-yyyy\''},
			{ name:'Lokasi', field:'LastLocation', width:'18%'},
            { displayName:'Plan Unit PDC',name:'Plan unit pdc', field:'PlanUnitinPDC', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\''},
			{ displayName:'Actual Unit PDC',name:'Actual unit pdc', field:'ActualUnitinPDC', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'Plan unit in cabang', field:'PlanUnitinCabang', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'Actual unit in cabang', field:'ActualUnitinCabang', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'Rencana Pengiriman', field:'PlanDelivery', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'Tipe Pengiriman', field:'SendingType', width:'15%'},
            { name:'Driver', field:'Driver', width:'10%'}
        ]
    };
	
	
	
	$scope.grid.onRegisterApi = function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRowFindLocation = gridApi.selection.getSelectedRows();
				
			});
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRowFindLocation = gridApi.selection.getSelectedRows();
				
			});
        };
});
