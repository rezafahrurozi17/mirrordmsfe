angular.module('app')
    .controller('MatchingStockDealerController', function(bsNotify,$scope, $http, ComboBoxFactory, CurrentUser, MatchingStockDealerFactory, FindStockFactory,ModelFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MainFormShow = true;
        $scope.showStock = false;
        $scope.SuggestFormShow = false;
		$scope.ShowMatchingStockDealerAdvSearch=true;
		
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.xRole = { selected: [] };
        $scope.draft = { AreaName: null, ProvinceName: null, CityRegencyName: null };
        $scope.filter = { AreaId: null, ProvinceId: null, CityRegencyId: null, VehicleModelId: null };
        $scope.disabledBtnRequestSwapping = false;
        //----------------------------------
        // Get Data 
        //----------------------------------

        ModelFactory.getData().then(function(res){
            $scope.getModel = res.data.Result;
             return res.data;
        });

        $scope.filteringData = function() {
			if($scope.filter.AreaId==null||(typeof $scope.filter.AreaId=="undefined"))
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Field Mandatory Harus Diisi.",
							type: 'warning'
						}
					);
			}
			else if($scope.filter.AreaId!=null&&(typeof $scope.filter.AreaId!="undefined"))
			{
				var getSO = "?AreaId=" + $scope.filter.AreaId + "&ProvinceId=" + $scope.filter.ProvinceId + "&CityRegencyId=" + $scope.filter.CityRegencyId + "&VehicleModelId=" + $scope.filter.VehicleModelId;
				MatchingStockDealerFactory.getDataSO(getSO).then(
					function(res) {
						$scope.outstandingSO = res.data.Result;
						console.log("succ=>", $scope.outstandingSO);
						$scope.loading = false;
						return res.data.Result;
					},
					function(err) {
						console.log("err=>", err);
					}
				);

				var getStock = "?AreaId=" + $scope.filter.AreaId + "&ProvinceId=" + $scope.filter.ProvinceId + "&CityRegencyId=" + $scope.filter.CityRegencyId + "&VehicleModelId=" + $scope.filter.VehicleModelId;
				MatchingStockDealerFactory.getDataStock(getStock).then(
					function(res) {
						$scope.stockFree = res.data.Result;
						console.log("stockFree=>", $scope.stockFree);
						$scope.loading = false;
						$scope.showStock = true;
						return res.data.Result;
					},
					function(err) {
						console.log("err=>", err);
					}
				);
			}
            
            
        }

        FindStockFactory.getDataArea().then(
            function(res) {
                $scope.getDataArea = res.data.Result;
            }
        );
		
		$scope.ActRefreshMatchingStockDealer = function() {
			$scope.filteringData();
		}

        $scope.getAreaName = function(select) {
            // console.log("asjhdsad", select);
            if (select == undefined) {
                $scope.draft.AreaName = "All";
                $scope.outstandingSO = [];
                $scope.stockFree = [];
            } else {
                $scope.draft.AreaName = select.AreaName;
            }
        }

        $scope.getProvinceName = function(select) {
            if (select == undefined) {
                $scope.draft.ProvinceName = "All";
            } else {
                $scope.draft.ProvinceName = select.ProvinceName;
            }
        }

        $scope.getCityRegencyName = function(select) {
            // console.log("select", select);
            if (select == undefined) {
                $scope.draft.CityRegencyName = "All";
            } else {
                $scope.draft.CityRegencyName = select.CityRegencyName;
            }

        }

        $scope.getProvince = function(Id) {
            if (Id == null) {
                $scope.draft.ProvinceName = "All";
            }
            try {
                if (Id != undefined) {
                    FindStockFactory.getDataProvincebyArea(Id).then(
                        function(res) {
                            $scope.getDataProvince = res.data.Result;
                        }
                    );
                } else if (Id == null) {
                    $scope.filter.ProvinceId = null, $scope.filter.CityRegencyId = null;
                }

            } catch (ex) {

            }
        }

        $scope.getCityRegency = function(Id) {
            if (Id == null) {
                $scope.draft.CityRegencyName = "All";
            }
            try {
                if (Id != undefined) {
                    ComboBoxFactory.getDataKabupaten("?start=1&limit=100&filterData=ProvinceId|" + Id).then(
                        function(res) {
                            $scope.getCity = res.data.Result;
                        }
                    );
                } else if (Id == null) {
                    $scope.filter.CityRegencyId = null;
                }
            } catch (ex) {

            }

        };

        $scope.btnsuggestSwapping = function() {
            var getSuggest = "?AreaId=" + $scope.filter.AreaId + "&ProvinceId=" + $scope.filter.ProvinceId + "&CityRegencyId=" + $scope.filter.CityRegencyId + "&VehicleModelId=" + $scope.filter.VehicleModelId;
            MatchingStockDealerFactory.getData(getSuggest).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    if(res.data.Result.length == 0){
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Model ini tidak ada.",
                            type: 'warning'
                        });
                    }
                    $scope.loading = false;
                    $scope.MainFormShow = false;
                    $scope.SuggestFormShow = true;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }

        $scope.refreshListSwapping = function(){
            var getSuggest = "?AreaId=" + $scope.filter.AreaId + "&ProvinceId=" + $scope.filter.ProvinceId + "&CityRegencyId=" + $scope.filter.CityRegencyId + "&VehicleModelId=" + $scope.filter.VehicleModelId;
            MatchingStockDealerFactory.getData(getSuggest).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    if(res.data.Result.length == 0){
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Model ini tidak ada.",
                            type: 'warning'
                        });
                    }
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnRequestSwapping = function() {
            $scope.disabledBtnRequestSwapping = true;
            MatchingStockDealerFactory.create($scope.selectedRows).then(
                function(res) {
                    console.log("Yes=>", res);
                    $scope.btnkembaliSuggestSwapping();
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Request Swapping berhasil diajukan.",
                        type: 'success'
                    });
                    $scope.disabledBtnRequestSwapping = false;
                    //$scope.selectedRows = []
                },
                function(err) {
                    console.log("err=>", err);
                    bsNotify.show({
                        title: "Gagal",
                        content: "Request Swapping gagal diajukan.",
                        type: 'danger'
                    });
                    $scope.disabledBtnRequestSwapping = false;
                }
            );
        }

        $scope.btnkembaliSuggestSwapping = function() {
            
            // $scope.disabledBtnRequestSwapping = false;
            $scope.MainFormShow = true;
            $scope.SuggestFormShow = false;
            $scope.selectedRows = []
            // $scope.draft = { AreaName: null, ProvinceName: null, CityRegencyName: null };
        }


        $scope.selectedRows = []
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            $scope.selectedRows = rows;
        }

        $scope.getModelName = function (ModelSelected) {
            //console.log('Model',ModelSelected);
            $scope.SelectedModelName = '';
            $scope.SelectedModelName = ModelSelected.VehicleModelName;
            $scope.SelectedVehicleModelId = ModelSelected.VehicleModelId;
        }

        //var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.Ubah(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.$parent.LihatDetail(row.entity)">Detail</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Cabang yang meminta', field: 'OutletNameSO' },
                { name: 'Model', field: 'VehicleTypeDescription' },
                { name: 'Color', field: 'ColorName' },
                { displayName:'No SO' ,name: 'No SO', field: 'SOCode' },
                { name: 'Cabang Pemilik', field: 'OutletNameTarget' },
                { name: 'No Rangka', field: 'FrameNo' },
                { displayName:'Biaya ekspedisi TAM' ,name: 'Biaya ekspedisi TAM', field: 'TAMExpeditionCost' },
                { name: 'Lokasi unit', field: 'LastLocation' }
            ]
        };

    });