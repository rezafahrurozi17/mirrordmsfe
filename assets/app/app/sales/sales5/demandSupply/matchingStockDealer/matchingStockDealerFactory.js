angular.module('app')
  .factory('MatchingStockDealerFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getDataSO: function(param) {
        var res=$http.get('/api/sales/DemandSupplyMatchingStockDealer/SO' + param);
          return res;
      },

      getDataStock: function(param) {
        var res=$http.get('/api/sales/DemandSupplyMatchingStockDealer/Stock' + param);
          return res;
      },

      getData: function(param) {
        var res=$http.get('/api/sales/DemandSupplyMatchingStockDealer/Suggest' + param);
          return res;
      },

      getDataCity: function(param) {
          var res=$http.get('/api/sales/MLocationCityRegency');
          return res;
      },

      create: function(request) {
        return $http.post('/api/sales/DemandSupplyMMRequestSwapping', request);
      },
      
      update: function(SampleUITemplateFactory){
        return $http.put('/api/fw/Role', [{
            SalesProgramId: SampleUITemplateFactory.SalesProgramId,
            SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
      },
      
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });