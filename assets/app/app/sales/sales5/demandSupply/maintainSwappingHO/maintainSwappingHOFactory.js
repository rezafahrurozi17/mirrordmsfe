angular.module('app')
    .factory('MaintainSwappingHOFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
				var res = $http.get('/api/sales/DemandSupplyRequestSwapping/MaintainRequestSwapping?start=1&limit=10000&'+param);
                return res;
            },

            getDataNoRangka: function(param) {
                var res = $http.get('/api/sales/DemandSupplyStockView/Swapping' + param);
                return res;
            },

            getQuotation: function(param) {
                var res = $http.get('/api/sales/DemandSupplyRequestSwapping/MaintainRequestSwapping' + param);
                return res;
            },

            getType: function() {
                var res = $http.get('/api/sales/PCategoryQuotationType');
                return res;
            },

            getTermOfPayment: function() {
                var res = $http.get('/api/sales/PCategoryTermOfPayment');
                return res;
            },

            // swapping: function(Quotation) {
            //     return $http.post('/api/sales/DemandSupplyQuotation', Quotation);
            // },

            create: function(Quotation) {
                Quotation[0].SentDate = fixDate(Quotation[0].SentDate);
                return $http.post('/api/sales/DemandSupplyQuotation', Quotation);
            },

            update: function(Quotation) {
                return $http.put('/api/fw/Role', Quotation);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });