angular.module('app')
    .controller('MaintainSwappingHOController', function ($filter, bsNotify, bsAlert, $scope, $http, CurrentUser, MaintainSwappingHOFactory, MaintainSOFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MaintainRequestSwapping = true;
        $scope.StockView = false;
        $scope.BuatQuotationShow = false;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.selectedReq = [];

		$scope.dateOptionsMaintainRequestSwapping = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };


        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainRequestSwapping = null; //Model
        $scope.xRole = { selected: [] };
		$scope.filter={ startdate:null, enddate:null};
		var Da_Today_Date=new Date();
		$scope.filter.startdate=new Date(Da_Today_Date.setDate(1));
		$scope.filter.enddate=new Date();

        // $scope.urlPricing = "/api/fe/PricingEngine/?PricingId=30601&OutletId=" + $scope.user.OutletId + "&DisplayId=28030601";

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function () {
            MaintainSwappingHOFactory.getData($scope.filter).then(
                function (res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        }

        MaintainSOFactory.getOutlet().then(
            function (res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainSwappingHOFactory.getType().then(
            function (res) {
                $scope.dataType = res.data.Result;
                return res.data;
            }
        );

        MaintainSwappingHOFactory.getTermOfPayment().then(
            function (res) {
                $scope.termOfPayment = res.data.Result;
                return res.data;
            }
        );

        $scope.tempFilter = [];


        $scope.priceQuo = { UnitPrice: 0, UnitDiscount: 0 , ExpeditionSellPrice : 0, ExpeditionDiscount : 0};

        $scope.btnQuotation = function (data) {
            console.log('row', data);
            console.log('select', $scope.selectedReq);
            $scope.CreateQuo = data;
            console.log("test", $scope.CreateQuo);
            $scope.OutletNameOwner = $scope.CreateQuo.OutletNameOwner;
            $scope.OutletNameRequestor = $scope.CreateQuo.OutletNameRequestor;
            $scope.priceQuo = { UnitPrice: 0, UnitDiscount: 0 , ExpeditionSellPrice : 0, ExpeditionDiscount : 0};


            // _.extend( $scope.CreateQuo[0], price);
            $scope.jsData = {
                "Title": null,
                "HeaderInputs": {
                    "$Dekatsu$": $scope.CreateQuo.VehicleTypeDescription,
                    "$VehicleId$": $scope.CreateQuo.VehicleId,
                    "$OutletId$": $scope.user.OutletId,
                    "$VehiclePrice$": $scope.priceQuo.UnitPrice,
                    "$VehicleDisc$": $scope.priceQuo.UnitDiscount
                }
            };

            // $scope.Kalkulasi();

                var getRequestSwapping = "?ReceiveRequestSwappingId=" + $scope.CreateQuo.ReceiveRequestSwappingId + "&VehicleId=" + $scope.CreateQuo.vehicleId;
                MaintainSwappingHOFactory.getQuotation(getRequestSwapping).then(
                    function (res) {
                        $scope.dataQuotation = res.data.Result;
                        $scope.jsData = {
                            "Title": null,
                            "HeaderInputs": {
                                "$Dekatsu$": $scope.dataQuotation[0].VehicleTypeDescription,
                                "$VehicleId$": $scope.dataQuotation[0].VehicleId,
                                "$OutletId$": $scope.user.OutletId,
                                "$VehiclePrice$": $scope.priceQuo.UnitPrice,
                                "$VehicleDisc$": $scope.priceQuo.UnitDiscount
                            }
                        };
                        return res.data;
                    },
                    function(err){
                        var msg = "";
                        console.log("Cek Pesan Error", err.data.Message)
                        if (err.data.Message.split(':').length > 1) {
                            msg = err.data.Message.split(':')[1];
                            msg = msg.split('"')[1];
                        }
                        console.log("Cek Pesan Error", msg)
                        bsNotify.show({
                            title: "Warning Message",
                            content: msg,
                            type: 'warning'
                        });
                        $scope.MaintainRequestSwapping = true;
                        $scope.BuatQuotationShow = false;
                        return;
                    }
                );

                $scope.MaintainRequestSwapping = false;
                $scope.BuatQuotationShow = true;

        };
		
		$scope.MaintainRequestSwappingPaksaFilterTanggal = function () {
			if($scope.filter.enddate<$scope.filter.startdate)
			{
				$scope.filter.enddate=$scope.filter.startdate;
			}
		}

        $scope.btnkembaliQuotation = function () {
            $scope.BuatQuotationShow = false;
            $scope.MaintainRequestSwapping = true;
            $scope.dataQuotation = [];
            $scope.priceQuo = {};
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.onListSave = function (item) {
            console.log("save_modal");
        }

        $scope.onListCancel = function (item) {
            console.log("cancel_modal");
        }


        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function (rows) {
            $scope.selectedReq = rows;
            console.log("onSelectRows=>", $scope.selectedReq);
        }

        var btnDetail = '<i title="Detail" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.btnQuotation(row.entity)" ></i>'        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Nomor Quotation', field: 'QuotationNo', visible: true, width: '12%'},
                { name: 'Tanggal Permintaan Swapping', field: 'QuotationDate', visible: true, width: '12%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Cabang Pemilik', field: 'OutletNameOwner', width: '18%' },
                { name: 'Cabang Tujuan', field: 'OutletNameRequestor', width: '18%' },
                { name: 'Model Tipe', field: 'VehicleModelName', width: '20%' },
                { name: 'Tipe', field: 'VehicleTypeDescription', width: '20%' },
                { name: 'Warna', field: 'ColorName', width: '15%' },
                { displayName:'No Rangka/RRN' ,name: 'No Rangka/RRN', field: 'FrameNo', width: '15%' },
                { name: 'Status', field: 'QuotationStatusName', width: '15%' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    visible: true,
                    width: '10%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    pinnedRight: true,
                    enableColumnResizing: true,
                    cellTemplate: btnDetail
                }
            ]
        };

        $scope.gridrincianHarga = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName' },
                { name: 'Keterangan', field: 'Description' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };

        $scope.filterData = {
            defaultFilter: [
                { name: 'No Rangka', value: 'FrameNo' },
                { name: 'Aging', value: 'Aging' },
                { name: 'Lokasi Unit', value: 'LastLocation' },
                { name: 'Status Unit', value: 'StatusStockName' }

            ]
        };

        $scope.filterCustom = function () {
            // if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '') ||
                // ($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == '')) {
                // $scope.loading = false;
                // bsNotify.show({
                    // type: 'warning',
                    // title: "Peringatan",
                    // content: "Harap pilih kategori dan masukkan filter"
                // });
            // } else {
                // $scope.param = $scope.selectedFilter;
                // var inputfilter = $scope.textFilter;
                // var tempGrid = angular.copy($scope.tempFilter);
                // var objct = '{"' + $scope.param + '":"' + inputfilter + '"}'
                // $scope.gridStockView.data = $filter('filter')(tempGrid, JSON.parse(objct));
            // }
			
			$scope.param = $scope.selectedFilter;
            var inputfilter = $scope.textFilter;
            var tempGrid = angular.copy($scope.tempFilter);
            var objct = '{"' + $scope.param + '":"' + inputfilter + '"}';
           
			
            // $scope.gridStockView.data = $scope.tempFilter.filter(function(data) {
            //     //console.log(data, )
            //     switch($scope.param){
            //         case "FrameNo":  
            //         return (data.FrameNo == inputfilter)
            //         break;
            //         case "LastLocation":  
            //         return (data.LastLocation == inputfilter)
            //         break;
            //         case "StatusStockName":  
            //         return (data.StatusStockName == inputfilter)
            //         break;
            //         default:
            //         break;
            //     }
            // });
        }

        $scope.refreshFilter = function () {
            $scope.textFilter = '';
            $scope.selectedFilter = {};
        }
    });