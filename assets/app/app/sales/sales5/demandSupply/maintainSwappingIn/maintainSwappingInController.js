angular.module('app')
    .controller('MaintainSwappingInController', function ($filter, bsNotify, bsAlert, $scope, $http, CurrentUser, maintainSwappingInFactory, MaintainSOFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MaintainSwappingInOut = true;
        $scope.SwappingAdd = false;
        $scope.SwappingView = false;
        $scope.BuatQuotationShow = false;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.selectedReq = [];
        $scope.OutletNameOwner;
        $scope.disabledSamaBeda = false;

		$scope.dateOptionsMaintainSwappingIn = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };


        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getListDetail();
            //$scope.getComboBox ();
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainSwappingInOut = null; //Model
        $scope.xRole = { selected: [] };
		$scope.filter={ TanggalAwal:null, TanggalAkhir:null};
		var Da_Today_Date=new Date();
		$scope.filter.TanggalAwal=new Date(Da_Today_Date.setDate(1));
		$scope.filter.TanggalAkhir=new Date();
        $scope.filter.tanggalpermintaan=new Date();
        console.log('Cek Tanggal Awal', $scope.filter.TanggalAwal)
        console.log('Cek Tanggal Akhir', $scope.filter.TanggalAkhir)
        //console.log('Cek Tanggal Permintaan', $scope.filter.tanggalpermintaan)

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];

        maintainSwappingInFactory.getData($scope.filter).then(
            function (res) {
                $scope.grid.data = res.data.Result;
                $scope.loading = false;
                if($scope.user.RoleName == 'KACAB'){
                    $scope.disabledSwappingIn = true;
                    $scope.disabledSwappingOut = true;
                } else {
                    $scope.disabledSwappingIn = false;
                    $scope.disabledSwappingOut = false;
                }
                return res.data.Result;
            },
            function (err) {
                console.log("err=>", err);
            }
        );
        
        $scope.input = {};
        $scope.getData = function () {
            maintainSwappingInFactory.getData($scope.filter).then(
                function (res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.getListDetail = function(SwappInId) {
            maintainSwappingInFactory.getListDetail().then(
                function (res) 
                {
                    $scope.gridSwappingView.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;  
                }
            );
        }

        $scope.getListDetailOut = function(SwappInId) {
            maintainSwappingInFactory.getListDetailOut().then(
                function (res) 
                {
                    $scope.gridSwappingView.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;  
                }
            );
        }

        $scope.SwappingIn_OptionsSamaBeda = [ //Memasukkan nilai dari opsi checkbox
            {
                id: 1,
                name: "Sama",
            },
            {
                id: 2,
                name: "Beda",
            }
        ]

        $scope.SwappingIn_OptionsLocation = [
            {
                id: 1,
                name: "PDS"
            },
            {
                id: 2,
                name: "PDC"
            }
        ]

        $scope.SwappingIn_SwappType_Change = function(){

            maintainSwappingInFactory.getDealerSwapping($scope.SwappingIn_SwappType)
            .then(
               function(res){
                $scope.SwappingIn_OptionsBranch = res.data.Result;
                console.log('Res =', $scope.SwappingIn_OptionsBranch)
              }
            );
        }

        //-- Button Tambah di Header Sebelum Revisi UI
        // $scope.btnTambahHeader = function() {
        //     $scope.SwappingAdd = true;
        //     $scope.SwappingView = false;
        //     $scope.MaintainSwappingInOut = false;
        //     $scope.isDate = true;
        //     $scope.gridSwappingAdd.data = [];
        //     $scope.SwappingIn_SwappType = [];
        //     $scope.SwappingIn_SwappType = "";
        //     $scope.disabledSamaBeda = false;
        // }

        $scope.locationUnit = function(){
            $scope.disabledTambahTemp = false;
        }
        $scope.disabledTambahTemp = false;
        $scope.btnSwappingIn = function(){
            $scope.disabledOKSimpan = false;
            $scope.disabledOKSimpanOut = false;
            $scope.disabledTambahTemp = false;
            $scope.SwappCode = "Swapping In";
            $scope.Show_Location = false;
            $scope.Show_CabangPemilik = true;
            $scope.Show_CabangTujuan = false;
            $scope.SwappingAdd = true;
            $scope.SwappingView = false;
            $scope.MaintainSwappingInOut = false;
            $scope.isDate = true;
            $scope.gridSwappingAdd.data = [];
            $scope.SwappingIn_SwappType = [];
            $scope.SwappingIn_SwappType = "";
            $scope.SwappingIn_Branch = "";
            $scope.disabledSamaBeda = false;
            $scope.gridSwappingAdd.columnDefs[5].visible = true;
            $scope.gridSwappingAdd.columnDefs[6].visible = false;
            $scope.gridSwappingAdd.columnDefs[7].visible = false;
        }

        $scope.btnSwappingOut = function(){
            $scope.disabledOKSimpan = false;
            $scope.disabledOKSimpanOut = false;
            $scope.disabledTambahTemp = true;
            $scope.SwappCode = "Swapping Out";
            $scope.Show_Location = true;
            $scope.Show_CabangPemilik = false;
            $scope.Show_CabangTujuan = true;
            $scope.SwappingAdd = true;
            $scope.SwappingView = false;
            $scope.MaintainSwappingInOut = false;
            $scope.isDate = true;
            $scope.gridSwappingAdd.data = [];
            $scope.SwappingIn_SwappType = [];
            $scope.SwappingIn_SwappType = "";
            $scope.SwappingIn_Branch = "";
            $scope.disabledSamaBeda = false;
            $scope.gridSwappingAdd.columnDefs[5].visible = false;
            $scope.gridSwappingAdd.columnDefs[6].visible = true;
            $scope.gridSwappingAdd.columnDefs[7].visible = true;   
        }
            //$scope.input.SwappingIn_tanggalpermintaan = $scope.filter.SwappingIn_tanggalpermintaan;

            $scope.tempFilter = [];
       

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + date.getDate()).slice(-2)
                return fix;
            } else {
                return null;
            }
        };

        $scope.btnTambahTemp_Clear = function(){
            $scope.SwappingIn_FrameNo = "";
            $scope.SwappingIn_StandarPrice = "";
            $scope.SwappingIn_Branch = "";
            $scope.SwappingIn_Location = "";
            $scope.disabledSamaBeda = true;
        }

        $scope.OKSukses = false;
        $scope.BatalKembali = false;
        $scope.OKBatal = true;
        $scope.OKOutSukses = false;
        $scope.BatalOutKembali = false;
        $scope.OKOutBatal = true;

        $scope.NotifOK_Button_Clicked = function(){
            if($scope.gridSwappingAdd.data.length == 0){
                $scope.gridSwappingAdd.data.push({
                    tanggalpermintaan: $scope.input.tanggalpermintaan,
                    FrameNo: $scope.input.FrameNo,
                    StandarPrice: $scope.input.StandarPrice,
                    SwappTypeName: $scope.input.SwappTypeName,
                    DealerName: $scope.input.DealerId,
                    OutletOwnerId: $scope.SwappingIn_Branch,
                });
                $scope.btnTambahTemp_Clear();
            }else{
                var banding = 0;
                for(var i in $scope.gridSwappingAdd.data){
                    if ($scope.input.FrameNo == $scope.gridSwappingAdd.data[i].FrameNo){
                        banding = 1;
                    }
                }
                if(banding == 0){
                    $scope.gridSwappingAdd.data.push({
                        tanggalpermintaan: $scope.input.tanggalpermintaan,
                        FrameNo: $scope.input.FrameNo,
                        StandarPrice: $scope.input.StandarPrice,
                        SwappTypeName: $scope.input.SwappTypeName,
                        DealerName: $scope.input.DealerId,
                        OutletOwnerId: $scope.SwappingIn_Branch
                    });
                    $scope.btnTambahTemp_Clear();
                } else {
                     bsNotify.show({
                        title: 'Peringatan',
                        content: 'Nomor Rangka sudah ditambahkan',
                        type: 'warning'
                    });
                }
            }
        }

        $scope.NotifOKOut_Button_Clicked = function() {
            if($scope.gridSwappingAdd.data.length == 0){
                $scope.gridSwappingAdd.data.push({
                    tanggalpermintaan: $scope.input.tanggalpermintaan,
                    FrameNo: $scope.input.FrameNo,
                    StandarPrice: $scope.input.StandarPrice,
                    SwappTypeName: $scope.input.SwappTypeName,
                    DealerName: $scope.input.DealerId,
                    OutletRequestorId: $scope.SwappingIn_Branch,
                    LocationTypeName: $scope.input.LocationTypeName
                });
                $scope.btnTambahTemp_Clear();
            } else {
                var banding = 0;
                for(var i in $scope.gridSwappingAdd.data){
                    if ($scope.input.FrameNo == $scope.gridSwappingAdd.data[i].FrameNo){
                        banding = 1;
                    }
                }
                if(banding == 0){
                    $scope.gridSwappingAdd.data.push({
                        tanggalpermintaan: $scope.input.tanggalpermintaan,
                        FrameNo: $scope.input.FrameNo,
                        StandarPrice: $scope.input.StandarPrice,
                        SwappTypeName: $scope.input.SwappTypeName,
                        DealerName: $scope.input.DealerId,
                        OutletRequestorId: $scope.SwappingIn_Branch,
                        LocationTypeName: $scope.input.LocationTypeName
                    });
                    $scope.btnTambahTemp_Clear();
                } else {
                    bsNotify.show({
                        title: 'Peringatan',
                        content: 'Nomor Rangka sudah ditambahkan',
                        type: 'warning'
                    });                                       
                }
            } 
        }

        $scope.btnTambahTemp = function() {
            $scope.input = {}
            $scope.OKSukses = false;
            $scope.BatalKembali = false;
            $scope.OKBatal = true;
            $scope.OKOutSukses = false;
            $scope.BatalOutKembali = false;
            $scope.OKOutBatal = true;
            $scope.input.tanggalpermintaan = $scope.filter.tanggalpermintaan;
            $scope.input.FrameNo = $scope.SwappingIn_FrameNo;
            $scope.input.StandarPrice = $scope.SwappingIn_StandarPrice;
            if($scope.SwappingIn_SwappType == 1){
                $scope.input.SwappTypeName = "Sama"
            } else {
                $scope.input.SwappTypeName = "Beda"
            }
            $scope.input.DealerId = $scope.SwappingIn_Branch;
            if($scope.SwappingIn_Location == 1){
                $scope.input.LocationTypeName = "PDS"
            } else {
                $scope.input.LocationTypeName = "PDC"
            }
            $scope.input.tanggalpermintaan = fixDate($scope.input.tanggalpermintaan);
            for (var i in $scope.SwappingIn_OptionsBranch) {
                if($scope.SwappingIn_OptionsBranch[i].DealerId == $scope.SwappingIn_Branch) {
                    $scope.input.DealerId  = $scope.SwappingIn_OptionsBranch[i].DealerName;
                    $scope.DealerCode = $scope.SwappingIn_OptionsBranch[i].DealerCode;                  
                }
                console.log('Tes Dealer =', $scope.SwappingIn_OptionsBranch[i].DealerId)
            }
            if($scope.SwappCode == "Swapping In"){
                maintainSwappingInFactory.getNotifNoka($scope.SwappingIn_FrameNo, $scope.DealerCode)
                .then(
                   function(res){
                    $scope.cekNo = res.data.Result;
                    $scope.cekIdNoka = res.data.Result[0].CheckId;
                    $scope.notifnoka = res.data.Result[0].CheckMessage; 
                    console.log('Res =', $scope.cekNo)
                    if($scope.cekIdNoka == 1){
                    angular.element('#SwappingModalNotif').modal('show');
                    } else {
                        if($scope.SwappCode == "Swapping In"){
                            if($scope.cekIdNoka == 0 && $scope.notifnoka != 'Lolos'){
                                $scope.OKSukses = true;
                                $scope.BatalKembali = true;
                                $scope.OKBatal = false;
                                angular.element('#SwappingModalNotif').modal('show');
                            } 
                            else {
                                if($scope.gridSwappingAdd.data.length == 0){
                                    $scope.gridSwappingAdd.data.push({
                                        tanggalpermintaan: $scope.input.tanggalpermintaan,
                                        FrameNo: $scope.input.FrameNo,
                                        StandarPrice: $scope.input.StandarPrice,
                                        SwappTypeName: $scope.input.SwappTypeName,
                                        DealerName: $scope.input.DealerId,
                                        OutletOwnerId: $scope.SwappingIn_Branch,
                                    });
                                    $scope.btnTambahTemp_Clear();
                                }else{
                                    var banding = 0;
                                    for(var i in $scope.gridSwappingAdd.data){
                                        if ($scope.input.FrameNo == $scope.gridSwappingAdd.data[i].FrameNo){
                                            banding = 1;
                                        }
                                    }
                                    if(banding == 0){
                                        $scope.gridSwappingAdd.data.push({
                                            tanggalpermintaan: $scope.input.tanggalpermintaan,
                                            FrameNo: $scope.input.FrameNo,
                                            StandarPrice: $scope.input.StandarPrice,
                                            SwappTypeName: $scope.input.SwappTypeName,
                                            DealerName: $scope.input.DealerId,
                                            OutletOwnerId: $scope.SwappingIn_Branch
                                        });    
                                        $scope.btnTambahTemp_Clear();              
                                    } else {
                                        bsNotify.show({
                                            title: 'Peringatan',
                                            content: 'Nomor Rangka sudah ditambahkan',
                                            type: 'warning'
                                        });
                                    }
                                }  
                            }                         
                        }                  
                    }
                });

            } else { //di sini Swapping Out
                maintainSwappingInFactory.getNotifNokaOut($scope.SwappingIn_FrameNo, $scope.DealerCode, $scope.SwappingIn_Location)
                .then(
                   function(res){
                    $scope.cekNoOut = res.data.Result;
                    $scope.cekIdNokaOut = res.data.Result[0].CheckId;
                    $scope.notifoutnoka = res.data.Result[0].CheckMessage; 
                    console.log('ResOut =', $scope.cekNoOut)
                    if($scope.cekIdNokaOut == 1){
                    angular.element('#SwappingModalNotifOut').modal('show');
                    } else {
                        if($scope.SwappCode == "Swapping Out"){
                            if($scope.cekIdNokaOut == 0 && $scope.notifoutnoka != 'Lolos'){
                                $scope.OKOutSukses = true;
                                $scope.BatalOutKembali = true;
                                $scope.OKOutBatal = false;
                                angular.element('#SwappingModalNotifOut').modal('show');
                            } 
                            else {
                                if($scope.gridSwappingAdd.data.length == 0){
                                    $scope.gridSwappingAdd.data.push({
                                        tanggalpermintaan: $scope.input.tanggalpermintaan,
                                        FrameNo: $scope.input.FrameNo,
                                        StandarPrice: $scope.input.StandarPrice,
                                        SwappTypeName: $scope.input.SwappTypeName,
                                        DealerName: $scope.input.DealerId,
                                        OutletRequestorId: $scope.SwappingIn_Branch,
                                        LocationTypeName: $scope.input.LocationTypeName
                                    });
                                    $scope.btnTambahTemp_Clear();
                                } else {
                                    var banding = 0;
                                    for(var i in $scope.gridSwappingAdd.data){
                                        if ($scope.input.FrameNo == $scope.gridSwappingAdd.data[i].FrameNo){
                                            banding = 1;
                                        }
                                    }
                                    if(banding == 0){
                                        $scope.gridSwappingAdd.data.push({
                                            tanggalpermintaan: $scope.input.tanggalpermintaan,
                                            FrameNo: $scope.input.FrameNo,
                                            StandarPrice: $scope.input.StandarPrice,
                                            SwappTypeName: $scope.input.SwappTypeName,
                                            DealerName: $scope.input.DealerId,
                                            OutletRequestorId: $scope.SwappingIn_Branch,
                                            LocationTypeName: $scope.input.LocationTypeName
                                        });
                                        $scope.btnTambahTemp_Clear();
                                    } else {
                                        bsNotify.show({
                                            title: 'Peringatan',
                                            content: 'Nomor Rangka sudah ditambahkan',
                                            type: 'warning'
                                        });                                       
                                    }
                                } 
                            }                         
                        }                    
                    }
                });
                //--
                // if($scope.gridSwappingAdd.data.length == 0){
                //     $scope.gridSwappingAdd.data.push({
                //         tanggalpermintaan: $scope.input.tanggalpermintaan,
                //         FrameNo: $scope.input.FrameNo,
                //         StandarPrice: $scope.input.StandarPrice,
                //         SwappTypeName: $scope.input.SwappTypeName,
                //         DealerName: $scope.input.DealerId,
                //         OutletRequestorId: $scope.SwappingIn_Branch,
                //         LocationTypeName: $scope.input.LocationTypeName
                //     });
                //     $scope.btnTambahTemp_Clear();
                // } else {
                //     var banding = 0;
                //     for(var i in $scope.gridSwappingAdd.data){
                //         if ($scope.input.FrameNo == $scope.gridSwappingAdd.data[i].FrameNo){
                //             banding = 1;
                //         }
                //     }
                //     if(banding == 0){
                //         $scope.gridSwappingAdd.data.push({
                //             tanggalpermintaan: $scope.input.tanggalpermintaan,
                //             FrameNo: $scope.input.FrameNo,
                //             StandarPrice: $scope.input.StandarPrice,
                //             SwappTypeName: $scope.input.SwappTypeName,
                //             DealerName: $scope.input.DealerId,
                //             OutletRequestorId: $scope.SwappingIn_Branch,
                //             LocationTypeName: $scope.input.LocationTypeName
                //         });
                //         $scope.btnTambahTemp_Clear();
                //     } else {
                //         bsNotify.show({
                //             title: 'Peringatan',
                //             content: 'Nomor Rangka sudah ditambahkan',
                //             type: 'warning'
                //         });
                        
                //     }
                // } 
                //-- 
            }
        }

        $scope.NotifOKSimpan_Button_Clicked = function(){
            $scope.disabledOKSimpan = true;
               maintainSwappingInFactory.create($scope.dataSimpan)
                .then(
                   function(res){
                      bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil ditambahkan.",
                        type: 'success'
                      });
                      $scope.disabledSamaBeda = false;
                      $scope.gridSwappingAdd.data = {}
                      $scope.getData();

                  },
                  function(err){
                      var errMsg = "";
                      if(err.data.Message.split('#').length > 1){
                          errMsg = err.data.Message.split('#')[1];
                      }
                        bsNotify.show({
                            title: 'Gagal',
                            content: errMsg,
                            type: 'danger'
                        });
                        return true;
                    }
                );
                $scope.SwappingIn_FrameNo = "";
                $scope.SwappingIn_StandarPrice = "";
                $scope.SwappingIn_Branch = "";
                //$scope.SwappingIn_Location = "";
                $scope.MaintainSwappingInOut = true;
                $scope.SwappingAdd = false;
        }

        $scope.NotifOKSimpanOut_Button_Clicked = function(){
            $scope.disabledOKSimpanOut = true;
                maintainSwappingInFactory.createOut($scope.dataSimpan)
                .then(
                function(res){
                    bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil ditambahkan.",
                        type: 'success'
                    });
                    $scope.disabledSamaBeda = false;
                    $scope.gridSwappingAdd.data = {}
                    $scope.getData();

                },
                function(err){
                    var errMsg = "";
                    if(err.data.Message.split('#').length > 1){
                        errMsg = err.data.Message.split('#')[1];
                    }
                        bsNotify.show({
                            title: 'Gagal',
                            content: errMsg,
                            type: 'danger'
                        });
                        return true;
                    }
                );
                $scope.SwappingIn_FrameNo = "";
                $scope.SwappingIn_StandarPrice = "";
                $scope.SwappingIn_Branch = "";
                $scope.SwappingIn_Location = "";
                $scope.MaintainSwappingInOut = true;
                $scope.SwappingAdd = false;
        }

        $scope.btnSimpanTambah = function() {
            $scope.disabledOKSimpan = false;
            $scope.disabledOKSimpanOut = false;
            $scope.dataSimpan = [];
            //$scope.dataSimpan[0].data = {};

            $scope.dataSimpan.push({
                tanggalpermintaan: $scope.input.tanggalpermintaan,
                ListFrameNo: $scope.gridSwappingAdd.data
            });

            if($scope.SwappCode == "Swapping In"){
                angular.element('#SwappingModalSimpan').modal('show');
                $scope.SwappingIn_FrameNo = "";
                $scope.SwappingIn_StandarPrice = "";
                $scope.SwappingIn_Branch = "";
                $scope.SwappingIn_Location = "";
                $scope.MaintainSwappingInOut = false;
                $scope.SwappingAdd = true;
            } else {
                angular.element('#SwappingModalSimpanOut').modal('show');

                // maintainSwappingInFactory.createOut($scope.dataSimpan)
                // .then(
                //    function(res){
                //       bsNotify.show({
                //         title: 'Berhasil',
                //         content: "Data berhasil ditambahkan.",
                //         type: 'success'
                //       });
                //       $scope.disabledSamaBeda = false;
                //       $scope.gridSwappingAdd.data = {}
                //       $scope.getData();
    
                //   },
                //   function(err){
                //       var errMsg = "";
                //       if(err.data.Message.split('#').length > 1){
                //           errMsg = err.data.Message.split('#')[1];
                //       }
                //         bsNotify.show({
                //             title: 'Gagal',
                //             content: errMsg,
                //             type: 'danger'
                //         });
                //         return true;
                //     }
                // );
                $scope.SwappingIn_FrameNo = "";
                $scope.SwappingIn_StandarPrice = "";
                $scope.SwappingIn_Branch = "";
                $scope.SwappingIn_Location = "";
                $scope.MaintainSwappingInOut = false;
                $scope.SwappingAdd = true;
            }            
        }

        $scope.NotifBatalSimpan_Button_Clicked = function(){
            angular.element('#SwappingModalSimpan').modal('hide');
        }

        $scope.onlyNumber = function(id){
            if(id == 1)
            {
                $scope.SwappingIn_FrameNo = $scope.SwappingIn_FrameNo.replace(/[^A-Z0-9]/g, "");
            } else if (id == 2)
            {
                $scope.SwappingIn_StandarPrice = $scope.SwappingIn_StandarPrice.replace(/[^\d]/g, "");
            }
        } 

        $scope.btnKembaliTambah = function() {
            $scope.MaintainSwappingInOut = true;
            $scope.SwappingAdd = false;
            $scope.SwappingIn_FrameNo = "";
            $scope.SwappingIn_StandarPrice = "";
            $scope.SwappingIn_Branch = "";
            $scope.SwappingIn_Location = "";
        }

        $scope.btnKembaliDetail = function() {
            $scope.MaintainSwappingInOut = true;
            $scope.SwappingAdd = false;
            $scope.SwappingView = false;

            console.log('Tes',  $scope.MaintainSwappingInOut)
            console.log('Tes2',  $scope.SwappingAdd)
        }

        $scope.BatalSwap = function(row){
            //$scope.AlasanBatalSimpan_Button_Clicked = "";
            $scope.SwappInId = row.entity.SwappInId;
            $scope.SwappFlag = row.entity.SwappFlag;
            angular.element('#SwappingInModalBatal').modal('show');
            
        }

        $scope.AlasanBatalKembali_Button_Clicked = function(){
            angular.element('#SwappingInModalBatal').modal('hide');
        }

        $scope.AlasanBatalSimpan_Button_Clicked = function(){
    
            if($scope.SwappingIn_AlasanBatal == "" || $scope.SwappingIn_AlasanBatal == null || $scope.SwappingIn_AlasanBatal == undefined){
                bsNotify.show({
                    title: 'Peringatan',
                    content: 'Alasan Batal harus diisi',
                    type: 'warning'
                  });
                  return false;
            }
            if($scope.SwappFlag == "Swapping In"){
            var data;
            data = [{
                "SwappInId":  $scope.SwappInId,
                "Tipe": "Batal",
                "Reason": $scope.SwappingIn_AlasanBatal  
            }];

            maintainSwappingInFactory.getAlasanBatal(data)
                .then(
                   function(res){
                      bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil dibatalkan.",
                        type: 'success'
                    });
                    angular.element('#SwappingInModalBatal').modal('hide');
                    $scope.SwappingIn_AlasanBatal = "";
                    $scope.getData();

                })
            } else {
            var data;
            data = [{
                "SwappOutId":  $scope.SwappInId,
                "Tipe": "Batal",
                "Reason": $scope.SwappingIn_AlasanBatal  
            }];

            maintainSwappingInFactory.getAlasanBatalOut(data)
                .then(
                   function(res){
                      bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil dibatalkan.",
                        type: 'success'
                    });
                    angular.element('#SwappingInModalBatal').modal('hide');
                    $scope.SwappingIn_AlasanBatal = "";
                    $scope.getData();

                })
            }
        }

        $scope.btnTolak = function(){
            if($scope.SwappFlag == "Swapping In"){
            var data;
            data = [{
                "SwappInId":  $scope.SwappInId,
                "Tipe": "Tolak",
                "Reason": "" 
            }];

            maintainSwappingInFactory.getAlasanBatal(data)
                .then(
                   function(res){
                      bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil ditolak.",
                        type: 'success'
                    });
                    $scope.btnKembaliDetail();
                    $scope.getData();
                },
                function(err){
                    bsNotify.show({
                        title: 'Gagal',
                        content: err.data.Message,
                        type: 'danger'
                    });
                })
            } else {
            var data;
            data = [{
                "SwappOutId":  $scope.SwappInId,
                "Tipe": "Tolak",
                "Reason": "" 
            }];

            maintainSwappingInFactory.getAlasanBatalOut(data)
                .then(
                   function(res){
                      bsNotify.show({
                        title: 'Berhasil',
                        content: "Data berhasil ditolak.",
                        type: 'success'
                    });
                    $scope.btnKembaliDetail();
                    $scope.getData();
                },
                function(err){
                    bsNotify.show({
                        title: 'Gagal',
                        content: err.data.Message,
                        type: 'danger'
                    });
                })
            }
        }

        $scope.btnSetuju = function(){
            if($scope.SwappFlag == "Swapping In"){
            var dataApproval;
            dataApproval = [{
                "SwappTypeId" : $scope.filter.Tipepermintaanid,
                "SwappInId" : $scope.SwappInId
            }]
            maintainSwappingInFactory.getApproveDetail(dataApproval)
            .then(
               function(res){
                  bsNotify.show({
                    title: 'Berhasil',
                    content: "Data berhasil disetujui.",
                    type: 'success'
                });
                $scope.btnKembaliDetail();
                $scope.getData();
                $scope.disabledSetuju = true;
            },
            function(err){
                bsNotify.show({
                    title: 'Gagal',
                    content: err.data.Message,
                    type: 'danger'
                });
            })
            } else {
            var dataApproval;
            dataApproval = [{
                "SwappTypeId" : $scope.filter.Tipepermintaanid,
                "SwappOutId" : $scope.SwappInId
            }]
            maintainSwappingInFactory.getApproveDetailOut(dataApproval)
            .then(
                function(res){
                    bsNotify.show({
                    title: 'Berhasil',
                    content: "Data berhasil disetujui.",
                    type: 'success'
                });
                $scope.btnKembaliDetail();
                $scope.getData();
                $scope.disabledSetuju = true;
            },
            function(err){
                bsNotify.show({
                    title: 'Gagal',
                    content: err.data.Message,
                    type: 'danger'
                });
            })
            }
        }
		
		$scope.MaintainSwappingInFilterTanggal = function () {
			if($scope.filter.TanggalAkhir<$scope.filter.TanggalAwal)
			{
				$scope.filter.TanggalAkhir=$scope.filter.TanggalAwal;
			}
		}

        
        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.onListSave = function (item) {
            console.log("save_modal");
        }

        $scope.onListCancel = function (item) {
            console.log("cancel_modal");
        }


        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function (rows) {
            $scope.selectedReq = rows;
            console.log("onSelectRows=>", $scope.selectedReq);
        }
       
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            
            columnDefs: [
                { name: 'Nomor Permintaan', field: 'SwappInCode'},
                { name: 'Tanggal Permintaan', field: 'TanggalPermintaan', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Tipe Swapping', field: 'SwappFlag'},
                { name: 'Status', field: 'SwappStatusName'},
                {
                    name: "Action", enableColumnMenu: false, enableSorting: false, visible: true,
                    cellTemplate: '<i class="fa fa-fw fa-lg fa-list-alt" uib-tooltip="Lihat" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.DetailSwap(row)"></i><i class="fa fa-fw fa-lg fa-times" uib-tooltip="Batal" tooltip-placement="right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-show="row.entity.SwappStatusId == 1 && grid.appScope.$parent.user.RoleName == \'Admin Unit\'" ng-click="grid.appScope.$parent.BatalSwap(row)"></i>'
                }
               
            ]
        };

        $scope.DetailSwap = function(row) {
            $scope.SwappInId = row.entity.SwappInId;
            $scope.SwappFlag = row.entity.SwappFlag;
            console.log('Detail', row);
            if($scope.SwappFlag == "Swapping In"){
            maintainSwappingInFactory.getListDetail($scope.SwappInId).then(
                function (res) 
                {
                    $scope.gridSwappingView.data = res.data.Result;
                    $scope.gridSwappingView.columnDefs[1].visible = true;
                    $scope.gridSwappingView.columnDefs[2].visible = false;
                    $scope.loading = false;
                    console.log('Cek', $scope.gridSwappingView.data)
                    console.log('Cek FrameNo', res.data.FrameNo)
                    $scope.tempStatus = res.data.Result[0].Status;
                    $scope.SwappingIn_Nomorpermintaan = res.data.Result[0].Nomorpermintaan;
                    $scope.filter.Tanggalpermintaan = res.data.Result[0].Tanggalpermintaan;
                    //$scope.Tipepermintaanname = res.data.Result[0].Tipepermintaanname;
                    $scope.filter.Tipepermintaanid = res.data.Result[0].Tipepermintaanid;
                    //console.log('Tipe',  $scope.Tipepermintaanname)
                    console.log('Tipe Filter',  $scope.filter.Tipepermintaanid)
                    if($scope.tempStatus != 'Diajukan'){
                        $scope.disabledTolak = true;
                        $scope.disabledSetuju = true;
                    } else {
                        $scope.disabledTolak = false;
                        $scope.disabledSetuju = false;
                    }
                    return res.data.Result;

                }
            );
        } else {
            maintainSwappingInFactory.getListDetailOut($scope.SwappInId).then(
                function (res) 
                {
                    $scope.gridSwappingView.data = res.data.Result;
                    $scope.gridSwappingView.columnDefs[1].visible = false;
                    $scope.gridSwappingView.columnDefs[2].visible = true;
                    $scope.loading = false;
                    console.log('Cek', $scope.gridSwappingView.data)
                    console.log('Cek FrameNo', res.data.FrameNo)
                    $scope.tempStatus = res.data.Result[0].Status;
                    $scope.SwappingIn_Nomorpermintaan = res.data.Result[0].Nomorpermintaan;
                    $scope.filter.Tanggalpermintaan = res.data.Result[0].Tanggalpermintaan;
                    //$scope.Tipepermintaanname = res.data.Result[0].Tipepermintaanname;
                    $scope.filter.Tipepermintaanid = res.data.Result[0].Tipepermintaanid;
                    //console.log('Tipe',  $scope.Tipepermintaanname)
                    console.log('Tipe Filter',  $scope.filter.Tipepermintaanid)
                    if($scope.tempStatus != 'Diajukan'){
                        $scope.disabledTolak = true;
                        $scope.disabledSetuju = true;
                    } else {
                        $scope.disabledTolak = false;
                        $scope.disabledSetuju = false;
                    }
                    return res.data.Result;

                }
            );
        }
            if($scope.user.RoleName == 'KACAB'){
                $scope.Show_Btn_SetujuTolak = true;
            } else {
                $scope.Show_Btn_SetujuTolak = false;
            }
           
            $scope.SwappingView = true;
            $scope.SwappingAdd = false;
            $scope.MaintainSwappingInOut = false;
            $scope.disabledSamaBeda = true;
            $scope.isDate = true;
            
        }

        $scope.gridSwappingAdd = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'Tanggal Permintaan', visible: false, field: 'tanggalpermintaan' },
                { name: 'Nomor Rangka', field: 'FrameNo' },
                { name: 'Harga (include PPN)', field: 'StandarPrice' },
                { name: 'Sama PT/Beda PT', field: 'SwappTypeName' },
                { name: 'Cabang Pemilik ID', field: 'DealerId', visible: false },
                { name: 'Cabang Pemilik', field: 'DealerName'},
                { name: 'Cabang Tujuan', field: 'DealerName'},
                { name: 'Lokasi Unit', field: 'LocationTypeName'},

            ]
        };

        $scope.gridSwappingAdd.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridSwappingView = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'ID Detail', width: '15%', visible: false, field: 'SwappInId' },
                { name: 'Cabang Pemilik', width: '15%', field: 'Name' },
                { name: 'Cabang Tujuan', width: '15%', field: 'Name' },
                { name: 'Model', width: '15%', field: 'VehicleModelName' },
                { name: 'Tipe', width: '15%', field: 'Tipe' },
                { name: 'Warna', width: '15%', field: 'ColorName' },
                { name: 'Nomor Rangka', width: '15%', field: 'FrameNo' },
                { name: 'Tahun Kendaraan', width: '15%', field: 'AssemblyYear' },
                { name: 'Lokasi', width: '15%', field: 'Lokasi' },
                { name: 'Harga (include PPN)', width: '15%', field: 'Price' },

            ]
        };

        $scope.gridSwappingView.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
        };

        $scope.filterData = {
            defaultFilter: [
                { name: 'No Rangka', value: 'FrameNo' },
                { name: 'Aging', value: 'Aging' },
                { name: 'Lokasi Unit', value: 'LastLocation' },
                { name: 'Status Unit', value: 'StatusStockName' }

            ]
        };

        $scope.filterCustom = function () {
			
			$scope.param = $scope.selectedFilter;
            var inputfilter = $scope.textFilter;
            var tempGrid = angular.copy($scope.tempFilter);
            var objct = '{"' + $scope.param + '":"' + inputfilter + '"}';
            
			if (inputfilter == "" || inputfilter == null) 
			{
				$scope.gridSwappingAdd.data = tempGrid;
			}
			else 
			{
				$scope.gridSwappingAdd.data = $filter('filter')(tempGrid, JSON.parse(objct));
			}
        }

        $scope.refreshFilter = function () {
            $scope.textFilter = '';
            $scope.selectedFilter = {};
            $scope.gridSwappingAdd.data = $scope.tempFilter;
            $scope.gridSwappingView.data = $scope.tempFilter;

        }

        $scope.closeModal = function(){
            angular.element('.ui.modal.InfoCabang').modal('hide');
            angular.element('.ui.modal.InfoHO').modal('hide');
        }
    });