angular.module('app')
    .factory('maintainSwappingInFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() +
                    ('0' + (date.getMonth() + 1)).slice(-2) +
                    ('0' + date.getDate()).slice(-2);
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                // filter.TanggalAwal = fixDate(filter.TanggalAwal);
                // filter.TanggalAkhir = fixDate(filter.TanggalAkhir);
                // filter.tanggalpermintaan = fixDate(filter.tanggalpermintaan);
                var param = $httpParamSerializer(filter);
				var res = $http.get('/api/sales/DemandSupply/GetListSwappingIn/?Start=1&Limit=100&'+param);
                return res;
            },

            // getType: function() {
            //     var res = $http.get('/api/sales/DemandSupply/GetComboBoxType');
            //     return res;
            // },

            getListDetail: function(SwappInId) {
                var res = $http.get('/api/sales/DemandSupply/GetListDetailSwappingIn/?SwappInId=' + SwappInId);
                return res;
            },

            getListDetailOut: function(SwappOutId) {
                var res = $http.get('/api/sales/DemandSupply/GetListDetailSwappingOut/?SwappOutId=' + SwappOutId);
                return res;
            },

            getAlasanBatal: function(data) {
                var url = '/api/sales/DemandSupply/BatalRejectSwappingIn';
                var param = JSON.stringify(data);
                return $http.post(url, param);
            },

            getAlasanBatalOut: function(data) {
                var url = '/api/sales/DemandSupply/BatalRejectSwappingOut';
                var param = JSON.stringify(data);
                return $http.post(url, param);
            },

            getApproveDetail: function(data) {
                var url = '/api/sales/DemandSupply/ApprSwappingIn';
                var param = JSON.stringify(data);
                return $http.post(url, param);
                
            },

            getApproveDetailOut: function(data) {
                var url = '/api/sales/DemandSupply/ApprSwappingOut';
                var param = JSON.stringify(data);
                return $http.post(url, param);
                
            },

            getDealerSwapping: function(TipePT){
                var res = $http.get('/api/sales/DemandSupply/GetDealerSwappingIn/?TipePT='+TipePT);
                return res;
            },

            getNotifNoka: function(FrameNo, CheckId){
                var res = $http.get('/api/sales/DemandSupply/GetCheckSwappingIn/?Frameno='+FrameNo+'&DealerCode='+CheckId);
                return res;
            },

            getNotifNokaOut: function(FrameNo, CheckId, LocationTypeName){
                var res = $http.get('/api/sales/DemandSupply/GetCheckSwappingOut/?Frameno='+FrameNo+'&DealerCode='+CheckId+'&LocationTypeName='+LocationTypeName);
                return res;
            },

            create: function(createSwappingIn) {
                //inputSwap.SentDate = fixDate(inputSwap.SentDate);
                return $http.post('/api/sales/DemandSupply/SwappingInCreate', createSwappingIn);
            },

            createOut: function(createSwappingOut) {
                //inputSwap.SentDate = fixDate(inputSwap.SentDate);
                return $http.post('/api/sales/DemandSupply/SwappingOutCreate', createSwappingOut);
            }
        }
    });