angular.module('app')
    .controller('ModelAssignmentController', function(bsNotify,MobileHandling,uiGridConstants, uiGridGroupingConstants, $scope, $http, $httpParamSerializer, CurrentUser, ModelAssignmentFactory, MaintainSOFactory, ModelFactory, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.targetAssignment = true;
    $scope.targetDetail = false;
    $scope.loading=true;
    //$scope.mSelectAll = false;
    $scope.isEdit = "false";
    $scope.selectedRows = 0;

    $scope.$on('$viewContentLoaded', function() {
       
        $scope.gridData=[];
        $scope.filter = {EmployeeId: null, VehicleModelId: null, SalesCategoryId: null};
         //----------Begin Check Display---------
             
               $scope.bv=MobileHandling.browserVer();
               $scope.bver=MobileHandling.getBrowserVer();
               //console.log("browserVer=>",$scope.bv);
               //console.log("browVersion=>",$scope.bver);
               //console.log("tab: ",$scope.tab);
               if($scope.bv!=="Chrome"){
               if ($scope.bv=="Unknown") {
                 if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                 {
                   $scope.removeTab($scope.tab.sref);
                 }
               }        
               }
             
             //----------End Check Display-------
    });

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.Role = $scope.user;
    //console.log("user", $scope.Role);

    $scope.mTargetAssign = null; //Model
    $scope.xRole={selected:[]};
    
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,    
    };

    if($scope.Role.RoleName == "KACAB"){
        var getName = "?position=" + "SUPERVISORSALES";
        $scope.grid = {
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        columnDefs : [
            { name:'Supervisor', field:'EmployeeName',  width:'50%'}, 
			{ name:'JumlahModel', field:'ModelCount'},
            {
                name:'Action',  
                allowCellFocus: false,
                width:'10%',                    
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.LihatDetail(row.entity)" tabindex="0"> ' +
                '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a>' +

                '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Assign Model" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.editAssign(row.entity)" tabindex="0"> ' +
                '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a>'
            }
        ]
    };
        ModelAssignmentFactory.getDataSalesman(getName).then(
            function(res){
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );
    }
    else if($scope.Role.RoleName == "SUPERVISOR SALES"){
        var getName = "?position=" + "SALESMAN";
        $scope.grid = {
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,   
        columnDefs : [
            { name:'Sales', field:'EmployeeName',  width:'50%'}, 
			{ name:'Kategori Sales', field:'SalesCategoryName'}, 
			{ name:'JumlahModel', field:'ModelCount'},
            {
                name:'Action',  
                allowCellFocus: false,
                width:'10%',                    
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.LihatDetail(row.entity)" tabindex="0"> ' +
                '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a>' +

                '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Assign Model" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.editAssign(row.entity)" tabindex="0"> ' +
                '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a>'
            }
        ]
    };

        ModelAssignmentFactory.getDataSalesman(getName).then(
            function(res){
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );
    }
    else{
        MaintainSOFactory.getDataSalesman().then(
            function(res){
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );
    }
    
    //----------------------------------
    // Get Data test
    //----------------------------------
    var gridData = [];
    // $scope.getData = function() {
    //     ModelAssignmentFactory.getData($scope.filter).then(
    //         function(res){
    //             $scope.grid.data = res.data.Result;
    //             $scope.loading=false;
    //             return res.data.Result;
    //         },
    //         function(err){
    //             console.log("err=>",err);
    //         }
    //     );
    // }

    $scope.getData = function() {
        $scope.loading = true;
        var param = null;
        param = $httpParamSerializer($scope.filter);
        ModelAssignmentFactory.getData(param).then(function(res) {
            $scope.loading = false;
            $scope.grid.data = res.data.Result;
          
        })
    }

    // $scope.trueAll = function() {
        // //setTimeout(function(){
            // //console.log('test',$scope.mSelectAll);
            // if($scope.mSelectAll == true){
                // for (var i in $scope.gridModel.data){
                    // $scope.gridModel.data[i].pilih = true;
                // }
            // }else{
                // for (var i in $scope.gridModel.data){
                    // $scope.gridModel.data[i].pilih = false;
                // }
            // }
        // //});
       
        
    // }

    // $scope.cheking = function (){
        // //console.log("kesini ga");
        
      // //  setTimeout(function(){
            // var count = 0;
            // for(var i=0; i<$scope.gridModel.data.length; i++ ){
                // if($scope.gridModel.data[i].pilih == true){
                    // count++
                // }
            // }
            // //console.log("test", count, $scope.mSelectAll, $scope.gridModel.data);
            // if (count == $scope.gridModel.data.length && $scope.mSelectAll == false){
                // $scope.mSelectAll = true;
            // }else if(count == $scope.gridModel.data.length &&  $scope.mSelectAll == true){
                // $scope.mSelectAll = true;
            // }else if(count != $scope.gridModel.data.length &&  $scope.mSelectAll == false){
                // $scope.mSelectAll = false;
            // }else if(count != $scope.gridModel.data.length &&  $scope.mSelectAll == true){
                // $scope.mSelectAll = false;
            // }
        // //},0);
       
        
    // }

    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    MaintainSOFactory.getOutlet().then(
        function(res){
            $scope.getOutlet = res.data.Result;
            return res.data;
        }
    );

    ModelAssignmentFactory.getSCategory().then(
        function(res){
            $scope.categoriSales = res.data.Result;
            return res.data;
        }
    );

    $scope.LihatDetail = function (SelectData) {
        $scope.gridApiModelAssignmentDalem.selection.clearSelectedRows();
		$scope.ModelAssignmentAmanBuatCek=false;
		$scope.ViewDetail = angular.copy(SelectData);
        $scope.targetAssignment = false;
        $scope.targetDetail = true;
        $scope.disabledModel = true;
        $scope.isEdit = "false";

        var getSales = "?EmployeeId=" + $scope.ViewDetail.EmployeeId;
        if($scope.Role.RoleName == "SUPERVISOR SALES"){
            ModelAssignmentFactory.getDataDetailSpv(getSales).then(
                function(res){
                    $scope.gridModel.data = res.data.Result;
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
                        }
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
					$scope.ModelAssignmentAmanBuatCek=true;
                    //console.log('test', $scope.gridModel.data,  res.data.Result);
                    setTimeout(function(){
                        $scope.gridApi.treeBase.expandAllRows();
                    }, 300);
                    var count = 0;
                    for(var i in res.data.Result){
                        if(res.data.Result[i].pilih == true){
                            count++
                        }
                    }
                    //console.log('test2', count,  res.data.Result.length);
                    if (count == res.data.Result.length){
                        //$scope.mSelectAll = true;
                    }
                    //$scope.gridModel.showTreeRowHeader = false
                    //console.log("getDataDetail", $scope.getDataDetail);
                    return res.data;
                }
            );
        }else{
            ModelAssignmentFactory.getDataDetailKacab(getSales).then(
                function(res){
                    $scope.gridModel.data = res.data.Result;
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
                        }
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
					$scope.ModelAssignmentAmanBuatCek=true;
                    //console.log('test', $scope.gridModel.data,  res.data.Result);
                    setTimeout(function(){
                        $scope.gridApi.treeBase.expandAllRows();
                    }, 300);
                    var count = 0;
                    for(var i in res.data.Result){
                        if(res.data.Result[i].pilih == true){
                            count++
                        }
                    }
                    //console.log('test2', count,  res.data.Result.length);
                    if (count == res.data.Result.length){
                        //$scope.mSelectAll = true;
                    }
                    //$scope.gridModel.showTreeRowHeader = false
                    //console.log("getDataDetail", $scope.getDataDetail);
                    return res.data;
                }
            );
        }
        
        
    }

    $scope.btnBatalAssign = function () {
        //$scope.mSelectAll =false;
        $scope.targetAssignment = true;
        $scope.targetDetail = false;
    }

    $scope.editAssign = function (SelectData){
        $scope.gridApiModelAssignmentDalem.selection.clearSelectedRows();
		$scope.ModelAssignmentAmanBuatCek=false;
		$scope.ViewDetail = angular.copy(SelectData);
        $scope.targetAssignment = false;
        $scope.targetDetail = true;
        $scope.isEdit = "true";
        var getSales = "?EmployeeId=" + $scope.ViewDetail.EmployeeId;
        if($scope.Role.RoleName == "SUPERVISOR SALES"){
            ModelAssignmentFactory.getDataDetailSpv(getSales).then(
                function(res){
                    var count = 0;
                    for(var i in res.data.Result){
                        if(res.data.Result[i].pilih == true){
                            count++
                        }
                    }
                    if (count == res.data.Result.length){
                        //$scope.mSelectAll = true;
                    }
                    $scope.gridModel.data = res.data.Result;
					
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
                        }
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
					$scope.ModelAssignmentAmanBuatCek=true;
                   
                    setTimeout(function(){
                        $scope.gridApi.treeBase.expandAllRows();
                    }, 300);
                // $scope.gridModel.showTreeRowHeader = false;
                    //console.log("getDataDetail", $scope.gridModel);
                    //return res.data;
                }
            );
        }else{
            ModelAssignmentFactory.getDataDetailKacab(getSales).then(
                function(res){
                    var count = 0;
                    for(var i in res.data.Result){
                        if(res.data.Result[i].pilih == true){
                            count++
                        }
                    }
                    if (count == res.data.Result.length){
                        //$scope.mSelectAll = true;
                    }
                    $scope.gridModel.data = res.data.Result;
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
							
                        }
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
					$scope.ModelAssignmentAmanBuatCek=true;

                    setTimeout(function(){
                        $scope.gridApi.treeBase.expandAllRows();
                    }, 300);
                // $scope.gridModel.showTreeRowHeader = false;
                    //console.log("getDataDetail", $scope.gridModel);
                    return res.data;
                }
            );
        }
        $scope.disabledModel = false;
    }

    $scope.btnSimpanAssign = function (){
        ModelAssignmentFactory.create($scope.gridModel.data).then(
            function(res){
                //$scope.mSelectAll = false;
                $scope.targetAssignment = true;
                $scope.targetDetail = false;
                $scope.disabledModel = false;
                bsNotify.show({
                    title: "Berhasil",
                    content: "Model berhasil disimpan.",
                    type: 'success'
                });
                $scope.getData();
            },
            function(err)
                {console.log("err=>",failed);
            }
        );       

        
    }    

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    
    var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.editAssign(row.entity)">Ubah</u>&nbsp<u ng-click="grid.appScope.$parent.LihatDetail(row.entity)">Detail</u></span>'
    
    $scope.category = [];

    function build(data){
        $scope.category = [];
        for (var i in data){
            $scope.category.push({VehicleSegmentationProductConceptName: data[i].VehicleSegmentationProductConceptName});
        }

        var listModel = [];
        for(var i in data){
            for(var j in data[i].listTargetAssignmentDetailView){
                listModel.push({
                    EmployeeId: data[i].listTargetAssignmentDetailView[j].EmployeeId,
                    OutletCode: data[i].listTargetAssignmentDetailView[j].OutletCode,
                    OutletId: data[i].listTargetAssignmentDetailView[j].OutletId,
                    OutletName: data[i].listTargetAssignmentDetailView[j].OutletName,
                    TargetAssignmentId: data[i].listTargetAssignmentDetailView[j].TargetAssignmentId,
                    VehicleModelId: data[i].listTargetAssignmentDetailView[j].VehicleModelId,
                    VehicleSegmentationProductConceptName: data[i].listTargetAssignmentDetailView[j].VehicleSegmentationProductConceptName,
                    VehicleModelName: data[i].listTargetAssignmentDetailView[j].VehicleModelName,
                    VehicleSegmentationProductConceptId: data[i].listTargetAssignmentDetailView[j].VehicleSegmentationProductConceptId,
                    pilih: data[i].listTargetAssignmentDetailView[j].pilih
                });
            }   
        }
        console.log("category", $scope.category);
        console.log("unit", listModel);
        return listModel;
    }

    function rebuild(data){
        var temp = angular.copy(data);
        console.log("datanya", temp);
        $scope.rebuild = []
        for(var i in $scope.category){
            $scope.rebuild.push({
                VehicleSegmentationProductConceptId: $scope.category[i].VehicleSegmentationProductConceptId,
                VehicleSegmentationProductConceptName: $scope.category[i].VehicleSegmentationProductConceptName,
                listTargetAssignmentDetailView: []
            })
            for (var j in temp){
                if($scope.rebuild[i].VehicleSegmentationProductConceptName == temp[j].VehicleSegmentationProductConceptName){
                    $scope.rebuild[i].listTargetAssignmentDetailView.push({
                        EmployeeId: temp[j].EmployeeId,
                        OutletCode: temp[j].OutletCode,
                        OutletId: temp[j].OutletId,
                        OutletName: temp[j].OutletName,
                        TargetAssignmentId: temp[j].TargetAssignmentId,
                        VehicleModelId: temp[j].VehicleModelId,
                        VehicleSegmentationProductConceptName: temp[j].VehicleSegmentationProductConceptName,
                        VehicleModelName: temp[j].VehicleModelName,
                        VehicleSegmentationProductConceptId: temp[j].VehicleSegmentationProductConceptId,
                        pilih: temp[j].pilih
                    });
                }
                
            }
        }

        return $scope.rebuild;
    }
        


    //}
    //----------------------------------
    // Grid Setup
    //----------------------------------


    var action = '<div class="row" style="margin: 5px 0 0 10px">{{row.entity.VehicleModelName}}</div>';

    $scope.gridModel = {
        enableSorting: false,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10,25,50],
        paginationPageSize: 10,
        columnDefs: [
            // { name:'Segmentation', field:'VehicleSegmentationProductConceptName',
            //     grouping: { groupPriority: 0 },
            //     sort: { priority: 0, direction: 'asc' },
            //     cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
            // },
             
			{ name:'Model', cellTemplate: action, aggregationHideLabel: true},

        ],
        onRegisterApi: function( gridApi ) {
            $scope.gridApiModelAssignmentDalem = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope,function(row){
				if($scope.ModelAssignmentAmanBuatCek==true && $scope.isEdit == "false")
			   {
					//ini emang kosong
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
                        }
                    }
			   }
			   else
			   {
				$scope.Rows_Selected_InModelAssignmentDetail = gridApi.selection.getSelectedRows();
			   }
				
				if($scope.ModelAssignmentAmanBuatCek==true)
				{
					for(var x in $scope.gridModel.data){
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
				}
				
				
				
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
			   if($scope.ModelAssignmentAmanBuatCek==true && $scope.isEdit == "false")
			   {
					//ini emang kosong
					for(var x in $scope.gridModel.data){
                        if($scope.gridModel.data[x].pilih == true){
							$scope.gridApiModelAssignmentDalem.grid.modifyRows($scope.gridModel.data);
							$scope.gridApiModelAssignmentDalem.selection.selectRow($scope.gridModel.data[x]);
                        }
                    }
			   }
			   else
			   {
				$scope.Rows_Selected_InModelAssignmentDetail = gridApi.selection.getSelectedRows();
			   }
			   
			   if($scope.ModelAssignmentAmanBuatCek==true)
				{
					for(var x in $scope.gridModel.data){
						$scope.gridModel.data[x].pilih=false;
                    }
					
					for(var i = 0; i < $scope.gridModel.data.length; ++i)
					{
						for(var x = 0; x < $scope.Rows_Selected_InModelAssignmentDetail.length; ++x)
						{
							if($scope.gridModel.data[i].VehicleModelName==$scope.Rows_Selected_InModelAssignmentDetail[x].VehicleModelName)
							{
								$scope.gridModel.data[i].pilih=true;
							}
						}
					}
				}
			   
			});
          }
    };
	
	$scope.getData();
});
