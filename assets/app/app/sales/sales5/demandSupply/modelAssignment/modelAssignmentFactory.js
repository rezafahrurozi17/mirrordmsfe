angular.module('app')
  .factory('ModelAssignmentFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
          if(param==undefined || param==null || param==""){
              param="";
          }else{
              param="&"+param;
          }
              var res = $http.get('/api/sales/DemandSupplyModelAssignment/Filter?start=1&limit=1000'+param);
               return res;
          console.log("daata", res);
      },

      getDataDetailKacab: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignmentDetail/Kacab' + param);         
          return res;
      },

      getDataDetailSpv: function(param) {
        var res=$http.get('/api/sales/DemandSupplyTargetAssignmentDetail/SPV' + param);         
          return res;
      },

      getDataSalesman: function(param) {
          var res=$http.get('/api/sales/MProfileEmployee/' + param);
          return res;
      },

      getSCategory: function() {
        return $http.get('/api/sales/PCategorySalesman/?start=1&limit=1000');
      },

      create: function(modelAssignment) {
        return $http.post('/api/sales/DemandSupplyTargetAssignmentDetail', modelAssignment);
      }
    }
  });