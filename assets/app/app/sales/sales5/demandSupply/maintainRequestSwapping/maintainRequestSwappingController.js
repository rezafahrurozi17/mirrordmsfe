angular.module('app')
    .controller('MaintainRequestSwappingController', function ($filter, bsNotify, bsAlert, $scope, $http, CurrentUser, MaintainRequestSwappingFactory, MaintainSOFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MaintainRequestSwapping = true;
        $scope.StockView = false;
        $scope.BuatQuotationShow = false;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.selectedReq = [];
        $scope.OutletNameOwner;

		$scope.dateOptionsMaintainRequestSwapping = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };


        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainRequestSwapping = null; //Model
        $scope.xRole = { selected: [] };
		$scope.filter={ StartRequestDate:null, EndRequestDate:null};
		var Da_Today_Date=new Date();
		$scope.filter.StartRequestDate=new Date(Da_Today_Date.setDate(1));
		$scope.filter.EndRequestDate=new Date();

        // $scope.urlPricing = "/api/fe/PricingEngine/?PricingId=30601&OutletId=" + $scope.user.OutletId + "&DisplayId=28030601";

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function () {
            MaintainRequestSwappingFactory.getData($scope.filter).then(
                function (res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        }

        MaintainSOFactory.getOutlet().then(
            function (res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainRequestSwappingFactory.getType().then(
            function (res) {
                $scope.dataType = res.data.Result;
                return res.data;
            }
        );

        MaintainRequestSwappingFactory.getTermOfPayment().then(
            function (res) {
                $scope.termOfPayment = res.data.Result;
                return res.data;
            }
        );

        $scope.tempFilter = [];

        $scope.StockViewClick = function (selected) {
            //console.log("CVO",selected);
            $scope.selectedrow = selected;
            var getVehicleTypeColorId = "?VehicleTypeColorId=" + $scope.selectedrow.VehicleTypeColorId;
            MaintainRequestSwappingFactory.getDataNoRangka(getVehicleTypeColorId).then(
                function (res) {
                    $scope.gridStockView.data = res.data.Result;
                    $scope.tempFilter = res.data.Result
                    return res.data;
                }
            );
            $scope.MaintainRequestSwapping = false;
            $scope.StockView = true;
        }

        $scope.btnkembaliStockView = function () {
            $scope.StockView = false;
            $scope.MaintainRequestSwapping = true;
        }

        $scope.priceQuo = { UnitPrice: 0, UnitDiscount: 0 , ExpeditionSellPrice : 0, ExpeditionDiscount : 0};

        $scope.btnValidasiQuotation = function () {
            console.log('user',$scope.user)
            if($scope.user.RoleName == "Admin HO"){
                if($scope.selectedReq[0].SOCode == null || $scope.selectedReq[0].SOCode == ""){
                    $scope.btnQuotation();
                }else{
                    bsAlert.alert({
                        title: "Data Request Swapping ini adalah data request dari Cabang dan tidak bisa dilanjutkan oleh HO." ,
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                    },
                    function() {
                    },
                    function() {
                    })
                }
            }else{
                if($scope.selectedReq[0].SOCode == null || $scope.selectedReq[0].SOCode == ""){
                    bsAlert.alert({
                        title: "Data Request Swapping ini adalah data request dari HO dan tidak bisa dilanjutkan oleh cabang." ,
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                    },
                    function() {
                    },
                    function() {
                    })
                }else{
                    $scope.btnQuotation();
                }
            }
        }
        $scope.btnQuotation = function () {
            $scope.CreateQuo = $scope.selectedReq;
            console.log("test", $scope.CreateQuo);
            $scope.OutletNameOwner = $scope.CreateQuo[0].OutletNameOwner;


            $scope.priceQuo = { UnitPrice: 0, UnitDiscount: 0 , ExpeditionSellPrice : 0, ExpeditionDiscount : 0};


            // _.extend( $scope.CreateQuo[0], price);
            $scope.jsData = {
                "Title": null,
                "HeaderInputs": {
                    "$Dekatsu$": $scope.CreateQuo[0].VehicleTypeDescription,
                    "$VehicleId$": $scope.CreateQuo[0].VehicleId,
                    "$OutletId$": $scope.user.OutletId,
                    "$VehiclePrice$": $scope.priceQuo.UnitPrice,
                    "$VehicleDisc$": $scope.priceQuo.UnitDiscount
                }
            };

            // $scope.Kalkulasi();

            if (($scope.CreateQuo[0].FrameNo != "" && $scope.CreateQuo[0].VehicleId != null) || ($scope.CreateQuo[0].FrameNo != null && $scope.CreateQuo[0].VehicleId != null)) {
                var getRequestSwapping = "?ReceiveRequestSwappingId=" + $scope.CreateQuo[0].ReceiveRequestSwappingId + "&VehicleId=" + $scope.CreateQuo[0].VehicleId;
                MaintainRequestSwappingFactory.getQuotation(getRequestSwapping).then(
                    function (res) {
                        $scope.dataQuotation = res.data.Result;
                        $scope.jsData = {
                            "Title": null,
                            "HeaderInputs": {
                                "$Dekatsu$": $scope.dataQuotation[0].VehicleTypeDescription,
                                "$VehicleId$": $scope.dataQuotation[0].VehicleId,
                                "$OutletId$": $scope.user.OutletId,
                                "$VehiclePrice$": $scope.priceQuo.UnitPrice,
                                "$VehicleDisc$": $scope.priceQuo.UnitDiscount
                            }
                        };
                        return res.data;
                    },
                    function(err){
                        var msg = "";
                        console.log("Cek Pesan Error", err.data.Message)
                        if (err.data.Message.split(':').length > 1) {
                            msg = err.data.Message.split(':')[1];
                            msg = msg.split('"')[1];
                        }
                        console.log("Cek Pesan Error", msg)
                        bsNotify.show({
                            title: "Warning Message",
                            content: msg,
                            type: 'warning'
                        });
                        $scope.MaintainRequestSwapping = true;
                        $scope.BuatQuotationShow = false;
                        return;
                    }
                );

                $scope.MaintainRequestSwapping = false;
                $scope.BuatQuotationShow = true;
            } else {
                bsNotify.show({
                    title: "Warning Message",
                    content: "Silahkan pilih nomor rangka dengan melakukan Stock View",
                    type: 'warning'
                });
            }
        };
		
		$scope.MaintainRequestSwappingPaksaFilterTanggal = function () {
			if($scope.filter.EndRequestDate<$scope.filter.StartRequestDate)
			{
				$scope.filter.EndRequestDate=$scope.filter.StartRequestDate;
			}
		}

        $scope.Kalkulasi = function () {
            var OutletId = $scope.user.OutletId;

            if($scope.user.RoleName == "Admin HO"){
                OutletId = $scope.CreateQuo[0].OutletId;
            }

            switch ($scope.dataQuotation[0].QuotationTypeId) {
                case 1:
                    console.log("1 - Diferent PDS");
                    var url = '/api/fe/PricingEngine/?PricingId=30601&OutletId=' + OutletId + '&DisplayId=' + OutletId + '30601';
                    break;

                // unit & ekspedisi
                case 2:
                    console.log("2 - Same PDS");
                    var url = '/api/fe/PricingEngine/?PricingId=30701&OutletId=' + OutletId + '&DisplayId=' + OutletId + '30701';
                    break;

                // logistik
                case 3:
                    console.log("3 - Diferent PDC");
                    var url = '/api/fe/PricingEngine/?PricingId=30801&OutletId=' + OutletId + '&DisplayId=' + OutletId + '30801';
                    break;

                //  unit
                case 4:
                    console.log("4 - Same PDC");
                    var url = '/api/fe/PricingEngine/?PricingId=30901&OutletId=' + OutletId + '&DisplayId=' + OutletId + '30901';
                    break;

                //  0
            }


            $scope.jsData.HeaderInputs["$VehiclePrice$"] = parseInt($scope.priceQuo.UnitPrice);
            $scope.jsData.HeaderInputs["$VehicleDisc$"] = parseInt($scope.priceQuo.UnitDiscount);
            $scope.jsData.HeaderInputs["$EkspPrice$"] = parseInt($scope.priceQuo.ExpeditionSellPrice);
            $scope.jsData.HeaderInputs["$EkspDiscount$"] = parseInt($scope.priceQuo.ExpeditionDiscount);

            $http.post(url, $scope.jsData)
                .then(function (res) {
                    //console.log("res=>", res);
                    $scope.jsData = res.data;

                    switch ($scope.dataQuotation[0].QuotationTypeId) {
                        case 1: // unit & ekspedisi
                            console.log("1 - Diferent PDS");
                            $scope.dataQuotation[0].Dekatsu = $scope.jsData.Codes["$Dekatsu$"];
                            $scope.dataQuotation[0].SellPrice = parseInt($scope.jsData.Codes["[VehiclePrice]"]);
                            $scope.dataQuotation[0].Discount = parseInt($scope.jsData.Codes["[VehicleDisc]"]);
                            $scope.dataQuotation[0].PriceAfterDiscVehicle = parseInt($scope.jsData.Codes["[PriceAfterDiscVehicle]"]);
                            $scope.dataQuotation[0].PPH22 = parseInt($scope.jsData.Codes["[VehiclePPH22]"]);
                            $scope.dataQuotation[0].DPP = parseInt($scope.jsData.Codes["[VehicleDPP]"]);
                            $scope.dataQuotation[0].VAT = parseFloat($scope.jsData.Codes["[VehicleVAT]"]);
                            $scope.dataQuotation[0].Total = parseInt($scope.jsData.Codes["[VehicleTotal]"]);
                            $scope.dataQuotation[0].ExpeditionSellPrice = parseInt($scope.jsData.Codes["[EkspPrice]"]);
                            $scope.dataQuotation[0].ExpeditionDiscount = parseInt($scope.jsData.Codes["[EkspDiscount]"]);
                            $scope.dataQuotation[0].PriceAfterExpeditionDiscount = parseInt($scope.jsData.Codes["[PriceAfterDiscEks]"]);
                            $scope.dataQuotation[0].ExpeditionDPP = parseInt($scope.jsData.Codes["[EkspdDPP]"]);
                            $scope.dataQuotation[0].ExpeditionVAT = parseFloat($scope.jsData.Codes["[EkspdVAT]"]);
                            $scope.dataQuotation[0].ExpeditionTotal = parseInt($scope.jsData.Codes["[EkspdTotal]"]);
                            $scope.dataQuotation[0].TotalDPP = parseInt($scope.jsData.Codes["[TotalDPP]"]);
                            $scope.dataQuotation[0].TotalVAT = parseInt($scope.jsData.Codes["[TotalVAT]"]);
                            $scope.dataQuotation[0].GrandTotal = parseInt($scope.jsData.Codes["[GrandTotal]"]);
                            break;

                        case 2: // logistik
                            console.log("2 - Same PDS");
                            //$scope.dataQuotation[0].Dekatsu = $scope.jsData.Codes["$Dekatsu$"];
                            $scope.dataQuotation[0].ExpeditionSellPrice = parseInt($scope.jsData.Codes["[EkspPrice]"]);
                            $scope.dataQuotation[0].ExpeditionDiscount = parseInt($scope.jsData.Codes["[EkspDiscount]"]);
                            $scope.dataQuotation[0].PriceAfterExpeditionDiscount = parseInt($scope.jsData.Codes["[PriceAfterDiscEks]"]);
                            $scope.dataQuotation[0].ExpeditionDPP = parseInt($scope.jsData.Codes["[EkspdDPP]"]);
                            $scope.dataQuotation[0].ExpeditionVAT = parseFloat($scope.jsData.Codes["[EkspdVAT]"]).toFixed(2);
                            $scope.dataQuotation[0].ExpeditionTotal = parseInt($scope.jsData.Codes["[EkspdTotal]"]);
                            $scope.dataQuotation[0].TotalDPP = parseInt($scope.jsData.Codes["[TotalDPP]"]);
                            $scope.dataQuotation[0].TotalVAT = parseInt($scope.jsData.Codes["[TotalVAT]"]);
                            $scope.dataQuotation[0].GrandTotal = parseInt($scope.jsData.Codes["[GrandTotal]"]);
                            break;

                        case 3: //  unit
                            console.log("3 - Diferent PDC");
                            $scope.dataQuotation[0].Dekatsu = $scope.jsData.Codes["$Dekatsu$"];
                            $scope.dataQuotation[0].SellPrice = parseInt($scope.jsData.Codes["[VehiclePrice]"]);
                            $scope.dataQuotation[0].Discount = parseInt($scope.jsData.Codes["[VehicleDisc]"]);
                            $scope.dataQuotation[0].PriceAfterDiscVehicle = parseInt($scope.jsData.Codes["[PriceAfterDiscVehicle]"]);
                            $scope.dataQuotation[0].DPP = parseInt($scope.jsData.Codes["[VehicleDPP]"]);
                            $scope.dataQuotation[0].VAT = parseFloat($scope.jsData.Codes["[VehicleVAT]"]);
                            $scope.dataQuotation[0].PPH22 = parseInt($scope.jsData.Codes["[VehiclePPH22]"]);
                            $scope.dataQuotation[0].Total = parseInt($scope.jsData.Codes["[VehicleTotal]"]);
                            $scope.dataQuotation[0].TotalDPP = parseInt($scope.jsData.Codes["[TotalDPP]"]);
                            $scope.dataQuotation[0].TotalVAT = parseInt($scope.jsData.Codes["[TotalVAT]"]);
                            $scope.dataQuotation[0].GrandTotal = parseInt($scope.jsData.Codes["[GrandTotal]"]);
                            break;

                        case 4: //  0
                            console.log("4 - Same PDC");
                            $scope.dataQuotation[0].Dekatsu = $scope.jsData.Codes["$Dekatsu$"];
                            $scope.dataQuotation[0].SellPrice = parseInt($scope.jsData.Codes["[VehiclePrice]"]);
                            $scope.dataQuotation[0].Discount = parseInt($scope.jsData.Codes["[VehicleDisc]"]);
                            $scope.dataQuotation[0].PriceAfterDiscVehicle = parseInt($scope.jsData.Codes["[PriceAfterDiscVehicle]"]);
                            $scope.dataQuotation[0].DPP = parseInt($scope.jsData.Codes["[VehicleDPP]"]);
                            $scope.dataQuotation[0].VAT = parseFloat($scope.jsData.Codes["[VehicleVAT]"]);
                            $scope.dataQuotation[0].Total = parseInt($scope.jsData.Codes["[VehicleTotal]"]);
                            $scope.dataQuotation[0].PPH22 = parseInt($scope.jsData.Codes["[VehiclePPH22]"]);
                            $scope.dataQuotation[0].TotalDPP = parseInt($scope.jsData.Codes["[TotalDPP]"]);
                            $scope.dataQuotation[0].TotalVAT = parseInt($scope.jsData.Codes["[TotalVAT]"]);
                            $scope.dataQuotation[0].GrandTotal = parseInt($scope.jsData.Codes["[GrandTotal]"]);
                            break;
                    }

                });
        }

        $scope.btnsimpanQuotation = function () {
            var Separator = $scope.priceQuo.UnitPrice
            Separator = String(Separator).split("").reverse().join("")
                        .replace(/(\d{3}\B)/g, "$1.")
                        .split("").reverse().join("");  
            console.log("Cek Saparator", Separator)
            
            bsAlert.alert({
                title: "Apakah anda yakin harga unit yang akan dilakukan swapping sejumlah Rp. " + Separator ,
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            },
            function() {
                if($scope.user.RoleName == "Admin HO" && $scope.dataQuotation[0].QuotationTypeId == 4){
                    bsAlert.alert({
                        title: "Apakah anda yakin untuk merubah kepemilikan?" ,
                        text: "",
                        type: "question",
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak',
                    },
                    function() {
                        $scope.simpanQuotation();
                    },
                    function() {
                        console.log("batal")
                    })
                }else{
                    $scope.simpanQuotation();
                }
            },
            function() {
                console.log("batal")
            })
        }

        $scope.simpanQuotation = function(){
            if($scope.user.RoleName == "Admin HO" && $scope.dataQuotation[0].QuotationTypeId == 4){
                var confirmSwapping = ({
                    FrameNumber: angular.copy($scope.CreateQuo[0].FrameNo),
                    OutletDestination: angular.copy($scope.CreateQuo[0].OutletCodeRequestor),
                })

                console.log("data confirm swapping", confirmSwapping)
                MaintainRequestSwappingFactory.confirmSwapping(confirmSwapping).then(
                    function (res) {
                        console.log("confirmSwapping SUKSES");
                        MaintainRequestSwappingFactory.create($scope.dataQuotation).then(
                            function (res) {
                                //console.log("Succ=>", Success);
                                bsNotify.show({
                                    title: "Success Message",
                                    content: "Berhasil Buat Quotation",
                                    type: 'success'
                                });
            
                                $scope.dataQuotation = [];
                                $scope.priceQuo = {};
                                $scope.getData();
                                $scope.BuatQuotationShow = false;
                                $scope.MaintainRequestSwapping = true;
                            },
                            function (err) {
                                //console.log("err=>", err);
                                bsNotify.show({
                                    title: "Error Message",
                                    content: "Gagal Buat Quotation",
                                    type: 'danger'
                                });
                            }
                        );
                    },
                    function (err) {
                        //console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: err.data.Message,
                            type: 'danger'
                        });
                    }
                );
            }else{
                MaintainRequestSwappingFactory.create($scope.dataQuotation).then(
                    function (res) {
                        //console.log("Succ=>", Success);
                        bsNotify.show({
                            title: "Success Message",
                            content: "Berhasil Buat Quotation",
                            type: 'success'
                        });
    
                        $scope.dataQuotation = [];
                        $scope.priceQuo = {};
                        $scope.getData();
                        $scope.BuatQuotationShow = false;
                        $scope.MaintainRequestSwapping = true;
                    },
                    function (err) {
                        //console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: "Gagal Buat Quotation",
                            type: 'danger'
                        });
                    }
                );
            }
        }

        $scope.btnkembaliQuotation = function () {
            $scope.BuatQuotationShow = false;
            $scope.MaintainRequestSwapping = true;
            $scope.dataQuotation = [];
            $scope.priceQuo = {};
        }

        $scope.SwappingClick = function (selected) {
            $scope.Sample = selected;
            $scope.selectedrow.FrameNo = $scope.Sample.FrameNo;
            $scope.selectedrow.VehicleId = $scope.Sample.VehicleId;
            // MaintainRequestSwappingFactory.swapping($scope.selectedrow).then(
            //     function(res){
            //         console.log("Succ=>",success);
            //     },
            //     function(err)
            //         {console.log("err=>", err);
            //     }
            // );
            $scope.MaintainRequestSwapping = true;
            $scope.StockView = false;
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.onListSave = function (item) {
            console.log("save_modal");
        }

        $scope.onListCancel = function (item) {
            console.log("cancel_modal");
        }


        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function (rows) {
            $scope.selectedReq = rows;
            console.log("onSelectRows=>", $scope.selectedReq);
        }

        var btnStockView = '<i title="Stock View" ng-show="row.entity.SOCode != null" class="fa fa-fw fa-lg fa-arrow-circle-right" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.StockViewClick(row.entity)" ></i>'
        var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.SwappingClick(row.entity)">Swapping</u></span></u></span>'
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Tanggal Permintaan Swapping', field: 'RequestDate', visible: true, width: '12%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'No SO', field: 'SOCode', width: '18%' },
                { name: 'Cabang Pemilik', field: 'OutletNameOwner', width: '18%' },
                { name: 'Cabang Tujuan', field: 'OutletNameRequestor', width: '18%' },
                { name: 'Model', field: 'VehicleModelName', width: '12%' },
                { name: 'Tipe', field: 'VehicleTypeDescription', width: '20%' },
                { name: 'Warna', field: 'ColorName', width: '15%' },
                { displayName:'No Rangka/RRN' ,name: 'No Rangka/RRN', field: 'FrameNo', width: '15%' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    visible: true,
                    width: '10%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    pinnedRight: true,
                    enableColumnResizing: true,
                    cellTemplate: btnStockView
                }
            ]
        };

        $scope.gridStockView = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'No Rangka', width: '20%', field: 'FrameNo' },
                { name: 'Do tam', width: '10%', field: 'DOTAMDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Aging', width: '20%', field: 'Aging' },
                { name: 'Lokasi Unit', width: '20%', field: 'LocationName' },
                { name: 'Status', width: '20%', field: 'StatusStockName' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '10%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    pinnedRight: true,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };

        $scope.gridStockView.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selectedStock = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridrincianHarga = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'Model', field: 'VehicleModelName' },
                { name: 'Keterangan', field: 'Description' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };

        $scope.filterData = {
            defaultFilter: [
                { name: 'No Rangka', value: 'FrameNo' },
                { name: 'Aging', value: 'Aging' },
                { name: 'Lokasi Unit', value: 'LastLocation' },
                { name: 'Status Unit', value: 'StatusStockName' }

            ]
        };

        $scope.filterCustom = function () {
            // if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '') ||
                // ($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == '')) {
                // $scope.loading = false;
                // bsNotify.show({
                    // type: 'warning',
                    // title: "Peringatan",
                    // content: "Harap pilih kategori dan masukkan filter"
                // });
            // } else {
                // $scope.param = $scope.selectedFilter;
                // var inputfilter = $scope.textFilter;
                // var tempGrid = angular.copy($scope.tempFilter);
                // var objct = '{"' + $scope.param + '":"' + inputfilter + '"}'
                // $scope.gridStockView.data = $filter('filter')(tempGrid, JSON.parse(objct));
            // }
			
			$scope.param = $scope.selectedFilter;
            var inputfilter = $scope.textFilter;
            var tempGrid = angular.copy($scope.tempFilter);
            var objct = '{"' + $scope.param + '":"' + inputfilter + '"}';
            
			if (inputfilter == "" || inputfilter == null) 
			{
				$scope.gridStockView.data = tempGrid;
			}
			else 
			{
				$scope.gridStockView.data = $filter('filter')(tempGrid, JSON.parse(objct));
			}
			
            // $scope.gridStockView.data = $scope.tempFilter.filter(function(data) {
            //     //console.log(data, )
            //     switch($scope.param){
            //         case "FrameNo":  
            //         return (data.FrameNo == inputfilter)
            //         break;
            //         case "LastLocation":  
            //         return (data.LastLocation == inputfilter)
            //         break;
            //         case "StatusStockName":  
            //         return (data.StatusStockName == inputfilter)
            //         break;
            //         default:
            //         break;
            //     }
            // });
        }

        $scope.refreshFilter = function () {
            $scope.textFilter = '';
            $scope.selectedFilter = {};
            $scope.gridStockView.data = $scope.tempFilter;
        }

        $scope.closeModal = function(){
            angular.element('.ui.modal.InfoCabang').modal('hide');
            angular.element('.ui.modal.InfoHO').modal('hide');
        }
    });