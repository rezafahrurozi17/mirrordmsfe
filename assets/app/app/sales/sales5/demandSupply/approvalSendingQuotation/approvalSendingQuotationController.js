angular.module('app')
.controller('ApprovalSendingQuotationController', function($scope, $http, CurrentUser, ApprovalSendingQuotationFactory,$timeout,bsNotify) {
	$scope.ApprovalSendingQuotationMain=true;
	IfGotProblemWithModal();
	$scope.ApprovalSendingQuotationMultipleReject=false;
	$scope.ApprovalSendingQuotationSearchCriteriaStore="";
	$scope.ApprovalSendingQuotationSearchValueStore="";
	
	$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_PanelIcon = "+";
	$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "+";
	$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_PanelIcon = "+";
	$scope.ApprovalSendingQuotationSearchIsPressed=false;
	$scope.ApprovalSendingQuotationHideIncrement=false;
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalActionApprovalSendingQuotation').remove();
		  angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').remove();
		  angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalMulti').remove();
		  angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalIndividual').remove();
		});
	
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.optionApprovalSendingQuotationSearchCriteria = ApprovalSendingQuotationFactory.GetApprovalSendingQuotationSearchDropdownOptions();
        $scope.optionApprovalSendingQuotationSearchLimit = ApprovalSendingQuotationFactory.getDataIncrementLimit();
		//$scope.ApprovalSendingQuotationSearchTier=$scope.optionApprovalSendingQuotationSearchLimit[0];
		$scope.ApprovalSendingQuotationSearchLimit = $scope.optionApprovalSendingQuotationSearchLimit[0].SearchOptionId; 
		$scope.ApprovalSendingQuotationSearchCriteria = $scope.optionApprovalSendingQuotationSearchCriteria[0].SearchOptionId;
		//$scope.mApprovalSendingQuotation = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        ApprovalSendingQuotationFactory.getData().then(
			function(res){
				$scope.ApprovalSendingQuotationSearchIsPressed=false;
				$scope.ApprovalSendingQuotationGabungan = res.data.Result;
				if(res.data.Total<=5)
				{
					$scope.ApprovalSendingQuotationHideIncrement=true;
				}
				return res.data;
			},
			function(err){
				console.log("err=>",err);
			}
		);
		
		$scope.ApprovalSendingQuotationRefresh = function () {
			$scope.ApprovalSendingQuotationHideIncrement=false;
			ApprovalSendingQuotationFactory.getData().then(
				function(res){
					
					$scope.ApprovalSendingQuotationSearchIsPressed=false;
					$scope.ApprovalSendingQuotationGabungan = res.data.Result;
					$scope.ApprovalSendingQuotationSearchValue="";
					$scope.ResetSelection();
					if(res.data.Total<=5)
					{
						$scope.ApprovalSendingQuotationHideIncrement=true;
					}
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
		$scope.ApprovalSendingQuotationIncrementLimit = function () {
			//$scope.ApprovalSendingQuotationSearchTier++;
			//$scope.ApprovalSendingQuotationIncrementStart=($scope.ApprovalSendingQuotationSearchTier*$scope.ApprovalSendingQuotationSearchLimit)+1;
			$scope.ApprovalSendingQuotationHideIncrement=false;

			if($scope.ApprovalSendingQuotationSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalSendingQuotationGabungan.length)+1;
				ApprovalSendingQuotationFactory.getDataIncrement(masukin,$scope.ApprovalSendingQuotationSearchLimit).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalSendingQuotationGabungan.push(res.data.Result[i]);
						}
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.ApprovalSendingQuotationSearchLimit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalSendingQuotationHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			if($scope.ApprovalSendingQuotationSearchIsPressed==true)
			{
				//console.log("ulala",$scope.ApprovalSendingQuotationGabungan.length)
				var masukin=angular.copy($scope.ApprovalSendingQuotationGabungan.length)+1;
				ApprovalSendingQuotationFactory.getDataSearchApprovalSendingQuotationIncrement(masukin,$scope.ApprovalSendingQuotationSearchLimit,$scope.ApprovalSendingQuotationSearchCriteriaStore,$scope.ApprovalSendingQuotationSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalSendingQuotationGabungan.push(res.data.Result[i]);
						}
						// $scope.ApprovalSendingQuotationSearchCriteriaStore=angular.copy($scope.ApprovalSendingQuotationSearchCriteria);
						// $scope.ApprovalSendingQuotationSearchValueStore=angular.copy($scope.ApprovalSendingQuotationSearchValue);
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.ApprovalSendingQuotationSearchLimit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalSendingQuotationHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			
			
			

		};
		
		$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_Clicked = function () {
			$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman = !$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman;
			if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman == true) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman == false) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_PanelIcon = "+";
			}

		};
		
		$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_Clicked = function () {
			$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli = !$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli;
			if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli == true) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli == false) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "+";
			}

		};
		
		$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_Clicked = function () {
			$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga = !$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga;
			if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga == true) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga == false) {
				$scope.Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_PanelIcon = "+";
			}

		};
		
		
		
		$scope.FromListSetujuApprovalSendingQuotation = function () {
			
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalMulti').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalMulti').not(':first').remove();
			
			// ApprovalSendingQuotationFactory.SetujuApprovalSendingQuotationDariList($scope.selectionApprovalSendingQuotation).then(function () {
						// ApprovalSendingQuotationFactory.getData().then(
							// function(res){
							
								// bsNotify.show(
									// {
										// title: "Message",
										// content: "Data Di Approve",
										// type: 'warning'
									// }
								// );
							
								// $scope.ApprovalSendingQuotationSearchIsPressed=false;
								// $scope.ApprovalSendingQuotationGabungan = res.data.Result;
								// $scope.loading=false;
								// $scope.BatalApprovalSendingQuotation();
								// $scope.ResetSelection();
								// $scope.ApprovalSendingQuotationMultipleReject=false;
								// return res.data;
							// },
							// function(err){
								// console.log("err=>",err);
							// }
						// );
					// });
		};
		
		$scope.FromListTidakSetujuApprovalSendingQuotation = function () {
			$scope.ApprovalSendingQuotationMultipleReject=true;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').not(':first').remove();
		};
        
		$scope.TidakSetujuApprovalSendingQuotation = function () {
			
			if($scope.mApprovalSendingQuotation.StatusApprovalName=="Diajukan")
			{
				$scope.ApprovalSendingQuotationMultipleReject=false;
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').not(':first').remove();
			}
			else
			{
					bsNotify.show(
									{
										title: "Peringatan",
										content: "data sudah disetujui/reject",
										type: 'warning'
									}
								);
					angular.element('.ui.modal.ModalActionApprovalSendingQuotation').modal('hide');
					$scope.BatalAlasanApprovalSendingQuotation();
					$scope.BatalApprovalSendingQuotation();
			}
			
		};
		
		$scope.ActionApprovalSendingQuotation = function (SelectedData) {
			$scope.ApprovalSendingQuotationMultipleReject=false;
			$scope.mApprovalSendingQuotation=angular.copy(SelectedData);
			angular.element('.ui.modal.ModalActionApprovalSendingQuotation').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalActionApprovalSendingQuotation').not(':first').remove();
		}
		
		$scope.BatalActionApprovalSendingQuotation = function () {
			angular.element('.ui.modal.ModalActionApprovalSendingQuotation').modal('hide');
			$scope.mApprovalSendingQuotation={};
		}
		
		$scope.ModalApprovalSendingQuotationActionApprovalMultiBatal = function () {
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalMulti').modal('hide');
			//$scope.mApprovalSendingQuotation={};
		}
		
		$scope.ModalApprovalSendingQuotationActionApprovalIndividualBatal = function () {
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalIndividual').modal('hide');
			//$scope.mApprovalSendingQuotation={};
		}
		
		$scope.ModalApprovalSendingQuotationActionApprovalMultiApprove = function () {
			ApprovalSendingQuotationFactory.SetujuApprovalSendingQuotationDariList($scope.selectionApprovalSendingQuotation).then(function () {
						ApprovalSendingQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Approve",
										type: 'success'
									}
								);
								$scope.ModalApprovalSendingQuotationActionApprovalMultiBatal();
								$scope.ApprovalSendingQuotationSearchIsPressed=false;
								$scope.ApprovalSendingQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalApprovalSendingQuotation();
								$scope.ResetSelection();
								$scope.ApprovalSendingQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSendingQuotationHideIncrement=true;
								}
								$scope.ApprovalSendingQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
								bsNotify.show(
									{
										title: "Gagal",
										content: "Data gagal di approve.",
										type: 'danger'
									}
								);
							}
						);
					},
							function(err){
								console.log("err=>",err);
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							});
		}
		
		$scope.ModalApprovalSendingQuotationActionApprovalIndividualApprove = function () {
			if($scope.mApprovalSendingQuotation.StatusApprovalName=="Diajukan")
			{
				ApprovalSendingQuotationFactory.SetujuApprovalSendingQuotation($scope.mApprovalSendingQuotation).then(function () {
					ApprovalSendingQuotationFactory.getData().then(
						function(res){
						
							bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Approve",
										type: 'success'
									}
								);
							$scope.ModalApprovalSendingQuotationActionApprovalIndividualBatal();
							$scope.ApprovalSendingQuotationSearchIsPressed=false;
							$scope.ApprovalSendingQuotationGabungan = res.data.Result;
							$scope.loading=false;
							$scope.BatalApprovalSendingQuotation();
							$scope.ResetSelection();
							$scope.ApprovalSendingQuotationHideIncrement=false;
							if(res.data.Total<=5)
							{
								$scope.ApprovalSendingQuotationHideIncrement=true;
							}
							$scope.ApprovalSendingQuotationMultipleReject=false;
							
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				},
							function(err){
								console.log("err=>",err);
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							});
			}
			else
			{
				bsNotify.show(
									{
										title: "Peringatan",
										content: "data sudah disetujui/reject",
										type: 'warning'
									}
								);
				$scope.BatalAlasanApprovalSendingQuotation();
			}
		}
		
		$scope.SelectApprovalSendingQuotation = function () {
			
			$scope.ApprovalSendingQuotationMain=false;
			$scope.ApprovalSendingQuotationDetail=true;
			angular.element('.ui.modal.ModalActionApprovalSendingQuotation').modal('hide');
			
			ApprovalSendingQuotationFactory.getDaRincian($scope.mApprovalSendingQuotation.QuotationId).then(
				function(res){
					$scope.selected_data_ApprovalSendingQuotation = res.data.Result[0];
					return res.data;
				}
			);
		}
		
		$scope.BatalApprovalSendingQuotation = function () {
			$scope.mApprovalSendingQuotation ={};
			$scope.ApprovalSendingQuotationMain=true;
			$scope.ApprovalSendingQuotationDetail=false;
			angular.element('.ui.modal.ModalActionApprovalSendingQuotation').modal('hide');
		}
		
		$scope.BatalAlasanApprovalSendingQuotation = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSendingQuotation').modal('hide');
			//$scope.mApprovalSendingQuotation={};
		}
		
		$scope.SetujuApprovalSendingQuotation = function () {
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalIndividual').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalApprovalSendingQuotationActionApprovalIndividual').not(':first').remove();
			
			// if($scope.mApprovalSendingQuotation.StatusApprovalName=="Diajukan")
			// {
				// ApprovalSendingQuotationFactory.SetujuApprovalSendingQuotation($scope.mApprovalSendingQuotation).then(function () {
					// ApprovalSendingQuotationFactory.getData().then(
						// function(res){
						
							// bsNotify.show(
									// {
										// title: "Message",
										// content: "Data Di Approve",
										// type: 'warning'
									// }
								// );
						
							// $scope.ApprovalSendingQuotationSearchIsPressed=false;
							// $scope.ApprovalSendingQuotationGabungan = res.data.Result;
							// $scope.loading=false;
							// $scope.BatalApprovalSendingQuotation();
							// $scope.ResetSelection();
							// $scope.ApprovalSendingQuotationMultipleReject=false;
							
							// return res.data;
						// },
						// function(err){
							// console.log("err=>",err);
						// }
					// );
				// });
			// }
			// else
			// {
				// bsNotify.show(
									// {
										// title: "Message",
										// content: "data sudah disetujui/reject",
										// type: 'warning'
									// }
								// );
				// $scope.BatalAlasanApprovalSendingQuotation();
			// }
		
			

		};
		
		$scope.SubmitAlasanApprovalSendingQuotation = function () {
			if($scope.ApprovalSendingQuotationMultipleReject==false)
			{
				ApprovalSendingQuotationFactory.TidakSetujuApprovalSendingQuotation($scope.mApprovalSendingQuotation).then(function () {
						ApprovalSendingQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Reject",
										type: 'success'
									}
								);
							
								$scope.ApprovalSendingQuotationSearchIsPressed=false;
								$scope.ApprovalSendingQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalSendingQuotation();
								$scope.BatalApprovalSendingQuotation();
								$scope.ResetSelection();
								$scope.ApprovalSendingQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSendingQuotationHideIncrement=true;
								}
								$scope.ApprovalSendingQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
			else if($scope.ApprovalSendingQuotationMultipleReject==true)
			{
				for(var i = 0; i < $scope.selectionApprovalSendingQuotation.length; ++i)
				{
					//var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
					$scope.selectionApprovalSendingQuotation[i]['RejectReason']=angular.copy($scope.mApprovalSendingQuotation.RejectReason);
				}
				ApprovalSendingQuotationFactory.TidakSetujuApprovalSendingQuotationDariList($scope.selectionApprovalSendingQuotation).then(function () {
						ApprovalSendingQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Reject",
										type: 'success'
									}
								);
							
								$scope.ApprovalSendingQuotationSearchIsPressed=false;
								$scope.ApprovalSendingQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalSendingQuotation();
								$scope.BatalApprovalSendingQuotation();
								$scope.ResetSelection();
								$scope.ApprovalSendingQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSendingQuotationHideIncrement=true;
								}
								$scope.ApprovalSendingQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
		
			
		}

		$scope.ResetSelection = function () {
			$scope.selectionApprovalSendingQuotation=[{ OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationSendingId: null}];
			$scope.selectionApprovalSendingQuotation.splice(0, 1);
		}
		
		$scope.selectionApprovalSendingQuotation=[{ OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationSendingId: null}];
		$scope.selectionApprovalSendingQuotation.splice(0, 1);
		
		$scope.toggleSelection = function toggleSelection(SelectedAprovalSendingQuotationFromList) {
			var idx = $scope.selectionApprovalSendingQuotation.indexOf(SelectedAprovalSendingQuotationFromList);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionApprovalSendingQuotation.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionApprovalSendingQuotation.push(SelectedAprovalSendingQuotationFromList);
			}

			$scope.SelectionAman_ApprovalSendingQuotation=true;
			for(var i = 0; i < $scope.selectionApprovalSendingQuotation.length; ++i)
			{
				if($scope.selectionApprovalSendingQuotation[i].StatusApprovalName!="Diajukan")
				{
					$scope.SelectionAman_ApprovalSendingQuotation=false;
				}
			}
		  };
		  
		$scope.SearchApprovalSending = function () {
			$scope.ApprovalSendingQuotationHideIncrement=false;
			try
			{
				if($scope.ApprovalSendingQuotationSearchValue!="" && (typeof $scope.ApprovalSendingQuotationSearchValue!="undefined"))
				{
					ApprovalSendingQuotationFactory.getDataSearchApprovalSendingQuotation($scope.ApprovalSendingQuotationSearchLimit,$scope.ApprovalSendingQuotationSearchCriteria,$scope.ApprovalSendingQuotationSearchValue).then(function (res) {
						$scope.ApprovalSendingQuotationSearchIsPressed=true;
						$scope.ApprovalSendingQuotationGabungan = res.data.Result;
						
						if($scope.ApprovalSendingQuotationSearchLimit>=res.data.Total)
						{
							$scope.ApprovalSendingQuotationHideIncrement=true;
						}
						
						$scope.ResetSelection();
						$scope.ApprovalSendingQuotationSearchCriteriaStore=angular.copy($scope.ApprovalSendingQuotationSearchCriteria);
						$scope.ApprovalSendingQuotationSearchValueStore=angular.copy($scope.ApprovalSendingQuotationSearchValue);
					});
				}
				else
				{
					ApprovalSendingQuotationFactory.getData().then(function (res) {
						$scope.ApprovalSendingQuotationSearchIsPressed=false;
						$scope.ApprovalSendingQuotationGabungan = res.data.Result;
						
						if(res.data.Total<=5)
						{
							$scope.ApprovalSendingQuotationHideIncrement=true;
						}
						
						$scope.ResetSelection();
					});
				}
			}
			catch(e)
			{
				ApprovalSendingQuotationFactory.getData().then(function (res) {
					$scope.ApprovalSendingQuotationSearchIsPressed=false;
					$scope.ApprovalSendingQuotationGabungan = res.data.Result;
					
					if(res.data.Total<=5)
					{
						$scope.ApprovalSendingQuotationHideIncrement=true;
					}
					
					$scope.ResetSelection();
				});
			}
			

        }  

});
