angular.module('app')
  .factory('ApprovalSendingQuotationFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		//var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending');
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending?start=1&limit=5');
		return res;
        
      },
	  
	  getDaRincian: function(QuotationId) {
		var res=$http.get('/api/sales/DemandSupplyQuotation/?QuotationId='+QuotationId);
		return res;
        
      },
	  
	  getDataIncrement: function(Start,Limit) {
		//var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending');
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending?start='+Start+'&limit='+Limit);
		return res;
        
      },
	  
	  getDataSearchApprovalSendingQuotationIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  getDataIncrementLimit: function () {
        
        var da_json = [
          { SearchOptionId: 5, SearchOptionName: "5" },
		  { SearchOptionId: 10, SearchOptionName: "10" },
		  { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetApprovalSendingQuotationSearchDropdownOptions: function () {
        //{ SearchOptionId: "ApprovalCategoryName", SearchOptionName: "Kategori Nama Approval" },
        var da_json = [
          { SearchOptionId: "QuotationNo", SearchOptionName: "No. Quotation" },
		  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Model Tipe" },
		  { SearchOptionId: "ColorName", SearchOptionName: "Warna" },
          { SearchOptionId: "QuotationStatusName", SearchOptionName: "Status Quotation" }
        ];

        var res=da_json

        return res;
      },
	  
	  // getDataSearchApprovalSendingQuotation: function(SearchBy,SearchValue) {
		// var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending/?FilterData='+SearchBy+'|'+SearchValue);
		// return res;
        
      // },
	  
	  getDataSearchApprovalSendingQuotation: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationSending/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  SetujuApprovalSendingQuotation: function(ApprovalSendingQuotation){
		return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Approve', [ApprovalSendingQuotation]);									
      },
	  
	  SetujuApprovalSendingQuotationDariList: function(ApprovalSendingQuotation){
		return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Approve', ApprovalSendingQuotation);									
      },
	  
	  TidakSetujuApprovalSendingQuotation: function(ApprovalSendingQuotation){
        // return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Reject', [{
                                            // OutletId: ApprovalSendingQuotation.OutletId,
											// ApprovalDECId: ApprovalSendingQuotation.ApprovalDECId,
											// DECId: ApprovalSendingQuotation.DECId,
                                            // RejectReason: ApprovalSendingQuotation.RejectReason}]);
		return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Reject', [ApprovalSendingQuotation]);									
      },
	  TidakSetujuApprovalSendingQuotationDariList: function(ApprovalSendingQuotation){
        // return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Reject', [{
                                            // OutletId: ApprovalSendingQuotation.OutletId,
											// ApprovalDECId: ApprovalSendingQuotation.ApprovalDECId,
											// DECId: ApprovalSendingQuotation.DECId,
                                            // RejectReason: ApprovalSendingQuotation.RejectReason}]);
		return $http.put('/api/sales/DemandSupplyApprovalQuotationSending/Reject', ApprovalSendingQuotation);									
      },
	  
      create: function(ApprovalSendingQuotation) {
        return $http.post('/api/sales/DemandSupplyApprovalQuotationSending', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalSendingQuotation.SalesProgramName}]);
      },
      update: function(ApprovalSendingQuotation){
        return $http.put('/api/sales/DemandSupplyApprovalQuotationSending', [{
                                            SalesProgramId: ApprovalSendingQuotation.SalesProgramId,
                                            SalesProgramName: ApprovalSendingQuotation.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/DemandSupplyApprovalQuotationSending',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd