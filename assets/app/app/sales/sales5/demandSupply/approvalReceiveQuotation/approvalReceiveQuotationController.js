angular.module('app')
.controller('ApprovalReceiveQuotationController', function($scope, $http, CurrentUser, ApprovalReceiveQuotationFactory,$timeout,bsNotify) {
	$scope.ApprovalReceiveQuotationMain=true;
	//IfGotProblemWithModal();
	$scope.ApprovalReceiveQuotationMultipleReject=false;
	$scope.ApprovalReceiveQuotationSearchCriteriaStore="";
	$scope.ApprovalReceiveQuotationSearchValueStore="";
	
	$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_PanelIcon = "+";
	$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "+";
	$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_PanelIcon = "+";
	$scope.ApprovalReceiveQuotationSearchIsPressed=false;
	$scope.ApprovalReceiveQuotationHideIncrement=false;
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').remove();
		  angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').remove();
		  angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalMulti').remove();
		  angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalIndividual').remove();
		});
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		console.log("iblis",$scope.user);
		$scope.optionApprovalReceiveQuotationSearchCriteria = ApprovalReceiveQuotationFactory.GetApprovalReceiveQuotationSearchDropdownOptions();
        $scope.optionApprovalReceiveQuotationSearchLimit = ApprovalReceiveQuotationFactory.getDataIncrementLimit();
		//$scope.ApprovalReceiveQuotationSearchTier=$scope.optionApprovalReceiveQuotationSearchLimit[0];
		$scope.ApprovalReceiveQuotationSearchLimit = $scope.optionApprovalReceiveQuotationSearchLimit[0].SearchOptionId; 
		$scope.ApprovalReceiveQuotationSearchCriteria = $scope.optionApprovalReceiveQuotationSearchCriteria[0].SearchOptionId;
		//$scope.mApprovalReceiveQuotation = null; //Model
        $scope.xRole = { selected: [] };
		$scope.disabledModalApprovalReceiveQuotationActionApprovalMultiApprove = false;
		$scope.disabledModalApprovalReceiveQuotationActionApprovalIndividualApprove = false;
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        ApprovalReceiveQuotationFactory.getData().then(
			function(res){
				$scope.ApprovalReceiveQuotationSearchIsPressed=false;
				$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
				if(res.data.Total<=5)
				{
					$scope.ApprovalReceiveQuotationHideIncrement=true;
				}
				return res.data;
			},
			function(err){
				console.log("err=>",err);
			}
		);
		
		$scope.ApprovalReceiveQuotationRefresh = function () {
			$scope.ApprovalReceiveQuotationHideIncrement=false;
			ApprovalReceiveQuotationFactory.getData().then(
				function(res){
					
					$scope.ApprovalReceiveQuotationSearchIsPressed=false;
					$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
					$scope.ApprovalReceiveQuotationSearchValue="";
					$scope.ResetSelection();
					if(res.data.Total<=5)
					{
						$scope.ApprovalReceiveQuotationHideIncrement=true;
					}
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
		$scope.ApprovalReceiveQuotationIncrementLimit = function () {
			//$scope.ApprovalReceiveQuotationSearchTier++;
			//$scope.ApprovalReceiveQuotationIncrementStart=($scope.ApprovalReceiveQuotationSearchTier*$scope.ApprovalReceiveQuotationSearchLimit)+1;
			$scope.ApprovalReceiveQuotationHideIncrement=false;

			if($scope.ApprovalReceiveQuotationSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalReceiveQuotationGabungan.length)+1;
				ApprovalReceiveQuotationFactory.getDataIncrement(masukin,$scope.ApprovalReceiveQuotationSearchLimit).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalReceiveQuotationGabungan.push(res.data.Result[i]);
						}
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.ApprovalReceiveQuotationSearchLimit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalReceiveQuotationHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			if($scope.ApprovalReceiveQuotationSearchIsPressed==true)
			{
				//console.log("ulala",$scope.ApprovalReceiveQuotationGabungan.length)
				var masukin=angular.copy($scope.ApprovalReceiveQuotationGabungan.length)+1;
				ApprovalReceiveQuotationFactory.getDataSearchApprovalReceiveQuotationIncrement(masukin,$scope.ApprovalReceiveQuotationSearchLimit,$scope.ApprovalReceiveQuotationSearchCriteriaStore,$scope.ApprovalReceiveQuotationSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalReceiveQuotationGabungan.push(res.data.Result[i]);
						}
						// $scope.ApprovalReceiveQuotationSearchCriteriaStore=angular.copy($scope.ApprovalReceiveQuotationSearchCriteria);
						// $scope.ApprovalReceiveQuotationSearchValueStore=angular.copy($scope.ApprovalReceiveQuotationSearchValue);
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.ApprovalReceiveQuotationSearchLimit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalReceiveQuotationHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			
			
			

		};
		
		$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_Clicked = function () {
			$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman = !$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman;
			if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman == true) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman == false) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_PanelIcon = "+";
			}

		};
		
		$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_Clicked = function () {
			$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli = !$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli;
			if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli == true) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli == false) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon = "+";
			}

		};
		
		$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_Clicked = function () {
			$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga = !$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga;
			if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga == true) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga == false) {
				$scope.Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_PanelIcon = "+";
			}

		};
		
		$scope.FromListSetujuApprovalReceiveQuotation = function () {
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalMulti').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalMulti').not(':first').remove();
			$scope.selectionApprovalReceiveQuotationTabel=angular.copy($scope.selectionApprovalReceiveQuotation);
			
			for(var i = 0; i < $scope.selectionApprovalReceiveQuotationTabel.length; ++i)
			{
				var DaKirim={ DestinationBranchCode:$scope.selectionApprovalReceiveQuotationTabel[i].OutletCode, FrameNumber:$scope.selectionApprovalReceiveQuotationTabel[i].FrameNo};
     
				ApprovalReceiveQuotationFactory.GetDaTamExpedition(DaKirim).then(function (res) {
					$scope.selectionApprovalReceiveQuotationTabel[i].TAMExpedition=res.data.price;
				});
				
			}
		};
		
		$scope.FromListTidakSetujuApprovalReceiveQuotation = function () {
			$scope.ApprovalReceiveQuotationMultipleReject=true;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').not(':first').remove();
		};
        
		$scope.TidakSetujuApprovalReceiveQuotation = function () {
			if($scope.mApprovalReceiveQuotation.StatusApprovalName=="Diajukan")
			{
				$scope.ApprovalReceiveQuotationMultipleReject=false;
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').not(':first').remove();
			}
			else
			{
				bsNotify.show(
									{
										title: "Peringatan",
										content: "data sudah disetujui/reject",
										type: 'warning'
									}
								);
				angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').modal('hide');
				$scope.BatalAlasanApprovalReceiveQuotation();
				$scope.BatalApprovalReceiveQuotation();
			}
			
			
		};
		
		$scope.ActionApprovalReceiveQuotation = function (SelectedData) {
			$scope.ApprovalReceiveQuotationMultipleReject=false;
			$scope.mApprovalReceiveQuotation=angular.copy(SelectedData);
			angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').not(':first').remove();
		}
		
		$scope.BatalActionApprovalReceiveQuotation = function () {
			angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').modal('hide');
			$scope.mApprovalReceiveQuotation={};
		}
		
		$scope.ModalApprovalReceiveQuotationActionApprovalMultiBatal = function () {
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalMulti').modal('hide');
			//$scope.mApprovalReceiveQuotation={};
		}
		
		$scope.ModalApprovalReceiveQuotationActionApprovalIndividualBatal = function () {
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalIndividual').modal('hide');
			//$scope.mApprovalReceiveQuotation={};
		}
		
		$scope.ModalApprovalReceiveQuotationActionApprovalMultiApprove = function () {
			$scope.disabledModalApprovalReceiveQuotationActionApprovalMultiApprove = true;
			ApprovalReceiveQuotationFactory.SetujuApprovalReceiveQuotationDariList($scope.selectionApprovalReceiveQuotation).then(function () {
						ApprovalReceiveQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Approve",
										type: 'success'
									}
								);
								$scope.disabledModalApprovalReceiveQuotationActionApprovalMultiApprove = false; //penjagaan1
								$scope.ModalApprovalReceiveQuotationActionApprovalMultiBatal();
								$scope.ApprovalReceiveQuotationSearchIsPressed=false;
								$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalApprovalReceiveQuotation();
								$scope.ResetSelection();
								$scope.ApprovalReceiveQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalReceiveQuotationHideIncrement=true;
								}
								$scope.ApprovalReceiveQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					},
							function(err){
								console.log("err=>",err);
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledModalApprovalReceiveQuotationActionApprovalMultiApprove = false; //penjagaan1
							});					
		}
		
		$scope.ModalApprovalReceiveQuotationActionApprovalIndividualApprove = function () {
			$scope.disabledModalApprovalReceiveQuotationActionApprovalIndividualApprove = true;
			if($scope.mApprovalReceiveQuotation.StatusApprovalName=="Diajukan")
			{
				ApprovalReceiveQuotationFactory.SetujuApprovalReceiveQuotation($scope.mApprovalReceiveQuotation).then(function () {
					ApprovalReceiveQuotationFactory.getData().then(
						function(res){
						
							bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Approve",
										type: 'success'
									}
								);
							$scope.disabledModalApprovalReceiveQuotationActionApprovalIndividualApprove = false;
							$scope.ModalApprovalReceiveQuotationActionApprovalIndividualBatal();
							$scope.ApprovalReceiveQuotationSearchIsPressed=false;
							$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
							$scope.loading=false;
							$scope.BatalApprovalReceiveQuotation();
							$scope.ResetSelection();
							$scope.ApprovalReceiveQuotationHideIncrement=false;
							if(res.data.Total<=5)
							{
								$scope.ApprovalReceiveQuotationHideIncrement=true;
							}
							$scope.ApprovalReceiveQuotationMultipleReject=false;
							
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				},
							function(err){
								console.log("err=>",err);
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							});
							$scope.disabledModalApprovalReceiveQuotationActionApprovalIndividualApprove = false;
			}
			else
			{
				bsNotify.show(
									{
										title: "Peringatan",
										content: "data sudah disetujui/reject",
										type: 'warning'
									}
								);
								$scope.disabledModalApprovalReceiveQuotationActionApprovalIndividualApprove = false;
				$scope.BatalAlasanApprovalReceiveQuotation();
			}
		}
		
		$scope.SelectApprovalReceiveQuotation = function () {
			
			$scope.ApprovalReceiveQuotationMain=false;
			$scope.ApprovalReceiveQuotationDetail=true;
			angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').modal('hide');
			
		}
		
		$scope.BatalApprovalReceiveQuotation = function () {
			$scope.mApprovalReceiveQuotation ={};
			$scope.ApprovalReceiveQuotationMain=true;
			$scope.ApprovalReceiveQuotationDetail=false;
			angular.element('.ui.modal.ModalActionApprovalReceiveQuotation').modal('hide');
		}
		
		$scope.BatalAlasanApprovalReceiveQuotation = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalReceiveQuotation').modal('hide');
			//$scope.mApprovalReceiveQuotation={};
		}
		
		$scope.SetujuApprovalReceiveQuotation = function () {
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalIndividual').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalApprovalReceiveQuotationActionApprovalIndividual').not(':first').remove();
			
		};
		
		$scope.SubmitAlasanApprovalReceiveQuotation = function () {
			if($scope.ApprovalReceiveQuotationMultipleReject==false)
			{
				ApprovalReceiveQuotationFactory.TidakSetujuApprovalReceiveQuotation($scope.mApprovalReceiveQuotation).then(function () {
						ApprovalReceiveQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Reject",
										type: 'success'
									}
								);
							
								$scope.ApprovalReceiveQuotationSearchIsPressed=false;
								$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalReceiveQuotation();
								$scope.BatalApprovalReceiveQuotation();
								$scope.ResetSelection();
								$scope.ApprovalReceiveQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalReceiveQuotationHideIncrement=true;
								}
								$scope.ApprovalReceiveQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
			else if($scope.ApprovalReceiveQuotationMultipleReject==true)
			{
				for(var i = 0; i < $scope.selectionApprovalReceiveQuotation.length; ++i)
				{
					//var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
					$scope.selectionApprovalReceiveQuotation[i]['RejectReason']=angular.copy($scope.mApprovalReceiveQuotation.RejectReason);
				}
				ApprovalReceiveQuotationFactory.TidakSetujuApprovalReceiveQuotationDariList($scope.selectionApprovalReceiveQuotation).then(function () {
						ApprovalReceiveQuotationFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Sukses",
										content: "Data Di Reject",
										type: 'success'
									}
								);
							
								$scope.ApprovalReceiveQuotationSearchIsPressed=false;
								$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalReceiveQuotation();
								$scope.BatalApprovalReceiveQuotation();
								$scope.ResetSelection();
								$scope.ApprovalReceiveQuotationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalReceiveQuotationHideIncrement=true;
								}
								$scope.ApprovalReceiveQuotationMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
		
			
		}

		$scope.ResetSelection = function () {
			$scope.selectionApprovalReceiveQuotation=[{ OutletId: null,
											QuotationReceiveId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationReceiveId: null}];
			$scope.selectionApprovalReceiveQuotation.splice(0, 1);
		}
		
		$scope.selectionApprovalReceiveQuotation=[{ OutletId: null,
											QuotationReceiveId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationReceiveId: null}];
		$scope.selectionApprovalReceiveQuotation.splice(0, 1);
		
		$scope.toggleSelection = function toggleSelection(SelectedAprovalReceiveQuotationFromList) {
			var idx = $scope.selectionApprovalReceiveQuotation.indexOf(SelectedAprovalReceiveQuotationFromList);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionApprovalReceiveQuotation.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionApprovalReceiveQuotation.push(SelectedAprovalReceiveQuotationFromList);
			}

			$scope.SelectionAman_ApprovalReceiveQuotation=true;
			for(var i = 0; i < $scope.selectionApprovalReceiveQuotation.length; ++i)
			{
				if($scope.selectionApprovalReceiveQuotation[i].StatusApprovalName!="Diajukan")
				{
					$scope.SelectionAman_ApprovalReceiveQuotation=false;
				}
			}
		  };
		  
		$scope.SearchApprovalReceive = function () {
			$scope.ApprovalReceiveQuotationHideIncrement=false;
			try
			{
				if($scope.ApprovalReceiveQuotationSearchValue!="" && (typeof $scope.ApprovalReceiveQuotationSearchValue!="undefined"))
				{
					ApprovalReceiveQuotationFactory.getDataSearchApprovalReceiveQuotation($scope.ApprovalReceiveQuotationSearchLimit,$scope.ApprovalReceiveQuotationSearchCriteria,$scope.ApprovalReceiveQuotationSearchValue).then(function (res) {
						$scope.ApprovalReceiveQuotationSearchIsPressed=true;
						$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
						
						if($scope.ApprovalReceiveQuotationSearchLimit>=res.data.Total)
						{
							$scope.ApprovalReceiveQuotationHideIncrement=true;
						}
						
						$scope.ResetSelection();
						$scope.ApprovalReceiveQuotationSearchCriteriaStore=angular.copy($scope.ApprovalReceiveQuotationSearchCriteria);
						$scope.ApprovalReceiveQuotationSearchValueStore=angular.copy($scope.ApprovalReceiveQuotationSearchValue);
					});
				}
				else
				{
					ApprovalReceiveQuotationFactory.getData().then(function (res) {
						$scope.ApprovalReceiveQuotationSearchIsPressed=false;
						$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
						
						if(res.data.Total<=5)
						{
							$scope.ApprovalReceiveQuotationHideIncrement=true;
						}
						
						$scope.ResetSelection();
					});
				}
			}
			catch(e)
			{
				ApprovalReceiveQuotationFactory.getData().then(function (res) {
					$scope.ApprovalReceiveQuotationSearchIsPressed=false;
					$scope.ApprovalReceiveQuotationGabungan = res.data.Result;
					
					if(res.data.Total<=5)
					{
						$scope.ApprovalReceiveQuotationHideIncrement=true;
					}
					
					$scope.ResetSelection();
				});
			}
			

        }  

});
