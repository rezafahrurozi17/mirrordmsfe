angular.module('app')
  .factory('ApprovalReceiveQuotationFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		//var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive');
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive?start=1&limit=5');
		return res;
        
      },
	  
	  getDaRincian: function(QuotationReceiveId) {
		var res=$http.get('/api/sales/SAHSPKDetail/?QuotationReceiveId='+QuotationReceiveId);
		return res;
        
      },
	  
	  getDataIncrement: function(Start,Limit) {
		//var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive');
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive?start='+Start+'&limit='+Limit);
		return res;
        
      },
	  
	  getDataSearchApprovalReceiveQuotationIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  getDataIncrementLimit: function () {
        
        var da_json = [
          { SearchOptionId: 5, SearchOptionName: "5" },
		  { SearchOptionId: 10, SearchOptionName: "10" },
		  { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetApprovalReceiveQuotationSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "QuotationNo", SearchOptionName: "No. quotation" },
		  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Model tipe" },
		  { SearchOptionId: "ColorName", SearchOptionName: "warna" },
		  { SearchOptionId: "ApprovalCategoryName", SearchOptionName: "Kategori Nama Approval" },
          { SearchOptionId: "QuotationStatusName", SearchOptionName: "Status quotation" }
        ];

        var res=da_json

        return res;
      },
	  
	  // getDataSearchApprovalReceiveQuotation: function(SearchBy,SearchValue) {
		// var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive/?FilterData='+SearchBy+'|'+SearchValue);
		// return res;
        
      // },
	  
	  getDataSearchApprovalReceiveQuotation: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/DemandSupplyApprovalQuotationReceive/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  SetujuApprovalReceiveQuotation: function(ApprovalReceiveQuotation){
		return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Approve', [ApprovalReceiveQuotation]);									
      },
	  
	  SetujuApprovalReceiveQuotationDariList: function(ApprovalReceiveQuotation){
		return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Approve', ApprovalReceiveQuotation);									
      },
	  
	  TidakSetujuApprovalReceiveQuotation: function(ApprovalReceiveQuotation){
        // return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Reject', [{
                                            // OutletId: ApprovalReceiveQuotation.OutletId,
											// ApprovalDECId: ApprovalReceiveQuotation.ApprovalDECId,
											// DECId: ApprovalReceiveQuotation.DECId,
                                            // RejectReason: ApprovalReceiveQuotation.RejectReason}]);
		return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Reject', [ApprovalReceiveQuotation]);									
      },
	  TidakSetujuApprovalReceiveQuotationDariList: function(ApprovalReceiveQuotation){
        // return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Reject', [{
                                            // OutletId: ApprovalReceiveQuotation.OutletId,
											// ApprovalDECId: ApprovalReceiveQuotation.ApprovalDECId,
											// DECId: ApprovalReceiveQuotation.DECId,
                                            // RejectReason: ApprovalReceiveQuotation.RejectReason}]);
		return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive/Reject', ApprovalReceiveQuotation);									
      },
	  
      create: function(ApprovalReceiveQuotation) {
        return $http.post('/api/sales/DemandSupplyApprovalQuotationReceive', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalReceiveQuotation.SalesProgramName}]);
      },
	  GetDaTamExpedition: function(ApprovalReceiveQuotation) {
        return $http.post('/api/sales/TLSCalculateSwapping', {
                                            DestinationBranchCode: ApprovalReceiveQuotation.DestinationBranchCode,
                                            FrameNumber: ApprovalReceiveQuotation.FrameNumber});
      },
      update: function(ApprovalReceiveQuotation){
        return $http.put('/api/sales/DemandSupplyApprovalQuotationReceive', [{
                                            SalesProgramId: ApprovalReceiveQuotation.SalesProgramId,
                                            SalesProgramName: ApprovalReceiveQuotation.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/DemandSupplyApprovalQuotationReceive',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd