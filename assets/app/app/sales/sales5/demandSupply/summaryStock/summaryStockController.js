angular.module('app')
    .controller('SummaryStockController', function($scope,$filter, $http, $httpParamSerializer, CurrentUser, SummaryStockFactory, MaintainSOFactory, MaintainBillingFactory, ModelFactory, TipeFactory, ComboBoxFactory, $timeout, uiGridConstants, uiGridGroupingConstants) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.SummaryStock = true;
        $scope.ShowDetail = false;
        //$scope.show_modal = { show: false };

        $scope.$on('$viewContentLoaded', function() {
            //$scope.loading=true;
            //$scope.gridData=[];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.SummaryStockDisableFilterCabang=false;
		$scope.user = CurrentUser.user();
        $scope.filter = { OutletId: $scope.user.OutletId, ProvinceId: null, CityRegencyId: null, VehicleModelId: null, VehicleTypeId: null, ColorId: null };
        $scope.xRole = { selected: [] };

        // {
        //     name: 'Model',
        //     field: 'VehicleModelName',
        //     width: '13%',
        //     pinnedLeft: true,
        //     grouping: { groupPriority: 0 },
        //     sort: { priority: 0, direction: 'asc' },
        //     cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
        // },
        // { name: 'Type', field: 'VehicleTypeDescription', width: '13%', cellTemplate:'<div style="position: relative;top: 20%;"><label>{{row.entity.VehicleTypeDescription}}{{row.entity.KatashikiCode}}{{row.entity.SuffixCode}}</label></div>' },
        // { name: 'Color', field: 'ColorName', width: '13%' },
        // { name: 'RRN', field: 'RRNNo', width: '10%' },
        // { name: 'No Rangka', field: 'FrameNo', width: '13%' },
        // { name: 'PLOD', field: 'PLOD', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\'' },
        // { name: 'DO TAM', field: 'DOTAMDate', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\'' },
        // { name: 'Aging', field: 'Aging', width: '10%' },
        // { name: 'Category', field: 'StatusUnitName', width: '12%' },
        // { name: 'status', field: 'StatusStockName', width: '12%' },

        $scope.textFilter = '';
		$scope.filterData = {
			defaultFilter: [
				{ name: 'Model', value: 'VehicleModelName' },
				{ name: 'Tipe', value: 'VehicleTypeDescription' },
				{ name: 'Warna', value: 'ColorName' },
				{ name: 'RRN', value: 'RRNNo' },
                { name: 'No. Rangka', value: 'FrameNo' },
				{ name: 'Aging', value: 'Aging' },
				{ name: 'Kategori', value: 'StatusUnitName' }
			],
			advancedFilter: [
				// { name: 'Model', value: 'VehicleModelName', typeMandatory: 0 },
				// { name: 'Tipe', value: 'VehicleTypeName', typeMandatory: 0 },
				// { name: 'Tahun Produksi', value: 'VehicleModelYear', typeMandatory: 0 },
				// { name: 'Start Valid', value: 'DateFrom', typeMandatory: 0 },
				// { name: 'End Valid', value: 'DateTo', typeMandatory: 0 }
			]
		};
		
		// //tadi ini 2 ada di default filter
		// { name: 'PLOD', value: 'PLOD' },
				// { name: 'DO TAM', value: 'DOTAMDate' },
		
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.getData = function() {
            $scope.loading = true;
            console.log("Filter", $scope.filter);
            SummaryStockFactory.getData($scope.filter).then(
                function(res) {
                    $scope.loading = false;
					var SummaryStockNongol=res.data.Result;
					SummaryStockNongol = $filter('orderBy')(SummaryStockNongol, ['VehicleTypeDescription','PLOD'], false); 
                    //SummaryStockNongol = $filter('orderBy')(SummaryStockNongol, 'RRNNo', false);
                    $scope.TampungGrid = angular.copy(SummaryStockNongol);
					$scope.gridSummary.data = SummaryStockNongol;
                    return res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.filterSearch = function () {
			$scope.ControlLoadMore = 'Search';

			if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '')) {
				bsNotify.show({
					//size: 'big',
					type: 'warning',
					//timeout: 2000,
					title: "Peringatan",
					content: "Harap pilih kategori"
				});
			} else {
				var inputfilter = $scope.textFilter;
				var tempGrid = angular.copy($scope.gridSummary.data);
				var objct = '{"' + $scope.selectedFilter + '":"' + inputfilter + '"}'
				if (inputfilter == "" || inputfilter == null) {
					$scope.gridSummary.data = angular.copy($scope.TampungGrid);
				} else {
					tempGrid = angular.copy($scope.TampungGrid);
					$scope.gridSummary.data = $filter('filter')(tempGrid,  JSON.parse(objct));
				}
			
			}
		}

        MaintainSOFactory.getOutlet().then(
            function(res) {
                $scope.getOutlet = res.data.Result;
				//$scope.filter.OutletId=$scope.user.OutletId;
				
				if($scope.user.RoleName=="Admin Unit")
				{
					$scope.SummaryStockDisableFilterCabang=true;
				}
                return res.data;
            }
        );

        MaintainBillingFactory.getDataProvince().then(
            function(res) {
                $scope.getProvince = res.data.Result;
                return res.data;
            }
        );

        $scope.filterKabupaten = function(selected) {
            MaintainBillingFactory.getDataCity('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function(res) { $scope.getCity = res.data.Result; });
        };

        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        var listTipe = [];
        TipeFactory.getData().then(
            function(res) {
                listTipe = res.data.Result;
                return res.data;
            }
        );

        $scope.pilihType = function(selected) {
            console.log("CCC", selected);
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected).then(
                function(res) {
                    $scope.listWarna = res.data.Result;
                })
        };

        $scope.filterModel = function(Selectmodel) {
            console.log("listTipe", listTipe);
            $scope.getTipe = listTipe.filter(function(test) {
                return (test.VehicleModelId == Selectmodel.VehicleModelId);
            })
            var filterModel = $scope.listTipe.filter(function(obj) {
                return obj.VehicleTypeId == $scope.filter.VehicleTypeId;
            })
            if (filterModel.length == 0)
                $scope.filter.VehicleTypeId = null;
        };

        $scope.ClickDetail = function(selected) {
            $scope.detailRow = selected;
            console.log("Detail POP", $scope.detailRow);
			setTimeout(function() {
              angular.element('.ui.modal.show_modal_detail_summary_stock').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.show_modal_detail_summary_stock').not(':first').remove();  
            }, 1);
            //$scope.show_modal.show = !$scope.show_modal.show;
        };
		
		$scope.Tutup_show_modal_detail_summary_stock = function() {
			angular.element('.ui.modal.show_modal_detail_summary_stock').modal('hide');
		}

        $scope.onListCancel = function(item) {
            console.log("cancel_modal");
        };

        $scope.btnTutupDetailModal = function() {
            angular.element('#detailKendaraan').modal('hide');
        };

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        };

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        };

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };

        var DetailClick = '<i title="Detail" ng-if="row.entity.VehicleTypeDescription != null" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.ClickDetail(row.entity)" ></i>';
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.gridSummary = {
            enableColumnMenus: true,
            enableGroupHeaderSelection: true,
            enableRowHeaderSelection: true,
            enableSorting: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            enableColumnResizing: true,
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                {
                    name: 'Model',
                    field: 'VehicleModelName',
                    width: '13%',
                    pinnedLeft: true,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'Tipe', field: 'VehicleTypeDescription', width: '13%', cellTemplate:'<div style="position: relative;top: 20%;margin-left:5px"><label>{{row.entity.VehicleTypeDescription}}-{{row.entity.KatashikiCode}}-{{row.entity.SuffixCode}}</label></div>' },
                { name: 'Warna', field: 'ColorName', width: '13%' },
                { name: 'RRN', field: 'RRNNo', width: '10%' },
                { name: 'No Rangka', field: 'FrameNo', width: '13%' },
                { name: 'PLOD', field: 'PLOD', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\'' , type: 'number'},
                { name: 'DO TAM', field: 'DOTAMDate', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\'' , type: 'number'},
                { name: 'Aging', field: 'Aging', width: '10%' , type: 'number'},
                { name: 'Kategori', field: 'StatusUnitName', width: '12%' },
                { name: 'Status', field: 'StatusStockName', width: '12%' },
                { name: 'Alamat', field: 'DeliveryNoteAddress', width: '12%' },
                { name: 'Action', pinnedRight: true, width: '12%', cellTemplate: DetailClick }
                // { name: 'harga',cellFilter:'currency:"Rp."', field: 'Harga',treeAggregationType: uiGridGroupingConstants.aggregation.SUM, customTreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = aggregation.value;
                //     } }
            ],
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                // $timeout(function() {
                //     gridApi.grouping.clearGrouping();
                //     gridApi.grouping.groupColumn('Model');
                //     gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                // });
            }
        };
    });