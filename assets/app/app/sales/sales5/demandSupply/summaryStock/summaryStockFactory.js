angular.module('app')
  .factory('SummaryStockFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
    getData: function(filter) {
        for(key in filter){
            if(filter[key]==null || filter[key]==undefined)
                var kosong =+ filter[key];
        }
        var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/DemandSupplyStockView/Summary/?start=1&limit=1000000&'+ param +'&'+ kosong +'=""');
        return res;
    },
        
    create: function(SampleUITemplateFactory) {
    return $http.post('/api/fw/Role', [{
                                        //AppId: 1,
                                        SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
    },
      
    update: function(SampleUITemplateFactory){
    return $http.put('/api/fw/Role', [{
                                        SalesProgramId: SampleUITemplateFactory.SalesProgramId,
                                        SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
    },
      
    delete: function(id) {
    return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
    },
    
    }
  });