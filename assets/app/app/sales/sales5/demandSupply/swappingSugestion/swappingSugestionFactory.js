angular.module('app')
    .factory('SwappingSugestionFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/DemandSupplyMaintainMatching/?start=1&limit=100&' + $httpParamSerializer(filter));
                return res;
            },

            getDataStatus: function() {
                var res = $http.get('/api/sales/PStatusMatching');
                return res;
            },

            getDataMatchingVehicleFree: function(param) {
                var res = $http.get('/api/sales/DemandSupplyMMFreeVehicle' + param);
                return res;
            },

            getDataMatchingVehicleFreeGroupDealer: function(param) {
                var res = $http.get('/api/sales/DemandSupplyMMFreeVehicleGroupDealer' + param);
                return res;
            },

            getAreaDealer: function() {
                var res = $http.get('/api/sales/MSitesArea');
                return res;
            },

            match: function(MatchSwapping) {
                return $http.post('/api/sales/DemandSupplyMaintainMatching',MatchSwapping );
            },

            suggestMatch: function(Suggest) {
                return $http.put('/api/sales/DemandSupplyMaintainMatching/Suggest', Suggest);
            },

            update: function(unmatch) {
                return $http.put('/api/sales/DemandSupplyMaintainMatching/Unmatch',unmatch );
            },

            create: function(swappingSugestion) {
                return $http.post('/api/sales/DemandSupplyMMRequestSwapping', swappingSugestion);
            },
			
			GetSwappingSuggestionSearchDropdownOptions: function () {
        
				var da_json = [
				  { SearchOptionId: "FrameNoRRNNo", SearchOptionName: "No. Rangka" },
				  { SearchOptionId: "VehicleModelName", SearchOptionName: "Model" },
				  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Tipe" },
				];

				var res=da_json

				return res;
			  },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });