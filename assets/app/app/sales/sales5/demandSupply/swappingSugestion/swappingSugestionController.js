angular.module('app')
    .controller('SwappingSugestionController', function($scope, $http, bsNotify, CurrentUser, SwappingSugestionFactory,MobileHandling, MaintainSOFactory, FindStockFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.disabledBtnMatch = false;
        $scope.ManualMatching = true;
        $scope.SwappingSugestion = false;
        $scope.disabledMatch = false;
        $scope.show_modal = { show: false };
        $scope.disabledbtnproces = false;
        $scope.modalMode = 'new';
        $scope.modal_model = [];

        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
			//----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.rightEnableByte = null;
        $scope.user = CurrentUser.user();
        console.log("USER", $scope.user);
        $scope.mSwappingSugestion = null; //Model
        $scope.xRole = { selected: [] };
        $scope.disabledSwappingSuggestionUnmatchSimpan = false;
        // var mount = new Date();
        // var year = mount.getFullYear();
        // var mounth = mount.getMonth();
        // var lastdate = new Date(year, mounth+1, 0).getDate();
		
        // $scope.firstDate = new Date( mount.setDate("01"));
        // $scope.lastDate = new Date (mount.setDate(lastdate));
		
		var Da_Today_Date=new Date();
		$scope.firstDate = new Date(Da_Today_Date.setDate(1));
        $scope.lastDate = new Date();

        $scope.filter = { SPKDateStart: $scope.firstDate, SPKDateEnd: $scope.lastDate, SPKNo: null, StatusMatchingId: null };

        if ($scope.user.RoleName == "KACAB" || $scope.user.RoleName == "SUPERVISOR SALES") {
            $scope.showfindUnit = true;
        } else {
            $scope.showfindUnit = false;
        }

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        }

        $scope.tanggalmin = function(dt) {
            $scope.dateOptionsEnd.minDate = dt;
        }

        $scope.showAlert = function(message, number) {
            $.bigBox({
                title: 'SO Date Tidak Boleh Kosong.!',
                content: message,
                color: "#c00",
                icon: "fa fa-shield fadeInLeft animated"
                    // number: ''
            });
        };
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.small.modal.matching').remove();
		  angular.element('.ui.modal.NoRangka').remove();
		  angular.element('.ui.modal.GroupDealer').remove();
		  angular.element('.ui.small.modal.OtherDealer').remove();

		});

        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
        }

        $timeout(function () {
            console.log("rightEnableByte", $scope.rightEnableByte);
            checkBytes($scope.rightEnableByte);
            console.log("RIGHST =====>", $scope.RightsEnableForMenu);
        }, 1000)

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.SelectUnit = [];
        $scope.selectedSwappingSugestion = [];
        
        $scope.getData = function() {
            if($scope.filter.SPKDateStart!=null && (typeof $scope.filter.SPKDateStart!="undefined") && $scope.filter.SPKDateEnd!=null && (typeof $scope.filter.SPKDateEnd!="undefined"))
			{
				SwappingSugestionFactory.getData($scope.filter).then(
					function(res) {
						$scope.grid.data = res.data.Result;
						$scope.loading = false;
						return res.data.Result;
					},
					function(err) {
						bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
					}
				);
			}
			else if($scope.filter.SPKDateStart==null || (typeof $scope.filter.SPKDateStart=="undefined") || $scope.filter.SPKDateEnd==null || (typeof $scope.filter.SPKDateEnd=="undefined"))
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Field mandatory harus diisi.",
						type: 'warning'
					}
				);
			}
			
        }

        SwappingSugestionFactory.getDataStatus().then(
            function(res) {
                $scope.statusMatching = res.data.Result;
                return res.data;
            }
        );

        $scope.btnMatch = function() {          
            console.log("SelectUnit=>", $scope.SelectUnit);
            $scope.disabledBtnMatch = true;
            var count = 0;
            $scope.tampungMatch = [];
            $scope.tampungTidakMatch = [];
            for (var i in $scope.SelectUnit){
                if ($scope.SelectUnit[i].FrameNoRRNNo == "" || $scope.SelectUnit[i].FrameNoRRNNo == null || typeof $scope.SelectUnit[i].FrameNoRRNNo == 'undefined') {
                    count++;
                }
            }
                    if (count >= 1) {
                        // bsNotify.show({
                            // //size: 'big',
                            // type: 'warning',
                            // timeout: 3000,
                            // //color: "#c00",
                            // title: "Peringatan",
                            // content: "No Rangka / RRN Tidak Boleh Kosong"
                        // });
						
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Stock/MDP Tidak Tersedia",
								type: 'warning'
							}
                        );
                        $scope.disabledBtnMatch = false;
                                     
                }else {
                    for(var j in $scope.SelectUnit){
                        if ($scope.SelectUnit[j].StatusMatchId == 1) {
                            $scope.tampungMatch.push($scope.SelectUnit[j]);
                        }else{
                            $scope.tampungTidakMatch.push($scope.SelectUnit[j]);
                        }
                    }
                    
                    if($scope.tampungMatch.length > 0){
                        SwappingSugestionFactory.match($scope.tampungMatch).then(
                            function(res) {
                                SwappingSugestionFactory.getData($scope.filter).then(
                                    function(res) {
                                        $scope.grid.data = res.data.Result;
                                        bsNotify.show(
                                            {
                                                title: "Berhasil",
                                                content: "Data berhasil di Match",
                                                type: 'success'
                                            }
                                        );
                                        $scope.disabledBtnMatch = false;
                                    });
                            },
                            function(err) {
                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: err.data.Message,
                                        type: 'danger'
                                    }
                                );
                                $scope.disabledBtnMatch = false;
                            }
                        );  
                    }

                    if($scope.tampungTidakMatch.length > 0){
                        angular.element('.ui.small.modal.matching').modal('show');
						angular.element('.ui.small.modal.matching').not(':first').remove(); 
                    }
                }
                            
                //         } else {
                           
                //         }
                //     }
                    
                // }
            
        }

        $scope.btnSuggest = function() {
            SwappingSugestionFactory.suggestMatch($scope.SelectUnit).then(
                function(res) {
                    $scope.SuggestResult = res.data.Result;
                    //console.log("$scope.SuggestResult", $scope.SuggestResult);
                    //console.log("$scope.SelectUnit", $scope.SelectUnit);
                    // Ori
                    // for (var i in $scope.SuggestResult) {
                    //     for (var j in $scope.SelectUnit) {
                    //         $scope.SelectUnit[j].VehicleId = $scope.SuggestResult[i].VehicleId;
                    //         $scope.SelectUnit[j].FrameNo = $scope.SuggestResult[i].FrameNo;
                    //         $scope.SelectUnit[j].RRNNo = $scope.SuggestResult[i].RRNNo;
                    //         $scope.SelectUnit[j].FrameNoRRNNo = $scope.SuggestResult[i].FrameNoRRNNo;
                    //     }
                    // }
                    //Edited Fikri
                    for (var i in $scope.SelectUnit) {
                        var temp = $scope.SuggestResult.filter(function(x) {
                            return x.SOId == $scope.SelectUnit[i].SOId;
                        });

                        if (temp.length > 0) {
                            $scope.SelectUnit[i].VehicleId = temp[0].VehicleId;
                            $scope.SelectUnit[i].FrameNo = temp[0].FrameNo;
                            $scope.SelectUnit[i].RRNNo = temp[0].RRNNo;
                            $scope.SelectUnit[i].FrameNoRRNNo = temp[0].FrameNoRRNNo;
                        }
                    }
					$scope.SelectUnit = [];
                    //
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnUnmatch = function() {
            // // if ($scope.SelectUnit[0].StatusMatchId == 2 )
            // // {
            // //     $scope.SelectUnit[0].StatusMatchId = 1
            // //     $scope.SelectUnit[0].StatusMatchName = "Outstanding"
            // // }
            //    $scope.un_Match = angular.copy($scope.SelectUnit);
            //    for (var i in $scope.un_Match){
            //        $scope.un_Match[i] = {OutletId: $scope.un_Match[i].OutletId, SOId: $scope.un_Match[i].SOId, StatusMatchId: 1, StatusMatchName: "Outstanding"};
            //    }
			
			var SwappingSuggestionAmanUnmatch=true;
			
			for(var i = 0; i < $scope.SelectUnit.length; ++i)
			{	
				if($scope.SelectUnit[i].StatusMatchName== "Outstanding")
				{
					SwappingSuggestionAmanUnmatch=false;
					break;
				}
			}
			if(SwappingSuggestionAmanUnmatch==true)
			{
				
				$scope.SwappingSuggestionUnmatchReason="";
				
				
				setTimeout(function() {
					angular.element('.ui.modal.ModalUnmatchSwappingSuggestion').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalUnmatchSwappingSuggestion').not(':first').remove();  
				}, 1);
				// SwappingSugestionFactory.update($scope.SelectUnit).then(
					// function(res) {
						// $scope.getData($scope.filter);
						// bsNotify.show(
							// {
								// title: "Berhasil",
								// content: "Data berhasil di Un-Match",
								// type: 'success'
							// }
						// );
					// },
					// function(err) {
						// $scope.getData($scope.filter);
						// bsNotify.show({
								// title: "Gagal",
								// content: "" + err.data.Message,
								// type: 'danger'
							// });
					// }
				// );
			}
			else if(SwappingSuggestionAmanUnmatch==false)
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "RRN/No rangka yang di unmatch masih ada yang statusnya outstanding. Cek kembali datanya.",
						type: 'warning'
					}
				);
			}
			
         
        }
		
		$scope.SwappingSuggestionUnmatchSimpan = function() {
            $scope.disabledSwappingSuggestionUnmatchSimpan = true;
			for(var i = 0; i < $scope.SelectUnit.length; i++)
			{
				$scope.SelectUnit[i].UnmatchReason=$scope.SwappingSuggestionUnmatchReason;		
			}
			
			SwappingSugestionFactory.update($scope.SelectUnit).then(
					function(res) {
						$scope.getData($scope.filter);
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Data berhasil di Un-Match",
								type: 'success'
							}
                        );
                        $scope.disabledSwappingSuggestionUnmatchSimpan = false;
						angular.element('.ui.modal.ModalUnmatchSwappingSuggestion').modal('hide');
						$scope.SelectUnit = [];
					},
					function(err) {
						$scope.getData($scope.filter);
						bsNotify.show({
								title: "Gagal",
								content: "" + err.data.Message,
								type: 'danger'
                            });
                            $scope.disabledSwappingSuggestionUnmatchSimpan = false;
						angular.element('.ui.modal.ModalUnmatchSwappingSuggestion').modal('hide');
					}
				);
		}
		
		$scope.SwappingSuggestionUnmatchBatal = function() {
			angular.element('.ui.modal.ModalUnmatchSwappingSuggestion').modal('hide');
		}

        $scope.findUnit = function() {
            if ($scope.SelectUnit[0].StatusMatchId != 1) {
                console.log("XXX", $scope.SelectUnit);
                angular.element('.ui.small.modal.matching').modal('show');
				angular.element('.ui.small.modal.matching').not(':first').remove(); 
            } else {
                $scope.ManualMatching = false;
                $scope.SwappingSugestion = true;
            }
        }

        $scope.KembaliMaintain = function() {
            $scope.ManualMatching = true;
            $scope.SwappingSugestion = false;
			$scope.getData();
        }

        $scope.TutupModalMatching = function() {
            $scope.ManualMatching = true;
            $scope.SwappingSugestion = false;
            angular.element('.ui.small.modal.matching').modal('hide');
        }

        $scope.ClickColumnRRN = function(selected) {
            $scope.show_modal.show = !$scope.show_modal.show;
            $scope.selectRowRRN = [];
            setTimeout(function() {
                angular.element('.ui.modal.NoRangka').modal('refresh');
            }, 0);
            angular.element('.ui.modal.NoRangka').modal('show');
			angular.element('.ui.modal.NoRangka').not(':first').remove(); 

            $scope.SelectRRN = selected;
            var getRRN = "?VehicleTypeColorId=" + $scope.SelectRRN.VehicleTypeColorId + "&ProductionYear=" + $scope.SelectRRN.ProductionYear;
            SwappingSugestionFactory.getDataMatchingVehicleFree(getRRN).then(
                function(res) {
					$scope.gridMatchingVehicleRaw=angular.copy(res.data.Result);
					$scope.gridMatchingVehicle.data = res.data.Result;
                }
            );
        }
		
		$scope.SwappingSuggestionRangkaRrnSearchThingy = function() {
			
			$scope.gridMatchingVehicle.data = angular.copy($scope.gridMatchingVehicleRaw).filter(function (type) 
				{ 
					return (type.FrameNoRRNNo.includes($scope.SwappingSuggestionRangkaRrnSearchValue)); 
				})
		}

        $scope.onListSave = function(item) {
            $scope.show_modal = { show: false };
            angular.element('.ui.modal.NoRangka').modal('hide');
            $scope.SelectRRN.FrameNoRRNNo = $scope.selectRowRRN[0].FrameNoRRNNo;
            $scope.SelectRRN.RRNNo = $scope.selectRowRRN[0].RRNNo;
            $scope.SelectRRN.FrameNo = $scope.selectRowRRN[0].FrameNo;
            $scope.SelectRRN.VehicleId = $scope.selectRowRRN[0].VehicleId;
            console.log("SelectRRN", $scope.SelectRRN.FrameNoRRNNo);
        }

        $scope.onListCancel = function(item) {
            console.log("cancel_modal");
            angular.element('.ui.modal.NoRangka').modal('hide');
        }

        $scope.FindDealer = function(selectRow, index) {
            $scope.selectGroupDealer = selectRow;
            $scope.indexSelect = index;
            var getTypeColorId = "?VehicleTypeColorId=" + $scope.selectGroupDealer.VehicleTypeColorId + "&OtherDealer=" + false;
            SwappingSugestionFactory.getDataMatchingVehicleFreeGroupDealer(getTypeColorId).then(
                function(res) {
                    $scope.gridFindGroupDealer.data = res.data.Result;
                }
            )

            angular.element('.ui.modal.GroupDealer').modal('show');
			angular.element('.ui.modal.GroupDealer').not(':first').remove(); 
        }


       

        $scope.btnFindOther = function(selectRow, index) {
            $scope.selectOtherDealer = selectRow;
            $scope.indexSelect = index;
            $scope.OtherDealer = {};
            $scope.gridFindOtherDealer.data = [];

            SwappingSugestionFactory.getAreaDealer().then(
                function(res) {
                    $scope.DataArea = res.data.Result;
                }
            )

            setTimeout(function() {
                angular.element('.ui.modal.OtherDealer').modal('refresh');
            }, 0);

            angular.element('.ui.small.modal.OtherDealer').modal('show');
			angular.element('.ui.small.modal.OtherDealer').not(':first').remove(); 
        }

        $scope.getProvince = function(Id) {
            FindStockFactory.getDataProvincebyArea(Id).then(
                function(res) {
                    $scope.getDataProvince = res.data.Result;
                    return res.data;
                }
            );
        }

        $scope.SearchOtherDealer = function () {
            var getTypeColorId = "?VehicleTypeColorId=" + $scope.selectOtherDealer.VehicleTypeColorId + "&OtherDealer=" + true + "&AreaId=" + $scope.OtherDealer.AreaId + "&ProvinceId=" + $scope.OtherDealer.ProvinceId;
            SwappingSugestionFactory.getDataMatchingVehicleFreeGroupDealer(getTypeColorId).then(
                function(res) {
                    $scope.gridFindOtherDealer.data = res.data.Result;
                }
            )
        }

        $scope.btnPilihGroupDealer = function() {
            $scope.selectGroupDealer.OutletIdTarget = $scope.selctedRow[0].OutletIdTarget;
            $scope.selectGroupDealer.OutletNameTarget = $scope.selctedRow[0].OutletNameTarget;
            angular.element('.ui.modal.GroupDealer').modal('hide');
        }

        $scope.btnPilihOtherDealer = function(item) {
            $scope.selectOtherDealer.OutletIdTarget = $scope.selctedRow[0].OutletIdTarget;
            $scope.selectOtherDealer.OutletNameTarget = $scope.selctedRow[0].OutletNameTarget;
            angular.element('.ui.small.modal.OtherDealer').modal('hide');
        }

        $scope.btnTutupGroupDealer = function() {
            angular.element('.ui.modal.GroupDealer').modal('hide');
            angular.element('.ui.small.modal.OtherDealer').modal('hide');
        }

        $scope.btnRequestSwapping = function() {
            var NamaCabangAdaYangKosong=false;
			for(var i = 0; i < $scope.selectedSwappingSugestion.length; ++i)
			{	
				if($scope.selectedSwappingSugestion[i].OutletNameTarget==""||$scope.selectedSwappingSugestion[i].OutletNameTarget==null||(typeof $scope.selectedSwappingSugestion[i].OutletNameTarget=="undefined"))
				{
					NamaCabangAdaYangKosong=true;
				}
			}
			
			if(NamaCabangAdaYangKosong==true)
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Pastikan nama cabang sudah dipilih",
						type: 'warning'
					}
				);
			}
			else if(NamaCabangAdaYangKosong==false)
			{
				SwappingSugestionFactory.create($scope.gridSwappingSugestion.data).then(
					function(res) {

						$scope.ManualMatching = true;
						$scope.SwappingSugestion = false;
						$scope.SelectUnit = [];
						$scope.selectedSwappingSugestion = [];
						$scope.getData();
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Simpan Data Request Swapping Berhasil !",
								type: 'success'
							}
						);

					},
					function(err) {
						console.log("err=>", err);
						bsNotify.show(
							{
								title: "Gagal",
								content: "Simpan Data Request Swapping Gagal !",
								type: 'danger'
							}
						);
					}
				);
			}			

        }

        

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            $scope.SelectUnit = rows;
            console.log("UNIT", $scope.SelectUnit);
            $scope.gridSwappingSugestion.data = $scope.SelectUnit;

            if ($scope.SelectUnit != "") {
                for (var i in $scope.SelectUnit) {
                    $scope.SelectUnit[i].Check = true;
                }
                console.log("Check True =>", $scope.SelectUnit.Check);

                // if($scope.SelectUnit.Check == true){
                //     $scope.disabledbtnproces  = false;
                //     if($scope.SelectUnit.StatusMatchName == "Match") {
                //         $scope.disabledMatch = true;
                //         $scope.disabledsuggest = true;
                //     }
                //     else{
                //         $scope.disabledMatch = false;
                //         $scope.disabledsuggest = false;
                //     }
                // }
                // else{
                //     console.log("Check=>False");
                // }
            } else {
                $scope.SelectUnit.Check = false;
                console.log("Check False =>", $scope.SelectUnit.Check);

                if ($scope.SelectUnit.Check == false) {
                    console.log("Benar");
                    //$scope.disabledbtnproces  = true;
                } else {
                    console.log("Salah");
                }
            }
        }

        var actionClick = '<u style="color:blue"  ng-if="row.entity.FrameNoRRNNo ==\'\' || row.entity.FrameNoRRNNo ==null" ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowNew" ng-click="grid.appScope.$parent.ClickColumnRRN(row.entity)">...</u>&nbsp\
                           <div style="margin-top: -10px;" ng-if="row.entity.FrameNoRRNNo !=\'\'">{{row.entity.FrameNoRRNNo}}</div>'
        var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.FindDealer(row.entity, $index)">Same Company</u>&nbsp<u ng-click="grid.appScope.btnFindOther(row.entity)">Different Company</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'id', field: 'SOId', visible: false },
                { name: 'Model Tipe', field: 'VehicleTypeDescription', width: '24%' },
                { name: 'Warna', field: 'ColorName', width: '13%' },
                { name: 'Tahun Kendaraan', field: 'ProductionYear', width: '10%' },
                { name: 'No. Rangka / RRN', field:'FrameNoRRNNo', cellTemplate: actionClick, width: '15%' },
                { name: 'No. SPK',field: 'FormSPKNo', displayName: 'No. SPK',  width: '15%' },
                { name: 'Tanggal SPK',field: 'SpkDate', displayName: 'Tanggal SPK', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Tanggal SO',displayName: 'Tanggal SO', field: 'SoDate', cellFilter: 'date:\'dd-MM-yyyy\'', width: '8%' },
                { name: 'No. SO',displayName: 'No. SO', field: 'SOCode', width: '13%' },
                { name: 'Nama Prospek/Pelanggan',displayName: 'Nama Prospek/Pelanggan', field: 'CustomerName', width: '20%' },
                { name: 'Tanggal Rencana Pelunasan DP',displayName: 'Tanggal Rencana Pelunasan DP', field: 'DPPaidPlanDate', width: '15%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                {
                    name: 'status matching',
                    allowCellFocus: false,
                    width: '12%',
                    //pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    //enableColumnResizing: true,
                    field: 'StatusMatchName'
                },
                { name: 'Cabang Tujuan',displayName: 'Cabang Tujuan', field: 'OutletNameTarget', width: '13%' },
            ]
        };

        $scope.gridSwappingSugestion = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: true,
            enableColumnResizing: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
			//showTreeExpandNoChildren: true,
            columnDefs: [
                { displayName: 'Model Tipe', field: 'VehicleTypeDescription', width: '20%' },
                { name: 'warna', field: 'ColorName', width: '15%' },
                { displayName: 'No. SPK', field: 'FormSPKNo', width: '15%' },
                { displayName: 'Tanggal SPK', field: 'SpkDate', width: '12%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { displayName: 'No. SO', field: 'SOCode', width: '15%' },
                { displayName: 'Nama Prospek/Pelanggan', field: 'ProspectName', width: '20%' },
                { displayName: 'Nama Cabang', field: 'OutletNameTarget', width: '20%' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '15%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    cellTemplate: btnAction
                }
            ]
        };

        
        $scope.gridSwappingSugestion.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedSwappingSugestion = gridApi.selection.getSelectedRows();
                console.log("VRV", $scope.selectedSwappingSugestion);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridMatchingVehicle = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			enableColumnResizing: true,
			enableFiltering: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
			//showTreeExpandNoChildren: true,
            columnDefs: [
                { displayName: 'No. Rangka/RRN', field: 'FrameNoRRNNo'},
                { displayName: 'Model', field: 'VehicleModelName' },
                { displayName: 'Tipe', field: 'VehicleTypeDescription' },
                { displayName: 'Warna', field: 'ColorName' },
                { displayName: 'Tahun Produksi', field: 'ProductionYear' },
                { displayName: 'Status Stock', field: 'StatusStockName'}
            ]
        };

        $scope.gridMatchingVehicle.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectRowRRN = gridApi.selection.getSelectedRows();
                console.log("123", $scope.selectRowRRN);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectRowRRN = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridFindGroupDealer = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
			paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'OutletId', field: 'OutletIdTarget', visible: false },
                { name: 'Cabang', field: 'OutletNameTarget' }
            ]
        };

        $scope.gridFindGroupDealer.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
                console.log("VRV", $scope.selctedRow);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridFindOtherDealer = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
			//showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'OutletId', field: 'OutletIdTarget', visible: false },
                { name: 'Cabang', field: 'OutletNameTarget' }
            ]
        };

        $scope.gridFindOtherDealer.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
                console.log("other", $scope.selctedRow);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.grid.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
                console.log("other", $scope.selctedRow);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };
    });