angular.module('app')
    .controller('MaintainTSController', function($scope, $http, bsNotify, CurrentUser, MaintainTSFactory, MaintainQuotationFactory, $timeout, PrintRpt) {
        var year = new Date().getFullYear();
        var month = ('0' + (new Date().getMonth() + 1)).slice(-2);
        var date = ('0' + new Date().getDate()).slice(-2);
        var hour = ((new Date().getHours() < '10' ? '0' : '') + new Date().getHours());
        var minutes = ((new Date().getMinutes() < '10' ? '0' : '') + new Date().getMinutes());

        var now = new Date();
        var years = now.getFullYear();
        var mounth = now.getMonth();
        var lastdate = new Date(years, mounth + 1, 0).getDate();

        //$scope.lastDate = new Date(now.setDate(lastdate));
		$scope.lastDate = new Date();
		
		var Da_Today_Date=new Date();
		var daTsDateStart=new Date(Da_Today_Date.setDate(1));

        $scope.TimeQuot = {
            value: new Date(year, month, date, hour, minutes)
        };

        $scope.dateOptions ={
            startingDay: 1,
            format: 'dd/MM/yyyy'
        }
        // $scope.DetailQuotation.SentTime = new Date();
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ShowMaintainForm = true;
        $scope.ShowFormCreateTS = false;
        //$scope.ShowFormPreviewDeliveryNote = false;
        $scope.filter = { TSDateStart: daTsDateStart, TSDateEnd: $scope.lastDate, TSNo: null };

        $scope.flag = null;
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];



        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainTS = null; //Model
        $scope.xRole = { selected: [] };
        $scope.DetailQuotation = {};

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            if($scope.filter.TSDateStart == null || $scope.filter.TSDateEnd == null){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Mandatory harus diisi.",
                    type: 'warning'
                });
            }else{
                MaintainTSFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        MaintainQuotationFactory.getData().then(
            function(res) {
                $scope.Quotation = res.data.Result;
                return res.data.Result;
            }
        );

        MaintainTSFactory.getOutletRequestor().then(
            function(res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        // MaintainTSFactory.statusTS().then(
        //     function(res) {
        //         $scope.tsStatus = res.data.Result;
        //         return res.data;
        //     }
        // )

        $scope.filterModel = function(Selectmodel) {
            //$scope.getQuot = [{QuotationNo: "123", QuotationId: 1},{QuotationNo: "456", QuotationId: 2}];

            MaintainTSFactory.getQuotatoinList(Selectmodel.OutletId).then(function(res) {
                $scope.getQuot = res.data.Result;
            });
        }

        $scope.getDetailQuo = function() {
            var dataQuo = "?QuotationId=" + $scope.DetailQuotation.QuotationId + "&OutletIdReq=" + $scope.DetailQuotation.OutletId;
            MaintainTSFactory.getQuoDetail(dataQuo).then(
                function(res) {
                    $scope.DetailQuotation = res.data.Result[0];
                    console.log("succ=>", $scope.DetailQuotation);
                    $scope.loading = false;
                    return res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnBuatTS = function() {
            $scope.ShowMaintainForm = false;
            $scope.ShowFormCreateTS = true;
            $scope.DisableBuatTS = false;
            $scope.flag = "add";
            //console.log($scope.TimeQuot);

            var year = new Date().getFullYear();
            var month = ('0' + (new Date().getMonth() + 1)).slice(-2);
            var date = ('0' + new Date().getDate()).slice(-2);
            var hour = ((new Date().getHours() < '10' ? '0' : '') + new Date().getHours());
            var minutes = ((new Date().getMinutes() < '10' ? '0' : '') + new Date().getMinutes());

            $scope.DetailQuotation.SentDate = new Date(year, month, date, hour, minutes);
            $scope.minSendDate = year.toString()+'-'+month.toString()+'-'+date.toString();
        }

        

        $scope.btnSimpanTS = function() {
            if ($scope.flag == "add") {
                MaintainTSFactory.create($scope.DetailQuotation).then(
                    function(res) {
                        MaintainTSFactory.getData().then(
                            function(res) {
                                $scope.grid.data = res.data.Result;
                                $scope.loading = false;
                                return res.data.Result;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        $scope.ShowMaintainForm = true;
                        $scope.ShowFormCreateTS = false;
                        bsNotify.show({
                            title: "Success Message",
                            content: "Data Berhasil Disimpan",
                            type: 'success'
                        });
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                );
            } else {

                MaintainTSFactory.update($scope.DetailQuotation).then(
                    function(res) {
                        $scope.ShowMaintainForm = true;
                        $scope.ShowFormCreateTS = false;
                        bsNotify.show({
                            title: "Success Message",
                            content: "Data Berhasil Disimpan",
                            type: 'success'
                        });
                        MaintainTSFactory.getData().then(
                            function(res) {
                                $scope.grid.data = res.data.Result;
                                $scope.loading = false;
                                return res.data.Result;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                );
            }

        }

        $scope.dateChange = function(date) {
            $scope.DetailQuotation.SentTime = date;
        }

        $scope.btnUbahTS = function(selected) {
            $scope.selectedrow = selected;
            $scope.flag = "edit";
            $scope.TimeQuot.value = selected.SentTime;
            $scope.DetailQuotation = selected;

            $scope.ShowFormCreateTS = true;
            $scope.ShowMaintainForm = false;
            $scope.DisableBuatTS = false;
        }

        $scope.lihatTS = function(selected) {
            $scope.ShowFormCreateTS = true;
            $scope.ShowMaintainForm = false;
            $scope.DisableBuatTS = true;
            $scope.flag = "lihat";
            //$scope.DetailQuotation.SentTime = selected.SentTime;
            $scope.TimeQuot.value = selected.SentTime;
            $scope.selectedrow = selected;
            $scope.DetailQuotation = {};



            $scope.DetailQuotation = selected;
        }

        $scope.btnCatak = function() {
            $scope.btnPrintMaintainTs($scope.selectedrow.TransferStockId, $scope.selectedrow.FrameNo);
        }

        $scope.btnPreviewDokumen = function() {
            if ($scope.flag == "add") {
                MaintainTSFactory.create($scope.DetailQuotation).then(
                    function(res) {
                        $scope.getData();
                        bsNotify.show({
                            title: "Success Message",
                            content: "Data Berhasil Disimpan",
                            type: 'success'
                        });
                        $scope.ShowMaintainForm = true;
                        $scope.ShowFormCreateTS = false;
                        $scope.btnPrintMaintainTs(res.data.Result[0].TransferStockId, res.data.Result[0].FrameNo);
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                );
            } else {

                MaintainTSFactory.update($scope.DetailQuotation).then(
                    function(res) {
                        $scope.btnPrintMaintainTs($scope.DetailQuotation.TransferStockId, $scope.DetailQuotation.FrameNo);
                        $scope.ShowMaintainForm = true;
                        $scope.ShowFormCreateTS = false;
                        $scope.getData();
                        bsNotify.show({
                            title: "Success Message",
                            content: "Data Berhasil Disimpan",
                            type: 'success'
                        });
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Error Message",
                            content: "Data Gagal Disimpan",
                            type: 'danger'
                        });
                    }
                );
            }
        }

        $scope.btnPrintMaintainTs = function(TsId, Frame) {
            var pdfFile = null;

            PrintRpt.print("sales/PrintTransferStock?TransferStockId=" + TsId + "&FrameNo=" + Frame).success(function(res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);

                    console.log("pdf", fileURL);
                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                    pdfFile = fileURL;

                    if (pdfFile != null)
                        printJS(pdfFile);
                    else
                        console.log("error cetakan", pdfFile);
                })
                .error(function(res) {
                    console.log("error cetakan", pdfFile);
                });
        }

        // $scope.btnBatalModal = function(){
        //     angular.element('#ModalClosePO').modal('hide');
        // }

        $scope.btnKembaliFormCreateTS = function() {
            $scope.ShowFormCreateTS = false;
            $scope.ShowMaintainForm = true;
            $scope.flag = '';
            $scope.DetailQuotation = {};
        }

        $scope.btnKembaliPreviewDokumen = function() {
            //$scope.ShowFormPreviewDeliveryNote = false;
            $scope.ShowMaintainForm = true;
            $scope.flag = '';
            $scope.DetailQuotation = {};
        }


        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.BatalTSModal = function (rows){
            $scope.rowdata = angular.copy(rows);
            angular.element('.ui.small.modal.batalTS').modal('show');
        }

        $scope.BatalTS = function() {
            MaintainTSFactory.batal($scope.rowdata).then(
                function(res) {

                    bsNotify.show({
                        title: "Success Message",
                        content: "Berhasil Batal Quotation",
                        type: 'success'
                    });
                    angular.element('.ui.small.modal.batalTS').modal('hide');
                    $scope.getData();
                },
                function(err) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Gagal Batal Quotation",
                        type: 'danger'
                    });
                }
            );
        };

        $scope.keluarBatalTS = function (){
            angular.element('.ui.small.modal.batalTS').modal('hide');
        }

    //     var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" >\
    // <u ng-click="grid.appScope.$parent.lihatTS(row.entity)">Lihat</u>&nbsp\
    // <u ng-if="row.entity.TransferStockStatusName ==\'Dibuat\' || row.entity.TransferStockStatusName ==\'Diubah\'" ng-click="grid.appScope.$parent.btnUbahTS(row.entity)">Ubah</u></span>&nbsp\
    // <u ng-if="row.entity.TransferStockStatusName ==\'Dibuat\' || row.entity.TransferStockStatusName ==\'Diubah\'" ng-click="grid.appScope.$parent.BatalTSModal(row.entity)"">Batal</u></span>';


    var btnAction = '<i title="Lihat" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.lihatTS(row.entity)" ></i>  <i ng-if="row.entity.TransferStockStatusName ==\'Dibuat\' || row.entity.TransferStockStatusName ==\'Diubah\'" title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.btnUbahTS(row.entity)" ></i> <i ng-if="row.entity.TransferStockStatusName ==\'Dibuat\' || row.entity.TransferStockStatusName ==\'Diubah\'" title="Batal" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.BatalTSModal(row.entity)" ></i>';
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'POId', visible: false },
                { displayName: 'No Transfer Stok', name: 'No transfer stok', field: 'TransferStockNo' },
                { displayName: 'Tanggal Transfer Stok', name: 'tanggal transfer stok', field: 'TransferStockDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { displayName: 'Status Transfer Stok', name: 'Status transfer stok', field: 'TransferStockStatusName' },
                { name: 'Cabang yang meminta', field: 'RequesterOutletName' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '13%',
                    visible: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };
    });