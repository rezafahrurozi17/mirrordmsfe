angular.module('app')
    .factory('MaintainTSFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                filter.TSDateStart = fixDate(filter.TSDateStart);
                filter.TSDateEnd = fixDate(filter.TSDateEnd);
                var param = '&' + $httpParamSerializer(filter);
                var res = $http.get('/api/sales/DemandSupplyTransferStock?start=1&limit=1000' + param);
                return res;
            },
            getQuoDetail: function(param) {
                var res = $http.get('/api/sales/DemandSupplyTransferStock/QuotationDetail' + param);
                return res;
            },
            create: function(maintainTS) {
                maintainTS.SentDate = fixDate(maintainTS.SentDate);
                maintainTS.SentTime = fixDate(maintainTS.SentTime);
                var res = $http.post('/api/sales/DemandSupplyTransferStock', [maintainTS]);
                return res;
            },
            getQuotatoinList: function(Id) {
                var res = $http.get('/api/sales/DemandSupplyTransferStock/QuotationList?OutletIdReq=' + Id);
                return res;
            },
            update: function(maintainTS) {
                maintainTS.SentDate = fixDate(maintainTS.SentDate);
                maintainTS.SentTime = fixDate(maintainTS.SentTime);
                var res = $http.put('/api/sales/DemandSupplyTransferStock', [maintainTS]);
                return res;
            },
            getOutletRequestor: function() {
                var res = $http.get('/api/sales/MSitesOutletRequestor?start=1&limit=1000');
                return res;
            },
            // statusTS: function() {
            //     var res = $http.get('/api/sales/PStatusTS');
            //     return res;
            // },
            batal: function(maintainTS) {
                return $http.put('/api/sales/DemandSupplyTransferStock/Cancel', maintainTS);
            }
        }
    });