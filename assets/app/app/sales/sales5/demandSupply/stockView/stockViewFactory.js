angular.module('app')
  .factory('StockViewFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          // var kosong ="";
		  // for(key in filter){
              // if(filter[key]==null || filter[key]==undefined)
                  // kosong =+ filter[key];
          // }
          var param = $httpParamSerializer(filter);
          var res=$http.get('/api/sales/DemandSupplyStockView/?start=1&limit=10000000&'+ param );
          return res;
      },
	  
	  getYear: function() {
          var res=$http.get('/api/sales/DemandSupplyVehicleYearStock?IncludeCurrentYear=True');
          return res;
      },
      
      getOutlet: function() {
        var res=$http.get('/api/sales/DropDownOutletStockView');
        return res;
    },
    
      create: function(StockView) {
        return $http.post('/api/sales/DemandSupplyStockView', [{
            SalesProgramName: StockView.SalesProgramName}]);
      },
      
      update: function(StockView){
        return $http.put('/api/sales/DemandSupplyStockView', [{
              SalesProgramId: StockView.SalesProgramId,
              SalesProgramName: StockView.SalesProgramName}]);
      },
      
      delete: function(id) {
        return $http.delete('/sales/DemandSupplyStockView',{data:id,headers: {'Content-Type': 'application/json'}});
      },

      getValidateRequestSwapping: function(param) {
        var res = $http.get('/api/sales/DemandSupplyStockView/ValidateRequestSwapping?vin='+ param);
        return res;
      },

      createRequestSwapping: function(data) {
        return $http.post('/api/sales/DemandSupplyStockView/RequestSwapping', 
        {
            OutletId: data.OutletId,
            FrameNo: data.FrameNo,
            ProductionYear: data.ProductionYear,
            VehicleTypeColorId: data.VehicleTypeColorId,
            OutletIdTarget: data.OutletIdTarget
        });
      },

    }
  });