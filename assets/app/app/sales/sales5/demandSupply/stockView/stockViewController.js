angular.module('app')
    .controller('StockViewController', function($scope, $http, CurrentUser, $httpParamSerializer, StockViewFactory, MaintainSOFactory, ModelFactory, TipeFactory, StockViewMobileFactory, ComboBoxFactory, $timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.StockView = true;
    $scope.show_modaldetailKendaraan={show:false};
    $scope.show_modalRequestSwapping={show:false};
    $scope.validasiCabangSama = false;
    $scope.$on('$viewContentLoaded', function(){
        $scope.loading = true;
        $scope.gridData = [];
    });

    $scope.dateOptions = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    }

    $scope.dateRequestSwapping = new Date();
    $scope.ctRequestSwapping = null;

    document.getElementById("layoutContainer_StockViewForm").style.height = "500px";
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mStockView = null; //Model
    $scope.filter = {OutletId:null, VehicleModelId:null, VehicleTypeId:null, ColorId:null, Year:null};
    $scope.xRole = {selected:[]};

    $scope.isHO = false;
    $scope.requestSwappingDisabled = true;

    if($scope.user.RoleName == "Admin HO"){
        $scope.isHO = true;
    }
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        $scope.loading = true;
		
		if($scope.filter.OutletId!=null && (typeof $scope.filter.OutletId!="undefined") && $scope.filter.VehicleModelId!=null && (typeof $scope.filter.VehicleModelId!="undefined"))
		{
			StockViewFactory.getData($scope.filter).then(
				function(res){
					$scope.loading=false;
					$scope.grid.data = res.data.Result;
					return res.data.Result;
				},
				function(err){
					bsNotify.show(
						{
							title: "Gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		else if($scope.filter.OutletId==null || (typeof $scope.filter.OutletId=="undefined") || $scope.filter.VehicleModelId==null || (typeof $scope.filter.VehicleModelId=="undefined"))
		{
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Field mandatory harus diisi.",
					type: 'warning'
				}
			);
		}
		
        
    }

    StockViewFactory.getOutlet().then(
        function(res){
            $scope.getOutlet = res.data.Result;
            $scope.getOutletReq = res.data.Result;
			$scope.filter.OutletId=$scope.user.OutletId;a
            return res.data;
        }
    );

    ModelFactory.getData().then(
        function(res){
            $scope.optionsModel = res.data.Result;
            return res.data;
    });

    var listTipe = [];
    TipeFactory.getData().then(
        function(res){
            listTipe = res.data.Result;
            return res.data;
        }
    );

    $scope.pilihType = function (selected) {
        console.log("CCC", selected);
        ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected).then(
            function (res) {
            $scope.listWarna = res.data.Result;
        })
    };

    StockViewFactory.getYear().then(function(res){;
        $scope.dataYear = res.data.Result;
		$scope.filter.Year=$scope.dataYear[0].Year;
        return res.data;
    });

    $scope.filterModel = function(Selectmodel) {
        console.log("listTipe", listTipe);
        $scope.getTipe = listTipe.filter(function(test){
            return (test.VehicleModelId == Selectmodel.VehicleModelId);
        })
        var filterModel = $scope.listTipe.filter(function(obj){
            return obj.VehicleTypeId == $scope.mStockView.VehicleTypeId;
        })
        if(filterModel.length == 0) 
            $scope.mStockView.VehicleTypeId = null;
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    
    $scope.Detail = function(SelectData){
        $scope.detail = SelectData;
        console.log("Select", $scope.detail);
        angular.element('.ui.modal.show_modaldetailKendaraan').modal('show');
        //$scope.show_modaldetailKendaraan.show =! $scope.show_modaldetailKendaraan.show;
    }

    $scope.keluarModal = function (){
        angular.element('.ui.modal.show_modaldetailKendaraan').modal('hide');
    }
    
    // $scope.onListCancel = function(item){
    //     console.log("cancel_modal"); 
    // }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
        $scope.detail = rows;
        if(rows.length > 0){
            if(rows[0].StatusStockName.toLowerCase() == "free" && rows[0].StatusUnitName.toLowerCase() == "on stock"){
                $scope.requestSwappingDisabled = false;
            }else{
                $scope.requestSwappingDisabled = true;
            }
        }
        
    }

    var noSpkClicked = '<i title="Detail" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.Detail(row.entity)" ></i>';
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'StockViewId', field:'StockViewId', width:'7%', visible:false },
            { name:'Branch', field:'OutletName', width:'14%'}, 
            { name:'Model', field:'VehicleModelName', width:'8%'}, 
            { name:'Tipe', width: '20%',  cellTemplate: '<p style="margin:5px 0 0 5px;font-size:12px">{{row.entity.VehicleTypeDescription}} - {{row.entity.KatashikiCode}} - {{row.entity.SuffixCode}}</p>' },
            { name:'Warna', field:'ColorName', width:'13%'},
            { name:'Tahun Kendaraan', field:'ProductionYear', width:'9%'},
			{ name:'RRN', field:'RRNNo', width:'8%'},
			{ name:'No. Rangka', field:'FrameNo', width:'12%'},
			{ name:'PLOD',enableFiltering: false,visible:true, field:'PLOD', width:'7%', cellFilter: 'date:\'dd-MM-yyyy\'',type: 'number'},
			{ name:'Do tam',enableFiltering: false,visible:true, field:'DOTAMDate', width:'11%', cellFilter: 'date:\'dd-MM-yyyy\'',type: 'number'},
			{ name:'Aging', field:'Aging', width:'7%',type: 'number'},
            { name:'Kategori', field:'StatusUnitName', width:'8%'},
			{ name:'Status', field:'StatusStockName', width:'8%'},
            { name:'Lokasi', field:'LastLocation', width:'17%'},
            {
                name: 'action',
                allowCellFocus: false,
                width: '8%',
                pinnedRight: true,
				enableFiltering: false,
				visible:true,
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: noSpkClicked
            }
        ]
    };

    $scope.btnRequestSwapping = function(){
        StockViewFactory.getValidateRequestSwapping($scope.detail[0].FrameNo).then(
            function (res) {
                if(res.data == false){
                    angular.element('.ui.modal.ValidationSwapping').modal('show');
                }else{
                    angular.element('.ui.modal.show_modalRequestSwapping').modal('show');
                    console.log($scope.user)
                    $scope.BranchReq = [];
                    var CompanyId;
                    $scope.ctRequestSwapping = null;
                    if($scope.filter.OutletId != null || $scope.filter.OutletId != undefined || $scope.filter.OutletId != ""){
                        for(var i = 0; i < $scope.getOutletReq.length; i++){
                            if($scope.getOutletReq[i].OutletId == $scope.filter.OutletId){
                                CompanyId = $scope.getOutletReq[i].CompanyId;
                            }
                        }
                        for(var i = 0; i < $scope.getOutletReq.length; i++){
                            if($scope.getOutletReq[i].OutletId != $scope.filter.OutletId && $scope.getOutletReq[i].CompanyId == CompanyId){
                                $scope.BranchReq.push($scope.getOutletReq[i]);
                            }
                        }
                    }
                }
            },
            function(err){
                bsNotify.show({
                    title: "Warning Message",
                    content: err,
                    type: 'warning'
                });
                return;
            }
        );

        // angular.element('.ui.modal.show_modalRequestSwapping').modal('show');
    }

    $scope.keluarModalRequest = function (){
        angular.element('.ui.modal.show_modalRequestSwapping').modal('hide');
    }

    $scope.simpanRequestSwapping = function(){
        if($scope.ctRequestSwapping == null || $scope.ctRequestSwapping == ""){
            bsNotify.show({
                title: "Warning Message",
                content: "Cabang Tujuan Harus diisi",
                type: 'warning'
            });
            return;
        }
        $scope.data = {
            OutletId: $scope.detail[0].OutletId,
            FrameNo: $scope.detail[0].FrameNo,
            ProductionYear: $scope.detail[0].ProductionYear,
            VehicleTypeColorId: $scope.detail[0].VehicleTypeColorId,
            OutletIdTarget: $scope.ctRequestSwapping
        }
        StockViewFactory.createRequestSwapping($scope.data).then(
            function (res) {
                angular.element('.ui.modal.show_modalRequestSwapping').modal('hide');
                bsNotify.show({
                    title: "Success Message",
                    content: "Data Berhasil di Simpan",
                    type: 'success'
                });
                $scope.getData();
                $scope.requestSwappingDisabled = true;
            },
            function(err){
                bsNotify.show({
                    title: "Warning Message",
                    content: err.data.Message,
                    type: 'warning'
                });
                $scope.requestSwappingDisabled = true;
                return;
            }
        );
        
    }

    $scope.ValidationSwapping = function(){
        angular.element('.ui.modal.ValidationSwapping').modal('hide');
    }

    $scope.checkCabangTujuan = function(){
        if($scope.ctRequestSwapping == $scope.filter.OutletId){
            // bsNotify.show({
            //     title: "Warning Message",
            //     content: "Cabang Tujuan Tidak Boleh Sama Dengan Cabang Sekarang",
            //     type: 'warning'
            // });
            $scope.validasiCabangSama = true;
            // return;
        }else{
            $scope.validasiCabangSama = false;
        }
    }
});