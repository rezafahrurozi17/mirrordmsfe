angular.module('app')
.controller('BoardSalesController', function($scope,$interval,$compile, $http,$filter, CurrentUser, BoardSalesFactory,$timeout, Idle) {
	$scope.TodayDateBoardSales=new Date();
	StartTimeBoardSales();
	var PromiseBoardSalesMonitoringStockPageNaek;
	var PromiseBoardSalesMonitoringStockRefreshAll;
	$scope.CurrentPageBoardSalesMonitoringStock=1;
	$scope.MaxPageBoardSalesMonitoringStock=0;
	
	$scope.StartBoardSalesStockMonitoring = function() {
      // stops any running interval to avoid two intervals running at the same time
      $scope.StopBoardSales();

		// store the interval PromiseBoardSalesMonitoringStockRefreshAll
      PromiseBoardSalesMonitoringStockRefreshAll = $interval(function(){
					$scope.CurrentPageBoardSalesMonitoringStock=angular.copy(1);
					$scope.RefreshBoardSalesMonitoringStock();
				},300000);
      
      // store the interval PromiseBoardSalesMonitoringStockPageNaek
      PromiseBoardSalesMonitoringStockPageNaek = $interval(function(){
					$scope.BoardSales_MonitoringStockPageClicked($scope.CurrentPageBoardSalesMonitoringStock);
				},10000);
    };
  
    // stops the interval
    $scope.StopBoardSales = function() {
      $interval.cancel(PromiseBoardSalesMonitoringStockPageNaek);
	  $interval.cancel(PromiseBoardSalesMonitoringStockRefreshAll);
    };
  
    
	$scope.$on('$viewContentLoaded', function() {
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
    });
 
    // stops the interval when the scope is destroyed,
    // this usually happens when a route is changed and 
    // the ItemsController $scope gets destroyed. The
    // destruction of the ItemsController scope does not
    // guarantee the stopping of any intervals, you must
    // be responsible for stopping it when the scope is
    // is destroyed.
    $scope.$on('$destroy', function() {
      $scope.StopBoardSales();
	  Idle.watch();
    });
				
	$scope.RefreshBoardSalesMonitoringStock = function () {
		BoardSalesFactory.getData($scope.CurrentPageBoardSalesMonitoringStock).then(
			function(res){
				$scope.BoardSales_DataMonitoringStock_Data = res.data.Result;
				$scope.BoardSales_DataMonitoringStock_LastModifiedDate=new Date();
				$scope.MaxPageBoardSalesMonitoringStock=Math.ceil(angular.copy(res.data.Total/10));
				var Populate_Page = angular.element( document.querySelector( '#BoardSales_MonitoringStockPage' ));
				Populate_Page.html('');
				for(var x = 1; x <= $scope.MaxPageBoardSalesMonitoringStock; ++x)
				{
					Populate_Page.append('<a id="BoardSalesSelectedPage'+x+'" ng-click="BoardSales_MonitoringStockPageClicked('+x+')">'+x+'</a>  '); 
				}
				$compile(Populate_Page)($scope);
				
				for(var x = 1; x <= $scope.MaxPageBoardSalesMonitoringStock; ++x)
				{
					WarnainBoardSalesPageGaAktif(x);
				}
				WarnainBoardSalesPageAktif(1);
				return res.data;
			}
		);
		
		BoardSalesFactory.getModelBawah().then(
			function(res){
				$scope.BoardSales_MonitoringStockFreeModel_Data = res.data.Result;
				var Populate_ModelBawah = angular.element( document.querySelector( '#BoardSales_MonitoringStockModelBawah' ));			
				Populate_ModelBawah.html('');
				for(var x = 0; x < $scope.BoardSales_MonitoringStockFreeModel_Data.length; ++x)
				{
					
					if(x==($scope.BoardSales_MonitoringStockFreeModel_Data.length-1))
					{
						Populate_ModelBawah.append('<Label style="font-size: 17px;">'+$scope.BoardSales_MonitoringStockFreeModel_Data[x].VehicleModelName+'</label>:<label style="font-size: 17px;">'+$scope.BoardSales_MonitoringStockFreeModel_Data[x].Total+'  </label>');
					}
					else
					{
						Populate_ModelBawah.append('<Label style="font-size: 17px;">'+$scope.BoardSales_MonitoringStockFreeModel_Data[x].VehicleModelName+'</label>:<label style="font-size: 17px;">'+$scope.BoardSales_MonitoringStockFreeModel_Data[x].Total+' |&nbsp; </label>');
					}					
				}
				return res.data;
			}
		);
	}
				


				
				
	
	
	
	$scope.BoardSales_MonitoringStockPageClicked = function (ThePage) {
		$scope.CurrentPageBoardSalesMonitoringStock=angular.copy(ThePage);
		
		for(var x = 1; x <= $scope.MaxPageBoardSalesMonitoringStock; ++x)
		{
			WarnainBoardSalesPageGaAktif(x);
		}
		
		WarnainBoardSalesPageAktif(ThePage);
		
		BoardSalesFactory.getData(ThePage).then(
			function(res){
				$scope.BoardSales_DataMonitoringStock_Data = res.data.Result;
				return res.data;
			}
		);

		//bawah ini buat inkremen doang kog by 1 page
		$scope.CurrentPageBoardSalesMonitoringStock++;
		//reset kalo uda page max
		if($scope.CurrentPageBoardSalesMonitoringStock>$scope.MaxPageBoardSalesMonitoringStock)
		{
			$scope.CurrentPageBoardSalesMonitoringStock=angular.copy(1);
		}
		
	}
	
	// starting the interval by default
    $scope.StartBoardSalesStockMonitoring();
	$scope.RefreshBoardSalesMonitoringStock();
	
});
