angular.module('app')
  .factory('BoardSalesFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function(PageNumber) {
		var da_start=((PageNumber-1)*10)+1;
		var res=$http.get('/api/sales/StockMonitoring?start='+da_start+'&limit=10');
		//var res=$http.get('/api/sales/StockMonitoring?start='+da_start+'&limit=10&FilterData=HeaderStatusApprovalName|Diajukan');
		return res;
        
      },
	  
	  getModelBawah: function() {
		var res=$http.get('/api/sales/StockMonitoringTotal');
		return res;
        
      },
	  
      create: function(ApprovalDiskonSpk) {
        return $http.post('/api/sales/StockMonitoring', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      update: function(ApprovalDiskonSpk){
        return $http.put('/api/sales/StockMonitoring', [{
                                            SalesProgramId: ApprovalDiskonSpk.SalesProgramId,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/StockMonitoring',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd