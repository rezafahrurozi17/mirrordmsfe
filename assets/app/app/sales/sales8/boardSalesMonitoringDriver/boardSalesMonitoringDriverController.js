angular.module('app')
    .controller('BoardSalesMonitoringDriverController', function($scope, $interval, $http, $filter, CurrentUser, BoardSalesMonitoringDriverFactory, $timeout, Idle) {
        $scope.TodayDateBoardSalesMonitoringDriver_Raw = new Date();
        StartTimeBoardSalesMonitoringDriver();
        initialize(1, 10);
        $scope.TodayDateMinus1_Raw = angular.copy(new Date($scope.TodayDateBoardSalesMonitoringDriver_Raw.setDate($scope.TodayDateBoardSalesMonitoringDriver_Raw.getDate() - 1)));
        $scope.TodayDateMinus1 = $scope.TodayDateMinus1_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDateMinus1_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDateMinus1_Raw.getDate()).slice(-2);
        var tempToday = new Date();
        var tempTodayPlus1 = new Date();
        var tempTodayPlus2 = new Date();
        var tempTodayPlus3 = new Date();
        var tempTodayPlus4 = new Date();
        var tempTodayMinus2 = new Date();
        $scope.TodayDate = angular.copy(new Date(tempToday.setDate(tempToday.getDate())));
        $scope.TodayDate = $scope.TodayDate.getFullYear() + '-' + ('0' + ($scope.TodayDate.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate.getDate()).slice(-2);

        $scope.TodayDatePlus1 = angular.copy(new Date(tempTodayPlus1.setDate(tempTodayPlus1.getDate() + 1)));
        $scope.TodayDatePlus1 = $scope.TodayDatePlus1.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus1.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus1.getDate()).slice(-2);

        $scope.TodayDatePlus2 = angular.copy(new Date(tempTodayPlus2.setDate(tempTodayPlus2.getDate() + 2)));
        $scope.TodayDatePlus2 = $scope.TodayDatePlus2.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus2.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus2.getDate()).slice(-2);

        $scope.TodayDatePlus3 = angular.copy(new Date(tempTodayPlus3.setDate(tempTodayPlus3.getDate() + 3)));
        $scope.TodayDatePlus3 = $scope.TodayDatePlus3.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus3.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus3.getDate()).slice(-2);

        $scope.TodayDatePlus4 = angular.copy(new Date(tempTodayPlus4.setDate(tempTodayPlus4.getDate() + 4)));
        $scope.TodayDatePlus4 = $scope.TodayDatePlus4.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus4.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus4.getDate()).slice(-2);

        $scope.TodayDateMinus2 = angular.copy(new Date(tempTodayMinus2.setDate(tempTodayMinus2.getDate() - 2)));
        $scope.TodayDateMinus2 = $scope.TodayDateMinus2.getFullYear() + '-' + ('0' + ($scope.TodayDateMinus2.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDateMinus2.getDate()).slice(-2);


        $scope.BoardSalesMonitoringDriverOutletName = CurrentUser.user().OrgName;

        function initialize(start, limit) {
            BoardSalesMonitoringDriverFactory.getData('start=' + start + '&limit=' + limit).then(function(res) {
                $scope.BoardData = res.data.Result;
                $scope.Calculation = angular.copy($scope.BoardData[0]);


                var i = 0;
                var j = 0;

                for (var LoopTanggal = 0; LoopTanggal < 5; LoopTanggal++) {
                    i = 0;
                    for (k in $scope.BoardData[LoopTanggal].DetailMonitoringDriver) {
                        if (($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 6 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 8)) {
                            i = 0;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }

                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 8 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 10) {
                            i = 1;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 10 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 12) {
                            i = 2;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 12 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 14) {
                            i = 3;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 14 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 16) {
                            i = 4;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 16 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 18) {
                            i = 5;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 18 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 20) {
                            i = 6;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 20 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 22) {
                            i = 7;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        } else
                        if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() >= 22 && $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].ChangeDeliveryTime.getHours() < 24) {
                            i = 8;
                            var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + i + '_T' + j));
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanCustomer') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#D2E6C9;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanPDC') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#A0D5EC;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FCCFBA;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanRepair') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CA9FC4;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengirimanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#CEA668;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'PengambilanKaroseri') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:#FFF57F;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            } else
                            if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].TujuanPengirimanName == 'Others') {
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'Delay') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:red;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                } else 
                                if ($scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].StatusTask == 'OnTime') {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><span class="fa fa-circle" style="font-size:10px;color:green;float:right;"></span><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }else {
                                    myEl.append('<div style="background-color:grey;height:auto;float:left;width:48%;font-size: 9px;margin-left:1px;margin-right:1px;margin-top:1px;margin-bottom:1px;"><div><b>' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].DriverPDSName + '-</div><div style="font-size: 7px">' + $scope.BoardData[LoopTanggal].DetailMonitoringDriver[k].FrameNo + '</div></b></div>');
                                }
                            }
                        }
                    }
                    j += 1;
                }
            });
        }
        

        for (var Loop_Celeng_Tanggal = 0; Loop_Celeng_Tanggal < 5; Loop_Celeng_Tanggal++) {
            for (var Loop_Celeng_Waktu = 0; Loop_Celeng_Waktu < 9; Loop_Celeng_Waktu++) {
                var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + Loop_Celeng_Waktu + '_T' + Loop_Celeng_Tanggal));
                myEl.html('');
            }
        }

        
        $scope.DisabledNext = true;
        $scope.DisabledPrev = true;
        $scope.Step = 1;
        $scope.BoardSales_MonitoringDriver_PreviousDate = function() {
            $scope.Step = $scope.Step - 1;



            var tampungDateMin1 = angular.copy(new Date($scope.TodayDateMinus1));
            var tampungDate = angular.copy(new Date($scope.TodayDate));
            var tampungDatePlus1 = angular.copy(new Date($scope.TodayDatePlus1));
            var tampungDateMin2 = angular.copy(new Date($scope.TodayDatePlus2));
            var tampungDateMin3 = angular.copy(new Date($scope.TodayDatePlus3));

            $scope.TodayDateMinus1 = angular.copy(new Date(tampungDateMin1.setDate(tampungDateMin1.getDate() - 5)));
            $scope.TodayDateMinus1 = $scope.TodayDateMinus1.getFullYear() + '-' + ('0' + ($scope.TodayDateMinus1.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDateMinus1.getDate()).slice(-2);

            $scope.TodayDate = angular.copy(new Date(tampungDate.setDate(tampungDate.getDate() - 5)));
            $scope.TodayDate = $scope.TodayDate.getFullYear() + '-' + ('0' + ($scope.TodayDate.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate.getDate()).slice(-2);

            $scope.TodayDatePlus1 = angular.copy(new Date(tempTodayPlus1.setDate(tempTodayPlus1.getDate() - 5)));
            $scope.TodayDatePlus1 = $scope.TodayDatePlus1.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus1.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus1.getDate()).slice(-2);

            $scope.TodayDatePlus2 = angular.copy(new Date(tempTodayPlus2.setDate(tempTodayPlus2.getDate() - 5)));
            $scope.TodayDatePlus2 = $scope.TodayDatePlus2.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus2.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus2.getDate()).slice(-2);

            $scope.TodayDatePlus3 = angular.copy(new Date(tempTodayPlus3.setDate(tempTodayPlus3.getDate() - 5)));
            $scope.TodayDatePlus3 = $scope.TodayDatePlus3.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus3.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus3.getDate()).slice(-2);

            if ($scope.Step == 0) {

                $scope.DisabledPrev = false;
                $scope.BoardData = [];
                for (var Loop_Celeng_Tanggal = 0; Loop_Celeng_Tanggal < 5; Loop_Celeng_Tanggal++) {
                    for (var Loop_Celeng_Waktu = 0; Loop_Celeng_Waktu < 9; Loop_Celeng_Waktu++) {
                        var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + Loop_Celeng_Waktu + '_T' + Loop_Celeng_Tanggal));
                        myEl.html('');
                    }
                }
                initialize(-4, 10);

            }
            if ($scope.Step == 1) {
                $scope.BoardData = [];
                for (var Loop_Celeng_Tanggal = 0; Loop_Celeng_Tanggal < 5; Loop_Celeng_Tanggal++) {
                    for (var Loop_Celeng_Waktu = 0; Loop_Celeng_Waktu < 9; Loop_Celeng_Waktu++) {
                        var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + Loop_Celeng_Waktu + '_T' + Loop_Celeng_Tanggal));
                        myEl.html('');
                    }
                }
                initialize(1, 10);

                $scope.DisabledPrev = true;
            } else {
                $scope.DisabledPrev = true;

            }
        }


        $scope.BoardSales_MonitoringDriver_NextDate = function() {
            $scope.Step = $scope.Step + 1;
            var tampungDateMin1 = angular.copy(new Date($scope.TodayDateMinus1));
            var tampungDate = angular.copy(new Date($scope.TodayDate));
            var tampungDatePlus1 = angular.copy(new Date($scope.TodayDatePlus1));
            var tampungDateMin2 = angular.copy(new Date($scope.TodayDatePlus2));
            var tampungDateMin3 = angular.copy(new Date($scope.TodayDatePlus3));

            $scope.TodayDateMinus1 = angular.copy(new Date(tampungDateMin1.setDate(tampungDateMin1.getDate() + 5)));
            $scope.TodayDateMinus1 = $scope.TodayDateMinus1.getFullYear() + '-' + ('0' + ($scope.TodayDateMinus1.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDateMinus1.getDate()).slice(-2);

            $scope.TodayDate = angular.copy(new Date(tampungDate.setDate(tampungDate.getDate() + 5)));
            $scope.TodayDate = $scope.TodayDate.getFullYear() + '-' + ('0' + ($scope.TodayDate.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate.getDate()).slice(-2);

            $scope.TodayDatePlus1 = angular.copy(new Date(tempTodayPlus1.setDate(tempTodayPlus1.getDate() + 5)));
            $scope.TodayDatePlus1 = $scope.TodayDatePlus1.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus1.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus1.getDate()).slice(-2);

            $scope.TodayDatePlus2 = angular.copy(new Date(tempTodayPlus2.setDate(tempTodayPlus2.getDate() + 5)));
            $scope.TodayDatePlus2 = $scope.TodayDatePlus2.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus2.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus2.getDate()).slice(-2);

            $scope.TodayDatePlus3 = angular.copy(new Date(tempTodayPlus3.setDate(tempTodayPlus3.getDate() + 5)));
            $scope.TodayDatePlus3 = $scope.TodayDatePlus3.getFullYear() + '-' + ('0' + ($scope.TodayDatePlus3.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDatePlus3.getDate()).slice(-2);

            if ($scope.Step == 2) {

                $scope.DisabledNext = false;
                $scope.BoardData = [];
                for (var Loop_Celeng_Tanggal = 0; Loop_Celeng_Tanggal < 5; Loop_Celeng_Tanggal++) {
                    for (var Loop_Celeng_Waktu = 0; Loop_Celeng_Waktu < 9; Loop_Celeng_Waktu++) {
                        var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + Loop_Celeng_Waktu + '_T' + Loop_Celeng_Tanggal));
                        myEl.html('');
                    }
                }
                initialize(6, 10);

            }
            if ($scope.Step == 1) {
                $scope.BoardData = [];
                for (var Loop_Celeng_Tanggal = 0; Loop_Celeng_Tanggal < 5; Loop_Celeng_Tanggal++) {
                    for (var Loop_Celeng_Waktu = 0; Loop_Celeng_Waktu < 9; Loop_Celeng_Waktu++) {
                        var myEl = angular.element(document.querySelector('#Board_MonitoringDriver_W' + Loop_Celeng_Waktu + '_T' + Loop_Celeng_Tanggal));
                        myEl.html('');
                    }
                }
                initialize(1, 10);
                $scope.DisabledNext = true;
            } else {
                $scope.DisabledNext = true;

            }



        }

        $scope.$on('$viewContentLoaded', function() {
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)
        });
        
        $scope.$on('$destroy', function() {
            Idle.watch();
        });

    });