angular.module('app')
  .factory('BoardSalesMonitoringDriverFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
    //   getData: function(PageNumber) {
    // var da_start=((PageNumber-1)*10)+1;
    getData: function(param) {
    //var res=$http.get('/api/sales/MonitoringDriver?start='+da_start+'&limit=1000');
    var res=$http.get('/api/sales/MonitoringDriver?'+param);
		//var res=$http.get('/api/sales/MonitoringDriver?start='+da_start+'&limit=10&FilterData=HeaderStatusApprovalName|Diajukan');
		return res;
        
      },
	  
	  getSummaryAtas: function() {
		var res=$http.get('/api/sales/StockMonitoringTotal');
		return res;
        
      },
	  
      create: function(ApprovalDiskonSpk) {
        return $http.post('/api/sales/SpkApprovalDiscount', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      update: function(ApprovalDiskonSpk){
        return $http.put('/api/sales/SpkApprovalDiscount', [{
                                            SalesProgramId: ApprovalDiskonSpk.SalesProgramId,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SpkApprovalDiscount',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd