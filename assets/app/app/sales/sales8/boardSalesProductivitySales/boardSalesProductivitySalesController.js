angular.module('app')
.controller('BoardSalesProductivitySalesController', function($scope,$interval,$compile, $http,$filter, CurrentUser, BoardSalesProductivitySalesFactory,$timeout, Idle) {
	$scope.BoardSalesProductivitySalesDaMonth=new Date();
	StartTimeBoardSalesProductivitySales();
	$scope.AssignWarna = [];
	$scope.HardCodedWarna = [{"Warna":"rgb(153, 204, 255)"},{"Warna":"rgb(255, 204, 102)"},{"Warna":"rgb(102, 255, 51)"},{"Warna":"rgb(255, 204, 204)"},{"Warna":"rgb(153, 153, 102)"},{"Warna":"rgb(255, 153, 51)"},{"Warna":"rgb(255, 204, 153)"},{"Warna":"rgb(51, 153, 255)"},{"Warna":"rgb(128, 191, 255)"},{"Warna":"rgb(223, 128, 255)"},{"Warna":"rgb(255, 128, 128)"},{"Warna":"rgb(255, 255, 128)"},{"Warna":"rgb(191, 255, 128)"},{"Warna":"rgb(204, 102, 153)"},{"Warna":"rgb(204, 204, 0)"},{"Warna":"rgb(234, 124, 95)"},{"Warna":"rgb(204, 102, 0)"},{"Warna":"rgb(102, 153, 153)"},{"Warna":"rgb(209, 224, 224)"},{"Warna":"rgb(0, 179, 179)"}];
	var Da_warna = [];
	
	$scope.getUniqueArray = function (array) {
		var result = [];
		for(var x = 0; x < array.length; x++){
		if(result.indexOf(array[x].Golongan) == -1)
			result.push(array[x].Golongan);
		}
		return result;
	};
	
	var PromiseBoardSalesProductivitySalesPageNaek;
	var PromiseBoardSalesProductivitySalesRefreshAll;
	$scope.CurrentPageBoardSalesProductivitySales=1;
	$scope.MaxPageBoardSalesProductivitySales=0;
	
	$scope.StartBoardSalesProductivitySales = function() {
      // stops any running interval to avoid two intervals running at the same time
      $scope.StopBoardSalesProductivitySales();

		// store the interval PromiseBoardSalesProductivitySalesRefreshAll
      PromiseBoardSalesProductivitySalesRefreshAll = $interval(function(){
					$scope.CurrentPageBoardSalesProductivitySales=angular.copy(1);
					$scope.RefreshBoardSalesProductivitySales();
				},300000);
      
      // store the interval PromiseBoardSalesProductivitySalesPageNaek
      PromiseBoardSalesProductivitySalesPageNaek = $interval(function(){
					$scope.BoardSales_ProductivitySalesPageClicked($scope.CurrentPageBoardSalesProductivitySales);
				},10000);
    };
  
    // stops the interval
    $scope.StopBoardSalesProductivitySales = function() {
      $interval.cancel(PromiseBoardSalesProductivitySalesPageNaek);
	  $interval.cancel(PromiseBoardSalesProductivitySalesRefreshAll);
    };

	$scope.$on('$viewContentLoaded', function() {
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
    });

    // stops the interval when the scope is destroyed,
    // this usually happens when a route is changed and 
    // the ItemsController $scope gets destroyed. The
    // destruction of the ItemsController scope does not
    // guarantee the stopping of any intervals, you must
    // be responsible for stopping it when the scope is
    // is destroyed.
    $scope.$on('$destroy', function() {
      $scope.StopBoardSalesProductivitySales();
	  Idle.watch();
    });
	
	$scope.RefreshBoardSalesProductivitySales = function () {
		BoardSalesProductivitySalesFactory.getData($scope.CurrentPageBoardSalesProductivitySales).then(
			function(res){
				$scope.BoardSales_ProductivitySales_Data = res.data.Result;
				$scope.BoardSales_ProductivitySales_LastModifiedDate=new Date();
				
				$scope.BoardSales_ProductivitySales_Data=$filter('orderBy')($scope.BoardSales_ProductivitySales_Data, ['Golongan','Rank'], false);
				$scope.Macem_Kategori=$scope.getUniqueArray($scope.BoardSales_ProductivitySales_Data);

				
				for(var i = 0; i < $scope.Macem_Kategori.length; i++)
				{	
					Da_warna.push($scope.HardCodedWarna[i]['Warna']);
				}
				
				for(var i = 0; i < $scope.BoardSales_ProductivitySales_Data.length; i++)
				{
					for(var x = 0; x < $scope.Macem_Kategori.length; x++)
					{
						if($scope.BoardSales_ProductivitySales_Data[i].Golongan==$scope.Macem_Kategori[x])
						{
							$scope.AssignWarna[i]=Da_warna[x];
						}
					}
				}
				
				$scope.MaxPageBoardSalesProductivitySales=Math.ceil(angular.copy(res.data.Total/10));
				var Populate_Page = angular.element( document.querySelector( '#BoardSales_ProductivitySalesPage' ));
				Populate_Page.html('');
				for(var x = 1; x <= $scope.MaxPageBoardSalesProductivitySales; ++x)
				{
					Populate_Page.append('<a id="BoardSalesProductivitySalesSelectedPage'+x+'" ng-click="BoardSales_ProductivitySalesPageClicked('+x+')">'+x+'</a>  '); 
				}
				$compile(Populate_Page)($scope);
				for(var x = 1; x <= $scope.MaxPageBoardSalesProductivitySales; ++x)
				{
					WarnainBoardSalesProductivitySalesPageGaAktif(x);
				}
				WarnainBoardSalesProductivitySalesPageAktif(1);
				return res.data;
			}
		);
	}
	
	
		
	
	
	$scope.BoardSales_ProductivitySalesPageClicked = function (ThePage) {
		$scope.CurrentPageBoardSalesProductivitySales=angular.copy(ThePage);
		
		for(var x = 1; x <= $scope.MaxPageBoardSalesProductivitySales; ++x)
		{
			WarnainBoardSalesProductivitySalesPageGaAktif(x);
		}
		WarnainBoardSalesProductivitySalesPageAktif(ThePage);
		
		BoardSalesProductivitySalesFactory.getData(ThePage).then(
			function(res){
				$scope.BoardSales_ProductivitySales_Data = res.data.Result;
				$scope.BoardSales_ProductivitySales_Data=$filter('orderBy')($scope.BoardSales_ProductivitySales_Data, ['Golongan','Rank'], false);
				$scope.Macem_Kategori=$scope.getUniqueArray($scope.BoardSales_ProductivitySales_Data);
				
				for(var i = 0; i < $scope.Macem_Kategori.length; i++)
				{	
					Da_warna.push($scope.HardCodedWarna[i]['Warna']);
				}
				
				for(var i = 0; i < $scope.BoardSales_ProductivitySales_Data.length; i++)
				{
					for(var x = 0; x < $scope.Macem_Kategori.length; x++)
					{
						if($scope.BoardSales_ProductivitySales_Data[i].Golongan==$scope.Macem_Kategori[x])
						{
							$scope.AssignWarna[i]=Da_warna[x];
						}
					}
				}
				return res.data;
			}
		);

		//bawah ini buat inkremen doang kog by 1 page
		$scope.CurrentPageBoardSalesProductivitySales++;
		//reset kalo uda page max
		if($scope.CurrentPageBoardSalesProductivitySales>$scope.MaxPageBoardSalesProductivitySales)
		{
			$scope.CurrentPageBoardSalesProductivitySales=angular.copy(1);
		}
	}	
		
	// starting the interval by default
    $scope.StartBoardSalesProductivitySales();
	$scope.RefreshBoardSalesProductivitySales();
	
	
	
	
});
