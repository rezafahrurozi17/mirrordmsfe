angular.module('app')
  .factory('BoardSalesProductivitySalesFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function(PageNumber) {
		var da_start=((PageNumber-1)*10)+1;
		var res=$http.get('/api/sales/ProductivityBoard?start='+da_start+'&limit=10');
		//var res=$http.get('/api/sales/ProductivityBoard?start=1&limit=5&FilterData=HeaderStatusApprovalName|Diajukan');
		return res;
        
      },
	  
      create: function(ApprovalDiskonSpk) {
        return $http.post('/api/sales/ProductivityBoard', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      update: function(ApprovalDiskonSpk){
        return $http.put('/api/sales/ProductivityBoard', [{
                                            SalesProgramId: ApprovalDiskonSpk.SalesProgramId,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/ProductivityBoard',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd