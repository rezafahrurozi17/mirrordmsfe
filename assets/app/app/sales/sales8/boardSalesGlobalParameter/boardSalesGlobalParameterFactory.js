angular.module('app')
  .factory('BoardSalesGlobalParameterFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
	  getDataVehicleModel: function() {
        var res=$http.get('/api/sales/MUnitVehicleModelTomas'); 
        return res;
      },
	  getDataVehicleType: function() {
        var res=$http.get('/api/sales/MUnitVehicleTypeTomas');	  
        return res;
      },
	  getDataVehicleWarna: function(param) {
        var res=$http.get('/api/sales/MUnitVehicleTypeColorJoinTomas'+param);	  
        return res;
      },
      getDataBoardSalesProspect: function() {
                    var res=$http.get('/api/sales/GlobalParameterProspect');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesBoardPds: function() {
                    var res=$http.get('/api/sales/GlobalParameterMonitoringDriver');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesArOverdue: function() {
                    var res=$http.get('/api/sales/GlobalParameterAROverdue');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesSpkOutstanding: function() {
                    var res=$http.get('/api/sales/GlobalParameterSPKOutstanding');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesStockMonitoring: function() {
                    var res=$http.get('/api/sales/GlobalParameterStock');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesSalesTarget: function() {
                    var res=$http.get('/api/sales/SProspectingSalesTarget');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesPengaturanTampilan: function() {
                    var res=$http.get('/api/sales/GlobalParameterTampilanDashboard');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesTargetPenjualan: function() {
                    var res=$http.get('/api/sales/GlobalParameterTop5TargetSPKRS');
                    //console.log('res=>',res);
                return res;
      },
	
	  
	  
      create: function(ApprovalDiskonSpk) {
        return $http.post('/api/sales/StockMonitoring', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
      },
	  
      DataBoardSalesProspectupdate: function(DataBoardSalesProspect){
        return $http.put('/api/sales/GlobalParameterProspect', [{
                                            AgingPeriod1: DataBoardSalesProspect.AgingPeriod1,
											AgingPeriod2: DataBoardSalesProspect.AgingPeriod2,
											AgingPeriod3: DataBoardSalesProspect.AgingPeriod3,
											VehicleModelId: DataBoardSalesProspect.VehicleModelId,
											VehicleTypeId: DataBoardSalesProspect.VehicleTypeId,
                                            ColorId: DataBoardSalesProspect.ColorId}]);
      },
	  DataBoardSalesBoardPdsupdate: function(DataBoardSalesBoardPds){
        return $http.put('/api/sales/GlobalParameterMonitoringDriver', [{
                                            OnTimeRatio: DataBoardSalesBoardPds.OnTimeRatio}]);
      },
	  DataBoardSalesArOverdueupdate: function(DataBoardSalesArOverdue){
        return $http.put('/api/sales/GlobalParameterAROverdue', [{
                                            AgingPeriod1: DataBoardSalesArOverdue.AgingPeriod1,
											AgingPeriod2: DataBoardSalesArOverdue.AgingPeriod2,
                                            AgingPeriod3: DataBoardSalesArOverdue.AgingPeriod3}]);
      },
	  DataBoardSalesSpkOutstandingupdate: function(DataBoardSalesSpkOutstanding){
        return $http.put('/api/sales/GlobalParameterSPKOutstanding', [{
                                            AgingPeriod1: DataBoardSalesSpkOutstanding.AgingPeriod1,
											AgingPeriod2: DataBoardSalesSpkOutstanding.AgingPeriod2,
                                            AgingPeriod3: DataBoardSalesSpkOutstanding.AgingPeriod3}]);
      },
	  DataBoardSalesStockMonitoringupdate: function(DataBoardSalesStockMonitoringupdate){
		return $http.put('/api/sales/GlobalParameterStock',DataBoardSalesStockMonitoringupdate );									
      },
	  DataBoardSalesSalesTargetupdate: function(DataBoardSalesSalesTarget){
        return $http.put('/api/sales/SProspectingSalesTarget', [{
                                            ProspectToContactRatio: DataBoardSalesSalesTarget.ProspectToContactRatio,
											SPKToHotProspectRatio: DataBoardSalesSalesTarget.SPKToHotProspectRatio,
											HotProspectToProspectRatio: DataBoardSalesSalesTarget.HotProspectToProspectRatio,
											DOToSPKRatio: DataBoardSalesSalesTarget.DOToSPKRatio,
											SProspectingSalesTargetId: DataBoardSalesSalesTarget.SProspectingSalesTargetId,
											VehicleModelId: DataBoardSalesSalesTarget.VehicleModelId,
											Week1Ratio: DataBoardSalesSalesTarget.Week1Ratio,
											Week2Ratio: DataBoardSalesSalesTarget.Week2Ratio,
											Week3Ratio: DataBoardSalesSalesTarget.Week3Ratio,
                                            Week4Ratio: DataBoardSalesSalesTarget.Week4Ratio}]);
      },
	  DataBoardSalesPengaturanTampilanupdate: function(DataBoardSalesSalesTarget){
        return $http.put('/api/sales/GlobalParameterTampilanDashboard', [{
                                            AR: DataBoardSalesSalesTarget.AR,
											Prospecting: DataBoardSalesSalesTarget.Prospecting,
											ProspectingModel: DataBoardSalesSalesTarget.ProspectingModel,
											SPKOutstanding: DataBoardSalesSalesTarget.SPKOutstanding,
											SalesTarget: DataBoardSalesSalesTarget.SalesTarget,
											TargetPenjualanSPKRS: DataBoardSalesSalesTarget.TargetPenjualanSPKRS,
                                            Stock: DataBoardSalesSalesTarget.Stock}]);
      },
	  DataBoardSalesTargetPenjualanupdate: function(DataBoardSalesTargetPenjualan){
		return $http.put('/api/sales/GlobalParameterTop5TargetSPKRS',DataBoardSalesTargetPenjualan );									
      },
	  
      delete: function(id) {
        return $http.delete('/api/sales/StockMonitoring',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd