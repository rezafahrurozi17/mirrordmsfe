angular.module('app')
.controller('BoardSalesGlobalParameterController', function($scope, $http,$filter, CurrentUser, BoardSalesGlobalParameterFactory,$timeout,LocalService,$timeout,bsNotify) {
	$scope.Show_Hide_BoardSalesGlobalParameter_Board_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_Board = !$scope.Show_Hide_BoardSalesGlobalParameter_Board;
		};
		
	$scope.Show_Hide_BoardSalesGlobalParameter_Board_BoardPds_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_Board_BoardPds = !$scope.Show_Hide_BoardSalesGlobalParameter_Board_BoardPds;
		};	
		
	$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard = !$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard;
		};

	$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan = !$scope.Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan;
		};	
		
	$scope.Show_Hide_BoardSalesGlobalParameter_TargetPenjualan_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_TargetPenjualan = !$scope.Show_Hide_BoardSalesGlobalParameter_TargetPenjualan;
		};	
	
	$scope.Show_Hide_BoardSalesGlobalParameter_SpkOutstanding_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_SpkOutstanding = !$scope.Show_Hide_BoardSalesGlobalParameter_SpkOutstanding;
		};
		
	$scope.Show_Hide_BoardSalesGlobalParameter_SalesTarget_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_SalesTarget = !$scope.Show_Hide_BoardSalesGlobalParameter_SalesTarget;
		};

	$scope.Show_Hide_BoardSalesGlobalParameter_Prospecting_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_Prospecting = !$scope.Show_Hide_BoardSalesGlobalParameter_Prospecting;
		};
		
	$scope.Show_Hide_BoardSalesGlobalParameter_Ar_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_Ar = !$scope.Show_Hide_BoardSalesGlobalParameter_Ar;
		};

	$scope.Show_Hide_BoardSalesGlobalParameter_Stock_Clicked = function () {
			$scope.Show_Hide_BoardSalesGlobalParameter_Stock = !$scope.Show_Hide_BoardSalesGlobalParameter_Stock;
		};
		
		
	//buat model	
	BoardSalesGlobalParameterFactory.getDataVehicleModel().then(function (res) {
			$scope.optionsModelBoardSalesGlobalParameter = res.data.Result;
			return $scope.optionsModelBoardSalesGlobalParameter;
		});
	//buat tipe
	$scope.RawTipe = "";
		BoardSalesGlobalParameterFactory.getDataVehicleType().then(function (res) {
			$scope.RawTipe = res.data.Result;
			return $scope.RawTipe;
		}).then(function () {
				//buat ambil semua setting awal
				BoardSalesGlobalParameterFactory.getDataBoardSalesProspect().then(function (res) {
					

					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamProspecting={};
						$scope.BoardSalesGlobalParamselectedModelProspecting = {};
						$scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModel = {};
						$scope.BoardSalesGlobalParamselectedTipeProspecting = {};
						$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType = {};
						$scope.BoardSalesGlobalParamselectedwarnaProspecting = {};
						$scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna = {};
					}
					else
					{
						$scope.BoardSalesGlobalParamProspecting = res.data.Result[0];
						$scope.BoardSalesGlobalParamselectedModelProspecting = {};
						$scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModel = {};
						for (var i = 0; i < $scope.optionsModelBoardSalesGlobalParameter.length; ++i) {
							if ($scope.optionsModelBoardSalesGlobalParameter[i].VehicleModelId ==res.data.Result[0].VehicleModelId) {
								$scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModel = $scope.optionsModelBoardSalesGlobalParameter[i];
								break;
							}
						}
						$scope.filtertipeBoardSalesGlobalParameter($scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModel);
						$scope.BoardSalesGlobalParamselectedTipeProspecting = {};
						$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType = {};
						for (var i = 0; i < $scope.vehicleTypefilterBoardSalesGlobalParameter.length; ++i) {
							if ($scope.vehicleTypefilterBoardSalesGlobalParameter[i].VehicleTypeId == res.data.Result[0].VehicleTypeId) {
								$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType = $scope.vehicleTypefilterBoardSalesGlobalParameter[i];
								break;
							}
						}
						
						//$scope.filterColorBoardSalesGlobalParameter($scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType);
						$scope.BoardSalesGlobalParamselectedTipeProspecting.Description = $scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType.Description;
						$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleTypeId = $scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType.VehicleTypeId;
						
						$scope.BoardSalesGlobalParamProspecting.VehicleTypeId=$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType.VehicleTypeId;
						
						$scope.BoardSalesGlobalParamselectedwarnaProspecting = {};
						$scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna = {};
						BoardSalesGlobalParameterFactory.getDataVehicleWarna('?start=1&limit=100&filterData=VehicleTypeId|' + $scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType.VehicleTypeId).then(function (res) 
						{ 
							$scope.vehicleTypecolorfilterBoardSalesGlobalParameter = res.data.Result;
							for (var i = 0; i < $scope.vehicleTypecolorfilterBoardSalesGlobalParameter.length; ++i) {
								if ($scope.vehicleTypecolorfilterBoardSalesGlobalParameter[i].ColorId == $scope.BoardSalesGlobalParamProspecting.ColorId) {
									$scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna = $scope.vehicleTypecolorfilterBoardSalesGlobalParameter[i];
									break;
								}
							}
							$scope.selectedColorValueBoardSalesGlobalParameter($scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna);
						})
						return res.data;
					}
					
					
				});
			
				BoardSalesGlobalParameterFactory.getDataBoardSalesBoardPds().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamBoardPds ={};
					}
					else
					{
						$scope.BoardSalesGlobalParamBoardPds = res.data.Result[0];
					}
					
					return res.data;
				});
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesArOverdue().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamArOverdue = {};
					}
					else
					{
						$scope.BoardSalesGlobalParamArOverdue = res.data.Result[0];
					}
					
					return res.data;
				});
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesSpkOutstanding().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamSpkOutstanding = {};
					}
					else
					{
						$scope.BoardSalesGlobalParamSpkOutstanding = res.data.Result[0];
					}
					
					return res.data;
				});
				
				$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingChanged = function () {
					$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=true;
					if($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2>$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1 && $scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3>($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+1))
					{
						$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=false;
					}
					
				}
				
				$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingChanged_P1 = function () {
					$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2=angular.copy($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1+1);
					if($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3<=($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+2))
					{
						$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3=$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+2;
					}
					
					if((typeof $scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1=="undefined"))
					{
						$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1=0;
					}
					if((typeof $scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3=="undefined"))
					{
						$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3=$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+2;
					}
					
					$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=true;
					if($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2>$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1 && $scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3>($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+1))
					{
						$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=false;
					}
					
				}
				
				$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingChanged_P2 = function () {
					
					if($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3<=($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+2))
					{
						$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3=$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+2;
					}
					$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=true;
					if($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2>$scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod1 && $scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod3>($scope.BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+1))
					{
						$scope.BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman=false;
					}
					
				}
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesStockMonitoring().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamStockMonitoring = [];
					}
					else
					{
						$scope.BoardSalesGlobalParamStockMonitoring = res.data.Result;
					}
					
					return res.data;
				});
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesSalesTarget().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamSalesTarget ={};
					}
					else
					{
						$scope.BoardSalesGlobalParamSalesTarget = res.data.Result[0];
					}
					
					return res.data;
				});
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesPengaturanTampilan().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamPengaturanTampilan = {};
					}
					else
					{
						$scope.BoardSalesGlobalParamPengaturanTampilan = res.data.Result[0];
					}
					
					return res.data;
				});
				
				BoardSalesGlobalParameterFactory.getDataBoardSalesTargetPenjualan().then(function (res) {
					if(res.data.Result.length<=0)
					{
						$scope.BoardSalesGlobalParamTargetPenjualan = {};
					}
					else
					{
						$scope.BoardSalesGlobalParamTargetPenjualan = res.data.Result;
					}
					
					return res.data;
				});
		});

	$scope.BoardSalesGlobalParamselectedModelProspecting = null;
	$scope.BoardSalesGlobalParamselectedModelProspecting = {};
	$scope.BoardSalesGlobalParamselectedTipeProspecting = {};
	
	$scope.BoardSalesGlobalParameter_StockModelSelected = function (selected) {
		$scope.BoardSalesGlobalParameter_StockModelSelected_UniqueCheck=true;
		
		if((typeof $scope.BoardSalesGlobalParamStockMonitoring[0].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamStockMonitoring[1].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamStockMonitoring[2].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamStockMonitoring[3].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamStockMonitoring[4].VehicleModelId=="undefined")||$scope.BoardSalesGlobalParamStockMonitoring[0].VehicleModelId==null||$scope.BoardSalesGlobalParamStockMonitoring[1].VehicleModelId==null||$scope.BoardSalesGlobalParamStockMonitoring[2].VehicleModelId==null||$scope.BoardSalesGlobalParamStockMonitoring[3].VehicleModelId==null||$scope.BoardSalesGlobalParamStockMonitoring[4].VehicleModelId==null)
		{
			$scope.BoardSalesGlobalParameter_StockModelSelected_UniqueCheck=false;
		}
		else
		{
			for(var i = 0; i < $scope.BoardSalesGlobalParamStockMonitoring.length; ++i)
			{	
				for(var q = i+1; q < $scope.BoardSalesGlobalParamStockMonitoring.length; ++q)
				{
					if($scope.BoardSalesGlobalParamStockMonitoring[i].VehicleModelId==$scope.BoardSalesGlobalParamStockMonitoring[q].VehicleModelId)
					{
						$scope.BoardSalesGlobalParameter_StockModelSelected_UniqueCheck=false;
					}
				}
			}
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Ar_AgingChanged = function () {
		$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2>$scope.BoardSalesGlobalParamArOverdue.AgingPeriod1 && $scope.BoardSalesGlobalParamArOverdue.AgingPeriod3>($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Ar_AgingChanged_P1 = function () {
		$scope.BoardSalesGlobalParamArOverdue.AgingPeriod2=angular.copy($scope.BoardSalesGlobalParamArOverdue.AgingPeriod1+1);
		if($scope.BoardSalesGlobalParamArOverdue.AgingPeriod3<=($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=$scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamArOverdue.AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=$scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2>$scope.BoardSalesGlobalParamArOverdue.AgingPeriod1 && $scope.BoardSalesGlobalParamArOverdue.AgingPeriod3>($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Ar_AgingChanged_P2 = function () {
		
		if($scope.BoardSalesGlobalParamArOverdue.AgingPeriod3<=($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=$scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamArOverdue.AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamArOverdue.AgingPeriod3=$scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2>$scope.BoardSalesGlobalParamArOverdue.AgingPeriod1 && $scope.BoardSalesGlobalParamArOverdue.AgingPeriod3>($scope.BoardSalesGlobalParamArOverdue.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Ar_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Prospecting_AgingChanged = function () {
		$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamProspecting.AgingPeriod2>$scope.BoardSalesGlobalParamProspecting.AgingPeriod1 && $scope.BoardSalesGlobalParamProspecting.AgingPeriod3>($scope.BoardSalesGlobalParamProspecting.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Prospecting_AgingChanged_P1 = function () {
		$scope.BoardSalesGlobalParamProspecting.AgingPeriod2=angular.copy($scope.BoardSalesGlobalParamProspecting.AgingPeriod1+1);
		if($scope.BoardSalesGlobalParamProspecting.AgingPeriod3<=($scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod3=$scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamProspecting.AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamProspecting.AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod3=$scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamProspecting.AgingPeriod2>$scope.BoardSalesGlobalParamProspecting.AgingPeriod1 && $scope.BoardSalesGlobalParamProspecting.AgingPeriod3>($scope.BoardSalesGlobalParamProspecting.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Prospecting_AgingChanged_P2 = function () {
		
		if($scope.BoardSalesGlobalParamProspecting.AgingPeriod3<=($scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod3=$scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamProspecting.AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamProspecting.AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamProspecting.AgingPeriod3=$scope.BoardSalesGlobalParamProspecting.AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamProspecting.AgingPeriod2>$scope.BoardSalesGlobalParamProspecting.AgingPeriod1 && $scope.BoardSalesGlobalParamProspecting.AgingPeriod3>($scope.BoardSalesGlobalParamProspecting.AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Stock_AgingChanged = function () {
		$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2>$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1 && $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3>($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_Stock_AgingChanged_P1 = function () {
		$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2=angular.copy($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1+1);
		if($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3<=($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2>$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1 && $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3>($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=false;
		}
		
		
	}
	
	$scope.BoardSalesGlobalParameter_Stock_AgingChanged_P2 = function () {
		
		if($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3<=($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2;
		}
		
		if((typeof $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1=="undefined"))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1=0;
		}
		if((typeof $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=="undefined"))
		{
			$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+2;
		}
		
		$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=true;
		if($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2>$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1 && $scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3>($scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+1))
		{
			$scope.BoardSalesGlobalParameter_Stock_AgingAngkaGaAman=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_WeekChanged = function () {
		$scope.BoardSalesGlobalParameter_LebihDari100=false;
		
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.Week1Ratio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.Week1Ratio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.Week1Ratio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.Week2Ratio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.Week2Ratio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.Week2Ratio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.Week3Ratio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.Week3Ratio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.Week3Ratio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.Week4Ratio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.Week4Ratio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.Week4Ratio=0;
		}
		
		try
		{
			var setan=0;
			setan=$scope.BoardSalesGlobalParamSalesTarget.Week1Ratio+$scope.BoardSalesGlobalParamSalesTarget.Week2Ratio+$scope.BoardSalesGlobalParamSalesTarget.Week3Ratio+$scope.BoardSalesGlobalParamSalesTarget.Week4Ratio;
			if(setan>100)
			{
				$scope.BoardSalesGlobalParameter_LebihDari100=true;
			}
			else
			{
				$scope.BoardSalesGlobalParameter_LebihDari100=false;
			}
		}
		catch(ulala)
		{
			$scope.BoardSalesGlobalParameter_LebihDari100=false;
		}
		
	}
	
	$scope.BoardSalesGlobalParameter_KomposisiPercentageChanged = function () {
		$scope.BoardSalesGlobalParameter_LebihDari100_KomposisiPercentage=false;

		if((typeof $scope.BoardSalesGlobalParamSalesTarget.ProspectToContactRatio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.ProspectToContactRatio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.ProspectToContactRatio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.HotProspectToProspectRatio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.HotProspectToProspectRatio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.HotProspectToProspectRatio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.SPKToHotProspectRatio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.SPKToHotProspectRatio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.SPKToHotProspectRatio=0;
		}
		
		if((typeof $scope.BoardSalesGlobalParamSalesTarget.DOToSPKRatio=="undefined")||$scope.BoardSalesGlobalParamSalesTarget.DOToSPKRatio>100)
		{
			$scope.BoardSalesGlobalParamSalesTarget.DOToSPKRatio=0;
		}
		
		// try
		// {
			// var setan=0;
			// setan=$scope.BoardSalesGlobalParamSalesTarget.ProspectToContactRatio+$scope.BoardSalesGlobalParamSalesTarget.HotProspectToProspectRatio+$scope.BoardSalesGlobalParamSalesTarget.SPKToHotProspectRatio+$scope.BoardSalesGlobalParamSalesTarget.DOToSPKRatio;
			// if(setan>100)
			// {
				// $scope.BoardSalesGlobalParameter_LebihDari100_KomposisiPercentage=true;
			// }
			// else
			// {
				// $scope.BoardSalesGlobalParameter_LebihDari100_KomposisiPercentage=false;
			// }
		// }
		// catch(ulala)
		// {
			// $scope.BoardSalesGlobalParameter_LebihDari100_KomposisiPercentage=false;
		// }
		
	}
	
	
	
	$scope.BoardSalesGlobalParameter_TargetPenjualanSelected = function (selected) {
		$scope.BoardSalesGlobalParameter_TargetPenjualanSelected_UniqueCheck=true;
		
		if((typeof $scope.BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId=="undefined")||(typeof $scope.BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId=="undefined")||$scope.BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId==null||$scope.BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId==null||$scope.BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId==null||$scope.BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId==null||$scope.BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId==null)
		{
			$scope.BoardSalesGlobalParameter_TargetPenjualanSelected_UniqueCheck=false;
		}
		else
		{
			for(var i = 0; i < $scope.BoardSalesGlobalParamTargetPenjualan.length; ++i)
			{	
				for(var q = i+1; q < $scope.BoardSalesGlobalParamTargetPenjualan.length; ++q)
				{
					if($scope.BoardSalesGlobalParamTargetPenjualan[i].VehicleModelId==$scope.BoardSalesGlobalParamTargetPenjualan[q].VehicleModelId)
					{
						$scope.BoardSalesGlobalParameter_TargetPenjualanSelected_UniqueCheck=false;
					}
				}
			}
		}
		
		
		
	}
		

		$scope.filtertipeBoardSalesGlobalParameter = function (selected) {
			$scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModelId = selected.VehicleModelId;
			$scope.BoardSalesGlobalParamselectedModelProspecting.VehicleModelName = selected.VehicleModelName;
			$scope.BoardSalesGlobalParamProspecting.VehicleModelId=selected.VehicleModelId;
			$scope.vehicleTypefilterBoardSalesGlobalParameter = $scope.RawTipe.filter(function (type) 
			{ return (type.VehicleModelId == selected.VehicleModelId); })
			$scope.vehicleTypecolorfilterBoardSalesGlobalParameter=[];
			
			$scope.BoardSalesGlobalParamselectedTipeProspecting={};
			$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType=null;
			$scope.BoardSalesGlobalParamselectedTipeProspecting.Description = selected.Description;
			$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleTypeId = selected.VehicleTypeId;
			$scope.BoardSalesGlobalParamselectedwarnaProspecting={};
			$scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna=null;
			$scope.SelectedColor.ColorId = selected.ColorId;
			$scope.SelectedColor.ColorName = selected.ColorName;

		}

		$scope.BoardSalesGlobalParamselectedTipeProspecting = null;
		$scope.filterColorBoardSalesGlobalParameter = function (selected) {
			$scope.BoardSalesGlobalParamselectedTipeProspecting.Description = selected.Description;
			$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleTypeId = selected.VehicleTypeId;
			
			$scope.BoardSalesGlobalParamProspecting.VehicleTypeId=selected.VehicleTypeId;
			
			BoardSalesGlobalParameterFactory.getDataVehicleWarna('?start=1&limit=100&filterData=VehicleTypeId|' + selected.VehicleTypeId).then(function (res) 
			{ $scope.vehicleTypecolorfilterBoardSalesGlobalParameter = res.data.Result; })
		}
		
		$scope.BoardSalesGlobalParamselectedwarnaProspecting = null;
		$scope.SelectedColor = {};
		$scope.selectedColorValueBoardSalesGlobalParameter = function (selected) {
			
			$scope.SelectedColor.ColorId = selected.ColorId;
			$scope.SelectedColor.ColorName = selected.ColorName;
			
			$scope.BoardSalesGlobalParamProspecting.ColorId= selected.ColorId;
			

			
		}
				
		$scope.BoardSalesGlobalParameterSaveSetting = function () {
			 var promise = $timeout(BoardSalesGlobalParameterUbahSemua, 500);
			//  promise.then(BoardSalesGlobalParameterBilangSuksesDoang);
		};
		
		function BoardSalesGlobalParameterUbahSemua() { 
	
			if((typeof $scope.BoardSalesGlobalParamselectedTipeProspecting=="undefined")||$scope.BoardSalesGlobalParamselectedTipeProspecting==null||$scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType=="" || $scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType==null || (typeof $scope.BoardSalesGlobalParamselectedTipeProspecting.VehicleType=="undefined"))
			{
				$scope.BoardSalesGlobalParamProspecting.VehicleTypeId=null;
			}
			if($scope.BoardSalesGlobalParamProspecting.VehicleTypeId==null||$scope.BoardSalesGlobalParamProspecting.ColorId==null||(typeof $scope.BoardSalesGlobalParamProspecting.ColorId=="undefined")||$scope.BoardSalesGlobalParamselectedwarnaProspecting.Warna==null)
			{
				$scope.BoardSalesGlobalParamProspecting.ColorId=null;
			}
			// if($scope.BoardSalesGlobalParamProspecting.VehicleTypeId==null||$scope.BoardSalesGlobalParamProspecting.ColorId==null||(typeof $scope.BoardSalesGlobalParamProspecting.ColorId=="undefined"))
			// {
				// $scope.BoardSalesGlobalParamProspecting.ColorId=null;
			// }

			BoardSalesGlobalParameterFactory.DataBoardSalesProspectupdate($scope.BoardSalesGlobalParamProspecting).then(function () {
				BoardSalesGlobalParameterFactory.DataBoardSalesBoardPdsupdate($scope.BoardSalesGlobalParamBoardPds).then(function () {
					BoardSalesGlobalParameterFactory.DataBoardSalesArOverdueupdate($scope.BoardSalesGlobalParamArOverdue).then(function () {
						BoardSalesGlobalParameterFactory.DataBoardSalesSpkOutstandingupdate($scope.BoardSalesGlobalParamSpkOutstanding).then(function () {
							for (var i = 1; i < $scope.BoardSalesGlobalParamStockMonitoring.length; ++i) 
							{
								$scope.BoardSalesGlobalParamStockMonitoring[i].AgingPeriod1=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1;
								$scope.BoardSalesGlobalParamStockMonitoring[i].AgingPeriod2=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2;
								$scope.BoardSalesGlobalParamStockMonitoring[i].AgingPeriod3=$scope.BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3;
							}
							for (var i = 0; i < $scope.BoardSalesGlobalParamStockMonitoring.length; ++i) 
							{
								var da_thing=angular.copy(i);
								$scope.BoardSalesGlobalParamStockMonitoring[i].seq=da_thing+1;
								
							}
							
							BoardSalesGlobalParameterFactory.DataBoardSalesStockMonitoringupdate($scope.BoardSalesGlobalParamStockMonitoring).then(function () {
								BoardSalesGlobalParameterFactory.DataBoardSalesSalesTargetupdate($scope.BoardSalesGlobalParamSalesTarget).then(function () {
									BoardSalesGlobalParameterFactory.DataBoardSalesPengaturanTampilanupdate($scope.BoardSalesGlobalParamPengaturanTampilan).then(function () {
										BoardSalesGlobalParameterFactory.DataBoardSalesTargetPenjualanupdate($scope.BoardSalesGlobalParamTargetPenjualan).then(function () {
											BoardSalesGlobalParameterBilangSuksesDoang();
										});
									});
								});
							});
						});
					});
				});
			});

			return "Jalan";
		}
		
		function BoardSalesGlobalParameterBilangSuksesDoang() {
			bsNotify.show(
				{
					title: "Berhasil",
					content: "Data berhasil tersimpan.",
					type: 'success'
				}
			);
		}

		

	

	
});
