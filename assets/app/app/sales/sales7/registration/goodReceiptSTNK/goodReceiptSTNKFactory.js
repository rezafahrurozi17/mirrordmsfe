angular.module('app')
    .factory('goodReceiptSTNKFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());

                return fix;
            } else {
                return null;
            }

        };

        return {

            getData: function(filterStatusSTNKMain) {
                
                var param = $httpParamSerializer(filterStatusSTNKMain);
                var res = ''
                if( filterStatusSTNKMain.SuratJalanNo == null && filterStatusSTNKMain.ServiceBureauId == null && filterStatusSTNKMain.STNKGrStatusId == null && filterStatusSTNKMain.STNKGRCode == null && filterStatusSTNKMain.STNKGRDateStart == null && filterStatusSTNKMain.STNKGRDateEnd == null)
                {
                var res = $http.get('/api/sales/GRSTNKMaster/?start=1&limit=100' + param);
                }
                else
                {
                    param.EndSTNKGRDate = fixDate(param.EndSTNKGRDate);
                    param.StartSTNKGRDate = fixDate(param.StartSTNKGRDate);
                    console.log('param',param)
                var res = $http.get('/api/sales/GRSTNKMaster/?start=1&limit=100&' + param);
                }
                
                return res;
            },
            
            
            getDataLihatSTNKdetail: function(filter) {
                var res = $http.get('/api/sales/GRSTNKDetail/?STNKGRId=' + filter);
                return res;
            },

            getDataTambahSTNK: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/ListFrameNoGRSTNK/?' + param);
                return res;
            },

            getPStatusAfi: function() {
                var res = $http.get('/api/sales/PStatusAFI');
                return res;
            },

            getStatusGR: function() {
                var res = $http.get('/api/sales/PStatusGrSTNK');
                return res;
            },
            getDataVendor: function() {
                var res = $http.get('/api/sales/MProfileBiroJasa');
                return res;
            },

            create: function(goodReceiptSTNK) {
                return $http.post('/api/sales/GRSTNK/Insert', [{
                    SuratJalanNo: goodReceiptSTNK.SuratJalanNo,
                    Note: goodReceiptSTNK.Note,
                    listFrameNo: goodReceiptSTNK.listFrameNo
                }]);
            },
            batal: function(goodReceiptSTNK) {
                console.log('goodReceiptSTNK',goodReceiptSTNK);
                return $http.post('/api/sales/GRSTNK/Batal',[goodReceiptSTNK]);
            },
 
        }
    });
//ddd