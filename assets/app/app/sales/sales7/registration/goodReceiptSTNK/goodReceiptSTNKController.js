angular.module('app')
    .controller('goodReceiptSTNKController', function($scope, $http, CurrentUser, goodReceiptSTNKFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];

            $scope.loadingTambahSTNK = false;
            $scope.gridDataTambahSTNK = [];
            

            goodReceiptSTNKFactory.getStatusGR().then(function(res) {
                $scope.statusGRSTNK = res.data.Result; //status GR untuk combBox
                console.log('status GR STNK',statusGRSTNK);
            });
            goodReceiptSTNKFactory.getDataVendor().then(function (res) {
                $scope.vendor = res.data.Result; //birojasa = vendor
            });
            goodReceiptSTNKFactory.getPStatusAfi().then(function (res) {
            $scope.statusafi = res.data.Result;
        })


        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.goodReceiptSTNKOperation = '';
        $scope.user = CurrentUser.user();
        $scope.mgoodReceiptSTNK = null; //Model
        $scope.detailGRSTNK = [];
        $scope.daftarPO = true;
        $scope.fieldInput = {gridTambahGRSTNK:null, SuratJalanNo:null, Note:null};
        $scope.statusGRSTNK = { STNKGrStatusId: null, STNKGrStatusName: null, TotalData: null, LastModifiedDate: null, LastModifiedUserId: null, ServiceBureauId:null, ServiceBureauCode:null};
        $scope.filterStatusSTNKMain = { SuratJalanNo:null, ServiceBureauId:null, STNKGrStatusId:null, STNKGRCode:null, StartSTNKGRDate:null, EndSTNKGRDate:null};
        $scope.filterTambahStatusSTNK  = { StartReceiveSTNKDate:null, EndReceiveSTNKDate:null, ServiceBureauId:null };
        $scope.ShowGoodReceiptMain = true;
        $scope.ShowTambahGoodReceipt = false;
        $scope.ShowLihatGoodReceipt = false;
        $scope.disabledShowKonfirmasiGoodReceipt = false;
        $scope.disabledSimpanGRSTNK = false;

        $scope.dateOption ={
            format:'dd-MM-yyyy'
        }


        var currDate = new Date(); 
        $scope.filterStatusSTNKMain.StartSTNKGRDate = currDate;
        $scope.filterStatusSTNKMain.EndSTNKGRDate = currDate;

        // nowDate.getFullYear()+'-'+(nowDate.getMonth()+1)+'-'+nowDate.getDate();





        $scope.filterVendor = function(){
            if ($scope.goodReceiptSTNKOperation == 'tambah') {
                $scope.getDataTambahSTNK();

                $scope.filterStatusSTNKMain.ServiceBureauId = $scope.filterTambahStatusSTNK.ServiceBureauId;
                console.log('$scope.filterStatusSTNKMain',$scope.filterStatusSTNKMain);
            }
        }

        $scope.TambahGoodReceiptSTNK = function(){
            $scope.goodReceiptSTNKOperation = 'tambah';
            $scope.ShowTambahGoodReceipt = true;
            $scope.ShowGoodReceiptMain = false;
            $scope.ShowLihatGoodReceipt = false;
            $scope.gridTambahGRSTNK.data = [];
            $scope.selectRowgridTambahGRSTNK = [];
            $scope.filterTambahStatusSTNK  = { StartReceiveSTNKDate:null, EndReceiveSTNKDate:null, ServiceBureauId:null };
            $scope.fieldInput = {gridTambahGRSTNK:null, SuratJalanNo:null, Note:null};
            $scope.filterTambahStatusSTNK.ServiceBureauId = '';
            $scope.filterTambahStatusSTNK.StartReceiveSTNKDate = currDate;
            $scope.filterTambahStatusSTNK.EndReceiveSTNKDate = currDate;

        }

        $scope.KembaliGoodReceiptSTNK = function(){
            $scope.ShowGoodReceiptMain = true;
            $scope.ShowTambahGoodReceipt = false;
            $scope.ShowLihatGoodReceipt = false;
            $scope.goodReceiptSTNKOperation = '';
        }

        $scope.LihatGoodReceiptSTNK = function(row){
            $scope.gridLihatGRSTNK.data = [];
            $scope.goodReceiptSTNKOperation = 'lihat';
            $scope.ShowLihatGoodReceipt = true;
            $scope.ShowGoodReceiptMain = false;
            $scope.ShowTambahGoodReceipt = false;
            goodReceiptSTNKFactory.getDataLihatSTNKdetail(row.STNKGRId).then(function(res) 
            {
                    $scope.detailGRSTNK = res.data.Result[0];
                    $scope.gridLihatGRSTNK.data = $scope.detailGRSTNK.listFrameNo;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        var gridData = [];
        $scope.getData = function() {

            try {
                $scope.filterStatusSTNKMain.StartSTNKGRDate = $scope.filterStatusSTNKMain.StartSTNKGRDate.getFullYear()+'-'+($scope.filterStatusSTNKMain.StartSTNKGRDate.getMonth()+1)+'-'+$scope.filterStatusSTNKMain.StartSTNKGRDate.getDate();;
            }
            catch (e1) {
                $scope.filterStatusSTNKMain.StartSTNKGRDate = null;
            }

            try {
                $scope.filterStatusSTNKMain.EndSTNKGRDate = $scope.filterStatusSTNKMain.EndSTNKGRDate.getFullYear()+'-'+($scope.filterStatusSTNKMain.EndSTNKGRDate.getMonth()+1)+'-'+$scope.filterStatusSTNKMain.EndSTNKGRDate.getDate();;
            }
            catch (e1) {
                $scope.filterStatusSTNKMain.EndSTNKGRDate = null;
            }

            
            if ($scope.filterStatusSTNKMain.ServiceBureauId == null || (typeof $scope.filterStatusSTNKMain.ServiceBureauId == "undefined")) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Data Mandatory Harus Di Isi",
                        type: 'warning'
                    }
                );
            }else{
                goodReceiptSTNKFactory.getData($scope.filterStatusSTNKMain).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.ShowTambahGoodReceipt = false;
                        $scope.ShowGoodReceiptMain = true;
                        $scope.ShowLihatGoodReceipt = false;
                        $scope.loading = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            
        }

        var gridDataTambahSTNK = [];
        $scope.getDataTambahSTNK = function() {
            $scope.filterTambahStatusSTNK.StartReceiveSTNKDate = $scope.filterTambahStatusSTNK.StartReceiveSTNKDate.getFullYear()+'-'+($scope.filterTambahStatusSTNK.StartReceiveSTNKDate.getMonth()+1)+'-'+$scope.filterTambahStatusSTNK.StartReceiveSTNKDate.getDate();;
            $scope.filterTambahStatusSTNK.EndReceiveSTNKDate = $scope.filterTambahStatusSTNK.EndReceiveSTNKDate.getFullYear()+'-'+($scope.filterTambahStatusSTNK.EndReceiveSTNKDate.getMonth()+1)+'-'+$scope.filterTambahStatusSTNK.EndReceiveSTNKDate.getDate();;

            console.log('filterTambahStatusSTNK==>>',$scope.filterTambahStatusSTNK);
            if ($scope.filterTambahStatusSTNK.ServiceBureauId == null || $scope.filterTambahStatusSTNK.ServiceBureauId == "" || (typeof $scope.filterTambahStatusSTNK.ServiceBureauId == "undefined")) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Data Mandatory Harus Di Isi",
                        type: 'warning'
                    }
                );
            }else{
                goodReceiptSTNKFactory.getDataTambahSTNK($scope.filterTambahStatusSTNK).then(
                    function(res) {
                        $scope.gridTambahGRSTNK.data = res.data.Result;
                        $scope.loadingTambahSTNK = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        $scope.showKonfirmasiGoodReceipt = function() {
            $scope.disabledShowKonfirmasiGoodReceipt = true;
            setTimeout(function () { angular.element(".ui.modal.modalKonfirmasi").modal("refresh") }, 0);
            angular.element(".ui.modal.modalKonfirmasi").modal("show");
            angular.element('.ui.modal.modalKonfirmasi').not(':first').remove();
            $scope.gridSelectedGRSTNK.data = [];
            $scope.gridSelectedGRSTNK.data =$scope.selectRowgridTambahGRSTNK;
            
        }

        $scope.showKonfirmasiGoodReceiptFinal = function(row) {
            $scope.dataFromGridBatal = angular.copy(row);
            setTimeout(function () { angular.element(".ui.modal.modalPopUpBatal").modal("refresh") }, 0);
            angular.element(".ui.modal.modalPopUpBatal").modal("show");
            angular.element('.ui.modal.modalPopUpBatal').not(':first').remove();     
        }

        $scope.keluarModal = function(){
            angular.element(".ui.modal.modalKonfirmasi").modal("hide");
            $scope.disabledShowKonfirmasiGoodReceipt = false;
        }

        $scope.keluarModalKonfirmasiBatal = function(){
            angular.element(".ui.modal.modalPopUpBatal").modal("hide");
            $scope.dataFromGridBatal = null;
        }

        $scope.BatalGoodReceiptSTNK = function(){
            goodReceiptSTNKFactory.batal($scope.dataFromGridBatal).then(function(res) 
            {
                goodReceiptSTNKFactory.getData($scope.filterStatusSTNKMain).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;

                    bsNotify.show({
                        title: "Sukses",
                        content: "Data berhasil dibatalkan",
                        type: 'success'
                    });
                    $scope.dataFromGridBatal = null;
                    angular.element(".ui.modal.modalPopUpBatal").modal("refresh").modal("hide");
                },
                function(err) {
                    console.log("err=>", err);
                    bsNotify.show({
                        title: "Gagal",
                        content: "Gagal Ubah Data",
                        type: 'danger'
                    });
                    $scope.dataFromGridBatal = null;
                }
            );
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.simpanGRSTNK = function(){
            var yang_di_submit={SuratJalanNo:null,Note:null,listFrameNo:null};
            yang_di_submit.SuratJalanNo= $scope.fieldInput.SuratJalanNo;
            yang_di_submit.Note= $scope.fieldInput.Note;
            yang_di_submit.listFrameNo= $scope.gridSelectedGRSTNK.data;
                    
            $scope.disabledSimpanGRSTNK = true;

            if ($scope.fieldInput.SuratJalanNo == null) {
                bsNotify.show({
                    title: "Warning",
                    content: "Nomor Surat Jalan Harus di Isi",
                    type: 'warning'
                });
                $scope.disabledSimpanGRSTNK = false;

                $scope.keluarModal();
            }else{
                goodReceiptSTNKFactory.create(yang_di_submit).then(
                    function(res) {
                        try {
                            $scope.filterStatusSTNKMain.StartSTNKGRDate = $scope.filterStatusSTNKMain.StartSTNKGRDate.getFullYear()+'-'+($scope.filterStatusSTNKMain.StartSTNKGRDate.getMonth()+1)+'-'+$scope.filterStatusSTNKMain.StartSTNKGRDate.getDate();;
                        }
                        catch (e1) {
                            $scope.filterStatusSTNKMain.StartSTNKGRDate = null;
                        }
            
                        try {
                            $scope.filterStatusSTNKMain.EndSTNKGRDate = $scope.filterStatusSTNKMain.EndSTNKGRDate.getFullYear()+'-'+($scope.filterStatusSTNKMain.EndSTNKGRDate.getMonth()+1)+'-'+$scope.filterStatusSTNKMain.EndSTNKGRDate.getDate();;
                        }
                        catch (e1) {
                            $scope.filterStatusSTNKMain.EndSTNKGRDate = null;
                        }
                        
                        goodReceiptSTNKFactory.getData($scope.filterStatusSTNKMain).then(
                            function(res) {
                                $scope.grid.data = res.data.Result;
                                $scope.ShowTambahGoodReceipt = false;
                                $scope.ShowGoodReceiptMain = true;
                                $scope.ShowLihatGoodReceipt = false;
                                $scope.keluarModal();

                            bsNotify.show({
                                title: "Success",
                                content: "Berhasil terima STNK",
                                type: 'success'
                            });
                                    $scope.disabledSimpanGRSTNK = false;
                                    $scope.loading = false;
                                    return res.data;
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Gagal",
                            content: "Gagal Ubah Data",
                            type: 'danger'
                        });
                        $scope.disabledSimpanGRSTNK = false;
                    }
                );
            }

            
        }


        
        var actionbutton = ''+
        '<a href="" style="color:#777;"  class="trlink ng-scope" uib-tooltip="Lihat Good Receipt STNK" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.LihatGoodReceiptSTNK(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-address-card-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a  href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Batal Good Receipt STNK" tooltip-placement="bottom" onclick="this.blur()" ng-if="row.entity.TombolBatal==true" ng-click="grid.appScope.$parent.showKonfirmasiGoodReceiptFinal(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-close" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';

        var modalStatus = ''+
        '<div style="vertical-align:middle;">'+
        '<p style="padding:5px 0 0 3px;" ng-if="row.entity.STNKStatusName == \'STNK Delay dari Biro Jasa\'"><a href="" style="color:blue;" ng-click="grid.appScope.$parent.alasanterlambat(row.entity)">{{row.entity.STNKStatusName}}</a></p>' +
        '<p style="padding:5px 0 0 3px;" ng-if="row.entity.STNKStatusName != \'STNK Delay dari Biro Jasa\'">{{row.entity.STNKStatusName}}</p>'+
        '</div>'

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.test = function() {
            console.log("test", $scope.gridNoRangkaDocument.data);
            for (var i in $scope.gridNoRangkaDocument.data) {
                $scope.gridNoRangkaDocument.data[i].BiroJasa = $scope.birojasa;
            }
        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [

                { name: 'No. GR', field: 'STNKGRCode' },
                { name: 'Tanggal GR', field: 'STNKGRDate', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'No Surat Jalan', field: 'SuratJalanNo' },
                { name: 'GR Status', field: 'STNKGRStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '12%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbutton
                }

            ]
        };



        $scope.gridTambahGRSTNK = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No PO', field: 'POBiroJasaUnitNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'Cabang', field: 'OutletNameCabang' },
                { name: 'No. Rangka', field: 'FrameNo' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
                { name: 'Tanggal Terima STNK', field: 'ReceiveSTNKDate', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'STNK Status', field: 'STNKStatusName' },
            ]
        };

        $scope.gridTambahGRSTNK.onRegisterApi = function(gridApi) {
            $scope.grid_TambahGRSTNK = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectRowgridTambahGRSTNK = gridApi.selection.getSelectedRows();
                console.log("setan",$scope.selectRowgridTambahGRSTNK);
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectRowgridTambahGRSTNK = gridApi.selection.getSelectedRows();
                console.log("setan",$scope.selectRowgridTambahGRSTNK);
            });
        };


        $scope.gridSelectedGRSTNK = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No PO', field: 'POBiroJasaUnitNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'Cabang', field: 'OutletNameCabang' },
                { name: 'No. Rangka', field: 'FrameNo' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
                { name: 'Tanggal Terima STNK', field: 'ReceiveSTNKDate', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'STNK Status', field: 'STNKStatusName' },
            ]
        };



        $scope.gridLihatGRSTNK = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No PO', field: 'POBiroJasaUnitNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'Cabang', field: 'OutletNameCabang' },
                { name: 'No. Rangka', field: 'FrameNo' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
                { name: 'Tanggal Terima STNK', field: 'ReceiveSTNKDate', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'STNK Status', field: 'STNKStatusName' },
            ]
        };


    });