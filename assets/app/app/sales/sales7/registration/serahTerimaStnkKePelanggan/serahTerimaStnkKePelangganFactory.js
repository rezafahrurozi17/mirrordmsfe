angular.module('app')
  .factory('SerahTerimaStnkKePelangganFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        // var res=$http.get('/api/sales/SerahTerimaSTNKKePelanggan?start=1&limit=10000&'+$httpParamSerializer(filter));
        // return res;

        var param = $httpParamSerializer(filter);
      			var res = ''
      			if(filter.STNKName==null && filter.FrameNo==null && filter.BillingCode==null)
      			{
      				res=$http.get('/api/sales/SerahTerimaSTNKKePelanggan/?start=1&limit=1000000&'+ param);
      			}
      			else
      			{
      				res=$http.get(decodeURIComponent('/api/sales/SerahTerimaSTNKKePelanggan/?start=1&limit=1000000&'+ param));
      			}
            return res;

      },
	  
	  getDataCetak: function(FrameNo) {
        var res=$http.get('/api/sales/CetakTandaTerimaSTNKKePelanggan/?start=1&limit=100&filterData=FrameNo|'+FrameNo);
        return res;
      },
	  
      create: function(SerahTerimaStnkKePelanggan) {
        return $http.post('/api/sales/SerahTerimaSTNKKePelanggan', [{
                                            NamaChecklistItemAdministrasiPembayaran:SerahTerimaStnkKePelanggan.NamaChecklistItemAdministrasiPembayaran,

                                            }]);
      },
      update: function(SerahTerimaStnkKePelanggan){
        return $http.put('/api/sales/SerahTerimaSTNKKePelanggan/Update', [{
                                            OutletId:SerahTerimaStnkKePelanggan.OutletId,
											OutletName:SerahTerimaStnkKePelanggan.OutletName,
											SPKId:SerahTerimaStnkKePelanggan.SPKId,
											FormSPKNo:SerahTerimaStnkKePelanggan.FormSPKNo,
											SOId:SerahTerimaStnkKePelanggan.SOId,
											SOCode:SerahTerimaStnkKePelanggan.SOCode,
											STNKName:SerahTerimaStnkKePelanggan.STNKName,
											SerahTerimaSTNKBPKBStatusId:SerahTerimaStnkKePelanggan.SerahTerimaSTNKBPKBStatusId,
											SerahTerimaSTNKBPKBStatusName:SerahTerimaStnkKePelanggan.SerahTerimaSTNKBPKBStatusName,
											SuratDistribusiSTNKHeaderId:SerahTerimaStnkKePelanggan.SuratDistribusiSTNKHeaderId,
											SuratDistribusiSTNKNo:SerahTerimaStnkKePelanggan.SuratDistribusiSTNKNo,
											FrameNo:SerahTerimaStnkKePelanggan.FrameNo,
											JumlahDokumen:SerahTerimaStnkKePelanggan.JumlahDokumen,
											PrintCount:SerahTerimaStnkKePelanggan.PrintCount,
											CustomerName:SerahTerimaStnkKePelanggan.CustomerName,
											VehicleTypeColorId:SerahTerimaStnkKePelanggan.VehicleTypeColorId,
											VehicleTypeId:SerahTerimaStnkKePelanggan.VehicleTypeId,
											Description:SerahTerimaStnkKePelanggan.Description,
											KatashikiCode:SerahTerimaStnkKePelanggan.KatashikiCode,
											SuffixCode:SerahTerimaStnkKePelanggan.SuffixCode,
											VehicleModelId:SerahTerimaStnkKePelanggan.VehicleModelId,
											VehicleModelName:SerahTerimaStnkKePelanggan.VehicleModelName,
											ColorId:SerahTerimaStnkKePelanggan.ColorId,
											ColorCode:SerahTerimaStnkKePelanggan.ColorCode,
											ColorName:SerahTerimaStnkKePelanggan.ColorName,
											StatusCode:SerahTerimaStnkKePelanggan.StatusCode,
											TotalData:SerahTerimaStnkKePelanggan.TotalData,
											listDetail:SerahTerimaStnkKePelanggan.listDetail,
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SerahTerimaSTNKKePelanggan',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd