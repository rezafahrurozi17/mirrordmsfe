angular.module('app')
    .controller('SerahTerimaStnkKePelangganController', function(bsNotify, $scope, $http, CurrentUser, SerahTerimaStnkKePelangganFactory, $timeout) {
        // IfGotProblemWithModal();

        $scope.ShowTerimaSTNKKePelangganMain = true;
        $scope.ShowTerimaSTNKKePelangganCetak = false;
        //$scope.TodayDate='2017-05-18';

        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);

        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });

        $scope.$on('$destroy', function(){
            angular.element('.ui.modal.UpdatePenerimaanDokumenstnkkepelanggan').remove();
        });
        
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mSerahTerimaStnkKePelanggan = null; //Model
        $scope.xRole = { selected: [] };
        //$scope.checkAll = false;
		
		$scope.filter = {Id:1,STNKName: null, FrameNo: null, BillingCode: null};

        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            SerahTerimaStnkKePelangganFactory.getData($scope.filter).then(
                function(res) {

                    //setCheckList(res.data.Result);
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        function fixDate(date) {
            try {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } catch (ex) {
                return null;
            }
        }

        $scope.DetilTandaTerimaPenyerahanSTNK = function(selected_data) {
            if(selected_data.AssemblyType =="CKD"){
                //CKD = false
                selected_data.AssemblyType = "false";
            }else{
                //CBU = true
                selected_data.AssemblyType = "true";
            }
            $scope.KembaliKeTerimaSTNKKePelangganMain();
            $scope.The_Result = angular.copy(selected_data);
            $scope.printUrl = 'sales/TandaTerimaSTNKkePelanggan?FrameNo=' + selected_data.FrameNo + '&AssemblyType=' + selected_data.AssemblyType ;
            $scope.EditDetilTandaTerima = angular.copy($scope.The_Result.listDetail[0]);
            $scope.EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName = {};
            $scope.EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName = angular.copy($scope.The_Result.SerahTerimaSTNKBPKBStatusName);
            console.log('$scope.EditDetilTandaTerima',$scope.EditDetilTandaTerima);
            //$scope.CekAllDokumenLengkap();
            //$scope.dateOptionsMaxDateOptions =new Date();
			if($scope.EditDetilTandaTerima.TanggalPenyerahanDokumen==null)
			{
				$scope.EditDetilTandaTerima.TanggalPenyerahanDokumen=new Date();
			}
			
            angular.element('.ui.modal.UpdatePenerimaanDokumenstnkkepelanggan').modal('setting', { closable: false }).modal('show');
        }
		
		$scope.SerahTerimaStnkKePelangganNoticePajakCentang = function() {
			if($scope.EditDetilTandaTerima.NoticePajakBit==true)
			{
			}
			else if($scope.EditDetilTandaTerima.NoticePajakBit==false)
			{
				$scope.EditDetilTandaTerima.BBNKB=0;
				$scope.EditDetilTandaTerima.PKB=0;
				$scope.EditDetilTandaTerima.SWDKLLJ=0;
				$scope.EditDetilTandaTerima.BiayaAdmSTNK=0;
				$scope.EditDetilTandaTerima.BiayaAdmTNKB=0;
			}
		}

        $scope.SimpanUpdatePenerimaanDokumen = function() {
            $scope.The_listDetail = angular.copy($scope.EditDetilTandaTerima);

            //ini 3 jangan diapus ini kode jadul kalo ga bikin kerok
			//$scope.The_listDetail.STNKCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.STNKCustomerReceiveDate);
            //$scope.The_listDetail.NoticePajakCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.NoticePajakCustomerReceiveDate);
            //$scope.The_listDetail.PoliceNumberCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.PoliceNumberCustomerReceiveDate);

			$scope.The_listDetail.TanggalPenyerahanDokumen = fixDate($scope.EditDetilTandaTerima.TanggalPenyerahanDokumen);
            //$scope.The_Result['listDetail'][0]['PoliceNumber']="tadinya null";
            $scope.The_Result['listDetail'][0] = $scope.The_listDetail;
            $scope.SerahTerimaStnkKePelangganToUpdate = angular.copy($scope.The_Result);
            SerahTerimaStnkKePelangganFactory.update($scope.SerahTerimaStnkKePelangganToUpdate).then(function(res) {
                
				$scope.tempupdate = angular.copy(res.data.Result[0]);
                $scope.EditDetilTandaTerima = angular.copy(res.data.Result[0].listDetail[0]);
                //$scope.EditDetilTandaTerima.TanggalPenyerahanDokumen=new Date($scope.EditDetilTandaTerima.TanggalPenyerahanDokumen);
				
				//nanti di get
                SerahTerimaStnkKePelangganFactory.getData($scope.filter).then(
                    function(res) {
                        //setCheckList(res.data.Result);
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        
                        // for (var i in res.data.Result){
                            // if (res.data.Result[i].SuratDistribusiSTNKHeaderId == $scope.tempupdate.SuratDistribusiSTNKHeaderId){
                                // $scope.EditDetilTandaTerima = angular.copy(res.data.Result[i].listDetail[0]);
                                // $scope.EditDetilTandaTerima.TanggalPenyerahanDokumen=new Date($scope.EditDetilTandaTerima.TanggalPenyerahanDokumen);
								// //console.log("latest", $scope.EditDetilTandaTerima);
                                // //$scope.CekAllDokumenLengkap();
                            // }
                        // }

                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil simpan dokumen.",
                            type: 'success'
                        });


                    },
                    function(err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                ).then(function() {
                    //minta dimatiin ko kur dan andri by norman
                    //angular.element('.ui.modal.UpdatePenerimaanDokumenstnkkepelanggan').modal('hide');
                });
               //minta ditambahin sama ko kur dan andri by norman
                 //console.log('xxxx',res.data.Result);
                 
                //console.log("latest", $scope.tempupdate);
                 //$scope.EditDetilTandaTerima = angular.copy(res.data.Result[0]);

                 
                 //setCheckList($scope.EditDetilTandaTerima);
                 //console.log('jadinya',$scope.EditDetilTandaTerima);

            });


        }

        $scope.KembaliKeTerimaSTNKKePelangganMain = function() {
            $scope.checkAll = false
            $scope.STNKCustomerReceiveDateCekChanged = false;
            $scope.NoticePajakCustomerReceiveDateCekChanged = false;
            $scope.PlatNomorPolisiCustomerReceiveDateCekChanged = false;


            $scope.EditDetilTandaTerima = null;
            angular.element('.ui.modal.UpdatePenerimaanDokumenstnkkepelanggan').modal('hide');


        }

        // $scope.DebugSTNK = function() {
        //     console.log('Nomor Rangka -->', $scope.EditDetilTandaTerima.FrameNo,
        //                 'No STNK --> ', $scope.EditDetilTandaTerima.STNKNo,
        //                 'Nama Penerima --> ', $scope.EditDetilTandaTerima.NamaPenerima,
        //                 'No KTP --> ', $scope.EditDetilTandaTerima.NoKTP,
        //                 'No HandPhone --> ', $scope.EditDetilTandaTerima.NoHP,
        //                 'Tgl Kirim STNK --> ', $scope.EditDetilTandaTerima.STNKCustomerReceiveDate,
        //                 'Tgl Kirim Notice --> ', $scope.EditDetilTandaTerima.NoticePajakCustomerReceiveDate,
        //                 'Tgl Kirim Police --> ', $scope.EditDetilTandaTerima.PoliceNumberCustomerReceiveDate);
        // }

        $scope.KeTerimaSTNKKePelangganCetak = function() {
            angular.element('.ui.modal.UpdatePenerimaanDokumenstnkkepelanggan').modal('hide');

            SerahTerimaStnkKePelangganFactory.getDataCetak($scope.The_Result.FrameNo).then(function(res) {
                $scope.BuatCetak = res.data.Result;
            })

            $scope.ShowTerimaSTNKKePelangganMain = false;
            $scope.ShowTerimaSTNKKePelangganCetak = true;
        }

        $scope.KembaliKeShowTerimaSTNKKePelangganMain = function() {
            $scope.ShowTerimaSTNKKePelangganMain = true;
            $scope.ShowTerimaSTNKKePelangganCetak = false;
        }

        var customAction = ''+
            '<a style="color:#777;" tooltip-placement="left" uib-tooltip="Cetak" ng-click="grid.appScope.$parent.DetilTandaTerimaPenyerahanSTNK(row.entity)">' +
            '<i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px" aria-hidden="true"></i>' +
            '</a>';

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'Id', width: '7%', visible: false },
                { displayName:'No. Billing', name: 'No. Billing', field: 'BillingCode' },
                { name: 'Nama Pembeli', field: 'NamaPembeli' },
				{ name: 'Nama STNK', field: 'STNKName' },
				{ name: 'No Rangka', field: 'FrameNo' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
                {
                    name: 'Action',
                    visible: true,
                    width:'10%',
                    field: 'QtyDiterimaAtauNomorRangka',
                    cellTemplate: customAction,
                    cellClass: 'action-custom'}
            ]
        };


    });