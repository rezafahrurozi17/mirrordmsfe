angular.module('app')
  .factory('SerahTerimaFakturDanBpkbKePelangganFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());

                return fix;
            } else {
                return null;
            }

        };

        return {
            getData: function(filterSTBPKBMain) {
                var param = $httpParamSerializer(filterSTBPKBMain);
                var res = ''
                if(filterSTBPKBMain.NamaPembeli==null && filterSTBPKBMain.SOno==null && filterSTBPKBMain.FrameNo==null)
                {
                  res=$http.get('/api/sales/SerahTerimaBPKBKePelanggan/?start=1&limit=1000000&id=1'+ param);
                }
                else
                {
                  res=$http.get(decodeURIComponent('/api/sales/SerahTerimaBPKBKePelanggan/?start=1&limit=1000000&id=1&'+ param));
                }
                  return res;
            },

            
            getDataCetak: function(FrameNo) {
              // var res = $http.get('/api/sales/CetakTandaTerimaBPKBKePelanggan/?start=1&limit=100&filterData=FrameNo|' + FrameNo);
              var res = $http.get('/api/'+ FrameNo);
                return res;
            },

            
            create: function(SerahTerimaFakturDanBpkbKePelanggan) {
                return $http.post('/api/sales/SerahTerimaBPKBKePelanggan', [{
                NamaChecklistItemAdministrasiPembayaran:SerahTerimaFakturDanBpkbKePelanggan.NamaChecklistItemAdministrasiPembayaran,}]);
            },

            SerahTerimaBPKB: function(listSerahTerimaBPKB) {
               
                var listCetakanBSTKB = angular.copy(listSerahTerimaBPKB);
                console.log('listCetakanBSTKB go to back end ===>', listCetakanBSTKB);
                return $http.put('/api/sales/SerahTerimaBPKBKePelanggan/Update', listCetakanBSTKB);
            },


            update: function(SerahTerimaFakturDanBpkbKePelanggan){
                return $http.put('/api/sales/SerahTerimaBPKBKePelanggan/Update', SerahTerimaFakturDanBpkbKePelanggan);
                // return $http.put('/api/sales/SerahTerimaBPKBKePelanggan/Update', [{
                //               OutletId:SerahTerimaFakturDanBpkbKePelanggan.OutletId,
                // 							OutletName:SerahTerimaFakturDanBpkbKePelanggan.OutletName,
                // 							SPKId:SerahTerimaFakturDanBpkbKePelanggan.SPKId,
                // 							FormSPKNo:SerahTerimaFakturDanBpkbKePelanggan.FormSPKNo,
                // 							SOId:SerahTerimaFakturDanBpkbKePelanggan.SOId,
                // 							SOCode:SerahTerimaFakturDanBpkbKePelanggan.SOCode,
                // 							SerahTerimaSTNKBPKBStatusId:SerahTerimaFakturDanBpkbKePelanggan.SerahTerimaSTNKBPKBStatusId,
                // 							SerahTerimaSTNKBPKBStatusName:SerahTerimaFakturDanBpkbKePelanggan.SerahTerimaSTNKBPKBStatusName,
                // 							SuratDistribusiBPKBHeaderId:SerahTerimaFakturDanBpkbKePelanggan.SuratDistribusiBPKBHeaderId,
                // 							SuratDistribusiBPKBNo:SerahTerimaFakturDanBpkbKePelanggan.SuratDistribusiBPKBNo,
                // 							FrameNo:SerahTerimaFakturDanBpkbKePelanggan.FrameNo,
                // 							JumlahDokumen:SerahTerimaFakturDanBpkbKePelanggan.JumlahDokumen,
                // 							PrintCount:SerahTerimaFakturDanBpkbKePelanggan.PrintCount,
                // 							CustomerName:SerahTerimaFakturDanBpkbKePelanggan.CustomerName,
                // 							VehicleTypeColorId:SerahTerimaFakturDanBpkbKePelanggan.VehicleTypeColorId,
                // 							VehicleTypeId:SerahTerimaFakturDanBpkbKePelanggan.VehicleTypeId,
                // 							Description:SerahTerimaFakturDanBpkbKePelanggan.Description,
                // 							KatashikiCode:SerahTerimaFakturDanBpkbKePelanggan.KatashikiCode,
                // 							SuffixCode:SerahTerimaFakturDanBpkbKePelanggan.SuffixCode,
                // 							VehicleModelId:SerahTerimaFakturDanBpkbKePelanggan.VehicleModelId,
                // 							VehicleModelName:SerahTerimaFakturDanBpkbKePelanggan.VehicleModelName,
                // 							ColorId:SerahTerimaFakturDanBpkbKePelanggan.ColorId,
                // 							ColorCode:SerahTerimaFakturDanBpkbKePelanggan.ColorCode,
                // 							ColorName:SerahTerimaFakturDanBpkbKePelanggan.ColorName,
                // 							StatusCode:SerahTerimaFakturDanBpkbKePelanggan.StatusCode,
                // 							TotalData:SerahTerimaFakturDanBpkbKePelanggan.TotalData,
                // 							listDetail:SerahTerimaFakturDanBpkbKePelanggan.listDetail,

                //                                     //pid: role.pid,
                //                                     }]);
            },

            delete: function(id) {
              return $http.delete('/api/sales/SerahTerimaBPKBKePelanggan',{data:id,headers: {'Content-Type': 'application/json'}});
            },
        }
  });
 //ddd