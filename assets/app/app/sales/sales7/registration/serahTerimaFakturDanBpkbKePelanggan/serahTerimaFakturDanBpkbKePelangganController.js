angular.module('app')
    .controller('SerahTerimaFakturDanBpkbKePelangganController', function (bsNotify, $scope, $http, CurrentUser, SerahTerimaFakturDanBpkbKePelangganFactory, $timeout, PrintRpt) {
        // IfGotProblemWithModal();


        $scope.ShowSerahTerimaFakturDanBpkbKePelangganMain = true;
        $scope.ShowCetakSerahTerimaFakturDanBpkbKePelanggan = false;
        $scope.ShowSerahTerimaFakturDanBpkbKePelangganCetak = false;
        $scope.TanggalPenyerahanDokumenByCustom = new Date();
        $scope.selected_rows = [];
        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
        var checkboxes = document.getElementsByTagName('input');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
            }
        }

        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
        });

        $scope.$on('$destroy', function () {
            angular.element('.ui.modal.UpdatePenyerahanDokumen').remove();
        });

        $scope.dateOption = {
            format: 'dd-MM-yyyy'
        }




        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mSerahTerimaFakturDanBpkbKePelanggan = null; //Model
        $scope.xRole = { selected: [] };
        $scope.checkAll = false;
        $scope.countCek = 0;
        $scope.filterSTBPKBMain = { NamaPembeli: null, SOno: null, FrameNo: null };
        // $scope.EditDetilTandaTerimaListDetail = {};
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function () {
            console.log('$scope.filterSTBPKBMain', $scope.filterSTBPKBMain);
            SerahTerimaFakturDanBpkbKePelangganFactory.getData($scope.filterSTBPKBMain).then(
                function (res) {
                    setCheckList(res.data.Result);
                    $scope.grid.data = res.data.Result;
                    console.log('$scope.EditDetilTandaTerima', $scope.EditDetilTandaTerima);
                    $scope.loading = false;
                },
                function (err) {
                    console.log("err=>", err);
                    $scope.loading = false;
                }
            );
        }



        function setCheckList(grid) {
            //console.log("3dsdfsf",grid)
            angular.forEach(grid, function (data, index) {

                angular.merge(data, $scope.countCek);
                angular.merge(data, $scope.checkAll);

                if (data.listDetail.length > 0) {
                    if (data.listDetail[0].BPKBCustomerReceiveDate == null) {
                        data.listDetail[0].BPKBCustomerReceiveDateBit = false;
                    } else {
                        data.listDetail[0].BPKBCustomerReceiveDateBit = true;
                        data.countCek = data.countCek + 1;
                    }

                    if (data.listDetail[0].FakturTAMCustomerReceiveDate == null) {
                        data.listDetail[0].FakturTAMCustomerReceiveDateBit = false;
                    } else {
                        data.listDetail[0].FakturTAMCustomerReceiveDateBit = true;
                        data.countCek = data.countCek + 1;
                    }

                    if (data.listDetail[0].FormACustomerReceiveDate == null) {
                        data.listDetail[0].FormACustomerReceiveDateBit = false;
                    } else {
                        data.listDetail[0].FormACustomerReceiveDateBit = true;
                        data.countCek = data.countCek + 1;
                    }

                    if (data.listDetail[0].CertificateNIKCustomerReceiveDate == null) {
                        data.listDetail[0].CertificateNIKCustomerReceiveDateBit = false;
                    } else {
                        data.listDetail[0].CertificateNIKCustomerReceiveDateBit = true;
                        data.countCek = data.countCek + 1;
                    }

                    if (data.countCek == 4) {
                        data.checkAll = true;
                    } else {
                        data.checkAll = false;
                    }
                }

            });
        }

        //-------------------------------------
        // Tickbox function
        //-------------------------------------

        $scope.BPKBCustomerTickBox = function (op) {
            if (op.BPKBCustomerReceiveDate != null) {
                op.BPKBCustomerReceiveDateBit = true;
            } else if (op.BPKBCustomerReceiveDate == null) {
                op.BPKBCustomerReceiveDateBit = false;
            }
        }

        $scope.FakturTAMCustomerTickBox = function (op) {
            if (op.FakturTAMCustomerReceiveDate != null) {
                op.FakturTAMCustomerReceiveDateBit = true;
            } else if (op.FakturTAMCustomerReceiveDate == null) {
                op.FakturTAMCustomerReceiveDateBit = false;
            }
        }

        $scope.FormACustomerTickBox = function (op) {
            if (op.FormACustomerReceiveDate != null) {
                op.FormACustomerReceiveDateBit = true;
            } else if (op.FormACustomerReceiveDate == null) {
                op.FormACustomerReceiveDateBit = false;
            }
        }

        $scope.CertificateNIKCustomerTickBox = function (op) {
            if (op.CertificateNIKCustomerReceiveDate != null) {
                op.CertificateNIKCustomerReceiveDateBit = true;
            } else if (op.CertificateNIKCustomerReceiveDate == null) {
                op.CertificateNIKCustomerReceiveDateBit = false;
            }
        }

        //-----------------------------------------
        // End of tickbox
        //-----------------------------------------

        $scope.BPKBCustomerReceiveDate = function (obj) {
            if (obj.BPKBCustomerReceiveDateBit == true) {
                obj.BPKBCustomerReceiveDate = new Date();
                if (obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.FormACustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) {
                    $scope.checkAll = true;
                }
            } else {
                obj.BPKBCustomerReceiveDate = null;
                if ((obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.FormACustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) ||
                    obj.FakturTAMCustomerReceiveDateBit == false ||
                    obj.FormACustomerReceiveDateBit == false ||
                    obj.CertificateNIKCustomerReceiveDateBit == false) {
                    $scope.checkAll = false;
                }
            }
        }

        $scope.FakturTAMCustomerReceiveDate = function (obj) {
            if (obj.FakturTAMCustomerReceiveDateBit == true) {
                obj.FakturTAMCustomerReceiveDate = new Date();
                if (obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FormACustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) {
                    $scope.checkAll = true;
                }
            } else {
                obj.FakturTAMCustomerReceiveDate = null;
                if ((obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FormACustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) ||
                    obj.FakturTAMCustomerReceiveDateBit == false ||
                    obj.FormACustomerReceiveDateBit == false ||
                    obj.CertificateNIKCustomerReceiveDateBit == false) {
                    $scope.checkAll = false;
                }
            }
        }

        $scope.FormACustomerReceiveDate = function (obj) {
            if (obj.FormACustomerReceiveDateBit == true) {
                obj.FormACustomerReceiveDate = new Date();
                if (obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) {
                    $scope.checkAll = true;
                }
            } else {
                obj.FormACustomerReceiveDate = null;
                if ((obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) ||
                    obj.FakturTAMCustomerReceiveDateBit == false ||
                    obj.FormACustomerReceiveDateBit == false ||
                    obj.CertificateNIKCustomerReceiveDateBit == false) {
                    $scope.checkAll = false;
                }
            }
            $scope.EditDetilTandaTerima.FormACustomerReceiveDateBit = angular.copy(obj.FormACustomerReceiveDateBit);

            $scope.printUrl = 'sales/TandaTerimaBPKBkePelanggan?FrameNo=' + $scope.frameNo.FrameNo + '&FormA=' + $scope.EditDetilTandaTerima.listDetail[0].FormABit;
        }

        $scope.CertificateNIKCustomerReceiveDate = function (obj) {
            if (obj.CertificateNIKCustomerReceiveDateBit == true) {
                obj.CertificateNIKCustomerReceiveDate = new Date();
                if (obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) {
                    $scope.checkAll = true;
                }
            } else {
                obj.CertificateNIKCustomerReceiveDate = null;
                if ((obj.BPKBCustomerReceiveDateBit == true &&
                    obj.FakturTAMCustomerReceiveDateBit == true &&
                    obj.CertificateNIKCustomerReceiveDateBit == true) ||
                    obj.FakturTAMCustomerReceiveDateBit == false ||
                    obj.FormACustomerReceiveDateBit == false ||
                    obj.CertificateNIKCustomerReceiveDateBit == false) {
                    $scope.checkAll = false;
                }
            }
        }

        $scope.DateAll = function (checked, data) {
            if (checked == false) {
                data.BPKBCustomerReceiveDateBit = false;
                data.FakturTAMCustomerReceiveDateBit = false;
                data.FormACustomerReceiveDateBit = false;
                data.CertificateNIKCustomerReceiveDateBit = false;

                data.BPKBCustomerReceiveDate = null;
                data.FakturTAMCustomerReceiveDate = null;
                data.FormACustomerReceiveDate = null;
                data.CertificateNIKCustomerReceiveDate = null;
            } else if (checked == true) {
                data.BPKBCustomerReceiveDateBit = true;
                data.FakturTAMCustomerReceiveDateBit = true;
                data.FormACustomerReceiveDateBit = true;
                data.CertificateNIKCustomerReceiveDateBit = true;

                data.BPKBCustomerReceiveDate = new Date();
                data.FakturTAMCustomerReceiveDate = new Date();
                data.FormACustomerReceiveDate = new Date();
                data.CertificateNIKCustomerReceiveDate = new Date();
            }
        }

        function fixDate(date) {
            try {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } catch (ex) {
                return null;
            }
        }

        $scope.CekLisDokumenLengkap = function () {
            if ($scope.EditDetilTandaTerima.BPKBCustomerReceiveDateBit == true &&
                $scope.EditDetilTandaTerima.FakturTAMCustomerReceiveDateBit == true &&
                $scope.EditDetilTandaTerima.FormACustomerReceiveDateBit == true &&
                $scope.EditDetilTandaTerima.CertificateNIKCustomerReceiveDateBit == true) {
                $scope.checkAll = true;
            } else {
                $scope.checkAll = false;
            }
        }


        $scope.cetakGanda = function (dataCetakan) {

            var FormA = 'false';
            var FrameNo = '';
            for (var i = 0; i < dataCetakan.length; i++) {
                FrameNo += dataCetakan[i].FrameNo;
                if (i < dataCetakan.length - 1) {
                    FrameNo += ',';
                }
            }

            $scope.printUrl = 'sales/TandaTerimaBPKBkePelanggan?FrameNo=' + FrameNo + '&FormA=' + FormA; //FormA emang dikosongin, biar di isi backend

            //Call PrintRpt
            var pdfFile = null;
            PrintRpt.print($scope.printUrl).success(function (res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                pdfFile = fileURL;

                if (pdfFile != null)
                    printJS(pdfFile);
                else
                    console.log("error cetakan", pdfFile);
            })
                .error(function (res) {
                    console.log("error cetakan", pdfFile);
                });

        }

        $scope.onKembaliClick = function () {
            $scope.ShowCetakSerahTerimaFakturDanBpkbKePelanggan = false;
            $scope.ShowSerahTerimaFakturDanBpkbKePelangganMain = true;

            $scope.gridBPKBCetak.data = [];

        }

        $scope.onLanjutClick = function () {
            $scope.ShowCetakSerahTerimaFakturDanBpkbKePelanggan = true;
            $scope.ShowSerahTerimaFakturDanBpkbKePelangganMain = false;
            $scope.disableButton = 


            $scope.gridBPKBCetak.data = [];

            for (var i in $scope.selected_rows) {
                $scope.gridBPKBCetak.data[i] = angular.merge($scope.selected_rows[i], $scope.selected_rows[i].listDetail[0]);
            }

            console.log('$scope.gridBPKBCetak.data ===>', $scope.gridBPKBCetak.data);

        }

        $scope.onSelectRows = function (rows) {
            $scope.selected_rows = angular.copy(rows);
            console.log('$scope.selected_rows ==>', $scope.selected_rows);
        }
        
        $scope.onCetakClick = function name() {

            $scope.UpdatePenyerahanDokumen();
            // $scope.cetakGanda($scope.EditDetilTandaTerima);
            $scope.getData();
            $scope.selected_rows = [];
            


        }



        $scope.DetilTandaTerimaPenyerahanBPKB = function (selected_data) {
            $scope.The_Result = angular.copy(selected_data);
            $scope.EditDetilTandaTerima = angular.copy($scope.The_Result);
            $scope.CekLisDokumenLengkap();
            $scope.frameNo = $scope.The_Result;
            $scope.formA = $scope.EditDetilTandaTerima;
            $scope.printUrl = 'sales/TandaTerimaBPKBkePelanggan?FrameNo=' + $scope.frameNo.FrameNo + '&FormA=' + $scope.EditDetilTandaTerima.listDetail[0].FormABit;
            angular.element('.ui.modal.UpdatePenyerahanDokumen').modal('setting', { closable: false }).modal('show');
        }

        $scope.debugConsole = function () {
            console.log('Depan => ', $scope.EditDetilTandaTerima.FormACustomerReceiveDateBit);
            console.log('Print => ', $scope.printUrl);
        }

        $scope.CentangAllDokumenChanged = function (selected_data) {

            if ($scope.CentangAllDokumen == true) {
                $scope.BPKBCustomerReceiveDateCekChanged = true;
                $scope.FakturTAMCustomerReceiveDateCekChanged = true;
                $scope.FormACustomerReceiveDateCekChanged = true;
                $scope.CertificateNIKCustomerReceiveDateCekChanged = true;
                $scope.DisablePasSemuaDicentang = true;
            } else if ($scope.CentangAllDokumen == false) {
                $scope.BPKBCustomerReceiveDateCekChanged = false;
                $scope.FakturTAMCustomerReceiveDateCekChanged = false;
                $scope.FormACustomerReceiveDateCekChanged = false;
                $scope.CertificateNIKCustomerReceiveDateCekChanged = false;
                $scope.DisablePasSemuaDicentang = false;
            }
            $scope.BPKBCustomerReceiveDateChanged();
            $scope.FakturTAMCustomerReceiveDateChanged();
            $scope.FormACustomerReceiveDateChanged();
            $scope.CertificateNIKCustomerReceiveDateChanged();
        }



        $scope.SimpanUpdatePenyerahanDokumen = function () {
            $scope.The_listDetail = angular.copy($scope.EditDetilTandaTerima);

            $scope.The_listDetail.BPKBCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.BPKBCustomerReceiveDate);
            $scope.The_listDetail.FakturTAMCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.FakturTAMCustomerReceiveDate);
            $scope.The_listDetail.FormACustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.FormACustomerReceiveDate);
            $scope.The_listDetail.CertificateNIKCustomerReceiveDate = fixDate($scope.EditDetilTandaTerima.CertificateNIKCustomerReceiveDate);

            $scope.The_Result['listDetail'][0] = $scope.The_listDetail;
            $scope.SerahTerimaFakturDanBpkbKePelangganToUpdate = angular.copy($scope.The_Result);



            SerahTerimaFakturDanBpkbKePelangganFactory.update($scope.SerahTerimaFakturDanBpkbKePelangganToUpdate).then(function (res) {
                $scope.tempupdate = angular.copy(res.data.Result[0]);
                //nanti di get
                SerahTerimaFakturDanBpkbKePelangganFactory.getData($scope.filterSTBPKBMain).then(
                    function (res) {
                        setCheckList(res.data.Result);
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;

                        for (var i in res.data.Result) {
                            if (res.data.Result[i].SuratDistribusiBPKBHeaderId == $scope.tempupdate.SuratDistribusiBPKBHeaderId) {
                                $scope.EditDetilTandaTerima = angular.copy(res.data.Result[i].listDetail[0]);
                                $scope.CekAllDokumenLengkap();
                            }
                        }

                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil simpan dokumen.",
                            type: 'success'
                        });
                        //return res.data;
                    },
                    function (err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : " + err.data.Message,
                            type: 'danger'
                        });
                        // console.log("err=>", err);
                    }
                ).then(function () {
                });

            });
        }


        $scope.UpdatePenyerahanDokumen = function () {

            $scope.EditDetilTandaTerima = Object.assign({},$scope.gridBPKBCetak.data);

            for (var i in $scope.EditDetilTandaTerima) {
                $scope.EditDetilTandaTerima[i].listDetail[0].TanggalPenyerahanDokumen = fixDate($scope.TanggalPenyerahanDokumenByCustom);
                $scope.EditDetilTandaTerima[i].TanggalPenyerahanDokumen = fixDate($scope.TanggalPenyerahanDokumenByCustom);
                $scope.EditDetilTandaTerima[i].listDetail[0].BPKBNo = $scope.EditDetilTandaTerima[i].BPKBNo;
            }

            $scope.EditDetilTandaTerima = Object.assign([], $scope.EditDetilTandaTerima);


            console.log('$scope.EditDetilTandaTerima ===>', $scope.EditDetilTandaTerima);

            SerahTerimaFakturDanBpkbKePelangganFactory.SerahTerimaBPKB($scope.EditDetilTandaTerima).then(
                function (res) {
                    // $scope.EditDetilTandaTerima = angular.copy(res.data.Result[0]);

                    $scope.onKembaliClick();

                    bsNotify.show({
                        title: "Berhasil",
                        content: "Serah Terima Faktur dan BPKB Berhasil.",
                        type: 'success'
                    });
                    $scope.cetakGanda($scope.EditDetilTandaTerima);

                    SerahTerimaFakturDanBpkbKePelangganFactory.getData($scope.filterSTBPKBMain).then(
                        function (res) {
                            $scope.grid.data = res.data.Result;
                            $scope.loading = false;
                        },
                        function (err) {
                            bsNotify.show({
                                title: "Error",
                                content: "Pesan Error : " + err.data.Message,
                                type: 'danger'
                            });
                        }
                    ).then(function () {
                    });
                },
                function (err) {
                    console.log("err=>", err);
                    bsNotify.show({
                        title: "Gagal",
                        content: "Serah Terima Faktur dan BPKB Tidak Berhasil.",
                        type: 'danger'
                    });
                }
            );

        }

        $scope.KembaliKeTerimaBPKBKePelangganMain = function () {
            $scope.checkAll = false
            $scope.BPKBCustomerReceiveDateCekChanged = false;
            $scope.FakturTAMCustomerReceiveDateCekChanged = false;
            $scope.FormACustomerReceiveDateCekChanged = false;
            $scope.CertificateNIKCustomerReceiveDateCekChanged = false;

            $scope.EditDetilTandaTerima = null;
            angular.element('.ui.modal.UpdatePenyerahanDokumen').modal('hide');
        }

        $scope.KeSerahTerimaFakturDanBpkbKePelangganCetak = function () {
            angular.element('.ui.modal.UpdatePenyerahanDokumen').modal('hide');

            SerahTerimaFakturDanBpkbKePelangganFactory.getDataCetak($scope.The_Result.FrameNo).then(function (res) {
                $scope.BuatCetak = res.data.Result;
            })

            $scope.ShowSerahTerimaFakturDanBpkbKePelangganMain = false;
            $scope.ShowSerahTerimaFakturDanBpkbKePelangganCetak = true;
        }

        $scope.KembaliKeSerahTerimaFakturDanBpkbKePelangganMain = function () {
            $scope.ShowSerahTerimaFakturDanBpkbKePelangganMain = true;
            $scope.ShowSerahTerimaFakturDanBpkbKePelangganCetak = false;
        }

        var customActionGrid = '' +
            '<a style="color:#777;" tooltip-placement="left" uib-tooltip="Cetak" ng-click="grid.appScope.$parent.DetilTandaTerimaPenyerahanBPKB(row.entity)">' +
            '<i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:10px;" aria-hidden="true"></i>' +
            '</a>';

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'NoBilling', field: 'BillingCode' },
                { name: 'NamaPembeli', field: 'NamaPembeli' },
                { name: 'NamaSTNK', field: 'STNKName' },
                { name: 'NoRangka', field: 'FrameNo' },
                { name: 'ModelTipeKendaraan', field: 'Description' },
                { name: 'Status', field: 'SerahTerimaSTNKBPKBStatusName' },
                


                // { name: 'Id', field: 'Id', width: '7%', visible: false },
                // { name: 'NoSpk', field: 'FormSPKNo' },
                // { name: 'NoSo', field: 'SOCode' },
                // { name: 'NoRangka', field: 'FrameNo' },
                // { name: 'NamaPelanggan', field: 'CustomerName' },
                // { name: 'Tipe Model Kendaraan', field: 'Description' },
                // { name: 'Status', field: 'SerahTerimaSTNKBPKBStatusName' },
                // {
                //     name: 'Action',
                //     field: 'QtyDiterimaAtauNomorRangka',
                //     cellTemplate: customActionGrid,
                //     cellClass: 'action-right'
                // }
            ]
        };



        $scope.gridBPKBCetak = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'No Rangka', field: 'FrameNo', visible: true },
                { name: 'Pembayaran', field: 'FundSourceName', enableCellEdit: false },
                { name: 'Model Tipe Kendaraan', field: 'Description', enableCellEdit: false },

                { name: 'Nama Penerima Pelanggan / Leasing', field: 'NamaPembeli', enableCellEdit: false },
                { name: 'Nama BPKB', field: 'BPKBName', enableCellEdit: false },
                {
                    name: 'No BPKB*',
                    field: 'BPKBNo',
                    enableColumnMenu: false,editableCellTemplate:
                    "<div><form name=\"inputForm\"><input type=\"INPUT_TYPE\" ng-class=\"'colt' + col.uid\" ui-grid-editor ng-model=\"MODEL_COL_FIELD\"  maxlength=10 required></form></div>",
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return 'canEdit'; },
                    enableCellEdit: true

                },
                { name: 'No KTP', field: 'NoKTP', enableCellEdit: false },
                { name: 'Handphone', field: 'NoHPPembeli', enableCellEdit: false },
                { name: 'No Polisi', field: 'PoliceNumber', enableCellEdit: false }
            ]
        };

        $scope.disableButton = true;
        $scope.gridBPKBCetak.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            // FUNCTION EDIT DALAM GRID
           
            $scope.gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                console.log('new value===> ', newValue);
                console.log('rowEntity ===>', rowEntity);
                if (newValue == '' || newValue == null) {
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nomor BPKB tidak boleh kosong",
                        type: 'warning'
                    });

                    rowEntity.BPKBNo = 'Wajid diisi!';
                } else {
                    rowEntity.BPKBNo = newValue;
                }
                var countError = 0;
                for(var i in $scope.gridBPKBCetak.data){
                    if($scope.gridBPKBCetak.data[i].BPKBNo == null || $scope.gridBPKBCetak.data[i].BPKBNo == ''){
                        countError++;
                    }
                }
                if(countError > 0){
                    $scope.disableButton = true;
                }else{
                    $scope.disableButton = false;
                }
            });
        };


    });