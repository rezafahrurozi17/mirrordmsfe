angular.module('app')
    .controller('MaintainPOBirojasaController', function ($scope, $http, uiGridConstants, CurrentUser, MaintainSOFactory, MaintainPOBirojasaFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
            // angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('hide');
            // angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainPOBirojasa = null; //Model
        $scope.rightStatus = null;
        $scope.daftarPO = true;
        $scope.statussekarang = null;
        $scope.printPO = null;
        $scope.filter = { Id: 0, POBiroJasaUnitNo: null, POJasaStatusId: null };
        $scope.disabledSimpan = false; 
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.noRangkaSelectedBiroJasaStatus = false;

        MaintainSOFactory.getOutlet().then(
            function (res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainPOBirojasaFactory.getDataVendor().then(function (res) {
            $scope.birojasa = res.data.Result;
        });

        MaintainPOBirojasaFactory.getPOJasa().then(function (res) {
            $scope.status = res.data.Result;
        });

        $scope.simpan = function () {
            $scope.disabledSimpan = true;
            var MaintainPoBiroJasaPriceGaAdaYangNull = false;
            for (var i = 0; i < $scope.gridNoRangkaDocument.data.length; ++i) {
                if ($scope.gridNoRangkaDocument.data[i].NoticePrice == null || $scope.gridNoRangkaDocument.data[i].UnNoticePrice == null) {
                    // if ($scope.gridNoRangkaDocument.data[i].NoticePrice == null) {
                    MaintainPoBiroJasaPriceGaAdaYangNull = false; //gagal
                    break;
                } else if ($scope.gridNoRangkaDocument.data[i].NoticePrice != null || $scope.gridNoRangkaDocument.data[i].UnNoticePrice != null) {
                // } else if ($scope.gridNoRangkaDocument.data[i].NoticePrice != null) {
                    MaintainPoBiroJasaPriceGaAdaYangNull = true; //lolos
                }
            }

            if (MaintainPoBiroJasaPriceGaAdaYangNull == true) {
                if ($scope.statussekarang == 'ubah') {

                    $scope.list = angular.copy($scope.gridNoRangkaDocument.data);
                    $scope.mMaintainPOBirojasa.listFrameNo = angular.copy($scope.list);
                    $scope.mMaintainPOBirojasa.TotalPrice = 0;

                    $scope.gridDetailNoRangka.data = angular.copy($scope.gridNoRangkaDocument.data);

                    $scope.mMaintainPOBirojasa.TotalPrice = 0;
                    for (var i = 0; i < $scope.list.length; ++i) {
                        $scope.mMaintainPOBirojasa.TotalPrice += $scope.list[i].NoticePrice + $scope.list[i].UnNoticePrice;
                        // $scope.mMaintainPOBirojasa.TotalPrice += $scope.list[i].NoticePrice;
                    }

                    // for (var i in $scope.list) {
                    // $scope.mMaintainPOBirojasa.TotalPrice = $scope.mMaintainPOBirojasa.TotalPrice + $scope.list[i].Price;
                    // }
                    console.log("setan1", $scope.list);
                    console.log("setan2", $scope.mMaintainPOBirojasa);
                    MaintainPOBirojasaFactory.ubah($scope.mMaintainPOBirojasa).then(function (res) {
                        console.log("setan4", res);
                        $scope.mMaintainPOBirojasa = res.data.Result[0];
                        $scope.printPO = 'sales/POBiroJasa?POBiroJasaUnitId=' + $scope.mMaintainPOBirojasa.POBiroJasaUnitId;
                        $scope.gridDetailNoRangka.data = res.data.Result[0].listFrameNo;
                        $scope.detailBirojasa = true;
                        $scope.daftarPO = false;
                        $scope.statussekarang = null;
                        $scope.noRangkaSelectedBiroJasaStatus = false;
                        angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('setting', 'transition hidden', 'slide left').modal({ closable: false }).modal('hide');
                        $scope.disabledSimpan = false;

                    }, function(err) {
                        console.log('err nya', err)
                        var  errMsg = "";
                        if (err.data.Message.split('#').length > 1) {
                            errMsg = err.data.Message.split('#')[1];
                        }
                        bsNotify.show({
                            title: "Peringatan",
                            content: errMsg,
                            type: 'warning'
                        });
                        $scope.disabledSimpan = false;
                    });
                } else {
                    $scope.list = angular.copy($scope.gridNoRangkaDocument.data);
                    $scope.mMaintainPOBirojasa.listFrameNo = $scope.list;
                    $scope.mMaintainPOBirojasa.TotalPrice = 0;
                    for (var i in $scope.list) {
                        $scope.mMaintainPOBirojasa.TotalPrice = $scope.mMaintainPOBirojasa.TotalPrice + $scope.list[i].NoticePrice + $scope.list[i].UnNoticePrice;
                        // $scope.mMaintainPOBirojasa.TotalPrice = $scope.mMaintainPOBirojasa.TotalPrice + $scope.list[i].NoticePrice;
                    }

                    // console.log("hasil Akhir", $scope.mMaintainPOBirojasa);
                    // console.log("test", $scope.gridNoRangkaDocument.data);
                    MaintainPOBirojasaFactory.create($scope.mMaintainPOBirojasa).then(function (res) {
                        $scope.statussekarang = null;
                        $scope.mMaintainPOBirojasa = res.data.Result[0];
                        $scope.printPO = 'sales/POBiroJasa?POBiroJasaUnitId=' + $scope.mMaintainPOBirojasa.POBiroJasaUnitId;
                        $scope.gridDetailNoRangka.data = res.data.Result[0].listFrameNo;
                        $scope.detailBirojasa = true;
                        $scope.rightStatus = null;
                        $scope.daftarPO = false;
                        $scope.noRangkaSelectedBiroJasaStatus = false;
                        angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('setting', 'transition hidden', 'slide left').modal({ closable: false }).modal('hide');
                        $scope.disabledSimpan = false;

                    }, function(err) {
                        console.log('err nya', err)
                        $scope.disabledSimpan = false;
                    });
                }
            } else if (MaintainPoBiroJasaPriceGaAdaYangNull == false) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda harus mengisi field harga.",
                    type: 'warning'
                });
                $scope.disabledSimpan = false;
            }



        }

        $scope.kirim = function () {
            MaintainPOBirojasaFactory.kirim($scope.mMaintainPOBirojasa).then(function (res) {
                $scope.gridDetailNoRangka.data = $scope.list;

                bsNotify.show({
                    title: "berhasil",
                    content: "PO berhasil dikirim.",
                    type: 'success'
                });

                $scope.detailBirojasa = false;
                $scope.daftarPO = true;
                $scope.getData();
            })
        }
		
		$scope.MaintainPoBiroJasaDownloadFile = function () {
			var excelData = [];
            var fileName = "";
            console.log('$scope.mMaintainPOBirojasa',$scope.mMaintainPOBirojasa);
			for(var i = 0; i < $scope.gridDetailNoRangka.data.length; ++i)
			{
				
				
				
				excelData.push({"No": i+1,
                                "Model Code":$scope.gridDetailNoRangka.data[i].VehicleModelCode,
                                // "No Aplikasi":$scope.gridDetailNoRangka.data[i].NoAplikasi,
                                "Application Date":$scope.gridDetailNoRangka.data[i].ApplicationDate,
                                "Effective Date":$scope.gridDetailNoRangka.data[i].EffectiveDate,
                                "Customer Name":$scope.gridDetailNoRangka.data[i].CustomerBuyerName,
                                "Address":$scope.gridDetailNoRangka.data[i].Address,
                                "Address 2":$scope.gridDetailNoRangka.data[i].Address2,
                                "Reference Number":$scope.gridDetailNoRangka.data[i].ReferenceNumber,
                                "Model Tipe Kendaraan":$scope.gridDetailNoRangka.data[i].ModelTipeKendaraan,
                                "Frame Number":$scope.gridDetailNoRangka.data[i].FrameNo,
                                "Engine Number":$scope.gridDetailNoRangka.data[i].EngineNo,
                                "Warna":$scope.gridDetailNoRangka.data[i].ColorName,
                                "Cabang":$scope.gridDetailNoRangka.data[i].OutletCode,
                                "DO Number, Do Date & Region Code":$scope.gridDetailNoRangka.data[i].DOInfo,
                                "Jenis":$scope.gridDetailNoRangka.data[i].JenisUnit,
                                "Model":$scope.gridDetailNoRangka.data[i].ModelUnit,
                                "Nomor PO": $scope.mMaintainPOBirojasa.POBiroJasaUnitNo,
                                "Vendor Biro Jasa":$scope.mMaintainPOBirojasa.ServiceBureauName,
                                "Nama STNK":$scope.gridDetailNoRangka.data[i].CustomerName,
								"Wilayah PO":$scope.gridDetailNoRangka.data[i].WilayahPO});				
			}
			
            fileName = $scope.mMaintainPOBirojasa.POBiroJasaUnitNo;

            XLSXInterface.writeToXLSX(excelData, fileName);
		}

        $scope.ubah = function (stastus) {
            $scope.gridNoRangka.data = [];
            $scope.statussekarang = stastus;
            setTimeout(function () { angular.element(".ui.modal.pilihNoRangkaBiroJasa").modal("refresh"); }, 0);
            angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('setting', 'transition', 'slide right').modal({ allowMultiple: false }).modal('show');
            $scope.selctedRow = null;
            $scope.gridNoRangka.data = angular.copy($scope.gridDetailNoRangka.data);
            var countData = $scope.gridNoRangka.data.length;

            setTimeout(function () {
                for (var i = 0; i < countData; i++) {
                    $scope.loading = false;
                    $scope.gridApi.selection.toggleRowSelection($scope.gridNoRangka.data[i]);
                };
            }, 1000);

            MaintainPOBirojasaFactory.getDataNoRangka().then(function (res) {
                $scope.norangka = [];
                $scope.norangka = res.data.Result;
                for (var i in $scope.norangka) {
                    $scope.gridNoRangka.data.push($scope.norangka[i]);
                }
                console.log('testing --> ', $scope.norangka);
            });
        }

        $scope.batal = function () {
            MaintainPOBirojasaFactory.batal($scope.mMaintainPOBirojasa).then(function (res) {
                $scope.gridDetailNoRangka.data = $scope.list;
                $scope.detailBirojasa = false;
                $scope.daftarPO = true;
                $scope.getData();
            })
        }

        $scope.keluarFrame = function () {
            setTimeout(function () { angular.element(".ui.modal.pilihNoRangkaBiroJasa").modal("refresh"); }, 0);
            angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal("hide");
            $scope.gridNoRangka.data = [];
            $scope.norangka = [];
        }

        $scope.buatPO = function () {
            $scope.rightStatus = null;
            $scope.mMaintainPOBirojasa = {};
            $scope.mMaintainPOBirojasa.ServiceBureauId = null;
            $scope.disabledSimpan = false;
            MaintainPOBirojasaFactory.getDataNoRangka().then(function (res) {
                $scope.norangka = res.data.Result;
                setTimeout(function () { angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('refresh'); }, 0);
                angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('setting', { closable: false }).modal('show');
                $scope.selctedRow = null;
                $scope.gridNoRangka.data = $scope.norangka;
                $scope.gridApi.selection.clearSelectedRows();
            });
        }

        $scope.$on('$destroy', function () {
            angular.element('.ui.modal.noRangkaSelectedBiroJasa').remove();
            angular.element('.ui.modal.pilihNoRangkaBiroJasa').remove();
        });

        $scope.lanjut = function (rows) {
            $scope.noRangkaSelectedBiroJasaStatus = false;
            
            angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('hide');


            setTimeout(function () {
                //angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');
                $scope.gridNoRangkaDocument.data = rows;
                angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('refresh');
                console.log("setan3", rows);

            }, 100);

            setTimeout(function () {
                $scope.noRangkaSelectedBiroJasaStatus = true;
                angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('setting', { allowMultiple: false, detachable: false, observeChanges: true, closable: false }).modal('show').modal('refresh');
                angular.element('.ui.modal.noRangkaSelectedBiroJasa').not(':first').remove();
            }, 200);
            //angular.element('.ui.modal.pilihNoRangka').modal('hide');

        }

        $scope.back = function () {
            $scope.disabledSimpan = false;


            $scope.mMaintainPOBirojasa.ServiceBureauId = null;
            $scope.gridNoRangkaDocument.data = angular.copy($scope.tempData);
            $scope.TotalPriceJasa = 0;
            angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');

            setTimeout(function () {
                //angular.element('.ui.modal.noRangkaSelectedBiroJasa').modal('hide');
                angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('refresh');
            }, 0);

            setTimeout(function () {
                angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('setting', { closable: false }).modal('show');
                angular.element('.ui.modal.pilihNoRangkaBiroJasa').not(':first').remove();
            }, 1);
        }

        $scope.awal = function () {
            $scope.loading = true;
            $scope.getData();
            $scope.detailBirojasa = false, $scope.daftarPO = true;
        }


        $scope.lihatDetail = function (row) {
            $scope.selctedRow = 1;
            $scope.rightStatus = row.POJasaStatusName;
            $scope.mMaintainPOBirojasa = angular.copy(row);
            $scope.detailBirojasa = true, $scope.daftarPO = false;
            $scope.printPO = 'sales/POBiroJasa?POBiroJasaUnitId=' + row.POBiroJasaUnitId;
            $scope.gridDetailNoRangka.data = row.listFrameNo;
        }

        var gridData = [];
        $scope.getData = function () {
            console.log('Sebelum --> ', $scope.filter);
            $scope.loading = true;
            MaintainPOBirojasaFactory.getData($scope.filter).then(function (res) {
                $scope.grid.data = res.data.Result;
                console.log('Sebelum --> ', $scope.filter);
                $scope.loading = false;
            });
        }

        $scope.getPriceJasa = function (bureauiId) {
            //var reGet = { "ServiceBureauId" : bureauiId}
            if (bureauiId != null | bureauiId != undefined) {
                $scope.tempData = angular.copy($scope.gridNoRangkaDocument.data);
                for (var i in $scope.tempData) {
                     $scope.tempData[i].ServiceBureauId = bureauiId;
                }
                console.log("test", $scope.tempData);
                MaintainPOBirojasaFactory.getPriceJasa($scope.tempData).then(function (res) {
                    $scope.gridNoRangkaDocument.data = res.data.Result;
                    var totalPrice = 0;
                    for (var i in $scope.gridNoRangkaDocument.data) {
                        totalPrice = totalPrice + $scope.gridNoRangkaDocument.data[i].NoticePrice + $scope.gridNoRangkaDocument.data[i].UnNoticePrice;
                    }
                    $scope.TotalPriceJasa = angular.copy(totalPrice);
                });
            }else{
                $scope.gridNoRangkaDocument.data = angular.copy($scope.tempData);
                $scope.TotalPriceJasa = 0;
            }

        }

        // var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Lihat AFI" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">Lihat Detil</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        var actionbutton = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.lihatDetail(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            '</a>'

        var actionUploadDocumen = '<button class="ui icon inverted grey button ng-scope" tooltip-placement="bottom" uib-tooltip="Update Dokumen" style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px" onclick="this.blur()" ng-click="grid.appScope.updateDocument(row.entity)" tabindex="0"> <i class="fa fa-fw fa-lg fa-upload"></i> </button>';
        var cross = '<button class="ui icon inverted grey button ng-scope" tooltip-placement="bottom" uib-tooltip="Hapus" style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px" onclick="this.blur()" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" tabindex="0">  <i class="fa fa-fw fa-lg fa-times"></i> </button>';


        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.test = function () {
            for (var i in $scope.gridNoRangkaDocument.data) {
                $scope.gridNoRangkaDocument.data[i].BiroJasa = $scope.birojasa;
            }
        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', visible: false},
                { displayName: 'No. PO', name: 'no po', field: 'POBiroJasaUnitNo' },
                { name: 'jumlah mobil yang diajukan s_t_n_k/BPKB', field: 'JumlahMobil', displayName: 'Jumlah Mobil yang diajukan STNK/BPKB' },
                { name: 'status', field: 'POJasaStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '20%',
                    //pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbutton
                }

            ]
        };

        $scope.gridNoRangka = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,

            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'cabang', field: 'OutletRequestPOName', width: '25%' },
                { name: 'no rangka', field: 'FrameNo', width: '25%' },
                { name: 'nama pelanggan', field: 'CustomerName', width: '25%' },
                { name: 'model tipe kendaraan', field: 'Description', width: '30%' },
            ]
        };

        $scope.gridNoRangkaDocument = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: true,
            showGridFooter: false,
            showColumnFooter: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'cabang', field: 'OutletRequestPOName', enableCellEdit: false },
                { name: 'no rangka', field: 'FrameNo', enableCellEdit: false },
                { name: 'nama pelanggan', field: 'CustomerName', enableCellEdit: false },
                { name: 'model tipe kendaraan', field: 'Description', enableCellEdit: false },
                {
                    name: 'Notice Price *',
                    field: 'NoticePrice',
                    cellFilter: 'currency:"Rp."',
                    enableCellEdit: true,
                    type: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp." : 0 ',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = 'Biaya : ' + aggregation.value
                    }
                },
                {
                    name: 'unnotice price *',
                    field: 'UnNoticePrice',
                    cellFilter: 'currency:"Rp."',
                    enableCellEdit: true,
                    type: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp." : 0 ',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = 'Biaya : ' + aggregation.value
                    }
                }

            ]
        };
        $scope.TotalPriceJasa = 0;

        $scope.gridNoRangkaDocument.onRegisterApi = function (gridApi) {
            $scope.gridFrameNo = gridApi;
            $scope.gridFrameNo.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (newValue < 0) {
                    newValue = 0;
                    rowEntity[colDef.field] = "Biaya " + newValue;
                } else if (newValue > 1000000000000) {
                    rowEntity.Price = 0;
                    bsNotify.show({
                        title: 'Peringatan!',
                        content: 'Angka yang anda masukan terlalu banyak. Maksimal 1.000.000.000.000',
                        type: 'warning'
                    });
                } else {
                    var totalPrice = 0;
                    for (var i in $scope.gridNoRangkaDocument.data) {
                        totalPrice = totalPrice + $scope.gridNoRangkaDocument.data[i].NoticePrice + $scope.gridNoRangkaDocument.data[i].UnNoticePrice;
                    }
                    $scope.TotalPriceJasa = angular.copy(totalPrice);
                    console.log($scope.TotalPriceJasa);
                }
            });
        }

        $scope.gridDetailNoRangka = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: true,
            showGridFooter: false,
            showColumnFooter: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'cabang', field: 'OutletRequestPOName' },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model tipe kendaraan', field: 'Description' },
				{
                    name: 'Notice Price',
                    field: 'NoticePrice',
                    cellFilter: 'currency:"Rp."',
                    enableCellEdit: true,
                    type: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp." : 0 ',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = 'Biaya : ' + aggregation.value
                    }
                },
                {
                    name: 'unnotice price',
                    field: 'UnNoticePrice',
                    cellFilter: 'currency:"Rp."',
                    enableCellEdit: true,
                    type: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp." : 0 ',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = 'Biaya : ' + aggregation.value
                    }
                },
                {
                    name: 'harga',
                    field: 'Price',
                    cellFilter: 'currency:"Rp."',
                    type: 'number',
					visible: false,
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Total Biaya : Rp." : 0 ',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = 'Biaya : ' + aggregation.value
                    }
                }

            ]
        };

        $scope.gridNoRangka.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

    });