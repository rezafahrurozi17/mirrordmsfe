angular.module('app')
    .factory('MaintainPOBirojasaFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/MaintainPOBiroJasa/?start=1&limit=100&' + param);
                return res;
            },
            getDataNoRangka: function() {
                var res = $http.get('/api/sales/LIstFrameNoBiroJasa');
                return res;
            },
            getDataVendor: function() {
                var res = $http.get('/api/sales/MProfileBiroJasa');
                return res;
            },
            getPOJasa: function() {
                var res = $http.get('/api/sales/PStatusPOJasa');
                return res;
            },
            getPriceJasa: function(data){
                var res = $http.post('/api/sales/ListFrameNoBiroJasaWithPrice',data);
                return res;
            },
            create: function(regist) {
                var res = $http.post('/api/sales/SAHPOBiroJasaUnit/Insert', [regist]);
                return res;
            },
            kirim: function(regist) {
                return $http.put('/api/sales/SAHPOBiroJasaUnit/Kirim', [regist]);
            },
            batal: function(regist) {
                return $http.put('/api/sales/SAHPOBiroJasaUnit/Batal', [regist]);
            },
            ubah: function(regist) {
                return $http.put('/api/sales/SAHPOBiroJasaUnit/Update', [regist]);
            }
        }
    });
