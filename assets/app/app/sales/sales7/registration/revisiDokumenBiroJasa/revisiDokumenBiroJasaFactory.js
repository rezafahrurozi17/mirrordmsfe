angular.module('app')
    .factory('RevisiDokumenBiroJasaFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());

                return fix;
            } else {
                return null;
            }

        };

        return {
            getData: function(filter) {
                // filter.JadwalPenerimaan = fixDate(filter.JadwalPenerimaan);
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/ReceiveSTNK?start=1&limit=1000&' + param);
                return res;
            },
            getOutletCabang: function() {
                var res = $http.get('/api/sales/MSitesOutletCAO');
                return res;
            },
            getVendor: function() {
                var res = $http.get('/api/sales/MProfileBiroJasa');
                return res;
            },
			getPStatusAfi: function() {
                var res = $http.get('/api/sales/PStatusAFI');
                return res;
            },
            update: function(RevisiDokumenBiroJasa) {
                return $http.put('/api/sales/TerimaSTNKDariBiroJasa', [{
                    OutletId: RevisiDokumenBiroJasa.OutletId,
                    OutletName: RevisiDokumenBiroJasa.OutletName,
                    TotalData: RevisiDokumenBiroJasa.TotalData
                }]);
            },
            create: function(RevisiDokumenBiroJasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa', [{
                    OutletId: RevisiDokumenBiroJasa.OutletId,
                    OutletName: RevisiDokumenBiroJasa.OutletName,
                    TotalData: RevisiDokumenBiroJasa.TotalData
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/ReceiveSTNK', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd