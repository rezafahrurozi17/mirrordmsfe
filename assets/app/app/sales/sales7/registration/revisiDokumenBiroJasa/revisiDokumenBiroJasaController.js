angular.module('app')
    .controller('RevisiDokumenBiroJasaController', function($scope, $http, CurrentUser, RevisiDokumenBiroJasaFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

		$scope.RevisiDokumenBiroJasaOperation="";
		
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            RevisiDokumenBiroJasaFactory.getOutletCabang().then(function(res) {
                $scope.outletCabang = res.data.Result;
            });

            RevisiDokumenBiroJasaFactory.getVendor().then(function(res) {
                $scope.VendorRevisiDokumenBiroJasa = res.data.Result;
            });
			
			RevisiDokumenBiroJasaFactory.getPStatusAfi().then(function(res) {
                var yang_belom_di_filter = res.data.Result;
				
				var yang_uda_di_filter = yang_belom_di_filter.filter(function(semprul) {
					return (semprul.StatusName == "Proses Biro Jasa" || semprul.StatusName == "Terima STNK Dari Biro Jasa" || semprul.StatusName == "Terima BPKB Dari Biro Jasa");
				})
				
				$scope.OptionsStatusafi = yang_uda_di_filter;
            });

        });
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiStnk').remove();
		  angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiBpkb').remove();
		});	
		
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mRevisiDokumenBiroJasa = null; //Model
        $scope.RevisiDokumenBiroJasaMain = true;
        $scope.filter = {  OutletId: null, POCode: null,FrameNo: null,StatusAFIId: null, ServiceBureauId: null,SODateStart: null};
        $scope.dateOption ={
            format:'dd-MM-yyyy'
        }
        

        //----------------------------------s
        // Get Data
        //----------------------------------

        
        $scope.RevisiDokumenBiroJasaRevisiStnk = function() {
			$scope.RevisiDokumenBiroJasaOperation="Revisi";
			setTimeout(function() {
					angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiStnk').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiStnk').not(':first').remove();
				}, 1);
            
        }
		
		$scope.RevisiDokumenBiroJasaRevisiBpkb = function() {
			$scope.RevisiDokumenBiroJasaOperation="Revisi";
            setTimeout(function() {
					angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiBpkb').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiBpkb').not(':first').remove();
				}, 1);
        }
		
		$scope.KembaliModalRevisiDokumenBiroJasaRevisiBpkb = function() {

            angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiBpkb').modal('hide');
        }
		
		$scope.KembaliModalRevisiDokumenBiroJasaRevisiStnk = function() {

            angular.element('.ui.modal.ModalRevisiDokumenBiroJasaRevisiStnk').modal('hide');
        }

        

        $scope.simpanTerimaSTNK = function() {
            $scope.The_SelectedTerimaSTNK = angular.copy($scope.rowsDocument);
            $scope.mtd = { Method: "Terima" };
            $scope.mTerimaSTNK = angular.merge({},$scope.The_SelectedTerimaSTNK,$scope.mtd);
            //console.log("adasdasdsa", $scope.mTerimaSTNK);

            RevisiDokumenBiroJasaFactory.create($scope.mTerimaSTNK).then(function() {
                //nanti di get
                RevisiDokumenBiroJasaFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Success",
                            content: "Berhasil terima STNK",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                        // console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromterimaSTNK();
                });

            });

        }

        

        var gridData = [];
        $scope.getData = function() {
            RevisiDokumenBiroJasaFactory.getData($scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        
        var centangan_STNK='<div style="vertical-align:middle;"><i ng-if="row.entity.StatusMatchId==\'Setan\'" class="fa fa-check-square" aria-hidden="true"></i><i ng-if="row.entity.StatusMatchId==\'Setan\'" class="fa fa-square" aria-hidden="true"></i></div>';
		
		var centangan_BPKB='<div style="vertical-align:middle;"><i ng-if="row.entity.StatusMatchId==\'Setan\'" class="fa fa-check-square" aria-hidden="true"></i><i ng-if="row.entity.StatusMatchId==\'Setan\'" class="fa fa-square" aria-hidden="true"></i></div>';
		
        

        

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', visible: false},
                { displayName: 'No PO', name: 'no po', field: 'POBiroJasaUnitNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'cabang', field: 'OutletRequestPOName' },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'ModelTipeKendaraan', field: 'Description' },
                { name: 'model kendaraan', field: 'VehicleModelName' },
                { name: 'STNK', field: 'VehicleModelName' ,cellTemplate: centangan_STNK},
				{ name: 'BPKB', field: 'VehicleModelNamee' ,cellTemplate: centangan_BPKB},

            ]
        };


    });