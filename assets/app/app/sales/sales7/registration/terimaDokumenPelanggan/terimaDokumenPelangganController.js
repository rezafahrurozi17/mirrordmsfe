angular.module('app')
    .controller('TerimaDokumenPelangganController', function($scope, $http, CurrentUser, TerimaDokumenPelangganFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];

            TerimaDokumenPelangganFactory.getStatus().then(function(res) {
                $scope.status = res.data.Result;
            });

            TerimaDokumenPelangganFactory.getOutletCabang().then(function(res) {
                $scope.outletCabang = res.data.Result;
            })
        });
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.formDocumenUpload').remove();
		  angular.element('.ui.modal.formDocumenUploadsingle').remove();
		  //ini tadi ga ada tapi beres ilangin aja kalo jadi ngaco
		});
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mterimaDokumenPelanggan = null; //Model
        $scope.gridDokumenPelanggan = true;
        //$scope.xRole = { selected: [] };
        $scope.filter = { Id: 0, OutletId: null, TerimaDokumenStatusId: null, SuratDistribusiNo: null };

        $scope.listContainer = true;
        $scope.listDetail = false;
        $scope.rowsDocument = null;
        $scope.formData = false;
        $scope.noRangka = false;
        $scope.num = 0;
        $scope.numview = 1;
        $scope.disabledsimpanupdatesingle = false;
        $scope.disabledsimpanupdate = false;
        //$scope.selctedRow = [{NoRangka:null,NamaPelanggan:null,VehicleModel:null}];

        //$scope.frameno = false;
        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            TerimaDokumenPelangganFactory.getData($scope.filter).then(function(res) {
                    $scope.grid.data = res.data.Result;
                    //console.log("", $scope.grid.data);
                    angular.forEach($scope.grid.data, function(Doc, DocIndx) {
                        angular.forEach(Doc.listFrameNo, function(Frame, FrameIndx) {
                            angular.forEach(Frame.listDetail, function(Detail, DetailIndx) {
                                if (Detail.ReceivedDate == null || Detail.ReceivedDate == undefined) {
                                    Detail.ReceivedDateBit = false;
                                } else {
                                    Detail.ReceivedDateBit = true;
                                }

                            });
                        });
                    });
                },
                function(err) {
                    console.log("err=>", err);
                }

            )
        };

        $scope.rowselected = {};
        $scope.rowsDocument = [];
        $scope.rowsDocuments = [];

        $scope.UpdateDokumen = function(row) {
            $scope.num = 0;
			$scope.numview = 1;
			
			angular.element('.ui.modal.formDocumenUpload').modal('refresh').modal('show');
            $scope.rowselected = row;
            $scope.rowsDocument = angular.copy(row.listFrameNo);
            $scope.rowsDocument[$scope.num];
            // $scope.checkdate(false)
        }

        $scope.simpanupdate = function() {
            $scope.disabledsimpanupdate = true;
            $scope.rowselected.listFrameNo = $scope.rowsDocument;
            TerimaDokumenPelangganFactory.update($scope.rowselected).then(function(res) {
                angular.element('.ui.modal.formDocumenUpload').modal('refresh').modal('hide');
                $scope.getData();
                bsNotify.show({
                    title: "Berhasil",
                    content: "Berhasil simpan dokumen",
                    type: 'success'
                });
                $scope.disabledsimpanupdate = false;
            })
        }

        $scope.simpanupdatesingle = function() {
            $scope.disabledsimpanupdatesingle = true;
            $scope.temprowsDocuments = [];
            $scope.temprowsDocuments.push($scope.rowsDocuments);
            $scope.rowselected.listFrameNo = $scope.temprowsDocuments;
            TerimaDokumenPelangganFactory.update($scope.rowselected).then(function(res) {

                angular.forEach(res.data.Result, function(Doc, DocIndx) {
                    angular.forEach(Doc.listFrameNo, function(Frame, FrameIndx) {
                        angular.forEach(Frame.listDetail, function(Detail, DetailIndx) {
                            if (Detail.ReceivedDate == null || Detail.ReceivedDate == undefined) {
                                Detail.ReceivedDateBit = false;
                            } else {
                                Detail.ReceivedDateBit = true;
                            }

                        });
                    });
                });

                $scope.gridDetailDokumen.data = res.data.Result[0].listFrameNo;
                angular.element('.ui.modal.formDocumenUploadsingle').modal('refresh').modal('hide');
                // $scope.gridDokumenPelanggan = true; 
                $scope.DetailDokumen = true;
                $scope.getData();
                bsNotify.show({
                    title: "Berhasil",
                    content: "Berhasil simpan dokumen",
                    type: 'success'
                });
                $scope.disabledsimpanupdatesingle = false;
            })
        }

        var i = 0;

        $scope.next = function() {
            $scope.num = $scope.num + 1;
            $scope.numview = $scope.num + 1;
			console.log("setan1",$scope.num);
			console.log("setan2",$scope.numview);

        }

        $scope.prev = function() {
            $scope.num = $scope.num - 1;
            $scope.numview = $scope.num + 1;//ini tadinya-1
			console.log("setan1",$scope.num);
			console.log("setan2",$scope.numview);
        }


        $scope.listRangka = function(row) {
            angular.element('.ui.modal.formDocumenUploadsingle').modal('show');
            // console.log("selected rows", row);
            $scope.rowsDocuments = angular.copy(row);
            //console.log(' $scope.rowsDocuments', $scope.rowsDocuments);
            // $scope.checkdate(false)
            // $scope.documen.ReceivedDateBit = false;
        }

        $scope.lihatDetail = function(row) {
            $scope.gridDokumenPelanggan = false, $scope.DetailDokumen = true;
            $scope.gridDetailDokumen.data = row.listFrameNo;
            $scope.rowselected = row;
        }

        $scope.back = function() {
            $scope.gridDokumenPelanggan = true, $scope.DetailDokumen = false;
        }

        $scope.close = function() {
            angular.element(".ui.modal.formDocumenUpload").modal("hide");
            angular.element('.ui.modal.formDocumenUploadsingle').modal('hide');

        }

        $scope.viewImage = function(dokumen) {
            console.log('dokumen',dokumen);
            TerimaDokumenPelangganFactory.getImage(dokumen.DocumentURL).then(function (res) {
                $scope.dokumenName = dokumen.DocumentName;
                $scope.dokumenData = res.data.UpDocObj;
                setTimeout(function () { angular.element(".ui.modal.dokumenTerimaLihat").modal("refresh") }, 0);
                angular.element(".ui.modal.dokumenTerimaLihat").modal("show");
                angular.element('.ui.modal.dokumenTerimaLihat').not(':first').remove();
            });
        }

        $scope.KeluarDokumenTerima = function(){
            angular.element(".ui.modal.dokumenTerimaLihat").modal("refresh").modal("hide");
            setTimeout(function () { angular.element(".ui.modal.formDocumenUploadsingle").modal("refresh") }, 0);
            angular.element(".ui.modal.formDocumenUploadsingle").modal("show");
        }

        $scope.checkdate = function(doc, row ,page) {
            if (doc.ReceivedDateBit == false) {
                doc.ReceivedDate = new Date();
                
                if (page == 'detail'){
                    var cekAllDocChecked = 0;
                    for (var i=0; i<row.listDetail.length; i++){
                        if (row.listDetail[i].ReceivedDateBit == true){
                            cekAllDocChecked++;
                        }
                    }

                    if (cekAllDocChecked == (row.listDetail.length-1)){
                        row.DokumenLengkap = true;
                    } else {
                        row.DokumenLengkap = false;
                    }
                }
            } else {
                doc.ReceivedDate = null;
                // jika awal nya true dan di uncheck masuk ke sini.
                if (page == 'detail'){
                    if (row.DokumenLengkap == true){
                        row.DokumenLengkap = false;
                    }
                }

            }
        }

        $scope.checkdatedepan = function(docs, rows, pages){

            if (docs.ReceivedDateBit == false) {
                docs.ReceivedDate = new Date();
                
                if (pages == 'main'){
                    var cekAllDocChecked = 0;
                    for (var i=0; i<rows.listDetail.length; i++){
                        if (rows.listDetail[i].ReceivedDateBit == true){
                            cekAllDocChecked++;
                        }
                    }

                    if (cekAllDocChecked == (rows.listDetail.length-1)){
                        rows.DokumenLengkap = true;
                    } else {
                        rows.DokumenLengkap = false;
                    }
                }
            } else {
                docs.ReceivedDate = null;
                // jika awal nya true dan di uncheck masuk ke sini.
                if (pages == 'main'){
                    if (rows.DokumenLengkap == true){
                        rows.DokumenLengkap = false;
                    }
                }

            }
        }


        $scope.checkKelengkapanDepan = function(listDetailDepan, valDepan) {
            console.log(listDetailDepan, valDepan)
            if (valDepan == false){
                //  false berarti belum centang, dan mau di check/centang. maka loop nya yg ini :
                for (var i=0; i<listDetailDepan.length; i++){ //sampai jumlah data terpenuhi
                    if (listDetailDepan[i].ReceivedDateBit == false){ //jika dokumen/data nya belum centang(berniali false) maka 
                        listDetailDepan[i].ReceivedDateBit = true; //ubah jadi centang/true
                        listDetailDepan[i].ReceivedDate = new Date(); // lalu data ReceivedDate  diubah jadi tgl saat dicentang
                    }
                }
            } else {
                //sebelumnya checked/centang, tapi mau di uncheck
                for (var i=0; i<listDetailDepan.length; i++){
                    if (listDetailDepan[i].ReceivedDateBit == true){
                        listDetailDepan[i].ReceivedDateBit = false;
                        listDetailDepan[i].ReceivedDate = null;
                    }
                }
            }
        }

        $scope.checkKelengkapan = function(listDetail, val) {
            console.log(listDetail, val)
            if (val == false){
                //  false berarti belum centang, dan mau di check/centang. maka loop nya yg ini :
                for (var i=0; i<listDetail.length; i++){ //sampai jumlah data terpenuhi
                    if (listDetail[i].ReceivedDateBit == false){ //jika dokumen/data nya belum centang(berniali false) maka 
                        listDetail[i].ReceivedDateBit = true; //ubah jadi centang/true
                        listDetail[i].ReceivedDate = new Date(); // lalu data ReceivedDate  diubah jadi tgl saat dicentang
                    }
                }
            } else {
                //sebelumnya checked/centang, tapi mau di uncheck
                for (var i=0; i<listDetail.length; i++){
                    if (listDetail[i].ReceivedDateBit == true){
                        listDetail[i].ReceivedDateBit = false;
                        listDetail[i].ReceivedDate = null;
                    }
                }
            }
        }

        $scope.checkifDate = function(doc) {
            if (doc.ReceivedDate == null) {
                doc.ReceivedDateBit = false;
            } else {
                doc.ReceivedDateBit = true;
            }
        }

        // var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Lihat Detil" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">Lihat Detil</u>&nbsp' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Update Dokumen" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)">Update Dokumen</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        var actionbutton = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.lihatDetail(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update Dokumen" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';

        // var actionbuttondocument = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Update Documen" ng-click="grid.appScope.listRangka(row.entity)">Update Documen</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        var actionbuttondocument = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update Dokumen" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.listRangka(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            '</a>';


        var checkdisterima = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.TerimaBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.TerimaBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

        var checklengkap = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.LengkapBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.LengkapBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

            
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.test = function() {
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------  
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'no distribusi', field: 'SuratDistribusiNo' },
                { name: 'cabang', field: 'OutletName' },
                { name: 'qty diterima/qty no rangka', field: 'QtyDokumenPerFrameNo' },
                { name: 'status', field: 'TerimaDokumenStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbutton
                }
            ]
        };

        $scope.pageArray = [10,25,50];
        
        $scope.gridDetailDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes:  $scope.pageArray,
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'diterima', cellTemplate: checkdisterima },
                { name: 'lengkap', cellTemplate: checklengkap },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model kendaraan', field: 'Description' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbuttondocument
                }
            ]
        };


    });