angular.module('app')
    .factory('TerimaDokumenPelangganFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(param) {
                var filter = $httpParamSerializer(param);
                var res = $http.get('/api/sales/TerimaDokumen?start=1&limit=1000&' + filter);
                return res;
            },
            update: function(terimaDokumen) {
                return $http.put('/api/sales/TerimaDokumen/update', [terimaDokumen]);
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },
            getStatus: function() {
                var res = $http.get('/api/sales/PStatusTerimaDokumen');
                return res;
            },
            getOutletCabang: function() {
                var res = $http.get('/api/sales/MSitesOutletCAO');
                return res;
            }
        }
    });
//ddd