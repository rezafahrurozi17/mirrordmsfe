angular.module('app')
    .factory('TerimaBpkbFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            // getData: function(filter) {
            //     var param = $httpParamSerializer(filter);
            //     var res = $http.get('/api/sales/ReceiveBPKB?start=1&limit=1000&' + param);
            //     return res;
            // },

            getData: function(filter) {
                var param = $httpParamSerializer(filter);
                console.log('get',param);
                param.TanggalPenerimaan = fixDate(filter.param);
                var res = $http.get('/api/sales/TerimaBPKBDariBiroJasa/GetData?start=1&limit=1000000&' + param);
                return res;
            },
            
            getPStatusAfi: function() {
                var res = $http.get('/api/sales/PStatusAFI');
                return res;
            },

            create: function(TerimaBpkb) {
                return $http.post('/api/sales/TerimaBPKBDariBiroJasa', [{
                    OutletId: TerimaBpkb.OutletId,
                    OutletName: TerimaBpkb.OutletName,
                    OutletRequestPOId: TerimaBpkb.OutletRequestPOId,
                    OutletRequestPOName: TerimaBpkb.OutletRequestPOName,
                    ServiceBureauId: TerimaBpkb.ServiceBureauId,
                    ServiceBureauCode: TerimaBpkb.ServiceBureauCode,
                    ServiceBureauName: TerimaBpkb.ServiceBureauName,
                    POBiroJasaUnitId: TerimaBpkb.POBiroJasaUnitId,
                    POBiroJasaUnitNo: TerimaBpkb.POBiroJasaUnitNo,
                    POBiroJasaUnitDate: fixDate(TerimaBpkb.POBiroJasaUnitDate),
                    FrameNo: TerimaBpkb.FrameNo,
                    CustomerId: TerimaBpkb.CustomerId,
                    CustomerName: TerimaBpkb.CustomerName,
                    VehicleTypeColorId: TerimaBpkb.VehicleTypeColorId,
                    Description: TerimaBpkb.Description,
                    KatashikiCode: TerimaBpkb.KatashikiCode,
                    Method: TerimaBpkb.Method,
                    SuffixCode: TerimaBpkb.SuffixCode,
                    VehicleModelId: TerimaBpkb.VehicleModelId,
                    VehicleModelName: TerimaBpkb.VehicleModelName,
                    ColorId: TerimaBpkb.ColorId,
                    ColorCode: TerimaBpkb.ColorCode,
                    ColorName: TerimaBpkb.ColorName,
                    ReceiveId: TerimaBpkb.ReceiveId,
                    BPKBName: TerimaBpkb.BPKBName,
                    BPKBNo: TerimaBpkb.BPKBNo,
                    PoliceNumber: TerimaBpkb.PoliceNumber,
                    RequestBPKBDate: fixDate(TerimaBpkb.RequestBPKBDate),
                    ReceiveBPKBDate: fixDate(TerimaBpkb.ReceiveBPKBDate),
                    RevisiBPKBBit: TerimaBpkb.RevisiBPKBBit,
                    AlasanRevisiBPKB: TerimaBpkb.AlasanRevisiBPKB,
                    AlasanKeterlambatanBPKB: TerimaBpkb.AlasanKeterlambatanBPKB,
                    LamaKeterlambatanBPKB: TerimaBpkb.LamaKeterlambatanBPKB,
                    TotalData: TerimaBpkb.TotalData
                }]);
            },

            batalTerimaBPKB: function(listBatalTerimaBPKB) {
                return $http.post('/api/sales/TerimaBPKBDariBiroJasa/BatalTerima', listBatalTerimaBPKB);
            },

            TerimaBPKB: function(listTerimaBPKB) {
                return $http.post('/api/sales/TerimaBPKBDariBiroJasa/TerimaBPKB', listTerimaBPKB);
            },

            update: function(TerimaBpkb) {
                return $http.put('/api/sales/TerimaBPKBDariBiroJasa', [{
                    OutletId: TerimaBpkb.OutletId,
                    OutletName: TerimaBpkb.OutletName,
                    OutletRequestPOId: TerimaBpkb.OutletRequestPOId,
                    OutletRequestPOName: TerimaBpkb.OutletRequestPOName,
                    ServiceBureauId: TerimaBpkb.ServiceBureauId,
                    ServiceBureauCode: TerimaBpkb.ServiceBureauCode,
                    ServiceBureauName: TerimaBpkb.ServiceBureauName,
                    POBiroJasaUnitId: TerimaBpkb.POBiroJasaUnitId,
                    POBiroJasaUnitNo: TerimaBpkb.POBiroJasaUnitNo,
                    POBiroJasaUnitDate: fixDate(TerimaBpkb.POBiroJasaUnitDate),
                    FrameNo: TerimaBpkb.FrameNo,
                    CustomerId: TerimaBpkb.CustomerId,
                    CustomerName: TerimaBpkb.CustomerName,
                    VehicleTypeColorId: TerimaBpkb.VehicleTypeColorId,
                    Method: TerimaBpkb.Method,
                    Description: TerimaBpkb.Description,
                    KatashikiCode: TerimaBpkb.KatashikiCode,
                    SuffixCode: TerimaBpkb.SuffixCode,
                    VehicleModelId: TerimaBpkb.VehicleModelId,
                    VehicleModelName: TerimaBpkb.VehicleModelName,
                    ColorId: TerimaBpkb.ColorId,
                    ColorCode: TerimaBpkb.ColorCode,
                    ColorName: TerimaBpkb.ColorName,
                    ReceiveId: TerimaBpkb.ReceiveId,
                    BPKBName: TerimaBpkb.BPKBName,
                    BPKBNo: TerimaBpkb.BPKBNo,
                    PoliceNumber: TerimaBpkb.PoliceNumber,
                    RequestBPKBDate: fixDate(TerimaBpkb.RequestBPKBDate),
                    ReceiveBPKBDate: fixDate(TerimaBpkb.ReceiveBPKBDate),
                    RevisiBPKBBit: TerimaBpkb.RevisiBPKBBit,
                    AlasanRevisiBPKB: TerimaBpkb.AlasanRevisiBPKB,
                    AlasanKeterlambatanBPKB: TerimaBpkb.AlasanKeterlambatanBPKB,
                    LamaKeterlambatanBPKB: TerimaBpkb.LamaKeterlambatanBPKB,
                    TotalData: TerimaBpkb.TotalData
                }]);
            },

            delete: function(id) {
                return $http.delete('/api/sales/ReceiveBPKB', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });