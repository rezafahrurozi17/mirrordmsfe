angular.module('app')
    .controller('TerimaBpkbController', function($scope, $http, CurrentUser, TerimaBpkbFactory, TerimaSTNKBirojasaFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            TerimaSTNKBirojasaFactory.getOutletCabang().then(function(res) {
                $scope.outletCabang = res.data.Result;
            });

            TerimaSTNKBirojasaFactory.getVendor().then(function(res) {
                $scope.vendor = res.data.Result;
            });
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.disabledTerimaBPKB = true;
        $scope.user = CurrentUser.user();
        $scope.mTerimaBpkb = null; //Model
        $scope.daftarPO = true;
        //$scope.filter = { Id: 0, OutletId: null, POBiroJasaUnitNo: null, FrameNo: null, CustomerName: null, ServiceBureauId: null };
        $scope.filter = { Id: 0, OutletId: null, POBiroJasaUnitNo: null, FrameNo: null, STNKName: null, ServiceBureauId: null };
        //----------------------------------
        // Get Data
        //----------------------------------

        TerimaBpkbFactory.getPStatusAfi().then(function (res) {
            //$scope.statusafi = res.data.Result;

            var yang_belom_di_filter = res.data.Result;
				
				var yang_uda_di_filter = yang_belom_di_filter.filter(function(statusAFIFilter) {
					return (statusAFIFilter.StatusName == "Proses Biro Jasa" || statusAFIFilter.StatusName == "Terima BPKB Dari Biro Jasa");
				})
				
                $scope.statusafi = yang_uda_di_filter;
                
        })

        $scope.terimaBPKB = function(SelectedterimaBPKB) {
            SetReceiveBPKBDateMinDate(SelectedterimaBPKB.POBiroJasaUnitDate); //ini kode buat logika tanggal ga bole mundur
            $scope.rowsDocument = angular.copy(SelectedterimaBPKB);
            angular.element('.ui.small.modal.terimaBPKB').modal('setting', { closable: false }).modal('show');
        }

        $scope.simpanterimaBPKB = function() {
            $scope.The_SelectedterimaBPKB = angular.copy($scope.rowsDocument);
            $scope.mtd = { Method: "Terima" };
            $scope.mTerimaBPKB = angular.merge({},$scope.The_SelectedterimaBPKB,$scope.mtd);

            TerimaBpkbFactory.create($scope.mTerimaBPKB).then(function() {
                //nanti di get
                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Dokumen BPKB sudah diterima.",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                        // console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromterimaBPKB();

                });

            });

        }

        $scope.backFromterimaBPKB = function() {

            $scope.rowsDocument = null;
            angular.element('.ui.small.modal.terimaBPKB').modal('hide');
        }

        $scope.alasanterlambat = function(SelectedDataTerlambat) {
            $scope.backFromAlasanRevisi();
            $scope.The_SelectedDataTerlambat = angular.copy(SelectedDataTerlambat);
            $scope.SelectedDataTerlambat_FrameNo = angular.copy(SelectedDataTerlambat.FrameNo);
            $scope.SelectedDataTerlambat_PONo = angular.copy(SelectedDataTerlambat.POBiroJasaUnitNo);
            $scope.SelectedDataTerlambat_Model = angular.copy(SelectedDataTerlambat.VehicleModelName);
            $scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB = angular.copy(SelectedDataTerlambat.AlasanKeterlambatanBPKB);
            $scope.SelectedDataTerlambat_LamaKeterlambatanBPKB = angular.copy(SelectedDataTerlambat.LamaKeterlambatanBPKB);
            angular.element('.ui.small.modal.alasanterlambatBPKB').modal('setting', { closable: false }).modal('show');
        }

        $scope.simpanAlasanRevisi = function() {
            $scope.The_SelectedDataTerlambat['AlasanKeterlambatanBPKB'] = angular.copy($scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB);
            $scope.The_SelectedDataTerlambat['LamaKeterlambatanBPKB'] = angular.copy($scope.SelectedDataTerlambat_LamaKeterlambatanBPKB);
            $scope.mtd = { Method: "Delay" };
            $scope.mBpkbMethod = angular.merge({},$scope.The_SelectedDataTerlambat,$scope.mtd);

            TerimaBpkbFactory.create($scope.mBpkbMethod).then(function() {
                //nanti di get
                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Delay penerimaan BPKB sudah diupdate.",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                        // console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromAlasanRevisi();

                });

            });
        }

        $scope.backFromAlasanRevisi = function() {
            $scope.SelectedDataTerlambat_FrameNo = null;
            $scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB = null;
            $scope.SelectedDataTerlambat_LamaKeterlambatanBPKB = null;
            angular.element('.ui.small.modal.alasanterlambatBPKB').modal('hide');
        }

        $scope.batalRevisiBPKB = function (){
            angular.element('.ui.small.modal.alasanrevisiBPKB').modal('hide');
        }

        $scope.revisiBPKB = function(SelectedRevisiBpkb) {
            $scope.The_SelectedRevisiBPKB = angular.copy(SelectedRevisiBpkb);
            $scope.SelectedDataRevisiBPKB = angular.copy(SelectedRevisiBpkb);
            console.log("test", SelectedRevisiBpkb);
            angular.element('.ui.small.modal.revisibpkb').modal('setting', { closable: false }).modal('show');
        }

        $scope.openRevisiBPKB = function (){
            angular.element('.ui.small.modal.alasanrevisiBPKB').modal('setting', { closable: false }).modal('show');
            angular.element('.ui.small.modal.revisibpkb').modal('setting', { closable: false }).modal('hide')
        }

        $scope.SaveRevisiBPKB = function() {
            $scope.The_SelectedRevisiBPKB['AlasanRevisiBPKB'] = angular.copy($scope.SelectedDataRevisiBPKB.AlasanRevisiBPKB);
            $scope.The_SelectedRevisiBPKB['LamaKeterlambatanBPKB'] = angular.copy($scope.SelectedDataRevisiBPKB.LamaKeterlambatanBPKB);
            $scope.The_SelectedRevisiBPKB['RevisiBPKBBit'] = angular.copy(true);
            $scope.mtd = {Method: "Revisi"};
            $scope.mRevisiBPKB = angular.merge({},$scope.The_SelectedRevisiBPKB,$scope.mtd); 

            TerimaBpkbFactory.create($scope.mRevisiBPKB).then(function() {
                //nanti di get
                bsNotify.show({
                    title: "Berhasil",
                    content: "Delay penerimaan BPKB sudah diupdate.",
                    type: 'success'
                });
                angular.element('.ui.small.modal.alasanrevisiBPKB').modal('setting', { closable: false }).modal('hide');
                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromRevisiBPKB();

                });

            });
        }

        $scope.backFromRevisiBPKB = function() {
            angular.element('.ui.small.modal.revisibpkb').modal('hide');
        }

        var gridData = [];
        $scope.getData = function() {
            TerimaBpkbFactory.getData($scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        var Da_TodayDate=new Date();
		var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
		//$scope.filter.TanggalPenerimaan=Da_TodayDate;

        // var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" ng-if="row.entity.TombolDelay == true" uib-tooltip="Alasan Delay BPKB" ng-click="grid.appScope.$parent.alasanterlambat(row.entity)">Alasan Delay</u>&nbsp' +
        //     '|&nbsp<u tooltip-placement="bottom" ng-if="row.entity.TombolRevisi == true" uib-tooltip="Revisi BPKB" ng-click="grid.appScope.$parent.revisiBPKB(row.entity)">Revisi</u>&nbsp' +
        //     '|&nbsp<u tooltip-placement="bottom" ng-if="row.entity.TombolTerima == true" uib-tooltip="Terima BPKB" ng-click="grid.appScope.$parent.terimaBPKB(row.entity)">Terima</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        var actionbutton =''+
        '<a href="" style="color:#777;" ng-show="row.entity.TombolRevisi == true" class="trlink ng-scope" uib-tooltip="Revisi BPKB" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.revisiBPKB(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a  href="" style="color:#777;" ng-show="row.entity.TombolTerima == true" class="trlink ng-scope" uib-tooltip="Terima BPKB" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.terimaBPKB(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-handshake-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';

        var actionbpkbbutton =''+
        '<div class="col-xs-1" style="margin-top: 8px"> ' +
        
        '<span><i ng-class="{\'fa fa-square-o\' : row.entity.TerimaBPKBBit == false || !toogle, \'fa fa-check-square\' : row.entity.TerimaBPKBBit == true || toogle}" style="font-size: 15px" aria-hidden="true"></i> ' +
        '</div>';


        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            $scope.SelectedRows = rows;
            if(rows.length > 0){
                $scope.disabledTerimaBPKB =false;
            }else{
                $scope.disabledTerimaBPKB = true;
            }
        }
        // $scope.loadingonProses =false;

        $scope.terimaBPKB = function() {
        // if ($scope.SelectedRows == null || $scope.SelectedRows == undefined){
        //     $scope.disabledTerimaBPKB = true;
        // }
            $scope.disabledTerimaBPKB =true;
            // $scope.loadingonProses = true;
           console.log('isi row',$scope.SelectedRows);
           TerimaBpkbFactory.TerimaBPKB($scope.SelectedRows).then(
            function(res) {
                bsNotify.show({
                    title: "Berhasil",
                    content: "Dokumen BPKB berhasil diterima.",
                    type: 'success'
                });
                // $scope.disabledTerimaBPKB = false;
                // $scope.loadingonProses =false;
                $scope.getData();
                //$scope.grid.data = res.data.Result;
                //$scope.loading = false;
                //return res.data;
            },
            function(err) {
                console.log("err=>", err);
                bsNotify.show({
                    title: "Gagal",
                    content: "Data Gagal disimpan.",
                    type: 'danger'
                });
                // $scope.disabledTerimaBPKB = false;
                // $scope.loadingonProses = false;
            }
        );

        }

        $scope.batalTerimaBPKB = function() {
            console.log('isi row',$scope.SelectedRows);
            TerimaBpkbFactory.batalTerimaBPKB($scope.SelectedRows).then(
                    function(res) {
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Peneriman dokumen BPKB berhasil dibatalkan.",
                            type: 'success'
                        });

                        $scope.getData();
                        //$scope.grid.data = res.data.Result;
                        //$scope.loading = false;
                        //return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Gagal",
                            content: "Data Gagal disimpan.",
                            type: 'danger'
                        });
                    }
                );
             
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.test = function() {
            for (var i in $scope.gridNoRangkaDocument.data) {
                $scope.gridNoRangkaDocument.data[i].BiroJasa = $scope.birojasa;
            }
        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', visible: false},
                { name: 'no polisi', field: 'PoliceNumber' },
                // { name: 'no po', field: 'POBiroJasaUnitNo' },
                { name: 'Nama STNK',displayname:'Nama STNK', field: 'STNKName' },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'Cabang', field: 'OutletRequestPOName' },
                
                // { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model tipe kendaraan', field: 'Description' },
                // { name: 'Status', field: 'BPKBStatusName'},

                {
                    name: 'BPKB',
                    displayname:'BPKB',
                    allowCellFocus: false,
                    // width: '10%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    enableFiltering:false,
                    visible:true,
                    cellTemplate: actionbpkbbutton
                }

                // {
                //     name: 'action',
                //     allowCellFocus: false,
                //     width: '10%',
                //     pinnedRight: true,
                //     enableColumnMenu: false,
                //     enableSorting: false,
                //     enableColumnResizing: true,
                //     cellTemplate: actionbutton
                // }
            ]
        };
    });