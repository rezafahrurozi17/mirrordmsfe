angular.module('app')
  .factory('TerimaDokumenBpkbFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    // function fixDate(date) {
    //   if (date != null || date != undefined) {
    //       var fix = date.getFullYear() + '-' +
    //           ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
    //           ('0' + date.getDate()).slice(-2) + 'T' +
    //           ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
    //           ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
    //           ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
    //       return fix;
    //   } else {
    //       return null;
    //   }
    // };
    return {
      getData: function(param) {
        if(param == undefined || param == null || param == "") {
          param="";
        } else {
          param = "&" + param
        }
        var res = $http.get('/api/sales/TerimaDokumenBPKB/?start=1&limit=1000'+param);
        return res;
        // var param=$httpParamSerializer(filter);
        // var res=$http.get('/api/sales/TerimaDokumenBPKB/?start=1&limit=1000&'+param);
        // return res;
      },
      update: function(TerimaDokumenBpkb){
        // for(var i in TerimaDokumenBpkb.listFrameNo){
        //   for(var j in TerimaDokumenBpkb.listFrameNo[i].listDetail){
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].BPKBReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].BPKBReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FakturTAMReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FakturTAMReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FormStatusGesekReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FormStatusGesekReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].CertificateNIKReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].CertificateNIKReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FormAReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].FormAReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiReceiveDate);
        //     TerimaDokumenBpkb.listFrameNo[i].listDetail[j].PIBReceiveDate = fixDate(TerimaDokumenBpkb.listFrameNo[i].listDetail[j].PIBReceiveDate);
        //   }
        // }
        var res = $http.put('/api/sales/TerimaDokumenBPKB/Update', [{
                      OutletId:TerimaDokumenBpkb.OutletId,
											OutletName:TerimaDokumenBpkb.OutletName,
											SuratDistribusiBPKBHeaderId:TerimaDokumenBpkb.SuratDistribusiBPKBHeaderId,
											SuratDistribusiBPKBNo:TerimaDokumenBpkb.SuratDistribusiBPKBNo,
											QtyDokumenLengkap:TerimaDokumenBpkb.QtyDokumenLengkap,
											QtyFrameNo:TerimaDokumenBpkb.QtyFrameNo,
											QtyDokumenLengkapPerQtyFrameNo:TerimaDokumenBpkb.QtyDokumenLengkapPerQtyFrameNo,
											TerimaDokumenSTNKBPKBStatusId:TerimaDokumenBpkb.TerimaDokumenSTNKBPKBStatusId,
											TerimaDokumenSTNKBPKBStatusName:TerimaDokumenBpkb.TerimaDokumenSTNKBPKBStatusName,
											StatusCode:TerimaDokumenBpkb.StatusCode,
											TotalData:TerimaDokumenBpkb.TotalData,
											listFrameNo:TerimaDokumenBpkb.listFrameNo,
											LastModifiedDate:TerimaDokumenBpkb.LastModifiedDate,
											LastModifiedUserId:TerimaDokumenBpkb.LastModifiedUserId,
                                            }]);
        return res;
      }
    }
  });
 //ddd