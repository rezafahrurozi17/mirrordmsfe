angular.module('app')
    .controller('TerimaDokumenBpkbController', function($httpParamSerializer, $scope, $http, CurrentUser, TerimaDokumenBpkbFactory, TerimaDokumenStnkFactory, $timeout,bsNotify) {
	// IfGotProblemWithModal();

	$scope.ShowTerimaDokumenBPKBMain=true;
	$scope.DetilSuratDistribusiBpkb="";
	$scope.filter = {Id:0, TerimaDokumenSTNKBPKBStatusId : null
		            , FrameNo : null, SuratDistribusiBPKBNo : null, CustomerName : null};
	
    //----------------------------------
    // Start-Up
	//----------------------------------
	
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
	});
	
	$scope.$on('$destroy', function(){
		angular.element('.ui.modal.UpdatePenerimaanDokumen').remove();
	})
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTerimaDokumenBpkb = null; //Model
    $scope.xRole={selected:[]};

	$scope.getStatusDokumen = [];
	TerimaDokumenStnkFactory.getStatus().then(function(res) {
		$scope.getStatusDokumen = res.data.Result;
	});

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
	$scope.getData = function() {
		var param=$httpParamSerializer($scope.filter);
        TerimaDokumenBpkbFactory.getData(param).then(
            function(res){
				$scope.grid.data = res.data.Result;
				$scope.CekCeklist($scope.grid.data);

				console.log("asdsad",$scope.grid.data );
                $scope.loading=false;
                //return res.data;
            },
            function(err){
            }
		);
	// 	TerimaDokumenBpkbFactory.getData($scope.filter).then(function(res){
	// 		$scope.grid.data = res.data.Result;
	// 		$scope.loading=false;
	// 	},
	// function(err){
	// 	console.log("err=>", err);
	// }
	// );
    }			
    
	function DateFix(date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() + '-' +
			('0' + (date.getMonth() + 1)).slice(-2) + '-' +
			('0' + date.getDate()).slice(-2) + 'T' +
			((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
			((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
			((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
			return fix;
		} else { return null };
	}

	$scope.KembaliKeTerimaDokumenBpkbMain = function() {
			$scope.ShowDetilSuratDistribusiBpkb=false;
			$scope.DetilSuratDistribusiBpkb="";
			$scope.ShowTerimaDokumenBPKBMain=true;
		}
		
		$scope.backFromterimaSTNK = function(){
			angular.element('.ui.modal.UpdatePenerimaanDokumen').modal('hide');
		}
	
	$scope.ShowDetil = function(selected_data) {
			console.log('isi coklat -> ', selected_data);
			$scope.The_Result=angular.copy(selected_data);
	
			$scope.ShowTerimaDokumenBPKBMain=false;
			
			$scope.grid2.data=selected_data.listFrameNo;
			
			for(var i = 0; i < $scope.grid2.data.length; ++i)
			{
				if($scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusName == 'Dokumen Diterima Di Cabang')
				{
					$scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusNameDiTable=true;
				}
				else if($scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusName == 'Dokumen Dikirim Ke Cabang')
				{
					$scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusNameDiTable=false;
				}
			}
			
			for(var i = 0; i < $scope.grid2.data.length; ++i)
			{
				if($scope.grid2.data[i].SuratDistribusiStatusName == 'Lengkap')
				{
					$scope.grid2.data[i].SuratDistribusiStatusNameDiTable=true;
				}
				else
				{
					$scope.grid2.data[i].SuratDistribusiStatusNameDiTable=false;
				}
			}

			
			$scope.ShowDetilSuratDistribusiBpkb=true;

        }
	
	$scope.UpdateDetilSuratDistribusiBpkb = function(selected_data) {
			
			$scope.The_listFrameNo=angular.copy(selected_data);
			$scope.SelectedDetilSuratDistribusi=selected_data.listDetail[0];
			
			if($scope.SelectedDetilSuratDistribusi.SuratDistribusiStatusName == "Lengkap")
			{
				$scope.EditDetilSuratDistribusiBPKBKelengkapan=true;
			}
			else if($scope.SelectedDetilSuratDistribusi.SuratDistribusiStatusName == "Belum Lengkap")
			{
				$scope.EditDetilSuratDistribusiBPKBKelengkapan=false;
			}
			
			$scope.CentangAllDokumen == false;
			$scope.FakturTAMReceiveCekChanged= false;
			$scope.FormStatusGesekReceiveDateCekChanged= false;
			$scope.CertificateNIKReceiveDateCekChanged= false;
			$scope.SuratUjiTipeReceiveDateCekChanged= false;
			$scope.FormAReceiveDateCekChanged= false;
			$scope.SuratRekomendasiReceiveDateCekChanged= false;
			$scope.PIBReceiveDateCekChanged= false;
			$scope.BPKBReceiveDateCekChanged= false;
			$scope.DisablePasSemuaDicentang= false;
			
			$scope.OldFakturTAMReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.FakturTAMReceiveDate);
			$scope.OldFormStatusGesekReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.FormStatusGesekReceiveDate);
			$scope.OldCertificateNIKReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.CertificateNIKReceiveDate);
			$scope.OldSuratUjiTipeReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.SuratUjiTipeReceiveDate);
			$scope.OldFormAReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.FormAReceiveDate);
			$scope.OldSuratRekomendasiReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.SuratRekomendasiReceiveDate);
			$scope.OldPIBReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.PIBReceiveDate);
			$scope.OldBPKBReceiveDate=angular.copy($scope.SelectedDetilSuratDistribusi.BPKBReceiveDate);
			
			if($scope.OldFakturTAMReceiveDate!=null)
			{
				$scope.ProcessedOldFakturTAMReceiveDate=$scope.OldFakturTAMReceiveDate.getFullYear()+'-'+('0' + ($scope.OldFakturTAMReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldFakturTAMReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldFakturTAMReceiveDate=null;
			}
			
			if($scope.OldFormStatusGesekReceiveDate!=null)
			{
				$scope.ProcessedOldFormStatusGesekReceiveDate=$scope.OldFormStatusGesekReceiveDate.getFullYear()+'-'+('0' + ($scope.OldFormStatusGesekReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldFormStatusGesekReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldFormStatusGesekReceiveDate=null;
			}
			
			if($scope.OldCertificateNIKReceiveDate!=null)
			{
				$scope.ProcessedOldCertificateNIKReceiveDate=$scope.OldCertificateNIKReceiveDate.getFullYear()+'-'+('0' + ($scope.OldCertificateNIKReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldCertificateNIKReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldCertificateNIKReceiveDate=null;
			}
			
			if($scope.OldSuratUjiTipeReceiveDate!=null)
			{
				$scope.ProcessedOldSuratUjiTipeReceiveDate=$scope.OldSuratUjiTipeReceiveDate.getFullYear()+'-'+('0' + ($scope.OldSuratUjiTipeReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldSuratUjiTipeReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldSuratUjiTipeReceiveDate=null;
			}
			
			if($scope.OldFormAReceiveDate!=null)
			{
				$scope.ProcessedOldFormAReceiveDate=$scope.OldFormAReceiveDate.getFullYear()+'-'+('0' + ($scope.OldFormAReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldFormAReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldFormAReceiveDate=null;
			}
			
			if($scope.OldSuratRekomendasiReceiveDate!=null)
			{
				$scope.ProcessedOldSuratRekomendasiReceiveDate=$scope.OldSuratRekomendasiReceiveDate.getFullYear()+'-'+('0' + ($scope.OldSuratRekomendasiReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldSuratRekomendasiReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldSuratRekomendasiReceiveDate=null;
			}
			
			if($scope.OldPIBReceiveDate!=null)
			{
				$scope.ProcessedOldPIBReceiveDate=$scope.OldPIBReceiveDate.getFullYear()+'-'+('0' + ($scope.OldPIBReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldPIBReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldPIBReceiveDate=null;
			}
			
			if($scope.OldBPKBReceiveDate!=null)
			{
				$scope.ProcessedOldBPKBReceiveDate=$scope.OldBPKBReceiveDate.getFullYear()+'-'+('0' + ($scope.OldBPKBReceiveDate.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldBPKBReceiveDate.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldBPKBReceiveDate=null;
			}
			
			$scope.EditDetilSuratDistribusi=angular.copy($scope.SelectedDetilSuratDistribusi);
			angular.element('.ui.modal.UpdatePenerimaanDokumen').modal('show');
        }
	
	$scope.BPKBReceiveDateChanged = function(status) {
		
		if(status == true)
		{
			$scope.EditDetilSuratDistribusi.BPKBReceiveDate=new Date();
			if($scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
					$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
				}
		}else{
			$scope.EditDetilSuratDistribusi.BPKBReceiveDate=null;
			if(($scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
				$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
				$scope.$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
				$scope.$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
				$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
				$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
					$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
				}
		}
		// if($scope.BPKBReceiveDateCekChanged == false)
		// {
		// 	$scope.EditDetilSuratDistribusi.BPKBReceiveDate=$scope.OldBPKBReceiveDate;	
		// }
		
	}

	$scope.FakturTAMReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.FakturTAMReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.FakturTAMReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.FakturTAMReceiveCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.FakturTAMReceiveDate=$scope.OldFakturTAMReceiveDate;	
			// }
			
        }

	$scope.FormStatusGesekReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.FormStatusGesekReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDate=$scope.OldFormStatusGesekReceiveDate;	
			// }
			
        }

	$scope.CertificateNIKReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.CertificateNIKReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDate=$scope.OldCertificateNIKReceiveDate;	
			// }
			
        }
		
	$scope.SuratUjiTipeReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.SuratUjiTipeReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDate=$scope.OldSuratUjiTipeReceiveDate;	
			// }
			
        }

	$scope.FormAReceiveDateChanged = function(status) {
			
			if(status== true)
			{
				$scope.EditDetilSuratDistribusi.FormAReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.FormAReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.FormAReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.FormAReceiveDate=$scope.OldFormAReceiveDate;	
			// }
			
        }
		
	$scope.SuratRekomendasiReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.SuratRekomendasiReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDate=$scope.OldSuratRekomendasiReceiveDate;	
			// }
			
        }

	$scope.PIBReceiveDateChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.PIBReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.PIBReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == true &&
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged == false ||
					$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}	
			// if($scope.PIBReceiveDateCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.PIBReceiveDate=$scope.OldPIBReceiveDate;	
			// }
			
        }
		
	//------------------------------------
	//  Fungsi sinkronisasi tickbox
	//------------------------------------

	$scope.BPKBTickBox = function(Obj){
		if(Obj.BPKBReceiveDate != null){
			Obj.BPKBReceiveDateCekChanged = true;
			if(Obj.FakturTAMReceiveCekChanged == true &&
				Obj.FormStatusGesekReceiveDateCekChanged == true &&
				Obj.CertificateNIKReceiveDateCekChanged == true &&
				Obj.SuratUjiTipeReceiveDateCekChanged == true &&
				Obj.FormAReceiveDateCekChanged == true &&
				Obj.SuratRekomendasiReceiveDateCekChanged == true &&
				Obj.PIBReceiveDateCekChanged == true){
					Obj.DokumenLengkap = true;
				}
		} else if(Obj.BPKBReceiveDate == null) {
			Obj.BPKBReceiveDateCekChanged = false;
			if((Obj.FakturTAMReceiveCekChanged == true &&
				Obj.FormStatusGesekReceiveDateCekChanged == true &&
				Obj.CertificateNIKReceiveDateCekChanged == true &&
				Obj.SuratUjiTipeReceiveDateCekChanged == true &&
				Obj.FormAReceiveDateCekChanged == true &&
				Obj.SuratRekomendasiReceiveDateCekChanged == true &&
				Obj.PIBReceiveDateCekChanged == true) ||
				Obj.FakturTAMReceiveCekChanged == false ||
				Obj.FormStatusGesekReceiveDateCekChanged == false ||
				Obj.CertificateNIKReceiveDateCekChanged == false ||
				Obj.SuratUjiTipeReceiveDateCekChanged == false ||
				Obj.FormAReceiveDateCekChanged == false ||
				Obj.SuratRekomendasiReceiveDateCekChanged == false ||
				Obj.PIBReceiveDateCekChanged == false){
					Obj.DokumenLengkap = false;
				}
		}
	}

	$scope.FakturTAMTickBox = function(lontong){
		if(lontong.FakturTAMReceiveDate != null){
			lontong.FakturTAMReceiveCekChanged = true;
			if(lontong.BPKBReceiveDateCekChanged == true &&
				lontong.FormStatusGesekReceiveDateCekChanged == true &&
				lontong.CertificateNIKReceiveDateCekChanged == true &&
				lontong.SuratUjiTipeReceiveDateCekChanged == true &&
				lontong.FormAReceiveDateCekChanged == true &&
				lontong.SuratRekomendasiReceiveDateCekChanged == true &&
				lontong.PIBReceiveDateCekChanged == true){
					lontong.DokumenLengkap = true;
				}
		} else if(lontong.FakturTAMReceiveDate == null){
			lontong.FakturTAMReceiveCekChanged = false;
			if((lontong.BPKBReceiveDateCekChanged == true &&
				lontong.FormStatusGesekReceiveDateCekChanged == true &&
				lontong.CertificateNIKReceiveDateCekChanged == true &&
				lontong.SuratUjiTipeReceiveDateCekChanged == true &&
				lontong.FormAReceiveDateCekChanged == true &&
				lontong.SuratRekomendasiReceiveDateCekChanged == true &&
				lontong.PIBReceiveDateCekChanged == true) ||
				lontong.BPKBReceiveDateCekChanged == false ||
				lontong.FormStatusGesekReceiveDateCekChanged == false ||
				lontong.CertificateNIKReceiveDateCekChanged == false ||
				lontong.SuratUjiTipeReceiveDateCekChanged == false ||
				lontong.FormAReceiveDateCekChanged == false ||
				lontong.SuratRekomendasiReceiveDateCekChanged == false ||
				lontong.PIBReceiveDateCekChanged == false){
					lontong.DokumenLengkap = false;
				}
		}
	}

	$scope.FormStatusGesekTickBox = function(lol){
		if(lol.FormStatusGesekReceiveDate != null) {
			lol.FormStatusGesekReceiveDateCekChanged = true;
			if(lol.BPKBReceiveDateCekChanged == true &&
				lol.FakturTAMReceiveCekChanged == true &&
				lol.CertificateNIKReceiveDateCekChanged == true &&
				lol.SuratUjiTipeReceiveDateCekChanged == true &&
				lol.FormAReceiveDateCekChanged == true &&
				lol.SuratRekomendasiReceiveDateCekChanged == true &&
				lol.PIBReceiveDateCekChanged == true){
					lol.DokumenLengkap = true;
				}
		} else if(lol.FormStatusGesekReceiveDate == null) {
			lol.FormStatusGesekReceiveDateCekChanged = false;
			if((lol.BPKBReceiveDateCekChanged == true &&
				lol.FakturTAMReceiveCekChanged == true &&
				lol.CertificateNIKReceiveDateCekChanged == true &&
				lol.SuratUjiTipeReceiveDateCekChanged == true &&
				lol.FormAReceiveDateCekChanged == true &&
				lol.SuratRekomendasiReceiveDateCekChanged == true &&
				lol.PIBReceiveDateCekChanged == true) ||
				lol.BPKBReceiveDateCekChanged == false ||
				lol.FakturTAMReceiveCekChanged == false ||
				lol.CertificateNIKReceiveDateCekChanged == false ||
				lol.SuratUjiTipeReceiveDateCekChanged == false ||
				lol.FormAReceiveDateCekChanged == false ||
				lol.SuratRekomendasiReceiveDateCekChanged == false ||
				lol.PIBReceiveDateCekChanged == false){
					lol.DokumenLengkap = false;
				}
		}
	}

	$scope.CertificateNIKTickBox = function(sid) {
		if(sid.CertificateNIKReceiveDate != null) {
			sid.CertificateNIKReceiveDateCekChanged = true;
			if(sid.BPKBReceiveDateCekChanged == true &&
				sid.FakturTAMReceiveCekChanged == true &&
				sid.FormStatusGesekReceiveDateCekChanged == true &&
				sid.SuratUjiTipeReceiveDateCekChanged == true &&
				sid.FormAReceiveDateCekChanged == true &&
				sid.SuratRekomendasiReceiveDateCekChanged == true &&
				sid.PIBReceiveDateCekChanged == true){
					sid.DokumenLengkap = true;
				}
		} else if(sid.CertificateNIKReceiveDate == null) {
			sid.CertificateNIKReceiveDateCekChanged = false;
			if((sid.BPKBReceiveDateCekChanged == true &&
				sid.FakturTAMReceiveCekChanged == true &&
				sid.FormStatusGesekReceiveDateCekChanged == true &&
				sid.SuratUjiTipeReceiveDateCekChanged == true &&
				sid.FormAReceiveDateCekChanged == true &&
				sid.SuratRekomendasiReceiveDateCekChanged == true &&
				sid.PIBReceiveDateCekChanged == true) ||
				sid.BPKBReceiveDateCekChanged == false ||
				sid.FakturTAMReceiveCekChanged == false ||
				sid.FormStatusGesekReceiveDateCekChanged == false ||
				sid.SuratUjiTipeReceiveDateCekChanged == false ||
				sid.FormAReceiveDateCekChanged == false ||
				sid.SuratRekomendasiReceiveDateCekChanged == false ||
				sid.PIBReceiveDateCekChanged == false){
					sid.DokumenLengkap = false;
				}
		}
	}

	$scope.SuratUjiTipeTickBox = function(cls) {
		if(cls.SuratUjiTipeReceiveDate != null) {
			cls.SuratUjiTipeReceiveDateCekChanged = true;
			if(cls.BPKBReceiveDateCekChanged == true &&
				cls.FakturTAMReceiveCekChanged == true &&
				cls.FormStatusGesekReceiveDateCekChanged == true &&
				cls.CertificateNIKReceiveDateCekChanged == true &&
				cls.FormAReceiveDateCekChanged == true &&
				cls.SuratRekomendasiReceiveDateCekChanged == true &&
				cls.PIBReceiveDateCekChanged == true){
					cls.DokumenLengkap = true;
				}
		} else if(cls.SuratUjiTipeReceiveDate == null) {
			cls.SuratUjiTipeReceiveDateCekChanged = false;
		}
	}

	$scope.FormATickBox = function(xnxx){
		if(xnxx.FormAReceiveDate != null) {
			xnxx.FormAReceiveDateCekChanged = true;
			if(xnxx.BPKBReceiveDateCekChanged == true &&
				xnxx.FakturTAMReceiveCekChanged == true &&
				xnxx.FormStatusGesekReceiveDateCekChanged == true &&
				xnxx.CertificateNIKReceiveDateCekChanged == true &&
				xnxx.SuratUjiTipeReceiveDateCekChanged == true &&
				xnxx.SuratRekomendasiReceiveDateCekChanged == true &&
				xnxx.PIBReceiveDateCekChanged == true){
					xnxx.DokumenLengkap = true;
				}
		} else if(xnxx.FormAReceiveDate == null) {
			xnxx.FormAReceiveDateCekChanged = false;
			if((xnxx.BPKBReceiveDateCekChanged == true &&
				xnxx.FakturTAMReceiveCekChanged == true &&
				xnxx.FormStatusGesekReceiveDateCekChanged == true &&
				xnxx.CertificateNIKReceiveDateCekChanged == true &&
				xnxx.SuratUjiTipeReceiveDateCekChanged == true &&
				xnxx.SuratRekomendasiReceiveDateCekChanged == true &&
				xnxx.PIBReceiveDateCekChanged == true) ||
				xnxx.BPKBReceiveDateCekChanged == false ||
				xnxx.FakturTAMReceiveCekChanged == false ||
				xnxx.FormStatusGesekReceiveDateCekChanged == false ||
				xnxx.CertificateNIKReceiveDateCekChanged == false ||
				xnxx.SuratUjiTipeReceiveDateCekChanged == false ||
				xnxx.SuratRekomendasiReceiveDateCekChanged == false ||
				xnxx.PIBReceiveDateCekChanged == false){
					xnxx.DokumenLengkap = false;
				}
		}
	}

	$scope.SuratRekomendasiTickBox = function(tik) {
		if(tik.SuratRekomendasiReceiveDate != null) {
			tik.SuratRekomendasiReceiveDateCekChanged = true;
			if(tik.BPKBReceiveDateCekChanged == true &&
				tik.FakturTAMReceiveCekChanged == true &&
				tik.FormStatusGesekReceiveDateCekChanged == true &&
				tik.CertificateNIKReceiveDateCekChanged == true &&
				tik.SuratUjiTipeReceiveDateCekChanged == true &&
				tik.FormAReceiveDateCekChanged == true &&
				tik.PIBReceiveDateCekChanged == true){
					tik.DokumenLengkap = true;
				}
		} else if(tik.SuratRekomendasiReceiveDate == null) {
			tik.SuratRekomendasiReceiveDateCekChanged = false;
			if((tik.BPKBReceiveDateCekChanged == true &&
				tik.FakturTAMReceiveCekChanged == true &&
				tik.FormStatusGesekReceiveDateCekChanged == true &&
				tik.CertificateNIKReceiveDateCekChanged == true &&
				tik.SuratUjiTipeReceiveDateCekChanged == true &&
				tik.FormAReceiveDateCekChanged == true &&
				tik.PIBReceiveDateCekChanged == true) ||
				tik.BPKBReceiveDateCekChanged == false ||
				tik.FakturTAMReceiveCekChanged == false ||
				tik.FormStatusGesekReceiveDateCekChanged == false ||
				tik.CertificateNIKReceiveDateCekChanged == false ||
				tik.SuratUjiTipeReceiveDateCekChanged == false ||
				tik.FormAReceiveDateCekChanged == false ||
				tik.PIBReceiveDateCekChanged == false){
					tik.DokumenLengkap = false;
				}
		}
	}

	$scope.PIBTickBox = function(last) {
		if(last.PIBReceiveDate != null) {
			last.PIBReceiveDateCekChanged = true;
			if(last.BPKBReceiveDateCekChanged == true &&
				last.FakturTAMReceiveCekChanged == true &&
				last.FormStatusGesekReceiveDateCekChanged == true &&
				last.CertificateNIKReceiveDateCekChanged == true &&
				last.SuratUjiTipeReceiveDateCekChanged == true &&
				last.FormAReceiveDateCekChanged == true &&
				last.SuratRekomendasiReceiveDateCekChanged == true){
					last.DokumenLengkap = true;
				}
		} else if(last.PIBReceiveDate == null) {
			last.PIBReceiveDateCekChanged = false;
			if((last.BPKBReceiveDateCekChanged == true &&
				last.FakturTAMReceiveCekChanged == true &&
				last.FormStatusGesekReceiveDateCekChanged == true &&
				last.CertificateNIKReceiveDateCekChanged == true &&
				last.SuratUjiTipeReceiveDateCekChanged == true &&
				last.FormAReceiveDateCekChanged == true &&
				last.SuratRekomendasiReceiveDateCekChanged == true) ||
				last.BPKBReceiveDateCekChanged == false ||
				last.FakturTAMReceiveCekChanged == false ||
				last.FormStatusGesekReceiveDateCekChanged == false ||
				last.CertificateNIKReceiveDateCekChanged == false ||
				last.SuratUjiTipeReceiveDateCekChanged == false ||
				last.FormAReceiveDateCekChanged == false ||
				last.SuratRekomendasiReceiveDateCekChanged == false){
					last.DokumenLengkap = false;
				}
		}
	}

	//------------------------------------
	//  End fungsi sinkronisasi tickbox
	//------------------------------------
		
	$scope.SimpanUpdatePenerimaanDokumen = function() {
			//angular.forEach($scope.SelectedDetilSuratDistribusi, function(value, key) {	
			//$scope.SelectedDetilSuratDistribusi[key] = $scope.EditDetilSuratDistribusi[key];			
			//});
			
			if($scope.EditDetilSuratDistribusiBPKBKelengkapan == true)
			{
				$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId']=1;
				$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName']="Lengkap";					
			}
			if($scope.EditDetilSuratDistribusiBPKBKelengkapan == false)
			{
				$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId']=2;
				$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName']="Belum Lengkap";
			}
			
			$scope.The_listFrameNo['listDetail'][0]=angular.copy($scope.EditDetilSuratDistribusi);
			// $scope.The_Result = angular.copy($scope.EditDetilSuratDistribusi);
			
			$scope.The_listFrameNo['listDetail'][0].FakturTAMSendDate = DateFix($scope.EditDetilSuratDistribusi.FakturTAMSendDate);			
			$scope.The_listFrameNo['listDetail'][0].FakturTAMReceiveDate = DateFix($scope.EditDetilSuratDistribusi.FakturTAMReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].FormStatusGesekSendDate = DateFix($scope.EditDetilSuratDistribusi.FormStatusGesekSendDate);			
			$scope.The_listFrameNo['listDetail'][0].FormStatusGesekReceiveDate = DateFix($scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].CertificateNIKSendDate = DateFix($scope.EditDetilSuratDistribusi.CertificateNIKSendDate);
			$scope.The_listFrameNo['listDetail'][0].CertificateNIKReceiveDate = DateFix($scope.EditDetilSuratDistribusi.CertificateNIKReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].SuratUjiTipeSendDate = DateFix($scope.EditDetilSuratDistribusi.SuratUjiTipeSendDate);
			$scope.The_listFrameNo['listDetail'][0].SuratUjiTipeReceiveDate = DateFix($scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].FormASendDate= DateFix($scope.EditDetilSuratDistribusi.FormASendDate);
			$scope.The_listFrameNo['listDetail'][0].FormAReceiveDate= DateFix($scope.EditDetilSuratDistribusi.FormAReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].SuratRekomendasiSendDate= DateFix($scope.EditDetilSuratDistribusi.SuratRekomendasiSendDate);
			$scope.The_listFrameNo['listDetail'][0].SuratRekomendasiReceiveDate= DateFix($scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].PIBSendDate= DateFix($scope.EditDetilSuratDistribusi.PIBSendDate);
			$scope.The_listFrameNo['listDetail'][0].PIBReceiveDate= DateFix($scope.EditDetilSuratDistribusi.PIBReceiveDate);
			$scope.The_listFrameNo['listDetail'][0].BPKBSendDate= DateFix($scope.EditDetilSuratDistribusi.BPKBSendDate);
			$scope.The_listFrameNo['listDetail'][0].BPKBReceiveDate= DateFix($scope.EditDetilSuratDistribusi.BPKBReceiveDate);
			
			$scope.The_Result['listFrameNo'][0]=[angular.copy($scope.The_listFrameNo)];
			// $scope.The_Result.listFrameNo = [$scope.The_listFrameNo];
			
			$scope.TerimaDokumenBPKBToUpdate={ 'OutletId':$scope.The_Result.OutletId,
												'OutletName':$scope.The_Result.OutletName,
												'SuratDistribusiBPKBHeaderId':$scope.The_Result.SuratDistribusiBPKBHeaderId,
												'SuratDistribusiBPKBNo':$scope.The_Result.SuratDistribusiBPKBNo,
												'QtyDokumenLengkap':$scope.The_Result.QtyDokumenLengkap,
												'QtyFrameNo':$scope.The_Result.QtyFrameNo,
												'QtyDokumenLengkapPerQtyFrameNo':$scope.The_Result.QtyDokumenLengkapPerQtyFrameNo,
												'TerimaDokumenSTNKBPKBStatusId':$scope.The_Result.TerimaDokumenSTNKBPKBStatusId,
												'TerimaDokumenSTNKBPKBStatusName':$scope.The_Result.TerimaDokumenSTNKBPKBStatusName,
												'StatusCode':$scope.The_Result.StatusCode,
												'TotalData':$scope.The_Result.TotalData,
												'listFrameNo':$scope.The_Result.listFrameNo[0] };
			// console.log('Isi Result --> ', $scope.TerimaDokumenBPKBToUpdate);
			TerimaDokumenBpkbFactory.update($scope.TerimaDokumenBPKBToUpdate).then(function (res) {
					//nanti di get
					angular.element('.ui.modal.UpdatePenerimaanDokumen').modal('hide');
					var tempResult = res.data.Result;
					console.log('Temp Result = ', res.data.Result);
					$scope.CekCeklist(tempResult);
					console.log('Isi Grid = ', tempResult);
					$scope.grid2.data = tempResult[0].listFrameNo;
					//$scope.ShowDetilSuratDistribusiBpkb=false;
					//$scope.ShowTerimaDokumenBPKBMain=true;
					$scope.getData();
					bsNotify.show({
						title: "Success",
						content: "Berhasil simpan dokumen",
						type: 'success'
					});
					// for(var i = 0; i < $scope.grid2.data.length; ++i)
					// {
					// 	if($scope.grid2.data[i]['SuratDistribusiBPKBDetailId'] == $scope.The_listFrameNo['SuratDistribusiBPKBDetailId'])
					// 	{	
					// 		$scope.grid2.data[i]['SuratDistribusiCabangTerimaDariCAOStatusId']=$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId'];
					// 		$scope.grid2.data[i]['SuratDistribusiStatusName']=$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName'];
					// 	}
					// 	if($scope.grid2.data[i]['SuratDistribusiStatusName'] == "Lengkap")
					// 	{	
					// 		$scope.grid2.data[i]['SuratDistribusiStatusNameDiTable']=true;
					// 	}
					// 	if($scope.grid2.data[i]['SuratDistribusiStatusName'] == "Belum Lengkap")
					// 	{
					// 		$scope.grid2.data[i]['SuratDistribusiStatusNameDiTable']=false;
					// 	}
					// }
					
				});
			

        }

	$scope.CentangAllDokumenChanged = function(selected_data) {
			
			if(selected_data == true)
			{
				$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged= true;
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged= true;
				$scope.EditDetilSuratDistribusi.DisablePasSemuaDicentang= true;
				$scope.FakturTAMReceiveDateChanged(true);
				$scope.FormStatusGesekReceiveDateChanged(true);
				$scope.CertificateNIKReceiveDateChanged(true);
				$scope.SuratUjiTipeReceiveDateChanged(true);
				$scope.FormAReceiveDateChanged(true);
				$scope.SuratRekomendasiReceiveDateChanged(true);
				$scope.PIBReceiveDateChanged(true);
				$scope.BPKBReceiveDateChanged(true);
				$scope.DisablePasSemuaDicentang = true;
			}
			else if(selected_data == false)
			{
				$scope.EditDetilSuratDistribusi.FakturTAMReceiveCekChanged= false;
				$scope.EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.FormAReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.PIBReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.BPKBReceiveDateCekChanged= false;
				$scope.EditDetilSuratDistribusi.DisablePasSemuaDicentang= false;
				$scope.FakturTAMReceiveDateChanged(false);
				$scope.FormStatusGesekReceiveDateChanged(false);
				$scope.CertificateNIKReceiveDateChanged(false);
				$scope.SuratUjiTipeReceiveDateChanged(false);
				$scope.FormAReceiveDateChanged(false);
				$scope.SuratRekomendasiReceiveDateChanged(false);
				$scope.PIBReceiveDateChanged(false);
				$scope.BPKBReceiveDateChanged(false);
				$scope.DisablePasSemuaDicentang = false;
				
			}
			
		}
		

		$scope.CekCeklist = function(ArrayData) {
            angular.forEach(ArrayData, function(data, dataIndx) {
                angular.forEach(data.listFrameNo, function(frame, frameIndx) {
                    angular.forEach(frame.listDetail, function(detail, detailIndx) {
                        if ((detail.BPKBReceiveDate != null || detail.BPKBReceiveDate != undefined)) {
                            detail.BPKBReceiveDateCekChanged = true;
                        } else {
                            detail.BPKBReceiveDateCekChanged = false;
                        };

                        if ((detail.CertificateNIKReceiveDate != null || detail.CertificateNIKReceiveDate != undefined)) {
                            detail.CertificateNIKReceiveDateCekChanged = true;
                        } else {
                            detail.CertificateNIKReceiveDateCekChanged = false;
                        };

                        if ((detail.FakturTAMReceiveDate != null || detail.FakturTAMReceiveDate != undefined)) {
                            detail.FakturTAMReceiveCekChanged = true;
                        } else {
                            detail.FakturTAMReceiveCekChanged = false;
						};
						
						if ((detail.FormAReceiveDate != null || detail.FormAReceiveDate != undefined)) {
                            detail.FormAReceiveDateCekChanged = true;
                        } else {
                            detail.FormAReceiveDateCekChanged = false;
                        };

                        if ((detail.FormStatusGesekReceiveDate != null || detail.FormStatusGesekReceiveDate != undefined)) {
                            detail.FormStatusGesekReceiveDateCekChanged = true;
                        } else {
                            detail.FormStatusGesekReceiveDateCekChanged = false;
                        };

                        if ((detail.PIBReceiveDate != null || detail.PIBReceiveDate != undefined)) {
                            detail.PIBReceiveDateCekChanged = true;
                        } else {
                            detail.PIBReceiveDateCekChanged = false;
						};
						
						if ((detail.SuratRekomendasiReceiveDate != null || detail.SuratRekomendasiReceiveDate != undefined)) {
                            detail.SuratRekomendasiReceiveDateCekChanged = true;
                        } else {
                            detail.SuratRekomendasiReceiveDateCekChanged = false;
                        };

                        if ((detail.SuratUjiTipeReceiveDate != null || detail.SuratUjiTipeReceiveDate != undefined)) {
                            detail.SuratUjiTipeReceiveDateCekChanged = true;
                        } else {
                            detail.SuratUjiTipeReceiveDateCekChanged = false;
                        };
                    })
                })
            });
    	}
		
		
	
	$scope.KelengkapanDokumenChanged = function(selected_data) {
			for(var i = 0; i < $scope.EditDetilSuratDistribusi.ListDokumen.length; ++i)
			{
				if($scope.EditDetilSuratDistribusi.ListDokumen[i].Uploaded == true)
				{
					$scope.EditDetilSuratDistribusi.Lengkap=true;
				}
				else
				{
					$scope.EditDetilSuratDistribusi.Lengkap=false;
					break;
				}
			}	
		}
		

    //----------------------------------
    // Grid Setup
	//----------------------------------
	var actionbutton = '<div><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.ShowDetil(row.entity)" tabindex="0"> ' +
	'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
	'</a></div>';

	var actionbuttonedit = '<div><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.UpdateDetilSuratDistribusiBpkb(row.entity)" tabindex="0"> ' +
	'<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
	'</a></div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'SuratDistribusiBPKBHeaderId', width:'7%', visible:false },
   			{ name:'No Surat Distribusi',  field: 'SuratDistribusiBPKBNo' },
            { name:'Qty Diterima/NomorRangka',  field: 'QtyDokumenLengkapPerQtyFrameNo' },
			{ name:'Status',  field: 'TerimaDokumenSTNKBPKBStatusName' },
			{ name:'Action', width: '10%' ,  cellTemplate:actionbutton }
        ]
    };
	
	$scope.grid2 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            // paginationPageSize: 15,
			data:$scope.DetilSuratDistribusiBpkb,
            columnDefs: [
				{ name: 'NoRangka', field: 'FrameNo' },
				{ name: 'NamaPelanggan', field: 'CustomerName' },
				{ name: 'Model', field: 'VehicleModelName' },
				{ name: 'Diterima', field: 'Diterima', width: '10%', cellTemplate:'<input disabled ng-model="row.entity.TerimaBit" type="checkbox"   />'},
                { name: 'Lengkap', field: 'Lengkap', width: '10%', cellTemplate:'<input disabled ng-model="row.entity.LengkapBit" type="checkbox"   />'},
                { name: 'Action', field: '', width: '10%' , cellTemplate:actionbuttonedit},

            ]
        };
});
