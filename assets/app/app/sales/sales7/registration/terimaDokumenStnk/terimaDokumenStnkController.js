angular.module('app')
    .controller('TerimaDokumenStnkController', function($httpParamSerializer, $stateParams ,$scope, $http, CurrentUser, TerimaDokumenStnkFactory,$timeout,bsNotify) {
	// IfGotProblemWithModal();

	$scope.ShowTerimaDokumenSTNKMain=true;
	$scope.DetilSuratDistribusi="";
	
	
	
    //----------------------------------
    // Start-Up
	//----------------------------------
	
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
	});
	
	$scope.$on('$destroy', function(){
		angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').remove();
	});
	
    //----------------------------------
    // Initialization
    //----------------------------------
	$scope.user = CurrentUser.user();
	$scope.breadcrums={};
	$scope.tab = $stateParams.tab;
	console.log("menu",$scope.tab);
	$scope.indexgrid = 0;
    $scope.mTerimaDokumenStnk = null; //Model
	$scope.xRole={selected:[]};
	$scope.filter = {Id:0, TerimaDokumenSTNKBPKBStatusId : null
		            , FrameNo : null, SuratDistribusiSTNKNo : null, CustomerName : null};
	
	
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];	
	$scope.getData = function() {
		var param = $httpParamSerializer($scope.filter);
        TerimaDokumenStnkFactory.getData(param).then(
            function(res){
                $scope.grid.data = res.data.Result;
				$scope.loading=false;
				$scope.CekCeklist($scope.grid.data);
                //return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }			
	
	$scope.getStatusDokumen = [];
	TerimaDokumenStnkFactory.getStatus().then(function(res) {
		$scope.getStatusDokumen = res.data.Result;
	});
	
	$scope.KembaliKeTerimaDokumenSTNKMain = function() {
			$scope.ShowDetilSuratDistribusiStnk=false;
			$scope.DetilSuratDistribusi="";
			$scope.ShowTerimaDokumenSTNKMain=true;
			$scope.getData();
        }
	
	$scope.ShowDetil = function(selected_data) {
		$scope.breadcrums.title="Lihat";
			$scope.The_Result=angular.copy(selected_data);
			$scope.ShowTerimaDokumenSTNKMain=false;
			console.log("testasdasd", $scope.The_Result);
			//alert(selected_data.listDetail);
			$scope.grid2.data=selected_data.listFrameNo;

			// for(var i in $scope.The_Result.listFrameNo){
			// 	for (var j in $scope.The_Result.listFrameNo[i].listDetail){
			// 		if($scope.The_Result.listFrameNo[i].TerimaDokumenSTNKBPKBStatusName == "Dokumen Diterima Di Cabang"){
			// 			$scope.EditDetilSuratDistribusi.StnkTerimaCekChanged = true; 
			// 			$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged = true; 
			// 			$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged = true;
			// 			$scope.EditDetilSuratDistribusi.CentangAllDokumen = true;
			// 		}
			// 	}
			// }
			
			// for(var i = 0; i < $scope.grid2.data.length; ++i)
			// {
			// 	if($scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusName == 'Dokumen Diterima Di Cabang')
			// 	{
			// 		$scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusNameDiTable=true;
			// 	}
			// 	else if($scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusName == 'Dokumen Dikirim Ke Cabang')
			// 	{
			// 		$scope.grid2.data[i].TerimaDokumenSTNKBPKBStatusNameDiTable=false;
			// 	}
			// }
			
			// for(var i = 0; i < $scope.grid2.data.length; ++i)
			// {
			// 	if($scope.grid2.data[i].SuratDistribusiStatusName == 'Lengkap')
			// 	{
			// 		$scope.grid2.data[i].SuratDistribusiStatusNameDiTable=true;
			// 	}
			// 	else
			// 	{
			// 		$scope.grid2.data[i].SuratDistribusiStatusNameDiTable=false;
			// 	}
			// }
			
			
			$scope.ShowDetilSuratDistribusiStnk=true;

        }
		
	$scope.isLengkapTable = function(){
    return true;
	};	
	
	$scope.UpdateDetilSuratDistribusiStnk = function(selected_data) {
			$scope.The_listFrameNo=angular.copy(selected_data);
			$scope.SelectedDetilSuratDistribusi=selected_data.listDetail[0];
			
			// if($scope.SelectedDetilSuratDistribusi.SuratDistribusiStatusName == "Lengkap")
			// {
			// 	$scope.EditDetilSuratDistribusiSTNKKelengkapan=true;
			// }
			// else if($scope.SelectedDetilSuratDistribusi.SuratDistribusiStatusName == "Belum Lengkap")
			// {
			// 	$scope.EditDetilSuratDistribusiSTNKKelengkapan=false;
			// }
			
			$scope.OldTanggalTerimaSTNK=angular.copy($scope.SelectedDetilSuratDistribusi.STNKReceiveDate);
			$scope.OldTanggalTerimaPoliceNumber=angular.copy($scope.SelectedDetilSuratDistribusi.PoliceNumberReceiveDate);
			$scope.OldTanggalTerimaNoticePajak=angular.copy($scope.SelectedDetilSuratDistribusi.NoticePajakReceiveDate);
			
			if($scope.OldTanggalTerimaSTNK!=null)
			{
				$scope.ProcessedOldTanggalTerimaSTNK=$scope.OldTanggalTerimaSTNK.getFullYear()+'-'+('0' + ($scope.OldTanggalTerimaSTNK.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldTanggalTerimaSTNK.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldTanggalTerimaSTNK=null;
			}
			
			if($scope.OldTanggalTerimaPoliceNumber!=null)
			{
				$scope.ProcessedOldTanggalTerimaPoliceNumber=$scope.OldTanggalTerimaPoliceNumber.getFullYear()+'-'+('0' + ($scope.OldTanggalTerimaPoliceNumber.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldTanggalTerimaPoliceNumber.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldTanggalTerimaPoliceNumber=null;
			}
			
			if($scope.OldTanggalTerimaNoticePajak!=null)
			{
				$scope.ProcessedOldTanggalTerimaNoticePajak=$scope.OldTanggalTerimaNoticePajak.getFullYear()+'-'+('0' + ($scope.OldTanggalTerimaNoticePajak.getMonth()+1)).slice(-2)+'-'+('0' + $scope.OldTanggalTerimaNoticePajak.getDate()).slice(-2);
			}
			else
			{
				$scope.ProcessedOldTanggalTerimaNoticePajak=null;
			}
			//console.log('$scope.SelectedDetilSuratDistribusi',$scope.SelectedDetilSuratDistribusi);
			$scope.EditDetilSuratDistribusi=angular.copy($scope.SelectedDetilSuratDistribusi);
			//console.log('$scope.EditDetilSuratDistribusi',$scope.EditDetilSuratDistribusi);
			angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('show');
		}
		
		function DateFix(date) {
			if (date != null || date != undefined) {
				var fix = date.getFullYear() + '-' +
				('0' + (date.getMonth() + 1)).slice(-2) + '-' +
				('0' + date.getDate()).slice(-2) 
				// + 'T' +
				// ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
				// ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
				// ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
				return fix;
			} else { return null };
		}
		
	$scope.SimpanUpdatePenerimaanDokumen = function() {
			//angular.forEach($scope.SelectedDetilSuratDistribusi, function(value, key) {	
			//$scope.SelectedDetilSuratDistribusi[key] = $scope.EditDetilSuratDistribusi[key];			
			//});
			
			if($scope.EditDetilSuratDistribusi.DokumenLengkap == true)
			{
				$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId']=1;
				$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName']="Lengkap";				
			}
			if($scope.EditDetilSuratDistribusi.DokumenLengkap == false)
			{
				$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId']=2;
				$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName']="Belum Lengkap";
			}
			
			
			$scope.The_listFrameNo['listDetail'][0]=angular.copy($scope.EditDetilSuratDistribusi);
			
			// $scope.The_listFrameNo['listDetail'][0].STNKSendDate= DateFix($scope.EditDetilSuratDistribusi.STNKSendDate);
			// $scope.The_listFrameNo['listDetail'][0].STNKReceiveDate= DateFix($scope.EditDetilSuratDistribusi.STNKReceiveDate);
			// $scope.The_listFrameNo['listDetail'][0].PoliceNumberSendDate= DateFix($scope.EditDetilSuratDistribusi.PoliceNumberSendDate);
			// $scope.The_listFrameNo['listDetail'][0].PoliceNumberReceiveDate= DateFix($scope.EditDetilSuratDistribusi.PoliceNumberReceiveDate);
			// $scope.The_listFrameNo['listDetail'][0].NoticePajakSendDate= DateFix($scope.EditDetilSuratDistribusi.NoticePajakSendDate);
			// $scope.The_listFrameNo['listDetail'][0].NoticePajakReceiveDate= DateFix($scope.EditDetilSuratDistribusi.NoticePajakReceiveDate);

			$scope.The_Result.listFrameNo = [$scope.The_listFrameNo];

			console.log("asdasd", $scope.The_Result);
			//$scope.The_Result['listFrameNo'][0]=[angular.copy($scope.The_listFrameNo)];
			
			//$scope.TerimaDokumenSTNKToUpdate={ 'OutletId':$scope.The_Result.OutletId,'OutletName':$scope.The_Result.OutletName,'SuratDistribusiSTNKHeaderId':$scope.The_Result.SuratDistribusiSTNKHeaderId,'SuratDistribusiSTNKNo':$scope.The_Result.SuratDistribusiSTNKNo,'QtyDokumenLengkap':$scope.The_Result.QtyDokumenLengkap,'QtyFrameNo':$scope.The_Result.QtyFrameNo,'QtyDokumenLengkapPerQtyFrameNo':$scope.The_Result.QtyDokumenLengkapPerQtyFrameNo,'TerimaDokumenSTNKBPKBStatusId':$scope.The_Result.TerimaDokumenSTNKBPKBStatusId,'TerimaDokumenSTNKBPKBStatusName':$scope.The_Result.TerimaDokumenSTNKBPKBStatusName,'StatusCode':$scope.The_Result.StatusCode,'TotalData':$scope.The_Result.TotalData,'listFrameNo':$scope.The_Result.listFrameNo[0] };
			
			TerimaDokumenStnkFactory.update($scope.The_Result).then(function (res) {
					//nanti di get
					angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
					var tempResult = res.data.Result;
					//console.log("wrweewt",res.data.Result);
					$scope.CekCeklist(tempResult);
					//console.log("fjhgjghjgh",tempResult);
					$scope.grid2.data = tempResult[0].listFrameNo;
					$scope.ShowDetilSuratDistribusiStnk = true;
					// $scope.ShowTerimaDokumenSTNKMain = true;
					$scope.getData();
					bsNotify.show({
						title: "Success",
						content: "Berhasil simpan dokumen",
						type: 'success'
					});
					//console.log("adasd",$scope.grid2.data, tempResult.listFrameNo);

					//TerimaDokumenStnkFactory.getData().then(
					//	function(res){
					//		$scope.grid.data = res.data.Result;
					//		$scope.loading=false;
					//		console.log("data dari Factory",res.data.Result);
					//		return res.data;
					//	},
					//	function(err){
					//		console.log("err=>",err);
					//	}
					//);

					// for(var i = 0; i < $scope.grid2.data.length; ++i)
					// {
					// 	if($scope.grid2.data[i]['SuratDistribusiSTNKDetailId'] == $scope.The_listFrameNo['SuratDistribusiSTNKDetailId'])
					// 	{	
					// 		$scope.grid2.data[i]['SuratDistribusiCabangTerimaDariCAOStatusId']=$scope.EditDetilSuratDistribusi['SuratDistribusiCabangTerimaDariCAOStatusId'];
					// 		$scope.grid2.data[i]['SuratDistribusiStatusName']=$scope.EditDetilSuratDistribusi['SuratDistribusiStatusName'];
					// 	}
					// 	if($scope.grid2.data[i]['SuratDistribusiStatusName'] == "Lengkap")
					// 	{	
					// 		$scope.grid2.data[i]['SuratDistribusiStatusNameDiTable']=true;
					// 	}
					// 	if($scope.grid2.data[i]['SuratDistribusiStatusName'] == "Belum Lengkap")
					// 	{
					// 		$scope.grid2.data[i]['SuratDistribusiStatusNameDiTable']=false;
					// 	}
					// }

					// $scope.grid2.data[$scope.indexgrid] = $scope.TerimaDokumenSTNKToUpdate;
				});
				
		}	

	$scope.backFromterimaSTNK = function (){
		angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
	}
	
	$scope.CentangAllDokumenChanged = function(selected_data) {
			
			if(selected_data == true)
			{
				$scope.EditDetilSuratDistribusi.StnkTerimaCekChanged= true;
				$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged= true;
				$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged= true;
				$scope.DisablePasSemuaDicentang= true;
				$scope.StnkTerimaChanged(true);
				$scope.PoliceNumberTerimaChanged(true);
				$scope.NoticePajakTerimaChanged(true);
			
				
			}
			else if(selected_data == false)
			{
				$scope.EditDetilSuratDistribusi.StnkTerimaCekChanged= false;
				$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged= false;
				$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged= false;
				$scope.DisablePasSemuaDicentang= false;
				$scope.StnkTerimaChanged(false);
				$scope.PoliceNumberTerimaChanged(false);
				$scope.NoticePajakTerimaChanged(false);
			}
	}

	//-----------------------------------------
	// Fungsi sinkronisasi TickBox
	//-----------------------------------------

	$scope.STNKTicBox = function(tick){
		if(tick.STNKReceiveDate != null) {
			tick.StnkTerimaCekChanged = true;
			if(tick.PoliceNumberTerimaCekChanged == true &&
				tick.NoticePajakTerimaCekChanged == true){
					tick.DokumenLengkap = true;
			}

		} else if(tick.STNKReceiveDate == null) {
			tick.StnkTerimaCekChanged = false;
			if((tick.PoliceNumberTerimaCekChanged == true &&
				tick.NoticePajakTerimaCekChanged == true) ||
				tick.PoliceNumberTerimaCekChanged == false ||
				tick.NoticePajakTerimaCekChanged == false){
					tick.DokumenLengkap = false;
			}
		}
	}

	$scope.PoliceNumberTicBox = function(tick) {
		if(tick.PoliceNumberReceiveDate != null) {
			tick.PoliceNumberTerimaCekChanged = true;
			if(tick.StnkTerimaCekChanged == true &&
				tick.NoticePajakTerimaCekChanged == true){
					tick.DokumenLengkap = true;
				}
		} else if(tick.PoliceNumberReceiveDate == null) {
			tick.PoliceNumberTerimaCekChanged = false;
			if((tick.StnkTerimaCekChanged == true &&
				tick.NoticePajakTerimaCekChanged == true) ||
				tick.StnkTerimaCekChanged == false ||
				tick.NoticePajakTerimaCekChanged == false){
					tick.DokumenLengkap = false;
				}
		}
	}

	$scope.NoticePajakTicBox = function(tick) {
		if(tick.NoticePajakReceiveDate != null) {
			tick.NoticePajakTerimaCekChanged = true;
			if(tick.StnkTerimaCekChanged == true &&
				tick.PoliceNumberTerimaCekChanged == true){
					tick.DokumenLengkap = true;
				}
		} else if(tick.NoticePajakReceiveDate == null) {
			tick.NoticePajakTerimaCekChanged = false;
			if((tick.StnkTerimaCekChanged == true &&
				tick.PoliceNumberTerimaCekChanged == true) ||
				tick.StnkTerimaCekChanged == false ||
				tick.PoliceNumberTerimaCekChanged == false){
					tick.DokumenLengkap = false;
				}
		}
	}

	//----------------------------------------
	// End function sinkronisasi TickBox
	//----------------------------------------

	$scope.StnkTerimaChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.STNKReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == true){
					$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
				}
			}else{
				$scope.EditDetilSuratDistribusi.STNKReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == false ||
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == false){
					$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
				}
			}
			// if($scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.STNKReceiveDate=$scope.OldTanggalTerimaSTNK;	
			// }
			
        }

	$scope.PoliceNumberTerimaChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.PoliceNumberReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.PoliceNumberReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == false ||
					$scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}
			}
			// if($scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.PoliceNumberReceiveDate=$scope.OldTanggalTerimaPoliceNumber;	
			// }
			
        }
		
	$scope.NoticePajakTerimaChanged = function(status) {
			
			if(status == true)
			{
				$scope.EditDetilSuratDistribusi.NoticePajakReceiveDate=new Date();
				if($scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == true){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = true;
					}
			}else{
				$scope.EditDetilSuratDistribusi.NoticePajakReceiveDate=null;
				if(($scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == true &&
					$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == true) ||
					$scope.EditDetilSuratDistribusi.StnkTerimaCekChanged == false ||
					$scope.EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged == false){
						$scope.EditDetilSuratDistribusi.DokumenLengkap = false;
					}	
			}
			// if($scope.EditDetilSuratDistribusi.NoticePajakTerimaCekChanged == false)
			// {
			// 	$scope.EditDetilSuratDistribusi.NoticePajakReceiveDate=$scope.OldTanggalTerimaNoticePajak;	
			// }
			
		}	
		
	 $scope.CekCeklist = function(ArrayData) {
            angular.forEach(ArrayData, function(data, dataIndx) {
                angular.forEach(data.listFrameNo, function(frame, frameIndx) {
                    angular.forEach(frame.listDetail, function(detail, detailIndx) {
                        if ((detail.STNKReceiveDate != null || detail.STNKReceiveDate != undefined)) {
                            detail.StnkTerimaCekChanged = true;
                        } else {
                            detail.StnkTerimaCekChanged = false;
                        };

                        if ((detail.PoliceNumberReceiveDate != null || detail.PoliceNumberReceiveDate != undefined)) {
                            detail.PoliceNumberTerimaCekChanged = true;
                        } else {
                            detail.PoliceNumberTerimaCekChanged = false;
                        };

                        if ((detail.NoticePajakReceiveDate != null || detail.NoticePajakReceiveDate != undefined)) {
                            detail.NoticePajakTerimaCekChanged = true;
                        } else {
                            detail.NoticePajakTerimaCekChanged = false;
                        };
                    })
                })
            });
	}
	
	var actionbutton = '<div><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.ShowDetil(row.entity)" tabindex="0"> ' +
	'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
	'</a></div>';

	var actionbuttonedit = '<div><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.UpdateDetilSuratDistribusiStnk(row.entity)" tabindex="0"> ' +
	'<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
	'</a></div>';
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
   			{ name:'No Surat Distribusi',  field: 'SuratDistribusiSTNKNo' },
            { name:'Qty Diterima/NomorRangka',  field: 'QtyDokumenLengkapPerQtyFrameNo' },
			{ name:'Status',  field: 'TerimaDokumenSTNKBPKBStatusName' },
			{ name:'Action', width: '10%', cellTemplate: actionbutton }
        ]
    };
	
	$scope.grid2 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
             paginationPageSizes: [10, 25, 50],
            // paginationPageSize: 15,
			data:$scope.DetilSuratDistribusi,
            columnDefs: [
				{ name: 'NoRangka', field: 'FrameNo' },
				{ name: 'NamaPelanggan', field: 'CustomerName' },
				{ name: 'Model', field: 'VehicleModelName' },
				{ name: 'Diterima', field: 'TerimaDokumenSTNKBPKBStatusNameDiTable', width: '7%' , cellTemplate:'<input disabled ng-model="row.entity.TerimaBit" type="checkbox"   />'},
                { name: 'Lengkap', field: 'SuratDistribusiStatusNameDiTable', width: '7%' , cellTemplate:'<input disabled ng-model="row.entity.LengkapBit" type="checkbox"   />'},
                { name: 'Action', width: '10%', cellTemplate:actionbuttonedit},
            ]
        };
});
