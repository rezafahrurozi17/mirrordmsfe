angular.module('app')
  .factory('TerimaDokumenStnkFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    // function fixDate(date) {
    //   if (date != null || date != undefined) {
    //       var fix = date.getFullYear() + '-' +
    //           ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
    //           ('0' + date.getDate()).slice(-2) + 'T' +
    //           ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
    //           ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
    //           ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
    //       return fix;
    //   } else {
    //       return null;
    //   }
    // };
    return {
      getData: function(param) {
        if (param == undefined || param == null || param == "") {
            param = "";
        } else {
            param = "&" + param;
        }
        var res=$http.get('/api/sales/TerimaDokumenSTNK/?start=1&limit=1000'+param);
        return res;
      },
      getStatus: function() {
        var res=$http.get('/api/sales/PStatusTerimaDokumenSTNKBPKB');
        return res;
      },
      create: function(TerimaDokumenStnk) {
        return $http.post('/api/sales/TerimaDokumenSTNK', [{
                                            SuratDistribusiSTNKHeaderId:TerimaDokumenStnk.SuratDistribusiSTNKHeaderId,
											SuratDistribusiCabangTerimaDariCAOStatusId:1,
											STNKReceiveDate:TerimaDokumenStnk.STNKReceiveDate,
											PoliceNumberReceiveDate:TerimaDokumenStnk.PoliceNumberReceiveDate,
											NoticePajakReceiveDate:TerimaDokumenStnk.NoticePajakReceiveDate,
                                            }]);
      },
      update: function(TerimaDokumenStnk){
        // for(var i in TerimaDokumenStnk.listFrameNo){
        //   for(var j in TerimaDokumenStnk.listFrameNo[i].listDetail){
        //     TerimaDokumenStnk.listFrameNo[i].listDetail[j].STNKSendDate = fixDate(TerimaDokumenStnk.listFrameNo[i].listDetail[j].STNKSendDate);
        //     TerimaDokumenStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate = fixDate(TerimaDokumenStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate);
        //     TerimaDokumenStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate = fixDate(TerimaDokumenStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate);
        //   }
        // }
        var res = $http.put('/api/sales/TerimaDokumenSTNK/Update', [{
											OutletId:TerimaDokumenStnk.OutletId,
											OutletName:TerimaDokumenStnk.OutletName,
											SuratDistribusiSTNKHeaderId:TerimaDokumenStnk.SuratDistribusiSTNKHeaderId,
											SuratDistribusiSTNKNo:TerimaDokumenStnk.SuratDistribusiSTNKNo,
											QtyDokumenLengkap:TerimaDokumenStnk.QtyDokumenLengkap,
											QtyFrameNo:TerimaDokumenStnk.QtyFrameNo,
											QtyDokumenLengkapPerQtyFrameNo:TerimaDokumenStnk.QtyDokumenLengkapPerQtyFrameNo,
											TerimaDokumenSTNKBPKBStatusId:TerimaDokumenStnk.TerimaDokumenSTNKBPKBStatusId,
											TerimaDokumenSTNKBPKBStatusName:TerimaDokumenStnk.TerimaDokumenSTNKBPKBStatusName,
											StatusCode:TerimaDokumenStnk.StatusCode,
											TotalData:TerimaDokumenStnk.TotalData,
											listFrameNo:TerimaDokumenStnk.listFrameNo,
											LastModifiedDate:TerimaDokumenStnk.LastModifiedDate,
											LastModifiedUserId:TerimaDokumenStnk.LastModifiedUserId,
                                            }]);
        return res;
      },
      delete: function(id) {
        return $http.delete('/api/sales/TerimaDokumenSTNK',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd