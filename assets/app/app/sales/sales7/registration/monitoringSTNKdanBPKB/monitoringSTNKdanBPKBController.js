angular.module('app')
    .controller('MonitoringSTNKBPKBController', function(bsNotify, $scope, $http, CurrentUser, TerimaSTNKBirojasaFactory, TerimaBpkbFactory, MonitoringStnkBpkbFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            // $scope.gridStnk = [];
            // $scope.gridBpkb = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mTerimaBpkb = null; //Model
        $scope.daftarPO = true;
        $scope.filter = { DateTimeJadwalPenerimaan: new Date };
        $scope.dateOption = {
            format: 'dd/MM/yyyy',
            newDate: new Date()
        };

        $scope.dateOptionMax = {
            format: 'dd/MM/yyyy',
            newDate: new Date()
        }

        $scope.$watch('rowsDocument.POBiroJasaUnitDate', function(newValue, oldValue) {
            console.log(angular.isDate(newValue), newValue, angular.isDefined($scope.rowsDocument.POBiroJasaUnitDate),oldValue);
            if(angular.isDate(newValue) == true && angular.isDefined($scope.rowsDocument.POBiroJasaUnitDate) == true){
                $scope.dateOptionMax.minDate = $scope.rowsDocument.POBiroJasaUnitDate;
            }
        });

        $scope.changeDate = function(date){
            $scope.dateOptionMax.minDate = date;
        }
        //----------------------------------
        // Get Data
        //----------------------------------

        $scope.tanggal =function (param){
            var date = angular.copy($scope.filter.DateTimeJadwalPenerimaan);
            if(param == 'plus'){
                var plus = new Date($scope.filter.DateTimeJadwalPenerimaan.getDate() + 1);
                $scope.filter.DateTimeJadwalPenerimaan = new Date(date.setDate(plus));
                $scope.getDataStnk(); $scope.getDataBpkb();
            }else if (param == 'min'){
                var min = new Date($scope.filter.DateTimeJadwalPenerimaan.getDate() - 1);
                $scope.filter.DateTimeJadwalPenerimaan = new Date(date.setDate(min));
                $scope.getDataStnk(); $scope.getDataBpkb();
            }
        };

        var gridStnk = [];
        $scope.getDataStnk = function() {
            MonitoringStnkBpkbFactory.getDataStnk($scope.filter.DateTimeJadwalPenerimaan).then(
                function(res) {
                    $scope.gridStnk.data = res.data.Result;
                    $scope.loading = false;
                   // return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        var gridBpkb = [];
        $scope.getDataBpkb = function() {
            MonitoringStnkBpkbFactory.getDataBpkb($scope.filter.DateTimeJadwalPenerimaan).then(
                function(res) {
                    $scope.gridBpkb.data = res.data.Result;
                    $scope.loading = false;
                   // return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.getDataStnk();
        $scope.getDataBpkb();

        $scope.getAll = function(date) {
            $scope.filter = { DateTimeJadwalPenerimaan: date };

            MonitoringStnkBpkbFactory.getDataStnk($scope.filter.DateTimeJadwalPenerimaan).then(
                function(res) {
                    $scope.gridStnk.data = res.data.Result;
                    $scope.loading = false;
                   // return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            MonitoringStnkBpkbFactory.getDataBpkb($scope.filter.DateTimeJadwalPenerimaan).then(
                function(res) {
                    $scope.gridBpkb.data = res.data.Result;
                    $scope.loading = false;
                    //return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        ///////////////// STNK //////////////////////////

        $scope.terimaSTNK = function(SelectedTerimaSTNK) {
            //SetReceiveSTNKDateMinDate(SelectedTerimaSTNK.POBiroJasaUnitDate); //ini kode buat logika tanggal ga bole mundur
            $scope.rowsDocument = angular.copy(SelectedTerimaSTNK);
         
            $scope.CentangSTNK();
            $scope.CentangPoliceNumber();
            $scope.CentangNoticePajak();

            setTimeout(function() {
                angular.element('.ui.modal.terimaSTNKMonitoring').modal('refresh');
                $scope.dateOptionMax.minDate = $scope.rowsDocument.POBiroJasaUnitDate;
            }, 0);
            angular.element('.ui.modal.terimaSTNKMonitoring').modal('setting', { closable: false }).modal('show');
            angular.element('.ui.modal.terimaSTNKMonitoring').not(':first').remove();

        }

        $scope.CentangSTNK = function() {
            try {
                if ($scope.rowsDocument.STNKNo.length > 0) {
                    $scope.NoSTNKCekChanged = true;
                }
            } catch (error) {
                $scope.NoSTNKCekChanged = false;
            }

        }

        $scope.CentangPoliceNumber = function() {
            try {
                if ($scope.rowsDocument.PoliceNumber.length > 0) {
                    $scope.PoliceNumberCekChanged = true;
                }
            } catch (error) {
                $scope.PoliceNumberCekChanged = false;
            }
        }

        $scope.CentangNoticePajak = function() {
            try {
                if ($scope.rowsDocument['BBNKB'].toString().length > 0 || $scope.rowsDocument['PKB'].toString().length > 0 || $scope.rowsDocument['SWDKLLJ'].toString().length > 0 || $scope.rowsDocument['BiayaAdmSTNK'].toString().length > 0 || $scope.rowsDocument['BiayaAdmTNKB'].toString().length > 0) {
                    $scope.NoticePajakCekChanged = true;
                }
            } catch (error) {
                $scope.NoticePajakCekChanged = false;
            }
        }

        $scope.simpanTerimaSTNK = function() {
            $scope.The_SelectedTerimaSTNK = angular.copy($scope.rowsDocument);
            $scope.mtd = { Method: "Terima" };
            $scope.mTerimaSTNK = angular.merge({},$scope.The_SelectedTerimaSTNK,$scope.mtd);

            TerimaSTNKBirojasaFactory.create($scope.mTerimaSTNK).then(function() {
                //nanti di get
                // TerimaSTNKBirojasaFactory.getData().then(
                //     function(res) {
                //         $scope.grid.data = res.data.Result;
                //         $scope.loading = false;
                        
                //         //return res.data;
                //     },
                //     function(err) {
                //         console.log("err=>", err);
                //     }
                // )

                bsNotify.show({
                    title: "Berhasil",
                    content: "Data berhasil disimpan.",
                    type: 'success'
                });

                $scope.loading = false;
                $scope.notifSuccess();
                $scope.getDataStnk();
                $scope.backFromterimaSTNK();

            });

        }

        $scope.backFromterimaSTNK = function() {

            $scope.rowsDocument = null;
            angular.element('.ui.modal.terimaSTNKMonitoring').modal('hide');
        }

        $scope.alasanterlambatstnk = function(SelectedDataTerlambat) {
            $scope.backFromAlasanRevisiSTNK();
            $scope.The_SelectedDataTerlambat = angular.copy(SelectedDataTerlambat);
            $scope.SelectedDataTerlambat_FrameNo = angular.copy(SelectedDataTerlambat.FrameNo);
            $scope.SelectedDataTerlambat_POBiroJasaUnitNo = angular.copy(SelectedDataTerlambat.POBiroJasaUnitNo);
            $scope.SelectedDataTerlambat_Model = angular.copy(SelectedDataTerlambat.VehicleModelName);
            $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK = angular.copy(SelectedDataTerlambat.AlasanKeterlambatanSTNK);
            $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = angular.copy(SelectedDataTerlambat.LamaKeterlambatanSTNK);
            angular.element('.ui.small.modal.alasanterlambatstnkMonitoring').modal('setting', { closable: false }).modal('show');
        }

        $scope.simpanAlasanRevisiSTNK = function() {
            $scope.The_SelectedDataTerlambat['AlasanKeterlambatanSTNK'] = angular.copy($scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK);
            $scope.The_SelectedDataTerlambat['LamaKeterlambatanSTNK'] = angular.copy($scope.SelectedDataTerlambat_LamaKeterlambatanSTNK);

            // if($scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK == undefined || $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK == null || $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK =="" ){
            //     bsNotify.show({
            //         title: "Peringatan",
            //         content: "Silahkan isi alasan keterlamabatan terlebih dahulu.",
            //         type: 'warning'
            //     });
            // } else {
                $scope.mtd = { Method: "Delay" };
                $scope.mSTNKMethod = angular.merge({},$scope.The_SelectedDataTerlambat,$scope.mtd);
                TerimaSTNKBirojasaFactory.create($scope.mSTNKMethod).then(function() {
                    //nanti di get
                    $scope.loading = false;
                    $scope.notifSuccess();
                    $scope.getDataStnk();
                    $scope.backFromAlasanRevisiSTNK();

                    // TerimaSTNKBirojasaFactory.getData().then(
                    //     function(res) {
                    //         $scope.grid.data = res.data.Result;
                    //         $scope.loading = false;
                           
                    //         //return res.data;
                    //     },
                    //     function(err) {
                    //         console.log("err=>", err);
                    //     }
                    // );
                    //.then(function() {
                        

                   // });

                });
            // }
        }

        $scope.backFromAlasanRevisiSTNK = function() {
            $scope.SelectedDataTerlambat_FrameNo = null;
            $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK = null;
            $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = null;
            angular.element('.ui.small.modal.alasanterlambatstnkMonitoring').modal('hide');
        }

        $scope.revisiStnk = function(SelectedRevisiStnk) {
            $scope.The_SelectedRevisiSTNK = angular.copy(SelectedRevisiStnk);
            angular.element('.ui.small.modal.revisiStnkMonitoring').modal('setting', { closable: false }).modal('show');
        }

        $scope.openSaveRevisiSTNK = function (){
            angular.element('.ui.modal.revisiStnk').modal('setting', { closable: false }).modal('hide');
            angular.element('.ui.modal.alasanrevisiSTNK').modal('setting', { closable: false }).modal('show');
        }

        $scope.batalRevisiSTNK = function (){
            angular.element('.ui.modal.alasanrevisiSTNK').modal('hide');
        }



        $scope.SaveRevisiSTNK = function() {
            $scope.The_SelectedRevisiSTNK['RevisiSTNKBit'] = angular.copy(true);

            $scope.mtd = {Method: "Revisi"};
            $scope.mRevisiSTNK = angular.merge({},$scope.The_SelectedRevisiSTNK,$scope.mtd); 

            TerimaSTNKBirojasaFactory.create($scope.mRevisiSTNK).then(function() {
                //nanti di get
                angular.element('.ui.modal.alasanrevisiSTNK').modal('setting', { closable: false }).modal('hide');
                $scope.notifSuccess();
                $scope.getDataStnk();

                // TerimaSTNKBirojasaFactory.getData().then(
                //     function(res) {
                //         $scope.grid.data = res.data.Result;
                //         $scope.loading = false;
                //         bsNotify.show({
                //             title: "Berhasil",
                //             content: "Berhasil revisi STNK.",
                //             type: 'success'
                //         });
                //         //return res.data;
                //     },
                //     function(err) {
                //         // console.log("err=>", err);
                //         bsNotify.show({
                //             title: "Gagal",
                //             content: "Terjadi kegagalan. Error : "+err.data.Message,
                //             type: 'danger'
                //         });
                //     }
                // //).then(function() {
                //    // $scope.backFromRevisiSTNK();

                // );

                $scope.backFromRevisiSTNK();

           });
        }

        $scope.backFromRevisiSTNK = function() {
            angular.element('.ui.small.modal.revisiStnkMonitoring').modal('hide');
        }

        $scope.input = function(Qty) {
            if(Qty <= 0 ){
                $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = 0;
            }else{
                var first = $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK.substring(0, 1);
                var length = $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK.length;
                var fixed = $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK.substring(1, length);
                if(first == 0){
                    $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = fixed;
                }
            }
            // var asd = angular.copy($scope.mdistributionSpk.FormSPKNoFrom);
            // $scope.mdistributionSpk.FormSPKNoTo = asd;
        }

        ///////////////// BPKB //////////////////////////

        $scope.terimaBPKB = function(SelectedterimaBPKB) {
            SetReceiveBPKBDateMinDate(SelectedterimaBPKB.POBiroJasaUnitDate); //ini kode buat logika tanggal ga bole mundur
            $scope.rowsDocument = angular.copy(SelectedterimaBPKB);
            angular.element('.ui.modal.terimaBPKBMonitoring').modal('setting', { closable: false }).modal('show');
        }

        $scope.simpanterimaBPKB = function() {
            $scope.The_SelectedterimaBPKB = angular.copy($scope.rowsDocument);

            TerimaBpkbFactory.create($scope.The_SelectedterimaBPKB).then(function() {
                //nanti di get
                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.gridStnk.data = res.data.Result;
                        $scope.loading = false;
                        $scope.notifSuccess();
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                )
                //.then(function() {
                    $scope.backFromterimaBPKB();

                //});

            });

        }

        $scope.backFromterimaBPKB = function() {

            $scope.rowsDocument = null;
            angular.element('.ui.modal.terimaBPKBMonitoring').modal('hide');
        }

        $scope.alasanterlambatbpkb = function(SelectedDataTerlambat) {
            $scope.backFromAlasanRevisiBPKB();
            $scope.The_SelectedDataTerlambat = angular.copy(SelectedDataTerlambat);
            $scope.SelectedDataTerlambat_FrameNo = angular.copy(SelectedDataTerlambat.FrameNo);
            $scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB = angular.copy(SelectedDataTerlambat.AlasanKeterlambatanBPKB);
            $scope.SelectedDataTerlambat_LamaKeterlambatanBPKB = angular.copy(SelectedDataTerlambat.LamaKeterlambatanBPKB);
            angular.element(".ui.modal.alasanterlambatbpkbMonitoring").modal("setting", { closable: false }).modal("show");
        }

        $scope.simpanAlasanRevisiBPKB = function() {
            $scope.The_SelectedDataTerlambat['AlasanKeterlambatanBPKB'] = angular.copy($scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB);
            $scope.The_SelectedDataTerlambat['LamaKeterlambatanBPKB'] = angular.copy($scope.SelectedDataTerlambat_LamaKeterlambatanBPKB);

            TerimaBpkbFactory.update($scope.The_SelectedDataTerlambat).then(function() {
                //nanti di get


                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.gridStnk.data = res.data.Result;
                        $scope.loading = false;
                        $scope.notifSuccess();
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromAlasanRevisi();

                });

            });
        }

        $scope.backFromAlasanRevisiBPKB = function() {
            $scope.SelectedDataTerlambat_FrameNo = null;
            $scope.SelectedDataTerlambat_AlasanKeterlambatanBPKB = null;
            $scope.SelectedDataTerlambat_LamaKeterlambatanBPKB = null;
            angular.element('.ui.modal.alasanterlambatbpkbMonitoring').modal('hide');
        }

        $scope.revisiBPKB = function(SelectedRevisiBpkb) {
            $scope.The_SelectedRevisiBPKB = angular.copy(SelectedRevisiBpkb);
            angular.element('.ui.modal.revisibpkbMonitoring').modal('setting', { closable: false }).modal('show');
        }

        $scope.SaveRevisiBPKB = function() {
            $scope.The_SelectedRevisiBPKB['RevisiBPKBBit'] = angular.copy(true);

            TerimaBpkbFactory.update($scope.The_SelectedRevisiBPKB).then(function() {
                //nanti di get
                TerimaBpkbFactory.getData().then(
                    function(res) {
                        $scope.gridStnk.data = res.data.Result;
                        $scope.loading = false;

                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromRevisiBPKB();

                });

            });
        }

        $scope.backFromRevisiBPKB = function() {
            angular.element('.ui.modal.revisibpkbMonitoring').modal('hide');
        }

        $scope.notifSuccess = function (){
            bsNotify.show({
                title: "Berhasil",
                content: "Data berhasil disimpan.",
                type: 'success'
            });
        }

        var actionbuttonstnk = ''+
        '<a href="" style="color:#777;" ng-show="row.entity.TombolDelay == true" class="trlink ng-scope" uib-tooltip="Alasan Delay STNK" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.alasanterlambatstnk(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-calendar-plus-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a href="" style="color:#777;" ng-show="row.entity.TombolRevisi == true" class="trlink ng-scope" uib-tooltip="Revisi STNK" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.revisiStnk(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a  href="" style="color:#777;" ng-show="row.entity.TombolTerima == true" class="trlink ng-scope" uib-tooltip="Terima STNK" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.terimaSTNK(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-handshake-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';

        // var actionbuttonbpkb = ''+
        // '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Alasan Delay BPKB" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.alasanterlambatbpkb(row.entity)" tabindex="0"> ' +
        // '<i class="fa fa-fw fa-lg fa-calendar-plus-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        // '</a>' +

        // '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Revisi BPKB" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.revisiBPKB(row.entity)" tabindex="0"> ' +
        // '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        // '</a>' +

        // '<a  href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Terima BPKB" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.terimaBPKB(row.entity)" tabindex="0"> ' +
        // '<i class="fa fa-fw fa-lg fa-handshake-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        // '</a>';

        // var actionbuttonbpkb = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Alasan Delay BPKB" ng-click="grid.appScope.alasanterlambatbpkb(row.entity)">Alasan Delay BPKB</u>&nbsp' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Revisi BPKB" ng-click="grid.appScope.revisiBPKB(row.entity)">Revisi BPKB</u>&nbsp' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Terima BPKB" ng-click="grid.appScope.terimaBPKB(row.entity)">Terima BPKB</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        // var actionbuttonstnk = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Alasan Delay STNK" ng-click="grid.appScope.alasanterlambatstnk(row.entity)">Alasan Delay STNK</u>&nbsp' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Revisi STNK" ng-click="grid.appScope.revisiStnk(row.entity)">Revisi STNK</u>&nbsp' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Terima STNK" ng-click="grid.appScope.terimaSTNK(row.entity)">Terima STNK</u>&nbsp' +
        //     '</p>' +
        //     '</a>';


        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------

        // $scope.comboSetup = function() {
        //     if($scope.data.optionData.name == "No. PO") {
        //         $scope.param = $scope.data.optionData.id;
        //     }
        // }

        $scope.FilterGrid = function() {
            // console.log('Adidot --> ', $scope.data.selectOption, $scope.filter.Search);
            if($scope.filter.Search == undefined || $scope.data.selectOption == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Isi filter dan pilih combobox terlebih dahulu",
                    type: 'warning'
                });
            } else {

                MonitoringStnkBpkbFactory.getSearchData($scope.data.selectOption, $scope.filter.Search).then(
                    function(res) {
                        $scope.gridStnk.data = res.data.Result;
                        console.log('Didot --> ', res.data.Result);
                    }
                );
            }
        }

        $scope.data = {
            selectOption: null,
            optionData: [
                { id: 'POBiroJasaUnitNo', name: 'No. PO' },
                { id: 'ServiceBureauName', name: 'Vendor' },
                { id: 'OutletName', name: 'Cabang' },
                { id: 'FrameNo', name: 'No. Rangka'},
                { id: 'VehicleModelName', name: 'Model'}
            ]
        };

        $scope.gridStnk = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
             paginationPageSizes: [10, 25, 50],
             paginationPageSize: 10,
            columnDefs: [
                // { name: 'Id', field: 'Id', visible: false},
                { name: 'no po', field: 'POBiroJasaUnitNo' },
                { name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'cabang', field: 'OutletName' },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'model kendaraan', field: 'VehicleModelName' },
                { name: 'status', field: 'STNKStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '12%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbuttonstnk
                }
            ]
        };

        // $scope.gridBpkb = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [
        //         // { name: 'Id', field: 'Id', visible: false},
        //         { name: 'no po', field: 'POBiroJasaUnitNo' },
        //         { name: 'Vendor', field: 'ServiceBureauName' },
        //         //{ name: 'cabang', field: 'OutletName' },
        //         { name: 'no rangka', field: 'FrameNo' },
        //         //{ name: 'nama pelanggan', field: 'CustomerName' },
        //         { name: 'model kendaraan', field: 'VehicleModelName' },
        //         {
        //             name: 'action',
        //             allowCellFocus: false,
        //             width: '30%',
        //             pinnedRight: true,
        //             enableColumnMenu: false,
        //             enableSorting: false,
        //             enableColumnResizing: true,
        //             cellTemplate: actionbuttonbpkb
        //         }
        //     ]
        // };
    });