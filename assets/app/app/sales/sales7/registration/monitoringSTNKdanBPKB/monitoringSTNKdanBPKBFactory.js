angular.module('app')
    .factory('MonitoringStnkBpkbFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        return {
            getDataStnk: function(param) {
                var tanggal = param.getFullYear() + '-' +
                    ('0' + (param.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + param.getDate()).slice(-2);

                var res = $http.get('/api/sales/MonitoringSTNKPOBiroJasaUnit?start=1&limit=1000&JadwalPenerimaan=' + tanggal);
                return res;
            },

            getSearchData: function(param1, param2){
                var res=$http.get('/api/sales/MonitoringSTNKPOBiroJasaUnit/?start=1&limit=100&filterData='+param1+'|' + param2);
                return res;
            },

            getDataBpkb: function(param) {
                var tanggal = param.getFullYear() + '-' +
                    ('0' + (param.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + param.getDate()).slice(-2);

                var res = $http.get('/api/sales/MonitoringBPKBPOBiroJasaUnit?start=1&limit=1000&JadwalPenerimaan=' + tanggal);
                return res;
            },

        }
    });