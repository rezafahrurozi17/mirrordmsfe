angular
  .module("app")
  .controller(
    "MaintainAfiController",
    function (
      bsNotify,
      $stateParams,
      $scope,
      $http,
      CurrentUser,
      MaintainAfiFactory,
      $timeout,
      ComboBoxFactory
    ) {
      //----------------------------------
      // Start-Up
      //----------------------------------

      $scope.$on("$viewContentLoaded", function () {
        $scope.loading = false;
        $scope.gridData = [];
      });
      var minDate = new Date();
      //----------------------------------
      // Initialization
      //----------------------------------
      $scope.disabledAjuafiketam = false;
      $scope.disabledTambahAjuAfi = false;
      $scope.user = CurrentUser.user();
      $scope.breadcrums = {};
      $scope.tab = angular.copy($stateParams.tab);
      //console.log("menu",$scope.tabAFI);
      $scope.mMaintainAfi = null; //Model
      //$scope.xRole = { selected: [] };
      $scope.filter = {
        Id: 0,
        StatusAFIId: null,
        FrameNo: null,
        SONo: null,
        CustomerName: null,
      };

      MaintainAfiFactory.getPStatusAfi().then(function (res) {
        $scope.statusafi = res.data.Result;
      });

      if ($scope.user.RoleName == "CAO/HO") {
        $scope.filter.StatusAFIId = 2;
      } else {
        $scope.filter.StatusAFIId = 1;
      }

      MaintainAfiFactory.getDataNew($scope.filter).then(function (res) {
        $scope.grid.data = res.data.Result;
        $scope.loading = false;
      });

      $scope.urlCetakan = null;
      $scope.loading = false;
      $scope.printAfiform = false;
      $scope.printAtpm = false;
      $scope.listContainer = true;
      $scope.formData = false;
      $scope.actionAfi = null;
      $scope.selectedData = [];
      $scope.regioncode = [];
      $scope.isValidate = false;

      $scope.dateOptions = {
        format: "dd-MM-yyyy",
      };

      $scope.dateOptionsfaktur = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 0,
      };

      ComboBoxFactory.getDataProvince().then(function (res) {
        $scope.province = res.data.Result;
      });
      //=======================
      // $scope.$watch("mMaintainAfi.ProvinceId", function (newValue, oldValue) {
      //     if ($scope.mMaintainAfi.ProvinceId != null && typeof $scope.mMaintainAfi.ProvinceId != "undefined") {
      //         ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.mMaintainAfi.ProvinceId).then(function (res) {
      //             $scope.kabupaten0 = res.data.Result;
      //         });
      //     };
      // })

      // $scope.$watch("mMaintainAfi.CityRegencyId", function (newValue, oldValue) {
      //     if ($scope.mMaintainAfi.CityRegencyId != null && typeof $scope.mMaintainAfi.CityRegencyId != "undefined") {
      //         ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.mMaintainAfi.CityRegencyId).then(function (res) {
      //             $scope.kecamatan0 = res.data.Result;
      //         });
      //     };
      // })

      // $scope.$watch("mMaintainAfi.DistrictId", function (newValue, oldValue) {
      //     if ($scope.mMaintainAfi.DistrictId != null && typeof $scope.mMaintainAfi.DistrictId != "undefined") {
      //         ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.mMaintainAfi.DistrictId).then(function (res) {
      //             $scope.kelurahan0 = res.data.Result;
      //         });
      //     };
      // })
      // /////

      // =====================
      $scope.filterKabupaten = function (selected, param) {
        try {
          if (selected == null && param !== undefined) {
            selected = param.ProvinceId;
          }
          ComboBoxFactory.getDataKabupaten(
            "?start=1&limit=100&filterData=ProvinceId|" + selected
          ).then(function (res) {
            $scope.kabupaten = res.data.Result;
            $scope.mMaintainAfi.CityRegencyId = null;
            $scope.mMaintainAfi.DistrictId = null;
            $scope.mMaintainAfi.VillageId = null;
            if (
              $scope.mMaintainAfi.ProvinceId == null ||
              $scope.mMaintainAfi.ProvinceId == undefined ||
              $scope.mMaintainAfi.ProvinceId == ""
            ) {
              $scope.mMaintainAfi.AFIRegionId = null;
            }
            if (param !== undefined) {
              $scope.filterKecamatan(param.CityRegencyId, param);
              $scope.mMaintainAfi.CityRegencyId = param.CityRegencyId;
            } else {
              $scope.mMaintainAfi.AFIRegionId = null;
              $scope.mMaintainAfi.ProvinceId = selected;
              for (var i in $scope.province) {
                if (
                  $scope.mMaintainAfi.ProvinceId ==
                  $scope.province[i].ProvinceId
                ) {
                  $scope.mMaintainAfi.ProvinceName =
                    $scope.province[i].ProvinceName;
                }
              }
              console.log("Cek Perubahan", $scope.mMaintainAfi);
            }
          });
        } catch (err) {}
      };

      $scope.filterKecamatan = function (selected, param) {
        try {
          MaintainAfiFactory.getRegionCode(
            selected,
            $scope.mMaintainAfi.ProvinceId
          ).then(function (res) {
            $scope.regioncode = res.data.Result;
            for (var i = 0; i < res.data.Result.length; i++) {
              $scope.regioncode[i].xPostalCode =
                $scope.regioncode[i].PostalCode +
                " - " +
                $scope.regioncode[i].Kota;
              $scope.regioncode[i].PostalCode =
                $scope.regioncode[i].PostalCode.toString();
            }
          });
          ComboBoxFactory.getDataKecamatan(
            "?start=1&limit=100&filterData=CityRegencyId|" + selected
          ).then(function (res) {
            $scope.kecamatan = res.data.Result;
            $scope.mMaintainAfi.DistrictId = null;
            $scope.mMaintainAfi.VillageId = null;
            if (param !== undefined) {
              $scope.filterKelurahan(param.DistrictId, param);
              $scope.mMaintainAfi.DistrictId = param.DistrictId;
            } else {
              $scope.mMaintainAfi.AFIRegionId = null;
              $scope.mMaintainAfi.CityRegencyId = selected;
              for (var i in $scope.kabupaten) {
                if (
                  $scope.mMaintainAfi.CityRegencyId ==
                  $scope.kabupaten[i].CityRegencyId
                ) {
                  $scope.mMaintainAfi.CityRegencyName =
                    $scope.kabupaten[i].CityRegencyName;
                }
              }
              console.log("Cek Perubahan", $scope.mMaintainAfi);
            }

            if (
              $scope.mMaintainAfi.CityRegencyId == null ||
              $scope.mMaintainAfi.CityRegencyId == undefined ||
              $scope.mMaintainAfi.CityRegencyId == ""
            ) {
              $scope.mMaintainAfi.AFIRegionId = null;
            }
          });
        } catch (err) {}
      };

      $scope.filterKelurahan = function (selected, param) {
        try {
          $scope.mMaintainAfi.VillageId = null;
          MaintainAfiFactory.NewgetRegionCode(
            $scope.mMaintainAfi.CityRegencyId,
            $scope.mMaintainAfi.ProvinceId,
            selected,
            $scope.mMaintainAfi.VillageId
          ).then(function (res) {
            $scope.regioncode = res.data.Result;
            for (var i = 0; i < res.data.Result.length; i++) {
              $scope.regioncode[i].xPostalCode =
                $scope.regioncode[i].PostalCode +
                " - " +
                $scope.regioncode[i].Kota;
              $scope.regioncode[i].PostalCode =
                $scope.regioncode[i].PostalCode.toString();
            }
          });
          ComboBoxFactory.getDataKelurahan(
            "?start=1&limit=100&filterData=DistrictId|" + selected
          ).then(function (res) {
            $scope.kelurahan = res.data.Result;
            $scope.mMaintainAfi.VillageId = null;
            if (param !== undefined) {
              $scope.mMaintainAfi.VillageId = param.VillageId;
            } else {
              $scope.mMaintainAfi.AFIRegionId = null;
              $scope.mMaintainAfi.DistrictId = selected;
              // for(var i in $scope.kecamatan){
              //     if($scope.mMaintainAfi.DistrictId == $scope.kecamatan[i].DistrictId){
              //         $scope.mMaintainAfi.DistrictName = $scope.kecamatan[i].DistrictName
              //     }
              // }
              console.log("Cek Perubahan", $scope.mMaintainAfi);
            }

            if (
              $scope.mMaintainAfi.DistrictId == null ||
              $scope.mMaintainAfi.DistrictId == undefined ||
              $scope.mMaintainAfi.DistrictId == ""
            ) {
              $scope.mMaintainAfi.AFIRegionId = null;
            }
          });
        } catch (err) {}
      };
      $scope.filterVillage = function (selected, param) {
        try {
          MaintainAfiFactory.NewgetRegionCode(
            $scope.mMaintainAfi.CityRegencyId,
            $scope.mMaintainAfi.ProvinceId,
            $scope.mMaintainAfi.DistrictId,
            selected
          ).then(function (res) {
            $scope.regioncode = res.data.Result;
            for (var i = 0; i < res.data.Result.length; i++) {
              $scope.regioncode[i].xPostalCode =
                $scope.regioncode[i].PostalCode +
                " - " +
                $scope.regioncode[i].Kota;
              $scope.regioncode[i].PostalCode =
                $scope.regioncode[i].PostalCode.toString();
            }
          });
          if (
            $scope.mMaintainAfi.VillageId == null ||
            $scope.mMaintainAfi.VillageId == undefined ||
            $scope.mMaintainAfi.VillageId == ""
          ) {
            $scope.mMaintainAfi.AFIRegionId = null;
          }

          if (param !== undefined) {
          } else {
            $scope.mMaintainAfi.AFIRegionId = null;
          }
          $scope.mMaintainAfi.VillageId = selected;
          for (var i in $scope.kelurahan) {
            if (
              $scope.mMaintainAfi.VillageId == $scope.kelurahan[i].VillageId
            ) {
              $scope.mMaintainAfi.VillageName = $scope.kelurahan[i].VillageName;
              $scope.mMaintainAfi.PostalCode = $scope.kelurahan[i].PostalCode;
            }
          }
          console.log("Cek Perubahan", $scope.mMaintainAfi);
        } catch (err) {}
      };

      // $scope.filterRegionCode = function (Id){
      //     MaintainAfiFactory.getRegionCode(Id).then(function(res) { $scope.regioncode = res.data.Result });
      // }

      //MaintainAfiFactory.getRegionCode().then(function(res) { $scope.regioncode = res.data.Result });

      MaintainAfiFactory.getCarType().then(function (res) {
        $scope.cartype = res.data.Result;
      });
      MaintainAfiFactory.getRevisionAfiCategory().then(function (res) {
        $scope.afirevision = res.data.Result;
      });

      //----------------------------------
      // Get Data
      //----------------------------------
      $scope.printAfi = function () {
        $scope.listContainer = false;
        $scope.printAfiform = true;
      };

      $scope.printAfibtn = function () {
        $scope.listContainer = false;
        $scope.printAtpm = true;
      };

      $scope.Close = function () {
        $scope.listContainer = true;
        $scope.printAfiform = false;
        $scope.printAtpm = false;
      };

      $scope.revisi = function () {
        $scope.listContainer = false;
        $scope.formData = true;
      };

      $scope.view = function () {
        $scope.primaryContent = true;
        $scope.listContainer = false;
        $scope.frameno = false;
        $scope.formData = true;
      };

      $scope.cancleation = function () {
        $scope.listContainer = false;
        $scope.formData = true;
      };

      $scope.cancle = function () {
        $scope.tipe = null;
        $scope.listContainer = true;
        $scope.formAfi = false;
        $scope.actionAfi = null;
      };

      $scope.getData = function () {
        MaintainAfiFactory.getDataNew($scope.filter).then(function (res) {
          $scope.grid.data = res.data.Result;
          $scope.loading = false;
          $scope.isHAveAjuAfiTAM = true;
        });
      };

      $scope.loadImage = function () {};

      $scope.KeluarDokumenAfi = function () {
        angular
          .element(".ui.modal.dokumenAfiLihat")
          .modal("refresh")
          .modal("hide");
      };

      $scope.openDokumenlist = function () {
        ComboBoxFactory.getDataDocument().then(function (res) {
          $scope.dokumentlist = res.data.Result;
          setTimeout(function () {
            angular.element(".ui.modal.dokumentListAFI").modal("refresh");
          }, 0);
          angular
            .element(".ui.modal.dokumentListAFI")
            .modal({ observeChanges: true })
            .modal("show");
          angular.element(".ui.modal.dokumentListAFI").not(":first").remove();
        });
      };

      $scope.selectedItems = [];
      $scope.toggleCheckedAfiDoc = function (data) {
        if (data.checked) {
          data.checked = false;
          var index = $scope.selectedItems.indexOf(data);
          $scope.selectedItems.splice(index, 1);
        } else {
          data.checked = true;
          $scope.selectedItems.push(data);
        }
      };

      $scope.ApproveData = function () {
        $scope.dateOptionsfaktur.minDate = new Date();
        angular.element(".ui.modal.dokumentListAFI").modal("hide");
        if ($scope.mMaintainAfi.listDokumen.length == 0) {
          $scope.mMaintainAfi.listDokumen = angular.copy($scope.selectedItems);
        } else {
          for (var i in $scope.mMaintainAfi.listDokumen) {
            for (var j in $scope.selectedItems) {
              if (
                $scope.selectedItems[j].DocumentId ==
                $scope.mMaintainAfi.listDokumen[i].DocumentId
              ) {
                $scope.selectedItems.splice(j, 1);
              }
            }
          }
          for (var i in $scope.selectedItems) {
            $scope.mMaintainAfi.listDokumen.push($scope.selectedItems[i]);
          }
        }

        for (var i = 0; i < $scope.selectedItems.length; i++) {
          $scope.selectedItems[i].ApprovalFlag = 1;
          delete $scope.selectedItems[i].checked;
        }

        $scope.selectedItems = [];
      };

      $scope.cekStatusUpload = function (object, check) {
        if (check != null || check != undefined) {
        }
      };

      $scope.RejectData = function () {
        for (var i = 0; i < $scope.selectedItems.length; i++) {
          $scope.selectedItems[i].ApprovalFlag = 0;
          delete $scope.selectedItems[i].checked;
        }

        $scope.selectedItems = [];
        angular.element(".ui.modal.dokumentListAFI").modal("hide");
      };

      $scope.removeExtension = function (index) {
        $scope.mMaintainAfi.listDokumen.splice(index, 1);
      };

      var cross =
        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Hapus" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.revisiAfi(row.entity) tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        "</a>";

      var actionbutton =
        '<div ng-if=""><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat AFI" tooltip-placement="right" onclick="this.blur()" ng-hide="row.entity.TombolDetail == false" ng-click="grid.appScope.$parent.liahAfi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        "</a></div>" +
        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Revisi AFI" tooltip-placement="bottom" onclick="this.blur()" ng-hide="row.entity.TombolRevisi == false" ng-click="grid.appScope.$parent.revisiAfi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        "</a>" +
        '<a  href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Batal AFI" tooltip-placement="bottom" onclick="this.blur()" ng-hide="row.entity.TombolBatal == false" ng-click="grid.appScope.$parent.batalAfi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        "</a>";

      var actionbuttonCAO =
        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat AFI" tooltip-placement="bottom" onclick="this.blur()" ng-hide="row.entity.TombolDetail == false" ng-click="grid.appScope.$parent.liahAfi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        "</a>";

      $scope.tambahAFi = function () {
        $scope.breadcrums.title = "Tambah";
        $scope.tipe = angular.copy($scope.selectedData[0].CustomerTypeDesc);
        $scope.mMaintainAfi = angular.copy($scope.selectedData[0]);
        $scope.PlusRight = $scope.selectedData[0].TombolAju;
        $scope.mMaintainAfi.CustomColorName =
          $scope.mMaintainAfi.CustomColorName;

        if ($scope.mMaintainAfi.InvoiceStartEffectiveDate != null) {
          // $scope.dateOptionsfaktur.minDate = $scope.mMaintainAfi.InvoiceStartEffectiveDate;
          $scope.dateOptionsfaktur.minDate = minDate;
        } else {
          $scope.dateOptionsfaktur.minDate = minDate;
        }

        console.log(
          "inidiaaa -->",
          $scope.dateOptionsfaktur,
          $scope.mMaintainAfi.InvoiceStartEffectiveDate
        );

        angular.forEach(
          $scope.mMaintainAfi.listDokumen,
          function (Doc, DocIndx) {
            Doc.UploadDokumenCustomer = { UpDocObj: null, FileName: null };
            Doc.UploadDokumenCustomer.UpDocObj = Doc.DocumentData;
            Doc.UploadDokumenCustomer.FileName = Doc.DocumentDataName;
          }
        );
        $scope.actionAfi = null;
        $scope.formAfi = true;
        $scope.listContainer = false;
        try {
          // ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selectedData[0].ProvinceId).then(function (res) { $scope.kabupaten = res.data.Result; });
          $scope.filterKabupaten(null, $scope.selectedData[0]);
        } catch (err) {}
      };

      $scope.liahAfi = function (rows) {
        $scope.selectedData = [];
        $scope.selectedData.push(rows);

        $scope.breadcrums.title = "Lihat";
        $scope.urlCetakan =
          "sales/PermohonanPengajuanFakturATPMdanSTNK?AFIId=" +
          rows.AFIId +
          "&FrameNo=" +
          rows.FrameNo;
        $scope.tipe = angular.copy(rows.CustomerTypeDesc);
        $scope.actionAfi = "lihatAfi";
        $scope.mMaintainAfi = angular.copy(rows);
        angular.forEach(
          $scope.mMaintainAfi.listDokumen,
          function (Doc, DocIndx) {
            Doc.UploadDokumenCustomer = { UpDocObj: null, FileName: null };
            Doc.UploadDokumenCustomer.UpDocObj = Doc.DocumentData;
            Doc.UploadDokumenCustomer.FileName = Doc.DocumentDataName;
          }
        );
        $scope.formAfi = true;
        $scope.listContainer = false;

        console.log("rows", rows);
        $scope.viewAfi = angular.copy(rows);

        try {
          // ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.mMaintainAfi.ProvinceId).then(function (res) { $scope.kabupaten = res.data.Result; });
          $scope.filterKabupaten(null, $scope.selectedData[0]);
        } catch (err) {}
      };

      $scope.testdebug = function () {};

      $scope.selectedRow = {};
      $scope.revisiAfi = function (rows) {
        // -- REVISI TANPA HARUS CEKLIS DI GRID
        $scope.selectedData = [];
        $scope.selectedData.push(rows);
        //
        angular.element(".ui.modal.revisionAFI").modal("show");
        $scope.selectedRow = angular.copy(rows);
      };

      $scope.BatalKategori = function () {
        $scope.CategoryAfi = [];
        angular.element(".ui.modal.revisionAFI").modal("hide");
      };

      $scope.PilihKategori = function () {
        $scope.breadcrums.title = "Ubah";
        $scope.tipe = angular.copy($scope.selectedRow.CustomerTypeDesc);
        $scope.actionAfi = "revisiAfi";
        $scope.mMaintainAfi = angular.copy($scope.selectedRow);
        angular.forEach(
          $scope.mMaintainAfi.listDokumen,
          function (Doc, DocIndx) {
            Doc.UploadDokumenCustomer = { UpDocObj: null, FileName: null };
            Doc.UploadDokumenCustomer.UpDocObj = Doc.DocumentData;
            Doc.UploadDokumenCustomer.FileName = Doc.DocumentDataName;
          }
        );
        $scope.formAfi = true;
        $scope.listContainer = false;
        $scope.selectedCategoryAfi = $scope.CategoryAfi[0];
        //console.log("testcodeAFI", $scope.selectedCategoryAfi);
        //$scope.IdRevisiAfi = $scope.selectedCategoryAfi;
        $scope.IdRevisiAfi = {
          RevisionAFIId: $scope.selectedCategoryAfi.RevisionAFIId,
          RevisionAFICode: $scope.selectedCategoryAfi.RevisionAFICode,
        };
        $scope.CodeRevisiAfi = $scope.IdRevisiAfi.RevisionAFICode;

        angular.merge($scope.mMaintainAfi, $scope.IdRevisiAfi);
        //angular.merge($scope.mMaintainAfi,  $scope.CodeRevisiAfi);
        angular.element(".ui.modal.revisionAFI").modal("hide");
        $scope.CategoryAfi = [];

        try {
          // ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selectedRow.ProvinceId).then(function (res) { $scope.kabupaten = res.data.Result; });
          $scope.filterKabupaten(null, $scope.selectedData[0]);
        } catch (err) {}
        // if ($scope.selectedCategoryAfi.RevisionAFICode == 'REVC') {
        //     $scope.kecuali = false;
        // }
      };

      $scope.CategoryAfi = [];

      $scope.toggleAFICatChecked = function (data) {
        $scope.CategoryAfi.splice(0, 1);
        $scope.CategoryAfi.push(data);
      };

      $scope.batalAfi = function (rows) {
        // -- HAPUS TANPA HARUS CEKLIS DI GRID
        $scope.selectedData = [];
        $scope.selectedData.push(rows);
        //
        $scope.tipe = angular.copy(rows.CustomerTypeDesc);
        $scope.actionAfi = "batalAfi";
        $scope.mMaintainAfi = angular.copy(rows);
        $scope.formAfi = true;
        $scope.listContainer = false;

        // console.log('rows batal afi-->',rows);
        // $scope.batal = angular.copy(rows);

        try {
          // ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.mMaintainAfi.ProvinceId).then(function (res) { $scope.kabupaten = res.data.Result; });
          $scope.filterKabupaten(null, $scope.selectedData[0]);
        } catch (err) {}
      };

      $scope.viewDocumentAFI = function (dataDocument) {
        if (
          angular.isUndefined(dataDocument.DocumentURL) ||
          dataDocument.DocumentURL == "" ||
          dataDocument.DocumentURL == null
        ) {
          bsNotify.show({
            title: "Error Message",
            content: "Dokumen Belum DiUpload",
            type: "danger",
          });
        } else {
          MaintainAfiFactory.getImage(dataDocument.DocumentURL).then(function (
            res
          ) {
            $scope.dokumenName = dataDocument.DocumentName;
            $scope.dokumenData = res.data.UpDocObj;
            setTimeout(function () {
              angular.element(".ui.modal.dokumenAfiLihat").modal("refresh");
            }, 0);
            angular.element(".ui.modal.dokumenAfiLihat").modal("show");
            angular.element(".ui.modal.dokumenAfiLihat").not(":first").remove();
          });
        }
      };

      $scope.onSelectRows = function (rows) {
        var count = 0;
        $scope.isHAveAjuAfiTAM = true;
        $scope.selectedData = rows;
        for (var i in $scope.selectedData) {
          if (
            $scope.selectedData[i].RevisionAFIName.match(/Aju AFI Ke CAO.*/) ||
            $scope.selectedData[i].RevisionAFIName.match(/Approve.*/) ||
            $scope.selectedData[i].RevisionAFIName.match(/Reject.*/) ||
            $scope.selectedData[i].RevisionAFIName.match(/Dokumen Diterima.*/)
          ) {
            count++;
          }
        }
        if (count == $scope.selectedData.length) {
          $scope.isHAveAjuAfiTAM = false;
        }
        //console.log("onSelectRows=>", $scope.selectedData, count, $scope.isHAveAjuAfiTAM);
      };
      $scope.changeFormatDate = function (item) {
        var tmpParam = item;
        tmpParam = new Date(tmpParam);
        var finalDate;
        var yyyy = tmpParam.getFullYear().toString();
        var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = tmpParam.getDate().toString();
        finalDate =
          yyyy +
          "-" +
          (mm[1] ? mm : "0" + mm[0]) +
          "-" +
          (dd[1] ? dd : "0" + dd[0]);

        return finalDate;
      };

      $scope.ajuafiketam = function () {
        $scope.disabledAjuafiketam = true;
        for (var i in $scope.selectedData) {
          $scope.selectedData[i].InvoiceStartEffectiveDate =
            $scope.changeFormatDate(
              $scope.selectedData[i].InvoiceStartEffectiveDate
            );
        }
        for (var j in $scope.selectedData) {
          $scope.selectedData[j].InvoiceEndEffectiveDate =
            $scope.changeFormatDate(
              $scope.selectedData[j].InvoiceEndEffectiveDate
            );
        }
        MaintainAfiFactory.ajuAfiTAM($scope.selectedData).then(
          function (res) {
            bsNotify.show({
              title: "Success Message",
              // content: "AFI Berhasi Diajukan Ke TAM", tadinya ini
              content: res.data.ResponseMessage,
              type: "success",
            });
            $scope.disabledAjuafiketam = false;
            $scope.selectedData = [];
            $scope.getData();
          },
          function (res) {
            // console.log("sdasd", res, data);
            bsNotify.show({
              title: "Gagal",
              content: res.data.Message,
              type: "danger",
            });
            $scope.disabledAjuafiketam = false;
          }
        );
      };

      $scope.tambahAjuAfi = function () {
        $scope.disabledTambahAjuAfi = true;
        if ($scope.actionAfi == "revisiAfi") {
          MaintainAfiFactory.revisiAfi($scope.mMaintainAfi).then(function (
            res
          ) {
            $scope.grid.data = [];
            bsNotify.show({
              title: "Success Message",
              content: "Revisi Butuh Approval",
              type: "warning",
            });

            $scope.disabledTambahAjuAfi = false;
            $scope.loading = false;
            $scope.getData();
            $scope.formAfi = false;
            $scope.listContainer = true;
            $scope.actionAfi = null;
          });
        } else if ($scope.actionAfi == "batalAfi") {
          MaintainAfiFactory.batalAfi($scope.mMaintainAfi).then(function (res) {
            $scope.grid.data = [];
            bsNotify.show({
              title: "Berhasil",
              content: "Batal AFI Butuh Approval",
              type: "success",
            });
            $scope.disabledTambahAjuAfi = false;
            $scope.loading = false;
            $scope.getData();
            $scope.formAfi = false;
            $scope.listContainer = true;
            $scope.actionAfi = null;
          });
        } else {
          angular.forEach(
            $scope.mMaintainAfi.listDokumen,
            function (Doc, DocIndx) {
              Doc.DocumentData = "";
              if (angular.isUndefined(Doc.UploadDokumenCustomer) == false) {
                Doc.UploadDokumenCustomer.UpDocObj = btoa(
                  Doc.UploadDokumenCustomer.UpDocObj
                );
              }
            }
          );

          MaintainAfiFactory.create($scope.mMaintainAfi).then(
            function (res) {
              $scope.grid.data = [];
              console.log("resssss", res);
              bsNotify.show({
                title: "Success Message",
                // content: "AFI Berhasil Dikirim Ke CAO/HO", tadinya ini
                content: res.data.ResponseMessage,
                type: "success",
              });
              $scope.disabledTambahAjuAfi = false;
              $scope.loading = false;
              $scope.getData();
              $scope.formAfi = false;
              $scope.listContainer = true;
              $scope.actionAfi = null;
            },
            function (res) {
              console.log("sdasd", res, data);
              bsNotify.show({
                title: "Gagal",
                content: res.data.Message,
                type: "danger",
              });
              $scope.disabledTambahAjuAfi = false;
            }
          );
          // }
        }
      };

      $scope.btntest = function () {
        console.log($scope.angka(929012987));
      };

      $scope.angka = function (satuan) {
        var huruf = [
          "",
          "Satu",
          "Dua",
          "Tiga",
          "Empat",
          "Lima",
          "Enam",
          "Tujuh",
          "Delapan",
          "Sembilan",
          "Sepuluh",
          "Sebelas",
        ];
        //$scope.hasil="";
        if (satuan < 12) return huruf[satuan];
        else if (satuan < 20) return $scope.angka(satuan - 10) + " Belas";
        else if (satuan < 100)
          return (
            $scope.angka(satuan / 10) + " Puluh " + $scope.angka(satuan % 10)
          );
        else if (satuan < 200) return "Seratus " + $scope.angka(satuan - 100);
        else if (satuan < 1000)
          return (
            $scope.angka(satuan / 100) + " Ratus " + $scope.angka(satuan % 100)
          );
        else if (satuan < 2000) return "Seribu " + $scope.angka(satuan - 1000);
        else if (satuan < 1000000)
          return (
            $scope.angka(satuan / 1000) + " Ribu " + $scope.angka(satuan % 1000)
          );
        else if (satuan < 1000000000)
          return (
            $scope.angka(satuan / 1000000) +
            " Juta " +
            $scope.angka(satuan % 1000000)
          );
        // else if(satuan>=1000000000)
        // $scope.hasil="Angka terlalu besar, harus kurang dari 1 milyar!";
        return "";
      };

      //----------------------------------
      // Grid Setup
      //----------------------------------
      $scope.coldef = [];
      if ($scope.user.RoleName == "CAO/HO") {
        $scope.multiselects = true;
        console.log("kesini cao ini mah", $scope.multiselects);
        $scope.coldef = [
          // { name: 'Id', field: 'Id', }
          { name: "Cabang", field: "OutletName", width: "20%" },
          { name: "no rangka", field: "FrameNo", width: "18%" },
          { name: "nama pelanggan", field: "NamaPelanggan", width: "15%" },
          { name: "tipe model kendaraan", field: "Description", width: "25%" },
          { name: "status", field: "StatusName", width: "15%" },
          {
            name: "action",
            //allowCellFocus: false,
            width: "7%",
            //pinnedRight: true,
            enableColumnMenu: false,
            enableSorting: false,
            enableColumnResizing: true,
            cellTemplate:
              '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat AFI" tooltip-placement="bottom" ng-hide="row.entity.TombolDetail == false" onclick="this.blur()" ng-click="grid.appScope.$parent.liahAfi(row.entity)" tabindex="0"> ' +
              '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
              "</a>",
          },
        ];
      } else {
        $scope.multiselects = false;
        console.log("harusnya kesini", $scope.multiselects);
        $scope.coldef = [
          // { name: 'Id', field: 'Id', }
          {
            name: "no spk",
            displayName: "No. SPK",
            field: "FormSPKNo",
            width: "15%",
          },
          {
            name: "no so",
            displayName: "No. SO",
            field: "SoCode",
            width: "15%",
          },
          { name: "no rangka", field: "FrameNo", width: "15%" },
          { name: "nama pelanggan", field: "NamaPelanggan", width: "15%" },
          { name: "tipe model kendaraan", field: "Description", width: "15%" },
          { name: "status", field: "StatusName", width: "15%" },
          {
            name: "action",
            allowCellFocus: false,
            width: "10%",
            //pinnedRight: true,
            enableColumnMenu: false,
            enableSorting: false,
            enableColumnResizing: true,
            cellTemplate:
              '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat AFI" tooltip-placement="right" ng-hide="row.entity.TombolDetail == false" onclick="this.blur()" ng-click="grid.appScope.$parent.liahAfi(row.entity)" tabindex="0"> ' +
              '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
              "</a>" +
              '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Revisi AFI" tooltip-placement="bottom" onclick="this.blur()" ng-hide="row.entity.TombolRevisi == false" ng-click="grid.appScope.$parent.revisiAfi(row.entity)" tabindex="0"> ' +
              '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
              "</a>" +
              '<a  href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Batal AFI" tooltip-placement="bottom" onclick="this.blur()" ng-hide="row.entity.TombolBatal == false" ng-click="grid.appScope.$parent.batalAfi(row.entity)" tabindex="0"> ' +
              '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
              "</a>",
          },
        ];
      }

      $scope.allowPatternName = function (event, type, item) {
        console.log("event", event);
        var patternRegex;
        if (type == 1) {
          patternRegex = /[a-zA-Z]|[0-9]|[ ]|[,./-]/i; //NUMERIC ONLY
        } else if (type == 2) {
          patternRegex = /[a-zA-Z]|[0-9]|[ ]|[''",.()-]|[&]/i; //ALPHANUMERIC ONLY
          // if (item.includes("*")) {
          //     event.preventDefault();
          //     return false;
          // }
        } else if (type == 3) {
          patternRegex = /\d|[a-z]/i;
        }
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
          event.preventDefault();
          return false;
        }
      };

      $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: $scope.user.RoleName == "CAO/HO" ? true : false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: $scope.coldef,
      };
    }
  );
