angular.module('app')
    .factory('MaintainAfiFactory', function($httpParamSerializer, $http, CurrentUser) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    '00';
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/AFIMaintain?start=1&limit=10000000&' + param);
                return res;
            },
            getDataNew: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/AFIMaintainNew?start=1&limit=1000&' + param);
                return res;
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },
            getPStatusAfi: function() {
                var res = $http.get('/api/sales/PStatusAFI');
                return res;
            },
            getRegionCode: function(Id,ProvinceId) {
                var res = $http.get('/api/sales/AFIRegion/?Id=0&ProvinceId='+ProvinceId+'&CityRegencyId='+Id);
                return res;
            },
            NewgetRegionCode: function(Id,ProvinceId, DistrictId, VillageId ) {
                var res = $http.get('/api/sales/AFIRegion/?Id=0&ProvinceId='+ProvinceId+'&CityRegencyId='+Id+'&DistrictId='+DistrictId+'&VillageId='+VillageId);
                return res;
            },
            getCarType: function() {
                var res = $http.get('/api/sales/AFICarType');
                return res;
            },
            getRevisionAfiCategory: function() {
                var res = $http.get('/api/sales/PRevisionAFI');
                return res;
            },
            ajuAfiTAM: function(data) {
                // console.log("", data)
                return $http.put('/api/sales/AjuAFIKeTAM/Update', data);
            },
            create: function(dataAfi) {
                dataAfi.InvoiceStartEffectiveDate = fixDate(dataAfi.InvoiceStartEffectiveDate);
                //console.log("TEST", dataAfi);
                return $http.post('/api/sales/AFIMaintain/Insert', [dataAfi]);
            },
            revisiAfi: function(dataAfi) {
                dataAfi.InvoiceStartEffectiveDate = fixDate(dataAfi.InvoiceStartEffectiveDate);
                return $http.put('/api/sales/RevisiAFI', [dataAfi]);
            },
            batalAfi: function(dataAfi) {
                return $http.put('/api/sales/BatalAFI', [dataAfi]);
            }
        }
    });