angular.module('app')
    .controller('TerimaSTNKBirojasaController', function($scope, $http, CurrentUser, TerimaSTNKBirojasaFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            TerimaSTNKBirojasaFactory.getOutletCabang().then(function(res) {
                $scope.outletCabang = res.data.Result;
            });

            TerimaSTNKBirojasaFactory.getVendor().then(function(res) {
                $scope.vendor = res.data.Result;
            });

        });
		
		TerimaSTNKBirojasaFactory.getPStatusAfi().then(function(res) {
                var yang_belom_di_filter = res.data.Result;
				
				var yang_uda_di_filter = yang_belom_di_filter.filter(function(semprul) {
					return (semprul.StatusName == "Proses Biro Jasa" || semprul.StatusName == "Terima STNK Dari Biro Jasa" || semprul.StatusName == "Terima Notice Dari Biro Jasa" || semprul.StatusName == "Terima No Polisi Dari Biro Jasa" || semprul.StatusName == "Terima Lengkap Dari Biro Jasa");
				})
				
				$scope.OptionsStatusafiTerimaStnkDariBiroJasa = yang_uda_di_filter;
            });
		
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mTerimaSTNKBirojasa = null; //Model
        $scope.daftarPO = true;
        //$scope.filter = { Id: 0, OutletId: null, POBiroJasaUnitNo: null, FrameNo: null, CustomerName: null, ServiceBureauId: null, JadwalPenerimaan: null, StatusAFIId: null };
        $scope.filter = { OutletId: null, POBiroJasaUnitNo: null, FrameNo: null, NamaPemilik: null, ServiceBureauId: null, TanggalPenerimaan: null, StatusAFIId: null };
		$scope.dateOption ={
            format:'dd-MM-yyyy'
        }
        var currDate = new Date();
        $scope.filter.TanggalPenerimaan = currDate;

        //----------------------------------s
        // Get Data
        //----------------------------------

        $scope.testing = function(){
            console.log("valid", $scope.validSTNKBirojasa);
        }
		
		$scope.TerimaSTNKBirojasaTerimaLengkap = function() {
			TerimaSTNKBirojasaFactory.TerimaLengkap($scope.TerimaStnkDariBiroJasaYangDipilih).then(function() {
                TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil Terima Lengkap",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
		}
		
		$scope.TerimaSTNKBirojasaTerimaStnk = function() {
			TerimaSTNKBirojasaFactory.TerimaSTNK($scope.TerimaStnkDariBiroJasaYangDipilih).then(function() {
                TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil Terima STNK",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
		}
		
		$scope.TerimaSTNKBirojasaTerimaNotice = function() {
			TerimaSTNKBirojasaFactory.TerimaNotice($scope.TerimaStnkDariBiroJasaYangDipilih).then(function() {
                TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil Terima Notice",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
		}
		
		$scope.TerimaSTNKBirojasaTerimaPlat = function() {
			TerimaSTNKBirojasaFactory.TerimaPlat($scope.TerimaStnkDariBiroJasaYangDipilih).then(function() {
                TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil Terima Plat",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
		}
		
		$scope.TerimaSTNKBirojasaBatalTerima = function() {
			TerimaSTNKBirojasaFactory.BatalTerima($scope.TerimaStnkDariBiroJasaYangDipilih).then(function() {
                TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil Batal Terima",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
		}

        $scope.terimaSTNK = function(SelectedTerimaSTNK) {

            SetReceiveSTNKDateMinDate(SelectedTerimaSTNK.POBiroJasaUnitDate); //ini kode buat logika tanggal ga bole mundur
            $scope.rowsDocument = angular.copy(SelectedTerimaSTNK);


            $scope.CentangSTNK();
            $scope.CentangPoliceNumber();
            $scope.CentangNoticePajak();

            setTimeout(function() {
                angular.element('.ui.modal.terimaSTNK').modal('refresh');
            }, 0);
            angular.element('.ui.modal.terimaSTNK').modal('setting', { closable: false }).modal('show');
            angular.element('.ui.modal.terimaSTNK').not(':first').remove();
        }

        $scope.CentangSTNK = function() {
            try {
                if ($scope.rowsDocument.STNKNo.length > 0) {
                    $scope.NoSTNKCekChanged = true;
                }
            } catch (error) {
                $scope.NoSTNKCekChanged = false;
            }

        }

        $scope.CentangPoliceNumber = function() {

            try {
                if ($scope.rowsDocument.PoliceNumber.length > 0) {
                    $scope.PoliceNumberCekChanged = true;
                }
            } catch (error) {
                $scope.PoliceNumberCekChanged = false;
            }
        }

        $scope.CentangNoticePajak = function() {

            try {

                if ($scope.rowsDocument['BBNKB'].toString().length > 0 || $scope.rowsDocument['PKB'].toString().length > 0 || $scope.rowsDocument['SWDKLLJ'].toString().length > 0 || $scope.rowsDocument['BiayaAdmSTNK'].toString().length > 0 || $scope.rowsDocument['BiayaAdmTNKB'].toString().length > 0) {
                    $scope.NoticePajakCekChanged = true;
                }
            } catch (error) {
                $scope.NoticePajakCekChanged = false;
            }
        }

        $scope.simpanTerimaSTNK = function() {
            $scope.The_SelectedTerimaSTNK = angular.copy($scope.rowsDocument);
            $scope.mtd = { Method: "Terima" };
            $scope.mTerimaSTNK = angular.merge({},$scope.The_SelectedTerimaSTNK,$scope.mtd);

            TerimaSTNKBirojasaFactory.create($scope.mTerimaSTNK).then(function() {
                //nanti di get
                TerimaSTNKBirojasaFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Success",
                            content: "Berhasil terima STNK",
                            type: 'success'
                        });
                        return res.data;
                    },
                    function(err) {
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                        // console.log("err=>", err);
                    }
                ).then(function() {
                    $scope.backFromterimaSTNK();
                });

            });

        }

        $scope.backFromterimaSTNK = function() {
            $scope.rowsDocument = null;
            angular.element('.ui.modal.terimaSTNK').modal('hide');
        }

        $scope.alasanterlambat = function(SelectedDataTerlambat) {
            $scope.backFromAlasanRevisi();
            $scope.The_SelectedDataTerlambat = angular.copy(SelectedDataTerlambat);
            $scope.SelectedDataTerlambat_FrameNo = angular.copy(SelectedDataTerlambat.FrameNo);
            $scope.SelectedDataTerlambat_POBiroJasaUnitNo = angular.copy(SelectedDataTerlambat.POBiroJasaUnitNo);
            $scope.SelectedDataTerlambat_Model = angular.copy(SelectedDataTerlambat.VehicleModelName);
            $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK = angular.copy(SelectedDataTerlambat.AlasanKeterlambatanSTNK);
            $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = angular.copy(SelectedDataTerlambat.LamaKeterlambatanSTNK);
            angular.element('.ui.modal.alasanterlambatSTNK').modal('setting', { closable: false }).modal('show');
        }

        $scope.simpanAlasanRevisi = function() {
            $scope.The_SelectedDataTerlambat['AlasanKeterlambatanSTNK'] = angular.copy($scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK);
            $scope.The_SelectedDataTerlambat['LamaKeterlambatanSTNK'] = angular.copy($scope.SelectedDataTerlambat_LamaKeterlambatanSTNK);
            $scope.mtd = { Method: "Delay" };
            $scope.mSTNKMethod = angular.merge({},$scope.The_SelectedDataTerlambat,$scope.mtd);

            TerimaSTNKBirojasaFactory.create($scope.mSTNKMethod).then(function() {
                //nanti di get
                TerimaSTNKBirojasaFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil memperbarui alasan",
                            type: 'success'
                        });
                        $scope.backFromAlasanRevisi();
                        return res.data;
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Pesan Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                )
            });
        }

        $scope.backFromAlasanRevisi = function() {
            $scope.SelectedDataTerlambat_FrameNo = null;
            $scope.SelectedDataTerlambat_AlasanKeterlambatanSTNK = null;
            $scope.SelectedDataTerlambat_LamaKeterlambatanSTNK = null;
            angular.element('.ui.modal.alasanterlambatSTNK').modal('hide');
        }

        $scope.batalRevisiSTNK = function (){
            angular.element('.ui.modal.alasanrevisiSTNK').modal('hide');
        }

        $scope.revisiStnk = function(SelectedRevisiStnk) {
            $scope.The_SelectedRevisiSTNK = angular.copy(SelectedRevisiStnk);
            $scope.SelectedDataRevisi = angular.copy(SelectedRevisiStnk);
            // $scope.SelectedDataRevisi.FrameNo = angular.copy(SelectedDataTerlambat.FrameNo);
            // $scope.SelectedDataRevisi.POBiroJasaUnitNo = angular.copy(SelectedDataTerlambat.POBiroJasaUnitNo);
            // $scope.SelectedDataRevisi.VehicleModelName = angular.copy(SelectedDataTerlambat.VehicleModelName);
            // $scope.SelectedDataRevisi.AlasanRevisiSTNK = angular.copy(SelectedDataTerlambat.AlasanRevisiSTNK);
            // $scope.SelectedDataRevisi.LamaKeterlambatanSTNK = angular.copy(SelectedDataTerlambat.LamaKeterlambatanSTNK);
            angular.element('.ui.modal.revisiStnk').modal('setting', { closable: false }).modal('show');
        }

        $scope.openSaveRevisiSTNK = function (){
            angular.element('.ui.modal.revisiStnk').modal('setting', { closable: false }).modal('hide');
            angular.element('.ui.modal.alasanrevisiSTNK').modal('setting', { closable: false }).modal('show');
        }

        $scope.SaveRevisiSTNK = function() {
            $scope.The_SelectedRevisiSTNK['AlasanRevisiSTNK'] = angular.copy($scope.SelectedDataRevisi.AlasanRevisiSTNK);
            $scope.The_SelectedRevisiSTNK['LamaKeterlambatanSTNK'] = angular.copy($scope.SelectedDataRevisi.LamaKeterlambatanSTNK);
            $scope.The_SelectedRevisiSTNK['RevisiSTNKBit'] = angular.copy(true);
            $scope.mtd = {Method: "Revisi"};
            $scope.mRevisiSTNK = angular.merge({},$scope.The_SelectedRevisiSTNK,$scope.mtd); 

            TerimaSTNKBirojasaFactory.create($scope.mRevisiSTNK).then(function() {
                //nanti di get
                TerimaSTNKBirojasaFactory.getData().then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        angular.element('.ui.modal.alasanrevisiSTNK').modal('setting', { closable: false }).modal('hide');
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Berhasil revisi STNK",
                            type: 'success'
                        });
                        return res.data;
                        
                    },
                    function(err) {
                        // console.log("err=>", err);
                        bsNotify.show({
                            title: "Error",
                            content: "Terjadi Error : "+err.data.Message,
                            type: 'danger'
                        });
                    }
                ).then(function() {
                    $scope.backFromRevisiSTNK();
                });

            });
        }

        $scope.backFromRevisiSTNK = function() {
            angular.element('.ui.modal.revisiStnk').modal('hide');
        }

        var gridData = [];
        $scope.getData = function() {
            TerimaSTNKBirojasaFactory.getData($scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        // var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" ng-if="row.entity.TombolDelay == true" uib-tooltip="Alasan Delay STNK" ng-click="grid.appScope.$parent.alasanterlambat(row.entity)">Alasan Delay</u>&nbsp' +
        //     '|&nbsp<u tooltip-placement="bottom" ng-if="row.entity.TombolRevisi == true" uib-tooltip="Revisi STNK" ng-click="grid.appScope.$parent.revisiStnk(row.entity)">Revisi</u>&nbsp' +
        //     '|&nbsp<u tooltip-placement="bottom" ng-if="row.entity.TombolTerima == true" uib-tooltip="Terima STNK" ng-click="grid.appScope.$parent.terimaSTNK(row.entity)">Terima</u>&nbsp' +
        //     '</p>' +
        //     '</a>';
        
        var actionbutton = ''+
        '<a href="" style="color:#777;" ng-show="row.entity.TombolDelay == true" class="trlink ng-scope" uib-tooltip="Alasan Delay STNK" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.alasanterlambat(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-calendar-plus-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a href="" style="color:#777;" ng-show="row.entity.TombolRevisi == true" class="trlink ng-scope" uib-tooltip="Revisi STNK" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.revisiStnk(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +

        '<a  href="" style="color:#777;" ng-show="row.entity.TombolTerima == true" class="trlink ng-scope" uib-tooltip="Terima STNK" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.terimaSTNK(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-handshake-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>';

        var modalStatus = ''+
        '<div style="vertical-align:middle;">'+
        '<p style="padding:5px 0 0 3px;" ng-if="row.entity.STNKStatusName == \'STNK Delay dari Biro Jasa\'"><a href="" style="color:blue;" ng-click="grid.appScope.$parent.alasanterlambat(row.entity)">{{row.entity.STNKStatusName}}</a></p>' +
        '<p style="padding:5px 0 0 3px;" ng-if="row.entity.STNKStatusName != \'STNK Delay dari Biro Jasa\'">{{row.entity.STNKStatusName}}</p>'+
        '</div>'
		
		//var centangan_STNK='<div style="vertical-align:middle;"><i ng-if="row.entity.TerimaSTNKBit==true" class="fa fa-check-square" aria-hidden="true"></i><i ng-if="row.entity.TerimaSTNKBit==false" class="fa fa-square" aria-hidden="true"></i></div>';
		var centangan_STNK =''+
        '<div class="col-xs-1" align="center" style="margin-top: 8px"> ' +
        
        '<span><i ng-class="{\'fa fa-square-o\' : row.entity.TerimaSTNKBit == false || !toogle, \'fa fa-check-square\' : row.entity.TerimaSTNKBit == true || toogle}" style="font-size: 15px" aria-hidden="true"></i> ' +
        '</div>';
		//var centangan_PlatNoPol='<div style="vertical-align:middle;"><i ng-if="row.entity.TerimaPlatNoPolBit==true" class="fa fa-check-square" aria-hidden="true"></i><i ng-if="row.entity.TerimaPlatNoPolBit==false" class="fa fa-square" aria-hidden="true"></i></div>';
		var centangan_PlatNoPol =''+
        '<div class="col-xs-1" align="center" style="margin-top: 8px"> ' +
        
        '<span><i ng-class="{\'fa fa-square-o\' : row.entity.TerimaPlatNoPolBit == false || !toogle, \'fa fa-check-square\' : row.entity.TerimaPlatNoPolBit == true || toogle}" style="font-size: 15px" aria-hidden="true"></i> ' +
        '</div>';
		//var centangan_NoticePajak='<div style="vertical-align:middle;"><i ng-if="row.entity.TerimaNoticePajakBit==true" class="fa fa-check-square" aria-hidden="true"></i><i ng-if="row.entity.TerimaNoticePajakBit==false" class="fa fa-square" aria-hidden="true"></i></div>';
		var centangan_NoticePajak =''+
        '<div class="col-xs-1" align="center" style="margin-top: 8px"> ' +
        
        '<span><i ng-class="{\'fa fa-square-o\' : row.entity.TerimaNoticePajakBit == false || !toogle, \'fa fa-check-square\' : row.entity.TerimaNoticePajakBit == true || toogle}" style="font-size: 15px" aria-hidden="true"></i> ' +
        '</div>';
		
		
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
			$scope.TerimaStnkDariBiroJasaYangDipilih=angular.copy(rows);
			
			$scope.TerimaStnkDariBiroJasaBatalAman=true;
			for(var i = 0; i < $scope.TerimaStnkDariBiroJasaYangDipilih.length; ++i)
			{	
				if($scope.TerimaStnkDariBiroJasaYangDipilih[i].TombolBatalTerima == false)
				{
					$scope.TerimaStnkDariBiroJasaBatalAman=false;
					break;
				}
			}
			
			
			console.log("iblis", $scope.TerimaStnkDariBiroJasaYangDipilih);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.test = function() {
            console.log("test", $scope.gridNoRangkaDocument.data);
            for (var i in $scope.gridNoRangkaDocument.data) {
                $scope.gridNoRangkaDocument.data[i].BiroJasa = $scope.birojasa;
            }
        }

        // $scope.grid = {
            // enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            // //showTreeExpandNoChildren: true,
            // // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // // paginationPageSize: 15,
            // columnDefs: [
                // // { name: 'Id', field: 'Id', visible: false},
                // { displayName: 'No PO', name: 'no po', field: 'POBiroJasaUnitNo' },
                // { name: 'Vendor', field: 'ServiceBureauName' },
                // { name: 'cabang', field: 'OutletRequestPOName' },
                // { name: 'no rangka', field: 'FrameNo' },
                // { name: 'nama pelanggan', field: 'CustomerName' },
                // { name: 'model kendaraan', field: 'VehicleModelName' },
                // { name: 'Perkiraan Tgl Terima', field: 'JadwalPenerimaan', width: '10%',
                    // cellFilter: 'date:\'dd-MM-yyyy\'' },
                // // { name: 'Status', field: 'STNKStatusName'},
                // {
                    // name: 'Status',
                    // allowCellFocus: false,
                    // width: '15%',
                    // enableColumnMenu: false,
                    // enableSorting: false,
                    // enableColumnResizing: true,
                    // cellTemplate: modalStatus
                // },
                // {
                    // name: 'action',
                    // allowCellFocus: false,
                    // width: '12%',
                   // // pinnedRight: true,
                    // enableColumnMenu: false,
                    // enableSorting: false,
                    // enableColumnResizing: true,
                    // cellTemplate: actionbutton
                // }

            // ]
        // };
		
		$scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', visible: false},
                { displayName: 'No. PO', name: 'no po', field: 'POBiroJasaUnitNo' },
                { name: 'Nama Pemilik', field: 'NamaPemilik' },
				{ name: 'No. rangka', field: 'FrameNo' },
				{ name: 'Vendor', field: 'ServiceBureauName' },
                { name: 'Cabang', field: 'OutletRequestPOName' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
				{ displayName: 'STNK',name: 'STNK', field: 'TerimaSTNKBit' ,cellTemplate: centangan_STNK},
				{ name: 'Plat No Pol', field: 'TerimaPlatNoPolBit' ,cellTemplate: centangan_PlatNoPol},
				{ name: 'Notice Pajak', field: 'TerimaNoticePajakBit' ,cellTemplate: centangan_NoticePajak},
            ]
        };


    });