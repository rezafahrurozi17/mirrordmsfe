angular.module('app')
    .factory('TerimaSTNKBirojasaFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());

                return fix;
            } else {
                return null;
            }

        };

        return {
            getData: function(filter) {
                // filter.JadwalPenerimaan = fixDate(filter.JadwalPenerimaan);
                var param = $httpParamSerializer(filter);
                //var res = $http.get('/api/sales/ReceiveSTNK?start=1&limit=1000&' + param);
				var res = ''
				if(filter.OutletId==null && filter.POBiroJasaUnitNo==null && filter.FrameNo==null && filter.NamaPemilik==null && filter.ServiceBureauId==null && filter.TanggalPenerimaan==null && filter.StatusAFIId==null)
				{
					res = $http.get('/api/sales/TerimaSTNKDariBiroJasa/GetData?start=1&limit=1000000' + param);
				}
				else
				{
					res = $http.get('/api/sales/TerimaSTNKDariBiroJasa/GetData?start=1&limit=1000000&' + param);
				}
				
                return res;
            },
            getOutletCabang: function() {
                var res = $http.get('/api/sales/MSitesOutletCAO');
                return res;
            },
			getPStatusAfi: function() {
                var res = $http.get('/api/sales/PStatusAFI');
                return res;
            },
            getVendor: function() {
                var res = $http.get('/api/sales/MProfileBiroJasa');
                return res;
            },
			
			TerimaLengkap: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa/TerimaLengkap',TerimaSTNKBirojasa );
            },
			
			TerimaSTNK: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa/TerimaSTNK',TerimaSTNKBirojasa );
            },
			
			TerimaNotice: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa/TerimaNotice',TerimaSTNKBirojasa );
            },
			
			TerimaPlat: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa/TerimaPlat',TerimaSTNKBirojasa );
            },
			
			BatalTerima: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa/BatalTerima',TerimaSTNKBirojasa );
            },
			
            update: function(TerimaSTNKBirojasa) {
                return $http.put('/api/sales/TerimaSTNKDariBiroJasa', [{
                    OutletId: TerimaSTNKBirojasa.OutletId,
                    OutletName: TerimaSTNKBirojasa.OutletName,
                    OutletRequestPOId: TerimaSTNKBirojasa.OutletRequestPOId,
                    OutletRequestPOName: TerimaSTNKBirojasa.OutletRequestPOName,
                    ServiceBureauId: TerimaSTNKBirojasa.ServiceBureauId,
                    ServiceBureauCode: TerimaSTNKBirojasa.ServiceBureauCode,
                    ServiceBureauName: TerimaSTNKBirojasa.ServiceBureauName,
                    POBiroJasaUnitId: TerimaSTNKBirojasa.POBiroJasaUnitId,
                    POBiroJasaUnitNo: TerimaSTNKBirojasa.POBiroJasaUnitNo,
                    POBiroJasaUnitDate: fixDate(POBiroJasaUnitDate),
                    FrameNo: TerimaSTNKBirojasa.FrameNo,
                    CustomerId: TerimaSTNKBirojasa.CustomerId,
                    CustomerName: TerimaSTNKBirojasa.CustomerName,
                    VehicleTypeColorId: TerimaSTNKBirojasa.VehicleTypeColorId,
                    Description: TerimaSTNKBirojasa.Description,
                    KatashikiCode: TerimaSTNKBirojasa.KatashikiCode,
                    SuffixCode: TerimaSTNKBirojasa.SuffixCode,
                    VehicleModelId: TerimaSTNKBirojasa.VehicleModelId,
                    VehicleModelName: TerimaSTNKBirojasa.VehicleModelName,
                    ColorId: TerimaSTNKBirojasa.ColorId,
                    ColorCode: TerimaSTNKBirojasa.ColorCode,
                    ColorName: TerimaSTNKBirojasa.ColorName,
                    ReceiveId: TerimaSTNKBirojasa.ReceiveId,
                    STNKName: TerimaSTNKBirojasa.STNKName,
                    STNKNo: TerimaSTNKBirojasa.STNKNo,
                    PoliceNumber: TerimaSTNKBirojasa.PoliceNumber,
                    Method: TerimaSTNKBirojasa.Method,
                    RequestSTNKDate: fixDate(TerimaSTNKBirojasa.RequestSTNKDate),
                    ReceiveSTNKDate: fixDate(TerimaSTNKBirojasa.ReceiveSTNKDate),
                    BBNKB: TerimaSTNKBirojasa.BBNKB,
                    PKB: TerimaSTNKBirojasa.PKB,
                    SWDKLLJ: TerimaSTNKBirojasa.SWDKLLJ,
                    BiayaAdmSTNK: TerimaSTNKBirojasa.BiayaAdmSTNK,
                    BiayaAdmTNKB: TerimaSTNKBirojasa.BiayaAdmTNKB,
                    RevisiSTNKBit: TerimaSTNKBirojasa.RevisiSTNKBit,
                    AlasanRevisiSTNK: TerimaSTNKBirojasa.AlasanRevisiSTNK,
                    AlasanKeterlambatanSTNK: TerimaSTNKBirojasa.AlasanKeterlambatanSTNK,
                    LamaKeterlambatanSTNK: TerimaSTNKBirojasa.LamaKeterlambatanSTNK,
                    TotalData: TerimaSTNKBirojasa.TotalData
                }]);
            },
            create: function(TerimaSTNKBirojasa) {
                return $http.post('/api/sales/TerimaSTNKDariBiroJasa', [{
                    OutletId: TerimaSTNKBirojasa.OutletId,
                    OutletName: TerimaSTNKBirojasa.OutletName,
                    OutletRequestPOId: TerimaSTNKBirojasa.OutletRequestPOId,
                    OutletRequestPOName: TerimaSTNKBirojasa.OutletRequestPOName,
                    ServiceBureauId: TerimaSTNKBirojasa.ServiceBureauId,
                    ServiceBureauCode: TerimaSTNKBirojasa.ServiceBureauCode,
                    ServiceBureauName: TerimaSTNKBirojasa.ServiceBureauName,
                    POBiroJasaUnitId: TerimaSTNKBirojasa.POBiroJasaUnitId,
                    POBiroJasaUnitNo: TerimaSTNKBirojasa.POBiroJasaUnitNo,
                    POBiroJasaUnitDate: fixDate(TerimaSTNKBirojasa.POBiroJasaUnitDate),
                    FrameNo: TerimaSTNKBirojasa.FrameNo,
                    CustomerId: TerimaSTNKBirojasa.CustomerId,
                    CustomerName: TerimaSTNKBirojasa.CustomerName,
                    VehicleTypeColorId: TerimaSTNKBirojasa.VehicleTypeColorId,
                    Description: TerimaSTNKBirojasa.Description,
                    KatashikiCode: TerimaSTNKBirojasa.KatashikiCode,
                    SuffixCode: TerimaSTNKBirojasa.SuffixCode,
                    VehicleModelId: TerimaSTNKBirojasa.VehicleModelId,
                    VehicleModelName: TerimaSTNKBirojasa.VehicleModelName,
                    ColorId: TerimaSTNKBirojasa.ColorId,
                    ColorCode: TerimaSTNKBirojasa.ColorCode,
                    ColorName: TerimaSTNKBirojasa.ColorName,
                    //ReceiveId: TerimaSTNKBirojasa.ReceiveId,
                    STNKName: TerimaSTNKBirojasa.STNKName,
                    STNKNo: TerimaSTNKBirojasa.STNKNo,
                    PoliceNumber: TerimaSTNKBirojasa.PoliceNumber,
                    Method: TerimaSTNKBirojasa.Method,
                    RequestSTNKDate: fixDate(TerimaSTNKBirojasa.RequestSTNKDate),
                    ReceiveSTNKDate: fixDate(TerimaSTNKBirojasa.ReceiveSTNKDate),
                    BBNKB: TerimaSTNKBirojasa.BBNKB,
                    PKB: TerimaSTNKBirojasa.PKB,
                    SWDKLLJ: TerimaSTNKBirojasa.SWDKLLJ,
                    BiayaAdmSTNK: TerimaSTNKBirojasa.BiayaAdmSTNK,
                    BiayaAdmTNKB: TerimaSTNKBirojasa.BiayaAdmTNKB,
                    RevisiSTNKBit: TerimaSTNKBirojasa.RevisiSTNKBit,
                    AlasanRevisiSTNK: TerimaSTNKBirojasa.AlasanRevisiSTNK,
                    AlasanKeterlambatanSTNK: TerimaSTNKBirojasa.AlasanKeterlambatanSTNK,
                    LamaKeterlambatanSTNK: TerimaSTNKBirojasa.LamaKeterlambatanSTNK,
                    TotalData: TerimaSTNKBirojasa.TotalData
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/ReceiveSTNK', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd