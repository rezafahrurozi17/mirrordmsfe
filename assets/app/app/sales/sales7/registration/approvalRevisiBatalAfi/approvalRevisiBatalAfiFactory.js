angular.module('app')
    .factory('ApprovalRevisiBatalAfiFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user(); //tadinya ini kurung ga ada
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            getData: function() {
                if (currentUser.RoleName == "ADH") {
                    var res = $http.get('/api/sales/ApprovalRevisiBatalAFIADH');
                    return res;
                } else if (currentUser.RoleName == "CAO/HO") {
                    var res = $http.get('/api/sales/ApprovalRevisiBatalAFICAO');
                    return res;
                }


            },

            // create: function(ApprovalRevisiBatalAfi) {
            //     return $http.post('/api/sales/CProspectListApproval', [{
            //         //AppId: 1,
            //         SalesProgramName: ApprovalRevisiBatalAfi.SalesProgramName
            //     }]);
            // },
            // update: function(ApprovalRevisiBatalAfi) {
            //     return $http.put('/api/sales/CProspectListApproval', [{
            //         SalesProgramId: ApprovalRevisiBatalAfi.SalesProgramId,
            //         SalesProgramName: ApprovalRevisiBatalAfi.SalesProgramName
            //     }]);
            // },

            SetujuApprovalRevisiBatalAfi: function(ApprovalRevisiBatalAfi) {
                //console.log("==>OIOIOIOIOI",ApprovalRevisiBatalAfi);
                ApprovalRevisiBatalAfi.BillingDate = fixDate(ApprovalRevisiBatalAfi.BillingDate);
                ApprovalRevisiBatalAfi.InvoiceEndEffectiveDate = fixDate(ApprovalRevisiBatalAfi.InvoiceEndEffectiveDate);
                ApprovalRevisiBatalAfi.InvoiceStartEffectiveDate = fixDate(ApprovalRevisiBatalAfi.InvoiceStartEffectiveDate);

                if (currentUser.RoleName == "ADH") {
                    if (ApprovalRevisiBatalAfi.StatusName == "Request Revisi AFI") {
                        return $http.put('/api/sales/ADHSetujuRevisiAFI', [ApprovalRevisiBatalAfi]);
                    } else if (ApprovalRevisiBatalAfi.StatusName == "Request Batal AFI") {
                        return $http.put('/api/sales/ADHSetujuBatalAFI', [ApprovalRevisiBatalAfi]);
                    }
                } else if (currentUser.RoleName == "CAO/HO") {
                    if (ApprovalRevisiBatalAfi.StatusName == "Approve Revisi By ADH" || ApprovalRevisiBatalAfi.StatusName == "Reject Revisi By ADH") {
                        return $http.put('/api/sales/CAOSetujuRevisiAFI', [ApprovalRevisiBatalAfi]);
                    } else if (ApprovalRevisiBatalAfi.StatusName == "Approve Batal By ADH" || ApprovalRevisiBatalAfi.StatusName == "Reject Batal By ADH") {
                        return $http.put('/api/sales/CAOSetujuBatalAFI', [ApprovalRevisiBatalAfi]);
                    }
                }
            },

            TidakSetujuApprovalRevisiBatalAfi: function(ApprovalRevisiBatalAfi) {
                console.log("==>022020202020",ApprovalRevisiBatalAfi);
                ApprovalRevisiBatalAfi.BillingDate = fixDate(ApprovalRevisiBatalAfi.BillingDate);
                ApprovalRevisiBatalAfi.InvoiceEndEffectiveDate = fixDate(ApprovalRevisiBatalAfi.InvoiceEndEffectiveDate);
                ApprovalRevisiBatalAfi.InvoiceStartEffectiveDate = fixDate(ApprovalRevisiBatalAfi.InvoiceStartEffectiveDate);
                if (currentUser.RoleName == "ADH") {
                    if (ApprovalRevisiBatalAfi.StatusName == "Request Revisi AFI") {
                        return $http.put('/api/sales/ADHTolakRevisiAFI', [ApprovalRevisiBatalAfi]);
                    } else if (ApprovalRevisiBatalAfi.StatusName == "Request Batal AFI") {
                        return $http.put('/api/sales/ADHTolakBatalAFI', [ApprovalRevisiBatalAfi]);
                    }
                } else if (currentUser.RoleName == "CAO/HO") {
                    if (ApprovalRevisiBatalAfi.StatusName == "Approve Revisi By ADH" || ApprovalRevisiBatalAfi.StatusName == "Reject Revisi By ADH") {
                        return $http.put('/api/sales/CAOTolakRevisiAFI', [ApprovalRevisiBatalAfi]);
                    } else if (ApprovalRevisiBatalAfi.StatusName == "Approve Batal By ADH" || ApprovalRevisiBatalAfi.StatusName == "Reject Batal By ADH") {
                        return $http.put('/api/sales/CAOTolakBatalAFI', [ApprovalRevisiBatalAfi]);
                    }
                }
            },

            // delete: function(id) {
            //     return $http.delete('/api/sales/CProspectListApproval', { data: id, headers: { 'Content-Type': 'application/json' } });
            // },
        }
    });
//ddd