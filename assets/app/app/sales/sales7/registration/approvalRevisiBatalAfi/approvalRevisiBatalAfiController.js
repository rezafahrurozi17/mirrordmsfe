angular.module('app')
    .controller('ApprovalRevisiBatalAfiController', function($scope, $http, $filter, bsNotify, CurrentUser, MaintainAfiFactory, ApprovalRevisiBatalAfiFactory, $timeout) {
        $scope.ApprovalRevisiBatalAfiMain = true;
        IfGotProblemWithModal();



        $scope.$on('$viewContentLoaded', function() {
            //$scope.loading=true; tadinya true
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        //$scope.mApprovalRevisiBatalAfi = null; //Model
        $scope.xRole = { selected: [] };

        // TRIGGER WHEN BEFORE GO TO DETAIL FORM
	
	    // $scope.onShowDetail = function(row, mode){
        // console.log(row);
        //     if(mode !=='new'){
        //         MaintainAfiFactory.getRegionCode().then(function(res) { 
        //             $scope.regioncode = res.data.Result 
        //         });
        //     }
        // }
        //----------------------------------
        // Get Data
        //----------------------------------

        // MaintainAfiFactory.getRegionCode().then(function(res) { $scope.regioncode = res.data.Result });

        MaintainAfiFactory.getCarType().then(function(res) { $scope.cartype = res.data.Result });

        var gridData = [];
        $scope.getData = function() {
            ApprovalRevisiBatalAfiFactory.getData().then(
                function(res) {


                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.ActionApprovalRevisiBatalAfi = function(SelectedData) {
            $scope.actionAfi = 'lihatAfi';
            $scope.mApprovalRevisiBatalAfi = angular.copy(SelectedData);
            MaintainAfiFactory.getRegionCode(SelectedData.CityRegencyId,SelectedData.ProvinceId).then(function(res) { 
                            $scope.regioncode = res.data.Result 
                            for (var i = 0; i < res.data.Result.length; i++){
                                $scope.regioncode[i].xPostalCode = $scope.regioncode[i].PostalCode + ' - ' + $scope.regioncode[i].Kota;
                            }
                        });
            
            var tempProv = [{
                ProvinceName: SelectedData.ProvinceName,
                ProvinceId: SelectedData.ProvinceId
            }];
            $scope.province = tempProv;
            $scope.mApprovalRevisiBatalAfi.ProvinceId = SelectedData.ProvinceId;

            var tempKab = [{
                CityRegencyName: SelectedData.CityRegencyName,
                CityRegencyId: SelectedData.CityRegencyId
            }];
            $scope.kabupaten = tempKab;
            $scope.mApprovalRevisiBatalAfi.CityRegencyId = SelectedData.CityRegencyId;

            var tempKec = [{
                DistrictName: SelectedData.DistrictName,
                DistrictId: SelectedData.DistrictId
            }];
            $scope.kecamatan = tempKec;
            $scope.mApprovalRevisiBatalAfi.DistrictId = SelectedData.DistrictId;

            var tempKel = [{
                PostalCode: SelectedData.PostalCode,
                VillageName: SelectedData.VillageName,
                VillageId: SelectedData.VillageId
            }];
            $scope.kelurahan = tempKel;

            $scope.mApprovalRevisiBatalAfi.VillageId = SelectedData.VillageId;


            $scope.SelectApprovalRevisiBatalAfi();
        }



        $scope.SelectApprovalRevisiBatalAfi = function() {
            if ($scope.user.RoleName == "ADH" || $scope.user.RoleName == "CAO/HO") {
                $scope.UserIsCaoHoAdmin = true;
            } else {
                $scope.UserIsCaoHoAdmin = false;
            }
            $scope.ApprovalRevisiBatalAfiMain = false;
            $scope.ApprovalRevisiBatalAfiDetail = true;



        }

        $scope.BatalApprovalRevisiBatalAfi = function() {
            $scope.mApprovalRevisiBatalAfi = {};
            $scope.ApprovalRevisiBatalAfiMain = true;
            $scope.ApprovalRevisiBatalAfiDetail = false;
        }

        $scope.TidakSetujuApprovalRevisiBatalAfi = function() {
            if ($scope.user.RoleName == "ADH" || $scope.user.RoleName == "CAO/HO") {
                angular.element('.ui.modal.ModalAlasanPenolakanApprovalRevisiBatalAfi').modal('setting', { closable: false }).modal('show');
            } else {
                alert('Hak Akses Anda Tidak Cukup!');
            }

        };

        $scope.BatalAlasanApprovalRevisiBatalAfi = function() {
            angular.element('.ui.modal.ModalAlasanPenolakanApprovalRevisiBatalAfi').modal('hide');
            //$scope.mApprovalRevisiBatalAfi={};
        }

        $scope.SetujuApprovalRevisiBatalAfi = function() {

               

            if ($scope.user.RoleName == "ADH" || $scope.user.RoleName == "CAO/HO") {
                ApprovalRevisiBatalAfiFactory.SetujuApprovalRevisiBatalAfi($scope.mApprovalRevisiBatalAfi).then(function(res) {

                    var respond = res.data.ResponseMessage;
                    

                    var lengthstring = res.data.ResponseMessage.length;
                    var count = (respond.match(/#/g) || []).length;
                    var index = respond.indexOf('#');


                    ApprovalRevisiBatalAfiFactory.getData().then(
                        function(res) {



                            $scope.grid.data = res.data.Result;
                            $scope.loading = false;
                            $scope.BatalApprovalRevisiBatalAfi();

                            bsNotify.show({
                                title: "Berhasil",
                                // content: "Revisi Butuh Approval.",
                                content: respond.slice(index +1 , lengthstring),
                                type: 'success'
                            });

                            return res.data;
                        },
                        function(err) {
                            console.log("err=>", err);
                            bsNotify.show({
                                title: "Gagal",
                                // content: "Revisi Gagal Disimpan.",
                                content: respond.slice(index +1 , lengthstring),
                                type: 'danger'
                            });
                        }
                    );
                });
            } else {
                alert('Hak Akses Anda Tidak Cukup!');
            }

        };

        $scope.SubmitAlasanApprovalRevisiBatalAfi = function() {
                
            ApprovalRevisiBatalAfiFactory.TidakSetujuApprovalRevisiBatalAfi($scope.mApprovalRevisiBatalAfi).then(function(res) {
                var respondtolak = res.data.ResponseMessage;
                console.log('respond',respondtolak);
                // console.log('respond',respondxxxxx);
                var lengthstring = res.data.ResponseMessage.length;
                var count = (respondtolak.match(/#/g) || []).length;
                var index = respondtolak.indexOf('#');

                ApprovalRevisiBatalAfiFactory.getData().then(
                    function(res) {

                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        $scope.BatalAlasanApprovalRevisiBatalAfi();
                        $scope.BatalApprovalRevisiBatalAfi();

                        bsNotify.show({
                            title: "Berhasil",
                            // content: "Batal AFI Butuh Approval.",
                            content: respondtolak.slice(index +1 , lengthstring),
                            type: 'success'
                        });

                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Gagal",
                            // content: "Batal AFI Gagal.",
                            content: respondtolak.slice(index +1 , lengthstring),
                            type: 'danger'
                        });
                    }
                );
            });

        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'No Rangka', field: 'FrameNo', visible: true },
                { name: 'Nama Pelanggan', field: 'NamaPelanggan' },
                { name: 'Model Kendaraan', field: 'VehicleModelName' },
                { name: 'Status', field: 'StatusName' },
                {
                    name: 'Action',
                    width: '10%',
                    cellTemplate: '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat AFI" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.ActionApprovalRevisiBatalAfi(row.entity)" tabindex="0"> ' +
                        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                        '</a>'
                },
            ]
        };





    });

