angular.module('app')
    .factory('MaintainSuratDistribusiSTNKFactory', function($httpParamSerializer, $http, CurrentUser) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            getData: function(filter) {
                // if (filter.StartSentDate != null) {
                //     filter.StartSentDate = filter.StartSentDate.getFullYear() + '-' +
                //         ('0' + (filter.StartSentDate.getMonth() + 1)).slice(-2) + '-' +
                //         ('0' + filter.StartSentDate.getDate()).slice(-2) + 'T' +
                //         ((filter.StartSentDate.getHours() < '10' ? '0' : '') + filter.StartSentDate.getHours()) + ':' +
                //         ((filter.StartSentDate.getMinutes() < '10' ? '0' : '') + filter.StartSentDate.getMinutes()) + ':' +
                //         ((filter.StartSentDate.getSeconds() < '10' ? '0' : '') + filter.StartSentDate.getSeconds());
                // };

                // if (filter.EndSentDate != null) {
                //     filter.EndSentDate = filter.EndSentDate.getFullYear() + '-' +
                //         ('0' + (filter.EndSentDate.getMonth() + 1)).slice(-2) + '-' +
                //         ('0' + filter.EndSentDate.getDate()).slice(-2) + 'T' +
                //         ((filter.EndSentDate.getHours() < '10' ? '0' : '') + filter.EndSentDate.getHours()) + ':' +
                //         ((filter.EndSentDate.getMinutes() < '10' ? '0' : '') + filter.EndSentDate.getMinutes()) + ':' +
                //         ((filter.EndSentDate.getSeconds() < '10' ? '0' : '') + filter.EndSentDate.getSeconds());
                // };
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/SAHSuratDistribusiSTNK?start=1&limit=1000&' + param);
                return res;
            },

            getDataToCetak: function(SuratDistribusiNo) {
                var res = $http.get('/api/sales/RegistrationCetakSuratDistribusiSTNK/?start=1&limit=100&filterData=SuratDistribusiNo|' + SuratDistribusiNo);
                return res;
            },

            // getOutlet: function() {
            //     var res=$http.get('api/sales/ListCabangCAO');
            //     return res;
            // },

            getPilihDataForInsert: function(param) {
                if (param == undefined || param == null || param == "") {
                    param = "";
                } else {
                    param = "&" + param;
                }
                var res = $http.get('/api/sales/STNKSelesaiDariBiroJasa/?start=1&limit=1000000'+param);
                return res;
            },

            getOutlet: function(){
                var res = $http.get('/api/sales/ListCabangCAO');
                return res;
            },

            create: function(MaintainSuratDistribusiStnk) {
                for(var i in MaintainSuratDistribusiStnk.listFrameNo){
                    for(var j in MaintainSuratDistribusiStnk.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate);
                    }
                }
                var res = $http.post('/api/sales/SAHSuratDistribusiSTNK/Insert', [{
                    OutletId: MaintainSuratDistribusiStnk.OutletId,
                    OutletName: MaintainSuratDistribusiStnk.OutletName,
                    SuratDistribusiSTNKNo: MaintainSuratDistribusiStnk.SuratDistribusiSTNKNo,
                    SuratDistribusiSTNKSentDate: MaintainSuratDistribusiStnk.SuratDistribusiSTNKSentDate,
                    JumlahDokumen: MaintainSuratDistribusiStnk.JumlahDokumen,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiStnk.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiStnk.StatusCode,
                    listFrameNo: MaintainSuratDistribusiStnk.listFrameNo,
                    LastModifiedDate: null,
                    LastModifiedUserId: null,
                }]);
                return res;
            },

            update: function(MaintainSuratDistribusiStnk) {
                for(var i in MaintainSuratDistribusiStnk.listFrameNo){
                    for(var j in MaintainSuratDistribusiStnk.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate);
                    }
                }
                return $http.put('/api/sales/SAHSuratDistribusiSTNK/Update', [{
                    OutletCAOId: MaintainSuratDistribusiStnk.OutletCAOId,
                    OutletCAOName: MaintainSuratDistribusiStnk.OutletCAOName,
                    OutletId: MaintainSuratDistribusiStnk.OutletId,
                    OutletName: MaintainSuratDistribusiStnk.OutletName,
                    SuratDistribusiSTNKHeaderId: MaintainSuratDistribusiStnk.SuratDistribusiSTNKHeaderId,
                    SuratDistribusiSTNKNo: MaintainSuratDistribusiStnk.SuratDistribusiSTNKNo,
                    SuratDistribusiSTNKSentDate: MaintainSuratDistribusiStnk.SuratDistribusiSTNKSentDate,
                    JumlahDokumen: MaintainSuratDistribusiStnk.JumlahDokumen,
                    PrintCount: MaintainSuratDistribusiStnk.PrintCount,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiStnk.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiStnk.StatusCode,
                    TotalData: MaintainSuratDistribusiStnk.TotalData,
                    listFrameNo: MaintainSuratDistribusiStnk.listFrameNo,
                    LastModifiedDate: MaintainSuratDistribusiStnk.LastModifiedDate,
                    LastModifiedUserId: MaintainSuratDistribusiStnk.LastModifiedUserId,
                }]);
            },

            Kirim: function(MaintainSuratDistribusiStnk) {
                for(var i in MaintainSuratDistribusiStnk.listFrameNo){
                    for(var j in MaintainSuratDistribusiStnk.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].STNKSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].PoliceNumberSendDate);
                        MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate = fixDate(MaintainSuratDistribusiStnk.listFrameNo[i].listDetail[j].NoticePajakSendDate);
                    }
                }
                return $http.put('/api/sales/SAHSuratDistribusiSTNK/Kirim', [{
                    OutletCAOId: MaintainSuratDistribusiStnk.OutletCAOId,
                    OutletCAOName: MaintainSuratDistribusiStnk.OutletCAOName,
                    OutletId: MaintainSuratDistribusiStnk.OutletId,
                    OutletName: MaintainSuratDistribusiStnk.OutletName,
                    SuratDistribusiSTNKHeaderId: MaintainSuratDistribusiStnk.SuratDistribusiSTNKHeaderId,
                    SuratDistribusiSTNKNo: MaintainSuratDistribusiStnk.SuratDistribusiSTNKNo,
                    SuratDistribusiSTNKSentDate: MaintainSuratDistribusiStnk.SuratDistribusiSTNKSentDate,
                    JumlahDokumen: MaintainSuratDistribusiStnk.JumlahDokumen,
                    PrintCount: MaintainSuratDistribusiStnk.PrintCount,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiStnk.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiStnk.StatusCode,
                    TotalData: MaintainSuratDistribusiStnk.TotalData,
                    listFrameNo: MaintainSuratDistribusiStnk.listFrameNo,
                    LastModifiedDate: MaintainSuratDistribusiStnk.LastModifiedDate,
                    LastModifiedUserId: MaintainSuratDistribusiStnk.LastModifiedUserId,
                    //pid: role.pid,
                }]);
            },

            delete: function(id) {
                return $http.delete('/api/sales/SAHSuratDistribusiSTNK', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });