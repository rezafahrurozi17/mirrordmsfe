angular.module('app')
    .controller('MaintainSuratDistribusiSTNKController', function($httpParamSerializer, $scope, $http, CurrentUser, MaintainSuratDistribusiSTNKFactory, $timeout, bsNotify) {
        //angular.element('.ui.modal.UpdatePenerimaanDokumen').remove();
        // IfGotProblemWithModal();

        $scope.ShowMaintainSuratDistribusiStnkMain = true;
        $scope.DetilSuratDistribusi = "";
        $scope.MaintainSuratDistribusiStnk_Operation = "";
        $scope.UpdatePenerimaanDokumen = { show: false };
        $scope.selctedRow = [];
        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            MaintainSuratDistribusiSTNKFactory.getOutlet().then(
                function(res){
                    $scope.outletList = res.data.Result;
                }
            )
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.status = 'buat';
        $scope.mMaintainSuratDistribusiStnk = null; //Model
        $scope.xRole = { selected: [] };
        $scope.show = {Wizard :false};
        $scope.button = { FrameTab: false, DokumenTab: false };
        $scope.filter = { Id: 0, OutletId: null, SuratDistribusiSTNKNo: null, FrameNo: null, StartSentDate: null, EndSentDate: null };
        $scope.Search = {Id:0, OutletId:null, FrameNo:null};

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.dateOptionsStart = {
            format: 'dd-MM-yyyy'
        };

        $scope.dateOptionsEnd = {
            format: 'dd-MM-yyyy'
        };

        function fixDateJustMinutes(date) {
            if (date != null || date != undefined) {
                var fix = null;
                var year = date.getFullYear();
                var mont = ('0' + (date.getMonth())).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var hour = ((date.getHours() < '10' ? '0' : '') + date.getHours());
                var Minute = ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes());
                
                fix = new Date(year, mont, day, hour, Minute);
                return fix;
            } else {
                return null;
            }
        };

        $scope.tanggalmin = function(date) {
            $scope.dateOptionsEnd.minDate = date;
            if (date == null || date == undefined) {
                $scope.filter.EndSentDate = null;
            } else {
                $scope.filter.EndSentDate = $scope.filter.StartSentDate;
            }
        }

        // MaintainSOFactory.getOutlet().then(
        //     function(res) {
        //         $scope.getOutlet = res.data.Result;
        //         return res.data;
        //     }
        // );

        $scope.SearchFrame = function (){
            var param = $httpParamSerializer($scope.Search);
            if($scope.status == 'buat'){
                $scope.gridPilihFrame.data = [];
                console.log("Buat=>");
                MaintainSuratDistribusiSTNKFactory.getPilihDataForInsert(param).then(
                    function(res) {
                        $scope.gridPilihFrame.data = res.data.Result;
                        $scope.loading = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }else{
                console.log("Ubah=>");
                $scope.gridPilihFrame.data = [];
                MaintainSuratDistribusiSTNKFactory.getPilihDataForInsert(param).then(
                    function(res) {   
                        console.log("cek isi", $scope.ListHapus);
                        if($scope.ListHapus.length != 0 ){
                            for(var i in $scope.ListHapus){
                                $scope.gridPilihFrame.data.push($scope.ListHapus[i]);
                            }
                        };
                        for(var i in res.data.Result){
                            $scope.gridPilihFrame.data.push(res.data.Result[i])
                        };
                         //= res.data.Result;
                        $scope.loading = false;
                        //return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
           
        };

        var gridData = [];
        $scope.getData = function() {
            MaintainSuratDistribusiSTNKFactory.getData($scope.filter).then(
                function(res) {
                    $scope.loading = true;
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    angular.forEach($scope.grid.data, function(data, dataIndx) {
                        angular.forEach(data.listFrameNo, function(frame, frameIndx) {
                            angular.forEach(frame.listDetail, function(detail, detailIndx) {
                                if (detail.STNKSendDate != null || detail.STNKSendDate != undefined) {
                                    detail.STNKSendDateCekChanged = true;
                                } else {
                                    detail.STNKSendDateCekChanged = false;
                                };

                                if (detail.NoticePajakSendDate != null || detail.NoticePajakSendDate != undefined) {
                                    detail.NoticePajakSendDateCekChanged = true;
                                } else {
                                    detail.NoticePajakSendDateCekChanged = false;
                                };

                                if (detail.PoliceNumberSendDate != null || detail.PoliceNumberSendDate != undefined) {
                                    detail.PlatNomorPolisiSendDateCekChanged = true;
                                } else {
                                    detail.PlatNomorPolisiSendDateCekChanged = false;
                                };
                            })
                        })
                    });
                    console.log("")
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        //Button Tambah Surat Distribusi
        $scope.BuatDistribusiSTNK = function() {
            console.log("'asdasd",$scope.gridPilihFrame);
            //$scope.gridPilihFrame.columnDefs = $scope.defBuat;
            //$scope.STNKTerpilih.columnDefs = $scope.stnkDefBuat;
            $scope.ShowMaintainSuratDistribusiStnkMain = false;
            // MaintainSuratDistribusiSTNKFactory.getPilihDataForInsert().then(
            //     function(res) {
            //         $scope.gridPilihFrame.data = res.data.Result;
            //         $scope.loading = false;
            //         return res.data;
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            $scope.show = { Wizard: true };
            $scope.button = { FrameTab: true, DokumenTab: false };
            $scope.status = 'buat';
            $scope.gridPilihFrame.data = [];
            $scope.MaintainSuratDistribusiStnk_Operation = "Insert";
            angular.element("#tabHeader1DisbSTNK").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbSTNK").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");

        };

        $scope.Lanjut = function() {
            //$scope.gridPilihFrame.columnDefs = $scope.defBuat;
            //$scope.STNKTerpilih.columnDefs = $scope.stnkDefBuat;
            angular.element("#tabHeader1DisbSTNK").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2DisbSTNK").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };

             if ($scope.status == 'ubah') {

                for (var i in $scope.STNKTerpilih.data) {
                    for (var j in $scope.selctedRow) {
                        if ($scope.STNKTerpilih.data[i].FrameNo == $scope.selctedRow[j].FrameNo) {
                            $scope.selctedRow.splice(j, 1);
                        }
                    }
                }

                if ($scope.selctedRow.length > 0) {
                    angular.forEach($scope.selctedRow, function(selctedRow, Index) {
                        var tempselectedRow = angular.copy(selctedRow);
                        tempselectedRow.listDetail = [];
                        selctedRow = tempselectedRow;
                        var templistDetail = {
                                'OutletId': selctedRow.OutletRequestPOId,
                                'SuratDistribusiSTNKNo': null,
                                'PrintCountToCustomer': null,
                                'FrameNo': selctedRow.FrameNo,
                                'CustomerName': selctedRow.CustomerName,
                                'STNKNo': selctedRow.STNKNo,
                                'PoliceNumber': selctedRow.PoliceNumber,
                                'NamaPenerima': null,
                                'NoKTP': null,
                                'NoHP': null,
                                'STNKSendDate': null,
                                'PoliceNumberSendDate': null,
                                'NoticePajakSendDate': null,
                                'PoliceNumberSendDate': null,
                                'SuratDistribusiCAOKirimKeCabangStatusId': null,
                                'StatusCode': 1
                        };
                        selctedRow.listDetail.push(templistDetail);
                        $scope.STNKTerpilih.data.push(selctedRow);
                    })
                }

                $scope.status = 'ubah';

            } else {
                for(var i in $scope.selctedRow){
                    var tempselectedRow = angular.copy($scope.selctedRow[i]);
                    tempselectedRow.listDetail = [];
                    $scope.selctedRow[i] = tempselectedRow;
                    $scope.templistDetail = {
                            'OutletId': $scope.selctedRow[i].OutletRequestPOId,
                            'SuratDistribusiSTNKNo': null,
                            'PrintCountToCustomer': null,
                            'FrameNo': $scope.selctedRow[i].FrameNo,
                            'CustomerName': $scope.selctedRow[i].CustomerName,
                            'STNKNo': $scope.selctedRow[i].STNKNo,
                            'PoliceNumber': $scope.selctedRow[i].PoliceNumber,
                            'NamaPenerima': null,
                            'NoKTP': null,
                            'NoHP': null,
                            'STNKSendDate': null,
                            'PoliceNumberSendDate': null,
                            'NoticePajakSendDate': null,
                            'PoliceNumberSendDate': null,
                            'SuratDistribusiCAOKirimKeCabangStatusId': null,
                            'StatusCode': 1
                    };
                    $scope.selctedRow[i].listDetail.push($scope.templistDetail);
                }
                $scope.STNKTerpilih.data = angular.copy($scope.selctedRow);
                
                angular.forEach($scope.STNKTerpilih.data, function(data, dataindx) {
                                angular.forEach(data.listDetail, function(detail, detailIndx) {
                                    detail.STNKSendDateCekChanged = false;
                                    detail.NoticePajakSendDateCekChanged = false;
                                    detail.PlatNomorPolisiSendDateCekChanged = false;
                                });
                            });
                 
                $scope.status = 'buat';
            }

            //console.log("uuyuk",$scope.selctedRow);
            // try {
            //     if ($scope.selctedRow.length > 0) {
            //         $scope.CancelEditFrom = "NotDetilSuratDistribusi";
                    
            //         //$scope.ShowPilihData = false;
            //         //$scope.ShowBuatSuratDistribusi = true;

            //         //atas ini bikin dulu nullnya result
            //         $scope.Header = {
            //             'OutletId': null,
            //             'OutletName': null,
            //             'SuratDistribusiSTNKNo': null,
            //             'SuratDistribusiSTNKSentDate': null,
            //             'JumlahDokumen': $scope.selctedRow.length,
            //             'TerimaDokumenSTNKBPKBStatusId': 1,
            //             'StatusCode': 1,
            //             'listFrameNo': []
            //         };

            //         //$scope.Header.listFrameNo = $scope.selctedRow;

            //         ///console.log("hasilnya sekarang", $scope.Header);
            //         for (var i = 0; i < $scope.selctedRow.length; ++i) {
            //             $scope.listDetail = {
            //                 'OutletId': $scope.selctedRow[i].OutletRequestPOId,
            //                 'SuratDistribusiSTNKNo': null,
            //                 'PrintCountToCustomer': null,
            //                 'FrameNo': $scope.selctedRow[i].FrameNo,
            //                 'CustomerName': $scope.selctedRow[i].CustomerName,
            //                 'STNKNo': $scope.selctedRow[i].STNKNo,
            //                 'PoliceNumber': $scope.selctedRow[i].PoliceNumber,
            //                 'NamaPenerima': null,
            //                 'NoKTP': null,
            //                 'NoHP': null,
            //                 'STNKSendDate': null,
            //                 'PoliceNumberSendDate': null,
            //                 'NoticePajakSendDate': null,
            //                 'PoliceNumberSendDate': null,
            //                 'SuratDistribusiCAOKirimKeCabangStatusId': null,
            //                 'StatusCode': 1
            //             };

            //             $scope.listFrameNo = {
            //                 'OutletId': $scope.selctedRow[i]['OutletRequestPOId'],
            //                 'OutletName': $scope.selctedRow[i]['OutletRequestPOName'],
            //                 'OutletRequestPOName': $scope.selctedRow[i]['OutletRequestPOName'],
            //                 'SuratDistribusiSTNKNo': null,
            //                 'FrameNo': $scope.selctedRow[i]['FrameNo'],
            //                 'CustomerName': $scope.selctedRow[i]['CustomerName'],
            //                 'SuratDistribusiCAOKirimKeCabangStatusId': null,
            //                 'SuratDistribusiStatusName': null,
            //                 'VehicleTypeColorId': $scope.selctedRow[i]['VehicleTypeColorId'],
            //                 'Description': null,
            //                 'KatashikiCode': null,
            //                 'SuffixCode': null,
            //                 'VehicleModelId': $scope.selctedRow[i]['VehicleModelId'],
            //                 'VehicleModelCode': null,
            //                 'VehicleModelName': $scope.selctedRow[i]['VehicleModelName'],
            //                 'ColorId': $scope.selctedRow[i]['ColorId'],
            //                 'ColorCode': $scope.selctedRow[i]['ColorCode'],
            //                 'ColorName': $scope.selctedRow[i]['ColorName'],
            //                 'StatusCode': 1,
            //                 'Tipe' : $scope.selctedRow[i]['Description'],
            //                 'listDetail': [$scope.listDetail]
            //             };

            //             $scope.Header.listFrameNo.push($scope.listFrameNo);
            //             //bawah ini bikin kode buat insert ke result dummy
            //         }

            //         $scope.STNKTerpilih.data = $scope.Header.listFrameNo;

            //         angular.forEach($scope.STNKTerpilih.data, function(data, dataindx) {
            //             angular.forEach(data.listDetail, function(detail, detailIndx) {
            //                 detail.STNKSendDateCekChanged = false;
            //                 detail.NoticePajakSendDateCekChanged = false;
            //                 detail.PlatNomorPolisiSendDateCekChanged = false;
            //             });
            //         });
            //     } else {
            //         // alert('please select one');
            //     }
            // } catch (err) {
            //     // alert('please select one');
            // }
        };

        $scope.back = function() {
            angular.element("#tabHeader1DisbSTNK").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbSTNK").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.button = { FrameTab: true, DokumenTab: false };
            console.log("ubsattusah===>", $scope.status);
           //$scope.status = "ubah";
        }

        $scope.UpdateDetilBuatSuratDistribusiSTNK = function(selected_data,index) {
            //if($scope.MaintainSuratDistribusiStnk_Operation == "Update")
            //{

            //$scope.gridPilihFrame.columnDefs[1].visible = true;
            console.log("tyty", selected_data,index);
           // var s = angular.copy(selected_data);
            $scope.KembaliDariSimpanUpdatePenerimaanDokumen();

            if ((angular.isUndefined(selected_data.listDetail[0].DokumenLengkap) == true) ||
                selected_data.listDetail[0].DokumenLengkap == false ||
                selected_data.listDetail[0].DokumenLengkap == null) {
                selected_data.listDetail[0].DokumenLengkap = false;
            } else {
                selected_data.listDetail[0].DokumenLengkap = true;
            }

            $scope.The_listFrameNo = selected_data;
            //$scope.EditDetilBuatSuratDistribusi = $scope.tempDetail[index];
            $scope.indexTemp = index;
            $scope.EditDetilBuatSuratDistribusi = $scope.The_listFrameNo.listDetail[0];

            // if ($scope.EditDetilBuatSuratDistribusi.SuratDistribusiStatusName == "Lengkap") {
            //     $scope.EditDetilMaintainSuratDistribusiSTNKKelengkapan = true;
            // } else if ($scope.EditDetilBuatSuratDistribusi.SuratDistribusiStatusName == "Belum Lengkap") {
            //     $scope.EditDetilMaintainSuratDistribusiSTNKKelengkapan = false;
            // }

            //$scope.EditDetilBuatSuratDistribusi = angular.copy($scope.SelectedDetilBuatSuratDistribusi);

            //console.log("select", selected_data);

            // angular.forEach($scope.grid.data, function(data, dataIndx) {
            //     angular.forEach(data.listFrameNo, function(frame, frameIndx) {
            //         angular.forEach(frame.listDetail, function(detail, detailIndx) {
            //             if (detail.STNKSendDate != null || detail.STNKSendDate != undefined) {
            //                 detail.STNKSendDateCekChanged = true;
            //             } else {
            //                 detail.STNKSendDateCekChanged = false;
            //             };

            //             if (detail.NoticePajakSendDate != null || detail.NoticePajakSendDate != undefined) {
            //                 detail.NoticePajakSendDateCekChanged = true;
            //             } else {
            //                 detail.NoticePajakSendDateCekChanged = false;
            //             };

            //             if (detail.PoliceNumberSendDate != null || detail.PoliceNumberSendDate != undefined) {
            //                 detail.PlatNomorPolisiSendDateCekChanged = true;
            //             } else {
            //                 detail.PlatNomorPolisiSendDateCekChanged = false;
            //             };
            //         })
            //     })
            // });



            //}
            //else if($scope.MaintainSuratDistribusiStnk_Operation == "Insert")
            //{
            //}

            //$scope.dateOptionsMinDateOptions.minDate =new Date();
            angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('setting', { closable: false }).modal('show');
        }

    
        $scope.ShowDetil = function(SelectedData) {
            $scope.ShowMaintainSuratDistribusiStnkMain = false;
            $scope.ShowDetailSuratDistribusi = true;

            $scope.Header = SelectedData;
            $scope.print = 'sales/PengirimanSTNKPelanggankeCabang?SuratDistribusiSTNKNo=' + $scope.Header.SuratDistribusiSTNKNo;

            $scope.grid4.data = SelectedData.listFrameNo;
            console.log("detail bind", SelectedData)
            var copyDetail = angular.copy(SelectedData);  

            $scope.tempDetail = [];
            for(var i in copyDetail.listFrameNo){
                $scope.tempDetail.push(angular.copy(copyDetail.listFrameNo[i].listDetail[0]));
            }

        }

        $scope.KembaliKeMaintainSuratDistribusiStnkMain = function() {
            $scope.ShowPilihData = false;
            $scope.ShowDetailSuratDistribusi = false;
            angular.element("#tabHeader1DisbSTNK").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbSTNK").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.getData();

            $scope.selctedRow = [];
            $scope.button = { FrameTab: false, DokumenTab: false };
            $scope.show = { Wizard: false };
            $scope.DetilSuratDistribusi = "";
            $scope.grid4.data = [];
            $scope.ShowMaintainSuratDistribusiStnkMain = true;
        }

        $scope.KembaliKePilihData = function() {
            $scope.ShowBuatSuratDistribusi = false;
            $scope.STNKTerpilih.data = {};

            if ($scope.CancelEditFrom == "NotDetilSuratDistribusi") {
                $scope.ShowPilihData = true;
            } else if ($scope.CancelEditFrom == "DetilSuratDistribusi") {
                $scope.ShowDetailSuratDistribusi = true;
            }
        }


        $scope.KembaliKeDetilDariCetak = function(SelectedData) {
            $scope.ShowCetakPengirimanSTNKKeCabang = false;
            $scope.ShowDetailSuratDistribusi = true;
        }

        $scope.KembaliKeBuatSuratDistribusi = function() {
            $scope.CancelEditFrom = "DetilSuratDistribusi";
            $scope.status = "ubah";
            console.log("ubah===>", $scope.status);
            angular.element("#tabHeader1DisbSTNK").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2DisbSTNK").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2DisbSTNK").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };
            $scope.show = { Wizard: true };
            //$scope.gridPilihFrame.columnDefs = $scope.defUbah;
            //$scope.STNKTerpilih.columnDefs = $scope.stnkDefUbah;
            $scope.selctedRow = [];
            $scope.ShowDetailSuratDistribusi = false;
            //$scope.ShowBuatSuratDistribusi = true;
            for(var i in $scope.grid4.data){
                $scope.grid4.data[i].OutletRequestPOName = $scope.grid4.data[i].OutletName;
            }
            $scope.STNKTerpilih.data = $scope.grid4.data;
            $scope.gridPilihFrame.data = [];
            
        }

        $scope.KirimSuratDistribusi = function() {
            //$scope.MaintainSuratDistribusiStnkToKirim = { 'OutletCAOId': $scope.The_Result.OutletCAOId, 'OutletCAOName': $scope.The_Result.OutletCAOName, 'OutletId': $scope.The_Result.OutletId, 'OutletName': $scope.The_Result.OutletName, 'SuratDistribusiSTNKHeaderId': $scope.The_Result.SuratDistribusiSTNKHeaderId, 'SuratDistribusiSTNKNo': $scope.The_Result.SuratDistribusiSTNKNo, 'SuratDistribusiSTNKSentDate': $scope.The_Result.SuratDistribusiSTNKSentDate, 'JumlahDokumen': $scope.The_Result.JumlahDokumen, 'PrintCount': $scope.The_Result.PrintCount, 'TerimaDokumenSTNKBPKBStatusId': $scope.The_Result.TerimaDokumenSTNKBPKBStatusId, 'StatusCode': $scope.The_Result.StatusCode, 'TotalData': $scope.The_Result.TotalData, 'listFrameNo': $scope.The_Result.listFrameNo };
            MaintainSuratDistribusiSTNKFactory.Kirim($scope.Header).then(function() {
                //nanti di get
                // alert('Data Dikirim');
                $scope.ShowMaintainSuratDistribusiStnkMain = true;
                $scope.ShowDetailSuratDistribusi = false;
                $scope.getData();
                bsNotify.show({
                    title: "Berhasil",
                    content: "Dokumen berhasil dikirim.",
                    type: 'success'
                });
            });

        }

        $scope.SimpanUpdatePenerimaanDokumen = function(){
            // console.log('aa',$scope.tempDetail);
            // console.log('bb',$scope.EditDetilBuatSuratDistribusi);

            if(typeof $scope.tempDetail=='undefined'){
                angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
            }
            else{
                
                //$scope.The_listFrameNo.listDetail[0] = angular.copy($scope.tempDetail[$scope.indexTemp]);
                angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
                $scope.tempDetail[$scope.indexTemp] = angular.copy($scope.EditDetilBuatSuratDistribusi);
            }

           
        }

        
        $scope.SimpanUpdatePenerimaanDokumenKeDatabase = function() {
           // $scope.Header.listFrameNo = $scope.STNKTerpilih.data;
           angular.element('.ui.modal.lodingdistribusistnk').modal('show');
            if ($scope.status == "ubah") {
                console.log("ubah simpan");
                //$scope.MaintainSuratDistribusiStnkToUpdate = { 'OutletCAOId': $scope.The_Result.OutletCAOId, 'OutletCAOName': $scope.The_Result.OutletCAOName, 'OutletId': $scope.The_Result.OutletId, 'OutletName': $scope.The_Result.OutletName, 'SuratDistribusiSTNKHeaderId': $scope.The_Result.SuratDistribusiSTNKHeaderId, 'SuratDistribusiSTNKNo': $scope.The_Result.SuratDistribusiSTNKNo, 'SuratDistribusiSTNKSentDate': $scope.The_Result.SuratDistribusiSTNKSentDate, 'JumlahDokumen': $scope.The_Result.JumlahDokumen, 'PrintCount': $scope.The_Result.PrintCount, 'TerimaDokumenSTNKBPKBStatusId': $scope.The_Result.TerimaDokumenSTNKBPKBStatusId, 'StatusCode': $scope.The_Result.StatusCode, 'TotalData': $scope.The_Result.TotalData, 'listFrameNo': $scope.The_Result.listFrameNo };
                MaintainSuratDistribusiSTNKFactory.update($scope.Header).then(function() {
                    //nanti di get
                    // $scope.KeDetilSuratDistribusi();
                    $scope.selctedRow=[];
                    $scope.getData();
                    $scope.ShowMaintainSuratDistribusiStnkMain = true;
                    $scope.ShowBuatSuratDistribusi = false;
                    $scope.show.Wizard = false;
                    angular.element('.ui.modal.lodingdistribusistnk').modal('hide');
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Berhasil update dokumen.",
                        type: 'success'
                    });
                });
            } else if ($scope.status == "buat") {
                console.log("buat simpan");
                $scope.Header = {
                                'OutletId': null,
                                'OutletName': null,
                                'SuratDistribusiSTNKNo': null,
                                'SuratDistribusiSTNKSentDate': null,
                                'JumlahDokumen': $scope.STNKTerpilih.data.length,
                                'TerimaDokumenSTNKBPKBStatusId': 1,
                                'StatusCode': 1,
                                'listFrameNo': []
                };
                $scope.Header.listFrameNo = $scope.STNKTerpilih.data;
                console.log("asdasdsad", $scope.Header.listFrameNo);
                for (var i in $scope.Header.listFrameNo){
                    $scope.Header.listFrameNo[i].OutletId = $scope.Header.listFrameNo[i].OutletRequestPOId;
                }

                var sds = angular.copy($scope.Header.listFrameNo);
                console.log("asdasdsad", sds);
                //$scope.Header.OutletId = $scope.Header.listFrameNo[0].OutletId;
                // for(var i in $scope.Header.listFrameNo){
                //     $scope.Header.listFrameNo[i].listDetail[0] = $scope.tempDetail[i];
                // }
                MaintainSuratDistribusiSTNKFactory.create($scope.Header).then(function(res) {
                    //nanti di get
                    // $scope.loadingDistribusiSTNK = false;
                    angular.forEach(res.data.Result, function(data, dataIndx) {
                        angular.forEach(data.listFrameNo, function(frame, frameIndx) {
                            angular.forEach(frame.listDetail, function(detail, detailIndx) {
                                if (detail.STNKSendDate != null || detail.STNKSendDate != undefined) {
                                    detail.STNKSendDateCekChanged = true;
                                } else {
                                    detail.STNKSendDateCekChanged = false;
                                };

                                if (detail.NoticePajakSendDate != null || detail.NoticePajakSendDate != undefined) {
                                    detail.NoticePajakSendDateCekChanged = true;
                                } else {
                                    detail.NoticePajakSendDateCekChanged = false;
                                };

                                if (detail.PoliceNumberSendDate != null || detail.PoliceNumberSendDate != undefined) {
                                    detail.PlatNomorPolisiSendDateCekChanged = true;
                                } else {
                                    detail.PlatNomorPolisiSendDateCekChanged = false;
                                };
                            })
                        })
                    });

                    $scope.print = 'sales/PengirimanSTNKPelanggankeCabang?SuratDistribusiSTNKNo=' + res.data.Result[0].SuratDistribusiSTNKNo;
                    $scope.selctedRow=[];
                    $scope.ShowDetil(res.data.Result[0]);
                    $scope.show.Wizard = false;
                    $scope.ShowDetailSuratDistribusi = true;
                    $scope.ShowBuatSuratDistribusi = false;
                    angular.element('.ui.modal.lodingdistribusistnk').modal('hide');
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Berhasil simpan dokumen baru.",
                        type: 'success'
                    });
                });
            }
        }

        $scope.showModalConfirmBatalStnk = function() {
            var numItems = angular.element('.ui.modal.confirmBatalStnk').length;
            setTimeout(function() {
                angular.element('.ui.modal.confirmBatalStnk').modal('refresh');
		}, 0);
		if (numItems > 1){
				angular.element('.ui.modal.confirmBatalStnk').not(':first').remove();
			}
			angular.element('.ui.modal.confirmBatalStnk').modal('show');
        }

        $scope.KeluarModalBatalStnk = function() {
            angular.element('.ui.modal.confirmBatalStnk').modal('hide');
        }

        $scope.BatalSuratDistribusi_Clicked = function() {
            MaintainSuratDistribusiSTNKFactory.delete([$scope.Header.SuratDistribusiSTNKHeaderId]).then(function() {
                //nanti di get
                $scope.show.Wizard = false;
                $scope.ShowDetailSuratDistribusi = false;
                $scope.KeluarModalBatalStnk();
                $scope.ShowMaintainSuratDistribusiStnkMain = true;
                bsNotify.show({
                    title: "Success",
                    content: "Surat distribusi STNK berhasil dibatalkan",
                    type: 'success'
                });
                $scope.getData();
            });

        }

        $scope.KeDetilSuratDistribusi = function() {
            $scope.ShowDetailSuratDistribusi = true;
            $scope.ShowBuatSuratDistribusi = false;

            //ini cuma asal pass ke detaildoang
            $scope.grid4.data = $scope.STNKTerpilih.data;

        }

        //======================================
        // function ticbox
        //======================================

        $scope.STNKFieldChanged = function(obj){
            if(obj.STNKSendDate != null){
                obj.STNKSendDateCekChanged = true;
            } else {
                obj.STNKSendDateCekChanged = false;
            }
        }

        $scope.PoliceNumberFieldChanged = function(obj){
            if(obj.PoliceNumberSendDate != null){
                obj.PlatNomorPolisiSendDateCekChanged = true;
            } else {
                obj.PlatNomorPolisiSendDateCekChanged = false;
            }
        }

        $scope.NoticePajakFieldChanged = function(obj){
            if(obj.NoticePajakSendDate != null){
                obj.NoticePajakSendDateCekChanged = true;
            } else {
                obj.NoticePajakSendDateCekChanged = false;
            }
        }

        //========================================
        // End funtion tickbox
        //========================================

        $scope.STNKSendDateChanged = function(obj) {
            if (obj.STNKSendDateCekChanged == true) {
                obj.STNKSendDate = fixDateJustMinutes(new Date());
                if(obj.PlatNomorPolisiSendDateCekChanged == true &&
                    obj.NoticePajakSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            } else {
                obj.STNKSendDate = null;
                if((obj.PlatNomorPolisiSendDateCekChanged == true &&
                    obj.NoticePajakSendDateCekChanged == true) ||
                    obj.PlatNomorPolisiSendDateCekChanged == false ||
                    obj.NoticePajakSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.PlatNomorPolisiSendDateChanged = function(obj) {
            if (obj.PlatNomorPolisiSendDateCekChanged == true) {
                obj.PoliceNumberSendDate = fixDateJustMinutes(new Date());
                if(obj.STNKSendDateCekChanged == true &&
                    obj.NoticePajakSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            } else {
                obj.PoliceNumberSendDate = null;
                if((obj.STNKSendDateCekChanged == true &&
                    obj.NoticePajakSendDateCekChanged == true) ||
                    obj.STNKSendDateCekChanged == false ||
                    obj.NoticePajakSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.NoticePajakSendDateChanged = function(obj) {
            if (obj.NoticePajakSendDateCekChanged == true) {
                obj.NoticePajakSendDate = fixDateJustMinutes(new Date());
                if(obj.STNKSendDateCekChanged == true &&
                    obj.PlatNomorPolisiSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            } else {
                obj.NoticePajakSendDate = null;
                if((obj.STNKSendDateCekChanged == true &&
                    obj.PlatNomorPolisiSendDateCekChanged == true) ||
                    obj.STNKSendDateCekChanged == false ||
                    obj.PlatNomorPolisiSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.CentangAllDokumenChanged = function(Lengkap, selected_data) {
            if (Lengkap == true) {
                selected_data.STNKSendDateCekChanged = true;
                selected_data.PlatNomorPolisiSendDateCekChanged = true;
                selected_data.NoticePajakSendDateCekChanged = true;
                selected_data.DisablePasSemuaDicentang = true;

                selected_data.STNKSendDate = fixDateJustMinutes(new Date());
                selected_data.NoticePajakSendDate = fixDateJustMinutes(new Date());
                selected_data.PoliceNumberSendDate = fixDateJustMinutes(new Date());

            } else if (Lengkap == false) {
                selected_data.STNKSendDateCekChanged = false;
                selected_data.NoticePajakSendDateCekChanged = false;
                selected_data.PlatNomorPolisiSendDateCekChanged = false;
                selected_data.DisablePasSemuaDicentang = false;

                selected_data.STNKSendDate = null;
                selected_data.NoticePajakSendDate = null;
                selected_data.PoliceNumberSendDate = null;
            }
        }


        $scope.KembaliDariSimpanUpdatePenerimaanDokumen = function() {
            $scope.CentangAllDokumen = false
            $scope.STNKSendDateCekChanged = false;
            $scope.NoticePajakSendDateCekChanged = false;
            $scope.PlatNomorPolisiSendDateCekChanged = false;
            $scope.EditDetilBuatSuratDistribusi = null;


            //angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');

        }

        $scope.keluarModal = function (){
            // console.log('testnih',$scope.The_listFrameNo);
            // console.log('testnih2',$scope.tempDetail);

            if(typeof $scope.tempDetail=='undefined'){
                angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
            }
            else{                
                angular.element('.ui.modal.UpdatePenerimaanDokumenSTNK').modal('hide');
                $scope.The_listFrameNo.listDetail[0] = angular.copy($scope.tempDetail[$scope.indexTemp]);
            }
            
        }

        $scope.KelengkapanDokumenChanged = function(selected_data) {
            for (var i = 0; i < $scope.EditDetilBuatSuratDistribusi.ListDokumen.length; ++i) {
                if ($scope.EditDetilBuatSuratDistribusi.ListDokumen[i].Uploaded == true) {
                    $scope.EditDetilBuatSuratDistribusi.Lengkap = true;
                } else {
                    $scope.EditDetilBuatSuratDistribusi.Lengkap = false;
                    break;
                }
            }
        }

        $scope.ListHapus = [];
        $scope.HapusBuatSuratDistribusi = function(row, distribusiId, index) {
            var temp = angular.copy(row);
            console.log("isi hapus===>", row);
            // if (angular.isDefined(distribusiId)) {
            //     if (temp.SuratDistribusiId == distribusiId) {
            //         $scope.ListHapus.push(temp);
            //     }
            // }

            // angular.forEach($scope.gridPilihFrame.data, function(data, index) {
            //     if (data.FrameNo == temp.FrameNo) {
            //         $scope.gridPilihFrame.data.splice(index, 1);
            //     }
            // })

            //$scope.gridPilihFrame.data.push(temp);

            angular.forEach($scope.STNKTerpilih.data, function(data, index) {
                if (data.FrameNo == row.FrameNo) {
                    $scope.ListHapus.push(temp);
                    $scope.STNKTerpilih.data.splice(index, 1);
                }
            })

            console.log("isi hapus tempp===>",  $scope.ListHapus);

            // var Selected_Index_To_Delete = $scope.STNKTerpilih.data.indexOf(selected_data);
            // if (Selected_Index_To_Delete != -1) {
            //     $scope.STNKTerpilih.data.splice(Selected_Index_To_Delete, 1);

            //     if ($scope.MaintainSuratDistribusiStnk_Operation == "Update") {
            //         for (var i = 0; i < $scope.The_Result['listFrameNo'].length; ++i) {
            //             if ($scope.The_Result['listFrameNo'][i]['SuratDistribusiSTNKHeaderId'] == selected_data.SuratDistribusiSTNKHeaderId) {
            //                 $scope.The_Result['listFrameNo'].splice(i, 1);
            //             }
            //         }
            //     } else if ($scope.MaintainSuratDistribusiStnk_Operation == "Insert") {
            //         for (var i = 0; i < $scope.The_Result_Insert['listFrameNo'].length; ++i) {
            //             if ($scope.The_Result_Insert['listFrameNo'][i]['FrameNo'] == selected_data.FrameNo) {
            //                 $scope.The_Result_Insert['listFrameNo'].splice(i, 1);
            //             }
            //         }
            //     }


            // }
        }

        $scope.CetakPengirimanSTNKKeCabang = function() {
            $scope.ShowDetailSuratDistribusi = false;
            $scope.ShowCetakPengirimanSTNKKeCabang = true;

            MaintainSuratDistribusiSTNKFactory.getDataToCetak($scope.SelectedSTNKToPrint.SuratDistribusiSTNKNo).then(function(res) {
                $scope.PageMaintainSuratDistribusiStnkCetak = res.data.Result;
                return $scope.PageMaintainSuratDistribusiStnkCetak;
            });

        }
        var actionbutton = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.ShowDetil(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' ;

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'SuratDistribusiSTNKHeaderId', width: '7%', visible: false },
                { name: 'Cabang', field: 'OutletName' },
                { name: 'NomorDistribusi', field: 'SuratDistribusiSTNKNo' },
                { name: 'TanggalPengiriman', field: 'SuratDistribusiSTNKSentDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'JumlahDokumen', width: '10%', field: 'JumlahDokumen' },
                { name: 'Status', field: 'TerimaDokumenSTNKBPKBStatusName'},
                { name: 'Action', field: '', width: '10%', cellTemplate: actionbutton }
            ]
        };

        // $scope.defBuat =  [
        //     { name: 'Cabang', field: 'OutletRequestPOName', width: '25%' },
        //     { name: 'NoRangka', field: 'FrameNo', width: '25%' },
        //     { name: 'NamaPelanggan', field: 'CustomerName', width: '15%' },
        //     { name: 'Model Kendaraan', field: 'VehicleModelName', width: '10%' },
        //     { name: 'Tipe', field: 'Description', width: '25%' },
        // ]

        //$scope.defUbah =  

        $scope.gridPilihFrame = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            //data:$scope.DetilSuratDistribusi,
            columnDefs: [
                { name: 'Cabang', field: 'OutletRequestPOName', width: '25%' },
                { name: 'NoRangka', field: 'FrameNo', width: '25%' },
                { name: 'NamaPelanggan', field: 'CustomerName', width: '15%' },
                { name: 'Model Kendaraan', field: 'VehicleModelName', width: '10%' },
                { name: 'Tipe', field: 'Description', width: '25%' },
            ]
        };

        var action =
        '<a style="color:#777;" tooltip-placement="bottom" uib-tooltip="Update Document" ng-click="grid.appScope.UpdateDetilBuatSuratDistribusiSTNK(row.entity, grid.renderContainers.body.visibleRowCache.indexOf(row))">' +
        '<i class="fa fa-upload fa-lg" style="padding:8px 8px 8px 0px;margin-left:10px;" aria-hidden="true"></i>' +
        '</a>' +
        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Hapus" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.HapusBuatSuratDistribusi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:10px;"></i>' +
        '</a>';

        // $scope.stnkDefBuat = ;

        //  $scope.stnkDefUbah = [
        //      { name: 'Cabang', field: 'OutletName' },
        //      { name: 'NoRangka', field: 'FrameNo' },
        //      { name: 'NamaPelanggan', field: 'CustomerName' },
        //      { name: 'Model Kendaraan', field: 'VehicleModelName' },
        //      { name: 'Tipe', field: 'Description', visible: false },
        //      { name: 'Action', cellTemplate: action , width: '12%',}
             
        //  ];

        $scope.STNKTerpilih = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            //data:$scope.selctedRow,
            columnDefs: [
                { name: 'Cabang', field: 'OutletRequestPOName' },
                { name: 'NoRangka', field: 'FrameNo' },
                { name: 'NamaPelanggan', field: 'CustomerName' },
                { name: 'Model Kendaraan', field: 'VehicleModelName' },
                { name: 'Tipe', field: 'Description' },
                { name: 'Action', cellTemplate: action , width: '12%',}
                
            ]
        };

        $scope.grid4 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            //data:$scope.selctedRow,
            columnDefs: [
                { name: 'Cabang', field: 'OutletName'},
                { name: 'NoRangka', field: 'FrameNo'},
                { name: 'NamaPelanggan', field: 'CustomerName' },
                { name: 'Model Kendaraan', field: 'VehicleModelName'},
                { name: 'Tipe', field: 'Description'},
                { name: 'Status', field: 'SuratDistribusiStatusName', width: '10%' },
            ]
        };

        $scope.gridPilihFrame.onRegisterApi = function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
                console.log("console", $scope.selctedRow);
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };
    });