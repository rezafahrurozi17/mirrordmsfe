angular.module('app')
    .controller('MaintainSuratDistribusiController', function($scope, $http, CurrentUser, MaintainSuratDistribusiFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        // $scope.$on('$viewContentLoaded', function() {
        //     $scope.loading = false;
        //     $scope.gridData = [];
        // });
        //----------------------------------
        // Initialization
        //----------------------------------
        //$scope.user = CurrentUser.user();
        $scope.mMaintainSuratDistribusi = null; //Model
        //$scope.xRole = { selected: [] };
        $scope.show = { Wizard: false };
        $scope.printAfiform = false;
        $scope.printAtpm = false;
        $scope.listContainer = true;
        $scope.listDetail = false;
        $scope.formData = false;
        $scope.noRangka = false;
        $scope.button = { FrameTab: false, DokumenTab: false };
        $scope.status = 'buat';
        $scope.selctedRow = [];
        var dateNow = new Date();
        $scope.disabledSimpandistribusi = false;
//         var mount = new Date();
//         var year = mount.getFullYear();
//         var mounth = mount.getMonth();
//         var lastdate = new Date(year, mounth + 1, 0).getDate();

//         $scope.firstDate = new Date(mount.setDate("01"));
//         $scope.lastDate = new Date(mount.setDate(lastdate));

        // $scope.selctedRow = [{ NoRangka: null, NamaPelanggan: null, VehicleModel: null }];
        //$scope.filter = { Id: 0, StartSentDate: $scope.firstDate, EndSentDate: new Date(), FrameNo: null, SuratDistribusiNo: null, CustomerName: null };
        // $scope.selctedRow = [{ NoRangka: null, NamaPelanggan: null, VehicleModel: null }];
        $scope.filter = { Id: 0, StartSentDate: new Date(dateNow.getFullYear(), dateNow.getMonth(), 1), EndSentDate: new Date(), FrameNo: null, SuratDistribusiNo: null, CustomerName: null };

        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionLastDate = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

        $scope.tanggalmin = function(dt) {
            $scope.dateOptionLastDate.minDate = dt;
            if (dt == null || dt == undefined) {
                $scope.filter.dateTo = null;
            } else {
                if ($scope.filter.StartSentDate < $scope.filter.EndSentDate) {

                } else {
                    $scope.filter.EndSentDate = $scope.filter.StartSentDate;
                }

            }
        }

       
        $scope.buatSuratDistribusi = function() {
            $scope.status = 'buat';
            //angular.element('.ui.modal.noRangka').modal('setting', 'transition', 'slide right').modal('show');
            angular.element("#tabHeader1").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2").removeClass("ui step ng-scope  active").addClass("ui step ng-scope");
            angular.element("#tabContent1").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.button = { FrameTab: true, DokumenTab: false };

            MaintainSuratDistribusiFactory.getDataNoRangka().then(function(res) {
                $scope.gridNoRangka.data = res.data.Result;
                $scope.show = { Wizard: true };
                $scope.listContainer = false;
                $scope.noRangka = true;
            })
        }

        $scope.tambahFrame = function() {
            $scope.gridNoRangka.data = [];
            $scope.buatSuratDistribusi();
        }

        $scope.lihatDetail = function(row) {
            $scope.listContainer = false;
            $scope.listDetail = true;

            $scope.headerDetail = angular.copy(row);
            // copy ke grid detail pada LIhat Detail
            $scope.detailList.data = angular.copy($scope.headerDetail.listFrameNo);
            //-buat temporary untuk restore
            $scope.tempdetail = angular.copy($scope.detailList.data);

            // copy ke list dokumen frame no
            //$scope.gridNoRangkaDocumen.data = angular.copy($scope.headerDetail.listFrameNo);

            $scope.SuratDistribusiId = $scope.headerDetail.SuratDistribusiId;
            $scope.noSurat = $scope.headerDetail.SuratDistribusiNo;
            $scope.ctkUrl = 'sales/PengirimanDokumenPelanggankeCAOorHO?SuratDistribusiNo=' + $scope.noSurat;
        }

        $scope.UbahSuratDistribusi = function() {
            $scope.status = 'ubah';

            $scope.show = { Wizard: true };
            $scope.listContainer = false;
            $scope.listDetail = false;

            angular.element("#tabHeader1").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };

            MaintainSuratDistribusiFactory.getDataNoRangka().then(function(res) {
                $scope.gridNoRangka.data = res.data.Result;

            })

            $scope.gridNoRangkaDocumen.data = angular.copy($scope.detailList.data);
        }

        $scope.lanjut = function(rows) {
            angular.element("#tabHeader1").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };

            $scope.rows = angular.copy(rows);

            if ($scope.status == 'ubah') {

                for (var i in $scope.gridNoRangkaDocumen.data) {
                    for (var j in $scope.rows) {
                        if ($scope.gridNoRangkaDocumen.data[i].FrameNo == $scope.rows[j].FrameNo) {
                            $scope.rows.splice(j, 1);
                        }
                    }
                }

                if ($scope.rows.length > 0) {
                    angular.forEach($scope.rows, function(row, Index) {
                        $scope.gridNoRangkaDocumen.data.push(row);
                    })
                }
                //$scope.gridNoRangkaDocumen.data.push($scope.selctedRow);
                // angular.element('.ui.modal.noRangkaSelected').modal('setting', 'transition', 'slide left').modal('show');
                $scope.status = 'ubah';
            } else {
                $scope.gridNoRangkaDocumen.data = angular.copy(rows);
                // angular.element('.ui.modal.noRangkaSelected').modal('setting', 'transition', 'slide left').modal('show');
                $scope.status = 'buat';
            }

            //angular.element('#noRangka').modal('hide');
        }

        $scope.resetData = function() {
            angular.element('.ui.modal.noRangkaSelected').modal('hide');
            $scope.selctedRow = [{ NoRangka: null, NamaPelanggan: null, VehicleModel: null }];
            $scope.detailList.data = angular.copy($scope.tempdetail);
        }

        // $scope.listTempDoc = [];
        // $scope.fileDocKK = [];

        $scope.formDocumenUpload = function(rows) {
            setTimeout(function() {
                angular.element('.ui.small.modal.formDocumenUpload').modal('refresh');
            }, 0);
            angular.element('.ui.small.modal.formDocumenUpload').modal('show');
            $scope.rowsDocument = rows;
            // $scope.rowsDocument.listDetail = [];
            MaintainSuratDistribusiFactory.getDocumentType().then(function(res) {
                $scope.browserdata = { UploadDokumenCustomer: undefined };
                $scope.dokumen = res.data.Result;
                var dokumenasli = angular.copy($scope.rowsDocument.listDetail);
                for (var i in $scope.dokumen) {
                    angular.merge($scope.dokumen[i], $scope.browserdata);
                }
                for (var i in $scope.rowsDocument.listDetail) {
                    for (var j in $scope.dokumen) {
                        if ($scope.rowsDocument.listDetail[i].DocumentId == $scope.dokumen[j].DocumentId) {
                            $scope.dokumen[j] = $scope.rowsDocument.listDetail[i];
                        }
                    }
                }
                for (var i in $scope.rowsDocument.listDetail) {
                    if (angular.isUndefined($scope.rowsDocument.listDetail[i].DocumentData && $scope.rowsDocument.listDetail[i].DocumentDataName) != true) {
                        $scope.rowsDocument.listDetail[i].UploadDokumenCustomer = {
                            FileName: $scope.rowsDocument.listDetail[i].DocumentDataName,
                            UpDocObj: $scope.rowsDocument.listDetail[i].DocumentData
                        }
                    }
                }
                $scope.rowsDocument.listDetail = $scope.dokumen;
                if($scope.rowsDocument.SuratDistribusiStatusId == 1){
                    $scope.rowsDocument.SuratDistribusiStatusChecked == true;
                }else{
                    $scope.rowsDocument.SuratDistribusiStatusChecked == false;
                }
                
            });
            console.log('$scope.rowsDocument',$scope.rowsDocument);
        }

        $scope.simpandistribusi = function() {
            $scope.disabledSimpandistribusi = true;
            var count = 0;
            // for(var i in $scope.gridNoRangkaDocumen.data){
            //     count++;
            // }
            $scope.dataterakhir = { JumlahDokumen: null, TerimaDokumenStatusId: 1, listFrameNo: [] };
            for (var i in $scope.gridNoRangkaDocumen.data) {
                if ($scope.gridNoRangkaDocumen.data[i].SuratDistribusiStatusChecked == true) {
                    $scope.gridNoRangkaDocumen.data[i].SuratDistribusiStatusId = 1;
                } else {
                    $scope.gridNoRangkaDocumen.data[i].SuratDistribusiStatusId = 2;
                }
            }

            for (var i in $scope.gridNoRangkaDocumen.data) {
                for (var j in $scope.gridNoRangkaDocumen.data[i].listDetail) {
                    if (angular.isUndefined($scope.gridNoRangkaDocumen.data[i].listDetail[j].SOId == true) &&
                        angular.isUndefined($scope.gridNoRangkaDocumen.data[i].listDetail[j].UploadDokumenCustomer == true)) {
                        $scope.gridNoRangkaDocumen.data[i].listDetail.splice(j, 1);
                    }
                }
            }

            for (var i in $scope.gridNoRangkaDocumen.data) {
                for (var j in $scope.gridNoRangkaDocumen.data[i].listDetail) {
                    if (($scope.gridNoRangkaDocumen.data[i].listDetail[j].SOId != null || $scope.gridNoRangkaDocumen.data[i].listDetail[j].SOId != undefined) ||
                        ($scope.gridNoRangkaDocumen.data[i].listDetail[j].UploadDokumenCustomer != null || $scope.gridNoRangkaDocumen.data[i].listDetail[j].UploadDokumenCustomer != undefined)) {
                        $scope.gridNoRangkaDocumen.data[i].listDetail[j].StatusDocumentId = 2;
                    }
                }
            }

            // else {
            //     $scope.gridNoRangkaDocumen.data[i].listDetail[j].StatusDocumentId = 2;
            // }


            $scope.dataterakhir.listFrameNo = $scope.gridNoRangkaDocumen.data;

            if ($scope.status == 'buat') {
                for (var i in $scope.dataterakhir.listFrameNo) {
                    count ++;
                    for (var j in $scope.dataterakhir.listFrameNo[i].listDetail) {
                        if (angular.isUndefined($scope.dataterakhir.listFrameNo[i].listDetail[j].DocumentData) == false) {
                            $scope.dataterakhir.listFrameNo[i].listDetail[j].DocumentData = "";
                        }
                    }
                }

                $scope.dataterakhir.JumlahDokumen = count;
                //console.log("too", $scope.dataterakhir);

                angular.forEach($scope.dataterakhir.listFrameNo, function(Data, DataIndx) {
                    count++;
                    angular.forEach(Data.listDetail, function(Doc, DocIdx) {
                        if (angular.isUndefined(Doc.DocumentData) == false) {
                            Doc.DocumentData = "";
                        }
                        if (angular.isUndefined(Doc.UploadDokumenCustomer) == false) {
                            Doc.UploadDokumenCustomer.UpDocObj = btoa(Doc.UploadDokumenCustomer.UpDocObj);
                        }
                    })
                });

                MaintainSuratDistribusiFactory.create($scope.dataterakhir).then(function(res) {
                    $scope.show.Wizard = false;
                    $scope.listDetail = true;
                    $scope.listContainer = false;
                    $scope.headerDetail = res.data.Result[0];
                    $scope.selctedRow =[];
                    $scope.status = 'buat';
                    for (var i in res.data.Result) {
                        for (var j in res.data.Result[i].listFrameNo) {
                            if (res.data.Result[i].listFrameNo[j].SuratDistribusiStatusId == 1) {
                                res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = true;
                            } else {
                                res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = false;
                            }
                        }
                    }
                    // isi lagi saat data behasil disimpan ke list detail
                    $scope.detailList.data = angular.copy($scope.headerDetail.listFrameNo);
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Surat berhasil dibuat.",
                        type: 'success'
                    });
                    $scope.disabledSimpandistribusi = false;
                    $scope.noSurat = $scope.headerDetail.SuratDistribusiNo;
                    $scope.ctkUrl = 'sales/PengirimanDokumenPelanggankeCAOorHO?SuratDistribusiNo=' + $scope.noSurat;
                    $scope.gridNoRangkaDocumen.data = [];
                });
            } else {
                $scope.listHapus = [];

                $scope.dataterakhirupdate = $scope.headerDetail;
                $scope.dataterakhirupdate.listFrameNo = angular.copy($scope.gridNoRangkaDocumen.data);

                
                angular.forEach($scope.dataterakhirupdate.listFrameNo, function(Data, DataIndx) {
                    count++;
                    angular.forEach(Data.listDetail, function(Doc, DocIdx) {
                        if (angular.isUndefined(Doc.DocumentData) == false) {
                            Doc.DocumentData = "";
                        }
                        if (angular.isUndefined(Doc.UploadDokumenCustomer == false)) {
                            Doc.UploadDokumenCustomer.UpDocObj = btoa(Doc.UploadDokumenCustomer.UpDocObj);
                        }
                    })
                });

                $scope.dataterakhir.JumlahDokumen = count;

                // $scope.dataterakhirupdate.listFrameNo.listDetail
                for(var i in $scope.dataterakhirupdate.listFrameNo){
                    console.log('$scope.rowsDocument.SuratDistribusiStatusChecked',$scope.rowsDocument.SuratDistribusiStatusChecked)
                    console.log('$scope.gridNoRangkaDocumen',$scope.gridNoRangkaDocumen)
                    $scope.dataterakhirupdate.listFrameNo[i].SuratDistribusiStatusChecked == $scope.rowsDocument.SuratDistribusiStatusChecked;
                }
                for(var i in $scope.dataterakhirupdate.listFrameNo)
				{
                    // console.log('masuk 1');
                    // console.log('isi data',$scope.dataterakhirupdate);
                    if ($scope.dataterakhirupdate.listFrameNo[i].SuratDistribusiStatusChecked == true){
                        $scope.dataterakhirupdate.listFrameNo[i].SuratDistribusiStatusId == 1;
                    }else{
                        $scope.dataterakhirupdate.listFrameNo[i].SuratDistribusiStatusId == 2;
                    }
                   
                    for (var j in $scope.dataterakhirupdate.listFrameNo[i].listDetail)
                    {
                        // console.log('masuk sini ga');
                        // console.log('isi data 2',$scope.dataterakhirupdate.listFrameNo[i].listDetail[j]);
                        if((typeof $scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer=="undefined" || $scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer==null)){
                            $scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer=null;
                        }else
                        if((typeof $scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer!="undefined")){
                            $scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer.UpDocObj=btoa($scope.dataterakhirupdate.listFrameNo[i].listDetail[j].UploadDokumenCustomer.UpDocObj);
                        }
                        

                    }
                   
                }
                
                console.log('$scope.dataterakhirupdate',$scope.dataterakhirupdate);
                // console.log('$scope.dataterakhirupdate',$scope.dataterakhirupdatexxxxxxx);

                MaintainSuratDistribusiFactory.update($scope.dataterakhirupdate).then(function(res) {
                    //angular.element('.ui.modal.noRangkaSelected').modal('hide');
                    $scope.show.Wizard = false;
                    $scope.listDetail = true;
                    $scope.listContainer = false;
                    
                    for (var i in res.data.Result) {
                        // console.log('masuk 1');
                        for (var j in res.data.Result[i].listFrameNo) {
                            // console.log('masuk 2');
                            if (res.data.Result[i].listFrameNo[j].SuratDistribusiStatusId == 1) {
                                console.log('masuk 3');
                                res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = true;
                            } else {
                                console.log('masuk 4');
                                res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = false;
                            }
                        }
                    }
                    $scope.headerDetail = res.data.Result[0];
                    $scope.detailList.data = angular.copy($scope.headerDetail.listFrameNo);
                    console.log('isi grid',$scope.detailList.data);
                    $scope.selctedRow =[];
                    $scope.status = 'buat';
                    
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Surat berhasil diubah.",
                        type: 'success'
                    });
                    $scope.disabledSimpandistribusi = false;
                    $scope.noSurat = $scope.headerDetail.SuratDistribusiNo;
                    $scope.gridNoRangkaDocumen.data = [];
                })
            }

        }

        $scope.KirimSuratDistribusi = function() {
            MaintainSuratDistribusiFactory.send($scope.headerDetail).then(function(res) {
                $scope.listDetail = false;
                $scope.listContainer = true;
                bsNotify.show({
                    title: "Berhasil",
                    content: "Surat berhasil dikirim.",
                    type: 'success'
                });
                $scope.getData();
            })
        }

        $scope.BatalSuratDistribusi = function() {
            MaintainSuratDistribusiFactory.delete($scope.headerDetail.SuratDistribusiId).then(function(res) {
                $scope.listDetail = false;
                $scope.listContainer = true;
                bsNotify.show({
                    title: "Berhasil",
                    content: "Surat berhasil dibatalkan.",
                    type: 'success'
                });
                $scope.getData();
            })
        }

        $scope.viewImage = function(dokumen) {
            MaintainSuratDistribusiFactory.getImage(dokumen).then(function(res) {
            })
        }


        $scope.tempSave = function() {
            angular.element('.ui.small.modal.formDocumenUpload').modal('hide');
        }

        $scope.kembali = function() {
            angular.element('.ui.small.modal.formDocumenUpload').modal('hide');
        }

        $scope.back = function() {
            angular.element("#tabHeader1").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.button = { FrameTab: true, DokumenTab: false };
        }

        $scope.removeImage = function(obj,idx){
            obj.UploadDokumenCustomer = {};
        }

        $scope.Batal = function() {
            $scope.status = 'ubah';            
            $scope.listContainer = true;
            $scope.show = { Wizard: false }
        }

        $scope.close = function() {
            angular.element('.ui.modal.noRangka').modal('hide');
            angular.element('.ui.modal.noRangkaSelected').modal('hide');
            angular.element('.ui.small.modal.formDocumenUpload').modal('hide');
        }

        $scope.kembaliList = function() {
            $scope.listContainer = true;
            $scope.listDetail = false;
            $scope.getData();
        }

        $scope.ubah = function() {
            angular.element('.ui.modal.noRangkaSelected').modal('show');
            $scope.gridNoRangka.data = $scope.headerDetail.listFrameNo;
        }


        $scope.ListHapus = [];
        $scope.hapus = function(row, distribusiId, index) {
           // console.log("kesini", row, distribusiId, index);
            
            var temp = angular.copy(row);

            if (angular.isDefined(distribusiId)) {
                if (temp.SuratDistribusiId == distribusiId) {
                    $scope.ListHapus.push(temp);
                }
            }

            angular.forEach($scope.gridNoRangka.data, function(data, index) {
                if (data.FrameNo == temp.FrameNo) {
                    $scope.gridNoRangka.data.splice(index, 1);
                }
            })

            $scope.gridNoRangka.data.push(temp);

            angular.forEach($scope.gridNoRangkaDocumen.data, function(data, index) {
                if (data.FrameNo == row.FrameNo) {
                    $scope.gridNoRangkaDocumen.data.splice(index, 1);
                }
            })

        }

        var gridData = [];
        $scope.getData = function() {
            MaintainSuratDistribusiFactory.getData($scope.filter).then(function(res) {
                for (var i in res.data.Result) {
                    for (var j in res.data.Result[i].listFrameNo) {
                        if (res.data.Result[i].listFrameNo[j].SuratDistribusiStatusId == 1) {
                            res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = true;
                        } else {
                            res.data.Result[i].listFrameNo[j].SuratDistribusiStatusChecked = false;
                        }
                    }
                }
                $scope.grid.data = res.data.Result;
            })
        }

        var actionbutton = '<a style="color:#777;" tooltip-placement="bottom" uib-tooltip="Lihat Detil" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">' +
            '<i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:10px;" aria-hidden="true"></i>' +
            '</a>';
            // '<a style="color:blue;" onclick=""; ng-click="">' +
            // '<p style="padding:5px 0 0 5px">' +
            // '<u tooltip-placement="bottom" uib-tooltip="Lihat Detil" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">Lihat Detil</u>&nbsp' +
            // '</p>' +
            // '</a>';

        var actionUploadDocumen = 
            '<a style="color:#777;" tooltip-placement="bottom" uib-tooltip="Ubah Dokumen" ng-click="grid.appScope.formDocumenUpload(row.entity)">' +
            '<i class="fa fa-upload fa-lg" style="padding:8px 8px 8px 0px;margin-left:10px;" aria-hidden="true"></i>' +
            '</a>' +
            '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Hapus" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.hapus(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:10px;"></i>' +
            '</a>';

        //----------------------------------
        // Grid Setup
        //----------------------------------  
       // $scope.grid = {columnDefs: [{ name: 'jumlah dokumen' }]};
        $scope.pagingArray = [10,25,50];
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: $scope.pagingArray,
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'cabang', field: 'OutletName' },
                { name: 'no distribusi', field: 'SuratDistribusiNo' },
                { name: 'tanggal pengiriman', field: 'SentDate', cellFilter: 'date:"dd/MM/yyyy"' },
                { name: 'jumlah dokumen kendaraan yang dimiliki', field: 'JumlahDokumen' },
                { name: 'status', field: 'TerimaDokumenStatusName' },
                {
                    name: 'action',
                    width: '8%',
                    visible: true,
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: actionbutton
                }
            ]
        };


        $scope.gridNoRangka = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: $scope.pagingArray,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'nomor rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model kendaraan', field: 'Description' },
            ]
        };

        $scope.detailList = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            paginationPageSizes: $scope.pagingArray,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                // { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'nomor rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model kendaraan', field: 'Description' },
                { name: 'status', field: 'SuratDistribusiStatusName' }
            ]
        };

        $scope.gridNoRangkaDocumen = {
            enableSorting: true,
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: $scope.pagingArray,
            //data: MaintainSuratDistribusiFactory.getDataNoRangka(),
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                //  { name: 'Id', field: 'Id', width: '15%', visible: false },
                //{ name: 'Hapus', cellTemplate: dropNoRangka, width: '5%' },
                { name: 'nomor rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model kendaraan', field: 'Description' },
                {
                    name: 'action',
                    width: '12%',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionUploadDocumen
                }
            ]
        };

        $scope.gridNoRangka.onRegisterApi = function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };


    });