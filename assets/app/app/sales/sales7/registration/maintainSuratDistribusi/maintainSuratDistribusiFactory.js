angular.module('app')
    .factory('MaintainSuratDistribusiFactory', function($httpParamSerializer, $http, CurrentUser) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            getData: function(filter) {
                filter.StartSentDate = fixDate(filter.StartSentDate);
                filter.EndSentDate = fixDate(filter.EndSentDate);
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/SAHSuratDistribusi?start=1&limit=1000000&' + param);
                return res;
            },
            getDataNoRangka: function() {
                var res = $http.get('/api/sales/ListFrameNoAfi');
                return res;
            },
            getDocumentType: function() {
                var res = $http.get('/api/sales/PCategoryDocumentSuratDistribusi');
                return res;
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
            },
            create: function(MaintainSuratDistribusi) {
               // console.log(MaintainSuratDistribusi);
                var res = $http.post('/api/sales/SAHSuratDistribusi/Insert', [MaintainSuratDistribusi]);
                return res;
            },
            send: function(MaintainSuratDistribusi) {
                return $http.put('/api/sales/SAHSuratDistribusi/Kirim', [{
                    OutletId: MaintainSuratDistribusi.OutletId,
                    SuratDistribusiId: MaintainSuratDistribusi.SuratDistribusiId
                }])
            },
            update: function(MaintainSuratDistribusi) {
                return $http.put('/api/sales/SAHSuratDistribusi/Update', [MaintainSuratDistribusi]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/SAHSuratDistribusi', { data: [id], headers: { 'Content-Type': 'application/json' } });
            },
           
        }
    });
//ddd