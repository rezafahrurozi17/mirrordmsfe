angular.module('app')
  .factory('TerimaDokumenFakturTAMFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;

    function fixDate(date) {
      if (date != null || date != undefined) {
          var fix = date.getFullYear() + '-' +
              ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
              ('0' + date.getDate()).slice(-2) + 'T' +
              ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
              ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
              '00';
          return fix;
      } else {
          return null;
      }
  };
   
    return {
      getData: function(filter) {
        if (filter == undefined || filter == null || filter == "") {
          filter = "";
        } else {
          filter = "&" + filter;
        }

        //var param = $httpParamSerializer(filter);
        var res = $http.get('/api/sales/FakturTAMKeCAO/?start=1&limit=1000'+filter);
        return res;
      },
      update: function(data){
        for(var i in data.ListFrameNo) {
          for(var j in data.ListFrameNo[i].ListDetailDokumen){
            data.ListFrameNo[i].ListDetailDokumen[j].ReceiveDate = fixDate(data.ListFrameNo[i].ListDetailDokumen[j].ReceiveDate);
            data.ListFrameNo[i].ListDetailDokumen[j].SentDate = fixDate(data.ListFrameNo[i].ListDetailDokumen[j].SentDate);
          }
        }
        return $http.put('/api/sales/FakturTAMKeCAO/Update', [data]);
      },
      getStatus: function(){
        var res = $http.get('/api/sales/PStatusFakturTAMKeCAO');
        return res;
      }
    }
  });