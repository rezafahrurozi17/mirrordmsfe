angular.module('app')
    .controller('TerimaDokumenFakturTAMController', function($httpParamSerializer,$filter, bsNotify, $scope, $http, CurrentUser, TerimaDokumenFakturTAMFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mterimaDokumenFakturTAM = null; //Model
        //$scope.xRole = { selected: [] };
        $scope.listContainer = true;
        $scope.listDetail = false;
        $scope.DokumenLengkap = false;
        $scope.rowsDocument = null;
        $scope.rowselected = [];
        $scope.formData = false;
        $scope.noRangka = false;
        $scope.num = 0;
        $scope.numview = 1;
        $scope.filter = {Id:0, FakturTAMKeCAOStatusId:null, FrameNo:null, NoSuratDistribusi:null, CustomerName:null};
        $scope.disabledSimpanupdate = false;
        $scope.disabledSimpanupdateSingle = false;
        
        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            var param = $httpParamSerializer($scope.filter);
            TerimaDokumenFakturTAMFactory.getData(param).then(function(res) {
                $scope.loading = false;
                    $scope.grid.data = res.data.Result;
                    angular.forEach($scope.grid.data, function(Doc, DocIndx) {
                        angular.forEach(Doc.ListFrameNo, function(Frame, FrameIndx) {
                            angular.forEach(Frame.ListDetailDokumen, function(Detail, DetailIndx) {
                                if (Detail.ReceiveDate == null || Detail.ReceiveDate == undefined) {
                                    Detail.ReceivedDateBit = false;
                                } else {
                                    Detail.ReceivedDateBit = true;
                                }
                            });
                        });
                    });
                },
                function(err) {
                    console.log("err=>", err)
                }
            );
        }

        TerimaDokumenFakturTAMFactory.getStatus().then(function (res){
            $scope.statusterima = res.data.Result;
        })

        $scope.checkdate = function(doc) {
            if (doc.ReceivedDateBit == false) {
                doc.ReceiveDate = fixDateJustMinutes(new Date());
            } else {
                doc.ReceiveDate = null;
            }
        }

        $scope.kembali = function() {
            $scope.listContainer = true;
            $scope.listDetail = false;
        }

        //----------------------------------
        // function sinkronisasi ticbox
        //----------------------------------

        $scope.ReceiveDateTicBox = function(op) {
            if(op.ReceiveDate != null) {
                op.ReceivedDateBit = true;
            } else if(op.ReceiveDate == null) {
                op.ReceivedDateBit = false;
            }
        }

        //----------------------------------
        // End function ticbox
        //----------------------------------
       
        function fixDateJustMinutes(date) {
            if (date != null || date != undefined) {
                var fix = null;
                var year = date.getFullYear();
                var mont = ('0' + (date.getMonth())).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var hour = ((date.getHours() < '10' ? '0' : '') + date.getHours());
                var Minute = ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes());
                
                fix = new Date(year, mont, day, hour, Minute);
                return fix;
            } else {
                return null;
            }
        };

        $scope.UpdateDokumen = function(row) {
            setTimeout(function() {
                angular.element('.ui.modal.formDocumenfaktur').modal('refresh');
            }, 0);
            angular.element('.ui.modal.formDocumenfaktur').modal('show');
            $scope.rowselected = angular.copy(row);
            $scope.rowsDocument = row.ListFrameNo;
			$scope.len = $scope.rowsDocument.length;
			
			for(var i = 0; i < $scope.rowsDocument.length; ++i)
			{	
				for(var x = 0; x < $scope.rowsDocument[i].ListDetailDokumen.length; ++x)
				{	
					
					var semprul=new Date($scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate.getFullYear(), $scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate.getMonth(), $scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate.getDate(), $scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate.getHours(), $scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate.getMinutes());
					$scope.rowsDocument[i].ListDetailDokumen[x].ReceiveDate=semprul;
				}
			}

            
            $scope.rowsDocument[$scope.num];
        }


        $scope.next = function() {
            $scope.num ++;
            $scope.numview ++;
        }

        $scope.prev = function() {
            $scope.num --;
            $scope.numview --;
        }

        $scope.test = function(data) {
            console.log("+++++>>>>adsda", $scope.header);
        }


        /////////Satuan////////////////////////////////////////////////////////////////
        $scope.lihatDetail = function(row) {
            $scope.listNorangka = null;
            $scope.header = angular.copy(row);
            console.log("7895hgdfhdgf", $scope.header);
            $scope.gridDetailDokumen.data = $scope.header.ListFrameNo;
            $scope.listNorangka = $scope.header.ListFrameNo;
            $scope.listContainer = false;
            $scope.listDetail = true;
        }

        $scope.listRangka = function(row) {
            $scope.DokumenLengkap = null;
            setTimeout(function() {
                angular.element('.ui.modal.formDocumenfakturTamSingle').modal('refresh');
            }, 0);
            angular.element('.ui.modal.formDocumenfakturTamSingle').modal('show');
            console.log("7895hgdfhdgf", row);
            $scope.rowsDocument = row;


            // $scope.noRangka = row.FrameNo;
            // $scope.namaPelanggan = row.CustomerName;
            // $scope.DokumenLengkap = row.DokumenLengkap;
            // $scope.rowsDocument = row.ListDetailDokumen;
        }

        $scope.simpanupdateSingle = function() {
            $scope.disabledSimpanupdateSingle = true;
            console.log("asdasd", $scope.rowsDocument);
            console.log("asdasd", $scope.header);

            angular.forEach($scope.header.ListFrameNo, function(FrameIndx, Frame){
                angular.forEach(Frame.ListDetailDokumen, function(DocIndx, Doc){
                    Doc.ReceiveDate = fixDate(Doc.ReceiveDate);
                    Doc.SentDate = fixDate(Doc.SentDate);
                })
            });

            TerimaDokumenFakturTAMFactory.update($scope.header).then(function(res) {
                $scope.listContainer = true;
                $scope.listDetail = false;
                $scope.num = 0;
                $scope.numview = 1;
				$scope.TesLagi=angular.copy(res.data.Result[0].FakturTAMKeCAOHeaderId);
                
				var param = $httpParamSerializer($scope.filter);
				TerimaDokumenFakturTAMFactory.getData(param).then(function(res) {
						$scope.grid.data = res.data.Result;
						angular.forEach($scope.grid.data, function(Doc, DocIndx) {
							angular.forEach(Doc.ListFrameNo, function(Frame, FrameIndx) {
								angular.forEach(Frame.ListDetailDokumen, function(Detail, DetailIndx) {
									if (Detail.ReceiveDate == null || Detail.ReceiveDate == undefined) {
										Detail.ReceivedDateBit = false;
									} else {
										Detail.ReceivedDateBit = true;
									}
								});
							});
						});
						
						setTimeout(function() {
							angular.element('.ui.modal.formDocumenfakturTamSingle').modal('refresh');
							angular.element('.ui.modal.formDocumenfakturTam').modal('refresh');
						}, 0);
						angular.element('.ui.modal.formDocumenfakturTamSingle').modal('hide');
						angular.element('.ui.modal.formDocumenfakturTam').modal('hide');
						$scope.listContainer = false;
						$scope.listDetail = true;
						bsNotify.show({
							title: "Berhasil",
							content: "Berhasil disimpan.",
							type: 'success'
                        });
                        $scope.disabledSimpanupdateSingle = false;
						$scope.kembali();
								
								for(var i = 0; i < $scope.grid.data.length; ++i)
								{	
									if($scope.grid.data[i].FakturTAMKeCAOHeaderId==$scope.TesLagi)
									{
										$scope.lihatDetail($scope.grid.data[i]);
										break;
									}
								}
						
					},
					function(err) {
						console.log("err=>", err)
					}
				);
				
				
                
				
				//ini dibawah paksaan tadiga ada
				// for(var i = 0; i < res.data.Result[0].ListFrameNo.ListDetailDokumen.length; ++i)
				// {
					// if (res.data.Result[0].ListFrameNo.ListDetailDokumen[i].ReceiveDate == null || res.data.Result[0].ListFrameNo.ListDetailDokumen[i].ReceiveDate == undefined) 
					// {
                        // res.data.Result[0].ListFrameNo.ListDetailDokumen[i].ReceivedDateBit = false;
                    // } else 
					// {
                        // res.data.Result[0].ListFrameNo.ListDetailDokumen[i].ReceivedDateBit = true;
                    // }
				// }
				
                // $scope.gridDetailDokumen.data = res.data.Result[0].ListFrameNo;
            })
        }
        //////////////////////////////////////////////////////////////////////////

        $scope.simpanupdate = function() {
            $scope.disabledSimpanupdate = true;
            $scope.rowselected.ListFrameNo = $scope.rowsDocument;

            for (var j in $scope.rowselected.ListFrameNo) {
                if ($scope.rowselected.ListFrameNo[j].TerimaFakturTAMKeCAOStatusCheck == true) {
                    $scope.rowselected.ListFrameNo[j].TerimaFakturTAMKeCAOStatusId = 1;
                } else {
                    $scope.rowselected.ListFrameNo[j].TerimaFakturTAMKeCAOStatusId = 2;
                }
            }

            angular.forEach($scope.rowselected.ListFrameNo, function(FrameIndx, Frame){
                angular.forEach(Frame.ListDetailDokumen, function(DocIndx, Doc){
                    Doc.ReceiveDate = fixDate(Doc.ReceiveDate);
                    Doc.SentDate = fixDate(Doc.SentDate);
                })
            });

            console.log("adasd",$scope.rowselected);

            TerimaDokumenFakturTAMFactory.update($scope.rowselected).then(function(res) {
                $scope.listContainer = true;
                $scope.listDetail = false;
                $scope.num = 0;
                $scope.numview = 1;
                angular.element(".ui.modal.formDocumenfaktur").modal("hide");
                $scope.getData();
                bsNotify.show({
                    title: "Berhasil",
                    content: "Berhasil disimpan.",
                    type: 'success'
                });
                $scope.disabledSimpanupdate = false;
            });
        }

        $scope.keluar = function() {
            angular.element(".ui.modal.formDocumenfakturTamSingle").modal("hide");
            angular.element(".ui.modal.formDocumenfaktur").modal("hide");
            $scope.num = 0;
            $scope.numview = 1;
        }

        var actionbutton = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="right" onclick="this.blur()" ng-click="grid.appScope.$parent.lihatDetail(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            '</a>' +

            '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update Dokumen" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            '</a>';

            // '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Penerimaan Dokumen" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)" tabindex="0"> ' +
            // '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            // '</a>' ;

        // var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Lihat Detil" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">Lihat Detil</u>&nbsp' +
        //     '|&nbsp<u tooltip-placement="bottom" uib-tooltip="Update Dokumen" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)">Update Dokumen</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        // var actionbuttondocument = '<a style="color:blue;" onclick=""; ng-click="">' +
        //     '<p style="padding:5px 0 0 5px">' +
        //     '<u tooltip-placement="bottom" uib-tooltip="Update Documen" ng-click="grid.appScope.listRangka(row.entity)">Update Document</u>&nbsp' +
        //     '</p>' +
        //     '</a>';

        var actionbuttondocument = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Update Dokumen" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.listRangka(row.entity)" tabindex="0"> ' +
            '<i class="fa fa-fw fa-lg fa fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
            '</a>';

        var checkdisterima = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.TerimaBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.TerimaBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

        var checklengkap = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.LengkapBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.LengkapBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

  
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
 

        //----------------------------------
        // Grid Setup
        //----------------------------------  
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'No Distribusi', field: 'NoSuratDistribusi' },
                { name: 'qty diterima/qty no rangka', field: 'QtyDokumenLengkapPerQtyFrameNo' },
                { name: 'status', field: 'FakturTAMKeCAOStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbutton
                }
            ]
        };

        $scope.gridDetailDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'diterima', width:'8%', cellTemplate: checkdisterima },
                { name: 'lengkap', width:'8%', cellTemplate: checklengkap },
                { name: 'Cabang', field: 'OutletName' },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'Model Tipe Kendaraan', field: 'Description' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbuttondocument,
                    width: '8%'
                }
            ]
        };

    });