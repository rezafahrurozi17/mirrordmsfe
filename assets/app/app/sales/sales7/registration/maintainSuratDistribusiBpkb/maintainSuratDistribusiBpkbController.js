angular.module('app')
    .controller('MaintainSuratDistribusiBpkbController', function($httpParamSerializer, $scope, $http, CurrentUser, MaintainSOFactory, MaintainSuratDistribusiBpkbFactory, $timeout, bsNotify) {
        //angular.element('.ui.modal.UpdatePenerimaanDokumen').remove();
        // IfGotProblemWithModal();

        $scope.ShowMaintainSuratDistribusiBpkbMain = true;
        $scope.DetilSuratDistribusi = "";
        $scope.MaintainSuratDistribusiBpkb_Operation = "";
        $scope.UpdatePenerimaanDokumen = { show: false };
        $scope.Search = {Id:0, OutletId:null, FrameNo:null};
        $scope.show = { Wizard: false };
        $scope.filter = { Id: 0, OutletId: null, SuratDistribusiSTNKNo: null, FrameNo: null, StartSentDate: null, EndSentDate: null };
        $scope.button = { FrameTab: false, DokumenTab: false };
        $scope.status = 'buat';
        $scope.selctedRow = [];
        $scope.dateOptionsStart = { startingDay: 1,
            format: 'dd/MM/yyyy'};
        $scope.dateOptionsEnd = { startingDay: 1,
            format: 'dd/MM/yyyy'};
        // $scope.MaintainSuratDistribusiBpkbToUpdate = {};

        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            MaintainSuratDistribusiBpkbFactory.getFrameByBranchList().then(
                function(res) {
                    $scope.outletlist = res.data.Result;
                    //return res.data;
                }
            );
        });

        $scope.$on('$destroy', function() {
            angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').remove();
            angular.element('.ui.modal.confirmBatalBpkb').remove();
          });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainSuratDistribusiBpkb = null; //Model
        $scope.xRole = { selected: [] };
        $scope.disabledSimpanUpdatePenerimaanDokumenKeDatabase = false;
        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() { 
           // var param = $httpParamSerializer($scope.filter);
            MaintainSuratDistribusiBpkbFactory.getData($scope.filter).then(
                function(res) {
                    $scope.loading = true;
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    $scope.CekCeklist($scope.grid.data);
                    //console.log("data dari Factory", res.data.Result);

                    //return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.SearchFrame = function (){
            var param = $httpParamSerializer($scope.Search);
            if($scope.status == 'buat'){
                $scope.grid2.data = [];
                console.log("Buat=>");
                MaintainSuratDistribusiBpkbFactory.getPilihDataForInsert(param).then(
                    function(res) {
                        $scope.grid2.data = res.data.Result;
                        $scope.loading = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }else{
                console.log("Ubah=>");
                $scope.grid2.data = [];
                MaintainSuratDistribusiBpkbFactory.getPilihDataForInsert(param).then(
                    function(res) {   
                        if($scope.ListHapus.length != 0 ){
                            for(var i in $scope.ListHapus){
                                $scope.grid2.data.push($scope.ListHapus[i]);
                            }
                        };
                        for(var i in res.data.Result){
                            $scope.grid2.data.push(res.data.Result[i])
                        };
                         //= res.data.Result;
                        $scope.loading = false;
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
           
        }

        $scope.KePilihData = function() {
            $scope.ShowMaintainSuratDistribusiBpkbMain = false;
            $scope.show = { Wizard: true };
            $scope.button = { FrameTab: true, DokumenTab: false };
            $scope.status = 'buat';
            $scope.grid2.data = [];
            angular.element("#tabHeader1DisbBPKB").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbBPKB").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");

            // MaintainSuratDistribusiBpkbFactory.getPilihDataForInsert().then(
            //     function(res) {
            //         $scope.grid2.data = res.data.Result;
            //         $scope.loading = false;
            //         //return res.data;
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );

            $scope.ShowPilihData = true;
            //$scope.MaintainSuratDistribusiBpkb_Operation = "Insert";
        }

        $scope.KeBuatSuratDistribusi = function() {
            angular.element("#tabHeader1DisbBPKB").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2DisbBPKB").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };

             if ($scope.status == 'ubah') {

                for (var i in $scope.grid3.data) {
                    for (var j in $scope.selctedRow) {
                        if ($scope.grid3.data[i].FrameNo == $scope.selctedRow[j].FrameNo) {
                            $scope.selctedRow.splice(j, 1);
                        }
                    }
                }

                if ($scope.selctedRow.length > 0) {
                    angular.forEach($scope.selctedRow, function(selctedRow, Index) {
                        var tempselectedRow = angular.copy(selctedRow);
                        tempselectedRow.listDetail = [];
                        selctedRow = tempselectedRow;
                        var templistDetail = {
                            'OutletId': selctedRow.OutletRequestPOId,
                            'SuratDistribusiBPKBNo': null,
                            'PrintCountToCustomer': null,
                            'FrameNo': selctedRow.FrameNo,
                            'CustomerName': selctedRow.CustomerName,
                            'BPKBNo': selctedRow.BPKBNo,
                            'PoliceNumber': selctedRow.PoliceNumber,
                            'NamaPenerima': null,
                            'NoKTP': null,
                            'NoHP': null,
                            'BPKBSendDate': null,
                            'FakturTAMSendDate': null,
                            'FormStatusGesekSendDate': null,
                            'CertificateNIKSendDate': null,
                            'SuratUjiTipeSendDate': null,
                            'FormASendDate': null,
                            'SuratRekomendasiSendDate': null,
                            'PIBSendDate': null,
                            'SuratDistribusiCAOKirimKeCabangStatusId': null,
                                // 'StatusCode': 1
                        };
                        selctedRow.listDetail.push(templistDetail);
                        $scope.grid3.data.push(selctedRow);
                    })
                }

                $scope.status = 'ubah';

            } else {
                for(var i in $scope.selctedRow){
                    var tempselectedRow = angular.copy($scope.selctedRow[i]);
                    tempselectedRow.listDetail = [];
                    $scope.selctedRow[i] = tempselectedRow;
                    $scope.templistDetail = {
                        'OutletId': $scope.selctedRow[i]['OutletRequestPOId'],
                                        'SuratDistribusiBPKBNo': null,
                                        'PrintCountToCustomer': null,
                                        'FrameNo': $scope.selctedRow[i]['FrameNo'],
                                        'CustomerName': $scope.selctedRow[i]['CustomerName'],
                                        'BPKBNo': $scope.selctedRow[i]['BPKBNo'],
                                        'PoliceNumber': $scope.selctedRow[i]['PoliceNumber'],
                                        'NamaPenerima': null,
                                        'NoKTP': null,
                                        'NoHP': null,
                                        'BPKBSendDate': null,
                                        'FakturTAMSendDate': null,
                                        'FormStatusGesekSendDate': null,
                                        'CertificateNIKSendDate': null,
                                        'SuratUjiTipeSendDate': null,
                                        'FormASendDate': null,
                                        'SuratRekomendasiSendDate': null,
                                        'PIBSendDate': null,
                                        'SuratDistribusiCAOKirimKeCabangStatusId': null,
                            // 'StatusCode': 1
                    };
                    $scope.selctedRow[i].listDetail.push($scope.templistDetail);
                }
                $scope.grid3.data = angular.copy($scope.selctedRow);
                
                angular.forEach($scope.grid3.data, function(data, dataindx) {
                                angular.forEach(data.listDetail, function(detail, detailIndx) {
                                        detail.FakturTAMSendDateCekChanged = false;
                                        detail.FormStatusGesekSendDateCekChanged = false;
                                        detail.CertificateNIKSendDateCekChanged = false;
                                        detail.SuratUjiTipeSendDateCekChanged = false;
                                        detail.FormASendDateCekChanged = false;
                                        detail.SuratRekomendasiSendDateCekChanged = false;
                                        detail.PIBSendDateCekChanged = false;
                                        detail.BPKBSendDateChanged = false;
                                });
                });
                 
                $scope.status = 'buat';
            }
        };

        $scope.UpdateDetilBuatSuratDistribusiBPKB = function(selected_data, index) {
            //if($scope.MaintainSuratDistribusiBpkb_Operation == "Update")
            //{
            $scope.KembaliDariSimpanUpdatePenerimaanDokumen();

            if ((angular.isUndefined(selected_data.listDetail[0].DokumenLengkap) == true) ||
                selected_data.listDetail[0].DokumenLengkap == false ||
                selected_data.listDetail[0].DokumenLengkap == null) {
                selected_data.listDetail[0].DokumenLengkap = false;
            } else {
                selected_data.listDetail[0].DokumenLengkap = true;
            }

            // $scope.EditDetilBuatSuratDistribusi = selected_data.listDetail[0];
            $scope.The_listFrameNo = selected_data;
            $scope.indexTemp = index;
            $scope.EditDetilBuatSuratDistribusi = $scope.The_listFrameNo.listDetail[0];

            angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').modal('setting', { closable: false }).modal('show');
                // angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').not(':first').remove();

        }

        $scope.ShowDetil = function(SelectedData) {
            $scope.The_Result = SelectedData;
            $scope.printUrl = 'sales/PengirimanBPKBPelanggankeCabang?SuratDistribusiBPKBNo=' + SelectedData.SuratDistribusiBPKBNo;
            console.log('Hasil Akhir => ', $scope.The_Result);
            $scope.SelectedBPKBToPrint = SelectedData;
            $scope.ShowMaintainSuratDistribusiBpkbMain = false;
            $scope.ShowDetailSuratDistribusiDetail = true;
            $scope.ShowBuatSuratDistribusi = false;
            $scope.ShowDetailSuratDistribusi = false;
            $scope.ShowPilihData = false;

            $scope.grid4.data = SelectedData['listFrameNo'];
            var copyDetail = angular.copy(SelectedData);  

            $scope.tempDetail = [];
            for(var i in copyDetail.listFrameNo){
                $scope.tempDetail.push(angular.copy(copyDetail.listFrameNo[i].listDetail[0]));
            }
        }

        $scope.KembaliKeDetilDariCetak = function(SelectedData) {
            $scope.ShowCetakPengirimanBPKBKeCabang = false;
            $scope.ShowDetailSuratDistribusi = true;
        }

        $scope.KembaliKeMaintainSuratDistribusiBpkbMain = function() {
            $scope.ShowPilihData = false;
            $scope.ShowDetailSuratDistribusi = false;
            $scope.ShowDetailSuratDistribusiDetail = false;
            $scope.show = { Wizard: false };
            $scope.button = { FrameTab: false, DokumenTab: false };
            $scope.getData();
            angular.element("#tabHeader1DisbBPKB").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbBPKB").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.ShowMaintainSuratDistribusiBpkbMain = true;
            $scope.DetilSuratDistribusi = "";
            $scope.grid4.data = [];
            $scope.ShowMaintainSuratDistribusiBpkbMain = true;
        }

        $scope.KembaliKePilihData = function() {
            angular.element("#tabHeader1DisbBPKB").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabHeader2DisbBPKB").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabContent1DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            angular.element("#tabContent2DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            $scope.button = { FrameTab: true, DokumenTab: false };
            console.log("ubsattusah===>", $scope.status);
            // $scope.ShowBuatSuratDistribusi = false;
            // $scope.grid3.data = {};
            // $scope.ShowPilihData = true;

            // if ($scope.CancelEditFrom == "NotDetilSuratDistribusi") {
            //     $scope.ShowPilihData = true;
            // } else if ($scope.CancelEditFrom == "DetilSuratDistribusi") {
            //     $scope.ShowDetailSuratDistribusi = true;
            // }
        }

        $scope.KembaliKeMaintainSuratDistribusiStnkMain = function (){
            $scope.show = { Wizard: false };
            $scope.button = { FrameTab: false, DokumenTab: false };
            $scope.ShowMaintainSuratDistribusiBpkbMain = true;
        }


        $scope.KembaliKeBuatSuratDistribusi = function() {
            $scope.CancelEditFrom = "DetilSuratDistribusi";
            $scope.status = 'ubah';
            console.log("ubah===>", $scope.status);
            angular.element("#tabHeader1DisbBPKB").removeClass("ui step ng-scope active").addClass("ui step ng-scope");
            angular.element("#tabHeader2DisbBPKB").removeClass("ui step ng-scope").addClass("ui step ng-scope active");
            angular.element("#tabContent1DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope").addClass("ui segment ng-scope ng-isolate-scope ng-hide");
            angular.element("#tabContent2DisbBPKB").removeClass("ui segment ng-scope ng-isolate-scope ng-hide").addClass("ui segment ng-scope ng-isolate-scope");
            $scope.button = { FrameTab: false, DokumenTab: true };
            $scope.show = { Wizard: true };
            //$scope.gridPilihFrame.columnDefs = $scope.defUbah;
            //$scope.STNKTerpilih.columnDefs = $scope.stnkDefUbah;
            $scope.selctedRow = [];
            $scope.ShowDetailSuratDistribusi = false;
            //$scope.ShowBuatSuratDistribusi = true;
            for(var i in $scope.grid4.data){
                $scope.grid4.data[i].OutletRequestPOName = $scope.grid4.data[i].OutletName;
            }
            $scope.ShowDetailSuratDistribusi = false;
            $scope.ShowBuatSuratDistribusi = true;
            $scope.ShowDetailSuratDistribusiDetail = false;
            $scope.grid3.data = $scope.grid4.data;
            $scope.MaintainSuratDistribusiBpkb_Operation = "Update";

        }

        $scope.KirimSuratDistribusi = function() {
            $scope.MaintainSuratDistribusiBpkbToKirim = { 'OutletCAOId': $scope.The_Result.OutletCAOId, 'OutletCAOName': $scope.The_Result.OutletCAOName, 'OutletId': $scope.The_Result.OutletId, 'OutletName': $scope.The_Result.OutletName, 'SuratDistribusiBPKBHeaderId': $scope.The_Result.SuratDistribusiBPKBHeaderId, 'SuratDistribusiBPKBNo': $scope.The_Result.SuratDistribusiBPKBNo, 'SuratDistribusiBPKBSentDate': $scope.The_Result.SuratDistribusiBPKBSentDate, 'JumlahDokumen': $scope.The_Result.JumlahDokumen, 'PrintCount': $scope.The_Result.PrintCount, 'TerimaDokumenSTNKBPKBStatusId': $scope.The_Result.TerimaDokumenSTNKBPKBStatusId, 'StatusCode': $scope.The_Result.StatusCode, 'TotalData': $scope.The_Result.TotalData, 'listFrameNo': $scope.The_Result.listFrameNo };
            MaintainSuratDistribusiBpkbFactory.Kirim($scope.MaintainSuratDistribusiBpkbToKirim).then(function() {
                $scope.ShowMaintainSuratDistribusiBpkbMain = true;
                $scope.ShowDetailSuratDistribusiDetail = false;
                $scope.getData();
                bsNotify.show({
                    title: "Berhasil",
                    content: "Berhasil kirim dokumen.",
                    type: 'success'
                });
                //nanti di get
                //alert('Data Dikirim');
            });
        }

        $scope.SimpanUpdatePenerimaanDokumenKeDatabase = function() {
            $scope.disabledSimpanUpdatePenerimaanDokumenKeDatabase = true;
            if ($scope.status == "ubah") {
                console.log("ubah simpan");
                MaintainSuratDistribusiBpkbFactory.update($scope.The_Result).then(function() {
                    //nanti di get
                    // $scope.KeDetilSuratDistribusi();
                    $scope.selctedRow=[];
                    $scope.getData();
                    $scope.ShowBuatSuratDistribusi = false;
                    $scope.show.Wizard = false;
                    $scope.ShowMaintainSuratDistribusiBpkbMain = true;
                    console.log('Setelah Update --> ', $scope.grid3.data);
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Berhasil update dokumen",
                        type: 'success'
                    });
                    $scope.disabledSimpanUpdatePenerimaanDokumenKeDatabase = false;
                });
            } else if ($scope.status == "buat") {
                console.log("buat simpan");
                $scope.Header = {
                                'OutletId': null, 
                                'OutletName': null, 
                                'SuratDistribusiBPKBNo': null, 
                                'SuratDistribusiBPKBSentDate': null, 
                                'JumlahDokumen': $scope.grid3.data.length, 
                                'TerimaDokumenSTNKBPKBStatusId': 1, 
                                'StatusCode': 1, 
                                'listFrameNo': []
                };
                $scope.Header.listFrameNo = $scope.grid3.data;
                //console.log("asdasdsad", $scope.Header.listFrameNo);
                for (var i in $scope.Header.listFrameNo){
                    $scope.Header.listFrameNo[i].OutletId = $scope.Header.listFrameNo[i].OutletRequestPOId;
                }

                var sds = angular.copy($scope.Header.listFrameNo);
                //console.log("asdasdsad", sds);
                //$scope.Header.OutletId = $scope.Header.listFrameNo[0].OutletId;
                // for(var i in $scope.Header.listFrameNo){
                //     for(var j in $scope.Header.listFrameNo[i].listDetail){
                //         $scope.Header.listFrameNo[i].listDetail[j].BPKBSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].BPKBSendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].FakturTAMSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].FakturTAMSendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].FormStatusGesekSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].FormStatusGesekSendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].CertificateNIKSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].CertificateNIKSendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate); 
                //         $scope.Header.listFrameNo[i].listDetail[j].FormASendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].FormASendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate);
                //         $scope.Header.listFrameNo[i].listDetail[j].PIBSendDate = DateFix($scope.Header.listFrameNo[i].listDetail[j].PIBSendDate);
                //     }
                // }
                var test = angular.copy($scope.Header);
                console.log('Test Tgl --> ', test);
                MaintainSuratDistribusiBpkbFactory.create($scope.Header).then(function(res) {
                    //nanti di get
                    $scope.CekCeklist(res.data.Result);
                    $scope.print = 'sales/PengirimanSTNKPelanggankeCabang?SuratDistribusiSTNKNo=' + res.data.Result[0].SuratDistribusiSTNKNo;
                    $scope.selctedRow=[];
                    $scope.ShowDetil(res.data.Result[0]);
                    $scope.show.Wizard = false;
                    $scope.ShowDetailSuratDistribusi = true;
                    $scope.ShowBuatSuratDistribusi = false;
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Berhasil simpan dokumen baru.",
                        type: 'success'
                    });
                    $scope.disabledSimpanUpdatePenerimaanDokumenKeDatabase = false;
                });
            }
            
            // if ($scope.MaintainSuratDistribusiBpkb_Operation == "Update") {
            //     $scope.MaintainSuratDistribusiBpkbToUpdate = { 
            //         'OutletCAOId': $scope.The_Result.OutletCAOId, 
            //         'OutletCAOName': $scope.The_Result.OutletCAOName, 
            //         'OutletId': $scope.The_Result.OutletId, 
            //         'OutletName': $scope.The_Result.OutletName, 
            //         'SuratDistribusiBPKBHeaderId': $scope.The_Result.SuratDistribusiBPKBHeaderId, 
            //         'SuratDistribusiBPKBNo': $scope.The_Result.SuratDistribusiBPKBNo, 
            //         'SuratDistribusiBPKBSentDate': $scope.The_Result.SuratDistribusiBPKBSentDate, 
            //         'JumlahDokumen': $scope.The_Result.JumlahDokumen, 
            //         'PrintCount': $scope.The_Result.PrintCount, 
            //         'TerimaDokumenSTNKBPKBStatusId': $scope.The_Result.TerimaDokumenSTNKBPKBStatusId, 
            //         'StatusCode': $scope.The_Result.StatusCode, 
            //         'TotalData': $scope.The_Result.TotalData, 
            //         'listFrameNo': $scope.The_Result.listFrameNo };
            //     MaintainSuratDistribusiBpkbFactory.update($scope.MaintainSuratDistribusiBpkbToUpdate).then(function() {
            //         $scope.ShowBuatSuratDistribusi = false;
            //         $scope.ShowMaintainSuratDistribusiBpkbMain = true;
            //     });
            // } else if ($scope.MaintainSuratDistribusiBpkb_Operation == "Insert") {
            //     console.log("si dia", $scope.The_Result_Insert);
            //     MaintainSuratDistribusiBpkbFactory.create($scope.The_Result_Insert).then(function(res) {
            //         $scope.ShowDetil(res.data.Result[0]);
            //         $scope.CekCeklist(res.data.Result[0]);
            //         $scope.printUrl = 'sales/PengirimanBPKBPelanggankeCabang?SuratDistribusiBPKBNo=' + res.data.Result[0].SuratDistribusiBPKBNo;
            //     });
            // }
        }

        $scope.confirmModalBatalBpkb = function() {
            var numItems = angular.element('.ui.modal.confirmBatalBpkb').length;
            setTimeout(function() {
                angular.element('.ui.modal.confirmBatalBpkb').modal('refresh');
		}, 0);
		if (numItems > 1){
				angular.element('.ui.modal.confirmBatalBpkb').not(':first').remove();
			}
			angular.element('.ui.modal.confirmBatalBpkb').modal('show');
        }

        $scope.KeluarModalBatalStnk = function() {
            angular.element('.ui.modal.confirmBatalBpkb').modal('hide');
        }

        $scope.BatalSuratDistribusi_Clicked = function() {
            MaintainSuratDistribusiBpkbFactory.delete([$scope.The_Result.SuratDistribusiBPKBHeaderId]).then(function() {
                //nanti di get
                $scope.ShowDetailSuratDistribusiDetail = false;
                $scope.ShowMaintainSuratDistribusiBpkbMain = true;
                $scope.KeluarModalBatalStnk();
                bsNotify.show({
                    title: "Success",
                    content: "Surat distribusi BPKB berhasil dibatalkan",
                    type: 'success'
                });
                // MaintainSuratDistribusiBpkbFactory.getData().then(
                //     function(res) {
                //         $scope.grid.data = res.data.Result;
                //         $scope.CekCeklist($scope.grid.data);
                //         $scope.loading = false;
                //         return res.data;
                //     }
                // ).then(function() {
                //     $scope.KembaliKeMaintainSuratDistribusiBpkbMain();
                // });
                $scope.getData();
            });

            //$scope.KembaliKeMaintainSuratDistribusiBpkbMain
        }

        $scope.KeDetilSuratDistribusi = function() {
            $scope.ShowDetailSuratDistribusi = true;
            $scope.ShowBuatSuratDistribusi = false;
            
            //ini cuma asal pass ke detaildoang
            $scope.grid4.data = $scope.grid3.data;

        }

        function fixDateJustMinutes(date) {
            if (date != null || date != undefined) {
                var fix = null;
                var year = date.getFullYear();
                var mont = ('0' + (date.getMonth())).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var hour = ((date.getHours() < '10' ? '0' : '') + date.getHours());
                var Minute = ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes());
                
                fix = new Date(year, mont, day, hour, Minute);
                return fix;
            } else {
                return null;
            }
        };

        //--------------------------------------------
        // Function Sinkronisasi TickBox
        //--------------------------------------------

        $scope.BPKBTickBox = function(op) {
            if(op.BPKBSendDate != null) {
                op.BPKBSendDateChanged = true;
            } else if(op.BPKBSendDate == null) {
                op.BPKBSendDateChanged = false;
            }
        }

        $scope.FakturTAMTickBox = function(op) {
            if(op.FakturTAMSendDate != null) {
                op.FakturTAMSendDateCekChanged = true;
            } else if(op.FakturTAMSendDate == null) {
                op.FakturTAMSendDateCekChanged = false;
            }
        }

        $scope.FormStatusGesekTickBox = function(op) {
            if(op.FormStatusGesekSendDate != null) {
                op.FormStatusGesekSendDateCekChanged = true;
            } else if(op.FormStatusGesekSendDate == null) {
                op.FormStatusGesekSendDateCekChanged = false;
            }
        }

        $scope.CertificateNIKTickBox = function(op) {
            if(op.CertificateNIKSendDate != null) {
                op.CertificateNIKSendDateCekChanged = true;
            } else if(op.CertificateNIKSendDate == null) {
                op.CertificateNIKSendDateCekChanged = false;
            }
        }

        $scope.SuratUjiTipeTickBox = function(op) {
            if(op.SuratUjiTipeSendDate != null) {
                op.SuratUjiTipeSendDateCekChanged = true;
            } else if(op.SuratUjiTipeSendDate == null) {
                op.SuratUjiTipeSendDateCekChanged = false;
            }
        }

        $scope.FormATickBox = function(op) {
            if(op.FormASendDate != null) {
                op.FormASendDateCekChanged = true;
            } else if(op.FormASendDate == null) {
                op.FormASendDateCekChanged = false;
            }
        }

        $scope.SuratRekomendasiTickBox = function(op) {
            if(op.SuratRekomendasiSendDate != null) {
                op.SuratRekomendasiSendDateCekChanged = true;
            } else if(op.SuratRekomendasiSendDate == null) {
                op.SuratRekomendasiSendDateCekChanged = false;
            }
        }

        $scope.PIBTickBox = function(op) {
            if(op.PIBSendDate != null) {
                op.PIBSendDateCekChanged = true;
            } else if(op.PIBSendDate == null) {
                op.PIBSendDateCekChanged = false;
            }
        }

        // --------------------------------- 
        //          End TickBox
        // ---------------------------------

        $scope.BPKBSendDateChanged = function(obj) {
            if (obj.BPKBSendDateChanged == true) {
                obj.BPKBSendDate = fixDateJustMinutes(new Date());
                if(obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.BPKBSendDateChanged == false) {
                obj.BPKBSendDate = null;
                if((obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false ||
                    obj.FormASendDateCekChanged == false ||
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.FakturTAMSendDateChanged = function(obj) {
            if (obj.FakturTAMSendDateCekChanged == true) {
                obj.FakturTAMSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.FakturTAMSendDateCekChanged == false) {
                obj.FakturTAMSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false ||
                    obj.FormASendDateCekChanged == false ||
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }


        $scope.FormStatusGesekSendDateChanged = function(obj) {
            if (obj.FormStatusGesekSendDateCekChanged == true) {
                obj.FormStatusGesekSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.FormStatusGesekSendDateCekChanged == false) {
                obj.FormStatusGesekSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false ||
                    obj.FormASendDateCekChanged == false ||
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.CertificateNIKSendDateChanged = function(obj) {
            if (obj.CertificateNIKSendDateCekChanged == true) {
                obj.CertificateNIKSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.CertificateNIKSendDateCekChanged == false) {
                obj.CertificateNIKSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false ||
                    obj.FormASendDateCekChanged == false ||
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.SuratUjiTipeSendDateChanged = function(obj) {
            if (obj.SuratUjiTipeSendDateCekChanged == true) {
                obj.SuratUjiTipeSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.SuratUjiTipeSendDateCekChanged == false) {
                obj.SuratUjiTipeSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.FormASendDateCekChanged == false &&
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.FormASendDateChanged = function(obj) {
            if (obj.FormASendDateCekChanged == true) {
                obj.FormASendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.FormASendDateCekChanged == false) {
                obj.FormASendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false &&
                    obj.SuratRekomendasiSendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.SuratRekomendasiSendDateChanged = function(obj) {
            if (obj.SuratRekomendasiSendDateCekChanged == true) {
                obj.SuratRekomendasiSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.SuratRekomendasiSendDateCekChanged == false) {
                obj.SuratRekomendasiSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.PIBSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false &&
                    obj.FormASendDateCekChanged == false ||
                    obj.PIBSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.PIBSendDateChanged = function(obj) {
            if (obj.PIBSendDateCekChanged == true) {
                obj.PIBSendDate = fixDateJustMinutes(new Date());
                if(obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true){
                        obj.DokumenLengkap = true;
                    }
            }
            if (obj.PIBSendDateCekChanged == false) {
                obj.PIBSendDate = null;
                if((obj.BPKBSendDateChanged == true &&
                    obj.FakturTAMSendDateCekChanged == true &&
                    obj.FormStatusGesekSendDateCekChanged == true &&
                    obj.CertificateNIKSendDateCekChanged == true &&
                    obj.SuratUjiTipeSendDateCekChanged == true &&
                    obj.FormASendDateCekChanged == true &&
                    obj.SuratRekomendasiSendDateCekChanged == true) ||
                    obj.BPKBSendDateChanged == false ||
                    obj.FakturTAMSendDateCekChanged == false ||
                    obj.FormStatusGesekSendDateCekChanged == false ||
                    obj.CertificateNIKSendDateCekChanged == false ||
                    obj.SuratUjiTipeSendDateCekChanged == false &&
                    obj.FormASendDateCekChanged == false ||
                    obj.SuratRekomendasiSendDateCekChanged == false){
                        obj.DokumenLengkap = false;
                    }
            }
        }

        $scope.CentangAllDokumenChanged = function(Lengkap, selected_data) {
            if (Lengkap == true) {
                selected_data.FakturTAMSendDateCekChanged = true;
                selected_data.FormStatusGesekSendDateCekChanged = true;
                selected_data.CertificateNIKSendDateCekChanged = true;
                selected_data.SuratUjiTipeSendDateCekChanged = true;
                selected_data.FormASendDateCekChanged = true;
                selected_data.SuratRekomendasiSendDateCekChanged = true;
                selected_data.PIBSendDateCekChanged = true;
                selected_data.BPKBSendDateChanged = true;
                selected_data.DisablePasSemuaDicentang = true;

                selected_data.FakturTAMSendDate = fixDateJustMinutes(new Date());
                selected_data.FormStatusGesekSendDate = fixDateJustMinutes(new Date());
                selected_data.CertificateNIKSendDate = fixDateJustMinutes(new Date());
                selected_data.SuratUjiTipeSendDate = fixDateJustMinutes(new Date());
                selected_data.FormASendDate = fixDateJustMinutes(new Date());
                selected_data.SuratRekomendasiSendDate = fixDateJustMinutes(new Date());
                selected_data.PIBSendDate = fixDateJustMinutes(new Date());
                selected_data.BPKBSendDate = fixDateJustMinutes(new Date());
            } else if (Lengkap == false) {
                selected_data.FakturTAMSendDateCekChanged = false;
                selected_data.FormStatusGesekSendDateCekChanged = false;
                selected_data.CertificateNIKSendDateCekChanged = false;
                selected_data.SuratUjiTipeSendDateCekChanged = false;
                selected_data.FormASendDateCekChanged = false;
                selected_data.SuratRekomendasiSendDateCekChanged = false;
                selected_data.PIBSendDateCekChanged = false;
                selected_data.BPKBSendDateChanged = false;
                selected_data.DisablePasSemuaDicentang = false;

                selected_data.FakturTAMSendDate = null;
                selected_data.FormStatusGesekSendDate = null;
                selected_data.CertificateNIKSendDate = null;
                selected_data.SuratUjiTipeSendDate = null;
                selected_data.FormASendDate = null;
                selected_data.SuratRekomendasiSendDate = null;
                selected_data.PIBSendDate = null;
                selected_data.BPKBSendDate = null;
            }
        }

        function DateFix(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + date.getDate()).slice(-2) + 'T' +
                ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;}
            else{ return null };
        }

        $scope.tempDetail = [];
        $scope.SimpanUpdatePenerimaanDokumen = function() {
            console.log("test", $scope.tempDetail);
            console.log("test1", $scope.EditDetilBuatSuratDistribusi,$scope.indexTemp);
            if(angular.isUndefined($scope.tempDetail) == false){
                $scope.tempDetail[$scope.indexTemp] = angular.copy($scope.EditDetilBuatSuratDistribusi);
            }
            angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').modal('hide');
            //angular.element('.ui.modal.UpdatePenerimaanDokumen').remove();

        }

        $scope.KembaliDariSimpanUpdatePenerimaanDokumen = function() {
            $scope.CentangAllDokumen = false
            $scope.FakturTAMSendDateCekChanged = false;
            $scope.FormStatusGesekSendDateCekChanged = false;
            $scope.CertificateNIKSendDateCekChanged = false;
            $scope.SuratUjiTipeSendDateCekChanged = false;
            $scope.FormASendDateCekChanged = false;
            $scope.SuratRekomendasiSendDateCekChanged = false;
            $scope.PIBSendDateCekChanged = false;
            $scope.BPKBSendDateCekChanged = false;
            $scope.DisablePasSemuaDicentang = false;

            $scope.EditDetilBuatSuratDistribusi = null;
            angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').modal('hide');
            //angular.element('.ui.modal.UpdatePenerimaanDokumen').remove();
        }

        $scope.keluarModal = function(){
            console.log('listframeno', $scope.The_listFrameNo);
            console.log('listframeno', $scope.tempDetail);
            if(angular.isUndefined($scope.tempDetail) == false){
                $scope.The_listFrameNo.listDetail[0] = angular.copy($scope.tempDetail[$scope.indexTemp]);
            };
           
            angular.element('.ui.modal.UpdatePenerimaanDokumenBPKB').modal('hide');
        }

        $scope.KelengkapanDokumenChanged = function(selected_data) {
            for (var i = 0; i < $scope.EditDetilBuatSuratDistribusi.ListDokumen.length; ++i) {
                if ($scope.EditDetilBuatSuratDistribusi.ListDokumen[i].Uploaded == true) {
                    $scope.EditDetilBuatSuratDistribusi.Lengkap = true;
                } else {
                    $scope.EditDetilBuatSuratDistribusi.Lengkap = false;
                    break;
                }
            }
        }

        $scope.ListHapus = [];
        $scope.HapusBuatSuratDistribusi = function(selected_data) {
            //alert(index);
            var temp = angular.copy(selected_data);
            angular.forEach($scope.grid3.data, function(data, index) {
                if (data.FrameNo == selected_data.FrameNo) {
                    $scope.ListHapus.push(temp);
                    $scope.grid3.data.splice(index, 1);
                }
            });

            console.log("hapus",$scope.ListHapus );
            // var Selected_Index_To_Delete = $scope.grid3.data.indexOf(selected_data);
            // if (Selected_Index_To_Delete != -1) {
            //     $scope.grid3.data.splice(Selected_Index_To_Delete, 1);

            //     if ($scope.MaintainSuratDistribusiBpkb_Operation == "Update") {
            //         for (var i = 0; i < $scope.The_Result['listFrameNo'].length; ++i) {
            //             if ($scope.The_Result['listFrameNo'][i]['SuratDistribusiBPKBDetailId'] == selected_data.SuratDistribusiBPKBDetailId) {
            //                 $scope.The_Result['listFrameNo'].splice(i, 1);
            //             }
            //         }
            //     } else if ($scope.MaintainSuratDistribusiBpkb_Operation == "Insert") {
            //         for (var i = 0; i < $scope.The_Result_Insert['listFrameNo'].length; ++i) {
            //             if ($scope.The_Result_Insert['listFrameNo'][i]['FrameNo'] == selected_data.FrameNo) {
            //                 $scope.The_Result_Insert['listFrameNo'].splice(i, 1);
            //             }
            //         }
            //     }
            // }
        }

        $scope.CetakPengirimanBPKBKeCabang = function() {
            $scope.ShowDetailSuratDistribusi = false;
            $scope.ShowCetakPengirimanBPKBKeCabang = true;

            MaintainSuratDistribusiBpkbFactory.getDataToCetak($scope.SelectedBPKBToPrint.SuratDistribusiBPKBNo).then(function(res) {
                $scope.PageMaintainSuratDistribusiBpkbCetak = res.data.Result;
                return $scope.PageMaintainSuratDistribusiBpkbCetak;
            });
            //$scope.SelectedBPKBToPrint.SuratDistribusiBPKBNo
            //PageMaintainSuratDistribusiBpkbCetak
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------

        var actionbutton = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Detail" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.ShowDetil(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' ;

        //grid Depan
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Cabang', field: 'OutletName' },
                { name: 'NomorDistribusi', field: 'SuratDistribusiBPKBNo' },
                { name: 'TanggalPengiriman', field: 'SuratDistribusiBPKBSentDate',cellFilter: 'date:\'dd-MM-yyyy\'', width: '10%' },
                { name: 'JumlahDokumen',  width: '10%',field: 'JumlahDokumen' },
                { name: 'Status', field: 'TerimaDokumenSTNKBPKBStatusName'},
                { name: 'Action', field: '', visible:true , width: '10%', cellTemplate: actionbutton }
            ]
        };

        //Grid Frame
        $scope.grid2 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
             paginationPageSizes: [10, 25, 50],
             paginationPageSize: 10,
            //data:$scope.DetilSuratDistribusi,
            columnDefs: [
                { name: 'Cabang', field: 'OutletRequestPOName', width: '20%' },
                { name: 'NoRangka', field: 'FrameNo', width: '20%' },
                { name: 'NamaPelanggan', field: 'CustomerName', width: '20%' },
                { name: 'Model Kendaraan', field: 'VehicleModelName', width: '20%' },
                { name: 'Tipe', field: 'Description', width: '20%' },
            ]
        };

        var action =
        '<a style="color:#777;" tooltip-placement="bottom" uib-tooltip="Update Document" ng-click="grid.appScope.UpdateDetilBuatSuratDistribusiBPKB(row.entity, grid.renderContainers.body.visibleRowCache.indexOf(row))">' +
        '<i class="fa fa-upload fa-lg" style="padding:8px 8px 8px 0px;margin-left:10px;" aria-hidden="true"></i>' +
        '</a>' +
        '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Hapus" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.HapusBuatSuratDistribusi(row.entity)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:10px;"></i>' +
        '</a>';

        //Grid Frame terpilih
        $scope.grid3 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
             paginationPageSizes: [10, 25, 50],
             paginationPageSize: 10,
            //data:$scope.selctedRow,
            columnDefs: [
                //{ name: 'Hapus', width: '6%', cellTemplate: '<i class="fa fa-times" ng-click="grid.appScope.HapusBuatSuratDistribusi(row.entity)" aria-hidden="true" ></i>' },
                { name: 'Cabang', field: 'OutletRequestPOName'},
                { name: 'NoRangka', field: 'FrameNo'},
                { name: 'NamaPelanggan', field: 'CustomerName'},
                { name: 'Model Kendaraan', field: 'VehicleModelName'},
                { name: 'Tipe', field: 'Description'},
                { name: 'Action', visible:true, width: '12%', cellTemplate: action}
                //'<a ng-click="grid.appScope.UpdateDetilBuatSuratDistribusiBPKB(row.entity)" >Update Dokumen</a>' },
            ]
        };

        //Grid Detail
        $scope.grid4 = {
            enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            //data:$scope.selctedRow,
            columnDefs: [
                { name: 'Cabang', field: 'OutletName' },
                { name: 'NoRangka', field: 'FrameNo' },
                { name: 'NamaPelanggan', field: 'CustomerName' },
                { name: 'Model Kendaraan', field: 'VehicleModelName' },
                { name: 'Tipe', field: 'Description' },
                { name: 'Status', field: 'SuratDistribusiStatusName' },
            ]
        };

        $scope.grid2.onRegisterApi = function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.CekCeklist = function(ArrayData) {
            angular.forEach(ArrayData, function(data, dataIndx) {
                angular.forEach(data.listFrameNo, function(frame, frameIndx) {
                    angular.forEach(frame.listDetail, function(detail, detailIndx) {
                        if (detail.FakturTAMSendDate != null || detail.FakturTAMSendDate != undefined) {
                            detail.FakturTAMSendDateCekChanged = true;
                        } else {
                            detail.FakturTAMSendDateCekChanged = false;
                        };

                        if (detail.FormStatusGesekSendDate != null || detail.FormStatusGesekSendDate != undefined) {
                            detail.FormStatusGesekSendDateCekChanged = true;
                        } else {
                            detail.FormStatusGesekSendDateCekChanged = false;
                        };

                        if (detail.CertificateNIKSendDate != null || detail.CertificateNIKSendDate != undefined) {
                            detail.CertificateNIKSendDateCekChanged = true;
                        } else {
                            detail.CertificateNIKSendDateCekChanged = false;
                        };

                        if (detail.SuratUjiTipeSendDate != null || detail.SuratUjiTipeSendDate != undefined) {
                            detail.SuratUjiTipeSendDateCekChanged = true;
                        } else {
                            detail.SuratUjiTipeSendDateCekChanged = false;
                        };

                        if (detail.FormASendDate != null || detail.FormASendDate != undefined) {
                            detail.FormASendDateCekChanged = true;
                        } else {
                            detail.FormASendDateCekChanged = false;
                        };

                        if (detail.SuratRekomendasiSendDate != null || detail.SuratRekomendasiSendDate != undefined) {
                            detail.SuratRekomendasiSendDateCekChanged = true;
                        } else {
                            detail.SuratRekomendasiSendDateCekChanged = false;
                        };

                        if (detail.PIBSendDate != null || detail.PIBSendDate != undefined) {
                            detail.PIBSendDateCekChanged = true;
                        } else {
                            detail.PIBSendDateCekChanged = false;
                        };

                        if (detail.BPKBSendDate != null || detail.BPKBSendDate != undefined) {
                            detail.BPKBSendDateChanged = true;
                        } else {
                            detail.BPKBSendDateChanged = false;
                        };
                    })
                })
            });
        }
});