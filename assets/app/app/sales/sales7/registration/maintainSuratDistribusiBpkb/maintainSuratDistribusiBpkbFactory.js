angular.module('app')
    .factory('MaintainSuratDistribusiBpkbFactory', function($httpParamSerializer, $http, CurrentUser) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            getData: function(filter) {
                // if (filter.StartSentDate != null) {
                //     filter.StartSentDate = filter.StartSentDate.getFullYear() + '-' +
                //         ('0' + (filter.StartSentDate.getMonth() + 1)).slice(-2) + '-' +
                //         ('0' + filter.StartSentDate.getDate()).slice(-2) + 'T' +
                //         ((filter.StartSentDate.getHours() < '10' ? '0' : '') + filter.StartSentDate.getHours()) + ':' +
                //         ((filter.StartSentDate.getMinutes() < '10' ? '0' : '') + filter.StartSentDate.getMinutes()) + ':' +
                //         ((filter.StartSentDate.getSeconds() < '10' ? '0' : '') + filter.StartSentDate.getSeconds());
                // };

                // if (filter.EndSentDate != null) {
                //     filter.EndSentDate = filter.EndSentDate.getFullYear() + '-' +
                //         ('0' + (filter.EndSentDate.getMonth() + 1)).slice(-2) + '-' +
                //         ('0' + filter.EndSentDate.getDate()).slice(-2) + 'T' +
                //         ((filter.EndSentDate.getHours() < '10' ? '0' : '') + filter.EndSentDate.getHours()) + ':' +
                //         ((filter.EndSentDate.getMinutes() < '10' ? '0' : '') + filter.EndSentDate.getMinutes()) + ':' +
                //         ((filter.EndSentDate.getSeconds() < '10' ? '0' : '') + filter.EndSentDate.getSeconds());
                // };
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/SAHSuratDistribusiBPKB?start=1&limit=1000&' + param);
                return res;
            },

            getDataToCetak: function(SuratDistribusiNo) {
                var res = $http.get('/api/sales/RegistrationCetakSuratDistribusiBPKB/?start=1&limit=100&filterData=SuratDistribusiNo|' + SuratDistribusiNo);
                return res;
            },

            getPilihDataForInsert: function(param) {
                if (param == undefined || param == null || param == "") {
                    param = "";
                } else {
                    param = "&" + param;
                }
                var res = $http.get('/api/sales/BPKBSelesaiDariBiroJasa/?start=1&limit=1000000'+param);
                return res;
            },

            getFrameByBranchList: function(){
                var res = $http.get('/api/sales/ListCabangCAO');
                return res;
            },
            
            create: function(MaintainSuratDistribusiBpkb) {
                for(var i in MaintainSuratDistribusiBpkb.listFrameNo){
                    for(var j in MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate);
                    }
                }
                MaintainSuratDistribusiBpkb.StartSentDate = fixDate(filter.StartSentDate);
                filter.EndSentDate = fixDate(filter.EndSentDate);
                var res = $http.post('/api/sales/SAHSuratDistribusiBPKB/Insert', [{
                    OutletId: MaintainSuratDistribusiBpkb.OutletId,
                    OutletName: MaintainSuratDistribusiBpkb.OutletName,
                    SuratDistribusiBPKBNo: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBNo,
                    SuratDistribusiBPKBSentDate: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBSentDate,
                    JumlahDokumen: MaintainSuratDistribusiBpkb.JumlahDokumen,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiBpkb.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiBpkb.StatusCode,
                    listFrameNo: MaintainSuratDistribusiBpkb.listFrameNo,
                    LastModifiedDate: null,
                    LastModifiedUserId: null,
                }]);
                return res;
            },
            update: function(MaintainSuratDistribusiBpkb) {
                for(var i in MaintainSuratDistribusiBpkb.listFrameNo){
                    for(var j in MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate);
                    }
                }
                // console.log('maintain surat --> ', MaintainSuratDistribusiBpkb);
                return $http.put('/api/sales/SAHSuratDistribusiBPKB/Update', [{
                    OutletCAOId: MaintainSuratDistribusiBpkb.OutletCAOId,
                    OutletCAOName: MaintainSuratDistribusiBpkb.OutletCAOName,
                    OutletId: MaintainSuratDistribusiBpkb.OutletId,
                    OutletName: MaintainSuratDistribusiBpkb.OutletName,
                    SuratDistribusiBPKBHeaderId: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBHeaderId,
                    SuratDistribusiBPKBNo: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBNo,
                    SuratDistribusiBPKBSentDate: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBSentDate,
                    JumlahDokumen: MaintainSuratDistribusiBpkb.JumlahDokumen,
                    PrintCount: MaintainSuratDistribusiBpkb.PrintCount,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiBpkb.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiBpkb.StatusCode,
                    TotalData: MaintainSuratDistribusiBpkb.TotalData,
                    listFrameNo: MaintainSuratDistribusiBpkb.listFrameNo,
                    LastModifiedDate: MaintainSuratDistribusiBpkb.LastModifiedDate,
                    LastModifiedUserId: MaintainSuratDistribusiBpkb.LastModifiedUserId
                }]);
            },
            Kirim: function(MaintainSuratDistribusiBpkb) {
                for(var i in MaintainSuratDistribusiBpkb.listFrameNo){
                    for(var j in MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail){
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].BPKBSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FakturTAMSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormStatusGesekSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].CertificateNIKSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratUjiTipeSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].FormASendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].SuratRekomendasiSendDate);
                        MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate = fixDate(MaintainSuratDistribusiBpkb.listFrameNo[i].listDetail[j].PIBSendDate);
                    }
                }
                return $http.put('/api/sales/SAHSuratDistribusiBPKB/Kirim', [{
                    OutletCAOId: MaintainSuratDistribusiBpkb.OutletCAOId,
                    OutletCAOName: MaintainSuratDistribusiBpkb.OutletCAOName,
                    OutletId: MaintainSuratDistribusiBpkb.OutletId,
                    OutletName: MaintainSuratDistribusiBpkb.OutletName,
                    SuratDistribusiBPKBHeaderId: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBHeaderId,
                    SuratDistribusiBPKBNo: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBNo,
                    SuratDistribusiBPKBSentDate: MaintainSuratDistribusiBpkb.SuratDistribusiBPKBSentDate,
                    JumlahDokumen: MaintainSuratDistribusiBpkb.JumlahDokumen,
                    PrintCount: MaintainSuratDistribusiBpkb.PrintCount,
                    TerimaDokumenSTNKBPKBStatusId: MaintainSuratDistribusiBpkb.TerimaDokumenSTNKBPKBStatusId,
                    StatusCode: MaintainSuratDistribusiBpkb.StatusCode,
                    TotalData: MaintainSuratDistribusiBpkb.TotalData,
                    listFrameNo: MaintainSuratDistribusiBpkb.listFrameNo,
                    LastModifiedDate: MaintainSuratDistribusiBpkb.LastModifiedDate,
                    LastModifiedUserId: MaintainSuratDistribusiBpkb.LastModifiedUserId
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/SAHSuratDistribusiBPKB', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
