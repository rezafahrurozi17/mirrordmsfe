angular.module('app')
.controller('ActivityWorkingCalenderController', function($scope, $http, CurrentUser, ActivityWorkingCalender,$timeout) {
    var da_json=[
				{ActivityWorkingCalenderID: "1",  NamaActivityWorkingCalender: "name 1",  TanggalActivityWorkingCalender: "12/22/2016",  KeteranganActivityWorkingCalender: "d1"},
				{ActivityWorkingCalenderID: "2",  NamaActivityWorkingCalender: "name 2",  TanggalActivityWorkingCalender: "12/22/2016",  KeteranganActivityWorkingCalender: "d2"},
				{ActivityWorkingCalenderID: "3",  NamaActivityWorkingCalender: "name 3",  TanggalActivityWorkingCalender: "12/22/2016",  KeteranganActivityWorkingCalender: "d3"},
			  ];
        
        $scope.ulala=da_json;
		
	$scope.intended_operation="None";
	$scope.Selected_ID="0";
	
	$scope.LookSelected = function (NamaActivityWorkingCalender,TanggalActivityWorkingCalender,KeteranganActivityWorkingCalender) {

	   $scope.mActivityWorkingCalender_NamaActivityWorkingCalender=NamaActivityWorkingCalender;
	   
	   //$scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=new Date(TanggalActivityWorkingCalender);
	   var TanggalActivityWorkingCalender_Corrected = new Date(TanggalActivityWorkingCalender);
	   TanggalActivityWorkingCalender_Corrected.setDate(TanggalActivityWorkingCalender_Corrected.getDate() + 1);
	   $scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=new Date(TanggalActivityWorkingCalender_Corrected);
	   
	   $scope.mActivityWorkingCalender_KeteranganActivityWorkingCalender=KeteranganActivityWorkingCalender;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.ChangeSelected = function (ActivityWorkingCalenderID,NamaActivityWorkingCalender,TanggalActivityWorkingCalender,KeteranganActivityWorkingCalender) {
	   $scope.intended_operation="Update_Ops";
	   $scope.Selected_ID=ActivityWorkingCalenderID;
	   $scope.mActivityWorkingCalender_NamaActivityWorkingCalender=NamaActivityWorkingCalender;
	   
	   //$scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=new Date(TanggalActivityWorkingCalender);
	   var TanggalActivityWorkingCalender_Corrected = new Date(TanggalActivityWorkingCalender);
	   TanggalActivityWorkingCalender_Corrected.setDate(TanggalActivityWorkingCalender_Corrected.getDate() + 1);
	   $scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=new Date(TanggalActivityWorkingCalender_Corrected);
	   
	   $scope.mActivityWorkingCalender_KeteranganActivityWorkingCalender=KeteranganActivityWorkingCalender;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Batal_Button = true;
	   $scope.Submit_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Batal_Clicked = function () {
$scope.Selected_ID="0";
	   $scope.mActivityWorkingCalender_NamaActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_KeteranganActivityWorkingCalender=null;
	   
	   $scope.intended_operation="None";
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Add_Clicked = function () {
			$scope.Selected_ID="0";
		   $scope.mActivityWorkingCalender_NamaActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_KeteranganActivityWorkingCalender=null;
		   
		   $scope.intended_operation="Insert_Ops";
		   
		   $scope.ShowParameter = !$scope.ShowParameter;
		   $scope.ShowTable = !$scope.ShowTable;
		   $scope.Submit_Button = true;
		   $scope.Batal_Button = true;
		   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Simpan_Clicked = function () {
		//put httppost,delete,cll here
		if($scope.intended_operation=="Update_Ops")
		{
			alert($scope.Selected_ID);
		}
		else if($scope.intended_operation="Insert_Ops")
		{
			alert($scope.Selected_ID);
		}
		
		//clear all input stuff first 
	   $scope.mActivityWorkingCalender_NamaActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_TanggalActivityWorkingCalender=null;
	   $scope.mActivityWorkingCalender_KeteranganActivityWorkingCalender=null;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	   $scope.intended_operation="None";
	   $scope.Selected_ID="0";
	};
		
});
