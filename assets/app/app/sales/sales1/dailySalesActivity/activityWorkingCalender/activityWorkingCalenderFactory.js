angular.module('app')
  .factory('ActivityWorkingCalender', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityWorkingCalender) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            NamaActivityWorkingCalender: ActivityWorkingCalender.NamaActivityWorkingCalender,
                                            TanggalActivityWorkingCalender: ActivityWorkingCalender.TanggalActivityWorkingCalender,
                                            KeteranganActivityWorkingCalender: ActivityWorkingCalender.KeteranganActivityWorkingCalender}]);
      },
      update: function(ActivityWorkingCalender){
        return $http.put('/api/fw/Role', [{
                                            ActivityWorkingCalenderID: ActivityWorkingCalender.ActivityWorkingCalenderID,
                                            NamaActivityWorkingCalender: ActivityWorkingCalender.NamaActivityWorkingCalender,
                                            TanggalActivityWorkingCalender: ActivityWorkingCalender.TanggalActivityWorkingCalender,
                                            KeteranganActivityWorkingCalender: ActivityWorkingCalender.KeteranganActivityWorkingCalender}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd