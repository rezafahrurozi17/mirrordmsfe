angular.module('app')
    .controller('DaftarSuspectController', function($scope, $http, CurrentUser, DaftarSuspectFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
	
	$scope.optionDaftarSuspectSuspectSearchCriteria = DaftarSuspectFactory.GetDaftarSuspectSuspectSearchDropdownOptions();
	$scope.DaftarSuspectSuspectSearchBy=$scope.optionDaftarSuspectSuspectSearchCriteria[0].SearchOptionId;
	
	$scope.optionDaftarSuspectProspectSearchCriteria = DaftarSuspectFactory.GetDaftarSuspectProspectSearchDropdownOptions();
	$scope.DaftarSuspectProspectSearchBy=$scope.optionDaftarSuspectProspectSearchCriteria[0].SearchOptionId;
	
	$scope.optionDaftarSuspectPelangganSearchCriteria = DaftarSuspectFactory.GetDaftarSuspectPelangganSearchDropdownOptions();
	$scope.DaftarSuspectPelangganSearchBy=$scope.optionDaftarSuspectPelangganSearchCriteria[0].SearchOptionId;
	
    $scope.mDaftarSuspect = null; //Model
    $scope.xRole={selected:[]};
	$scope.TemporaryDisableAssign=true;
	$scope.TemporaryDisableProspectAssign=true;
	$scope.TemporaryDisablePelangganAssign=true;
	
	$scope.DailySalesDaftarSuspectAllSelected=false;
	$scope.DailySalesDaftarProspectAllSelected=false;
    //----------------------------------
    // Get Data
    //----------------------------------

        
			
		if($scope.user.RoleName == 'KACAB')
		{
			
			DaftarSuspectFactory.getDataSuspectForKacab().then(function(res){
            $scope.listDataChecklistSuspect = res.data.Result;
            return $scope.listDataChecklistSuspect;
			});
			
			DaftarSuspectFactory.getDataProspectForKacab().then(function(res){
            $scope.listDataChecklistProspect = res.data.Result;
            return $scope.listDataChecklistProspect;
			});
			
			DaftarSuspectFactory.getDataPelangganForKacab().then(function(res){
            $scope.listDataChecklistPelanggan = res.data.Result;
            return $scope.listDataChecklistPelanggan;
			});
			
			DaftarSuspectFactory.getDataSupervisor().then(function (res) {
			$scope.OptionSupervisor = res.data.Result;
			$scope.SelectedSupervisor=$scope.OptionSupervisor[0].EmployeeId;
			$scope.SelectedSupervisorForProspect=$scope.OptionSupervisor[0].EmployeeId;
			$scope.SelectedSupervisorForPelanggan=$scope.OptionSupervisor[0].EmployeeId;
			return $scope.OptionSupervisor;
			});
		}
		else if($scope.user.RoleName == 'SUPERVISOR SALES')
		{	
			
			DaftarSuspectFactory.getDataSuspectForSupervisor().then(function(res){
            $scope.listDataChecklistSuspect = res.data.Result;
            return $scope.listDataChecklistSuspect;
			});
			
			DaftarSuspectFactory.getDataProspectForSupervisor().then(function(res){
            $scope.listDataChecklistProspect = res.data.Result;
            return $scope.listDataChecklistProspect;
			});
			
			DaftarSuspectFactory.getDataPelangganForSupervisor().then(function(res){
            $scope.listDataChecklistPelanggan = res.data.Result;
            return $scope.listDataChecklistPelanggan;
			});
			
			DaftarSuspectFactory.getDataSales().then(function (res) {
			$scope.OptionSales = res.data.Result;
			
			$scope.SelectedSalesman=$scope.OptionSales[0].EmployeeId;
			$scope.SelectedSalesmanForProspect=$scope.OptionSales[0].EmployeeId;
			$scope.SelectedSalesmanForPelanggan=$scope.OptionSales[0].EmployeeId;
			
			return $scope.OptionSales;
			});
		}
				
		$scope.ToggleDaftarSuspectSuspectSearchIcon = function () {
			$scope.ToggleDaftarSuspectSuspectSearchStuffs=!$scope.ToggleDaftarSuspectSuspectSearchStuffs;
        }
		
		$scope.ToggleDaftarSuspectProspectSearchIcon = function () {
			$scope.ToggleDaftarSuspectProspectSearchStuffs=!$scope.ToggleDaftarSuspectProspectSearchStuffs;
        }
		
		$scope.ToggleDaftarSuspectPelangganSearchIcon = function () {
			$scope.ToggleDaftarSuspectPelangganSearchStuffs=!$scope.ToggleDaftarSuspectPelangganSearchStuffs;
        }
		
		$scope.DaftarSuspectSuspectRefresh = function () {
			if($scope.user.RoleName == 'KACAB')
			{
				
				DaftarSuspectFactory.getDataSuspectForKacab().then(function(res){
				$scope.listDataChecklistSuspect = res.data.Result;
				$scope.DailySalesDaftarSuspectAllSelected=false;
				$scope.selection=[];
				return $scope.listDataChecklistSuspect;
				});
			}
			else if($scope.user.RoleName == 'SUPERVISOR SALES')
			{	
				
				DaftarSuspectFactory.getDataSuspectForSupervisor().then(function(res){
				$scope.listDataChecklistSuspect = res.data.Result;
				$scope.DailySalesDaftarSuspectAllSelected=false;
				$scope.selection=[];
				return $scope.listDataChecklistSuspect;
				});
			}
			
			$scope.DaftarSuspectSuspectSearchValue="";
			$scope.DaftarSuspectSuspectSearchBy=$scope.optionDaftarSuspectSuspectSearchCriteria[0].SearchOptionId;
		}
		
		$scope.DaftarSuspectProspectRefresh = function () {
			if($scope.user.RoleName == 'KACAB')
			{
				DaftarSuspectFactory.getDataProspectForKacab().then(function(res){
				$scope.listDataChecklistProspect = res.data.Result;
				$scope.DailySalesDaftarProspectAllSelected=false;
				$scope.selectionProspect=[];
				return $scope.listDataChecklistProspect;
				});
			}
			else if($scope.user.RoleName == 'SUPERVISOR SALES')
			{	
				DaftarSuspectFactory.getDataProspectForSupervisor().then(function(res){
				$scope.listDataChecklistProspect = res.data.Result;
				$scope.DailySalesDaftarProspectAllSelected=false;
				$scope.selectionProspect=[];
				return $scope.listDataChecklistProspect;
				});
			}
			
			$scope.DaftarSuspectProspectSearchValue="";
			$scope.DaftarSuspectProspectSearchBy=$scope.optionDaftarSuspectProspectSearchCriteria[0].SearchOptionId;
		}
		
		$scope.DaftarSuspectPelangganRefresh = function () {
			if($scope.user.RoleName == 'KACAB')
			{	
				DaftarSuspectFactory.getDataPelangganForKacab().then(function(res){
				$scope.listDataChecklistPelanggan = res.data.Result;
				$scope.DailySalesDaftarPelangganAllSelected=false;
				$scope.selectionPelanggan=[];
				return $scope.listDataChecklistPelanggan;
				});
			}
			else if($scope.user.RoleName == 'SUPERVISOR SALES')
			{		
				DaftarSuspectFactory.getDataPelangganForSupervisor().then(function(res){
				$scope.listDataChecklistPelanggan = res.data.Result;
				$scope.DailySalesDaftarPelangganAllSelected=false;
				$scope.selectionPelanggan=[];
				return $scope.listDataChecklistPelanggan;
				});
			}
			
			$scope.DaftarSuspectPelangganSearchValue="";
			$scope.DaftarSuspectPelangganSearchBy=$scope.optionDaftarSuspectPelangganSearchCriteria[0].SearchOptionId;
		}
		
		$scope.selection=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
		$scope.selection.splice(0, 1);
		
		$scope.selectionProspect=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
		$scope.selectionProspect.splice(0, 1);
		
		$scope.selectionPelanggan=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
		$scope.selectionPelanggan.splice(0, 1);
		
        $scope.selectedItems = [];
		$scope.selectedItemsProspect = [];
		$scope.selectedItemsPelanggan = [];
		
		$scope.DailySalesDaftarSuspectAllSelectedToggle = function () {
			if($scope.DailySalesDaftarSuspectAllSelected==true)
			{
				$scope.DailySalesDaftarSuspectAllSelected=false;
			}
			else if($scope.DailySalesDaftarSuspectAllSelected==false)
			{
				$scope.DailySalesDaftarSuspectAllSelected=true;
			}
			
			if($scope.DailySalesDaftarSuspectAllSelected==true)
			{
				$scope.selection=[];
				for(var i=0;i<$scope.listDataChecklistSuspect.length; i++)
				{
					$scope.toggleSelection($scope.listDataChecklistSuspect[i]);
				}
				
			}
			else if($scope.DailySalesDaftarSuspectAllSelected==false)
			{
				$scope.selection=[];
			}
		}
		
		$scope.DailySalesDaftarProspectAllSelectedToggle = function () {
			
			if($scope.DailySalesDaftarProspectAllSelected==true)
			{
				$scope.DailySalesDaftarProspectAllSelected=false;
			}
			else if($scope.DailySalesDaftarProspectAllSelected==false)
			{
				$scope.DailySalesDaftarProspectAllSelected=true;
			}
			
			if($scope.DailySalesDaftarProspectAllSelected==true)
			{
				$scope.selectionProspect=[];
				for(var i=0;i<$scope.listDataChecklistProspect.length; i++)
				{
					$scope.toggleSelectionProspect($scope.listDataChecklistProspect[i]);
				}
				
			}
			else if($scope.DailySalesDaftarProspectAllSelected==false)
			{
				$scope.selectionProspect=[];
			}
		}
		
		$scope.DailySalesDaftarPelangganAllSelectedToggle = function () {
			if($scope.DailySalesDaftarPelangganAllSelected==true)
			{
				$scope.DailySalesDaftarPelangganAllSelected=false;
			}
			else if($scope.DailySalesDaftarPelangganAllSelected==false)
			{
				$scope.DailySalesDaftarPelangganAllSelected=true;
			}
			
			if($scope.DailySalesDaftarPelangganAllSelected==true)
			{
				$scope.selectionPelanggan=[];
				for(var i=0;i<$scope.listDataChecklistPelanggan.length; i++)
				{
					$scope.toggleSelectionPelanggan($scope.listDataChecklistPelanggan[i]);
				}
				
			}
			else if($scope.DailySalesDaftarPelangganAllSelected==false)
			{
				$scope.selectionPelanggan=[];
			}
		}
		
		$scope.SearchDaftarSuspectSuspect = function () {
			try
			{
				if($scope.DaftarSuspectSuspectSearchValue!="" && (typeof $scope.DaftarSuspectSuspectSearchValue!="undefined"))
				{
					if($scope.user.RoleName == 'KACAB')
					{
						DaftarSuspectFactory.getSearchDataSuspectForKacab($scope.DaftarSuspectSuspectSearchBy,$scope.DaftarSuspectSuspectSearchValue).then(function(res){
						$scope.listDataChecklistSuspect = res.data.Result;
						$scope.DailySalesDaftarSuspectAllSelected=false;
						$scope.selection=[];
						return $scope.listDataChecklistSuspect;
						});
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{	
						DaftarSuspectFactory.getSearchDataSuspectForSupervisor($scope.DaftarSuspectSuspectSearchBy,$scope.DaftarSuspectSuspectSearchValue).then(function(res){
						$scope.listDataChecklistSuspect = res.data.Result;
						$scope.DailySalesDaftarSuspectAllSelected=false;
						$scope.selection=[];
						return $scope.listDataChecklistSuspect;
						});
					}
				}
				else
				{
					$scope.DaftarSuspectSuspectRefresh();
				}
			}
			catch(e)
			{
				$scope.DaftarSuspectSuspectRefresh();
			}
        }

		$scope.SearchDaftarSuspectProspect = function () {
			try
			{
				if($scope.DaftarSuspectProspectSearchValue!="" && (typeof $scope.DaftarSuspectProspectSearchValue!="undefined"))
				{
					if($scope.user.RoleName == 'KACAB')
					{
						DaftarSuspectFactory.getSearchDataProspectForKacab($scope.DaftarSuspectProspectSearchBy,$scope.DaftarSuspectProspectSearchValue).then(function(res){
						$scope.listDataChecklistProspect = res.data.Result;
						$scope.DailySalesDaftarProspectAllSelected=false;
						$scope.selectionProspect=[];
						return $scope.listDataChecklistProspect;
						});
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{	
						DaftarSuspectFactory.getSearchDataProspectForSupervisor($scope.DaftarSuspectProspectSearchBy,$scope.DaftarSuspectProspectSearchValue).then(function(res){
						$scope.listDataChecklistProspect = res.data.Result;
						$scope.DailySalesDaftarProspectAllSelected=false;
						$scope.selectionProspect=[];
						return $scope.listDataChecklistProspect;
						});
					}
				}
				else
				{
					$scope.DaftarSuspectProspectRefresh();
				}
			}
			catch(e)
			{
				$scope.DaftarSuspectProspectRefresh();
			}
        }
		
		$scope.SearchDaftarSuspectPelanggan = function () {
			try
			{
				if($scope.DaftarSuspectPelangganSearchValue!="" && (typeof $scope.DaftarSuspectPelangganSearchValue!="undefined"))
				{
					if($scope.user.RoleName == 'KACAB')
					{
						DaftarSuspectFactory.getSearchDataPelangganForKacab($scope.DaftarSuspectPelangganSearchBy,$scope.DaftarSuspectPelangganSearchValue).then(function(res){
						$scope.listDataChecklistPelanggan = res.data.Result;
						$scope.DailySalesDaftarPelangganAllSelected=false;
						$scope.selectionPelanggan=[];
						return $scope.listDataChecklistPelanggan;
						});
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{	
						DaftarSuspectFactory.getSearchDataPelangganForSupervisor($scope.DaftarSuspectPelangganSearchBy,$scope.DaftarSuspectPelangganSearchValue).then(function(res){
						$scope.listDataChecklistPelanggan = res.data.Result;
						$scope.DailySalesDaftarPelangganAllSelected=false;
						$scope.selectionPelanggan=[];
						return $scope.listDataChecklistPelanggan;
						});
					}
				}
				else
				{
					$scope.DaftarSuspectPelangganRefresh();
				}
			}
			catch(e)
			{
				$scope.DaftarSuspectPelangganRefresh();
			}
        }
		
		$scope.toggleSelection = function toggleSelection(Suspect) {
			var idx = $scope.selection.indexOf(Suspect);

			// Is currently selected
			if (idx > -1) {
			  $scope.selection.splice(idx, 1);
			  //$scope.DailySalesDaftarSuspectAllSelected=false;
			}

			// Is newly selected
			else {
			  $scope.selection.push(Suspect);
			}

			if ($scope.selection.length ==$scope.listDataChecklistSuspect.length) {
			  $scope.DailySalesDaftarSuspectAllSelected=true;
			}
			else if ($scope.selection.length <$scope.listDataChecklistSuspect.length) {
			  $scope.DailySalesDaftarSuspectAllSelected=false;
			}
			

		  };
		  
		$scope.toggleSelectionProspect = function toggleSelectionProspect(Prospect) {
			var idx = $scope.selectionProspect.indexOf(Prospect);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionProspect.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionProspect.push(Prospect);
			}

			if ($scope.selectionProspect.length ==$scope.listDataChecklistProspect.length) {
			  $scope.DailySalesDaftarProspectAllSelected=true;
			}
			else if ($scope.selectionProspect.length <$scope.listDataChecklistProspect.length) {
			  $scope.DailySalesDaftarProspectAllSelected=false;
			}
			

		  };
		  
		$scope.toggleSelectionPelanggan = function toggleSelectionPelanggan(Pelanggan) {
			var idx = $scope.selectionPelanggan.indexOf(Pelanggan);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionPelanggan.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionPelanggan.push(Pelanggan);
			}

			if ($scope.selectionPelanggan.length ==$scope.listDataChecklistPelanggan.length) {
			  $scope.DailySalesDaftarPelangganAllSelected=true;
			}
			else if ($scope.selectionPelanggan.length <$scope.listDataChecklistPelanggan.length) {
			  $scope.DailySalesDaftarPelangganAllSelected=false;
			}
			

		  };
         

        $scope.toggleChecked = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);
                
            } else {
                data.checked = true;
                $scope.selectedItems.push(data);
                
            }
            
        };
		
		$scope.toggleCheckedProspect = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItemsProspect.indexOf(data);
                $scope.selectedItemsProspect.splice(index, 1);
                
            } else {
                data.checked = true;
                $scope.selectedItemsProspect.push(data);
                
            }
            
        };
		
		$scope.toggleCheckedPelanggan = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItemsPelanggan.indexOf(data);
                $scope.selectedItemsPelanggan.splice(index, 1);
                
            } else {
                data.checked = true;
                $scope.selectedItemsPelanggan.push(data);
                
            }
            
        };
		
		$scope.SelectedSupervisorBrubah = function(dabrubah)
        {
			$scope.SelectedSupervisor=dabrubah;
		}
		
		$scope.SelectedSupervisorBrubahForProspect = function(dabrubah)
        {
			$scope.SelectedSupervisorForProspect=dabrubah;
		}
		
		$scope.SelectedSupervisorBrubahForPelanggan = function(dabrubah)
        {
			$scope.SelectedSupervisorForPelanggan=dabrubah;
		}
		
		$scope.SelectedSalesBrubah = function(dabrubah)
        {
			$scope.SelectedSalesman=dabrubah;
		}
		
		$scope.SelectedSalesBrubahForProspect = function(dabrubah)
        {
			$scope.SelectedSalesmanForProspect=dabrubah;
		}
		
		$scope.SelectedSalesBrubahForPelanggan = function(dabrubah)
        {
			$scope.SelectedSalesmanForPelanggan=dabrubah;
		}


        $scope.ApproveData = function()
        {
			if($scope.selection.length>0)
			{
				for(var i=0;i<$scope.selection.length; i++)
				{	
					$scope.TemporaryDisableAssign=false;
					if($scope.user.RoleName == 'KACAB')
					{
						
						$scope.selection[i]['SupervisorUserId']=$scope.SelectedSupervisor;
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{
						$scope.selection[i]['SalesId']=$scope.SelectedSalesman;
					}
					$scope.selection[i]['GenerateDate']=$scope.selection[i].GenerateDate.getFullYear()+'-'
												  +('0' + ($scope.selection[i].GenerateDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.selection[i].GenerateDate.getDate()).slice(-2)+'T'
												  +(($scope.selection[i].GenerateDate.getHours()<'10'?'0':'')+ $scope.selection[i].GenerateDate.getHours())+':'
												  +(($scope.selection[i].GenerateDate.getMinutes()<'10'?'0':'')+ $scope.selection[i].GenerateDate.getMinutes())+':'
												  +(($scope.selection[i].GenerateDate.getSeconds()<'10'?'0':'')+ $scope.selection[i].GenerateDate.getSeconds());
				}

				//yang dibawah ini 1 aja teorinya cukup
					DaftarSuspectFactory.AssignSupervisor($scope.selection).then(function () {
						if($scope.user.RoleName == 'KACAB')
						{
							DaftarSuspectFactory.getDataSuspectForKacab().then(function(res){
							$scope.listDataChecklistSuspect = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selection=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selection.splice(0, 1);
							return $scope.listDataChecklistSuspect;
							});
						}
						else if($scope.user.RoleName == 'SUPERVISOR SALES')
						{	
							DaftarSuspectFactory.getDataSuspectForSupervisor().then(function(res){
							$scope.listDataChecklistSuspect = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selection=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selection.splice(0, 1);
							return $scope.listDataChecklistSuspect;
							});
						}
						$scope.TemporaryDisableAssign=true;
						$scope.selectedItems = [];	
					});
			}
			else
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Pilih paling tidak 1 data.",
						type: 'warning'
					}
				);
			}
        };
		
		$scope.ApproveProspectData = function()
        {
			if($scope.selectionProspect.length>0)
			{
				for(var i=0;i<$scope.selectionProspect.length; i++)
				{	
				
					$scope.TemporaryDisableProspectAssign=false;
					if($scope.user.RoleName == 'KACAB')
					{
						$scope.selectionProspect[i]['SupervisorUserId']=$scope.SelectedSupervisorForProspect;
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{
						$scope.selectionProspect[i]['SalesId']=$scope.SelectedSalesmanForProspect;
					}
					
					$scope.selectionProspect[i]['GenerateDate']=$scope.selectionProspect[i].GenerateDate.getFullYear()+'-'
												  +('0' + ($scope.selectionProspect[i].GenerateDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.selectionProspect[i].GenerateDate.getDate()).slice(-2)+'T'
												  +(($scope.selectionProspect[i].GenerateDate.getHours()<'10'?'0':'')+ $scope.selectionProspect[i].GenerateDate.getHours())+':'
												  +(($scope.selectionProspect[i].GenerateDate.getMinutes()<'10'?'0':'')+ $scope.selectionProspect[i].GenerateDate.getMinutes())+':'
												  +(($scope.selectionProspect[i].GenerateDate.getSeconds()<'10'?'0':'')+ $scope.selectionProspect[i].GenerateDate.getSeconds());
				}
				
				//yang dibawah ini 1 aja teorinya cukup
					DaftarSuspectFactory.AssignSupervisorForProspect($scope.selectionProspect).then(function () {
						if($scope.user.RoleName == 'KACAB')
						{
							DaftarSuspectFactory.getDataProspectForKacab().then(function(res){
							$scope.listDataChecklistProspect = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selectionProspect=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selectionProspect.splice(0, 1);
							return $scope.listDataChecklistProspect;
							});
						}
						else if($scope.user.RoleName == 'SUPERVISOR SALES')
						{	
							DaftarSuspectFactory.getDataProspectForSupervisor().then(function(res){
							$scope.listDataChecklistProspect = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selectionProspect=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selectionProspect.splice(0, 1);
							return $scope.listDataChecklistProspect;
							});
						}
						$scope.TemporaryDisableProspectAssign=true;
						$scope.selectedItemsProspect = [];	
					});
			}
			else
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Pilih paling tidak 1 data.",
						type: 'warning'
					}
				);
			}
        };
		
		$scope.ApprovePelangganData = function()
        {
			if($scope.selectionPelanggan.length>0)
			{
				for(var i=0;i<$scope.selectionPelanggan.length; i++)
				{	
					$scope.TemporaryDisablePelangganAssign=false;
					if($scope.user.RoleName == 'KACAB')
					{
						$scope.selectionPelanggan[i]['SupervisorUserId']=$scope.SelectedSupervisorForPelanggan;
					}
					else if($scope.user.RoleName == 'SUPERVISOR SALES')
					{
						$scope.selectionPelanggan[i]['SalesId']=$scope.SelectedSalesmanForPelanggan;
					}
					$scope.selectionPelanggan[i]['GenerateDate']=$scope.selectionPelanggan[i].GenerateDate.getFullYear()+'-'
												  +('0' + ($scope.selectionPelanggan[i].GenerateDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.selectionPelanggan[i].GenerateDate.getDate()).slice(-2)+'T'
												  +(($scope.selectionPelanggan[i].GenerateDate.getHours()<'10'?'0':'')+ $scope.selectionPelanggan[i].GenerateDate.getHours())+':'
												  +(($scope.selectionPelanggan[i].GenerateDate.getMinutes()<'10'?'0':'')+ $scope.selectionPelanggan[i].GenerateDate.getMinutes())+':'
												  +(($scope.selectionPelanggan[i].GenerateDate.getSeconds()<'10'?'0':'')+ $scope.selectionPelanggan[i].GenerateDate.getSeconds());
				}
				
				//yang dibawah ini 1 aja teorinya cukup
					DaftarSuspectFactory.AssignSupervisorForPelanggan($scope.selectionPelanggan).then(function () {
						if($scope.user.RoleName == 'KACAB')
						{
							DaftarSuspectFactory.getDataPelangganForKacab().then(function(res){
							$scope.listDataChecklistPelanggan = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selectionPelanggan=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selectionPelanggan.splice(0, 1);
							return $scope.listDataChecklistPelanggan;
							});
						}
						else if($scope.user.RoleName == 'SUPERVISOR SALES')
						{	
							DaftarSuspectFactory.getDataPelangganForSupervisor().then(function(res){
							$scope.listDataChecklistPelanggan = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data sudah berhasil didistribusikan.",
									type: 'success'
								}
							);
							$scope.selectionPelanggan=[{ SuspectId: null,
											OutletId: null,
											SuspectCode: null,
											SuspectName: null,
											InstanceName: null,
											PhoneNumber: null,
											HP: null,
											Email: null,
											Address: null,
											Note: null,
											CustomerCategoryId: null,
											VehicleModelId: null,
											PurchasePurposeId: null,
											PurchaseTimeId: null,
											KacabId: null,
											SupervisorUserId: null,
											SalesId: null,
											DataSourceId: null,
											SuspectStatusId: null,
                                            GenerateDate: null}];
							$scope.selectionPelanggan.splice(0, 1);
							return $scope.listDataChecklistPelanggan;
							});
						}
						$scope.TemporaryDisablePelangganAssign=true;
						$scope.selectedItemsPelanggan = [];	
					});
			}
			else
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Pilih paling tidak 1 data.",
						type: 'warning'
					}
				);
			}
        };

        $scope.RejectData = function()
        {
            for(var i=0;i<$scope.selectedItems.length; i++)
            {
                $scope.selectedItems[i].ApprovalFlag = 0;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];

        };




    var gridData = [];
    $scope.getData = function() {
        DaftarSuspect.getData()
        .then(
            function(res){
                gridData = [];
				var da_json=[
        {AlasanPengecualianPengirimanId: "1",  NamaAlasanPengecualianPengiriman: "name 1",  InputByAdmin: "d1",  InputBySalesman: "d1"},
        {AlasanPengecualianPengirimanId: "2",  NamaAlasanPengecualianPengiriman: "name 2",  InputByAdmin: "d2",  InputBySalesman: "d2"},
        {AlasanPengecualianPengirimanId: "3",  NamaAlasanPengecualianPengiriman: "name 3",  InputByAdmin: "d3",  InputBySalesman: "d3"},
      ];
        
        $scope.grid.data=da_json;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Terjadi error.",
						type: 'danger'
					}
				);
            }
        );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        //console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        //console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'SuspectId', width:'7%', visible:false },
            { name:'name', field:'NamaAlasanPengecualianPengiriman' },
            { name:'InputByAdmin',  field: 'InputByAdmin' },
												{ name:'InputBySalesman',  field: 'InputBySalesman' },
        ]
				};
				

					$scope.CustomerLoadMore = function () {
						var tempStart = $scope.listDataChecklistPelanggan.length + 1;
						var tempLimit = 100;

						var da_filter = '?start=' + tempStart + '&limit=' + tempLimit;


						if ($scope.DaftarSuspectPelangganSearchValue != "" && (typeof $scope.DaftarSuspectPelangganSearchValue != "undefined")) {
							console.log('ini kalo filternya keisi');

							if ($scope.user.RoleName == 'KACAB') {

								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataPelangganForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Customer = res.data.Result;
									$scope.TotalCustomer = res.data.Total;
									for (var i in $scope.Temp_List_Customer) {
										$scope.listDataChecklistPelanggan.push($scope.Temp_List_Customer[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {
								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataPelangganForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Customer = res.data.Result;
									$scope.TotalCustomer = res.data.Total;
									for (var i in $scope.Temp_List_Customer) {
										$scope.listDataChecklistPelanggan.push($scope.Temp_List_Customer[i]);
									}

								});
							}
						}else {
							console.log('ini kalo filternya kosong');
							if ($scope.user.RoleName == 'KACAB') {


								DaftarSuspectFactory.getSearchDataPelangganForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Customer = res.data.Result;
									$scope.TotalCustomer = res.data.Total;
									for (var i in $scope.Temp_List_Customer) {
										$scope.listDataChecklistPelanggan.push($scope.Temp_List_Customer[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {

								DaftarSuspectFactory.getSearchDataPelangganForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Customer = res.data.Result;
									$scope.TotalCustomer = res.data.Total;
									for (var i in $scope.Temp_List_Customer) {
										$scope.listDataChecklistPelanggan.push($scope.Temp_List_Customer[i]);
									}

								});
							}
						}



					}

					$scope.ProspectLoadMore = function () {
						var tempStart = $scope.listDataChecklistProspect.length + 1;
						var tempLimit = 100;

						var da_filter = '?start=' + tempStart + '&limit=' + tempLimit;


						if ($scope.DaftarSuspectPelangganSearchValue != "" && (typeof $scope.DaftarSuspectPelangganSearchValue != "undefined")) {
							console.log('ini kalo filternya keisi');

							if ($scope.user.RoleName == 'KACAB') {

								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataProspectForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Prospect = res.data.Result;
									$scope.TotalProspect = res.data.Total;
									for (var i in $scope.Temp_List_Prospect) {
										$scope.listDataChecklistProspect.push($scope.Temp_List_Prospect[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {
								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataProspectForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Prospect = res.data.Result;
									$scope.TotalProspect = res.data.Total;
									for (var i in $scope.Temp_List_Prospect) {
										$scope.listDataChecklistProspect.push($scope.Temp_List_Prospect[i]);
									}

								});
							}
						} else {
							console.log('ini kalo filternya kosong');
							if ($scope.user.RoleName == 'KACAB') {


								DaftarSuspectFactory.getSearchDataProspectForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Prospect = res.data.Result;
									$scope.TotalProspect = res.data.Total;
									for (var i in $scope.Temp_List_Prospect) {
										$scope.listDataChecklistProspect.push($scope.Temp_List_Prospect[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {

								DaftarSuspectFactory.getSearchDataProspectForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Prospect = res.data.Result;
									$scope.TotalProspect = res.data.Total;
									for (var i in $scope.Temp_List_Prospect) {
										$scope.listDataChecklistProspect.push($scope.Temp_List_Prospect[i]);
									}

								});
							}
						}



					}

					$scope.SuspectLoadMore = function () {
						var tempStart = $scope.listDataChecklistSuspect.length + 1;
						var tempLimit = 100;

						var da_filter = '?start=' + tempStart + '&limit=' + tempLimit;


						if ($scope.DaftarSuspectPelangganSearchValue != "" && (typeof $scope.DaftarSuspectPelangganSearchValue != "undefined")) {
							console.log('ini kalo filternya keisi');

							if ($scope.user.RoleName == 'KACAB') {

								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataSuspectForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Suspect = res.data.Result;
									$scope.TotalSuspect = res.data.Total;
									for (var i in $scope.Temp_List_Suspect) {
										$scope.listDataChecklistSuspect.push($scope.Temp_List_Suspect[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {
								da_filter += "&filterData=" + $scope.DaftarSuspectPelangganSearchBy + "|" + $scope.DaftarSuspectPelangganSearchValue;

								DaftarSuspectFactory.getSearchDataSuspectForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Suspect = res.data.Result;
									$scope.TotalSuspect = res.data.Total;
									for (var i in $scope.Temp_List_Suspect) {
										$scope.listDataChecklistSuspect.push($scope.Temp_List_Suspect[i]);
									}

								});
							}
						} else {
							console.log('ini kalo filternya kosong');
							if ($scope.user.RoleName == 'KACAB') {


								DaftarSuspectFactory.getSearchDataSuspectForKacabLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Suspect = res.data.Result;
									$scope.TotalSuspect = res.data.Total;
									for (var i in $scope.Temp_List_Suspect) {
										$scope.listDataChecklistSuspect.push($scope.Temp_List_Suspect[i]);
									}

								});
							}
							else if ($scope.user.RoleName == 'SUPERVISOR SALES') {

								DaftarSuspectFactory.getSearchDataSuspectForSupervisorLoadMore(da_filter).then(function (res) {

									$scope.Temp_List_Suspect = res.data.Result;
									$scope.TotalSuspect = res.data.Total;
									for (var i in $scope.Temp_List_Suspect) {
										$scope.listDataChecklistSuspect.push($scope.Temp_List_Suspect[i]);
									}

								});
							}
						}



					}
});
