angular.module('app')
  .factory('DaftarSuspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
	  GetDaftarSuspectSuspectSearchDropdownOptions: function () {
        // var da_json = [
          // { SearchOptionId: "SuspectName", SearchOptionName: "Suspect Name" },
		  // { SearchOptionId: "Address", SearchOptionName: "Address" },
		  // { SearchOptionId: "PhoneNumber", SearchOptionName: "Phone Number" },
		  // { SearchOptionId: "HP", SearchOptionName: "HP" },
        // ];
		var da_json = [
          { SearchOptionId: "SuspectName", SearchOptionName: "Nama" },
		  { SearchOptionId: "HP", SearchOptionName: "No Handphone" },
		  { SearchOptionId: "DataSourceName", SearchOptionName: "Sumber Data" },
        ];
        var res=da_json
        return res;
      },
	  GetDaftarSuspectProspectSearchDropdownOptions: function () {
        // var da_json = [
          // { SearchOptionId: "ProspectName", SearchOptionName: "Prospect Name" },
		  // { SearchOptionId: "Address", SearchOptionName: "Address" },
		  // { SearchOptionId: "PhoneNumber", SearchOptionName: "Phone Number" },
		  // { SearchOptionId: "HP", SearchOptionName: "HP" },
        // ];
		
		var da_json = [
          { SearchOptionId: "ProspectName", SearchOptionName: "Nama" },
		  { SearchOptionId: "HP", SearchOptionName: "No Handphone" },
        ];
        var res=da_json
        return res;
      },
	  GetDaftarSuspectPelangganSearchDropdownOptions: function () {
        // var da_json = [
          // { SearchOptionId: "CustomerName", SearchOptionName: "Customer Name" },
		  // { SearchOptionId: "Address", SearchOptionName: "Address" },
		  // { SearchOptionId: "TelfonInstitusi", SearchOptionName: "Telfon Institusi" },
		  // { SearchOptionId: "Handphone", SearchOptionName: "Handphone" },
        // ];
		
		var da_json = [
          { SearchOptionId: "CustomerName", SearchOptionName: "Nama" },
		  { SearchOptionId: "Handphone", SearchOptionName: "No Handphone" },
        ];
        var res=da_json
        return res;
      },
      getDataSuspectForKacab: function() {
        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=KacabId|'+KacabId);
		var res=$http.get('/api/sales/SDSSuspectListForKacab');
        //console.log('res=>',res);
        return res;
      },
	  getDataSuspectForSupervisor: function() {

        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=SupervisorUserId|'+SupervisorUserId);
        var res=$http.get('/api/sales/SDSSuspectListForSupervisor');
		//console.log('res=>',res);
        return res;
      },
	  
	  getSearchDataSuspectForKacab: function(SearchBy,SearchValue) {
      var res=$http.get('/api/sales/SDSSuspectListForKacab/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
      //console.log('res=>',res);
      return res;
    },

    getSearchDataSuspectForKacabLoadMore: function(param) {
      var res=$http.get('/api/sales/SDSSuspectListForKacab/'+param);
      //console.log('res=>',res);
      return res;
    },

	  getSearchDataSuspectForSupervisor: function(SearchBy,SearchValue) {
      var res=$http.get('/api/sales/SDSSuspectListForSupervisor/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
		  //console.log('res=>',res);
      return res;
    },

    getSearchDataSuspectForSupervisorLoadMore: function(param) {
      var res=$http.get('/api/sales/SDSSuspectListForSupervisor/'+ param);
		  //console.log('res=>',res);
      return res;
    },
	  
	  getSearchDataProspectForKacab: function(SearchBy,SearchValue) {
      var res=$http.get('/api/sales/SDSProspectListForKacab/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
      //console.log('res=>',res);
      return res;
    },

    getSearchDataProspectForKacabLoadMore: function(param) {
      var res=$http.get('/api/sales/SDSProspectListForKacab/' + param);
      //console.log('res=>',res);
      return res;
    },
	  getSearchDataProspectForSupervisor: function(SearchBy,SearchValue) {
      var res=$http.get('/api/sales/SDSProspectListForSupervisor/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
      //console.log('res=>',res);
      return res;
    },

    getSearchDataProspectForSupervisorLoadMore: function(param) {
      var res=$http.get('/api/sales/SDSProspectListForSupervisor/' + param);
      //console.log('res=>',res);
      return res;
    },
	  
	  getSearchDataPelangganForKacab: function(SearchBy,SearchValue) {
		var res=$http.get('/api/sales/SDSCustomerListForKacab/?start=1&limit=100&filterData='+SearchBy+'|'+SearchValue);
        //console.log('res=>',res);
        return res;
      },

    getSearchDataPelangganForKacabLoadMore: function (param) {
      //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=KacabId|'+KacabId);
      var res = $http.get('/api/sales/SDSCustomerListForKacab/' + param);
      // var res = $http.get('/api/sales/SDSCustomerListForKacab');
      //console.log('res=>',res);
      return res;
    },
	  getSearchDataPelangganForSupervisor: function(SearchBy,SearchValue) {
        var res=$http.get('/api/sales/SDSCustomerListForSupervisor/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
		//console.log('res=>',res);
        return res;
    },

      getSearchDataPelangganForSupervisorLoadMore: function (param) {
      var res = $http.get('/api/sales/SDSCustomerListForSupervisor/'+ param);
      //console.log('res=>',res);
      return res;
    },
	  
	  getDataProspectForKacab: function() {
        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=KacabId|'+KacabId);
		var res=$http.get('/api/sales/SDSProspectListForKacab');
        //console.log('res=>',res);
        return res;
      },
	  getDataProspectForSupervisor: function() {

        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=SupervisorUserId|'+SupervisorUserId);
        var res=$http.get('/api/sales/SDSProspectListForSupervisor');
		//console.log('res=>',res);
        return res;
      },
	    
	  getDataPelangganForKacab: function() {
        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=KacabId|'+KacabId);
      var res = $http.get('/api/sales/SDSCustomerListForKacab?start=1&limit=100');
      // var res = $http.get('/api/sales/SDSCustomerListForKacab');
        //console.log('res=>',res);
        return res;
      },


    
	  getDataPelangganForSupervisor: function() {

        //var res=$http.get('/api/sales/CSuspectList/?start=1&limit=10000&filterData=SupervisorUserId|'+SupervisorUserId);
        var res=$http.get('/api/sales/SDSCustomerListForSupervisor');
		//console.log('res=>',res);
        return res;
      },
	  
	  getDataSupervisor: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=SupervisorSales');
        //console.log('res=>',res);
        return res;
      },
	  
	  getDataSales: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=salesman');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityAlasanPengecualianPengiriman) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
											InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
                                            InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      },
      AssignSupervisor: function(Suspect){
		
		if(currentUser.RoleName == "KACAB")
		{
			return $http.put('/api/sales/SDSSuspectListForKacab',Suspect );
		}
		else if(currentUser.RoleName == "SUPERVISOR SALES")
		{
			return $http.put('/api/sales/SDSSuspectListForSupervisor',Suspect );
		}
	  
        
      },
	  
	  AssignSupervisorForProspect: function(Suspect){
		
		if(currentUser.RoleName == "KACAB")
		{
			return $http.put('/api/sales/SDSProspectListForKacab',Suspect );
		}
		else if(currentUser.RoleName == "SUPERVISOR SALES")
		{
			return $http.put('/api/sales/SDSProspectListForSupervisor',Suspect );
		}
	  
        
      },
	  
	  AssignSupervisorForPelanggan: function(Suspect){
		
		if(currentUser.RoleName == "KACAB")
		{
			return $http.put('/api/sales/SDSCustomerListForKacab',Suspect );
		}
		else if(currentUser.RoleName == "SUPERVISOR SALES")
		{
			return $http.put('/api/sales/SDSCustomerListForSupervisor',Suspect );
		}
	  
        
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd