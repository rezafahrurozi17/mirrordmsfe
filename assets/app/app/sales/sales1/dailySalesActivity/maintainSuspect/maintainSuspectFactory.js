angular.module('app')
    .factory('MaintainSuspectFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/sales/SDSSuspectForAdmin');
                //console.log('res=>',res);
                return res;
            },
			getDataSearch: function(SearchBy,SearchValue) {
                var res = $http.get('/api/sales/SDSSuspectForAdmin/?filterData='+SearchBy+'|'+SearchValue);
                return res;
            },

            getParentFilter: function() {
                var res = $http.get('/api/sales/CSuspectDataFilter');
                return res;
            },

            create: function(MaintainSuspect) {
                return $http.post('/api/sales/SDSSuspectForAdmin', [{
                    //AppId: 1,
                    NamaAlasanPengecualianPengiriman: MaintainSuspect.NamaAlasanPengecualianPengiriman,
                    InputByAdmin: MaintainSuspect.InputByAdmin,
                    InputBySalesman: MaintainSuspect.InputBySalesman
                }]);
            },

            GenerateSuspect: function(DataFilterId, DataGeneratorId) {
                return $http.post('/api/sales/SDSSuspectGenerate/?dataFilterId=' + DataFilterId + "&dataGeneratorId=" + DataGeneratorId, [{}]);
            },

            UploadExcelDataToServer: function(MaintainSuspect) {
                var res = $http.post('/api/sales/SDSSuspectUpload3ForAdmin', MaintainSuspect);
                return res;
                //tadinya pake /api/sales/SDSSuspectUploadForAdmin
            },
            update: function(Suspect) {
                return $http.put('/api/sales/SDSSuspectForAdmin', [{
                    SuspectId: Suspect.SuspectId,
                    OutletId: Suspect.OutletId,
                    SuspectCode: Suspect.SuspectCode,
                    SuspectName: Suspect.SuspectName,
                    InstanceName: Suspect.InstanceName,
                    PhoneNumber: Suspect.PhoneNumber,
                    HP: Suspect.HP,
                    Email: Suspect.Email,
                    Address: Suspect.Address,
                    Note: Suspect.Note,
                    CustomerCategoryId: Suspect.CustomerCategoryId,
                    VehicleModelId: Suspect.VehicleModelId,
                    PurchasePurposeId: Suspect.PurchasePurposeId,
                    PurchaseTimeId: Suspect.PurchaseTimeId,
                    KacabId: Suspect.KacabId,
                    SupervisorUserId: Suspect.SupervisorUserId,
                    SalesId: Suspect.SalesId,
                    DataSourceId: Suspect.DataSourceId,
                    SuspectStatusId: Suspect.SuspectStatusId,
                    GenerateDate: Suspect.GenerateDate
                }]);
            },
			
			GetMaintainSuspectSearchDropdownOptions: function () {
        
				var da_json = [
				  { SearchOptionId: "SuspectName", SearchOptionName: "Nama" },
				  { SearchOptionId: "HP", SearchOptionName: "HP" },
				  { SearchOptionId: "Email", SearchOptionName: "Email" },
				  { SearchOptionId: "Address", SearchOptionName: "Address" },
				  { SearchOptionId: "DataSourceName", SearchOptionName: "Sumber Data" },
				];

				var res=da_json

				return res;
			  },

            AssignKacab: function(Suspect) {
                return $http.put('/api/sales/SDSSuspectForAdmin', Suspect);
            },
            delete: function(id) {
                return $http.delete('/api/sales/SDSSuspectForAdmin', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd