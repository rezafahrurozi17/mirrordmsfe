angular.module('app')
    .controller('GenerateMaintainSuspectController', function($scope, $http, CurrentUser, GenerateMaintainSuspectFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mGenerateMaintainSuspect = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            GenerateMaintainSuspectFactory.getData()
                .then(
                    function(res) {
                        gridData = [];
                        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                        //$scope.grid.data = res.data.Result;nanti balikin


                        var da_json = [
                            { NamaSuspectId: "1", NamaSuspect: "Albertus", NoTelp: "035-860-4333", Email: "eu.nulla@ultrice.com", Alamat: "1045 Aeneon Avenue", SumberData: "TPM" },
                            { NamaSuspectId: "2", NamaSuspect: "Raditya", NoTelp: "021-878-231", Email: "", Alamat: "112-321 Odio Rd", SumberData: "Service" },
                            { NamaSuspectId: "3", NamaSuspect: "Arianto", NoTelp: "022-32746-232", Email: "bb@bb.com", Alamat: "", SumberData: "Service" },
                            { NamaSuspectId: "4", NamaSuspect: "Galvin Wong", NoTelp: "031-0995-323", Email: "", Alamat: "144 Nulla Street", SumberData: "TVC" },
                            { NamaSuspectId: "5", NamaSuspect: "Hop Stewart", NoTelp: "123-432-4234", Email: "", Alamat: "157-2400 Massa Street", SumberData: "Event" },
                        ];

                        $scope.grid.data = da_json;


                        //console.log("ActivityAlasanPengecualianPengiriman=>",res.data.Result);nanti balikin
                        //console.log("grid data=>",$scope.grid.data);
                        //$scope.roleData = res.data;
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
                console.log("onSelectRows=>", rows);
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'no', field: 'NamaSuspectId' },
                { name: 'nama suspect', field: 'NamaSuspect' },
                { name: 'no. telepon', field: 'NoTelp' },
                { name: 'email', field: 'Email' },
                { name: 'alamat', field: 'Alamat' },
                { name: 'sumber data', field: 'SumberData' }
            ]
        };
    });