angular.module('app')
  .factory('ViewTaskListFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityAlasanPengecualianPengiriman) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
											InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
                                            InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      },
      update: function(ActivityAlasanPengecualianPengiriman){
        return $http.put('/api/fw/Role', [{
                                            AlasanPengecualianPengirimanId: ActivityAlasanPengecualianPengiriman.AlasanPengecualianPengirimanId,
											NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
                                            InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
                                            InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd