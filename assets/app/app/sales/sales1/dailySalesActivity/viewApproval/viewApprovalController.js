angular.module('app')
    .controller('ViewApprovalController', function($scope, $http, CurrentUser, ViewApprovalFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mViewApproval = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        ViewApprovalFactory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                //$scope.grid.data = res.data.Result;nanti balikin
				
				
				var da_json=[
        {AlasanPengecualianPengirimanId: "1",  NamaAlasanPengecualianPengiriman: "name 1",  InputByAdmin: "d1",  InputBySalesman: "d1"},
        {AlasanPengecualianPengirimanId: "2",  NamaAlasanPengecualianPengiriman: "name 2",  InputByAdmin: "d2",  InputBySalesman: "d2"},
        {AlasanPengecualianPengirimanId: "3",  NamaAlasanPengecualianPengiriman: "name 3",  InputByAdmin: "d3",  InputBySalesman: "d3"},
      ];
        
        $scope.grid.data=da_json;
				
				
                //console.log("ActivityAlasanPengecualianPengiriman=>",res.data.Result);nanti balikin
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'AlasanPengecualianPengirimanId', width:'7%', visible:false },
            { name:'name', field:'NamaAlasanPengecualianPengiriman' },
            { name:'InputByAdmin',  field: 'InputByAdmin' },
			{ name:'InputBySalesman',  field: 'InputBySalesman' },
        ]
    };
});
