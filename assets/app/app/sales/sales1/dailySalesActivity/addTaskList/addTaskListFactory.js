angular.module('app')
  .factory('AddTaskListFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
	//alert(currentUser.RoleName);
    return {
      getData: function(param) {
		if(currentUser.RoleName == "SALES")
		{	
			var str = param;
			var AlreadyReplaced1 = str.replace("&StatusTaskId=1", "");
			var AlreadyReplaced2 = AlreadyReplaced1.replace("&ActivityTypeId=3", "");
			
			if(param.indexOf('limit')==-1)
			{
				var res=$http.get('/api/sales/SDSActivitySalesmanAll/Filter/?Date='+param);
				return res;
			}
			else
			{
				var res=$http.get('/api/sales/SDSActivitySalesmanAll/Filter'+AlreadyReplaced2);
				return res;
			}
			
			
			
			
		}
		else
		{
			var str = param;
			var AlreadyReplaced1 = str.replace("&StatusTaskId=1", "");
			var AlreadyReplaced2 = AlreadyReplaced1.replace("&ActivityTypeId=3", "");
			
			if(param.indexOf('limit')==-1)
			{
				var res=$http.get('/api/sales/SDSActivitySupervisorAll/Filter/?Date='+param);
				return res;
			}
			else
			{
				var res=$http.get('/api/sales/SDSActivitySupervisorAll/Filter'+AlreadyReplaced2);
				return res;
			}
			
			
		}
        

        
      },
	  
	  getDataForCalendar: function() {
		if(currentUser.RoleName == "SALES")
		{	//kalo status task id 0 delay kalo 1 open/reschedule kalo 2 complete/cancel
			var res=$http.get('/api/sales/SDSActivitySalesmanAll/StartDate/?statusTaskId=1');
			return res;
		}
		else
		{
			var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=1');
			return res;
		}
        
      },
	  
	  getDataForCalendarCompleteOrCancel: function() {
		if(currentUser.RoleName == "SALES")
		{
			var res=$http.get('/api/sales/SDSActivitySalesmanAll/StartDate/?statusTaskId=2');
			return res;
		}
		else
		{
			var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=2');
			return res;
		}
        
      },
	  
	  getDataForCalendarDelay: function() {
		if(currentUser.RoleName == "SALES")
		{
			var res=$http.get('/api/sales/SDSActivitySalesmanAll/StartDate/?statusTaskId=0');
			return res;
		}
		else
		{
			var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=0');
			return res;
		}
        
      },
	  
	  getDataForFilterByCalendar: function(StartDate) {
		if(currentUser.RoleName == "SALES")
		{
			var res=$http.get('/api/sales/SDSActivitySalesmanAll/Filter/?Date='+StartDate);
			return res;
		}
		else
		{
			var res=$http.get('/api/sales/SDSActivitySupervisorAll/Filter/?Date='+StartDate);
			return res;
		}
        
      },
	  
	  getDataIfSales: function() {
        var res=$http.get('/api/sales/SDSActivitySalesmanAll/Filter');

        return res;
      },
	  getStatusTask: function() {
        var res=$http.get('/api/sales/SDSStatusTask');

        return res;
      },
			
	  getActivityPredefineRespondParent: function(ActivityTypeId,TaskTypeId) {
                var res=$http.get('/api/sales/ActivityPreDefineRespondNew/?&ParentId=0&ActivityTypeId='+ActivityTypeId+'&TaskTypeId='+TaskTypeId);
                return res;
            },
		
	  getActivityPredefineRespondChildThingy: function(ParentId,ActivityTypeId,TaskTypeId) {
                var res=$http.get('/api/sales/ActivityPreDefineRespondNew/?&ParentId='+ParentId+'&ActivityTypeId='+ActivityTypeId+'&TaskTypeId='+TaskTypeId);
                return res;
            },
			
	  getActivityPredefineRespond: function() {
                var res=$http.get('/api/sales/ActivityPreDefineRespondNew');
                return res;
            },		
			
      getDataProspect: function () {
        //var res = $http.get('/api/sales/SDSGetProspectList');
        var res = $http.get('/api/sales/SDSGetProspectList/?SalesId='+currentUser.EmployeeId);

        return res;
      },
	  
	  getDataProspectIndividual: function (ProspectId) {
        var res = $http.get('/api/sales/SDSGetProspectList/?ProspectId='+ProspectId);
        

        return res;
      },
	  
	  GetProspectSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "1", SearchOptionName: "Nama Prospect/Nama Pelanggan" },
          { SearchOptionId: "2", SearchOptionName: "Kode Prospect" }
        ];

        var res=da_json

        return res;
      },
	  
	  GetSearchProspectByName: function(SearchValue) {
        var res=$http.get('/api/sales/SDSGetProspectList/?prospectSPKCode=&ProspectName='+SearchValue+"&SalesId="+currentUser.EmployeeId);

        return res;
      },
	  
	  GetSearchProspectByProspectCode: function(SearchValue) {
        var res=$http.get('/api/sales/SDSGetProspectList/?prospectSPKCode='+SearchValue+'&ProspectName='+"&SalesId="+currentUser.EmployeeId);

        return res;
      },
	
	  
	  getDataSales: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=salesman');
        //console.log('res=>',res);
        return res;
      },
	  getTipeAkivitas: function() {
        var res=$http.get('/api/sales/SDSActivityType')
        return res;
      },
	  getTipeTugas: function() {
        var res=$http.get('/api/sales/SDSActivityTaskType')
        return res;
      },
	  
      create: function(AddTaskList) {
		var Da_ReminderDate="";
		try
		{
			Da_ReminderDate= AddTaskList.ReminderDate.getFullYear()+'-'
												  +('0' + (AddTaskList.ReminderDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + AddTaskList.ReminderDate.getDate()).slice(-2)+'T'
												  +((AddTaskList.ReminderDate.getHours()<'10'?'0':'')+ AddTaskList.ReminderDate.getHours())+':'
												  +((AddTaskList.ReminderDate.getMinutes()<'10'?'0':'')+ AddTaskList.ReminderDate.getMinutes())+':'
												  +((AddTaskList.ReminderDate.getSeconds()<'10'?'0':'')+ AddTaskList.ReminderDate.getSeconds());
		}
		catch(e1)
		{
			Da_ReminderDate=null;
		}
		
		if(currentUser.RoleName == "SALES")
		{
			return $http.post('/api/sales/SDSActivitySalesman', [{
                                            TaskCode: AddTaskList.TaskCode,
											TaskName: AddTaskList.TaskName,
											TaskDescription: AddTaskList.TaskDescription,
											StartDate: AddTaskList.StartDate.getFullYear()+'-'
											+('0' + (AddTaskList.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.StartDate.getDate()).slice(-2)+'T'
											+((AddTaskList.StartDate.getHours()<'10'?'0':'')+ AddTaskList.StartDate.getHours())+':'
											+((AddTaskList.StartDate.getMinutes()<'10'?'0':'')+ AddTaskList.StartDate.getMinutes())+':'
											+((AddTaskList.StartDate.getSeconds()<'10'?'0':'')+ AddTaskList.StartDate.getSeconds()),
											
											StartTime: AddTaskList.StartTime,
											DueDate: AddTaskList.DueDate.getFullYear()+'-'
											+('0' + (AddTaskList.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.DueDate.getDate()).slice(-2)+'T'
											+((AddTaskList.DueDate.getHours()<'10'?'0':'')+ AddTaskList.DueDate.getHours())+':'
											+((AddTaskList.DueDate.getMinutes()<'10'?'0':'')+ AddTaskList.DueDate.getMinutes())+':'
											+((AddTaskList.DueDate.getSeconds()<'10'?'0':'')+ AddTaskList.DueDate.getSeconds()),
											
                                            DueTime: AddTaskList.DueTime,
											ReminderDate: Da_ReminderDate,
											
											ReminderTime: AddTaskList.ReminderTime,
                                            FullDayActivityBit: AddTaskList.FullDayActivityBit,
											ImportantTaskBit: AddTaskList.ImportantTaskBit,
											ActionName: AddTaskList.ActionName,
                                            MovingTaskDesc: AddTaskList.MovingTaskDesc,
											SalesId: AddTaskList.SalesId,
											ProspectId: AddTaskList.ProspectId,
                                            ActivityTypeId: AddTaskList.ActivityTypeId,
											TaskTypeId: AddTaskList.TaskTypeId,
											ActivityPreDefineRespondId: AddTaskList.ActivityPreDefineRespondId,
                                            StatusTaskId: AddTaskList.StatusTaskId
											}]);
		}
		else
		{
			return $http.post('/api/sales/SDSActivitySupervisor', [{
                                            TaskCode: AddTaskList.TaskCode,
											TaskName: AddTaskList.TaskName,
											TaskDescription: AddTaskList.TaskDescription,
											StartDate: AddTaskList.StartDate.getFullYear()+'-'
											+('0' + (AddTaskList.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.StartDate.getDate()).slice(-2)+'T'
											+((AddTaskList.StartDate.getHours()<'10'?'0':'')+ AddTaskList.StartDate.getHours())+':'
											+((AddTaskList.StartDate.getMinutes()<'10'?'0':'')+ AddTaskList.StartDate.getMinutes())+':'
											+((AddTaskList.StartDate.getSeconds()<'10'?'0':'')+ AddTaskList.StartDate.getSeconds()),
											
											StartTime: AddTaskList.StartTime,
											DueDate: AddTaskList.DueDate.getFullYear()+'-'
											+('0' + (AddTaskList.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.DueDate.getDate()).slice(-2)+'T'
											+((AddTaskList.DueDate.getHours()<'10'?'0':'')+ AddTaskList.DueDate.getHours())+':'
											+((AddTaskList.DueDate.getMinutes()<'10'?'0':'')+ AddTaskList.DueDate.getMinutes())+':'
											+((AddTaskList.DueDate.getSeconds()<'10'?'0':'')+ AddTaskList.DueDate.getSeconds()),
											
                                            DueTime: AddTaskList.DueTime,
											ReminderDate: Da_ReminderDate,
											
											ReminderTime: AddTaskList.ReminderTime,
                                            FullDayActivityBit: AddTaskList.FullDayActivityBit,
											ImportantTaskBit: AddTaskList.ImportantTaskBit,
											ActionName: AddTaskList.ActionName,
                                            MovingTaskDesc: AddTaskList.MovingTaskDesc,
											SalesId: AddTaskList.SalesId,
											ProspectId: AddTaskList.ProspectId,
                                            ActivityTypeId: AddTaskList.ActivityTypeId,
											TaskTypeId: AddTaskList.TaskTypeId,
											ActivityPreDefineRespondId: AddTaskList.ActivityPreDefineRespondId,
                                            StatusTaskId: AddTaskList.StatusTaskId
											}]);
		}
        
      },
	  
	  MakeComment: function(AddTaskList) {
        return $http.post('/api/sales/SDSComment', [{
                                            //AppId: 1,
                                            TaskId: AddTaskList.TaskId,
											CommentDate: AddTaskList.CommentDate,
                                            CommentDesc: AddTaskList.CommentDesc}]);
      },
      
	  update: function(AddTaskList){
		var Da_ReminderDate="";
		try
		{
			Da_ReminderDate= AddTaskList.ReminderDate.getFullYear()+'-'
												  +('0' + (AddTaskList.ReminderDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + AddTaskList.ReminderDate.getDate()).slice(-2)+'T'
												  +((AddTaskList.ReminderDate.getHours()<'10'?'0':'')+ AddTaskList.ReminderDate.getHours())+':'
												  +((AddTaskList.ReminderDate.getMinutes()<'10'?'0':'')+ AddTaskList.ReminderDate.getMinutes())+':'
												  +((AddTaskList.ReminderDate.getSeconds()<'10'?'0':'')+ AddTaskList.ReminderDate.getSeconds());
		}
		catch(e1)
		{
			Da_ReminderDate=null;
		}
	  
		if(currentUser.RoleName == "SALES")
		{
			return $http.put('/api/sales/SDSActivitySalesman', [{
                                            OutletId: AddTaskList.OutletId,
											TaskId: AddTaskList.TaskId,
                                            TaskCode: AddTaskList.TaskCode,
											TaskName: AddTaskList.TaskName,
											TaskDescription: AddTaskList.TaskDescription,
											StartDate: AddTaskList.StartDate.getFullYear()+'-'
											+('0' + (AddTaskList.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.StartDate.getDate()).slice(-2)+'T'
											+((AddTaskList.StartDate.getHours()<'10'?'0':'')+ AddTaskList.StartDate.getHours())+':'
											+((AddTaskList.StartDate.getMinutes()<'10'?'0':'')+ AddTaskList.StartDate.getMinutes())+':'
											+((AddTaskList.StartDate.getSeconds()<'10'?'0':'')+ AddTaskList.StartDate.getSeconds()),
											
											StartTime: AddTaskList.StartTime,
											DueDate: AddTaskList.DueDate.getFullYear()+'-'
											+('0' + (AddTaskList.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.DueDate.getDate()).slice(-2)+'T'
											+((AddTaskList.DueDate.getHours()<'10'?'0':'')+ AddTaskList.DueDate.getHours())+':'
											+((AddTaskList.DueDate.getMinutes()<'10'?'0':'')+ AddTaskList.DueDate.getMinutes())+':'
											+((AddTaskList.DueDate.getSeconds()<'10'?'0':'')+ AddTaskList.DueDate.getSeconds()),
											
                                            DueTime: AddTaskList.DueTime,
											ReminderDate: Da_ReminderDate,
											
											ReminderTime: AddTaskList.ReminderTime,
                                            FullDayActivityBit: AddTaskList.FullDayActivityBit,
											ImportantTaskBit: AddTaskList.ImportantTaskBit,
											ActionName: AddTaskList.ActionName,
                                            MovingTaskDesc: AddTaskList.MovingTaskDesc,
											SalesId: AddTaskList.SalesId,
											ProspectId: AddTaskList.ProspectId,
                                            ActivityTypeId: AddTaskList.ActivityTypeId,
											TaskTypeId: AddTaskList.TaskTypeId,
											ActivityPreDefineRespondId: AddTaskList.ActivityPreDefineRespondId,
                                            ReasonNote: AddTaskList.ReasonNote,
											StatusTaskId: AddTaskList.StatusTaskId,
                                            StatusCode: AddTaskList.StatusCode}]);
		}
		else
		{
			return $http.put('/api/sales/SDSActivitySupervisor', [{
                                            OutletId: AddTaskList.OutletId,
											TaskId: AddTaskList.TaskId,
                                            TaskCode: AddTaskList.TaskCode,
											TaskName: AddTaskList.TaskName,
											TaskDescription: AddTaskList.TaskDescription,
											StartDate: AddTaskList.StartDate.getFullYear()+'-'
											+('0' + (AddTaskList.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.StartDate.getDate()).slice(-2)+'T'
											+((AddTaskList.StartDate.getHours()<'10'?'0':'')+ AddTaskList.StartDate.getHours())+':'
											+((AddTaskList.StartDate.getMinutes()<'10'?'0':'')+ AddTaskList.StartDate.getMinutes())+':'
											+((AddTaskList.StartDate.getSeconds()<'10'?'0':'')+ AddTaskList.StartDate.getSeconds()),
											
											StartTime: AddTaskList.StartTime,
											DueDate: AddTaskList.DueDate.getFullYear()+'-'
											+('0' + (AddTaskList.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + AddTaskList.DueDate.getDate()).slice(-2)+'T'
											+((AddTaskList.DueDate.getHours()<'10'?'0':'')+ AddTaskList.DueDate.getHours())+':'
											+((AddTaskList.DueDate.getMinutes()<'10'?'0':'')+ AddTaskList.DueDate.getMinutes())+':'
											+((AddTaskList.DueDate.getSeconds()<'10'?'0':'')+ AddTaskList.DueDate.getSeconds()),
											
                                            DueTime: AddTaskList.DueTime,
											ReminderDate: Da_ReminderDate,
											
											ReminderTime: AddTaskList.ReminderTime,
                                            FullDayActivityBit: AddTaskList.FullDayActivityBit,
											ImportantTaskBit: AddTaskList.ImportantTaskBit,
											ActionName: AddTaskList.ActionName,
                                            MovingTaskDesc: AddTaskList.MovingTaskDesc,
											SalesId: AddTaskList.SalesId,
											ProspectId: AddTaskList.ProspectId,
                                            ActivityTypeId: AddTaskList.ActivityTypeId,
											TaskTypeId: AddTaskList.TaskTypeId,
											ActivityPreDefineRespondId: AddTaskList.ActivityPreDefineRespondId,
                                            ReasonNote: AddTaskList.ReasonNote,
											StatusTaskId: AddTaskList.StatusTaskId,
                                            StatusCode: AddTaskList.StatusCode}]);
		}
        
											
		
				
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd