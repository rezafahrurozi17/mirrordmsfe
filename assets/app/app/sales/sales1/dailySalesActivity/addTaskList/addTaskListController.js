angular.module('app')
    .controller('AddTaskListController', function ($rootScope,$scope,$filter, $http, CurrentUser, AddTaskListFactory,LeadTimeProspectFuFactory, $timeout,bsNotify) {

		$scope.AddTaskListOperation="";
		
		$scope.intended_operation = "None";
		$scope.formProspecting = false;
		$scope.TheUpdateIsReschedule=false;
		
		$scope.TodayDate_Raw=new Date();
		$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
		$scope.KembaliAfterComment=false;
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalAddTaskListNotifikasiKembali').remove();
		  angular.element('.ui.modal.AddTaskListForm').remove();
		  angular.element('.ui.modal.ModalProspectBuatAddTaskList').remove();
		});
		//----------------------------------
        // Initialization
        //----------------------------------
		$scope.disabledSaveData = false;

		$scope.bind_data={
			data:[],
			filter:[]
		};
		 $scope.filterData = {
                defaultFilter: [
                    { name: 'Model Name', value: 'ModelName' },
                    { name: 'Tipe Kendaraan', value: 'TipeKendaraan' },
                    { name: 'Color', value: 'Color' }
                ],
                advancedFilter: [
					{ name: 'StatusTaskName', value: 'StatusTaskId', typeMandatory: 0 },
                    { name: 'ActivityTypeName', value: 'ActivityTypeId', typeMandatory: 0 },
					{ name: 'Date', value: 'Date', typeMandatory: 0 }
                ]
            };
		
		AddTaskListFactory.getStatusTask().then(function (res) {
				$scope.OptionsStatusTask = res.data.Result;
				$scope.HighlightTheIncomplete();
				return $scope.OptionsStatusTask;
			});
		
		$scope.optionProspectSearchCriteria = AddTaskListFactory.GetProspectSearchDropdownOptions();
		$scope.viewcalendar = false;
		
		AddTaskListFactory.getDataSales().then(function (res) {
			$scope.optionsSalesForce = res.data.Result;
			return $scope.optionsSalesForce;
		});

        $scope.addNewComment = function (data) {
            try
			{
				$scope.commentInput = {
					TaskId: $scope.selected_data.TaskId,
					CommentDate: new Date(),
					CommentDesc: data.CommentContent
				};
			
				if((typeof $scope.commentInput.CommentDesc!="undefined") && $scope.commentInput.CommentDesc!="null" && $scope.commentInput.CommentDesc!="")
				{	
					AddTaskListFactory.MakeComment($scope.commentInput).then(function () {
						$scope.selected_data.ListSDSCommentView.push({'SalesId':$scope.user.EmployeeId,'TaskId':$scope.selected_data.TaskId,'CommentDate':new Date(),'CommentDesc':$scope.commentInput.CommentDesc,'SalesName':$scope.selected_data.SalesName});
						$scope.KembaliAfterComment=true;
					});
				}
				else
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Pastikan comment terisi dengan benar.",
							type: 'warning'
						}
					);
				}
			}
			catch(ExceptionComment)
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Pastikan comment terisi dengan benar.",
						type: 'warning'
					}
				);
			}
        }
        
		$scope.SearchAwalByActivityTypeNameClicked = function (DaSelectedSearch) {
			$scope.bind_data.filter.ActivityTypeId=DaSelectedSearch;
        }

		
		
		$scope.SearchWithCalendarChanged = function (calendarThingy) {
			var da_DateToSearch=calendarThingy;
			var da_DateToSearchKirim=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1)).slice(-2)+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';
			$scope.bind_data.filter.Date=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1)).slice(-2)+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';
			//AddTaskListFactory.getDataForFilterByCalendar(da_DateToSearchKirim).then(
			AddTaskListFactory.getData(da_DateToSearchKirim).then(
							function(res){
								$scope.bind_data.data=[];
								$scope.bind_data.data = res.data.Result;
								$scope.loading=false;
								$scope.total=res.data.Total;
								$scope.now=res.data.Total;
								if($scope.total>0)
								{
									$scope.isEmpty=false;
								}
								else if($scope.total<=0)
								{
									$scope.isEmpty=true;
								}
								//return res.data;
							}
						);
        }

		
		AddTaskListFactory.getTipeAkivitas().then(function (res) {
			$scope.optionsTipeAktivitas = res.data.Result;
			return $scope.optionsTipeAktivitas;
		}).then(function () {

			$scope.optionsTipeAktivitasFiltered=angular.copy($scope.optionsTipeAktivitas);
			$scope.optionsTipeAktivitasFiltered.splice(0,$scope.optionsTipeAktivitas.length);

			for(var i = 0; i < $scope.optionsTipeAktivitas.length; ++i)
			{	
				if($scope.optionsTipeAktivitas[i].ActivityTypeName == "Customer/Prospect" || $scope.optionsTipeAktivitas[i].ActivityTypeName == "Others")
				{
					$scope.optionsTipeAktivitasFiltered.push($scope.optionsTipeAktivitas[i]);
				}
			}
		});

        $scope.tipeCustomer = false;
        $scope.tipeOther = false;
        $scope.DaftarAktivitas = function () {
		
			AddTaskListFactory.getTipeTugas().then(function (res) {
				$scope.optionsTipeTugas = res.data.Result;
				return $scope.optionsTipeTugas;
			}).then(function () {
				if ($scope.selected_data.ActivityTypeId == 1) 
				{
					for(var ee = 0; ee < $scope.optionsTipeTugas.length; ee++)
					{
						if($scope.optionsTipeTugas[ee].TaskTypeName=="Other")
						{
							$scope.optionsTipeTugas.splice(ee, 1);
						}
					}
					if($scope.selected_data.ActivityTypeId == 1&&$scope.AddTaskListOperation=="Insert")
					{
						$scope.selected_data.TaskTypeId=$scope.optionsTipeTugas[0].TaskTypeId;
					}
				}
			});
		
            if ($scope.selected_data.ActivityTypeId == 1) {
                $scope.comboTipeTugas = true;
                $scope.tipeCustomer = true;
                $scope.tipeOther = false;
				
            }
            else if ($scope.selected_data.ActivityTypeId == 2) {
                $scope.comboTipeTugas = false;
                $scope.tipeOther = true;
                $scope.tipeCustomer = false;
            }
        };

        


        $scope.viewCalendarMode = function () {
            if ($scope.viewcalendar == false) {
                $scope.viewcalendar = true;
            } else{
                $scope.viewcalendar = false;
				//$scope.bind_data.filter.Date=null;
				$rootScope.$emit("RefreshDirective", {});
				// AddTaskListFactory.getData("/?start=1&limit=5").then(
								// function(res){
									// $scope.bind_data.data = res.data.Result;
									// $scope.loading=false;
									// $scope.total=res.data.Total;
									// $scope.now=res.data.Total;
									// //return res.data;
								// }
							// );
            }
        }
		
		$scope.DaStartDateChanged = function () {
			$scope.MinDueDate_Raw=$scope.selected_data.StartDate;
			$scope.MinDueDate=$scope.MinDueDate_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDate_Raw.getDate()).slice(-2);
			//$scope.MaxReminderDate_Raw=$scope.selected_data.StartDate;
			//$scope.MaxReminderDate=$scope.MaxReminderDate_Raw.getFullYear()+'-'+('0' + ($scope.MaxReminderDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MaxReminderDate_Raw.getDate()).slice(-2);
			$scope.MaxReminderDate=$scope.selected_data.StartDate;
			if($scope.selected_data.StartDate>=$scope.selected_data.DueDate)
			{
				$scope.selected_data.DueDate=$scope.selected_data.StartDate;
				$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
			}
			if($scope.selected_data.StartDate<=$scope.selected_data.ReminderDate)
			{
				$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
			}
			
			$scope.AddTaskListRescheduleGaDiubah=false;
			
		}
		
		$scope.DaWaktuMulaiChanged = function () {
			$scope.AddTaskListRescheduleGaDiubah=false;
		}
		
		$scope.DaDueDateChanged = function () {
			$scope.AddTaskListRescheduleGaDiubah=false;
		}
		
		$scope.DaWaktuAkhirChanged = function () {
			$scope.AddTaskListRescheduleGaDiubah=false;
		}
		
		$scope.DaWaktuPengingatChanged = function () {
			$scope.AddTaskListRescheduleGaDiubah=false;
		}
		
		$scope.DaTugasPentingChanged = function () {
			$scope.AddTaskListRescheduleGaDiubah=false;
		}
		
		$scope.DaStartDateChanged_forUpdating = function () {
			$scope.MinDueDate_Raw=$scope.selected_data.StartDate;
			$scope.MinDueDate=$scope.MinDueDate_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDate_Raw.getDate()).slice(-2);
			//$scope.MaxReminderDate_Raw=$scope.selected_data.StartDate;
			//$scope.MaxReminderDate=$scope.MaxReminderDate_Raw.getFullYear()+'-'+('0' + ($scope.MaxReminderDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MaxReminderDate_Raw.getDate()).slice(-2);
			$scope.MaxReminderDate=$scope.selected_data.StartDate;
		}

        $scope.addNewTask = function () {
            $scope.comboTipeTugas = false;
            $scope.tipeCustomer = false;
            $scope.tipeOther = false;
			
			$scope.listTask = true;
            $scope.tambahTugas = true;
            $scope.addBtn = true;
            $scope.viewType = true;
            $scope.selected_data = [];
			$scope.AddTaskListOperation="Insert";
			$scope.DisableWhenEdit=false;
			$scope.viewcalendar=false;
			$scope.MinDueDate_Raw=new Date();
			$scope.MinDueDate=$scope.MinDueDate_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDate_Raw.getDate()).slice(-2);
			
			if($scope.user.RoleName=="SUPERVISOR SALES")
			{
				$scope.Show_Da_Salesman=true;
			}
			else if($scope.user.RoleName=="SALES")
			{
				$scope.Show_Da_Salesman=false;
			}
			
			$scope.selected_data.FullDayActivityBit=false;
			$scope.IsSeharian=false;
			$scope.selected_data.ReminderDate=null;
			$scope.selected_data.ReminderTime=null;
			
			$scope.selected_data.StartDate=$scope.TodayDate_Raw;
			$scope.selected_data.DueDate=$scope.TodayDate_Raw;
			$scope.selected_data.ReminderDate=$scope.TodayDate_Raw;
			
			var Da_Sekarang=new Date();
			var Da_Waktu_Sekarang=$filter('date')(Da_Sekarang, 'HH:mm');
			var RawSekarangTime_Get = Da_Waktu_Sekarang.split(':');
				$scope.ProcessedSekarangTime_Get = {    
				 value: new Date($scope.TodayDate_Raw.getFullYear(), $scope.TodayDate_Raw.getMonth(), $scope.TodayDate_Raw.getDate(), RawSekarangTime_Get[0], RawSekarangTime_Get[1])
				 };
				 
			$scope.selected_data.StartTime=angular.copy($scope.ProcessedSekarangTime_Get.value);
			$scope.selected_data.DueTime=angular.copy($scope.ProcessedSekarangTime_Get.value);
			$scope.selected_data.DueTime.setHours($scope.selected_data.DueTime.getHours()+1);
			
			$scope.selected_data.ReminderTime=angular.copy($scope.ProcessedSekarangTime_Get.value);
        }
		
		$scope.AddTaskListReminderChanged = function () {
			if($scope.selected_data.ReminderDate==null)
			{
				$scope.MaxReminderDate=null;
			}
			else
			{
				//$scope.MaxReminderDate=$scope.MaxReminderDate_Raw.getFullYear()+'-'+('0' + ($scope.MaxReminderDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MaxReminderDate_Raw.getDate()).slice(-2);
				if($scope.selected_data.StartDate<=$scope.selected_data.ReminderDate)
				{
					$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
				}
			}
		}
		
		$scope.HighlightTheIncomplete = function () {
			$scope.events=[];
			$scope.Color_Da_Calendar=[{date:null,status: ''}];
			$scope.Color_Da_Calendar.splice(0,1);
			$scope.Raw_Color_Da_Calendar_Incomplete =[];
			$scope.Raw_Color_Da_Calendar_Complete =[];
			
			AddTaskListFactory.getDataForCalendar().then(function (res) {
				$scope.Raw_Color_Da_Calendar_Incomplete = res.data;
				for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Incomplete.length; i++)
				{	
					$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Incomplete[i].toString()),status: 'secondDate'});
				}
				
				AddTaskListFactory.getDataForCalendarCompleteOrCancel().then(function (res) {
					$scope.Raw_Color_Da_Calendar_Complete = res.data;
					for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Complete.length; i++)
					{	
						for(var ix = 0; ix < $scope.Raw_Color_Da_Calendar_Incomplete.length; ix++)
						{
							if($scope.Raw_Color_Da_Calendar_Complete[i].date==$scope.Raw_Color_Da_Calendar_Incomplete[ix].date)
							{//harusnya ini != tapi entah napa == mala jalan != mala kaga
								$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Complete[i].toString()),status: 'thirdDate'});
							}
							//else
							//{
							//	$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Complete[i].toString()),status: 'secondDate'});
							//}
						}
						if($scope.Raw_Color_Da_Calendar_Incomplete.length<=0)
						{
							$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Complete[i].toString()),status: 'thirdDate'});
						}
					}
					
					AddTaskListFactory.getDataForCalendarDelay().then(function (res2) {
						$scope.Raw_Color_Da_Calendar_Delay = res2.data;
						for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Delay.length; i++)
						{	
							$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Delay[i].toString()),status: 'fourthDate'});
						}
						
						for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Delay.length; i++)
						{	
							for(var ii = 0; ii < $scope.Color_Da_Calendar.length; ii++)
							{
								var da_tanggal_to_compare=new Date($scope.Raw_Color_Da_Calendar_Delay[i].toString());
								if($scope.Color_Da_Calendar[ii].date.toString()==da_tanggal_to_compare.toString())
								{
									$scope.Color_Da_Calendar[ii].status= 'fourthDate';
								}
							}
						}
						
						for(var ii = 0; ii < $scope.Color_Da_Calendar.length; ii++)
						{	
							$scope.events.push($scope.Color_Da_Calendar[ii]);
							$scope.refreshCalendar = false;
							$timeout(function () { $scope.refreshCalendar = true }, 0);
						}
					});
					
					
					
					
				});
			});
        }
		
		$scope.HighlightTheCompleteOrCancel = function () {
			$scope.Raw_Color_Da_Calendar =[];
			AddTaskListFactory.getDataForCalendarCompleteOrCancel().then(function (res) {
				$scope.Raw_Color_Da_Calendar = res.data;
			});
        }
		
        $scope.cancelAddNewTask = function () {
            // $scope.listTask = false;
            // $scope.tambahTugas = false;
            // $scope.addBtn = false;
            // $scope.viewType = false;
			setTimeout(function() {
              angular.element('.ui.modal.ModalAddTaskListNotifikasiKembali').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalAddTaskListNotifikasiKembali').not(':first').remove();  
            }, 1);
			
		}
		
		$scope.AddTaskListJadiKembaliUbah = function () {
            $scope.listTask = false;
            $scope.tambahTugas = false;
            $scope.addBtn = false;
            $scope.viewType = false;
			angular.element('.ui.modal.ModalAddTaskListNotifikasiKembali').modal('hide');
        }

        $scope.lihatTask = function () {
            $scope.AddTaskListPertamaLihat=true;
			$scope.AddTaskListBarangAsli=angular.copy($scope.selected_data);
			
			$scope.viewcalendar=false;
			$scope.listTask = true;
            $scope.viewType = true;
            $scope.addBtn = true;
            $scope.detilTugas = true;
			$scope.ShowTheTaskListReasonTxtbx=false;
            angular.element('.ui.modal.AddTaskListForm').modal('hide');
			
			$scope.selected_data.StartTime_Lihat=angular.copy($scope.selected_data.StartTime);
			$scope.selected_data.DueTime_Lihat=angular.copy($scope.selected_data.DueTime);
			
			
			
			if($scope.selected_data.ActivityPreDefineRespondId==null)
			{
				AddTaskListFactory.getActivityPredefineRespondParent($scope.selected_data.ActivityTypeId,$scope.selected_data.TaskTypeId).then(function(res){
					$scope.PredefineRespondFromDB = res.data.Result;
					$scope.ShowTheTaskListReasonTxtbx=false;
					return $scope.PredefineRespondFromDB;
				});
				$scope.HideIfNotYetSelected=true;
				$scope.ShowAddTaskListPredefine=false;
				$scope.ActivityPreDefineRespondNameOriginal=$scope.selected_data.ActivityPreDefineRespondName;
				$scope.StatusTaskIdOriginal=$scope.selected_data.StatusTaskId;
				$scope.StatusTaskNameOriginal=$scope.selected_data.StatusTaskName;
				$scope.ActivityPreDefineRespondIdOriginal=null;
			}
			else
			{
				$scope.PredefineRespondFromDB = {};
				$scope.HideIfNotYetSelected=false;
				$scope.ShowAddTaskListPredefine=true;
			}
        }
		
		$scope.UpdateSDSActivityPredefineRespond = function (SelectedActivityPreDefineRespond) {
			$scope.TheSelectedActivityPreDefineRespondId=SelectedActivityPreDefineRespond.ActivityPreDefineRespondId;
			
			AddTaskListFactory.getActivityPredefineRespondChildThingy(SelectedActivityPreDefineRespond.ActivityPreDefineRespondId,$scope.selected_data.ActivityTypeId,$scope.selected_data.TaskTypeId).then(function(res){
				//if(res.data.Result[0].ActivityPreDefineRespondName=="Data berhasil tersimpan!"||res.data.Result[0].ActivityPreDefineRespondName=="Input reason (End Task)")
				//{
				//	$scope.PredefineRespondFromDB = {};
				//	$scope.TheAddTaskListActionTaskList=res.data.Result;
				//	return $scope.TheAddTaskListActionTaskList;
				//}
				//else
				//{
					$scope.PredefineRespondFromDB = res.data.Result;
					$scope.TheAddTaskListActionTaskList=null;
					return $scope.PredefineRespondFromDB;
				//}
				
			}).then(function(){

				if(SelectedActivityPreDefineRespond!=null)
				{
					if(SelectedActivityPreDefineRespond.EndRespond=="Data berhasil tersimpan!"||SelectedActivityPreDefineRespond.EndRespond=="Input reason (End Task)")
					{
						$scope.selected_data['ActivityPreDefineRespondId']=SelectedActivityPreDefineRespond.ActivityPreDefineRespondId;
						$scope.selected_data['ActivityPreDefineRespondName']=SelectedActivityPreDefineRespond.ActivityPreDefineRespondName;
						$scope.selected_data['StatusTaskId']=4;
						$scope.selected_data['StatusTaskName']="Completed";
							
						if(SelectedActivityPreDefineRespond.EndRespond=="Input reason (End Task)")
						{	
							$scope.ShowTheTaskListReasonTxtbx=true;
							//if($scope.TheAddTaskListActionTaskList[0].ActivityPreDefineRespondId==8 || $scope.TheAddTaskListActionTaskList[0].ActivityPreDefineRespondId==34 || $scope.TheAddTaskListActionTaskList[0].ActivityPreDefineRespondId==46|| $scope.TheAddTaskListActionTaskList[0].ActivityPreDefineRespondId==58)
							//{
								$scope.selected_data['StatusTaskId']=5;
								$scope.selected_data['StatusTaskName']="Canceled";
							//}
						}
						else
						{
							$scope.StartTimeToUpdate=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
							$scope.DueTimeToUpdate=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
							$scope.ReminderTimeToUpdate=$filter('date')($scope.selected_data.ReminderTime, 'HH:mm:ss');
							
							$scope.selected_data['StartTime']=$scope.StartTimeToUpdate;
							$scope.selected_data['DueTime']=$scope.DueTimeToUpdate;
							$scope.selected_data['ReminderTime']=$scope.ReminderTimeToUpdate;
							$scope.selected_data['ReasonNote']="";
							
								AddTaskListFactory.update($scope.selected_data).then(function () {
									
									$rootScope.$emit("RefreshDirective", {});
								}).then(function () {
									$scope.TheUpdateIsReschedule=false;
									//$scope.BatalLihatTask();
									$scope.HighlightTheIncomplete();
									$scope.ShowAddTaskListPredefine=true;
									bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil tersimpan.",
											type: 'success'
										}
									);
								});
						}
					}
				}			
			});
        }
		
		$scope.KembaliKePilihPredefine = function () {
			$scope.selected_data.ReasonNote="";
			$scope.selected_data.ActivityPreDefineRespondName=$scope.ActivityPreDefineRespondNameOriginal;
			$scope.selected_data.StatusTaskId=$scope.StatusTaskIdOriginal;
			$scope.selected_data.StatusTaskName=$scope.StatusTaskNameOriginal;
			$scope.selected_data.ActivityPreDefineRespondId=$scope.ActivityPreDefineRespondIdOriginal;
			$scope.ShowTheTaskListReasonTxtbx=false;
			
			AddTaskListFactory.getActivityPredefineRespondParent($scope.selected_data.ActivityTypeId,$scope.selected_data.TaskTypeId).then(function(res){
					$scope.PredefineRespondFromDB = res.data.Result;
					return $scope.PredefineRespondFromDB;
				});
		}
		
		$scope.UpdatePredefineAndSubmitAlasan = function () {
			$scope.StartTimeToUpdate=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
			$scope.DueTimeToUpdate=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
			$scope.ReminderTimeToUpdate=$filter('date')($scope.selected_data.ReminderTime, 'HH:mm:ss');
						
			$scope.selected_data['StartTime']=$scope.StartTimeToUpdate;
			$scope.selected_data['DueTime']=$scope.DueTimeToUpdate;
			$scope.selected_data['ReminderTime']=$scope.ReminderTimeToUpdate;
			//$scope.selected_data['ReasonNote']="";
						
				AddTaskListFactory.update($scope.selected_data).then(function () {
					
					$rootScope.$emit("RefreshDirective", {});
				}).then(function () {
					$scope.TheUpdateIsReschedule=false;
					//$scope.BatalLihatTask();
					$scope.HighlightTheIncomplete();
					$scope.ShowTheTaskListReasonTxtbx=false;
					$scope.ShowAddTaskListPredefine=true;
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Data berhasil tersimpan.",
							type: 'success'
						}
					);
				});
		}
		
		$scope.RefreshPredefineRespondChoice = function () {
			AddTaskListFactory.getActivityPredefineRespondParent($scope.selected_data.ActivityTypeId,$scope.selected_data.TaskTypeId).then(function(res){
				$scope.PredefineRespondFromDB = res.data.Result;
				$scope.ShowTheTaskListReasonTxtbx=false;
				return $scope.PredefineRespondFromDB;
			});
        }

        $scope.BatalLihatTask = function () {
			$scope.listTask = false;
            $scope.viewType = false;
            $scope.addBtn = false;
            $scope.detilTugas = false;
            $scope.btnActionReschedule = false;
            $scope.btnPredefined = false;
            $scope.RescheduleTask = false;
            $scope.btnSimpan = false;
            $scope.btnPindahTugas = false;
            $scope.btnUbahTugas = false;
			$scope.tambahTugas = false;
			
			if($scope.KembaliAfterComment==true)
			{
				$rootScope.$emit("RefreshDirective", {});
				$scope.KembaliAfterComment=false;
				$scope.TheUpdateIsReschedule=false;
				
			}
        }

        $scope.editTask = function () {
            
			
			if($scope.user.RoleName == "SALES" &&$scope.selected_data.StatusTaskName!="Canceled"&&$scope.selected_data.StatusTaskName!="Completed")
			{
				$scope.viewcalendar=false;
				$scope.listTask = true;
				$scope.tambahTugas = true;
				$scope.addBtn = true;
				$scope.viewType = true;
				angular.element('.ui.modal.AddTaskListForm').modal('hide');
				$scope.AddTaskListOperation="Update";
				$scope.DisableWhenEdit=true;
				$scope.DaStartDateChanged_forUpdating();

				if($scope.user.RoleName=="SUPERVISOR SALES")
				{
					$scope.Show_Da_Salesman=true;
				}
				else if($scope.user.RoleName=="SALES")
				{
					$scope.Show_Da_Salesman=false;
				}
				
				var RawStartTime_Get = $scope.selected_data.StartTime.split(':');
				$scope.ProcessedStartTime_Get = {    
				 value: new Date(2015, 10, 10, RawStartTime_Get[0], RawStartTime_Get[1], RawStartTime_Get[2])
				 };
				 
				$scope.selected_data.StartTime=$scope.ProcessedStartTime_Get.value;
				
				var RawDueTime_Get = $scope.selected_data.DueTime.split(':');
				$scope.ProcessedDueTime_Get = {    
				 value: new Date(2015, 10, 10, RawDueTime_Get[0], RawDueTime_Get[1], RawDueTime_Get[2])
				 };
				 
				$scope.selected_data.DueTime=$scope.ProcessedDueTime_Get.value;
				
				
				var RawReminderTime_Get = $scope.selected_data.ReminderTime.split(':');
				$scope.ProcessedReminderTime_Get = {    
				 value: new Date(2015, 10, 10, RawReminderTime_Get[0], RawReminderTime_Get[1], RawReminderTime_Get[2])
				 };
				 
				$scope.selected_data.ReminderTime=$scope.ProcessedReminderTime_Get.value;
				$scope.SehariPenuhChanged();

				$scope.AddTaskListStartDateLama=angular.copy( $scope.selected_data.StartDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.StartDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.StartDate.getHours()<'10'?'0':'')+ $scope.selected_data.StartDate.getHours())+':'
											+(($scope.selected_data.StartDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.StartDate.getMinutes())+':'
											+(($scope.selected_data.StartDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.StartDate.getSeconds()));
											
				$scope.AddTaskListDueDateLama=angular.copy( $scope.selected_data.DueDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.DueDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.DueDate.getHours()<'10'?'0':'')+ $scope.selected_data.DueDate.getHours())+':'
											+(($scope.selected_data.DueDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.DueDate.getMinutes())+':'
											+(($scope.selected_data.DueDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.DueDate.getSeconds()));							
				
				$scope.AddTaskListFullDayActivityBitLama=angular.copy($scope.selected_data.FullDayActivityBit);
				
				$scope.AddTaskListStartTimeToCompare=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
				$scope.AddTaskListStartTimeLama=$scope.AddTaskListStartTimeToCompare;
				
				$scope.AddTaskListDueTimeToCompare=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
				$scope.AddTaskListDueTimeLama=$scope.AddTaskListDueTimeToCompare;
				$scope.DaftarAktivitas();
			}
			else
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda tidak boleh mengedit data.",
						type: 'warning'
					}
				);
			}
        }
		

        $scope.rescheduleTask = function () {
            
			if($scope.AddTaskListPertamaLihat==false)
			{
				$scope.selected_data=angular.copy($scope.AddTaskListBarangAsli);
				$scope.selected_data.StartTime_Lihat=angular.copy($scope.selected_data.StartTime);
				$scope.selected_data.DueTime_Lihat=angular.copy($scope.selected_data.DueTime);
			
			}
			if($scope.selected_data.StatusTaskName=="Canceled"||$scope.selected_data.StatusTaskName=="Completed")
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Tidak dapat melakukan reschedule/perubahan karena status sudah completed/cancelled.",
						type: 'warning'
					}
				);
			}
			else
			{
				$scope.btnActionReschedule = true;
				$scope.btnPredefined = true;
				$scope.RescheduleTask = true;
				$scope.btnSimpan = true;
				$scope.btnPindahTugas = true;
				$scope.btnUbahTugas = true;
				$scope.btnKembaliLihatTugas = true;
				$scope.btnKembaliReschedule = true;
				$scope.AddTaskListOperation="Update";
				$scope.DaStartDateChanged_forUpdating();
				$scope.TheUpdateIsReschedule=true;

				if($scope.user.RoleName=="SUPERVISOR SALES")
				{
					$scope.Show_Da_Salesman=true;
				}
				else if($scope.user.RoleName=="SALES")
				{
					$scope.Show_Da_Salesman=false;
				}
				
				var RawStartTime_Get = $scope.selected_data.StartTime.split(':');
				$scope.ProcessedStartTime_Get = {    
					value: new Date(2015, 10, 10, RawStartTime_Get[0], RawStartTime_Get[1], RawStartTime_Get[2])
				};
					 
				$scope.selected_data.StartTime=$scope.ProcessedStartTime_Get.value;
				
				var RawDueTime_Get = $scope.selected_data.DueTime.split(':');
				$scope.ProcessedDueTime_Get = {    
					value: new Date(2015, 10, 10, RawDueTime_Get[0], RawDueTime_Get[1], RawDueTime_Get[2])
				};
					 
				$scope.selected_data.DueTime=$scope.ProcessedDueTime_Get.value;
				
				var RawReminderTime_Get = $scope.selected_data.ReminderTime.split(':');
				$scope.ProcessedReminderTime_Get = {    
					value: new Date(2015, 10, 10, RawReminderTime_Get[0], RawReminderTime_Get[1], RawReminderTime_Get[2])
				};
					 
				$scope.selected_data.ReminderTime=$scope.ProcessedReminderTime_Get.value;
				
				$scope.SehariPenuhChanged();
			}
			$scope.AddTaskListRescheduleGaDiubah=true;
        }
		
		$scope.pindahTugaskanTask = function () {
            
			if($scope.selected_data.StatusTaskName=="Canceled"||$scope.selected_data.StatusTaskName=="Completed")
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Tidak dapat melakukan reschedule/perubahan karena status sudah completed/cancelled.",
						type: 'warning'
					}
				);
			}
			else
			{
				$scope.btnActionReschedule = true;
				$scope.btnPredefined = true;
				$scope.MovingTask = true;
				$scope.btnSimpan = true;
				$scope.btnPindahTugas = true;
				$scope.btnUbahTugas = true;
				$scope.btnKembaliLihatTugas = true;
				$scope.btnKembaliReschedule = true;
				$scope.AddTaskListOperation="Update";
			}
        }
		
		$scope.GetInformasiDetailProspect = function () {
			$scope.formProspecting = true;
            $scope.listTask = true;
            $scope.viewType = true;
            $scope.addBtn = true;
            $scope.detilTugas = false;
			$rootScope.ProspectDetailFromAddTaskList = $scope.selected_data.ProspectId;
            $scope.childmethod();
            
        }
		
		$scope.BackFromInformasiDetailProspect = function () {
			$scope.formProspecting = false;
			$scope.listTask = true;
            $scope.viewType = true;
            $scope.addBtn = true;
            $scope.detilTugas = true;
		}
		
		$scope.childmethod = function() {
            //$rootScope.$emit("lihatDETILINFOPROSPEK", {});
			setTimeout(function(){
                $rootScope.$emit("lihatDETILINFOPROSPEK", {})
            },2000);
        }
		
		$rootScope.$on("balikdariDETILINFOPROSPEK", function(){
           $scope.BackFromInformasiDetailProspect();
        });

        $scope.BatalRescheduleTask = function () {
			$scope.AddTaskListPertamaLihat=false;
			$scope.btnActionReschedule = false;
            $scope.btnPredefined = false;
            $scope.RescheduleTask = false;
			$scope.MovingTask = false;
            $scope.btnSimpan = false;
            $scope.btnPindahTugas = false;
            $scope.btnUbahTugas = false;
            $scope.btnKembaliLihatTugas = false;
            $scope.btnKembaliReschedule = false;


        }
		
		$scope.SehariPenuhChanged = function () {

			if($scope.selected_data.FullDayActivityBit==true)
			{
				$scope.IsSeharian=true;
				
			}
			else if($scope.selected_data.FullDayActivityBit==false)
			{
				$scope.IsSeharian=false;
				
			}
			$scope.AddTaskListRescheduleGaDiubah=false;

        }

        $scope.ubahTugas = function () {
            
			if($scope.selected_data.StatusTaskName=="Canceled"||$scope.selected_data.StatusTaskName=="Completed")
			{
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Tidak dapat melakukan reschedule/perubahan karena status sudah completed/cancelled",
						type: 'warning'
					}
				);
			}
			else
			{
				$scope.detilTugas = false;
				$scope.tambahTugas = true;
				$scope.DisableWhenEdit=true;
				$scope.AddTaskListOperation="Update";
				$scope.TheUpdateIsReschedule=false;
				
				if($scope.user.RoleName=="SUPERVISOR SALES")
				{
					$scope.Show_Da_Salesman=true;
				}
				else if($scope.user.RoleName=="SALES")
				{
					$scope.Show_Da_Salesman=false;
				}
				
				var RawStartTime_Get = $scope.selected_data.StartTime.split(':');
				$scope.ProcessedStartTime_Get = {    
					value: new Date(2015, 10, 10, RawStartTime_Get[0], RawStartTime_Get[1], RawStartTime_Get[2])
				};
					 
				$scope.selected_data.StartTime=$scope.ProcessedStartTime_Get.value;
				
				
				var RawDueTime_Get = $scope.selected_data.DueTime.split(':');
				$scope.ProcessedDueTime_Get = {    
					value: new Date(2015, 10, 10, RawDueTime_Get[0], RawDueTime_Get[1], RawDueTime_Get[2])
				};
					 
				$scope.selected_data.DueTime=$scope.ProcessedDueTime_Get.value;
				
				var RawReminderTime_Get = $scope.selected_data.ReminderTime.split(':');
				$scope.ProcessedReminderTime_Get = {    
					value: new Date(2015, 10, 10, RawReminderTime_Get[0], RawReminderTime_Get[1], RawReminderTime_Get[2])
				};
					 
				$scope.selected_data.ReminderTime=$scope.ProcessedReminderTime_Get.value;
				
				$scope.SehariPenuhChanged();
				$scope.DaftarAktivitas();
			}
        }
		
		
		$scope.SaveData = function () {
			$scope.disabledSaveData = true;
			$scope.StartTimeToUpdate=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
			$scope.DueTimeToUpdate=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
			$scope.ReminderTimeToUpdate=$filter('date')($scope.selected_data.ReminderTime, 'HH:mm:ss');
			
			$scope.AddTaskListPaksaErrorJamDue=false;
			$scope.AddTaskListPaksaErrorJamReminder=false;
			
			if($scope.selected_data.FullDayActivityBit==false)
			{
				var UlalaStartDate=angular.copy($scope.selected_data.StartDate);
				var UlalaDueDate=angular.copy($scope.selected_data.DueDate);
				var UlalaReminderDate=angular.copy($scope.selected_data.ReminderDate)
				
				
				if(UlalaStartDate.toString()==UlalaDueDate.toString())
				{

					if($scope.DueTimeToUpdate>=$scope.StartTimeToUpdate)
					{
						$scope.AddTaskListPaksaErrorJamDue=false;
					}
					else
					{
						$scope.AddTaskListPaksaErrorJamDue=true;
					}

				}
				
				if(UlalaStartDate.toString()==UlalaReminderDate.toString())
				{
					if($scope.ReminderTimeToUpdate<=$scope.StartTimeToUpdate)
					{
						$scope.AddTaskListPaksaErrorJamReminder=false;
					}
					else
					{
						$scope.AddTaskListPaksaErrorJamReminder=true;
					}
				}
			}

			$scope.selected_data['StartTime']=$scope.StartTimeToUpdate;
			$scope.selected_data['DueTime']=$scope.DueTimeToUpdate;
			$scope.selected_data['ReminderTime']=$scope.ReminderTimeToUpdate;
			
			$scope.DaErrorCheck=false;

			try
			{
				if($scope.selected_data['TaskDescription'].length>0 &&  (typeof $scope.selected_data['StartDate']!="undefined")&& (typeof $scope.selected_data['DueDate']!="undefined"))
				{	
					if($scope.selected_data['ActivityTypeId']==2)
					{
						if($scope.selected_data['TaskName'].length>0)
						{	
							$scope.DaErrorCheck=true;
						}
								
					}
					else if($scope.selected_data['ActivityTypeId']==1)
					{
						if($scope.selected_data['ProspectId'].toString().length>0)
						{
							$scope.DaErrorCheck=true;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==true)
					{
						if((typeof $scope.selected_data['ReminderTime']!="undefined"))
						{
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==false)
					{
						if((typeof $scope.selected_data['StartTime']!="undefined")&& (typeof $scope.selected_data['DueTime']!="undefined"))
						{
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
							
							
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
				}
				else
				{
					$scope.DaErrorCheck=false;
				}
			}
			catch(eeee)
			{
				$scope.DaErrorCheck=false;
			}
			
			
			
			
			
			
			if($scope.DaErrorCheck==true && $scope.AddTaskListPaksaErrorJamDue==false && $scope.AddTaskListPaksaErrorJamReminder==false)
			{
				if($scope.AddTaskListOperation=="Update")
				{
					if($scope.TheUpdateIsReschedule==true)
					{
						$scope.selected_data['StatusTaskId']=3;
						$scope.selected_data['StatusTaskName']="Reschedule";
					}
					
					
					$scope.StartToSubmitRaw= $scope.selected_data.StartDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.StartDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.StartDate.getHours()<'10'?'0':'')+ $scope.selected_data.StartDate.getHours())+':'
											+(($scope.selected_data.StartDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.StartDate.getMinutes())+':'
											+(($scope.selected_data.StartDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.StartDate.getSeconds());
						
						$scope.DueToSubmitRaw= $scope.selected_data.DueDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.DueDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.DueDate.getHours()<'10'?'0':'')+ $scope.selected_data.DueDate.getHours())+':'
											+(($scope.selected_data.DueDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.DueDate.getMinutes())+':'
											+(($scope.selected_data.DueDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.DueDate.getSeconds());
						
						if(($scope.StartToSubmitRaw!=$scope.AddTaskListStartDateLama)||($scope.AddTaskListDueDateLama!=$scope.DueToSubmitRaw))
						{
							$scope.selected_data['StatusTaskId']=3;
							$scope.selected_data['StatusTaskName']="Reschedule";
						}
						
						if($scope.AddTaskListFullDayActivityBitLama!=$scope.selected_data.FullDayActivityBit)
						{
							$scope.selected_data['StatusTaskId']=3;
							$scope.selected_data['StatusTaskName']="Reschedule";
						}
						else if(($scope.AddTaskListFullDayActivityBitLama==false && $scope.selected_data.FullDayActivityBit==false)&&($scope.AddTaskListStartTimeLama!=$scope.selected_data.StartTime||$scope.AddTaskListDueTimeLama!=$scope.selected_data.DueTime))
						{
							$scope.selected_data['StatusTaskId']=3;
							$scope.selected_data['StatusTaskName']="Reschedule";
						}

						$scope.StartToSubmit=$filter('date')(new Date($scope.StartToSubmitRaw), 'M/d/yy');
						$scope.DueToSubmit=$filter('date')(new Date($scope.DueToSubmitRaw), 'M/d/yy');
						
						AddTaskListFactory.getData("").then(
								function(res){
									$scope.DaGetToCheck = res.data.Result;
								}
							).then(function () {
								for (var i = 0; i < $scope.DaGetToCheck.length; i++) 
								{
									var datestarttocheck=$filter('date')(new Date($scope.DaGetToCheck[i].StartDate), 'M/d/yy');
									var dateduetocheck=$filter('date')(new Date($scope.DaGetToCheck[i].DueDate), 'M/d/yy');
									
									var datestartTimetocheckRaw= $scope.DaGetToCheck[i].StartTime.split(':');
									var datestartTimetocheckProcessed=new Date(2015, 10, 10, datestartTimetocheckRaw[0], datestartTimetocheckRaw[1], datestartTimetocheckRaw[2]);
									var datestartTimetocheck=$filter('date')(datestartTimetocheckProcessed, 'HH:mm:ss');
									
									var datedueTimetocheckRaw= $scope.DaGetToCheck[i].DueTime.split(':');
									var datedueTimetocheckProcessed=new Date(2015, 10, 10, datedueTimetocheckRaw[0], datedueTimetocheckRaw[1], datedueTimetocheckRaw[2]);
									var datedueTimetocheck=$filter('date')(datedueTimetocheckProcessed, 'HH:mm:ss');
									
									
									if($scope.DaGetToCheck[i].TaskId!=$scope.selected_data.TaskId&&($scope.DaGetToCheck[i].StatusTaskName=="Open" || $scope.DaGetToCheck[i].StatusTaskName=="Reschedule" || $scope.DaGetToCheck[i].StatusTaskName=="Delay") &&($scope.StartToSubmit <= dateduetocheck && datestarttocheck <= $scope.DueToSubmit)) 
									{
									  if($scope.selected_data.FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==false && ($scope.StartTimeToUpdate <= datedueTimetocheck && datestartTimetocheck <= $scope.DueTimeToUpdate))
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  break;
									}
									else 
									{
									  //return false;
									}
								}
							});
					
						AddTaskListFactory.update($scope.selected_data).then(function () {
							
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
							$scope.disabledSaveData = false;
							$rootScope.$emit("RefreshDirective", {});
						}).then(function () {
							$scope.TheUpdateIsReschedule=false;
							$scope.BatalRescheduleTask();
							$scope.BatalLihatTask();
							$scope.HighlightTheIncomplete();
						});
					
				}
				else if($scope.AddTaskListOperation=="Insert")
				{
					if($scope.selected_data.ActivityTypeId==1)
					{
						$scope.selected_data['TaskName']="";
					}
					else if($scope.selected_data.ActivityTypeId==2)
					{
						$scope.selected_data['ProspectId']=null;
						for(var i = 0; i < $scope.optionsTipeTugas.length; i++)
						{
							if($scope.optionsTipeTugas[i].TaskTypeName=="Other")
							{
								$scope.selected_data['TaskTypeId']=$scope.optionsTipeTugas[i].TaskTypeId;
							}
						}
						//$scope.selected_data['TaskTypeId']=5;
					}
					
						$scope.StartToSubmitRaw= $scope.selected_data.StartDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.StartDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.StartDate.getHours()<'10'?'0':'')+ $scope.selected_data.StartDate.getHours())+':'
											+(($scope.selected_data.StartDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.StartDate.getMinutes())+':'
											+(($scope.selected_data.StartDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.StartDate.getSeconds());
						
						$scope.DueToSubmitRaw= $scope.selected_data.DueDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.DueDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.DueDate.getHours()<'10'?'0':'')+ $scope.selected_data.DueDate.getHours())+':'
											+(($scope.selected_data.DueDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.DueDate.getMinutes())+':'
											+(($scope.selected_data.DueDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.DueDate.getSeconds());

						$scope.StartToSubmit=$filter('date')(new Date($scope.StartToSubmitRaw), 'M/d/yy');
						$scope.DueToSubmit=$filter('date')(new Date($scope.DueToSubmitRaw), 'M/d/yy');
						
						AddTaskListFactory.getData("").then(
								function(res){
									$scope.DaGetToCheck = res.data.Result;
								}
							).then(function () {
								for (var i = 0; i < $scope.DaGetToCheck.length; i++) 
								{

									var datestarttocheck=$filter('date')(new Date($scope.DaGetToCheck[i].StartDate), 'M/d/yy');
									var dateduetocheck=$filter('date')(new Date($scope.DaGetToCheck[i].DueDate), 'M/d/yy');
									
									var datestartTimetocheckRaw= $scope.DaGetToCheck[i].StartTime.split(':');
									var datestartTimetocheckProcessed=new Date(2015, 10, 10, datestartTimetocheckRaw[0], datestartTimetocheckRaw[1], datestartTimetocheckRaw[2]);
									var datestartTimetocheck=$filter('date')(datestartTimetocheckProcessed, 'HH:mm:ss');
									
									var datedueTimetocheckRaw= $scope.DaGetToCheck[i].DueTime.split(':');
									var datedueTimetocheckProcessed=new Date(2015, 10, 10, datedueTimetocheckRaw[0], datedueTimetocheckRaw[1], datedueTimetocheckRaw[2]);
									var datedueTimetocheck=$filter('date')(datedueTimetocheckProcessed, 'HH:mm:ss');
									
									
									if(($scope.DaGetToCheck[i].StatusTaskName=="Open" || $scope.DaGetToCheck[i].StatusTaskName=="Reschedule" || $scope.DaGetToCheck[i].StatusTaskName=="Delay") &&($scope.StartToSubmit <= dateduetocheck && datestarttocheck <= $scope.DueToSubmit)) 									
									{

									  if($scope.selected_data.FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==false && ($scope.StartTimeToUpdate <= datedueTimetocheck && datestartTimetocheck <= $scope.DueTimeToUpdate))
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
											$scope.disabledSaveData = false;
									  }
									  
									  break;
									}
									else 
									{
									  //return false;
									}
								}
							});
						
					
						

						AddTaskListFactory.create($scope.selected_data).then(function () {
							
							$rootScope.$emit("RefreshDirective", {});
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
							$scope.disabledSaveData = false;
						}).then(function () {
							$scope.TheUpdateIsReschedule=false;
							//$scope.cancelAddNewTask();
							$scope.listTask = false;
							$scope.tambahTugas = false;
							$scope.addBtn = false;
							$scope.viewType = false;
							$scope.HighlightTheIncomplete();
						});
				}
			}
			else
			{
				if($scope.AddTaskListPaksaErrorJamDue==true || $scope.AddTaskListPaksaErrorJamReminder==true)
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Tolong pastikan ulang data waktu.",
							type: 'warning'
						}
					);
					$scope.disabledSaveData = false;
				}
				else
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data mandatory harus diisi dan pastikan data terisi dengan benar.",
							type: 'warning'
						}
					);
					$scope.disabledSaveData = false;
				}
				
			}

			
        }

        //Begin Calendar
        $scope.refreshCalendar = true;
        $scope.today = function () {

			$scope.bind_data.filter.Date = new Date();
			$scope.HighlightTheIncomplete();
			$scope.SearchWithCalendarChanged($scope.bind_data.filter.Date);

			
        };
		
        
		$scope.bind_data.filter.Date = new Date();
		$scope.today();
		
        $scope.clear = function () {
            $scope.bind_data.filter.Date = null;
            //$scope.dt2 = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function () {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function (year, month, day) {
            $scope.bind_data.filter.Date = new Date(year, month, day);
            //$scope.dt2 = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);

        $scope.testDate = {
            date: new Date(),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(),
            status: 'secondDate'
        };
		
		$scope.testDate3 = {
            date: new Date(),
            status: 'secondDate'
        };

		
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        //End Calendar

        //Begin DatePicker
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.firstDateChange = function () {
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };


		

        //End DatePicker  
        $scope.lookupProspect = function () {
			//$scope.DisableWhenEdit=true;
			
			
			AddTaskListFactory.getDataProspect().then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				setTimeout(function() {     
					//angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');
					angular.element('.ui.modal.ModalProspectBuatAddTaskList').modal('refresh'); 
				}, 0);
				
				setTimeout(function() {
					angular.element('.ui.modal.ModalProspectBuatAddTaskList').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalProspectBuatAddTaskList').not(':first').remove();
					$scope.ProspectSearchValue="";
				}, 1);
				
			});
			$scope.ProspectSearchCriteria=$scope.optionProspectSearchCriteria[0].SearchOptionId;
			
        }
		
        $scope.BatalLookupProspect = function () {
			//$scope.DisableWhenEdit=false;
			angular.element('.ui.modal.ModalProspectBuatAddTaskList').modal('hide');

        }
		
		$scope.AddTaskListBatalKembaliUbah = function () {
			angular.element('.ui.modal.ModalAddTaskListNotifikasiKembali').modal('hide');
        }
		
		$scope.SearchProspect = function () {

			
			if($scope.ProspectSearchCriteria==1 && $scope.ProspectSearchValue!="")
			{
				AddTaskListFactory.GetSearchProspectByName($scope.ProspectSearchValue).then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				
				});
			}
			else if($scope.ProspectSearchCriteria==2 && $scope.ProspectSearchValue!="")
			{
				AddTaskListFactory.GetSearchProspectByProspectCode($scope.ProspectSearchValue).then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				
				});
			}
			else
			{
				AddTaskListFactory.getDataProspect().then(function (res) {
					$scope.gridNoProspect = res.data.Result;
					
				});
			}
			
			

        }
		
		
		

		
        $scope.pilihProspect = function (ProspectNo) {
            $scope.selected_data.ProspectId=angular.copy(ProspectNo.ProspectId);
			$scope.selected_data.ProspectCode=angular.copy(ProspectNo.ProspectCode);
			$scope.selected_data.ProspectName=angular.copy(ProspectNo.ProspectName);
            $scope.selected_data.ProspectHP = angular.copy(ProspectNo.HP);
            $scope.selected_data.ProspectAddress = angular.copy(ProspectNo.Address);
            angular.element('.ui.modal.ModalProspectBuatAddTaskList').modal('hide');
        }
		
		$scope.buatAddTaskListDariProspect = function (){
			$scope.selected_data.ActivityTypeId = 1;
			$scope.DaftarAktivitas();
			$scope.DisableWhenEdit=true;

			$scope.selected_data.FullDayActivityBit=false;
			$scope.IsSeharian=false;

			$scope.selected_data.ImportantTaskBit=false;

			
			AddTaskListFactory.getTipeTugas().then(function (res) {
				$scope.optionsTipeTugas = res.data.Result;
				return $scope.optionsTipeTugas;
			}).then(function () {
				if ($scope.selected_data.ActivityTypeId == 1) 
				{
					for(var ee = 0; ee < $scope.optionsTipeTugas.length; ee++)
					{
						if($scope.optionsTipeTugas[ee].TaskTypeName=="Other")
						{
							$scope.optionsTipeTugas.splice(ee, 1);
						}
					}
				}
				for(var i = 0; i < $scope.optionsTipeTugas.length; i++)
				{	
					if($scope.optionsTipeTugas[i].TaskTypeName=="Telepon")
					{
						$scope.selected_data.TaskTypeId=$scope.optionsTipeTugas[i].TaskTypeId;
					}
				}
			});
			
			//$scope.selected_data.TaskTypeId=$scope.optionsTipeTugas[i].TaskTypeId;
			
			$scope.selected_data.TaskDescription="Telepon Customer/Prospect";
			
			$scope.selected_data.ReminderDate=null;
			
			var RawStartTimeFromProspectDefault={    
				 value: new Date(2015, 10, 10, 8, 0, 0)
				 };
			var RawDueTimeFromProspectDefault={    
				 value: new Date(2015, 10, 10, 10,0, 0)
				 };
			var RawReminderTimeFromProspectDefault={    
				 value: new Date(2015, 10, 10,8, 0, 0)
				 };
			//if CategoryProspectNameDariProspecting
			LeadTimeProspectFuFactory.getData().then(function (res) {
				for(var i = 0; i < res.data.Result.length; i++)
				{	//res.data.Result[0].ProspectId
					if(res.data.Result[i].CategoryProspectName==$rootScope.CategoryProspectNameDariProspecting)
					{
						var ToSetDefaultDates=angular.copy($scope.TodayDate_Raw);
						var ToSetDefaultDates2=angular.copy($scope.TodayDate_Raw);
						$scope.selected_data.StartDate=new Date(ToSetDefaultDates.setDate(ToSetDefaultDates.getDate() + res.data.Result[i].LeadTime));
						$scope.selected_data.DueDate=$scope.selected_data.StartDate;
						$scope.selected_data.ReminderDate=new Date(ToSetDefaultDates2.setDate(ToSetDefaultDates2.getDate() + (res.data.Result[i].LeadTime-1)));
					}
				}
				
			});
			
			$scope.selected_data.StartTime=RawStartTimeFromProspectDefault.value;
			$scope.selected_data.DueTime=RawDueTimeFromProspectDefault.value;
			$scope.selected_data.ReminderTime=RawReminderTimeFromProspectDefault.value;
			
			AddTaskListFactory.getDataProspectIndividual($rootScope.ProspectIdDariProspecting).then(function (res) {
				$scope.selected_data.ProspectId=angular.copy(res.data.Result[0].ProspectId);
				$scope.selected_data.ProspectCode=angular.copy(res.data.Result[0].ProspectCode);
				$scope.selected_data.ProspectName=angular.copy(res.data.Result[0].ProspectName);
				$scope.selected_data.ProspectHP = angular.copy(res.data.Result[0].HP);
				$scope.selected_data.ProspectAddress = angular.copy(res.data.Result[0].Address);
				
			});
			
		}
		
		$scope.buatAddTaskListDariProspectSave = function (){
			$scope.StartTimeToUpdate=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
			$scope.DueTimeToUpdate=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
			$scope.ReminderTimeToUpdate=$filter('date')($scope.selected_data.ReminderTime, 'HH:mm:ss');
			
			$scope.AddTaskListPaksaErrorJamDue=false;
			$scope.AddTaskListPaksaErrorJamReminder=false;
			
			if($scope.selected_data.FullDayActivityBit==false)
			{
				if($scope.selected_data.StartDate==$scope.selected_data.DueDate)
				{
					if($scope.DueTimeToUpdate>=$scope.StartTimeToUpdate)
					{
						$scope.AddTaskListPaksaErrorJamDue=false;
					}
					else
					{
						$scope.AddTaskListPaksaErrorJamDue=true;
					}
				}
				
				if($scope.selected_data.StartDate==$scope.selected_data.ReminderDate)
				{
					if($scope.ReminderTimeToUpdate<=$scope.StartTimeToUpdate)
					{
						$scope.AddTaskListPaksaErrorJamReminder=false;
					}
					else
					{
						$scope.AddTaskListPaksaErrorJamReminder=true;
					}
				}
			}

			$scope.selected_data['StartTime']=$scope.StartTimeToUpdate;
			$scope.selected_data['DueTime']=$scope.DueTimeToUpdate;
			$scope.selected_data['ReminderTime']=$scope.ReminderTimeToUpdate;
			
			$scope.DaErrorCheck=false;

			try
			{
				if($scope.selected_data['TaskDescription'].length>0 &&  (typeof $scope.selected_data['StartDate']!="undefined")&& (typeof $scope.selected_data['DueDate']!="undefined"))
				{	
					if($scope.selected_data['ActivityTypeId']==2)
					{
						if($scope.selected_data['TaskName'].length>0)
						{	
							$scope.DaErrorCheck=true;
						}
								
					}
					else if($scope.selected_data['ActivityTypeId']==1)
					{
						if($scope.selected_data['ProspectId'].toString().length>0)
						{
							$scope.DaErrorCheck=true;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==true)
					{
						if((typeof $scope.selected_data['ReminderTime']!="undefined"))
						{
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==false)
					{
						if((typeof $scope.selected_data['StartTime']!="undefined")&& (typeof $scope.selected_data['DueTime']!="undefined"))
						{
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
							
							
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
				}
				else
				{
					$scope.DaErrorCheck=false;
				}
			}
			catch(eeee)
			{
				$scope.DaErrorCheck=false;
			}
			
			
			
			
			
			
			if($scope.DaErrorCheck==true && $scope.AddTaskListPaksaErrorJamDue==false && $scope.AddTaskListPaksaErrorJamReminder==false)
			{
					if($scope.selected_data.ActivityTypeId==1)
					{
						$scope.selected_data['TaskName']="";
					}
					else if($scope.selected_data.ActivityTypeId==2)
					{
						$scope.selected_data['ProspectId']=null;
						for(var i = 0; i < $scope.optionsTipeTugas.length; i++)
						{
							if($scope.optionsTipeTugas[i].TaskTypeName=="Other")
							{
								$scope.selected_data['TaskTypeId']=$scope.optionsTipeTugas[i].TaskTypeId;
							}
						}
						//$scope.selected_data['TaskTypeId']=5;
					}
					
						$scope.StartToSubmitRaw= $scope.selected_data.StartDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.StartDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.StartDate.getHours()<'10'?'0':'')+ $scope.selected_data.StartDate.getHours())+':'
											+(($scope.selected_data.StartDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.StartDate.getMinutes())+':'
											+(($scope.selected_data.StartDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.StartDate.getSeconds());
						
						$scope.DueToSubmitRaw= $scope.selected_data.DueDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.DueDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.DueDate.getHours()<'10'?'0':'')+ $scope.selected_data.DueDate.getHours())+':'
											+(($scope.selected_data.DueDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.DueDate.getMinutes())+':'
											+(($scope.selected_data.DueDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.DueDate.getSeconds());
					
						$scope.StartToSubmit=$filter('date')(new Date($scope.StartToSubmitRaw), 'M/d/yy');
						$scope.DueToSubmit=$filter('date')(new Date($scope.DueToSubmitRaw), 'M/d/yy');
						
						AddTaskListFactory.getData("").then(
								function(res){
									$scope.DaGetToCheck = res.data.Result;
								}
							).then(function () {
								for (var i = 0; i < $scope.DaGetToCheck.length; i++) 
								{
									var datestarttocheck=$filter('date')(new Date($scope.DaGetToCheck[i].StartDate), 'M/d/yy');
									var dateduetocheck=$filter('date')(new Date($scope.DaGetToCheck[i].DueDate), 'M/d/yy');
									
									var datestartTimetocheckRaw= $scope.DaGetToCheck[i].StartTime.split(':');
									var datestartTimetocheckProcessed=new Date(2015, 10, 10, datestartTimetocheckRaw[0], datestartTimetocheckRaw[1], datestartTimetocheckRaw[2]);
									var datestartTimetocheck=$filter('date')(datestartTimetocheckProcessed, 'HH:mm:ss');
									
									var datedueTimetocheckRaw= $scope.DaGetToCheck[i].DueTime.split(':');
									var datedueTimetocheckProcessed=new Date(2015, 10, 10, datedueTimetocheckRaw[0], datedueTimetocheckRaw[1], datedueTimetocheckRaw[2]);
									var datedueTimetocheck=$filter('date')(datedueTimetocheckProcessed, 'HH:mm:ss');
									
									if(($scope.DaGetToCheck[i].StatusTaskName=="Open" || $scope.DaGetToCheck[i].StatusTaskName=="Reschedule" || $scope.DaGetToCheck[i].StatusTaskName=="Delay") &&($scope.StartToSubmit <= dateduetocheck && datestarttocheck <= $scope.DueToSubmit)) 									
									{
									  if($scope.selected_data.FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==true)
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
									  }
									  else if($scope.selected_data.FullDayActivityBit==false && $scope.DaGetToCheck[i].FullDayActivityBit==false && ($scope.StartTimeToUpdate <= datedueTimetocheck && datestartTimetocheck <= $scope.DueTimeToUpdate))
									  {
											bsNotify.show(
												{
													title: "Peringatan",
													content: "Ada aktifitas yang berlangsung pada tanggal yang dibuat",
													type: 'warning'
												}
											);
									  }
									  break;
									}
									else 
									{
									  //return false;
									}
								}
							});
						
					
						

						AddTaskListFactory.create($scope.selected_data).then(function () {
							
							$rootScope.$emit("RefreshDirective", {});
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
						}).then(function () {
							$scope.BackToProspectFromTaskListchildmethod();
						});
				
			}
			else
			{
				if($scope.AddTaskListPaksaErrorJamDue==true || $scope.AddTaskListPaksaErrorJamReminder==true)
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Tolong pastikan ulang data waktu.",
							type: 'warning'
						}
					);
				}
				else
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data mandatory harus diisi dan pastikan data terisi dengan benar.",
							type: 'warning'
						}
					);
				}
				
			}
			
		}

		$rootScope.$on("lihatADDTASKLISTDARIPROSPECT", function(){
           $scope.buatAddTaskListDariProspect();
        });
		
		$scope.BackToProspectFromTaskList = function () {

			//$scope.ShowProspectDetail = false;
			$scope.BackToProspectFromTaskListchildmethod();
		};
		
		$scope.BackToProspectFromTaskListchildmethod = function() {
            $rootScope.$emit("balikdariDETILINFOTASKLIST", {});
        }




        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mAddTaskList = null; //Model
        $scope.xRole = { selected: [] };
		
		
		
		

        //----------------------------------
        // Get Data
        //----------------------------------
       

        //----------------------------------
        // Grid Setup
        //----------------------------------
       
        
    });
