angular.module('app')
  .factory('ListProspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(ActivityChecklistItemDecFactory) {
        return $http.post('/api/fw/Role', [{
                                            SalesProgramName: ActivityChecklistItemDecFactory.Name
                                           }]);
      },
      update: function(ActivityGroupChecklistItemDecFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: ActivityChecklistItemDecFactory.Id,
                                            //pid: role.pid,
                                            SalesName: ActivityChecklistItemDecFactory.Name
                                           }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });