angular.module('app')
  .factory('DailySalesFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(negotiation) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      update: function(negotiation){
        return $http.put('/api/fw/Role', [{
                                            Id: negotiation.Id,
                                            //pid: negotiation.pid,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });