angular.module('app')
  .factory('ViewSalesmanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
		var str = param;
		//var AlreadyReplaced1 = str.replace("&StatusTaskId=1", "");
		var AlreadyReplaced1 = str.replace("&StatusTaskId=1&", "");
		var AlreadyReplaced2 = AlreadyReplaced1.replace("&ActivityTypeId=3", "");
	  
       var res=$http.get('/api/sales/SDSActivitySupervisorPerSales/Filter/?'+AlreadyReplaced2);
        return res;
      },
	  
	  getDataSales: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=salesman');
        //console.log('res=>',res);
        return res;
      },
	  
       getComment: function() {
                //var res=$http.get('/api/fw/Role');
                var result = [
                    { CommentId: "1", CommentUser: "Jeane", CommentDate: "17-01-2017", CommentContent: "Hi, I wanted to make sure got the latest product update. Did Admin's feature get it to me?" },
                    { CommentId: "2", CommentUser: "Calvina", CommentDate: "11-02-2017", CommentContent: "Did the product is good?" },
                    { CommentId: "3", CommentUser: "Irene", CommentDate: "15-02-2017", CommentContent: "How much the full version of the product?" },
                ];

                return result;
            },
	
	getDataProspect: function (DaSalesId) {
        //var res = $http.get('/api/sales/SDSGetProspectList');
		var res = $http.get('/api/sales/SDSGetProspectList/?SalesId='+DaSalesId);
        //console.log('res=>',res);
        return res;
      },
	  
	getDataForCalendar: function() {
		var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=1');
		return res;
        
      },
	  
	getDataForCalendarCompleteOrCancel: function() {
		var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=2');
		return res;
      },
	  
	getDataForCalendarDelay: function() {
		var res=$http.get('/api/sales/SDSActivitySupervisorAll/StartDate/?statusTaskId=0');
		return res;
      },  
	  
	getDataForFilterByCalendar: function(StartDate) {
		var res=$http.get('/api/sales/SDSActivitySupervisorPerSales/Filter/?Date='+StartDate);
		return res;
      },  

	getStatusTask: function() {
        var res=$http.get('/api/sales/SDSStatusTask');

        return res;
      },
	  
	GetProspectSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "1", SearchOptionName: "Nama Prospect/Nama Pelanggan" },
          { SearchOptionId: "2", SearchOptionName: "Kode Prospect" }
        ];

        var res=da_json

        return res;
      },
	  
	  GetSearchProspectByName: function(SearchValue,DaSalesId) {
        var res=$http.get('/api/sales/SDSGetProspectList/?prospectSPKCode=&ProspectName='+SearchValue+"&SalesId="+DaSalesId);

        return res;
      },
	  GetSearchProspectByProspectCode: function(SearchValue,DaSalesId) {
        var res=$http.get('/api/sales/SDSGetProspectList/?prospectSPKCode='+SearchValue+'&ProspectName='+"&SalesId="+DaSalesId);

        return res;
      },  
	
			
	getDataSales: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=salesman');
        //console.log('res=>',res);
        return res;
      },
	  getTipeAkivitas: function() {
        var res=$http.get('/api/sales/SDSActivityType')
        return res;
      },
	  getTipeTugas: function() {
        var res=$http.get('/api/sales/SDSActivityTaskType')
        return res;
      },
	  
      create: function(ViewSalesman) {
	  

		console.log("setan",ViewSalesman);
        return $http.post('/api/sales/SDSActivitySupervisor', ViewSalesman);
      },
	  MakeComment: function(ViewSalesman) {
        return $http.post('/api/sales/SDSComment', [{
                                            //AppId: 1,
                                            TaskId: ViewSalesman.TaskId,
											SalesId:ViewSalesman.SalesId,
											CommentDate: ViewSalesman.CommentDate,
                                            CommentDesc: ViewSalesman.CommentDesc}]);
      },
      update: function(ViewSalesman){
	  
		var Da_ReminderDate="";
		try
		{
			Da_ReminderDate= ViewSalesman.ReminderDate.getFullYear()+'-'
												  +('0' + (ViewSalesman.ReminderDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + ViewSalesman.ReminderDate.getDate()).slice(-2)+'T'
												  +((ViewSalesman.ReminderDate.getHours()<'10'?'0':'')+ ViewSalesman.ReminderDate.getHours())+':'
												  +((ViewSalesman.ReminderDate.getMinutes()<'10'?'0':'')+ ViewSalesman.ReminderDate.getMinutes())+':'
												  +((ViewSalesman.ReminderDate.getSeconds()<'10'?'0':'')+ ViewSalesman.ReminderDate.getSeconds());
		}
		catch(e1)
		{
			Da_ReminderDate=null;
		}
	  
        return $http.put('/api/sales/SDSActivitySupervisor', [{
                                            OutletId: ViewSalesman.OutletId,
											TaskId: ViewSalesman.TaskId,
                                            TaskCode: ViewSalesman.TaskCode,
											TaskName: ViewSalesman.TaskName,
											TaskDescription: ViewSalesman.TaskDescription,
											StartDate: ViewSalesman.StartDate.getFullYear()+'-'
											+('0' + (ViewSalesman.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ViewSalesman.StartDate.getDate()).slice(-2)+'T'
											+((ViewSalesman.StartDate.getHours()<'10'?'0':'')+ ViewSalesman.StartDate.getHours())+':'
											+((ViewSalesman.StartDate.getMinutes()<'10'?'0':'')+ ViewSalesman.StartDate.getMinutes())+':'
											+((ViewSalesman.StartDate.getSeconds()<'10'?'0':'')+ ViewSalesman.StartDate.getSeconds()),
											
											StartTime: ViewSalesman.StartTime,
											DueDate: ViewSalesman.DueDate.getFullYear()+'-'
											+('0' + (ViewSalesman.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + ViewSalesman.DueDate.getDate()).slice(-2)+'T'
											+((ViewSalesman.DueDate.getHours()<'10'?'0':'')+ ViewSalesman.DueDate.getHours())+':'
											+((ViewSalesman.DueDate.getMinutes()<'10'?'0':'')+ ViewSalesman.DueDate.getMinutes())+':'
											+((ViewSalesman.DueDate.getSeconds()<'10'?'0':'')+ ViewSalesman.DueDate.getSeconds()),
											
                                            DueTime: ViewSalesman.DueTime,
											ReminderDate: Da_ReminderDate,
											
											ReminderTime: ViewSalesman.ReminderTime,
                                            FullDayActivityBit: ViewSalesman.FullDayActivityBit,
											ImportantTaskBit: ViewSalesman.ImportantTaskBit,
											ActionName: ViewSalesman.ActionName,
                                            MovingTaskDesc: ViewSalesman.MovingTaskDesc,
											SalesId: ViewSalesman.SalesId,
											ProspectId: ViewSalesman.ProspectId,
                                            ActivityTypeId: ViewSalesman.ActivityTypeId,
											TaskTypeId: ViewSalesman.TaskTypeId,
											ActivityPreDefineRespondId: ViewSalesman.ActivityPreDefineRespondId,
                                            ReasonNote: ViewSalesman.ReasonNote,
											StatusTaskId: ViewSalesman.StatusTaskId,
                                            StatusCode: ViewSalesman.StatusCode}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd