angular.module('app')
    .controller('ViewSalesmanController', function($scope,$filter, $http, CurrentUser, ViewSalesmanFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.TodayDate_Raw=new Date();
	$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
	$scope.KembaliAfterComment=false;
	//$scope.ViewSalesmanKomenTab=true;
	$scope.ShowViewSalesmanSearch=false;
	$scope.viewcalendar=false;
	
	ViewSalesmanFactory.getDataSales().then(function (res) {
			$scope.optionsSalesForce = res.data.Result;
			return $scope.optionsSalesForce;
		});
		
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalProspect').remove();

		});	

	$scope.optionProspectSearchCriteria = ViewSalesmanFactory.GetProspectSearchDropdownOptions();

	ViewSalesmanFactory.getTipeAkivitas().then(function (res) {
			$scope.optionsTipeAktivitas = res.data.Result;
			return $scope.optionsTipeAktivitas;
		}).then(function () {
			$scope.optionsTipeAktivitasFiltered=angular.copy($scope.optionsTipeAktivitas);
			$scope.optionsTipeAktivitasFiltered.splice(0,$scope.optionsTipeAktivitas.length);

			for(var i = 0; i < $scope.optionsTipeAktivitas.length; ++i)
			{	//alert($scope.optionsTipeAktivitas[i].ActivityTypeName);
				if($scope.optionsTipeAktivitas[i].ActivityTypeName == "Customer/Prospect" || $scope.optionsTipeAktivitas[i].ActivityTypeName == "Others")
				{
					$scope.optionsTipeAktivitasFiltered.push($scope.optionsTipeAktivitas[i]);
				}
			}
		});	
	
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
	
	$scope.HighlightTheIncomplete = function () {
			$scope.events=[];
			$scope.Color_Da_Calendar=[{date:null,status: ''}];
			$scope.Color_Da_Calendar.splice(0,1);
			
			$scope.Raw_Color_Da_Calendar_Incomplete =[];
			$scope.Raw_Color_Da_Calendar_Complete =[];
			
			ViewSalesmanFactory.getDataForCalendar().then(function (res) {
				$scope.Raw_Color_Da_Calendar_Incomplete = res.data;
				for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Incomplete.length; i++)
				{	
					$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Incomplete[i].toString()),status: 'secondDate'});
				}
				
				ViewSalesmanFactory.getDataForCalendarCompleteOrCancel().then(function (res) {
					$scope.Raw_Color_Da_Calendar_Complete = res.data;
					for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Complete.length; i++)
					{	
						for(var ix = 0; ix < $scope.Raw_Color_Da_Calendar_Incomplete.length; ix++)
						{
							if($scope.Raw_Color_Da_Calendar_Complete[i].date==$scope.Raw_Color_Da_Calendar_Incomplete[ix].date)
							{//harusnya ini != tapi entah napa == mala jalan != mala kaga
								$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Complete[i].toString()),status: 'thirdDate'});
							}

						}
						if($scope.Raw_Color_Da_Calendar_Incomplete.length<=0)
						{
							$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Complete[i].toString()),status: 'thirdDate'});
						}
					}
					
					ViewSalesmanFactory.getDataForCalendarDelay().then(function (res2) {
						$scope.Raw_Color_Da_Calendar_Delay = res2.data;
						for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Delay.length; i++)
						{	
							$scope.Color_Da_Calendar.push({date:new Date($scope.Raw_Color_Da_Calendar_Delay[i].toString()),status: 'fourthDate'});
						}
						
						for(var i = 0; i < $scope.Raw_Color_Da_Calendar_Delay.length; i++)
						{	
							for(var ii = 0; ii < $scope.Color_Da_Calendar.length; ii++)
							{
								var da_tanggal_to_compare=new Date($scope.Raw_Color_Da_Calendar_Delay[i].toString());
								if($scope.Color_Da_Calendar[ii].date.toString()==da_tanggal_to_compare.toString())
								{
									$scope.Color_Da_Calendar[ii].status= 'fourthDate';
								}
							}
						}
						
						for(var ii = 0; ii < $scope.Color_Da_Calendar.length; ii++)
						{	
							$scope.events.push($scope.Color_Da_Calendar[ii]);
							$scope.refreshCalendar = false;
							$timeout(function () { $scope.refreshCalendar = true }, 0);
						}
					});
					
					
				});

			});

        }
		

	
	$scope.SaveData = function () {
			$scope.StartTimeToUpdate=$filter('date')($scope.selected_data.StartTime, 'HH:mm:ss');
			$scope.DueTimeToUpdate=$filter('date')($scope.selected_data.DueTime, 'HH:mm:ss');
			$scope.ReminderTimeToUpdate=$filter('date')($scope.selected_data.ReminderTime, 'HH:mm:ss');
			
			$scope.ViewSalesmanPaksaErrorJamDue=false;
			$scope.ViewSalesmanPaksaErrorJamReminder=false;
			
			if($scope.selected_data.FullDayActivityBit==false)
			{
				
				var UlalaStartDate=angular.copy($scope.selected_data.StartDate);
				var UlalaDueDate=angular.copy($scope.selected_data.DueDate);
				var UlalaReminderDate=angular.copy($scope.selected_data.ReminderDate)
				
				
				if(UlalaStartDate.toString()==UlalaDueDate.toString())
				{
					if($scope.DueTimeToUpdate>=$scope.StartTimeToUpdate)
					{
						$scope.ViewSalesmanPaksaErrorJamDue=false;
					}
					else
					{
						$scope.ViewSalesmanPaksaErrorJamDue=true;
					}
				}
				
				if(UlalaStartDate.toString()==UlalaReminderDate.toString())
				{
					if($scope.ReminderTimeToUpdate<=$scope.StartTimeToUpdate)
					{
						$scope.ViewSalesmanPaksaErrorJamReminder=false;
					}
					else
					{
						$scope.ViewSalesmanPaksaErrorJamReminder=true;
					}
				}
			}
			
			$scope.selected_data['StartTime']=$scope.StartTimeToUpdate;
			$scope.selected_data['DueTime']=$scope.DueTimeToUpdate;
			$scope.selected_data['ReminderTime']=$scope.ReminderTimeToUpdate;


			$scope.DaErrorCheck=false;
			try
			{
				if($scope.selected_data['TaskDescription'].length>0 &&  (typeof $scope.selected_data['StartDate']!="undefined")&& (typeof $scope.selected_data['DueDate']!="undefined"))
				{	
					if($scope.selected_data['ActivityTypeId']==2)
					{
						if($scope.selected_data['TaskName'].length>0)
						{	
							$scope.DaErrorCheck=true;
						}
								
					}
					else if($scope.selected_data['ActivityTypeId']==1)
					{
						if($scope.selected_data['ProspectId'].toString().length>0)
						{
							$scope.DaErrorCheck=true;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==true)
					{
						if((typeof $scope.selected_data['ReminderDate']!="undefined")||(typeof $scope.selected_data['ReminderTime']!="undefined"))
						{
							
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
					
					if($scope.selected_data.FullDayActivityBit==false)
					{
						if((typeof $scope.selected_data['StartTime']!="undefined")&& (typeof $scope.selected_data['DueTime']!="undefined"))
						{
							$scope.DaErrorCheck=false;
							if((typeof $scope.selected_data['ReminderDate']!="undefined" && ( $scope.selected_data['ReminderDate']!=null))&&(typeof $scope.selected_data['ReminderTime']!="undefined" && ( $scope.selected_data['ReminderTime']!=null)))
							{
								$scope.DaErrorCheck=true;
							}
							else
							{
								$scope.DaErrorCheck=false;
							}
							
							
						}
						else
						{
							$scope.DaErrorCheck=false;
						}
					}
				}
				else
				{
					$scope.DaErrorCheck=false;
				}
			}
			catch(eeee)
			{
				$scope.DaErrorCheck=false;
			}
			
			if($scope.DaErrorCheck==true && $scope.ViewSalesmanPaksaErrorJamDue==false && $scope.ViewSalesmanPaksaErrorJamReminder==false)
			{
				if($scope.selected_data.ActivityTypeId==1)
				{
					$scope.selected_data['TaskName']="";
				}
				else if($scope.selected_data.ActivityTypeId==2)
				{	
					$scope.selected_data['ProspectId']=null;
					for(var i = 0; i < $scope.optionsTipeTugas.length; i++)
					{
						if($scope.optionsTipeTugas[i].TaskTypeName=="Other")
						{
							$scope.selected_data['TaskTypeId']=$scope.optionsTipeTugas[i].TaskTypeId;
						}
					}
				}
				
				$scope.selected_data.StartDate=$scope.selected_data.StartDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.StartDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.StartDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.StartDate.getHours()<'10'?'0':'')+ $scope.selected_data.StartDate.getHours())+':'
											+(($scope.selected_data.StartDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.StartDate.getMinutes())+':'
											+(($scope.selected_data.StartDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.StartDate.getSeconds());
							
				$scope.selected_data.DueDate=$scope.selected_data.DueDate.getFullYear()+'-'
											+('0' + ($scope.selected_data.DueDate.getMonth()+1)).slice(-2)+'-'
											+('0' + $scope.selected_data.DueDate.getDate()).slice(-2)+'T'
											+(($scope.selected_data.DueDate.getHours()<'10'?'0':'')+ $scope.selected_data.DueDate.getHours())+':'
											+(($scope.selected_data.DueDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.DueDate.getMinutes())+':'
											+(($scope.selected_data.DueDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.DueDate.getSeconds());

				try
				{
					$scope.selected_data.ReminderDate= $scope.selected_data.ReminderDate.getFullYear()+'-'
														  +('0' + ($scope.selected_data.ReminderDate.getMonth()+1)).slice(-2)+'-'
														  +('0' + $scope.selected_data.ReminderDate.getDate()).slice(-2)+'T'
														  +(($scope.selected_data.ReminderDate.getHours()<'10'?'0':'')+ $scope.selected_data.ReminderDate.getHours())+':'
														  +(($scope.selected_data.ReminderDate.getMinutes()<'10'?'0':'')+ $scope.selected_data.ReminderDate.getMinutes())+':'
														  +(($scope.selected_data.ReminderDate.getSeconds()<'10'?'0':'')+ $scope.selected_data.ReminderDate.getSeconds());
				}
				catch(e1)
				{
					$scope.selected_data.ReminderDate=null;
				}
				
				if($scope.selected_data['ActivityTypeId']==1)
				{
					var ViewSalesmanSingle=[{TaskCode: $scope.selected_data.TaskCode,
													TaskName: $scope.selected_data.TaskName,
													TaskDescription: $scope.selected_data.TaskDescription,
													StartDate: $scope.selected_data.StartDate,
													StartTime: $scope.selected_data.StartTime,
													DueDate: $scope.selected_data.DueDate,
													DueTime: $scope.selected_data.DueTime,
													ReminderDate: $scope.selected_data.ReminderDate,
													ReminderTime: $scope.selected_data.ReminderTime,
													FullDayActivityBit: $scope.selected_data.FullDayActivityBit,
													ImportantTaskBit: $scope.selected_data.ImportantTaskBit,
													ActionName: $scope.selected_data.ActionName,
													MovingTaskDesc: $scope.selected_data.MovingTaskDesc,
													SalesId: $scope.selected_data.SalesId,
													ProspectId: $scope.selected_data.ProspectId,
													ActivityTypeId: $scope.selected_data.ActivityTypeId,
													TaskTypeId: $scope.selected_data.TaskTypeId,
													ActivityPreDefineRespondId: $scope.selected_data.ActivityPreDefineRespondId,
													StatusTaskId: $scope.selected_data.StatusTaskId}];
					
					ViewSalesmanFactory.create(ViewSalesmanSingle).then(function () {
						ViewSalesmanFactory.getData("").then(
							function(res){
								$scope.getSalesman  = res.data.Result; 
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil tersimpan.",
										type: 'success'
									}
								);
							}
						).then(function () {
							$scope.cancelAddNewTask();
							$scope.HighlightTheIncomplete();
						});
					});
				}
				else if($scope.selected_data['ActivityTypeId']==2)
				{
					$scope.ViewSalesmanMultiTambah=[{TaskCode: null,
													TaskName: null,
													TaskDescription: null,
													StartDate: null,
													StartTime: null,
													DueDate: null,
													DueTime: null,
													ReminderDate: null,
													ReminderTime: null,
													FullDayActivityBit: null,
													ImportantTaskBit: null,
													ActionName: null,
													MovingTaskDesc: null,
													SalesId: null,
													ProspectId: null,
													ActivityTypeId: null,
													TaskTypeId: null,
													ActivityPreDefineRespondId: null,
													StatusTaskId: null}];
													
					$scope.ViewSalesmanMultiTambah.splice(0,1);
					
					for(var ulala = 0; ulala < $scope.selected_data.DaSalesIdMulti.length; ulala++)
					{
						$scope.ViewSalesmanMultiTambah.push({TaskCode: $scope.selected_data.TaskCode,
													TaskName: $scope.selected_data.TaskName,
													TaskDescription: $scope.selected_data.TaskDescription,
													StartDate: $scope.selected_data.StartDate,
													StartTime: $scope.selected_data.StartTime,
													DueDate: $scope.selected_data.DueDate,
													DueTime: $scope.selected_data.DueTime,
													ReminderDate: $scope.selected_data.ReminderDate,
													ReminderTime: $scope.selected_data.ReminderTime,
													FullDayActivityBit: $scope.selected_data.FullDayActivityBit,
													ImportantTaskBit: $scope.selected_data.ImportantTaskBit,
													ActionName: $scope.selected_data.ActionName,
													MovingTaskDesc: $scope.selected_data.MovingTaskDesc,
													SalesId: $scope.selected_data.DaSalesIdMulti[ulala],
													ProspectId: $scope.selected_data.ProspectId,
													ActivityTypeId: $scope.selected_data.ActivityTypeId,
													TaskTypeId: $scope.selected_data.TaskTypeId,
													ActivityPreDefineRespondId: $scope.selected_data.ActivityPreDefineRespondId,
													StatusTaskId: $scope.selected_data.StatusTaskId});
					}
					ViewSalesmanFactory.create($scope.ViewSalesmanMultiTambah).then(function () {
						ViewSalesmanFactory.getData("").then(
							function(res){
								$scope.getSalesman  = res.data.Result; 
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil tersimpan.",
										type: 'success'
									}
								);
							}
						).then(function () {
							$scope.cancelAddNewTask();
							$scope.HighlightTheIncomplete();
						});
					});
				}
				
				
			}
			else
			{
				if($scope.ViewSalesmanPaksaErrorJamDue==true || $scope.ViewSalesmanPaksaErrorJamReminder==true)
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Tolong pastikan ulang data waktu.",
							type: 'warning'
						}
					);
				}
				else
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data mandatory harus di isi dan pastikan data terisi dengan benar.",
							type: 'warning'
						}
					);
				}
				
			}

				
				
        }
	
	$scope.tipeCustomer = false;
        $scope.tipeOther = false;
        $scope.DaftarAktivitas = function () {
		
			//$scope.selectionItemTipeTugas = listDataTipeTugas;
			ViewSalesmanFactory.getTipeTugas().then(function (res) {
				$scope.optionsTipeTugas = res.data.Result;
				return $scope.optionsTipeTugas;
			}).then(function () {
				if ($scope.selected_data.ActivityTypeId == 1) 
				{
					for(var ee = 0; ee < $scope.optionsTipeTugas.length; ee++)
					{
						if($scope.optionsTipeTugas[ee].TaskTypeName=="Other")
						{
							$scope.optionsTipeTugas.splice(ee, 1);
						}
					}
				}
				
				if($scope.selected_data.ActivityTypeId == 1&&$scope.ViewSalesmanOperation=="Insert")
				{
					$scope.selected_data.TaskTypeId=$scope.optionsTipeTugas[0].TaskTypeId;
				}
			});
		
            if ($scope.selected_data.ActivityTypeId == 1) {
                $scope.comboTipeTugas = true;
                $scope.tipeCustomer = true;
                $scope.tipeOther = false;
				

            }
            else if ($scope.selected_data.ActivityTypeId == 2) {
                $scope.comboTipeTugas = false;
                $scope.tipeOther = true;
                $scope.tipeCustomer = false;
				
            }
        };
		
	$scope.SehariPenuhChanged = function () {

			if($scope.selected_data.FullDayActivityBit==true)
			{
				$scope.IsSeharian=true;
			}
			else if($scope.selected_data.FullDayActivityBit==false)
			{
				$scope.IsSeharian=false;
			}

        }
		
	$scope.ToggleViewSalesmanSearch = function () {

			$scope.ShowViewSalesmanSearch=!$scope.ShowViewSalesmanSearch;

        }

	$scope.RefreshViewSalesmanData = function () {
		ViewSalesmanFactory.getData("").then(function(res){
            $scope.getSalesman = res.data.Result;
			$scope.HighlightTheIncomplete();
            return $scope.getSalesman;
        });
		$scope.ShowViewSalesmanSearch=false;
		//$scope.viewcalendar=false;
    }

	$scope.SearchViewSalesman = function () {
		
		
		
		var Da_SearchByStatus="";
		var Da_SearchByTipeAktivitas="";
		
		
		if((typeof $scope.SearchByTipeAktivitas=="undefined")||$scope.SearchByTipeAktivitas=="")
		{
			Da_SearchByTipeAktivitas="";
		}
		else
		{
			Da_SearchByTipeAktivitas="&ActivityTypeId="+$scope.SearchByTipeAktivitas;
		}
		
		if((typeof $scope.SearchByStatus=="undefined")||$scope.SearchByStatus=="")
		{
			Da_SearchByStatus="";
		}
		else
		{
			if(Da_SearchByTipeAktivitas=="")
			{
				Da_SearchByStatus="StatusTaskId="+$scope.SearchByStatus;
			}
			else
			{
				Da_SearchByStatus="&StatusTaskId="+$scope.SearchByStatus;
			}
			
		}
		
		if(Da_SearchByStatus=="")
		{
			
			if((typeof $scope.SearchByTipeAktivitas=="undefined")||$scope.SearchByTipeAktivitas=="")
			{
				Da_SearchByTipeAktivitas="";
			}
			else
			{
				Da_SearchByTipeAktivitas="ActivityTypeId="+$scope.SearchByTipeAktivitas;
			}
			
		}
		else
		{
			Da_SearchByTipeAktivitas="&ActivityTypeId="+$scope.SearchByTipeAktivitas;
		}
		
		
		var DaStartDateSearchParam="";
		var DaViewSalesmanKalendernyNongol=angular.copy($scope.viewcalendar);
		
		if($scope.dt1==null || (typeof $scope.dt1=="undefined")||$scope.dt1=="")
		{
			DaStartDateSearchParam="";
		}
		else
		{
			// if(Da_SearchByStatus=="" && Da_SearchByTipeAktivitas=="")
			// {
				// DaStartDateSearchParam="StartDate="+$scope.dt1;
			// }
			// else
			// {
				// DaStartDateSearchParam="&StartDate="+$scope.dt1;
			// }
			console.log("setan1",DaViewSalesmanKalendernyNongol);
			//console.log("setan2",$scope.dt1);
			if(DaViewSalesmanKalendernyNongol==true)
			{
				DaStartDateSearchParam="&Date="+$scope.dt1;
			}
			else
			{
				DaStartDateSearchParam="";
			}
			
			
		}
	
		ViewSalesmanFactory.getData(Da_SearchByStatus+Da_SearchByTipeAktivitas+DaStartDateSearchParam).then(function(res){
            $scope.getSalesman = res.data.Result;
            return $scope.getSalesman;
        });
		
    }
	
	$scope.DaStartDateChanged = function () {
			$scope.MinDueDate_Raw=$scope.selected_data.StartDate;
			$scope.MinDueDate=$scope.MinDueDate_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDate_Raw.getDate()).slice(-2);
			
			//$scope.MaxReminderDate_Raw=$scope.selected_data.StartDate;
			//$scope.MaxReminderDate=$scope.MaxReminderDate_Raw.getFullYear()+'-'+('0' + ($scope.MaxReminderDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MaxReminderDate_Raw.getDate()).slice(-2);
		
			$scope.MaxReminderDate=$scope.selected_data.StartDate;
			
			if($scope.selected_data.StartDate>=$scope.selected_data.DueDate)
			{
				$scope.selected_data.DueDate=$scope.selected_data.StartDate;
				$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
			}
			
			if($scope.selected_data.StartDate<=$scope.selected_data.ReminderDate)
			{
				$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
			}
			
		}
	
	$scope.addNewTask = function () {
			$scope.comboTipeTugas = false;
            $scope.tipeCustomer = false;
            $scope.tipeOther = false;
			
			$scope.ViewSalesmanOperation="Insert";
            $scope.tambahTugas = true;
            $scope.addBtn = true;
            $scope.viewType = true;//
            $scope.selected_data = [];
			$scope.viewcalendar=false;
			$scope.MinDueDate_Raw=new Date();
			$scope.MinDueDate=$scope.MinDueDate_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDate_Raw.getDate()).slice(-2);
			
			$scope.ViewSalesman=true;
			$scope.selected_data.FullDayActivityBit=false;
			$scope.IsSeharian=false;
			$scope.selected_data.ReminderDate=null;
			$scope.selected_data.ReminderTime=null;
			
			$scope.selected_data.StartDate=$scope.TodayDate_Raw;
			$scope.selected_data.DueDate=$scope.TodayDate_Raw;
			$scope.selected_data.ReminderDate=$scope.TodayDate_Raw;
			
			var Da_Sekarang=new Date();
			var Da_Waktu_Sekarang=$filter('date')(Da_Sekarang, 'HH:mm');
			var RawSekarangTime_Get = Da_Waktu_Sekarang.split(':');
				$scope.ProcessedSekarangTime_Get = {    
				 value: new Date($scope.TodayDate_Raw.getFullYear(), $scope.TodayDate_Raw.getMonth(), $scope.TodayDate_Raw.getDate(), RawSekarangTime_Get[0], RawSekarangTime_Get[1])
				 };
				 
			$scope.selected_data.StartTime=angular.copy($scope.ProcessedSekarangTime_Get.value);
			$scope.selected_data.DueTime=angular.copy($scope.ProcessedSekarangTime_Get.value);
			$scope.selected_data.DueTime.setHours($scope.selected_data.DueTime.getHours()+1);
			$scope.selected_data.ReminderTime=angular.copy($scope.ProcessedSekarangTime_Get.value);

        }
		
	$scope.ViewSalesmanReminderChanged = function () {
		if($scope.selected_data.ReminderDate==null)
		{
			$scope.MaxReminderDate=null;
		}
		else
		{
			//$scope.MaxReminderDate=$scope.MaxReminderDate_Raw.getFullYear()+'-'+('0' + ($scope.MaxReminderDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MaxReminderDate_Raw.getDate()).slice(-2);
			if($scope.selected_data.StartDate<=$scope.selected_data.ReminderDate)
			{
				$scope.selected_data.ReminderDate=$scope.selected_data.StartDate;
			}
		}
	}
		
	$scope.cancelAddNewTask = function () {
            $scope.movingTask = false;
            $scope.btnPindahTugas = false;
            $scope.btnKembaliList = false;
            $scope.btnSimpanTask = false;
            $scope.btnKembaliPindah = false;
			
			$scope.tambahTugas = false;
            $scope.addBtn = false;
            $scope.viewType = false;//
            $scope.selected_data = [];

			$scope.ViewSalesman=false;//
			
        }	

    $scope.commentInput = {
            Id: "",
            CommentUser: "New User",
            CommentDate: Date.now(),
            CommentContent: ""
        };

        $scope.intended_operation = "None";

        $scope.ListComment = ViewSalesmanFactory.getComment();

        $scope.addNewComment = function (data) {
            $scope.commentInput = {
                TaskId: $scope.Selected_task.TaskId,
				SalesId: $scope.user.EmployeeId,
				CommentDate: new Date(),
                CommentDesc: data.CommentContent
            };
            
			if((typeof $scope.commentInput.CommentDesc!="undefined") && $scope.commentInput.CommentDesc!="null" && $scope.commentInput.CommentDesc!="")
			{
				ViewSalesmanFactory.MakeComment($scope.commentInput).then(function () {
					$scope.Selected_task.ListSDSCommentView.push({'SalesId':$scope.user.EmployeeId,'TaskId':$scope.Selected_task.TaskId,'CommentDate':new Date(),'CommentDesc':$scope.commentInput.CommentDesc,'SalesName':$scope.user.EmployeeName});
					$scope.KembaliAfterComment=true;
				});
			}
			else
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Pastikan comment terisi dengan benar!",
							type: 'warning'
						}
					);
			}
			
			
			
        }
		
	$scope.SearchWithCalendarChanged = function (calendarThingy) {
			var da_DateToSearch=calendarThingy;
			var da_DateToSearchKirim=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1)).slice(-2)+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';
			$scope.dt1=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1)).slice(-2)+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';

			ViewSalesmanFactory.getDataForFilterByCalendar(da_DateToSearchKirim).then(
							function(res){
								$scope.getSalesman = res.data.Result;
								$scope.ShowListGroup=true
								$scope.loading=false;
							}
						);
						
						
						
        }	

     $scope.viewCalendarMode = function () {
            if ($scope.viewcalendar == false) {
				//$scope.today();
                $scope.viewcalendar = true;
            } else{
                $scope.viewcalendar = false;
				//$scope.dt1=null;
				ViewSalesmanFactory.getData("").then(function(res){
					$scope.getSalesman = res.data.Result;
					return $scope.getSalesman;
				});
				
            }
        }

        //Begin Calendar
        $scope.refreshCalendar = true;
        $scope.today = function () {
            $scope.dt1 = new Date();
			$scope.HighlightTheIncomplete();
			$scope.SearchWithCalendarChanged($scope.dt1);
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt1 = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function () {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function (year, month, day) {
            $scope.dt1 = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);

        $scope.testDate = {
            date: new Date(2017, 2, 11),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(2017, 2, 15),
            status: 'firstDate'
        };

        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        $scope.events.push($scope.testDate);


        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        //End Calendar

        //Begin DatePicker
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.firstDateChange = function () {
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        //End DatePicker 

        $scope.lihatDetailTask = function (task) {
            $scope.ViewSalesmanOperation="Lihat";
			$scope.ViewSalesman = true;
            $scope.detilTugas = true;
            $scope.viewType = true;
			$scope.viewcalendar=false;
			//$scope.ViewSalesmanKomenTab=true;
			//$scope.btnPindahTugas = false;
            $scope.Selected_task = task;
            
        }
         $scope.batalLihatDetailTask = function () {
            $scope.ViewSalesman = false;
            $scope.detilTugas = false;
            $scope.viewType = false;
			
			if($scope.KembaliAfterComment==true)
			{
				ViewSalesmanFactory.getData("").then(
				function(res){
						$scope.getSalesman  = res.data.Result;
						$scope.KembaliAfterComment=false;
						$scope.loading=false;
					}
				);
			}
         
            
        }
        $scope.lihatKomen = function (task) {
            $scope.ViewSalesman = true;
            $scope.detilTugas = true;
            $scope.viewType = true;
            $scope.Selected_task = task;
            $scope.activeForm = 1;
            
        }
        $scope.pindahTugaskanTask = function () {
            
			if($scope.Selected_task.StatusTaskName=="Canceled"||$scope.Selected_task.StatusTaskName=="Completed")
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Tidak dapat melakukan reschedule/perubahan karena status sudah completed/cancelled.",
							type: 'warning'
						}
					);
			}
			else
			{
				$scope.movingTask = true;
				$scope.btnPindahTugas = true;
				$scope.btnKembaliList = true;
				$scope.btnSimpanTask = true;
				$scope.btnKembaliPindah = true;
				$scope.ViewSalesmanIdSalesmanAsli={};
				$scope.ViewSalesmanIdSalesmanAsli=angular.copy($scope.Selected_task.SalesId);
				//$scope.ViewSalesmanKomenTab=false;
			}
			
			
        }
        $scope.batalpindahTask = function () {
            $scope.movingTask = false;
            $scope.btnPindahTugas = false;
            $scope.btnKembaliList = false;
            $scope.btnSimpanTask = false;
            $scope.btnKembaliPindah = false;
			//$scope.ViewSalesmanKomenTab=true;
        }
		
		
		$scope.simpanTask = function () {
			
			ViewSalesmanFactory.update($scope.Selected_task).then(function () {
					ViewSalesmanFactory.getData("").then(
						function(res){
							$scope.getSalesman = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data berhasil tersimpan.",
									type: 'success'
								}
							);
							return res.data;
						}
					);
				});
				$scope.batalpindahTask();
				$scope.batalLihatDetailTask();
				$scope.HighlightTheIncomplete();
        }
        
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mViewSalesman = null; //Model
    $scope.xRole={selected:[]};
    //----------------------------------
    // Get Data
    //----------------------------------

    ViewSalesmanFactory.getStatusTask().then(function (res) {
				$scope.OptionsStatusTask = res.data.Result;
				$scope.HighlightTheIncomplete();
				return $scope.OptionsStatusTask;
			});

    var gridData = [];
     //$scope.getSalesman=ViewSalesmanFactory.getData().Result;
	 // ViewSalesmanFactory.getData("").then(function(res){
            // $scope.getSalesman = res.data.Result;
            // return $scope.getSalesman;
        // });
		
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        //console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        //console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
	

	
	$scope.lookupProspect = function () {
			
			ViewSalesmanFactory.getDataProspect($scope.selected_data.SalesId).then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				setTimeout(function() {     
					//angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');
					angular.element('.ui.modal.ModalProspect').modal('refresh'); 
				}, 0);
				
				setTimeout(function() {
					angular.element('.ui.modal.ModalProspect').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalProspect').not(':first').remove();
					$scope.ProspectSearchValue="";
				}, 1);
				
			});
			$scope.ProspectSearchCriteria=$scope.optionProspectSearchCriteria[0].SearchOptionId;
			
        }
		
        $scope.BatalLookupProspect = function () {
			angular.element('.ui.modal.ModalProspect').modal('hide');
        }
		
		$scope.SearchProspect = function () {
			if($scope.ProspectSearchCriteria==1 && $scope.ProspectSearchValue!="")
			{
				ViewSalesmanFactory.GetSearchProspectByName($scope.ProspectSearchValue,$scope.selected_data.SalesId).then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				
				});
			}
			else if($scope.ProspectSearchCriteria==2 && $scope.ProspectSearchValue!="")
			{
				ViewSalesmanFactory.GetSearchProspectByProspectCode($scope.ProspectSearchValue,$scope.selected_data.SalesId).then(function (res) {
				$scope.gridNoProspect = res.data.Result;
				
				});
			}
			else
			{
				ViewSalesmanFactory.getDataProspect($scope.selected_data.SalesId).then(function (res) {
					$scope.gridNoProspect = res.data.Result;
					
				});
			}
			
			

        }
	
	$scope.pilihProspect = function (ProspectNo) {
            $scope.selected_data.ProspectId=angular.copy(ProspectNo.ProspectId);
			$scope.selected_data.ProspectCode=angular.copy(ProspectNo.ProspectCode);
			$scope.selected_data.ProspectName=angular.copy(ProspectNo.ProspectName);
            $scope.NoHP = ProspectNo.HP;
            $scope.Alamat = ProspectNo.Address;
            angular.element('.ui.modal.ModalProspect').modal('hide');
        }
	
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
		enableColumnResizing: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'AlasanPengecualianPengirimanId', width:'7%', visible:false },
            { name:'name', field:'NamaAlasanPengecualianPengiriman' },
            { name:'InputByAdmin',  field: 'InputByAdmin' },
			{ name:'InputBySalesman',  field: 'InputBySalesman' },
        ]
    };
	
	
});
