angular.module('app')
  .factory('ProvinsiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		var optionsProvinsi_Json = [{ name: "nanti isi", value: "nanti isi" }, { name: "Jawa Barat", value: "Jawa Barat" }, { name: "Jawa Timur", value: "Jawa Timur" }];
		res=optionsProvinsi_Json;	  
        return res;
      },
      create: function(provinsi) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      update: function(provinsi){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: provinsi.SalesProgramId,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd