angular.module('app')
  .factory('ApprovalDropSalesSupervisorFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/sales/CProspectListApproval');
		var res=$http.get('/api/sales/CProspectListApproval?start=1&limit=10');
        //console.log('res=>',res);
        return res;
      },
	  getDataIncrement: function(Start,Limit) {
		//var res=$http.get('/api/sales/CProspectListApproval');
		var res=$http.get('/api/sales/CProspectListApproval?start='+Start+'&limit='+Limit);
		return res;
        
      },
	  
	  getDataSearchApprovalDropSalesSupervisorIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/CProspectListApproval/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  getDataIncrementLimit: function () {
        
        var da_json = [
          //{ SearchOptionId: 2, SearchOptionName: "2" },
		  //{ SearchOptionId: 5, SearchOptionName: "5" },
		  { SearchOptionId: 10, SearchOptionName: "10" }
        ];

        var res=da_json

        return res;
      },
	  
	  GetApprovalDropSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "ProspectName", SearchOptionName: "Nama Prospek" },
		  { SearchOptionId: "InstanceName", SearchOptionName: "Nama Instansi" },
          { SearchOptionId: "StatusApprovalName", SearchOptionName: "Status" }
        ];

        var res=da_json

        return res;
      },
	  
	  getDataSearchApprovalDropSalesSupervisor: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/CProspectListApproval/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
      create: function(ApprovalDropSalesSupervisor) {
        return $http.post('/api/sales/CProspectListApproval', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDropSalesSupervisor.SalesProgramName}]);
      },
      SetujuApprovalDropSalesSupervisor: function(ApprovalDropSalesSupervisor){
        // return $http.put('/api/sales/CSuspect/Approver', [{
                                            // OutletId: ApprovalDropSalesSupervisor.OutletId,
											// ProspectId: ApprovalDropSalesSupervisor.ProspectId,
											// SuspectId: ApprovalDropSalesSupervisor.SuspectId,
											// CustomerId: ApprovalDropSalesSupervisor.CustomerId,
                                            // ApprovalProspectId: ApprovalDropSalesSupervisor.ApprovalProspectId}]);
		return $http.put('/api/sales/CSuspect/Approver', [ApprovalDropSalesSupervisor]);									
      },
	  TidakSetujuApprovalDropSalesSupervisor: function(ApprovalDropSalesSupervisor){
        return $http.put('/api/sales/CSuspect/Reject', [{
                                            OutletId: ApprovalDropSalesSupervisor.OutletId,
											ProspectId: ApprovalDropSalesSupervisor.ProspectId,
											SuspectId: ApprovalDropSalesSupervisor.SuspectId,
											CustomerId: ApprovalDropSalesSupervisor.CustomerId,
											RejectReason: ApprovalDropSalesSupervisor.RejectReason,
                                            ApprovalProspectId: ApprovalDropSalesSupervisor.ApprovalProspectId}]);
		//return $http.put('/api/sales/CSuspect/Reject', [ApprovalDropSalesSupervisor]);									
      },
      delete: function(id) {
        return $http.delete('/api/sales/CProspectListApproval',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd