angular.module('app')
  .factory('InformasiUnitYangDiminatiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		var ProspectDrop_InformasiUnitYangDiminati_Json=[
				{Id: "1",Model: "Avanza",Tipe: "2.5 S A/T",Warna: "Black Mica",Qty: "1"},
				{Id: "2",Model: "Avanza",Tipe: "2.5 S A/T",Warna: "Black Mica",Qty: "2"},
				{Id: "3",Model: "Avanza",Tipe: "2.5 S A/T",Warna: "Black Mica",Qty: "3"},
			  ];
		var res=ProspectDrop_InformasiUnitYangDiminati_Json;	  
        return res;
      },
      create: function(informasiUnitYangDiminati) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: informasiUnitYangDiminati.SalesProgramName}]);
      },
      update: function(informasiUnitYangDiminati){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: informasiUnitYangDiminati.SalesProgramId,
                                            SalesProgramName: informasiUnitYangDiminati.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd