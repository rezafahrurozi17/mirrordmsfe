angular.module('app')
  .factory('KabupatenKotaFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		var optionsKabupatenKota_Json = [{ name: "Jakarta", value: "Jakarta" }, { name: "Bandung", value: "Bandung" }, { name: "Surabaya", value: "Surabaya" }];
		res=optionsKabupatenKota_Json;	  
        return res;
      },
      create: function(kabupatenKota) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: kabupatenKota.SalesProgramName}]);
      },
      update: function(kabupatenKota){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: kabupatenKota.SalesProgramId,
                                            SalesProgramName: kabupatenKota.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd