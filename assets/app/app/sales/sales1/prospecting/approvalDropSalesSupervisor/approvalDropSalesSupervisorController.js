angular.module('app')
.controller('ApprovalDropSalesSupervisorController', function($scope, $http, CurrentUser, ApprovalDropSalesSupervisorFactory,ProspectInformasiUnitYangDiminatiFactory,KategoriPelangganFactory,$timeout,bsNotify) {
	$scope.ApprovalDropSalesSupervisorMain=true;
	IfGotProblemWithModal();
	$scope.Show_Hide_DropProspect_Informasi_Pelanggan_PanelIcon = "+";
	$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "+";
	$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati_PanelIcon = "+";
	$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain_PanelIcon = "+";
	$scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "+";
	$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
	
	// $scope.$on('$viewContentLoaded', function () {
            // //$scope.loading=true; tadinya true
            // $scope.loading = false;
            // $scope.gridData = [];
        // });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.optionApprovalDropSalesSupervisorSearchCriteria = ApprovalDropSalesSupervisorFactory.GetApprovalDropSearchDropdownOptions();
        $scope.optionApprovalDropSalesSupervisorSearchLimit = ApprovalDropSalesSupervisorFactory.getDataIncrementLimit();
		$scope.ApprovalDropSalesSupervisorSearchLimit = $scope.optionApprovalDropSalesSupervisorSearchLimit[0].SearchOptionId; 
		$scope.ApprovalDropSalesSupervisorSearchCriteria = $scope.optionApprovalDropSalesSupervisorSearchCriteria[0].SearchOptionId;
		$scope.mProspect_Prospect = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        // $scope.getData = function() {
		// ApprovalDropSalesSupervisorFactory.getData().then(
				// function(res){
					// $scope.grid.data = res.data.Result;
					// $scope.loading=false;
					// console.log("data dari Factory",res.data.Result);
					// return res.data;
				// },
				// function(err){
					// console.log("err=>",err);
				// }
			// );
		// }
		
		ApprovalDropSalesSupervisorFactory.getData().then(
			function(res){
				$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
				$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
				$scope.totalLoadMore = res.data.Total;
				console.log("approval data: "+res.data);
				return res.data;

			},
			function(err){
				console.log("err=>",err);
			}
		);
		
		$scope.ApprovalDropSalesSupervisorRefresh = function () {
			ApprovalDropSalesSupervisorFactory.getData().then(
				function(res){
					
					$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
					$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
					$scope.totalLoadMore = res.data.Total;
					$scope.ApprovalDropSalesSupervisorSearchValue="";
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
		$scope.ApprovalDropSalesSupervisorIncrementLimit = function () {
			//$scope.ApprovalDropSalesSupervisorSearchTier++;
			//$scope.ApprovalDropSalesSupervisorIncrementStart=($scope.ApprovalDropSalesSupervisorSearchTier*$scope.ApprovalDropSalesSupervisorSearchLimit)+1;
			$scope.loadingMore = true;

			if($scope.ApprovalDropSalesSupervisorSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalDropSalesSupervisorGabungan.length)+1;
				ApprovalDropSalesSupervisorFactory.getDataIncrement(masukin,10).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDropSalesSupervisorGabungan.push(res.data.Result[i]);
						}
						$scope.loadingMore = false;
						return res.data;
						
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			if($scope.ApprovalDropSalesSupervisorSearchIsPressed==true)
			{
				//console.log("ulala",$scope.ApprovalDropSalesSupervisorGabungan.length)
				var masukin=angular.copy($scope.ApprovalDropSalesSupervisorGabungan.length)+1;
				ApprovalDropSalesSupervisorFactory.getDataSearchApprovalDropSalesSupervisorIncrement(masukin,10,$scope.ApprovalDropSalesSupervisorSearchCriteriaStore,$scope.ApprovalDropSalesSupervisorSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDropSalesSupervisorGabungan.push(res.data.Result[i]);
						}
						$scope.loadingMore = false;
						return res.data;
						
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			
			
			

		};
		
        // function roleFlattenAndSetLevel(node, lvl) {
            // for (var i = 0; i < node.length; i++) {
                // node[i].$$treeLevel = lvl;
                // gridData.push(node[i]);
                // if (node[i].child.length > 0) {
                    // roleFlattenAndSetLevel(node[i].child, lvl + 1)
                // } else {

                // }
            // }
            // return gridData;
        // }
        // $scope.selectRole = function (rows) {
            // console.log("onSelectRows=>", rows);
            // $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        // }
        // $scope.onSelectRows = function (rows) {
            // console.log("onSelectRows=>", rows);
        // }
	
	KategoriPelangganFactory.getData().then(function (res) {
			$scope.optionsKategoriPelanggan = res.data.Result;
			return $scope.optionsKategoriPelanggan;
		});
	
		
	$scope.lanjutProspect = {};		
	$scope.SelectDropProspect = function (SelectedData) {
			$scope.ApprovalDropSalesSupervisorMain=false;
			$scope.ShowDropProspectDetail=true;

			$scope.lanjutProspect.CustomerTypeId = SelectedData.CustomerTypeId;
			
			console.log('test2',SelectedData);
			
			ProspectInformasiUnitYangDiminatiFactory.getData(SelectedData.ProspectId).then(function (res) {
				$scope.ContentApprovalDropSalesSupervisorGabungan_InformasiUnitYangDiminati_To_Repeat = res.data.Result;
				return $scope.ContentApprovalDropSalesSupervisorGabungan_InformasiUnitYangDiminati_To_Repeat;
			});

			//$scope.List_Prospect_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;
			$scope.ContentApprovalDropSalesSupervisorGabungan_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;

			$scope.Prospect_InformasiUnitYangDiminati_Action_ShowHide = false;
			$scope.Prospect_InformasiKendaraanLain_Action_ShowHide = false;

			$scope.mProspect_Prospect = angular.copy(SelectedData);
			
			var tempProv = [{
				ProvinceName : SelectedData.ProvinceName,
				ProvinceId : SelectedData.ProvinceId
			}];
			$scope.province = tempProv;
			$scope.selectedProvince={};
			$scope.selectedProvince.Province={};
			for (var i = 0; i < $scope.province.length; ++i) {
				if ($scope.province[i].ProvinceId == SelectedData.ProvinceId) {
					$scope.selectedProvince.Province = $scope.province[i];
					break;
				}
			}

			var tempKab = [{
				CityRegencyName : SelectedData.CityRegencyName,
				CityRegencyId : SelectedData.CityRegencyId
			}];
			$scope.kabupaten = tempKab;
			$scope.selectedCityRegency={};
			$scope.selectedCityRegency.CityRegency={};
			for (var i = 0; i < $scope.kabupaten.length; ++i) {
				if ($scope.kabupaten[i].CityRegencyId == SelectedData.CityRegencyId) {
					$scope.selectedCityRegency.CityRegency = $scope.kabupaten[i];
					break;
				}
			}

			var tempKec = [{
				DistrictName : SelectedData.DistrictName,
				DistrictId : SelectedData.DistrictId
			}];
			$scope.kecamatan = tempKec;
			$scope.selectedDistrict={};
			$scope.selectedDistrict.District={};
			for (var i = 0; i < $scope.kecamatan.length; ++i) {
				if ($scope.kecamatan[i].DistrictId == SelectedData.DistrictId) {
					$scope.selectedDistrict.District = $scope.kecamatan[i];
					break;
				}
			}

			var tempKel = [{
				VillageName : SelectedData.VillageName,
				VillageId : SelectedData.VillageId
			}];
			$scope.kelurahan = tempKel;
			$scope.selectedVillage={};
			$scope.selectedVillage.Village={};
			for (var i = 0; i < $scope.kelurahan.length; ++i) {
				if ($scope.kelurahan[i].VillageId == SelectedData.VillageId) {
					$scope.selectedVillage.Village = $scope.kelurahan[i];
					break;
				}
			}

			
			var tempAgama = [{
				CustomerReligionName : SelectedData.CustomerReligionName,
				CustomerReligionId : SelectedData.CustomerReligionId
			}];
			$scope.optionsAgama = tempAgama;
			$scope.selectAgama={};
			$scope.selectAgama.Agama={};
			for (var i = 0; i < $scope.optionsAgama.length; ++i) {
				if ($scope.optionsAgama[i].CustomerReligionId == SelectedData.CustomerReligionId) {
					$scope.selectAgama.Agama = $scope.optionsAgama[i];
					break;
				}
			}
			
			
			var tempSuku = [{
				CustomerEthnicName : SelectedData.CustomerEthnicName,
				CustomerEthnicId : SelectedData.CustomerEthnicId
			}];
			$scope.optionsSuku = tempSuku;
			$scope.selectedSuku={};
			$scope.selectedSuku.Suku={};
			for (var i = 0; i < $scope.optionsSuku.length; ++i) {
				if ($scope.optionsSuku[i].CustomerEthnicId == SelectedData.CustomerEthnicId) {
					$scope.selectedSuku.Suku = $scope.optionsSuku[i];
					break;
				}
			}

			var tempJabatan = [{
				CustomerPositionName : SelectedData.CustomerPositionName,
				CustomerPositionId : SelectedData.CustomerPositionId
			}];
			$scope.optionsJabatan = tempJabatan;
			$scope.selectedJabatan={};
			$scope.selectedJabatan.Jabatan={};
			for (var i = 0; i < $scope.optionsJabatan.length; ++i) {
				if ($scope.optionsJabatan[i].CustomerPositionId == SelectedData.CustomerPositionId) {
					$scope.selectedJabatan.Jabatan = $scope.optionsJabatan[i];
					break;
				}
			}
			
			var tempMetodePembayaran = [{
				FundSourceName : SelectedData.FundSourceName,
				FundSourceId : SelectedData.FundSourceId
			}];
			$scope.optionsMetodePembayaran = tempMetodePembayaran;
			$scope.selectedFundSource={};
			$scope.selectedFundSource.FundSource={};
			for (var i = 0; i < $scope.optionsMetodePembayaran.length; ++i) {
				if ($scope.optionsMetodePembayaran[i].FundSourceId == SelectedData.FundSourceId) {
					$scope.selectedFundSource.FundSource = $scope.optionsMetodePembayaran[i];
					break;
				}
			}

			var tempKebutuhan = [{
				PurchaseTimeName : SelectedData.PurchaseTimeName,
				PurchaseTimeId : SelectedData.PurchaseTimeId
			}];
			$scope.optionsTingkatKebutuhan = tempKebutuhan;
			$scope.selectedKebutuhan={};
			$scope.selectedKebutuhan.Kebutuhan={};
			for (var i = 0; i < $scope.optionsTingkatKebutuhan.length; ++i) {
				if ($scope.optionsTingkatKebutuhan[i].PurchaseTimeId == SelectedData.PurchaseTimeId) {
					$scope.selectedKebutuhan.Kebutuhan = $scope.optionsTingkatKebutuhan[i];
					break;
				}
			}

			
			var tempCategoryProspect = [{
				CategoryProspectName : SelectedData.CategoryProspectName,
				CategoryProspectId : SelectedData.CategoryProspectId
			}];
			$scope.optionsKategoriProspect = tempCategoryProspect;
			$scope.selectedKategori={};
			$scope.selectedKategori.Kategori={};
			for (var i = 0; i < $scope.optionsKategoriProspect.length; ++i) {
				if ($scope.optionsKategoriProspect[i].CategoryProspectId == SelectedData.CategoryProspectId) {
					$scope.selectedKategori.Kategori = $scope.optionsKategoriProspect[i];
					break;
				}
			}

			
			var tempPurchasePurpose = [{
				PurchasePurposeName : SelectedData.PurchasePurposeName,
				PurchasePurposeId : SelectedData.PurchasePurposeId
			}];
			$scope.optionsPurchasePurpose = tempPurchasePurpose;
			$scope.selectedPurchasePurpose={};
			$scope.selectedPurchasePurpose.PurchasePurpose={};
			for (var i = 0; i < $scope.optionsPurchasePurpose.length; ++i) {
				if ($scope.optionsPurchasePurpose[i].PurchasePurposeId == SelectedData.PurchasePurposeId) {
					$scope.selectedPurchasePurpose.PurchasePurpose = $scope.optionsPurchasePurpose[i];
					break;
				}
			}


			$scope.mProspect_Prospect.StatusPelanggan = "Prospect";

			$scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon = "+";
			$scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = true;



			var ProspectKategori_SelectedIndex = 0;
			for (var i = 0; i < $scope.optionsKategoriPelanggan.length; ++i) {
				if ($scope.optionsKategoriPelanggan[i].CustomerTypeId == SelectedData.CustomerTypeId) {
					ProspectKategori_SelectedIndex = i;
				}
			}
			//if($scope.optionsKategoriPelanggan[ProspectKategori_SelectedIndex].CustomerCategoryName=="Individu")
			KategoriPelangganFactory.getData().then(function (res) {
				$scope.optionsKategoriPelanggan = res.data.Result;
				return $scope.optionsKategoriPelanggan;
			});

			$scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
			
			if ($scope.optionsKategoriPelanggan[ProspectKategori_SelectedIndex].CustomerTypeDesc == "Individu") {
				$scope.ProspectIndividu = true;
				$scope.ProspectInstansi = false;

				for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
					if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc != "Individu") {
						$scope.optionsKategoriPelangganFiltered.splice(i, 1);
					}
				}
			}
			else {
				$scope.ProspectIndividu = false;
				$scope.ProspectInstansi = true;
				//$scope.ContentApprovalDropSalesSupervisorGabungan_InformasiKendaraanLain_To_Repeat = "";

				for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
					if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc == "Individu") {
						$scope.optionsKategoriPelangganFiltered.splice(i, 1);
					}
				}
			}

			if (SelectedData.CategoryProspectId == 4) {//nanti benerin conditionnya kalo bisa
				//nanti balikin
				$scope.Show_mProspect_Prospect_KategoriProspect = true;
				$scope.Show_mProspect_Prospect_KategoriAwal = true;
				$scope.Show_mProspect_Prospect_AlasanPenurunan = true;
				$scope.ProspectEditButtonToggle = false;
				//disini dropdown harus mati
			}
			if (SelectedData.BeforeCategoryProspectId == null || SelectedData.BeforeCategoryProspectId == 0) {	//disini dropdown harus nyala

				//nanti balikin
				$scope.Show_mProspect_Prospect_KategoriProspect = true;
				$scope.Show_mProspect_Prospect_KategoriAwal = false;
				$scope.Show_mProspect_Prospect_AlasanPenurunan = false;
			}
			else if (SelectedData.BeforeCategoryProspectId != null || SelectedData.BeforeCategoryProspectId != 0) {//disini dropdown harus nyala setan


				$scope.Show_mProspect_Prospect_KategoriProspect = true;
				$scope.Show_mProspect_Prospect_KategoriAwal = true;



				if (SelectedData.CategoryProspectId > SelectedData.BeforeCategoryProspectId) {
					$scope.Show_mProspect_Prospect_AlasanPenurunan = true;
				}
				else {
					$scope.Show_mProspect_Prospect_AlasanPenurunan = false;
				}
			}

			

			$scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = true;
			$scope.ContentApprovalDropSalesSupervisorGabungan_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;
			console.log('other', $scope.ContentApprovalDropSalesSupervisorGabungan_InformasiKendaraanLain_To_Repeat);

		};

	$scope.BatalApprovalDropSalesSupervisor = function () {
		$scope.mProspect_Prospect ={};
		$scope.ApprovalDropSalesSupervisorMain=true;
		$scope.ShowDropProspectDetail=false;
	}
	
	ApprovalDropSalesSupervisorFactory.getData().then(function (res) {
			$scope.ProspectDropData = res.data.Result;
		});
	
	$scope.ProspectToDropSelected = function (SelectedProspectToDrop) {
		$scope.ApprovalDropSalesSupervisorMain=false;
		$scope.ShowDropProspectDetail=true;
		$scope.SelectProspect(SelectedProspectToDrop);
	}
	
	$scope.Show_Hide_DropProspect_Informasi_Pelanggan_Clicked = function () {
			$scope.Show_Hide_DropProspect_Informasi_Pelanggan = !$scope.Show_Hide_DropProspect_Informasi_Pelanggan;
			if ($scope.Show_Hide_DropProspect_Informasi_Pelanggan == true) {
				$scope.Show_Hide_DropProspect_Informasi_Pelanggan_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_DropProspect_Informasi_Pelanggan == false) {
				$scope.Show_Hide_DropProspect_Informasi_Pelanggan_PanelIcon = "+";
			}

		};
	
	$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati_Clicked = function () {
			$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati = !$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati;
			if ($scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati == true) {
				$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati == false) {
				$scope.Show_Hide_DropProspect_Informasi_Unit_Yang_Diminati_PanelIcon = "+";
			}

		};
		
	$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit_Clicked = function () {
			$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit = !$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit;
			if ($scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit == true) {
				$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit == false) {
				$scope.Show_Hide_DropProspect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "+";
			}

		};

	$scope.Show_Hide_Prospect_Informasi_Lain_Clicked = function () {
			$scope.Show_Hide_Prospect_Informasi_Lain = !$scope.Show_Hide_Prospect_Informasi_Lain;
			if ($scope.Show_Hide_Prospect_Informasi_Lain == true) {
				$scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_Prospect_Informasi_Lain == false) {
				$scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "+";
			}

		};
	
	$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain_Clicked = function () {
			$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain = !$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain;
			if ($scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain == true) {
				$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain == false) {
				$scope.Show_Hide_DropProspect_Informasi_Kendaraan_Lain_PanelIcon = "+";
			}

		};
		
	$scope.SetujuApprovalDropSalesSupervisor = function () {
			ApprovalDropSalesSupervisorFactory.SetujuApprovalDropSalesSupervisor($scope.mProspect_Prospect).then(function () {
					ApprovalDropSalesSupervisorFactory.getData().then(
						function(res){
							$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
							$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
							$scope.totalLoadMore = res.data.Total;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							// $scope.loading=false;
							// console.log("data dari Factory",res.data.Result);
							$scope.BatalApprovalDropSalesSupervisor();
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				},function(err){
                console.log("setan",err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Data gagal disimpan",
						type: 'danger'
					}
				);
            });

		};

	$scope.TidakSetujuApprovalDropSalesSupervisor = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalDropSalesSupervisor').modal('setting',{closable:false}).modal('show');
		};
	
	$scope.BatalAlasanApprovalDrop = function () {
		angular.element('.ui.modal.ModalAlasanPenolakanApprovalDropSalesSupervisor').modal('hide');
		//$scope.mProspect_Prospect.RejectReason={};
	}
	
	$scope.SubmitAlasanApprovalDrop = function () {
		ApprovalDropSalesSupervisorFactory.TidakSetujuApprovalDropSalesSupervisor($scope.mProspect_Prospect).then(function () {
					ApprovalDropSalesSupervisorFactory.getData().then(
						function(res){
							$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
							$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
							$scope.totalLoadMore = res.data.Total;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							// $scope.loading=false;
							// console.log("data dari Factory",res.data.Result);
							$scope.BatalApprovalDropSalesSupervisor();
							$scope.BatalAlasanApprovalDrop();
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				},function(err){
                console.log("setan",err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Data gagal disimpan",
						type: 'danger'
					}
				);
            });
	}
	
	$scope.SearchApprovalDropSalesSupervisor = function () {
		
			try
			{
				if($scope.ApprovalDropSalesSupervisorSearchValue!="" && (typeof $scope.ApprovalDropSalesSupervisorSearchValue!="undefined"))
				{
					ApprovalDropSalesSupervisorFactory.getDataSearchApprovalDropSalesSupervisor(10,$scope.ApprovalDropSalesSupervisorSearchCriteria,$scope.ApprovalDropSalesSupervisorSearchValue).then(function (res) {
						
						$scope.ApprovalDropSalesSupervisorSearchIsPressed=true;
						$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
						$scope.totalLoadMore = res.data.Total;
						$scope.ApprovalDropSalesSupervisorSearchCriteriaStore=angular.copy($scope.ApprovalDropSalesSupervisorSearchCriteria);
						$scope.ApprovalDropSalesSupervisorSearchValueStore=angular.copy($scope.ApprovalDropSalesSupervisorSearchValue);
					});
				}
				else
				{
					ApprovalDropSalesSupervisorFactory.getData().then(function (res) {
						
						$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
						$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
						$scope.totalLoadMore = res.data.Total;
					});
				}
			}
			catch(e)
			{
				ApprovalDropSalesSupervisorFactory.getData().then(function (res) {
					
					$scope.ApprovalDropSalesSupervisorSearchIsPressed=false;
					$scope.ApprovalDropSalesSupervisorGabungan = res.data.Result;
					$scope.totalLoadMore = res.data.Total;
				});
			}
			

        }
		
	// $scope.grid = {
            // enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            // //showTreeExpandNoChildren: true,
            // // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // // paginationPageSize: 15,
            // columnDefs: [
                // { name: 'Prospect Code', field: 'ProspectCode', width: '7%', visible: true },
                // { name: 'Nama Prospect/Instansi', field: 'ProspectName', cellTemplate:'<label>{{row.entity.ProspectName}}</label><label>{{row.entity.InstanceName}}</label>'  },
				// { name: 'Nama Sales', field: 'EmployeeName' },
				// { name: 'Category Baru', field: 'CategoryProspectName' },
				// { name: 'Alasan', field: 'ReasonDropProspectName' },
				// { name: 'Lihat', cellTemplate:'<a ng-click="grid.appScope.$parent.SelectDropProspect(row.entity)" >Lihat Detil</a>' },
                
            // ]
        // };	
	
	
	
	

});



 
// angular.module('app').factory('SampleUITemplateFactory', function ($http) {
      // return {
          // getData: function () {
              // //var res = $http.get('file:///C:/Git/DMS-Sales-Prototype-Mike/DMS.Website/assets/app/app/sales/master/profileSalesProgram/the_thing.json');
				// var res = null;
			  // //uda bisa sampe sini ternyata
              // return res;
          // }
      // }
  // });
