angular.module('app')
  .factory('ProvinsiFactory', function($http, CurrentUser) {
    if(_myCache==undefined)
      var _myCache = [];
    //var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res = null;
        if(_myCache.length==0){
        var user = CurrentUser.user();
        var res=$http.get('/api/sales/MLocationProvince');
        res.then(function(res){
            
            _myCache = res.data.Result;
          });
        }else{
          res = {
            then:function(cbOk,cbErr){
              cbOk({data:{Result:angular.copy(_myCache)}});
            }
          }
        }
        //console.log('res=>',res);
		//var optionsProvinsi_Json = [{ name: "nanti isi", value: "nanti isi" }, { name: "Jawa Barat", value: "Jawa Barat" }, { name: "Jawa Timur", value: "Jawa Timur" }];
		//var res=optionsProvinsi_Json;	  
        return res;
      },
      create: function(provinsi) {
        return $http.post('/api/sales/MLocationProvince', [{
                                            //AppId: 1,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      update: function(provinsi){
        return $http.put('/api/sales/MLocationProvince', [{
                                            SalesProgramId: provinsi.SalesProgramId,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MLocationProvince',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd