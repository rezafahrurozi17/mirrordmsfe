angular.module('app')
  .factory('SukuFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerSuku');
        //console.log('res=>',res);
		var optionsSuku_Json = [{ name: "Jawa", value: "Jawa" }, { name: "Batak", value: "Batak" }, { name: "Tiong Hua", value: "Tiong Hua" }, { name: "Lainnya", value: "Lainnya" }];
		//var res=optionsSuku_Json;	  
        return res;
      },
      create: function(suku) {
        return $http.post('/api/sales/CCustomerSuku', [{
                                            //AppId: 1,
                                            SalesProgramName: suku.SalesProgramName}]);
      },
      update: function(suku){
        return $http.put('/api/sales/CCustomerSuku', [{
                                            SalesProgramId: suku.SalesProgramId,
                                            SalesProgramName: suku.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerSuku',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd