angular.module('app')
  .factory('PelangganFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        // var res = $http.get('/api/sales/CCustomerList' + param);
        var res = $http.get('/api/sales/CCustomerList/Search' + param);
        //console.log('res=>',res);
		var Pelanggan_Json=[
				{Id: "1",Toyota_Id: "t1",CustomerCategoryId: 1,Kategori_Fleet: "Non-Fleet",  Tanggal_Input_Pelanggan: new Date("1/12/2016"),Nama_Lengkap: "nama1",Nama_Panggilan: "n1",  No_Telepon_Rumah: "123",No_Handphone: "12345678",  Email: "a@b.com",  Alamat_Sesuai_Identitas: "alamat 1",Provinsi: "Jawa Barat",Kabupaten_Kota: "Bandung",Kecamatan: "Tanjung Priok",Kelurahan: "Sunter",Kode_Pos: "12312",Tanggal_Lahir: new Date("1/13/2017"),Hobi: "molor",Agama: "Hindu",Suku: "Batak",Jabatan: "jabatan2",Nama_Perusahaan: "perusahaan",No_Telepon_Kantor: "123",Alamat_Perusahaan: "Alamat_Perusahaan",RTRW:"1",Nama_Institusi: ""},
				{Id: "2",Toyota_Id: "t2",CustomerCategoryId: 1,Kategori_Fleet: "Non-Fleet",  Tanggal_Input_Pelanggan: new Date("1/12/2016"),Nama_Lengkap: "nama2",Nama_Panggilan: "n2",  No_Telepon_Rumah: "123",No_Handphone: "12345678",  Email: "a@b.com",  Alamat_Sesuai_Identitas: "alamat 2",Provinsi: "Jawa Barat",Kabupaten_Kota: "Bandung",Kecamatan: "Tanjung Priok",Kelurahan: "Sunter",Kode_Pos: "3452354",Tanggal_Lahir: new Date("1/13/2017"),Hobi: "molor",Agama: "Hindu",Suku: "Batak",Jabatan: "jabatan2",Nama_Perusahaan: "perusahaan",No_Telepon_Kantor: "123",Alamat_Perusahaan: "Alamat_Perusahaan",RTRW:"1",Nama_Institusi: ""},
				{Id: "3",Toyota_Id: "t3",CustomerCategoryId: 2,Kategori_Fleet: "Non-Fleet",  Tanggal_Input_Pelanggan: new Date("1/12/2016"),Nama_Lengkap: "nama3",Nama_Panggilan: "",  No_Telepon_Rumah: "123",No_Handphone: "12345678",  Email: "a@b.com",  Alamat_Sesuai_Identitas: "alamat 3",Provinsi: "Jawa Barat",Kabupaten_Kota: "Bandung",Kecamatan: "Tanjung Priok",Kelurahan: "Sunter",Kode_Pos: "24354",Tanggal_Lahir: new Date("1/13/2017"),Hobi: "",Agama: "",Suku: "",Jabatan: "",Nama_Perusahaan: "",No_Telepon_Kantor: "",Alamat_Perusahaan: "",RTRW:"1",Nama_Institusi: "aneh"},
			  ];
		//var res=Pelanggan_Json;	  
        return res;
      },

      cekCustomerToProspect: function(CustomerId) {
        var res=$http.get('/api/sales/CekProspectFromCustomer/?CustomerId='+CustomerId);
        return res;
      },


      create: function(Prospect) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: Prospect.SalesProgramName}]);
      },
      update: function(Prospect){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: Prospect.SalesProgramId,
                                            SalesProgramName: Prospect.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd