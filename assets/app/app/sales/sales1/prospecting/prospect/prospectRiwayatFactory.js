angular.module('app')
  .factory('ProspectRiwayatFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getDataByProspectId: function(ProspectId) {
        var res=$http.get('/api/sales/SProspectingFollowUpHistory/?start=1&limit=100&filterData=ProspectId|'+ProspectId);	  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/SProspectingFollowUpHistory', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/SProspectingFollowUpHistory', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SProspectingFollowUpHistory',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd