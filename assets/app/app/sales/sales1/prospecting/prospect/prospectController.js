angular.module('app')
    .controller('ProspectController', function ($rootScope, $scope, $http, CurrentUser, SimulasiKreditFactory,
        ProspectFactory,
        PelangganFactory,
        SuspectFactory,
        WarnaFactory,
        ProvinsiFactory,
        KabupatenKotaFactory,
        SukuFactory,
        ProspectInformasiUnitYangDiminatiFactory,
        ProspectInformasiKendaraanLainFactory,
        PelangganInformasiKendaraanLainFactory,
        CategoryProspectSourceFactory,
        JabatanFactory,
        MerekMobilFactory,
        KategoriFleetFactory,
        KecamatanFactory,
        KategoriPelangganFactory,
        ProspectRiwayatFactory,
        TingkatKebutuhanFactory,
        AgamaFactory,
        AlasanDropProspectFactory,
        TujuanPembelianFactory,
        KelurahanFactory,
        MetodePembayaranFactory,
        KategoriProspectFactory,
        ProspectPengecekanDataFactory,
        SalesForceFactory,
        TipeFactory,
        ProfileLeasing,
        LeasingTenorFactory,
        SimulasiAsuransiFactory,
        ProfileInsuranceFactory,
        ProfileLeasingSimulationFactory,
        CreateSpkFactory, ComboBoxFactory,
        BookingUnitFactory, StockInfoAvaliabilityFactory, AksesorisFactory, BookingUnitProspectFactory, FinanceFundSourceFactory, bsNotify,
        NegotiationFactory, RiwayatFollowUpFactory, ModelFactory, StatusFollowUpTestDriveFactory, NegotiationDetailFactory, $timeout, LocalService, $window) {


        //////////////////////////////////////////////////////////////////////////
        //// FIX UT
        /////////////////////////////////////////////////////////////////////////
        $scope.tahunkendaraanlain = function (value){
            if (value == 0){
                $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.VehicleYear = null;
            }
        }
		
		$scope.ShowProspectingSearch=false;
		
		$scope.ToggleProspectingSearch = function () {

            if ($scope.ShowProspectingSearch == false){
                $scope.ShowProspectingSearch = true ;
            }else{
                $scope.ShowProspectingSearch = false;
            }

        }

        $scope.qtyUnit = function (value){
            if (value == 0){
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = null;
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        /// Param Global
        //////////////////////////////////////////////////////////////////////////////
        $scope.online = {};
        $scope.$on('$viewContentLoaded', function () {
            $scope.online = navigator.onLine;
            $window.addEventListener("offline", function () {
                $scope.$apply(function () {
                    $scope.online = false;
                    
                    $scope.status = true;
                    setTimeout(function () {
                        $scope.activeForm = 2;
                    }, 0);
                    // $scope.MaintainTab = {suspect: false, prospectonline: false, prospectoffline: true, customer: false}
                    // angular.element("#tabsuspect").removeClass("uib-tab nav-item ng-isolate-scope active").addClass("uib-tab nav-item ng-scope ng-isolate-scope");
                    // angular.element("#tabcustomer").removeClass("uib-tab nav-item ng-isolate-scope active").addClass("uib-tab nav-item ng-scope ng-isolate-scope");
                    // angular.element("#tabprospectoffline").removeClass("uib-tab nav-item ng-scope ng-isolate-scope").addClass("uib-tab nav-item ng-isolate-scope active");
                    // angular.element("#tabprospectonline").removeClass("uib-tab nav-item ng-isolate-scope active").addClass("uib-tab nav-item ng-scope ng-isolate-scope");
                });
            }, false);

            $window.addEventListener("online", function () {
                $scope.$apply(function () {
                    $scope.online = true;
                    setTimeout(function () {
                        $scope.activeForm = 0;
                    }, 0);
                });
            }, false);


            ProspectFactory.openIndexedDB().then(function (res) {
                //vm.refreshList();
            });
        });

        $scope.setActiveTab = function(index){
            $scope.activeTab = index;
          }
        
        $scope.user = CurrentUser.user();
        $scope.ShowNonMandatory = false;
        $scope.NonMandatoryDown = true;
        $scope.NonMandatoryUp = false;
        $scope.listUnitYangDiminati = undefined;

        $scope.NonMandatoryPelanggan = false;
        $scope.NonMandatoryDownPelanggan = true;
        $scope.NonMandatoryUpPelanggan = false;
        $scope.ProspectEditButtonToggle = false;
        $scope.TabInfoLain = false;

        $scope.showModelInput = false;
        $scope.showCustomerInput = true;
        $scope.disabledSaveValidasiPelwil = false;
        $scope.disabledSaveProspect = false;

        $scope.initialUbah = 'Tambah';

        $scope.urlPricing = '/api/fe/PricingEngine?PricingId=10&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
        $scope.jsData = { Title: null, HeaderInputs: { $InputTotalAcc$: null, $OutletId$: null, $VehicleTypeColorId$: null, $VehicleYear$: null } }


        ModelFactory.getData().then(function (res) {
            $scope.optionsModelType = res.data.Result;
            return $scope.optionsModelType;
        });

        ProspectFactory.getDataConfigModule().then(function (res) {
            $scope.ModulConfig = res.data.Result[0];

            //return $scope.optionsModelType;
        });


        ComboBoxFactory.getDataFundSource().then(function (res) { $scope.fundsource = res.data.Result; });

        $scope.List_Pelanggan_InformasiKendaraanLain_To_Repeat = null;
        $scope.StockInfoAvaliability = null;

        //IfGotProblemWithModal();
        $scope.$on('$destroy', function () {
            angular.element('.ui.modal.ModalPerluasanJaminan').remove();
            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').remove();
            angular.element('.ui.modal.ModalKaroseriBookingUnit').remove();
            angular.element('.ui.modal.ModalTambahanBiaya').remove();
            angular.element('.ui.modal.selectvehicleprocess').remove();
            angular.element('.ui.modal.dokumentList').remove();
            angular.element('.ui.modal.ModalProspect').remove();
            angular.element('.ui.modal.ModalPelanggan').remove();
            angular.element('.ui.modal.ModalSuspect').remove();
            angular.element('.ui.modal.ModalAction').remove();
            angular.element('.ui.modal.selectVehicleProspect').remove();
			angular.element('.ui.modal.ModalProspectAktifkanDataSuspect').remove();
			angular.element('.ui.modal.ModalProspectNonAktifkanDataSuspect').remove();
        });



        $scope.bind_data = {
            data: [],
            filter: []
        };

        $scope.ListTahunKendaraan = [];
        ProspectFactory.getYearProspect().then(function (res) {
            $scope.ListTahunKendaraan = res.data.Result;

        });

        $scope.OpenNonMandatory = function () {
            if ($scope.ShowNonMandatory == false) {
                $scope.ShowNonMandatory = true;
                $scope.NonMandatoryDown = false;
                $scope.NonMandatoryUp = true;
            } else {
                $scope.ShowNonMandatory = false;
                $scope.NonMandatoryDown = true;
                $scope.NonMandatoryUp = false;
            }
        }


        $scope.OpenNonMandatoryPelanggan = function () {
            if ($scope.NonMandatoryPelanggan == false) {
                $scope.NonMandatoryPelanggan = true;
                $scope.NonMandatoryDownPelanggan = false;
                $scope.NonMandatoryUpPelanggan = true;
            } else {
                $scope.NonMandatoryPelanggan = false;
                $scope.NonMandatoryDownPelanggan = true;
                $scope.NonMandatoryUpPelanggan = false;
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        /// Dari Negotiation
        //////////////////////////////////////////////////////////////////////////////
        $scope.menuSimulasiKredit = false;
        var Data_Simulasi_Kredit = "";
        var Data_Simulasi_Asuransi = "";
        $scope.Lokasi_Outlet = "Jakarta";
        $scope.Nama_Outlet = "AUTO 2000 Sunter";


        $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "+";
        $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "+";
        $scope.ShowTabelSimulasiKredit_PanelIcon = "+";

        $scope.dateOptions = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
            minDate: new Date(),
        };



        //// Modal Simulasi /////
        $scope.openSimulasiKredit = function () {
            if ($scope.ModulConfig.NEGOTIATION == false) {
                // setTimeout(function () {
                //     angular.element('.ui.modal.ModalSettingModul').modal('refresh');
                // }, 0);
                // angular.element('.ui.modal.ModalSettingModul').modal('show');

                bsNotify.show(
                    {
                        title: "Info",
                        content: "This feature is under construction.",
                        type: 'info',
                        timeout: 6000
                    }
                );
                angular.element('.ui.modal.ModalProspect').modal('hide');
            } else {
            $scope.mSimulasiKredit = $scope.Prospect_Selected;

            $scope.menuSimulasiKredit = true;
            $scope.formData = false;
            angular.element('.ui.modal.ModalProspect').modal('hide');
            }

        }

        $scope.openSimulasiAsusransi = function () {
            if ($scope.ModulConfig.NEGOTIATION == false) {
                // setTimeout(function () {
                //     angular.element('.ui.modal.ModalSettingModul').modal('refresh');
                // }, 0);
                // angular.element('.ui.modal.ModalSettingModul').modal('show');
                bsNotify.show(
                    {
                        title: "Info",
                        content: "This feature is under construction.",
                        type: 'info',
                        timeout: 6000
                    }
                );
                angular.element('.ui.modal.ModalProspect').modal('hide');
            } else {
            $scope.mSimulasiAsuransi = $scope.Prospect_Selected
            $scope.ShowSimulasiAsuransi = true;
            $scope.formData = false;
            angular.element('.ui.modal.ModalProspect').modal('hide');
            }
        }

        $scope.tutupSimulasiAsuransi = function () {
            $scope.ShowSimulasiAsuransi = false;
            $scope.formData = true;
        }

        //////////////////////////////

        $scope.kembalidarikredit = function () {
            $scope.menuSimulasiKredit = false;
            $scope.detailKredit = false;
            $scope.formData = true;
        }

        $scope.ToSimulasiKredit_Clicked = function () {
            $scope.formData = false;
            $scope.ShowSimulasiKredit = true;
            $scope.menuSimulasiKredit = false;
        };

        $scope.saveSimulasi = function () {
            SimulasiKreditFactory.create($scope.mSimulasiKredit).then(function (res) {

            })
        }


        $scope.RawTipe = "";
        // TipeFactory.getData().then(function (res) {
        //     $scope.RawTipe = res.data.Result;
        //     return $scope.RawTipe;
        // });

        



        //bawah ini nanti cari gimana onload
        $scope.mSimulasiAsuransi_PerluasanJaminan = "";
        $scope.mSimulasiAsuransi_PerluasanJaminan = [{ Nama_Jaminan: null }];
        $scope.mSimulasiAsuransi_PerluasanJaminan.splice(0, 1);

        $scope.mSimulasiAsuransi_NamaJaminan_Submit = function () {
            $scope.mSimulasiAsuransi_PerluasanJaminan.push({ 'Nama_Jaminan': $scope.mSimulasiAsuransi_NamaJaminan_Txtbx });
            $scope.mSimulasiAsuransi_NamaJaminan_Txtbx = "";
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('hide');
        };

        $scope.RemoveJaminan = function (IndexJaminan) {
            $scope.mSimulasiAsuransi_PerluasanJaminan.splice(IndexJaminan, 1);
        };

        $scope.ProspectingTipeKendaraanUkuranMobile = function () {
            return "ProspectingBuatUkuranTipeKendaraan";

        }

        $scope.TabKreditPressed = function () {
            Data_Simulasi_Asuransi = ""
            Data_Simulasi_Kredit = SimulasiKreditFactory.getData();
            $scope.mSimulasiKredit = angular.copy(Data_Simulasi_Kredit[0]);
            //$scope.mSimulasiKredit_Prospect=Data_Simulasi_Kredit[0].Nama_Lengkap;
        };

        $scope.TabAsuransiPressed = function () {
            Data_Simulasi_Kredit = "";
            Data_Simulasi_Asuransi = SimulasiAsuransiFactory.getData();
            $scope.mSimulasiAsuransi = angular.copy(Data_Simulasi_Asuransi[0]);
            //$scope.mSimulasiAsuransi_Prospect=Data_Simulasi_Asuransi[0].Nama_Lengkap;
        };

        $scope.PerluasanJaminanModal = function () {
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('show');
            angular.element('.ui.modal.ModalPerluasanJaminan').not(':first').remove();
        };

        $scope.selectedEkstensionInsurance = [];
        $scope.toggleSelectedExtension = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedEkstensionInsurance.indexOf(data);
                $scope.selectedEkstensionInsurance.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedEkstensionInsurance.push(data);
            }
        }

        $scope.filterProductAsuransi = function(selected) {
            console.log('select',selected);
            if (selected != undefined || selected != null) {
                //ComboBoxFactory.getDataInsuranceProduct(selected).then(function(res) { $scope.product = res.data.Result; });
                ComboBoxFactory.getDataInsuranceEkstension(selected).then(function(res) { $scope.insuranceextension = res.data.Result; });
            }
        }

        $scope.OkSelectedExtension = function() {
            //$scope.selected_data.ListInfoPerluasanJaminan = $scope.selectedEkstensionInsurance;
            angular.element(".ui.modal.ModalPerluasanJaminan").modal("hide");
            // for (var i = 0; i < $scope.selectedEkstensionInsurance.length; i++) {
            //     $scope.selectedEkstensionInsurance[i].ApprovalFlag = 1;
            //     delete $scope.selectedEkstensionInsurance[i].checked;
            // }
            // $scope.selectedEkstensionInsurance = [];
        }

        $scope.mSimulasiAsuransi_NamaJaminan_Cancel = function () {
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('hide');
        };

        // $scope.ToSimulasiKredit_Clicked = function () {
        // 	$scope.formData=false;
        // 	$scope.ShowSimulasiKredit=true;
        // 	$scope.menuSimulasiKredit=false;
        // };

        $scope.ToSimulasiAsuransi_Clicked = function () {
            $scope.formData = false;
            $scope.ShowSimulasiAsuransi = true;
        };

        $scope.BackToTabClicked = function () {
            $scope.formData = true;
            $scope.ShowSimulasiKredit = false;
            $scope.ShowSimulasiAsuransi = false;
        };

        $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_Clicked = function () {
            $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan = !$scope.Show_Hide_SimulasiAsuransi_ModelKendaraan;
            if ($scope.Show_Hide_SimulasiAsuransi_ModelKendaraan == true) {
                $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "-";
            } else if ($scope.Show_Hide_SimulasiAsuransi_ModelKendaraan == false) {
                $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_SimulasiAsuransi_Premi_Clicked = function () {
            $scope.Show_Hide_SimulasiAsuransi_Premi = !$scope.Show_Hide_SimulasiAsuransi_Premi;
            if ($scope.Show_Hide_SimulasiAsuransi_Premi == true) {
                $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "-";
            } else if ($scope.Show_Hide_SimulasiAsuransi_Premi == false) {
                $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "+";
            }

        };

        $scope.ShowTabelSimulasiKredit = function () {
            $scope.TabelSimulasiKredit = !$scope.TabelSimulasiKredit;
            if ($scope.TabelSimulasiKredit == true) {
                $scope.ShowTabelSimulasiKredit_PanelIcon = "-";
            } else if ($scope.TabelSimulasiKredit == false) {
                $scope.ShowTabelSimulasiKredit_PanelIcon = "+";
            }

        };

        $scope.ModelKendaraan_KreditChanged = function () {
            var Filtered_Tipe1 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe1.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe.length; ++i) {
                if ($scope.RawTipe[i].VehicleModelId == $scope.mSimulasiKredit.VehicleModelId) {
                    Filtered_Tipe1.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
                }
            }

            ProspectFactory.getDaVehicleType('?start=1&limit=1000&filterData=VehicleModelId|' + $scope.mSimulasiKredit.VehicleModelId).then(function (res) {
                $scope.optionsTipe_Kredit = res.data.Result;
               // return $scope.RawTipe;
            });

            //$scope.optionsTipe_Kredit = Filtered_Tipe1;
            // $scope.mSimulasiKredit['VehicleTypeId']={};
        };

        $scope.ModelColor_KreditChanged = function () {
            // var Filtered_Tipe1 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            // Filtered_Tipe1.splice(0, 1);
            // for (var i = 0; i < $scope.RawTipe.length; ++i) {
            //     if ($scope.RawTipe[i].VehicleModelId == $scope.mSimulasiKredit.VehicleModelId) {
            //         Filtered_Tipe1.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
            //     }
            // }

            WarnaFactory.getData('?start=1&limit=100&filterData=VehicleTypeId|' + $scope.mSimulasiKredit.VehicleTypeId).then(function (res) {
                $scope.typecolor = res.data.Result;
                //$scope.vehicleTypecolorfilter = $scope.optionsWarna;
            });
               // return $scope.RawTipe;
            // });

            //$scope.optionsTipe_Kredit = Filtered_Tipe1;
            // $scope.mSimulasiKredit['VehicleTypeId']={};
        };


        $scope.ModelKendaraan_AsuransiChanged = function () {
            var Filtered_Tipe2 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe2.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe.length; ++i) {
                if ($scope.RawTipe[i].VehicleModelId == $scope.mSimulasiAsuransi.VehicleModelId) {
                    Filtered_Tipe2.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
                }
            }
            $scope.optionsTipe_Asuransi = Filtered_Tipe2;
            // $scope.mSimulasiAsuransi['VehicleTypeId']={};
        };


        $scope.LeasingChanged = function () {
            // var Filtered_Tipe3 = [{ LeasingLeasingTenorId: null, LeasingId: null, LeasingName: null, LeasingTenorTime: null, LeasingTenorId: null }];
            // Filtered_Tipe3.splice(0, 1);
            // for (var i = 0; i < $scope.RawTenor.length; ++i) {
            //     if ($scope.RawTenor[i].LeasingId == $scope.mSimulasiKredit.LeasingId) {
            //         Filtered_Tipe3.push({ LeasingLeasingTenorId: $scope.RawTenor[i].LeasingLeasingTenorId, LeasingId: $scope.RawTenor[i].LeasingId, LeasingName: $scope.RawTenor[i].LeasingName, LeasingTenorTime: $scope.RawTenor[i].LeasingTenorTime, LeasingTenorId: $scope.RawTenor[i].LeasingTenorId });
            //     }
            // }
            // $scope.optionsTenor = Filtered_Tipe3;

            ProspectFactory.getDataTenor($scope.mSimulasiKredit.LeasingId).then(
                function(res) {
                    $scope.optionsTenor = res.data.Result;
                },
                function(err) {}
            )

        
            // $scope.mSimulasiKredit['LeasingLeasingTenorId']={};
        };

        $scope.optionsCariBerdasarkan = [{ LeasingSimulationId: "1", LeasingSimulationName: "DP" },{ LeasingSimulationId: "2", LeasingSimulationName: "Tenor" }];

        //////////////////////////////////////////////////////////////////////////////
        ///// End Negotiation
        //////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////
        ///// Start Booking Unit 
        /////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////
        ///// End Booking Unit
        /////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////
        ///// Test Drive 
        /////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        /// GANTT Cart
        //////////////////////////////////////////////////////////////////////
        var chart = { dataProvider: null };
        $scope.listTestDrive = [];
        $scope.mNegotiation_Detail = {};
        $scope.belumAdatesdrive = false;
        $scope.tempdatetestdrive = new Date();
        var dataProviders = [];
        $scope.hour = [];
        var dateGantt = null;
        $scope.isList = false;
        $scope.pilihTestDrive = {};
        $scope.outdataProviders = {};
        //$scope.intended_operation = null;
        $scope.statusBuat = null;
        $scope.ShowNegotiationIncrementJamMulai = true;
        $scope.ShowNegotiationDecrementJamMulai = true;
        $scope.ShowNegotiationIncrementJamAkhir = true;
        $scope.ShowNegotiationDecrementJamAkhir = true;
        //$scope.optionsReason = [{ reason: "Ya", value: true }, { reason: "Tidak", value: false }];

        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
        }

        NegotiationFactory.getDataVehicleTestDrive().then(function (res) {
            $scope.VehicleTestDrive = res.data.Result;
        })

        //////////////////// Modal TestDrive //////////////////////
        $scope.statusActionTestDrive = null;
        $scope.TestDrive = function (data) {
            $scope.pilihTestDrive = {};
            $scope.statusActionTestDrive = null;
            if ($scope.ModulConfig.NEGOTIATION == false) {
                bsNotify.show(
                    {
                        title: "Info",
                        content: "This feature is under construction.",
                        type: 'info',
                        timeout: 6000
                    }
                );
                angular.element('.ui.modal.ModalProspect').modal('hide');
                angular.element('.ui.modal.ModalAction').modal('hide');
            } else {
                $scope.mNegotiation = data;
                $scope.dateOptionsFrom.minDate = new Date();
                angular.element('.ui.modal.selectvehicletestdrive').modal('show');
                $scope.vehicletoSelecttestdrive = data.CProspectDetailUnitView;
                angular.element('.ui.modal.ModalProspect').modal('hide');
                angular.element('.ui.modal.ModalAction').modal('hide');
            }
        }

        $scope.VehicleCheckedTestdrive = function (data) {
            //$scope.vehicleselcted.splice(0, 1);
            //$scope.vehicleselcted.push(data);
			//tadinya cuma ini 2 line diatas
			
			if($scope.vehicleselcted.length==0)
			{
				$scope.vehicleselcted.push(data);
				//$scope.vehicleselcted[0].ProductionYear = parseInt($scope.vehicleselcted[0].ProductionYear);
			}
			else
			{
				$scope.vehicleselcted.splice(0, 1);
			}
        };

        $scope.OkTestDrive = function () {
            NegotiationFactory.getDataVehicleTestDriveTersediaApaKaga($scope.vehicleselcted[0]).then(function (res){
                if(res.data.Result.length < 1){
                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: "Tipe ini tidak ada diaset test drive.",
                            type: 'warning',
                            timeout: 6000
                        }
                    );
                    $scope.ShowParameter = true;
                    $scope.formData = false;
                    $scope.ShowProspectDetail = false;
                    angular.element('.ui.modal.selectvehicletestdrive').modal('hide');
                }else{
                    $scope.pilihTestDrive.VehicleTypeId = angular.copy($scope.vehicleselcted[0].VehicleTypeId);
                        $scope.$watch('$scope.pilihTestDrive.VehicleTypeId', function (newValue, oldValue) {
                            chart = { dataProvider: null };
                            $scope.isChart = false;
                            $scope.isList = false;
                            $scope.pilihTestDrive.OutletId = null;
                            $scope.pilihTestDrive.DateTestDrive = null;
                            $scope.outletdetail = [];
                            $scope.pilihTestDrive.TestDriveUnitId = null;
                            for(var i in $scope.VehicleTestDrive){
                                if($scope.VehicleTestDrive[i].VehicleTypeId == $scope.pilihTestDrive.VehicleTypeId){
                                    $scope.detailVehicle = $scope.VehicleTestDrive[i];
                                }
                            }
                        });
                    $scope.ShowParameter = true;
                    $scope.formData = false;
                    $scope.ShowProspectDetail = false;
                    angular.element('.ui.modal.selectvehicletestdrive').modal('hide');
                }
            });
        }

        $scope.cancelTestDrive = function () {
            angular.element('.ui.modal.selectvehicletestdrive').modal('hide');
            $scope.vehicleselcted = [];
        }

        ///////////////////// View Prospect /////////////////////////////////
        function distinctVehicle (templistunit){
            var temp = [];
            for(var i in templistunit){
                if(temp.length == 0){
                    temp.push(templistunit[i]);
                }else{
                    var isSame = false;
                    for(var j in temp){
                        if(temp[j].VehicleTypeId == templistunit[i].VehicleTypeId){
                            isSame = true;
                        }
                    }
                    if(isSame == false){
                        temp.push(templistunit[i]);
                    }
                }
            }; 
            return temp;
        };
        
        $scope.Go_To_Jadwal_Test_Drive = function () {
            var templistunit = angular.copy($scope.VehicleTestDrive);
            ////// Distinct VehicleType //////
            $scope.listdistinctVehicle = distinctVehicle(templistunit);
            if($scope.statusActionTestDrive == 'isEdit'){

                $scope.pilihTestDrive.DateTestDrive = new Date();

                if ($scope.listTestDrive.length > 0) {
                    console.log("kesini ga", $scope.listTestDrive);
                    $scope.pilihTestDrive = {
                        VehicleTypeId: $scope.listTestDrive[0].VehicleTypeId,
                        TestDriveUnitId: $scope.listTestDrive[0].TestDriveUnitId,
                        DateTestDrive: $scope.tempdatetestdrive,
                        StartDate: $scope.listTestDrive[0].StartDate,
                        EndDate: $scope.listTestDrive[0].EndDate,
                        OutletOwnerId: $scope.listTestDrive[0].OutletOwnerId
                    }
                }

                $scope.$watch('$scope.pilihTestDrive.DateTestDrive', function (newValue, oldValue) {
                    $scope.listSchudleTest($scope.pilihTestDrive.DateTestDrive);
                });
            }
            
            ///// Refill Get Tes Drive /////
            //$scope.pilihTestDrive.DateTestDrive = $scope.tempdatetestdrive;

            // $scope.pilihT estDrive.StartDate = new Date();
            // $scope.pilihTestDrive.EndDate = new Date();

            // var tempnowDate = new Date();
            // var nowDate = tempnowDate.getHours();

            // if(nowDate <= 8){
            //     $scope.pilihTestDrive.StartDate.setHours(8);
            //     $scope.pilihTestDrive.StartDate.setMinutes(0);
            //     $scope.pilihTestDrive.EndDate.setHours(8);
            //     $scope.pilihTestDrive.EndDate.setMinutes(0);
            // }else{
            //     $scope.pilihTestDrive.StartDate.setHours(nowDate);
            //     $scope.pilihTestDrive.StartDate.setMinutes(0);
            //     $scope.pilihTestDrive.EndDate.setHours(nowDate);
            //     $scope.pilihTestDrive.EndDate.setMinutes(0);
            // }

            $scope.isChart = true;
            $scope.ShowParameter = false;
            $scope.ShowJadwalTestDrive = true;
            $scope.Batal_Button = false;
            $scope.Submit_Button = false;
        };

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':00:00';
                return fix;
            } else {
                return null;
            }
        };

        $scope.submit = function () {
            $scope.datestart = fixDate(new Date($scope.pilihTestDrive.StartDate));
            $scope.dateend = fixDate(new Date($scope.pilihTestDrive.EndDate));

            $scope.listTestDrive.splice(0, 1);
                $scope.listTestDrive.push({
                    VehicleType: $scope.detailVehicle.Description,
                    TestDriveUnitId: $scope.pilihTestDrive.TestDriveUnitId,
                    VehicleTypeId: $scope.pilihTestDrive.VehicleTypeId,
                    StartDate: $scope.datestart,
                    EndDate: $scope.dateend,
                    OutletOwnerId: $scope.pilihTestDrive.OutletOwnerId
                });

            NegotiationFactory.getCekTestDrive($scope.listTestDrive[0]).then(function (res){
                var hasil = res;
                if(res.data == false){
                    $scope.ShowParameter = true;
                    $scope.ShowJadwalTestDrive = false;
                    $scope.Batal_Button = true;
                    $scope.Submit_Button = true;
                    $scope.statusActionTestDrive = 'isEdit';
                    //$scope.pilihTestDrive = {};
                    dataProviders = [];
                }else{
                    bsNotify.show({
                        title: "Warning Message",
                        content: "Sudah Ada Test Drive Pada Jam ini",
                        type: 'warning'
                    });
                }
            });
        };

        ///////////// Get Tes Drive //////////////////////////////
        $scope.changed = function (enddate) {
            $scope.pilihTestDrive.EndDate = angular.copy(enddate);
        }

        $scope.listSchudleTest = function (dt) {
            $scope.pilihTestDrive.DateTestDrive = new Date(dt);
            $scope.tempdatetestdrive = angular.copy($scope.pilihTestDrive.DateTestDrive);
            console.log("asdasd", $scope.tempdatetestdrive);
            $scope.pilihTestDrive.OutletId = null;
            $scope.isChart = false;
            $scope.isList = true;

            NegotiationFactory.getTestDrive($scope.pilihTestDrive).then(function (res) {
                $scope.outletdetail = res.data.Result;
                if ($scope.outletdetail.length != 0) {
                    $scope.isList = true;
                }
            })
        }

        // $scope.detailVehicleselect = function (selected) {
        //     $scope.detailVehicle = selected

        // }



        $scope.hour = [];
        // $scope.detailVehicleselect = function (selected) {
        //     $scope.detailVehicle = selected;
        // }

        $scope.detailVehicleclear = function () {
            // if($scope.listTestDrive.length > 0 ){

            // }else{
            chart = { dataProvider: null };
            $scope.isChart = false;
            $scope.isList = false;
            $scope.pilihTestDrive.OutletId = null;
            $scope.pilihTestDrive.DateTestDrive = null;
            $scope.outletdetail = [];
            $scope.pilihTestDrive.TestDriveUnitId = null;
            for(var i in $scope.VehicleTestDrive){
                if( $scope.VehicleTestDrive[i].VehicleTypeId == newValue){
                    $scope.detailVehicle =  $scope.VehicleTestDrive[i];
                }
            }
            //}


        }


        $scope.dataJam = function (outlet) {

            $scope.isChart = true;
            var count = 0;

            $scope.detailVehicle = { VehicleTypeId: null, Description: null };

            var jumlahunit = outlet.ListUnitTestDrive.length;

            for (var i in outlet.ListUnitTestDrive) {
                if (outlet.ListUnitTestDrive[i].ListJadwalTestDrive.length == 0) {
                    count++;
                }
            }

            if (jumlahunit == count) {
                $scope.belumAdatesdrive = true;
                bsNotify.show({
                    //size: 'big',
                    type: 'info',
                    //timeout: 2000,
                    title: "Info",
                    content: "Hari ini belum ada jadwal test drive."
                });
            }

            for (var i in $scope.VehicleTestDrive) {
                if ($scope.VehicleTestDrive[i].VehicleTypeId == $scope.pilihTestDrive.VehicleTypeId) {
                    $scope.detailVehicle.VehicleTypeId = $scope.VehicleTestDrive[i].VehicleTypeId;
                    $scope.detailVehicle.Description = $scope.VehicleTestDrive[i].Description;
                }

            }

            var year = new Date($scope.pilihTestDrive.DateTestDrive).getFullYear();
            var mounth = ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2);
            var date = ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2);
            var hour = (new Date().getHours() < '10' ? '0' : '') + new Date().getHours();


            var hourstart = (new Date($scope.mNegotiation.DateStart).getHours() < '10' ? '0' : '') + new Date().getHours();
            var hoursend = (new Date($scope.mNegotiation.EndDate).getHours() < '10' ? '0' : '') + new Date().getHours();

            $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.DateTestDrive);
            $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.DateTestDrive);

            var nowDate = $scope.pilihTestDrive.DateTestDrive.getHours();

            if (nowDate <= 8) {
                $scope.pilihTestDrive.StartDate.setHours(8);
                $scope.pilihTestDrive.StartDate.setMinutes(0);
                $scope.pilihTestDrive.EndDate.setHours(8);
                $scope.pilihTestDrive.EndDate.setMinutes(0);
            } else if (nowDate >= 23) {
                $scope.pilihTestDrive.StartDate.setHours(23);
                $scope.pilihTestDrive.StartDate.setMinutes(0);
                $scope.pilihTestDrive.EndDate.setHours(8);
                $scope.pilihTestDrive.EndDate.setMinutes(23);
            } else {
                $scope.pilihTestDrive.StartDate.setHours(nowDate);
                $scope.pilihTestDrive.StartDate.setMinutes(0);
                $scope.pilihTestDrive.EndDate.setHours(nowDate);
                $scope.pilihTestDrive.EndDate.setMinutes(0);
            }


            dateGantt = year + '-' + mounth + '-' + date;

            dataProviders = outlet.ListUnitTestDrive;
            $scope.outdataProviders = dataProviders;

            $scope.platNo = [{ TestDriveUnitId: null, PoliceNumber: null }];
            for (var i in dataProviders) {
                $scope.platNo.push({
                    OutletOwnerId: dataProviders[i].OutletOwnerId,
                    TestDriveUnitId: dataProviders[i].TestDriveUnitId,
                    PoliceNumber: dataProviders[i].PoliceNumber
                });
            }
			
			if(dataProviders[0].ListJadwalTestDrive.length<=0)
			{
				console.log("ulala");
				dataProviders[0].ListJadwalTestDrive[0]=[{ ColorId: null,
				DateEnd: null,
				DateStart: null,
				OutletId: null,
				PoliceNumber: null,
				ScheduleId: null,
				StatusFollowUpTestDriveId: null,
				StatusTestDriveId: null,
				TestDriveUnitId: null,
				TimeDuration: null,
				TimeEnd: null,
				TimeStart: null,
				VehicleMappingId: null,
				VehicleModelId: null,
				VehicleTypeId: null,
				color:null}];

				
			}
			
			for (var i = 0; i < dataProviders[0].ListJadwalTestDrive.length; ++i) {
                try {
                    //var raw_garis_atas = dataProviders[0].ListJadwalTestDrive[i].DateEnd;
                    //var tanggal_garis_atas = raw_garis_atas.getFullYear() + '-' + ('0' + (raw_garis_atas.getMonth() + 1)).slice(-2) + '-' + ('0' + raw_garis_atas.getDate()).slice(-2);
                    //var jam_garis_atas = $filter('date')(raw_garis_atas, 'HH:mm:ss');
                    // da_guides.push({
                        // value: AmCharts.stringToDate(tanggal_garis_atas + " " + jam_garis_atas, "YYYY-MM-DD HH:NN"),
                        // lineColor: "#000000",
                        // lineThickness: 4,
                        // dashLength: 2,
                        // inside: true,
                        // labelRotation: 90,
                        // label: "" + jam_garis_atas
                    // });
                    if (i == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#67b7dc";
                    } else if (i == 1) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#71c9f2";
                    } else if (i % 3 == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#88f1ff";
                    } else if (i % 2 == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#b1ffff";
                    } else {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#42ff33";
                    }
                } catch (ulala) {}

            }
			
            chart = AmCharts.makeChart('chartdiv', {
                "type": "gantt",
                "theme": "light",
                "zoomOutText": "",
                "marginRight": 0,
                "marginLeft": 0,
                "period": "hh",
                "dataDateFormat": "YYYY-MM-DD",
                "balloonDateFormat": "JJ:NN",
                "columnWidth": 1,
                "valueAxis": {
                    "type": "number",
                    "includeHidden": true,
                    "maximum": 22.5,
                    "minimum": 8,
                    "reversed":true,
                    "showBalloon": false,
                    "labelFunction": function(data) {return ''+data+":00";},
                    //"guides": da_guides
                },
                "categoryAxis": {
                    "position": "top"
                },
                "brightnessStep": 10,
                "graph": {
                    "fillAlphas": 1,
                    //"balloonText": "<b>[[task]]</b>: [[open]] [[value]]",
                    "labelPosition": "top",
                    "showBalloon": false
                },
                "rotate": false,
                "categoryField": "PoliceNumber",
                "segmentsField": "ListJadwalTestDrive",
                "colorField": "color",
                "startDate": dateGantt,
                "startField": "TimeStart",
                "endField": "TimeEnd",
                "durationField": "duration",
                "dataProvider": dataProviders,
                "valueScrollbar": {
                    "autoGridCount": true,
                    "enabled": false
                },
                "chartCursor": {
                    "cursorColor": "#55bb76",
                    "valueBalloonsEnabled": true,
                    "cursorAlpha": 0,
                    "valueLineAlpha": 0.5,
                    "valueLineBalloonEnabled": true,
                    "valueLineEnabled": true,
                    "zoomable": false,
                    "valueZoomable": false
                },
                "export": {
                    "enabled": false
                }
            });

        }

        $scope.platnoAmbil = function (selected) {
            $scope.pilihTestDrive.OutletOwnerId = selected.OutletOwnerId;
            var tempunit = $scope.outdataProviders.filter(function (type) {
                return (type.TestDriveUnitId == $scope.pilihTestDrive.TestDriveUnitId);
            })

            $scope.hour = [];
            for (var i in tempunit[0].ListJadwalTestDrive) {
                $scope.hour.push({ start: new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(), end: new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours() })

            }

        }


        $scope.savetestdrive = function () {
            $scope.mNegotiationPost = [];
            for (var i in $scope.listTestDrive) {
                $scope.mNegotiationPost.push({
                    ProspectId: $scope.mNegotiation.ProspectId,
                    TestDriveUnitId: $scope.listTestDrive[i].TestDriveUnitId,
                    OutletId: $scope.listTestDrive[i].OutletOwnerId,
                    DateStart: $scope.listTestDrive[i].StartDate,
                    DateEnd: $scope.listTestDrive[i].EndDate
                })
            }


            NegotiationFactory.create($scope.mNegotiationPost).then(function (res) {
                var respond = res.data.ResponseMessage;
                var lengthstring = res.data.ResponseMessage.length;
                var count = (respond.match(/#/g) || []).length;
                var index = respond.indexOf('#');
                
                if (count >= 1){
                    bsNotify.show({
                        title: "Info Message",
                        content: respond.slice(index +1 , lengthstring),
                        type: 'info'
                    });
                }else{
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Test drive berhasil diajukan.",
                        type: 'success'
                    });
                }
                $scope.statusActionTestDrive = null;
                $scope.ShowParameter = false;
                $scope.formData = true;
                $scope.mNegotiationPost = {};
                $scope.listTestDrive = [];
            })

        }

        $scope.kembalidaritestdrive = function () {
            $scope.ShowParameter = false;
            $scope.formData = true;
            $scope.vehicleselcted = [];
        }

        // $scope.getdataOutlet = function (outlet){
        // 	//dataProviders = outlet.ListUnitTestDrive;

        // }
        $scope.refreshjadwal = function () {

            for (var i in $scope.outletdetail) {
                if ($scope.outletdetail[i].OutletId == $scope.pilihTestDrive.OutletId) {
                    $scope.outletdetailselect = $scope.outletdetail[i];
                }
            }
            $scope.dataJam($scope.outletdetailselect);

        }


        //////////////////////////////////////////////////////////////////////
        ///
        //////////////////////////////////////////////////////////////////////


        $scope.intended_operation = "None"; //status operation
        var Selected_Detail_Id = ""; //ini buat selected id yg detail
        $scope.ArrayRiwayatFollowUP = null; //ini riwayat by default 0 dulu


        $scope.backtesdrive = function () {
            $scope.ShowParameter = true;
            $scope.ShowJadwalTestDrive = false;
            $scope.listTestDrive = [];
            //$scope.formData = true;
        }

        $scope.NegotiationIncrementStartDateTime = function () {
            if ($scope.pilihTestDrive.StartDate.getHours() < 22) {
                $scope.pilihTestDrive.StartDate.setHours($scope.pilihTestDrive.StartDate.getHours() + 1);
                $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.StartDate);
                $scope.changed($scope.pilihTestDrive.StartDate);
            }
            else if ($scope.pilihTestDrive.StartDate.getHours() >= 22) {
                $scope.ShowNegotiationIncrementJamMulai = false;
                // $scope.ShowNegotiationIncrementJamMulai=true;
                // $scope.ShowNegotiationDecrementJamMulai=true;
                // $scope.ShowNegotiationIncrementJamAkhir=true;
                // $scope.ShowNegotiationDecrementJamAkhir=true;
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Anda melebihi batas waktu yang ditentukan.",
                        type: 'warning'
                    }
                );
            }

        }

        $scope.NegotiationDecrementStartDateTime = function () {
            if ($scope.pilihTestDrive.StartDate.getHours() > 8) {
                $scope.pilihTestDrive.StartDate.setHours($scope.pilihTestDrive.StartDate.getHours() - 1);
                $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.StartDate);
                $scope.changed($scope.pilihTestDrive.StartDate);
            }
            else if ($scope.pilihTestDrive.StartDate.getHours() <= 8) {
                $scope.ShowNegotiationDecrementJamMulai = false;
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Anda melebihi batas waktu yang ditentukan.",
                        type: 'warning'
                    }
                );
            }

        }

        $scope.NegotiationIncrementEndDateTime = function () {
            if ($scope.pilihTestDrive.EndDate.getHours() < 22) {
                $scope.pilihTestDrive.EndDate.setHours($scope.pilihTestDrive.EndDate.getHours() + 1);
                $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.EndDate);
            }
            else if ($scope.pilihTestDrive.EndDate.getHours() >= 22) {
                $scope.ShowNegotiationIncrementJamAkhir = false;
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Anda melebihi batas waktu yang ditentukan.",
                        type: 'warning'
                    }
                );
            }

        }

        $scope.NegotiationDecrementEndDateTime = function () {
            if ($scope.pilihTestDrive.EndDate.getHours() > 8) {
                $scope.pilihTestDrive.EndDate.setHours($scope.pilihTestDrive.EndDate.getHours() - 1);
                $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.EndDate);
            }
            else if ($scope.pilihTestDrive.DateEndBaru.getHours() <= 8) {
                $scope.ShowNegotiationDecrementJamAkhir = false;
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Anda melebihi batas waktu yang ditentukan.",
                        type: 'warning'
                    }
                );
            }

        }

        $scope.changed = function (enddate) {
            $scope.pilihTestDrive.EndDate = angular.copy(enddate);
        }


        /////////////////////////////////////////////////////////////////////////////
        ///// End Test Drive 
        /////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////
        ///// Buat SPK
        /////////////////////////////////////////////////////////////////////////////
        $scope.selected_data = {};
        $scope.selected_data.ListInfoDocument = [];
        $scope.selected_data.ListInfoPelanggan = [];
        $scope.selected_data.ListInfoPerluasanJaminan = [];
        $scope.selected_data.ListInfoLeasing = [];
        $scope.selected_data.ListInfoUnit = [];
        $scope.prospectToSpk = [];
        $scope.vehicleselcted = [];

        $scope.BuatSPKProspect = function () {
            $rootScope.$emit("buatSPKProspect", {});
        }

        $scope.buatSPK = function (data) {
            var tempunit = angular.copy(data.CProspectDetailUnitView);
            var arrayTempVehicleType = [];
            for(var i in tempunit){
                if(arrayTempVehicleType.length == 0){
                    arrayTempVehicleType.push({
                        VehicleTypeId : tempunit[i].VehicleTypeId
                    });
                }else{
                    var sama = false;
                    for(var j in arrayTempVehicleType){
                        if (tempunit[i].VehicleTypeId == arrayTempVehicleType[j].VehicleTypeId){
                            sama = true;
                        }
                    }
                    if(sama == false){
                        arrayTempVehicleType.push({
                            VehicleTypeId : tempunit[i].VehicleTypeId
                        });
                    }
                }
            };



            // var buildVehicle = [];
            // for(var i in tempunit){
            //     if(buldVehicle.length == 0){
            //         buildVehicle.push({
            //             OutletId: temp[i].OutletId,
            //             VehicleTypeId: temp[i].VehicleTypeId,
            //             Description: temp[i].Description,
            //             VehicleModelId: temp[i].VehicleModelId,
            //             ColorId: temp[i].ColorId,
            //             ColorName: temp[i].ColorName,
            //             VehicleTypeColorId: temp[i].VehicleTypeColorId,
            //             RRNNo: null,
            //             FrameNo: null,
            //             ListAccessories: [],
            //             ListAccessoriesPackage: [],
            //         })
            //     }
            // }

            // $scope.selected_data.ListInfoUnit[0].ListDetailUnit = [];
            // for (var i = 0; i < $scope.selected_data.ListInfoUnit[0].Qty; i++) {
            //     $scope.selected_data.ListInfoUnit[0].ListDetailUnit.push({
            //         OutletId: $scope.selected_data.ListInfoUnit[0].OutletId,
            //         VehicleTypeId: $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
            //         Description: $scope.selected_data.ListInfoUnit[0].Description,

            //         VehicleModelId: $scope.selected_data.ListInfoUnit[0].VehicleModelId,
            //         ColorId: $scope.selected_data.ListInfoUnit[0].ColorId,
            //         ColorName: $scope.selected_data.ListInfoUnit[0].ColorName,
            //         VehicleTypeColorId: $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId,
            //         RRNNo: null,
            //         FrameNo: null,
            //         ListAccessories: [],
            //         ListAccessoriesPackage: [],
            //     });
            // }

            //console.log("typeid", data);

            if ($scope.ModulConfig.SPK == false) {
                ProspectFactory.dealProspectUnderConstruction(data).then(function(res){
                    $scope.back();
                    bsNotify.show(
                        {
                            title: "Berhasil",
                            content: "SPK berhasil dibuat.",
                            type: 'succes',
                            timeout: 6000
                        }
                    );
                    angular.element('.ui.modal.ModalProspect').modal('hide');
                    angular.element('.ui.modal.ModalAction').modal('hide');
                });
            } else {
                if(data.CustomerTypeId == null || data.CustomerTypeId == undefined || data.CustomerTypeId == ""){
                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: "Tentukan kategori pelanggan dahulu.",
                            type: 'warning',
                            timeout: 6000
                        }
                    );
                }else{
                    angular.element('.ui.modal.selectvehicleprocess').modal('show');
                    angular.element('.ui.modal.selectvehicleprocess').not(':first').remove();
                    
                    $scope.prospectToSpk = data;
                    $scope.vehicletoSelect = data.CProspectDetailUnitView;
                }

            }
        }

        $scope.VehicleChecked = function (data) {
            //$scope.vehicleselcted.splice(0, 1);
			if($scope.vehicleselcted.length==0)
			{
				$scope.vehicleselcted.push(data);
				$scope.vehicleselcted[0].ProductionYear = parseInt($scope.vehicleselcted[0].ProductionYear);
			}
			else
			{
				$scope.vehicleselcted.splice(0, 1);
			}
            //$scope.vehicleselcted.push(data);
            //$scope.vehicleselcted[0].ProductionYear = parseInt($scope.vehicleselcted[0].ProductionYear);
			console.log("datanya", data);
			//console.log("datanya2", $scope.vehicleselcted);
        };

        $scope.KembaliPilihVehicle = function () {
            angular.element('.ui.modal.selectvehicleprocess').modal('hide');
            $scope.vehicleselcted = [];
        }


        $rootScope.$on("KembalidariProspect", function () {
            $scope.back();
        })

        $scope.back = function () {
            $scope.formData = true;
            $scope.toSpk = false;
            $scope.buatSPKProspect = false;
            ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Prospect_To_Repeat = res.data.Result;
                $scope.TotalProspect = res.data.Total;
                $scope.Connection.Online = true;
                $scope.Connection.Netstate = 'online';
                //return $scope.List_Prospect_To_Repeat;
            },
                function (err) {

                    $scope.Connection.Online = false;
                    $scope.Connection.Netstate = 'offline';
                }
            );
        }

        $scope.okSpk = function () {
            //angular.element('.ui.modal.selectvehicleprocess').modal({allowMultiple: true}).modal('hide');

            setTimeout(function () {
                angular.element('.ui.modal.lodingspk').modal('show');
                // angular.element('.ui.modal.lodingspk').modal({allowMultiple: true}).modal('show');
                console.log("test");
                angular.element('.ui.modal.selectvehicleprocess').modal('hide');
                $scope.toSpk = true;

            }, 0);
        }


        $scope.PilihVehicleBuatSPK = function () {
            //bsLoading.event(true);
            // setTimeout(function(){ 
            console.log("init selesai");
            $scope.buatSPKProspect = true;
            $rootScope.statusAction = 'buat';
            $scope.formData = false;
            $scope.ShowProspectDetail = false;
            setTimeout(function () {
                angular.element('.ui.modal.lodingspk').modal('hide');
            }, 0);
            $scope.selected_data.ProspectId = $scope.prospectToSpk.ProspectId;
            $scope.selected_data.ProspectCode = $scope.prospectToSpk.ProspectCode;
            $scope.selected_data.SalesId = $scope.prospectToSpk.SalesId;
            $scope.selected_data.CustomerId = $scope.prospectToSpk.CustomerId;
            $scope.selected_data.ToyotaId = $scope.prospectToSpk.ToyotaId;
            $scope.selected_data.FundSourceId = $scope.prospectToSpk.FundSourceId;
            $scope.selected_data.TradeInBit = $scope.prospectToSpk.TradeInBit;
            if ($scope.prospectToSpk.CustomerTypeId == 1) {
                $scope.selected_data.CustomerTypeId = 3;
            } else {
                $scope.selected_data.CustomerTypeId = $scope.prospectToSpk.CustomerTypeId;
            }

            $scope.selected_data.ListInfoUnit = $scope.vehicleselcted;

            $scope.selected_data.ListInfoUnit[0].ListDetailUnit = [];
            for (var i = 0; i < $scope.selected_data.ListInfoUnit[0].Qty; i++) {
                $scope.selected_data.ListInfoUnit[0].ListDetailUnit.push({
                    OutletId: $scope.selected_data.ListInfoUnit[0].OutletId,
                    VehicleTypeId: $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                    Description: $scope.selected_data.ListInfoUnit[0].Description,

                    VehicleModelId: $scope.selected_data.ListInfoUnit[0].VehicleModelId,
                    ColorId: $scope.selected_data.ListInfoUnit[0].ColorId,
                    ColorName: $scope.selected_data.ListInfoUnit[0].ColorName,
                    VehicleTypeColorId: $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId,
                    RRNNo: null,
                    FrameNo: null,
                    ListAccessories: [],
                    ListAccessoriesPackage: [],
                });
            }

            $rootScope.selected_data = $scope.selected_data;
            $rootScope.prospectToSpk = $scope.prospectToSpk;
            console.log("ceku", $rootScope.selected_data, $rootScope.prospectToSpk);
            $scope.BuatSPKProspect();
            $scope.vehicleselcted = [];

            // }, 2000);


        }
        /////////////////////////////////////////////////////////////////////////////////////////
        ///////
        /////////////////////////////////////////////////////////////////////////////////////////

        ComboBoxFactory.getDataInsurance().then(function(res) { $scope.optionsPerusahaanAsuransi = res.data.Result; });

        $scope.btnTambahperluasan = function () {
            angular.element('.ui.modal.insuranceEkstension').modal('show');
            angular.element('.ui.modal.insuranceEkstension').not(':first').remove();
        }

        $scope.openDokumenlist = function () {
            ComboBoxFactory.getDataDocument().then(function (res) {
                $scope.dokumentlist = res.data.Result;
                angular.element('.ui.modal.dokumentList').modal('refresh').modal('show');
                angular.element('.ui.modal.dokumentList').not(':first').remove();
            })
        }

        $scope.selectedItems = [];
        $scope.selectedItemsKaroseri = [];

        $scope.toggleChecked = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);

            } else {
                data.checked = true;
                $scope.selectedItems.push(data);

            }
        };

        $scope.ApproveData = function () {
            angular.element('.ui.modal.dokumentList').modal('hide');
            $scope.selected_data.ListInfoDocument = $scope.selectedItems;
            for (var i in $scope.selected_data.ListInfoDocument) {
                $scope.selected_data.ListInfoDocument[i].UploadDataDocument = {};
                // for (var j in $scope.selected_data.ListInfoDocument[i].DocumentURL){
                //     $scope.selected_data.ListInfoDocument[i].DocumentURL[j].
                // }
                $scope.fileKK[i].push({ "Branch": null, "DocumentTypeId": null, "FileName": null, "NameTable": null, "TableRefId": null, "strBase64": null })
            }

            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 1;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];
        };

        $scope.RejectData = function () {
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 0;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];
        };


        $scope.btnTambahKaroseri = function () {
            $scope.selected_data.KaroseriPrice = 2500000;
            $scope.selected_data.Karoseri = "Karoseri";
        }

        $scope.removeKaroseri = function () {
            $scope.selected_data.KaroseriPrice = null;
            $scope.selected_data.Karoseri = null;
        }

        $scope.removeImage = function (index) {
            $scope.fileKK[index].Branch = undefined;
            $scope.fileKK[index].DocumentTypeId = undefined;
            $scope.fileKK[index].FileName = undefined;
            $scope.fileKK[index].NameTable = undefined;
            $scope.fileKK[index].TableRefId = undefined;
            $scope.fileKK[index].strBase64 = undefined;
        }


        $scope.AccessoriesChange = function (id) {
            $scope.brp = 0;
            $scope.brp = id + 1;

        }


        ////////////////////////////////////////////////////////////////////////////
        ///// End SPK
        ////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////////////
        /// Begin Prospect
        ///////////////////////////////////////////////////////////
        ComboBoxFactory.getDataProvince().then(function (res) {
            $scope.province = res.data.Result;
            var saveProvince = JSON.stringify(res.data.Result);
            LocalService.unset('Province');
            LocalService.set('Province', saveProvince);
        });



        $scope.selectedProvince = null;
        $scope.mProspect_Pelanggan = {};
        $scope.filterKabupaten = function (selected) {
            $scope.mProspect_Prospect.ProvinceId = selected;
            $scope.mProspect_Pelanggan.ProvinceId = selected;

            if ($scope.online == false) {
                $scope.tempKabupaten = JSON.parse(LocalService.get('Kabupaten'));
                $scope.kabupaten = $scope.tempKabupaten.filter(function (type) { return (type.ProvinceId == selected); })
            } else {
                KabupatenKotaFactory.getData('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function (res) {
                    $scope.tempKabupaten = res.data.Result;
                    $scope.kabupaten = $scope.tempKabupaten;
                });

            }
        }

        $scope.provinceValue = function (selected) {
            $scope.mProspect_BuatProspect.ProvinceId = selected;
            if ($scope.online == false) {
                $scope.tempKabupaten = JSON.parse(LocalService.get('Kabupaten'));
                $scope.kabupaten = $scope.tempKabupaten.filter(function (type) { return (type.ProvinceId == selected); })
            } else {

                KabupatenKotaFactory.getData('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function (res) {
                    $scope.tempKabupaten = res.data.Result;
                    $scope.kabupaten = $scope.tempKabupaten;
                });

            }
        }
        $scope.selectedSumberData = null;
        $scope.sumberDataValue = function (selected) {

            $scope.mProspect_BuatProspect.ProspectSourceId = selected.ProspectSourceId;
            $scope.mProspect_BuatProspect.ProspectSourceName = selected.ProspectSourceName;

        }
		
        $scope.selectedCategoryFleet = null;
        $scope.categoryFleetValue = function (selected) {
            $scope.mProspect_Prospect.CategoryFleetId = selected;

        }
        $scope.selectedCategoryPelanggan = null;
        $scope.categoryPelangganValue = function (selected) {
            $scope.mProspect_Prospect.CustomerTypeId = selected.CustomerTypeId;
            $scope.mProspect_Prospect.CustomerTypeDesc = selected.CustomerTypeDesc;


        }
        $scope.selectAgama = null;
        $scope.agamaValue = function (selected) {
            $scope.mProspect_Prospect.CustomerReligionId = selected;
            $scope.mProspect_Pelanggan.CustomerReligionId = selected;

        }
        $scope.selectedSuku = null;
        $scope.sukuValue = function (selected) {
            $scope.mProspect_Prospect.CustomerEthnicId = selected;
            $scope.mProspect_Pelanggan.CustomerEthnicId = selected;

        }
        $scope.selectedJabatan = null;
        $scope.jabatanValue = function (selected) {
            $scope.mProspect_Prospect.CustomerPositionId = selected;
            $scope.mProspect_Pelanggan.CustomerPositionId = selected;

        }
        $scope.selectedFundSource = null;
        $scope.fundSourceValue = function (selected) {
            $scope.mProspect_Prospect.FundSourceId = selected;

        }
        $scope.selectedKebutuhan = null;
        $scope.selectedKategori = null;

        $scope.kebutuhanValue = function (selected) {

            $scope.mProspect_Prospect.PurchaseTimeId = selected;
            if ($scope.controlData == 'SimpanProspect'){
                $scope.Prospect_Selected = {};
                $scope.Prospect_Selected.PurchaseTimeId = selected;
            }else
            if ($scope.controlData == 'SimpanSuspect'){
                $scope.Prospect_Selected = {};
                $scope.Prospect_Selected.PurchaseTimeId = selected;
            }else
            {
                $scope.Prospect_Selected.PurchaseTimeId = selected;
            }
            console.log('selected',selected);

            if (selected == 1) {

                $scope.selectedKategori = {};
                $scope.selectedKategori.Kategori = {};
                $scope.selectedKategori.Kategori = $scope.optionsKategoriProspect[0];
                $scope.mProspect_Prospect.CategoryProspectId = $scope.optionsKategoriProspect[0].CategoryProspectId;

            }
            if (selected == 2) {

                $scope.selectedKategori = {};
                $scope.selectedKategori.Kategori = {};
                $scope.selectedKategori.Kategori = $scope.optionsKategoriProspect[1];
                $scope.mProspect_Prospect.CategoryProspectId = $scope.optionsKategoriProspect[1].CategoryProspectId;

            }
            if (selected == 3) {

                $scope.selectedKategori = {};
                $scope.selectedKategori.Kategori = {};
                $scope.selectedKategori.Kategori = $scope.optionsKategoriProspect[2];
                $scope.mProspect_Prospect.CategoryProspectId = $scope.optionsKategoriProspect[2].CategoryProspectId;

            }

        }



        $scope.selectedKategori = null;
        $scope.kategoriValue = function (selected) {
            $scope.mProspect_Prospect.CategoryProspectId = selected;
            if (selected == 1) {
                $scope.mProspect_Prospect.CategoryProspectName = 'Hot';
            } else if (selected == 2){
                $scope.mProspect_Prospect.CategoryProspectName = 'Medium';
            } else{
                $scope.mProspect_Prospect.CategoryProspectName = 'Low';
            }
            console.log('kategori value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName );

        }
        $scope.selectedPurchasePurpose = null;
        $scope.purchasePurposeValue = function (selected) {
            $scope.mProspect_Prospect.PurchasePurposeId = selected;

        }
		
		$scope.selectedKlasifikasiPembelian = {};
        $scope.KlasifikasiPembelianValue = function (selected) {
            $scope.mProspect_Prospect.PurchasingClassificationId = selected.PurchasingClassificationId;

        }
		
        $scope.selectedMerek = null;
        $scope.merekValue = function (selected) {

            $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.BrandId = selected.BrandId;
            $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.BrandName = selected.BrandName;

        }

        $scope.selectedCityRegency = null;
        $scope.filterKecamatan = function (selected) {
            $scope.mProspect_Prospect.CityRegencyId = selected;
            $scope.mProspect_Pelanggan.CityRegencyId = selected;
            KecamatanFactory.getData('?start=1&limit=100&filterData=CityRegencyId|' + selected).then(function (res) {
                $scope.tempKecamatan = res.data.Result;
                $scope.kecamatan = $scope.tempKecamatan;


            });
            //$scope.kecamatan = $scope.tempKecamatan.filter(function (type) { return (type.CityRegencyId == selected); })
            //     ProspectFactory.getPelanggaranWilayan().then(function(res) {
            //         $scope.ConfigPelanggaran = res.data.Result[0];


            //         if ( $scope.ConfigPelanggaran.PelanggaranWilayahBit == true) {
            //             ProspectFactory.getDataLocation().then(function(res) {
            //                 $scope.OutletLocation = res.data.Result;

            //                 $scope.cekOutlet = $scope.OutletLocation[0].CityRegencyId;

            //                 //return $scope.optionsKategoriPelanggan;
            //                 if ($scope.mProspect_Prospect.CityRegencyId == $scope.cekOutlet || $scope.mProspect_Prospect.CityRegencyId == null || typeof $scope.mProspect_Prospect.CityRegencyId == "undefined") {
            //                     //$scope.Execute_SimpanEditDetailProspect();
            //                     $scope.ModalPelanggaranWilayah = { show: false };
            //                 } else {
            //                     bsNotify.show({
            //                         title: "Peringatan",
            //                         content: "Data prospek yang anda input termasuk pelanggaran wilayah, Data tersebut tidak bisa dilanjutkan ke menu berikutnya",
            //                         type: 'warning'
            //                     });
            //                 }

            //             });

            //         }else
            //         {
            //             $scope.ModalPelanggaranWilayah = { show: false };
            //         }
            //     }
            // );


        }

        $scope.selectedDistrict = null;
        $scope.filterKelurahan = function (selected) {
            $scope.mProspect_Prospect.DistrictId = selected;
            $scope.mProspect_Pelanggan.DistrictId = selected;

            KelurahanFactory.getData('?start=1&limit=100&filterData=DistrictId|' + selected).then(function (res) {
                $scope.tempKelurahan = res.data.Result;

                $scope.kelurahan = $scope.tempKelurahan;


            })

        }

        $scope.selectedVillage = null;
        $scope.kelurahanValue = function (selected) {
            $scope.mProspect_Prospect.PostalCode = '';
            $scope.mProspect_Prospect.VillageId = selected;
            $scope.mProspect_Pelanggan.VillageId = selected;
            $scope.tempPostalCode = $scope.kelurahan.filter(function (type) { return (type.VillageId == selected); });

            $scope.mProspect_Prospect.PostalCode = $scope.tempPostalCode[0].PostalCode;
            $scope.mProspect_Pelanggan.PostalCode = $scope.tempPostalCode[0].PostalCode;


        }
        $scope.selectedPostalCode = null;
        $scope.postalCodeValue = function (selected) {
            $scope.mProspect_Prospect.PostalCode = selected;
            $scope.mProspect_Pelanggan.PostalCode = selected;

        }

        $scope.filterWarna = function (selected) {
            $scope.warna = $scope.tempwarna.filter(function (type) {
                return (type.VehicleTypeId == selected);
            })
        };

        $scope.filterTenor = function (selected) {
            $scope.tenor = $scope.temptenor.filter(function (type) {
                return (type.LeasingId == selected);
            })
        }

        $scope.filterProduct = function (selected) {
            $scope.product = $scope.tempproductinsurance.filter(function (type) { return (type.InsuranceId == selected); });
            $Scope.insuranceextension = $scope.tempinsuranceextension.filter(function (type) { return (type.InsuranceId == selected) })
        }
        //var firstTrigger = true;
        //$scope.List_Suspect_To_Repeat=SuspectFactory.getData();

        //start loading
        // angular.element('.ui.modal.loadingDataProspecting').modal('show');
        $scope.openLoading = true;

        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
            $scope.List_Suspect_To_Repeat = res.data.Result;
            $scope.TotalSuspect = res.data.Total;
            setTimeout(function () {
                //angular.element('.ui.modal.loadingDataProspecting').modal('hide');
                //$scope.openLoading = false;
            }, 0);
            return $scope.List_Suspect_To_Repeat;
        });

        $scope.SuspectLoadMore = function () {
            var tempStart = $scope.List_Suspect_To_Repeat.length + 1;
            var tempLimit = 30;

            if ($scope.ControlLoadMore == 'Search') {
                
				var da_filter = "";
					
					da_filter = '?start=' + tempStart + '&limit=' + tempLimit;
					
					if ($scope.selectTab == 'Prospect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&ProspectStartDate="+fixDate($scope.ProspectStartDate)+"&ProspectEndDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
                        }

                        
                        

					}
					
					if ($scope.selectTab == 'Suspect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&AssignDate="+fixDate($scope.ProspectStartDate)+"&AssignDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						// if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						// {
							
							// da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						// }
					}
					
					if ($scope.selectTab == 'Customer')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&SpkDate="+fixDate($scope.ProspectStartDate)+"&SpkDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						}
					}
					
				da_filter +="&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter;
				
				SuspectFactory.getData(da_filter).then(function (res) {
                    $scope.Temp_List_Suspect = res.data.Result;
                    $scope.TotalSuspect = res.data.Total;
                    for (var i in $scope.Temp_List_Suspect) {
                        $scope.List_Suspect_To_Repeat.push($scope.Temp_List_Suspect[i]);
                    }


                })
            } else {
                SuspectFactory.getData('?start=' + tempStart + '&limit=' + tempLimit).then(function (res) {
                    $scope.Temp_List_Suspect = res.data.Result;
                    $scope.TotalSuspect = res.data.Total;

                    for (var i in $scope.Temp_List_Suspect) {
                        $scope.List_Suspect_To_Repeat.push($scope.Temp_List_Suspect[i]);
                    }

                    setTimeout(function () {
                        angular.element('.ui.modal.loadingDataProspecting').modal('hide');
                    }, 0);
                })
            }

        }



        //$scope.List_Pelanggan_To_Repeat=PelangganFactory.getData();
        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
            $scope.List_Pelanggan_To_Repeat = res.data.Result;
            $scope.TotalCustomer = res.data.Total;
            return $scope.List_Pelanggan_To_Repeat;
        });

        $scope.CustomerLoadMore = function () {
            var tempStart = $scope.List_Pelanggan_To_Repeat.length + 1;
            var tempLimit = 30;

            if ($scope.ControlLoadMore == 'Search') {
                
				var da_filter = "";
					
					da_filter = '?start=' + tempStart + '&limit=' + tempLimit;
					
					if ($scope.selectTab == 'Prospect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&ProspectStartDate="+fixDate($scope.ProspectStartDate)+"&ProspectEndDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						}
					}
					
					if ($scope.selectTab == 'Suspect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&AssignDate="+fixDate($scope.ProspectStartDate)+"&AssignDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						// if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						// {
							
							// da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						// }
					}
					
					if ($scope.selectTab == 'Customer')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&SpkDate="+fixDate($scope.ProspectStartDate)+"&SpkDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						}
					}
					
				da_filter +="&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter;
				
				PelangganFactory.getData(da_filter).then(function (res) {
                    $scope.Temp_List_Customer = res.data.Result;
                    $scope.TotalCustomer = res.data.Total;
                    for (var i in $scope.Temp_List_Customer) {
                        $scope.List_Pelanggan_To_Repeat.push($scope.Temp_List_Customer[i]);
                    }
                })
            } else {
                PelangganFactory.getData('?start=' + tempStart + '&limit=' + tempLimit).then(function (res) {
                    $scope.Temp_List_Customer = res.data.Result;
                    $scope.TotalCustomer = res.data.Total;

                    for (var i in $scope.Temp_List_Customer) {
                        $scope.List_Pelanggan_To_Repeat.push($scope.Temp_List_Customer[i]);
                    }
                })
            }



        }

        //$scope.List_Prospect_To_Repeat=ProspectFactory.getData();
        $scope.Connection = {};
        $scope.Connection.Online = {};
        $scope.Connection.Netstate = {};

        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
            $scope.List_Prospect_To_Repeat = res.data.Result;
            $scope.TotalProspect = res.data.Total;
            $scope.Connection.Online = true;
            $scope.Connection.Netstate = 'online';
            //return $scope.List_Prospect_To_Repeat;
            setTimeout(function () {
                //angular.element('.ui.modal.loadingDataProspecting').modal('hide');
                $scope.openLoading = false;
            }, 0);
        },
            function (err) {

                $scope.Connection.Online = false;
                $scope.Connection.Netstate = 'offline';
                setTimeout(function () {
                    //angular.element('.ui.modal.loadingDataProspecting').modal('hide');
                    $scope.openLoading = false;
                }, 0);

            }
        );

        $scope.ProspectLoadMore = function () {
            var tempStart = $scope.List_Prospect_To_Repeat.length + 1;
            var tempLimit = 30;

            if ($scope.ControlLoadMore == 'Search') {
                
                var da_filter = "";
                var limit = 1;
					
					if($scope.selectTab == 'Prospect')
					{
						da_filter = "?start=" + $scope.startGet + "" + limit+"&Id=1";
					}
					else
					{
						da_filter = "?start=" + $scope.startGet + "" + limit+"";
					}
					
					if ($scope.selectTab == 'Prospect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&ProspectStartDate="+fixDate($scope.ProspectStartDate)+"&ProspectEndDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null)
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
                        }

                       
					}
					
					if ($scope.selectTab == 'Suspect')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&AssignDate="+fixDate($scope.ProspectStartDate)+"&AssignDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						// if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						// {
							
							// da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						// }
					}
					
					if ($scope.selectTab == 'Customer')
					{
						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							da_filter+="&SpkDate="+fixDate($scope.ProspectStartDate)+"&SpkDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						{
							
							da_filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
						}
					}
					
				da_filter +="&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter;
				//'?start=' + tempStart + '&limit=' + tempLimit + "&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter
				ProspectFactory.getData(da_filter).then(function (res) {
                    $scope.Temp_List_Prospect = res.data.Result;
                    $scope.TotalProspect = res.data.Total;
                    for (var i in $scope.Temp_List_Prospect) {
                        $scope.List_Prospect_To_Repeat.push($scope.Temp_List_Prospect[i]);
                    }
                })
            } else {
                ProspectFactory.getData('?start=' + tempStart + '&limit=' + tempLimit).then(function (res) {
                    $scope.Temp_List_Prospect = res.data.Result;
                    $scope.TotalProspect = res.data.Total;

                    for (var i in $scope.Temp_List_Prospect) {
                        $scope.List_Prospect_To_Repeat.push($scope.Temp_List_Prospect[i]);
                    }


                })
            }


        }


        CategoryProspectSourceFactory.getData().then(function (res) {
            $scope.optionsSumberProspect = res.data.Result;
            var saveSumberProsect = JSON.stringify(res.data.Result);
            LocalService.unset('ProspectSource');
            LocalService.set('ProspectSource', saveSumberProsect);

            return $scope.optionsSumberProspect;
        });

        CategoryProspectSourceFactory.getDataSuspectSource().then(function (res) {
            $scope.optionsSumberSuspect = res.data.Result;
            // var saveSumberProsect = JSON.stringify(res.data.Result);
            // LocalService.unset('ProspectSource');
            // LocalService.set('ProspectSource', saveSumberProsect);

            // return $scope.optionsSumberProspect;
        });
		
		ProspectFactory.getDataKlasifikasiPembelian().then(function (res) {
            $scope.optionsKlasifikasiPembelian = res.data.Result;
            
        });
        // }

        SalesForceFactory.getData().then(function (res) {
            $scope.optionsSalesForce = res.data.Result;
            return $scope.optionsSalesForce;
        });



        $scope.Prospect_Operation_InformasiUnitYangDiminati = "";


        $scope.Prospect_FromModule_InformasiKendaraanLain = "";
        $scope.Prospect_Operation_InformasiKendaraanLain = "";

        //$scope.optionsMerekMobil = [{ name: "Toyota", value: "Toyota" }, { name: "BMW", value: "BMW" }];
        MerekMobilFactory.getData().then(function (res) {
            $scope.optionsMerekMobil = res.data.Result;
            return $scope.optionsMerekMobil;
        });


        //$scope.optionsModel = ModelFactory.getData();
        ModelFactory.getData().then(function (res) {
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });

        ProfileLeasing.getData().then(function (res) {
            $scope.optionsLeasing = res.data.Result;
            return $scope.optionsLeasing;
        });

        $scope.selectedModel = null;
        $scope.SelectedModel = {};
        $scope.SelectedTipe = {};


        $scope.filtertipe = function (selected) {
            $scope.selectedTipe.VehicleType = null;

            if ($scope.initialUbah == 'Ubah') {
                $scope.initialUbah = 'Tambah';
            }
            else if ($scope.initialUbah == 'Tambah') {
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId = selected.VehicleModelId;
                $scope.SelectedModel.VehicleModelId = selected.VehicleModelId;

                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName = selected.VehicleModelName;
                $scope.SelectedModel.VehicleModelName = selected.VehicleModelName;

				//ini tadinya pake factory TipeFactory tapi refi ketemu bug jadi aku ganti gini
                ProspectFactory.getDaVehicleType('?start=1&limit=1000&filterData=VehicleModelId|' + selected.VehicleModelId).then(function (res) {
                    $scope.vehicleTypefilter = res.data.Result;
                    return $scope.RawTipe;
                });

                console.log("masuk sini 3");
                //$scope.vehicleTypefilter = $scope.RawTipe.filter(function(type) { return (type.VehicleModelId == selected.VehicleModelId); })
                
				
				$scope.selectedwarna = {};
                $scope.selectedwarna.Warna = null;
				
				$scope.selectedTipe = {};
                $scope.selectedTipe.VehicleType = null;
				
				//ini dibawah tadi ga ada
				$scope.SelectedColor.ColorId = null;
				$scope.SelectedColor.ColorName = null;
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = null;
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = null;
				
				
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = null;
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = null;
				
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = null;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = null;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode = null;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode = null;
				
            }
        }

        $scope.getProspectingUkuranBuatTipeKendaraan = function () {
            return "ProspectKecilinSelectOptionBuatMobile";//ini ngambil kelas dari css

        }

        $scope.selectedTipe = null;
        $scope.filterColor = function (selected) {
            console.log('$scope.initialUbah', $scope.initialUbah);
            if ($scope.initialUbah == 'Ubah') {
                $scope.initialUbah = 'Tambah';
            }
            else if ($scope.initialUbah == 'Tambah') {
                console.log('selected', selected);
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = selected.VehicleTypeId;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = selected.Description;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode = selected.KatashikiCode;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode = selected.SuffixCode;
                $scope.SelectedTipe.Description = selected.Description;
                $scope.SelectedTipe.VehicleTypeId = selected.VehicleTypeId;
                $scope.SelectedTipe.KatashikiCode = selected.KatashikiCode;
                $scope.SelectedTipe.SuffixCode = selected.SuffixCode;
                console.log('$scope.SelectedTipe', $scope.SelectedTipe);

                WarnaFactory.getData('?start=1&limit=100&filterData=VehicleTypeId|' + selected.VehicleTypeId).then(function (res) {
                    $scope.optionsWarna = res.data.Result;
                    $scope.vehicleTypecolorfilter = $scope.optionsWarna;
                });
				
				$scope.selectedwarna = {};
                $scope.selectedwarna.Warna = null;
				
				//4 linedibawah tadi ga ada
				$scope.SelectedColor.ColorId = null;
				$scope.SelectedColor.ColorName = null;
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = null;
				$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = null;
            }

        }


        //$scope.optionsTipe = TipeFactory.getData();
        $scope.RawTipe = "";
        //$scope.RawTipeFiltered=[{ VehicleTypeId: null,VehicleTypeName:null}];
        //$scope.RawTipeFiltered.splice(0, 1);
        // TipeFactory.getData().then(function(res) {
        //     $scope.RawTipe = res.data.Result;
        //     return $scope.RawTipe;
        // });


        TujuanPembelianFactory.getData().then(function (res) {
            $scope.optionsPurchasePurpose = res.data.Result;
            return $scope.optionsPurchasePurpose;
        });

        //$scope.optionsKategoriPelanggan = [{ name: "Individu", value: "Individu" }, { name: "Perusahaan", value: "Perusahaan" }, { name: "Pemerintah", value: "Pemerintah" }, { name: "Yayasan", value: "Yayasan" }];
        KategoriPelangganFactory.getData().then(function (res) {
            $scope.optionsKategoriPelanggan = res.data.Result;
            return $scope.optionsKategoriPelanggan;
        });


        //$scope.optionsKategoriFleet = [{ name: "Fleet", value: "Fleet" }, { name: "Non-Fleet", value: "Non-Fleet" }];
        KategoriFleetFactory.getData().then(function (res) {
            $scope.optionsKategoriFleet = res.data.Result;
            return $scope.optionsKategoriFleet;
        });

        //$scope.optionsAgama = [{ name: "Islam", value: "Islam" }, { name: "Katholik", value: "Katholik" }, { name: "Kristen Protestan", value: "Kristen Protestan" }, { name: "Hindu", value: "Hindu" }, { name: "Buddha", value: "Buddha" }, { name: "Kong Hu Cu", value: "Kong Hu CU" }, { name: "Lainnya", value: "Lainnya" }];
        AgamaFactory.getData().then(function (res) {
            $scope.optionsAgama = res.data.Result;
            return $scope.optionsAgama;
        });


        SukuFactory.getData().then(function (res) {
            $scope.optionsSuku = res.data.Result;
            return $scope.optionsSuku;
        });

        //$scope.optionsJabatan = [{ name: "jabatan1", value: "jabatan1" }, { name: "jabatan2", value: "jabatan2" }];
        JabatanFactory.getData().then(function (res) {
			$scope.optionsJabatan = res.data.Result;
            return $scope.optionsJabatan;
        });

        //$scope.optionsMetodePembayaran = [{ name: "Pembayaran Cash", value: "Pembayaran Cash" }, { name: "Pembayaran Kredit", value: "Pembayaran Kredit" }];
        MetodePembayaranFactory.getData().then(function (res) {
            $scope.optionsMetodePembayaran = res.data.Result;
            return $scope.optionsMetodePembayaran;
        });


        //$scope.optionsTingkatKebutuhan = [{ name: "<1 Bulan", value: "<1 Bulan" }, { name: "1-3 Bulan", value: "1-3 Bulan" }, { name: ">3 Bulan", value: ">3 Bulan" }];
        TingkatKebutuhanFactory.getData().then(function (res) {
            $scope.optionsTingkatKebutuhan = res.data.Result;
            return $scope.optionsTingkatKebutuhan;
        });

        //$scope.optionsAlasanDrop = [{ name: "Diskon Tidak Sesuai", value: "Diskon Tidak Sesuai" }, { name: "Pindah Ke Brand Lain", value: "Pindah Ke Brand Lain" }, { name: "Menunda Rencana Pembelian Lebih Dari 2 Bulan", value: "Menunda Rencana Pembelian Lebih Dari 2 Bulan" }, { name: "Sudah Deal Dengan Cabang Lain", value: "Sudah Deal Dengan Cabang Lain" }, { name: "Lainnya", value: "Lainnya" }];
        AlasanDropProspectFactory.getData().then(function (res) {
            $scope.optionsAlasanDrop = res.data.Result;
            //return $scope.optionsAlasanDrop;
        });

        //$scope.optionsKategoriProspect = [{ name: "Hot", value: "Hot" }, { name: "Medium", value: "Medium" }, { name: "Low", value: "Low" }, { name: "Drop", value: "Drop" }];
        KategoriProspectFactory.getData().then(function (res) {
            $scope.optionsKategoriProspect = res.data.Result;
            return $scope.optionsKategoriProspect;
        });
		
		$scope.optionsProspectKategoriData = [{
                Valid: 1,
                ProspectKategoriDataName: "Valid"
            },
            {
                Valid: 0,
                ProspectKategoriDataName: "Invalid"
            }


        ];
		
		$scope.ProspectingFilterKategoriData=$scope.optionsProspectKategoriData[0].Valid;
		



        //$scope.optionsKeperluanPenggunaanKendaraan = [{ name: "Pribadi", value: "Pribadi" }, { name: "Usaha", value: "Usaha" }, { name: "Lain-Lain", value: "Lain-Lain" }];


        $scope.Show_Hide_Pelanggan_Informasi_Pelanggan_PanelIcon = "+";
        $scope.Show_Hide_Prospect_Informasi_Pelanggan_PanelIcon = "+";
        $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "+";
        $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati_PanelIcon = "+";
        $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_PanelIcon = "+";
        $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon = "+";

        $scope.formData = true;
        $scope.From_Page = "";
        $scope.From_Page_Prospect_Or_Not = "";
        $scope.Drop_From_Page = "";


        $scope.SuspectActionSelected = function (SelectedSuspect) {
            $scope.Suspect_Selected_Data = SelectedSuspect;
            console.log('SuspectActionSelected ===>',SelectedSuspect);
            $scope.CustomerTypeId = SelectedSuspect.CustomerTypeId

            if($scope.CustomerTypeId == 3){
                $scope.CustomerTypeId = 1
                console.log('CustomerTypeId aslinya 3 eh jadi 1=====>',$scope.CustomerTypeId);
            }else{
                console.log('CustomerTypeId asli =====>',$scope.CustomerTypeId);
            }
            
            $scope.getCPD(SelectedSuspect.SuspectCode);
            console.log('getCPD From SuspectCode ====>',SelectedSuspect.SuspectCode);
            

            // $scope.ModalSuspect={show:true};
            angular.element('.ui.modal.ModalSuspect').modal('show');
            angular.element('.ui.modal.ModalSuspect').not(':first').remove();
        };

        $scope.Show_Hide_Pelanggan_Informasi_Pelanggan_Clicked = function () {
            $scope.Show_Hide_Pelanggan_Informasi_Pelanggan = !$scope.Show_Hide_Pelanggan_Informasi_Pelanggan;
            if ($scope.Show_Hide_Pelanggan_Informasi_Pelanggan == true) {
                $scope.Show_Hide_Pelanggan_Informasi_Pelanggan_PanelIcon = "-";
            } else if ($scope.Show_Hide_Pelanggan_Informasi_Pelanggan == false) {
                $scope.Show_Hide_Pelanggan_Informasi_Pelanggan_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_Prospect_Informasi_Pelanggan_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_Pelanggan = !$scope.Show_Hide_Prospect_Informasi_Pelanggan;
            if ($scope.Show_Hide_Prospect_Informasi_Pelanggan == true) {
                $scope.Show_Hide_Prospect_Informasi_Pelanggan_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Informasi_Pelanggan == false) {
                $scope.Show_Hide_Prospect_Informasi_Pelanggan_PanelIcon = "+";
            }

        };


        $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit = !$scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit;
            if ($scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit == true) {
                $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit == false) {
                $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "+";
            }

        };
        $scope.Show_Hide_Prospect_Informasi_Lain_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_Lain = !$scope.Show_Hide_Prospect_Informasi_Lain;
            if ($scope.Show_Hide_Prospect_Informasi_Lain == true) {
                $scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Informasi_Lain == false) {
                $scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati = !$scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati;
            if ($scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati == true) {
                $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati == false) {
                $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_Clicked = function () {
            $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain = !$scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain;
            if ($scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain == true) {
                $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_PanelIcon = "-";
            } else if ($scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain == false) {
                $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = !$scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain;
            if ($scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain == true) {
                $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain == false) {
                $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon = "+";
            }
        };


        $scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform_Clicked = function () {
            $scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform = !$scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform;
            if ($scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform == true) {
                $scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform_PanelIcon = "-";
                // $scope.dummyDataAccordionAdit();
                console.log('ini buka tab baru ===>', $scope.dataCPD, $scope.dataCPD.length);
                if($scope.dataCPD.length == 0){
                    $scope.clearDataCPD();
                    if(isAddProspect == true){
                        bsNotify.show({
                            title: "Data CPD Kosong",
                            content: "Tidak dapat menampilkan data Informasi Dari Customer Data Platform ",
                            type: 'warning'
                        }); 
                    }
                }

            } else if ($scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform == false) {
                $scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform_PanelIcon = "+";
            }
        };

        $scope.Show_Hide_Prospect_Customer_Interest_Clicked = function () {
            $scope.Show_Hide_Prospect_Customer_Interest = !$scope.Show_Hide_Prospect_Customer_Interest;
            if ($scope.Show_Hide_Prospect_Customer_Interest == true) {
                $scope.Show_Hide_Prospect_Customer_Interest_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Customer_Interest == false) {
                $scope.Show_Hide_Prospect_Customer_Interest_PanelIcon = "+";
            }
        };

        $scope.Show_Hide_Prospect_Customer_Profile_Clicked = function () {
            $scope.Show_Hide_Prospect_Customer_Profile = !$scope.Show_Hide_Prospect_Customer_Profile;
            if ($scope.Show_Hide_Prospect_Customer_Profile == true) {
                $scope.Show_Hide_Prospect_Customer_Profile_PanelIcon = "-";
            } else if ($scope.Show_Hide_Prospect_Customer_Profile == false) {
                $scope.Show_Hide_Prospect_Customer_Profile_PanelIcon = "+";
            }
        };


        


        $scope.PelangganActionSelected = function (SelectedPelanggan) {

            console.log('PelangganActionSelected ===>',SelectedPelanggan)
            $scope.getCPD(SelectedPelanggan.ToyotaId);
            console.log('getCPD From ToyotaId ====>',SelectedPelanggan.ToyotaId);

            $scope.CustomerTypeId = SelectedPelanggan.CustomerTypeId

            if($scope.CustomerTypeId == 3){
                $scope.CustomerTypeId = 1
                console.log('CustomerTypeId aslinya 3 eh jadi 1=====>',$scope.CustomerTypeId);
            }else{
                console.log('CustomerTypeId asli =====>',$scope.CustomerTypeId);
            }



            $scope.Pelanggan_Selected = SelectedPelanggan;
            angular.element('.ui.modal.ModalPelanggan').modal('show');
            angular.element('.ui.modal.ModalPelanggan').not(':first').remove();
        };



        $scope.CustomerTypeId = null;
        $scope.ProspectActionSelected = function (SelectedData) {
            console.log('ProspectActionSelected ====>',SelectedData);
            

            var kaloAdaDataToyotaIdPakeIniAja = null
            if(SelectedData.ToyotaId != null){
                kaloAdaDataToyotaIdPakeIniAja = SelectedData.ToyotaId;
                console.log('ProspectActionSelected getCPD by ToyotaId ========>',SelectedData.ToyotaId);
            }else{
                kaloAdaDataToyotaIdPakeIniAja = SelectedData.ProspectCode;
                console.log('ProspectActionSelected getCPD by ProspectCode ====>',SelectedData.ProspectCode);
            }
            $scope.getCPD(kaloAdaDataToyotaIdPakeIniAja);
            console.log('getCPD From ProspectCode ====>',kaloAdaDataToyotaIdPakeIniAja);

            angular.element('.ui.modal.ModalProspect').modal('hide');
            $scope.Prospect_Selected = SelectedData;

            setTimeout(function () {
                angular.element('.ui.modal.ModalProspect').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalProspect').modal('show');

            angular.element('.ui.modal.ModalProspect').not(':first').remove();
        };

        $scope.ProspectOfflineActionSelected = function (SelectedDataOffline) {
            angular.element('.ui.modal.ModalProspectOffline').modal('hide');
            $scope.ProspectOffline_Selected = SelectedDataOffline;
            setTimeout(function () {
                angular.element('.ui.modal.ModalProspectOffline').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalProspectOffline').modal('show');

            angular.element('.ui.modal.ModalProspectOffline').not(':first').remove();
        }

        $scope.Pelanggan_InformasiKendaraanLain_Action = function (SelectedData) {
            $scope.Pelanggan_InformasiKendaraanLain_Selected_Data = SelectedData;
            //$scope.ModalPelanggan_InformasiKendaraanLain_Action = { show: true };
            setTimeout(function () {
                angular.element('.ui.modal.ModalPelanggan_InformasiKendaraanLain_Action').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalPelanggan_InformasiKendaraanLain_Action').modal('show');
        };


        


        
        

        $scope.dataCPD = [];

        $scope.clearDataCPD = function(){
            $scope.dataCPD.push({
                AdsHistory               : [{data:' - '}],
                FormSubmittedHistory     : [{data:' - '}],
                OfflineJourney           : [{data:' - '}],
                HistoryCar               : [{data:' - '}],
                InterestModelFormOffline : [{data:' - '}],
                Program                  : [{data:' - '}],
                MelihatIklan             : [{data:' - '}],
                OnlineBehavior           : [{data:' - '}],
                InterestModelFormOnline  : [{data:' - '}],
                Scoring                  : ' - ',
                Category                 : ' - ',
                JumlahdiProspek          : ' - ',
                TanggalLahir             : ' - ',
                Umur                     : ' - ',
                StatusPernikahan         : ' - ',
                RangePendapatanGross     : ' - ',
                Hobby                    : ' - ',
                Pekerjaan                : ' - ',
                JenisKelamin             : ' - ',
            })

            // $scope.dataCPD[0].AdsHistory.push({data:"-"})              
            // $scope.dataCPD[0].FormSubmittedHistory.push({data:"-"})    7
            // $scope.dataCPD[0].OfflineJourney.push({data:"-"})          
            // $scope.dataCPD[0].HistoryCar.push({data:"-"})              
            // $scope.dataCPD[0].InterestModelFormOffline.push({data:"-"})
            // $scope.dataCPD[0].Program.push({data:"-"})                 
            // $scope.dataCPD[0].MelihatIklan.push({data:"-"})            
            // $scope.dataCPD[0].OnlineBehavior.push({data:"-"})          
            // $scope.dataCPD[0].InterestModelFormOnline.push({data:"-"}) 
        }


        $scope.dateFormatDDMMYYYY = function(date){
            if(date == '' || date == undefined || date == null || date == "NULL"){
                $scope.dataCPD[0].Umur = '-'
                return '-';
            }else{
                var initial = date.split('/');
                var tgl = initial[2];
                var bln = initial[1];
                var thn = initial[0];
                var hasil = tgl + '/' +bln + '/' +thn;

                $scope.countAge(hasil)
                return hasil;
            }
        }


        $scope.dataCPDstring = function(data){
            if(data == '' || data == undefined || data == null || data == "NULL"){
                return '-';
            }else{
                return data;
            }
        }

        $scope.makeArray = function(data) {
            if(data == null || data == undefined || data == '' || data == "NULL"){
                console.log('makeArray | ga masuk sini  ====>',data)
                return [{data : ' - '}]
            }else{
                console.log('makeArray | masuk sini ok ====>',data)
                var split = data.split('|');
                var finalArray = [];
                for(var i in split){
                    finalArray.push({data:split[i]})
                }
                return finalArray;
            }
        }

        $scope.countAge = function(data) {
            if(data == null || data == undefined || data == '' || data == "NULL"){
                $scope.dataCPD[0].Umur = '-'
            }else{
                console.log('countAge ===>',data)
                var split = data.split('/');
                var born = parseInt(split[2])
                var year = (new Date()).getFullYear();
                var age = year - born;
                $scope.dataCPD[0].Umur = age;
            }
        }


        $scope.getCPD = function (param){
            $scope.Show_Hide_Prospect_Informasi_Dari_Customer_Data_Platform = false
            isAddProspect = false;
            $scope.dataCPD = [];
            ProspectFactory.getDataCPD(param).then(
                function (res) {
                    if(res.data.Result.length > 0){
                        $scope.dataCPD.push(res.data.Result[0]);
                        console.log('getDataCPD ===>',$scope.dataCPD);
                        $scope.dataCPD[0].TanggalLahir             = $scope.dateFormatDDMMYYYY($scope.dataCPD[0].TanggalLahir);
                        $scope.dataCPD[0].AdsHistory               = $scope.makeArray($scope.dataCPD[0].AdsHistory);
                        $scope.dataCPD[0].FormSubmittedHistory     = $scope.makeArray($scope.dataCPD[0].FormSubmittedHistory);
                        $scope.dataCPD[0].OfflineJourney           = $scope.makeArray($scope.dataCPD[0].OfflineJourney);
                        $scope.dataCPD[0].HistoryCar               = $scope.makeArray($scope.dataCPD[0].HistoryCar);
                        $scope.dataCPD[0].InterestModelFormOffline = $scope.makeArray($scope.dataCPD[0].InterestModelFormOffline);
                        $scope.dataCPD[0].Program                  = $scope.makeArray($scope.dataCPD[0].Program);
                        $scope.dataCPD[0].MelihatIklan             = $scope.makeArray($scope.dataCPD[0].MelihatIklan);
                        $scope.dataCPD[0].OnlineBehavior           = $scope.makeArray($scope.dataCPD[0].OnlineBehavior);
                        // $scope.dataCPD[0].Umur                     = $scope.countAge($scope.dataCPD[0].TanggalLahir);
                        $scope.dataCPD[0].InterestModelFormOnline  = $scope.makeArray($scope.dataCPD[0].InterestModelFormOnline);
                        $scope.dataCPD[0].Scoring                  = $scope.dataCPDstring($scope.dataCPD[0].Scoring);
                        $scope.dataCPD[0].Category                 = $scope.dataCPDstring($scope.dataCPD[0].Category);
                        $scope.dataCPD[0].JumlahdiProspek          = $scope.dataCPDstring($scope.dataCPD[0].JumlahdiProspek);
                        $scope.dataCPD[0].StatusPernikahan         = $scope.dataCPDstring($scope.dataCPD[0].StatusPernikahan);
                        $scope.dataCPD[0].JenisKelamin             = $scope.dataCPDstring($scope.dataCPD[0].JenisKelamin);
                        $scope.dataCPD[0].Hobby                    = $scope.dataCPDstring($scope.dataCPD[0].Hobby);
                        $scope.dataCPD[0].RangePendapatanGross     = $scope.dataCPDstring($scope.dataCPD[0].RangePendapatanGross);
                        $scope.dataCPD[0].Pekerjaan                = $scope.dataCPDstring($scope.dataCPD[0].Pekerjaan);
                    }else{
                        console.log('getDataCPD kosong ga ada array nya ===>',res);
                    }
                }
            );
        }

        $scope.ShowPelangganDisTelp = false;
        $scope.ListKendaraanPelanggan = {};
        $scope.SelectPelanggan = function (SelectedData) {
            $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
            $scope.ShowPelangganDisTelp = true;
            $scope.ListKendaraanPelanggan = SelectedData.ListKendaraan;
            $scope.mProspect_Prospect = angular.copy(SelectedData);
            
            $scope.selectedProvince = {};
            $scope.selectedProvince.Province = {};
            for (var i = 0; i < $scope.province.length; ++i) {
                if ($scope.province[i].ProvinceId == SelectedData.ProvinceId) {
                    $scope.selectedProvince.Province = $scope.province[i];
                    break;
                }
            }

            var tempKab = [{
                CityRegencyName: SelectedData.CityRegencyName,
                CityRegencyId: SelectedData.CityRegencyId
            }];
            $scope.kabupaten = tempKab;
            $scope.selectedCityRegency = {};
            $scope.selectedCityRegency.CityRegency = {};
            for (var i = 0; i < $scope.kabupaten.length; ++i) {
                if ($scope.kabupaten[i].CityRegencyId == SelectedData.CityRegencyId) {
                    $scope.selectedCityRegency.CityRegency = $scope.kabupaten[i];
                    break;
                }
            }

            var tempKec = [{
                DistrictName: SelectedData.DistrictName,
                DistrictId: SelectedData.DistrictId
            }];
            $scope.kecamatan = tempKec;
            $scope.selectedDistrict = {};
            $scope.selectedDistrict.District = {};
            for (var i = 0; i < $scope.kecamatan.length; ++i) {
                if ($scope.kecamatan[i].DistrictId == SelectedData.DistrictId) {
                    $scope.selectedDistrict.District = $scope.kecamatan[i];
                    break;
                }
            }

            var tempKel = [{
                VillageName: SelectedData.VillageName,
                VillageId: SelectedData.VillageId
            }];
            $scope.kelurahan = tempKel;
            $scope.selectedVillage = {};
            $scope.selectedVillage.Village = {};
            for (var i = 0; i < $scope.kelurahan.length; ++i) {
                if ($scope.kelurahan[i].VillageId == SelectedData.VillageId) {
                    $scope.selectedVillage.Village = $scope.kelurahan[i];
                    break;
                }
            }

            $scope.selectedCategoryFleet = {};
            $scope.selectedCategoryFleet.CategoryFleet = {};
            for (var i = 0; i < $scope.optionsKategoriFleet.length; ++i) {
                if ($scope.optionsKategoriFleet[i].CategoryFleetId == SelectedData.CategoryFleetId) {
                    $scope.selectedCategoryFleet.CategoryFleet = $scope.optionsKategoriFleet[i];
                    break;
                }
            }

            $scope.selectAgama = {};
            $scope.selectAgama.Agama = {};
            for (var i = 0; i < $scope.optionsAgama.length; ++i) {
                if ($scope.optionsAgama[i].CustomerReligionId == SelectedData.CustomerReligionId) {
                    $scope.selectAgama.Agama = $scope.optionsAgama[i];
                    break;
                }
            }

            $scope.selectedSuku = {};
            $scope.selectedSuku.Suku = {};
            for (var i = 0; i < $scope.optionsSuku.length; ++i) {
                if ($scope.optionsSuku[i].CustomerEthnicId == SelectedData.CustomerEthnicId) {
                    $scope.selectedSuku.Suku = $scope.optionsSuku[i];
                    break;
                }
            }

            $scope.Show_Hide_Pelanggan_Informasi_Pelanggan == true;
            $scope.Show_Hide_Pelanggan_Informasi_Pelanggan_PanelIcon = "+";
            $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain == true;
            $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_PanelIcon = "+";

            //$scope.optionsKabupatenKota = KabupatenKotaFactory.getData();
            // KabupatenKotaFactory.getData().then(function (res) {
            // 	$scope.optionsKabupatenKota = res.data.Result;
            // 	return $scope.optionsKabupatenKota;
            // });

            $scope.From_Page_Prospect_Or_Not = "Not_Prospect";
            $scope.ShowPelangganDetail = true
            $scope.formData = false;
            $scope.ModalPelanggan = { show: false };


            $scope.GunakanUntukProspekDetailPelanggan_Button = true;
            $scope.BatalEditDetailPelanggan_Button = false;
            $scope.SimpanEditDetailPelanggan_Button = false;
            $scope.EditButtonToggle = true;


            //coba lagi
            //for(var i = 0; i < $scope.List_Pelanggan_To_Repeat.length; ++i)
            //{
            //if($scope.List_Pelanggan_To_Repeat[i].Id == Id)
            //{	

            $scope.mProspect_Pelanggan = angular.copy(SelectedData);
            $scope.mProspect_Pelanggan.StatusPelanggan = "Pelanggan";

            $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain_PanelIcon = "+";
            $scope.Show_Hide_Pelanggan_Informasi_Kendaraan_Lain = false;


            var PelangganKategori_SelectedIndex = 0;
            for (var i = 0; i < $scope.optionsKategoriPelanggan.length; ++i) {
                if ($scope.optionsKategoriPelanggan[i].CustomerTypeId == SelectedData.CustomerTypeId) {
                    PelangganKategori_SelectedIndex = i;
                }
            }


            if ($scope.optionsKategoriPelanggan[PelangganKategori_SelectedIndex].CustomerTypeDesc == "Individu") {
                $scope.PelangganIndividu = true;
                $scope.PelangganInstansi = false;
                $scope.List_Pelanggan_InformasiKendaraanLain_To_Repeat = PelangganInformasiKendaraanLainFactory.getData();
            } else {
                $scope.PelangganIndividu = false;
                $scope.PelangganInstansi = true;
                $scope.List_Pelanggan_InformasiKendaraanLain_To_Repeat = "";
            }


            //}	
            //}

            //disini disable semua
            $scope.mProspect_Pelanggan_ToyotaId_EnableDisable = false;
            $scope.mProspect_Pelanggan_KategoriPelanggan_EnableDisable = false;
            $scope.mProspect_Pelanggan_KategoriFleet_EnableDisable = false;
            $scope.mProspect_Pelanggan_NamaLengkap_EnableDisable = false;
            $scope.mProspect_Pelanggan_NamaPanggilan_EnableDisable = false;
            $scope.mProspect_Pelanggan_NamaInstitusi_EnableDisable = false;
            $scope.mProspect_Pelanggan_NamaPIC_EnableDisable = false;
            $scope.mProspect_Pelanggan_AlamatInstitusi_EnableDisable = false;
            $scope.mProspect_Pelanggan_NoTeleponInstitusi_EnableDisable = false;
            $scope.mProspect_Pelanggan_NoHandphonePIC_EnableDisable = false;
            $scope.mProspect_Pelanggan_EmailPIC_EnableDisable = false;
            $scope.mProspect_Pelanggan_NoTeleponRumah_EnableDisable = false;
            $scope.mProspect_Pelanggan_NoHandphone_EnableDisable = false;
            $scope.mProspect_Pelanggan_Email_EnableDisable = false;
            $scope.mProspect_Pelanggan_AlamatSesuaiIdentitas_EnableDisable = false;
            $scope.mProspect_Pelanggan_Provinsi_EnableDisable = false;
            $scope.mProspect_Pelanggan_KabupatenKota_EnableDisable = false;
            $scope.mProspect_Pelanggan_Kecamatan_EnableDisable = false;
            $scope.mProspect_Pelanggan_Kelurahan_EnableDisable = false;
            $scope.mProspect_Pelanggan_KodePos_EnableDisable = false;
            $scope.mProspect_Pelanggan_TanggalLahir_EnableDisable = false;
            $scope.mProspect_Pelanggan_Hobi_EnableDisable = false;
            $scope.mProspect_Pelanggan_Agama_EnableDisable = false;
            $scope.mProspect_Pelanggan_Suku_EnableDisable = false;
            $scope.mProspect_Pelanggan_Pekerjaan_EnableDisable = false;
            $scope.mProspect_Pelanggan_Pekerjaan_EnableDisable = false;
            $scope.mProspect_Pelanggan_Jabatan_EnableDisable = false;
            $scope.mProspect_Pelanggan_NamaPerusahaan_EnableDisable = false;
            $scope.mProspect_Pelanggan_TeleponKantor_EnableDisable = false;
            $scope.mProspect_Pelanggan_AlamatPerusahaan_EnableDisable = false;
            $scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_ShowHide = true;
            $scope.Pelanggan_InformasiKendaraanLain_Action_ShowHide = false;

            angular.element('.ui.modal.ModalPelanggan').modal('hide');


        };

        $scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_Clicked = function () {
            $scope.ShowPelangganDetail = false;
            $scope.ShowPelangganRiwayat = true;
        };

        $scope.Back_To_Pelanggan_Detail_Clicked = function () {
            $scope.ShowPelangganRiwayat = false;
            $scope.ShowPelangganDetail = true;

        };

        $scope.filterData = {
            defaultFilter: [
                { name: 'Nama', value: 'SuspectName' },
                { name: 'No Handphone', value: 'HP' },
                { name: 'Sumber Data', value: 'DataSourceName' },
				{ name: 'Kategori Data', value: 'Valid' }
            ],
            advancedFilter: [
                { name: 'Nama', value: 'SuspectName', typeMandatory: 1 },
                { name: 'No Handphone', value: 'HP', typeMandatory: 0 },
                { name: 'Sumber Data', value: 'DataSourceName', typeMandatory: 1 },
				{ name: 'Kategori Data', value: 'Valid', typeMandatory: 1 }
            ]
        };


        $scope.showTab = function (tab) {
            $scope.selectTab = tab;

            if ($scope.selectTab == 'Prospect') {
                $scope.ProspectStartDate = null;
                $scope.ProspectEndDate = null;
                $scope.textFilter = '';
                $scope.filterData = {
                    defaultFilter: [
                        { name: 'Nama', value: 'ProspectName' },
                        { name: 'No Handphone', value: 'HP' },
                        { name: 'Model', value: 'ModelType' },
                        { name: 'Kategori Pelanggan', value: 'CategoryProspectName' },
                        { name: 'Klasifikasi Pembelian', value: 'PurchasingClassificationName' }

                    ],
                    advancedFilter: [
                        { name: 'Nama', value: 'SuspectName', typeMandatory: 1 },
                        { name: 'No Handphone', value: 'HP', typeMandatory: 0 },
                        { name: 'Model', value: 'ModelType' },
                        { name: 'Sumber Data', value: 'DataSourceName', typeMandatory: 1 },
                        { name: 'Klasifikasi Pembelian', value: 'PurchasingClassificationName', typeMandatory: 0 }
                    ]
                };
            } else
                if ($scope.selectTab == 'Customer') {
                    $scope.ProspectStartDate = null;
                    $scope.ProspectEndDate = null;
                    $scope.textFilter = '';
                    $scope.filterData = {
                        defaultFilter: [
                            { name: 'Nama', value: 'CustomerName' },
                            { name: 'No Handphone', value: 'HP' }
                            // { name: 'Tanggal SPK Terakhir', value: 'SpkDate' }
                        ],
                        advancedFilter: [
                            { name: 'Nama', value: 'SuspectName', typeMandatory: 1 },
                            { name: 'No Handphone', value: 'HP', typeMandatory: 0 }
                            // { name: 'Tanggal SPK Terakhir', value: 'SpkDate', typeMandatory: 1 }
                        ]
                    };
                } else {
                    $scope.ProspectStartDate = null;
                    $scope.ProspectEndDate = null;
                    //suspect
                    $scope.textFilter = '';
                    $scope.filterData = {
                        defaultFilter: [
                            
                            { name: 'Nama', value: 'SuspectName' },
                            { name: 'No Handphone', value: 'HP' },
                            { name: 'Sumber Data', value: 'DataSourceName' },
                            { name: 'Invalid Data', value: 'Valid' },
                            { name: 'Drop', value: 'ProspectDrop' }
                            // { name: 'Kategori Data', value: 'Valid' }
                        ],
                        advancedFilter: [
                            { name: 'Nama', value: 'SuspectName', typeMandatory: 1 },
                            { name: 'No Handphone', value: 'HP', typeMandatory: 0 },
                            { name: 'Sumber Data', value: 'DataSourceName', typeMandatory: 1 },
                            { name: 'Invalid Data', value: 'Valid', typeMandatory: 0 },
                            { name: 'Drop', value: 'ProspectDrop', typeMandatory: 0 }
                            // { name: 'Kategori Data', value: 'Valid', typeMandatory: 1 }
                        ]
                    };

                }
        }

        

        $scope.CreateSpkAkalAkalan = function () {

            console.log('selectedFilter ===>', $scope.selectedFilter);
            if ($scope.selectedFilter == "ModelType") {
                $scope.showModelInput = true;
                $scope.showCustomerInput = false;
                console.log('pilih model ')
                console.log('$scope.ProspectModelSearch ===>', $scope.ProspectModelSearch);
                $scope.disableFilter = false;
                
            } else if ($scope.selectedFilter == "Valid") {
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
                console.log('pilih valid');
                $scope.ProspectModelSearch = null;
                $scope.disableFilter = true;
                $scope.ProspectingFilterKategoriData = 0;
                $scope.textFilter = null;
                $scope.ProspectStartDate = null;
                $scope.ProspectEndDate = null;
                
            } else if ($scope.selectedFilter == "ProspectDrop") {
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
                console.log('pilih drop');
                $scope.ProspectModelSearch = null;
                $scope.disableFilter = true;
                $scope.textFilter = null;
                $scope.ProspectingDropValue = 1;
                $scope.ProspectStartDate = null;
                $scope.ProspectEndDate = null;
                
            }else {
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
                console.log('pilih selain model');
                $scope.ProspectModelSearch = null;
                $scope.disableFilter = false;

            }
    
        }

        




        $scope.ControlLoadMore = 'NoSearch';
        $scope.filterSearch = function () {
            $scope.ControlLoadMore = 'Search';
			
			// if (($scope.selectedFilter!="Valid"&& $scope.selectTab != 'Prospect' &&($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '')) ||
                // ($scope.selectedFilter!="Valid"&& $scope.selectTab != 'Prospect' &&($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == '')))


            if (($scope.selectedFilter!="Valid"&& $scope.selectTab != 'Suspect'&& $scope.selectTab != 'Customer'&& $scope.selectTab != 'Prospect' &&($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '')) ||
                ($scope.selectedFilter!="Valid"&& $scope.selectTab != 'Suspect'&& $scope.selectTab != 'Customer'&& $scope.selectTab != 'Prospect' &&($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == ''))) {
                $scope.loading = false;
                bsNotify.show({
                    //size: 'big',
                    type: 'warning',
                    //timeout: 2000,
                    title: "Peringatan",
                    content: "Harap pilih kategori dan masukkan filter"
                });
            } else {
                $scope.AdvSearchStatus = false;
                $scope.loading = true;
                $scope.startGet = 1;
                //$scope.limit = $scope.pages.pageselected;
                $scope.loadMoreStatus = 2;
                $scope.bind_data.data = [];
                var limit = "&limit=15";
                var filter = "";

                if ($scope.AdvSearchStatus == true) {
                    console.log('$scope.AdvSearchStatus true ====>', $scope.AdvSearchStatus);

                        filter = "/?start=" + $scope.startGet + "" + limit + "" + $scope.filterParamAdvTemp + "&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter;
                    
                } else if ($scope.AdvSearchStatus == false) {
                    console.log('$scope.AdvSearchStatus false ====>', $scope.AdvSearchStatus);

                    $scope.filterParamSearchTemp = "";
                    for (var i = 0; i < $scope.filterData.defaultFilter.length; i++) {

                        var temp = $scope.filterData.advancedFilter[i];
                        // var tempdata = $scope.bind_data.filter[temp.value];


                    }
					
					if($scope.selectTab == 'Prospect')
					{
						filter = "/?start=" + $scope.startGet + "" + limit+"&Id=1";
					}
					else
					{
						filter = "/?start=" + $scope.startGet + "" + limit;
					}

					// if ($scope.selectTab == 'Suspect')
					// {
						// if($scope.ProspectingFilterKategoriData==false)
						// {
							// filter+="&Valid=0";
						// }
						// else if($scope.ProspectingFilterKategoriData==true)
						// {
							// filter+="&Valid=1";
						// }
					// }
					
                    if ($scope.selectTab == 'Prospect' )
					{   
                        

                        console.log('ini mode prospek dengan model & tanggal');

                        // if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '') ||
                        //     ($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == '')) {


                        //     filter = "/?start=" + $scope.startGet + "" + limit + "&filterData= null";

                        // }



						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
                            
							filter+="&ProspectStartDate="+fixDate($scope.ProspectStartDate)+"&ProspectEndDate="+fixDate($scope.ProspectEndDate);
						}
						
						
                        if ($scope.ProspectModelSearch != null || (typeof $scope.ProspectModelSearch != "undefined") && $scope.selectedFilter == "ModelType")
						{
                            
							
                            filter += "&VehicleModelId=" + $scope.ProspectModelSearch.VehicleModelId;
                            
						}
					}
					
                    if ($scope.selectTab == 'Customer')
					{   

                        console.log('ini mode pelanggan dengan model & tanggal');

						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							filter+="&SpkStartDate="+fixDate($scope.ProspectStartDate)+"&SpkEndDate="+fixDate($scope.ProspectEndDate);
						}
						
						
						// if($scope.ProspectModelSearch!=null || (typeof $scope.ProspectModelSearch!="undefined"))
						// {
							
                        //     filter+="&VehicleModelId="+$scope.ProspectModelSearch.VehicleModelId;
                            
						// }
					}
					
                    if ($scope.selectTab == 'Suspect')
					{   

                        console.log('ini mode pelanggan dengan model & tanggal');

						if(($scope.ProspectStartDate!=null && $scope.ProspectEndDate!=null) || ((typeof $scope.ProspectStartDate!="undefined")&&(typeof $scope.ProspectEndDate!="undefined")))
						{
							filter+="&AssignStartDate="+fixDate($scope.ProspectStartDate)+"&AssignEndDate="+fixDate($scope.ProspectEndDate);
						}

					}
					
					if($scope.selectTab == 'Suspect' && $scope.selectedFilter=="Valid")
					{
						filter +="&filterData=" + $scope.selectedFilter + "|" + $scope.ProspectingFilterKategoriData;
                    }
                    
                    else if ($scope.selectTab == 'Suspect' && $scope.selectedFilter == "ProspectDrop")
                    {
                        filter += "&filterData=" + $scope.selectedFilter + "|" + $scope.ProspectingDropValue;
                    }
					else{   
                        if (($scope.selectedFilter == null || $scope.selectedFilter == undefined || $scope.selectedFilter == '') ||
                            ($scope.textFilter == null || $scope.textFilter == undefined || $scope.textFilter == '')) {

                            console.log('ini kalo filter atasnya kosong');
                            filter += "&filterData=null";
                                
                        }else{
                            console.log('ini kalo filternya ke isi ===> suspect');
                            if ($scope.selectedFilter == "ModelType") {
                                filter += "&filterData=null";
                            }else{
                                filter += "&filterData=" + $scope.selectedFilter + "|" + $scope.textFilter;
                            }
                            
                        }
					}
					
					
                }


                if ($scope.selectTab == 'Suspect') {
                    SuspectFactory.getData(filter)
                        .then(
                        function (res) {
                            $scope.loading = false;

                            $scope.List_Suspect_To_Repeat = res.data.Result;
                            $scope.TotalSuspect = res.data.Total;
                            // $scope.total = res.data.Total;
                            // if ($scope.total <= 0){
                            //     $scope.isEmpty = true;
                            //      $scope.now = 0;
                            // }else{
                            //     $scope.isEmpty = false;
                            // }
                        },
                        function (err) {

                        }
                        );
                } else
                    if ($scope.selectTab == 'Prospect') {
						
						
                        //if($scope.selectedFilter == "ProspectDate" && document.getElementById("Prospect_SearchByDate").value!=null && document.getElementById("Prospect_SearchByDate").value!="")
						//{
						//	filter = "/?start=" + $scope.startGet + "" + limit + "&filterData=" + $scope.selectedFilter + "|" + document.getElementById("Prospect_SearchByDate").value;
						//}
						//else if($scope.selectedFilter == "ProspectDate" && document.getElementById("Prospect_SearchByDate").value==null)
						//{
						//	$scope.loading = false;
						//	bsNotify.show({
						//		//size: 'big',
						//		type: 'warning',
						//		//timeout: 2000,
						//		title: "Peringatan",
						//		content: "Harap pilih kategori dan masukkan filter"
						//	});
						//}
						ProspectFactory.getData(filter)
                            .then(
                            function (res) {
                                $scope.loading = false;

                                $scope.List_Prospect_To_Repeat = res.data.Result;
                                $scope.TotalProspect = res.data.Total;
                                // $scope.total = res.data.Total;
                                // if ($scope.total <= 0){
                                //     $scope.isEmpty = true;
                                //      $scope.now = 0;
                                // }else{
                                //     $scope.isEmpty = false;
                                // }
                            },
                            function (err) {

                            }
                            );
                    } else {
                        PelangganFactory.getData(filter)
                            .then(
                            function (res) {
                                $scope.loading = false;

                                $scope.List_Pelanggan_To_Repeat = res.data.Result;
                                $scope.TotalCustomer = res.data.Total;
                                // $scope.total = res.data.Total;
                                // if ($scope.total <= 0){
                                //     $scope.isEmpty = true;
                                //      $scope.now = 0;
                                // }else{
                                //     $scope.isEmpty = false;
                                // }
                            },
                            function (err) {

                            }
                            );

                    }


            }
        }

        $scope.refreshFilter = function () {
            $scope.SaveAllOfflineProspect();
            console.log('$scope.selectTab',$scope.selectTab);
            $scope.showTab ($scope.selectTab);
            //$scope.textFilter = '';
            $scope.ControlLoadMore = 'NoSearch';
            SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Suspect_To_Repeat = res.data.Result;
                return $scope.List_Suspect_To_Repeat;
            });

            //$scope.List_Pelanggan_To_Repeat=PelangganFactory.getData();
            PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Pelanggan_To_Repeat = res.data.Result;
                return $scope.List_Pelanggan_To_Repeat;
            });

            //$scope.List_Prospect_To_Repeat=ProspectFactory.getData();
            ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Prospect_To_Repeat = res.data.Result;
                //return $scope.List_Prospect_To_Repeat;
            });
            // $scope.selectTab = tab;
            //console.log('$scope.selectTab',$scope.selectTab);
			$scope.ProspectModelSearch=null;
			$scope.ProspectStartDate=null;
            $scope.ProspectEndDate=null;
            $scope.selectedFilter = null
            $scope.showCustomerInput = true;
            $scope.showModelInput = false;
            $scope.disableFilter = false;
        }

        $scope.disabledSuspect = true;
        $scope.ShowTelp = false;
        $scope.ShowDisTelp = true;
        $scope.SelectSuspect = function (SelectedData) {

            $scope.From_Page_Prospect_Or_Not = "Not_Prospect";
            $scope.ShowSuspectDetail = true;
            $scope.formData = false;
            $scope.disabledSuspect = true;

            angular.element('.ui.modal.ModalSuspect').modal('hide');
            $scope.ClearPelanggan();



            var SuspectKategoriPelanggan_SelectedIndex = 0;
            for (var i = 0; i < $scope.optionsPurchasePurpose.length; ++i) {
                if ($scope.optionsPurchasePurpose[i].PurchasePurposeId == SelectedData.PurchasePurposeId) {
                    SuspectKategoriPelanggan_SelectedIndex = i;
                }
            }


            if ($scope.optionsPurchasePurpose[SuspectKategoriPelanggan_SelectedIndex].PurchasePurposeName == "Pribadi") {
                $scope.SuspectIndividu = true;
                $scope.SuspectInstansi = false;
            } else {
                $scope.SuspectIndividu = false;
                $scope.SuspectInstansi = true;
            }


            $scope.SelectedSuspect = angular.copy(SelectedData);


        };
        $scope.simpanSuspectToProspect = function (SuspectData) {

            $scope.controlData = 'SimpanSuspect';
            $scope.controlLihat = 'Buat';
            $scope.ShowPengecekanData = true;

            $scope.flagCustType = {};
            $scope.flagCustType.CustomerTypeId = null;
            $scope.flagCustType.CustomerTypeId = SuspectData.CustomerTypeId;
            var urlCekData = "/?start=1&limit=100&ProspectName=" + SuspectData.SuspectName + "&Handphone=" + SuspectData.HP;

            ProspectPengecekanDataFactory.getData(urlCekData).then(function (res) {
                $scope.List_Would_Be_Prospect_To_Repeat = res.data.Result;
            });

            angular.element('.ui.modal.ModalSuspect').modal('hide');
            angular.element('.ui.modal.ModalPelanggan').modal('hide');
            $scope.mProspect_PengecekanData = angular.copy(SuspectData);
            $scope.ShowSuspectDetail = false;
            $scope.formData = false;
        };

        $scope.SaveCustomer = function () {
            SuspectFactory.createCustomerToProspect($scope.Pelanggan_Selected).then(
                function (res) {
                    SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                        $scope.List_Suspect_To_Repeat = res.data.Result;
                    })

                    //return $scope.List_Suspect_To_Repeat;
                    ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                        $scope.List_Prospect_To_Repeat = res.data.Result;
                        //return $scope.List_Prospect_To_Repeat;
                    })
                    PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                        $scope.List_Pelanggan_To_Repeat = res.data.Result;
                        //return $scope.List_Prospect_To_Repeat;
                    })

                    bsNotify.show({
                        title: "Sukses",
                        content: "Data prospect berhasil disimpan",
                        type: 'success'
                    });

                },
                function (err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: "Input data prospect gagal.",
                        type: 'danger'
                    });
                }
            )


            angular.element('.ui.modal.ModalSuspect').modal('hide');
            angular.element('.ui.modal.ModalPelanggan').modal('hide');
            angular.element('.ui.modal.CekCustomer').modal('hide');
            $scope.ShowSuspectDetail = false;
            $scope.ShowPelangganDetail = false;
            $scope.formData = true;
        }

        $scope.CancelSaveCustomer = function () {
            angular.element('.ui.modal.CekCustomer').modal('hide');
            $scope.ShowSuspectDetail = false;
            $scope.ShowPelangganDetail = false;
            $scope.formData = true;
        }
        
        $scope.simpanCustomerToProspect = function () {
            angular.element('.ui.modal.ModalSuspect').modal('hide');
            angular.element('.ui.modal.ModalPelanggan').modal('hide');
            PelangganFactory.cekCustomerToProspect($scope.Pelanggan_Selected.CustomerId).then(
                function (res) {
                    $scope.CekCustomer = res.data.Result[0];

                    if($scope.CekCustomer.ProspectExistBit == true){
                        setTimeout(function () {
                            angular.element('.ui.modal.CekCustomer').modal('refresh');
                        }, 0);
                        angular.element('.ui.modal.CekCustomer').modal('show');
                    }else{
                        $scope.SaveCustomer ();
                    }
                },
                function (err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: err.data.Message,
                        type: 'danger'
                    });
                }
            )
        };
		
		$scope.ProspectingMauAkftifkanSuspect = function () {
			angular.element('.ui.modal.ModalProspectAktifkanDataSuspect').modal('show');
            angular.element('.ui.modal.ModalProspectAktifkanDataSuspect').not(':first').remove();
		}
		
		$scope.ProspectingMauNonAkftifkanSuspect = function () {
			angular.element('.ui.modal.ModalProspectNonAktifkanDataSuspect').modal('show');
            angular.element('.ui.modal.ModalProspectNonAktifkanDataSuspect').not(':first').remove();
		}
		
		$scope.ProspectAktifkanDataSuspect = function () {
			$scope.Prospect_mau_diaktifin = {};
			$scope.Prospect_mau_diaktifin.Valid = 1;
			$scope.Prospect_mau_diaktifin.SuspectId = $scope.Suspect_Selected_Data.SuspectId;
			SuspectFactory.AktifkanSuspect($scope.Prospect_mau_diaktifin).then(function () {
                    $scope.refreshFilter();
					angular.element('.ui.modal.ModalProspectAktifkanDataSuspect').modal('hide');
					bsNotify.show({
                        title: "Berhasil",
                        content: "Data berhasil diaktifkan.",
                        type: 'success'
                    });
					$scope.BackToDatabase();
                },
                    function(err) {
						bsNotify.show({
									title: "Gagal",
									content: "Data gagal diaktifkan.",
									type: 'danger'
								});
                        
                    });
		}
		
		$scope.ProspectNonAktifkanDataSuspect = function () {
			$scope.Prospect_mau_dinonaktifin = {};
			$scope.Prospect_mau_dinonaktifin.Valid = 0;
			$scope.Prospect_mau_dinonaktifin.SuspectId = $scope.Suspect_Selected_Data.SuspectId;
			SuspectFactory.AktifkanSuspect($scope.Prospect_mau_dinonaktifin).then(function () {
                    $scope.refreshFilter();
					angular.element('.ui.modal.ModalProspectNonAktifkanDataSuspect').modal('hide');
					bsNotify.show({
                        title: "Berhasil",
                        content: "Data berhasil dinonaktifkan.",
                        type: 'success'
                    });
					$scope.BackToDatabase();
                },
                    function(err) {
						bsNotify.show({
									title: "Gagal",
									content: "Data gagal dinonaktifkan.",
									type: 'danger'
								});
                        
                    });
		}
		
		$scope.KembaliProspectAktifkanDataSuspect = function () {
			angular.element('.ui.modal.ModalProspectAktifkanDataSuspect').modal('hide');
		}
		
		$scope.KembaliProspectNonAktifkanDataSuspect = function () {
			angular.element('.ui.modal.ModalProspectNonAktifkanDataSuspect').modal('hide');
		}

        $scope.BackToDatabase = function () {
            $scope.ShowSuspectDetail = false;
            $scope.ShowPelangganDetail = false;
            $scope.ShowProspectDetail = false;
            $scope.ShowBuatProspect = false;
            $scope.formData = true;

            //ini clear suspect
            $scope.ClearSuspect();

            //ini clear pelanggan
            $scope.ClearPelanggan();

            //ini clear buat prospect	
            $scope.ClearBuatProspect();

            //ini clear pelanggan punya info lain
            $scope.List_Pelanggan_InformasiKendaraanLain_To_Repeat.length = 0;

            //ini clear prospect


        };

        $scope.controlLihat = 'Buat';
        $scope.BackToListDatabase = function () {
            if ($scope.controlLihat == 'Buat') {
                angular.element('.ui.modal.ModalBackToDatabase').modal('show');
            } else {
                $scope.Prospect_Toggle_Edit_Detail_Prospect();
                $scope.ProspectEditButtonToggle = true;
                $scope.KategoriKeyFailsafe = false;
                $scope.ShowDisTelp = true;
                $scope.ShowTelp = false;
                $scope.mProspect_Prospect_KategoriPelanggan_EnableDisable = false;
                //$scope.ShowProspectDetail = false;
                $scope.formData = false;
            }
        }

        $scope.BackToRealListDatabase = function() {
            // if ($scope.controlLihat == 'Buat') {
            //     angular.element('.ui.modal.ModalBackToDatabase').modal('show');
            // } else {
                $scope.Prospect_Toggle_Edit_Detail_Prospect();
                $scope.ProspectEditButtonToggle = false;
                $scope.KategoriKeyFailsafe = false;
                $scope.ShowDisTelp = false;
                $scope.ShowTelp = false;
                $scope.ShowProspectDetail = false;
                $scope.formData = true;
            //}
        }

        $scope.ClearBuatProspect = function () {
            $scope.mProspect_BuatProspect_Nama = "";
            $scope.mProspect_BuatProspect_NoTelepon = "";
            $scope.mProspect_BuatProspect_NoHandphone = "";
            $scope.mProspect_BuatProspect_Email = "";
            $scope.mProspect_BuatProspect_LokasiOrEvent = "";


        };

        $scope.ClearSuspect = function () {

            $scope.SelectedSuspect = "";
        };

        $scope.ClearPelanggan = function () {

            $scope.mProspect_Pelanggan = "";
        };

        $scope.ToBuatProspectNotFromProspectPage = function (SelectedData, FromPage, FromDatabase) {




            $scope.mProspect_BuatProspect_Provinsi = $scope.optionsProvinsi[0];
            $scope.mProspect_PengecekanDataBaru_Provinsi = $scope.optionsProvinsi[0];



            if (FromPage == "Suspect") {
                //yang ini biar di bagian buat prospek nongol yg bisa nongol
                $scope.From_Page = FromPage;
                $scope.SelectSuspect(SelectedData);

                SuspectFactory.updateSuspectToClose($scope.SelectedSuspect).then(function () {
                    //yang di dalem ini tu buat di tabel doang
                });


                $scope.mProspect_BuatProspect = angular.copy($scope.SelectedSuspect);
                //ini dibawah override khusus buat suspect name
                $scope.mProspect_BuatProspect['Nama_Lengkap'] = angular.copy($scope.SelectedSuspect['SuspectName']);
                //$scope.mProspect_BuatProspect['Nama_Lengkap']=angular.copy($scope.SelectedSuspect['DataSourceId']);

                //ini clear soal di form sebelom ga kepake lagi
                $scope.SelectedSuspect = "";
            } else if (FromPage == "Pelanggan") {

                $scope.From_Page = FromPage;
                $scope.SelectPelanggan(SelectedData);
                $scope.mProspect_BuatProspect = angular.copy($scope.Pelanggan_Selected);
                $scope.mProspect_BuatProspect.No_Telepon = angular.copy($scope.Pelanggan_Selected.No_Telepon_Rumah);
                $scope.mProspect_BuatProspect.Alamat = angular.copy($scope.Pelanggan_Selected.Alamat_Sesuai_Identitas);


            }

            if ($scope.mProspect_BuatProspect.InstanceName == "") {
                $scope.BuatProspectIndividu = true;
                $scope.BuatProspectInstansi = false;
            } else if ($scope.mProspect_BuatProspect.InstanceName != "") {
                $scope.BuatProspectIndividu = false;
                $scope.BuatProspectInstansi = true;
            }

            if (FromDatabase == "Yes") {
                $scope.From_Page_Prospect_Or_Not = "Not_Prospect";
                $scope.formData = false;
                $scope.ShowSuspectDetail = false;
                $scope.ShowPelangganDetail = false;
                $scope.ShowBuatProspect = true;
                $scope.ModalSuspect = { show: false };
                $scope.ModalPelanggan = { show: false };
            } else if (FromDatabase == "No") {
                $scope.ShowSuspectDetail = false;
                $scope.ShowPelangganDetail = false;
                $scope.ShowBuatProspect = true;
            }


        };

        $scope.Ya_HarusDiisi = function () {
            $scope.ModalDataHarusDiisi = { show: false };
        }

        $scope.ToPengecekanData = function (cekdata) {
            if ($scope.flagCustType.CustomerTypeId == 1) {

                if (typeof $scope.mProspect_BuatProspect.ProspectName == "undefined" || typeof $scope.mProspect_BuatProspect.HP == "undefined") {

                    bsNotify.show({
                        title: "Peringatan",
                        content: "Data mandatory harus diisi.",
                        type: 'warning'
                    });
                } else {

                    $scope.ShowBuatProspect = false;
                    $scope.ShowPengecekanData = true;
                    $scope.mProspect_PengecekanData = angular.copy($scope.mProspect_BuatProspect);
                    $scope.mProspect_PengecekanData.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
                    $scope.mProspect_PengecekanData.CustomerTypeDesc = "Individu";


                    var urlCekData = "/?start=1&limit=100&ProspectName=" + cekdata.ProspectName + "&Handphone=" + cekdata.HP;

                    ProspectPengecekanDataFactory.getData(urlCekData).then(function (res) {
                        $scope.List_Would_Be_Prospect_To_Repeat = res.data.Result;
                    });

                }

            } else {


                if (typeof $scope.mProspect_BuatProspect.InstanceName == "undefined" || typeof $scope.mProspect_BuatProspect.HP == "undefined") {

                    bsNotify.show({
                        title: "Peringatan",
                        content: "Data mandatory harus diisi.",
                        type: 'warning'
                    });
                } else {

                    $scope.ShowBuatProspect = false;
                    $scope.ShowPengecekanData = true;
                    $scope.mProspect_PengecekanData = angular.copy($scope.mProspect_BuatProspect);


                    var urlCekData = "/?start=1&limit=100&ProspectName=" + cekdata.ProspectName + "&Handphone=" + cekdata.HP;

                    ProspectPengecekanDataFactory.getData(urlCekData).then(function (res) {
                        $scope.List_Would_Be_Prospect_To_Repeat = res.data.Result;
                    });

                }

            }
        };

        $scope.BatalPengecekan = function () {

            if ($scope.controlData == 'SimpanProspect') {
                $scope.mProspect_PengecekanData = "";


                $scope.ShowBuatProspect = true;
                $scope.ShowPengecekanData = false;
            } else {
                $scope.ShowPengecekanData = false;
                $scope.ShowSuspectDetail = false;
                $scope.ShowPelangganDetail = false;
                $scope.ShowProspectDetail = false;
                $scope.ShowBuatProspect = false;
                $scope.formData = true;
            }

        };

        $scope.clearAllComboBox = function () {
            $scope.selectedProvince = {};
            $scope.selectedSumberData = {};
            $scope.selectedCategoryPelanggan = {};
            $scope.selectedCategoryFleet = {};
            $scope.selectedProvince = {};
            $scope.selectedCityRegency = {};
            $scope.selectedDistrict = {};
            $scope.selectedVillage = {};
            $scope.selectedPostalCode = {};
            $scope.selectAgama = {};
            $scope.selectedSuku = {};
            $scope.selectedJabatan = {};
            $scope.selectedModel = {};
            $scope.selectedTipe = {};
            $scope.selectedwarna = {};
            $scope.selectedTahun = {};
            $scope.selectedFundSource = {};
            $scope.selectedKebutuhan = {};
            $scope.selectedKategori = {};
            $scope.selectedPurchasePurpose = {};
            $scope.selectedMerek = {};
            $scope.selectedKlasifikasiPembelian.PurchasingClassificationId = {};


        }

        $scope.closeModalModalPengecekanDataUntukProspect = function () {
            angular.element('.ui.modal.ModalPengecekanDataUntukProspect').modal('hide');  
        }


        $scope.BuatProspectDataBaru = function (buatData) {
            $scope.Back_pengecekan = 'Pengecekan';
            $scope.mProspect_Prospect_TradeIn_EnableDisable = true;
            $scope.selectedCategoryFleet = {};
            $scope.selectedCategoryFleet.CategoryFleet = {};
            $scope.selectedCategoryFleet.CategoryFleet = $scope.optionsKategoriFleet[1];
            $scope.ProspectEditButtonToggle = false;


            if (typeof $scope.lanjutProspect == "undefined") {
                $scope.lanjutProspect = {};
                $scope.lanjutProspect.CustomerTypeId = null;
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;

            } else {
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
            }
            $scope.user = CurrentUser.user();
            $scope.DataBaru = buatData;
            if (angular.isUndefined($scope.mProspect_PengecekanData.InstanceName) == true) {
                //kalo individu
                $scope.mProspect_PengecekanData.InstanceName = "";
                $scope.mProspect_PengecekanDataBaru = angular.copy($scope.mProspect_PengecekanData);

                var statusProspect = 'Prospect';


                $scope.mProspect_PengecekanDataBaru.StatusProspect = statusProspect;
                $scope.mProspect_PengecekanDataBaru.Sales_Force = $scope.user.EmployeeName;

                $scope.cekDataStatus = true;
            } else if (angular.isUndefined($scope.mProspect_PengecekanData.InstanceName) == false) {
                //kalo instansi
                $scope.mProspect_PengecekanDataBaru = angular.copy($scope.mProspect_PengecekanData);

                var statusProspect = 'Prospect';
                $scope.mProspect_PengecekanDataBaru.StatusProspect = statusProspect;
                $scope.mProspect_PengecekanDataBaru.Sales_Force = $scope.user.EmployeeName;


                $scope.cekDataStatus = true;
            }


            $scope.mProspect_PengecekanDataBaru.Status = $scope.From_Page;
            //$scope.mProspect_PengecekanDataBaru.Sales_Force = "ga ada";

            $scope.ShowPengecekanData = false;
            //$scope.ShowLihatPengecekanData = true;
            $scope.ToProspectDetailNotFromProspect();
            $scope.TabInfoLain = false;
            // $scope.ModalPengecekanDataUntukProspect = { show: false };
            angular.element('.ui.modal.ModalPengecekanDataUntukProspect').modal('hide');

        };

        $scope.PengecekanDataUntukProspectModal = function (SelectedWouldBe) {
            $scope.WouldBeProspectSelected = SelectedWouldBe;
            // $scope.ModalPengecekanDataUntukProspect = { show: true };
            angular.element('.ui.modal.ModalPengecekanDataUntukProspect').modal('show');

        };


        $scope.disabledLihatDetil = true;
        $scope.mProspect_Prospect_Code_EnableDisable = true;
        $scope.LihatDataWouldBeProspecting = function (SelectedWouldBe) {

            console.log('LihatDataWouldBeProspecting | gunakan data untuk prospect')
            console.log('LihatDataWouldBeProspecting | CustomerTypeId ',SelectedWouldBe.CustomerTypeId);

            $scope.flagCustType = { CustomerTypeId: null }
            if(SelectedWouldBe.CustomerTypeId == 1){
                $scope.flagCustType.CustomerTypeId = 1;
                console.log('LihatDataWouldBeProspecting | individu ===> ',$scope.flagCustType.CustomerTypeId)
            }else{
                $scope.flagCustType.CustomerTypeId = 2;
                console.log('LihatDataWouldBeProspecting | instansi ===> ',$scope.flagCustType.CustomerTypeId)
            }


            console.log('aaaaaaaaaaaaaa',SelectedWouldBe);

            if (typeof $scope.lanjutProspect == "undefined") {
                $scope.lanjutProspect = {};
                $scope.lanjutProspect.CustomerTypeId = null;
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;


            } else {
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
            }

            KategoriPelangganFactory.getData().then(function (res) {
                $scope.optionsKategoriPelanggan = res.data.Result;
                $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
            });



            $scope.mProspect_PengecekanDataBaru = angular.copy(SelectedWouldBe);
            $scope.mProspect_PengecekanDataBaru['Nama_Lengkap'] = angular.copy(SelectedWouldBe['Name']);
            //urus status

            if (SelectedWouldBe.CustomerId != null) {
                $scope.mProspect_PengecekanDataBaru['Status'] = "Pelanggan";
            } else if (SelectedWouldBe.CustomerId == null) {
                $scope.mProspect_PengecekanDataBaru['Status'] = "Prospect";
            }

            if (SelectedWouldBe.InstanceName == "") {
                $scope.BuatProspectIndividu = true;
                $scope.BuatProspectInstansi = false;
            } else if (SelectedWouldBe.InstanceName != "") {
                $scope.BuatProspectIndividu = false;
                $scope.BuatProspectInstansi = true;
            }

            $scope.ShowPengecekanData = false;
            $scope.ShowLihatPengecekanData = true;
            // $scope.ModalPengecekanDataUntukProspect = { show: false };
            angular.element('.ui.modal.ModalPengecekanDataUntukProspect').modal('hide');
            $scope.disabledLihatDetil = true;
            $scope.mProspect_Prospect_ToyotaId_EnableDisable = true;
            $scope.mProspect_Prospect_Code_EnableDisable = true;
            $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;

        };

        $scope.ExpressLihatDataWouldBeProspecting = function (SelectedWouldBe) {
            $scope.controlData = 'SimpanProspect';
            if (typeof $scope.lanjutProspect == "undefined") {
                $scope.lanjutProspect = {};
                $scope.lanjutProspect.CustomerTypeId = null;
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;


            } else {
                $scope.lanjutProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
            }

            $scope.LihatDataWouldBeProspecting(SelectedWouldBe); //ini mungkin ga beres ati ati
            $scope.ToProspectDetailNotFromProspect();
            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = [];
            $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;

            $scope.selectedProvince = {};
            $scope.selectedProvince.Province = {};
            for (var i = 0; i < $scope.province.length; ++i) {
                if ($scope.province[i].ProvinceId == SelectedWouldBe.ProvinceId) {
                    $scope.selectedProvince.Province = $scope.province[i];
                    break;
                }
            }

            $scope.selectedCategoryPelanggan = {};
            $scope.selectedCategoryPelanggan.CustomerType = {};
            for (var i = 0; i < $scope.optionsKategoriPelanggan.length; ++i) {
                if ($scope.optionsKategoriPelanggan[i].CustomerTypeId == SelectedWouldBe.CustomerTypeId) {
                    $scope.selectedCategoryPelanggan.CustomerType = $scope.optionsKategoriPelanggan[i];
                    break;
                }
            }

            $scope.selectedCategoryFleet = {};
            $scope.selectedCategoryFleet.CategoryFleet = {};
            for (var i = 0; i < $scope.optionsKategoriFleet.length; ++i) {
                if ($scope.optionsKategoriFleet[i].CategoryFleetId == SelectedWouldBe.CategoryFleetId) {
                    $scope.selectedCategoryFleet.CategoryFleet = $scope.optionsKategoriFleet[i];
                    break;
                }
            }

            var tampungKab = [{
                CityRegencyName: SelectedWouldBe.CityRegencyName,
                CityRegencyId: SelectedWouldBe.CityRegencyId
            }];


            var tempKab = {};
            tempKab.CityRegencyId = null;
            tempKab.CityRegencyName = null;

            $scope.kabupaten = tempKab;
            $scope.selectedCityRegency = {};
            $scope.selectedCityRegency.CityRegency = {};

            KabupatenKotaFactory.getData('?start=1&limit=100&filterData=ProvinceId|' + SelectedWouldBe.ProvinceId).then(function (res) {
                $scope.tempKabupaten = res.data.Result;
                $scope.kabupaten = $scope.tempKabupaten;
                for (var i = 0; i < $scope.kabupaten.length; ++i) {
                    if ($scope.kabupaten[i].CityRegencyId == SelectedWouldBe.CityRegencyId) {
                        $scope.selectedCityRegency.CityRegency = $scope.kabupaten[i];
                        break;
                    }
                }
            })

            var tampungKec = {};
            tampungKec.DistrictId = null;
            tampungKec.DistrictName = null;

            $scope.kecamatan = tampungKec;
            $scope.selectedDistrict = {};
            $scope.selectedDistrict.DistrictId = {};

            KecamatanFactory.getData('?start=1&limit=100&filterData=CityRegencyId|' + SelectedWouldBe.CityRegencyId).then(function (res) {
                $scope.tempKecamatan = res.data.Result;
                $scope.kecamatan = $scope.tempKecamatan;
                for (var i = 0; i < $scope.kecamatan.length; ++i) {
                    if ($scope.kecamatan[i].DistrictId == SelectedWouldBe.DistrictId) {
                        $scope.selectedDistrict.District = $scope.kecamatan[i];
                        break;
                    }
                }
            })

            var tampungKel = [{
                VillageId: SelectedWouldBe.VillageId,
                VillageName: SelectedWouldBe.VillageName

            }];
            var tempKel = {};
            tempKel.VillageId = null;
            tempKel.VillageName = null;

            $scope.kelurahan = [];

            if (tampungKel[0].VillageId == null) {
                tempKel.VillageId = '';
                tempKel.VillageName = '';
                KelurahanFactory.getData('?start=1&limit=100&filterData=DistrictId|' + SelectedWouldBe.DistrictId).then(function (res) {
                    $scope.tempKelurahan = res.data.Result;

                    $scope.kelurahan = $scope.tempKelurahan;
                })
            } else {
                tempKel.VillageId = tampungKel[0].VillageId;
                tempKel.VillageName = tampungKel[0].VillageName;
                $scope.kelurahan = [];
            }

            //console.log('tempKel',tempKel);
            //console.log('$scope.kelurahan',$scope.kelurahan);
            $scope.kelurahan.push(tempKel);
            $scope.selectedVillage = {};
            $scope.selectedVillage.Village = {};
            for (var i = 0; i < $scope.kelurahan.length; ++i) {
                if ($scope.kelurahan[i].VillageId == SelectedWouldBe.VillageId) {
                    $scope.selectedVillage.Village = $scope.kelurahan[i];
                    break;
                }
            }

            $scope.selectAgama = {};
            $scope.selectAgama.Agama = {};
            for (var i = 0; i < $scope.optionsAgama.length; ++i) {
                if ($scope.optionsAgama[i].CustomerReligionId == SelectedWouldBe.CustomerReligionId) {
                    $scope.selectAgama.Agama = $scope.optionsAgama[i];
                    break;
                }
            }

            $scope.selectedSuku = {};
            $scope.selectedSuku.Suku = {};
            for (var i = 0; i < $scope.optionsSuku.length; ++i) {
                if ($scope.optionsSuku[i].CustomerEthnicId == SelectedWouldBe.CustomerEthnicId) {
                    $scope.selectedSuku.Suku = $scope.optionsSuku[i];
                    break;
                }
            }

            $scope.selectedJabatan = {};
            $scope.selectedJabatan.Jabatan = {};
            for (var i = 0; i < $scope.optionsJabatan.length; ++i) {
                if ($scope.optionsJabatan[i].CustomerPositionId == SelectedWouldBe.CustomerPositionId) {
                    $scope.selectedJabatan.Jabatan = $scope.optionsJabatan[i];
                    break;
                }
            }

            
        };

        $scope.BackToPengecekanData = function () {
            $scope.ShowLihatPengecekanData = false;
            $scope.ShowPengecekanData = true;

            $scope.mProspect_PengecekanDataBaru = "";
        };

        $scope.Pelanggan_HapusInformasiKendaraanLain = function (IndexInfoKendaraanLain, FromTable) {
            $scope.List_Pelanggan_InformasiKendaraanLain_To_Repeat.splice(IndexInfoKendaraanLain, 1);
            $scope.ModalPelanggan_InformasiKendaraanLain_Action = { show: false };
            angular.element('.ui.modal.ModalPelanggan_InformasiKendaraanLain_Action').modal('hide');
        };

        $scope.Pelanggan_Toggle_Edit_Detail_Pelanggan = function () {
            //alert('changes not saved unless you clicked simpan');
            $scope.BatalEditDetailPelanggan_Button = !$scope.BatalEditDetailPelanggan_Button;
            $scope.SimpanEditDetailPelanggan_Button = !$scope.SimpanEditDetailPelanggan_Button;
            $scope.Pelanggan_InformasiKendaraanLain_Add_Vehicle_Button = !$scope.Pelanggan_InformasiKendaraanLain_Add_Vehicle_Button;
            $scope.GunakanUntukProspekDetailPelanggan_Button = !$scope.GunakanUntukProspekDetailPelanggan_Button;
            $scope.EditButtonToggle = false;

            $scope.mProspect_Pelanggan_ToyotaId_EnableDisable = !$scope.mProspect_Pelanggan_ToyotaId_EnableDisable;
            //$scope.mProspect_Pelanggan_KategoriPelanggan_EnableDisable = !$scope.mProspect_Pelanggan_KategoriPelanggan_EnableDisable;
            //$scope.mProspect_Pelanggan_KategoriFleet_EnableDisable = !$scope.mProspect_Pelanggan_KategoriFleet_EnableDisable;
            $scope.mProspect_Pelanggan_NamaLengkap_EnableDisable = !$scope.mProspect_Pelanggan_NamaLengkap_EnableDisable;
            $scope.mProspect_Pelanggan_NamaPanggilan_EnableDisable = !$scope.mProspect_Pelanggan_NamaPanggilan_EnableDisable;
            $scope.mProspect_Pelanggan_NamaInstitusi_EnableDisable = !$scope.mProspect_Pelanggan_NamaInstitusi_EnableDisable;
            $scope.mProspect_Pelanggan_NamaPIC_EnableDisable = !$scope.mProspect_Pelanggan_NamaPIC_EnableDisable;
            $scope.mProspect_Pelanggan_AlamatInstitusi_EnableDisable = !$scope.mProspect_Pelanggan_AlamatInstitusi_EnableDisable;
            $scope.mProspect_Pelanggan_NoTeleponInstitusi_EnableDisable = !$scope.mProspect_Pelanggan_NoTeleponInstitusi_EnableDisable;
            $scope.mProspect_Pelanggan_NoHandphonePIC_EnableDisable = !$scope.mProspect_Pelanggan_NoHandphonePIC_EnableDisable;
            $scope.mProspect_Pelanggan_EmailPIC_EnableDisable = !$scope.mProspect_Pelanggan_EmailPIC_EnableDisable;
            $scope.mProspect_Pelanggan_NoTeleponRumah_EnableDisable = !$scope.mProspect_Pelanggan_NoTeleponRumah_EnableDisable;
            $scope.mProspect_Pelanggan_NoHandphone_EnableDisable = !$scope.mProspect_Pelanggan_NoHandphone_EnableDisable;
            $scope.mProspect_Pelanggan_Email_EnableDisable = !$scope.mProspect_Pelanggan_Email_EnableDisable;
            $scope.mProspect_Pelanggan_AlamatSesuaiIdentitas_EnableDisable = !$scope.mProspect_Pelanggan_AlamatSesuaiIdentitas_EnableDisable;
            $scope.mProspect_Pelanggan_Provinsi_EnableDisable = !$scope.mProspect_Pelanggan_Provinsi_EnableDisable;
            $scope.mProspect_Pelanggan_KabupatenKota_EnableDisable = !$scope.mProspect_Pelanggan_KabupatenKota_EnableDisable;
            $scope.mProspect_Pelanggan_Kecamatan_EnableDisable = !$scope.mProspect_Pelanggan_Kecamatan_EnableDisable;
            $scope.mProspect_Pelanggan_Kelurahan_EnableDisable = !$scope.mProspect_Pelanggan_Kelurahan_EnableDisable;
            $scope.mProspect_Pelanggan_KodePos_EnableDisable = !$scope.mProspect_Pelanggan_KodePos_EnableDisable;
            $scope.mProspect_Pelanggan_TanggalLahir_EnableDisable = !$scope.mProspect_Pelanggan_TanggalLahir_EnableDisable;
            $scope.mProspect_Pelanggan_Hobi_EnableDisable = !$scope.mProspect_Pelanggan_Hobi_EnableDisable;
            $scope.mProspect_Pelanggan_Agama_EnableDisable = !$scope.mProspect_Pelanggan_Agama_EnableDisable;
            $scope.mProspect_Pelanggan_Suku_EnableDisable = !$scope.mProspect_Pelanggan_Suku_EnableDisable;
            $scope.mProspect_Pelanggan_Pekerjaan_EnableDisable = !$scope.mProspect_Pelanggan_Pekerjaan_EnableDisable;
            $scope.mProspect_Pelanggan_Jabatan_EnableDisable = !$scope.mProspect_Pelanggan_Jabatan_EnableDisable;
            $scope.mProspect_Pelanggan_NamaPerusahaan_EnableDisable = !$scope.mProspect_Pelanggan_NamaPerusahaan_EnableDisable;
            $scope.mProspect_Pelanggan_TeleponKantor_EnableDisable = !$scope.mProspect_Pelanggan_TeleponKantor_EnableDisable;
            $scope.mProspect_Pelanggan_AlamatPerusahaan_EnableDisable = !$scope.mProspect_Pelanggan_AlamatPerusahaan_EnableDisable;
            $scope.mProspect_Pelanggan_RT_EnableDisable = !$scope.mProspect_Pelanggan_RT_EnableDisable;
            $scope.mProspect_Pelanggan_RW_EnableDisable = !$scope.mProspect_Pelanggan_RW_EnableDisable; //fitri
            $scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_ShowHide = !$scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_ShowHide;
            $scope.Pelanggan_InformasiKendaraanLain_Action_ShowHide = !$scope.Pelanggan_InformasiKendaraanLain_Action_ShowHide;
            $scope.ShowDisTelp = false;
            $scope.ShowTelp = true;
        };

        $scope.Prospect_Toggle_Edit_Detail_Prospect = function () {



            console.log('$scope.mProspect_Prospect ===>', $scope.mProspect_Prospect);

            
            $scope.listUnitYangDiminati = angular.copy($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat);
            console.log('ini listUnitYangDiminati dari get"an prospect ===>', $scope.listUnitYangDiminati);

            if ($scope.selectTab != 'Prospect' && $scope.controlData == 'UpdateProspect'){
                if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                    $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                    $scope.listUnitYangDiminati[0].SuffixCode == null ||
                    $scope.listUnitYangDiminati[0].ColorName == null ||
                    $scope.listUnitYangDiminati[0].Qty == null) {

                    $scope.mProspect_Prospect.CategoryProspectId = 1; //hot

                } else if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                    $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                    $scope.listUnitYangDiminati[0].SuffixCode == null) {

                    $scope.mProspect_Prospect.CategoryProspectId = 2; //medium

                } else if ($scope.listUnitYangDiminati[0].VehicleModelName == null) {

                    $scope.mProspect_Prospect.CategoryProspectId = 3; //low
                }

                $scope.kategoriValue($scope.mProspect_Prospect.CategoryProspectId);
            }

            $scope.controlData = 'UpdateProspect';
            $scope.mProspect_Prospect['BeforeCategoryProspectId'] = angular.copy($scope.mProspect_Prospect['CategoryProspectId']);

            $scope.BatalEditDetailProspect_Button = !$scope.BatalEditDetailProspect_Button;
            $scope.SimpanEditDetailProspect_Button = !$scope.SimpanEditDetailProspect_Button;
            $scope.Prospect_InformasiUnitYangDiminati_Tambah_Kendaraan_Button = !$scope.Prospect_InformasiUnitYangDiminati_Tambah_Kendaraan_Button;
            $scope.Prospect_InformasiKendaraanLain_Add_Vehicle_Button = !$scope.Prospect_InformasiKendaraanLain_Add_Vehicle_Button;
            $scope.Prospect_ActionFooter_Button = !$scope.Prospect_ActionFooter_Button;
            $scope.ProspectEditButtonToggle = false;
            $scope.KategoriKeyFailsafe = true;
            //alert('changes not saved unless you clicked simpan');
            $scope.Prospect_InformasiUnitYangDiminati_Action_ShowHide = !$scope.Prospect_InformasiUnitYangDiminati_Action_ShowHide;
            $scope.mProspect_Prospect_ToyotaId_EnableDisable = !$scope.mProspect_Prospect_ToyotaId_EnableDisable;
            $scope.mProspect_Prospect_KategoriPelanggan_EnableDisable = !$scope.mProspect_Pelanggan_KategoriPelanggan_EnableDisable;
            //$scope.mProspect_Prospect_KategoriFleet_EnableDisable = !$scope.mProspect_Prospect_KategoriFleet_EnableDisable;
            $scope.mProspect_Prospect_NamaLengkap_EnableDisable = !$scope.mProspect_Prospect_NamaLengkap_EnableDisable;
            $scope.mProspect_Prospect_NamaPanggilan_EnableDisable = !$scope.mProspect_Prospect_NamaPanggilan_EnableDisable;
            $scope.mProspect_Prospect_NoTeleponRumah_EnableDisable = !$scope.mProspect_Prospect_NoTeleponRumah_EnableDisable;
            $scope.mProspect_Prospect_NoHandphone_EnableDisable = !$scope.mProspect_Prospect_NoHandphone_EnableDisable;
            $scope.mProspect_Prospect_Email_EnableDisable = !$scope.mProspect_Prospect_Email_EnableDisable;
            $scope.mProspect_Prospect_AlamatSesuaiIdentitas_EnableDisable = !$scope.mProspect_Prospect_AlamatSesuaiIdentitas_EnableDisable;
            $scope.mProspect_Prospect_NamaInstansi_EnableDisable = !$scope.mProspect_Prospect_NamaInstansi_EnableDisable;
            $scope.mProspect_Prospect_NamaPenanggungJawab_EnableDisable = !$scope.mProspect_Prospect_NamaPenanggungJawab_EnableDisable;
            $scope.mProspect_Prospect_AlamatInstansi_EnableDisable = !$scope.mProspect_Prospect_AlamatInstansi_EnableDisable;
            $scope.mProspect_Prospect_RT_EnableDisable = !$scope.mProspect_Prospect_RT_EnableDisable;
            $scope.mProspect_Prospect_RW_EnableDisable = !$scope.mProspect_Prospect_RW_EnableDisable; //fitri
            $scope.mProspect_Prospect_NoTeleponInstansi_EnableDisable = !$scope.mProspect_Prospect_NoTeleponInstansi_EnableDisable;
            $scope.mProspect_Prospect_NoHandphonePenanggungJawab_EnableDisable = !$scope.mProspect_Prospect_NoHandphonePenanggungJawab_EnableDisable;
            $scope.mProspect_Prospect_EmailPenanggungJawab_EnableDisable = !$scope.mProspect_Prospect_EmailPenanggungJawab_EnableDisable;
            $scope.mProspect_Prospect_Provinsi_EnableDisable = !$scope.mProspect_Prospect_Provinsi_EnableDisable;
            $scope.mProspect_Prospect_KabupatenKota_EnableDisable = !$scope.mProspect_Prospect_KabupatenKota_EnableDisable;
            $scope.mProspect_Prospect_Kecamatan_EnableDisable = !$scope.mProspect_Prospect_Kecamatan_EnableDisable;
            $scope.mProspect_Prospect_Kelurahan_EnableDisable = !$scope.mProspect_Prospect_Kelurahan_EnableDisable;
            $scope.mProspect_Prospect_KodePos_EnableDisable = !$scope.mProspect_Prospect_KodePos_EnableDisable;
            $scope.mProspect_Prospect_TanggalLahir_EnableDisable = !$scope.mProspect_Prospect_TanggalLahir_EnableDisable;
            $scope.mProspect_Prospect_Hobi_EnableDisable = !$scope.mProspect_Prospect_Hobi_EnableDisable;
            $scope.mProspect_Prospect_Agama_EnableDisable = !$scope.mProspect_Prospect_Agama_EnableDisable;
            $scope.mProspect_Prospect_Suku_EnableDisable = !$scope.mProspect_Prospect_Suku_EnableDisable;
            $scope.mProspect_Prospect_Pekerjaan_EnableDisable = !$scope.mProspect_Prospect_Pekerjaan_EnableDisable;
            $scope.mProspect_Prospect_Jabatan_EnableDisable = !$scope.mProspect_Prospect_Jabatan_EnableDisable;
            $scope.mProspect_ProspectNamaPerusahaan_EnableDisable = !$scope.mProspect_ProspectNamaPerusahaan_EnableDisable;
            $scope.mProspect_Prospect_TeleponKantor_EnableDisable = !$scope.mProspect_Prospect_TeleponKantor_EnableDisable;

            $scope.mProspect_Prospect_NamaPerusahaan_EnableDisable = !$scope.mProspect_Prospect_NamaPerusahaan_EnableDisable;
            $scope.mProspect_Prospect_AlamatPerusahaan_EnableDisable = !$scope.mProspect_Prospect_AlamatPerusahaan_EnableDisable;
            $scope.mProspect_Prospect_MetodePembayaran_EnableDisable = !$scope.mProspect_Prospect_MetodePembayaran_EnableDisable;
            $scope.mProspect_Prospect_TradeIn_EnableDisable = !$scope.mProspect_Prospect_TradeIn_EnableDisable;
            $scope.mProspect_Prospect_TingkatKebutuhan_EnableDisable = !$scope.mProspect_Prospect_TingkatKebutuhan_EnableDisable;
            $scope.mProspect_Prospect_KategoriProspect_EnableDisable = !$scope.mProspect_Prospect_KategoriProspect_EnableDisable;
            $scope.mProspect_Prospect_KeperluanPenggunaanKendaraan_EnableDisable = !$scope.mProspect_Prospect_KeperluanPenggunaanKendaraan_EnableDisable;
            $scope.mProspect_Prospect_Catatan_EnableDisable = !$scope.mProspect_Prospect_Catatan_EnableDisable;

            $scope.Prospect_InformasiKendaraanLain_Action_ShowHide = !$scope.Prospect_InformasiKendaraanLain_Action_ShowHide;
            $scope.ShowDisTelp = false;
            $scope.ShowTelp = true;
            //$scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_ShowHide=!$scope.Pelanggan_InformasiKendaraanLain_To_Informasi_Riwayat_ShowHide;
            //$scope.Pelanggan_InformasiKendaraanLain_Action_ShowHide=!$scope.Pelanggan_InformasiKendaraanLain_Action_ShowHide;
        };

        $scope.Pelanggan_Batal_Edit_Detail_Pelanggan = function () {
            //alert('changes not saved unless you clicked simpan');

            $scope.Pelanggan_Toggle_Edit_Detail_Pelanggan();
            $scope.SelectPelanggan($scope.Pelanggan_Selected);

        };

        $scope.Prospect_Batal_Edit_Detail_Prospect = function () {
            //alert('changes not saved unless you clicked simpan');

            $scope.Prospect_Toggle_Edit_Detail_Prospect();
            $scope.KategoriKeyFailsafe = false;
            if ($scope.From_Page == "Prospect") {
                $scope.SelectProspect($scope.Prospect_Selected); //ini refresh
            }


            //ini dibawah asal coba dulu
            if ($scope.From_Page_Prospect_Or_Not == "Not_Prospect") {

            }

            $scope.ShowProspectDetail = false;
            $scope.ShowPengecekanData = true;
        };



        $scope.SimpanEditDetailPelanggan = function (SelectedCustomer) {

            angular.forEach($scope.Pelanggan_Selected, function (value, key) {

                $scope.Pelanggan_Selected[key] = $scope.mProspect_Pelanggan[key];
            });



            ProspectFactory.editDataCustomer($scope.Pelanggan_Selected).then(
                function (res) {


                    $scope.Pelanggan_Toggle_Edit_Detail_Pelanggan();
                    $scope.EditButtonToggle = true;
                },
                function (err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: "Edit data customer gagal.",
                        type: 'danger'
                    });
                }
            )

        };



        $scope.CustomerTypeId = null;
        $scope.SelectProspect = function (SelectedData) {
            $scope.controlLihat = 'Lihat';

            $scope.alesanMunculDariMana = "Main Menu";

            $scope.selectedProspect = SelectedData;


            console.log('SelectProspect ===>',SelectedData)
            $scope.lanjutProspect = {};
            $scope.lanjutProspect.CustomerTypeId = null;
            $scope.lanjutProspect.CustomerTypeId = $scope.selectedProspect.CustomerTypeId;

            $scope.KategoriKeyFailsafe = false;
            //if ($scope.From_Page == "Prospect") 
            {

                $scope.From_Page_Prospect_Or_Not = "Prospect";
                $scope.ShowProspectDetail = true;
                $scope.formData = false;
                //$scope.ModalProspect = { show: false };
                $scope.Prospect_ActionFooter_Button = true;
                $scope.ProspectEditButtonToggle = true;
                $scope.BatalEditDetailProspect_Button = false;
                $scope.SimpanEditDetailProspect_Button = false;
                $scope.ShowQualifyingTools = false;
                $scope.Prospect_InformasiUnitYangDiminati_Tambah_Kendaraan_Button = false;
                $scope.Prospect_InformasiKendaraanLain_Add_Vehicle_Button = false;
                angular.element('.ui.modal.ModalProspect').modal('hide');
                //$scope.mProspect_Prospect={};
            }




            $scope.ClearPelanggan();
            $scope.ClearSuspect();
            $scope.Drop_From_Page = "From_Prospect";
            $scope.From_Page = "Prospect";



            ProspectInformasiUnitYangDiminatiFactory.getData(SelectedData.ProspectId).then(function (res) {
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = res.data.Result;

            });

            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;
            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;


            $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati = false;
            $scope.Show_Hide_Prospect_Informasi_Unit_Yang_Diminati_PanelIcon = "+";
            $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = false;
            $scope.Prospect_InformasiUnitYangDiminati_Action_ShowHide = false;
            $scope.Prospect_InformasiKendaraanLain_Action_ShowHide = false;

            $scope.Show_Hide_Prospect_Informasi_Pelanggan = false;
            $scope.Show_Hide_Prospect_Informasi_Pelanggan_PanelIcon = "+";

            $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit = false;
            $scope.Show_Hide_Prospect_Informasi_KebutuhanTerkaitUnit_PanelIcon = "+";

            $scope.TabInfoLain = true;
            $scope.Show_Hide_Prospect_Informasi_Lain = false;
            $scope.Show_Hide_Prospect_Informasi_Lain_PanelIcon = "+";

            $scope.mProspect_Prospect_TradeIn_EnableDisable = false;
            $scope.ShowDisTelp = true;
            $scope.ShowTelp = false;


            $scope.mProspect_Prospect = angular.copy(SelectedData);
            $scope.mProspect_ProspectBalikinKeawal = angular.copy(SelectedData);
            console.log('$scope.mProspect_Prospect dari daftar tugas ===>', $scope.mProspect_Prospect);
            // $scope.getCPD($scope.mProspect_Prospect.ProspectCode);

            if($scope.mProspect_Prospect.ToyotaId != null){
                kaloAdaDataToyotaIdPakeIniAja = $scope.mProspect_Prospect.ToyotaId;
                console.log('ProspectActionSelected dari daftar tugas getCPD by ToyotaId ========>',$scope.mProspect_Prospect.ToyotaId);
            }else{
                kaloAdaDataToyotaIdPakeIniAja = $scope.mProspect_Prospect.ProspectCode;
                console.log('ProspectActionSelected dari daftar tugas getCPD by ProspectCode ====>',$scope.mProspect_Prospect.ProspectCode);
            }

            $scope.CustomerTypeId = $scope.mProspect_Prospect.CustomerTypeId;

            if($scope.CustomerTypeId == 3){
                $scope.CustomerTypeId = 1
                console.log('CustomerTypeId aslinya 3 eh jadi 1=====>',$scope.CustomerTypeId);
            }else{
                console.log('CustomerTypeId asli =====>',$scope.CustomerTypeId);
            }

            $scope.getCPD(kaloAdaDataToyotaIdPakeIniAja);




            $scope.selectedProvince = {};
            $scope.selectedProvince.Province = {};
            for (var i = 0; i < $scope.province.length; ++i) {
                if ($scope.province[i].ProvinceId == SelectedData.ProvinceId) {
                    $scope.selectedProvince.Province = $scope.province[i];
                    break;
                }
            }


            $scope.selectedCategoryPelanggan = {};
            $scope.selectedCategoryPelanggan.CustomerType = {};
            for (var i = 0; i < $scope.optionsKategoriPelanggan.length; ++i) {
                if ($scope.optionsKategoriPelanggan[i].CustomerTypeId == $scope.Prospect_Selected.CustomerTypeId) {
                    $scope.selectedCategoryPelanggan.CustomerType = $scope.optionsKategoriPelanggan[i];
                    break;
                }
            }

            $scope.selectedCategoryFleet = {};
            $scope.selectedCategoryFleet.CategoryFleet = {};
            for (var i = 0; i < $scope.optionsKategoriFleet.length; ++i) {
                if ($scope.optionsKategoriFleet[i].CategoryFleetId == $scope.Prospect_Selected.CategoryFleetId) {
                    $scope.selectedCategoryFleet.CategoryFleet = $scope.optionsKategoriFleet[i];
                    break;
                }
            }

            var tampungKab = [{
                CityRegencyName: SelectedData.CityRegencyName,
                CityRegencyId: SelectedData.CityRegencyId
            }];


            var tempKab = {};
            tempKab.CityRegencyId = null;
            tempKab.CityRegencyName = null;

            $scope.kabupaten = tempKab;
            $scope.selectedCityRegency = {};
            $scope.selectedCityRegency.CityRegency = {};

            KabupatenKotaFactory.getData('?start=1&limit=100&filterData=ProvinceId|' + SelectedData.ProvinceId).then(function (res) {
                $scope.tempKabupaten = res.data.Result;
                $scope.kabupaten = $scope.tempKabupaten;
                for (var i = 0; i < $scope.kabupaten.length; ++i) {
                    if ($scope.kabupaten[i].CityRegencyId == SelectedData.CityRegencyId) {
                        $scope.selectedCityRegency.CityRegency = $scope.kabupaten[i];
                        break;
                    }
                }
            })

            // if (tampungKab[0].CityRegencyId == null) {
            //     tempKab.CityRegencyId = '';
            //     tempKab.CityRegencyName = '';
            //     KabupatenKotaFactory.getData('?start=1&limit=100&filterData=ProvinceId|' + SelectedData.ProvinceId).then(function(res) {
            //         $scope.tempKabupaten = res.data.Result;
            //         $scope.kabupaten = $scope.tempKabupaten;

            //     })
            // } else {
            //     tempKab.CityRegencyId = tampungKab[0].CityRegencyId;
            //     tempKab.CityRegencyName = tampungKab[0].CityRegencyName;
            //     $scope.kabupaten = [];
            // }

            // $scope.kabupaten.push(tempKab);
            // $scope.selectedCityRegency = {};
            // $scope.selectedCityRegency.CityRegency = {};
            // for (var i = 0; i < $scope.kabupaten.length; ++i) {
            //     if ($scope.kabupaten[i].CityRegencyId == SelectedData.CityRegencyId) {
            //         $scope.selectedCityRegency.CityRegency = $scope.kabupaten[i];
            //         break;
            //     }
            // }

            //norman
            var tampungKec = {};
            tampungKec.DistrictId = null;
            tampungKec.DistrictName = null;

            $scope.kecamatan = tampungKec;
            $scope.selectedDistrict = {};
            $scope.selectedDistrict.DistrictId = {};

            KecamatanFactory.getData('?start=1&limit=100&filterData=CityRegencyId|' + SelectedData.CityRegencyId).then(function (res) {
                $scope.tempKecamatan = res.data.Result;
                $scope.kecamatan = $scope.tempKecamatan;
                for (var i = 0; i < $scope.kecamatan.length; ++i) {
                    if ($scope.kecamatan[i].DistrictId == SelectedData.DistrictId) {
                        $scope.selectedDistrict.District = $scope.kecamatan[i];
                        break;
                    }
                }
            })

            // var tampungKec = [{
            //     DistrictName: SelectedData.DistrictName,
            //     DistrictId: SelectedData.DistrictId
            // }];
            // var tempKec = {};
            // tempKec.DistrictId = null;
            // tempKec.DistrictName = null;


            // $scope.kecamatan = [];
            // if (tampungKec[0].DistrictId == null) {
            //     tempKec.DistrictId = '';
            //     tempKec.DistrictName = '';
            //     KecamatanFactory.getData('?start=1&limit=100&filterData=CityRegencyId|' + SelectedData.CityRegencyId).then(function (res) {
            //         $scope.tempKecamatan = res.data.Result;
            //         $scope.kecamatan = $scope.tempKecamatan;
            //     })
            // } else {
            //     tempKec.DistrictId = tampungKec[0].DistrictId;
            //     tempKec.DistrictName = tampungKec[0].DistrictName;
            //     $scope.kecamatan = [];
            // }

            // $scope.kecamatan.push(tempKec);

            // $scope.selectedDistrict = {};
            // $scope.selectedDistrict.District = {};
            // for (var i = 0; i < $scope.kecamatan.length; ++i) {
            //     if ($scope.kecamatan[i].DistrictId == $scope.Prospect_Selected.DistrictId) {
            //         $scope.selectedDistrict.District = $scope.kecamatan[i];
            //         break;
            //     }
            // }

            var tampungKel = [{
                VillageId: SelectedData.VillageId,
                VillageName: SelectedData.VillageName

            }];
            var tempKel = {};
            tempKel.VillageId = null;
            tempKel.VillageName = null;

            $scope.kelurahan = [];

            if (tampungKel[0].VillageId == null) {
                tempKel.VillageId = '';
                tempKel.VillageName = '';
                KelurahanFactory.getData('?start=1&limit=100&filterData=DistrictId|' + SelectedData.DistrictId).then(function (res) {
                    $scope.tempKelurahan = res.data.Result;

                    $scope.kelurahan = $scope.tempKelurahan;
                })
            } else {
                tempKel.VillageId = tampungKel[0].VillageId;
                tempKel.VillageName = tampungKel[0].VillageName;
                $scope.kelurahan = [];
            }

            //console.log('tempKel',tempKel);
            //console.log('$scope.kelurahan',$scope.kelurahan);
            $scope.kelurahan.push(tempKel);
            $scope.selectedVillage = {};
            $scope.selectedVillage.Village = {};
            for (var i = 0; i < $scope.kelurahan.length; ++i) {
                if ($scope.kelurahan[i].VillageId == $scope.Prospect_Selected.VillageId) {
                    $scope.selectedVillage.Village = $scope.kelurahan[i];
                    break;
                }
            }

            $scope.selectAgama = {};
            $scope.selectAgama.Agama = {};
            for (var i = 0; i < $scope.optionsAgama.length; ++i) {
                if ($scope.optionsAgama[i].CustomerReligionId == SelectedData.CustomerReligionId) {
                    $scope.selectAgama.Agama = $scope.optionsAgama[i];
                    break;
                }
            }


            $scope.selectedSuku = {};
            $scope.selectedSuku.Suku = {};
            for (var i = 0; i < $scope.optionsSuku.length; ++i) {
                if ($scope.optionsSuku[i].CustomerEthnicId == SelectedData.CustomerEthnicId) {
                    $scope.selectedSuku.Suku = $scope.optionsSuku[i];
                    break;
                }
            }


            $scope.selectedJabatan = {};
            $scope.selectedJabatan.Jabatan = {};
            for (var i = 0; i < $scope.optionsJabatan.length; ++i) {
                if ($scope.optionsJabatan[i].CustomerPositionId == SelectedData.CustomerPositionId) {
                    $scope.selectedJabatan.Jabatan = $scope.optionsJabatan[i];
                    break;
                }
            }


            $scope.selectedFundSource = {};
            $scope.selectedFundSource.FundSource = {};
            for (var i = 0; i < $scope.optionsMetodePembayaran.length; ++i) {
                if ($scope.optionsMetodePembayaran[i].FundSourceId == SelectedData.FundSourceId) {
                    $scope.selectedFundSource.FundSource = $scope.optionsMetodePembayaran[i];
                    break;
                }
            }


            $scope.selectedKebutuhan = {};
            $scope.selectedKebutuhan.Kebutuhan = {};
            for (var i = 0; i < $scope.optionsTingkatKebutuhan.length; ++i) {
                if ($scope.optionsTingkatKebutuhan[i].PurchaseTimeId == SelectedData.PurchaseTimeId) {
                    $scope.selectedKebutuhan.Kebutuhan = $scope.optionsTingkatKebutuhan[i];
                    break;
                }
            }


            $scope.selectedKategori = {};
            $scope.selectedKategori.Kategori = {};
            for (var i = 0; i < $scope.optionsKategoriProspect.length; ++i) {
                if ($scope.optionsKategoriProspect[i].CategoryProspectId == SelectedData.CategoryProspectId) {
                    $scope.selectedKategori.Kategori = $scope.optionsKategoriProspect[i];
                    break;
                }
            }


            $scope.selectedPurchasePurpose = {};
            $scope.selectedPurchasePurpose.PurchasePurpose = {};
            for (var i = 0; i < $scope.optionsPurchasePurpose.length; ++i) {
                if ($scope.optionsPurchasePurpose[i].PurchasePurposeId == SelectedData.PurchasePurposeId) {
                    $scope.selectedPurchasePurpose.PurchasePurpose = $scope.optionsPurchasePurpose[i];
                    break;
                }
            }
			
			$scope.selectedKlasifikasiPembelian = {};
            $scope.selectedKlasifikasiPembelian.PurchasingClassificationId = {};
            for (var i = 0; i < $scope.optionsKlasifikasiPembelian.length; ++i) {
                if ($scope.optionsKlasifikasiPembelian[i].PurchasingClassificationId == SelectedData.PurchasingClassificationId) {
                    $scope.selectedKlasifikasiPembelian.PurchasingClassificationId = $scope.optionsKlasifikasiPembelian[i];
                    break;
                }
            }


            $scope.mProspect_Prospect.StatusPelanggan = "Prospect";

            $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon = "+";
            $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = false;



            var ProspectKategori_SelectedIndex = 0;
            for (var i = 0; i < $scope.optionsKategoriPelanggan.length; ++i) {
                if ($scope.optionsKategoriPelanggan[i].CustomerTypeId == SelectedData.CustomerTypeId) {
                    ProspectKategori_SelectedIndex = i;
                }
            }
            //if($scope.optionsKategoriPelanggan[ProspectKategori_SelectedIndex].CustomerCategoryName=="Individu")
            KategoriPelangganFactory.getData().then(function (res) {
                $scope.optionsKategoriPelanggan = res.data.Result;
                //return $scope.optionsKategoriPelanggan;
            });


            $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
            if ($scope.optionsKategoriPelanggan[ProspectKategori_SelectedIndex].CustomerTypeDesc == "Individu") {
                $scope.ProspectIndividu = true;
                $scope.ProspectInstansi = false;



                for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
                    if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc != "Individu") {
                        $scope.optionsKategoriPelangganFiltered.splice(i, 1);
                    }
                }
            } else {
                $scope.ProspectIndividu = false;
                $scope.ProspectInstansi = true;
                //$scope.List_Prospect_InformasiKendaraanLain_To_Repeat = "";

                for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
                    if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc == "Individu") {
                        $scope.optionsKategoriPelangganFiltered.splice(i, 1);
                    }
                }
            }

            if (SelectedData.CategoryProspectId == 4) { //nanti benerin conditionnya kalo bisa
                //nanti balikin
                $scope.Show_mProspect_Prospect_KategoriProspect = true;
                $scope.Show_mProspect_Prospect_KategoriAwal = true;
                $scope.Show_mProspect_Prospect_AlasanPenurunan = true;
                $scope.ProspectEditButtonToggle = false;
                //disini dropdown harus mati
            }
            if (SelectedData.BeforeCategoryProspectId == null || SelectedData.BeforeCategoryProspectId == 0) { //disini dropdown harus nyala

                //nanti balikin
                $scope.Show_mProspect_Prospect_KategoriProspect = true;
                $scope.Show_mProspect_Prospect_KategoriAwal = false;
                $scope.Show_mProspect_Prospect_AlasanPenurunan = false;
            } else if (SelectedData.BeforeCategoryProspectId != null || SelectedData.BeforeCategoryProspectId != 0) { //disini dropdown harus nyala setan


                $scope.Show_mProspect_Prospect_KategoriProspect = true;
                $scope.Show_mProspect_Prospect_KategoriAwal = true;



                if (SelectedData.CategoryProspectId > SelectedData.BeforeCategoryProspectId) {
                    $scope.Show_mProspect_Prospect_AlasanPenurunan = true;
                } else {
                    $scope.Show_mProspect_Prospect_AlasanPenurunan = false;
                }
            }

            //}



            //}
            //$scope.KategoriKeyFailsafe=true;
            //disini disable semua
            $scope.mProspect_Prospect_MetodePembayaran_EnableDisable = false;
            $scope.mProspect_Prospect_TingkatKebutuhan_EnableDisable = false;
            $scope.mProspect_Prospect_KategoriProspect_EnableDisable = false;
            $scope.mProspect_Prospect_KeperluanPenggunaanKendaraan_EnableDisable = false;

            $scope.mProspect_Prospect_NamaInstansi_EnableDisable = false;
            $scope.mProspect_Prospect_NamaPenanggungJawab_EnableDisable = false;
            $scope.mProspect_Prospect_AlamatInstansi_EnableDisable = false;
            $scope.mProspect_Prospect_RT_EnableDisable = false; //fitri
            $scope.mProspect_Prospect_RW_EnableDisable = false; //fitri
            $scope.mProspect_Prospect_NoTeleponInstansi_EnableDisable = false;
            $scope.mProspect_Prospect_NoHandphonePenanggungJawab_EnableDisable = false;
            $scope.mProspect_Prospect_EmailPenanggungJawab_EnableDisable = false;

            $scope.mProspect_Prospect_ToyotaId_EnableDisable = false;
            $scope.mProspect_Prospect_KategoriPelanggan_EnableDisable = false;
            //$scope.mProspect_Prospect_KategoriFleet_EnableDisable = false;
            $scope.mProspect_Prospect_NamaLengkap_EnableDisable = false;
            $scope.mProspect_Prospect_NamaPanggilan_EnableDisable = false;
            $scope.mProspect_Prospect_NoTeleponRumah_EnableDisable = false;
            $scope.mProspect_Prospect_NoHandphone_EnableDisable = false;
            $scope.mProspect_Prospect_Email_EnableDisable = false;
            $scope.mProspect_Prospect_AlamatSesuaiIdentitas_EnableDisable = false;
            $scope.mProspect_Prospect_Provinsi_EnableDisable = false;
            $scope.mProspect_Prospect_KabupatenKota_EnableDisable = false;
            $scope.mProspect_Prospect_Kecamatan_EnableDisable = false;
            $scope.mProspect_Prospect_Kelurahan_EnableDisable = false;
            $scope.mProspect_Prospect_KodePos_EnableDisable = false;
            $scope.mProspect_Prospect_TanggalLahir_EnableDisable = false;
            $scope.mProspect_Prospect_Hobi_EnableDisable = false;
            $scope.mProspect_Prospect_Agama_EnableDisable = false;
            $scope.mProspect_Prospect_Suku_EnableDisable = false;
            $scope.mProspect_Prospect_Pekerjaan_EnableDisable = false;
            $scope.mProspect_Prospect_Jabatan_EnableDisable = false;
            $scope.mProspect_Prospect_NamaPerusahaan_EnableDisable = false;
            $scope.mProspect_Prospect_TeleponKantor_EnableDisable = false;
            $scope.mProspect_Prospect_AlamatPerusahaan_EnableDisable = false;
            $scope.mProspect_Prospect_Catatan_EnableDisable = false;

            angular.element('.ui.modal.ModalProspect').modal('hide');
            $scope.Show_Hide_Prospect_Informasi_Kendaraan_Lain = false;
            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = SelectedData.CProspectDetailOtherVehicleView;
            console.log('lanjutProspect', $scope.lanjutProspect);

        };

        $scope.QualifyingToolsCalculateResult = function () {
            var Qualifying_P1 = parseInt($scope.Kebutuhan_Fisik);
            var Qualifying_P2 = parseInt($scope.Jumlah_Penumpang);
            var Qualifying_P3 = parseInt($scope.Jarak_Yang_Sering_Ditempuh);
            var Total_Qualifying_Score = Qualifying_P1 + Qualifying_P2 + Qualifying_P3;
            if (Total_Qualifying_Score >= 1 && Total_Qualifying_Score <= 3) {
                $scope.Result_Total_Qualifying_Score = "Low";
            } else if (Total_Qualifying_Score >= 4 && Total_Qualifying_Score <= 6) {
                $scope.Result_Total_Qualifying_Score = "Medium";
            } else if (Total_Qualifying_Score >= 7) {
                $scope.Result_Total_Qualifying_Score = "Hot";
            }


            $scope.ModalQualifyingTools = { show: true };

        };

        $scope.QualifyingToolsDecisionClicked = function (Decision) {
            if (Decision == "Ya") {
                //alert(Decision);
            } else if (Decision == "Tidak") {
                $scope.ShowQualifyingTools = false; //ini qualifying tool
                $scope.ShowProspectRiwayat = false; //ini numpang doang
                $scope.Show_Prospect_TambahUbah_InformasiUnitYangDiminati = false; //ini numpang doang
                $scope.Show_Prospect_TambahUbah_InformasiKendaraanLain = false; //ini numpang doang
                $scope.ShowProspectDetail = true;
            }
            $scope.ModalQualifyingTools = { show: false };
        };
        $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati = {};

        $scope.Prospect_Ubah_InformasiUnitYangDiminati = function (SelectedData, Operation) {

            

            //console.log("Tipe n Color", SelectedData);
            //vehicleTypefilter
            // var tempTypeData = [];
            // $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId = SelectedData.VehicleModelId;
            // $scope.SelectedModel.VehicleModelId = SelectedData.VehicleModelId;

            // $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName = SelectedData.VehicleModelName;
            // $scope.SelectedModel.VehicleModelName = SelectedData.VehicleModelName;

            // //



            //$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = '';


            

            
            $scope.ShowProspectDetail = false;
            $scope.Show_Prospect_TambahUbah_InformasiUnitYangDiminati = true;

            angular.element('.ui.modal.ModalProspect_InformasiUnitYangDiminati_Action').modal('hide');


            $scope.Prospect_Operation_InformasiUnitYangDiminati = Operation;

            if (Operation == "Update") {
                $scope.initialUbah = 'Ubah';

                $scope.selectedModel = {};
                $scope.selectedModel.VehicleModel = {};
                for (var i = 0; i < $scope.optionsModel.length; ++i) {
                    if ($scope.optionsModel[i].VehicleModelId == SelectedData.VehicleModelId) {
                        $scope.selectedModel.VehicleModel = $scope.optionsModel[i];
                        $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId = $scope.selectedModel.VehicleModel.VehicleModelId;
                        $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName = $scope.selectedModel.VehicleModel.VehicleModelName;
                        break;
                    }
                }

                // TipeFactory.getData('?start=1&limit=100&filterData=VehicleModelId|' + SelectedData.VehicleModelId).then(function(res) {
                //     $scope.vehicleTypefilter = res.data.Result;
                //     return $scope.RawTipe;
                // });

                var tempType = [{
                    Description: SelectedData.Description,
                    VehicleTypeId: SelectedData.VehicleTypeId,
                    KatashikiCode: SelectedData.KatashikiCode,
                    SuffixCode: SelectedData.SuffixCode
                }];
                $scope.vehicleTypefilter = tempType;
                $scope.selectedTipe = {};
                $scope.selectedTipe.VehicleType = {};
                // for (var i = 0; i < $scope.vehicleTypefilter.length; ++i) {
                // if ($scope.vehicleTypefilter[i].VehicleTypeId == SelectedData.VehicleTypeId) {
                // $scope.selectedTipe.VehicleType = $scope.vehicleTypefilter[i];
                // break;
                // }
                // }


                //pak nor ini tadinya TipeFactory.getData aku ganti cuma biar seragam 
				//soal ada bug laen cara benerinnya diganti jadi gini kalo jadi masalah balikin aja				
                ProspectFactory.getDaVehicleType('?start=1&limit=1000&filterData=VehicleModelId|' + SelectedData.VehicleModelId).then(function (res) {
                    $scope.vehicleTypefilter = res.data.Result;
                    console.log("data masuk sini", $scope.vehicleTypefilter);
                    //return $scope.RawTipe;
                    for (var i = 0; i < $scope.vehicleTypefilter.length; ++i) {
                        if ($scope.vehicleTypefilter[i].VehicleTypeId == SelectedData.VehicleTypeId) {
                            $scope.selectedTipe.VehicleType = $scope.vehicleTypefilter[i];
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = $scope.selectedTipe.VehicleType.VehicleTypeId;
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = $scope.selectedTipe.VehicleType.Description;
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode = $scope.selectedTipe.VehicleType.KatashikiCode;
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode = $scope.selectedTipe.VehicleType.SuffixCode;
                            break;
                        }
                    }
                });

                var tempColor = [{
                    ColorName: SelectedData.ColorName,
                    ColorId: SelectedData.ColorId
                }];
                $scope.vehicleTypecolorfilter = tempColor;
                $scope.selectedwarna = {};
                $scope.selectedwarna.Warna = {};
                // for (var i = 0; i < $scope.vehicleTypecolorfilter.length; ++i) {
                // if ($scope.vehicleTypecolorfilter[i].ColorId == SelectedData.ColorId) {
                // $scope.selectedwarna.Warna = $scope.vehicleTypecolorfilter[i];

                // break;
                // }
                // }

                WarnaFactory.getData('?start=1&limit=100&filterData=VehicleTypeId|' + SelectedData.VehicleTypeId).then(function (res) {
                    // $scope.optionsWarna = res.data.Result;
                    $scope.vehicleTypecolorfilter = res.data.Result;
                    for (var i = 0; i < $scope.vehicleTypecolorfilter.length; ++i) {
                        if ($scope.vehicleTypecolorfilter[i].ColorId == SelectedData.ColorId) {
                            $scope.selectedwarna.Warna = $scope.vehicleTypecolorfilter[i];
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = $scope.selectedwarna.Warna.ColorId;
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = $scope.selectedwarna.Warna.ColorName;

                            break;
                        }
                    }
                });

                var tempYear = [{
                    Year: SelectedData.ProductionYear
                }];

                //var tempYear = [];
                //tempYear.Year = null;
                $scope.ListTahunKendaraan = tempYear;
                $scope.selectedTahun = {}
                $scope.selectedTahun.Tahun = {};
                ProspectFactory.getYearProspect().then(function (res) {
                    $scope.ListTahunKendaraan = res.data.Result;
                    //$scope.ListTahunKendaraan = tempYear;
                    for (var i = 0; i < $scope.ListTahunKendaraan.length; ++i) {
                        if ($scope.ListTahunKendaraan[i].Year == SelectedData.ProductionYear) {
                            $scope.selectedTahun.Tahun = $scope.ListTahunKendaraan[i];
                            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ProductionYear = $scope.selectedTahun.Tahun.Year;

                            break;
                        }
                    }
                    //return $scope.optionsModelType;
                });

                // if (tampungYear[0].Year == null) {
                //     tempYear.Year = '';
                //     ProspectFactory.getYearProspect().then(function(res) {
                //         $scope.tempYear = res.data.Result;
                //         $scope.ListTahunKendaraan = tempYear;
                //         //return $scope.optionsModelType;
                //     });
                // } else {
                //     $scope.ListTahunKendaraan = tampungYear;
                //     $scope.selectedTahun = {};
                //     $scope.selectedTahun.Tahun = {};
                //     $scope.selectedTahun.Tahun = $scope.ListTahunKendaraan;
                //     for (var i = 0; i < $scope.ListTahunKendaraan.length; ++i) {
                //         if ($scope.ListTahunKendaraan[i].Year == SelectedData.ProductionYear) {
                //             $scope.selectedTahun.Tahun = $scope.ListTahunKendaraan[i];
                //             break;
                //         }
                //     }
                // }



                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = SelectedData.Qty;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName = $scope.selectedModel.VehicleModel.VehicleModelName;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId = $scope.selectedModel.VehicleModel.VehicleModelId;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = $scope.selectedTipe.VehicleType.Description;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode = $scope.selectedTipe.VehicleType.KatashikiCode;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode = $scope.selectedTipe.VehicleType.SuffixCode;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = $scope.selectedTipe.VehicleType.VehicleTypeId;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = $scope.selectedwarna.Warna.ColorName;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = $scope.selectedwarna.Warna.ColorId;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ProductionYear = $scope.selectedTahun.Tahun.Year;

                console.log("masuk sini 2");
                $scope.initialUbah = 'Tambah';
            } else if (Operation == "Insert") {
                $scope.initialUbah = 'Tambah';
                //$scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati = "";
                //mungkin kalo pake $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati ={};
                //bisa cuma aku ga berani ubah terlalu banyak
                $scope.selectedModel = {};
                $scope.selectedModel.VehicleModel = {};
                $scope.selectedTipe = {};
                $scope.selectedTipe.VehicleType = {};
                $scope.selectedwarna = {};
                $scope.selectedwarna.Warna = {};

           


                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati = {};
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = "";
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = "";
				
				$scope.vehicleTypefilter=[];
				$scope.vehicleTypecolorfilter=[];

                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = 1;
                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = angular.copy(1);

                $scope.selectedTahun = {};
                //$scope.selectedTahun.Tahun = $scope.ListTahunKendaraan[0];
                for (var i = 0; i < $scope.ListTahunKendaraan.length; ++i) {
                	if ($scope.ListTahunKendaraan[i].Year == new Date().getFullYear()) {
                		$scope.selectedTahun.Tahun = $scope.ListTahunKendaraan[i];
                		break;
                	}
                }
                console.log('$scope.selectedTahun.Tahun',$scope.selectedTahun.Tahun);
                if(typeof $scope.selectedTahun.Tahun == 'undefined'){
                    $scope.selectedTahun.Tahun = $scope.ListTahunKendaraan[0];
                }else{
                    $scope.selectedTahunValue($scope.selectedTahun.Tahun);
                }
               
                


            }


        };

        $scope.Prospect_Hapus_InformasiUnitYangDiminati = function () {
            if ($scope.controlData == 'UpdateProspect'){
                $scope.listUnitYangDiminati = angular.copy($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat);
            }
            $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat.splice($scope.IndexUnitDiminati, 1);
            $scope.listUnitYangDiminati.splice($scope.IndexUnitDiminati, 1);
            angular.element('.ui.modal.ModalProspect_InformasiUnitYangDiminati_Action').modal('hide');
        };

        $scope.ModelValue = function (selectModel) {


            $scope.SelectedModel = selectModel;
        }
        $scope.TipeValue = function (selectTipe) {

            $scope.SelectedTipe = selectTipe;
        }
        $scope.selectedwarna = null;
        $scope.SelectedColor = {};
        $scope.selectedColorValue = function (selected) {


            $scope.SelectedColor.ColorId = selected.ColorId;
            $scope.SelectedColor.ColorName = selected.ColorName;
            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId = $scope.SelectedColor.ColorId;
            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName = $scope.SelectedColor.ColorName;
        }

        $scope.selectedTahun = null;
        $scope.SelectedTahun = {};
        $scope.selectedTahunValue = function (selected) {

            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ProductionYear = selected.Year;
            $scope.SelectedTahun.ProductionYear = selected.Year;
        }

        $scope.ColorValue = function (selectColor) {

            $scope.SelectedColor = selectColor;

        }
        $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = [];
        $scope.ProspectSimpanInformasiUnitYangDiminati = function (dataDiminati) {

            console.log


            //kosongin data yg sebelumnya
            $scope.selectedTipe.VehicleType = null;
            $scope.selectedwarna.Warna = null;
            $scope.selectedTipe.Description = null;
            $scope.selectedTipe.KatashikiCode = null;
            $scope.selectedTipe.SuffixCode = null;
            

            if ($scope.Prospect_Operation_InformasiUnitYangDiminati == "Update") {
                $scope.tampungDetailUnitId = {};
                $scope.tampungDetailUnitId.ProspectId = {};
                try {
                $scope.tampungDetailUnitId = $scope.selectedProspect.CProspectDetailUnitView[$scope.IndexUnitDiminati];
                } catch (e) {
                    $scope.tampungDetailUnitId = {};
                }

                console.log('$scope.SelectedModel', $scope.SelectedModel);
                console.log('$scope.SelectedTipe', $scope.SelectedTipe);
                console.log('$scope.SelectedColor', $scope.SelectedColor);
                console.log('$scope.SelectedTahun', $scope.SelectedTahun);
                console.log('dataDiminati', dataDiminati);
                // var getVehicle = {};
                // getVehicle = angular.merge({},dataDiminati, $scope.SelectedModel, $scope.SelectedTipe, $scope.SelectedColor, $scope.SelectedTahun)
                // var insertVehicle = angular.copy(getVehicle);
                // console.log('insertVehicle',insertVehicle);
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].ColorId = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorId;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].ColorName = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ColorName;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].Description = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Description;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].KatashikiCode = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.KatashikiCode;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].ProductionYear = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ProductionYear;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].ProspectId = dataDiminati.ProspectId;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].Qty = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].SuffixCode = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.SuffixCode;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].VehicleModelId = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].VehicleModelName = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelName;
                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[$scope.IndexUnitDiminati].VehicleTypeId = $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleTypeId;
                console.log('$scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat', $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat);

            } else if ($scope.Prospect_Operation_InformasiUnitYangDiminati == "Insert") {

                


                $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.ProspectId = $scope.mProspect_Prospect.ProspectId;

                $scope.tampungDetailUnitId = {};
                $scope.tampungDetailUnitId.DetailUnitId = null;
                try {

                    $scope.selectedTipe = {};

                    // console.log('$scope.tampungDetailUnitId ===>', $scope.tampungDetailUnitId);
                    console.log('dataDiminati===>',dataDiminati);
                    // console.log('$scope.SelectedModel ===>',$scope.SelectedModel);
                    // console.log('$scope.SelectedTipe ===>',$scope.SelectedTipe);
                    // console.log('$scope.SelectedColor ===>',$scope.SelectedColor);
                    // console.log('$scope.SelectedTahun ===>',$scope.SelectedTahun);
                    console.log('mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty ===>', $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty);

                    // var getVehicle = angular.merge({}, $scope.tampungDetailUnitId, dataDiminati, $scope.SelectedModel, $scope.SelectedTipe, $scope.SelectedColor, $scope.SelectedTahun);
                    var getVehicle = angular.merge({}, $scope.tampungDetailUnitId, dataDiminati);

                    console.log('getVehicle try ===>', getVehicle);
                    $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat.push(getVehicle);


                } catch (e) {
                    $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = [];
                    $scope.tampungDetailUnitId = {};
                    $scope.tampungDetailUnitId.DetailUnitId = null;

                    // var getVehicle = angular.merge({}, dataDiminati, $scope.SelectedModel, $scope.SelectedTipe, $scope.SelectedColor, $scope.SelectedTahun)
                    var getVehicle = angular.merge({}, dataDiminati);

                    console.log('getVehicle catch ===>', getVehicle);
                    $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat.push(getVehicle);


                }
            }
            $scope.Show_Prospect_TambahUbah_InformasiUnitYangDiminati = false;
            $scope.ShowProspectDetail = true;
            $scope.selectedModel = {};
            $scope.selectedTipe = {};
            $scope.selectedwarna = {};
            $scope.selectedTahun = {};
            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.Qty = null;
			
			// $scope.selectedKategori = {};
            // $scope.selectedKategori.Kategori = {};
			
            console.log("iblis1",$scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat);
            console.log("iblis2",$scope.selectedKategori);



            
            
            $scope.listUnitYangDiminati = angular.copy($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat);

			// ini punya mike, bikin field kategori prospek berubah ketika input "informasi unit yang diminati"
			// if($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!="undefined")&& $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ProductionYear!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ProductionYear!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty>0 && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty!="undefined"))
			// {
			// 	$scope.selectedKategori.Kategori=$scope.optionsKategoriProspect[0];
            //     $scope.kategoriValue($scope.selectedKategori.Kategori.CategoryProspectId);
            //     console.log('ini rubah ke apaa 1');
			// }
			
			// if($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="undefined"))
			// {
            //     // ini rubah kategoti pelanggan jadi low
			// 	$scope.selectedKategori.Kategori=$scope.optionsKategoriProspect[2];
            //     $scope.kategoriValue($scope.selectedKategori.Kategori.CategoryProspectId);
            //     console.log('ini rubah ke apaa 2');
			// }
			
			// if($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="undefined"))
			// {
            //     // ini rubah kategori pelanggan jadi medium
			// 	$scope.selectedKategori.Kategori=$scope.optionsKategoriProspect[1];
            //     $scope.kategoriValue($scope.selectedKategori.Kategori.CategoryProspectId);
            //     console.log('ini rubah ke apaa 3');
			// }
			
			// if($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].VehicleModelName!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Description!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!="" && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ColorName!="undefined")&& $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ProductionYear!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].ProductionYear!="undefined") && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty>0 && $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty!=null && (typeof $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat[0].Qty!="undefined"))
			// {
            //     //ini rubah kategori pelanggan jadi hot
			// 	$scope.selectedKategori.Kategori=$scope.optionsKategoriProspect[0];
            //     $scope.kategoriValue($scope.selectedKategori.Kategori.CategoryProspectId);
            //     console.log('ini rubah ke apaa 4');
			// }
			
			

        };

        $scope.Prospect_Ubah_InformasiKendaraanLain = function (Selected_Data, Operation, FromModule) {

            if (FromModule == "Prospect") {
                $scope.Pelanggan_TambahUbah_InformasiKendaraanLain_Batal_Button = false;
                $scope.Prospect_TambahUbah_InformasiKendaraanLain_Batal_Button = true;
            } else if (FromModule == "Pelanggan") {
                $scope.Pelanggan_TambahUbah_InformasiKendaraanLain_Batal_Button = true;
                $scope.Prospect_TambahUbah_InformasiKendaraanLain_Batal_Button = false;
            }
            $scope.Prospect_FromModule_InformasiKendaraanLain = FromModule;

            if (Operation == "Update") {

                $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain = angular.copy(Selected_Data);

                $scope.selectedMerek = {};
                $scope.selectedMerek.Merek = {};
                for (var i = 0; i < $scope.optionsMerekMobil.length; ++i) {
                    if ($scope.optionsMerekMobil[i].BrandId == Selected_Data.BrandId) {
                        $scope.selectedMerek.Merek = $scope.optionsMerekMobil[i];
                        break;
                    }
                }
            } else if (Operation == "Insert") {
                $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain = {};
                $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.VehiclePrice = 0;
                $scope.selectedMerek = {};
                //alert(Operation);
            }
            $scope.Prospect_Operation_InformasiKendaraanLain = Operation;

            $scope.ShowProspectDetail = false;
            $scope.ShowPelangganDetail = false;
            $scope.Show_Prospect_TambahUbah_InformasiKendaraanLain = true;

            //$scope.ModalProspect_InformasiKendaraanLain_Action = { show: false };
            angular.element('.ui.modal.ModalProspect_InformasiKendaraanLain_Action').modal('hide');
            //$scope.ModalPelanggan_InformasiKendaraanLain_Action = { show: false };
            angular.element('.ui.modal.ModalPelanggan_InformasiKendaraanLain_Action').modal('hide');
        };

        $scope.Prospect_Hapus_InformasiKendaraanLain = function (Selected_Data) {



            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat.splice($scope.IndexOtherVehicle, 1);
            //$scope.ModalProspect_InformasiKendaraanLain_Action = { show: false };
            angular.element('.ui.modal.ModalProspect_InformasiKendaraanLain_Action').modal('hide');
        };


        $scope.tempKendaraanLain = [];
        $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = [];
        $scope.ProspectSimpanInformasiKendaraanLain = function (tempKendaraan) {
            //validasi harga beli kendaraan start
            var tmpPrice = $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.VehiclePrice.toString();
            console.log("Cek Data Model",tmpPrice.length);
            if(tmpPrice.length > 13){
                bsNotify.show({
                    title: "Peringatan",
                    content: 'Harga Beli kendaraan tidak boleh lebih dari 13 digit',
                    type: 'warning'
                });
                return true;
            }
            //validasi harga beli kendaraan End

            if ($scope.Prospect_Operation_InformasiKendaraanLain == "Update") {
                if ($scope.Prospect_FromModule_InformasiKendaraanLain == "Prospect") {

                    $scope.tampungDetailOtherUnit = {};
                    // console.log('$scope.selectedProspect',$scope.selectedProspect);
                    if($scope.selectedProspect !=undefined){
                        $scope.tampungDetailOtherUnit = $scope.selectedProspect.CProspectDetailOtherVehicleView[$scope.IndexOtherVehicle];
                        $scope.tampungDetailOtherUnit.DetailOtherVehicleId = {};
                    $scope.tampungDetailOtherUnit.ProspectId = {};
                    $scope.tampungDetailOtherUnit.DetailOtherVehicleId = $scope.selectedProspect.CProspectDetailOtherVehicleView[$scope.IndexOtherVehicle].DetailOtherVehicleId;

                    $scope.tampungDetailOtherUnit.ProspectId = $scope.selectedProspect.ProspectId;
                    }

                    $scope.tempKendaraanLain.push(tempKendaraan);
                    var objectKendaraanLain = angular.merge({}, $scope.tampungDetailOtherUnit, $scope.SelectedMerek, tempKendaraan)
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].BrandId = objectKendaraanLain.BrandId;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].BrandName = objectKendaraanLain.BrandName;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].ColorName = objectKendaraanLain.ColorName;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].DetailOtherVehicleId = objectKendaraanLain.DetailOtherVehicleId;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].ModelName = objectKendaraanLain.ModelName;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].ProspectId = objectKendaraanLain.ProspectId;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].TypeName = objectKendaraanLain.TypeName;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].VehiclePrice = objectKendaraanLain.VehiclePrice;
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat[$scope.IndexOtherVehicle].VehicleYear = objectKendaraanLain.VehicleYear;


                } else if ($scope.Prospect_FromModule_InformasiKendaraanLain == "Pelanggan") {

                    angular.forEach($scope.Pelanggan_InformasiKendaraanLain_Selected_Data, function (value, key) {
                        $scope.Pelanggan_InformasiKendaraanLain_Selected_Data[key] = $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain[key];
                    });
                }
            } else if ($scope.Prospect_Operation_InformasiKendaraanLain == "Insert") {

                if ($scope.Prospect_FromModule_InformasiKendaraanLain == "Prospect") {
                    $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.ProspectId = $scope.mProspect_Prospect.ProspectId;
                    $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.DetailOtherVehicleId = null;

                    $scope.tempKendaraanLain.push(tempKendaraan);
                    var objectKendaraanLain = angular.merge({}, $scope.SelectedMerek, tempKendaraan)
                    $scope.List_Prospect_InformasiKendaraanLain_To_Repeat.push(objectKendaraanLain);

                } else if ($scope.Prospect_FromModule_InformasiKendaraanLain == "Pelanggan") {

                }
            }

            $scope.ShowProspectDetail = true;
            $scope.Show_Prospect_TambahUbah_InformasiKendaraanLain = false;
            $scope.selectedMerek.Merek = '';
            $scope.Prospect_InformasiKendaraanLain_Action_ShowHide = true;
        };

        $scope.Prospect_InformasiUnitYangDiminati_Action_Selected = function (IndexU, Selected_Data) {

            $scope.IndexUnitDiminati = IndexU;
            $scope.Prospect_InformasiUnitYangDiminati_Selected_Data = Selected_Data;

            setTimeout(function () {
                angular.element('.ui.modal.ModalProspect_InformasiUnitYangDiminati_Action').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalProspect_InformasiUnitYangDiminati_Action').modal('show');
        };

        $scope.Prospect_InformasiKendaraanLain_Action_Selected = function (IndexOtherVehicle, Selected_Data) {
            $scope.IndexOtherVehicle = IndexOtherVehicle;
            $scope.Prospect_InformasiKendaraanLain_Selected_Data = Selected_Data;

            setTimeout(function () {
                angular.element('.ui.modal.ModalProspect_InformasiKendaraanLain_Action').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalProspect_InformasiKendaraanLain_Action').modal('show');
        };

        

        $scope.ToProspectDetailNotFromProspect = function (lanjutProspect) {
            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = [];



            //ini pass yang suspect sama pelanggan yang ada di pengecekan sonyol
            $scope.mProspect_Prospect = angular.copy($scope.mProspect_PengecekanDataBaru);
            if ($scope.mProspect_Prospect.ProspectName != null) {
                $scope.mProspect_Prospect['ProspectName'] = angular.copy($scope.mProspect_PengecekanDataBaru['ProspectName']);
            } else {
                $scope.mProspect_Prospect['ProspectName'] = angular.copy($scope.mProspect_PengecekanDataBaru['SuspectName']);
                $scope.mProspect_Prospect.CustomerTypeId = 1;
            }

            if ($scope.mProspect_PengecekanDataBaru.InstanceName == null) {

                $scope.mProspect_Prospect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
                $scope.mProspect_Prospect.CustomerTypeDesc = "Individu";

            } else if ($scope.mProspect_PengecekanDataBaru.InstanceName != null) {
                $scope.mProspect_Prospect.CustomerTypeDesc = $scope.mProspect_Prospect.CustomerTypeDesc;
            }

            if ($scope.online == false) {
                $scope.optionsKategoriPelanggan = JSON.parse(LocalService.get('KategoriPelanggan'));
            } else {
                KategoriPelangganFactory.getData().then(function (res) {
                    $scope.optionsKategoriPelanggan = res.data.Result;
                    var saveKategoriPelanggan = JSON.stringify(res.data.Result);
                    LocalService.unset('KategoriPelanggan');
                    LocalService.set('KategoriPelanggan', saveKategoriPelanggan);
                    $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
                    //return $scope.optionsKategoriPelanggan;
                });
            }
            $scope.optionsKategoriPelangganFiltered = $scope.optionsKategoriPelanggan;
            if ($scope.mProspect_Prospect.InstanceName == "" || $scope.mProspect_Prospect.InstanceName == null) {
                //$scope.mProspect_Prospect.Prospect_Type="Individu";
                $scope.ProspectIndividu = true;
                $scope.ProspectInstansi = false;

                for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
                    if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc != "Individu") {
                        $scope.optionsKategoriPelangganFiltered.splice(i, 1);
                    }
                }
            } else if ($scope.mProspect_Prospect.InstanceName != "" || $scope.mProspect_Prospect.InstanceName != null) {
                //$scope.mProspect_Prospect.Prospect_Type="Instansi";
                $scope.ProspectIndividu = false;
                $scope.ProspectInstansi = true;

                for (var i = 0; i < $scope.optionsKategoriPelangganFiltered.length; ++i) {
                    if ($scope.optionsKategoriPelangganFiltered[i].CustomerTypeDesc == "Individu") {
                        $scope.optionsKategoriPelangganFiltered.splice(i, 1);
                    }
                }
            }



            $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = "";
            //$scope.List_Prospect_InformasiKendaraanLain_To_Repeat = "";

            $scope.Show_mProspect_Prospect_KategoriProspect = true;
            $scope.Show_mProspect_Prospect_KategoriAwal = false;
            $scope.Show_mProspect_Prospect_AlasanPenurunan = false;

            if ($scope.From_Page == "Pelanggan") {
                //ini tadinya buat masukin ke prospect tapi sejak pake anglar copy ga dipake
            }


            $scope.Prospect_InformasiUnitYangDiminati_Action_ShowHide = true;


            //disini disable semua



            $scope.mProspect_Prospect_MetodePembayaran_EnableDisable = true;
            $scope.mProspect_Prospect_TingkatKebutuhan_EnableDisable = true;
            $scope.mProspect_Prospect_KategoriProspect_EnableDisable = true;
            $scope.mProspect_Prospect_KeperluanPenggunaanKendaraan_EnableDisable = true;

            $scope.mProspect_Prospect_NamaInstansi_EnableDisable = true;
            $scope.mProspect_Prospect_NamaPenanggungJawab_EnableDisable = true;
            $scope.mProspect_Prospect_AlamatInstansi_EnableDisable = true;
            $scope.mProspect_Prospect_RT_EnableDisable = true; //fitri
            $scope.mProspect_Prospect_RW_EnableDisable = true; //fitri
            $scope.mProspect_Prospect_NoTeleponInstansi_EnableDisable = true;
            $scope.mProspect_Prospect_NoHandphonePenanggungJawab_EnableDisable = true;
            $scope.mProspect_Prospect_EmailPenanggungJawab_EnableDisable = true;

            $scope.mProspect_Prospect_ToyotaId_EnableDisable = true;
            $scope.mProspect_Prospect_KategoriPelanggan_EnableDisable = true;
            //$scope.mProspect_Prospect_KategoriFleet_EnableDisable = true;
            $scope.mProspect_Prospect_NamaLengkap_EnableDisable = true;
            $scope.mProspect_Prospect_NamaPanggilan_EnableDisable = true;
            $scope.mProspect_Prospect_NoTeleponRumah_EnableDisable = true;
            $scope.mProspect_Prospect_NoHandphone_EnableDisable = true;
            $scope.mProspect_Prospect_Email_EnableDisable = true;
            $scope.mProspect_Prospect_AlamatSesuaiIdentitas_EnableDisable = true;
            $scope.mProspect_Prospect_Provinsi_EnableDisable = true;
            $scope.mProspect_Prospect_KabupatenKota_EnableDisable = true;
            $scope.mProspect_Prospect_Kecamatan_EnableDisable = true;
            $scope.mProspect_Prospect_Kelurahan_EnableDisable = true;
            $scope.mProspect_Prospect_KodePos_EnableDisable = true;
            $scope.mProspect_Prospect_TanggalLahir_EnableDisable = true;
            $scope.mProspect_Prospect_Hobi_EnableDisable = true;
            $scope.mProspect_Prospect_Agama_EnableDisable = true;
            $scope.mProspect_Prospect_Suku_EnableDisable = true;
            $scope.mProspect_Prospect_Pekerjaan_EnableDisable = true;
            $scope.mProspect_Prospect_Jabatan_EnableDisable = true;
            $scope.mProspect_Prospect_NamaPerusahaan_EnableDisable = true;
            $scope.mProspect_Prospect_TeleponKantor_EnableDisable = true;
            $scope.mProspect_Prospect_AlamatPerusahaan_EnableDisable = true;
            $scope.mProspect_Prospect_Catatan_EnableDisable = true;



            $scope.ShowProspectDetail = true;
            $scope.ShowLihatPengecekanData = false;
            $scope.Prospect_ActionFooter_Button = false;
            $scope.ShowPengecekanData = false; //ati ati
            // $scope.ModalPengecekanDataUntukProspect = { show: false }; //ati ati
            angular.element('.ui.modal.ModalPengecekanDataUntukProspect').modal('hide');
            $scope.SimpanEditDetailProspect_Button = true; //ini mungkin nanti pake button laen
            $scope.BatalEditDetailProspect_Button = true; //ini mungkin nanti pake button laen
            $scope.ProspectEditButtonToggle = false;
            $scope.Prospect_InformasiUnitYangDiminati_Tambah_Kendaraan_Button = true;
            $scope.Prospect_InformasiKendaraanLain_Add_Vehicle_Button = true;
            //point of interest From_Page_Prospect_Or_Not
        };

        $scope.controlData = 'SimpanProspect';
        $scope.dataDrop = 'UpdateNonCategory';
        $scope.listUnitYangDiminati = undefined;
        $scope.SaveValidasiPelwil = function () {
            $scope.disabledSaveValidasiPelwil = true;
            if($scope.online == false){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Pastikan anda terhubung dengan jaringan.",
                    type: 'warning'
                });
                $scope.disabledSaveValidasiPelwil = false;
            }else{

            ProspectFactory.getPelanggaranWilayah('?ProvinceId=' + $scope.mProspect_Prospect.ProvinceId + '&CityRegencyId=' + $scope.mProspect_Prospect.CityRegencyId + '&DistrictId=' + $scope.mProspect_Prospect.DistrictId).then(function (res) {
                $scope.Pelwil = res.data.Result[0];
                //console.log('$scope.Pelwil',$scope.Pelwil.PelanggaranWilayahBit);
                // $scope.Pelwil.PelanggaranWilayahBit = true;
                if ($scope.Pelwil.PelanggaranWilayahBit == false) {
                   
                        $scope.SimpanEditDetailProspect();
                        // $scope.disabledSaveValidasiPelwil = false;
                } else {
                    setTimeout(function () {
                        angular.element('.ui.modal.ModalPelanggaranWilayah').modal('refresh');
                    }, 0);
                    angular.element('.ui.modal.ModalPelanggaranWilayah').modal('show');
                }
            },
            function (err) {
               
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: 'Field provinsi dan kabupaten harus diisi',
                        type: 'warning'
                    }
                );
                $scope.disabledSaveValidasiPelwil = false;
            }
            );
        }
        }

        $scope.KembaliPelwil = function () {
            angular.element('.ui.modal.ModalPelanggaranWilayah').modal('hide');
        }

        //tambahan
        $scope.controlData_SimpanSuspect = function () {
            var objectProspect = angular.merge({}, $scope.mProspect_Prospect, $scope.saveFlagCustType)
            
            objectProspect.CProspectDetailUnitView = [];
                objectProspect.CProspectDetailOtherVehicleView = [];

                if (objectProspect.CustomerTypeDesc == "Individu") {

                    objectProspect.CustomerTypeId = 1;
                }


                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = angular.copy($scope.listUnitYangDiminati);


                if ($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat.length == 0) {
                    objectProspect.CProspectDetailUnitView = [];
                } else {
                    objectProspect.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                }
                //objectProspect.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                objectProspect.CProspectDetailOtherVehicleView = $scope.List_Prospect_InformasiKendaraanLain_To_Repeat;



                var objectProspectdetailunit = angular.merge({}, objectProspect)
                try {
                    objectProspectdetailunit.Birthdate = objectProspectdetailunit.Birthdate.getFullYear() + '-' +
                        ('0' + (objectProspectdetailunit.Birthdate.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + objectProspectdetailunit.Birthdate.getDate()).slice(-2) + 'T' +
                        ((objectProspectdetailunit.Birthdate.getHours() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getHours()) + ':' +
                        ((objectProspectdetailunit.Birthdate.getMinutes() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getMinutes()) + ':' +
                        ((objectProspectdetailunit.Birthdate.getSeconds() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getSeconds());

                    objectProspectdetailunit.Birthdate.slice(0, 10);
                } catch (e1) {
                    objectProspectdetailunit.Birthdate = null;
                }
                objectProspectdetailunit.CategoryFleetId = 2;
                SuspectFactory.createSaveToProspect(objectProspectdetailunit).then(
                    function (res) {
                        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Suspect_To_Repeat = res.data.Result;
                        })

                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;

                        })
                        bsNotify.show({
                            title: "Sukses",
                            content: "Data prospect berhasil disimpan",
                            type: 'success'
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                        $scope.listUnitYangDiminati = undefined;

                        $scope.clearAllComboBox();
                        $scope.ShowProspectDetail = false;
                        $scope.formData = true;

                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Input data prospect gagal.",
                            type: 'danger'
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                    }
                )
            

        }


        $scope.controlData_SimpanProspect = function () {
            var objectProspect = angular.merge({}, $scope.mProspect_Prospect, $scope.saveFlagCustType)

            
                objectProspect.CProspectDetailUnitView = [];
                objectProspect.CProspectDetailOtherVehicleView = [];

                if (objectProspect.CustomerTypeDesc == "Individu") {

                    objectProspect.CustomerTypeId = 1;
                } else {
                    objectProspect.CustomerTypeId = $scope.mProspect_Prospect.CustomerTypeId;
                }

                $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = angular.copy($scope.listUnitYangDiminati);

                


                if ($scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat.length == 0) {
                    objectProspect.CProspectDetailUnitView = [];
                } else {
                    objectProspect.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                }
                //objectProspect.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                objectProspect.CProspectDetailOtherVehicleView = $scope.List_Prospect_InformasiKendaraanLain_To_Repeat;



                var objectProspectdetailunit = angular.merge({}, objectProspect)
                try {
                    objectProspectdetailunit.Birthdate = objectProspectdetailunit.Birthdate.getFullYear() + '-' +
                        ('0' + (objectProspectdetailunit.Birthdate.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + objectProspectdetailunit.Birthdate.getDate()).slice(-2) + 'T' +
                        ((objectProspectdetailunit.Birthdate.getHours() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getHours()) + ':' +
                        ((objectProspectdetailunit.Birthdate.getMinutes() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getMinutes()) + ':' +
                        ((objectProspectdetailunit.Birthdate.getSeconds() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getSeconds());

                    objectProspectdetailunit.Birthdate.slice(0, 10);
                } catch (e1) {
                    objectProspectdetailunit.Birthdate = null;
                }
                objectProspectdetailunit.CategoryFleetId = 2;
                SuspectFactory.createSaveToProspect(objectProspectdetailunit).then(
                    function (res) {
                        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Suspect_To_Repeat = res.data.Result;
                        })

                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;

                        })
                        bsNotify.show({
                            title: "Sukses",
                            content: "Data prospect berhasil disimpan",
                            type: 'success'
                        });
                        $scope.disabledSaveValidasiPelwil = false;

                        $scope.listUnitYangDiminati = undefined;

                        $scope.clearAllComboBox();
                        $scope.ShowProspectDetail = false;
                        $scope.formData = true;

                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Input data prospect gagal.",
                            type: 'danger'
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                    }
                )
            
        }


        $scope.controlData_DalemanUpdateProspect = function () {
            var objectProspectDetil = angular.merge({}, $scope.mProspect_Prospect)

           
            objectProspectDetil.CProspectDetailUnitView = [];
            objectProspectDetil.CProspectDetailOtherVehicleView = [];

            $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat = angular.copy($scope.listUnitYangDiminati);

            objectProspectDetil.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
            objectProspectDetil.CProspectDetailOtherVehicleView = $scope.List_Prospect_InformasiKendaraanLain_To_Repeat;

            var objectProspectdetailunit = angular.merge({}, objectProspectDetil)

            try {
                objectProspectdetailunit.Birthdate = objectProspectdetailunit.Birthdate.getFullYear() + '-' +
                    ('0' + (objectProspectdetailunit.Birthdate.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + objectProspectdetailunit.Birthdate.getDate()).slice(-2) + 'T' +
                    ((objectProspectdetailunit.Birthdate.getHours() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getHours()) + ':' +
                    ((objectProspectdetailunit.Birthdate.getMinutes() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getMinutes()) + ':' +
                    ((objectProspectdetailunit.Birthdate.getSeconds() < '10' ? '0' : '') + objectProspectdetailunit.Birthdate.getSeconds());

                objectProspectdetailunit.Birthdate.slice(0, 10);
            } catch (e1) {
                objectProspectdetailunit.Birthdate = null;
            }


            ProspectFactory.update(objectProspectdetailunit).then(function () {

                SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                    $scope.List_Suspect_To_Repeat = res.data.Result;
                })

                //return $scope.List_Suspect_To_Repeat;
                ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                    $scope.List_Prospect_To_Repeat = res.data.Result;
                    //return $scope.List_Prospect_To_Repeat;
                })
                PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                    $scope.List_Pelanggan_To_Repeat = res.data.Result;
                    //return $scope.List_Prospect_To_Repeat;
                })

                


                $scope.clearAllComboBox();
                $scope.ShowProspectDetail = false;
                $scope.formData = true;

            
                bsNotify.show({
                    title: "Sukses",
                    content: "Data prospect berhasil disimpan",
                    type: 'success'
                });
                $scope.disabledSaveValidasiPelwil = false;  

                
                

            },
                function (err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: "Update data prospect gagal.",
                        type: 'danger'
                    });
                    $scope.disabledSaveValidasiPelwil = false;
                }
            )

        }

        $scope.prosesUpdateProspect = function () {

            

            if ($scope.listUnitYangDiminati == null || $scope.listUnitYangDiminati == '' || $scope.listUnitYangDiminati == undefined) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Unit yang diminati harus dilengkapi.",
                    type: 'warning'
                });
                $scope.disabledSaveValidasiPelwil = false;
            } else {
                if ($scope.mProspect_Prospect.CategoryProspectId == 1) { //hot
                    console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);

                    // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                        $scope.listUnitYangDiminati[0].SuffixCode == null ||
                        $scope.listUnitYangDiminati[0].ColorName == null ||
                        $scope.listUnitYangDiminati[0].Qty == null) {
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Unit yang diminati belum lengkap Model, Type, Warna dan Qty harus dilengkapi",
                            type: 'warning',
                            timeout: 6000
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                    
                    } else {
                        
                        $scope.controlData_DalemanUpdateProspect();

                    }

                } else if ($scope.mProspect_Prospect.CategoryProspectId == 2) { //medium
                    console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);

                    // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                        $scope.listUnitYangDiminati[0].SuffixCode == null) {
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Unit yang diminati belum lengkap Model dan Type harus dilengkapi",
                            type: 'warning',
                            timeout: 6000
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                    } else {

                        $scope.controlData_DalemanUpdateProspect();

                    }
                } else {
                    // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null) { //low
                        console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);


                        bsNotify.show({
                            title: "Peringatan",
                            content: "Unit yang diminati belum lengkap Model harus dilengkapi",
                            type: 'warning',
                            timeout: 6000
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                       
                        
                    } else {
                        

                        $scope.controlData_DalemanUpdateProspect();

                    }
                }
            }
        }

        
        $scope.SimpanEditDetailProspect = function () {
            var tempCheck = 1; //$scope.ModalDataHarusDiisi = { show: true };
            angular.element('.ui.modal.ModalPelanggaranWilayah').modal('hide');

            
            if (tempCheck == 1) {
                if ($scope.controlData == 'SimpanProspect') {
                    console.log('ini masuk case Simpan pertamaa ===> $scope.controlData == SimpanProspect');
                    

                    console.log('ini yang data yang ada di list unit yang diminati Prospect ===>', $scope.listUnitYangDiminati);
                    console.log('$scope.mProspect_Prospect.CategoryProspectId kategori value Prospect===>', $scope.mProspect_Prospect.CategoryProspectId);

                    if ($scope.listUnitYangDiminati == null || $scope.listUnitYangDiminati == '' || $scope.listUnitYangDiminati == undefined) {
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Unit yang diminati harus dilengkapi.",
                            type: 'warning'
                        });
                        $scope.disabledSaveValidasiPelwil = false;
                    } else {
                        if ($scope.mProspect_Prospect.CategoryProspectId == 1) { //hot
                            // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                                if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                    $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                    $scope.listUnitYangDiminati[0].SuffixCode == null ||
                                    $scope.listUnitYangDiminati[0].ColorName == null ||
                                    $scope.listUnitYangDiminati[0].Qty == null) {
                                    bsNotify.show({
                                        title: "Peringatan",
                                        content: "Unit yang diminati belum lengkap Model, Type, Warna dan Qty harus dilengkapi",
                                        type: 'warning',
                                        timeout: 6000
                                    });
                                    $scope.disabledSaveValidasiPelwil = false;
                                } else {

                                    $scope.controlData_SimpanProspect();


                                }
                        } else if ($scope.mProspect_Prospect.CategoryProspectId == 2) { //medium
                            // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                                if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                    $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                    $scope.listUnitYangDiminati[0].SuffixCode == null) {
                                    bsNotify.show({
                                        title: "Peringatan",
                                        content: "Unit yang diminati belum lengkap Model dan Type harus dilengkapi",
                                        type: 'warning',
                                        timeout: 6000
                                    });
                                    $scope.disabledSaveValidasiPelwil = false;
                                } else {

                                    $scope.controlData_SimpanProspect();

                                }
                        } else {
                            // for (var i = 0; i < $scope.listUnitYangDiminati.length; i++)
                                if ($scope.listUnitYangDiminati[0].VehicleModelName == null) { //low

                                    bsNotify.show({
                                        title: "Peringatan",
                                        content: "Unit yang diminati belum lengkap Model harus dilengkapi",
                                        type: 'warning',
                                        timeout: 6000
                                    });
                                    $scope.disabledSaveValidasiPelwil = false;
                                } else {

                                    $scope.controlData_SimpanProspect();

                                }
                        }
                    }



                } else if ($scope.controlData == 'SimpanSuspect') {
                        console.log('ini masuk case Simpan Keduaaa ===> $scope.controlData == SimpanSuspect');

                        console.log('ini yang data yang ada di list unit yang diminati ===>', $scope.listUnitYangDiminati);
                        console.log('$scope.mProspect_Prospect.CategoryProspectId kategori value===>', $scope.mProspect_Prospect.CategoryProspectId);

                        if ($scope.listUnitYangDiminati == null || $scope.listUnitYangDiminati == '' || $scope.listUnitYangDiminati == undefined) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "Unit yang diminati harus dilengkapi.",
                                type: 'warning'
                            });
                            $scope.disabledSaveValidasiPelwil = false;
                        } else {
                            if ($scope.mProspect_Prospect.CategoryProspectId == 1){ //hot
                                
                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                        $scope.listUnitYangDiminati[0].SuffixCode == null ||
                                        $scope.listUnitYangDiminati[0].ColorName == null ||
                                        $scope.listUnitYangDiminati[0].Qty == null )
                                    {
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model, Type, Warna dan Qty harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                    }else{
                                    
                                            $scope.controlData_SimpanSuspect();
                                        
                                        
                                    }
                            }else if ($scope.mProspect_Prospect.CategoryProspectId == 2) { //medium
                                
                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                        $scope.listUnitYangDiminati[0].SuffixCode == null) 
                                    {
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model dan Type harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                    } else {
                                    
                                            $scope.controlData_SimpanSuspect();
                                        
                                    }
                            }else{
                                
                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ) { //low
                                        
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                    } else {
                                    
                                            $scope.controlData_SimpanSuspect();
                                        
                                    }
                            }
                        }


                        
                       
                } else if ($scope.controlData == 'UpdateProspect') {
                    console.log('ini masuk case Simpan Ketiga ===> $scope.controlData == UpdateProspect');
                    $scope.isDownCategory = false;
                    $scope.alesanMunculDariMana = "Update Prospect";



                    if ($scope.mProspect_Prospect.BeforeCategoryProspectId == 1) { //hot

                        if ($scope.mProspect_Prospect.CategoryProspectId > $scope.mProspect_Prospect.BeforeCategoryProspectId) {
                            console.log('kategori prospect Tadinya ===>', $scope.mProspect_Prospect.BeforeCategoryProspectId, ' jadi->', $scope.mProspect_Prospect.CategoryProspectId);
                            
                            $scope.mProspect_Prospect.ReasonDropProspectId = null;
                            $scope.mProspect_Prospect_AlasanDrop_Textbox = null;
                            
                            $scope.ShowDropProspectReason = true;
                            $scope.ShowProspectDetail = false;

                            console.log('ini downgrade');


                            $scope.isDownCategory = true;

                            if ($scope.listUnitYangDiminati == null || $scope.listUnitYangDiminati == '' || $scope.listUnitYangDiminati == undefined) {
                                bsNotify.show({
                                    title: "Peringatan",
                                    content: "Unit yang diminati harus dilengkapi.",
                                    type: 'warning'
                                });
                                $scope.disabledSaveValidasiPelwil = false;
                            } else {
                                if ($scope.mProspect_Prospect.CategoryProspectId == 1) { //hot
                                    console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);

                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                        $scope.listUnitYangDiminati[0].SuffixCode == null ||
                                        $scope.listUnitYangDiminati[0].ColorName == null ||
                                        $scope.listUnitYangDiminati[0].Qty == null) {
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model, Type, Warna dan Qty harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                        $scope.ShowDropProspectReason = false;
                                        $scope.ShowProspectDetail = true;
                                    } else {
                                        $scope.dataDrop = 'UpdateCategory';
                                    }

                                } else if ($scope.mProspect_Prospect.CategoryProspectId == 2) { //medium
                                    console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);

                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null ||
                                        $scope.listUnitYangDiminati[0].KatashikiCode == null ||
                                        $scope.listUnitYangDiminati[0].SuffixCode == null) {
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model dan Type harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                        $scope.ShowDropProspectReason = false;
                                        $scope.ShowProspectDetail = true;
                                    } else {
                                        $scope.dataDrop = 'UpdateCategory';                                    }
                                } else {
                                    if ($scope.listUnitYangDiminati[0].VehicleModelName == null) { //low
                                        console.log('Kategore Value ===>', $scope.mProspect_Prospect.CategoryProspectId, $scope.mProspect_Prospect.CategoryProspectName);


                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Unit yang diminati belum lengkap Model harus dilengkapi",
                                            type: 'warning',
                                            timeout: 6000
                                        });
                                        $scope.disabledSaveValidasiPelwil = false;
                                        $scope.ShowDropProspectReason = false;
                                        $scope.ShowProspectDetail = true;
                                    } else {
                                        $scope.dataDrop = 'UpdateCategory';
                                    }
                                }
                            }
                            
                            
                        } else {
                            console.log('kategori prospect Tadinya ===>', $scope.mProspect_Prospect.BeforeCategoryProspectId, ' jadi->', $scope.mProspect_Prospect.CategoryProspectId);
                            $scope.prosesUpdateProspect();
                            console.log(' ini upgrade /  gaberubah samsek');
                        }

                    } else { //medium atau low
                        console.log('kategori prospect Tadinya ===>', $scope.mProspect_Prospect.BeforeCategoryProspectId, ' jadi->', $scope.mProspect_Prospect.CategoryProspectId);
                        console.log('ketiga update');
                        $scope.prosesUpdateProspect();
                    }

                    
                    

                }
            }
        
        };

        $scope.RefreshListOffline = function () {
            ProspectFactory.getProspectOffline().then(
                function (res) {
                    $scope.ProspectOffline = res;

                });
        }

        $scope.SaveAllOfflineProspect = function () {
            ProspectFactory.getProspectOffline().then(
                function (data) {

                    for (var i in data) {
                        ProspectFactory.createSaveOffline(data[i]).then(
                            function (res) {
                                ProspectFactory.clearProspectOffline().then(function (res) {

                                    SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                                        $scope.List_Suspect_To_Repeat = res.data.Result;
                                    })

                                    ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                                        $scope.List_Prospect_To_Repeat = res.data.Result;
                                        //return $scope.List_Prospect_To_Repeat;
                                    });

                                })

                                bsNotify.show({
                                    title: "Sukses",
                                    content: "Data suspect berhasil disimpan",
                                    type: 'success'
                                });

                            },
                            function (err) {

                                bsNotify.show({
                                    title: "Gagal",
                                    content: "Input data prospect gagal.",
                                    type: 'danger'
                                });
                            }
                        )
                    }
                });
        }

        $scope.SaveProspect = function () {
            $scope.disabledSaveProspect = true;
            $scope.mProspect_BuatProspect.CustomerTypeId = $scope.flagCustType.CustomerTypeId;
            $scope.mProspect_BuatProspect.CProspectDetailUnitView = [];
            $scope.mProspect_BuatProspect.CProspectDetailOtherVehicleView = [];
            $scope.mProspect_BuatProspect.CategoryProspectId = 3;
            $scope.mProspect_BuatProspect.PurchaseTimeId = 3;


            if ($scope.online == false) {

                ProspectFactory.createOffline($scope.mProspect_BuatProspect).then(
                    function (res) {

                        $scope.RefreshListOffline();
                        // ProspectFactory.getData('?start=1&limit=15').then(function(res) {
                        //     $scope.List_Prospect_To_Repeat = res.data.Result;

                        // })
                        $scope.clearAllComboBox();

                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Input data prospect gagal.",
                            type: 'danger'
                        });
                        $scope.disabledSaveProspect = false;
                    }
                )
            } else {
                //$scope.SaveAllOfflineProspect();
                ProspectFactory.createSaveOffline($scope.mProspect_BuatProspect).then(
                    function (res) {
                        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Suspect_To_Repeat = res.data.Result;
                        })
                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        $scope.clearAllComboBox();
                        $scope.disabledSaveProspect = false;
                        bsNotify.show({
                            title: "Sukses",
                            content: "Data suspect berhasil disimpan",
                            type: 'success'
                        });
                        $scope.disabledSaveProspect = false;
                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Input data prospect gagal.",
                            type: 'danger'
                        });
                        $scope.disabledSaveProspect = false;
                    }
                )
            }
            $scope.ShowBuatProspect = false;
            $scope.formData = true;
            $scope.disabledSaveProspect = false;


        }

        $scope.CustomerCategoryValue = function (selecTypeCustomer) {
            $scope.SelectedTypeCustomer = selecTypeCustomer;
            $scope.saveFlagCustType.CustomerTypeId = selecTypeCustomer.CustomerTypeId;
        }


        $scope.Execute_SimpanEditDetailProspect = function (mProspect_Prospect) {

            if ($scope.selectTab == "Prospect") {

                if ($scope.mProspect_Prospect.CategoryProspectId != $scope.mProspect_Prospect.BeforeCategoryProspectId) { //ini kalo kategori prospect brubah

                    if ($scope.mProspect_Prospect.CategoryProspectId > $scope.mProspect_Prospect.BeforeCategoryProspectId) { //ini kalo kategori prospect brubah
                        //alert('nonglin tu benda yang mau apa kaga di drop baru update');


                        $scope.mProspect_Prospect_AlasanDrop_Textbox = "";

                        $scope.ShowProspectDetail = false;
                        //$scope.formData = true;
                        $scope.ShowDropProspectReason = true;
                        // $scope.ShowProspectDetail = false;

                    } else {
                        $scope.Simpan_If_Kategori_Prospek_Berubah();
                    }

                } else { //ini kalo kategori prospect ga brubah
                    //alert('update detail prospect');

                    var objectProspectDetil = angular.merge({}, $scope.mProspect_Prospect)

                    objectProspectDetil.CProspectDetailUnitView = [];
                    objectProspectDetil.CProspectDetailOtherVehicleView = [];

                    objectProspectDetil.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                    objectProspectDetil.CProspectDetailOtherVehicleView = $scope.List_Prospect_InformasiKendaraanLain_To_Repeat;



                    var objectProspectdetailunit = angular.merge({}, objectProspectDetil)

                    ProspectFactory.update(objectProspectdetailunit).then(function () {

                        SuspectFactory.getData('?start=1&limit=15').then(function (res) { })
                        $scope.List_Suspect_To_Repeat = res.data.Result;
                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })

                        $scope.ShowProspectDetail = false;
                        $scope.formData = true;

                    }),
                        function (err) {

                            bsNotify.show({
                                title: "Gagal",
                                content: "Update data prospect gagal.",
                                type: 'danger'
                            });
                        }



                }
            } else {
                //alert('ini insert ke prospect abis dari pelanggan ataususpect');


                var objectProspect = angular.merge({}, $scope.mProspect_Prospect, $scope.saveFlagCustType)

                objectProspect.CProspectDetailUnitView = [];
                objectProspect.CProspectDetailOtherVehicleView = [];

                if (objectProspect.CustomerTypeDesc == "Individu") {

                    objectProspect.CustomerTypeId = 1;
                }


                objectProspect.CProspectDetailUnitView = $scope.List_Prospect_InformasiUnitYangDiminati_To_Repeat;
                objectProspect.CProspectDetailOtherVehicleView = $scope.List_Prospect_InformasiKendaraanLain_To_Repeat;


                var objectProspectdetailunit = angular.merge({}, objectProspect)

                if (objectProspectdetailunit.SuspectId != null) {

                    SuspectFactory.createSaveToProspect(objectProspectdetailunit).then(
                        function (res) {
                            SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Suspect_To_Repeat = res.data.Result;
                            })

                            //return $scope.List_Suspect_To_Repeat;
                            ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Prospect_To_Repeat = res.data.Result;
                                //return $scope.List_Prospect_To_Repeat;
                            })
                            PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Pelanggan_To_Repeat = res.data.Result;
                                //return $scope.List_Prospect_To_Repeat;
                            })



                        }),
                        function (err) {

                            bsNotify.show({
                                title: "Gagal",
                                content: "Input data prospect gagal.",
                                type: 'danger'
                            });
                        }

                } else {

                    ProspectFactory.create(objectProspectdetailunit).then(
                        function (res) {
                            SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Suspect_To_Repeat = res.data.Result;
                            })

                            //return $scope.List_Suspect_To_Repeat;
                            ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Prospect_To_Repeat = res.data.Result;
                                //return $scope.List_Prospect_To_Repeat;
                            })
                            PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                                $scope.List_Pelanggan_To_Repeat = res.data.Result;
                                //return $scope.List_Prospect_To_Repeat;
                            })


                            $scope.ShowProspectDetail = false;
                            $scope.formData = true;
                        }),
                        function (err) {

                            bsNotify.show({
                                title: "Gagal",
                                content: "Input data prospect gagal.",
                                type: 'danger'
                            });
                        }
                }




            }
            $scope.ModalPelanggaranWilayah = { show: false };
            $scope.List_Prospect_InformasiKendaraanLain_To_Repeat = [];

        };

        $scope.TidakExecute_SimpanEditDetailProspect = function () {
            $scope.ModalPelanggaranWilayah = { show: false };
        };

        $scope.okKonfiramsi = function(){
            var numLength = angular.element('.ui.modal.konfirmasidropprospect').length;
            setTimeout(function(){
                angular.element('.ui.modal.konfirmasidropprospect').modal('refresh');
            });
            if(numLength > 1){
                angular.element('.ui.modal.konfirmasidropprospect').not(':first').remove();
            }
            angular.element('.ui.modal.konfirmasidropprospect').modal("show");
        };

        $scope.BatalKonfiramsi = function(){
            angular.element('.ui.modal.konfirmasidropprospect').modal("hide");
        }


        $scope.SimpanAlasanDrop = function () {
            var IdRevisi = $scope.mProspect_Prospect.ReasonDropProspectId;
            var AnotherReason = $scope.mProspect_Prospect_AlasanDrop_Textbox;
            var KategoriProspect = $scope.mProspect_Prospect.CategoryProspectId;
            //var PurchaseTime = $scope.mProspect_Prospect.PurchaseTimeId;
            angular.element('.ui.modal.konfirmasidropprospect').modal("hide");
            
            $scope.Prospect_Selected.ReasonDropProspectId = IdRevisi;
            $scope.Prospect_Selected.AnotherReason = AnotherReason;
            $scope.Prospect_Selected.CategoryProspectId = KategoriProspect;
           // $scope.Prospect_Selected.PurchaseTimeId = PurchaseTime;

            $scope.mProspect_Prospect.ReasonDropProspectId = IdRevisi;
            $scope.mProspect_Prospect.AnotherReason = AnotherReason;
            $scope.mProspect_Prospect.CategoryProspectId = KategoriProspect;

            console.log('$scope.mProspect_Prospect',$scope.mProspect_Prospect);
            if ($scope.dataDrop == 'UpdateNonCategory') {


                ProspectFactory.dropProspectToApproval($scope.Prospect_Selected).then(
                    function (res) {
                        angular.element('.ui.modal.konfirmasidropprospect').modal("hide");
                        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Suspect_To_Repeat = res.data.Result;
                        })

                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })



                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Drop prospect gagal.",
                            type: 'danger'
                        });
                    }
                
                
                );

                $scope.ShowDropProspectReason = false;
                $scope.formData = true;
            } else
                if ($scope.dataDrop == 'UpdateCategory') {

                    ProspectFactory.update($scope.mProspect_Prospect).then(function () {
                        angular.element('.ui.modal.konfirmasidropprospect').modal("hide");
                        SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Suspect_To_Repeat = res.data.Result;
                        })

                        //return $scope.List_Suspect_To_Repeat;
                        ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Prospect_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })
                        PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                            $scope.List_Pelanggan_To_Repeat = res.data.Result;
                            //return $scope.List_Prospect_To_Repeat;
                        })


                        bsNotify.show({
                            title: "Sukses",
                            content: "Data prospect berhasil disimpan",
                            type: 'success'
                        });
                        


                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: "Drop prospect gagal.",
                            type: 'danger'
                        });
                    }
                
                )
                    $scope.ShowDropProspectReason = false;
                    $scope.formData = true;

                }


        };

        $scope.Simpan_If_Kategori_Prospek_Berubah = function () {
            //alert('ke save');
            if ($scope.mProspect_Prospect.CategoryProspectId > $scope.mProspect_Prospect.BeforeCategoryProspectId) {
                $scope.mProspect_Prospect['AnotherReason'] = $scope.AlasanDropSelected + " \n" + $scope.mProspect_Prospect_AlasanDrop_Textbox;
            } else {
                $scope.mProspect_Prospect['AnotherReason'] = "";
            }

            ProspectFactory.update($scope.mProspect_Prospect).then(function () {
                angular.forEach($scope.Prospect_Selected, function (value, key) {
                    $scope.Prospect_Selected[key] = $scope.mProspect_Prospect[key];
                });
                if ($scope.mProspect_Prospect.CategoryProspectId > $scope.mProspect_Prospect.BeforeCategoryProspectId) {
                    $scope.Prospect_Selected['AnotherReason'] = $scope.AlasanDropSelected + "\n" + $scope.mProspect_Prospect_AlasanDrop_Textbox;
                } else {
                    $scope.Prospect_Selected['AnotherReason'] = "";
                }

            });




        };

        $scope.DropProspect = function (SelectedData) {
            $scope.mProspect_Prospect = {};
            $scope.mProspect_Prospect.ReasonDropProspectId = null;
            $scope.mProspect_Prospect_AlasanDrop_Textbox = null;
            $scope.dataDrop = 'UpdateNonCategory';
            $scope.Prospect_Selected = SelectedData;

            $scope.formData = false;
            $scope.ShowProspectDetail = false;
            $scope.ShowDropProspectReason = true;
            angular.element('.ui.modal.ModalProspect').modal('hide');
            angular.element('.ui.modal.ModalAction').modal('hide');

        };

        $scope.Prospect_To_QualifyingTool_Clicked = function () {
            $scope.ShowQualifyingTools = true;
            $scope.ShowProspectDetail = false;
        };

        $scope.Prospect_To_ProspectRiwayat_Clicked = function () {
            $scope.ShowProspectRiwayat = true;
            $scope.ShowProspectDetail = false;
            //assign repeat buat riwayat
            ProspectRiwayatFactory.getDataByProspectId($scope.mProspect_Prospect.ProspectId).then(function (res) {
                $scope.List_ProspectRiwayat_To_Repeat = res.data.Result;
                return $scope.List_ProspectRiwayat_To_Repeat;
            });
        };

        $scope.PelangganBatalUbahInformasiKendaraanLainClicked = function () {
            $scope.ShowPelangganDetail = true;
            $scope.Show_Prospect_TambahUbah_InformasiKendaraanLain = false;
            
        };

        $scope.HideDropProspectReason = function () {

            //langsung balik ke mainmenu
            if ($scope.alesanMunculDariMana == "Update Prospect"){
                console.log('$scope.alesanMunculDariMana ===> ', $scope.alesanMunculDariMana);



                    // $scope.SelectProspect($scope.mProspect_ProspectBalikinKeawal);
                    
                    $scope.ShowProspectDetail = true;
                    $scope.ShowDropProspectReason = false;
                    $scope.formData = false;
                    $scope.SelectProspect($scope.mProspect_Prospect);
                    
                    $scope.Prospect_Toggle_Edit_Detail_Prospect();



            }else{
                console.log('$scope.alesanMunculDariMana ===> ', $scope.alesanMunculDariMana);
                    $scope.ShowDropProspectReason = false;
                    $scope.formData = true;
            }
            



        };


        $scope.ActionModal = function () {
            angular.element('.ui.modal.ModalAction').modal('show');
            angular.element('.ui.modal.ModalAction').not(':first').remove();
            //$scope.ModalAction = { show: true };
        };

        var isAddProspect = false;
        $scope.AddProspect = function () {

            if ($scope.online == false) {
                $scope.optionsSumberProspect = JSON.parse(LocalService.get('ProspectSource'));
                $scope.province = JSON.parse(LocalService.get('Province'));
            }

            isAddProspect = true
            $scope.dataCPD = [];

            $scope.controlData = 'SimpanProspect';
            $scope.controlLihat = 'Buat';
            $scope.formData = false;
            $scope.ShowPilihProspect = true;
            $scope.selectedProvince = {};
            $scope.selectedProvince.Province = {};

            $scope.selectedSumberData = {};
            $scope.selectedSumberData.ProspectSource = {};
            $scope.clearAllComboBox();


        };

        $scope.saveFlagCustType = { CustomerTypeId: null }
        $scope.ToAddProspectScreen = function (Pilihan) {

            $scope.ShowPilihProspect = false;
            $scope.ShowBuatProspect = true;
            $scope.From_Page = "";
            $scope.mProspect_BuatProspect = "";
            $scope.mProspect_PengecekanDataBaru = "";
            $scope.formValidBuat.$setUntouched();
            //$scope.mProspect_PengecekanDataBaru={};

            $scope.flagCustType = { CustomerTypeId: null }

            if (Pilihan == "Individu") {
                
                $scope.BuatProspectIndividu = true;
                $scope.BuatProspectInstansi = false;
                $scope.flagCustType.CustomerTypeId = 1;
                $scope.saveFlagCustType.CustomerTypeId = 1;
            } else if (Pilihan == "Instansi") {
                $scope.BuatProspectIndividu = false;
                $scope.BuatProspectInstansi = true;
                $scope.flagCustType.CustomerTypeId = 2;
            }



        };

        $scope.BackToDatabaseFromPilihTipeProspect = function () {
            $scope.formData = true;
            $scope.ShowPilihProspect = false;

        };

        // $scope.Back_pengecekan = 'Pengecekan';
        $scope.Back_pengecekan = 'LihatDetail';
        $scope.YaBackToDB = function () {
            if ($scope.Back_pengecekan == 'Pengecekan') {
                $scope.ShowSuspectDetail = false;
                $scope.ShowPelangganDetail = false;
                $scope.ShowProspectDetail = false;
                $scope.ShowBuatProspect = false;
                $scope.ShowPengecekanData = true;


                //ini clear suspect
                $scope.ClearSuspect();

                //ini clear pelanggan
                $scope.ClearPelanggan();

                //ini clear buat prospect	
                $scope.ClearBuatProspect();

                $scope.clearAllComboBox();
                angular.element('.ui.modal.ModalBackToDatabase').modal('hide');
            } else {
                $scope.ShowSuspectDetail = false;
                $scope.ShowPelangganDetail = false;
                $scope.ShowProspectDetail = false;
                $scope.ShowBuatProspect = false;
                $scope.formData = true;


                //ini clear suspect
                $scope.ClearSuspect();

                //ini clear pelanggan
                $scope.ClearPelanggan();

                //ini clear buat prospect	
                $scope.ClearBuatProspect();
                $scope.clearAllComboBox();
                angular.element('.ui.modal.ModalBackToDatabase').modal('hide');
            }
            $scope.Back_pengecekan = 'LihatDetail';
        }

        $scope.NoBackToDB = function () {
            angular.element('.ui.modal.ModalBackToDatabase').modal('hide');
        }


        $scope.AlasanSelectionChanged = function (AlasanSelected) {

            $scope.AlasanDropSelected = AlasanSelected.ReasonDropProspectName;
        };

        $scope.SelectedModelInfoKendaraanLainChanged = function () {



            var Filtered_Tipe = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe.length; ++i) { //mProspect_Prospect_TambahUbah_InformasiKendaraanLain.VehicleModelId
                if ($scope.RawTipe[i].VehicleModelId == $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain.VehicleModelId) {
                    Filtered_Tipe.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
                }
            }
            $scope.optionsTipe = Filtered_Tipe;
            $scope.mProspect_Prospect_TambahUbah_InformasiKendaraanLain['VehicleTypeId'] = {};
        };

        $scope.buatProspectDariAddTaskList = function () {

            ProspectFactory.getProspectIndividual($rootScope.ProspectDetailFromAddTaskList).then(function (res) {
                $scope.Prospect_Selected = res.data.Result[0];
                $scope.SelectProspect(res.data.Result[0]);
            });
            ;
        }

        $rootScope.$on("lihatDETILINFOPROSPEK", function () {
            $scope.buatProspectDariAddTaskList();
        });

        $scope.BackToTaskListFromProspect = function () {

            $scope.ShowProspectDetail = false;
            $scope.BackFromchildmethod();



        };

        $scope.BackFromchildmethod = function () {
            $rootScope.$emit("balikdariDETILINFOPROSPEK", {});
        }



        $scope.SelectedModelProspectInfoUnitYangDiminatiChanged = function () {



            var Filtered_Tipe2 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe2.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe.length; ++i) { //mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId
                if ($scope.RawTipe[i].VehicleModelId == $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati.VehicleModelId) {
                    Filtered_Tipe2.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
                }
            }
            $scope.optionsTipe_ProspectInfoUnitYangDiminati = Filtered_Tipe2;
            $scope.mProspect_Prospect_TambahUbah_InformasiUnitYangDiminati['VehicleTypeId'] = {};
        };

        /////////////////////////////////////////////////////////////////////////////
        ///// Buat Booking Unit
        /////////////////////////////////////////////////////////////////////////////

        $scope.VehicleSelectProspect = [];
        $scope.ProspectToBooking = function () {

            $scope.BookFromProspect = true;
            setTimeout(function () {
                $rootScope.$emit("buatBookingProspect", {})
            }, 2000);

            //$rootScope.BookingUnit = angular.copy($scope.getBookingUnit);
        }

        $scope.VehicleCheckedProspect = function (data) {
            
            $scope.VehicleSelectProspect.splice(0, 1);
            $scope.VehicleSelectProspect.push(data);

            $rootScope.ProsBookingVehicle = angular.copy($scope.VehicleSelectProspect);

        };
        $scope.PilihVehicleProspect = function () {
            angular.element('.ui.modal.selectVehicleProspect').modal('hide');
            $scope.formData = false;
            $scope.BookFromProspect = true;
            $scope.ProspectToBooking();
        }

        $scope.openBookingUnit = function (SelectedProsp) {
            if ($scope.ModulConfig.NEGOTIATION == false) {
                // setTimeout(function () {
                //     angular.element('.ui.modal.ModalSettingModul').modal('refresh');
                // }, 0);
                // angular.element('.ui.modal.ModalSettingModul').modal('show');

                bsNotify.show(
                    {
                        title: "Info",
                        content: "This feature is under construction.",
                        type: 'info',
                        timeout: 6000
                    }
                );
                angular.element('.ui.modal.ModalProspect').modal('hide');

            } else {


            $scope.ProspecBookingUnit = angular.copy(SelectedProsp);


            if (SelectedProsp.CategoryProspectName != "Hot") {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Status data prospect bukan hot. Silakan mengubah status prospect menjadi hot.",
                        type: 'warning'
                    }
                );
            }

            $rootScope.ProsBooking = angular.copy($scope.ProspecBookingUnit);
            console.log('$scope.ProspecBookingUnit.CProspectDetailUnitView',$scope.ProspecBookingUnit.CProspectDetailUnitView);
            $scope.vehicletoSelectProspect = [];
            $scope.vehicletoSelectProspect = $scope.ProspecBookingUnit.CProspectDetailUnitView;
            $scope.VehicleSelectProspect = [];

            angular.element('.ui.modal.ModalProspect').modal('hide');
            angular.element('.ui.modal.selectVehicleProspect').modal('show');
            angular.element('.ui.modal.selectVehicleProspect').not(':first').remove();
            $scope.ShowProspectDetail = false;
        }


        }

        $rootScope.$on("cancelBookingProspect", function () {
            //$scope.statusAction = 'DariProspect';
            SuspectFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Suspect_To_Repeat = res.data.Result;
            })

            //return $scope.List_Suspect_To_Repeat;
            ProspectFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Prospect_To_Repeat = res.data.Result;
                //return $scope.List_Prospect_To_Repeat;
            })
            PelangganFactory.getData('?start=1&limit=15').then(function (res) {
                $scope.List_Pelanggan_To_Repeat = res.data.Result;
                //return $scope.List_Prospect_To_Repeat;
            })
            $scope.BookFromProspect = false;
            $scope.Show_Pembuatan_Booking_Unit = false;
            $scope.formData = true;
            //$scope.BookingUnit = angular.copy($rootScope.BookingUnit);
            //$scope.PopulateKategory();
            //$scope.bookingUnitSPK();
        });

        $scope.KembaliPilihVehicleProspect = function () {
            angular.element('.ui.modal.selectVehicleProspect').modal('hide');
        }

        /////////////////////////////////////////////////////////////////////////////
        ///// End Booking Unit
        /////////////////////////////////////////////////////////////////////////////

        $scope.Prospect_AddTaskListFromProspect = function () {
            if ($scope.ModulConfig.DAILY_SALES == false) {
                // setTimeout(function () {
                //     angular.element('.ui.modal.ModalSettingModul').modal('refresh');
                // }, 0);
                // angular.element('.ui.modal.ModalSettingModul').modal('show');

                bsNotify.show(
                    {
                        title: "Info",
                        content: "This feature is under construction.",
                        type: 'info',
                        timeout: 6000
                    }
                );
                angular.element('.ui.modal.ModalProspect').modal('hide');
                angular.element('.ui.modal.ModalAction').modal('hide');
            } else {
                $rootScope.ProspectIdDariProspecting = $scope.Prospect_Selected.ProspectId;
                $rootScope.CategoryProspectNameDariProspecting = $scope.Prospect_Selected.CategoryProspectName;
                //$rootScope.$emit("lihatADDTASKLISTDARIPROSPECT", {});
                setTimeout(function () {
                    $rootScope.$emit("lihatADDTASKLISTDARIPROSPECT", {})
                }, 2000);
                $scope.formAddTaskList = true;
                $scope.formData = false;
                $scope.ShowProspectDetail = false;
                angular.element('.ui.modal.ModalProspect').modal('hide');
                angular.element('.ui.modal.ModalAction').modal('hide');
            }
        }

        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            var patternRegex
            if (type == 1) {
                patternRegex = /[a-zA-Z]|[ ]|[.'']/i; // NAME ONLY 
            } else if (type == 2) {
                patternRegex = /[a-zA-Z]|[0-9]|[ ]|['",.()-]|[&]/i; //ALPHANUMERIC ONLY
                // if (item.includes("*")) {
                //     event.preventDefault();
                //     return false;
                // }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $rootScope.$on("balikdariDETILINFOTASKLIST", function () {
            $scope.formAddTaskList = false;
            $scope.formData = true;
        });

        // cek online / offline



    });