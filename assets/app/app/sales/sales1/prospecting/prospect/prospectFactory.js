angular.module('app')
	.factory('ProspectFactory', function ($http, CurrentUser, $window, $q) {
		var currentUser = CurrentUser.user;
		var indexedDB = $window.indexedDB;
		
		var db = null;
		var lastIndex = 0;

		function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

		return {

			
			getDataCPD: function (param) {
				var res = $http.get('/api/sales/CDPData?start=1&limit=100&filterData=' + param);
				return res;
			},

			getData: function (param) {
				var res = $http.get('/api/sales/CProspectList' + param);
				  
				return res;
			},
			getDataProspectName: function () {
				var res = $http.get('/api/sales/CProspectList');
				return res;
			},
			getDataKlasifikasiPembelian: function () {
				var res = $http.get('/api/sales/MPurchasingClassification');
				return res;
			},
			getDaVehicleType: function(param) {
				var res=$http.get('/api/sales/MUnitVehicleTypeTomas'+param);  
				return res;
			},

			getDataConfigModule: function () {
				var res = $http.get('/api/sales/SGlobalModuleConfig/?start=1&limit=100');
				return res;
			},

			getProspectIndividual: function (ProspectId) {
				var res = $http.get('/api/sales/CProspectList/?start=1&limit=100&filterData=ProspectId|' + ProspectId);
				return res;
			},

			getDataTenor: function (LeasingId) {
				var res = $http.get('/api/sales/MProfileLeasingLeasingTenor/?start=1&limit=100&filterData=LeasingId|' + LeasingId);

				return res;
			},

			getCategoriNumberPlate: function () {
				var res = $http.get('/api/sales/PCategoryNumberPlate');
				return res;
			},

			getPaket: function () {
				var res = $http.get('/api/sales/MUnitAccessoriespackage');
				  
				return res;
			},
			getDataLocation: function () {
				var res = $http.get('/api/sales/ProspectingOutletLocation');

				return res;
			},

			getPelanggaranWilayah: function (param) {
				var res = $http.get('/api/sales/ProspectingCekPelWil/'+param);

				return res;
			},

			getDataItemAksesoris: function (param) {
				var res = $http.get('/api/sales/SNegotiationBookingUnitAccessories' + param);
					  
				return res;
			},
			getKalkulasiPDD: function (param) {
				var res = $http.get('/api/sales/PDDSimulasiKalkulasi/' + param);
					  
				return res;
			},
			getDataPengajuanDiskon: function () {
				var res = $http.get('/api/sales/negotiationapprovalrole');
					  
				return res;
			},
			simpanBookingUnit: function (BookingUnit) {
				
				return $http.post('/api/sales/BookingUnit', [BookingUnit]);
			},
			simpanBookingUnitPDD: function (BookingUnitPDD) {
				
				return $http.post('/api/sales/PDDBookingUnit/Insert', BookingUnitPDD);
			},
			create: function (Prospect) {
				return $http.post('/api/sales/CProspectList', [Prospect]);
				
			},
			createSaveOffline: function (Prospect) {
				return $http.post('/api/sales/CProspectListOffline', [Prospect]);
			},
			update: function (Prospect) {
				Prospect.ProspectDate = fixDate(Prospect.ProspectDate);
				return $http.put('/api/sales/CProspectList', [Prospect]);
			},
			editDataCustomer: function (DataCustomer) {
				return $http.put('/api/sales/CCustomerList/Update', [DataCustomer]);
			},

			getYearProspect: function () {
				var res = $http.get('/api/sales/DemandSupplyVehicleYearStock?IncludeCurrentYear=True');
				return res;
			},
			dropProspectToSuspect: function (dropProspect) {
				return $http.put('/api/sales/CSuspect/DropProspect', [dropProspect]);
			},
			dropProspectToApproval: function (dropProspect) {
				return $http.put('/api/sales/CSuspect/DropProspectApproval', [dropProspect]);
			},

			// dropProspectToApproval: function (dropProspect) {
			// 	return $http.put('/api/sales/CSuspect/DropProspectApproval', [dropProspect]);
			// },

			openIndexedDB: function () {
				var deferred = $q.defer();
				var version = 1;
				var request = indexedDB.open("PospectData", version);

				request.onupgradeneeded = function (e) {
					db = e.target.result;

					e.target.transaction.onerror = indexedDB.onerror;

					if (db.objectStoreNames.contains("Prospect")) {
						db.deleteObjectStore("Prospect");
					}

					var store = db.createObjectStore("Prospect", {
						keyPath: "IdProspect"
					});
				};

				request.onsuccess = function (e) {
					db = e.target.result;
					deferred.resolve();
				};

				request.onerror = function () {
					deferred.reject();
				};
				
				return deferred.promise;
			},

			createOffline: function (saveProspect) {
				var deferred = $q.defer();
				
				if (db === null) {
					deferred.reject("IndexDB is not opened yet!");
					
				} else {
					
					var trans = db.transaction(["Prospect"], "readwrite");
					var store = trans.objectStore("Prospect");
					lastIndex++;
					
					var request = store.put({
						"IdProspect": lastIndex,
						"CustomerTypeId": saveProspect.CustomerTypeId,
						"Email": saveProspect.Email,
						"HP": saveProspect.HP,
						"LocationEvent": saveProspect.LocationEvent,
						"PhoneNumber": saveProspect.PhoneNumber,
						"ProspectName": saveProspect.ProspectName,
						"InstanceName": saveProspect.InstanceName,
						"ProspectSourceId": saveProspect.ProspectSourceId,
						"ProspectSourceName": saveProspect.ProspectSourceName,
						"ProvinceId": saveProspect.ProvinceId,
						"CProspectDetailUnitView": [],
						"CProspectDetailOtherVehicleView": [],
						"CategoryProspectId": 3,
						"PurchaseTimeId": 3

					});

					request.onsuccess = function (e) {
						
						deferred.resolve();
					};

					request.onerror = function (e) {
						
						deferred.reject("Prospect couldn't be added!");
					};
				}
				return deferred.promise;
			},

			getProspectOffline: function () {

				var deferred = $q.defer();

				if (db === null) {
					deferred.reject("IndexDB is not opened yet!");
				}
				else {
					var trans = db.transaction(["Prospect"], "readwrite");
					var store = trans.objectStore("Prospect");
					var Prospect = [];

					// Get everything in the store;
					var keyRange = IDBKeyRange.lowerBound(0);
					var cursorRequest = store.openCursor(keyRange);

					cursorRequest.onsuccess = function (e) {
						var result = e.target.result;
						if (result === null || result === undefined) {
							deferred.resolve(Prospect);
						} else {
							Prospect.push(result.value);
							if (result.value.id > lastIndex) {
								lastIndex = result.value.id;
							}
							result.
								continue();
						}
					};

					cursorRequest.onerror = function (e) {
						
						deferred.reject("Something went wrong!!!");
					};
				}

				return deferred.promise;
			},

			clearProspectOffline: function () {
				var deferred = $q.defer();

				if (db === null) {
					deferred.reject("IndexDB is not opened yet!");
				} else {
					var trans = db.transaction(["Prospect"], "readwrite");
					var store = trans.objectStore("Prospect");

					var request = store.clear();

					request.onsuccess = function (e) {
						deferred.resolve();
					};

					request.onerror = function (e) {
						
						deferred.reject("Prospect db couldn't be clear");
					};
				}

				return deferred.promise;
			},

			dealProspectUnderConstruction: function(Prospect) {
				return $http.put('/api/sales/ProspectDeal', [{ProspectId: Prospect.ProspectId}]);
			},

			delete: function (id) {
				return $http.delete('/api/sales/CProspectList', { data: id, headers: { 'Content-Type': 'application/json' } });
			},
		}
	});
 //ddd