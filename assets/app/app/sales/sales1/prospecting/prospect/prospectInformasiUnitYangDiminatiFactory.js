angular.module('app')
  .factory('ProspectInformasiUnitYangDiminatiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(ProspectId) {
        var res=$http.get('/api/sales/ListCProspect_DetailUnitJoin/?start=1&limit=100&filterData=ProspectId|'+ProspectId);
        //console.log('res=>',res);
		var Prospect_InformasiUnitYangDiminati_Json=[
				{Id: "1",Model: 1,Tipe: 4,Warna: "Black Mica",Qty: 1},
				{Id: "2",Model: 1,Tipe: 5,Warna: "Black Mica",Qty: 2},
				{Id: "3",Model: 1,Tipe: 4,Warna: "Black Mica",Qty: 3},
			  ];
		//var res=Prospect_InformasiUnitYangDiminati_Json;	  
        return res;
      },
      create: function(prospectInformasiUnitYangDiminati) {
        return $http.post('/api/sales/ListCProspect_DetailUnitJoin', [{
                                            ProspectId: prospectInformasiUnitYangDiminati.ProspectId,
											VehicleModelId: prospectInformasiUnitYangDiminati.VehicleModelId,
											VehicleTypeId: prospectInformasiUnitYangDiminati.VehicleTypeId,
											ColorId: prospectInformasiUnitYangDiminati.ColorId,
                                            Qty: prospectInformasiUnitYangDiminati.Qty}]);
      },
      update: function(prospectInformasiUnitYangDiminati){
        return $http.put('/api/sales/ListCProspect_DetailUnitJoin', [{
                                            DetailUnitId: prospectInformasiUnitYangDiminati.DetailUnitId,
											OutletId: prospectInformasiUnitYangDiminati.OutletId,
											ProspectId: prospectInformasiUnitYangDiminati.ProspectId,
											VehicleModelId: prospectInformasiUnitYangDiminati.VehicleModelId,
											VehicleTypeId: prospectInformasiUnitYangDiminati.VehicleTypeId,
											ColorId: prospectInformasiUnitYangDiminati.ColorId,
                                            Qty: prospectInformasiUnitYangDiminati.Qty}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/ListCProspect_DetailUnitJoin',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd