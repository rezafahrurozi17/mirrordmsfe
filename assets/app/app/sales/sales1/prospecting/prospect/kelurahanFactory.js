angular.module('app')
  .factory('KelurahanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/MLocationKelurahan'+param);
        return res;
      },
      create: function(provinsi) {
        return $http.post('/api/sales/MLocationKelurahan', [{
                                            //AppId: 1,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      update: function(provinsi){
        return $http.put('/api/sales/MLocationKelurahan', [{
                                            SalesProgramId: provinsi.SalesProgramId,
                                            SalesProgramName: provinsi.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MLocationKelurahan',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd