angular.module('app')
  .factory('ProspectPengecekanDataFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/ProspectingPengecekanData'+param);
        //console.log('res=>',res);
		
		var Would_Be_Prospect_Json=[
				{ProspectId: 12,  CustomerId: 1,Address:"address dummy",Email:"a@b.com",PhoneNumber:"123",HP:"123",ProvinceId:2,SalesId:1,ProspectStatusId:1,Name:"nama1",ProvinceName:"Jawa Barat",ProspectStatusName:"Open",InstanceName:"instance"},
				{ProspectId: 13,  CustomerId: null,Address:"address dummy",Email:"a@b.com",PhoneNumber:"123",HP:"123",ProvinceId:2,SalesId:1,ProspectStatusId:1,Name:"nama1",ProvinceName:"Jawa Barat",ProspectStatusName:"Open",InstanceName:"instance"},
			  ];
		//var res=Would_Be_Prospect_Json;	  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/ProspectingPengecekanData', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/ProspectingPengecekanData', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/ProspectingPengecekanData',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd