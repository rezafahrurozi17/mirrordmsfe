angular.module('app')
  .factory('KodePosFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MLocationPostalCode');
        //console.log('res=>',res);  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/MLocationPostalCode', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/MLocationPostalCode', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MLocationPostalCode',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd