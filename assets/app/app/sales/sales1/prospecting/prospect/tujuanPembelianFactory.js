angular.module('app')
  .factory('TujuanPembelianFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryListPurchasePurpose');
  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/PCategoryListPurchasePurpose', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/PCategoryListPurchasePurpose', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryListPurchasePurpose',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd