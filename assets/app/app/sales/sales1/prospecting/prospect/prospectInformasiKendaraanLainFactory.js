angular.module('app')
  .factory('ProspectInformasiKendaraanLainFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(ProspectId) {
        var res=$http.get('/api/sales/CProspectDetailOtherVehicle/?start=1&limit=100&filterData=ProspectId|'+ProspectId);
        //console.log('res=>',res);
		var Prospect_InformasiKendaraanLain_Json=[
				{Id: "1",Merk: "Toyota",Model: 1,Tipe: 4,Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
				{Id: "2",Merk: "Toyota",Model: 1,Tipe: 5,Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
				{Id: "3",Merk: "Mazda",Model: 1,Tipe: 4,Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
			  ];
		//var res=Prospect_InformasiKendaraanLain_Json;	  
        return res;
      },
      create: function(prospectInformasiKendaraanLain) {
        return $http.post('/api/sales/CProspectDetailOtherVehicle', [{
                                            ProspectId: prospectInformasiKendaraanLain.ProspectId,
											BrandId: prospectInformasiKendaraanLain.BrandId,
											ModelName: prospectInformasiKendaraanLain.ModelName,
											TypeName: prospectInformasiKendaraanLain.TypeName,
											ColorName: prospectInformasiKendaraanLain.ColorName,
											VehicleYear: prospectInformasiKendaraanLain.VehicleYear,
                                            VehiclePrice: prospectInformasiKendaraanLain.VehiclePrice}]);
      },
      update: function(prospectInformasiKendaraanLain){
        return $http.put('/api/sales/CProspectDetailOtherVehicle', [{
                                            DetailOtherVehicleId: prospectInformasiKendaraanLain.DetailOtherVehicleId,
											OutletId: prospectInformasiKendaraanLain.OutletId,
											ProspectId: prospectInformasiKendaraanLain.ProspectId,
											BrandId: prospectInformasiKendaraanLain.BrandId,
											ModelName: prospectInformasiKendaraanLain.ModelName,
											TypeName: prospectInformasiKendaraanLain.TypeName,
											ColorName: prospectInformasiKendaraanLain.ColorName,
											VehicleYear: prospectInformasiKendaraanLain.VehicleYear,
                                            VehiclePrice: prospectInformasiKendaraanLain.VehiclePrice}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CProspectDetailOtherVehicle',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd