angular.module('app')
  .factory('SuspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
						// var res = $http.get('/api/sales/CSuspectList' + param);
						var res = $http.get('/api/sales/CSuspectList/Search' + param);
        //console.log('res=>',res);
		var Suspect_Json=[
				{SuspectId: 1,  GenerateDate: new Date("1/12/2016"),DataSourceId: 5,SuspectName: "nama1",  PhoneNumber: "123",HP: "12345678",  Email: "a@b.com",  Address: "alamat 1",  VehicleModelId: 93,PurchasePurposeId: 1,PurchaseTimeId: 2,Provinsi: "Jawa Barat",InstanceName: "",CustomerCategoryId: 1},
				{SuspectId: 2,  GenerateDate: new Date("1/12/2016"),DataSourceId: 2,SuspectName: "nama2",  PhoneNumber: "123",HP: "12345678",  Email: "a@b.com",  Address: "alamat 2",  VehicleModelId: 93,PurchasePurposeId: 2,PurchaseTimeId: 3,Provinsi: "Jawa Barat",InstanceName: "",CustomerCategoryId: 1},
				{SuspectId: 3,  GenerateDate: new Date("1/12/2016"),DataSourceId: 5,SuspectName: "nama3",  PhoneNumber: "123",HP: "12345678",  Email: "a@b.com",  Address: "alamat 3",  VehicleModelId: 93,PurchasePurposeId: 1,PurchaseTimeId: 4,Provinsi: "Jawa Barat",InstanceName: "instansi1",CustomerCategoryId: 2},
			  ];
		//var res=Suspect_Json;	  
        return res;
      },
	  AktifkanSuspect:function(DaSuspects)
      {
          return $http.put('/api/sales/CSuspect/Valid', [DaSuspects]);
      },
      create: function(Suspect) {
        return $http.post('/api/sales/CSuspectList', [{
                                            SuspectCode: Suspect.SuspectCode,
											SuspectName: Suspect.SuspectName,
											InstanceName: Suspect.InstanceName,
											PhoneNumber: Suspect.PhoneNumber,
											HP: Suspect.HP,
											Email: Suspect.Email,
											Address: Suspect.Address,
											Note: Suspect.Note,
											CustomerCategoryId: Suspect.CustomerCategoryId,
											VehicleModelId: Suspect.VehicleModelId,
											PurchasePurposeId: Suspect.PurchasePurposeId,
											PurchaseTimeId: Suspect.PurchaseTimeId,
											KacabId: Suspect.KacabId,
											SupervisorUserId: Suspect.SupervisorUserId,
											SalesId: Suspect.SalesId,
											DataSourceId: Suspect.DataSourceId,
											SuspectStatusId: Suspect.SuspectStatusId,
                                            GenerateDate: Suspect.GenerateDate}]);
      },
			createSaveToProspect: function(Suspect) {
        return $http.put('/api/sales/CSuspect/CreateProspect',[Suspect] );
			},
			
			createCustomerToProspect: function(Suspect) {
        return $http.post('/api/sales/CreateProspectFromCustomer',[Suspect] );
      },
      update: function(Suspect){
        return $http.put('/api/sales/CSuspectList', [{
                                            SuspectId: Suspect.SuspectId,
											OutletId: Suspect.OutletId,
											SuspectCode: Suspect.SuspectCode,
											SuspectName: Suspect.SuspectName,
											InstanceName: Suspect.InstanceName,
											PhoneNumber: Suspect.PhoneNumber,
											HP: Suspect.HP,
											Email: Suspect.Email,
											Address: Suspect.Address,
											Note: Suspect.Note,
											CustomerCategoryId: Suspect.CustomerCategoryId,
											VehicleModelId: Suspect.VehicleModelId,
											PurchasePurposeId: Suspect.PurchasePurposeId,
											PurchaseTimeId: Suspect.PurchaseTimeId,
											KacabId: Suspect.KacabId,
											SupervisorUserId: Suspect.SupervisorUserId,
											SalesId: Suspect.SalesId,
											DataSourceId: Suspect.DataSourceId,
											SuspectStatusId: Suspect.SuspectStatusId,
                                            GenerateDate: Suspect.GenerateDate}]);
      },
	  updateSuspectToClose: function(Suspect){
        return $http.put('/api/sales/CSuspectList', [{
                                            SuspectId: Suspect.SuspectId,
											OutletId: Suspect.OutletId,
											SuspectCode: Suspect.SuspectCode,
											SuspectName: Suspect.SuspectName,
											InstanceName: Suspect.InstanceName,
											PhoneNumber: Suspect.PhoneNumber,
											HP: Suspect.HP,
											Email: Suspect.Email,
											Address: Suspect.Address,
											Note: Suspect.Note,
											CustomerCategoryId: Suspect.CustomerCategoryId,
											VehicleModelId: Suspect.VehicleModelId,
											PurchasePurposeId: Suspect.PurchasePurposeId,
											PurchaseTimeId: Suspect.PurchaseTimeId,
											KacabId: Suspect.KacabId,
											SupervisorUserId: Suspect.SupervisorUserId,
											SalesId: Suspect.SalesId,
											DataSourceId: Suspect.DataSourceId,
											SuspectStatusId: 2,
                                            GenerateDate: Suspect.GenerateDate}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CSuspectList',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd