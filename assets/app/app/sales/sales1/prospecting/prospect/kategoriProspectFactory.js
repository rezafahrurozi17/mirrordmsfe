angular.module('app')
  .factory('KategoriProspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryProspect?start=1&limit=100&filter=Drop');
        	  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/PCategoryProspect', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/PCategoryProspect', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryProspect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd