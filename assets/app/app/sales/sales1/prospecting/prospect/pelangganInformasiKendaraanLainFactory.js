angular.module('app')
  .factory('PelangganInformasiKendaraanLainFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		var Pelanggan_InformasiKendaraanLain_Json=[
				{Id: "1",Merk: "Toyota",Model: "Avanza",Tipe: "1.5 G M/T",Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
				{Id: "2",Merk: "Toyota",Model: "Avanza",Tipe: "1.5 G M/T",Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
				{Id: "3",Merk: "Mazda",Model: "Avanza",Tipe: "1.5 G M/T",Warna: "Black Mica",Tahun: "2016",HargaBeli: "1000000"},
			  ];
		var res=Pelanggan_InformasiKendaraanLain_Json;	  
        return res;
      },
      create: function(pelangganInformasiKendaraanLain) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: pelangganInformasiKendaraanLain.SalesProgramName}]);
      },
      update: function(pelangganInformasiKendaraanLain){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: pelangganInformasiKendaraanLain.SalesProgramId,
                                            SalesProgramName: pelangganInformasiKendaraanLain.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd