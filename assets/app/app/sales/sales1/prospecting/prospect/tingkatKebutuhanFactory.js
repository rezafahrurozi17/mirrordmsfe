angular.module('app')
  .factory('TingkatKebutuhanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PCategoryListPurchaseTime');
        
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/PCategoryListPurchaseTime', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/PCategoryListPurchaseTime', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PCategoryListPurchaseTime',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd