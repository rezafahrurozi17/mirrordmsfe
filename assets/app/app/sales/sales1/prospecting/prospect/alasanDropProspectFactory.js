angular.module('app')
  .factory('AlasanDropProspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MActivityReasonDropProspect');	  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/MActivityReasonDropProspect', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/MActivityReasonDropProspect', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MActivityReasonDropProspect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd