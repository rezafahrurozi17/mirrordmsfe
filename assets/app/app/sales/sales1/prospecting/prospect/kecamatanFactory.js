angular.module('app')
  .factory('KecamatanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/MLocationKecamatan'+param);
        //console.log('res=>',res);
		
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/MLocationKecamatan', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/MLocationKecamatan', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MLocationKecamatan',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd