angular.module('app')
  .factory('SalesForceFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?position=salesman');
        //console.log('res=>',res);
		var optionsWarna_Json = [{ name: "Black Mica", value: "Black Mica" },{ name: "White", value: "White" },{ name: "Silver Metalic", value: "Silver Metalic" },{ name: "Red", value: "Red" },{ name: "Pink", value: "Pink" }];
		//var res=optionsWarna_Json;	  
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/MProfileEmployee/?position=salesman', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/MProfileEmployee/?position=salesman', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileEmployee/?position=salesman',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd