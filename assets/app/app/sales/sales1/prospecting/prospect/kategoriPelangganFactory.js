angular.module('app')
  .factory('KategoriPelangganFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerCategoryProspecting');
          
        return res;
      },
      create: function(Warna) {
        return $http.post('/api/sales/CCustomerCategory', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/CCustomerCategory', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerCategory',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd