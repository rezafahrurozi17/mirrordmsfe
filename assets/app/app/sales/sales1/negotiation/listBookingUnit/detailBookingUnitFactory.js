angular.module('app')
  .factory('DetailBookingUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(BookingUnitId) {
        var res=$http.get('/api/sales/SNegotiationDetailBookingUnit/?start=1&limit=100&filterData=BookingUnitId|'+BookingUnitId);
        //console.log('res=>',res);
		var ListBookingUnit_Pengecekan_InfoKendaraan_Json=[
        {Id: "1",RRNNo: "RRNNo1",FrameNo: "MHFE2CJ3JEK078961",Sold:"Yes"},
        {Id: "2",RRNNo: "RRNNo2",FrameNo: "MHFE2CJ3JEK078962",Sold:"No"},
        {Id: "3",RRNNo: "RRNNo3",FrameNo: "MHFE2CJ3JEK078963",Sold:"No"},
		];
		//var res=ListBookingUnit_Pengecekan_InfoKendaraan_Json;
        return res;
      },
      create: function(pengecekanInfoKendaraan) {
        return $http.post('/api/sales/SNegotiationDetailBookingUnit', [{
                                            //AppId: 1,
                                            SalesProgramName: pengecekanInfoKendaraan.SalesProgramName}]);
      },
      update: function(pengecekanInfoKendaraan){
        return $http.put('/api/sales/SNegotiationDetailBookingUnit', [{
                                            SalesProgramId: pengecekanInfoKendaraan.SalesProgramId,
                                            SalesProgramName: pengecekanInfoKendaraan.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationDetailBookingUnit',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd