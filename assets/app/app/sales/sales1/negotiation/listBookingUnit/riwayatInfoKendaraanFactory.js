angular.module('app')
  .factory('RiwayatInfoKendaraanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(Id) {
        var res=$http.get('/api/sales/SNegotiationBookingUnitRiwayat/?start=1&limit=100&filterData=DetailBookingUnitId|'+Id);
        
        return res;
      },
      create: function(riwayatInfoKendaraan) {
        return $http.post('/api/sales/SNegotiationBookingUnitRiwayat', [{
                                            //AppId: 1,
                                            SalesProgramName: riwayatInfoKendaraan.SalesProgramName}]);
      },
      update: function(riwayatInfoKendaraan){
        return $http.put('/api/sales/SNegotiationBookingUnitRiwayat', [{
                                            SalesProgramId: riwayatInfoKendaraan.SalesProgramId,
                                            SalesProgramName: riwayatInfoKendaraan.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnitRiwayat',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd