angular.module('app')
  .factory('PembatalanBookingUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SNegotiationBookingUnitPembatalan');
        //console.log('res=>',res);
		var List_Booking_Unit_Json=[
				{Id: "1",  BookingUnitId: "123456781",ProspectName: "Dwayne Johnson",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "Approved",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "2",  BookingUnitId: "123456782",ProspectName: "Andrew Ongko",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "Rejected",  Model: "Avanza",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "3",  BookingUnitId: "123456783",ProspectName: "Kurniadi",LastModifiedDate: "Revised 12 Sep 2016",StatusApprovalDiscountName: "Pending",  Model: "Avanza",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "4",  BookingUnitId: "123456784",ProspectName: "Christina",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "5",  BookingUnitId: "123456785",ProspectName: "AMuro Ray",LastModifiedDate: "Created 14 Sep 2016",StatusApprovalDiscountName: "",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
			  ];
		
		//var res = {
        //             Result: List_Booking_Unit_Json,
        //             Start:1,
        //             Limit: 10,
        //             Total:20
        //         };
		//res=List_Booking_Unit_Json;
        return res;
      },
      create: function(listBookingUnitFactory) {
        return $http.post('/api/sales/SNegotiationBookingUnitPembatalan', [{
                                            //AppId: 1,
                                            SalesProgramName: ListBookingUnitFactory.SalesProgramName}]);
      },
      update: function(ListBookingUnitFactory){
        return $http.put('/api/sales/SNegotiationBookingUnitPembatalan', [{
                                            SalesProgramId: ListBookingUnitFactory.SalesProgramId,
                                            SalesProgramName: ListBookingUnitFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnitPembatalan',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd