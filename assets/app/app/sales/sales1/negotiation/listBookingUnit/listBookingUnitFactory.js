angular.module('app')
  .factory('ListBookingUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/BookingUnit'+param);
        //console.log('res=>',res);
		    var List_Booking_Unit_Json=[
				{Id: "1",  BookingUnitId: "123456781",ProspectName: "Dwayne Johnson",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "Approved",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "2",  BookingUnitId: "123456782",ProspectName: "Andrew Ongko",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "Rejected",  Model: "Avanza",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "3",  BookingUnitId: "123456783",ProspectName: "Kurniadi",LastModifiedDate: "Revised 12 Sep 2016",StatusApprovalDiscountName: "Pending",  Model: "Avanza",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "4",  BookingUnitId: "123456784",ProspectName: "Christina",LastModifiedDate: "Created 12 Sep 2016",StatusApprovalDiscountName: "",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
				{Id: "5",  BookingUnitId: "123456785",ProspectName: "AMuro Ray",LastModifiedDate: "Created 14 Sep 2016",StatusApprovalDiscountName: "",  Model: "Innova",  Type: "1.5 G M/T", Warna: "Red"},
			  ];
        return res;
      },

      getProspect: function() {
          var res = $http.get('/api/sales/negotiationprospectlist/');
          return res;
      },

      getProspectRevisi: function() {
          var res = $http.get('/api/sales/negotiationprospectlistRevisi/');
          return res;
      },

      bookUnitToReserved: function(ListVehicle) {
          return $http.post('/api/sales/CVehicleListBookingUnit', ListVehicle); 
      },

      getResonRevision: function() {
          var res = $http.get('/api/sales/PCategoryReasonRevision/');
          //  console.log("res Factory", res);
          return res;
      },

      getMaksimalDiskon: function(param) {
        var res = $http.get('/api/sales/CekMaksimalDiskon/'+param);
        //  console.log("res Factory", res);
        return res;
    },

      simpanRevisiPDDBooking: function(BookingUnitPDDBooking) {
		  //console.log('isiprospek',Prospect);
        return $http.put('/api/sales/PDDBookingUnit/Revisi',BookingUnitPDDBooking);
      },

      create: function(listBookingUnitFactory) {
        return $http.post('/api/sales/SNegotiationBookingUnit', [{
                                            //AppId: 1,
                                            SalesProgramName: ListBookingUnitFactory.SalesProgramName}]);
      },
      update: function(ListBookingUnitFactory){
        return $http.put('/api/sales/SNegotiationBookingUnit', [{
                                            SalesProgramId: ListBookingUnitFactory.SalesProgramId,
                                            SalesProgramName: ListBookingUnitFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnit',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd