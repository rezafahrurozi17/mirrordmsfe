angular.module('app')
    .controller('ListBookingUnitController', function($scope, $http, CreateSpkFactory, CurrentUser, MaintainPDDFactory, ListBookingUnitFactory, RiwayatInfoKendaraanFactory,
        DetailBookingUnitFactory, PembatalanBookingUnitFactory, ProfileLeasing, StockInfoAvaliabilityFactory,
        AksesorisFactory, CategoryProspectSourceFactory, MetodePembayaranFactory, ProspectFactory, ComboBoxFactory, $timeout, bsNotify, $rootScope) {

        $scope.ShowListBookingUnit = true;
        $scope.user = CurrentUser.user();
        $scope.$on('$destroy', function() {
            angular.element('.ui.modal.ListBookingUnitForm').remove();
            angular.element('.ui.modal.ModalAlasanRevisi').remove();
            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').remove();
            angular.element('.ui.modal.ModalKaroseriBookingUnit').remove();
            angular.element('.ui.modal.selectvehicleprocessbooking').remove();
            angular.element('.ui.modal.ModalRevisionAlertPDD').remove();
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.disabledCekStok = false;

        $scope.bind_data = {
            data: [],
            filter: []
        };


        $scope.filterData = {
            defaultFilter: [
                { name: 'Nama Prospek', value: 'ProspectName' },
                { name: 'Kode Booking', value: 'BookingUnitId' },
                { name: 'Status Diskon', value: 'StatusApprovalName' }
            ],
            advancedFilter: [
                { name: 'ProspectName', value: 'ProspectName', typeMandatory: 1 },
                { name: 'BookingUnitId', value: 'BookingUnitId', typeMandatory: 0 },
                { name: 'StatusApprovalDiscountName', value: 'StatusApprovalName', typeMandatory: 1 }
            ]
        };
        

        var d = new Date();
            d.setHours(0);
            d.setMinutes(0);
            $scope.min = d;

        var d2 = new Date();
            d2.setHours(23);
            d2.setMinutes(59);
            $scope.max = d2;

            $scope.dateOptions5 = {
                // formatYear: 'yyyy-mm-dd',
                // startingDay: 1,
                // minMode: 'year'
                startingDay: 1,
                format: "dd/MM/yyyy",
                disableWeekend: 1,
                minDate: new Date(),
            };

        //$scope.List_Booking_Unit_To_Repeat = ListBookingUnitFactory.getData();
        $scope.detailaccs = false;
        $scope.opendetail = function() {
            switch ($scope.detailaccs) {
                case false:
                    $scope.detailaccs = true;
                    break;
                case true:
                    $scope.detailaccs = false;
                    break;
            }
        }

        $scope.BackToListBookingUnitClicked = function() {
            $scope.ShowListBookingUnit = true;
            $scope.ShowListBookingUnitRiwayat = false;
            $scope.ShowListBookingUnitPengecekan = false;
        };

        ListBookingUnitFactory.getResonRevision().then(
            function(res) {
                $scope.listRevision = res.data.Result;
            }
        )

        $scope.RevisiActionClicked = function(Selected_Data) {
            $scope.SelectDataRevisi = angular.copy(Selected_Data);

            if ($scope.SelectDataRevisi.StatusApprovalName == 'Ditolak') {
                angular.element('.ui.modal.ListBookingUnitForm').modal('hide');
                //angular.element('.ui.modal.ModalAlasanRevisi').modal('hide');
                $scope.ShowListBookingUnit = false;
                $scope.Show_Pembuatan_Booking_Unit = true;
                $scope.mBookingUnit = {};
                $scope.flagPDD = 'Buat';

                ListBookingUnitFactory.getProspectRevisi().then(
                    function(res) {
                        $scope.getProspect = res.data.Result;
                        //return res.data;

                        $scope.mBookingUnit.ProspectId = $scope.SelectDataRevisi.ProspectId;

                        $scope.ProspectRevisi = $scope.getProspect.filter(function(type) {
                            return (type.ProspectId == $scope.SelectDataRevisi.ProspectId);
                        })

                        $scope.mBookingUnit.HP = $scope.ProspectRevisi[0].HP;
                        $scope.mBookingUnit.Email = $scope.ProspectRevisi[0].Email;
                        $scope.mBookingUnit.ProspectDetailUnit = $scope.ProspectRevisi[0].ProspectDetailUnit;
                    }
                );

            } else {


                angular.element('.ui.modal.ListBookingUnitForm').modal('hide');
                setTimeout(function() {
                    angular.element('.ui.modal.ModalAlasanRevisi').modal('refresh');
                }, 0);
                angular.element('.ui.modal.ModalAlasanRevisi').modal('show');
                angular.element('.ui.modal.ModalAlasanRevisi').not(':first').remove();
                $scope.PilihanRevisiBooking = {};
                $scope.vehicletoSelect = [];
            }
        };

        $scope.vehicletoSelect = [];
        $scope.Lanjut_AlasanRevisi_Clicked = function(Id) {
            $scope.flagPDD = 'Revisi';

            angular.element('.ui.modal.ModalAlasanRevisi').modal('hide');
            setTimeout(function() {
                angular.element('.ui.modal.selectvehicleprocessbooking').modal('refresh');
            }, 0);
            angular.element('.ui.modal.selectvehicleprocessbooking').modal('show');
            angular.element('.ui.modal.selectvehicleprocessbooking').not(':first').remove();

            $scope.vehicletoSelect = $scope.SelectDataRevisi.ListDetailBookingUnit;

            $scope.vehicleselcted = [];
            // }
        };



        $scope.To_Riwayat_Booking_Unit = function(selected_data) {
            $scope.ListBookingUnit_Riwayat_History_To_Repeat = [];
            $scope.SelectedKendaraanId = '';
            $scope.mlistBookingUnit_BookingUnitHeader = angular.copy(selected_data);

            DetailBookingUnitFactory.getData(selected_data.BookingUnitId).then(function(res) {
                $scope.ListBookingUnit_Riwayat_InfoKendaraan_To_Repeat = res.data.Result;

                return $scope.ListBookingUnit_Riwayat_InfoKendaraan_To_Repeat;
            });

            //$scope.SelectedKendaraanId
            $scope.ShowListBookingUnit = false;
            $scope.ShowListBookingUnitRiwayat = true;
            angular.element('.ui.modal.ListBookingUnitForm').modal('hide');
        };

        $scope.RiwayatKendaraanChanged = function(Id) {
            $scope.ListBookingUnit_Riwayat_History_To_Repeat = "";
            RiwayatInfoKendaraanFactory.getData(Id).then(function(res) {
                $scope.ListBookingUnit_Riwayat_History_To_Repeat = res.data.Result;
                return $scope.ListBookingUnit_Riwayat_History_To_Repeat;
            });

            //$scope.ListBookingUnit_Riwayat_History_To_Repeat=History_Booking_Json;

        };

        $scope.PembatalanBookingUnit = function() {
            PembatalanBookingUnitFactory.delete([$scope.mlistBookingUnit_BookingUnitHeader.BookingUnitId]).then(function(res) {
                //$scope.ListBookingUnit_Riwayat_History_To_Repeat = res.data.Result;
                //return $scope.ListBookingUnit_Riwayat_History_To_Repeat;

                $rootScope.$emit('RefreshDirective', {})
                $scope.ShowListBookingUnit = true;
                $scope.ShowListBookingUnitRiwayat = false;
                $scope.ShowListBookingUnitPengecekan = false;

            });

            //PembatalanBookingUnitFactory.delete([$scope.mlistBookingUnit_BookingUnitHeader.BookingUnitId]).then();
            // $scope.ShowListBookingUnit = true;
            // $scope.ShowListBookingUnitRiwayat = false;
            // $scope.ShowListBookingUnitPengecekan = false;
        };

        $scope.BatalBooking = function(SelectedBooking) {
            PembatalanBookingUnitFactory.delete([SelectedBooking.BookingUnitId]).then(function(res) {
                //$scope.ListBookingUnit_Riwayat_History_To_Repeat = res.data.Result;
                //return $scope.ListBookingUnit_Riwayat_History_To_Repeat;
				bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil dibatalkan.",
											type: 'success'
										}
									);
                $rootScope.$emit('RefreshDirective', {})
                angular.element('.ui.modal.ListBookingUnitForm').modal('hide');

            });

            //PembatalanBookingUnitFactory.delete([$scope.mlistBookingUnit_BookingUnitHeader.BookingUnitId]).then();
            // $scope.ShowListBookingUnit = true;
            // $scope.ShowListBookingUnitRiwayat = false;
            // $scope.ShowListBookingUnitPengecekan = false;
        };

        $scope.To_pengecekan_Booking_Unit = function(selected_data) {

            // $scope.Show_mlistBookingUnit_Pengecekan_PembatalanBookingUnit_Button=false;
            // $scope.Show_mlistBookingUnit_Pengecekan_BuatSPK_Button=true;

            // $scope.mlistBookingUnit_BookingUnitHeader=angular.copy(selected_data);

            // DetailBookingUnitFactory.getData(selected_data.BookingUnitId).then(function(res){
            //     $scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat = res.data.Result;
            //     return $scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat;
            // });

            // for(var i = 0; i < $scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat.length; ++i)
            // {	
            // 	if($scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat[i].Sold == "Yes")
            // 	{
            // 		$scope.Show_mlistBookingUnit_Pengecekan_BuatSPK_Button=false;
            // 		$scope.Show_mlistBookingUnit_Pengecekan_PembatalanBookingUnit_Button=true;
            // 	}
            // }
            angular.element('.ui.modal.ListBookingUnitForm').modal('hide');
            $scope.ShowListBookingUnit = false;
            $scope.ShowListBookingUnitPengecekan = true;
			

            $scope.mlistBookingUnit_BookingUnitHeader = angular.copy(selected_data);
            $scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat = angular.copy(selected_data.ListDetailBookingUnit);
            var countStatus = 0;
            for (var i = 0; i < $scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat.length; ++i) {
                if ($scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat[i].StatusStockVehicleId != 1) {
                    countStatus++;
                }
            }
            if (countStatus >= 1) {
                $scope.Show_mlistBookingUnit_Pengecekan_BuatSPK_Button = false;
                $scope.Show_mlistBookingUnit_Pengecekan_PembatalanBookingUnit_Button = true;

            } else {
                $scope.Show_mlistBookingUnit_Pengecekan_BuatSPK_Button = true;
                $scope.Show_mlistBookingUnit_Pengecekan_PembatalanBookingUnit_Button = false;
            }


        };

        $scope.Batal_AlasanRevisi_Clicked = function() {
            angular.element('.ui.modal.ModalAlasanRevisi').modal('hide');
        };

        //----------------------
        // Tambahan Aldo
        //----------------------
        $scope.flagPDD = 'Buat';
        $scope.AddBooking = function() {
            $scope.flagPDD = 'Buat';
            $scope.ShowListBookingUnit = false;
            $scope.Show_Pembuatan_Booking_Unit = true;
            $scope.mBookingUnit = {};
            $scope.DisabledDataProspect = false;

            ListBookingUnitFactory.getProspect().then(
                function(res) {
                    $scope.getProspect = res.data.Result;
                    return res.data;
                }
            );
        };


        $scope.Head_Info_Kendaraan = [];
        $scope.TotalQuantity = 0;
        $scope.selectProspectName = function(selected) {
            $scope.TotalQuantity = 0;

            $scope.mBookingUnit = selected;
            //$scope.mBookingUnit.ProspectDetailUnit = null;
            $scope.mBookingUnit.ProspectDetailUnit = selected.ProspectDetailUnit;
            for (var i in selected.ProspectDetailUnit) {
                $scope.TotalQuantity = $scope.TotalQuantity + selected.ProspectDetailUnit[i].Qty;
            }

            $scope.Head_Info_Kendaraan = angular.copy($scope.mBookingUnit);

        }

        $scope.CekStok = function() {
            $scope.disabledCekStok = true;

            $scope.countDataItem = 1;
            $scope.countData = 1;

            if (typeof $scope.SelectedUnit == "undefined" || $scope.SelectedUnit.length == 0) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Silahkan pilih unit terlebih dahulu.",
                    type: 'warning'
                });
                $scope.disabledCekStok = false;
            }

            $scope.Head_Info_Kendaraan = angular.copy($scope.SelectedUnit);
            $scope.Head_Info_Kendaraan.ProspectName = $scope.mBookingUnit.ProspectName;

            var getStok = "/?start=1&limit=" + $scope.SelectedUnit.Qty + "&vehiclemodelid=" + $scope.SelectedUnit.VehicleModelId + "&vehicletypeid=" + $scope.SelectedUnit.VehicleTypeId + "&colorid=" + $scope.SelectedUnit.ColorId + "&assemblyyear=" + $scope.SelectedUnit.ProductionYear;

            StockInfoAvaliabilityFactory.getData(getStok).then(function(res) {
                $scope.ArrayStockInfoAvaliability = res.data.Result;
                var count_Stock_Free = 0;
                for (var i = 0; i < res.data.Result.length; ++i) {
                    if (res.data.Result[i].StatusStockName == "Free") {
                        count_Stock_Free++;
                    }

                }
                $scope.Jumlah_Stock_Free = count_Stock_Free;

                var count_Stock_Reserved = 0;
                for (var i = 0; i < res.data.Result.length; ++i) {
                    if (res.data.Result[i].StatusStockName == "Reserved") {
                        count_Stock_Reserved++;
                    }
                }
                $scope.Jumlah_Stock_Reserved = count_Stock_Reserved;

                //return $scope.ArrayStockInfoAvaliability;
                if ($scope.Jumlah_Stock_Free < $scope.SelectedUnit.Qty || typeof $scope.Jumlah_Stock_Free == 'undefined') {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Stock unit yang diminati tidak tersedia",
                        type: 'danger'
                    });
                    $scope.disabledCekStok = false;
                } else {
                    $scope.Show_Pembuatan_Booking_Unit = !$scope.Show_Pembuatan_Booking_Unit;
                    $scope.Show_Stok_Info_Booking_Unit = !$scope.Show_Stok_Info_Booking_Unit;
                }
            });

            $scope.listPlateNumber = [];
            $scope.tampungBookFee = [];
            $scope.listTampungBookFee = [];
            $scope.tampungBookFee.BookingFee = {};
            $scope.tampungBookFee.RequestDiscount = {};
            $scope.TotalBookFee = 0;
            $scope.TotalAjuDiskon = 0;
            $scope.disabledPlatNomor = false;
            $scope.TotalHargaKendaraan = 0;
            $scope.TotalListPaket = [];
            $scope.TotalListAcc = [];
            $scope.TotalListKaroseri = [];
            $scope.mNegotiation_Pdd = {};
            $scope.RincianHarga = [];

        };

        $scope.KembaliBookingList = function() {
            if ($scope.statusAction == 'DariProspect') {
                $scope.CancelBookingFromProspect();
            } else {
                $scope.Show_Pembuatan_Booking_Unit = false;
                $scope.ShowListBookingUnit = true;
                $rootScope.$emit('RefreshDirective', {})
            }
        }

        $scope.Back_To_Prospect_Info = function() {
            if ($scope.statusAction == 'DariProspect') {
                $scope.Show_Pembuatan_Booking_Unit = !$scope.Show_Pembuatan_Booking_Unit;
                $scope.Show_Stok_Info_Booking_Unit = !$scope.Show_Stok_Info_Booking_Unit;
            } else {
                $scope.Head_Info_Kendaraan = [];
                $scope.Show_Pembuatan_Booking_Unit = !$scope.Show_Pembuatan_Booking_Unit;
                $scope.Show_Stok_Info_Booking_Unit = !$scope.Show_Stok_Info_Booking_Unit;
                $scope.ArrayStockInfoAvaliability = [];
                $scope.SelectedUnit = [];
            }
        };

        $scope.ShowHideFreeStock_Clicked = function() {
            $scope.ShowHideFreeStock = !$scope.ShowHideFreeStock;
            if ($scope.ShowHideFreeStock == true) {
                $scope.ShowHideFreeStock_PanelIcon = "-";
            } else if ($scope.ShowHideFreeStock == false) {
                $scope.ShowHideFreeStock_PanelIcon = "+";
            }
        };

        $scope.ShowHideReservedStock_Clicked = function() {
            $scope.ShowHideReservedStock = !$scope.ShowHideReservedStock;
            if ($scope.ShowHideReservedStock == true) {
                $scope.ShowHideReservedStock_PanelIcon = "-";
            } else if ($scope.ShowHideReservedStock == false) {
                $scope.ShowHideReservedStock_PanelIcon = "+";
            }
        };

        $scope.tipePlateValue = function(selected) {
            $scope.selectedTipePlate = selected;
        }

        $scope.Define_PDD = function(kendaraan) {
            console.log('$scope.mBookingUnit', $scope.mBookingUnit);
            //console.log('kendaraan', kendaraan);
            $scope.SelectedKendaraan = kendaraan;
            // var detailVehicle = $scope.SelectedKendaraan.ProspectDetailUnit[0];
            // $scope.vehicleselcted=[];
            // console.log('ttt',detailVehicle);
            $scope.vehicleselcted.push($scope.SelectedKendaraan);
            console.log('aaa',$scope.vehicleselcted);
            if ($scope.countDataItem > 1) {
                $scope.disabledDiskon = true;
                $scope.disabledBookFee = true;
                $scope.disabledNegoPdd = true;
                $scope.checkNoPol = false;
                //$scope.mBookingUnit_AjuDiskon.ChoosePoliceNo = '';

            } else {
                $scope.disabledDiskon = false;
                $scope.disabledBookFee = false;
                $scope.disabledNegoPdd = false;
                $scope.checkNoPol = false;
                $scope.mNegotiation_Pdd.FundSourceId = $scope.mBookingUnit.FundSourceId;
                $scope.mNegotiation_Pdd.FundSourceName = $scope.mBookingUnit.FundSourceName;
                $scope.mNegotiation_Pdd.TradeInBit = $scope.mBookingUnit.TradeInBit;
                $scope.SelectedFundSource = {};
                $scope.SelectedFundSource.FundSourceId = $scope.mBookingUnit.FundSourceId;
                $scope.SelectedFundSource.FundSourceName = $scope.mBookingUnit.FundSourceName;

                if ($scope.mNegotiation_Pdd.FundSourceName == "Cash") {
                    $scope.Leasing_Details_Nego = true;
                    $scope.mNegotiation_Pdd.LeasingId = "";
                    $scope.mNegotiation_Pdd.LeasingTenorId = "";
                    $scope.mNegotiation_Pdd.Bunga = "";
                } else if ($scope.mNegotiation_Pdd.FundSourceName == "Credit") {
                    $scope.Leasing_Details_Nego = false;
                }
                //$scope.mBookingUnit_AjuDiskon.ChoosePoliceNo = '';
            }
            $scope.mBookingUnit_AjuDiskon = {};
            $scope.mBookingUnit_AjuDiskon.BookingFee = {};
            $scope.mBookingUnit_AjuDiskon.BookingFee = $scope.SelectedKendaraan.BookingFee;

            $scope.Show_Pembuatan_Booking_Unit_PDD = !$scope.Show_Pembuatan_Booking_Unit_PDD;
            $scope.Show_Stok_Info_Booking_Unit = !$scope.Show_Stok_Info_Booking_Unit;

            $scope.Head_Info_Kendaraan.FrameNo = kendaraan.FrameNo;
            $scope.Head_Info_Kendaraan.AssemblyYear = kendaraan.TahunKendaraan;
            $scope.Head_Info_Kendaraan.VehiclePrice = kendaraan.Price;
            $scope.Head_Info_Kendaraan.AssemblyYear = kendaraan.AssemblyYear;
            $scope.checkSentWithSTNK = true;
            $scope.mNegotiation_Pdd.SentWithSTNK = true;

            ProfileLeasing.getData().then(function(res) {
                $scope.LeasingOptions = res.data.Result;
                return $scope.LeasingOptions;
            });
        };

        $scope.Kembali_Ke_Stok_Info = function() {
            $scope.Show_Pembuatan_Booking_Unit_PDD = !$scope.Show_Pembuatan_Booking_Unit_PDD;
            $scope.Show_Stok_Info_Booking_Unit = !$scope.Show_Stok_Info_Booking_Unit;
        };

        $scope.mNegotiation_Pdd_Selected_MetodePembayaran = function(selectFundSource) {
            $scope.SelectedFundSource = selectFundSource;

            if ($scope.SelectedFundSource.FundSourceName == "Cash") {
                $scope.Leasing_Details_Nego = true;
                $scope.mNegotiation_Pdd.LeasingId = "";
                $scope.mNegotiation_Pdd.LeasingTenorId = "";
                $scope.mNegotiation_Pdd.Bunga = "";
            } else if ($scope.SelectedFundSource.FundSourceName == "Credit") {
                $scope.Leasing_Details_Nego = false;

            }
            // for other stuff ...
        };

        $scope.setDefaultValue = function(data) {
            if (data == null || data < 0 || data == undefined || data == "") {
                data = 0;
            }
        }

        $scope.tampungReqDiskon = 0;
        $scope.tampungReqBookFee = 0;
        $scope.To_Booking_Fee_Dan_Aju_Diskon = function(selectPDDForm) {
            
            if($scope.Leasing_Details_Nego == false){
                
                if ($scope.mNegotiation_Pdd.LeasingId == '' || $scope.mNegotiation_Pdd.LeasingTenorId == '' || $scope.mNegotiation_Pdd.Bunga == '' ){
                   
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Leasing, Tenor, Bunga (%) harus diisi.",
                        type: 'warning'
                    });
                }else{
                    $scope.mBookingUnit_AjuDiskon = {};
                    $scope.mBookingUnit_AjuDiskon.TipePlatNomor = {};
                    $scope.mBookingUnit_AjuDiskon.BBNAdjustment = 0;
                    $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment = 0;
                    $scope.mBookingUnit_AjuDiskon.DiscountSubDp = 0;
                    $scope.mBookingUnit_AjuDiskon.DiscountSubRate = 0;
        
        
                    if ($scope.countData > 1) {
                        $scope.disabledDiskon = true;
                        $scope.mBookingUnit_AjuDiskon.RequestDiscount = $scope.tampungReqDiskon;
                        $scope.mBookingUnit_AjuDiskon.BookingFee = $scope.tampungReqBookFee;
                    } else {
                        $scope.disabledDiskon = false;
                        $scope.mBookingUnit_AjuDiskon.RequestDiscount = 0;
                        $scope.mBookingUnit_AjuDiskon.BookingFee = $scope.SelectedKendaraan.BookingFee;
                    }
        
                    $scope.disabledPlatNomor = false;
                    $scope.mBookingUnit_AjuDiskon.TipePlatNomor = $scope.TipePlatNomorOptions[0];
                    $scope.selectedTipePlate = $scope.TipePlatNomorOptions[0];
        
                    //--------------CR2 Sumber Dana----------------//
                    $scope.SelectedFormPDD = selectPDDForm;
        
                    $scope.Show_Pembuatan_Booking_Unit_PDD = !$scope.Show_Pembuatan_Booking_Unit_PDD;
                    $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = !$scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon;
        
        
        
                    //$scope.mBookingUnit_AjuDiskon.BookingFee = $scope.SelectedKendaraan.BookingFee;
        
                    $scope.Head_Info_Kendaraan.Price = $scope.ArrayStockInfoAvaliability.Price;
                    //$scope.mBookingUnit_AjuDiskon = {};
                    //$scope.mBookingUnit_AjuDiskon.TipePlatNomor = '';
                    $scope.mBookingUnit_AjuDiskon.PlatNomor = '';
                    if ($scope.mNegotiation_Pdd.DirectDeliveryPDC == true) {
                        $scope.mNegotiation_Pdd.DirectDeliveryPDC == true;
                    } else {
                        $scope.mNegotiation_Pdd.DirectDeliveryPDC = null;
                    }
        
        
                    $scope.mBookingUnit_AjuDiskon.Aksesoris = "";
                    $scope.mBookingUnit_AjuDiskon.Aksesoris = [{ Id: null, Nama_Aksesoris: null, Harga_Aksesoris: null, Kategori_Aksesoris: null, Selected: null, Jumlah: null }];
                    $scope.mBookingUnit_AjuDiskon.Aksesoris.splice(0, 1);
        
                    //yang bawah buat karoseri
                    $scope.mBookingUnit_AjuDiskon.Karoseri = "";
                    $scope.mBookingUnit_AjuDiskon.Karoseri = [{ Nama: null, Harga: null }];
                    $scope.mBookingUnit_AjuDiskon.Karoseri.splice(0, 1);
        
                    // $scope.mBookingUnit_AjuDiskon.Biaya = "";
                    // $scope.mBookingUnit_AjuDiskon.Biaya = [{ Nama: null, Harga: null }];
                    // $scope.mBookingUnit_AjuDiskon.Biaya.splice(0, 1);
        
                    ProspectFactory.getDataPengajuanDiskon().then(
                        function(res) {
                            $scope.PengajuanDiskon = res.data.Result;
        
                        },
                        function(err) {
        
                        }
                    ) 
                }

            }else{
            $scope.mBookingUnit_AjuDiskon = {};
            $scope.mBookingUnit_AjuDiskon.TipePlatNomor = {};
            $scope.mBookingUnit_AjuDiskon.BBNAdjustment = 0;
            $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment = 0;
            $scope.mBookingUnit_AjuDiskon.DiscountSubDp = 0;
            $scope.mBookingUnit_AjuDiskon.DiscountSubRate = 0;


            if ($scope.countData > 1) {
                $scope.disabledDiskon = true;
                $scope.mBookingUnit_AjuDiskon.RequestDiscount = $scope.tampungReqDiskon;
                $scope.mBookingUnit_AjuDiskon.BookingFee = $scope.tampungReqBookFee;
            } else {
                $scope.disabledDiskon = false;
                $scope.mBookingUnit_AjuDiskon.RequestDiscount = 0;
                $scope.mBookingUnit_AjuDiskon.BookingFee = $scope.SelectedKendaraan.BookingFee;
            }

            $scope.disabledPlatNomor = false;
            $scope.mBookingUnit_AjuDiskon.TipePlatNomor = $scope.TipePlatNomorOptions[0];
            $scope.selectedTipePlate = $scope.TipePlatNomorOptions[0];

            //--------------CR2 Sumber Dana----------------//
            $scope.SelectedFormPDD = selectPDDForm;

            $scope.Show_Pembuatan_Booking_Unit_PDD = !$scope.Show_Pembuatan_Booking_Unit_PDD;
            $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = !$scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon;



            //$scope.mBookingUnit_AjuDiskon.BookingFee = $scope.SelectedKendaraan.BookingFee;

            $scope.Head_Info_Kendaraan.Price = $scope.ArrayStockInfoAvaliability.Price;
            //$scope.mBookingUnit_AjuDiskon = {};
            //$scope.mBookingUnit_AjuDiskon.TipePlatNomor = '';
            $scope.mBookingUnit_AjuDiskon.PlatNomor = '';
            if ($scope.mNegotiation_Pdd.DirectDeliveryPDC == true) {
                $scope.mNegotiation_Pdd.DirectDeliveryPDC == true;
            } else {
                $scope.mNegotiation_Pdd.DirectDeliveryPDC = null;
            }


            $scope.mBookingUnit_AjuDiskon.Aksesoris = "";
            $scope.mBookingUnit_AjuDiskon.Aksesoris = [{ Id: null, Nama_Aksesoris: null, Harga_Aksesoris: null, Kategori_Aksesoris: null, Selected: null, Jumlah: null }];
            $scope.mBookingUnit_AjuDiskon.Aksesoris.splice(0, 1);

            //yang bawah buat karoseri
            $scope.mBookingUnit_AjuDiskon.Karoseri = "";
            $scope.mBookingUnit_AjuDiskon.Karoseri = [{ Nama: null, Harga: null }];
            $scope.mBookingUnit_AjuDiskon.Karoseri.splice(0, 1);

            // $scope.mBookingUnit_AjuDiskon.Biaya = "";
            // $scope.mBookingUnit_AjuDiskon.Biaya = [{ Nama: null, Harga: null }];
            // $scope.mBookingUnit_AjuDiskon.Biaya.splice(0, 1);

            ProspectFactory.getDataPengajuanDiskon().then(
                function(res) {
                    $scope.PengajuanDiskon = res.data.Result;

                },
                function(err) {

                }
            )
        }
        };

        MetodePembayaranFactory.getData().then(function(res) {
            $scope.optionsMetodePembayaran = res.data.Result;
            return $scope.optionsMetodePembayaran;
        });

        $scope.leasing = function(selected) {
            ProspectFactory.getDataTenor(selected.LeasingId).then(
                function(res) {
                    $scope.tenor = res.data.Result;
                },
                function(err) {}
            )
        }

        $scope.tenorValue = function(selected) {
            $scope.SelectedTenor = selected;
        }

        $scope.listPlateNumber = [];
        $scope.tampungBookFee = [];
        $scope.listTampungBookFee = [];
        $scope.tampungBookFee.BookingFee = {};
        $scope.tampungBookFee.RequestDiscount = {};
        $scope.TotalBookFee = 0;
        $scope.TotalAjuDiskon = 0;
        $scope.TotalListPaket = [];
        $scope.TotalListAcc = [];
        $scope.TotalListKaroseri = [];
        $scope.totalQTYPaket = 0;
        $scope.TotalHargaKendaraan = 0;
        $scope.checkDirectDeliveri = false;
        $scope.checkOnOffTheRoad = false;
        $scope.checkSentWithSTNK = false;
        $scope.checkNoPol = false;
        $scope.jsData = {};
        $scope.DisabledStnk = false;
        $scope.SetOffTheRoad = function(Data) {
            console.log('Data', Data);
            if (Data == true) {
                $scope.checkSentWithSTNK = false;
                $scope.DisabledStnk = true;
            } else {
                $scope.checkSentWithSTNK = true;
                $scope.DisabledStnk = false;
            }
        }

        $scope.debug = function() {
            console.log("console log1", $scope.tempListPaketAcc);
            console.log("console log2", $scope.tempListAcc);
            console.log("console log3", $scope.mBookingUnit_AjuDiskon);
            console.log("console log4", $scope.mNegotiation_Pdd);
        }

        $scope.debug2 = function() {
            console.log("console log1", $scope.tempListPaketAcc);
            console.log("console log2", $scope.tempListAcc);
            console.log("console log3", $scope.mBookingUnit_AjuDiskon);
            console.log("console log4", $scope.mNegotiation_Pdd);
        }

        function maxDiscAcc(paket, satuan) {
            var maxDiscAccValue = 0;
            for (var k in paket) {
                if (paket[k].Price > maxDiscAccValue &&
                    paket[k].IsDiscount == true) {
                    maxDiscAccValue = paket[k].Price;
                }
            };
            for (var l in satuan) {
                if (satuan[l].Price > maxDiscAccValue &&
                    satuan[l].IsDiscount == true) {
                    maxDiscAccValue = satuan[l].Price;
                }
            };
            return maxDiscAccValue;
        };

        $scope.totalDiscount = 0;
        $scope.forAjuBookingUnit = {};
        $scope.TampungTotalTotalSatuan = 0;
        $scope.TampungTotalTotalPaket = 0;
        $scope.KalkulasiBookingUnitDanAjuDiskon = function(selectAjuDiskon) {
            $scope.DateStart();
            $scope.TotalHargaKendaraan = $scope.Head_Info_Kendaraan.Price * $scope.Head_Info_Kendaraan.Qty;

            //--------------CR2 Unit----------------//
            $scope.SelectedAjuDiskon = angular.copy(selectAjuDiskon);
            $scope.forAjuBookingUnit = angular.copy(selectAjuDiskon);
            console.log("jhgsjhfgsd", $scope.SelectedAjuDiskon);
            //$scope.selectedTipePlate = [];
            $scope.selectedTipePlate = angular.merge({}, $scope.selectedTipePlate, $scope.SelectedAjuDiskon);
            $scope.listPlateNumber.push($scope.selectedTipePlate);
            //$scope.listPlateNumber.push($scope.SelectedAjuDiskon.PlatNomor);

            $scope.PilihPdd = angular.merge({}, $scope.SelectedKendaraan, $scope.SelectedFundSource);
            //console.log('$scope.PilihPdd',$scope.PilihPdd);
            //---------------- CR2 -------------------\\
            $scope.totalDiscount = angular.copy($scope.SelectedAjuDiskon.RequestDiscount +
                $scope.SelectedAjuDiskon.DiscountSubDp +
                $scope.SelectedAjuDiskon.DiscountSubRate +
                maxDiscAcc($scope.tempListPaketAcc, $scope.tempListAcc));

            console.log("total disc", $scope.totalDiscount);
            ListBookingUnitFactory.getMaksimalDiskon('?VehicleTypeId=' + $scope.PilihPdd.VehicleTypeId + '&VehicleModelYear=' + $scope.PilihPdd.AssemblyYear + '&RequestDiscount=' + $scope.totalDiscount).then(
                function(res) {
                    //$scope.listRevision = res.data.Result;
                    $scope.inputAccPaket = angular.copy($scope.tempListPaketAcc);
                    $scope.inputAcc = angular.copy($scope.tempListAcc);
                    $scope.inputKaroseri = angular.copy($scope.tempListKaroseri);
                    angular.forEach($scope.inputAccPaket, function(acc, accIdx) {
                        var param = {
                            "Title": "Paket Aksesoris",
                            "HeaderInputs": {
                                "$AccPackageId$": acc.AccessoriesPackageId,
                                "$IsDiscount$": acc.IsDiscount == true ? 1 : 0,
                                "$QTY$": acc.Qty,
                                "$OutletId$": $scope.user.OutletId
                            }
                        };

                        $http.post('/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                            .then(function(res) {

                                $scope.inputAccPaket[accIdx].PPN = parseInt(res.data.Codes["[PPN]"]);
                                $scope.inputAccPaket[accIdx].TotalIncludeVAT = parseInt(res.data.Codes["[TotalHargaAcc]"]);
                                //$scope.inputAcc[accIdx].Discount =  parseInt(res.data.Codes["[Discount]"]);
                                $scope.inputAccPaket[accIdx].DPP = parseInt(res.data.Codes["[DPP]"]);
                                $scope.TampungTotalTotalPaket = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                                $scope.jsData.HeaderInputs['$TotalACC$'] = $scope.TampungTotalTotalSatuan + $scope.TampungTotalTotalPaket;
                                // $scope.jsData.HeaderInputs['$TotalACC$'] = parseInt($scope.jsData.HeaderInputs['$TotalACC$']) + parseInt(res.data.Codes["[TotalHargaAcc]"]);
                            });
                    });

                    angular.forEach($scope.inputAcc, function(acc, accIdx) {
                        console.log('ACC',acc);
                        console.log('$scope.inputAcc',$scope.inputAcc);
                        var param = {
                            "Title": "Aksesoris",
                            "HeaderInputs": {
                                "$AccessoriesId$": acc.AccessoriesId,
                                "$IsDiscount$": acc.IsDiscount == true ? 1 : 0,
                                "$QTY$": acc.Qty,
                                "$AccessoriesCode$": acc.AccessoriesCode,
                                "$OutletId$": $scope.user.OutletId,
                                "$VehicleTypeId$": $scope.PilihPdd.VehicleTypeId
                                
                            }
                        };

                        $http.post('/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                            .then(function(res) {
                                $scope.inputAcc[accIdx].PPN = parseInt(res.data.Codes["[PPN]"]);
                                $scope.inputAcc[accIdx].TotalIncludeVAT = parseInt(res.data.Codes["[TotalHargaAcc]"]);
                                //$scope.inputAcc[accIdx].Discount =  parseInt(res.data.Codes["[Discount]"]);
                                $scope.inputAcc[accIdx].DPP = parseInt(res.data.Codes["[DPP]"]);
                                $scope.TampungTotalTotalSatuan = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                                $scope.jsData.HeaderInputs['$TotalACC$'] = $scope.TampungTotalTotalSatuan + $scope.TampungTotalTotalPaket;
                                console.log('$scope.jsData1',$scope.jsData);
                                // $scope.jsData.HeaderInputs['$TotalACC$'] = parseInt($scope.jsData.HeaderInputs['$TotalACC$']) + parseInt(res.data.Codes["[TotalHargaAcc]"]);
                            });
                    });

                    angular.forEach($scope.inputKaroseri, function(acc, accIdx) {
                        var param = {
                            "Title": "Karoseri",
                            "HeaderInputs": {
                                "$KaroseriId$": acc.KaroseriId,
                                "$QTY$": acc.Qty,
                                "$OutletId$": $scope.user.OutletId,
                                "$VehicleTypeId$": $scope.modalKaroseri.VehicleTypeId,
                                "$Discount$": 0,
                            }
                        };

                        $http.post('/api/fe/PricingEngine?PricingId=20004&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                            .then(function(res) {

                                $scope.inputKaroseri[accIdx].PPN = res.data.Codes["[PPN]"];
                                $scope.inputKaroseri[accIdx].DPP = res.data.Codes["[DPP]"];
                                $scope.inputKaroseri[accIdx].TotalIncludeVAT = res.data.Codes["[TotalHargaKar]"];
                                $scope.inputKaroseri[accIdx].KaroseriDPP = res.data.Codes["#Price#"];

                                $scope.jsData.HeaderInputs['$TotalKaroseri$'] = parseFloat($scope.jsData.HeaderInputs['$TotalKaroseri$']) + parseFloat(res.data.Codes["[TotalHargaKar]"]);
                            });
                    });

                    //Total Paket Aksesoris
                    if ($scope.TotalListPaket.length == 0) {
                        $scope.TotalListPaket = angular.copy($scope.inputAccPaket);
                    } else {
                        for (var i = 0; i < $scope.tempListPaketAcc.length; i++) {
                            for (var j = 0; j < $scope.TotalListPaket.length; j++) {
                                if ($scope.TotalListPaket[j].AccessoriesPackageId == $scope.tempListPaketAcc[i].AccessoriesPackageId) {
                                    $scope.TotalListPaket[j].Qty = $scope.TotalListPaket[j].Qty + $scope.tempListPaketAcc[i].Qty;
                                    $scope.tempListPaketAcc.splice(i, 1);
                                } else {
                                    $scope.TotalListPaket.push($scope.tempListPaketAcc[i]);
                                }
                            }
                        }
                    }

                    if ($scope.TotalListAcc.length == 0) {
                        $scope.TotalListAcc = angular.copy($scope.inputAcc);
                    } else {
                        for (var i = 0; i < $scope.tempListAcc.length; i++) {
                            for (var j = 0; j < $scope.TotalListAcc.length; j++) {
                                if ($scope.TotalListAcc[j].AccessoriesId == $scope.tempListAcc[i].AccessoriesId) {
                                    $scope.TotalListAcc[j].Qty = $scope.TotalListAcc[j].Qty + $scope.tempListAcc[i].Qty;
                                    $scope.tempListAcc.splice(i, 1);
                                } else {
                                    $scope.TotalListAcc.push($scope.tempListAcc[i]);
                                }
                            }
                        }
                    }

                    //Total KAroseri
                    if ($scope.TotalListKaroseri.length == 0) {
                        $scope.TotalListKaroseri = angular.copy($scope.tempListKaroseri);
                    } else {
                        for (var i = 0; i < $scope.tempListKaroseri.length; i++) {
                            for (var j = 0; j < $scope.TotalListKaroseri.length; j++) {
                                if ($scope.TotalListKaroseri[j].KaroseriId == $scope.tempListKaroseri[i].KaroseriId) {
                                    $scope.TotalListKaroseri[j].Qty = $scope.TotalListKaroseri[j].Qty + $scope.tempListKaroseri[i].Qty;
                                    $scope.tempListKaroseri.splice(i, 1);
                                } else {
                                    $scope.TotalListKaroseri.push($scope.tempListKaroseri[i]);
                                }
                            }
                        }
                    }

                    if (typeof $scope.SelectedAjuDiskon.BookingFee == "undefined") {
                        $scope.tampungBookFee.BookingFee = 0;
                    } else {
                        $scope.tampungBookFee.BookingFee = parseInt(angular.copy($scope.SelectedAjuDiskon.BookingFee));
                    }

                    if (typeof $scope.SelectedAjuDiskon.RequestDiscount == "undefined") {
                        $scope.tampungBookFee.RequestDiscount = 0;
                    } else {
                        $scope.tampungBookFee.RequestDiscount = parseInt(angular.copy($scope.SelectedAjuDiskon.RequestDiscount));
                    }

                    $scope.listTampungBookFee.push($scope.tampungBookFee);
                    for (var i in $scope.listTampungBookFee) {
                        $scope.TotalBookFee = $scope.TotalBookFee + $scope.listTampungBookFee[i].BookingFee;
                    }

                    for (var i in $scope.listTampungBookFee) {
                        $scope.TotalAjuDiskon = $scope.TotalAjuDiskon + $scope.listTampungBookFee[i].RequestDiscount;
                    }

                    //Pricing Engine
                    $scope.urlPricing = '/api/fe/PricingEngine?PricingId=10003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                    $scope.jsData = { Title: null, HeaderInputs: { $VehicleTypeColorId$: null, $OnOffTheRoadId$: null, $BBNSAdj$: 0, $BBNAdj$: 0, $DSubsidiDP$: 0, $DSubsidiRate$: 0, $MCommision$: 0, $Diskon$: 0, $TotalACC$: 0, $TotalKaroseri$: 0, $Qty$: 0, $OutletId$: null, $VehicleTypeColorId$: null, $AssemblyYear$: null } }
                    console.log('$scope.TampungTotalTotalSatuan',$scope.TampungTotalTotalSatuan);
                    console.log(' $scope.jsData', $scope.TampungTotalTotalPaket);
                    $scope.jsData.HeaderInputs.$OutletId$ = $scope.user.OutletId;
                    $scope.jsData.HeaderInputs.$VehicleTypeColorId$ = $scope.Head_Info_Kendaraan.VehicleTypeColorId;
                    //$VehicleTypeColorId$: scope.unit[0].ListDetailUnit[0].VehicleTypeColorId,
                    $scope.jsData.HeaderInputs.$OnOffTheRoadId$ = $scope.mNegotiation_Pdd.OnOffTheRoad == null || $scope.mNegotiation_Pdd.OnOffTheRoad == undefined || $scope.mNegotiation_Pdd.OnOffTheRoad == false ? 1 : 0;
                    $scope.jsData.HeaderInputs.$BBNAdj$ = $scope.mBookingUnit_AjuDiskon.BBNAdjustment == null || $scope.mBookingUnit_AjuDiskon.BBNAdjustment == undefined || $scope.mBookingUnit_AjuDiskon.BBNAdjustment == "" ? 0 : $scope.mBookingUnit_AjuDiskon.BBNAdjustment;
                    $scope.jsData.HeaderInputs.$BBNSAdj$ = $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment == null || $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment == undefined || $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment == "" ? 0 : $scope.mBookingUnit_AjuDiskon.BBNServiceAdjustment;
                    $scope.jsData.HeaderInputs.$DSubsidiDP$ = $scope.mBookingUnit_AjuDiskon.DiscountSubDp == null || $scope.mBookingUnit_AjuDiskon.DiscountSubDp == undefined || $scope.mBookingUnit_AjuDiskon.DiscountSubDp == "" ? 0 : $scope.mBookingUnit_AjuDiskon.DiscountSubDp;
                    $scope.jsData.HeaderInputs.$DSubsidiRate$ = $scope.mBookingUnit_AjuDiskon.DiscountSubRate == null || $scope.mBookingUnit_AjuDiskon.DiscountSubRate == undefined || $scope.mBookingUnit_AjuDiskon.DiscountSubRate == "" ? 0 : $scope.mBookingUnit_AjuDiskon.DiscountSubRate;
                    $scope.jsData.HeaderInputs.$MCommision$ = 0.0;
                    // $scope.jsData.HeaderInputs.$TotalACC$ = $scope.TampungTotalTotalSatuan + $scope.TampungTotalTotalPaket;
                    $scope.jsData.HeaderInputs.$BookingFee$ = $scope.TotalBookFee;
                    $scope.jsData.HeaderInputs.$Diskon$ = $scope.TotalAjuDiskon == undefined || $scope.TotalAjuDiskon == null || $scope.TotalAjuDiskon == "" ? 0 : $scope.TotalAjuDiskon;
                    $scope.jsData.HeaderInputs.$AssemblyYear$ = parseInt($scope.Head_Info_Kendaraan.ProductionYear);
                    $scope.jsData.HeaderInputs.$Qty$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyFee$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyDisc$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyDiscSub$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyMediatorCom$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyDPP$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyPPN$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyBBN$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyPPH$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyVAT$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyDiscSubRate$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyPAD$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyBBNAdj$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyBBNSvcAdj$ = $scope.Head_Info_Kendaraan.Qty;
                    // $scope.jsData.HeaderInputs.$QtyTotBBN$ = $scope.Head_Info_Kendaraan.Qty;

                    $scope.KalkulasiTLSDate = {};
                    $scope.KalkulasiTLSDate.FrameNumber = "";
                    $scope.KalkulasiTLSDate.RRN = "";
                    $scope.KalkulasiTLSDate.DTPLOD = "";
                    $scope.KalkulasiTLSDate.PaketAksesorisTAM = "";
                    $scope.KalkulasiTLSDate.DeliveryCategory = "";


                    if ($scope.mNegotiation_Pdd.DirectDeliveryPDC == null || $scope.mNegotiation_Pdd.DirectDeliveryPDC == false ||$scope.mNegotiation_Pdd.DirectDeliveryPDC == undefined) {
                        $scope.KalkulasiTLSDate.DeliveryCategory = 1;
                    } else {
                        $scope.KalkulasiTLSDate.DeliveryCategory = 3;
                    }

                    if ($scope.SelectedKendaraan.FrameNo == null) {
                        $scope.KalkulasiTLSDate.FrameNumber = "";
                        $scope.KalkulasiTLSDate.RRN = $scope.SelectedKendaraan.RRNNo;
                        $scope.KalkulasiTLSDate.DTPLOD = $scope.SelectedKendaraan.PLOD;
                    } else {
                        $scope.KalkulasiTLSDate.FrameNumber = $scope.SelectedKendaraan.FrameNo;
                        $scope.KalkulasiTLSDate.RRN = "";
                        $scope.KalkulasiTLSDate.DTPLOD = "";
                    }

                    MaintainPDDFactory.kalkulasiTLSDate($scope.KalkulasiTLSDate).then(
                        function(res) {
                            $scope.TLSDate = res.data;

                            $scope.TLSDate.earliestPDD = $scope.TLSDate.earliestPDD.getFullYear() + '-' +
                                ('0' + ($scope.TLSDate.earliestPDD.getMonth() + 1)).slice(-2) + '-' +
                                ('0' + $scope.TLSDate.earliestPDD.getDate()).slice(-2) + 'T' +
                                (($scope.TLSDate.earliestPDD.getHours() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getHours()) + ':' +
                                (($scope.TLSDate.earliestPDD.getMinutes() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getMinutes()) + ':' +
                                (($scope.TLSDate.earliestPDD.getSeconds() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getSeconds());

                            $scope.TLSDate.earliestPDD.slice(0, 10);

                            if ($scope.PilihPdd.FrameNo == null && $scope.PilihPdd.FundSourceName == 'Cash') {
                                $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = false;
                                $scope.MainPDD = false;
                                $scope.MainRRNKredit = false;
                                $scope.MainRRNCash = true;
                                // if( $scope.flagPDD == 'Revisi'){
                                //     $scope.testDateRRN = {
                                //         date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                //         status: 'firstDate'
                                //     };
                                //     $scope.testDateRRN2 = {
                                //         date: new Date($scope.vehicleselcted[0].DPDate),
                                //         status: 'secondDate'
                                //     };
                                //     $scope.testDateRRN3 = {
                                //         date: new Date($scope.vehicleselcted[0].FullPaymentDate),
                                //         status: 'thirdDate'
                                //     };
                                // }

                            }
                            if ($scope.PilihPdd.FrameNo == null && $scope.PilihPdd.FundSourceName == 'Credit') {
                                $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = false;
                                $scope.MainRRNKredit = true;
                                // if( $scope.flagPDD == 'Revisi'){
                                //     $scope.testDateRRN = {
                                //         date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                //         status: 'firstDate'
                                //     };
                                //     $scope.testDateRRN2 = {
                                //         date: new Date($scope.vehicleselcted[0].TDPDate),
                                //         status: 'secondDate'
                                //     };

                                // }

                            }
                            if ($scope.PilihPdd.FrameNo != null && $scope.PilihPdd.FundSourceName == 'Cash') {
                                $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = false;
                                $scope.MainPDD = true;
                                // console.log('test 1');
                                // console.log('$scope.flagPDD',$scope.flagPDD);
                                // if( $scope.flagPDD == 'Revisi'){
                                //     console.log('test 2');
                                //     $scope.testDate = {
                                //         date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                //         status: 'firstDate'
                                //     };
                                //     $scope.testDate2 = {
                                //         date: new Date($scope.vehicleselcted[0].DPDate),
                                //         status: 'secondDate'
                                //     };
                                //     $scope.testDate3 = {
                                //         date: new Date($scope.vehicleselcted[0].FullPaymentDate),
                                //         status: 'thirdDate'
                                //     };
                                // }
                            }
                            if ($scope.PilihPdd.FrameNo != null && $scope.PilihPdd.FundSourceName == 'Credit') {
                                $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = false;
                                $scope.MainPDDKredit = true;
                                // if( $scope.flagPDD == 'Revisi'){
                                //     $scope.testDate = {
                                //         date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                //         status: 'firstDate'
                                //     };
                                //     $scope.testDate2 = {
                                //         date: new Date($scope.vehicleselcted[0].TDPDate),
                                //         status: 'secondDate'
                                //     };

                                // }
                            }
                        },
                        function(err) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: err.data.Message,
                                type: 'warning'
                            });
                        }
                    )
                },
                function(err) {
                    bsNotify.show({
                        title: "Peringatan",
                        content: err.data.Message,
                        type: 'warning'
                    });
                }
            )

        };

        $scope.Back_To_Define_Pdd = function() {
            $scope.Show_Pembuatan_Booking_Unit_PDD = !$scope.Show_Pembuatan_Booking_Unit_PDD;
            $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = !$scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon;
            $scope.mBookingUnit_AjuDiskon.Aksesoris = "";
        };

        $scope.tipeaccesories = [{ Name: "Paket" }, { Name: "Satuan" }];
        $scope.TambahAksesoris = function() {
            $scope.modalAccs = angular.copy($scope.Head_Info_Kendaraan);

            $scope.selectedItems = [];
            $scope.acessoriesVehicle = [];
            $scope.acessoriesVehiclesatuan = [];
            $scope.searchAccesoriestype = '';
            $scope.Satuan = null;
            $scope.searchAccesories = {};
            $scope.searchAccesories.PartName = '';
            setTimeout(function() {
                angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('show');
            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').not(':first').remove();
        };

        $scope.AccessoriesBookingUnitChange = function(vehicleTypeId, tipe) {
            $scope.searchAccesoriestype = tipe;
            if (tipe == 'Paket') {

                $scope.Satuan = null;
                ComboBoxFactory.getDataAccsessoriesPackage(vehicleTypeId).then(function(res) {
                    $scope.acessoriesVehicle = res.data.Result;
                    setTimeout(function() {
                        angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('refresh');
                    }, 0);
                    angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('show');

                })
            } else if (tipe == 'Satuan') {
                setTimeout(function() {
                    angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('refresh');
                }, 0);
                angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('show');
                $scope.Satuan = 'OK';
                $scope.acessoriesVehicle = [];
            }
        }

        $scope.AccesoriesSatuanBookingUnit = function(vehicleId, param) {
            if (typeof param.Classification == 'undefined') {
                param.Classification = false;
            }

            ComboBoxFactory.getDataAcessories(vehicleId, param.Classification, param.PartName).then(function(res) {
                $scope.acessoriesVehiclesatuan = res.data.Result;
                setTimeout(function() {
                    angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('refresh');
                }, 0);
                angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('show');
            })

        }

        $scope.selectedItems = [];
        $scope.selectedItemsKaroseri = [];

        $scope.toggleCheckedPackageAccsBookingUnit = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);

            } else {
                data.checked = true;
                $scope.selectedItems.push(data);

            }
        }

        $scope.toggleCheckedPcsAccsBookingUnit = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);

            } else {
                data.checked = true;
                $scope.selectedItems.push(data);

            }
        };

        $scope.tempAksesoris = [];
        $scope.tempListPaketAcc = [];
        $scope.tempListAcc = [];
        $scope.totalPaket = 0;
        $scope.inputAcc = [];
        $scope.inputAccPaket = [];
        $scope.PilihAksesoriesBookingUnit = function(selectAksesories) {
            if ($scope.searchAccesoriestype == 'Paket') {
                console.log('$scope.tempListPaketAcc', $scope.tempListPaketAcc);
                console.log('$scope.selectedItems', $scope.selectedItems);
                if ($scope.tempListPaketAcc.length == 0) {
                    for (var i in $scope.selectedItems) {
                        $scope.tempListPaketAcc.push({
                            AccessoriesPackageId: $scope.selectedItems[i].AccessoriesPackageId,
                            AccessoriesPackageName: $scope.selectedItems[i].AccessoriesPackageName,
                            Price: $scope.selectedItems[i].Price,
                            IsDiscount: false,
                            Discount: 0,
                            Qty: $scope.selectedItems[i].Qty,
                            PPN: 0,
                            TotalIncludeVAT: 0,
                            DPP: 0,
                            listDetail: $scope.selectedItems[i].listDetail
                                //SubTotalPaket:($scope.selectedItems[i].Price * $scope.selectedItems[i].Qty)

                        });
                        $scope.tempListPaketAcc[i].Qty = 1;
                        $scope.tempListPaketAcc[i].Qty = angular.copy(1);
                    }

                } else if ($scope.tempListPaketAcc.length > 0) {
                    for (var i in $scope.tempListPaketAcc) {
                        for (var j in $scope.selectedItems) {
                            if ($scope.tempListPaketAcc[i].AccessoriesPackageId == $scope.selectedItems[j].AccessoriesPackageId) {
                                $scope.tempListPaketAcc[i].Qty++;
                                $scope.selectedItems.splice(j, 1);
                            }
                        }
                    }

                    if ($scope.selectedItems.length > 0) {
                        for (var k in $scope.selectedItems) {
                            $scope.tempListPaketAcc.push({
                                AccessoriesPackageId: $scope.selectedItems[k].AccessoriesPackageId,
                                AccessoriesPackageName: $scope.selectedItems[k].AccessoriesPackageName,
                                Price: $scope.selectedItems[k].Price,
                                IsDiscount: false,
                                Discount: 0,
                                Qty: 1,
                                PPN: 0,
                                TotalIncludeVAT: 0,
                                DPP: 0,
                                listDetail: $scope.selectedItems[k].listDetail
                            });
                            // $scope.tempListPaketAcc[k].Qty = 1;
                            // $scope.tempListPaketAcc[k].Qty = angular.copy(1);
                        }

                    }

                }
                // angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('hide');
                // $scope.tempAksesoris = angular.merge([], $scope.tempListPaketAcc, $scope.tempListAcc)

            } else if ($scope.searchAccesoriestype == 'Satuan') {
                if ($scope.tempListAcc.length == 0) {
                    for (var j in $scope.selectedItems) {
                        $scope.tempListAcc.push({
                            AccessoriesId: $scope.selectedItems[j].PartsId,
                            AccessoriesCode: $scope.selectedItems[j].PartsCode,
                            AccessoriesName: $scope.selectedItems[j].PartsName,
                            Price: $scope.selectedItems[j].RetailPrice,
                            Qty: $scope.selectedItems[j].Qty,
                            IsDiscount: false,
                            Discount: 0,
                            PPN: 0,
                            TotalIncludeVAT: 0,
                            DPP: 0,
                            listDetail: $scope.selectedItems[j].listDetail
                                //SubTotalAcc:($scope.selectedItems[i].Price * $scope.selectedItems[i].Qty)

                        });
                        $scope.tempListAcc[j].Qty = 1;
                        $scope.tempListAcc[j].Qty = angular.copy(1);
                    }
                } else {
                    for (var j in $scope.tempListAcc) {
                        for (var i in $scope.selectedItems) {
                            if ($scope.tempListAcc[j].AccessoriesId == $scope.selectedItems[i].PartsId) {
                                $scope.tempListAcc[j].Qty++;
                                $scope.selectedItems.splice(i, 1);
                            }
                        }

                    }

                    if ($scope.selectedItems.length > 0) {
                        for (var k in $scope.selectedItems) {
                            $scope.tempListAcc.push({
                                AccessoriesId: $scope.selectedItems[k].PartsId,
                                AccessoriesCode: $scope.selectedItems[j].PartsCode,
                                AccessoriesName: $scope.selectedItems[k].PartsName,
                                Price: $scope.selectedItems[k].RetailPrice,
                                IsDiscount: false,
                                Discount: 0,
                                Qty: 1,
                                PPN: 0,
                                TotalIncludeVAT: 0,
                                DPP: 0,
                                listDetail: $scope.selectedItems[k].listDetail
                                    //SubTotalAcc:($scope.selectedItems[i].Price * $scope.selectedItems[i].Qty)

                            });
                            // $scope.tempListAcc[k].Qty = 1;
                            // $scope.tempListAcc[k].Qty = angular.copy(1);
                        }
                    }

                }
                // angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('hide');
                // $scope.tempAksesoris = angular.merge([], $scope.tempListPaketAcc, $scope.tempListAcc)
            }

            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('hide');
            $scope.tempAksesoris = angular.merge([], $scope.tempListPaketAcc, $scope.tempListAcc)

        }

        $scope.CancelPilihAksesoriesBookingUnit = function() {
            angular.element('.ui.modal.ModalTambahAksesorisBookingUnit').modal('hide');
        }

        $scope.toggleCheckedKaroseri = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItemsKaroseri.indexOf(data);
                $scope.selectedItemsKaroseri.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedItemsKaroseri.push(data);

            }
        };

        $scope.TambahKaroseri = function() {
            $scope.modalKaroseri = angular.copy($scope.Head_Info_Kendaraan);

            $scope.selectedItemsKaroseri = [];
            $scope.Karoseri = [];
            console.log('$scope.modalKaroseri',$scope.modalKaroseri);
            ComboBoxFactory.getKaroseri($scope.modalKaroseri.VehicleTypeId,$scope.modalKaroseri.VehicleTypeColorId).then(function(res) {
                $scope.Karoseri = res.data.Result;
                setTimeout(function() {
                    angular.element('.ui.modal.ModalKaroseriBookingUnit').modal('refresh');
                }, 0);
                angular.element('.ui.modal.ModalKaroseriBookingUnit').modal('show');
                angular.element('.ui.modal.ModalKaroseriBookingUnit').not(':first').remove();
            })
        };

        $scope.RemoveAksesorisPaket = function(IndexAksesoris) {
            $scope.tempListPaketAcc.splice(IndexAksesoris, 1);
        };
        $scope.RemoveAksesoris = function(IndexAksesoris) {
            $scope.tempListAcc.splice(IndexAksesoris, 1);
        };

        $scope.tempListKaroseri = [];
        $scope.inputKaroseri = [];
        $scope.PilihKaroseri = function() {
            for (var i in $scope.selectedItemsKaroseri) {
                $scope.tempListKaroseri.push({
                    KaroseriId: $scope.selectedItemsKaroseri[i].KaroseriId,
                    KaroseriName: $scope.selectedItemsKaroseri[i].KaroseriName,
                    Price: $scope.selectedItemsKaroseri[i].Price,
                    Qty: 1,
                    PPN: 0,
                    TotalIncludeVAT: 0,
                    KaroseriDPP: 0

                });
                //SubTotalPaket:($scope.selectedItems[i].Price * $scope.selectedItems[i].Qty)
                //$scope.tempListKaroseri = angular.merge({}, $scope.selectedItemsKaroseri)
                angular.element('.ui.modal.ModalKaroseriBookingUnit').modal('hide');
            }
        }

        $scope.CancelPilihKaroseri = function() {
            angular.element('.ui.modal.ModalKaroseriBookingUnit').modal('hide');
        }

        $scope.removeKaroseri = function(IndexKaroseri) {
            $scope.tempListKaroseri.splice(IndexKaroseri, 1);
        }

        ProspectFactory.getCategoriNumberPlate().then(function(res) {
            $scope.TipePlatNomorOptions = res.data.Result;

        })

        $scope.tempNopol = {};
        $scope.selectNoPol = function(selectedValue) {

            $scope.tempNopol.ChoosePoliceNo = selectedValue;

            if (selectedValue == true) {
                $scope.disabledPlatNomor = true;
            } else {
                $scope.disabledPlatNomor = false;
            }
        }

        $scope.Back_To_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = function() {
            $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = !$scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon;
            $scope.Show_Penambahan_Aksesoris = !$scope.Show_Penambahan_Aksesoris;

        };

        $scope.dateOptionsBook = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.testDate = {
            date: new Date(),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDate3 = {
            date: new Date(),
            status: 'thirdDate'
        };
        $scope.testDate4 = {
            date: new Date(),
            status: 'fourthDate'
        };
        $scope.testDate5 = {
            date: new Date(),
            status: 'fifthDate'
        };
        $scope.testDateRRN = {
            date: new Date(),
            status: 'firstDate'
        };
        $scope.testDateRRN2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDateRRN3 = {
            date: new Date(),
            status: 'thirdDate'
        };


        $scope.DateStart = function() {

            $scope.testDate = {
                date: new Date(),
                status: 'firstDate'
            };

            $scope.testDate2 = {
                date: new Date(),
                status: 'secondDate'
            };
            $scope.testDate3 = {
                date: new Date(),
                status: 'thirdDate'
            };
            $scope.testDate4 = {
                date: new Date(),
                status: 'fourthDate'
            };
            $scope.testDate5 = {
                date: new Date(),
                status: 'fifthDate'
            };
            $scope.testDateRRN = {
                date: new Date(),
                status: 'firstDate'
            };
            $scope.testDateRRN2 = {
                date: new Date(),
                status: 'secondDate'
            };
            $scope.testDateRRN3 = {
                date: new Date(),
                status: 'thirdDate'
            };

            $scope.events.splice($scope.testDate);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
        $scope.mytime.setMinutes('00');

        $scope.mytime2 = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
        $scope.mytime2.setMinutes('00');

        $scope.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };

        //Begin Calendar
        $scope.refreshCalendar = true;
        $scope.today = function() {
            $scope.dt1 = new Date();
            $scope.dt2 = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt1 = null;
            $scope.dt2 = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
			startingDay: 1,
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function() {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function(year, month, day) {
            $scope.dt1 = new Date(year, month, day);
            $scope.dt2 = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);


        $scope.testDate = {
            date: new Date(),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDate3 = {
            date: new Date(),
            status: 'thirdDate'
        };
        $scope.testDate4 = {
            date: new Date(),
            status: 'fourthDate'
        };
        $scope.testDate5 = {
            date: new Date(),
            status: 'fifthDate'
        };
        $scope.testDateRRN = {
            date: new Date(),
            status: 'firstDate'
        };
        $scope.testDateRRN2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDateRRN3 = {
            date: new Date(),
            status: 'thirdDate'
        };

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        //End Calendar

        $scope.cancelBuatPDD = function() {
            if ($scope.flagPDD == 'Revisi') {
                $scope.ShowListBookingUnit = true;
                $scope.MainPDD = false;
                $scope.MainPDDKredit = false;
                $scope.MainRRNCash = false;
                $scope.MainRRNKredit = false;
                $scope.DetilKalkulasiPDDCash = false;
                $rootScope.$emit('RefreshDirective', {})

            } else {
                $scope.Show_Pembuatan_Booking_Booking_Fee_Dan_Aju_Diskon = true;
                $scope.MainPDD = false;
                $scope.MainPDDKredit = false;
                $scope.MainRRNCash = false;
                $scope.MainRRNKredit = false;
                $scope.DetilKalkulasiPDDCash = false;

                $scope.listPlateNumber = [];
                $scope.tampungBookFee = [];
                $scope.listTampungBookFee = [];
                $scope.tampungBookFee.BookingFee = {};
                $scope.tampungBookFee.RequestDiscount = {};
                $scope.TotalBookFee = 0;
                $scope.TotalAjuDiskon = 0;

                $scope.TotalListPaket = [];
                $scope.TotalListAcc = [];
                $scope.TotalListKaroseri = [];
                $rootScope.$emit('RefreshDirective', {})
            }

        }

        $scope.kalkulasiPDDNorangka = function() {
            var tglDOc = $scope.testDate.date.getFullYear() + '-' +
                ('0' + ($scope.testDate.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDate.date.getDate()).slice(-2) + 'T' +
                (($scope.testDate.date.getHours() < '10' ? '0' : '') + $scope.testDate.date.getHours()) + ':' +
                (($scope.testDate.date.getMinutes() < '10' ? '0' : '') + $scope.testDate.date.getMinutes()) + ':' +
                (($scope.testDate.date.getSeconds() < '10' ? '0' : '') + $scope.testDate.date.getSeconds());

            tglDOc.slice(0, 10);

            var tglDP = $scope.testDate2.date.getFullYear() + '-' +
                ('0' + ($scope.testDate2.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDate2.date.getDate()).slice(-2) + 'T' +
                (($scope.testDate2.date.getHours() < '10' ? '0' : '') + $scope.testDate2.date.getHours()) + ':' +
                (($scope.testDate2.date.getMinutes() < '10' ? '0' : '') + $scope.testDate2.date.getMinutes()) + ':' +
                (($scope.testDate2.date.getSeconds() < '10' ? '0' : '') + $scope.testDate2.date.getSeconds());


            tglDP.slice(0, 10);

            var tglFUllPayment = $scope.testDate3.date.getFullYear() + '-' +
                ('0' + ($scope.testDate3.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDate3.date.getDate()).slice(-2) + 'T' +
                (($scope.testDate3.date.getHours() < '10' ? '0' : '') + $scope.testDate3.date.getHours()) + ':' +
                (($scope.testDate3.date.getMinutes() < '10' ? '0' : '') + $scope.testDate3.date.getMinutes()) + ':' +
                (($scope.testDate3.date.getSeconds() < '10' ? '0' : '') + $scope.testDate3.date.getSeconds());

            tglFUllPayment.slice(0, 10);


            var PLODDate = $scope.SelectedKendaraan.PLOD.getFullYear() + '-' +
                ('0' + ($scope.SelectedKendaraan.PLOD.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.SelectedKendaraan.PLOD.getDate()).slice(-2) + 'T' +
                (($scope.SelectedKendaraan.PLOD.getHours() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getHours()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getMinutes() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getMinutes()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getSeconds() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var temDirectPDC = false;
            var sentSTNK = false;
            var choosePolise = false;

            if (typeof $scope.SelectedFormPDD.DirectDeliveryPDC == "undefined") {
                temDirectPDC = false;
            } else {
                temDirectPDC = true;
            }

            if (typeof $scope.SelectedFormPDD.SentWithSTNK == "undefined") {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }

            if (typeof $scope.SelectedAjuDiskon.ChoosePoliceNo == "undefined") {
                choosePolise = false;
            } else {
                choosePolise = true;
            }

            var accBit = false;
            if ($scope.tempListAcc.length == 0) {
                accBit = false;
            } else {
                accBit = true;
            }

            var karoBit = false;
            if ($scope.tempListKaroseri.length == 0) {
                karoBit = false;
            } else {
                karoBit = true;
            }
            //$scope.mNegotiation_Pdd.OnOffTheRoad
            //console.log('$scope.mNegotiation_Pdd.OnOffTheRoad',$scope.mNegotiation_Pdd.OnOffTheRoad);


            var offRoad = false;
            if (typeof $scope.mNegotiation_Pdd.OnOffTheRoad == 'undefined' || $scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                offRoad = false;
            } else {
                offRoad = true;
            }

            //console.log('getlKalkulasi',getlKalkulasi);
            console.log('kal', $scope.vehicleselcted);
            var getlKalkulasi = "?&RRNNo=" + $scope.PilihPdd.RRNNo + "&FrameNo=" + $scope.PilihPdd.FrameNo + "&FundSourceId=" + $scope.PilihPdd.FundSourceId + "&DocCompRecDate=" + tglDOc + "&DPDate=" + tglDP + "&FullPaymentDate=" + tglFUllPayment + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + temDirectPDC + "&ChoosePoliceNo=" + choosePolise + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA + "&KaroseriBit=" + karoBit + "&AccesoriesBit=" + accBit + "&OffTheRoad=" + offRoad + "&VehicleId=" + $scope.vehicleselcted[0].VehicleId;
            ProspectFactory.getKalkulasiPDD(getlKalkulasi).then(
                function(res) {

                    $scope.bind_data.data = res.data.Result;
                    $scope.DateStart();

                    var date01 = $scope.bind_data.data[0]['DocCompRecDate'];
                    var date02 = $scope.bind_data.data[0]['DPDate'];
                    var date03 = $scope.bind_data.data[0]['FullPaymentDate'];

                    $scope.testDate1 = {
                        date: date01,
                        status: 'firstDate'
                    };

                    $scope.events.push($scope.testDate1);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate2 = {
                        date: date02,
                        status: 'secondDate'
                    };

                    $scope.events.push($scope.testDate2);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate3 = {
                        date: date03,
                        status: 'thirdDate'
                    };

                    $scope.events.push($scope.testDate3);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.fourthDateChange();

                    var date05 = $scope.bind_data.data[0]['SentUnitDate'];
                    var pengirimanSTNK = $scope.bind_data.data[0]['SentWithSTNK'];
                    
                    if(pengirimanSTNK==true){
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(date05),
                        };

                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };
                        
                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDCash = true;
                        $scope.MainPDD = false;

                    }else{
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(),
                        };
                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };
                        
                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDCash = true;
                        $scope.MainPDD = false;
                    }

                    // $scope.testDate5 = {
                    //     date: date05,
                    //     status: 'fifthDate'
                    // };

                    // $scope.events.push($scope.testDate5);
                    // $scope.KalkulasiPDDCash = true;
                    // $scope.MainPDD = false;
                },
                function(err) {

                    bsNotify.show({
                        title: "Peringatan",
                        content: err.data.Message,
                        type: 'warning'
                    });
                }
            )
        };

        $scope.firstDateChange = function() {
            $scope.events.push($scope.testDate);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
        $scope.popup3 = {
            opened: false
        };

        $scope.popup5 = {
            opened: false
        };


        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.secondDateChange = function() {
            $scope.events.push($scope.testDate2);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.open3 = function() {
            $scope.popup3.opened = true;
        };

        $scope.thirdDateChange = function() {
            $scope.events.push($scope.testDate3);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.fourthDateChange = function() {

            var date04 = $scope.bind_data.data[0]['STNKDoneDate'];

            $scope.testDate4 = {
                date: date04,
                status: 'fourthDate'
            };

            $scope.events.push($scope.testDate4);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.secondDateRRNChange = function() {

            $scope.events.push($scope.testDateRRN2);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };
        $scope.popupRRN1 = {
            opened: false
        };


        $scope.popupRRN2 = {
            opened: false
        };
        $scope.openRRN3 = function() {
            $scope.popupRRN3.opened = true;
        };

        $scope.thirdDateRRNChange = function() {

            $scope.events.push($scope.testDateRRN3);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };


        $scope.popupRRN3 = {
            opened: false
        };


        $scope.cancelkalkulasiRRNCredit = function() {
            $scope.MainRRNKredit = true;
            $scope.KalkulasiRRNKredit = false;
        }

        $scope.cancelPDDCashNorangka = function() {

            $scope.MainPDD = true;
            $scope.KalkulasiPDDCash = false;

        }

        $scope.TempPDD = [];
        $scope.grandTotal = 0;
        $scope.BookUnit = [];
        $scope.BookUnit.ListDetailBookingUnit = [];
        $scope.BookingUnitPdd = [];
        $scope.tampungInputBooking = [];
        $scope.tampungPDD = [];
        $scope.inputDataPDD = [];

        $scope.simpanPDDCash = function() {
            var timeEta = $scope.mytime;
            var dateEta = $scope.testDate5.date;

            try{
                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                    ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                    ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                    (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
            
                        if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "Tanggal Pengiriman Tidak Boleh Kurang Dari Tanggal Suggest PDD, Tanggal Suggest PDD : " + $scope.bind_data.data[0]['SentUnitDate'],
                                type: 'warning'
                            });
                        } else {
                            if ($scope.flagPDD == 'Revisi') {
            
                                $scope.InsertRevisiPDD = [];
                                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
            
                                $scope.bind_data.data[0].ChoosePoliceNo = $scope.vehicleselcted[0].ChoosePoliceNo;
                                $scope.bind_data.data[0].DirectDeliveryPDC = $scope.SelectDataRevisi.DirectDeliveryPDC;
                                $scope.bind_data.data[0].EarliestPDD = $scope.TLSDate.earliestPDD;
                                $scope.bind_data.data[0].OnOffTheRoad = $scope.SelectDataRevisi.OffTheRoad;
                                //$scope.bind_data.data[0].OnOffTheRoadId =
                                //$scope.bind_data.data[0].SentUnitTime =
                                $scope.bind_data.data[0].SentWithSTNK = $scope.SelectDataRevisi.SentWithSTNK;
                                $scope.bind_data.data[0].StatusUnitId = $scope.vehicleselcted[0].StatusUnitId;
                                $scope.bind_data.data[0].DetailBookingUnitId = $scope.vehicleselcted[0].DetailBookingUnitId;
                                $scope.bind_data.data[0].ReasonRevisionId = $scope.PilihanRevisiBooking.ReasonRevisionId;
            
                                $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                    ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                    ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                    (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
            
                                $scope.InsertRevisiPDD.push($scope.bind_data.data[0]);
            
                                ListBookingUnitFactory.simpanRevisiPDDBooking($scope.InsertRevisiPDD).then(
                                    function() {
            
                                        $rootScope.$emit('RefreshDirective', {})
                                        $scope.inputDataPDD = [];
                                        $scope.tampungInputBooking = [];
                                        $scope.KalkulasiPDDCash = false;
                                        $scope.ShowListBookingUnit = true;
            
                                    },
                                    function(err) {
            
                                        bsNotify.show({
                                            title: "Gagal",
                                            content: "Revisi PDD gagal",
                                            type: 'danger'
                                        });
                                    }
            
                                )
            
                            } else {
                                $scope.KalkulasiTLSDate.RequestedPDD = null;
                                $scope.KalkulasiTLSDate.RequestedPDD = $scope.testDate5.date;
            
                                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitDate = dateEta };
            
                                $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                    ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                    ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                    (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
            
            
                                for (var i in $scope.bind_data.data) {
                                    $scope.bind_data.data[i].SentUnitDate = dateEta;
                                    try {
                                        $scope.bind_data.data[i].SentUnitDate = $scope.bind_data.data[i].SentUnitDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].SentUnitDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].SentUnitDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].SentUnitDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].SentUnitDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].SentUnitDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getSeconds());
                                    } catch (e1) {
                                        $scope.bind_data.data[i].SentUnitDate = null;
                                    }
            
                                    try {
                                        $scope.bind_data.data[i].DocCompRecDate = $scope.bind_data.data[i].DocCompRecDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].DocCompRecDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].DocCompRecDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getSeconds());
                                    } catch (e1) {
                                        $scope.bind_data.data[i].DocCompRecDate = null;
                                    }
            
                                    try {
                                        $scope.bind_data.data[i].DPDate = $scope.bind_data.data[i].DPDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].DPDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].DPDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getSeconds());
                                    } catch (e2) {
                                        $scope.bind_data.data[i].DPDate = null;
                                    }
            
                                    try {
                                        $scope.bind_data.data[i].TDPDate = $scope.bind_data.data[i].TDPDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].TDPDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].TDPDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getSeconds());
                                    } catch (e3) {
                                        $scope.bind_data.data[i].TDPDate = null;
                                    }
            
                                    try {
                                        $scope.bind_data.data[i].FullPaymentDate = $scope.bind_data.data[i].FullPaymentDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].FullPaymentDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].FullPaymentDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getSeconds());
                                    } catch (e4) {
                                        $scope.bind_data.data[i].FullPaymentDate = null;
                                    }
                                    try {
                                        $scope.bind_data.data[i].STNKDoneDate = $scope.bind_data.data[i].STNKDoneDate.getFullYear() + '-' +
                                            ('0' + ($scope.bind_data.data[i].STNKDoneDate.getMonth() + 1)).slice(-2) + '-' +
                                            ('0' + $scope.bind_data.data[i].STNKDoneDate.getDate()).slice(-2) + 'T' +
                                            (($scope.bind_data.data[i].STNKDoneDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getHours()) + ':' +
                                            (($scope.bind_data.data[i].STNKDoneDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getMinutes()) + ':' +
                                            (($scope.bind_data.data[i].STNKDoneDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getSeconds());
                                    } catch (e4) {
                                        $scope.bind_data.data[i].STNKDoneDate = null;
                                    }
                                };
            
            
                                $scope.TempPDD = $scope.bind_data.data;
            
                                if (typeof $scope.checkDirectDeliveri == "undefined") {
                                    $scope.mNegotiation_Pdd.DirectDeliveryPDC = false;
                                }
            
                                if (typeof $scope.checkOnOffTheRoad == "undefined") {
                                    $scope.mNegotiation_Pdd.OnOffTheRoad = false;
                                }
            
                                if (typeof $scope.checkSentWithSTNK == "undefined") {
                                    $scope.mNegotiation_Pdd.SentWithSTNK = false;
                                }
            
                                if (typeof $scope.checkNoPol == "undefined") {
                                    $scope.SelectedAjuDiskon.ChoosePoliceNo = false;
                                }
            
                                $scope.mNegotiation_Pdd.OnOffTheRoadId = null;
                                if ($scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                                    $scope.mNegotiation_Pdd.OnOffTheRoadId = 1;
                                }
                                if ($scope.mNegotiation_Pdd.OnOffTheRoad == true) {
                                    $scope.mNegotiation_Pdd.OnOffTheRoadId = 2;
                                }
            
            
                                var selectPoliceNo = {};
                                selectPoliceNo.ChoosePoliceNo = {};
                                selectPoliceNo.ChoosePoliceNo = $scope.SelectedAjuDiskon.ChoosePoliceNo;
            
                                var StatusUnit = {};
                                StatusUnit.StatusUnitId = {};
                                StatusUnit.StatusUnitId = $scope.SelectedKendaraan.StatusUnitId;
            
            
                                $scope.tampungPDD = angular.merge({}, $scope.TempPDD[0], $scope.mNegotiation_Pdd, selectPoliceNo, StatusUnit)
            
            
                                $scope.inputDataPDD.push($scope.tampungPDD);
            
                                $scope.BookUnit.ListDetailBookingUnit = [];
                                $scope.BookUnit.ListDetailBookingUnit.push($scope.SelectedKendaraan);
                                $scope.BookUnit.FundSourceId = $scope.SelectedFundSource.FundSourceId;
            
                                $scope.BookUnit.SNegotiationSimulasiKredit = null;
                                $scope.BookUnit.SNegotiationSimulasiAsuransi = null;
                                var karoId = 0;
                                var karoPrice = 0;
                                var karoppn = 0;
                                var karototalvat = 0;
                                var dpp = 0;
            
                                if ($scope.inputKaroseri.length == 0) {
            
                                    karoId = 0
                                    karoPrice = 0;
                                    karoppn = 0;
                                    karototalvat = 0;
                                    dpp = 0;
                                } else {
            
                                    karoId = $scope.inputKaroseri[0].KaroseriId;
                                    karoPrice = $scope.inputKaroseri[0].Price;
                                    karoppn = $scope.inputKaroseri[0].PPN;
                                    karototalvat = $scope.inputKaroseri[0].TotalIncludeVAT;
                                    dpp = $scope.inputKaroseri[0].KaroseriDPP;
                                }
                                if (typeof $scope.tempNopol.ChoosePoliceNo == "undefined") {
                                    $scope.tempNopol.ChoosePoliceNo = false;
                                } else {
                                    $scope.tempNopol.ChoosePoliceNo = true;
                                }
            
            
                                $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = [];
                                $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = [];
                                $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = $scope.inputAcc;
                                $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = $scope.inputAccPaket;
            
            
                                $scope.BookUnit.ListDetailBookingUnit[0].KaroseriId = karoId;
                                $scope.BookUnit.ListDetailBookingUnit[0].KaroseriPrice = karoPrice;
            
                                $scope.BookUnit.ListDetailBookingUnit[0].PPNKaroseri = karoppn;
                                $scope.BookUnit.ListDetailBookingUnit[0].TotalIncludeVATKaroseri = karototalvat;
                                $scope.BookUnit.ListDetailBookingUnit[0].KaroseriDPP = dpp;
            
                                $scope.BookUnit.ListDetailBookingUnit[0].ChoosePoliceNo = $scope.tempNopol.ChoosePoliceNo;
            
                                //BookingUnit_AjuDiskon.ChoosePoliceNo
            
                                $scope.BookUnit.ListDetailBookingUnit[0].NumberPlateId = $scope.listPlateNumber[0].NumberPlateId;
                                $scope.BookUnit.ListDetailBookingUnit[0].PoliceNo = $scope.listPlateNumber[0].PlatNomor;
            
            
            
                                for (var i in $scope.ArrayStockInfoAvaliability) {
                                    if ($scope.ArrayStockInfoAvaliability[i].RRNNo == $scope.SelectedKendaraan.RRNNo && $scope.ArrayStockInfoAvaliability[i].FrameNo == $scope.SelectedKendaraan.FrameNo) {
                                        $scope.ArrayStockInfoAvaliability.splice(i, 1);
                                        $scope.Jumlah_Stock_Free = $scope.Jumlah_Stock_Free - 1;
                                    }
                                }
            
                                $scope.selectedTipePlate = [];
            
                                $scope.inputBookingUnit = [];
                                $scope.inputBookingUnit = angular.merge({}, $scope.BookUnit, $scope.mNegotiation_Pdd, $scope.tampungBookFee);
            
                                $scope.BookUnit.ListDetailBookingUnit[0] = angular.merge({}, $scope.BookUnit.ListDetailBookingUnit[0], $scope.tampungBookFee);
                                $scope.tampungInputBooking.push($scope.BookUnit.ListDetailBookingUnit[0]);
            
            
                                if ($scope.countData == $scope.TotalQuantity) {
            
                                    $scope.KalkulasiPDDCash = false;
                                    $scope.Show_RincianHarga = true;
                                    $scope.GetDataFromFactory();
            
                                } else {
                                    if ($scope.countData == $scope.Head_Info_Kendaraan.Qty) {
                                        $scope.Show_RincianHarga = true;
                                        $scope.KalkulasiPDDCash = false;
            
                                    } else {
                                        $scope.Show_Stok_Info_Booking_Unit = true;
                                        $scope.KalkulasiPDDCash = false;
            
                                    }
            
            
                                }
                                $scope.countData++;
                                $scope.countDataItem++;
            
                                if ($scope.tempListPaketAcc.length == 0) {
                                    //$scope.tempAksesoris.Jumlah = 0;
                                    var jmlAksesorisPaket = 0;
                                    var PriceAksesoris = 0;
                                    var TotalPaket = 0;
            
                                } else
                                if ($scope.tempListAcc.length == 0) {
                                    var jmlAksesoris = 0;
                                    var RetailAccPrice = 0;
                                    var TotalAcc = 0;
                                } else {
            
                                    for (var j in $scope.tempListPaketAcc) {
                                        TotalPaket = TotalPaket + ($scope.tempListPaketAcc[j].SubTotalPaket);
            
                                    }
            
                                    for (var k in $scope.tempListAcc) {
                                        TotalAcc = TotalAcc + ($scope.tempListAcc[j].SubTotalAcc);
            
                                    }
            
                                }
            
            
            
                                if ($scope.tempListKaroseri.length == 0) {
                                    //$scope.tempListKaroseri.Price = 0;
            
                                    var PriceKaroseri = 0;
            
                                } else {
                                    var PriceKaroseri = $scope.tempListKaroseri[0].Price;
                                }
            
                                if (typeof $scope.SelectedAjuDiskon.BookingFee == "undefined") {
                                    var bookFee = 0;
                                } else {
                                    var bookFee = $scope.SelectedAjuDiskon.BookingFee;
                                }
            
                                if (typeof $scope.SelectedAjuDiskon.RequestDiscount == "undefined") {
                                    var ajuDisc = 0;
                                } else {
                                    var ajuDisc = $scope.SelectedAjuDiskon.RequestDiscount;
                                }
            
            
                                $scope.grandTotal = ($scope.Head_Info_Kendaraan.Qty * $scope.Head_Info_Kendaraan.Price) - ($scope.Head_Info_Kendaraan.Qty * bookFee) + TotalPaket + TotalAcc + ($scope.Head_Info_Kendaraan.Qty * PriceKaroseri) - ($scope.Head_Info_Kendaraan.Qty * ajuDisc);
            
            
                                $scope.tempListPaketAcc = [];
                                $scope.tempListAcc = [];
                                $scope.tempListKaroseri = [];
                                $scope.tampungBookFee = { BookingFee: 0, RequestDiscount: 0 };
                                $scope.listTampungBookFee = [];
            
                                $scope.inputAccPaket = [];
                                $scope.inputAcc = [];
                                $scope.inputKaroseri = [];
                                $scope.tampungReqBookFee = $scope.SelectedAjuDiskon.BookingFee;
                                $scope.tampungReqDiskon = $scope.SelectedAjuDiskon.RequestDiscount;
                            }
                        }
            }
            catch(ex){
                bsNotify.show({
                    title: "Peringatan",
                    content: "ETA Pelanggan tidak boleh lebih dari 23:59",
                    type: 'warning'
                });
            }

            
        };

        $scope.cancelDateChange = function() {
            var index = $scope.events.indexOf($scope.testDate4);
            $scope.events.splice(index, 1);

            index = $scope.events.indexOf($scope.testDate5);
            $scope.events.splice(index, 1);

            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd/MM/yyyy','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.firstDateRRNChange = function() {
            $scope.events.push($scope.testDateRRN);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.openRRN1 = function() {
            $scope.popupRRN1.opened = true;
        };
        $scope.openRRN2 = function() {
            $scope.popupRRN2.opened = true;
        };

        $scope.KembaliPdd = function() {
            $scope.Show_Pembuatan_Booking_Unit = true;
            $scope.Show_RincianHarga = false;

            $scope.listPlateNumber = [];
            $scope.tampungBookFee = [];
            $scope.listTampungBookFee = [];
            $scope.tampungBookFee.BookingFee = {};
            $scope.tampungBookFee.RequestDiscount = {};
            $scope.TotalBookFee = 0;
            $scope.TotalAjuDiskon = 0;
            $scope.disabledPlatNomor = false;
            $scope.TotalHargaKendaraan = 0;
            $scope.TotalListPaket = [];
            $scope.TotalListAcc = [];
            $scope.TotalListKaroseri = [];
            $scope.mNegotiation_Pdd = {};
            $scope.RincianHarga = [];
            $scope.BookUnit = [];
            $scope.inputAccPaket = [];
            $scope.inputAcc = [];
            $scope.inputKaroseri = [];
            $scope.inputDataPDD = [];
        }

        $scope.kalkulasiPDDNorangkaCredit = function() {

            var tglDOc = $scope.testDate.date.getFullYear() + '-' +
                ('0' + ($scope.testDate.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDate.date.getDate()).slice(-2) + 'T' +
                (($scope.testDate.date.getHours() < '10' ? '0' : '') + $scope.testDate.date.getHours()) + ':' +
                (($scope.testDate.date.getMinutes() < '10' ? '0' : '') + $scope.testDate.date.getMinutes()) + ':' +
                (($scope.testDate.date.getSeconds() < '10' ? '0' : '') + $scope.testDate.date.getSeconds());

            tglDOc.slice(0, 10);

            var tglTDP = $scope.testDate2.date.getFullYear() + '-' +
                ('0' + ($scope.testDate2.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDate2.date.getDate()).slice(-2) + 'T' +
                (($scope.testDate2.date.getHours() < '10' ? '0' : '') + $scope.testDate2.date.getHours()) + ':' +
                (($scope.testDate2.date.getMinutes() < '10' ? '0' : '') + $scope.testDate2.date.getMinutes()) + ':' +
                (($scope.testDate2.date.getSeconds() < '10' ? '0' : '') + $scope.testDate2.date.getSeconds());


            tglTDP.slice(0, 10);


            var PLODDate = $scope.SelectedKendaraan.PLOD.getFullYear() + '-' +
                ('0' + ($scope.SelectedKendaraan.PLOD.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.SelectedKendaraan.PLOD.getDate()).slice(-2) + 'T' +
                (($scope.SelectedKendaraan.PLOD.getHours() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getHours()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getMinutes() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getMinutes()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getSeconds() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var temDirectPDC = false;
            var sentSTNK = false;
            var choosePolise = false;

            if (typeof $scope.SelectedFormPDD.DirectDeliveryDariPDC == "undefined") {
                temDirectPDC = false;
            } else {
                temDirectPDC = true;
            }

            if (typeof $scope.SelectedFormPDD.PengirimanDenganSTNK == "undefined") {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }

            if (typeof $scope.SelectedAjuDiskon.CeklistPilihNopol == "undefined") {
                choosePolise = false;
            } else {
                choosePolise = true;
            }

            var accBit = false;
            if ($scope.tempListAcc.length == 0) {
                accBit = false;
            } else {
                accBit = true;
            }

            var karoBit = false;
            if ($scope.tempListKaroseri.length == 0) {
                karoBit = false;
            } else {
                karoBit = true;
            }

            var offRoad = false;
            if (typeof $scope.mNegotiation_Pdd.OnOffTheRoad == 'undefined' || $scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                offRoad = false;
            } else {
                offRoad = true;
            }

            console.log('kal', $scope.vehicleselcted);
            var getlKalkulasi = "?&RRNNo=" + $scope.PilihPdd.RRNNo + "&FrameNo=" + $scope.PilihPdd.FrameNo + "&FundSourceId=" + $scope.PilihPdd.FundSourceId + "&DocCompRecDate=" + tglDOc + "&TDPDate=" + tglTDP + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + temDirectPDC + "&ChoosePoliceNo=" + choosePolise + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA + "&KaroseriBit=" + karoBit + "&AccesoriesBit=" + accBit + "&OffTheRoad=" + offRoad + "&VehicleId=" + $scope.vehicleselcted[0].VehicleId;
            ProspectFactory.getKalkulasiPDD(getlKalkulasi).then(
                function(res) {

                    $scope.bind_data.data = res.data.Result;
                    $scope.DateStart();

                    var date01 = $scope.bind_data.data[0]['DocCompRecDate'];
                    var date02 = $scope.bind_data.data[0]['TDPDate'];
                    // var date03 = $scope.bind_data.data[0]['FullPaymentDate'];

                    $scope.testDate1 = {
                        date: date01,
                        status: 'firstDate'
                    };

                    $scope.events.push($scope.testDate1);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate2 = {
                        date: date02,
                        status: 'secondDate'
                    };

                    $scope.events.push($scope.testDate2);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);
                    // $scope.testDate3 = {
                    //     date: date03,
                    //     status: 'thirdDate'
                    // };

                    $scope.fourthDateChange();
                    $scope.sixDateChange();
                    //$scope.fifthDateChange();
                    var date05 = $scope.bind_data.data[0]['SentUnitDate'];
                    var pengirimanSTNK = $scope.bind_data.data[0]['SentWithSTNK'];
                    
                    if(pengirimanSTNK==true){
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(date05),
                        };

                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };
                        
                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDKredit = true;
                        $scope.MainPDDKredit = false;

                    }else{
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(),
                        };
                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };
                        
                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDKredit = true;
                        $scope.MainPDDKredit = false;
                    }

                    // $scope.testDate5 = {
                    //     date: date05,
                    //     status: 'fifthDate'
                    // };

                    // $scope.events.push($scope.testDate5);
                    // $scope.KalkulasiPDDKredit = true;
                    // $scope.MainPDDKredit = false;
                },
                function(err) {

                    bsNotify.show({
                        title: "Peringatan",
                        content: err.data.Message,
                        type: 'warning'
                    });
                }
            )



        };

        $scope.cancelPDDNorangkaCredit = function() {
            $scope.MainPDDKredit = true;
            $scope.KalkulasiPDDKredit = false;
        }

        $scope.disabledDiskon = false;
        $scope.disabledBookFee = false;
        $scope.simpanPDDCredit = function() {
            var timeEta = $scope.mytime2;
            var dateEta = $scope.testDate5.date;
            //for (var i in $scope.bind_data) { $scope.bind_data[i].SentUnitTime = timeEta };
            //for (var i in $scope.bind_data) { $scope.bind_data[i].SentUnitDate = dateEta };
            try{
                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                    ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                    ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                    (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                    (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
                                    if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                                        bsNotify.show({
                                            title: "Peringatan",
                                            content: "Tanggal Pengiriman Tidak Boleh Kurang Dari Tanggal Suggest PDD, Tanggal Suggest PDD : " + $scope.bind_data.data[0]['SentUnitDate'],
                                            type: 'warning'
                                        });
                                    } else {
                        
                                        if ($scope.flagPDD == 'Revisi') {
                        
                                            $scope.InsertRevisiPDD = [];
                                            for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                                            $scope.bind_data.data[0].ChoosePoliceNo = $scope.vehicleselcted[0].ChoosePoliceNo;
                                            $scope.bind_data.data[0].DirectDeliveryPDC = $scope.SelectDataRevisi.DirectDeliveryPDC;
                                            $scope.bind_data.data[0].EarliestPDD = $scope.TLSDate.earliestPDD;
                                            $scope.bind_data.data[0].OnOffTheRoad = $scope.SelectDataRevisi.OffTheRoad;
                                            //$scope.bind_data.data[0].OnOffTheRoadId =
                                            //$scope.bind_data.data[0].SentUnitTime =
                                            $scope.bind_data.data[0].SentWithSTNK = $scope.SelectDataRevisi.SentWithSTNK;
                                            $scope.bind_data.data[0].StatusUnitId = $scope.vehicleselcted[0].StatusUnitId;
                                            $scope.bind_data.data[0].DetailBookingUnitId = $scope.vehicleselcted[0].DetailBookingUnitId;
                                            $scope.bind_data.data[0].ReasonRevisionId = $scope.PilihanRevisiBooking.ReasonRevisionId;
                        
                                            $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                                ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                                ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                                (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                                (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                                (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
                        
                        
                                            $scope.InsertRevisiPDD.push($scope.bind_data.data[0]);
                        
                        
                                            ListBookingUnitFactory.simpanRevisiPDDBooking($scope.InsertRevisiPDD).then(
                                                function() {
                        
                                                    $rootScope.$emit('RefreshDirective', {})
                                                    $scope.inputDataPDD = [];
                                                    $scope.tampungInputBooking = [];
                                                    $scope.KalkulasiPDDKredit = false;
                                                    $scope.ShowListBookingUnit = true;
                                                    $scope.disabledDiskon = true;
                                                    $scope.disabledBookFee = true;
                        
                                                },
                                                function(err) {
                        
                                                    bsNotify.show({
                                                        title: "Gagal",
                                                        content: "Revisi PDD Gagal",
                                                        type: 'danger'
                                                    });
                                                }
                        
                                            )
                        
                                        } else {
                        
                                            $scope.KalkulasiTLSDate.RequestedPDD = null;
                                            $scope.KalkulasiTLSDate.RequestedPDD = $scope.testDate5.date;
                        
                                            for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                                            for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                                            for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitDate = dateEta };
                        
                                            $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-' +
                                                ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-' +
                                                ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T' +
                                                (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':' +
                                                (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':' +
                                                (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
                        
                        
                                            for (var i in $scope.bind_data.data) {
                                                $scope.bind_data.data[i].SentUnitDate = dateEta;
                                                try {
                                                    $scope.bind_data.data[i].SentUnitDate = $scope.bind_data.data[i].SentUnitDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].SentUnitDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].SentUnitDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].SentUnitDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].SentUnitDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].SentUnitDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getSeconds());
                                                } catch (e1) {
                                                    $scope.bind_data.data[i].SentUnitDate = null;
                                                }
                        
                                                try {
                                                    $scope.bind_data.data[i].DocCompRecDate = $scope.bind_data.data[i].DocCompRecDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].DocCompRecDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].DocCompRecDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getSeconds());
                                                } catch (e1) {
                                                    $scope.bind_data.data[i].DocCompRecDate = null;
                                                }
                        
                                                try {
                                                    $scope.bind_data.data[i].DPDate = $scope.bind_data.data[i].DPDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].DPDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].DPDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getSeconds());
                                                } catch (e2) {
                                                    $scope.bind_data.data[i].DPDate = null;
                                                }
                        
                                                try {
                                                    $scope.bind_data.data[i].TDPDate = $scope.bind_data.data[i].TDPDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].TDPDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].TDPDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getSeconds());
                                                } catch (e3) {
                                                    $scope.bind_data.data[i].TDPDate = null;
                                                }
                        
                                                try {
                                                    $scope.bind_data.data[i].FullPaymentDate = $scope.bind_data.data[i].FullPaymentDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].FullPaymentDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].FullPaymentDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getSeconds());
                                                } catch (e4) {
                                                    $scope.bind_data.data[i].FullPaymentDate = null;
                                                }
                        
                                                try {
                                                    $scope.bind_data.data[i].STNKDoneDate = $scope.bind_data.data[i].STNKDoneDate.getFullYear() + '-' +
                                                        ('0' + ($scope.bind_data.data[i].STNKDoneDate.getMonth() + 1)).slice(-2) + '-' +
                                                        ('0' + $scope.bind_data.data[i].STNKDoneDate.getDate()).slice(-2) + 'T' +
                                                        (($scope.bind_data.data[i].STNKDoneDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getHours()) + ':' +
                                                        (($scope.bind_data.data[i].STNKDoneDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getMinutes()) + ':' +
                                                        (($scope.bind_data.data[i].STNKDoneDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getSeconds());
                                                } catch (e4) {
                                                    $scope.bind_data.data[i].STNKDoneDate = null;
                                                }
                                            };
                        
                                            $scope.TempPDD = $scope.bind_data.data;
                                            if (typeof $scope.checkDirectDeliveri == "undefined") {
                                                $scope.mNegotiation_Pdd.DirectDeliveryPDC = false;
                                            }
                        
                                            if (typeof $scope.checkOnOffTheRoad == "undefined") {
                                                $scope.mNegotiation_Pdd.OnOffTheRoad = false;
                                            }
                        
                                            if (typeof $scope.checkSentWithSTNK == "undefined") {
                                                $scope.mNegotiation_Pdd.SentWithSTNK = false;
                                            }
                        
                                            if (typeof $scope.checkNoPol == "undefined") {
                                                $scope.SelectedAjuDiskon.ChoosePoliceNo = false;
                                            }
                        
                                            $scope.mNegotiation_Pdd.OnOffTheRoadId = null;
                                            if ($scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                                                $scope.mNegotiation_Pdd.OnOffTheRoadId = 1;
                                            }
                                            if ($scope.mNegotiation_Pdd.OnOffTheRoad == true) {
                                                $scope.mNegotiation_Pdd.OnOffTheRoadId = 2;
                                            }
                        
                        
                                            var selectPoliceNo = {};
                                            selectPoliceNo.ChoosePoliceNo = {};
                                            selectPoliceNo.ChoosePoliceNo = $scope.SelectedAjuDiskon.ChoosePoliceNo;
                        
                                            var StatusUnit = {};
                                            StatusUnit.StatusUnitId = {};
                                            StatusUnit.StatusUnitId = $scope.SelectedKendaraan.StatusUnitId;
                        
                        
                                            $scope.tampungPDD = angular.merge({}, $scope.TempPDD[0], $scope.mNegotiation_Pdd, selectPoliceNo, StatusUnit)
                        
                                            $scope.inputDataPDD.push($scope.tampungPDD);
                        
                                            $scope.BookUnit.ListDetailBookingUnit = [];
                                            $scope.BookUnit.ListDetailBookingUnit.push($scope.SelectedKendaraan);
                                            $scope.BookUnit.FundSourceId = $scope.SelectedFundSource.FundSourceId;
                        
                                            $scope.BookUnit.SNegotiationSimulasiKredit = null;
                                            $scope.BookUnit.SNegotiationSimulasiAsuransi = null;
                        
                                            var karoId = 0;
                                            var karoPrice = 0;
                                            var karoppn = 0;
                                            var karototalvat = 0;
                                            var dpp = 0;
                        
                                            if ($scope.inputKaroseri.length == 0) {
                        
                                                karoId = 0
                                                karoPrice = 0;
                                                karoppn = 0;
                                                karototalvat = 0;
                                                dpp = 0;
                                            } else {
                        
                                                karoId = $scope.inputKaroseri[0].KaroseriId;
                                                karoPrice = $scope.inputKaroseri[0].Price;
                                                karoppn = $scope.inputKaroseri[0].PPN;
                                                karototalvat = $scope.inputKaroseri[0].TotalIncludeVAT;
                                                dpp = $scope.inputKaroseri[0].KaroseriDPP;
                                            }
                        
                                            if (typeof $scope.tempNopol.ChoosePoliceNo == "undefined") {
                                                $scope.tempNopol.ChoosePoliceNo = false;
                                            } else {
                                                $scope.tempNopol.ChoosePoliceNo = true;
                                            }
                        
                        
                                            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = [];
                                            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = [];
                                            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = $scope.inputAcc;
                                            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = $scope.inputAccPaket;
                        
                        
                                            $scope.BookUnit.ListDetailBookingUnit[0].KaroseriId = karoId;
                                            $scope.BookUnit.ListDetailBookingUnit[0].KaroseriPrice = karoPrice;
                                            $scope.BookUnit.ListDetailBookingUnit[0].KaroseriDPP = dpp;
                        
                                            $scope.BookUnit.ListDetailBookingUnit[0].PPNKaroseri = karoppn;
                                            $scope.BookUnit.ListDetailBookingUnit[0].TotalIncludeVATKaroseri = karototalvat;
                        
                        
                                            $scope.BookUnit.ListDetailBookingUnit[0].ChoosePoliceNo = $scope.tempNopol.ChoosePoliceNo;
                        
                                            $scope.BookUnit.ListDetailBookingUnit[0].NumberPlateId = $scope.listPlateNumber[0].NumberPlateId;
                                            $scope.BookUnit.ListDetailBookingUnit[0].PoliceNo = $scope.listPlateNumber[0].PlatNomor;
                        
                                            for (var i in $scope.ArrayStockInfoAvaliability) {
                                                if ($scope.ArrayStockInfoAvaliability[i].RRNNo == $scope.SelectedKendaraan.RRNNo && $scope.ArrayStockInfoAvaliability[i].FrameNo == $scope.SelectedKendaraan.FrameNo) {
                                                    $scope.ArrayStockInfoAvaliability.splice(i, 1);
                                                    $scope.Jumlah_Stock_Free = $scope.Jumlah_Stock_Free - 1;
                                                }
                                            }
                        
                                            $scope.selectedTipePlate = [];
                        
                                            $scope.inputBookingUnit = [];
                                            $scope.inputBookingUnit = angular.merge({}, $scope.BookUnit, $scope.mNegotiation_Pdd);
                                            $scope.BookUnit.ListDetailBookingUnit[0] = angular.merge({}, $scope.BookUnit.ListDetailBookingUnit[0], $scope.tampungBookFee);
                                            $scope.tampungInputBooking.push($scope.BookUnit.ListDetailBookingUnit[0]);
                        
                        
                                            if ($scope.countData == $scope.TotalQuantity) {
                        
                                                $scope.KalkulasiPDDKredit = false;
                                                $scope.Show_RincianHarga = true;
                                                $scope.GetDataFromFactory();
                                            } else {
                                                if ($scope.countData == $scope.Head_Info_Kendaraan.Qty) {
                                                    $scope.Show_RincianHarga = true;
                                                    $scope.KalkulasiPDDKredit = false;
                                                    //$scope.GetDataFromFactory();
                                                } else {
                                                    $scope.Show_Stok_Info_Booking_Unit = true;
                                                    $scope.KalkulasiPDDKredit = false;
                                                    // $scope.checkNoPol = false;
                                                    // $scope.disabledPlatNomor = false;
                                                }
                        
                        
                                            }
                                            $scope.countData++;
                                            $scope.countDataItem++;
                        
                                            if ($scope.tempListPaketAcc.length == 0) {
                                                //$scope.tempAksesoris.Jumlah = 0;
                                                var jmlAksesorisPaket = 0;
                                                var PriceAksesoris = 0;
                                                var TotalPaket = 0;
                        
                                            } else
                                            if ($scope.tempListAcc.length == 0) {
                                                var jmlAksesoris = 0;
                                                var RetailAccPrice = 0;
                                                var TotalAcc = 0;
                                            } else {
                        
                                                for (var j in $scope.tempListPaketAcc) {
                                                    TotalPaket = TotalPaket + ($scope.tempListPaketAcc[j].SubTotalPaket);
                        
                                                }
                        
                                                for (var k in $scope.tempListAcc) {
                                                    TotalAcc = TotalAcc + ($scope.tempListAcc[j].SubTotalAcc);
                        
                                                }
                        
                                            }
                        
                        
                        
                                            if ($scope.tempListKaroseri.length == 0) {
                                                //$scope.tempListKaroseri.Price = 0;
                        
                                                var PriceKaroseri = 0;
                        
                                            } else {
                                                var PriceKaroseri = $scope.tempListKaroseri[0].Price;
                                            }
                        
                                            if (typeof $scope.SelectedAjuDiskon.BookingFee == "undefined") {
                                                var bookFee = 0;
                                            } else {
                                                var bookFee = $scope.SelectedAjuDiskon.BookingFee;
                                            }
                        
                                            if (typeof $scope.SelectedAjuDiskon.RequestDiscount == "undefined") {
                                                var ajuDisc = 0;
                                            } else {
                                                var ajuDisc = $scope.SelectedAjuDiskon.RequestDiscount;
                                            }
                        
                        
                                            $scope.grandTotal = ($scope.Head_Info_Kendaraan.Qty * $scope.Head_Info_Kendaraan.Price) - ($scope.Head_Info_Kendaraan.Qty * bookFee) + TotalPaket + TotalAcc + ($scope.Head_Info_Kendaraan.Qty * PriceKaroseri) - ($scope.Head_Info_Kendaraan.Qty * ajuDisc);
                        
                        
                                            $scope.tempListPaketAcc = [];
                                            $scope.tempListAcc = [];
                                            $scope.tempListKaroseri = [];
                                            $scope.tampungBookFee = { BookingFee: 0, RequestDiscount: 0 };
                                            $scope.listTampungBookFee = [];
                                            $scope.inputAccPaket = [];
                                            $scope.inputAcc = [];
                                            $scope.inputKaroseri = [];
                                            $scope.tampungReqBookFee = $scope.SelectedAjuDiskon.BookingFee;
                                            $scope.tampungReqDiskon = $scope.SelectedAjuDiskon.RequestDiscount;
                                        }
                        
                        
                                    }
                                }
            catch(ex){
                bsNotify.show({
                    title: "Peringatan",
                    content: "ETA Pelanggan tidak boleh lebih dari 23:59",
                    type: 'warning'
                });
            }

            
        };

        $scope.kalkulasiRRNCash = function() {
            var tglDOc = $scope.testDateRRN.date.getFullYear() + '-' +
                ('0' + ($scope.testDateRRN.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDateRRN.date.getDate()).slice(-2) + 'T' +
                (($scope.testDateRRN.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN.date.getHours()) + ':' +
                (($scope.testDateRRN.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN.date.getMinutes()) + ':' +
                (($scope.testDateRRN.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN.date.getSeconds());

            var tglDP = $scope.testDateRRN2.date.getFullYear() + '-' +
                ('0' + ($scope.testDateRRN2.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDateRRN2.date.getDate()).slice(-2) + 'T' +
                (($scope.testDateRRN2.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN2.date.getHours()) + ':' +
                (($scope.testDateRRN2.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN2.date.getMinutes()) + ':' +
                (($scope.testDateRRN2.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN2.date.getSeconds());

            var tglFUllPayment = $scope.testDateRRN3.date.getFullYear() + '-' +
                ('0' + ($scope.testDateRRN3.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDateRRN3.date.getDate()).slice(-2) + 'T' +
                (($scope.testDateRRN3.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN3.date.getHours()) + ':' +
                (($scope.testDateRRN3.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN3.date.getMinutes()) + ':' +
                (($scope.testDateRRN3.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN3.date.getSeconds());

            tglDOc.slice(0, 10);
            tglDP.slice(0, 10);
            tglFUllPayment.slice(0, 10);

            var PLODDate = $scope.SelectedKendaraan.PLOD.getFullYear() + '-' +
                ('0' + ($scope.SelectedKendaraan.PLOD.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.SelectedKendaraan.PLOD.getDate()).slice(-2) + 'T' +
                (($scope.SelectedKendaraan.PLOD.getHours() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getHours()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getMinutes() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getMinutes()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getSeconds() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var temDirectPDC = false;
            var sentSTNK = false;
            var choosePolise = false;
            if (typeof $scope.SelectedFormPDD.DirectDeliveryDariPDC == "undefined") {
                temDirectPDC = false;
            } else {
                temDirectPDC = true;
            }

            if (typeof $scope.SelectedFormPDD.PengirimanDenganSTNK == "undefined") {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }

            if (typeof $scope.SelectedAjuDiskon.CeklistPilihNopol == "undefined") {
                choosePolise = false;
            } else {
                choosePolise = true;
            }

            var accBit = false;
            if ($scope.tempListAcc.length == 0) {
                accBit = false;
            } else {
                accBit = true;
            }

            var karoBit = false;
            if ($scope.tempListKaroseri.length == 0) {
                karoBit = false;
            } else {
                karoBit = true;
            }

            var offRoad = false;
            if (typeof $scope.mNegotiation_Pdd.OnOffTheRoad == 'undefined' || $scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                offRoad = false;
            } else {
                offRoad = true;
            }

            console.log('kal', $scope.vehicleselcted);
            var getlKalkulasi = "?&RRNNo=" + $scope.PilihPdd.RRNNo + "&FrameNo=" + $scope.PilihPdd.FrameNo + "&FundSourceId=" + $scope.PilihPdd.FundSourceId + "&DocCompRecDate=" + tglDOc + "&DPDate=" + tglDP + "&FullPaymentDate=" + tglFUllPayment + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + temDirectPDC + "&ChoosePoliceNo=" + choosePolise + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA + "&KaroseriBit=" + karoBit + "&AccesoriesBit=" + accBit + "&OffTheRoad=" + offRoad + "&VehicleId=" + $scope.vehicleselcted[0].VehicleId;
            ProspectFactory.getKalkulasiPDD(getlKalkulasi).then(
                function(res) {

                    $scope.bind_data.data = res.data.Result;

                    $scope.events = [];
                    $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];
                    //$scope.Color_Da_CalendarAtLihat=[{date:null,status: 'fifthDate'}];
                    $scope.Color_Da_CalendarAtLihat.splice(0, 1);

                    $scope.a_awal = new Date($scope.bind_data.data[0].SentUnitDateWithoutWDP);
                    $scope.b_akhir = new Date($scope.bind_data.data[0].SentUnitDate);
                    var oneDayCalculation = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                    var diffDaysRRN = Math.round(Math.abs(($scope.b_akhir.getTime() - $scope.a_awal.getTime()) / (oneDayCalculation)));
                    var diffDaysRRNForLoop = diffDaysRRN + 1;

                    for (var i = 0; i < diffDaysRRNForLoop; i++) {
                        var date_to_insert = new Date(angular.copy($scope.a_awal).setDate(angular.copy($scope.a_awal).getDate() + i)); //ini tadinya i bukan 1 daripada a semua pake angular copy

                        date_to_insert = date_to_insert.getFullYear() + '-' + ('0' + (date_to_insert.getMonth() + 1)) + '-' + ('0' + (date_to_insert.getDate())).slice(-2) + '';
                        $scope.Color_Da_CalendarAtLihat.push({ date: date_to_insert, status: 'fifthDate' });
                    }

                    for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                        $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                        $scope.refreshCalendar = false;
                        $timeout(function() { $scope.refreshCalendar = true }, 0);
                    }

                    $scope.KalkulasiRRNCash = true;
                    $scope.MainRRNCash = false;
                },
                function(err) {

                    bsNotify.show({
                        title: "Peringatan",
                        content: err.data.Message,
                        type: 'warning'
                    });
                }
            )
        }

        $scope.cancelkalkulasiRRNCash = function() {
            $scope.MainRRNCash = true;
            $scope.KalkulasiRRNCash = false;
        }

        $scope.simpanRRNCash = function() {
            $scope.TempPDD = $scope.bind_data.data;

            if (typeof $scope.checkDirectDeliveri == "undefined") {
                $scope.mNegotiation_Pdd.DirectDeliveryPDC = false;
            }

            if (typeof $scope.checkOnOffTheRoad == "undefined") {
                $scope.mNegotiation_Pdd.OnOffTheRoad = false;
            }

            if (typeof $scope.checkSentWithSTNK == "undefined") {
                $scope.mNegotiation_Pdd.SentWithSTNK = false;
            }

            if (typeof $scope.checkNoPol == "undefined") {
                $scope.SelectedAjuDiskon.ChoosePoliceNo = false;
            }

            $scope.mNegotiation_Pdd.OnOffTheRoadId = null;
            if ($scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                $scope.mNegotiation_Pdd.OnOffTheRoadId = 1;
            }
            if ($scope.mNegotiation_Pdd.OnOffTheRoad == true) {
                $scope.mNegotiation_Pdd.OnOffTheRoadId = 2;
            }

            var selectPoliceNo = {};
            selectPoliceNo.ChoosePoliceNo = {};
            selectPoliceNo.ChoosePoliceNo = $scope.SelectedAjuDiskon.ChoosePoliceNo;

            var StatusUnit = {};
            StatusUnit.StatusUnitId = {};
            StatusUnit.StatusUnitId = $scope.SelectedKendaraan.StatusUnitId;


            $scope.tampungPDD = angular.merge({}, $scope.TempPDD[0], $scope.mNegotiation_Pdd, selectPoliceNo, StatusUnit)

            $scope.inputDataPDD.push($scope.tampungPDD);


            var karoId = 0;
            var karoPrice = 0;
            var karoppn = 0;
            var karototalvat = 0;
            var dpp = 0;

            if ($scope.inputKaroseri.length == 0) {

                karoId = 0
                karoPrice = 0;
                karoppn = 0;
                karototalvat = 0;
                dpp = 0;
            } else {

                karoId = $scope.inputKaroseri[0].KaroseriId;
                karoPrice = $scope.inputKaroseri[0].Price;
                karoppn = $scope.inputKaroseri[0].PPN;
                karototalvat = $scope.inputKaroseri[0].TotalIncludeVAT;
                dpp = $scope.inputKaroseri[0].KaroseriDPP;
            }

            if (typeof $scope.tempNopol.ChoosePoliceNo == "undefined") {
                $scope.tempNopol.ChoosePoliceNo = false;
            } else {
                $scope.tempNopol.ChoosePoliceNo = true;
            }

            $scope.BookUnit.ListDetailBookingUnit = [];
            $scope.BookUnit.ListDetailBookingUnit.push($scope.SelectedKendaraan);
            $scope.BookUnit.FundSourceId = $scope.SelectedFundSource.FundSourceId;

            $scope.BookUnit.SNegotiationSimulasiKredit = null;
            $scope.BookUnit.SNegotiationSimulasiAsuransi = null;

            $scope.BookUnit.ListDetailBookingUnit[0].PPNKaroseri = karoppn;
            $scope.BookUnit.ListDetailBookingUnit[0].TotalIncludeVATKaroseri = karototalvat;
            $scope.BookUnit.ListDetailBookingUnit[0].KaroseriDPP = dpp;

            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = [];
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = [];
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = $scope.inputAcc;
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = $scope.inputAccPaket;

            $scope.BookUnit.ListDetailBookingUnit[0].ChoosePoliceNo = $scope.tempNopol.ChoosePoliceNo;

            //BookingUnit_AjuDiskon.ChoosePoliceNo

            $scope.BookUnit.ListDetailBookingUnit[0].NumberPlateId = $scope.listPlateNumber[0].NumberPlateId;
            $scope.BookUnit.ListDetailBookingUnit[0].PoliceNo = $scope.listPlateNumber[0].PlatNomor;

            for (var i in $scope.ArrayStockInfoAvaliability) {
                if ($scope.ArrayStockInfoAvaliability[i].RRNNo == $scope.SelectedKendaraan.RRNNo && $scope.ArrayStockInfoAvaliability[i].FrameNo == $scope.SelectedKendaraan.FrameNo) {
                    $scope.ArrayStockInfoAvaliability.splice(i, 1);
                    $scope.Jumlah_Stock_Free = $scope.Jumlah_Stock_Free - 1;
                }
            }

            $scope.selectedTipePlate = [];

            $scope.inputBookingUnit = [];
            $scope.inputBookingUnit = angular.merge({}, $scope.BookUnit, $scope.mNegotiation_Pdd, $scope.tampungBookFee);
            $scope.BookUnit.ListDetailBookingUnit[0] = angular.merge({}, $scope.BookUnit.ListDetailBookingUnit[0], $scope.tampungBookFee);
            $scope.tampungInputBooking.push($scope.BookUnit.ListDetailBookingUnit[0]);
            //$scope.tampungInputBooking = angular.merge([],$scope.tampungInputBooking, $scope.tampungBookFee);

            if ($scope.countData == $scope.TotalQuantity) {

                $scope.KalkulasiRRNCash = false;
                $scope.Show_RincianHarga = true;
                $scope.GetDataFromFactory();
            } else {
                if ($scope.countData == $scope.Head_Info_Kendaraan.Qty) {
                    $scope.Show_RincianHarga = true;
                    $scope.KalkulasiRRNCash = false;
                    //$scope.GetDataFromFactory();
                } else {
                    $scope.Show_Stok_Info_Booking_Unit = true;
                    $scope.KalkulasiRRNCash = false;
                }


            }
            $scope.countData++;
            $scope.countDataItem++;

            if ($scope.tempListPaketAcc.length == 0) {
                //$scope.tempAksesoris.Jumlah = 0;
                var jmlAksesorisPaket = 0;
                var PriceAksesoris = 0;
                var TotalPaket = 0;

            } else
            if ($scope.tempListAcc.length == 0) {
                var jmlAksesoris = 0;
                var RetailAccPrice = 0;
                var TotalAcc = 0;
            } else {

                for (var j in $scope.tempListPaketAcc) {
                    TotalPaket = TotalPaket + ($scope.tempListPaketAcc[j].SubTotalPaket);

                }

                for (var k in $scope.tempListAcc) {
                    TotalAcc = TotalAcc + ($scope.tempListAcc[j].SubTotalAcc);

                }

            }



            if ($scope.tempListKaroseri.length == 0) {
                //$scope.tempListKaroseri.Price = 0;

                var PriceKaroseri = 0;

            } else {
                var PriceKaroseri = $scope.tempListKaroseri[0].Price;
            }

            if (typeof $scope.SelectedAjuDiskon.BookingFee == "undefined") {
                var bookFee = 0;
            } else {
                var bookFee = $scope.SelectedAjuDiskon.BookingFee;
            }

            if (typeof $scope.SelectedAjuDiskon.RequestDiscount == "undefined") {
                var ajuDisc = 0;
            } else {
                var ajuDisc = $scope.SelectedAjuDiskon.RequestDiscount;
            }


            $scope.grandTotal = ($scope.Head_Info_Kendaraan.Qty * $scope.Head_Info_Kendaraan.Price) - ($scope.Head_Info_Kendaraan.Qty * bookFee) + TotalPaket + TotalAcc + ($scope.Head_Info_Kendaraan.Qty * PriceKaroseri) - ($scope.Head_Info_Kendaraan.Qty * ajuDisc);


            $scope.tempListPaketAcc = [];
            $scope.tempListAcc = [];
            $scope.tempListKaroseri = [];
            $scope.tampungBookFee = { BookingFee: 0, RequestDiscount: 0 };
            $scope.listTampungBookFee = [];
            $scope.tampungReqBookFee = $scope.SelectedAjuDiskon.BookingFee;
            $scope.tampungReqDiskon = $scope.SelectedAjuDiskon.RequestDiscount;
        };

        $scope.kalkulasiRRNCredit = function() {
            var tglDOc = $scope.testDateRRN.date.getFullYear() + '-' +
                ('0' + ($scope.testDateRRN.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDateRRN.date.getDate()).slice(-2) + 'T' +
                (($scope.testDateRRN.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN.date.getHours()) + ':' +
                (($scope.testDateRRN.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN.date.getMinutes()) + ':' +
                (($scope.testDateRRN.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN.date.getSeconds());

            var tglTDP = $scope.testDateRRN2.date.getFullYear() + '-' +
                ('0' + ($scope.testDateRRN2.date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.testDateRRN2.date.getDate()).slice(-2) + 'T' +
                (($scope.testDateRRN2.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN2.date.getHours()) + ':' +
                (($scope.testDateRRN2.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN2.date.getMinutes()) + ':' +
                (($scope.testDateRRN2.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN2.date.getSeconds());

            tglDOc.slice(0, 10);
            tglTDP.slice(0, 10);

            var PLODDate = $scope.SelectedKendaraan.PLOD.getFullYear() + '-' +
                ('0' + ($scope.SelectedKendaraan.PLOD.getMonth() + 1)).slice(-2) + '-' +
                ('0' + $scope.SelectedKendaraan.PLOD.getDate()).slice(-2) + 'T' +
                (($scope.SelectedKendaraan.PLOD.getHours() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getHours()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getMinutes() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getMinutes()) + ':' +
                (($scope.SelectedKendaraan.PLOD.getSeconds() < '10' ? '0' : '') + $scope.SelectedKendaraan.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var temDirectPDC = false;
            var sentSTNK = false;
            var choosePolise = false;
            if (typeof $scope.SelectedFormPDD.DirectDeliveryDariPDC == "undefined") {
                temDirectPDC = false;
            } else {
                temDirectPDC = true;
            }

            if (typeof $scope.SelectedFormPDD.PengirimanDenganSTNK == "undefined") {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }

            if (typeof $scope.SelectedAjuDiskon.CeklistPilihNopol == "undefined") {
                choosePolise = false;
            } else {
                choosePolise = true;
            }

            var accBit = false;
            if ($scope.tempListAcc.length == 0) {
                accBit = false;
            } else {
                accBit = true;
            }

            var karoBit = false;
            if ($scope.tempListKaroseri.length == 0) {
                karoBit = false;
            } else {
                karoBit = true;
            }

            var offRoad = false;
            if (typeof $scope.mNegotiation_Pdd.OnOffTheRoad == 'undefined' || $scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                offRoad = false;
            } else {
                offRoad = true;
            }

            console.log('kal', $scope.vehicleselcted);
            var getlKalkulasi = "?&RRNNo=" + $scope.PilihPdd.RRNNo + "&FrameNo=" + $scope.PilihPdd.FrameNo + "&FundSourceId=" + $scope.PilihPdd.FundSourceId + "&DocCompRecDate=" + tglDOc + "&TDPDate=" + tglTDP + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + temDirectPDC + "&ChoosePoliceNo=" + choosePolise + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA + "&KaroseriBit=" + karoBit + "&AccesoriesBit=" + accBit + "&OffTheRoad=" + offRoad + "&VehicleId=" + $scope.vehicleselcted[0].VehicleId;
            ProspectFactory.getKalkulasiPDD(getlKalkulasi).then(
                function(res) {

                    $scope.bind_data.data = res.data.Result;

                    $scope.events = [];
                    $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];

                    $scope.Color_Da_CalendarAtLihat.splice(0, 1);

                    $scope.a_awal = new Date($scope.bind_data.data[0].SentUnitDateWithoutWDP);
                    $scope.b_akhir = new Date($scope.bind_data.data[0].SentUnitDate);
                    var oneDayCalculation = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                    var diffDaysRRN = Math.round(Math.abs(($scope.b_akhir.getTime() - $scope.a_awal.getTime()) / (oneDayCalculation)));
                    var diffDaysRRNForLoop = diffDaysRRN + 1;

                    for (var i = 0; i < diffDaysRRNForLoop; i++) {
                        var date_to_insert = new Date(angular.copy($scope.a_awal).setDate(angular.copy($scope.a_awal).getDate() + i)); //ini tadinya i bukan 1 daripada a semua pake angular copy

                        date_to_insert = date_to_insert.getFullYear() + '-' + ('0' + (date_to_insert.getMonth() + 1)) + '-' + ('0' + (date_to_insert.getDate())).slice(-2) + '';
                        $scope.Color_Da_CalendarAtLihat.push({ date: date_to_insert, status: 'fifthDate' });
                    }

                    for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                        $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                        $scope.refreshCalendar = false;
                        $timeout(function() { $scope.refreshCalendar = true }, 0);
                    }
                    $scope.KalkulasiRRNKredit = true;
                    $scope.MainRRNKredit = false;
                },
                function(err) {

                    bsNotify.show({
                        title: "Peringatan",
                        content: err.data.Message,
                        type: 'warning'
                    });
                }
            )



        }

        $scope.cancelkalkulasiRRNCredit = function() {
            $scope.MainRRNKredit = true;
            $scope.KalkulasiRRNKredit = false;
        }

        $scope.simpanRRNCredit = function() {
            $scope.TempPDD = $scope.bind_data.data;
            if (typeof $scope.checkDirectDeliveri == "undefined") {
                $scope.mNegotiation_Pdd.DirectDeliveryPDC = false;
            }

            if (typeof $scope.checkOnOffTheRoad == "undefined") {
                $scope.mNegotiation_Pdd.OnOffTheRoad = false;
            }

            if (typeof $scope.checkSentWithSTNK == "undefined") {
                $scope.mNegotiation_Pdd.SentWithSTNK = false;
            }

            if (typeof $scope.checkNoPol == "undefined") {
                $scope.SelectedAjuDiskon.ChoosePoliceNo = false;
            }

            $scope.mNegotiation_Pdd.OnOffTheRoadId = null;
            if ($scope.mNegotiation_Pdd.OnOffTheRoad == false) {
                $scope.mNegotiation_Pdd.OnOffTheRoadId = 1;
            }
            if ($scope.mNegotiation_Pdd.OnOffTheRoad == true) {
                $scope.mNegotiation_Pdd.OnOffTheRoadId = 2;
            }

            var selectPoliceNo = {};
            selectPoliceNo.ChoosePoliceNo = {};
            selectPoliceNo.ChoosePoliceNo = $scope.SelectedAjuDiskon.ChoosePoliceNo;

            var StatusUnit = {};
            StatusUnit.StatusUnitId = {};
            StatusUnit.StatusUnitId = $scope.SelectedKendaraan.StatusUnitId;


            $scope.tampungPDD = angular.merge({}, $scope.TempPDD[0], $scope.mNegotiation_Pdd, selectPoliceNo, StatusUnit)


            $scope.inputDataPDD.push($scope.tampungPDD);


            var karoId = 0;
            var karoPrice = 0;
            var karoppn = 0;
            var karototalvat = 0;
            var dpp = 0;

            if ($scope.inputKaroseri.length == 0) {
                karoId = 0
                karoPrice = 0;
                karoppn = 0;
                karototalvat = 0;
                dpp = 0;
            } else {
                karoId = $scope.inputKaroseri[0].KaroseriId;
                karoPrice = $scope.inputKaroseri[0].Price;
                karoppn = $scope.inputKaroseri[0].PPN;
                karototalvat = $scope.inputKaroseri[0].TotalIncludeVAT;
                dpp = $scope.inputKaroseri[0].KaroseriDPP;
            }

            if (typeof $scope.tempNopol.ChoosePoliceNo == "undefined") {
                $scope.tempNopol.ChoosePoliceNo = false;
            } else {
                $scope.tempNopol.ChoosePoliceNo = true;
            }

            $scope.BookUnit.ListDetailBookingUnit = [];
            $scope.BookUnit.ListDetailBookingUnit.push($scope.SelectedKendaraan);
            $scope.BookUnit.FundSourceId = $scope.SelectedFundSource.FundSourceId;

            $scope.BookUnit.SNegotiationSimulasiKredit = null;
            $scope.BookUnit.SNegotiationSimulasiAsuransi = null;

            $scope.BookUnit.ListDetailBookingUnit[0].PPNKaroseri = karoppn;
            $scope.BookUnit.ListDetailBookingUnit[0].TotalIncludeVATKaroseri = karototalvat;
            $scope.BookUnit.ListDetailBookingUnit[0].KaroseriDPP = dpp;


            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = [];
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = [];
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnit = $scope.inputAcc;
            $scope.BookUnit.ListDetailBookingUnit[0].ListDetailAccBookingUnitPackage = $scope.inputAccPaket;

            $scope.BookUnit.ListDetailBookingUnit[0].ChoosePoliceNo = $scope.tempNopol.ChoosePoliceNo;

            //BookingUnit_AjuDiskon.ChoosePoliceNo

            $scope.BookUnit.ListDetailBookingUnit[0].NumberPlateId = $scope.listPlateNumber[0].NumberPlateId;
            $scope.BookUnit.ListDetailBookingUnit[0].PoliceNo = $scope.listPlateNumber[0].PlatNomor;

            for (var i in $scope.ArrayStockInfoAvaliability) {
                if ($scope.ArrayStockInfoAvaliability[i].RRNNo == $scope.SelectedKendaraan.RRNNo && $scope.ArrayStockInfoAvaliability[i].FrameNo == $scope.SelectedKendaraan.FrameNo) {
                    $scope.ArrayStockInfoAvaliability.splice(i, 1);
                    $scope.Jumlah_Stock_Free = $scope.Jumlah_Stock_Free - 1;
                }
            }

            $scope.selectedTipePlate = [];

            $scope.inputBookingUnit = [];
            $scope.inputBookingUnit = angular.merge({}, $scope.BookUnit, $scope.mNegotiation_Pdd, $scope.tampungBookFee);
            $scope.BookUnit.ListDetailBookingUnit[0] = angular.merge({}, $scope.BookUnit.ListDetailBookingUnit[0], $scope.tampungBookFee);
            $scope.tampungInputBooking.push($scope.BookUnit.ListDetailBookingUnit[0]);


            if ($scope.countData == $scope.TotalQuantity) {

                $scope.KalkulasiRRNKredit = false;
                $scope.Show_RincianHarga = true;
                $scope.GetDataFromFactory();
            } else {
                if ($scope.countData == $scope.Head_Info_Kendaraan.Qty) {
                    $scope.Show_RincianHarga = true;
                    $scope.KalkulasiRRNKredit = false;
                    //$scope.GetDataFromFactory();
                } else {
                    $scope.Show_Stok_Info_Booking_Unit = true;
                    $scope.KalkulasiRRNKredit = false;
                }


            }
            $scope.countData++;
            $scope.countDataItem++;

            if ($scope.tempListPaketAcc.length == 0) {
                //$scope.tempAksesoris.Jumlah = 0;
                var jmlAksesorisPaket = 0;
                var PriceAksesoris = 0;
                var TotalPaket = 0;

            } else
            if ($scope.tempListAcc.length == 0) {
                var jmlAksesoris = 0;
                var RetailAccPrice = 0;
                var TotalAcc = 0;
            } else {

                for (var j in $scope.tempListPaketAcc) {
                    TotalPaket = TotalPaket + ($scope.tempListPaketAcc[j].SubTotalPaket);

                }

                for (var k in $scope.tempListAcc) {
                    TotalAcc = TotalAcc + ($scope.tempListAcc[j].SubTotalAcc);

                }

            }



            if ($scope.tempListKaroseri.length == 0) {
                //$scope.tempListKaroseri.Price = 0;

                var PriceKaroseri = 0;

            } else {
                var PriceKaroseri = $scope.tempListKaroseri[0].Price;
            }

            if (typeof $scope.SelectedAjuDiskon.BookingFee == "undefined") {
                var bookFee = 0;
            } else {
                var bookFee = $scope.SelectedAjuDiskon.BookingFee;
            }

            if (typeof $scope.SelectedAjuDiskon.RequestDiscount == "undefined") {
                var ajuDisc = 0;
            } else {
                var ajuDisc = $scope.SelectedAjuDiskon.RequestDiscount;
            }


            $scope.grandTotal = ($scope.Head_Info_Kendaraan.Qty * $scope.Head_Info_Kendaraan.Price) - ($scope.Head_Info_Kendaraan.Qty * bookFee) + TotalPaket + TotalAcc + ($scope.Head_Info_Kendaraan.Qty * PriceKaroseri) - ($scope.Head_Info_Kendaraan.Qty * ajuDisc);


            $scope.tempListPaketAcc = [];
            $scope.tempListAcc = [];
            $scope.tempListKaroseri = [];
            $scope.tampungBookFee = { BookingFee: 0, RequestDiscount: 0 };
            $scope.listTampungBookFee = [];
            $scope.tampungReqBookFee = $scope.SelectedAjuDiskon.BookingFee;
            $scope.tampungReqDiskon = $scope.SelectedAjuDiskon.RequestDiscount;
        };

        $scope.Hitung = function() {
            $http.post('/api/fe/PricingEngine?PricingId=10003&OutletId=' + $scope.user.OutletId + '&DisplayId=1', $scope.jsData)
                .then(function(res) {
                    $scope.jsData = res.data;
                    var bbn = angular.copy(parseFloat($scope.jsData.Codes["[BBN]"]));
                    if (bbn == null || bbn < 1 ) {
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Nilai BBN 0, hubungi Admin",
                            type: 'warning'
                        });
                    }
                    console.log('$scope.jsData ===>', $scope.jsData);
                    $scope.Head_Info_Kendaraan.RequestDiscount = angular.copy($scope.SelectedAjuDiskon.RequestDiscount +
                        $scope.SelectedAjuDiskon.DiscountSubDp +
                        $scope.SelectedAjuDiskon.DiscountSubRate + maxDiscAcc($scope.tempListPaketAcc, $scope.tempListAcc));
                });
        }

        $scope.addDisc = {};
        $scope.simpanBookingUnit = function() {
            console.log('$scope.jsData ===>', $scope.jsData);

            if (typeof $scope.RincianHarga == "undefined") {
                $scope.RincianHarga = {};
                $scope.RincianHarga.ReasonReqDiscount = {};
                $scope.RincianHarga.ReasonReqDiscount = "";
            } else {
                for (var i in $scope.tampungInputBooking) {
                    $scope.tampungInputBooking[i].ReasonReqDiscount = $scope.RincianHarga.ReasonReqDiscount;
                }
            }
            $scope.Head_Info_Kendaraan.ReasonReqDiscount = "";
            $scope.Head_Info_Kendaraan.ReasonReqDiscount = $scope.RincianHarga.ReasonReqDiscount;
            $scope.Head_Info_Kendaraan.RequestDiscount = "";

            $scope.Head_Info_Kendaraan = angular.merge({}, $scope.Head_Info_Kendaraan, $scope.mNegotiation_Pdd);
            //error disini

            $scope.Head_Info_Kendaraan.ListDetailBookingUnit = $scope.tampungInputBooking;
            $scope.Head_Info_Kendaraan.GrandTotal = $scope.jsData.Codes["[GrandTotal]"];
            // $scope.Head_Info_Kendaraan.DiscountSubsidiRate = $scope.jsData.Codes["[DSubsidiRate]"];
            // $scope.Head_Info_Kendaraan.DiscountSubsidiDP = $scope.jsData.Codes["[DSubsidiDP]"];
            // $scope.Head_Info_Kendaraan.MediatorCommision = $scope.jsData.Codes["[MCommision]"];
            $scope.Head_Info_Kendaraan.DiscountSubsidiRate = $scope.forAjuBookingUnit.DiscountSubRate;
            $scope.Head_Info_Kendaraan.DiscountSubsidiDP = $scope.forAjuBookingUnit.DiscountSubDp;
            $scope.Head_Info_Kendaraan.MediatorCommision = $scope.jsData.Codes["[MCommision]"];
            $scope.Head_Info_Kendaraan.PriceAfterDiscount = $scope.jsData.Codes["[PriceAfterDiskon]"];
            $scope.Head_Info_Kendaraan.DPP = $scope.jsData.Codes["[DPP]"];
            $scope.Head_Info_Kendaraan.PPN = $scope.jsData.Codes["[PPN]"];
            $scope.Head_Info_Kendaraan.BBN = $scope.jsData.Codes["[BBN]"];
            $scope.Head_Info_Kendaraan.BBNAdjustment = $scope.forAjuBookingUnit.BBNAdjustment;
            $scope.Head_Info_Kendaraan.BBNServiceAdjustment = $scope.forAjuBookingUnit.BBNServiceAdjustment;
            $scope.Head_Info_Kendaraan.TotalBBN = $scope.jsData.Codes["[TotalBBN]"];
            $scope.Head_Info_Kendaraan.PPH22 = $scope.jsData.Codes["[PPH22]"];
            $scope.Head_Info_Kendaraan.TotalInclVAT = $scope.jsData.Codes["[TotalHarga]"];
            $scope.Head_Info_Kendaraan.VehiclePrice = $scope.jsData.Codes["[HargaMobil]"];
            $scope.Head_Info_Kendaraan.BookingFee = $scope.forAjuBookingUnit.BookingFee;

            $scope.Head_Info_Kendaraan.TradeInBit = $scope.mNegotiation_Pdd.TradeInBit;
            if (typeof $scope.SelectedTenor == "undefined") {
                $scope.SelectedTenor = {};
                $scope.SelectedTenor.LeasingTenorId = null;
                $scope.SelectedTenor.LeasingTenorId = false;
            } else {
                $scope.Head_Info_Kendaraan.LeasingLeasingTenorId = $scope.SelectedTenor.LeasingLeasingTenorId;
            }
            if (typeof $scope.mNegotiation_Pdd.OnOffTheRoad == "undefined") {
                $scope.Head_Info_Kendaraan.OffTheRoad = false;
            } else {
                $scope.Head_Info_Kendaraan.OffTheRoad = $scope.mNegotiation_Pdd.OnOffTheRoad;
            }
            if (typeof $scope.Head_Info_Kendaraan.TradeInBit == "undefined") {
                $scope.Head_Info_Kendaraan.TradeInBit = false;
            } else {
                $scope.Head_Info_Kendaraan.TradeInBit = $scope.mNegotiation_Pdd.TradeInBit;
            }


            $scope.Head_Info_Kendaraan.Interest = $scope.mNegotiation_Pdd.Bunga;
            $scope.Head_Info_Kendaraan.RequestDiscount = $scope.Head_Info_Kendaraan.ListDetailBookingUnit[0].RequestDiscount +
                maxDiscAcc($scope.tempListPaketAcc, $scope.tempListAcc) +
                $scope.mBookingUnit_AjuDiskon.DiscountSubDp +
                $scope.mBookingUnit_AjuDiskon.DiscountSubRate;

            $scope.addDisc = {
                RequestMaxDiscountAcc: maxDiscAcc($scope.tempListPaketAcc, $scope.tempListAcc),
                RequestTotalDiscount: $scope.Head_Info_Kendaraan.RequestDiscount
            };

            _.extend($scope.Head_Info_Kendaraan, $scope.addDisc);

            console.log("test", $scope.Head_Info_Kendaraan);
            for (var i in $scope.Head_Info_Kendaraan.ListDetailBookingUnit) {
                for (var j in $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnit) {
                    if ($scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnit[j].IsDiscount == true) {
                        $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnit[j].Discount = $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnit[j].Price;
                    }
                }
                for (var k in $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage) {
                    if ($scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage[k].IsDiscount == true) {
                        $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage[k].Discount = $scope.Head_Info_Kendaraan.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage[k].Price;
                    }
                }


            }
            ProspectFactory.simpanBookingUnit($scope.Head_Info_Kendaraan).then(
                function() {
                    //$scope.BookingUnitPdd = angular.merge({},$scope.BookingUnitPdd,$scope.mNegotiation_Pdd);
                    ProspectFactory.simpanBookingUnitPDD($scope.inputDataPDD).then(
                        function() {

                            $rootScope.$emit('RefreshDirective', {})
                            $scope.inputDataPDD = [];
                            $scope.tampungInputBooking = [];

                        },
                        function(err) {

                            bsNotify.show({
                                title: "Gagal",
                                content: "Input PDD Gagal",
                                type: 'danger'
                            });
                        }

                    )
                    bsNotify.show({
                        title: "Success",
                        content: "Input Booking Unit Berhasil",
                        type: 'success'
                    });
                },

                function(err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: "Input Booking Unit Gagal",
                        type: 'danger'
                    });
                }
            );


            $scope.listPlateNumber = [];
            $scope.tampungBookFee = [];
            $scope.listTampungBookFee = [];
            $scope.tampungBookFee.BookingFee = {};
            $scope.tampungBookFee.RequestDiscount = {};
            $scope.TotalBookFee = 0;
            $scope.TotalAjuDiskon = 0;
            $scope.disabledPlatNomor = false;
            $scope.TotalHargaKendaraan = 0;
            $scope.TotalListPaket = [];
            $scope.TotalListAcc = [];
            $scope.TotalListKaroseri = [];
            $scope.mNegotiation_Pdd = [];
            $scope.RincianHarga = [];
            $scope.inputAcc = [];
            $scope.inputAccPaket = [];
            $scope.inputKaroseri = [];
            $scope.mBookingUnit_AjuDiskon = {};
            $scope.tampungReqDiskon = 0;
            $scope.tampungReqBookFee = 0;
            $scope.jsData.HeaderInputs['$TotalKaroseri$'] = 0;

            if ($scope.statusAction == 'DariProspect') {
                $scope.CancelBookingFromProspect();
            } else {
                $scope.ShowListBookingUnit = true;
                $scope.Show_RincianHarga = false;
            }
            // if ($scope.countData > $scope.TotalQuantity) {

            //     $scope.ShowListBookingUnit = true;
            //     $scope.Show_RincianHarga = false;

            // } else {
            //     $scope.Show_Pembuatan_Booking_Unit = true;
            //     $scope.Show_RincianHarga = false;
            // }
        };

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = ('0' + date.getDate()).slice(-2) + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    date.getFullYear()
                return fix;
            } else {
                return null;
            }
        };

        $scope.fifthDateChange = function() {
            if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Pengiriman Unit paling cepat bisa dilakukan pada tanggal : " + fixDate($scope.bind_data.data[0]['SentUnitDate']),
                        type: 'warning'
                    }
                );
            }
            $scope.events.push($scope.testDate5);
            $scope.refreshCalendar = false;
            $timeout(function() { $scope.refreshCalendar = true }, 0);
        };

        $scope.sixDateChange = function () {
                    
            var date06 = $scope.bind_data.data[0]['ApprovalLeasingDate'];
            
            $scope.testDate6 = {
                date: date06,
                status: 'sixDate'
            };
           
            $scope.events.push($scope.testDate6);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.open5 = function() {
            $scope.popup5.opened = true;
        };

        $scope.BookingtoSpk = function() {
            $rootScope.$emit("buatSPKBookingUnit", {})
                ///$rootScope.BookingUnit = angular.copy($scope.getBookingUnit);
        }

        $scope.BuatSPK = function(SelectListVehicle) {

            ListBookingUnitFactory.bookUnitToReserved($scope.ListBookingUnit_Pengecekan_InfoKendaraan_To_Repeat).then(function(res) {
                    $scope.TipePlatNomorOptions = res.data.Result;

                    $scope.ShowListBookingUnitPengecekan = false;
                    //$scope.ShowListBookingUnit = true;

                },
                function(err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: "Book To Reserved Gagal",
                        type: 'danger'
                    });
                })
            CreateSpkFactory.getBookingUnitSpk(SelectListVehicle).then(function(res) {
                $scope.getBookingUnit = res.data.Result[0];

                $rootScope.BookingUnit = $scope.getBookingUnit;
                $scope.BookingtoSpk();


                $scope.formspkbookingunit = true;
                $scope.ShowListBookingUnitPengecekan = false;
            });

        }

        $scope.backdariSpk = function() {
            $scope.formspkbookingunit = false;
            $scope.ShowListBookingUnit = true;
            $rootScope.$emit("RefreshDirective", {});
        }

        $rootScope.$on("KembalidariSpkBook", function() {
            $scope.backdariSpk();
        })

        //ACCS PricingEngine

        //$scope.jsData = { Title: null, HeaderInputs: { $VehicleTypeColorId$:null, $InputTotalAcc$:null, $OutletId$:null, $VehicleTypeColorId$:null, $VehicleYear$:null }}
        $scope.GetDataFromFactory = function() {
            angular.forEach($scope.TotalListPaket, function(acc, accIdx) {
                var param = {
                    "Title": "Paket Aksesoris",
                    "HeaderInputs": {
                        "$AccPackageId$": acc.AccessoriesPackageId,
                        "$IsDiscount$": acc.IsDiscount == true ? 1 : 0,
                        "$QTY$": acc.Qty,
                        "$OutletId$": $scope.user.OutletId
                    }
                };


                $http.post('/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                    .then(function(res) {

                        $scope.TotalListPaket[accIdx].PPN = res.data.Codes["[PPN]"];
                        $scope.TotalListPaket[accIdx].TotalHargaAcc = res.data.Codes["[TotalHargaAcc]"];
                        $scope.TotalListPaket[accIdx].DPP = res.data.Codes["[DPP]"];

                        $scope.TampungTotalTotalPaket = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                        $scope.jsData.HeaderInputs['$TotalACC$'] = $scope.TampungTotalTotalSatuan + $scope.TampungTotalTotalPaket;

                        // $scope.jsData.HeaderInputs['$TotalACC$'] = parseInt($scope.jsData.HeaderInputs['$TotalACC$']) + parseInt(res.data.Codes["[TotalHargaAcc]"]);
                    });
            });

            angular.forEach($scope.TotalListAcc, function(acc, accIdx) {
                var param = {
                    "Title": "Aksesoris",
                    "HeaderInputs": {
                        "$AccessoriesId$": acc.AccessoriesId,
                        "$IsDiscount$": acc.IsDiscount == true ? 1 : 0,
                        "$QTY$": acc.Qty,
                        "$AccessoriesCode$": acc.AccessoriesCode,
                        "$OutletId$": $scope.user.OutletId,
                        "$VehicleTypeId$": $scope.PilihPdd.VehicleTypeId
                    }
                };


                $http.post('/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                    .then(function(res) {

                        $scope.TotalListAcc[accIdx].PPN = res.data.Codes["[PPN]"];
                        $scope.TotalListAcc[accIdx].TotalHargaAcc = res.data.Codes["[TotalHargaAcc]"];
                        $scope.TotalListAcc[accIdx].DPP = res.data.Codes["[DPP]"];
                        
                        $scope.TampungTotalTotalSatuan = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                        $scope.jsData.HeaderInputs['$TotalACC$'] = $scope.TampungTotalTotalSatuan + $scope.TampungTotalTotalPaket;
                        console.log('$scope.jsData2',$scope.jsData);
                        // $scope.jsData.HeaderInputs['$TotalACC$'] = parseInt($scope.jsData.HeaderInputs['$TotalACC$']) + parseInt(res.data.Codes["[TotalHargaAcc]"]);
                    });
            });

            angular.forEach($scope.TotalListKaroseri, function(acc, accIdx) {
                var param = {
                    "Title": "Karoseri",
                    "HeaderInputs": {
                        "$KaroseriId$": acc.KaroseriId,
                        "$QTY$": acc.Qty,
                        "$OutletId$": $scope.user.OutletId,
                        "$VehicleTypeId$": $scope.modalKaroseri.VehicleTypeId,
                        "$Discount$": 0,
                        
                        
                    }
                };


                $http.post('/api/fe/PricingEngine?PricingId=20004&OutletId=' + $scope.user.OutletId + '&DisplayId=1', param)
                    .then(function(res) {

                        $scope.TotalListKaroseri[accIdx].PPN = res.data.Codes["[PPN]"];
                        $scope.TotalListKaroseri[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.TotalListKaroseri[accIdx].TotalHargaKaroseri = res.data.Codes["[TotalHargaKar]"];
                        $scope.inputKaroseri[accIdx].KaroseriDPP = res.data.Codes["#Price#"];

                        $scope.jsData.HeaderInputs['$TotalKaroseri$'] = parseInt($scope.jsData.HeaderInputs['$TotalKaroseri$']) + parseInt(res.data.Codes["[TotalHargaKar]"]);
                    });
            });
        }

        ////////////////////////////////////////////////////////////
        $scope.vehicleselcted = [];
        $scope.VehicleChecked = function(data) {
            $scope.vehicleselcted.splice(0, 1);
            $scope.vehicleselcted.push(data);
        };

        $scope.KembaliPilihVehiclePDD = function() {
            angular.element('.ui.modal.selectvehicleprocessbooking').modal('hide');

        }

        $scope.PilihVehicleBuatPDD = function() {
            $scope.DateStart();
            $scope.KalkulasiTLSDate = {};
            $scope.KalkulasiTLSDate.FrameNumber = "";
            $scope.KalkulasiTLSDate.RRN = "";
            $scope.KalkulasiTLSDate.DTPLOD = "";
            $scope.KalkulasiTLSDate.PaketAksesorisTAM = "";
            $scope.KalkulasiTLSDate.DeliveryCategory = "";


            if ($scope.SelectDataRevisi.DirectDeliveryPDC == false || $scope.SelectDataRevisi.DirectDeliveryPDC == null ||$scope.SelectDataRevisi.DirectDeliveryPDC == undefined ) {
                $scope.KalkulasiTLSDate.DeliveryCategory = 1;
            } else {
                $scope.KalkulasiTLSDate.DeliveryCategory = 3;
            }


            if ($scope.vehicleselcted[0].FrameNo == null) {
                $scope.KalkulasiTLSDate.FrameNumber = "";
                $scope.KalkulasiTLSDate.RRN = $scope.vehicleselcted[0].RRNNo;
                $scope.KalkulasiTLSDate.DTPLOD = $scope.vehicleselcted[0].PLOD;
            } else {
                $scope.KalkulasiTLSDate.FrameNumber = $scope.vehicleselcted[0].FrameNo;
                $scope.KalkulasiTLSDate.RRN = "";
                $scope.KalkulasiTLSDate.DTPLOD = "";
            }
            $scope.SelectedKendaraan = {};
            $scope.SelectedKendaraan.PLOD = null;
            $scope.SelectedKendaraan.PLOD = $scope.vehicleselcted[0].PLOD;
            $scope.SelectedFormPDD = {};
            $scope.SelectedFormPDD.DirectDeliveryPDC = null;
            $scope.SelectedFormPDD.SentWithSTNK = null;
            $scope.SelectedFormPDD.SentWithSTNK = null;
            $scope.SelectedFormPDD.DirectDeliveryPDC = $scope.SelectDataRevisi.DirectDeliveryPDC;
            $scope.SelectedFormPDD.SentWithSTNK = $scope.SelectDataRevisi.SentWithSTNK;
            $scope.SelectedAjuDiskon = {};
            $scope.SelectedAjuDiskon.ChoosePoliceNo = null;
            $scope.SelectedAjuDiskon.ChoosePoliceNo = $scope.vehicleselcted[0].ChoosePoliceNo;
            $scope.PilihPdd = {};
            $scope.PilihPdd.RRNNo = null;
            $scope.PilihPdd.FrameNo = null;
            $scope.PilihPdd.FundSourceId = null;
            $scope.PilihPdd.RRNNo = $scope.vehicleselcted[0].RRNNo;
            $scope.PilihPdd.FrameNo = $scope.vehicleselcted[0].FrameNo;
            $scope.PilihPdd.FundSourceId = $scope.SelectDataRevisi.FundSourceId;
            console.log('$scope.SelectDataRevisi', $scope.SelectDataRevisi);
            $scope.mNegotiation_Pdd = {};
            $scope.mNegotiation_Pdd.OnOffTheRoad = $scope.SelectDataRevisi.OffTheRoad;
            // $scope.TLSDate = {};
            // $scope.TLSDate.earliestPDD = null;
            // $scope.TLSDate.earliestPDD = $scope.vehicleselcted[0].PLOD;

            // $scope.TLSDate.earliestPDD = $scope.TLSDate.earliestPDD.getFullYear() + '-'
            // 	+ ('0' + ($scope.TLSDate.earliestPDD.getMonth() + 1)).slice(-2) + '-'
            // 	+ ('0' + $scope.TLSDate.earliestPDD.getDate()).slice(-2) + 'T'
            // 	+ (($scope.TLSDate.earliestPDD.getHours() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getHours()) + ':'
            // 	+ (($scope.TLSDate.earliestPDD.getMinutes() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getMinutes()) + ':'
            // 	+ (($scope.TLSDate.earliestPDD.getSeconds() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getSeconds());

            // $scope.TLSDate.earliestPDD.slice(0, 10);
            // $scope.TLSDate.statusFormA = null;
            // $scope.TLSDate.statusFormA = false;



            MaintainPDDFactory.kalkulasiTLSDate($scope.KalkulasiTLSDate).then(
                function(res) {
                    $scope.TLSDate = res.data;

                    $scope.TLSDate.earliestPDD = $scope.TLSDate.earliestPDD.getFullYear() + '-' +
                        ('0' + ($scope.TLSDate.earliestPDD.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + $scope.TLSDate.earliestPDD.getDate()).slice(-2) + 'T' +
                        (($scope.TLSDate.earliestPDD.getHours() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getHours()) + ':' +
                        (($scope.TLSDate.earliestPDD.getMinutes() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getMinutes()) + ':' +
                        (($scope.TLSDate.earliestPDD.getSeconds() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getSeconds());

                    $scope.TLSDate.earliestPDD.slice(0, 10);


                    if ($scope.vehicleselcted[0].FrameNo == null && $scope.SelectDataRevisi.FundSourceName == 'Cash') {

                        $scope.ShowListBookingUnit = false;
                        angular.element('.ui.modal.selectvehicleprocessbooking').modal('hide');
                        $scope.MainRRNCash = true;
                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDateRRN = {
                                date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDateRRN2 = {
                                date: new Date($scope.vehicleselcted[0].DPDate),
                                status: 'secondDate'
                            };
                            $scope.testDateRRN3 = {
                                date: new Date($scope.vehicleselcted[0].FullPaymentDate),
                                status: 'thirdDate'
                            };
                        }
                    }
                    if ($scope.vehicleselcted[0].FrameNo == null && $scope.SelectDataRevisi.FundSourceName != 'Cash') {

                        $scope.ShowListBookingUnit = false;
                        angular.element('.ui.modal.selectvehicleprocessbooking').modal('hide');
                        $scope.MainRRNKredit = true;
                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDateRRN = {
                                date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDateRRN2 = {
                                date: new Date($scope.vehicleselcted[0].TDPDate),
                                status: 'secondDate'
                            };

                        }
                    }
                    if ($scope.vehicleselcted[0].FrameNo != null && $scope.SelectDataRevisi.FundSourceName == 'Cash') {

                        $scope.ShowListBookingUnit = false;
                        angular.element('.ui.modal.selectvehicleprocessbooking').modal('hide');
                        $scope.MainPDD = true;
                        if ($scope.flagPDD == 'Revisi') {
                            console.log('test 2');
                            $scope.testDate = {
                                date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDate2 = {
                                date: new Date($scope.vehicleselcted[0].DPDate),
                                status: 'secondDate'
                            };
                            $scope.testDate3 = {
                                date: new Date($scope.vehicleselcted[0].FullPaymentDate),
                                status: 'thirdDate'
                            };
                        }
                    }
                    if ($scope.vehicleselcted[0].FrameNo != null && $scope.SelectDataRevisi.FundSourceName != 'Cash') {

                        $scope.ShowListBookingUnit = false;
                        angular.element('.ui.modal.selectvehicleprocessbooking').modal('hide');
                        $scope.MainPDDKredit = true;
                        if ($scope.flagPDD == 'Revisi') {
                            console.log('test 2');
                            $scope.testDate = {
                                date: new Date($scope.vehicleselcted[0].DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDate2 = {
                                date: new Date($scope.vehicleselcted[0].TDPDate),
                                status: 'secondDate'
                            };

                        }
                    }
                },
                function(err) {

                    bsNotify.show({
                        title: "Gagal",
                        content: err.data.Message,
                        type: 'danger'
                    });
                }
            )




        }

        $scope.OK_Clicked = function() {
            angular.element('.ui.modal.ModalRevisionAlertPDD').modal('hide');
        }

        $scope.DisabledDataProspect = false;
        $rootScope.$on("buatBookingProspect", function() {
            $scope.statusAction = 'DariProspect';
            $scope.BookFromProspect = true;
            $scope.Show_Pembuatan_Booking_Unit = true;

            $scope.SelectedUnit = {};
            $scope.SelectedUnit = $rootScope.ProsBookingVehicle[0];

            $scope.urlPricing = '/api/fe/PricingEngine?PricingId=10003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';

            ListBookingUnitFactory.getProspectRevisi().then(
                function(res) {
                    $scope.getProspect = res.data.Result;
                    $scope.mBookingUnit = {};
                    $scope.mBookingUnit.ProspectId = null;
                    $scope.mBookingUnit.ProspectId = $rootScope.ProsBooking.ProspectId;

                    $scope.ProspectRevisi = $scope.getProspect.filter(function(type) {
                        return (type.ProspectId == $rootScope.ProsBooking.ProspectId);
                    })

                    $scope.mBookingUnit.HP = $scope.ProspectRevisi[0].HP;
                    $scope.mBookingUnit.Email = $scope.ProspectRevisi[0].Email;
                    $scope.mBookingUnit.FundSourceId = $scope.ProspectRevisi[0].FundSourceId;
                    $scope.mBookingUnit.FundSourceName = $scope.ProspectRevisi[0].FundSourceName;
                    $scope.mBookingUnit.TradeInBit = $scope.ProspectRevisi[0].TradeInBit;
                    $scope.mBookingUnit.ProspectDetailUnit = $rootScope.ProsBookingVehicle;

                    $scope.mBookingUnit.ProspectName = $rootScope.ProsBooking.ProspectName;
                    $scope.DisabledDataProspect = true;
                }
            );

        });

        $scope.CancelBookingFromProspect = function() {
            $rootScope.$emit("cancelBookingProspect", {})
            $scope.BookFromProspect = false;
            $scope.Show_Pembuatan_Booking_Unit = false;
            ///$rootScope.BookingUnit = angular.copy($scope.getBookingUnit);
        }

        $scope.selectKendaraan = function(selected) {
            $scope.PilihKendaraan = selected;
        }

    });