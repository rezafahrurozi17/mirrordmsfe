angular.module('app')
    .factory('TestDrivePersetujuanFactory', function($http,$httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
			getSearchByStatus: function() {
                var res = $http.get('/api/sales/PStatusTestDrive');
                return res;
            },

            getData: function(filter) {            
				try
				{
					filter.DateStart=filter.DateStart.getFullYear()+'-'
						+('0' + (filter.DateStart.getMonth()+1)).slice(-2)+'-'
						+('0' + filter.DateStart.getDate()).slice(-2)+'T'
						+((filter.DateStart.getHours()<'10'?'0':'')+ filter.DateStart.getHours())+':'
						+((filter.DateStart.getMinutes()<'10'?'0':'')+ filter.DateStart.getMinutes())+':'
						+((filter.DateStart.getSeconds()<'10'?'0':'')+ filter.DateStart.getSeconds());
				}
				catch(e1)
				{
					filter.DateStart=null;
				}							
                var param = $httpParamSerializer(filter);
				var res = "";
				
				
				if(param.indexOf('DateStart')==-1 && param.indexOf('StatusTestDriveId')!=-1)
				{
					res = $http.get('/api/sales/NegotiationApprovalTestDrive2/GetByStatusTestDriveId/?start=1&limit=1000&'+ param+'');
					
				}
				else
				{
					res = $http.get('/api/sales/NegotiationApprovalTestDrive2/?start=1&limit=1000&' + param +'');
				}
                
                return res;
            },

			GetSearchByCategory: function () {
				var da_json = [
				  { SearchOptionId: "Salesman", SearchOptionName: "Salesman Name" },
				  { SearchOptionId: "ProspectName", SearchOptionName: "Prospect Name" }
				];
				var res=da_json
				return res;
		  	},

			getDataProspect: function () {
				var res = $http.get('/api/sales/SDSGetProspectListDetail');
				return res;
			},

			GetSearchProspectTestDriveByName: function(SearchValue) {
				var res=$http.get('/api/sales/SDSGetProspectListDetail/?prospectSPKCode=&ProspectName='+SearchValue);
				return res;
			},
			  
			GetSearchProspectTestDriveByProspectCode: function(SearchValue) {
				var res=$http.get('/api/sales/SDSGetProspectListDetail/?prospectSPKCode='+SearchValue+'&ProspectName=');
				return res;
			},

			GetProspectSearchDropdownOptions: function () {
				var da_json = [
					{ SearchOptionId: "1", SearchOptionName: "Nama Prospect/Nama Pelanggan" },
					{ SearchOptionId: "2", SearchOptionName: "Prospecting ID" }
				];
				var res=da_json
				return res;
			},

			getDataVehicleTestDrive: function() {
				var res=$http.get('/api/sales/TestDriveUnitByOutlet');
				return res;
			},

			getTestDrive: function (filter){
				var param = $httpParamSerializer(filter);
				var res=$http.get('/api/sales/ListTestDriveByOutlet/?'+param);
				return res;
			},  
			  
            create: function(TestDrivePersetujuan) {
                return $http.post('/api/sales/NegotiationApprovalTestDrive2', [{
                    ProspectId: TestDrivePersetujuan.ProspectId,
					ProspectName: TestDrivePersetujuan.ProspectName,
					StatusTestDriveId: TestDrivePersetujuan.StatusTestDriveId,
					VehicleModelId: TestDrivePersetujuan.VehicleModelId,
                    LastStartDate: TestDrivePersetujuan.LastStartDate,
					RequestSalesId: TestDrivePersetujuan.RequestSalesId
                }]);
            },

            update: function(TestDrivePersetujuan) {
                return $http.put('/api/sales/NegotiationApprovalTestDrive2', [{
                    TestDriveId: TestDrivePersetujuan.TestDriveId,
					OutletId: TestDrivePersetujuan.OutletId,
					ProspectId: TestDrivePersetujuan.ProspectId,
					ProspectName: TestDrivePersetujuan.ProspectName,
					StatusTestDriveId: TestDrivePersetujuan.StatusTestDriveId,
					VehicleModelId: TestDrivePersetujuan.VehicleModelId,
                    LastStartDate: TestDrivePersetujuan.LastStartDate,
					RequestSalesId: TestDrivePersetujuan.RequestSalesId
                }]);
            },
			
			SetujuTestDrivePersetujuan: function(TestDrivePersetujuan){
				TestDrivePersetujuan.DateStart=TestDrivePersetujuan.DateStart.getFullYear()+'-'
					+('0' + (TestDrivePersetujuan.DateStart.getMonth()+1)).slice(-2)+'-'
					+('0' + TestDrivePersetujuan.DateStart.getDate()).slice(-2)+'T'
					+((TestDrivePersetujuan.DateStart.getHours()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getHours())+':'
					+((TestDrivePersetujuan.DateStart.getMinutes()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getMinutes())+':'
					+((TestDrivePersetujuan.DateStart.getSeconds()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getSeconds());
				
				TestDrivePersetujuan.DateEnd=TestDrivePersetujuan.DateEnd.getFullYear()+'-'
					+('0' + (TestDrivePersetujuan.DateEnd.getMonth()+1)).slice(-2)+'-'
					+('0' + TestDrivePersetujuan.DateEnd.getDate()).slice(-2)+'T'
					+((TestDrivePersetujuan.DateEnd.getHours()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getHours())+':'
					+((TestDrivePersetujuan.DateEnd.getMinutes()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getMinutes())+':'
					+((TestDrivePersetujuan.DateEnd.getSeconds()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getSeconds());
				return $http.put('/api/sales/NegotiationApprovalTestDrive2/Approve', [TestDrivePersetujuan]);									
			},
			
			SetujuTestDrivePersetujuanSuggestion: function(TestDrivePersetujuan){
				TestDrivePersetujuan.DateStart=TestDrivePersetujuan.DateStart.getFullYear()+'-'
					+('0' + (TestDrivePersetujuan.DateStart.getMonth()+1)).slice(-2)+'-'
					+('0' + TestDrivePersetujuan.DateStart.getDate()).slice(-2)+'T'
					+((TestDrivePersetujuan.DateStart.getHours()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getHours())+':'
					+((TestDrivePersetujuan.DateStart.getMinutes()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getMinutes())+':'
					+((TestDrivePersetujuan.DateStart.getSeconds()<'10'?'0':'')+ TestDrivePersetujuan.DateStart.getSeconds());
				
				TestDrivePersetujuan.DateEnd=TestDrivePersetujuan.DateEnd.getFullYear()+'-'
					+('0' + (TestDrivePersetujuan.DateEnd.getMonth()+1)).slice(-2)+'-'
					+('0' + TestDrivePersetujuan.DateEnd.getDate()).slice(-2)+'T'
					+((TestDrivePersetujuan.DateEnd.getHours()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getHours())+':'
					+((TestDrivePersetujuan.DateEnd.getMinutes()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getMinutes())+':'
					+((TestDrivePersetujuan.DateEnd.getSeconds()<'10'?'0':'')+ TestDrivePersetujuan.DateEnd.getSeconds());
				return $http.put('/api/sales/NegotiationApprovalTestDrive2/Suggest', [TestDrivePersetujuan]);									
			},
			
            delete: function(id) {
                return $http.delete('/api/sales/NegotiationApprovalTestDrive2', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });