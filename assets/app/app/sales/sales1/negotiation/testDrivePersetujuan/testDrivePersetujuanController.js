angular.module('app')
    .controller('TestDrivePersetujuanController', function($scope, $http,$filter, $httpParamSerializer, CurrentUser, TestDrivePersetujuanFactory, $timeout, bsNotify, MobileHandling) {

        //----------------------------------
        // Start-Up
		//----------------------------------
		
		

		var chart = { dataProvider: null };
		$scope.ShowTestDrivePersetujuanMain=true;
		$scope.ShowAddTestDrivePersetujuanBtn=true;
		$scope.ShowTestDrivePersetujuanDetail=false;
		$scope.TestDrivePersetujuanLockData=true;
		$scope.intended_operation_TestDrivePersetujuan="";
		$scope.optionProspecTestDriveSearchCriteria = TestDrivePersetujuanFactory.GetProspectSearchDropdownOptions();
		$scope.filter={DateStart:null, StatusTestDriveId:null};
		$scope.filter.DateStart=new Date();
		$scope.$on('$destroy', function() {
            angular.element('.ui.modal.ModalProspectTestDrive').remove();

        });
		
		$scope.$on('$viewContentLoaded', function() {
			$scope.loading = false;
			$scope.gridData = [];
			//----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });
		
		$scope.TestDrivePersetujuanDateSearch = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
        }
		
		$scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
        }
		
		$scope.OptionSearchByCategory=TestDrivePersetujuanFactory.GetSearchByCategory();

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mtestDrivePersetujuan = null; //Model
		var chart = { dataProvider: null };
		var dataProviders = [];
		var da_guides=[];
		var dateGantt = null;
		//console.log("setan",$scope.user);

		TestDrivePersetujuanFactory.getSearchByStatus().then(function(res){
            $scope.StatusOption = res.data.Result;
			$scope.StatusOption.push({'LastModifiedDate':null,'LastModifiedUserId':null,'StatusCode':1,'StatusTestDriveId':null,'StatusTestDriveName':"All",'TotalData':res.data.Result.length});
            return $scope.StatusOption;
        }).then(function(res){
			var dfotall=$scope.StatusOption.length;
			$scope.filter.StatusTestDriveId=1;
			
			TestDrivePersetujuanFactory.getData($scope.filter)
            .then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
            );
		});

		
		$scope.detailVehicleselect = function (selected) {
			$scope.detailVehicle = selected;
		}
		
		TestDrivePersetujuanFactory.getDataVehicleTestDrive().then(function (res) {
			$scope.VehicleTestDrive = res.data.Result;
		})
		
		$scope.refreshjadwal = function (){
			if($scope.intended_operation_TestDrivePersetujuan == "Update_Ops"){
				for (var i in $scope.outletdetail){
					if ($scope.outletdetail[i].OutletId == $scope.mtestDrivePersetujuan.OutletId){
						$scope.outletdetailselect = $scope.outletdetail[i];
					}
				}
				$scope.dataJam($scope.outletdetailselect);
			}
		}
		
		$scope.listSchudleTest = function (dt) {


				$scope.mtestDrivePersetujuan.DateTestDrive = new Date(dt);
				$scope.mtestDrivePersetujuan.OutletId = null;
				$scope.isChart = false; $scope.isList = true;

			$scope.mtestDrivePersetujuanToPass={DateTestDrive:null,DateEnd:null,OutletId:null,VehicleTypeId:null};
			$scope.mtestDrivePersetujuanToPass.DateTestDrive=angular.copy($scope.mtestDrivePersetujuan.DateTestDrive);
			$scope.mtestDrivePersetujuanToPass.DateEnd=null;
			$scope.mtestDrivePersetujuanToPass.OutletId=null;
			$scope.mtestDrivePersetujuanToPass.VehicleTypeId=angular.copy($scope.mtestDrivePersetujuan.VehicleTypeId);
			
			TestDrivePersetujuanFactory.getTestDrive($scope.mtestDrivePersetujuanToPass).then(function (res) {
				$scope.outletdetail = res.data.Result;
				
				if ($scope.outletdetail.length != 0){
						$scope.isList = true;
				}
				
				$scope.mtestDrivePersetujuan.DateStart.setDate($scope.mtestDrivePersetujuan.DateTestDrive.getDate());
				$scope.mtestDrivePersetujuan.DateStart.setMonth($scope.mtestDrivePersetujuan.DateTestDrive.getMonth());
				$scope.mtestDrivePersetujuan.DateStart.setYear($scope.mtestDrivePersetujuan.DateTestDrive.getFullYear());
				
				$scope.mtestDrivePersetujuan.DateEnd.setDate($scope.mtestDrivePersetujuan.DateTestDrive.getDate());
				$scope.mtestDrivePersetujuan.DateEnd.setMonth($scope.mtestDrivePersetujuan.DateTestDrive.getMonth());
				$scope.mtestDrivePersetujuan.DateEnd.setYear($scope.mtestDrivePersetujuan.DateTestDrive.getFullYear());
				
				$scope.mtestDrivePersetujuan.DateStartBaru.setDate($scope.mtestDrivePersetujuan.DateTestDrive.getDate());
				$scope.mtestDrivePersetujuan.DateStartBaru.setMonth($scope.mtestDrivePersetujuan.DateTestDrive.getMonth());
				$scope.mtestDrivePersetujuan.DateStartBaru.setYear($scope.mtestDrivePersetujuan.DateTestDrive.getFullYear());
				
				$scope.mtestDrivePersetujuan.DateEndBaru.setDate($scope.mtestDrivePersetujuan.DateTestDrive.getDate());
				$scope.mtestDrivePersetujuan.DateEndBaru.setMonth($scope.mtestDrivePersetujuan.DateTestDrive.getMonth());
				$scope.mtestDrivePersetujuan.DateEndBaru.setYear($scope.mtestDrivePersetujuan.DateTestDrive.getFullYear());
				
				if($scope.outletdetail.length==0)
				{
					document.getElementById("chartdivTestDrivePersetujuan").style.visibility= "hidden";
				}
				else if($scope.outletdetail.length>0)
				{
					document.getElementById("chartdivTestDrivePersetujuan").style.visibility= "visible";
				}
				
				if($scope.intended_operation_TestDrivePersetujuan=="Update_Ops")
				{
					$scope.mtestDrivePersetujuan.OutletId=angular.copy($scope.mtestDrivePersetujuan_OriginalOutletId);
					$scope.mtestDrivePersetujuan.TestDriveUnitId=angular.copy($scope.$scope.mtestDrivePersetujuan.TestDriveUnitId);
				}
			})
		}

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            TestDrivePersetujuanFactory.getData($scope.filter)
            .then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
            );
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            //console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            //console.log("onSelectRows=>", rows);
        }
		
		$scope.TutupModal = function() {
            $scope.ModalTestDrivePersetujuanProspect={show:false};
			$scope.Detail_NamaProspect="";
			$scope.Detail_NoProspect="";
			$scope.Detail_Alamat="";
			$scope.Detail_Phone="";
			$scope.Detail_Email="";
			$scope.Detail_Mobile="";
			$scope.Detail_Notes="";
        }
		
		$scope.ShowProspect = function(selected_data) {
			$scope.Detail_NamaProspect=selected_data.ProspectName;
			$scope.Detail_NoProspect=selected_data.ProspectCode;
			$scope.Detail_Alamat=selected_data.Address;
			$scope.Detail_Phone=selected_data.PhoneNumber;
			$scope.Detail_Email=selected_data.Email;
			$scope.Detail_Mobile=selected_data.HP;
			$scope.Detail_Notes=selected_data.RequestNote;
			$scope.ModalTestDrivePersetujuanProspect={show:true};
        }
		
		$scope.addNewTestDrivePersetujuan = function(){
			$scope.mtestDrivePersetujuan={};
			$scope.DetailTestDrivePersetujuanSuggestionContent=true;
			$scope.DetailTestDrivePersetujuanSuggestion=true;
			$scope.ShowAddTestDrivePersetujuanBtn=false;
			$scope.ShowTestDrivePersetujuanMain=false;
			$scope.ShowTestDrivePersetujuanDetail=true;
			$scope.intended_operation_TestDrivePersetujuan="Insert_Ops";
			$scope.DetailTestDrivePersetujuanSuggestionContent=false;
			$scope.TestDrivePersetujuanLockData=false;
		}
		
		$scope.ShowHideDetailTestDrivePersetujuanSuggestionContent = function(){
			$scope.DetailTestDrivePersetujuanSuggestionContent=!$scope.DetailTestDrivePersetujuanSuggestionContent;
		}
		
		$scope.EditTestDrivePersetujuanDetail = function(selected_testdrivepersetujuan){
			$scope.TestDrivePersetujuanMatiinKaloAbisSuggest=false;
			$scope.SalesOption=[];
			var tempSales = [{
				EmployeeName : selected_testdrivepersetujuan.Salesman,
				EmployeeId : selected_testdrivepersetujuan.RequestSalesId
			}];
			$scope.SalesOption=angular.copy(tempSales);
			
			$scope.mtestDrivePersetujuan=angular.copy(selected_testdrivepersetujuan);
			
			//kalo approve bikin false bikin if disini
	
			//ini kalo bukan approve
			$scope.DetailTestDrivePersetujuanSuggestionContent=true;
			$scope.DetailTestDrivePersetujuanSuggestion=true;
			
			$scope.ShowAddTestDrivePersetujuanBtn=false;
			$scope.ShowTestDrivePersetujuanMain=false;
			$scope.ShowTestDrivePersetujuanDetail=true;
			$scope.TestDrivePersetujuanLockData=true;
			$scope.intended_operation_TestDrivePersetujuan="Update_Ops";
			$scope.DetailTestDrivePersetujuanSuggestionContent=false;
			
			

			$scope.ShowTestDrivePersetujuanIncrementJamMulai=true;
			$scope.ShowTestDrivePersetujuanDecrementJamMulai=true;
			$scope.ShowTestDrivePersetujuanIncrementJamAkhir=true;
			$scope.ShowTestDrivePersetujuanDecrementJamAkhir=true;
			
			
			$scope.mtestDrivePersetujuan.DateTestDrive=angular.copy(selected_testdrivepersetujuan.DateStart);
			$scope.listSchudleTest($scope.mtestDrivePersetujuan.DateTestDrive);
			
			$scope.mtestDrivePersetujuan.DateStartBaru=angular.copy(selected_testdrivepersetujuan.DateStart);
			$scope.mtestDrivePersetujuan.DateEndBaru=angular.copy(selected_testdrivepersetujuan.DateEnd);
			
			$scope.mtestDrivePersetujuan_OriginalOutletId=angular.copy(selected_testdrivepersetujuan.OutletId);

		}
		
		$scope.SimpanTestDrivePersetujuan = function(){
			
			$scope.mtestDrivePersetujuan.DateStart=angular.copy($scope.mtestDrivePersetujuan.DateStartBaru);
			$scope.mtestDrivePersetujuan.DateEnd=angular.copy($scope.mtestDrivePersetujuan.DateEndBaru);
			
			if($scope.mtestDrivePersetujuan.DateStart<$scope.mtestDrivePersetujuan.DateEnd)
			{
				
				if($scope.mtestDrivePersetujuan.StatusTestDriveName!="Selesai")
				{
					TestDrivePersetujuanFactory.SetujuTestDrivePersetujuan($scope.mtestDrivePersetujuan).then(function () {
						TestDrivePersetujuanFactory.getData($scope.filter).then(function(res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								ShowAddTestDrivePersetujuanBtn = true;
								ShowTestDrivePersetujuanDetail = false;
								$scope.BatalTestDrivePersetujuan();
								
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
							},
							function(err) {
								bsNotify.show(
									{
										title: "gagal",
										content: "Data gagal disimpan.",
										type: 'danger'
									}
								);
						   }
						);
					});
				}
				else if($scope.mtestDrivePersetujuan.StatusTestDriveName=="Selesai")
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data tidak dapat diubah karena test drive sudah selesai.",
							type: 'warning'
						}
					);
				}
			}
			else
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Jam mulai dan/atau selesai anda salah.",
							type: 'warning'
						}
					);
			}
			

			
		}
		
		$scope.SimpanTestDrivePersetujuanSuggestion = function(){
			
			$scope.mtestDrivePersetujuan.DateStart=angular.copy($scope.mtestDrivePersetujuan.DateStartBaru);
			$scope.mtestDrivePersetujuan.DateEnd=angular.copy($scope.mtestDrivePersetujuan.DateEndBaru);
			
			if($scope.mtestDrivePersetujuan.DateStart<$scope.mtestDrivePersetujuan.DateEnd)
			{
				
				if($scope.mtestDrivePersetujuan.StatusTestDriveName!="Selesai")
				{
					TestDrivePersetujuanFactory.SetujuTestDrivePersetujuanSuggestion($scope.mtestDrivePersetujuan).then(function () {
						TestDrivePersetujuanFactory.getData($scope.filter).then(function(res) {
								gridData = [];
								$scope.grid.data = res.data.Result;
								$scope.loading = false;
								//ShowAddTestDrivePersetujuanBtn = true;
								//ShowTestDrivePersetujuanDetail = false;
								
								$scope.mtestDrivePersetujuanToPass={DateTestDrive:null,DateEnd:null,OutletId:null,VehicleTypeId:null};
								$scope.mtestDrivePersetujuanToPass.DateTestDrive=angular.copy($scope.mtestDrivePersetujuan.DateTestDrive);
								$scope.mtestDrivePersetujuanToPass.DateEnd=null;
								$scope.mtestDrivePersetujuanToPass.OutletId=null;
								$scope.mtestDrivePersetujuanToPass.VehicleTypeId=angular.copy($scope.mtestDrivePersetujuan.VehicleTypeId);
								
								TestDrivePersetujuanFactory.getTestDrive($scope.mtestDrivePersetujuanToPass).then(function (res) {
										$scope.outletdetail = res.data.Result;
										$scope.refreshjadwal();
										$scope.TestDrivePersetujuanMatiinKaloAbisSuggest=true;
									}
								);
								
								//$scope.BatalTestDrivePersetujuan();
								
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
							},
							function(err) {
								bsNotify.show(
									{
										title: "gagal",
										content: "Data gagal disimpan.",
										type: 'danger'
									}
								);
						   }
						);
					});
				}
				else if($scope.mtestDrivePersetujuan.StatusTestDriveName=="Selesai")
				{
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Data tidak dapat diubah karena test drive sudah selesai.",
							type: 'warning'
						}
					);
				}
			}
			else
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Jam mulai dan/atau selesai anda salah.",
							type: 'warning'
						}
					);
			}
			

			
		}
		
		$scope.BatalTestDrivePersetujuan = function(){
			$scope.ShowAddTestDrivePersetujuanBtn=true;
			$scope.ShowTestDrivePersetujuanMain=true;
			$scope.ShowTestDrivePersetujuanDetail=false;
			$scope.intended_operation_TestDrivePersetujuan="";
			$scope.mtestDrivePersetujuan={};
		}
		
		$scope.pilihProspectTestDrive = function (ProspectNo) {
			$scope.mtestDrivePersetujuan.ProspectName=angular.copy(ProspectNo.ProspectName);
            angular.element('.ui.modal.ModalProspectTestDrive').modal('hide');
        }
		
		$scope.BatalLookupProspectTestDrive = function () {
			angular.element('.ui.modal.ModalProspectTestDrive').modal('hide');
        }
		
		$scope.LookupProspectTestDrive = function () {
			TestDrivePersetujuanFactory.getDataProspect().then(function (res) {
				$scope.gridNoProspectTestDrive.data = res.data.Result;
				
			});
			angular.element('.ui.modal.ModalProspectTestDrive').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalProspectTestDrive').not(':first').remove();
        }
		
		$scope.SearchProspectTestDrive = function () {
			if($scope.ProspectSearchCriteria==1 && $scope.ProspectSearchValue!="")
			{
				TestDrivePersetujuanFactory.GetSearchProspectTestDriveByName($scope.ProspectSearchValue).then(function (res) {
				$scope.gridNoProspectTestDrive.data = res.data.Result;
				
				});
			}
			else if($scope.ProspectSearchCriteria==2 && $scope.ProspectSearchValue!="")
			{
				TestDrivePersetujuanFactory.GetSearchProspectTestDriveByProspectCode($scope.ProspectSearchValue).then(function (res) {
				$scope.gridNoProspectTestDrive.data = res.data.Result;
				
				});
			}
			else
			{
				TestDrivePersetujuanFactory.getDataProspect().then(function (res) {
					$scope.gridNoProspectTestDrive.data = res.data.Result;
				});
			}
        }
		
		$scope.TestDrivePersetujuanIncrementStartDateTime = function() {
			if($scope.mtestDrivePersetujuan.DateStartBaru.getHours()<22)
			{
				$scope.mtestDrivePersetujuan.DateStartBaru.setHours($scope.mtestDrivePersetujuan.DateStartBaru.getHours()+1);
				$scope.mtestDrivePersetujuan.DateStartBaru=angular.copy($scope.mtestDrivePersetujuan.DateStartBaru);
				$scope.ShowTestDrivePersetujuanDecrementJamMulai= true;
			}
			else if($scope.mtestDrivePersetujuan.DateStartBaru.getHours()>=22)
			{
				$scope.ShowTestDrivePersetujuanIncrementJamMulai= false;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda melebihi batas waktu yang ditentukan.",
						type: 'warning'
					}
				);
			}
			
		}

		$scope.TestDrivePersetujuanDecrementStartDateTime = function() {
			if($scope.mtestDrivePersetujuan.DateStartBaru.getHours()>8)
			{
				$scope.mtestDrivePersetujuan.DateStartBaru.setHours($scope.mtestDrivePersetujuan.DateStartBaru.getHours()-1);
				$scope.mtestDrivePersetujuan.DateStartBaru=angular.copy($scope.mtestDrivePersetujuan.DateStartBaru);
				$scope.ShowTestDrivePersetujuanIncrementJamMulai= true;
			}
			else if($scope.mtestDrivePersetujuan.DateStartBaru.getHours()<=8)
			{
				$scope.ShowTestDrivePersetujuanDecrementJamMulai= false;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda melebihi batas waktu yang ditentukan.",
						type: 'warning'
					}
				);
			}
			
		}
		
		$scope.TestDrivePersetujuanIncrementEndDateTime = function() {
			if($scope.mtestDrivePersetujuan.DateEndBaru.getHours()<22)
			{
				$scope.mtestDrivePersetujuan.DateEndBaru.setHours($scope.mtestDrivePersetujuan.DateEndBaru.getHours()+1);
				$scope.mtestDrivePersetujuan.DateEndBaru=angular.copy($scope.mtestDrivePersetujuan.DateEndBaru);
				$scope.ShowTestDrivePersetujuanDecrementJamAkhir= true;
			}
			else if($scope.mtestDrivePersetujuan.DateEndBaru.getHours()>=22)
			{
				$scope.ShowTestDrivePersetujuanIncrementJamAkhir= false;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda melebihi batas waktu yang ditentukan.",
						type: 'warning'
					}
				);
			}
			
		}

		$scope.TestDrivePersetujuanDecrementEndDateTime = function() {
			if($scope.mtestDrivePersetujuan.DateEndBaru.getHours()>8)
			{
				$scope.mtestDrivePersetujuan.DateEndBaru.setHours($scope.mtestDrivePersetujuan.DateEndBaru.getHours()-1);
				$scope.mtestDrivePersetujuan.DateEndBaru=angular.copy($scope.mtestDrivePersetujuan.DateEndBaru);
				$scope.ShowTestDrivePersetujuanIncrementJamAkhir= true;
			}
			else if($scope.mtestDrivePersetujuan.DateEndBaru.getHours()<=8)
			{
				$scope.ShowTestDrivePersetujuanDecrementJamAkhir= false;
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Anda melebihi batas waktu yang ditentukan.",
						type: 'warning'
					}
				);
			}
		}
		
		$scope.dataJam = function (outlet) {
			$scope.isChart = true;
			$scope.detailVehicle = {VehicleTypeId:null, Description:null};

			for (var i in $scope.VehicleTestDrive){
				if ($scope.VehicleTestDrive[i].VehicleTypeId == $scope.mtestDrivePersetujuan.VehicleTypeId){
					$scope.detailVehicle.VehicleTypeId = $scope.VehicleTestDrive[i].VehicleTypeId;
					$scope.detailVehicle.Description = $scope.VehicleTestDrive[i].Description;
				}

			}
			
	
			var year = new Date($scope.mtestDrivePersetujuan.DateTestDrive).getFullYear();
			var mounth = ('0' + (new Date($scope.mtestDrivePersetujuan.DateTestDrive).getMonth() + 1)).slice(-2);
			var date = ('0' + new Date($scope.mtestDrivePersetujuan.DateTestDrive).getDate()).slice(-2);
			var hour = (new Date().getHours() < '10' ? '0' : '') + new Date().getHours();

			
			var hourstart = (new Date($scope.mtestDrivePersetujuan.DateStart).getHours() < '10' ? '0' : '') + new Date().getHours();
			var hoursend = (new Date($scope.mtestDrivePersetujuan.DateEnd).getHours() < '10' ? '0' : '') + new Date().getHours();

			if($scope.intended_operation_TestDrivePersetujuan == "Insert_Ops"){
				$scope.mtestDrivePersetujuan.DateStart = angular.copy(new Date($scope.mtestDrivePersetujuan.DateTestDrive).setHours(hour));
				$scope.mtestDrivePersetujuan.DateEnd = angular.copy(new Date($scope.mtestDrivePersetujuan.DateTestDrive).setHours(hour));
			} else if ($scope.intended_operation_TestDrivePersetujuan == "Update_Ops"){

				$scope.mtestDrivePersetujuan.DateStart  = new Date(new Date($scope.mtestDrivePersetujuan.DateStart).getFullYear(),
															new Date($scope.mtestDrivePersetujuan.DateStart).getMonth(),
															new Date($scope.mtestDrivePersetujuan.DateStart).getDate(),
															new Date($scope.mtestDrivePersetujuan.DateStart).getHours(),
															new Date($scope.mtestDrivePersetujuan.DateStart).getMinutes());
				
															
				$scope.mtestDrivePersetujuan.DateEnd  = new Date(new Date($scope.mtestDrivePersetujuan.DateEnd).getFullYear(),
															new Date($scope.mtestDrivePersetujuan.DateEnd).getMonth(),
															new Date($scope.mtestDrivePersetujuan.DateEnd).getDate(),
															new Date($scope.mtestDrivePersetujuan.DateEnd).getHours(),
															new Date($scope.mtestDrivePersetujuan.DateEnd).getMinutes());

			}

			dateGantt = year+'-'+mounth+'-'+date;
			
			dataProviders = outlet.ListUnitTestDrive;
			$scope.outdataProviders = dataProviders;

			
			$scope.platNo = [{ TestDriveUnitId: null, PoliceNumber: null }];
			$scope.platNo.splice(0, 1);
			for (var i in dataProviders) {
				$scope.platNo.push({
					TestDriveUnitId: dataProviders[0].TestDriveUnitId,
					PoliceNumber: dataProviders[0].PoliceNumber
				});
			}

			
			da_guides = [{ "value": null,
								"lineColor": null,
								"lineThickness": null,
								"dashLength": null,
								"inside": null,
								"labelRotation": null,
								"label": null }];
			da_guides.splice(0, 1);

			if(dataProviders[0].ListJadwalTestDrive.length<=0)
			{
				console.log("ulala");
				dataProviders[0].ListJadwalTestDrive[0]=[{ ColorId: null,
				DateEnd: null,
				DateStart: null,
				OutletId: null,
				PoliceNumber: null,
				ScheduleId: null,
				StatusFollowUpTestDriveId: null,
				StatusTestDriveId: null,
				TestDriveUnitId: null,
				TimeDuration: null,
				TimeEnd: null,
				TimeStart: null,
				VehicleMappingId: null,
				VehicleModelId: null,
				VehicleTypeId: null,
				color:null}];

				
			}
			
			for(var i = 0; i < dataProviders[0].ListJadwalTestDrive.length; ++i)
			{
				try
				{
					var raw_garis_atas=dataProviders[0].ListJadwalTestDrive[i].DateEnd;
					var tanggal_garis_atas=raw_garis_atas.getFullYear()+'-'+('0' + (raw_garis_atas.getMonth()+1)).slice(-2)+'-'+('0' + raw_garis_atas.getDate()).slice(-2);
					var jam_garis_atas=$filter('date')(raw_garis_atas, 'HH:mm:ss');
					da_guides.push({
									value: AmCharts.stringToDate( tanggal_garis_atas+" "+jam_garis_atas, "YYYY-MM-DD HH:NN"),
									lineColor: "#000000",
									lineThickness: 4,
									dashLength: 2,
									inside: true,
									labelRotation: 90,
									label: ""+jam_garis_atas
					});
					if(i==0)
					{
						dataProviders[0].ListJadwalTestDrive[i]['color']="#0000FF";
					}
					else if(i==1)
					{
						dataProviders[0].ListJadwalTestDrive[i]['color']="#23be8e";
					}
					else if(i%3==0)
					{
						dataProviders[0].ListJadwalTestDrive[i]['color']="#3232FF";
					}
					else if(i%2==0)
					{
						dataProviders[0].ListJadwalTestDrive[i]['color']="#0096FF";
					}
					else
					{
						dataProviders[0].ListJadwalTestDrive[i]['color']="#42ff33";
					}
				}
				catch(ulala)
				{
				}
				
			}
			

			
			chart = AmCharts.makeChart('chartdivTestDrivePersetujuan', {
				"type": "gantt",
				"theme": "light",
				"zoomOutText": "",
				"marginRight": 0,
				"marginLeft": 0,
				"period": "hh",
				"dataDateFormat": "YYYY-MM-DD",
				"showBalloon": false,
				"balloonDateFormat": "JJ:NN",
				"columnWidth": 1,
				"valueAxis": {
					"type": "number",
					"includeHidden": true,
					"maximum": 22.5,
					"minimum": 8,
					"reversed":true,
					"showBalloon": false,
					"labelFunction": function(data) {return ''+data+":00";},
					"guides": da_guides
				},
				"categoryAxis": {
					"position": "left"
				},
				"brightnessStep": 10,
				"graph": {
					"fillAlphas": 1,
					//"balloonText": "<b>[[task]]</b>: [[start]] [[value]]",
					"labelPosition": "top",
					"showBalloon": false

				},
				"rotate": false,
				"categoryField": "PoliceNumber",
				"segmentsField": "ListJadwalTestDrive",
				"colorField": "color",
				"startDate": dateGantt,
				"startField": "TimeStart",
				"endField": "TimeEnd",
				"durationField": "duration",
				"dataProvider": dataProviders,
				"valueScrollbar": {
					"autoGridCount": true,
					"enabled": false
				},
				"chartCursor": {
					"cursorColor": "#55bb76",
					"valueBalloonsEnabled": true,
					"cursorAlpha": 0,
					"valueLineAlpha": 0.5,
					"valueLineBalloonEnabled": true,
					"valueLineEnabled": true,
					"zoomable": false,
					"valueZoomable": true
				},
				"export": {
					"enabled": false
				}
			});

		}
		
		$scope.platnoAmbil = function(selected) {

            var tempunit = $scope.outdataProviders.filter(function(type) {
                return (type.TestDriveUnitId == $scope.mtestDrivePersetujuan.TestDriveUnitId);
            })

			$scope.mtestDrivePersetujuan.OutletId = tempunit[0].OutletOwnerId;
            $scope.hour = [];
            for (var i in tempunit[0].ListJadwalTestDrive) {
                $scope.hour.push({ start: new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(), end: new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours() })

            }

        }
		
		$scope.platnoAmbilChange = function() {
				var tempunit = $scope.outdataProviders.filter(function(type) {
                    return (type.TestDriveUnitId == $scope.mtestDrivePersetujuan.TestDriveUnitId);
                })

                $scope.hour = [];
                for (var i in tempunit[0].ListJadwalTestDrive) {
                    $scope.hour.push({ start: new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(), end: new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours() })
                }

        }

		
		
		var btnActionPilihProspect = '<a style="color:blue;"  ng-click=""> \
			<p style="padding:5px 0 0 5px" ng-click="grid.appScope.pilihProspectTestDrive(row.entity)"> \
            	<u>Pilih</u> \
            </p> \
			</a>';
		
        //----------------------------------
        // Grid Setup
        //----------------------------------
		//tadi di kolom bawah ada { name: 'Outlet', field: 'OutletName'},
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnMenus: false,
			gridMenuShowHideColumns:false,
			enableGridMenu:false,
			enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'TestDriveId', field: 'TestDriveId', visible: false },
                { name: 'Nama Prospek/Cabang yang meminta', field: 'ProspectName' , cellTemplate:'<div style="position: relative;top: 20%;"><a style="margin-left:5px;" ng-show="row.entity.OutletIdRequester==grid.appScope.$parent.user.OutletId" ng-click="grid.appScope.$parent.ShowProspect(row.entity)" >{{row.entity.ProspectName}}</a><label style="margin-left:5px;" ng-show="row.entity.OutletIdRequester!=grid.appScope.$parent.user.OutletId">{{row.entity.OutletRequesterName}}</label></div>'},
                { name: 'Model Kendaraan', field: 'VehicleTypeDescription'},
                { name: 'No Pol', field: 'PoliceNumber'},
				{ name: 'Tanggal Test Drive', field: 'DateStart', cellFilter: 'date:\'dd-MM-yyyy\'',enableFiltering: false},
				{ name: 'Salesman', field: 'Salesman'},
				{ name: 'Status', field: 'StatusTestDriveName'},
				{ 
					name: 'Action',
					enableFiltering: false,
					visible:true,
					width: '7%', 
					pinnedRight: true,
					enableColumnResizing: true,
					cellTemplate:'<div style="position: relative;top: 20%;"><i title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.EditTestDrivePersetujuanDetail(row.entity)" ></i></div>' 
				},
            ]
        };
		
		$scope.gridNoProspectTestDrive = {
            enableSorting: true,
            enableFiltering: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
			//ada kemungkinan ganti prospect code
            columnDefs: [
                { name: 'Prospecting ID', field: 'ProspectCode' },
                { name: 'Nama Prospect / Nama Pelanggan', field: 'ProspectName', width: '25%' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '25%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionPilihProspect
                }
            ]
        };
    });