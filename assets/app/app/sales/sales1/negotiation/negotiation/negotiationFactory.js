angular.module('app')
  .factory('NegotiationFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user();
    var RoleName = currentUser.RoleName;
    return {
      getData: function() {
        if(RoleName == "SALES"){
          //console.log("kesini sales")
          var res=$http.get('/api/sales/ListTestDrive/Sales');
          //console.log('res=>',res);
          return res;
        } else if (RoleName = "SUPERVISIOR SALES"){
          //console.log("kesini spv")
          var res=$http.get('/api/sales/ListTestDrive/SPV');
          //console.log('res=>',res);
          return res;
        } 
        // var res=$http.get('/api/sales/ListTestDrive/Sales');
        // //console.log('res=>',res);
        // return res;
      },
      getProspectList: function(){
        var res=$http.get('/api/sales/CProspectList');
        return res;
      },
      getFollowUp: function() {
        var res=$http.get('/api/sales/FollowUpTestDrive');
        return res;
      },
      getAlasanJadi: function() {
        var res=$http.get('/api/sales/MActivityReasonBuying');
        return res;
      },
      getAlasanBatal: function() {
        var res=$http.get('/api/sales/MActivityReasonCancelBuy');
        return res;
      },
      getDataVehicleTestDrive: function() {
        var res=$http.get('/api/sales/TestDriveUnitByOutlet');
        return res;
      },
	    getDataVehicleTestDriveTersediaApaKaga: function(unit) {
        var res=$http.get('/api/sales/TestDriveUnitByOutlet/?vehicleModelId='+unit.VehicleModelId+'&vehicleTypeId='+unit.VehicleTypeId);
        return res;
      },
      getTestDrive: function (filter){
        filter.DateTestDrive = new Date(filter.DateTestDrive).getFullYear() + '-'
					+ ('0' + (new Date(filter.DateTestDrive).getMonth() + 1)).slice(-2) + '-'
					+ ('0' + new Date(filter.DateTestDrive).getDate()).slice(-2)
        var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/ListTestDriveByOutlet/?'+param);
        return res;
      },
      getVehicleType: function(){
        var res=$http.get('/api/sales/MUnitVehicleType');
        return res;
      }, 
      getReason: function (Id){
        var res = $http.get('/api/sales/TestDriveSurveyReason/?scheduleId='+Id);
        return res;
      }, 
      getCekTestDrive: function (data){
        var res= $http.get('/api/sales/CekJadwalTestDriveBentrok/?TestDriveUnitId='+data.TestDriveUnitId+'&OutletId='+currentUser.OutletId+'&DateStart='+data.StartDate+'&DateEnd='+data.EndDate+'&ScheduleId='+data.ScheduleId);
        return res;
      },
      create: function(negotiation) {
        console.log("factory", negotiation)
        return $http.post('/api/sales/CreateJadwalTestDrive',[{ ProspectId: negotiation[0].ProspectId, 
                                                                TestDriveUnitId: negotiation[0].TestDriveUnitId, 
                                                                OutletOwnerId: negotiation[0].OutletOwnerId, 
                                                                DateStart: negotiation[0].DateStart, 
                                                                DateEnd: negotiation[0].DateEnd}]);
      },
      createSurveyYes: function (Negotiation){
        return $http.post('/api/sales/TestDriveSurveyReason',[{TestDriveId: Negotiation.TestDriveId,
                                                                InterestedBit: Negotiation.InterestedBit,
                                                                ScheduleId: Negotiation.ScheduleId,
                                                                TanggalTestDrive: Negotiation.TanggalTestDrive,
                                                                StartMeter: Negotiation.StartMeter,
                                                                LastMeter: Negotiation.LastMeter,
                                                                ListReason: Negotiation.ListReason,
                                                               }]);
      },
      createSurveyNo: function (Negotiation){
        return $http.post('/api/sales/TestDriveSurveyReasonCancelBuy',[{TestDriveId: Negotiation.TestDriveId,
                                                                      ScheduleId: Negotiation.ScheduleId,
                                                                      TanggalTestDrive: Negotiation.TanggalTestDrive,
                                                                      StartMeter: Negotiation.StartMeter,
                                                                      LastMeter: Negotiation.LastMeter,
                                                                      ListReasonCancelBuy: Negotiation.ListReasonCancelBuy,
                                                                      }])
      },
      batal: function(negotiation) {
        console.log(negotiation);
        return $http.put('/api/sales/BatalTestDrive', [{ TestDriveId: negotiation.TestDriveId,
                                                          ReasonCancle: negotiation.ReasonCancle
                                                       }])
      },
      update: function(negotiation){
        return $http.put('/api/sales/CreateJadwalTestDrive', [{ TestDriveId: negotiation[0].TestDriveId,
                                                                ProspectId: negotiation[0].ProspectId,
                                                                ScheduleId: negotiation[0].ScheduleId, 
                                                                TestDriveUnitId: negotiation[0].TestDriveUnitId, 
                                                                OutletOwnerId: negotiation[0].OutletOwnerId, 
                                                                DateStart: negotiation[0].DateStart, 
                                                                DateEnd: negotiation[0].DateEnd}]);
      },
    }
  });