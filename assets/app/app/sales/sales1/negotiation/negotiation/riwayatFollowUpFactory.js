angular.module('app')
  .factory('RiwayatFollowUpFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(TestDriveId) {
        var res=$http.get('/api/sales/FollowUpTestDrive?ScheduleId='+TestDriveId);
        return res;
      },
      createFollowUp: function(FollowUp) {
        return $http.put('/api/sales/FollowUpTestDrive', [{
                                                            ScheduleId: FollowUp.ScheduleId,
                                                            StatusFollowUpTestDriveId: FollowUp.StatusFollowUpTestDriveId,
                                                            Note: FollowUp.Note 
                                                           }]);
      },
      create: function(riwayatFollowUp) {
        return $http.post('/api/sales/RiwayatFollowUp', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: riwayatFollowUp.Name,
                                            Description: riwayatFollowUp.Description}]);
      },
      update: function(riwayatFollowUp){
        return $http.put('/api/sales/RiwayatFollowUp', [{
                                            Id: riwayatFollowUp.Id,
                                            //pid: riwayatFollowUp.pid,
                                            Name: riwayatFollowUp.Name,
                                            Description: riwayatFollowUp.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/RiwayatFollowUp',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });