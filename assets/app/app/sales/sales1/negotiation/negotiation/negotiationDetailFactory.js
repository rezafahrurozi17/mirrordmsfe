angular.module('app')
  .factory('NegotiationDetailFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(TestDriveId) {
        var res=$http.get('/api/sales/TestDriveDetailView/?start=1&limit=100&filterData=TestDriveId|'+TestDriveId);
        //console.log('res=>',res);
        return res;
      },
      create: function(negotiation) {
        return $http.post('/api/sales/TestDriveDetailView', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      update: function(negotiation){
        return $http.put('/api/sales/TestDriveDetailView', [{
                                            Id: negotiation.Id,
                                            //pid: negotiation.pid,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/TestDriveDetailView',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });