angular.module('app')
  .factory('StatusFollowUpTestDriveFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/PStatusFollowUpTestDrive');  
        return res;
      },

      create: function(Warna) {
        return $http.post('/api/sales/PStatusFollowUpTestDrive', [{
                                            //AppId: 1,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      update: function(Warna){
        return $http.put('/api/sales/PStatusFollowUpTestDrive', [{
                                            SalesProgramId: Warna.SalesProgramId,
                                            SalesProgramName: Warna.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/PStatusFollowUpTestDrive',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd