angular.module('app')
    .controller('NegotiationController', function($filter, $stateParams, bsNotify, $log, $scope, $http, CurrentUser, NegotiationFactory, RiwayatFollowUpFactory, ModelFactory, StatusFollowUpTestDriveFactory, NegotiationDetailFactory, $timeout) {


        /////////////////////////////////////////////////////////////////////////////
        ///// Test Drive 
        /////////////////////////////////////////////////////////////////////////////

        $scope.$on('$destroy', function() {
            angular.element(".ui.modal.ModalMenungguResponAdmin").remove();
            angular.element(".ui.modal.ModalSiapTestDrive").remove();
            angular.element(".ui.modal.ModalAlasanMembeli").remove();
            angular.element(".ui.modal.ModalAlasanTidakMembeli").remove();
        });

        $scope.kembalidaririwayatFollowup = function() {
            $scope.ShowTestDriveRiwayatFollowUP = false;
            $scope.ShowDetailTestDriveHeadSection = true;
            $scope.ShowDetailTestDriveSubMenu = true;
            $scope.Batal_Button = true;
        }

        $scope.kembalidariFollowup = function() {
            $scope.mNegotiation_Detail.Note = undefined
            $scope.ShowDetailTestDriveSubMenu = true;
            $scope.ShowDetailTestDriveSubMenuFollowUp = false;
            $scope.Batal_Button = true;
        }
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.disabledSavetestdrive = false;

        $scope.user = CurrentUser.user();
        $scope.breadcrums = {};
        $scope.tab = $stateParams.tab;
        $scope.Role = $scope.user.RoleName;
        $scope.intended_operation = null;
        $scope.Negtiation_UdaGantiJem = false;
        console.log("userlogin", $scope.Role);
        var chart = { dataProvider: null };
        $scope.listTestDrive = [];
        $scope.mNegotiation_Detail = {};
        $scope.belumAdatesdrive = false;
        var dataProviders = [];
        $scope.hour = [];
        //$scope.outdataProviders = [];
        var dateGantt = null;
        $scope.isList = false;
        $scope.pilihTestDrive = {};
        $scope.outdataProviders = {};
        //$scope.intended_operation = null;
        $scope.statusBuat = null;
        $scope.optionsReason = [{ reason: "Ya", value: true }, { reason: "Tidak", value: false }];

        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
        }

        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
            minDate: new Date(),
        }

        //$scope.pilihTestDrive.StartDate = new Date("2017-05-05 00:00:00");
        //console.log("strt date", $scope.pilihTestDrive.StartDate)
        //$scope.pilihTestDrive.EndDate = new Date("2017-05-05 00:00:00");

        // $scope.strtDate2 = toUTCDate($scope.pilihTestDrive.StartDate);


        //console.log(" date2", $scope.strtDate2)
        //Tuning
        // NegotiationFactory.getVehicleType().then(function (res) {
        // 	$scope.VehicleType = res.data.Result;
        // })

        NegotiationFactory.getDataVehicleTestDrive().then(function(res) {
            $scope.VehicleTestDrive = res.data.Result;
        })

        // ProspectFactory.getDataProspectName().then(function (res){
        // 	$scope.prospectlist = res.data.Result;
        // })


        // $scope.changed = function () {
        // 	$log.log('Time changed to: ' + $scope.pilihTestDrive.StartDate);
        // };

        $scope.changed = function(enddate) {
                if ($scope.intended_operation == "Insert_Ops") {
                    console.log("===========================================> Kesini ga");
                    $scope.pilihTestDrive.EndDate = angular.copy(enddate);
                }
            }
            // $scope.changed1 = function () {
            // 	$log.log('Time changed to: ' + $scope.pilihTestDrive.EndDate);
            // };
        $scope.prospectSelect = function(select) {
            $scope.mNegotiation.HP = angular.copy(select.HP);
            $scope.mNegotiation.Email = angular.copy(select.Email);
            $scope.mNegotiation.PurchaseTimeName = angular.copy(select.PurchaseTimeName);
        }


        $scope.listSchudleTest = function(dt) {
            console.log("tanggal", dt);
            //if($scope.intended_operation == "Insert_Ops"){
            $scope.pilihTestDrive.DateTestDrive = new Date(dt);
            console.log("tanggal", $scope.pilihTestDrive.DateTestDrive);
            $scope.pilihTestDrive.OutletId = null;

            $scope.pilihTestDrive.StartDate = new Date();
            $scope.pilihTestDrive.EndDate = new Date();
            $scope.isChart = false;
            $scope.isList = true;
            if ($scope.intended_operation == "Insert_Ops") {
                var nowDate = $scope.pilihTestDrive.DateTestDrive.getHours();

                if (nowDate <= 8) {
                    $scope.pilihTestDrive.StartDate.setHours(8);
                    $scope.pilihTestDrive.StartDate.setMinutes(0);
                    $scope.pilihTestDrive.EndDate.setHours(8);
                    $scope.pilihTestDrive.EndDate.setMinutes(0);
                } else if (nowDate >= 23) {
                    $scope.pilihTestDrive.StartDate.setHours(23);
                    $scope.pilihTestDrive.StartDate.setMinutes(0);
                    $scope.pilihTestDrive.EndDate.setHours(8);
                    $scope.pilihTestDrive.EndDate.setMinutes(23);
                } else {
                    $scope.pilihTestDrive.StartDate.setHours(nowDate);
                    $scope.pilihTestDrive.StartDate.setMinutes(0);
                    $scope.pilihTestDrive.EndDate.setHours(nowDate);
                    $scope.pilihTestDrive.EndDate.setMinutes(0);
                }
            }
            //}
            //$scope.outdataProviders = [];
            console.log("filter", $scope.pilihTestDrive);
            NegotiationFactory.getTestDrive($scope.pilihTestDrive).then(function(res) {
                $scope.outletdetail = res.data.Result;

                console.log("testdata", $scope.outletdetail);

                if ($scope.outletdetail.length != 0) {
                    $scope.isList = true;
                }
            })
        }

        $scope.refreshJadwalTestdrive = function() {
            $scope.outletdetail = [];
            $scope.platNo = [];
            $scope.isList = false;
            $scope.isChart = false;
            $scope.pilihTestDrive.TestDriveUnitId = null;
            $scope.pilihTestDrive.DateTestDrive = null;
            $scope.pilihTestDrive.StartDate.setHours(8);
            $scope.pilihTestDrive.EndDate.setHours(8);
            //$scope.pilihTestDrive.VehicleTypeId = null;
        }

        $scope.detailVehicleselect = function(selected) {
            $scope.detailVehicle = selected;
        }

        $scope.detailVehicleclear = function() {
            chart = { dataProvider: null };

            if ($scope.intended_operation == "Insert_Ops") {
                $scope.isChart = false;
                $scope.isList = false;
                $scope.pilihTestDrive.OutletId = null;
                $scope.pilihTestDrive.DateTestDrive = null;
                $scope.outletdetail = [];
            }
            //yang di bawah ini tadi ga ada semua
            $scope.detailVehicle.VehicleTypeId = $scope.pilihTestDrive.VehicleTypeId;
            for (var i = 0; i < $scope.VehicleTestDrive.length; ++i) {
                if ($scope.VehicleTestDrive[i].VehicleTypeId == $scope.detailVehicle.VehicleTypeId) {
                    $scope.detailVehicle.Description = $scope.VehicleTestDrive[i].Description;
                }
            }
        }

        // $scope.test = function () {
        // 	console.log("teste", $scope.detailVehicle);
        // }

        $scope.kembalidaritestdrive = function() {
            // if($scope.StatusBatal == 'dariDetail'){
            // 	$scope.ShowDetailTestDriveSubMenu = true;
            // 	$scope.ShowDetailTestDriveSubMenu = null
            // }else{
            $scope.intended_operation = null;
            $scope.outletdetail = [];
            $scope.mNegotiation_Detail.ReasonCancle = undefined;
            $scope.isChart = false;
            $scope.isList = false;
            $scope.ShowParameter = false;
            $scope.Add_Button = true;
            $scope.ShowTable = true;
            $scope.ShowDetailTestDriveBatal = false;
            $scope.ShowDetailTestDriveSubMenuFollowUp = false;
            $scope.ShowTestDriveSurvey = false;
            //}
        }

        $scope.back = function() {
            $scope.isChart = false;
            $scope.isList = false;
            $scope.ShowParameter = true;
            $scope.ShowJadwalTestDrive = false;
        }

        $scope.dataJam = function(outlet) {
            console.log("===> function data jam");
            console.log("outlet", outlet);
            $scope.isChart = true;
            var count = 0;
            //$scope.detailVehicle = $scope.pilihTestDrive;
            console.log("listvehcle", $scope.VehicleTestDrive);
            console.log("pilihtestdrive", $scope.pilihTestDrive);
            $scope.detailVehicle = { VehicleTypeId: null, Description: null };

            ///simpan outlet
            var tempOutletId = $scope.pilihTestDrive.OutletId;

            var jumlahunit = outlet.ListUnitTestDrive.length;

            for (var i in outlet.ListUnitTestDrive) {
                if (outlet.ListUnitTestDrive[i].ListJadwalTestDrive.length == 0) {
                    count++;
                }
            }

            if (jumlahunit == count) {
                $scope.belumAdatesdrive = true;
                bsNotify.show({
                    size: 'big',
                    type: 'success',
                    timeout: 2000,
                    title: "Hari ini belum ada jadwal test drive.",
                });
            }

            for (var i in $scope.VehicleTestDrive) {
                if ($scope.VehicleTestDrive[i].VehicleTypeId == $scope.pilihTestDrive.VehicleTypeId) {
                    $scope.detailVehicle.VehicleTypeId = $scope.VehicleTestDrive[i].VehicleTypeId;
                    $scope.detailVehicle.Description = $scope.VehicleTestDrive[i].Description;
                }

            }

            var year = new Date($scope.pilihTestDrive.DateTestDrive).getFullYear();
            var mounth = ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2);
            var date = ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2);
            var hour = (new Date().getHours() < '10' ? '0' : '') + new Date().getHours();
            console.log("negotiation", $scope.mNegotiation);

            var hourstart = (new Date($scope.mNegotiation.DateStart).getHours() < '10' ? '0' : '') + new Date().getHours();
            var hoursend = (new Date($scope.mNegotiation.EndDate).getHours() < '10' ? '0' : '') + new Date().getHours();


            //console.log("jam dapet", year, mounth, date, hour)
            if ($scope.intended_operation == "Insert_Ops") {
                $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.DateTestDrive);
                $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.DateTestDrive);
                $scope.pilihTestDrive.StartDate.setHours(8);
                $scope.pilihTestDrive.EndDate.setHours(8);
                console.log("semprul", $scope.pilihTestDrive.StartDate);
                console.log("semprul", $scope.pilihTestDrive.EndDate);
            } else if ($scope.intended_operation == "Update_Ops") {

                $scope.pilihTestDrive.StartDate = new Date(new Date($scope.mNegotiation.DateStart).getFullYear(),
                    new Date($scope.mNegotiation.DateStart).getMonth(),
                    new Date($scope.mNegotiation.DateStart).getDate(),
                    new Date($scope.mNegotiation.DateStart).getHours(),
                    new Date($scope.mNegotiation.DateStart).getMinutes());


                $scope.pilihTestDrive.EndDate = new Date(new Date($scope.mNegotiation.DateEnd).getFullYear(),
                    new Date($scope.mNegotiation.DateEnd).getMonth(),
                    new Date($scope.mNegotiation.DateEnd).getDate(),
                    new Date($scope.mNegotiation.DateEnd).getHours(),
                    new Date($scope.mNegotiation.DateEnd).getMinutes());



                console.log("jam dapet", $scope.pilihTestDrive);
            }
            // $scope.pilihTestDrive.StartDate = new Date($scope.pilihTestDrive.StartDate).setMinutes(00);
            // $scope.pilihTestDrive.EndDate = new Date($scope.pilihTestDrive).setMinutes(00);

            dateGantt = year + '-' + mounth + '-' + date;

            dataProviders = outlet.ListUnitTestDrive;
            $scope.outdataProviders = dataProviders;

            $scope.platNo = [{ TestDriveUnitId: null, PoliceNumber: null }];
            for (var i in dataProviders) {
                $scope.platNo.push({
                    OutletOwnerId: dataProviders[i].OutletOwnerId,
                    TestDriveUnitId: dataProviders[i].TestDriveUnitId,
                    PoliceNumber: dataProviders[i].PoliceNumber
                });
            }

            da_guides = [{
                "value": null,
                "lineColor": null,
                "lineThickness": null,
                "dashLength": null,
                "inside": null,
                "labelRotation": null,
                "label": null
            }];
            da_guides.splice(0, 1);

            if (dataProviders[0].ListJadwalTestDrive.length <= 0) {
                console.log("ulala");
                dataProviders[0].ListJadwalTestDrive[0] = [{
                    ColorId: null,
                    DateEnd: null,
                    DateStart: null,
                    OutletId: null,
                    PoliceNumber: null,
                    ScheduleId: null,
                    StatusFollowUpTestDriveId: null,
                    StatusTestDriveId: null,
                    TestDriveUnitId: null,
                    TimeDuration: null,
                    TimeEnd: null,
                    TimeStart: null,
                    VehicleMappingId: null,
                    VehicleModelId: null,
                    VehicleTypeId: null,
                    color: null
                }];


            }

            for (var i = 0; i < dataProviders[0].ListJadwalTestDrive.length; ++i) {
                try {
                    var raw_garis_atas = dataProviders[0].ListJadwalTestDrive[i].DateEnd;
                    var tanggal_garis_atas = raw_garis_atas.getFullYear() + '-' + ('0' + (raw_garis_atas.getMonth() + 1)).slice(-2) + '-' + ('0' + raw_garis_atas.getDate()).slice(-2);
                    var jam_garis_atas = $filter('date')(raw_garis_atas, 'HH:mm:ss');
                    da_guides.push({
                        value: AmCharts.stringToDate(tanggal_garis_atas + " " + jam_garis_atas, "YYYY-MM-DD HH:NN"),
                        lineColor: "#000000",
                        lineThickness: 4,
                        dashLength: 2,
                        inside: true,
                        labelRotation: 90,
                        label: "" + jam_garis_atas
                    });
                    if (i == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#67b7dc";
                    } else if (i == 1) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#71c9f2";
                    } else if (i % 3 == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#88f1ff";
                    } else if (i % 2 == 0) {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#b1ffff";
                    } else {
                        dataProviders[0].ListJadwalTestDrive[i]['color'] = "#42ff33";
                    }
                } catch (ulala) {}

            }

            chart = AmCharts.makeChart('chartdiv', {
                "type": "gantt",
                "theme": "light",
                "zoomOutText": "",
                "marginRight": 0,
                "marginLeft": 0,
                "period": "hh",
                "dataDateFormat": "YYYY-MM-DD",
                "balloonDateFormat": "JJ:NN",
                "columnWidth": 1,
                "valueAxis": {
                    "type": "number",
                    "includeHidden": true,
                    "maximum": 22.5,
                    "minimum": 8,
					"reversed":true,
					"showBalloon": false,
					"labelFunction": function(data) {return ''+data+":00";},
                        //"guides": da_guides
                },
                "categoryAxis": {
                    "position": "top"
                },
                "brightnessStep": 10,
                "graph": {
                    "fillAlphas": 1,
                    //"balloonText": "<b>[[task]]</b>: [[open]] [[value]]",
                    "labelPosition": "top",
                    "showBalloon": false
                },
                "rotate": false,
                "categoryField": "PoliceNumber",
                "segmentsField": "ListJadwalTestDrive",
                "colorField": "color",
                "startDate": dateGantt,
                "startField": "TimeStart",
                "endField": "TimeEnd",
                "durationField": "duration",
                "dataProvider": dataProviders,
                "valueScrollbar": {
                    "autoGridCount": true,
                    "enabled": false
                },
                "chartCursor": {
                    "cursorColor": "#55bb76",
                    "valueBalloonsEnabled": true,
                    "cursorAlpha": 0,
                    "valueLineAlpha": 0.5,
                    "valueLineBalloonEnabled": true,
                    "valueLineEnabled": true,
                    "zoomable": false,
                    "valueZoomable": false
                },
                "export": {
                    "enabled": false
                }
            });
            console.log("===>end function data jam");
        }

        $scope.platnoAmbil = function(selected) {
            console.log("test", selected);
            $scope.pilihTestDrive.OutletOwnerId = selected.OutletOwnerId;
            var tempunit = $scope.outdataProviders.filter(function(type) {
                return (type.TestDriveUnitId == $scope.pilihTestDrive.TestDriveUnitId);
            })
            console.log("unit", tempunit);
            $scope.hour = [];
            for (var i in tempunit[0].ListJadwalTestDrive) {
                $scope.hour.push({ start: new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(), end: new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours() })
                    // for (var j = new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(); j < new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours(); j++) {
                    // 	$scope.hour.push(j);
                    // }
            }
            console.log("jam array", $scope.hour);
        }

        $scope.platnoAmbilChange = function() {
            if ($scope.intended_operation == 'Update_Ops') {
                //console.log("test", selected);
                //$scope.pilihTestDrive.OutletOwnerId = selected.OutletOwnerId;
                var tempunit = $scope.outdataProviders.filter(function(type) {
                    return (type.TestDriveUnitId == $scope.pilihTestDrive.TestDriveUnitId);
                })
                console.log("unit", tempunit);
                $scope.hour = [];
                for (var i in tempunit[0].ListJadwalTestDrive) {
                    $scope.hour.push({ start: new Date(tempunit[0].ListJadwalTestDrive[i].DateStart).getHours(), end: new Date(tempunit[0].ListJadwalTestDrive[i].DateEnd).getHours() })
                }
                //console.log("jam array", $scope.hour);
                //break;
            }
        }

        function fixDate(date) {
            var datefixed = date.getFullYear() + '-' +
                ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + date.getDate()).slice(-2) + 'T' +
                ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                '00:' + '00';
            return datefixed;
        }


        $scope.submit = function() {
            console.log("===> function submit");
            console.log("Date Test Drive", $scope.pilihTestDrive.DateTestDrive);
            console.log("startDate", $scope.pilihTestDrive.StartDate);
            console.log("endDate", $scope.pilihTestDrive.EndDate);
            $scope.Negtiation_UdaGantiJem = true;
            if ($scope.intended_operation == 'Insert_Ops' || $scope.intended_operation == 'Buat_Detail') {
                $scope.datestart =
                    new Date($scope.pilihTestDrive.DateTestDrive).getFullYear() + '-' +
                    ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2) + 'T' +
                    ((new Date($scope.pilihTestDrive.StartDate).getHours() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.StartDate).getHours()) + ':' +
                    '00:' + '00';
                $scope.dateend =
                    new Date($scope.pilihTestDrive.DateTestDrive).getFullYear() + '-' +
                    ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2) + 'T' +
                    ((new Date($scope.pilihTestDrive.EndDate).getHours() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.EndDate).getHours()) + ':' +
                    '00:' + '00';

                console.log("setan3", $scope.pilihTestDrive.StartDate);
                console.log("setan4", $scope.pilihTestDrive.EndDate);
                $scope.NegotiationDaChengedStartDate = $scope.pilihTestDrive.StartDate;
                $scope.NegotiationDaChengedEndDate = $scope.pilihTestDrive.EndDate;

                // + ((new Date($scope.pilihTestDrive.EndDate).getMinutes() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.EndDate).getMinutes()) + ':'
                // + ((new Date($scope.pilihTestDrive.EndDate).getSeconds() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.EndDate).getSeconds());
                // $scope.pilihTestDrive.StartDate.setHours($scope.pilihTestDrive.StartDate.getHours() + $scope.pilihTestDrive.StartDate.getTimezoneOffset() / 60);
                // $scope.pilihTestDrive.EndDate.setHours($scope.pilihTestDrive.EndDate.getHours() +$scope.pilihTestDrive.EndDate.getTimezoneOffset() / 60);
                //console.log("NEgotiation", $scope.mNegotiation);
                //console.log("testdrive asdad", $scope.pilihTestDrive);
                console.log("startDate", $scope.datestart);
                console.log("endDate", $scope.dateend);

                var countstart = 0;
                var countEnd = 0;
                var range = 0;

                $scope.hasil = true;
                $scope.pilihJam = [];
                for (var j = new Date($scope.pilihTestDrive.StartDate).getHours(); j < new Date($scope.pilihTestDrive.EndDate).getHours(); j++) {
                    $scope.pilihJam.push(j);
                }

                for (var input in $scope.pilihJam) {
                    for (var jadwal in $scope.hour) {
                        if ($scope.hour[jadwal].start <= $scope.pilihJam[input] && $scope.pilihJam[input] < $scope.hour[jadwal].end) {
                            $scope.hasil = false;
                            break;
                        }
                    }
                }

                // if ($scope.hasil == false) {
                //     bsNotify.show({
                //         title: "Warning Message",
                //         content: "Sudah Ada Test Drive Pada Jam ini",
                //         type: 'warning'
                //     });

                // } else {
                $scope.listTestDrive.splice(0, 1);
                $scope.listTestDrive.push({
                    VehicleType: $scope.detailVehicle.Description,
                    TestDriveUnitId: $scope.pilihTestDrive.TestDriveUnitId,
                    VehicleTypeId: $scope.pilihTestDrive.VehicleTypeId,
                    StartDate: $scope.datestart,
                    EndDate: $scope.dateend,
                    OutletOwnerId: $scope.pilihTestDrive.OutletOwnerId,
                    ScheduleId: 0
                });

                NegotiationFactory.getCekTestDrive($scope.listTestDrive[0]).then(function(res) {
                    var hasil = res;
                    console.log("oioioioi", res);
                    if (res.data == false) {
                        $scope.ShowParameter = true;
                        $scope.ShowJadwalTestDrive = false;
                        $scope.Batal_Button = true;
                        $scope.Submit_Button = true;
                        $scope.pilihTestDrive = {};
                        dataProviders = [];
                    } else {
                        bsNotify.show({
                            title: "Warning Message",
                            content: "Sudah Ada Test Drive Pada Jam ini",
                            type: 'warning'
                        });
                    }
                });

                //}
            } else if ($scope.intended_operation == 'Update_Ops') {
                console.log("update bro");
                $scope.datestart = new Date($scope.pilihTestDrive.DateTestDrive).getFullYear() + '-' +
                    ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2) + 'T' +
                    ((new Date($scope.pilihTestDrive.StartDate).getHours() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.StartDate).getHours()) + ':' +
                    '00:' + '00';


                $scope.dateend = new Date($scope.pilihTestDrive.DateTestDrive).getFullYear() + '-' +
                    ('0' + (new Date($scope.pilihTestDrive.DateTestDrive).getMonth() + 1)).slice(-2) + '-' +
                    ('0' + new Date($scope.pilihTestDrive.DateTestDrive).getDate()).slice(-2) + 'T' +
                    ((new Date($scope.pilihTestDrive.EndDate).getHours() < '10' ? '0' : '') + new Date($scope.pilihTestDrive.EndDate).getHours()) + ':' +
                    '00:' + '00';

                console.log("setan3", new Date($scope.pilihTestDrive.StartDate));
                console.log("setan4", new Date($scope.pilihTestDrive.EndDate));
                $scope.NegotiationDaChengedStartDate = new Date($scope.pilihTestDrive.StartDate);
                $scope.NegotiationDaChengedEndDate = new Date($scope.pilihTestDrive.EndDate);

                console.log("debug", $scope.datestart + "-" + $scope.dateend);
                var countstart = 0;
                var countEnd = 0;
                var range = 0;

                $scope.hasil = true;

                $scope.pilihJam = [];
                for (var j = new Date($scope.pilihTestDrive.StartDate).getHours(); j < new Date($scope.pilihTestDrive.EndDate).getHours(); j++) {
                    $scope.pilihJam.push(j);
                }
                console.log("pilih jam", $scope.pilihJam);
                console.log("timestart", $scope.tempStart, $scope.pilihTestDrive.StartDate);
                console.log("tempend", $scope.tempEnd, $scope.pilihTestDrive.EndDate);

                for (var input in $scope.pilihJam) {
                    for (var jadwal in $scope.hour) {
                        if ($scope.hour[jadwal].start <= $scope.pilihJam[input] && $scope.pilihJam[input] < $scope.hour[jadwal].end) {
                            $scope.hasil = false;
                            break;
                        }
                    }
                }

                //if($scope.tempStart != $scope.pilihTestDrive.StartDate && $scope.tempEnd != $scope.pilihTestDrive.EndDate){
                // for (let input of $scope.pilihJam) {
                //     for (let jadwal of $scope.hour) {
                //         if (jadwal.start <= input && input < jadwal.end) {
                //             $scope.hasil = false;
                //             break;
                //         }
                //     }
                // }
                //}

                // if ($scope.hasil == false) {
                //     bsNotify.show({
                //         title: "Warning Message",
                //         content: "Sudah Ada Test Drive Pada Jam ini",
                //         type: 'warning'
                //     });
                // } else {
                $scope.listTestDrive.splice(0, 1);
                $scope.listTestDrive.push({
                    VehicleType: $scope.detailVehicle.Description,
                    TestDriveUnitId: $scope.pilihTestDrive.TestDriveUnitId,
                    VehicleTypeId: $scope.pilihTestDrive.VehicleTypeId,
                    StartDate: $scope.datestart,
                    EndDate: $scope.dateend,
                    OutletOwnerId: $scope.pilihTestDrive.OutletOwnerId,
                    ScheduleId: $scope.mNegotiation.ScheduleId
                });

                NegotiationFactory.getCekTestDrive($scope.listTestDrive[0]).then(function(res) {
                    var hasil = res;
                    console.log("oioioioi", res);
                    if (res.data == false) {
                        $scope.ShowParameter = true;
                        $scope.ShowJadwalTestDrive = false;
                        $scope.Batal_Button = true;
                        $scope.Submit_Button = true;
                        $scope.pilihTestDrive = {};
                        dataProviders = [];
                    } else {
                        bsNotify.show({
                            title: "Warning Message",
                            content: "Sudah Ada Test Drive Pada Jam ini",
                            type: 'warning'
                        });
                    }
                });
                // $scope.ShowParameter = true;
                // $scope.ShowJadwalTestDrive = false;
                // $scope.Batal_Button = true;
                // $scope.Submit_Button = true;
                // $scope.pilihTestDrive = {};
                // dataProviders = [];
            }
            //}


            //console.log("testdrive 2", $scope.pilihTestDrive);
            console.log("testdrive", $scope.listTestDrive);
            console.log("===>end function submit");

        };


        $scope.savetestdrive = function() {
            $scope.disabledSavetestdrive = true;
            console.log("sdsad", $scope.intended_operation);
            if ($scope.intended_operation == "Update_Ops") {
                $scope.mNegotiationPut = [];
                for (var i in $scope.listTestDrive) {
                    $scope.mNegotiationPut.push({
                        TestDriveId: $scope.mNegotiation.TestDriveId,
                        ProspectId: $scope.mNegotiation.ProspectId,
                        ScheduleId: $scope.mNegotiation.ScheduleId,
                        TestDriveUnitId: $scope.listTestDrive[i].TestDriveUnitId,
                        OutletOwnerId: $scope.listTestDrive[i].OutletOwnerId,
                        DateStart: $scope.listTestDrive[i].StartDate,
                        DateEnd: $scope.listTestDrive[i].EndDate
                    })
                }
                //console.log("submit", $scope.mNegotiationPost);
                NegotiationFactory.update($scope.mNegotiationPut).then(function(res) {
                    var respond = res.data.ResponseMessage;
                    var lengthstring = res.data.ResponseMessage.length;
                    var count = (respond.match(/#/g) || []).length;
                    var index = respond.indexOf('#');

                    if (count >= 1) {
                        bsNotify.show({
                            title: "Info Message",
                            content: respond.slice(index + 1, lengthstring),
                            type: 'info'
                        });
                        $scope.disabledSavetestdrive = false;
                    }

                    $scope.ShowParameter = false;
                    $scope.ShowTable = true;
                    $scope.initialize();
                    $scope.mNegotiationPut = {};
                    $scope.listTestDrive = [];
                })
            } else {
                $scope.mNegotiationPost = [];
                for (var i in $scope.listTestDrive) {

                    $scope.mNegotiationPost.push({
                        ProspectId: $scope.mNegotiation.ProspectId,
                        TestDriveUnitId: $scope.listTestDrive[i].TestDriveUnitId,
                        OutletOwnerId: $scope.listTestDrive[i].OutletOwnerId,
                        DateStart: $scope.listTestDrive[i].StartDate,
                        DateEnd: $scope.listTestDrive[i].EndDate
                    })
                }

                NegotiationFactory.create($scope.mNegotiationPost).then(function(res) {
                    var respond = res.data.ResponseMessage;
                    var lengthstring = res.data.ResponseMessage.length;
                    var count = (respond.match(/#/g) || []).length;
                    var index = respond.indexOf('#');

                    if (count >= 1) {
                        bsNotify.show({
                            title: "Info Message",
                            content: respond.slice(index + 1, lengthstring),
                            type: 'info'
                        });
                        $scope.disabledSavetestdrive = false;
                    } else {
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Test drive berhasil diajukan",
                            type: 'success'
                        });
                        $scope.disabledSavetestdrive = false;
                    }

                    $scope.ShowParameter = false;
                    $scope.ShowTable = true;
                    $scope.mNegotiationPost = {};
                    $scope.initialize();
                    $scope.listTestDrive = [];
                },
                        function (err) {

                            bsNotify.show(
                                {
                                    title: "Gagal",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                            $scope.disabledSavetestdrive = false;
                        })
            }
            // NegotiationFactory.create($scope.mNegotiationPost).then(function (res) {
            // 	$scope.ShowParameter = false; $scope.ShowTable = true;
            // 	$scope.mNegotiationPost = {};
            // 	$scope.initialize();
            // 	$scope.listTestDrive = [];
            // })
            //}
            $scope.Add_Button = true;
            $scope.intended_operation = null;
        }

        $scope.refreshjadwal = function() {
            console.log("kesini ng-change", $scope.outletdetail)
            if ($scope.intended_operation == "Update_Ops") {
                for (var i in $scope.outletdetail) {
                    if ($scope.outletdetail[i].OutletId == $scope.pilihTestDrive.OutletId) {
                        $scope.outletdetailselect = $scope.outletdetail[i];
                    }
                }
                $scope.dataJam($scope.outletdetailselect);
            }
        }

        //console.log(chart);
        //////////////////////////////////////////////////////////////////////
        ///
        //////////////////////////////////////////////////////////////////////

        $scope.SimpanFollowUp = function() {
            console.log("nego detail", $scope.mNegotiation_Detail);
            RiwayatFollowUpFactory.createFollowUp($scope.mNegotiation_Detail).then(function(res) {
                $scope.mNegotiation_Detail.Note = undefined;
                $scope.ShowDetailTestDriveSubMenu = true;
                $scope.ShowDetailTestDriveSubMenuFollowUp = false;
				bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil tersimpan.",
											type: 'success'
										}
									);
            })
        };

        $scope.BatalTestDrive = function() {
            NegotiationFactory.batal($scope.mNegotiation_Detail).then(function(res) {
                $scope.initialize();
                $scope.mNegotiation_Detail.ReasonCancle = undefined;
                $scope.ShowDetailTestDriveBatal = false;
                $scope.ShowTable = true;
                $scope.Add_Button = true;
				bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil tersimpan.",
											type: 'success'
										}
									);
            })
        };

        //$scope.ulala=NegotiationFactory.getData();
        $scope.initialize = function() {
            $scope.disabledSavetestdrive = false;
            NegotiationFactory.getData().then(function(res) {
                $scope.ulala = res.data.Result;

                var count_Menunggu_Respon = 0;
                for (var i = 0; i < res.data.Result.length; ++i) {
                    if (res.data.Result[i].StatusTestDriveName == 'Menunggu Respon Admin') {
                        count_Menunggu_Respon++;
                    }
                }
                $scope.Menunggu_Respon_length = count_Menunggu_Respon;

                var count_Siap_Test_Drive = 0;
                for (var i = 0; i < res.data.Result.length; ++i) {
                    if (res.data.Result[i].StatusTestDriveName == 'Siap Test Drive') {
                        count_Siap_Test_Drive++;
                    }
                }
                $scope.Siap_Test_Drive_length = count_Siap_Test_Drive;

                return $scope.ulala;
            });
			$scope.intended_operation = null;
        }

        $scope.initialize();


        $scope.Show_Hide_Menunggu_Respon_Admin_PanelIcon = "+";
        $scope.Show_Hide_Siap_Test_Drive_PanelIcon = "+";
        $scope.ShowTable = true;
        $scope.Add_Button = true;

        //$scope.optionsHasilStatusTestDrive = [{ name: "berhasil", value: "berhasil" }, { name: "gagal", value: "gagal" }];
        StatusFollowUpTestDriveFactory.getData().then(function(res) {
            $scope.optionsHasilStatusTestDrive = res.data.Result;
            return $scope.optionsHasilStatusTestDrive;
        });

        //$scope.optionsModelType = [{ name: "Avanza", value: "Avanza" }, { name: "Harrier", value: "Harrier" }, { name: "Innova", value: "Innova" }, { name: "Mark X", value: "Mark X" }, { name: "Alphard", value: "Alphard" }, { name: "FT86", value: "FT86" }];
        //    ModelFactory.getData().then(function(res){
        //         $scope.optionsModelType = res.data.Result;
        //         return $scope.optionsModelType;
        //     });



        //$scope.intended_operation="None";//status operation
        var Selected_Detail_Id = ""; //ini buat selected id yg detail
        $scope.ArrayRiwayatFollowUP = null; //ini riwayat by default 0 dulu

        $scope.Show_Hide_Menunggu_Respon_Admin = function() {
            $scope.Tabel_Menunggu_Respon = !$scope.Tabel_Menunggu_Respon;


            if ($scope.Tabel_Menunggu_Respon == true) {
                $scope.Show_Hide_Menunggu_Respon_Admin_PanelIcon = "-";
            } else if ($scope.Tabel_Menunggu_Respon == false) {
                $scope.Show_Hide_Menunggu_Respon_Admin_PanelIcon = "+";
            }
        };

        $scope.Show_Hide_Siap_Test_Drive = function() {
            $scope.Tabel_Siap_Test_Drive = !$scope.Tabel_Siap_Test_Drive;

            if ($scope.Tabel_Siap_Test_Drive == true) {
                $scope.Show_Hide_Siap_Test_Drive_PanelIcon = "-";
            } else if ($scope.Tabel_Siap_Test_Drive == false) {
                $scope.Show_Hide_Siap_Test_Drive_PanelIcon = "+";
            }
        };



        $scope.ActionSelected = function(From, SelectedData) {
            console.log("testing", SelectedData)
            $scope.SelectedTestDrive = SelectedData;

            $scope.statusProspect = $scope.SelectedTestDrive.StatusTestDriveName;
            //$scope.mNegotiation_Detail.TestDriveId = null;
            $scope.mNegotiation_Detail = angular.copy(SelectedData);
            if (From == "MenungguResponAdmin") {
                setTimeout(function() { angular.element(".ui.modal.ModalMenungguResponAdmin").modal("refresh"); }, 0)
                angular.element(".ui.modal.ModalMenungguResponAdmin").modal("show");
                angular.element(".ui.modal.ModalMenungguResponAdmin").not(":first").remove();
                //$scope.ModalMenungguResponAdmin={show:true};
            } else if (From == "SiapTestDrive") {

                setTimeout(function() { angular.element(".ui.modal.ModalSiapTestDrive").modal("refresh"); }, 0)
                angular.element(".ui.modal.ModalSiapTestDrive").modal("show");
                angular.element(".ui.modal.ModalSiapTestDrive").not(":first").remove();
            }

        };

        $scope.mNegotiation_jadwalBaru = {};

        $scope.permintaanJadwalBaru = function() {
            $scope.intended_operation = 'Buat_Detail';
            console.log("baru", $scope.mNegotiation_jadwalBaru);
            $scope.mNegotiation = { ProspectId: null, HP: null, Email: null, PruchaseTimeName: null };
            $scope.ShowParameter = true;
            $scope.ShowDetailTestDrive = false;
            $scope.mNegotiation.ProspectId = $scope.mNegotiation_jadwalBaru.ProspectId;
            $scope.mNegotiation.HP = $scope.mNegotiation_jadwalBaru.HP;
            $scope.mNegotiation.Email = $scope.mNegotiation_jadwalBaru.Email;
            $scope.mNegotiation.PurchaseTimeName = $scope.mNegotiation_jadwalBaru.PurchaseTimeName;
        }


        $scope.LookSelected = function(SelectedData) {
            //$scope.mNegotiation_Detail=angular.copy(SelectedData);
            $scope.Negtiation_UdaGantiJem = false;
            $scope.SchudleId = SelectedData.ScheduleId;
            Selected_Detail_Id = SelectedData.TestDriveId;
            console.log("++++++++++++++++++++++++++++++++ini baru saya buat", SelectedData);
            NegotiationDetailFactory.getData(SelectedData.TestDriveId).then(function(res) {
                $scope.mNegotiation_Detail = res.data.Result[0];
                // return $scope.mNegotiation_Detail;
                $scope.mNegotiation_jadwalBaru = angular.copy($scope.mNegotiation_Detail);
                console.log("testdrive", $scope.mNegotiation_Detail);
            });

            angular.element(".ui.modal.ModalMenungguResponAdmin").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalMenungguResponAdmin={show:false};
            angular.element(".ui.modal.ModalSiapTestDrive").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalSiapTestDrive={show:false};
            //$scope.ShowDetailTestDriveSubMenu=!$scope.ShowDetailTestDriveSubMenu;//tau apaan
            $scope.ShowDetailTestDriveSubMenu = true;
            $scope.ShowDetailTestDriveSubMenuFollowUp = false;
            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowParameter = false;
            $scope.ShowDetailTestDrive = true;
            $scope.ShowDetailTestDriveHeadSection = true;
            $scope.ShowTable = false;
            $scope.Submit_Button = false;
            $scope.Batal_Button = true;
            $scope.Add_Button = false;
            $scope.intended_operation = "Lihat_Ops";
        };

        $scope.tempStart = new Date();
        $scope.tempEnd = new Date();

        $scope.ChangeSelected = function(SelectedData) {
            $scope.breadcrums.title="Ubah";
            $scope.intended_operation = "Update_Ops";
            $scope.Negtiation_UdaGantiJem = false;
            //$scope.statusBuat = stsBuat;
            console.log("select", SelectedData, $scope.intended_operation);
            $scope.mNegotiation = angular.copy(SelectedData);
            // console.log("timestart", $scope.tempStart, $scope.pilihTestDrive.StartDate);
            // 	console.log("tempend", $scope.tempEnd, $scope.pilihTestDrive.EndDate);



            // console.log("timestart", $scope.tempStart, $scope.pilihTestDrive.StartDate);
            // console.log("tempend", $scope.tempEnd, $scope.pilihTestDrive.EndDate);

            $scope.listTestDrive.splice(0, 1);
            $scope.listTestDrive.push({
                TestDriveId: SelectedData.TestDriveId,
                ProspectId: SelectedData.ProspectId,
                ScheduleId: SelectedData.ScheduleId,
                TestDriveUnitId: SelectedData.TestDriveUnitId,
                OutletId: SelectedData.OutletId,
                VehicleType: SelectedData.DescriptionVehicleType,
                StartDate: SelectedData.DateStart,
                EndDate: SelectedData.DateEnd
            })

            //$scope.mNegotiation_Id=Id;
            angular.element(".ui.modal.ModalMenungguResponAdmin").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalMenungguResponAdmin={show:false};
            angular.element(".ui.modal.ModalSiapTestDrive").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalSiapTestDrive={show:false};

            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowParameter = true;
            $scope.ShowDetailTestDrive = false;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.ShowTable = false;
            $scope.Batal_Button = true;
            $scope.Submit_Button = true;
            $scope.Add_Button = false;
        };

        $scope.Batal_Clicked = function() {

            $scope.mProfileSalesProgram_SalesProgramName = null;
            $scope.intended_operation = null;

            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowParameter = false;
            $scope.ShowDetailTestDrive = false;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.ShowTable = true;
            $scope.Submit_Button = false;
            $scope.Batal_Button = false;
            $scope.Add_Button = true;
        };

        NegotiationFactory.getProspectList().then(
            function(res) {
                $scope.prospectlist = res.data.Result;


                $scope.prospectlist = $scope.prospectlist.filter(function(daoutletCabang) { //OrgId
                    return (daoutletCabang.CProspectDetailUnitView.length >= 1);
                })



                return res.data;
            }
        );

        $scope.Add_Clicked = function() {
            $scope.Negtiation_UdaGantiJem = false;
            $scope.listTestDrive = [];
            $scope.breadcrums.title="Tambah";
            $scope.mProfileSalesProgram_SalesProgramName = null;
            $scope.intended_operation = "Insert_Ops";
            $scope.mNegotiation = {};

            $scope.ShowParameter = true;
            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowDetailTestDrive = false;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.ShowTable = false;
            $scope.Submit_Button = true;
            $scope.Batal_Button = true;
            $scope.Add_Button = false;
        };

        $scope.Simpan_Clicked = function() {
            //put httppost,delete,cll here
            if ($scope.intended_operation == "Update_Ops") {
                alert("update");
            } else if ($scope.intended_operation = "Insert_Ops") {
                alert("insert by default Siap_Test_Drive dulu");
                $scope.mNegotiation.Status = "Menunggu_Respon";
                $scope.ulala.push($scope.mNegotiation);
            }


            var count_Menunggu_Respon = 0;
            for (var i = 0; i < $scope.ulala.length; ++i) {
                if ($scope.ulala[i].Status == 'Menunggu_Respon') {
                    count_Menunggu_Respon++;
                }

            }
            $scope.Menunggu_Respon_length = count_Menunggu_Respon;

            var count_Siap_Test_Drive = 0;
            for (var i = 0; i < $scope.ulala.length; ++i) {
                if ($scope.ulala[i].Status == 'Siap_Test_Drive') {
                    count_Siap_Test_Drive++;
                }

            }
            $scope.Siap_Test_Drive_length = count_Siap_Test_Drive;

            //clear all input stuff first 
            $scope.mProfileSalesProgram_SalesProgramName = null;

            $scope.ShowParameter = false;
            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowDetailTestDrive = false;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.ShowTable = true;
            $scope.Submit_Button = false;
            $scope.Batal_Button = false;
            $scope.Add_Button = true;

            $scope.intended_operation = "None";
        };

        $scope.Test_Drive_Follow_Up = function(Test_Drive_Status) {
            $scope.Batal_Button = false;
            $scope.Back_To_Test_Drive_Detail_Button = true;
            $scope.ShowDetailTestDriveSubMenuFollowUp = true;
            $scope.ShowDetailTestDriveSubMenu = false;
            //$scope.HasilStatusTestDrive=Test_Drive_Status


            if (Test_Drive_Status == "berhasil") {
                $scope.mNegotiation_Detail.StatusFollowUpTestDriveId = 1;
            } else if (Test_Drive_Status == "gagal") {
                $scope.mNegotiation_Detail.StatusFollowUpTestDriveId = 2;
            }
            $scope.HasilStatusTestDriveUnEditable = false; //ini disable lo bukan show

            //alert(Selected_Detail_Id);
        };

        $scope.Back_To_List_Test_Drive = function() {
            $scope.ShowTable = true;
            $scope.Add_Button = true;

            $scope.ShowDetailTestDrive = false; //harus
            $scope.ShowDetailTestDriveBatal = false; //harus
            $scope.ShowTestDriveSurvey = false; //harus
            $scope.Back_To_List_Test_Drive_Button = false;

        };

        $scope.Back_To_Test_Drive_Detail = function() {

            $scope.Batal_Button = true;
            $scope.ShowDetailTestDriveSubMenu = true;
            $scope.Back_To_Test_Drive_Detail_Button = false;
            $scope.ShowDetailTestDriveHeadSection = true;
            $scope.PertanyaanMinatYa = false; //nanti aja kalo mandek
            $scope.PertanyaanMinatTidak = false; //nanti aja kalo mandek

            //yg ini aneh buat bikin status test drive
            $scope.HasilStatusTestDriveUnEditable = false; //ini disable lo bukan show

            //sub nya matiin semua
            $scope.ShowDetailTestDriveSubMenuFollowUp = false;
            $scope.ShowTestDriveRiwayatFollowUP = false;
            $scope.ShowDetailTestDriveBatal = false;
            $scope.ShowTestDriveSurvey = false;
            $scope.ArrayRiwayatFollowUP.length = 0; //cari cara buat clear so far kalo ditaro di bawah sini no problem
        };


        $scope.Go_To_Riwayat_Follow_Up = function(ScheduleId) {
            $scope.ArrayRiwayatFollowUP = [];
            $scope.Batal_Button = false;
            $scope.ShowDetailTestDriveSubMenu = false;
            $scope.ShowDetailTestDrive = true;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.Back_To_Test_Drive_Detail_Button = true;
            $scope.ShowTestDriveRiwayatFollowUP = true;


            //alert('ini uda jalan tapi id nya dummy');
            //$scope.ArrayRiwayatFollowUP=RiwayatFollowUpFactory.getData();
            RiwayatFollowUpFactory.getData(ScheduleId).then(function(res) {
                $scope.ArrayRiwayatFollowUP = res.data.Result;
                console.log("inin ini", $scope.ArrayRiwayatFollowUP);
                $scope.assignStatus($scope.ArrayRiwayatFollowUP);
                console.log("nah ini", $scope.ArrayRiwayatFollowUP);
                // if ($scope.ArrayRiwayatFollowUP.StatusFollowUpTestDriveName == "") {

                // }
                // $scope.StatusFollowUpTestDriveName = []
                //return $scope.ArrayRiwayatFollowUP;
            });
        };

        $scope.assignStatus = function(data) {
            for (var i in data) {
                if (data[i].StatusFollowUpTestDriveId == 1) {
                    data[i].StatusFollowUpTestDriveName = "Sukses di Follow Up";
                } else if (data[i].StatusFollowUpTestDriveId == 2) {
                    data[i].StatusFollowUpTestDriveName = "Gagal di Follow Up";
                } else {
                    data.splice(i, 1);
                }
            }
            return data;
        };

        // $scope.Go_To_Riwayat_Follow_Up = function (ScheduleId) {
        // 	$scope.ArrayRiwayatFollowUP = [];
        //    $scope.Batal_Button = false;
        //    $scope.ShowDetailTestDriveSubMenu=false;
        //    $scope.ShowDetailTestDrive=true;
        //    $scope.ShowDetailTestDriveHeadSection=false;
        //    $scope.Back_To_Test_Drive_Detail_Button=true;
        //    $scope.ShowTestDriveRiwayatFollowUP=true;


        // 	//alert('ini uda jalan tapi id nya dummy');
        // 	//$scope.ArrayRiwayatFollowUP=RiwayatFollowUpFactory.getData();
        // 	RiwayatFollowUpFactory.getData(ScheduleId).then(function(res){
        //         $scope.ArrayRiwayatFollowUP = res.data.Result;
        // 		for (var i in $scope.ArrayRiwayatFollowUP){
        // 			if($scope.ArrayRiwayatFollowUP[i].StatusFollowUpTestDriveId == 1){
        // 				$scope.ArrayRiwayatFollowUP[i].StatusFollowUPName = "Sukses di Follow Up";
        // 			} else {
        // 				$scope.ArrayRiwayatFollowUP[i].StatusFollowUPName = "Gagal di Follow Up";
        // 			}
        // 		}
        // 		// if($scope.ArrayRiwayatFollowUP.StatusFollowUpTestDriveName == ""){

        // 		// }
        // 		$scope.StatusFollowUpTestDriveName = [];
        //         //return $scope.ArrayRiwayatFollowUP;
        //     });

        // };

        $scope.Test_Drive_Batal = function(statusBatal) {
            $scope.StatusBatal = statusBatal;
            $scope.Batal_Button = false;
            $scope.ShowDetailTestDriveSubMenu = false;
            $scope.ShowDetailTestDrive = true;
            $scope.ShowDetailTestDriveHeadSection = false;
            $scope.Back_To_List_Test_Drive_Button = false;
            $scope.Back_To_Test_Drive_Detail_Button = true;
            $scope.ShowDetailTestDriveBatal = true;
        };

        $scope.Test_Drive_Batal_From_List = function(SelectedDataToDelete) {
            $scope.intended_operation = "batal-testdrive";
            $scope.Negtiation_UdaGantiJem = false;
            //alert(Id);//ini belom kepake nanti baru dipikir
            angular.element(".ui.modal.ModalMenungguResponAdmin").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalMenungguResponAdmin={show:false};
            angular.element(".ui.modal.ModalSiapTestDrive").modal({ observeChanges: true }).modal("refresh").modal("hide");
            //$scope.ModalSiapTestDrive={show:false};

            $scope.ShowDetailTestDrive = true;

            $scope.ShowDetailTestDriveSubMenu = false;
            $scope.ShowDetailTestDriveSubMenuFollowUp = false;
            $scope.Back_To_Test_Drive_Detail_Button = false; //harus
            $scope.ShowParameter = false;

            $scope.ShowDetailTestDriveHeadSection = false; //harus
            $scope.ShowTable = false;

            $scope.Submit_Button = false; //harus
            $scope.Add_Button = false;
            $scope.Batal_Button = false;

            $scope.ShowDetailTestDriveBatal = true; //harus
            $scope.Back_To_List_Test_Drive_Button = true;


        };

        $scope.Test_Drive_Survey = function(From_Where) {
            //$scope.BawaData = angular.copy(SelectedData);
            //console.log("asdhgsd", $scope.BawaData);
            $scope.breadcrums.title="Selesai Test Drive";
            $scope.intended_operation = "selesai-testdrive";
            $scope.Negtiation_UdaGantiJem = false;
            $scope.dateOptions.minDate = new Date();
            $scope.mNegotiation_Detail.TanggalTestDrive = angular.copy($scope.mNegotiation_jadwalBaru.StartDate);
            if (From_Where == "Not_From_List") {
                $scope.mNegotiation_Detail.TanggalTestDrive = angular.copy($scope.mNegotiation_jadwalBaru.StartDate);
                NegotiationFactory.getReason($scope.mNegotiation_Detail.ScheduleId).then(function(res) {
                    $scope.mNegotiation_Detail = res.data.Result[0];
                    console.log("soresore", $scope.mNegotiation_Detail);
                    if ($scope.mNegotiation_Detail.InterestedBit == true || $scope.mNegotiation_Detail.InterestedBit == false) {
                        console.log("kesinilah1")
                        $scope.mNegotiation_AlasanMembeli = [{ OutletId: null, ReasonId: null, ReasonBuyingName: null, ScheduleId: null, TestDriveReasonId: null }];
                        for (var i in $scope.mNegotiation_Detail.ListReason) {
                            $scope.mNegotiation_AlasanMembeli.push({
                                OutletId: $scope.mNegotiation_Detail.ListReason[i].OutletId,
                                ReasonId: $scope.mNegotiation_Detail.ListReason[i].ReasonId,
                                ReasonBuyingName: $scope.mNegotiation_Detail.ListReason[i].ReasonName,
                                ScheduleId: $scope.mNegotiation_Detail.ListReason[i].ScheduleId,
                                TestDriveReasonId: $scope.mNegotiation_Detail.ListReason[i].TestDriveReasonId,
                            })
                        }
                        console.log("okokoko", $scope.mNegotiation_AlasanMembeli);
                    } else {
                        console.log("kesinilah2")
                        $scope.mNegotiation_AlasanMembeli = { OutletId: null, ReasonId: null, ReasonCancleBuying: null, ScheduleId: null, TestDriveReasonId: null };
                        // $scope.mNegotiation_AlasanTidakMembeli.push({ OutletId: $scope.mNegotiation_Detail.ListReason[i].OutletId,
                        // 											 ReasonId: $scope.mNegotiation_Detail.ListReason[i].ReasonId,
                        // 											 ReasonCancleBuying: $scope.mNegotiation_Detail.ListReason[i].ReasonName,
                        // 											 ScheduleId: $scope.mNegotiation_Detail.ListReason[i].ScheduleId,
                        // 											 TestDriveReasonId: $scope.mNegotiation_Detail.ListReason[i].TestDriveReasonId,
                        // 											 })
                    }

                    $scope.Batal_Button = false;
                    $scope.ShowDetailTestDriveSubMenu = false;
                    $scope.ShowDetailTestDrive = true;
                    $scope.ShowDetailTestDriveHeadSection = false;
                    $scope.Back_To_Test_Drive_Detail_Button = true;
                    $scope.Back_To_List_Test_Drive_Button = false;
                    $scope.ShowTestDriveSurvey = true;
                })
            } else if (From_Where == "From_List") {
                console.log("kesinioy2", $scope.SelectedTestDrive);
                $scope.mNegotiation_Detail.TanggalTestDrive = angular.copy(new Date($scope.SelectedTestDrive.DateStart));
                console.log("kesinioy2", $scope.mNegotiation_Detail.TanggalTestDrive);
                angular.element(".ui.modal.ModalMenungguResponAdmin").modal({ observeChanges: true }).modal("refresh").modal("hide");
                angular.element(".ui.modal.ModalSiapTestDrive").modal({ observeChanges: true }).modal("refresh").modal("hide");
                //$scope.ModalMenungguResponAdmin={show:false};//harus
                //$scope.ModalSiapTestDrive={show:false};

                $scope.ShowTable = false;
                $scope.Add_Button = false;

                $scope.Batal_Button = false;
                $scope.ShowDetailTestDriveSubMenu = false;
                $scope.ShowDetailTestDrive = true;
                $scope.Back_To_Test_Drive_Detail_Button = false;
                $scope.ShowTestDriveSurvey = true;
                $scope.Back_To_List_Test_Drive_Button = false;
            }

            //$scope.optionsReasion = [{ reason: "Ya", value: true }, { reason: "Tidak", value: false }];
            //$scope.mNegotiation_NiatMembeli = $scope.options[0];
            $scope.PertanyaanMinatYa = false;
            $scope.PertanyaanMinatTidak = false;

            $scope.mNegotiation_AlasanMembeliCustom = "";
            $scope.mNegotiation_AlasanMembeli = [{ ProspectNameAlasan: null }];
            $scope.mNegotiation_AlasanMembeli.splice(0, 1);

            $scope.mNegotiation_AlasanTidakMembeliCustom = "";
            $scope.mNegotiation_AlasanTidakMembeli = [{ ProspectNameAlasan: null }];
            $scope.mNegotiation_AlasanTidakMembeli.splice(0, 1);

        };

        $scope.PertanyaanNiatMembeli = function() {

            $scope.mNegotiation_AlasanMembeliCustom = "";
            $scope.mNegotiation_AlasanMembeli = [{ ProspectNameAlasan: null }];
            $scope.mNegotiation_AlasanMembeli.splice(0, 1);

            $scope.mNegotiation_AlasanTidakMembeliCustom = "";
            $scope.mNegotiation_AlasanTidakMembeli = [{ ProspectNameAlasan: null }];
            $scope.mNegotiation_AlasanTidakMembeli.splice(0, 1);

            if ($scope.mNegotiation_Detail.InterestedBit == true) {
                $scope.PertanyaanMinatYa = true; //nanti aja kalo mandek
                $scope.PertanyaanMinatTidak = false; //nanti aja kalo mandek
            } else if ($scope.mNegotiation_Detail.InterestedBit == false) {
                $scope.PertanyaanMinatYa = false; //nanti aja kalo mandek
                $scope.PertanyaanMinatTidak = true; //nanti aja kalo mandek
            }
        };

        $scope.RemoveAlasanYa = function(IndexAlasan) {
            $scope.mNegotiation_AlasanMembeli.splice(IndexAlasan, 1);
        };

        $scope.AlasanYaClicked = function() {
            //$scope.mNegotiation_AlasanMembeli+=" "+Alasan;
            $scope.mNegotiation_AlasanMembeli = [];
            for (var i = 0; i < $scope.DaftarAlasanMembeli_ToRepeat.length; ++i) {
                if ($scope.DaftarAlasanMembeli_ToRepeat[i].Selected == true) {
                    $scope.mNegotiation_AlasanMembeli.push({
                        ReasonId: $scope.DaftarAlasanMembeli_ToRepeat[i].ReasonBuyingId,
                        ReasonBuyingName: $scope.DaftarAlasanMembeli_ToRepeat[i].ReasonBuyingName
                    });
                }
            }
            //$scope.ModalAlasanMembeli = { show: false };
            angular.element('.ui.modal.ModalAlasanMembeli').modal('hide');
        };

        $scope.NegotiationBatalPilihAlasanMembeli = function() {
            angular.element('.ui.modal.ModalAlasanMembeli').modal('hide');
        };

        $scope.NegotiationBatalPilihAlasanTidakMembeli = function() {
            angular.element('.ui.modal.ModalAlasanTidakMembeli').modal('hide');
        };

        $scope.RemoveAlasanTidak = function(IndexAlasan) {
            $scope.mNegotiation_AlasanTidakMembeli.splice(IndexAlasan, 1);
        };

        $scope.AlasanTidakClicked = function() {
            //$scope.mNegotiation_AlasanTidakMembeli+=" "+Alasan;
            $scope.mNegotiation_AlasanTidakMembeli = [];
            for (var i = 0; i < $scope.DaftarAlasanTidakMembeli_ToRepeat.length; ++i) {
                if ($scope.DaftarAlasanTidakMembeli_ToRepeat[i].Selected == true) {
                    $scope.mNegotiation_AlasanTidakMembeli.push({
                        ReasonId: $scope.DaftarAlasanTidakMembeli_ToRepeat[i].ReasonCancelBuyId,
                        ReasonCancelBuyName: $scope.DaftarAlasanTidakMembeli_ToRepeat[i].ReasonCancelBuyName
                    });
                }
            }
            //$scope.ModalAlasanTidakMembeli = { show: false };
            angular.element('.ui.modal.ModalAlasanTidakMembeli').modal('hide');
        };

        $scope.SimpanSurveyTestDrive = function() {
            if ($scope.mNegotiation_Detail.InterestedBit == true) {
                $scope.mNegotiation_Detail.ListReason = $scope.mNegotiation_AlasanMembeli;
            } else {
                $scope.mNegotiation_Detail.ListReason = $scope.mNegotiation_AlasanTidakMembeli;
            }
            NegotiationFactory.createSurveyYes($scope.mNegotiation_Detail).then(function(res) {
                    $scope.initialize();
                    $scope.ShowTable = true;
                    $scope.Add_Button = true;
                    $scope.ShowTestDriveSurvey = false;
					bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil tersimpan.",
											type: 'success'
										}
									);
                })
                //}else{
                //	$scope.mNegotiation_Detail.ListReasonCancelBuy = $scope.mNegotiation_AlasanTidakMembeli;
                //	NegotiationFactory.createSurveyNo($scope.mNegotiation_Detail).then(function (res){

            //	})
            //}

            console.log("testing", $scope.mNegotiation_Detail);

            //$scope.mNegotiation_AlasanMembeli+=" "+Alasan;
            //    $scope.mNegotiation_AlasanMembeli.push({ 'ProspectNameAlasan': $scope.mNegotiation_AlasanMembeliCustom });//nanti ini di post terus kalo post uda jangan lupa di clear
        };

        $scope.PilihAlasanMembeli = function() {
            //mNegotiation_NiatMembeli
            $scope.loadingAlasan = true;
            NegotiationFactory.getAlasanJadi().then(function(res) {
                $scope.DaftarAlasanMembeli_ToRepeat = res.data.Result;
                // console.log("$scope.DaftarAlasanMembeli_ToRepeat", $scope.DaftarAlasanMembeli_ToRepeat);
                // console.log("$scope.mNegotiation_AlasanMembeli", $scope.mNegotiation_AlasanMembeli);
                for (var i = 0; i < $scope.DaftarAlasanMembeli_ToRepeat.length; i++) {
                    for (var j = 0; j < $scope.mNegotiation_AlasanMembeli.length; j++) {
                        // console.log(i, j);
                        if ($scope.mNegotiation_AlasanMembeli[j].ReasonId == $scope.DaftarAlasanMembeli_ToRepeat[i].ReasonBuyingId) {
                            $scope.DaftarAlasanMembeli_ToRepeat[i].Selected = true;
                        }
                    }
                }
                //console.log("PilihAlasanMembeli", $scope.DaftarAlasanMembeli_ToRepeat);
                // $scope.ModalAlasanMembeli = { show: true };
				setTimeout(function() {
                angular.element('.ui.modal.ModalAlasanMembeli').modal('refresh');
            }, 0);
                angular.element('.ui.modal.ModalAlasanMembeli').modal('setting', { closable: false }).modal('show');
                angular.element('.ui.modal.ModalAlasanMembeli').not(':first').remove();
            })
            $scope.loadingAlasan = false;
        };

        $scope.PilihAlasanTidakMembeli = function() {
            $scope.loadingAlasan = true;
            NegotiationFactory.getAlasanBatal().then(function(res) {
                $scope.DaftarAlasanTidakMembeli_ToRepeat = res.data.Result;
                // console.log("$scope.DaftarAlasanTidakMembeli_ToRepeat", $scope.DaftarAlasanTidakMembeli_ToRepeat);
                // console.log("$scope.mNegotiation_AlasanTidakMembeli", $scope.mNegotiation_AlasanTidakMembeli);
                for (var i = 0; i < $scope.DaftarAlasanTidakMembeli_ToRepeat.length; i++) {
                    for (var j = 0; j < $scope.mNegotiation_AlasanTidakMembeli.length; j++) {
                        // console.log(i, j);
                        if ($scope.mNegotiation_AlasanTidakMembeli[j].ReasonId == $scope.DaftarAlasanTidakMembeli_ToRepeat[i].ReasonCancelBuyId) {
                            $scope.DaftarAlasanTidakMembeli_ToRepeat[i].Selected = true;
                        }
                    }
                }
                //$scope.ModalAlasanTidakMembeli = { show: true };
                angular.element('.ui.modal.ModalAlasanTidakMembeli').modal('setting', { closable: false }).modal('show');
                angular.element('.ui.modal.ModalAlasanTidakMembeli').not(':first').remove();
            })
            $scope.loadingAlasan = false;
        };

        $scope.BackToPermintaanTestDriveClicked = function() {
            $scope.ShowParameter = true;
            $scope.ShowJadwalTestDrive = false;
            $scope.Batal_Button = true;
            $scope.Submit_Button = true;
        };

        $scope.Go_To_Jadwal_Test_Drive = function() {
            $scope.date = new Date();
            $scope.dateOptionsFrom.minDate = $scope.date;

            $scope.ShowNegotiationIncrementJamMulai = true;
            $scope.ShowNegotiationDecrementJamMulai = true;
            $scope.ShowNegotiationIncrementJamAkhir = true;
            $scope.ShowNegotiationDecrementJamAkhir = true;


            $scope.mNegotiationUbah = angular.copy($scope.mNegotiation);

            $scope.pilihTestDrive = {
                "VehicleTypeId": $scope.mNegotiationUbah.VehicleTypeId,
                "DateTestDrive": $scope.mNegotiationUbah.DateStart,
                "EndDate": $scope.mNegotiationUbah.DateEnd
            };

            // $scope.pilihTestDrive.OutletId = $scope.mNegotiationUbah.OutletOwnerId;//tadinya OutletId
            // $scope.pilihTestDrive.TestDriveUnitId = $scope.mNegotiationUbah.TestDriveUnitId;
            // $scope.pilihTestDrive.StartDate = $scope.mNegotiationUbah.DateStart;
            // $scope.pilihTestDrive.EndDate = $scope.mNegotiationUbah.DateEnd;

            if ($scope.intended_operation == "Update_Ops") {
                console.log("asas", $scope.pilihTestDrive);
                NegotiationFactory.getTestDrive($scope.pilihTestDrive).then(function(res) {
                    $scope.outletdetail = res.data.Result;
                    //console.log("......", $scope.outletdetail);
                    //$scope.dataJam($scope.outletdetail);
                    if ($scope.outletdetail.length != 0) {
                        console.log("masuk1");
                        console.log("test", $scope.mNegotiationUbah)
                            // $scope.pilihTestDrive.StartDate = new Date();
                            // $scope.pilihTestDrive.EndDate = new Date();
                        $scope.pilihTestDrive.OutletId = $scope.mNegotiationUbah.OutletOwnerId; //tadinya OutletId
                        $scope.pilihTestDrive.TestDriveUnitId = $scope.mNegotiationUbah.TestDriveUnitId;
                        $scope.pilihTestDrive.StartDate = $scope.mNegotiationUbah.DateStart;
                        $scope.pilihTestDrive.EndDate = $scope.mNegotiationUbah.DateEnd;

                        // if($scope.Negtiation_UdaGantiJem == false)
                        // {
                        // console.log("setan1",$scope.mNegotiationUbah);
                        // $scope.pilihTestDrive.StartDate = $scope.mNegotiationUbah.DateStart;
                        // $scope.pilihTestDrive.EndDate = $scope.mNegotiationUbah.DateEnd;
                        // }
                        // else if($scope.Negtiation_UdaGantiJem == true)
                        // {
                        // // console.log("setan2",$scope.NegotiationDaChengedStartDate);
                        // // //$scope.pilihTestDrive.StartDate = angular.copy($scope.NegotiationDaChengedStartDate);
                        // // $scope.pilihTestDrive.StartDate.setHours($scope.NegotiationDaChengedStartDate.getHours());
                        // // $scope.pilihTestDrive.StartDate=angular.copy($scope.pilihTestDrive.StartDate);

                        // // $scope.pilihTestDrive.EndDate = angular.copy($scope.NegotiationDaChengedEndDate);
                        // }


                        $scope.tempStart = angular.copy($scope.pilihTestDrive.StartDate);
                        $scope.tempEnd = angular.copy($scope.pilihTestDrive.EndDate);
                        $scope.isList = true;
                        $scope.isChart = true;
                    }
                    console.log("kesinidong", $scope.pilihTestDrive);
                })
            }



            $scope.ShowParameter = false;
            $scope.ShowJadwalTestDrive = true;
            $scope.Batal_Button = false;
            $scope.Submit_Button = false;
            console.log("===> end function  Go_To_Jadwal_Test_Drive");
        };

        $scope.NegotiationIncrementStartDateTime = function() {
            if ($scope.pilihTestDrive.StartDate.getHours() < 22) {
                $scope.pilihTestDrive.StartDate.setHours($scope.pilihTestDrive.StartDate.getHours() + 1);
                $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.StartDate);
                $scope.changed($scope.pilihTestDrive.StartDate);
            } else if ($scope.pilihTestDrive.StartDate.getHours() >= 22) {
                $scope.ShowNegotiationIncrementJamMulai = false;

                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda melebihi batas waktu yang ditentukan.",
                    type: 'warning'
                });
            }

        }

        $scope.NegotiationDecrementStartDateTime = function() {
            if ($scope.pilihTestDrive.StartDate.getHours() > 8) {
                $scope.pilihTestDrive.StartDate.setHours($scope.pilihTestDrive.StartDate.getHours() - 1);
                $scope.pilihTestDrive.StartDate = angular.copy($scope.pilihTestDrive.StartDate);
                $scope.changed($scope.pilihTestDrive.StartDate);
            } else if ($scope.pilihTestDrive.StartDate.getHours() <= 8) {
                $scope.ShowNegotiationDecrementJamMulai = false;
                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda melebihi batas waktu yang ditentukan.",
                    type: 'warning'
                });
            }

        }

        $scope.NegotiationIncrementEndDateTime = function() {
            if ($scope.pilihTestDrive.EndDate.getHours() < 22) {
                $scope.pilihTestDrive.EndDate.setHours($scope.pilihTestDrive.EndDate.getHours() + 1);
                $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.EndDate);
            } else if ($scope.pilihTestDrive.EndDate.getHours() >= 22) {
                $scope.ShowNegotiationIncrementJamAkhir = false;
                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda melebihi batas waktu yang ditentukan.",
                    type: 'warning'
                });
            }

        }

        $scope.NegotiationDecrementEndDateTime = function() {
            if ($scope.pilihTestDrive.EndDate.getHours() > 8) {
                $scope.pilihTestDrive.EndDate.setHours($scope.pilihTestDrive.EndDate.getHours() - 1);
                $scope.pilihTestDrive.EndDate = angular.copy($scope.pilihTestDrive.EndDate);
            } else if ($scope.pilihTestDrive.DateEndBaru.getHours() <= 8) {
                $scope.ShowNegotiationDecrementJamAkhir = false;
                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda melebihi batas waktu yang ditentukan.",
                    type: 'warning'
                });
            }

        }

        $scope.toggleChecked = function(data) {
            if (data.Selected) {
                data.Selected = false;
                // var index = $scope.selectedItems.indexOf(data);
                // $scope.selectedItems.splice(index, 1);
            } else {
                data.Selected = true;
                // $scope.selectedItems.push(data);
            }
        };


    });