angular.module('app')
  .factory('ApprovalDiskonNegotiationFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		//var res=$http.get('/api/sales/NegotiationApprovalDiscount');
		var res=$http.get('/api/sales/NegotiationApprovalDiscount?start=1&limit=5');
		return res;
        
      },
	  getDataIncrement: function(Start,Limit) {
		//var res=$http.get('/api/sales/NegotiationApprovalDiscount');
		var res=$http.get('/api/sales/NegotiationApprovalDiscount?start='+Start+'&limit='+Limit);
		return res;
        
      },
	  getBookingUnitSpk: function(BookingId){
        var res = $http.get('/api/sales/BookingUnitForSPK?start=1&limit=1000&BookingUnitId='+BookingId);
        return res;
      },
	  
	  getDataSearchApprovalDiskonNegotiationIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/NegotiationApprovalDiscount/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  getDataIncrementLimit: function () {
        
        var da_json = [
          { SearchOptionId: 5, SearchOptionName: "5" },
		  { SearchOptionId: 10, SearchOptionName: "10" },
		  { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetApprovalDiskonNegotiationSearchDropdownOptions: function () {
        
        var da_json = [
		  { SearchOptionId: "ProspectName", SearchOptionName: "Nama pelanggan" },
		  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Model tipe" },
		  { SearchOptionId: "Salesman", SearchOptionName: "Sales" },
		  { SearchOptionId: "HeaderStatusApprovalName", SearchOptionName: "Status" },
		  { SearchOptionId: "RequestDate", SearchOptionName: "Tanggal pengajuan" }
        ];

        var res=da_json

        return res;
      },
	  
	  getDataSearchApprovalDiskonNegotiation: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/NegotiationApprovalDiscount/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  SetujuApprovalDiskonNegotiation: function(ApprovalDiskonNegotiation){
		return $http.put('/api/sales/NegotiationApprovalDiscount/Approve', [ApprovalDiskonNegotiation]);									
      },
	  
	  SetujuApprovalDiskonNegotiationDariList: function(ApprovalDiskonNegotiation){
		return $http.put('/api/sales/NegotiationApprovalDiscount/Approve', ApprovalDiskonNegotiation);									
      },
	  
	  TidakSetujuApprovalDiskonNegotiation: function(ApprovalDiskonNegotiation){
        // return $http.put('/api/sales/NegotiationApprovalDiscount/Reject', [{
                                            // OutletId: ApprovalDiskonNegotiation.OutletId,
											// ApprovalDECId: ApprovalDiskonNegotiation.ApprovalDECId,
											// DECId: ApprovalDiskonNegotiation.DECId,
                                            // RejectReason: ApprovalDiskonNegotiation.RejectReason}]);
		return $http.put('/api/sales/NegotiationApprovalDiscount/Reject', [ApprovalDiskonNegotiation]);									
      },
	  TidakSetujuApprovalDiskonNegotiationDariList: function(ApprovalDiskonNegotiation){
        // return $http.put('/api/sales/NegotiationApprovalDiscount/Reject', [{
                                            // OutletId: ApprovalDiskonNegotiation.OutletId,
											// ApprovalDECId: ApprovalDiskonNegotiation.ApprovalDECId,
											// DECId: ApprovalDiskonNegotiation.DECId,
                                            // RejectReason: ApprovalDiskonNegotiation.RejectReason}]);
		return $http.put('/api/sales/NegotiationApprovalDiscount/Reject', ApprovalDiskonNegotiation);									
      },
	  
      create: function(ApprovalDiskonNegotiation) {
        return $http.post('/api/sales/NegotiationApprovalDiscount', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalDiskonNegotiation.SalesProgramName}]);
      },
      update: function(ApprovalDiskonNegotiation){
        return $http.put('/api/sales/NegotiationApprovalDiscount', [{
                                            SalesProgramId: ApprovalDiskonNegotiation.SalesProgramId,
                                            SalesProgramName: ApprovalDiskonNegotiation.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/NegotiationApprovalDiscount',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd