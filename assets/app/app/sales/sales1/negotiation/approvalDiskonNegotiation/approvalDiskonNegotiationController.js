angular.module('app')
.controller('ApprovalDiskonNegotiationController', function($scope, $http, CurrentUser, ApprovalDiskonNegotiationFactory,$timeout,bsNotify) {
	$scope.ApprovalDiskonNegotiationMain=true;
	IfGotProblemWithModal();
	$scope.ApprovalDiskonNegotiationMultipleReject=false;
	$scope.ApprovalDiskonNegotiationSearchCriteriaStore="";
	$scope.ApprovalDiskonNegotiationSearchValueStore="";
	
	$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval_PanelIcon = "+";
	$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga_PanelIcon = "+";
	$scope.ApprovalDiskonNegotiationHideIncrement=false;
	$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.optionApprovalDiskonNegotiationSearchCriteria = ApprovalDiskonNegotiationFactory.GetApprovalDiskonNegotiationSearchDropdownOptions();
        $scope.optionApprovalDiskonNegotiationSearchLimit = ApprovalDiskonNegotiationFactory.getDataIncrementLimit();
		$scope.ApprovalDiskonNegotiationSearchLimit = $scope.optionApprovalDiskonNegotiationSearchLimit[0].SearchOptionId; 
		$scope.ApprovalDiskonNegotiationSearchCriteria = $scope.optionApprovalDiskonNegotiationSearchCriteria[0].SearchOptionId;
		//$scope.mApprovalDiskonNegotiation = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        ApprovalDiskonNegotiationFactory.getData().then(
			function(res){
				
				$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
				$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
				
				for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
				{
					for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
					{
						if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
						{
							$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
							$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
						}	
					}
				}
				if(res.data.Total<=5)
				{
					$scope.ApprovalDiskonNegotiationHideIncrement=true;
				}
				return res.data;
			},
			function(err){
				bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
			}
		);
		
		$scope.ApprovalDiskonNegotiationRefresh = function () {
			$scope.ApprovalDiskonNegotiationHideIncrement=false;
			ApprovalDiskonNegotiationFactory.getData().then(
				function(res){
					
					$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
					$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
				
					for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
					{
						for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
						{
							if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
							{
								$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
								$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
							}	
						}
					}
					$scope.ApprovalDiskonNegotiationSearchValue="";
					$scope.ResetSelection();
					if(res.data.Total<=5)
					{
						$scope.ApprovalDiskonNegotiationHideIncrement=true;
					}
					return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		
		$scope.ApprovalDiskonNegotiationIncrementLimit = function () {
			$scope.ApprovalDiskonNegotiationHideIncrement=false;
			if($scope.ApprovalDiskonNegotiationSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalDiskonNegotiationGabungan.length)+1;
				ApprovalDiskonNegotiationFactory.getDataIncrement(masukin,$scope.ApprovalDiskonNegotiationSearchLimit).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDiskonNegotiationGabungan.push(res.data.Result[i]);
						}
						
						for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
							{
								if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
								}	
							}
						}
						var ApprovalDiskonNegotiationDaIncrementBablas=masukin+$scope.ApprovalDiskonNegotiationSearchLimit;
						if(ApprovalDiskonNegotiationDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalDiskonNegotiationHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						bsNotify.show(
							{
								title: "gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);
			}
			if($scope.ApprovalDiskonNegotiationSearchIsPressed==true)
			{
				var masukin=angular.copy($scope.ApprovalDiskonNegotiationGabungan.length)+1;
				ApprovalDiskonNegotiationFactory.getDataSearchApprovalDiskonNegotiationIncrement(masukin,$scope.ApprovalDiskonNegotiationSearchLimit,$scope.ApprovalDiskonNegotiationSearchCriteriaStore,$scope.ApprovalDiskonNegotiationSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDiskonNegotiationGabungan.push(res.data.Result[i]);
						}
						
						for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
							{
								if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
								}	
							}
						}
						
						var ApprovalDiskonNegotiationDaIncrementBablas=masukin+$scope.ApprovalDiskonNegotiationSearchLimit;
						if(ApprovalDiskonNegotiationDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalDiskonNegotiationHideIncrement=true;
						}

						return res.data;
					},
					function(err){
						bsNotify.show(
							{
								title: "gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);
			}
			
			
			

		};
		
		$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval_Clicked = function () {
			$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval = !$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval;
			if ($scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval == true) {
				$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval == false) {
				$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_HistoryApproval_PanelIcon = "+";
			}

		};
		
		$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga_Clicked = function () {
			$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga = !$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga;
			if ($scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga == true) {
				$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga == false) {
				$scope.Show_Hide_ApprovalDiskonNegotiation_Detail_DataHarga_PanelIcon = "+";
			}

		};
		
		$scope.FromListSetujuApprovalDiskonNegotiation = function () {
			ApprovalDiskonNegotiationFactory.SetujuApprovalDiskonNegotiationDariList($scope.selectionApprovalDiskonNegotiation).then(function () {
						ApprovalDiskonNegotiationFactory.getData().then(
							function(res){
								
								bsNotify.show(
									{
										title: "Message",
										content: "Diskon disetujui",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
								$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
									{
										if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
										}	
									}
								}
								
								$scope.loading=false;
								$scope.BatalApprovalDiskonNegotiation();
								$scope.ResetSelection();
								$scope.ApprovalDiskonNegotiationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonNegotiationHideIncrement=true;
								}
								$scope.ApprovalDiskonNegotiationMultipleReject=false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					});
		};
		
		$scope.FromListTidakSetujuApprovalDiskonNegotiation = function () {
			$scope.ApprovalDiskonNegotiationMultipleReject=true;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonNegotiation').modal('setting',{closable:false}).modal('show');
		};
        
		$scope.TidakSetujuApprovalDiskonNegotiation = function () {
			$scope.ApprovalDiskonNegotiationMultipleReject=false;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonNegotiation').modal('setting',{closable:false}).modal('show');
		};
		
		$scope.ActionApprovalDiskonNegotiation = function (SelectedData) {
			$scope.da_previous_sequenceApprovalDiskonNegotiation=-1;
			$scope.ApprovalDiskonNegotiation_Jegal=false;
			
			for(var i = 0; i < SelectedData.ListOfNegotiationApprovalDiscountView.length; ++i)
			{
				if(SelectedData.ListOfNegotiationApprovalDiscountView[i].ApproverRoleName==$scope.user.RoleName)
				{
					$scope.ApprovalDiskonNegotiation_Jegal=false;
				}
			}
			
			for(var i = 0; i < SelectedData.ListOfNegotiationApprovalDiscountView.length; ++i)
			{
				if(SelectedData.ListOfNegotiationApprovalDiscountView[i].ApproverRoleName==$scope.user.RoleName && SelectedData.ListOfNegotiationApprovalDiscountView[i].StatusApprovalName !="Diajukan")
				{
					$scope.ApprovalDiskonNegotiation_Jegal=true;
				}
				
				if(SelectedData.ListOfNegotiationApprovalDiscountView[i].ApproverRoleName==$scope.user.RoleName)
				{
					
					$scope.da_previous_sequenceApprovalDiskonNegotiation=angular.copy(SelectedData.ListOfNegotiationApprovalDiscountView[i].sequence)-1;
				}
			}
			
			for(var i = 0; i < SelectedData.ListOfNegotiationApprovalDiscountView.length; ++i)
			{
				if(SelectedData.ListOfNegotiationApprovalDiscountView[i].sequence==$scope.da_previous_sequenceApprovalDiskonNegotiation )
				{
					if(SelectedData.ListOfNegotiationApprovalDiscountView[i].StatusApprovalName !="Disetujui")
					{
						$scope.ApprovalDiskonNegotiation_Jegal=true;
					}
					
				}
			}
			
			$scope.ApprovalDiskonNegotiationMultipleReject=false;
			$scope.mApprovalDiskonNegotiation=angular.copy(SelectedData);
			angular.element('.ui.modal.ModalActionApprovalDiskonNegotiation').modal('setting',{closable:false}).modal('show');
		}
		
		$scope.BatalActionApprovalDiskonNegotiation = function () {
			angular.element('.ui.modal.ModalActionApprovalDiskonNegotiation').modal('hide');
			$scope.mApprovalDiskonNegotiation={};
		}
		
		$scope.SelectApprovalDiskonNegotiation = function () {
			
			$scope.ApprovalDiskonNegotiationMain=false;
			$scope.ApprovalDiskonNegotiationDetail=true;
			angular.element('.ui.modal.ModalActionApprovalDiskonNegotiation').modal('hide');
			
			ApprovalDiskonNegotiationFactory.getBookingUnitSpk($scope.mApprovalDiskonNegotiation.BookingUnitId).then(function (res) {
				$scope.BookingUnit = res.data.Result[0];
				$scope.bookingUnitSPK();
			});
			
			
		}
		
		$scope.bookingUnitSPK = function() {
            $scope.selected_data={};
            $scope.selected_data.SpkDate = new Date();
            if ($scope.BookingUnit.CustomerTypeId == 1) {
                $scope.selected_data.CustomerTypeId = 3;
            }

            $scope.selected_data.CustomerId = $scope.BookingUnit.CustomerId;
            $scope.selected_data.ProspectId = $scope.BookingUnit.ProspectId;
            $scope.selected_data.ProspectCode = $scope.BookingUnit.ProspectCode;
            $scope.selected_data.SalesId = $scope.BookingUnit.SalesId;
            $scope.selected_data.ToyotaId = $scope.BookingUnit.ToyotaId;

            if ($scope.BookingUnit.OffTheRoad == false) {
                $scope.selected_data.OnOffTheRoadId = 1;
            } else {
                $scope.selected_data.OnOffTheRoadId = 2;
            }

            if ($scope.BookingUnit.DirectDeliveryPDC == false) {
                $scope.selected_data.DeliveryCategoryId = 2;
            } else {
                $scope.selected_data.DeliveryCategoryId = 1;
            }

            if ($scope.BookingUnit.OffTheRoad == false) {
                $scope.selected_data.OnOffTheRoadId = 2;
            } else {
                $scope.selected_data.OnOffTheRoadId = 1;
            }

            $scope.selected_data.BookingFee = $scope.BookingUnit.BookingFee || 0.0;
            $scope.selected_data.GrandTotal = $scope.BookingUnit.GrandTotal || 0.0;
            $scope.selected_data.Discount = $scope.BookingUnit.RequestDiscount || 0.0;
            $scope.selected_data.VehiclePrice = $scope.BookingUnit.VehiclePrice || 0.0;

            $scope.selected_data.TDP_DP = 0;
            $scope.selected_data.DiscountSubsidiDP = $scope.BookingUnit.DiscountSubsidiDP || 0.0;
            $scope.selected_data.DiscountSubsidiRate = $scope.BookingUnit.DiscountSubsidiRate || 0.0;
            $scope.selected_data.MediatorCommision = $scope.BookingUnit.MediatorCommision || 0.0;
            $scope.selected_data.BBNAdjustment = $scope.BookingUnit.BBNAdjustment || 0.0;
            $scope.selected_data.BBNServiceAdjustment = $scope.BookingUnit.BBNServiceAdjustment || 0.0;
            $scope.selected_data.PriceAfterDiscount = $scope.BookingUnit.PriceAfterDiscount || 0.0;
            $scope.selected_data.DPP = $scope.BookingUnit.DPP || 0.0;
            $scope.selected_data.PPN = $scope.BookingUnit.PPN || 0.0;
            $scope.selected_data.BBN = $scope.BookingUnit.BBN || 0.0;
            $scope.selected_data.TotalBBN = $scope.BookingUnit.TotalBBN || 0.0;
            $scope.selected_data.PPH22 = $scope.BookingUnit.PPH22 || 0.0;
            $scope.selected_data.TotalInclVAT = $scope.BookingUnit.TotalInclVAT || 0.0;

            $scope.selected_data.KaroseriId = $scope.BookingUnit.KaroseriId;
            $scope.selected_data.KaroseriName = $scope.BookingUnit.KaroseriName;
            $scope.selected_data.KaroseriPrice = $scope.BookingUnit.KaroseriPrice;
            $scope.selected_data.KaroseriPPN = $scope.BookingUnit.PPNKaroseri;
            $scope.selected_data.KaroseriTotalInclVAT = $scope.BookingUnit.TotalInclVATKaroseri;
            $scope.selected_data.LeasingId = $scope.BookingUnit.LeasingId;
            $scope.selected_data.LeasingLeasingTenorId = $scope.BookingUnit.LeasingLeasingTenorId;

            $scope.selected_data.TradeInBit = $scope.BookingUnit.TradeInBit;
            $scope.selected_data.FundSourceId = $scope.BookingUnit.FundSourceId;

            if (angular.isUndefined($scope.BookingUnit.DPP) == true) {
                $scope.selected_data.DPP = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.PPH22) == true) {
                $scope.selected_data.PPH22 = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.PPN) == true) {
                $scope.selected_data.PPN = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.TotalBBN) == true) {
                $scope.selected_data.TotalBBN = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.TotalInclVAT) == true) {
                $scope.selected_data.TotalInclVAT = 0.0;
            }

            $scope.selected_data.EstimateDate = $scope.BookingUnit.SentUnitDateEstimateDate;
            $scope.selected_data.SentUnitDate = $scope.BookingUnit.SentUnitDate;
            $scope.selected_data.StatusPDDId = $scope.BookingUnit.StatusPDDId;

			$scope.selected_data.ListInfoPelanggan=[];
			
            $scope.selected_data.ListInfoPelanggan[0] = {
                CustomerCorrespondentId: 1,
                CustomerName: $scope.BookingUnit.ProspectName,
                CustomerAddress: $scope.BookingUnit.Address,
                ProvinceId: $scope.BookingUnit.ProvinceId,
                CityRegencyId: $scope.BookingUnit.CityRegencyId,
                DistrictId: $scope.BookingUnit.DistrictId,
                VillageId: $scope.BookingUnit.VillageId,
                PostalCode: $scope.BookingUnit.PostalCode,
                RTRW: $scope.BookingUnit.RTRW,
                HpNumber: $scope.BookingUnit.HP,
                Email: $scope.BookingUnit.Email,
                PhoneNumber: $scope.BookingUnit.PhoneNumber,
                NickName: $scope.BookingUnit.NickName,
                BirthDate: $scope.BookingUnit.Birthdate,
                Job: $scope.BookingUnit.WorkTitle,
                CustomerPositionId: $scope.BookingUnit.CustomerPositionId,
                CustomerReligionId: $scope.BookingUnit.CustomerReligionId,
                OfficeAddress: $scope.BookingUnit.OfficeAddress,
                OfficePhone: $scope.BookingUnit.OfficePhone,
                InstanceName: $scope.BookingUnit.InstanceName
            }

            if ($scope.selected_data.CustomerTypeId == 3) {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            } else {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 2 });
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            }

			$scope.selected_data.ListInfoUnit=[];
            
			try
			{
				$scope.selected_data.ListInfoUnit[0] = {
					"VehicleTypeId": $scope.BookingUnit.VehicleTypeId,
					"Description": $scope.BookingUnit.Description,
					"VehicleModelId": $scope.BookingUnit.VehicleModelId,
					"Qty": $scope.BookingUnit.Qty,
					"ColorId": $scope.BookingUnit.ColorId,
					"ColorName": $scope.BookingUnit.ColorName,
					"ProductionYear": $scope.BookingUnit.ListDetailBookingUnit[0].AssemblyYear,
					"ListDetailUnit": []
				};
			}
			catch(ExceptionComment)
			{
				$scope.selected_data.ListInfoUnit[0] = {
					"VehicleTypeId": $scope.BookingUnit.VehicleTypeId,
					"Description": $scope.BookingUnit.Description,
					"VehicleModelId": $scope.BookingUnit.VehicleModelId,
					"Qty": $scope.BookingUnit.Qty,
					"ColorId": $scope.BookingUnit.ColorId,
					"ColorName": $scope.BookingUnit.ColorName,
					"ProductionYear": null,
					"ListDetailUnit": []
				};
			}
            
			
            for (var i in $scope.BookingUnit.ListDetailBookingUnit) {
                $scope.selected_data.ListInfoUnit[0].ListDetailUnit.push({
                    VehicleTypeId: $scope.BookingUnit.ListDetailBookingUnit[i].VehicleTypeId,
                    Description: $scope.BookingUnit.Description,
                    VehicleModelId: $scope.BookingUnit.VehicleModelId,
                    ColorId: $scope.BookingUnit.ColorId,
                    ColorName: $scope.BookingUnit.ColorName,
                    RRNNo: $scope.BookingUnit.ListDetailBookingUnit[i].RRNNo,
                    FrameNo: $scope.BookingUnit.ListDetailBookingUnit[i].FrameNo,
                    SpecialReqNumberPlate: $scope.BookingUnit.ListDetailBookingUnit[i].PoliceNo,
                    GrandTotalAcc: 0,
                    DPP: $scope.BookingUnit.ListDetailBookingUnit[i].DPP,
                    ListAccessories: $scope.BookingUnit.ListDetailBookingUnit[i].ListDetailAccBookingUnit,
                    ListAccessoriesPackage: $scope.BookingUnit.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage
                });

            }

            for (var i in $scope.selected_data.ListInfoUnit[0].ListDetailUnit) {
                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage.length > 0) {
                    for (var j in $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc =
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc +
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage[j].TotalInclVAT;
                    };
                }

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories.length > 0) {
                    for (var j in $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc =
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc +
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories[j].TotalInclVAT;
                    };
                }

            }

        };
		
		$scope.BatalApprovalDiskonNegotiation = function () {
			$scope.mApprovalDiskonNegotiation ={};
			$scope.ApprovalDiskonNegotiationMain=true;
			$scope.ApprovalDiskonNegotiationDetail=false;
			angular.element('.ui.modal.ModalActionApprovalDiskonNegotiation').modal('hide');
		}
		
		$scope.BatalAlasanApprovalDiskonNegotiation = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonNegotiation').modal('hide');
		}
		
		$scope.SetujuApprovalDiskonNegotiation = function () {
			ApprovalDiskonNegotiationFactory.SetujuApprovalDiskonNegotiation($scope.mApprovalDiskonNegotiation).then(function () {
					ApprovalDiskonNegotiationFactory.getData().then(
						function(res){
							
							bsNotify.show(
									{
										title: "Message",
										content: "Diskon disetujui",
										type: 'success'
									}
								);
							
							$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
							$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
							
							for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
							{
								for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
								{
									if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
									{
										$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
										$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
									}	
								}
							}
							
							$scope.loading=false;
							$scope.BatalApprovalDiskonNegotiation();
							$scope.ResetSelection();
							$scope.ApprovalDiskonNegotiationHideIncrement=false;
							if(res.data.Total<=5)
							{
								$scope.ApprovalDiskonNegotiationHideIncrement=true;
							}
							$scope.ApprovalDiskonNegotiationMultipleReject=false;
							
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);
				});

		};
		
		$scope.SubmitAlasanApprovalDiskonNegotiation = function () {
			if($scope.ApprovalDiskonNegotiationMultipleReject==false)
			{
				for(var x = 0; x < $scope.mApprovalDiskonNegotiation['ListOfNegotiationApprovalDiscountView'].length; ++x)
				{
					$scope.mApprovalDiskonNegotiation['ListOfNegotiationApprovalDiscountView'][x]['RejectReason']=angular.copy($scope.mApprovalDiskonNegotiation.RejectReason);
				}
				ApprovalDiskonNegotiationFactory.TidakSetujuApprovalDiskonNegotiation($scope.mApprovalDiskonNegotiation).then(function () {
						ApprovalDiskonNegotiationFactory.getData().then(
							function(res){
								
								bsNotify.show(
									{
										title: "Message",
										content: "Data Di Reject",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
								$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
									{
										if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
										}	
									}
								}
								
								$scope.loading=false;
								$scope.BatalAlasanApprovalDiskonNegotiation();
								$scope.BatalApprovalDiskonNegotiation();
								$scope.ResetSelection();
								$scope.ApprovalDiskonNegotiationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonNegotiationHideIncrement=true;
								}
								$scope.ApprovalDiskonNegotiationMultipleReject=false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					});
			}
			else if($scope.ApprovalDiskonNegotiationMultipleReject==true)
			{
				for(var i = 0; i < $scope.selectionApprovalDiskonNegotiation.length; ++i)
				{
					for(var x = 0; x < $scope.selectionApprovalDiskonNegotiation[i]['ListOfNegotiationApprovalDiscountView'].length; ++x)
					{
						$scope.selectionApprovalDiskonNegotiation[i]['ListOfNegotiationApprovalDiscountView'][x]['RejectReason']=angular.copy($scope.mApprovalDiskonNegotiation.RejectReason);
					}
				}
				ApprovalDiskonNegotiationFactory.TidakSetujuApprovalDiskonNegotiationDariList($scope.selectionApprovalDiskonNegotiation).then(function () {
						ApprovalDiskonNegotiationFactory.getData().then(
							function(res){
								
								bsNotify.show(
									{
										title: "Message",
										content: "Data Di Reject",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
								$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
									{
										if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
											$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
										}	
									}
								}
								
								$scope.loading=false;
								$scope.BatalAlasanApprovalDiskonNegotiation();
								$scope.BatalApprovalDiskonNegotiation();
								$scope.ResetSelection();
								$scope.ApprovalDiskonNegotiationHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonNegotiationHideIncrement=true;
								}
								$scope.ApprovalDiskonNegotiationMultipleReject=false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "gagal",
										content: "Data gagal ditolak.",
										type: 'danger'
									}
								);
							}
						);
					});
			}
		
			
		}

		$scope.ResetSelection = function () {
			$scope.selectionApprovalDiskonNegotiation=[{ OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationSendingId: null}];
			$scope.selectionApprovalDiskonNegotiation.splice(0, 1);
		}
		
		$scope.selectionApprovalDiskonNegotiation=[{ OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalQuotationSendingId: null}];
		$scope.selectionApprovalDiskonNegotiation.splice(0, 1);
		
		$scope.toggleSelection = function toggleSelection(SelectedAprovalDiskonNegotiationFromList) {
			var idx = $scope.selectionApprovalDiskonNegotiation.indexOf(SelectedAprovalDiskonNegotiationFromList);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionApprovalDiskonNegotiation.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionApprovalDiskonNegotiation.push(SelectedAprovalDiskonNegotiationFromList);
			}
			
			$scope.SelectionAman_ApprovalDiskonNegotiation=true;
			$scope.da_previous_sequenceApprovalDiskonNegotiation=-1;
			
			
			for(var i = 0; i < $scope.selectionApprovalDiskonNegotiation.length; ++i)
			{
				if($scope.selectionApprovalDiskonNegotiation[i].HeaderStatusApprovalName!="Diajukan")
				{
					$scope.SelectionAman_ApprovalDiskonNegotiation=false;
				}
				
				for(var x = 0; x < $scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView.length; ++x)
				{
					if($scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName==$scope.user.RoleName && $scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName !="Diajukan")
					{
						$scope.SelectionAman_ApprovalDiskonNegotiation=false;
					}
					
					if($scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName==$scope.user.RoleName)
					{
						
						$scope.da_previous_sequenceApprovalDiskonNegotiation=angular.copy($scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].sequence)-1;
					}
				}
				
				for(var x = 0; x < $scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView.length; ++x)
				{
					if($scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].sequence==$scope.da_previous_sequenceApprovalDiskonNegotiation )
					{
						if($scope.selectionApprovalDiskonNegotiation[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName !="Disetujui")
						{
							$scope.SelectionAman_ApprovalDiskonNegotiation=false;
						}
						
					}
				}
			}
			
		  };
		  
		$scope.SearchApprovalDiskonNegotiation = function () {
			$scope.ApprovalDiskonNegotiationHideIncrement=false;
			try
			{
				if($scope.ApprovalDiskonNegotiationSearchValue!="" && (typeof $scope.ApprovalDiskonNegotiationSearchValue!="undefined"))
				{
					ApprovalDiskonNegotiationFactory.getDataSearchApprovalDiskonNegotiation($scope.ApprovalDiskonNegotiationSearchLimit,$scope.ApprovalDiskonNegotiationSearchCriteria,$scope.ApprovalDiskonNegotiationSearchValue).then(function (res) {
						
						$scope.ApprovalDiskonNegotiationSearchIsPressed=true;
						$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
						
						for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
							{
								if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
								}	
							}
						}
						
						if($scope.ApprovalDiskonNegotiationSearchLimit>=res.data.Total)
						{
							$scope.ApprovalDiskonNegotiationHideIncrement=true;
						}
						
						$scope.ResetSelection();
						$scope.ApprovalDiskonNegotiationSearchCriteriaStore=angular.copy($scope.ApprovalDiskonNegotiationSearchCriteria);
						$scope.ApprovalDiskonNegotiationSearchValueStore=angular.copy($scope.ApprovalDiskonNegotiationSearchValue);
					});
				}
				else
				{
					ApprovalDiskonNegotiationFactory.getData().then(function (res) {
						
						$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
						$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
						
						for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
							{
								if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
									$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
								}	
							}
						}
						
						if(res.data.Total<=5)
						{
							$scope.ApprovalDiskonNegotiationHideIncrement=true;
						}
						
						$scope.ResetSelection();
					});
				}
			}
			catch(e)
			{
				ApprovalDiskonNegotiationFactory.getData().then(function (res) {
					
					$scope.ApprovalDiskonNegotiationSearchIsPressed=false;
					$scope.ApprovalDiskonNegotiationGabungan = res.data.Result;
					
					for(var i = 0; i < $scope.ApprovalDiskonNegotiationGabungan.length; ++i)
					{
						for(var x = 0; x < $scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView.length; ++x)
						{
							if($scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].StatusApprovalName=="Disetujui")
							{
								$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverRoleName;
								$scope.ApprovalDiskonNegotiationGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonNegotiationGabungan[i].ListOfNegotiationApprovalDiscountView[x].ApproverName;
							}	
						}
					}
					
					if(res.data.Total<=5)
					{
						$scope.ApprovalDiskonNegotiationHideIncrement=true;
					}
					
					$scope.ResetSelection();
				});
			}
			

        }  

});
