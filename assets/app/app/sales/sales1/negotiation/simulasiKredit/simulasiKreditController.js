angular.module('app')
    .controller('SimulasiKreditController', function($scope, $http, CurrentUser, ListBookingUnitFactory, SimulasiKreditFactory,  ProfileLeasing, LeasingTenorFactory, SimulasiAsuransiFactory, ProfileInsuranceFactory, ProfileLeasingSimulationFactory, $timeout) {
        var Data_Simulasi_Kredit = "";
        var Data_Simulasi_Asuransi = "";
        $scope.Lokasi_Outlet = "Jakarta";
        $scope.Nama_Outlet = "AUTO 2000 Sunter";
        $scope.formData = true;

        $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "+";
        $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "+";
        $scope.ShowTabelSimulasiKredit_PanelIcon = "+";

        $scope.dateOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'year'
        };

        //$scope.optionsLeasing = ProfileLeasing.getData();
        ProfileLeasing.getData().then(function(res) {
            $scope.optionsLeasing = res.data.Result;
            return $scope.optionsLeasing;
        });

        ListBookingUnitFactory.getProspectRevisi().then(
            function(res) {
                $scope.prospects = res.data.Result;
                return $scope.prospects;
            }
        );


        //$scope.optionsModel = ModelFactory.getData();
        // ModelFactory.getData().then(function(res) {
        //     $scope.optionsModel = res.data.Result;
        //     return $scope.optionsModel;
        // });

        //$scope.optionsTipe = TipeFactory.getData();
        $scope.RawTipe = "";
        // TipeFactory.getData().then(function(res) {
        //     $scope.RawTipe = res.data.Result;
        //     return $scope.RawTipe;
        // });

        //$scope.optionsTenor = [{ name: "12 Bulan", value: "12 Bulan" }, { name: "24 Bulan", value: "24 Bulan" }, { name: "36 Bulan", value: "36 Bulan" }, { name: "48 Bulan", value: "48 Bulan" }, { name: "60 Bulan", value: "60 Bulan" }, { name: "72 Bulan", value: "72 Bulan" }];
        $scope.RawTenor = "";
        LeasingTenorFactory.getData().then(function(res) {
            $scope.RawTenor = res.data.Result;
            return $scope.RawTenor;
        });



        //$scope.optionsCariBerdasarkan = [{ name: "Max Angsuran", value: "Max Angsuran" }, { name: "Total Dp", value: "Total Dp" }];
        ProfileLeasingSimulationFactory.getData().then(function(res) {
            $scope.optionsCariBerdasarkan = res.data.Result;
            return $scope.optionsCariBerdasarkan;
        });



        //$scope.optionsPerusahaanAsuransi = [{ name: "Garda Oto", value: "Garda Oto" }, { name: "ACA", value: "ACA" }];
        ProfileInsuranceFactory.getData().then(function(res) {
            $scope.optionsPerusahaanAsuransi = res.data.Result;
            return $scope.optionsPerusahaanAsuransi;
        });






        //$scope.mSimulasiAsuransi_Model = $scope.optionsModel[0];
        //$scope.mSimulasiAsuransi_Tipe = $scope.optionsTipe[0];

        //navigateTab('tabContainer_SimulasiKredit','menuSimulasiAsuransi');
        //ulala();

        //bawah ini nanti cari gimana onload
        $scope.mSimulasiAsuransi_PerluasanJaminan = "";
        $scope.mSimulasiAsuransi_PerluasanJaminan = [{ Nama_Jaminan: null }];
        $scope.mSimulasiAsuransi_PerluasanJaminan.splice(0, 1);

        $scope.mSimulasiAsuransi_NamaJaminan_Submit = function() {
            $scope.mSimulasiAsuransi_PerluasanJaminan.push({ 'Nama_Jaminan': $scope.mSimulasiAsuransi_NamaJaminan_Txtbx });
            $scope.mSimulasiAsuransi_NamaJaminan_Txtbx = "";
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('hide');
        };

        $scope.RemoveJaminan = function(IndexJaminan) {
            $scope.mSimulasiAsuransi_PerluasanJaminan.splice(IndexJaminan, 1);
        };


        $scope.TabKreditPressed = function() {
            Data_Simulasi_Asuransi = ""
            Data_Simulasi_Kredit = SimulasiKreditFactory.getData();
            $scope.mSimulasiKredit = angular.copy(Data_Simulasi_Kredit[0]);

            //$scope.mSimulasiKredit_Prospect=Data_Simulasi_Kredit[0].Nama_Lengkap;
        };

        $scope.TabAsuransiPressed = function() {
            Data_Simulasi_Kredit = "";
            Data_Simulasi_Asuransi = SimulasiAsuransiFactory.getData();

            $scope.mSimulasiAsuransi = angular.copy(Data_Simulasi_Asuransi[0]);

            //$scope.mSimulasiAsuransi_Prospect=Data_Simulasi_Asuransi[0].Nama_Lengkap;


        };

        $scope.PerluasanJaminanModal = function() {
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('show');
        };

        $scope.mSimulasiAsuransi_NamaJaminan_Cancel = function() {
            angular.element('.ui.modal.ModalPerluasanJaminan').modal('hide');
        };

        $scope.ToSimulasiKredit_Clicked = function() {
            $scope.formData = false;
            $scope.ShowSimulasiKredit = true;
            $scope.menuSimulasiKredit = false;
        };

        $scope.ToSimulasiAsuransi_Clicked = function() {
            $scope.formData = false;
            $scope.ShowSimulasiAsuransi = true;
        };

        $scope.BackToTabClicked = function() {
            $scope.formData = true;
            $scope.ShowSimulasiKredit = false;
            $scope.ShowSimulasiAsuransi = false;
        };

        $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_Clicked = function() {
            $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan = !$scope.Show_Hide_SimulasiAsuransi_ModelKendaraan;
            if ($scope.Show_Hide_SimulasiAsuransi_ModelKendaraan == true) {
                $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "-";
            } else if ($scope.Show_Hide_SimulasiAsuransi_ModelKendaraan == false) {
                $scope.Show_Hide_SimulasiAsuransi_ModelKendaraan_PanelIcon = "+";
            }

        };

        $scope.Show_Hide_SimulasiAsuransi_Premi_Clicked = function() {
            $scope.Show_Hide_SimulasiAsuransi_Premi = !$scope.Show_Hide_SimulasiAsuransi_Premi;
            if ($scope.Show_Hide_SimulasiAsuransi_Premi == true) {
                $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "-";
            } else if ($scope.Show_Hide_SimulasiAsuransi_Premi == false) {
                $scope.Show_Hide_SimulasiAsuransi_Premi_PanelIcon = "+";
            }

        };

        $scope.ShowTabelSimulasiKredit = function() {
            $scope.TabelSimulasiKredit = !$scope.TabelSimulasiKredit;
            if ($scope.TabelSimulasiKredit == true) {
                $scope.ShowTabelSimulasiKredit_PanelIcon = "-";
            } else if ($scope.TabelSimulasiKredit == false) {
                $scope.ShowTabelSimulasiKredit_PanelIcon = "+";
            }

        };

        $scope.ModelKendaraan_KreditChanged = function(selected) {
            TipeFactory.getData('?start=1&limit=1000&filterData=VehicleModelId|' + selected).then(function(res) {
                $scope.optionsTipe = res.data.Result;
                return $scope.optionsTipe;
            });
        };

        $scope.TipeKendaraan_KreditChanged = function() {
            console.log("VehicleModelId :", $scope.mSimulasiKredit.VehicleModelId);
            console.log("VehicleTypeId :", $scope.mSimulasiKredit.VehicleTypeId);
            $scope.mSimulasiKredit.Nilai_Kendaraan = $scope.mSimulasiKredit.VehicleModelId + $scope.mSimulasiKredit.VehicleTypeId;
        };


        $scope.ModelKendaraan_AsuransiChanged = function() {

            var Filtered_Tipe2 = [{ VehicleTypeId: null, VehicleModelId: null, Description: null, KatashikiCode: null, SuffixCode: null }];
            Filtered_Tipe2.splice(0, 1);
            for (var i = 0; i < $scope.RawTipe.length; ++i) {
                if ($scope.RawTipe[i].VehicleModelId == $scope.mSimulasiAsuransi.VehicleModelId) {
                    Filtered_Tipe2.push({ VehicleTypeId: $scope.RawTipe[i].VehicleTypeId, VehicleModelId: $scope.RawTipe[i].VehicleModelId, Description: $scope.RawTipe[i].Description, KatashikiCode: $scope.RawTipe[i].KatashikiCode, SuffixCode: $scope.RawTipe[i].SuffixCode });
                }
            }
            $scope.optionsTipe_Asuransi = Filtered_Tipe2;
            $scope.mSimulasiAsuransi['VehicleTypeId'] = {};
        };

        $scope.LeasingChanged = function() {

            var Filtered_Tipe3 = [{ LeasingLeasingTenorId: null, LeasingId: null, LeasingName: null, LeasingTenorTime: null, LeasingTenorId: null }];
            Filtered_Tipe3.splice(0, 1);
            for (var i = 0; i < $scope.RawTenor.length; ++i) {
                if ($scope.RawTenor[i].LeasingId == $scope.mSimulasiKredit.LeasingId) {
                    Filtered_Tipe3.push({ LeasingLeasingTenorId: $scope.RawTenor[i].LeasingLeasingTenorId, LeasingId: $scope.RawTenor[i].LeasingId, LeasingName: $scope.RawTenor[i].LeasingName, LeasingTenorTime: $scope.RawTenor[i].LeasingTenorTime, LeasingTenorId: $scope.RawTenor[i].LeasingTenorId });
                }
            }
            $scope.optionsTenor = Filtered_Tipe3;
            $scope.mSimulasiKredit['LeasingLeasingTenorId'] = {};
        };



    });




// angular.module('app').factory('SampleUITemplateFactory', function ($http) {
// return {
// getData: function () {
// //var res = $http.get('file:///C:/Git/DMS-Sales-Prototype-Mike/DMS.Website/assets/app/app/sales/master/profileSalesProgram/the_thing.json');
// var res = null;
// //uda bisa sampe sini ternyata
// return res;
// }
// }
// });