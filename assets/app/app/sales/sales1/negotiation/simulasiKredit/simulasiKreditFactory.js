angular.module('app')
  .factory('SimulasiKreditFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      create: function(SimulasiKreditFactory) {
        console.log("MASuk", SimulasiKreditFactory);
        return $http.post('/api/sales/SNegotiationSimulasiKredit', [{                                      
                                            ProspectId: SimulasiKreditFactory.ProspectId,
                                            LeasingId: SimulasiKreditFactory.LeasingId,
                                            LeasingLeasingTenorId: SimulasiKreditFactory.LeasingLeasingTenorId,
                                            LeasingSimulationId: SimulasiKreditFactory.LeasingSimulationId,
                                            VehicleTypeColorId: SimulasiKreditFactory.VehicleTypeColorId,
                                            Price: SimulasiKreditFactory.Price,
                                            Nominal: SimulasiKreditFactory.Nominal
                                            }]);
      },
      update: function(SimulasiKreditFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: SimulasiKreditFactory.SalesProgramId,
                                            SalesProgramName: SimulasiKreditFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd