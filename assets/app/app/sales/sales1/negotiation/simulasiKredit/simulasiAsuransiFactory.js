angular.module('app')
  .factory('SimulasiAsuransiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var Data_Simulasi_Asuransi_Json=[
				{Id: "1",  Nama_Lengkap: "Selena Gomez",Perusahaan_Asuransi: "ACA",Model: 1,Tipe: "1.5 G M/T",Tahun: "2016",Harga_Kendaraan: "26000000",DP: "3000000",Sales_Force: "Didit"}
			  ];
		var res=Data_Simulasi_Asuransi_Json;	  
        return res;
      },
      create: function(SimulasiAsuransi) {
        return $http.post('/api/sales/SNegotiationSimulasiAsuransi', [SimulasiAsuransi]);
      },
      update: function(SimulasiAsuransi){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: SimulasiAsuransi.SalesProgramId,
                                            SalesProgramName: SimulasiAsuransi.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd