angular.module('app')
    .controller('BrosurSalesmanViewController', function($scope, $http, CurrentUser, BrosurSalesmanViewFactory,AddTaskListFactory,$timeout,bsNotify) {
    
	$scope.BrosurSalesmanViewMain=true;
	$scope.BrosurSalesmanViewEmailThingy=false;
	$scope.optionProspectSearchCriteriaBrosurSalesmanView = BrosurSalesmanViewFactory.GetProspectSearchDropdownOptions();
	$scope.ProspectSearchCriteriaBrosurSalesmanView=$scope.optionProspectSearchCriteriaBrosurSalesmanView[0].SearchOptionId;
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalBrosurSalesmanView').remove();
		  angular.element('.ui.modal.ModalProspectBuatBrosurSalesmanView').remove();
		});
	
	//$scope.List_Branch_To_Repeat=BrosurSalesmanViewFactory.getDataBranch();
	BrosurSalesmanViewFactory.getDataBranch().then(function(res){
            $scope.List_Branch_To_Repeat = res.data.Result;
            return $scope.List_Branch_To_Repeat;
        });
	
	//$scope.List_Tam_To_Repeat=BrosurSalesmanViewFactory.getDataTam();
	BrosurSalesmanViewFactory.getDataTam().then(function(res){
            $scope.List_Tam_To_Repeat = res.data.Result;
            return $scope.List_Tam_To_Repeat;
        });
	
	$scope.BrosurActionSelected = function (SelectedData,From) {
	   $scope.TheSelectedBrosur=angular.copy(SelectedData);
	   $scope.TheSelectedBrosur.SendTo={};
	   $scope.TheSelectedBrosur.Message={};
	   $scope.TheSelectedBrosur.SendTo="";
	   $scope.TheSelectedBrosur.Message="";
	   angular.element('.ui.modal.ModalBrosurSalesmanView').modal('show');
	   angular.element('.ui.modal.ModalBrosurSalesmanView').not(':first').remove();
	};
	
	$scope.KembaliDariBrosurSalesmanViewEmailThingy = function () {
		$scope.BrosurSalesmanViewMain=true;
		$scope.BrosurSalesmanViewEmailThingy=false;
	}
	
	$scope.BrosurSalesmanViewSendEmail = function () {
		
		$scope.BrosurSalesmanViewMain=false;
		$scope.BrosurSalesmanViewEmailThingy=true;
		$scope.TheSelectedBrosur.SendTo="";
		$scope.TheSelectedBrosur.Message="";
		angular.element('.ui.modal.ModalBrosurSalesmanView').modal('hide');
		
		// //pake $scope.TheSelectedBrosur buat send
		// $scope.TheSelectedBrosur.AttachmentData={};
		// $scope.TheSelectedBrosur.AttachmentData={ 'UpDocObj':btoa($scope.TheSelectedBrosur.BrochureData),
												// 'FileName':$scope.TheSelectedBrosur.BrochureDataName,
												// 'Path':"",
												// 'PrefixHtmlData':$scope.TheSelectedBrosur.BrochurePrefixHtmlData,
												// 'FileType':$scope.TheSelectedBrosur.BrochureFileType};
		
		// BrosurSalesmanViewFactory.Send_Da_Email($scope.TheSelectedBrosur).then(function () {
			// //nanti hide apa gimana
		// });
	};
	
	$scope.KirimEmailBrosurSalesmanView = function () {
		//pake $scope.TheSelectedBrosur buat send
		$scope.TheSelectedBrosur.AttachmentData={};
		BrosurSalesmanViewFactory.getImage($scope.TheSelectedBrosur.BrochureUrl).then(function(res) { 
			$scope.TheSelectedBrosur.AttachmentData={ 'UpDocObj':btoa(res.data.UpDocObj),
												'FileName':res.data.FileName,
												'Path':res.data.Path,
												//'PrefixHtmlData':res.data.PrefixHtmlData,
												'FileType':res.data.FileType};
			BrosurSalesmanViewFactory.Send_Da_Email($scope.TheSelectedBrosur).then(function () {
				//nanti hide apa gimana
				bsNotify.show(
								{
									title: "berhasil",
									content: "Email berhasil dikirim.",
									type: 'success'
								}
							);
				$scope.KembaliDariBrosurSalesmanViewEmailThingy();
			});									
		})
		
		
		
		
	}
	
	$scope.DownloadThingy = function(row){
            
			BrosurSalesmanViewFactory.getImage(row.BrochureUrl).then(function(res) { 
				$scope.dataStream2 = null;
				$scope.dataStream2= res.data.UpDocObj;
				$scope.TheFileTipeToDownload = $scope.dataStream2.split(';')[0];

				var blob2 = dataURItoBlob($scope.dataStream2);

				var blob = new Blob([blob2], { type: $scope.TheFileTipeToDownload.substring(5)});
				////change download.pdf to the name of whatever you want your file to be
				saveAs(blob, "download."+BrosurSalesmanViewGetFileTypeForDownloadExtension($scope.TheFileTipeToDownload.substring(5)));
			
			})
        }
		
		$scope.lookupProspectBrosurSalesmanView = function () {
			//$scope.DisableWhenEdit=true;
			angular.element('.ui.modal.ModalProspectBuatBrosurSalesmanView').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalProspectBuatBrosurSalesmanView').not(':first').remove();
			AddTaskListFactory.getDataProspect().then(function (res) {
				$scope.gridNoProspectBrosurSalesmanView = res.data.Result;
				
			});
			
        }
		
		$scope.BatallookupProspectBrosurSalesmanView = function () {
			//$scope.DisableWhenEdit=false;
			angular.element('.ui.modal.ModalProspectBuatBrosurSalesmanView').modal('hide');

        }
		
		$scope.pilihProspectBrosurSalesmanView = function (ProspectSelected) {
            $scope.TheSelectedBrosur.SendTo+=""+ProspectSelected.Email+";";
            angular.element('.ui.modal.ModalProspectBuatBrosurSalesmanView').modal('hide');
			
			//(temp.match(/;/g) || []).length;
			
			// if(($scope.TheSelectedBrosur.match(/;/g) || []).length>1)
			// {
				// $scope.TheSelectedBrosur.SendTo = $scope.TheSelectedBrosur.SendTo.substring(0, $scope.TheSelectedBrosur.SendTo.length-1);
			// }
			
        }
		
		$scope.SearchProspectBrosurSalesmanView = function () {

			
			if($scope.ProspectSearchCriteriaBrosurSalesmanView==1 && $scope.ProspectSearchValueBrosurSalesmanView!="")
			{
				BrosurSalesmanViewFactory.GetSearchProspectByName($scope.ProspectSearchValueBrosurSalesmanView).then(function (res) {
				$scope.gridNoProspectBrosurSalesmanView = res.data.Result;
				
				});
			}
			else if($scope.ProspectSearchCriteriaBrosurSalesmanView==2 && $scope.ProspectSearchValueBrosurSalesmanView!="")
			{
				BrosurSalesmanViewFactory.GetSearchProspectByEmail($scope.ProspectSearchValueBrosurSalesmanView).then(function (res) {
				$scope.gridNoProspectBrosurSalesmanView = res.data.Result;
				
				});
			}
			else
			{
				BrosurSalesmanViewFactory.getDataProspect().then(function (res) {
					$scope.gridNoProspectBrosurSalesmanView = res.data.Result;
					
				});
			}
			
			

        }
		
	
	
});
