angular.module('app')
  .factory('BrosurSalesmanViewFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();
    return {
      getDataBranch: function() {
        var res=$http.get('/api/sales/MAssetBrochureSales/?start=1&limit=10000&filterData=OutletId|'+currentUser.OutletId);
        return res;
      },
	  getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
      },
	  getDataTam: function() {
        var res=$http.get('/api/sales/MAssetBrochureSales/?start=1&limit=10000&filterData=InputByOrgTypeId|1');
        return res;
      },
	  
	  getDataProspect: function () {
        var res = $http.get('/api/sales/SDSGetProspectList');
        return res;
      },
	  
	  GetSearchProspectByName: function(SearchValue) {
        var res=$http.get('/api/sales/SDSGetProspectList/?prospectSPKCode=&ProspectName='+SearchValue);
        return res;
      },
	  
	  GetSearchProspectByEmail: function(SearchValue) {
        var res=$http.get('/api/sales/SDSGetProspectList/?Email='+SearchValue+'&ProspectName=');
        return res;
      },
	  
	  GetProspectSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "1", SearchOptionName: "Nama Prospect/Nama Pelanggan" },
          { SearchOptionId: "2", SearchOptionName: "Email" }
        ];

        var res=da_json

        return res;
      },
	  
	  Send_Da_Email: function(BrosurSalesmanView) {
	  
		var Da_TodayDate_Raw=new Date();
        var DaTodayDateForSubject=(Da_TodayDate_Raw.getDate())+'-'+(Da_TodayDate_Raw.getMonth()+1)+'-'+Da_TodayDate_Raw.getFullYear();
		
		var AlreadyReplaced1="";
		var AlreadyReplaced2="";
		
		if (BrosurSalesmanView.SendTo.substring(BrosurSalesmanView.SendTo.length-1) == ";")
		{
			AlreadyReplaced1 = BrosurSalesmanView.SendTo.substring(0, BrosurSalesmanView.SendTo.length-1);
		}
		else
		{
			AlreadyReplaced1=BrosurSalesmanView.SendTo;
		}
		
		//AlreadyReplaced2 = AlreadyReplaced1.replace(";", ",");tadinya soal harus gini
		return $http.post('/api/sales/EmailBrochure', [{
											SendTo: AlreadyReplaced1,
											SendCc: "",
											SendBcc: "",
											Subject: ""+DaTodayDateForSubject+" "+BrosurSalesmanView.BrochureName,
											AsHtml: true,
											Message: BrosurSalesmanView.Message,
											AttachmentData: BrosurSalesmanView.AttachmentData}]);
		// Message: '<img alt="gambarrambo2" src="'+BrosurSalesmanView.BrochureData+'" />',
								
      },
	  
      create: function(BrosurSalesmanView) {
        return $http.post('/api/sales/MAssetBrochure', [{
                                            //AppId: 1,
                                            NamaAlasanPengecualianPengiriman: BrosurSalesmanView.NamaAlasanPengecualianPengiriman,
											InputByAdmin: BrosurSalesmanView.InputByAdmin,
                                            InputBySalesman: BrosurSalesmanView.InputBySalesman}]);
      },
      update: function(BrosurSalesmanView){
        return $http.put('/api/sales/MAssetBrochure', [{
                                            AlasanPengecualianPengirimanId: BrosurSalesmanView.AlasanPengecualianPengirimanId,
											NamaAlasanPengecualianPengiriman: BrosurSalesmanView.NamaAlasanPengecualianPengiriman,
                                            InputByAdmin: BrosurSalesmanView.InputByAdmin,
                                            InputBySalesman: BrosurSalesmanView.InputBySalesman}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAssetBrochure',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd