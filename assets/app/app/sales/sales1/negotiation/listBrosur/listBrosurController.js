angular.module('app')
    .controller('ListBrosurController', function($scope,$stateParams, $http, CurrentUser, ListBrosurFactory,ListBrosurTypeFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
	//IfGotProblemWithModal();
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.mAssetBrosururMaster').remove();
		});
		
	$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };	
		
	$scope.dateOptionsListBrosurFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };
		
	$scope.dateOptionsListBrosurSampai = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };	
		
	$scope.ShowListBrosurMain=true;
	$scope.ShowAddListBrosurBtn=true;
	$scope.ListBrosurOperation="";
	$scope.ListBrosurSaveBtn=false;
	$scope.DisableListBrosurParameter=true;
	
	$scope.TodayDate_Raw=new Date();
	$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);		
	//$scope.MinDueDateListBrosur=$scope.TodayDate;
	
	$scope.dateOptionsListBrosurSampai.minDate=$scope.TodayDate;
	
	
	$scope.uploadFiles=[];
	$scope.filter={ BrochureTypeId:null, BrochureName:null,  ValidDate:null,  UploadById:null, ShowBit:null};
    $scope.filter.ShowBit=true;        

		

	$scope.filter.ValidDate=new Date();
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
	$scope.tab = $stateParams.tab;
	$scope.breadcrums = {};
    $scope.mListBrosur = {}; //Model
	$scope.mListBrosur.UploadDataBrochure = {};
    $scope.uploadFiles = [];
	$scope.xRole={selected:[]};
	$scope.disabledSaveTambahListBrosur = false;
	
	ListBrosurTypeFactory.getData().then(function(res){
            $scope.jenisBrosurOption = res.data.Result;
            return $scope.jenisBrosurOption;
        });
		
	ListBrosurFactory.getDaUpload().then(function(res){
            $scope.DaUploadByAtListBrosur = res.data.Result;
            return $scope.DaUploadByAtListBrosur;
        });	

    //----------------------------------
    // Get Data
    //----------------------------------
	
    var gridData = [];
    $scope.getData = function() {
		try
		{
			var da_ValidOn=$scope.filter.ValidDate;
			$scope.filter.ValidDate= da_ValidOn.getFullYear()+'-'
												+('0' + (da_ValidOn.getMonth()+1)).slice(-2)+'-'
												+('0' + da_ValidOn.getDate()).slice(-2);
		}
		catch(exceptionthingy2)
		{
			$scope.filter.ValidDate=null;
		}
		
		

			ListBrosurFactory.getData($scope.filter)
			.then(
				function(res){
					gridData = [];
					for(var i=0;i<res.data.Result.length;i++)
					{
						res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
						res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
						res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
					}
					$scope.grid.data = res.data.Result;
					$scope.uploadFiles = [];
					if(res.data.Result.length<=0)
					{
						bsNotify.show(
								{
									title: "Informasi",
									content: "Tidak ada data ditemukan.",
									type: 'info'
								}
							);
					}
					$scope.loading=false;
				},
				function(err){
					bsNotify.show(
									{
										title: "gagal",
										content: "Data tidak ditemukan.Terjadi error",
										type: 'danger'
									}
								);
				}
			);
	
    }
	
	$scope.TestBtn = function()
        {
            console.log("upload file", $scope.uploadFiles);
        }

	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        //console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        //console.log("onSelectRows=>",rows);
    }
	
	$scope.viewDaListBrosurDocument = function(row) {      
            
        }
		
	// $scope.ListBrosurTanggalMulaiSampaiSearchChanged = function () {
		
		// if($scope.filter.ValidOn==null)
		// {
			// $scope.filter.ValidUntil=null;
		// }
		
		// if($scope.filter.ValidOn>$scope.filter.ValidUntil)
		// {
			// $scope.filter.ValidUntil=$scope.filter.ValidOn;
		// }
	// }
	
	$scope.DaStartDateListBrosurChanged = function () {
		$scope.dateOptionsListBrosurFrom.minDate=$scope.TodayDate;
		$scope.MinDueDateListBrosur_Raw=$scope.mListBrosur.ValidOn;
		$scope.MinDueDateListBrosur=$scope.MinDueDateListBrosur_Raw.getFullYear()+'-'+('0' + ($scope.MinDueDateListBrosur_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinDueDateListBrosur_Raw.getDate()).slice(-2);
		
		$scope.dateOptionsListBrosurSampai.minDate=$scope.MinDueDateListBrosur;
		if($scope.mListBrosur.ValidOn>=$scope.mListBrosur.ValidUntil)
		{
				$scope.mListBrosur.ValidUntil=$scope.mListBrosur.ValidOn;
		}
	}
	
	$scope.addNewListBrosur = function () {
			$scope.mListBrosur={};
			$scope.uploadFiles=[];
			$scope.ListBrosurGambarKaloLihat={};
			$scope.ShowListBrosurMain=false;
			$scope.ShowAddListBrosurBtn=false;
			$scope.ShowListBrosurDetail=true;
			$scope.mListBrosur.ShowBit=false;
			$scope.ListBrosurOperation="Insert";
			$scope.DisableListBrosurParameter=false;
			$scope.ListBrosurSaveBtn=true;
			$scope.mListBrosur.ShowBit=true;
			
			$scope.mListBrosur.ValidOn=new Date();
			$scope.DaStartDateListBrosurChanged();
			$scope.mListBrosur.ValidUntil=new Date();
			$scope.breadcrums.title="Tambah";
			
		}
		
	$scope.editListBrosur = function(selected_listbrosur){
		$scope.breadcrums.title="Ubah";
		$scope.mListBrosur=angular.copy(selected_listbrosur);
		if($scope.mListBrosur.UploadDate!=null||(typeof $scope.mListBrosur.UploadDate!="undefined"))
			{
				var da_tanggal=""+$scope.mListBrosur.UploadDate.getDate();
				var da_bulan_raw=($scope.mListBrosur.UploadDate.getMonth()+1);
				var da_bulan="";
				if(da_bulan_raw==1)
				{
					da_bulan="Januari";
				}
				else if(da_bulan_raw==2)
				{
					da_bulan="Februari";
				}
				else if(da_bulan_raw==3)
				{
					da_bulan="Maret";
				}
				else if(da_bulan_raw==4)
				{
					da_bulan="April";
				}
				else if(da_bulan_raw==5)
				{
					da_bulan="Mei";
				}
				else if(da_bulan_raw==6)
				{
					da_bulan="Juni";
				}
				else if(da_bulan_raw==7)
				{
					da_bulan="Juli";
				}
				else if(da_bulan_raw==8)
				{
					da_bulan="Agustus";
				}
				else if(da_bulan_raw==9)
				{
					da_bulan="September";
				}
				else if(da_bulan_raw==10)
				{
					da_bulan="Oktober";
				}
				else if(da_bulan_raw==11)
				{
					da_bulan="November";
				}
				else if(da_bulan_raw==12)
				{
					da_bulan="Desember";
				}
				var da_tahun=""+$scope.mListBrosur.UploadDate.getFullYear();
				$scope.TanggalUploadDipaksa=""+da_tanggal+" "+da_bulan+" "+da_tahun;
			}
		
		ListBrosurFactory.getImage($scope.mListBrosur.BrochureUrl).then(function(res) {                
                //$scope.viewdocument = res.data.UpDocObj;               
                //$scope.viewdocumentName = res.data.FileName;
				console.log("setan",res.data);
				$scope.ListBrosurGambarKaloLihat={};
				$scope.uploadFiles[0]={};
				$scope.uploadFiles[0].UpDocObj={};
				$scope.uploadFiles[0].FileName={};
				$scope.uploadFiles[0].UpDocObj=res.data.UpDocObj;
				$scope.uploadFiles[0].FileName=res.data.FileName;
				$scope.mListBrosur.UploadDataBrochure={UpDocObj:res.data.UpDocObj,FileName:res.data.FileName};
            })

				
		$scope.ShowAddListBrosurBtn=false;
		$scope.ShowListBrosurMain=false;
		$scope.ShowListBrosurDetail=true;
		$scope.ListBrosurOperation="Update";
		$scope.DisableListBrosurParameter=false;
		$scope.ListBrosurSaveBtn=true;
			
		}

	$scope.lihatListBrosur = function(selected_listbrosur){
			$scope.mListBrosur=angular.copy(selected_listbrosur);
			$scope.TanggalUploadDipaksa="";
			if($scope.mListBrosur.UploadDate!=null||(typeof $scope.mListBrosur.UploadDate!="undefined"))
			{
				var da_tanggal=""+$scope.mListBrosur.UploadDate.getDate();
				var da_bulan_raw=($scope.mListBrosur.UploadDate.getMonth()+1);
				var da_bulan="";
				if(da_bulan_raw==1)
				{
					da_bulan="Januari";
				}
				else if(da_bulan_raw==2)
				{
					da_bulan="Februari";
				}
				else if(da_bulan_raw==3)
				{
					da_bulan="Maret";
				}
				else if(da_bulan_raw==4)
				{
					da_bulan="April";
				}
				else if(da_bulan_raw==5)
				{
					da_bulan="Mei";
				}
				else if(da_bulan_raw==6)
				{
					da_bulan="Juni";
				}
				else if(da_bulan_raw==7)
				{
					da_bulan="Juli";
				}
				else if(da_bulan_raw==8)
				{
					da_bulan="Agustus";
				}
				else if(da_bulan_raw==9)
				{
					da_bulan="September";
				}
				else if(da_bulan_raw==10)
				{
					da_bulan="Oktober";
				}
				else if(da_bulan_raw==11)
				{
					da_bulan="November";
				}
				else if(da_bulan_raw==12)
				{
					da_bulan="Desember";
				}
				var da_tahun=""+$scope.mListBrosur.UploadDate.getFullYear();
				$scope.TanggalUploadDipaksa=""+da_tanggal+" "+da_bulan+" "+da_tahun;
			}
			
			$scope.breadcrums.title="Lihat";
			ListBrosurFactory.getImage($scope.mListBrosur.BrochureUrl).then(function(res) { 
				$scope.uploadFiles[0]={};
				$scope.uploadFiles[0].UpDocObj={};
				$scope.uploadFiles[0].FileName={};
				$scope.ListBrosurGambarKaloLihat={};
				$scope.ListBrosurGambarKaloLihat=res.data.UpDocObj;
			})
			
			
			//var DaFileTipeToLihat = $scope.ListBrosurGambarKaloLihat.split(';')[0];
			//$scope.ListBrosurDaFileTipeLihat=DaFileTipeToLihat.substring(5);
			// if($scope.ListBrosurDaFileTipeLihat=="application/pdf")
			// {
				// var AlreadyReplaced1 = $scope.ListBrosurGambarKaloLihat.replace("application/pdf;", "");
				// $scope.ListBrosurGambarKaloLihat=AlreadyReplaced1;
			// }
			
			$scope.ShowAddListBrosurBtn=false;
			$scope.ShowListBrosurMain=false;
			$scope.ShowListBrosurDetail=true;
			$scope.ListBrosurOperation="Lihat";
			$scope.ListBrosurSaveBtn=false;
			$scope.DisableListBrosurParameter=true;
		}
		
	$scope.BatalTambahListBrosur = function(){
			$scope.mListBrosur={};
			$scope.uploadFiles=[];
			$scope.ListBrosurGambarKaloLihat={};
			$scope.ShowAddListBrosurBtn=true;
			$scope.ShowListBrosurMain=true;
			$scope.ShowListBrosurDetail=false;
			$scope.ListBrosurOperation="";
			$scope.ListBrosurSaveBtn=false;
			$scope.DisableListBrosurParameter=true;
			//IfGotProblemWithModal();
		}

	$scope.SaveTambahListBrosur = function(){
		$scope.disabledSaveTambahListBrosur = true;
		$scope.mListBrosur.UploadDataBrochure.UpDocObj= btoa($scope.mListBrosur.UploadDataBrochure.UpDocObj);	
		try
		{
			if($scope.ListBrosurOperation=="Insert")
			{
				ListBrosurFactory.create($scope.mListBrosur).then(function () {
					ListBrosurFactory.getData($scope.filter).then(
						function(res){
							for(var i=0;i<res.data.Result.length;i++)
							{
								res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
								res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
								res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
							}
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							
							bsNotify.show(
								{
									title: "berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
							$scope.disabledSaveTambahListBrosur = false;
							return res.data;
						}
					).then(function () {
						$scope.BatalTambahListBrosur();
					});
				});
			}
			else if($scope.ListBrosurOperation=="Update")
			{
				ListBrosurFactory.update($scope.mListBrosur).then(function () {
					ListBrosurFactory.getData($scope.filter).then(
						function(res){
							for(var i=0;i<res.data.Result.length;i++)
							{
								res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
								res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
								res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
							}
							$scope.grid.data = res.data.Result;
							
							bsNotify.show(
								{
									title: "berhasil",
									content: "Data berhasil disimpan.",
									type: 'success'
								}
							);
							$scope.disabledSaveTambahListBrosur = false;
							$scope.loading=false;
							return res.data;
						}
					).then(function () {
						$scope.BatalTambahListBrosur();
					});
				});
			}
		}
		catch(e1)
		{
			bsNotify.show(
						{
							title: "gagal",
							content: "Insert Data Gagal Pastikan Semua Data Terisi Dengan Benar!",
							type: 'danger'
						}
					);
					$scope.disabledSaveTambahListBrosur = false;
		}
		
	}
	
	$scope.DeleteManualListBrosur = function (id) {
			console.log('iddelete',id);
			//UploadByName
			var apus_list_brosur_aman=true;
			for(var i = 0; i < id.length; ++i)
			{	
				if(id[i].UploadByName!=$scope.user.RoleName)
				{
					apus_list_brosur_aman=false;
					break;
				}
			}
			
			if(apus_list_brosur_aman==true)
			{
				var arrayDelete = [];
				for(var i in id) {
					arrayDelete.push(id[i].BrochureId)
				}
				console.log('arraydel',arrayDelete);
				ListBrosurFactory.delete(arrayDelete).then(function () {
					ListBrosurFactory.getData($scope.filter).then(
						function(res){
							for(var i=0;i<res.data.Result.length;i++)
							{
								res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
								res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
								res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
							}
							$scope.grid.data = res.data.Result;
							bsNotify.show(
								{
									title: "berhasil",
									content: "Data berhasil dihapus.",
									type: 'success'
								}
							);
							$scope.loading=false;
							return res.data;
						}
					).then(function () {
						$scope.BatalTambahListBrosur();
					});
				}
					
							
				)
			}
			else if(apus_list_brosur_aman==false)
			{
				bsNotify.show(
									{
										title: "Peringatan",
										content: "Anda tidak dapat menghapus data milik pengguna lain",
										type: 'warning'
									}
								);
			}
			
			
		}
	
	$scope.HapusListBrosur = function(SelectedBrosurToDelete){
		ListBrosurFactory.delete([SelectedBrosurToDelete.BrochureId]).then(function () {
					ListBrosurFactory.getData($scope.filter).then(
						function(res){
							for(var i=0;i<res.data.Result.length;i++)
							{
								res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
								res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
								res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
							}
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							return res.data;
						}
					).then(function () {
						$scope.BatalTambahListBrosur();
					});
				});
	}
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
	
	//var viewDocument = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.viewDocuments(row.entity)">{{row.entity.BrochureName}}</u></p></a>';
      
	var viewDocument2 = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.DownloadThingy(row.entity)">Download</u></p></a>';  
    
	var viewDocument3 = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.lihatListBrosur(row.entity)">Lihat</u></p></a>'; 
	
	var viewDocument4 = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.editListBrosur(row.entity)">Ubah</u></p></a>';

	var viewDocument5 = '<i title="Lihat" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.lihatListBrosur(row.entity)" ></i>	<i ng-hide="grid.appScope.$parent.user.RoleName!=row.entity.UploadByName" title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.editListBrosur(row.entity)" ></i> <i title="Download" class="fa fa-fw fa-lg fa-download" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.DownloadThingy(row.entity)" ></i>';
		
		$scope.viewDocuments = function(row){
			 $scope.dataStream = null;
            angular.element('.ui.modal.mAssetBrosururMaster').modal('show');
			angular.element('.ui.modal.mAssetBrosururMaster').not(':first').remove();
            $scope.BrocureNames = row.BrochureName;
            $scope.dataStream = row.BrochureData;

        }
		

		
		$scope.DownloadThingy = function(row){
            
			ListBrosurFactory.getImage(row.BrochureUrl).then(function(res) { 
				$scope.dataStream2 = null;
				$scope.dataStream2= res.data.UpDocObj;
				$scope.TheFileTipeToDownload = $scope.dataStream2.split(';')[0];

				var blob2 = dataURItoBlob($scope.dataStream2);

				var blob = new Blob([blob2], { type: $scope.TheFileTipeToDownload.substring(5)});
				////change download.pdf to the name of whatever you want your file to be
				saveAs(blob, "download."+ListBrosurGetFileTypeForDownloadExtension($scope.TheFileTipeToDownload.substring(5)));
			
			})
			
			

        }
		
		
		
	var dateFormat = 'dd/MM/yyyy';
    var dateFilter = 'date:"dd/MM/yyyy"';
	$scope.dateOption = { startingDay: 1, format: dateFormat };
		
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: false,
		enableColumnMenus: false,
		enableColumnResizing: true,
        //showTreeExpandNoChildren: true,
        //paginationPageSizes: [10,20,50,100],
        //paginationPageSize: 10,
        columnDefs: [
            { name:'Jenis Brosur', field:'BrochureTypeName' },
            { name:'judul',  field: 'BrochureName' },
			{ name:'Tanggal Upload',  field: 'UploadDate' , cellFilter: dateFilter},
            { name:'tanggal dimulai',  field: 'ValidOn' , cellFilter: dateFilter},
            { name:'tanggal selesai',  field: 'ValidUntil', cellFilter: dateFilter },
            { name:'upload oleh',  field: 'UploadByName' },
            { name:'tampilkan',  field: 'ShowBit',enableFiltering: false,visible:true , width:'8%', cellTemplate:' <input type="checkbox" ng-disabled="true" ng-checked="row.entity.ShowBit">'},
			{ name:'Action', cellTemplate: viewDocument5,enableFiltering: false,visible:true },
			// { name:'Download', cellTemplate: viewDocument2, width:'8%' },
			// { name:'Lihat', cellTemplate: viewDocument3, width:'6%' },
			// { name:'Ubah', cellTemplate: viewDocument4 , width:'6%'},
        ]
    };
	
	ListBrosurFactory.getData($scope.filter).then(
						function(res){
							for(var i=0;i<res.data.Result.length;i++)
							{
								res.data.Result[i].UploadDate=new Date(res.data.Result[i].UploadDate.getFullYear(), res.data.Result[i].UploadDate.getMonth(), res.data.Result[i].UploadDate.getDate());
								res.data.Result[i].ValidOn=new Date(res.data.Result[i].ValidOn.getFullYear(), res.data.Result[i].ValidOn.getMonth(), res.data.Result[i].ValidOn.getDate());
								res.data.Result[i].ValidUntil==new Date(res.data.Result[i].ValidUntil.getFullYear(), res.data.Result[i].ValidUntil.getMonth(), res.data.Result[i].ValidUntil.getDate());
							}
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							return res.data;
						}
					);
});

angular.module('app').directive('myDirective', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      element.bind('change', function() {
        var formData = new FormData();
        formData.append('file', element[0].files[0]);

        // optional front-end logging 
        var fileObject = element[0].files[0];
        scope.fileLog = {
          'lastModified': fileObject.lastModified,
          'lastModifiedDate': fileObject.lastModifiedDate,
          'name': fileObject.name,
          'size': fileObject.size,
          'type': fileObject.type
        };
        
        scope.mAssetBrosur.BrochureUrl = "/imgs/content/Master/"+scope.fileLog.name;
        scope.$apply();


      });

    }
  };
});
