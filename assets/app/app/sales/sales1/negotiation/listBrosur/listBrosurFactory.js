angular.module('app')
  .factory('ListBrosurFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
		var param = $httpParamSerializer(filter);
        var res=$http.get('/api/sales/MAssetBrochure/filter/?'+param);
        //console.log('res=>',res);
		
        return res;
      },
	  getDaUpload: function() {
        var res=$http.get('/api/sales/MProfileEmployee/?RoleId=1016');  
        return res;
      },
	  getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
      },
      create: function(ListBrosur) {
        return $http.post('/api/sales/MAssetBrochure', [{
                                            //AppId: 1,
											BrochureTypeId: ListBrosur.BrochureTypeId,
											BrochureName: ListBrosur.BrochureName,
											UploadDataBrochure: ListBrosur.UploadDataBrochure,
											
											ValidOn: ListBrosur.ValidOn.getFullYear()+'-'
											+('0' + (ListBrosur.ValidOn.getMonth()+1)).slice(-2)+'-'
											+('0' + ListBrosur.ValidOn.getDate()).slice(-2)+'T'
											+((ListBrosur.ValidOn.getHours()<'10'?'0':'')+ ListBrosur.ValidOn.getHours())+':'
											+((ListBrosur.ValidOn.getMinutes()<'10'?'0':'')+ ListBrosur.ValidOn.getMinutes())+':'
											+((ListBrosur.ValidOn.getSeconds()<'10'?'0':'')+ ListBrosur.ValidOn.getSeconds()),
											
											ValidUntil: ListBrosur.ValidUntil.getFullYear()+'-'
											+('0' + (ListBrosur.ValidUntil.getMonth()+1)).slice(-2)+'-'
											+('0' + ListBrosur.ValidUntil.getDate()).slice(-2)+'T'
											+((ListBrosur.ValidUntil.getHours()<'10'?'0':'')+ ListBrosur.ValidUntil.getHours())+':'
											+((ListBrosur.ValidUntil.getMinutes()<'10'?'0':'')+ ListBrosur.ValidUntil.getMinutes())+':'
											+((ListBrosur.ValidUntil.getSeconds()<'10'?'0':'')+ ListBrosur.ValidUntil.getSeconds()),
											
                                            UploadDate: new Date(),
											UploadById: ListBrosur.UploadById,
											InputByOrgTypeId: ListBrosur.InputByOrgTypeId,
                                            ShowBit: ListBrosur.ShowBit}]);
      },
      update: function(ListBrosur){
        return $http.put('/api/sales/MAssetBrochure', [{
											BrochureId: ListBrosur.BrochureId,
											OutletId: ListBrosur.OutletId,
                                            BrochureTypeId: ListBrosur.BrochureTypeId,
											BrochureName: ListBrosur.BrochureName,
											UploadDataBrochure: ListBrosur.UploadDataBrochure,
											
											ValidOn: ListBrosur.ValidOn.getFullYear()+'-'
											+('0' + (ListBrosur.ValidOn.getMonth()+1)).slice(-2)+'-'
											+('0' + ListBrosur.ValidOn.getDate()).slice(-2)+'T'
											+((ListBrosur.ValidOn.getHours()<'10'?'0':'')+ ListBrosur.ValidOn.getHours())+':'
											+((ListBrosur.ValidOn.getMinutes()<'10'?'0':'')+ ListBrosur.ValidOn.getMinutes())+':'
											+((ListBrosur.ValidOn.getSeconds()<'10'?'0':'')+ ListBrosur.ValidOn.getSeconds()),
											
											ValidUntil: ListBrosur.ValidUntil.getFullYear()+'-'
											+('0' + (ListBrosur.ValidUntil.getMonth()+1)).slice(-2)+'-'
											+('0' + ListBrosur.ValidUntil.getDate()).slice(-2)+'T'
											+((ListBrosur.ValidUntil.getHours()<'10'?'0':'')+ ListBrosur.ValidUntil.getHours())+':'
											+((ListBrosur.ValidUntil.getMinutes()<'10'?'0':'')+ ListBrosur.ValidUntil.getMinutes())+':'
											+((ListBrosur.ValidUntil.getSeconds()<'10'?'0':'')+ ListBrosur.ValidUntil.getSeconds()),
											
                                            UploadDate: new Date(),
											UploadById: ListBrosur.UploadById,
											InputByOrgTypeId: ListBrosur.InputByOrgTypeId,
                                            ShowBit: ListBrosur.ShowBit}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAssetBrochure',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd