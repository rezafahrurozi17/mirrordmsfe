angular.module('app')
  .factory('ListBrosurTypeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MAssetBrochureType');
        //console.log('res=>',res);
		
        return res;
      },
      create: function(ActivityAlasanPengecualianPengiriman) {
        return $http.post('/api/sales/MAssetBrochureType', [{
                                            //AppId: 1,
                                            NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
											InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
                                            InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      },
      update: function(ActivityAlasanPengecualianPengiriman){
        return $http.put('/api/sales/MAssetBrochureType', [{
                                            AlasanPengecualianPengirimanId: ActivityAlasanPengecualianPengiriman.AlasanPengecualianPengirimanId,
											NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
                                            InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
                                            InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MAssetBrochureType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd