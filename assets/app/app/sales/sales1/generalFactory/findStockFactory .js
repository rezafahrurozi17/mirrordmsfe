angular.module('app')
    .factory('FindStockFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getDataGroupDealer: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/DemandSupplyFindStockGroupDealer/?' + $httpParamSerializer(filter));
                return res;
            },

            getDataOthDealer: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/DemandSupplyFindStockOtherGroupDealer/?start=1&limit=100&' + $httpParamSerializer(filter));
                return res;
            },

            getDataArea: function() {
                var res = $http.get('/api/sales/MSitesArea');
                return res;
            },

            getDataProvince: function() {
                var res = $http.get('/api/sales/MLocationProvince');
                return res;
            },

            getDataProvincebyArea: function(param) {
                var res = $http.get('/api/sales/MLocationProvince?AreaId=' + param);
                return res;
            },

            getTipeModel: function(param) {
                var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
                return res;
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });