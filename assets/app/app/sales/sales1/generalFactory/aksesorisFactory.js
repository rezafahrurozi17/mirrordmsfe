angular.module('app')
  .factory('AksesorisFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var Unfiltered_Aksesoris_Json=[
				{Id: "1",  Nama_Aksesoris: "Nama1",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Paket Aksesoris",  Selected: false,Jumlah: 1},
				{Id: "2",  Nama_Aksesoris: "Nama2",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Paket Aksesoris",  Selected: false,Jumlah: 1},
				{Id: "3",  Nama_Aksesoris: "Nama3",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Kaca Film",  Selected: false,Jumlah: 1},
				{Id: "4",  Nama_Aksesoris: "Nama4",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Kaca Film",  Selected: false,Jumlah: 1},
				{Id: "5",  Nama_Aksesoris: "Nama5",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Kaca Film",  Selected: false,Jumlah: 1},
				{Id: "6",  Nama_Aksesoris: "Nama6",  Harga_Aksesoris: "1000",Kategori_Aksesoris:"Kaca Film",  Selected: false,Jumlah: 1},
			  ];
		var res=Unfiltered_Aksesoris_Json;	  
		
        return res;
      },
      create: function(aksesoris) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: aksesoris.Name,
                                            Description: aksesoris.Description}]);
      },
      update: function(aksesoris){
        return $http.put('/api/fw/Role', [{
                                            Id: aksesoris.Id,
                                            //pid: negotiation.pid,
                                            Name: aksesoris.Name,
                                            Description: aksesoris.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });