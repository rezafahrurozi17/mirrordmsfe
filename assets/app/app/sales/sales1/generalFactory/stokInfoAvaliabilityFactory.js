angular.module('app')
  .factory('StockInfoAvaliabilityFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res=$http.get('/api/sales/CVehicleListStokInfoView'+param);
        //console.log('res=>',res);
		    //var res=json_StockInfoAvaliability;
        return res;
      },
      create: function(stockInfoAvaliability) {
        return $http.post('/api/sales/CVehicleListStokInfoView', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: stockInfoAvaliability.Name,
                                            Description: stockInfoAvaliability.Description}]);
      },
      update: function(stockInfoAvaliability){
        return $http.put('/api/sales/CVehicleListStokInfoView', [{
                                            Id: stockInfoAvaliability.Id,
                                            //pid: negotiation.pid,
                                            Name: stockInfoAvaliability.Name,
                                            Description: stockInfoAvaliability.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CVehicleListStokInfoView',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });