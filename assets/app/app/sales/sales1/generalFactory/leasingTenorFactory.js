angular.module('app')
  .factory('LeasingTenorFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileLeasingLeasingTenor'); 
        return res;
      },
      create: function(Tipe) {
        return $http.post('/api/sales/MProfileLeasingLeasingTenor', [{
                                            //AppId: 1,
                                            SalesProgramName: Tipe.SalesProgramName}]);
      },
      update: function(Tipe){
        return $http.put('/api/sales/MProfileLeasingLeasingTenor', [{
                                            SalesProgramId: Tipe.SalesProgramId,
                                            SalesProgramName: Tipe.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileLeasingLeasingTenor',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd