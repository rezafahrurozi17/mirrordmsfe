angular.module('app')
  .factory('MaintainPDDFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
                //var res = $http.get('/api/fw/Role');
                //console.log('res=>',res);

                var res = $http.get('/api/sales/PDDMaintain'+param);
              //  console.log("res Factory", res);

                // var result = [
                //     { PDDId: "1",FormSPKNo:"SPK12345",SPKDate:"2016-06-25T11:47:00.06",CustomerName:"Bruce Haney", VehicleModelName: "Avanza", KatashikiSuffixCode: "1.5 G M/T", ColorName: "White",StatusUnitName:"Ready",RRN:"EE02259",FrameNo:"MHKM5E12345678",FundSourceName:"Cash",DirectDeliveryPDC:false,ChoosePoliceNo:false,OnOffTheRoadId:2,SentWithSTNK:true,StatusPDDName:"Sudah Dibuat",CountRevisi:1,StatusPDC:''},
                //     { PDDId: "2",FormSPKNo:"SPK12345",SPKDate:"2016-06-25T11:47:00.06",CustomerName:"Cain Franks", VehicleModelName: "Avanza", KatashikiSuffixCode: "1.5 G M/T", ColorName: "White",StatusUnitName:"Ready",RRN:"EE02259",FrameNo:"MHKM5E12345678",FundSourceName:"Credit",DirectDeliveryPDC:false,ChoosePoliceNo:false,OnOffTheRoadId:2,SentWithSTNK:true,StatusPDDName:"Sudah Dibuat",CountRevisi:0,StatusPDC:'Sudah Dikirim PDC'},
                //     { PDDId: "3",FormSPKNo:"SPK12345",SPKDate:"2016-06-25T11:47:00.06",CustomerName:"Cain Rodriguez", VehicleModelName: "Avanza", KatashikiSuffixCode: "1.5 G M/T", ColorName: "White",StatusUnitName:"Ready",RRN:"EE02259",FrameNo:"MHKM5E12345678",FundSourceName:"Cash",DirectDeliveryPDC:false,ChoosePoliceNo:false,OnOffTheRoadId:1,SentWithSTNK:true,StatusPDDName:"Sudah Dibuat",CountRevisi:0,StatusPDC:''},
                //     { PDDId: "4",FormSPKNo:"SPK12345",SPKDate:"2016-06-25T11:47:00.06",CustomerName:"Coldwell Shepard", VehicleModelName: "Avanza", KatashikiSuffixCode: "1.5 G M/T", ColorName: "White",StatusUnitName:"Ready",RRN:"EE02259",FrameNo:"",FundSourceName:"Credit",DirectDeliveryPDC:false,ChoosePoliceNo:false,OnOffTheRoadId:2,SentWithSTNK:false,StatusPDDName:"Butuh Konfirmasi"},
                //     { PDDId: "5",FormSPKNo:"SPK12345",SPKDate:"2016-06-25T11:47:00.06",CustomerName:"Callum Logan", VehicleModelName: "Avanza", KatashikiSuffixCode: "1.5 G M/T", ColorName: "White",StatusUnitName:"Ready",RRN:"EE02259",FrameNo:"",FundSourceName:"Cash",DirectDeliveryPDC:false,ChoosePoliceNo:false,OnOffTheRoadId:1,SentWithSTNK:true,StatusPDDName:"Belum Dibuat"},
                // ];

                // var res = {
                //     Result: result,
                //     Start:1,
                //     Limit: 10,
                //     Total:20
                // };

                return res;
            },

            getKalkulasiPDD: function(param) {
              
                var res = $http.get('/api/sales/PDDKalkulasi/'+param);
              //  console.log("res Factory", res);


                return res;
            },
      getHistoryPDD: function(param) {
               
         var res = $http.get('/api/sales/PDDHistory/'+param); 
         return res;
      },

      getResonRevision: function() {
              
         var res = $http.get('/api/sales/PCategoryReasonRevision/');
         //  console.log("res Factory", res);
         return res;
      },
      createPDDCash: function(KalkulasiPDD) {
          
        return $http.post('/api/sales/PDDMaintain', KalkulasiPDD);
      },

      revisiPDDCash: function(RevisiKalkulasiPDD) {
        
		  
        return $http.put('/api/sales/PDDMaintain', RevisiKalkulasiPDD);
      },

      kalkulasiTLSDate: function(KalkulasiTLSDate) {
          
        return $http.post('/api/sales/TLSCalculateDeliveryRequest', KalkulasiTLSDate);
      },

      confirmPDDDateTLS: function(ConfirmPDDDate) {
          
        return $http.post('/api/sales/TLSConfirmDeliveryRequest', ConfirmPDDDate);
      },
      
      // create: function(ActivityAlasanPengecualianPengiriman) {
      //   return $http.post('/api/fw/Role', [{
      //                                       //AppId: 1,
      //                                       NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
			// 								InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
      //                                       InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      // },
      // update: function(ActivityAlasanPengecualianPengiriman){
      //   return $http.put('/api/fw/Role', [{
      //                                       AlasanPengecualianPengirimanId: ActivityAlasanPengecualianPengiriman.AlasanPengecualianPengirimanId,
			// 								NamaAlasanPengecualianPengiriman: ActivityAlasanPengecualianPengiriman.NamaAlasanPengecualianPengiriman,
      //                                       InputByAdmin: ActivityAlasanPengecualianPengiriman.InputByAdmin,
      //                                       InputBySalesman: ActivityAlasanPengecualianPengiriman.InputBySalesman}]);
      // },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd