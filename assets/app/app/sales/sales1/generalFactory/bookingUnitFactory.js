angular.module('app')
  .factory('BookingUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SNegotiationBookingUnitJoinProspectVehicleInfo');
        //console.log('res=>',res);
		
		// var json_Info_Kendaraan=[
    //     {UnitId: "1",  VehicleModelName: "Avanza",  Description: "1.5 G M/T", ColorName: "Red", Qty: "1"},
    //     {UnitId: "2",  VehicleModelName: "Innova",  Description: "1.5 G M/T",ColorName: "Red",  Qty: "3"},
    //     {UnitId: "3",  VehicleModelName: "Avanza",  Description: "1.5 G M/T", ColorName: "Red", Qty: "2"},
    //   ];
		//var res=json_Info_Kendaraan;
		
        return res;
      },
      create: function(bookingUnit) {
        return $http.post('/api/sales/SNegotiationBookingUnitJoinProspectVehicleInfo', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      update: function(bookingUnit){
        return $http.put('/api/sales/SNegotiationBookingUnitJoinProspectVehicleInfo', [{
                                            Id: negotiation.Id,
                                            //pid: negotiation.pid,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnitJoinProspectVehicleInfo',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });