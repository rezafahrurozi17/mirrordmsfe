angular.module('app')
  .factory('ProfileLeasing', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000');
        //console.log('res=>',res);
        return res;
      },
      create: function(ProfileLeasing) {
        return $http.post('/api/sales/MProfileLeasing', [{
											LeasingName:ProfileLeasing.LeasingName,
											ListLeasingTenor:ProfileLeasing.ListLeasingTenor
                                            }]);
      },
      update: function(ProfileLeasing){
        return $http.put('/api/sales/MProfileLeasing', [{
                      LeasingId:ProfileLeasing.LeasingId,
											OutletId:ProfileLeasing.OutletId,
											LeasingName:ProfileLeasing.LeasingName,
											ListLeasingTenor:ProfileLeasing.ListLeasingTenor
                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileLeasing',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd