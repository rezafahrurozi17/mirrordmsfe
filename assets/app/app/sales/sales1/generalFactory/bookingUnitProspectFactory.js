angular.module('app')
  .factory('BookingUnitProspectFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/SNegotiationBookingUnitJoinProspectView');
        //console.log('res=>',res);
		
		
        return res;
      },
      create: function(bookingUnit) {
        return $http.post('/api/sales/SNegotiationBookingUnitJoinProspectView', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      update: function(bookingUnit){
        return $http.put('/api/sales/SNegotiationBookingUnitJoinProspectView', [{
                                            Id: negotiation.Id,
                                            //pid: negotiation.pid,
                                            Name: negotiation.Name,
                                            Description: negotiation.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SNegotiationBookingUnitJoinProspectView',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });