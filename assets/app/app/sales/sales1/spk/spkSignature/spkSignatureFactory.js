angular.module('app')
    .factory('TemplateDigitalSignatureFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/fw/Role');
                //console.log('res=>',res);
                return res;
            },
            create: function(TemplateDigitalSignatureFactory) {
                return $http.post('/api/fw/Role', [{
                    //AppId: 1,
                    SalesProgramName: TemplateDigitalSignatureFactory.SalesProgramName
                }]);
            },
            update: function(TemplateDigitalSignatureFactory) {
                return $http.put('/api/fw/Role', [{
                    SalesProgramId: TemplateDigitalSignatureFactory.SalesProgramId,
                    SalesProgramName: TemplateDigitalSignatureFactory.SalesProgramName
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd