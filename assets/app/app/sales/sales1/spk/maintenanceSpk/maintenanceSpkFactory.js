angular.module('app')
    .factory('MaintenanceSpkFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(param) {
                //console.log("ini", "#"+param+"#");
                if (param == undefined || param == null || param == "") {
                    param = "";
                } else {
                    param = "&" + param;
                }
                var res = $http.get('/api/sales/SPKMaintainSPK/Filter?start=1&limit=10000' + param);
                return res;
                console.log("daata", res);
            },
            getDataAlasanHilang: function() {
                var res = $http.get('/api/sales/AdminHandlingSPKLost');
                return res;
            },
            getDataStatusSPK: function() {
                var res = $http.get('/api/sales/PStatusSpk');
                return res;
            },
            getSPKDetail: function(param) {
                var res = $http.get('/api/sales/SAHSPKDetail?SpkId=' + param);
                return res;
            },
            suratHilang: function(SpkTaking) {
                return $http.put('/api/sales/SAHSPKHilang', [{
                    SpkId: SpkTaking.SpkId,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    SpkLostNumber: SpkTaking.SpkLostNumber
                }]);
            },
            suratHilang: function(SpkTaking) {
                return $http.put('/api/sales/SAHSPKHilang', [{
                    SpkId: SpkTaking.SpkId,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    SpkLostNumber: SpkTaking.SpkLostNumber
                }]);
            },
            terimaSuratBatal: function(SpkTaking) {
                return $http.put('/api/sales/SAHSPKDocumentCancelReceiveBit', [{
                    SpkId: SpkTaking,
                }]);
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },
            delete: function(id) {
                return $http.delete('/api/drc', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });