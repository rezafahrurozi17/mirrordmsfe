angular.module('app')
    .controller('MaintenanceSpkController', function ( bsNotify, MobileHandling,$stateParams, $rootScope, $httpParamSerializer, $scope, $http, CurrentUser, ComboBoxFactory, DistributionSpkFactory, MaintenanceSpkFactory, $timeout, uiGridConstants, uiGridGroupingConstants) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.btnSuratKehilangan = true, $scope.btnPengembalianSpk = true, $scope.daftarSpk = true, $scope.lihatSPK = false, $scope.unitLihatView = false;
        var gridData = [];
        var mount = new Date();
        var year = mount.getFullYear();
        var mounth = mount.getMonth();
        var lastdate = new Date(year, mounth + 1, 0).getDate();
		
		function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':00:00';
                return fix;
            } else {
                return null;
            }
        };

        $scope.firstDate = new Date(mount.setDate("01"));
        $scope.lastDate = new Date(mount.setDate(lastdate));

        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};

        $scope.filter = { SpkStatusId: null, SalesId: null, dateFrom: $scope.firstDate, dateTo: new Date() };

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
              $scope.bv=MobileHandling.browserVer();
              $scope.bver=MobileHandling.getBrowserVer();
              //console.log("browserVer=>",$scope.bv);
              //console.log("browVersion=>",$scope.bver);
              //console.log("tab: ",$scope.tab);
              if($scope.bv!=="Chrome"){
              if ($scope.bv=="Unknown") {
                if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                {
                  $scope.removeTab($scope.tab.sref);
                }
              }        
              }
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.rightEnableByte = null;
        $scope.user = CurrentUser.user();
        $scope.breadcrums = {};
        $scope.tab = angular.copy($stateParams.tab);
        $scope.tinggi = window.screen.availHeight;
        $scope.lebar = window.screen.availWidth;

        $scope.gambar = {
            "max-width": 860,
            "max-height": 600
        }
        //console.log("menu",$scope.tabMaintenanceSPK);
        $scope.btnPengembalianSpk = true;
        $scope.btnSuratKehilangan = true;
        $scope.mmaintenanceSpk = null; //Model
        $scope.mSPK_Kehilangan = {};
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd/MM/yyyy'
        }

        $scope.dateOptionsmax = {
            startingDay: 1,
            format: 'dd/MM/yyyy'
        }

        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
        }

        $timeout(function () {
            console.log("rightEnableByte", $scope.rightEnableByte);
            checkBytes($scope.rightEnableByte);
            console.log("RIGHST =====>", $scope.RightsEnableForMenu);
        }, 1000)

        $scope.changeTgl = function (tgl) {
            $scope.dateOptionsmax.minDate = $scope.filter.dateFrom;
            if (tgl == null || tgl == undefined) {
                $scope.filter.dateTo = null;
            } else {
                if ($scope.filter.dateFrom < $scope.filter.dateTo) {

                } else {
                    $scope.filter.dateTo = $scope.filter.dateFrom;
                }

            }

        }

        $scope.NoSPK = {};

        $rootScope.$on("MintainSPKdariValid", function () {
            $scope.SpkId = $rootScope.SpkId;
            $scope.tab = $rootScope.tabSPKValid;
            var test = angular.copy($rootScope.tabSPKValid);
            console.log("qweqw3", test);
            
            $scope.lihatspk($scope.SpkId);

        });

        $scope.unitLihat = function () {
            if ($scope.unitLihatView == true) {
                $scope.unitLihatView = false;
            } else {
                $scope.unitLihatView = true;
            }
        }

        

        $scope.lihatspk = function (SpkId) {
            MaintenanceSpkFactory.getSPKDetail(SpkId).then(function (res) {
                $scope.NoSPK = res.data.Result[0];
                $scope.breadcrums.title="Lihat";
                $scope.tab = angular.copy($rootScope.tabSPKValid);
                $scope.detailSpk();
                setDefaultTab();
            })
        }

        $scope.detailSpk = function () {
           
            $scope.ViewDataSPK = $scope.NoSPK;
            //$scope.IndividuPembeli = true;
            $scope.lihatSPK = true, $scope.daftarSpk = false;
            $scope.KatgoriKoresponden = [];
            for (var i in $scope.NoSPK.ListInfoPelanggan) {
                $scope.KatgoriKoresponden.push({
                    CustomerCorrespondentId: $scope.NoSPK.ListInfoPelanggan[i].CustomerCorrespondentId,
                    CustomerCorrespondentName: $scope.NoSPK.ListInfoPelanggan[i].CustomerCorrespondentName
                });
            }

            var seqDoc = 0;
            $scope.gridDocument.data = [];
            for (var i in $scope.NoSPK.ListInfoDocument) {
                seqDoc++
                $scope.gridDocument.data.push({
                    seqDoc: seqDoc,
                    DocumentDataName: $scope.NoSPK.ListInfoDocument[i].DocumentDataName,
                    DocumentName: $scope.NoSPK.ListInfoDocument[i].DocumentName,
                    DocumentType: $scope.NoSPK.ListInfoDocument[i].DocumentType,
                    DocumentURL: $scope.NoSPK.ListInfoDocument[i].DocumentURL,
                    DocumentData: $scope.NoSPK.ListInfoDocument[i].DocumentData,
                    StatusDocumentName: $scope.NoSPK.ListInfoDocument[i].StatusDocumentName
                });
            }

            var seq = 0;
            $scope.gridinfoKendaraan.data = [];
            for (var i in $scope.NoSPK.ListInfoUnit) {
                seq++;
                $scope.gridinfoKendaraan.data.push({
                    NoItem: seq,
                    ModelType: $scope.NoSPK.ListInfoUnit[i].ModelType,
                    Qty: $scope.NoSPK.ListInfoUnit[i].Qty,
                    ColorName: $scope.NoSPK.ListInfoUnit[i].ColorName,
                    ColorCodeName: $scope.NoSPK.ListInfoUnit[i].ColorCodeName,
                    SpecialReqNumberPlate: $scope.NoSPK.ListInfoUnit[i].SpecialReqNumberPlate,
                    NumberPlateName: $scope.NoSPK.ListInfoUnit[i].NumberPlateName,
                    StatusStockName: $scope.NoSPK.ListInfoUnit[i].StatusStockName,
                    ProductionYear: $scope.NoSPK.ListInfoUnit[i].ProductionYear,
                    ListDetailUnit: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit
                });
            }

            $scope.NoSPK.ListInfoAccessoriesPckgWeb = [];
            for (var i in $scope.NoSPK.ListInfoUnit) {
                for (var j in $scope.NoSPK.ListInfoUnit[i].ListDetailUnit) {
                    for (var k in $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage) {
                        $scope.NoSPK.ListInfoAccessoriesPckgWeb.push({
                            Description: $scope.NoSPK.ListInfoUnit[i].ModelType,
                            AccessoriesPackageCode: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].AccessoriesPackageCode,
                            AccessoriesPackageName: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].AccessoriesPackageName,
                            Price: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Price * $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Qty,
                            Qty: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Qty,
                            IsDiscount: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].IsDiscount,
                            DetailAcc: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].ListDetail,
                            Discount: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Discount,
                        });
                    }
                }
            }
            $scope.gridAksesorisPackage.data = $scope.NoSPK.ListInfoAccessoriesPckgWeb;

            $scope.NoSPK.ListInfoAccessoriesWeb = [];
            for (var i in $scope.NoSPK.ListInfoUnit) {
                for (var j in $scope.NoSPK.ListInfoUnit[i].ListDetailUnit) {
                    for (var k in $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories) {
                        $scope.NoSPK.ListInfoAccessoriesWeb.push({
                            ModelType: $scope.NoSPK.ListInfoUnit[i].ModelType,
                            AccessoriesCode: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].AccessoriesCode,
                            AccessoriesName: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].AccessoriesName,
                            Price: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].Price * $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].Qty ,
                            Qty: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].Qty,
                            IsDiscount : $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].IsDiscount,
                            StatusAccName: $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].StatusAccName,
                            Discount : $scope.NoSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].Discount
                        })
                    }
                }
            }
            $scope.gridAksesoris.data = $scope.NoSPK.ListInfoAccessoriesWeb;
            console.log('$scope.gridAksesoris',$scope.gridAksesoris.data);

            var seqJaminan = 0;
            $scope.gridPerluasanJaminan.data = [];
            for (var i in $scope.NoSPK.ListInfoPerluasanJaminan) {
                seqJaminan++;
                $scope.gridPerluasanJaminan.data.push({
                    No: seqJaminan,
                    InsuranceExtensionName: $scope.NoSPK.ListInfoPerluasanJaminan[i].InsuranceExtensionName
                });
            }
        }

        $scope.lihatRRN = function (row) {
            for (var i in row.ListDetailUnit) {
                row.ListDetailUnit[i].ProductionYear = row.ProductionYear
            }
            var number = angular.element('.ui.modal.ModalFormRRN').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalFormRRN').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.ModalFormRRN').not(':first').remove();
            }
            angular.element('.ui.modal.ModalFormRRN').modal('show');
            $scope.gridNoRangka.data = row.ListDetailUnit;
        }

        $scope.keluarNORangaka = function () {
            angular.element('.ui.modal.ModalFormRRN').modal('hide');
        }

        ComboBoxFactory.getDataInsuranceUserType().then(function (res) { $scope.insuranceusertype = res.data.Result; });
        ComboBoxFactory.getJenisPembayaran().then(function (res) { $scope.paymentType = res.data.Result });

        ComboBoxFactory.getDataKategori().then(function (res) {
            var tempCategory = res.data.Result;
            $scope.kategoriPelanggan = tempCategory.filter(function (type) {
                return (type.ParentId == 2 || type.CustomerTypeId == 3);
            })
        });

        DistributionSpkFactory.getDataSales().then(function (res) {
            $scope.salesname = res.data.Result;
        });

        MaintenanceSpkFactory.getDataStatusSPK().then(function (res) {
            $scope.masterStatusSPK = res.data.Result;
        });

        $scope.changeInfoPelangganubah = function (selected) {
            if ($scope.ViewDataSPK.CustomerTypeDesc == "Individu" &&
                selected == 1) {
                $scope.IndividuPembeli = true;
                $scope.individuPemilik = false;
                $scope.perusahaanPembeli = false, $scope.perusahaanPenanggungJawab = false, $scope.perusahaanPemilik = false;
            } else if ($scope.ViewDataSPK.CustomerTypeDesc == "Individu" &&
                selected == 3) {
                $scope.individuPemilik = true;
                $scope.IndividuPembeli = false;
                $scope.perusahaanPembeli = false, $scope.perusahaanPenanggungJawab = false, $scope.perusahaanPemilik = false;
                // } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected == 1) {
                //     $scope.perusahaanPembeli = true, $scope.perusahaanPenanggungJawab = false, $scope.perusahaanPemilik = false;
                //     $scope.IndividuPembeli = false;
                //     $scope.individuPemilik = false;
                // } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected == 2) {
                //     $scope.perusahaanPenanggungJawab = true, $scope.perusahaanPembeli = false, $scope.perusahaanPemilik = false;
                //     $scope.IndividuPembeli = false;
                //     $scope.individuPemilik = false;
                // } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
                //         $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected == 3) {
                //     $scope.perusahaanPemilik = true, $scope.perusahaanPembeli = false, $scope.perusahaanPenanggungJawab = false;
                //     $scope.IndividuPembeli = false;
                //     $scope.individuPemilik = false;
                // } else {
            } else if ($scope.ViewDataSPK.CustomerTypeDesc != "Individu" && selected == 1) {
                $scope.perusahaanPembeli = true, $scope.perusahaanPenanggungJawab = false, $scope.perusahaanPemilik = false;
                $scope.IndividuPembeli = false;
                $scope.individuPemilik = false;
            } else if ($scope.ViewDataSPK.CustomerTypeDesc != "Individu" && selected == 2) {
                $scope.perusahaanPenanggungJawab = true, $scope.perusahaanPembeli = false, $scope.perusahaanPemilik = false;
                $scope.IndividuPembeli = false;
                $scope.individuPemilik = false;
            } else if ($scope.ViewDataSPK.CustomerTypeDesc != "Individu" && selected == 3) {
                $scope.perusahaanPemilik = true, $scope.perusahaanPembeli = false, $scope.perusahaanPenanggungJawab = false;
                $scope.IndividuPembeli = false;
                $scope.individuPemilik = false;
            } else {
                // kosong();
            }
        }

        // $scope.changeInfoPelanggan = function(selected) {
        //     console.log("asdasda", selected);
        //     if ($scope.ViewDataSPK.CustomerTypeDesc == "Individu" &&
        //         selected.CustomerCorrespondentName == "Pembeli") {
        //         $scope.IndividuPembeli = true;
        //         $scope.individuPemilik = false;
        //     } else if ($scope.ViewDataSPK.CustomerTypeDesc == "Individu" &&
        //         selected.CustomerCorrespondentName == "Pemilik") {
        //         $scope.individuPemilik = true;
        //         $scope.IndividuPembeli = false;
        //     } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected.CustomerCorrespondentName == "Pembeli") {
        //         $scope.perusahaanPembeli = true, $scope.perusahaanPenanggungJawab = false, $scope.perusahaanPemilik = false;
        //     } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected.CustomerCorrespondentName == "Penanggung Jawab") {
        //         $scope.perusahaanPenanggungJawab = true, $scope.perusahaanPembeli = false, $scope.perusahaanPemilik = false;
        //     } else if (($scope.ViewDataSPK.CustomerTypeDesc == "Perusahaan" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Pemerintah" ||
        //             $scope.ViewDataSPK.CustomerTypeDesc == "Yayasan") && selected.CustomerCorrespondentName == "Pemilik") {
        //         $scope.perusahaanPemilik = true, $scope.perusahaanPembeli = false, $scope.perusahaanPenanggungJawab = false;
        //     } else {
        //         // kosong();
        //     }
        // }

        //--------------------------------
        // Document
        //--------------------------------

        var viewDocument = '<i ng-hide="row.entity.StatusDocumentName == \'Empty\'" title="View" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.viewDocument(row.entity)" ></i>';

        $scope.status = false;

        $scope.viewDocument = function (row) {
            MaintenanceSpkFactory.getImage(row.DocumentURL).then(function (res) {
                console.log("aasdsa", res.data);
                $scope.viewdocument = res.data.UpDocObj;
                $scope.viewdocumentName = res.data.FileName;
                $scope.status = true;
                callModal();
            })

        }

        function callModal() {
            var number = angular.element('.ui.modal.dokumenSPKView').length;
            setTimeout(function () {
                angular.element('.ui.modal.dokumenSPKView').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.dokumenSPKView').not(':first').remove();
            }
            //$scope.gridAksesorisDetail.data = data.DetailAcc;
            //angular.element('.ui.modal.modalAccPackageDetail').modal('show');
            angular.element('.ui.modal.dokumenSPKView').modal({ closable: false }).modal('show');
        }

        $scope.keluarImg = function () {
            $scope.status = false;
            angular.element('.ui.modal.dokumenSPKView').modal('hide');
        }

        MaintenanceSpkFactory.getDataAlasanHilang().then(function (res) {
            $scope.alasanhilang = res.data.Result;
        })


        $scope.tutup = function () {
            $scope.lihatSPK = false;
            $scope.daftarSpk = true;
            $rootScope.$emit('KembaliMintainSPKdariValid', {});
        }

        $scope.modalSuratKehilangan = function () {
            angular.element('.ui.modal.ModalFormSuratHilang').modal('show');
        }

        $scope.modalKeluar = function () {
            angular.element('.ui.modal.ModalFormSuratHilang').modal("refresh").modal('hide');
        }


        $scope.btnTerimaSpk = function () {
            MaintenanceSpkFactory.terimaSuratBatal($scope.selected_rows[0].SpkId).then(function (res) {
                angular.element('.ui.modal.ModalFormTerima').modal('show');
                angular.element('.ui.modal.ModalFormTerimaSpk').modal('hide');
            });
        }

        $scope.btnTidakTerima = function () {
            angular.element('.ui.modal.ModalFormTerimaSpk').modal('hide');
        }

        $scope.btnbatalkehilanganSpk = function () {
            angular.element('.ui.modal.ModalFormSuratHilang').modal('hide');
        }

        $scope.btnOk = function () {
            angular.element('.ui.modal.ModalFormTerima').modal('hide');
        }

        //----------------------------------
        // Get Data
        //----------------------------------

        $scope.Category = function () {
            if ($scope.mKategori == 1) {
                $scope.kategoi = $scope.categoryIndividu;
            } else if ($scope.mKategori == 2) {
                $scope.kategoi = $scope.categoryInstansi;
            } else {
                $scope.kategoi = null;
            }
            return $scope.kategoi;
        };



        $scope.getData = function () {
            $scope.loading = true;
            var param = null;
			
			$scope.filter.dateFrom=fixDate($scope.filter.dateFrom);
			$scope.filter.dateTo=fixDate($scope.filter.dateTo);

            param = $httpParamSerializer($scope.filter);
            MaintenanceSpkFactory.getData(param).then(function (res) {
                $scope.loading = false;
                $scope.grid.data = res.data.Result;
            })
        }

        var list_data = MaintenanceSpkFactory.getData();
        $scope.Dokumen_List_To_Repeat = list_data;

        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
            $scope.selected_rows = rows;
            if ($scope.selected_rows.length == 0) {
                $scope.btnPengembalianSpk = true;
                $scope.btnSuratKehilangan = true;
            } else {
                for (var a = 0; a < $scope.selected_rows.length; a++) {
                    if ($scope.selected_rows[a].SPKStatusName == "Didistribusikan") {
                        $scope.btnPengembalianSpk = true;
                        $scope.btnSuratKehilangan = false;
                    } else if ($scope.selected_rows[a].SPKStatusName == "Batal" && $scope.selected_rows[a].SPKDocumentCancelReceiveBit != true) {
                        $scope.btnPengembalianSpk = false;
                        $scope.btnSuratKehilangan = true;
                    } else {
                        $scope.btnPengembalianSpk = true;
                        $scope.btnSuratKehilangan = true;
                    }

                }
            }

        }

        $scope.detail = function (data) {
            console.log(data);
        }


        $scope.statusSpk = function (data) {
            if (data.SPKStatusName == "Didistribusikan") {
                $scope.filter.dateFrom = null;
                $scope.filter.dateTo = null;
            }
        }

        $scope.btnkehilanganSpk = function () {
            $scope.mSPK_Kehilangan.SpkId = $scope.selected_rows[0].SpkId;
            MaintenanceSpkFactory.suratHilang($scope.mSPK_Kehilangan).then(function (res) {
                angular.element('.ui.modal.ModalFormSuratHilang').modal('hide');
                $scope.mSPK_Kehilangan = {};
                bsNotify.show({
                    size: 'big',
                    type: 'succes',
                    timeout: 2000,
                    title: "Sudah Masuk Approval",
                });
                $scope.getData();
            })
        }



        $scope.modalPenegembalianSpk = function () {
            angular.element('.ui.modal.ModalFormTerimaSpk').modal('show');
        }

        var btnActionLihatDetail = '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.lihatspk(row.entity.SpkId)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>'

        // var btnActionLihatDetail = '<a style="color:blue;"> <p style="padding:5px 0 0 5px"> \
        //                                 <u ng-click="grid.appScope.$parent.lihatspk(row.entity.SpkId); pelanggan()">Lihat</u></p> \
        //                             </a>';

        // var asdf = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.detail(rowHeader.entity)">Detil</u></p></a>';

        var accpackage = '<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-if="!row.groupHeader" ng-click="grid.appScope.detailaccpacket(row.entity)">Detil</u></p></a>';

        var btnLihatNoRangka = '<a style="color:blue;" onclick=""; ng-click=""><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.lihatRRN(row.entity)">Lihat No Rangka/RRN</u></p></a>';

        $scope.detailaccpacket = function (data) {
            console.log('datanya detail', data);
            var number = angular.element('.ui.modal.modalAccPackageDetail').length;
            setTimeout(function () {
                angular.element('.ui.modal.modalAccPackageDetail').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.modalAccPackageDetail').not(':first').remove();
            }
            $scope.gridAksesorisDetail.data = data.DetailAcc;
            console.log('$scope.gridAksesorisDetail.data',$scope.gridAksesorisDetail.data);
            angular.element('.ui.modal.modalAccPackageDetail').modal('show');
        }

        $scope.modalAccPackageDetail = function () {
            angular.element('.ui.modal.modalAccPackageDetail').modal('hide');
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { displayName:'No SPK', name: 'no spk', field: 'FormSPKNo', width: '15%' },
                { displayName:'Tanggal SPK', name: 'tanggal spk', width: '10%', field: 'SpkDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { displayName:'Status SPK', width: '12%' , name: 'status spk', field: 'SPKStatusName' },
                { name: 'sales', field: 'SalesName' },
                { name: 'kode prospek', field: 'ProspectCode', width: '12%' },
                { name: 'nama prospek/Pelanggan', field: 'ProspectName', width: '18%' },
                {
                    name: 'action', width: '8%',
                    allowCellFocus: false,
                    visible:true,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: btnActionLihatDetail
                }
            ]
        };

        $scope.gridinfoKendaraan = {
            enableSorting: true,
            enableRowSelection: false,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataInfokendaraan',
            columnDefs: [
                { displayName:'No.' ,name: 'No.', field: 'NoItem', width: '5%' },
                { name: 'model tipe', field: 'ModelType', width: '30%' },
                { name: 'quantity', field: 'Qty', width: '7%' },
                { name: 'warna', field: 'ColorCodeName', width: '23%' },
                //{ name: 'plat khusus', field: 'SpecialReqNumberPlate', width: '12%' },
                { name: 'kategori plat nomor', field: 'NumberPlateName', width: '11%' },
                //{ name: 'stock status', field: 'StatusStockName', width: '10%' },
                {
                    name: 'action',
                    width: '14%',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: btnLihatNoRangka
                }

            ]
        };

        $scope.gridNoRangka = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataInfokendaraan',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { displayName: 'RRN',name: 'rrn', field: 'RRNNo', width: '20%' },
                { name: 'no rangka', field: 'FrameNo', width: '20%' },
                { name: 'Plat no khusus', field:'SpecialReqNumberPlate' , width: '20%'},
                { name: 'tahun', field: 'ProductionYear', width: '20%' },
                { name: 'status match', field: 'StatusMatchName', width: '20%' },
            ]
        };

        $scope.gridDocument = {
            enableSorting: true,
            enableColumnResizing: true,
            paginationPageSizes: [10, 25, 50],
            //showTreeExpandNoChildren: true,
            columnDefs: [
                { name: 'Id', field: 'Id', visible: false },
                { name: '#', field: 'seqDoc', width: '10%' },
                { name: 'tipe dokumen', field: 'DocumentType' },
                { name: 'dokumen', cellTemplate: viewDocument },
                { name: 'status', field: 'StatusDocumentName' }
            ]
        };

        $scope.gridPerluasanJaminan = {
            enableSorting: true,
            enableRowSelection: true,
            enableColumnResizing: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataInfokendaraan',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: '#', field: 'No', width: '10%' },
                { name: 'Perluasan Jaminan', field: 'InsuranceExtensionName', width: '90%' },
            ]
        };


        var freeACC = 
        $scope.gridAksesorisPackage = {
            multiSelect: false,
            //enableSelectAll: false,
            enableColumnMenus: true,
            enableColumnResizing: true,
            //enableRowSelection: false,
            enableSorting: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                {
                    name: 'model tipe',
                    field: 'Description',
                    width: '40%',
                    pinnedLeft: true,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'No. Material', field: 'AccessoriesPackageCode' },
                { name: 'Nama Material', field: 'AccessoriesPackageName' },
                { name: 'qty', field: 'Qty', width: '10%' },
                { name: 'Diskon',field: 'Discount', cellFilter: 'currency:"Rp."'} ,
                // { name: 'Free', cellTemplate: '<input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" name="approval" ng-disabled="true" ng-checked="row.entity.IsDiscount == true">' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price',
                    width: '20%',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                {
                    name: 'Detil Aksesoris',
                    cellTemplate: accpackage

                }
            ],
        };

        $scope.gridAksesoris = {
            // enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            //enableSelectAll: false,
            enableColumnMenus: true,
            enableColumnResizing: true,
            //enableRowSelection: false,
            enableGroupHeaderSelection: true,
            enableRowHeaderSelection: true,
            enableSorting: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataAksesoris',
            columnDefs: [
               // { name: 'id', field: 'Id', visible: false },
                {
                    name: 'model tipe',
                    field: 'ModelType',
                    width: '40%',
                    pinnedLeft: true,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'No. Material', field: 'AccessoriesCode' },
                { name: 'Nama Material', field: 'AccessoriesName' },
                { name: 'qty', field: 'Qty', width: '10%', treeAggregationType: uiGridGroupingConstants.aggregation.SUM },
                { name: 'status', field: 'StatusAccName' },
                { name: 'Diskon', field: 'Discount', cellFilter: 'currency:"Rp."' },
                // Rollback SPK Galang
                // { name: 'Free', cellTemplate: '<input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" name="approval" ng-disabled="true" ng-checked="row.entity.IsDiscount == true">' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price',
                    width: '20%',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                }
            ],
        };

        $scope.gridAksesorisDetail = {
            // enableSorting: true,
            enableRowSelection: true,
            enableColumnMenus: true,
            enableSorting: true,
            enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: 'No. Material', field: 'AccessoriesCode' },
                { name: 'Nama Material', field: 'AccessoriesName' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price', attributes: '{style:"text-align:right;"}'
                }
            ],
        };

        $scope.gridAksesoris.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                var msg = 'row selected ' + row.isSelected;
                //$scope.selctedRow = gridApi.selection.getSelectedRows();
                if (row.internalRow == true && row.isSelected == true) {
                    var childRows = row.treeNode.children;
                    for (var j = 0, length = childRows.length; j < length; j++) {
                        var rowEntity = childRows[j].row.entity;
                        $scope.selctedRow = gridApi.selection.getSelectedRows(rowEntity);
                    }
                }

                if (row.internalRow == true && row.isSelected == false) {
                    var childRows = row.treeNode.children;
                    for (var j = 0, length = childRows.length; j < length; j++) {
                        var rowEntity = childRows[j].row.entity;
                        $scope.selctedRow = gridApi.selection.unSelectRow(rowEntity);
                    }
                }

            });

        };
    
        function setDefaultTab() {
            angular.element("#infoPelanggan_tab").addClass("active");
            angular.element("#dokumen_tab").removeClass("active").addClass("");
            angular.element("#infoPengiriman_tab").removeClass("active").addClass("");
            angular.element("#infoKendaraan_tab").removeClass("active").addClass("");
            angular.element("#aksesoris_tab").removeClass("active").addClass("");
            angular.element("#karoseri_tab").removeClass("active").addClass("");
            angular.element("#infoLeasing_tab").removeClass("active").addClass("");
            angular.element("#infoAsuransi_tab").removeClass("active").addClass("");
            angular.element("#rincianHarga_tab").removeClass("active").addClass("");
            angular.element("#infoMediator_tab").removeClass("active").addClass("");
            angular.element("#infoPemakai_tab").removeClass("active").addClass("");

            angular.element("#infoPelanggan").removeClass("tab-pane fade").addClass("tab-pane fade in active");
            angular.element("#dokumen").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPengiriman").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoKendaraan").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#aksesoris").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#karoseri").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoLeasing").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoAsuransi").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#rincianHarga").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoMediator").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPemakai").removeClass("tab-pane fade in active").addClass("tab-pane fade");
        }

    });
