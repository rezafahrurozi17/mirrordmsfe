angular.module('app')
    .controller('ApprovalOffTheRoadController', function($scope, $http, CurrentUser, ApprovalOffTheRoad,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mApprovalOffTheRoad = null; //Model
	$scope.xRole={selected:[]};
	console.log('$scope.user',$scope.user);

    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
        ApprovalOffTheRoad.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.doCustomSave = function (mdl,mode) {

		console.log('doCustomSave3',mode);//create update
		
		if(mode=="create")
		{
			ApprovalOffTheRoad.create($scope.mApprovalOffTheRoad).then(
				function(res) {
					ApprovalOffTheRoad.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if(mode=="update")
		{
			ApprovalOffTheRoad.update($scope.mApprovalOffTheRoad).then(
				function(res) {
					ApprovalOffTheRoad.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'No. SPK',    field:'CustomerReligionId'},
            { name:'Tangal SPK', field:'CustomerReligionName' },
			{ name:'Nama Pelanggan', field:'CustomerReligionName' },
			{ name:'Model', field:'VehicleModelName' },
			{ name:'Tipe', field:'Description' },
			{ name:'Tahun', field:'VehicleColorName' },
			{ name:'Nama Salesman', field:'VehicleColorName' },
			{ name:'Pesan', field:'VehicleColorName' },
			{ name:'Status', field:'VehicleColorName' }
        ]
    };
});


