angular.module('app')
  .factory('ApprovalOffTheRoad', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/CCustomerReligion');
        return res;
      },
      create: function(ApprovalOffTheRoad) {
        return $http.post('/api/sales/CCustomerReligion', [{
                                            //AppId: 1,
                                            CustomerReligionName: ApprovalOffTheRoad.CustomerReligionName}]);
      },
      update: function(ApprovalOffTheRoad){
        return $http.put('/api/sales/CCustomerReligion', [{
                                            CustomerReligionId: ApprovalOffTheRoad.CustomerReligionId,
                                            CustomerReligionName: ApprovalOffTheRoad.CustomerReligionName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd