angular.module('app')
  .factory('ApprovalDiskonSpkFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
    getData: function(limit) {
      var param = 5
      if(limit !== undefined && limit !== null){
        param = limit;
      }
		// var res=$http.get('/api/sales/SpkApprovalDiscount?start=1&limit=5&FilterData=HeaderStatusApprovalName|Diajukan');
		var res=$http.get('/api/sales/SpkApprovalDiscount?start=1&limit='+param);
		return res;
    },

    getDataOTR: function() {
    var res = $http.get('/api/sales/SPK_ApprovalOffTheRoad?start=1&limit=5');
		return res;
    },

    
    
	  
	  getDaRincian: function(spkId) {
		var res=$http.get('/api/sales/SAHSPKDetail/?spkId='+spkId);
		return res;
    },
	  
	  getDataIncrement: function(Start,Limit) {
      // var res = $http.get('/api/sales/SpkApprovalDiscount?start=' + Start + '&limit=' + Limit + '&FilterData=HeaderStatusApprovalName|Diajukan');
      var res = $http.get('/api/sales/SpkApprovalDiscount?start=' + Start + '&limit=' + Limit );
		return res;
    },

    getDataIncrementOTR: function(Start,Limit) {
      var res = $http.get('/api/sales/SPK_ApprovalOffTheRoad?start='+Start+'&limit='+Limit);
		return res;
    },



	  
	  getDataSearchApprovalDiskonSpkIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/SpkApprovalDiscount/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
    },

    getDataSearchApprovalOTRSpkIncrement: function(Start,limit,SearchBy,SearchValue) {
      var res = $http.get('/api/sales/SPK_ApprovalOffTheRoad/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
    },

	  getDataIncrementLimit: function () {
        
        var da_json = [
        { SearchOptionId: 5, SearchOptionName: "5" },
          { SearchOptionId: 10, SearchOptionName: "10" },
          { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res=da_json

        return res;
      },

      getDataIncrementOTRLimit: function () {

        var da_json = [
          { SearchOptionId: 5, SearchOptionName: "5" },
          { SearchOptionId: 10, SearchOptionName: "10" },
          { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res = da_json

        return res;
      },
	  
	  GetApprovalDiskonSpkSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "FormSPKNo", SearchOptionName: "No SPK" },
		  { SearchOptionId: "ProspectName", SearchOptionName: "Nama pelanggan" },
		  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Model tipe" },
		  { SearchOptionId: "Salesman", SearchOptionName: "Sales" },
		  { SearchOptionId: "HeaderStatusApprovalName", SearchOptionName: "Status" },
		  { SearchOptionId: "Discount", SearchOptionName: "Discount" }
        ];

        var res=da_json

        return res;
    },

    GetApprovalOTRSpkSearchDropdownOptions: function () {

      var da_json = [
        { SearchOptionId: "FormSPKNo", SearchOptionName: "No SPK" },
        { SearchOptionId: "CustomerName", SearchOptionName: "Nama pelanggan" },
        { SearchOptionId: "Description", SearchOptionName: "Model tipe" },
        { SearchOptionId: "Salesman", SearchOptionName: "Sales" },
        { SearchOptionId: "StatusApprovalName", SearchOptionName: "Status" },
      ];

      var res = da_json

      return res;
    },
	  
	  getDataSearchApprovalDiskonSpk: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/SpkApprovalDiscount/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
    },
    

    getDataSearchApprovalOTRSpk: function (limit, SearchBy, SearchValue) {
      var res = $http.get('/api/sales/SPK_ApprovalOffTheRoad/?start=1&limit=' + limit + '&FilterData=' + SearchBy + '|' + SearchValue);
      return res;
    },
    
    // =============================================
	  SetujuApprovalDiskonSpk: function(ApprovalDiskonSpk){
		return $http.put('/api/sales/SpkApprovalDiscount/Approve', [ApprovalDiskonSpk]);									
    },
	  
	  SetujuApprovalDiskonSpkDariList: function(ApprovalDiskonSpk){
		return $http.put('/api/sales/SpkApprovalDiscount/Approve', ApprovalDiskonSpk);									
    },
	  
	  TidakSetujuApprovalDiskonSpk: function(ApprovalDiskonSpk){
		return $http.put('/api/sales/SpkApprovalDiscount/Reject', [ApprovalDiskonSpk]);									
    },
	  TidakSetujuApprovalDiskonSpkDariList: function(ApprovalDiskonSpk){
		return $http.put('/api/sales/SpkApprovalDiscount/Reject', ApprovalDiskonSpk);									
    },
	  
    create: function(ApprovalDiskonSpk) {
      return $http.post('/api/sales/SpkApprovalDiscount', [{
                                          SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
    },
    update: function(ApprovalDiskonSpk){
      return $http.put('/api/sales/SpkApprovalDiscount', [{
      SalesProgramId: ApprovalDiskonSpk.SalesProgramId,
      SalesProgramName: ApprovalDiskonSpk.SalesProgramName}]);
    },
    delete: function(id) {
      return $http.delete('/api/sales/SpkApprovalDiscount',{data:id,headers: {'Content-Type': 'application/json'}});
    },
    // ========================================================
      SetujuApprovalOTRSpk: function (ApprovalOTRSpk) {
        return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Approval', [ApprovalOTRSpk]);
      },

      SetujuApprovalOTRSpkDariList: function (ApprovalOTRSpk) {
        return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Approval', ApprovalOTRSpk);
        // return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Approval',[{
        //   StatusApprovalId: ApprovalOTRSpk.StatusApprovalId
        // }]);
      },

      TidakSetujuApprovalOTRSpk: function (ApprovalOTRSpk) {
        return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Approval', ApprovalOTRSpk);
        // return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Reject', [{
        //   StatusApprovalId: ApprovalOTRSpk.StatusApprovalId,
        //   RejectReason: ApprovalOTRSpk.RejectReason
        // }]);
      },
      TidakSetujuApprovalOTRSpkDariList: function (ApprovalOTRSpk) {
        return $http.put('/api/sales/SPK_ApprovalOffTheRoad/Approval', ApprovalOTRSpk);
      },

      create: function (ApprovalOTRSpk) {
        return $http.post('/api/sales/SPK_ApprovalOffTheRoad', [{
          SalesProgramName: ApprovalOTRSpk.SalesProgramName
        }]);
      },
      update: function (ApprovalOTRSpk) {
        return $http.put('/api/sales/SPK_ApprovalOffTheRoad', [{
          SalesProgramId: ApprovalOTRSpk.SalesProgramId,
          SalesProgramName: ApprovalOTRSpk.SalesProgramName
        }]);
      },
      delete: function (id) {
        return $http.delete('/api/sales/SPK_ApprovalOffTheRoad', { data: id, headers: { 'Content-Type': 'application/json' } });
      },


      getHistoryOtr : function (SpkId) {
        var res = $http.get('/api/sales/Spk_ApprovalOffTheRoad/GetAll/?SpkId=' + SpkId);
        return res;
        
      }




    }
  });
