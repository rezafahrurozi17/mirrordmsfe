angular.module('app')
.controller('ApprovalDiskonSpkController', function($scope, $http, CurrentUser, ApprovalDiskonSpkFactory,$timeout,bsNotify) {
	$scope.ApprovalDiskonSpkMain=true;

	$scope.ApprovalDiskonSpkMultipleReject=false;
	$scope.ApprovalDiskonSpkSearchCriteriaStore="";
	$scope.ApprovalDiskonSpkSearchValueStore="";
	
	$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval_PanelIcon = "+";
	$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga_PanelIcon = "+";
	$scope.ApprovalDiskonSpkHideIncrement=false;
	$scope.selectionApprovalOTRSpk = [];

	$scope.ApprovalDiskonSpkDetail = false;
	$scope.ApprovalOTRSpkDetail = false;

	$scope.ApprovalDiskonSpkFilter = true;
	$scope.ApprovalOTRSpkFilter = false;
	$scope.mApprovalDiskonSpk = {};
	$scope.mApprovalDiskonSpk.RejectReason = '';
	$scope.mApprovalOTRSpk = {};
	$scope.mApprovalOTRSpk.RejectReason = '';

	$scope.ApprovalDiskonSpkSearchValue = "";

	$scope.user = CurrentUser.user();

	$scope.filter={};
	$scope.filter.limit = 5;

	
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').remove();
				angular.element('.ui.modal.ModalActionApprovalDiskonSpk').remove();
				angular.element('.ui.modal.ModalActionApprovalOTRSpk').remove();

		});

	
	
	$scope.ApprovalDiskonSpkSearchIsPressed=false;
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.optionApprovalDiskonSpkSearchCriteria = ApprovalDiskonSpkFactory.GetApprovalDiskonSpkSearchDropdownOptions();
		$scope.ApprovalDiskonSpkSearchCriteria = $scope.optionApprovalDiskonSpkSearchCriteria[0].SearchOptionId;

		$scope.optionApprovalOTRSpkSearchCriteria = ApprovalDiskonSpkFactory.GetApprovalOTRSpkSearchDropdownOptions();
	 $scope.ApprovalOTRSpkSearchCriteria = $scope.optionApprovalOTRSpkSearchCriteria[0].SearchOptionId;


		$scope.optionApprovalDiskonSpkSearchLimit = ApprovalDiskonSpkFactory.getDataIncrementLimit();
		$scope.filter.limit = $scope.optionApprovalDiskonSpkSearchLimit[0].SearchOptionId; 

	$scope.optionApprovalOTRSpkSearchLimit = ApprovalDiskonSpkFactory.getDataIncrementOTRLimit();
	$scope.ApprovalOTRSpkSearchLimit = $scope.optionApprovalOTRSpkSearchLimit[0].SearchOptionId; 




		$scope.xRole = { selected: [] };
		$scope.ApprovalOTRSpk = [];



		//----------------------------------
		// Get Data
		//----------------------------------

		var gridData = [];

		ApprovalDiskonSpkFactory.getData().then(
			function(res){	
					$scope.ApprovalDiskonSpkSearchIsPressed=false;
					$scope.ApprovalDiskonSpkGabungan = res.data.Result;

					for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
					{
						for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
						{
							if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
							{
								$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
								$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
							}
						}
					}
					if(res.data.Total<=5)
					{
						$scope.ApprovalDiskonSpkHideIncrement=true;
					}
					
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
		);

		ApprovalDiskonSpkFactory.getDataOTR().then(function (res) {
					$scope.ApprovalOTRSpkSearchIsPressed = false;
					$scope.ApprovalOTRSpk = res.data.Result;
					console.log('$scope.ApprovalOTRSpk ====>', $scope.ApprovalOTRSpk);

					// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
					// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
					// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
					// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
					// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
					// 		}
					// 	}
					// }
					if (res.data.Total <= 5) {
						$scope.ApprovalOTRSpkHideIncrement = true;
					}
					return res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
		);
		
	$scope.ApprovalDiskonSpkRefresh = function () {
		$scope.ApprovalDiskonSpkHideIncrement=false;
		console.log('filter.limit ===',$scope.filter.limit);
		ApprovalDiskonSpkFactory.getData($scope.filter.limit).then(
			function(res){
				
				$scope.ApprovalDiskonSpkSearchIsPressed=false;
				$scope.ApprovalDiskonSpkGabungan = res.data.Result;
				
				for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
				{
					for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
					{
						if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
						{
							$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
							$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						}
						
					}
				}
				$scope.ApprovalDiskonSpkSearchValue = "";
				$scope.ResetSelection();
				
				if(res.data.Total<=5)
				{
					$scope.ApprovalDiskonSpkHideIncrement=true;
				}
				
				return res.data;
			},
			function(err){
				console.log("err=>",err);
			}
		);
	}

	$scope.ApprovalOTRSpkRefresh = function () {
		$scope.ApprovalDiskonSpkHideIncrement = false;
		ApprovalDiskonSpkFactory.getDataOTR().then(
			function (res) {

				$scope.ApprovalOTRSpkSearchIsPressed = false;
				$scope.ApprovalOTRSpk = res.data.Result;

				// for (var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i) {
				// 	for (var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
				// 		if ($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
				// 			$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
				// 			$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir'] = $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
				// 		}

				// 	}
				// }
				$scope.ApprovalOTRSpkSearchValue = "";
				$scope.ResetSelection();

				if (res.data.Total <= 5) {
					$scope.ApprovalOTRSpkHideIncrement = true;
				}

				return res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
		);
	}

	$scope.ApprovalOTRSpkSearchCriteriaChange = function (filterType) {
		$scope.ApprovalOTRSpkSearchCriteria = filterType;
	}

	$scope.ApprovalDiskonSpkSearchCriteriaChange = function (filterType) {
		$scope.ApprovalDiskonSpkSearchCriteria = filterType;
	}


	$scope.ApprovalDiscountFilterTriger = function () {
		$scope.ApprovalDiskonSpkFilter = true;
		$scope.ApprovalOTRSpkFilter = false;
		console.log('filter diskon ===>', $scope.ApprovalDiskonSpkFilter);
	}

	$scope.ApprovalOTRFilterTriger = function () {
		$scope.ApprovalDiskonSpkFilter = false;
		$scope.ApprovalOTRSpkFilter = true;
		console.log('filter OTR ===>', $scope.ApprovalOTRSpkFilter);

	}

	// navigateTab('tabContainer_ApprovalDiscAndOTR', 'ApprovalDiskon');
	// $scope.ApprovalDiscountFilter();


		
		
		$scope.ApprovalDiskonSpkIncrementLimit = function () {
			$scope.ApprovalDiskonSpkHideIncrement=false;
			if($scope.ApprovalDiskonSpkSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalDiskonSpkGabungan.length)+1;
				ApprovalDiskonSpkFactory.getDataIncrement(masukin,$scope.filter.limit).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDiskonSpkGabungan.push(res.data.Result[i]);
						}
						
						for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
							{
								if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
								}	
							}
						}
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.filter.limit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalDiskonSpkHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			if($scope.ApprovalDiskonSpkSearchIsPressed==true)
			{
				var masukin=angular.copy($scope.ApprovalDiskonSpkGabungan.length)+1;
				ApprovalDiskonSpkFactory.getDataSearchApprovalDiskonSpkIncrement(masukin,$scope.filter.limit,$scope.ApprovalDiskonSpkSearchCriteriaStore,$scope.ApprovalDiskonSpkSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalDiskonSpkGabungan.push(res.data.Result[i]);
						}
						
						for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
							{
								if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
								}	
							}
						}
						
						var ApprovalDiskonSpkDaIncrementBablas=masukin+$scope.filter.limit;
						if(ApprovalDiskonSpkDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalDiskonSpkHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			
			
			

		};

		$scope.ApprovalOTRSpkIncrementLimit = function () {
			console.log('klik load more OTR');
			$scope.ApprovalOTRSpkHideIncrement = false;
			if ($scope.ApprovalOTRSpkSearchIsPressed == false) {
				console.log('$scope.ApprovalOTRSpkSearchIsPressed == false', $scope.ApprovalOTRSpkSearchIsPressed);

				var masukin = angular.copy($scope.ApprovalOTRSpk.length) + 1;
				ApprovalDiskonSpkFactory.getDataIncrementOTR(masukin, $scope.ApprovalOTRSpkSearchLimit).then(
					function (res) {
						for (var i = 0; i < res.data.Result.length; ++i) {
							$scope.ApprovalOTRSpk.push(res.data.Result[i]);
						}

						// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
						// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
						// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
						// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
						// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						// 		}
						// 	}
						// }

						var ApprovalOTRSpkDaIncrementBablas = masukin + $scope.ApprovalOTRSpkSearchLimit;
						if (ApprovalOTRSpkDaIncrementBablas >= res.data.Total) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
			else if ($scope.ApprovalOTRSpkSearchIsPressed == true) {
				console.log('$scope.ApprovalOTRSpkSearchIsPressed == true', $scope.ApprovalOTRSpkSearchIsPressed);
				var masukin = angular.copy($scope.ApprovalOTRSpk.length) + 1;
				ApprovalDiskonSpkFactory.getDataSearchApprovalOTRSpkIncrement(masukin, $scope.ApprovalOTRSpkSearchLimit, $scope.ApprovalOTRSpkSearchCriteriaStore, $scope.ApprovalOTRSpkSearchValueStore).then(
					function (res) {
						for (var i = 0; i < res.data.Result.length; ++i) {
							$scope.ApprovalOTRSpk.push(res.data.Result[i]);
						}

						// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
						// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
						// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
						// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
						// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						// 		}
						// 	}
						// }

						var ApprovalOTRSpkDaIncrementBablas = masukin + $scope.ApprovalOTRSpkSearchLimit;
						if (ApprovalOTRSpkDaIncrementBablas >= res.data.Total) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
			else{
				console.log('gamasuk mana mana');
			}


		};
		
		$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval_Clicked = function () {
			$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval = !$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval;
			if ($scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval == true) {
				$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval == false) {
				$scope.Show_Hide_ApprovalDiskonSpk_Detail_HistoryApproval_PanelIcon = "+";
			}

		};

	$scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval_Clicked = function () {
		$scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval = !$scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval;
		if ($scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval == true) {
			$scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval_PanelIcon = "-";
		}
		else if ($scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval == false) {
			$scope.Show_Hide_ApprovalOTRSpk_Detail_HistoryApproval_PanelIcon = "+";
		}

	};

		
		$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga_Clicked = function () {
			$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga = !$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga;
			if ($scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga == true) {
				$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga_PanelIcon = "-";
			}
			else if ($scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga == false) {
				$scope.Show_Hide_ApprovalDiskonSpk_Detail_DataHarga_PanelIcon = "+";
			}

		};
		
		$scope.FromListSetujuApprovalDiskonSpk = function () {
			
			ApprovalDiskonSpkFactory.SetujuApprovalDiskonSpkDariList($scope.selectionApprovalDiskonSpk).then(function () {
						
						ApprovalDiskonSpkFactory.getData().then(
							function(res){
								bsNotify.show(
									{
										title: "Message",
										content: "Diskon disetujui",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonSpkSearchIsPressed=false;
								$scope.ApprovalDiskonSpkGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
									{
										if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
										}
										
									}
								}
								
								$scope.loading=false;
								$scope.BatalApprovalDiskonSpk();
								
								$scope.ApprovalDiskonSpkHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonSpkHideIncrement=true;
								}
								$scope.ResetSelection();
								$scope.ApprovalDiskonSpkMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
		};
		
		$scope.FromListTidakSetujuApprovalDiskonSpk = function () {
			$scope.ApprovalDiskonSpkMultipleReject=true;
			
			setTimeout(function() {
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').not(':first').remove();  
            }, 1);
		};

		$scope.FromListSetujuApprovalOTRSpk = function () {
			var selectedOTR = $scope.selectionApprovalOTRSpk;
			console.log('selectedOTR ===>',selectedOTR);
			for (i = 0; i < selectedOTR.length; i++){
				console.log('selectedOTR[i].StatusApprovalId  ==>', selectedOTR[i].StatusApprovalId);
				selectedOTR[i].StatusApprovalId = 1;
				selectedOTR[i].RejectReason = null;
			}
			console.log('selectedOTR yang mau di update/setujui ===>', selectedOTR);

			ApprovalDiskonSpkFactory.SetujuApprovalOTRSpkDariList(selectedOTR).then(function () {

				ApprovalDiskonSpkFactory.getDataOTR().then(
					function (res) {
						bsNotify.show(
							{
								title: "Message",
								content: "Off The Road disetujui",
								type: 'success'
							}
						);
						$scope.ApprovalOTRSpkSearchIsPressed = false;
						$scope.ApprovalOTRSpk = res.data.Result;
						$scope.loading = false;
						$scope.BatalApprovalOTRSpk();

						$scope.ApprovalOTRSpkHideIncrement = false;
						if (res.data.Total <= 5) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}
						$scope.ResetSelection();
						$scope.ApprovalOTRSpkMultipleReject = false;
						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			});
		};

		$scope.FromListTidakSetujuApprovalOTRSpk = function () {
			$scope.ApprovalOTRSpkMultipleReject = true;
			$scope.mApprovalOTRSpk.RejectReason = '';

			console.log('$scope.mApprovalOTRSpk.RejectReason ===>', $scope.mApprovalOTRSpk.RejectReason);

			setTimeout(function () {
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalOTRSpk').modal('setting', { closable: false }).modal('show');
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalOTRSpk').not(':first').remove();
			}, 1);
		};

		
        
		$scope.TidakSetujuApprovalDiskonSpk = function () {
			$scope.ApprovalDiskonSpkMultipleReject=false;
			
			setTimeout(function() {
    angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').not(':first').remove();  
            }, 1);
		};

	$scope.TidakSetujuApprovalOTRSpk = function () {
		$scope.ApprovalOTRSpkMultipleReject = false;

		setTimeout(function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalOTRSpk').modal('setting', { closable: false }).modal('show');
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalOTRSpk').not(':first').remove();
		}, 1);
	};
		
		$scope.ActionApprovalDiskonSpk = function (SelectedData) {
			$scope.da_previous_sequenceApprovalDiskonSpk=-1;
			$scope.ApprovalDiskonSpk_Jegal=true;
			
			for(var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i)
			{
				if(SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName==$scope.user.RoleName)
				{
					$scope.ApprovalDiskonSpk_Jegal=false;
				}
			}
			
			for(var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i)
			{
				if(SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName==$scope.user.RoleName && SelectedData.ListOfSAHApprovalDiscountSPKView[i].StatusApprovalName !="Diajukan")
				{
					$scope.ApprovalDiskonSpk_Jegal=true;
					console.log("setan",$scope.da_previous_sequenceApprovalDiskonSpk);
				}
				
				if(SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName==$scope.user.RoleName)
				{
					
					$scope.da_previous_sequenceApprovalDiskonSpk=angular.copy(SelectedData.ListOfSAHApprovalDiscountSPKView[i].sequence)-1;
				}
			}
			
			for(var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i)
			{
				if(SelectedData.ListOfSAHApprovalDiscountSPKView[i].sequence==$scope.da_previous_sequenceApprovalDiskonSpk )
				{
					if(SelectedData.ListOfSAHApprovalDiscountSPKView[i].StatusApprovalName !="Disetujui")
					{
						$scope.ApprovalDiskonSpk_Jegal=true;
					}
					
				}
			}
			
			$scope.ApprovalDiskonSpkMultipleReject=false;
			$scope.mApprovalDiskonSpk=angular.copy(SelectedData);

			setTimeout(function() {
				angular.element('.ui.modal.ModalActionApprovalDiskonSpk').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalActionApprovalDiskonSpk').not(':first').remove();  
            }, 1);
		}

		$scope.ActionApprovalOTRSpk = function (SelectedData) {
			// $scope.da_previous_sequenceApprovalDiskonSpk = -1;
			$scope.ApprovalOTRSpk_Jegal = true;

			// for (var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i) {
			// 	if (SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName == $scope.user.RoleName) {
			// 		$scope.ApprovalOTRSpk_Jegal = false;
			// 	}
			// }

			// for (var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i) {
			// 	if (SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName == $scope.user.RoleName && SelectedData.ListOfSAHApprovalDiscountSPKView[i].StatusApprovalName != "Diajukan") {
			// 		$scope.ApprovalOTRSpk_Jegal = true;
			// 		console.log("setan", $scope.da_previous_sequenceApprovalDiskonSpk);
			// 	}

			// 	if (SelectedData.ListOfSAHApprovalDiscountSPKView[i].ApproverRoleName == $scope.user.RoleName) {

			// 		$scope.da_previous_sequenceApprovalDiskonSpk = angular.copy(SelectedData.ListOfSAHApprovalDiscountSPKView[i].sequence) - 1;
			// 	}
			// }

			// for (var i = 0; i < SelectedData.ListOfSAHApprovalDiscountSPKView.length; ++i) {
			// 	if (SelectedData.ListOfSAHApprovalDiscountSPKView[i].sequence == $scope.da_previous_sequenceApprovalDiskonSpk) {
			// 		if (SelectedData.ListOfSAHApprovalDiscountSPKView[i].StatusApprovalName != "Disetujui") {
			// 			$scope.ApprovalOTRSpk_Jegal = true;
			// 		}

			// 	}
			// }

			$scope.ApprovalOTRSpkMultipleReject = false;
			$scope.mApprovalOTRSpk = angular.copy(SelectedData);
			console.log('data approval OTR detail ===>', $scope.mApprovalOTRSpk);

			setTimeout(function () {
				angular.element('.ui.modal.ModalActionApprovalOTRSpk').modal('setting', { closable: false }).modal('show');
				angular.element('.ui.modal.ModalActionApprovalOTRSpk').not(':first').remove();
			}, 1);
		}
		
		$scope.BatalActionApprovalDiskonSpk = function () {
			angular.element('.ui.modal.ModalActionApprovalDiskonSpk').modal('hide');
			$scope.mApprovalDiskonSpk={};
		}

		

		$scope.count = 0;
		$scope.maxdiscAcc = 0;
		
		$scope.SelectApprovalDiskonSpk = function () {
			
			$scope.ApprovalDiskonSpkMain = false;
			$scope.ApprovalDiskonSpkDetail = true;
			$scope.ApprovalOTRSpkDetail = false;

			angular.element('.ui.modal.ModalActionApprovalDiskonSpk').modal('hide');
			
			ApprovalDiskonSpkFactory.getDaRincian($scope.mApprovalDiskonSpk.SPKId).then(
				function(res){
					$scope.selected_data = res.data.Result[0];
					$scope.count = 0;
					$scope.maxdiscAcc = 0;
						for (var j in $scope.selected_data.ListInfoUnit){
							$scope.count = $scope.count + $scope.selected_data.ListInfoUnit[j].ListDetailUnit.length;
							for(var k in $scope.selected_data.ListInfoUnit[j].ListDetailUnit){
								if($scope.selected_data.ListInfoUnit[j].ListDetailUnit[k].GrandTotalAcc > $scope.maxdiscAcc){
									$scope.maxdiscAcc = $scope.selected_data.ListInfoUnit[j].ListDetailUnit[k].GrandTotalAcc;
								}
							}		
						}
					//return res.data;
				}
			);
		}
		
		$scope.SelectApprovalOTRSpk = function () {
			
			$scope.ApprovalDiskonSpkMain= false;
			$scope.ApprovalDiskonSpkDetail = false;
			$scope.ApprovalOTRSpkDetail = true;

			

			$scope.simpenKlik = $scope.selectionApprovalOTRSpk;
			console.log('ini list yang dipilih ===>', $scope.simpenKlik);

			angular.element('.ui.modal.ModalActionApprovalOTRSpk').modal('hide');
			
			// ApprovalDiskonSpkFactory.getDaRincian($scope.mApprovalOTRSpk.SPKId).then(
			// 	function(res){
			// 		console.log('res getDaRincian ===>',res);
			// 		$scope.selected_data = res.data.Result[0];
			// 		$scope.count = 0;
			// 		$scope.maxdiscAcc = 0;
			// 			for (var j in $scope.selected_data.ListInfoUnit){
			// 				$scope.count = $scope.count + $scope.selected_data.ListInfoUnit[j].ListDetailUnit.length;
			// 				for(var k in $scope.selected_data.ListInfoUnit[j].ListDetailUnit){
			// 					if($scope.selected_data.ListInfoUnit[j].ListDetailUnit[k].GrandTotalAcc > $scope.maxdiscAcc){
			// 						$scope.maxdiscAcc = $scope.selected_data.ListInfoUnit[j].ListDetailUnit[k].GrandTotalAcc;
			// 					}
			// 				}		
			// 			}
			// 		//return res.data;
			// 	}
			// );

			ApprovalDiskonSpkFactory.getHistoryOtr($scope.mApprovalOTRSpk.SPKId).then(
				function (res) {
					$scope.historyOTR = res.data.Result;

					// $scope.historyOTR = [{
					// 																					ApprovalName: 'Galang',
					// 																					ApproverRoleId:1057,
					// 																					ApprovalDate: '20-10-2019',
					// 																					StatusApprovalName: 'Diajukan',
					// 																					ApprovalNote: 'test catatan json',
					// 																					RequestNote: 'tidak ada catatan request Note'
					// 																				},
																								
					// 																				{
					// 																					ApprovalName: 'Setiawan',
					// 																					ApproverRoleId: 1121,
					// 																					ApprovalDate: '20-10-2019',
					// 																					StatusApprovalName: 'Ditolak',
					// 																					ApprovalNote: 'test catatan index 1',
					// 																					RequestNote: 'tidak ada catatan request Note'
					// 																				}
					// 																			];
					console.log('res getDataHistoryOtr berdasarkan id ===>', $scope.historyOTR);
				}
			);


		}

		$scope.BatalApprovalDiskonSpk = function () {
			$scope.mApprovalDiskonSpk ={};
			$scope.ApprovalDiskonSpkMain=true;
			$scope.ApprovalDiskonSpkDetail=false;
			angular.element('.ui.modal.ModalActionApprovalDiskonSpk').modal('hide');
		}

		$scope.BatalApprovalOTRSpk = function () {
			$scope.mApprovalOTRSpk ={};
			$scope.ApprovalDiskonSpkMain=true;
			$scope.ApprovalOTRSpkDetail=false;
			
			angular.element('.ui.modal.ModalActionApprovalOTRSpk').modal('hide');
		}

		$scope.BatalActionApprovalOTRSpk = function () {
			$scope.mApprovalDiskonSpk ={};
			$scope.ApprovalDiskonSpkMain=true;
			$scope.ApprovalOTRSpkDetail=false;
			$scope.mApprovalOTRSpk.RejectReason = '';
			console.log('$scope.mApprovalOTRSpk harusnya kosong ===>', $scope.mApprovalOTRSpk);



			angular.element('.ui.modal.ModalActionApprovalOTRSpk').modal('hide');
		}
		

		
	$scope.BatalAlasanApprovalDiskonSpk = function () {
		angular.element('.ui.modal.ModalAlasanPenolakanApprovalDiskonSpk').modal('hide');
		//$scope.mApprovalDiskonSpk={};
	}

	$scope.BatalAlasanApprovalOTRSpk = function () {
		angular.element('.ui.modal.ModalAlasanPenolakanApprovalOTRSpk').modal('hide');
		//$scope.mApprovalDiskonSpk={};
	}
		
		$scope.SetujuApprovalDiskonSpk = function () {
			ApprovalDiskonSpkFactory.SetujuApprovalDiskonSpk($scope.mApprovalDiskonSpk).then(function () {
					ApprovalDiskonSpkFactory.getData().then(
						function(res){
							
							bsNotify.show(
								{
									title: "Message",
									content: "Diskon disetujui",
									type: 'success'
								}
							);
							
							$scope.ApprovalDiskonSpkSearchIsPressed=false;
							$scope.ApprovalDiskonSpkGabungan = res.data.Result;
							
							for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
							{
								for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
								{
									if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
									{
										$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
										$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
									}
									
								}
							}
							
							$scope.loading=false;
							$scope.BatalApprovalDiskonSpk();
							$scope.ResetSelection();
							
							$scope.ApprovalDiskonSpkHideIncrement=false;
							if(res.data.Total<=5)
							{
								$scope.ApprovalDiskonSpkHideIncrement=true;
							}
							
							$scope.ApprovalDiskonSpkMultipleReject=false;
							
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				});

		};

		$scope.SetujuApprovalOTRSpk = function () {
			$scope.mApprovalOTRSpk.StatusApprovalId = 1;
			$scope.mApprovalOTRSpk.RejectReason = '';
			ApprovalDiskonSpkFactory.SetujuApprovalOTRSpk($scope.mApprovalOTRSpk).then(function () {
				ApprovalDiskonSpkFactory.getDataOTR().then(
					function (res) {

						bsNotify.show(
							{
								title: "Message",
								content: "Off The Road disetujui",
								type: 'success'
							}
						);

						$scope.ApprovalOTRSpkSearchIsPressed = false;
						$scope.ApprovalOTRSpk = res.data.Result;


						$scope.loading = false;
						$scope.BatalApprovalOTRSpk();
						$scope.ResetSelection();

						$scope.ApprovalOTRSpkHideIncrement = false;
						if (res.data.Total <= 5) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						$scope.ApprovalOTRSpkMultipleReject = false;

						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			});

		};
		
		$scope.SubmitAlasanApprovalDiskonSpk = function () {
			if($scope.ApprovalDiskonSpkMultipleReject==false)
			{
				for(var x = 0; x < $scope.mApprovalDiskonSpk['ListOfSAHApprovalDiscountSPKView'].length; ++x)
				{
					$scope.mApprovalDiskonSpk['ListOfSAHApprovalDiscountSPKView'][x]['RejectReason']=angular.copy($scope.mApprovalDiskonSpk.RejectReason);
				}
				ApprovalDiskonSpkFactory.TidakSetujuApprovalDiskonSpk($scope.mApprovalDiskonSpk).then(function () {
						ApprovalDiskonSpkFactory.getData().then(
							function(res){
								
								bsNotify.show(
									{
										title: "Message",
										content: "Data di reject",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonSpkSearchIsPressed=false;
								$scope.ApprovalDiskonSpkGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
									{
										if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
										}
										
									}
								}
								
								$scope.loading=false;
								$scope.BatalAlasanApprovalDiskonSpk();
								$scope.BatalApprovalDiskonSpk();
								$scope.ResetSelection();
								
								$scope.ApprovalDiskonSpkHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonSpkHideIncrement=true;
								}
								
								$scope.ApprovalDiskonSpkMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
			else if($scope.ApprovalDiskonSpkMultipleReject==true)
			{
				for(var i = 0; i < $scope.selectionApprovalDiskonSpk.length; ++i)
				{
					//var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
					//$scope.selectionApprovalDiskonSpk[i]['RejectReason']=angular.copy($scope.mApprovalDiskonSpk.RejectReason);
					for(var x = 0; x < $scope.selectionApprovalDiskonSpk[i]['ListOfSAHApprovalDiscountSPKView'].length; ++x)
					{
						$scope.selectionApprovalDiskonSpk[i]['ListOfSAHApprovalDiscountSPKView'][x]['RejectReason']=angular.copy($scope.mApprovalDiskonSpk.RejectReason);
					}
				}
				ApprovalDiskonSpkFactory.TidakSetujuApprovalDiskonSpkDariList($scope.selectionApprovalDiskonSpk).then(function () {
						ApprovalDiskonSpkFactory.getData().then(
							function(res){
								
								bsNotify.show(
									{
										title: "Message",
										content: "Data di reject",
										type: 'success'
									}
								);
								
								$scope.ApprovalDiskonSpkSearchIsPressed=false;
								$scope.ApprovalDiskonSpkGabungan = res.data.Result;
								
								for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
								{
									for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
									{
										if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
										{
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
											$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
										}
										
									}
								}
								
								$scope.loading=false;
								$scope.BatalAlasanApprovalDiskonSpk();
								$scope.BatalApprovalDiskonSpk();
								$scope.ResetSelection();
								
								$scope.ApprovalDiskonSpkHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalDiskonSpkHideIncrement=true;
								}
								
								$scope.ApprovalDiskonSpkMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
		
			
		}


	$scope.SubmitAlasanApprovalOTRSpk = function () {

		// for (i = 0; i < $scope.selectionApprovalOTRSpk.length; i++) {
		// 	$scope.selectionApprovalOTRSpk[i].StatusApprovalId = 2;
		// 	$scope.selectionApprovalOTRSpk[i].RejectReason = $scope.selectionApprovalOTRSpk[0].RejectReason;
		// }
		// console.log('$scope.selectionApprovalOTRSpk yang mau di Tolak ===>', $scope.selectionApprovalOTRSpk);
		// console.log('RejectReason ===>', $scope.selectionApprovalOTRSpk[0].RejectReason);


		// ApprovalDiskonSpkFactory.TidakSetujuApprovalOTRSpk($scope.selectionApprovalOTRSpk).then(function () {

		// 	ApprovalDiskonSpkFactory.getDataOTR().then(
		// 		function (res) {
		// 			bsNotify.show(
		// 				{
		// 					title: "Message",
		// 					content: "OTR ditolak",
		// 					type: 'success'
		// 				}
		// 			);
		// 			$scope.ApprovalOTRSpkSearchIsPressed = false;
		// 			$scope.ApprovalOTRSpk = res.data.Result;
		// 			$scope.loading = false;
		// 			$scope.BatalApprovalOTRSpk();

		// 			$scope.ApprovalOTRSpkHideIncrement = false;
		// 			if (res.data.Total <= 5) {
		// 				$scope.ApprovalOTRSpkHideIncrement = true;
		// 			}
		// 			$scope.ResetSelection();
		// 			$scope.ApprovalOTRSpkMultipleReject = false;
		// 			return res.data;
		// 		},
		// 		function (err) {
		// 			console.log("err=>", err);
		// 		}
		// 	);
		// });

		if ($scope.ApprovalOTRSpkMultipleReject == false) {

			console.log('data tunggal');

			// $scope.mApprovalDiskonSpk = angular.copy($scope.selectionApprovalOTRSpk);
				$scope.mApprovalOTRSpk.StatusApprovalId= 2;
				console.log('$scope.mApprovalOTRSpk.StatusApprovalId ===>', $scope.mApprovalOTRSpk.StatusApprovalId);
				console.log('$scope.mApprovalOTRSpk.RejectReason ===>', $scope.mApprovalOTRSpk.RejectReason);

			ApprovalDiskonSpkFactory.TidakSetujuApprovalOTRSpk([$scope.mApprovalOTRSpk]).then(function () {
				ApprovalDiskonSpkFactory.getDataOTR().then(
					function (res) {

						bsNotify.show(
							{
								title: "Message",
								content: "Data di Tolak",
								type: 'success'
							}
						);

						$scope.ApprovalOTRSpkSearchIsPressed = false;
						$scope.ApprovalOTRSpk = res.data.Result;


						$scope.loading = false;
						$scope.BatalAlasanApprovalOTRSpk();
						$scope.BatalApprovalOTRSpk();
						$scope.ResetSelection();

						$scope.ApprovalOTRSpkHideIncrement = false;
						if (res.data.Total <= 5) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						$scope.ApprovalOTRSpkMultipleReject = false;
						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			});
		}
		else if ($scope.ApprovalOTRSpkMultipleReject == true) {
			console.log('data array');
			for (var i = 0; i < $scope.selectionApprovalOTRSpk.length; ++i) {
				$scope.selectionApprovalOTRSpk[i]['RejectReason']=angular.copy($scope.mApprovalOTRSpk.RejectReason);
				$scope.selectionApprovalOTRSpk[i].StatusApprovalId = 2;
			}
		
			ApprovalDiskonSpkFactory.TidakSetujuApprovalOTRSpkDariList($scope.selectionApprovalOTRSpk).then(function () {
				ApprovalDiskonSpkFactory.getDataOTR().then(
					function (res) {

						bsNotify.show(
							{
								title: "Message",
								content: "Data di Tolak",
								type: 'success'
							}
						);



						$scope.ApprovalOTRSpkSearchIsPressed = false;
						$scope.ApprovalOTRSpk = res.data.Result;

						// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
						// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
						// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
						// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
						// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						// 		}

						// 	}
						// }

						$scope.selectionApprovalOTRSpk = [];

						$scope.loading = false;
						$scope.BatalAlasanApprovalOTRSpk();
						$scope.BatalApprovalOTRSpk();
						$scope.ResetSelection();

						$scope.ApprovalOTRSpkHideIncrement = false;
						if (res.data.Total <= 5) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						$scope.ApprovalOTRSpkMultipleReject = false;
						return res.data;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			});
		}
	}

		

		$scope.ResetSelection = function () {
			$scope.selectionApprovalDiskonSpk=[{ 
											OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
           ApprovalQuotationSendingId: null}];
			$scope.selectionApprovalDiskonSpk.splice(0, 1);
		}

	
		$scope.selectionApprovalDiskonSpk=[{ OutletId: null,
											QuotationId: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
           ApprovalQuotationSendingId: null}];
		$scope.selectionApprovalDiskonSpk.splice(0, 1);
		
		$scope.toggleSelection = function toggleSelection(SelectedAprovalDiskonSpkFromList) {
			var idx = $scope.selectionApprovalDiskonSpk.indexOf(SelectedAprovalDiskonSpkFromList);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionApprovalDiskonSpk.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionApprovalDiskonSpk.push(SelectedAprovalDiskonSpkFromList);
			}

			$scope.SelectionAman_ApprovalDiskonSpk=true;
			$scope.da_previous_sequenceApprovalDiskonSpk=-1;
			
			
			for(var i = 0; i < $scope.selectionApprovalDiskonSpk.length; ++i)
			{
				if($scope.selectionApprovalDiskonSpk[i].HeaderStatusApprovalName!="Diajukan")
				{
					$scope.SelectionAman_ApprovalDiskonSpk=false;
				}
				
				for(var x = 0; x < $scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
				{
					if($scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName==$scope.user.RoleName && $scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName !="Diajukan")
					{
						$scope.SelectionAman_ApprovalDiskonSpk=false;
					}
					
					if($scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName==$scope.user.RoleName)
					{
						
						$scope.da_previous_sequenceApprovalDiskonSpk=angular.copy($scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].sequence)-1;
					}
				}
				
				for(var x = 0; x < $scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
				{
					if($scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].sequence==$scope.da_previous_sequenceApprovalDiskonSpk )
					{
						if($scope.selectionApprovalDiskonSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName !="Disetujui")
						{
							$scope.SelectionAman_ApprovalDiskonSpk=false;
						}
						
					}
				}
			}
		};

		$scope.toggleSelectionOTR = function toggleSelectionOTR(SelectedAprovalOTRSpkFromList) {
			var idx = $scope.selectionApprovalOTRSpk.indexOf(SelectedAprovalOTRSpkFromList);

			// Is currently selected
			if (idx > -1) {
				$scope.selectionApprovalOTRSpk.splice(idx, 1);
				$scope.ApprovalOTRSpkMultipleAccept = true;
				$scope.ApprovalOTRSpkMultipleReject = true;

			}

			// Is newly selected
			else {
				$scope.selectionApprovalOTRSpk.push(SelectedAprovalOTRSpkFromList);
				$scope.ApprovalOTRSpkMultipleAccept = false;
				$scope.ApprovalOTRSpkMultipleReject = false;

			}

			console.log("$scope.selectionApprovalOTRSpk====>", $scope.selectionApprovalOTRSpk);
			$scope.SelectionAman_ApprovalOTRSpk = true;
			$scope.da_previous_sequenceApprovalDiskonSpk = -1;
			


			for (var i = 0; i < $scope.selectionApprovalOTRSpk.length; ++i) {
				if ($scope.selectionApprovalOTRSpk[i].StatusApprovalName != "Diajukan") {
					$scope.SelectionAman_ApprovalOTRSpk = false;
				}
			}
		};

		  
		$scope.SearchApprovalDiskonSpk = function () {
			$scope.ApprovalDiskonSpkHideIncrement=false;
			try
			{ 
				$scope.ApprovalDiskonSpkSearchValue = document.getElementById("ApprovalDiskonSpkSearchValue").value;
				console.log('	$scope.ApprovalDiskonSpkSearchCriteria ===>', $scope.ApprovalDiskonSpkSearchCriteria);
				if($scope.ApprovalDiskonSpkSearchValue!="" && (typeof $scope.ApprovalDiskonSpkSearchValue!="undefined"))
				{
					ApprovalDiskonSpkFactory.getDataSearchApprovalDiskonSpk($scope.filter.limit, $scope.ApprovalDiskonSpkSearchCriteria,$scope.ApprovalDiskonSpkSearchValue).then(function (res) {
						
						$scope.ApprovalDiskonSpkSearchIsPressed=true;
						$scope.ApprovalDiskonSpkGabungan = res.data.Result;
						
						for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
							{
								if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
								}	
							}
						}
						
						if($scope.filter.limit>=res.data.Total)
						{
							$scope.ApprovalDiskonSpkHideIncrement=true;
						}
						
						$scope.ResetSelection();
						$scope.ApprovalDiskonSpkSearchCriteriaStore=angular.copy($scope.ApprovalDiskonSpkSearchCriteria);
						$scope.ApprovalDiskonSpkSearchValueStore=angular.copy($scope.ApprovalDiskonSpkSearchValue);
					});
				}
				else
				{
					ApprovalDiskonSpkFactory.getData().then(function (res) {
						
						$scope.ApprovalDiskonSpkSearchIsPressed=false;
						$scope.ApprovalDiskonSpkGabungan = res.data.Result;
						
						for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
						{
							for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
							{
								if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
								{
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
									$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
								}
									
							}
						}
						if(res.data.Total<=5)
						{
							$scope.ApprovalDiskonSpkHideIncrement=true;
						}
						
						$scope.ResetSelection();
					});
				}
			}
			catch(e)
			{
				ApprovalDiskonSpkFactory.getData().then(function (res) {
					
					$scope.ApprovalDiskonSpkSearchIsPressed=false;
					$scope.ApprovalDiskonSpkGabungan = res.data.Result;
					
					for(var i = 0; i < $scope.ApprovalDiskonSpkGabungan.length; ++i)
					{
						for(var x = 0; x < $scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView.length; ++x)
						{
							if($scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName=="Disetujui")
							{
								$scope.ApprovalDiskonSpkGabungan[i]['ApproverRoleNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
								$scope.ApprovalDiskonSpkGabungan[i]['ApproverNameTerakhir']=$scope.ApprovalDiskonSpkGabungan[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
							}
									
						}
					}
					
					if(res.data.Total<=5)
					{
						$scope.ApprovalDiskonSpkHideIncrement=true;
					}
					
					$scope.ResetSelection();
				});
			}
			

		}  
		
		$scope.SearchApprovalOTRSpk = function () {
			$scope.ApprovalOTRSpkHideIncrement = false;
			try {
				$scope.ApprovalOTRSpkSearchValue = document.getElementById("ApprovalOTRSpkSearchValue").value;

				console.log("$scope.ApprovalOTRSpkSearchCriteria ===>", $scope.ApprovalOTRSpkSearchCriteria);

				if ($scope.ApprovalOTRSpkSearchValue != "" && (typeof $scope.ApprovalOTRSpkSearchValue != "undefined")) {
					ApprovalDiskonSpkFactory.getDataSearchApprovalOTRSpk($scope.ApprovalOTRSpkSearchLimit, $scope.ApprovalOTRSpkSearchCriteria, $scope.ApprovalOTRSpkSearchValue).then(function (res) {

						$scope.ApprovalOTRSpkSearchIsPressed = true;
						$scope.ApprovalOTRSpk = res.data.Result;

						// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
						// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
						// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
						// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
						// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						// 		}
						// 	}
						// }

						if ($scope.ApprovalOTRSpkSearchLimit >= res.data.Total) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						$scope.ResetSelection();
						$scope.ApprovalOTRSpkSearchCriteriaStore = angular.copy($scope.ApprovalOTRSpkSearchCriteria);
						$scope.ApprovalOTRSpkSearchValueStore = angular.copy($scope.ApprovalOTRSpkSearchValue);
					});
				}
				else {
					ApprovalDiskonSpkFactory.getDataOTR().then(function (res) {

						$scope.ApprovalOTRSpkSearchIsPressed = false;
						$scope.ApprovalOTRSpk = res.data.Result;

						// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
						// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
						// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
						// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
						// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
						// 		}

						// 	}
						// }
						if (res.data.Total <= 5) {
							$scope.ApprovalOTRSpkHideIncrement = true;
						}

						$scope.ResetSelection();
					});
				}
			}
			catch (e) {
				ApprovalDiskonSpkFactory.getDataOTR().then(function (res) {

					$scope.ApprovalOTRSpkSearchIsPressed = false;
					$scope.ApprovalOTRSpk = res.data.Result;

					// for (var i = 0; i < $scope.ApprovalOTRSpk.length; ++i) {
					// 	for (var x = 0; x < $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView.length; ++x) {
					// 		if ($scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].StatusApprovalName == "Disetujui") {
					// 			$scope.ApprovalOTRSpk[i]['ApproverRoleNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverRoleName;
					// 			$scope.ApprovalOTRSpk[i]['ApproverNameTerakhir'] = $scope.ApprovalOTRSpk[i].ListOfSAHApprovalDiscountSPKView[x].ApproverName;
					// 		}

					// 	}
					// }

					if (res.data.Total <= 5) {
						$scope.ApprovalOTRSpkHideIncrement = true;
					}

					$scope.ResetSelection();
				});
			}



		} 
	

});
