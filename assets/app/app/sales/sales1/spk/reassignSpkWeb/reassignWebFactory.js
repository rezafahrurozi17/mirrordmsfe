angular.module('app')
    .factory('ReassignWebFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/ReasignSPKWeb/Filter?'+param);
                return res;
            },
             update: function(SpkTaking) {
                return $http.put('/api/sales/ReasignSPKWeb', [{
                    OutletId: SpkTaking.OutletId,
                    FormSPKId: SpkTaking.FormSPKId,
                    FormSPKRegisterId: SpkTaking.FormSPKRegisterId,
                    FormSPKNo: SpkTaking.FormSPKNo,
                    SalesId: SpkTaking.SalesId,
                    ReceiveDate: SpkTaking.ReceiveDate.getFullYear()+'-'
								+('0' + (SpkTaking.ReceiveDate.getMonth()+1)).slice(-2)+'-'
								+('0' + SpkTaking.ReceiveDate.getDate()).slice(-2)+'T'
								+((SpkTaking.ReceiveDate.getHours()<'10'?'0':'')+ SpkTaking.ReceiveDate.getHours())+':'
								+((SpkTaking.ReceiveDate.getMinutes()<'10'?'0':'')+ SpkTaking.ReceiveDate.getMinutes())+':'
								+((SpkTaking.ReceiveDate.getSeconds()<'10'?'0':'')+ SpkTaking.ReceiveDate.getSeconds())
                }]);
            },
            // delete: function(id) {
            //     return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            // },
        }
    });
