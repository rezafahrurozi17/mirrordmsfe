angular.module('app')
    .controller('ReassignWebController', function($scope, MobileHandling,$http, CurrentUser, ReassignWebFactory, SpkTakingFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
              $scope.bv=MobileHandling.browserVer();
              $scope.bver=MobileHandling.getBrowserVer();
              //console.log("browserVer=>",$scope.bv);
              //console.log("browVersion=>",$scope.bver);
              //console.log("tab: ",$scope.tab);
              if($scope.bv!=="Chrome"){
              if ($scope.bv=="Unknown") {
                if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                {
                  $scope.removeTab($scope.tab.sref);
                }
              }        
              }
            // $scope.gridData2 = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.disabledReassignSpkconfrm = false;
        $scope.cekDokumen = false;
        $scope.cekValidSpk = true;

        $scope.user = CurrentUser.user();
        $scope.mcekSpkValid = null; //Model
        $scope.filter={};
        //$scope.mcekSpkValidDocument = null;
        $scope.datereasign = {
             startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        SpkTakingFactory.getDataSales().then(function(res){
                $scope.sales = res.data.Result;
        });

        // var gridData = [];
        $scope.getData = function() {
            $scope.loading = true;
            ReassignWebFactory.getData($scope.filter).then(function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading = false;
            })
        }

        $scope.keluar = function (){
            angular.element('.ui.modal.signsales').modal('hide');
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.Reassign = function (entity){
             $scope.selected_grid = {};
            SpkTakingFactory.getDataSales().then(function (res){
                $scope.sales = res.data.Result;
            })
            $scope.selected_grid = entity;

                $scope.selected_grid.ReceiveDate = new Date ($scope.selected_grid.ReceiveDate.getFullYear()+'-'
								+('0' + ($scope.selected_grid.ReceiveDate.getMonth()+1)).slice(-2)+'-'
								+('0' + $scope.selected_grid.ReceiveDate.getDate()).slice(-2)+'T'
								+(($scope.selected_grid.ReceiveDate.getHours()<'10'?'0':'')+ $scope.selected_grid.ReceiveDate.getHours())+':'
								+(($scope.selected_grid.ReceiveDate.getMinutes()<'10'?'0':'')+ $scope.selected_grid.ReceiveDate.getMinutes())+':'
								+(($scope.selected_grid.ReceiveDate.getSeconds()<'10'?'0':'')+ $scope.selected_grid.ReceiveDate.getSeconds())); 
            
            angular.element('.ui.modal.signsales').modal('show');
        }

        $scope.reassignSpkconfrm = function (){
            $scope.disabledReassignSpkconfrm = true;
            $scope.loadingReasign = true;
            ReassignWebFactory.update($scope.selected_grid).then(function(){
                    $scope.loadingReasign = false;
                    $scope.disabledReassignSpkconfrm = false;
                     angular.element('.ui.modal.signsales').modal('hide');
                     ReassignWebFactory.getData().then(function(res){
                         $scope.grid.data = res.data.Result;
                         return res.data;
                     })
            })
        }


       var btnActionCekValid2 = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.$parent.Reassign(row.entity)"> \
                                        <u>Reassign</u> \
                                    </p> \
                                  </a>';

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            //enableRowSelection: true,
            //multiSelect: true,
            //enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { displayName: 'No SPK', name:'No SPK', field: 'FormSPKNo' },
                { name: 'nama sales', field: 'SalesName' },
                { name: 'tanggal terima', field: 'ReceiveDate',cellFilter: 'date:\'dd/MM/yyyy\'' },                
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '15%',
                    visible: true,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionCekValid2
                }

            ]
        };

    });