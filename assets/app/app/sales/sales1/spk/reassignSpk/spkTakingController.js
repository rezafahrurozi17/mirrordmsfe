angular.module('app')
    .controller('SpkTakingController', function($rootScope,$templateCache, bsNotify,$scope, $http, CurrentUser, SpkTakingFactory, $timeout) {
        IfGotProblemWithModal();
		//----------------------------------
        // Start-Up
        //----------------------------------
        // $scope.$on('$viewContentLoaded', function() {
        //     $scope.loading = true;
        //     $scope.gridData = [];
        // });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mspkTaking = null; //Model

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.dateOptions = {
            startingDay: 1,
            format: "dd-MM-yyyy",
            disableWeekend: 1
        };

        $scope.bind_data = {
                data: [],
                filter: []    
        };
        
        $scope.filterData = {
            defaultFilter: [
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'Nama Sales', value: 'SalesName' },
            ],
            advancedFilter: [
                { name: 'Nama', value: 'FormSPKNo', typeMandatory: 1 },
                { name: 'Type', value: 'SalesName', typeMandatory: 0 },
            ]
        };

        $scope.getSales = function (data){
            console.log(data);
           
        }

        SpkTakingFactory.getDataSales().then(function(res){
            $scope.sales = res.data.Result;
        });
        
        $scope.bind_data.data.TanggalTerima = new Date();

        $scope.reassignSpk = function(data){
            angular.element('.ui.small.modal.assign').modal('show');
                for(var i=0 ; i < $scope.sales.length; i++){
                    if($scope.sales[i].EmployeeId == data.SalesId){
                        $scope.tempoldSalesman = angular.copy($scope.sales[i]);
                        break;
                    }
                }
            var tanggal = new Date(data.TanggalTerima);
            $scope.selected_data.TanggalTerima = tanggal;
        }
		
		$scope.BatalReassignSpk = function(){
			angular.element('.ui.small.modal.assign').modal('refresh').modal('hide');
		}

        $scope.reassignSpkconfrm = function(data){
            for(var i=0 ; i < $scope.sales.length; i++){
                if($scope.sales[i].EmployeeId == data.SalesId){
                    $scope.tmpnewSalesman = angular.copy($scope.sales[i]);
                    break;
                }
            }
            SpkTakingFactory.update(data).then(function(){
                     angular.element('.ui.small.modal.assign').modal('refresh').modal('hide');
                     bsNotify.show({
                        title: "Berhasil",
                        content: 'SPK berhasil dipindahkan dari '+$scope.tempoldSalesman.EmployeeName+' ke '+$scope.tmpnewSalesman.EmployeeName,
                        type: 'succes'
                    });
                       $rootScope.$emit("RefreshDirective",{});
            },
            function (err) {
                bsNotify.show({
                    title: "Gagal",
                    content: 'SPK gagal dipindahkan '+$scope.tempoldSalesman.SalesName+' ke '+$scope.tmpnewSalesman.EmployeeName,
                    type: 'danger'
                });
            })
        }


    });

    