angular.module('app')
    .factory('SpkTakingFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(param) {
               var res = $http.get('/api/sales/ReasignSPKMobile'+param);
                //console.log('res=>',res);
                return res;
            },
             getDataSales: function() {
               var res = $http.get('/api/sales/MProfileEmployee/?position=salesman');

                return res;
            },
            update: function(SpkTaking) {
                return $http.put('/api/sales/ReasignSPKMobile', [{
                    OutletId: SpkTaking.OutletId,
                    FormSPKId: SpkTaking.FormSPKId,
                    FormSPKRegisterId: SpkTaking.FormSPKRegisterId,
                    FormSPKNo: SpkTaking.FormSPKNo,
                    SalesId: SpkTaking.SalesId,
                    ReceiveDate: SpkTaking.ReceiveDate.getFullYear()+'-'
								+('0' + (SpkTaking.ReceiveDate.getMonth()+1)).slice(-2)+'-'
								+('0' + SpkTaking.ReceiveDate.getDate()).slice(-2)+'T'
								+((SpkTaking.ReceiveDate.getHours()<'10'?'0':'')+ SpkTaking.ReceiveDate.getHours())+':'
								+((SpkTaking.ReceiveDate.getMinutes()<'10'?'0':'')+ SpkTaking.ReceiveDate.getMinutes())+':'
								+((SpkTaking.ReceiveDate.getSeconds()<'10'?'0':'')+ SpkTaking.ReceiveDate.getSeconds())
                }]);
            }
        }
    });