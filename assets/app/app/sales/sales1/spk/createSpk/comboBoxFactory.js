angular.module('app')
    .factory('ComboBoxFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        if (_myCache == undefined)
            var _myCache = [];
        return {
            getNoSPK: function () { var res = $http.get('/api/sales/GetFormSPKKosongBySales'); return res; },
            getNoSPKIsi: function () { var res = $http.get('/api/sales/GetFormSPKIsiBySales'); return res; },
            getDataKategori: function () { var res = $http.get('/api/sales/CCustomerCategory'); return res; },
            getDataProvince: function () {
                var res = null;
                if (_myCache.length == 0) {
                    var user = CurrentUser.user();
                    var res = $http.get('/api/sales/MLocationProvince');
                    res.then(function (res) {
                        _myCache = res.data.Result;
                        console.log('ini cache')
                    });
                } else {
                    res = {
                        then: function (cbOk, cbErr) {
                            cbOk({ data: { Result: angular.copy(_myCache) } });
                        }
                    }
                }
                console.log('ini bukan cache')
                return res;
            },
            getDataKabupaten: function (param) { var res = $http.get('/api/sales/MLocationCityRegency' + param); return res; },
            getDataKecamatan: function (param) { var res = $http.get('/api/sales/MLocationKecamatan' + param); return res; },
            getDataKelurahan: function (param) { var res = $http.get('/api/sales/MLocationKelurahan' + param); return res; },
            getDataGender: function () { var res = $http.get('/api/sales/CCustomerGender'); return res; },
            getDataStatusPernikahan: function () { var res = $http.get('/api/sales/CCustomerStatusMarital'); return res; },
            getDataAgama: function () { var res = $http.get('/api/sales/CCustomerReligion'); return res; },
            getDataJabatan: function () {
                var res = $http.get('/api/sales/CCustomerPosition');
                return res;
            },
            getDataKoresponden: function () {
                var res = $http.get('/api/sales/CCustomerCorrespondent');
                return res;
            },
            getDataSectorBisnis: function () {
                var res = $http.get('/api/sales/CCustomerSectorBusiness');
                return res;
            },
            getDataDeliveryCategory: function () {
                var res = $http.get('/api/sales/PCategoryDelivery');
                return res;
            },
            getDataRoad: function () {
                var res = $http.get('/api/sales/PCategoryOnOffTheRoad');
                return res;
            },
            getDataDocument: function () {
                var res = $http.get('/api/sales/PCategoryDocument');
                return res;
            },
            getDataVehicleModel: function () {
                var res = $http.get('/api/sales/MUnitVehicleModelTomas');
                return res;
            },
            getDataVehicleType: function (param) {
                var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
                return res;
            },
            getSalesProgram: function () {
                var res = $http.get('/api/sales/MProfileSalesProgram');
                return res;
            },
            getJenisPembayaran: function () {
                var res = $http.get('/api/sales/FFinancePaymentType');
                return res;
            },
            getFinanceFundSource: function () {
                var res = $http.get('/api/sales/FFinanceFundSource');
                return res;
            },
            getUnitYear: function () {
                var res = $http.get('/api/sales/DemandSupplyVehicleYearStock?IncludeCurrentYear=True');
                return res;
            },
            getBank: function () { var res = $http.get('/api/sales/AdminHandlingBank'); return res; },
            getDataKetegoriPlat: function () { var res = $http.get('/api/sales/PCategoryNumberPlate'); return res; },
            getDataWarna: function (param) { var res = $http.get('/api/sales/VehicleTypeColorJoinDetailTomas' + param); return res; },
            getDataLeasing: function () { var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000'); return res; },
            //getDataTenor: function (param) { var res = $http.get('/api/sales/MProfileLeasingLeasingTenor/?leasingId=' + param); return res; },
			getDataTenor: function (param) { var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000'); return res; },
            getDataSimulationTenor: function () { var res = $http.get('/api/sales/MProfileLeasingSimulation'); return res; },
            getDataInsurance: function () { var res = $http.get('/api/sales/MProfileInsurance'); return res; },
            getDataInsuranceProduct: function (param) { var res = $http.get('/api/sales/MProfileInsuranceProduct/?insuranceId=' + param); return res },
            getDataInsuranceUserType: function () { var res = $http.get('/api/sales/MProfileInsuranceUserType'); return res },
            getDataInsuranceType: function () { var res = $http.get('/api/sales/MProfileInsuranceType'); return res },
            getDataInsuranceEkstension: function (param) { var res = $http.get('/api/sales/MProfileInsuranceInsuranceExtension/?insuranceId=' + param); return res },
            getDataDeliveryMounth: function () { var res = $http.get('/api/sales/PCategoryPromiseDeliveryMonth'); return res; },
            getDataCategoryDelivery: function () { var res = $http.get('/api/sales/PCategoryDelivery'); return res; },
            getDataStatusPDD: function () { var res = $http.get('/api/sales/PStatusPDD'); return res; },
            getDataFundSource: function () { var res = $http.get('/api/sales/FFinanceFundSource'); return res; },
            getDataAccsessoriesPackage: function (param) { var res = $http.get('/api/sales/AccessoriesPaket/?VehicleTypeId=' + param); return res; },
            getDataAcessories: function (VehicleId, Tga, AccName) {
                if (AccName == undefined) {
                    AccName = '';
                }
                var res = $http.get('/api/sales/AccessoriesSatuan/?start=1&Limit=1000&VehicleTypeId=' + VehicleId + '&Classification=' + Tga + '&filterData=PartsNameCode|' + AccName);
                return res;
            },
            getKaroseri: function (VehicleId, colorid) { var res = $http.get('/api/sales/MpartsKaroseri/?start=1&Limit=1000&VehicleTypeId=' + VehicleId+'&VehicleTypeColorId=' +colorid); return res; },
            getKodePajak: function () { var res = $http.get('/api/sales/FFinanceCodeInvoiceTransactionTax'); return res; },
            getKodePajakbyId: function (Id) { var res = $http.get('/api/sales/FFinanceCodeInvoiceTransactionTax/?CodeInvoiceTransactionTaxId=' + Id); return res; }
        }
    });