angular.module('app')
    .factory('CreateSpkFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;

        function tanggal(tgl) {
            if (tgl != null || tgl != undefined) {
                var fix = tgl.getFullYear() + '-' +
                    ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + tgl.getDate()).slice(-2);
                return fix;
            } else {
                return null;
            }

        };

        return {
            getData: function(param) {
                var res = $http.get('/api/sales/SPKDaftarDraftSPKWithParam' + param);
                return res;
            },
            getSignConfig: function(param) {
                var res = $http.get('/api/sales/SPKCekESign');
                return res;
            },
            getDiscount: function(param) {
                if (param.discount == undefined || param.discount == null || param.discount == "") {
                    param.discount = 0;
                } else {
                    paramdiscount = param.discount;
                }
                var res = $http.get('/api/sales/SPKGetCekDiscountApprover/?discount=' + param.discount + '&vehicleTypeId=' + param.vehicle + '&vehicleModelYear=' + param.year);
                return res;
            },
            getDataDetail: function(param) {
                var res = $http.get('/api/sales/SAHSPKDetail?SpkId=' + param);
                return res;
            },
            getApprovalDiscount: function(SpkId) {
                var res = $http.get('/api/sales/SpkApprovalDiscount/GetAll/?SpkId=' + SpkId);
                return res;
            },
			getApprovalOffTheRoad: function(SpkId) {
                var res = $http.get('/api/sales/Spk_ApprovalOffTheRoad/GetAll/?SpkId=' + SpkId);
                return res;
            },
            getBookingUnitSpk: function(BookingId) {
                var res = $http.get('/api/sales/BookingUnitForSPK?start=1&limit=1000&BookingUnitId=' + BookingId);
                return res;
            },
            getGlobalMediator: function() {
                var res = $http.get('/api/sales/spk/GlobalParamSPK/');
                return res;
            },
            create: function(SpkTaking) {
                SpkTaking.SpkDate = tanggal(SpkTaking.SpkDate);


                return $http.post('/api/sales/SAHSPKDraft', [{
                    KTPNo: SpkTaking.KTPNo,
                    //ProspectId: SpkTaking.ProspectId,
                    ProspectId: SpkTaking.ProspectId,
                    BookingUnitId: SpkTaking.BookingUnitId,
                    KaroseriPPN: SpkTaking.KaroseriPPN,
					RequestNote: SpkTaking.RequestNote,
                    KaroseriTotalInclVAT: SpkTaking.KaroseriTotalInclVAT,
                    AjuDiscountSummary: SpkTaking.AjuDiscountSummary,
                            Discount: SpkTaking.Discount,
                            DiscountSubsidiDP: parseFloat(SpkTaking.DiscountSubsidiDP),
                            MediatorCommision: parseFloat(SpkTaking.MediatorCommision),
                            DPP: parseFloat(SpkTaking.DPP),
                            PPN: parseFloat(SpkTaking.PPN),
                            TotalBBN: parseFloat(SpkTaking.TotalBBN),
                            PPH22: parseFloat(SpkTaking.PPH22),
                            TotalInclVAT: parseFloat(SpkTaking.TotalInclVAT),
                            BookingFeeFundSourceId: SpkTaking.BookingFeeFundSourceId,
                            BookingFeeReceiveBit: SpkTaking.BookingFeeFundSourceId,
                            VehicleTypeId: SpkTaking.VehicleTypeId,
                            ProductionYear: SpkTaking.ProductionYear,
                    GrandTotal: SpkTaking.GrandTotal,
                            NoteDiscount: SpkTaking.NoteDiscount,
                    CustomerId: SpkTaking.CustomerId,
                    ToyotaId: SpkTaking.ToyotaId,
                    CustomerTypeId: SpkTaking.CustomerTypeId,
                    SalesId: SpkTaking.SalesId,
                    PenerimaSTNKBit: SpkTaking.PenerimaSTNKBit,
                    SpkId: SpkTaking.SpkId,
                    SpkDate: SpkTaking.SpkDate,
                    STNKDataBit: SpkTaking.STNKDataBit,
                    SpkStatusId: SpkTaking.SpkStatusId,
                    SpkCancelId: SpkTaking.SpkCancelId,
                    NoteCancel: SpkTaking.NoteCancel,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    ManualMatchingBit: SpkTaking.ManualMatchingBit,
                    QtyTotalUnit: SpkTaking.QtyTotalUnit,
                    KaroseriId: SpkTaking.KaroseriId,
                    KaroseriPrice: parseFloat(SpkTaking.KaroseriPrice),
                    TotalDP: SpkTaking.TotalDP,
                    SalesProgramId: SpkTaking.SalesProgramId,
                    FundSourceId: SpkTaking.FundSourceId,
                    PaymentTypeId: SpkTaking.PaymentTypeId,
                    LeasingId: SpkTaking.LeasingId,
                    LeasingLeasingTenorId: SpkTaking.LeasingLeasingTenorId,
                    LeasingSimulationId: SpkTaking.LeasingSimulationId,
                    InsuranceId: SpkTaking.InsuranceId,
                    InsuranceProductId: SpkTaking.InsuranceProductId,
                    InsuranceFundSourceId: SpkTaking.InsuranceFundSourceId,
                    InsuranceUserTypeId: SpkTaking.InsuranceUserTypeId,
                    InsuranceTypeId: SpkTaking.InsuranceTypeId,
                    WaktuPertanggungan: SpkTaking.WaktuPertanggungan,
                    PolisName: SpkTaking.WaktuPertanggungan,
                    EstimasiBiayaAsuransi: SpkTaking.EstimasiBiayaAsuransi,
                    BankId: SpkTaking.BankId,
                    //BankId: true,
                    TradeInBit: SpkTaking.TradeInBit,
                    Note: SpkTaking.Note,
                    VehiclePrice: SpkTaking.VehiclePrice,
                    BookingFee: SpkTaking.BookingFee,
                    UnnoticedCost: SpkTaking.UnnoticedCost,
                    StatusApprovalDiscountId: SpkTaking.StatusApprovalDiscountId,
                    Diskon: SpkTaking.Diskon,
                    TDP_DP: SpkTaking.TDP_DP,
                    GrandTotal: SpkTaking.GrandTotal,
                    ReceiverDataBit: SpkTaking.ReceiverDataBit,
                    ReceiverName: SpkTaking.ReceiverName,
                    ReceiverAddress: SpkTaking.ReceiverAddress,
                    ReceiverHP: SpkTaking.ReceiverHP,
                    SentUnitDate: SpkTaking.SentUnitDate,
                    EstimateDate: SpkTaking.EstimateDate,
                    StatusPDDId: SpkTaking.StatusPDDId,
                    DeliveryCategoryId: SpkTaking.DeliveryCategoryId,
                    OnOffTheRoadId: SpkTaking.OnOffTheRoadId,
                    PromiseDeliveryMonthId: SpkTaking.PromiseDeliveryMonthId,
                    MediatorName: SpkTaking.MediatorName,
                    MediatorPhoneNo: SpkTaking.MediatorPhoneNo,
                    MediatorKTPNo: SpkTaking.MediatorKTPNo,
                    PemakaiPembeliBit: SpkTaking.PemakaiPembeliBit,
                    PemakaiPemilikBit: SpkTaking.PemakaiPemilikBit,
                    PemakaiName: SpkTaking.PemakaiName,
                    PemakaiHP: SpkTaking.PemakaiHP,
                    PemakaiAddress: SpkTaking.PemakaiAddress,
                    DocumentCompletedBit: SpkTaking.DocumentCompletedBit,
                    URLDigitalSign: SpkTaking.URLDigitalSign,
                    BatchId: SpkTaking.BatchId,
                    PostingDate: SpkTaking.PostingDate,
                    UploadDataDigitalSign: SpkTaking.UploadDataDigitalSign,
                    ListInfoDocument: SpkTaking.ListInfoDocument,
                    ListInfoLeasing: SpkTaking.ListInfoLeasing,
                    ListInfoPelanggan: SpkTaking.ListInfoPelanggan,
                    ListInfoPerluasanJaminan: SpkTaking.ListInfoPerluasanJaminan,
                    ListInfoUnit: SpkTaking.ListInfoUnit,
                    CodeInvoiceTransactionTaxId: SpkTaking.CodeInvoiceTransactionTaxId,
                    FreePPNBit: SpkTaking.FreePPNBit,
                    FreePPnBMBit: SpkTaking.FreePPnBMBit,
                    DiscountSubsidiRate: SpkTaking.DiscountSubsidiRate,
                    PriceAfterDiscount: SpkTaking.PriceAfterDiscount,
                    BBN: SpkTaking.BBN,
                    BBNAdjustment: SpkTaking.BBNAdjustment,
                    BBNServiceAdjustment: SpkTaking.BBNServiceAdjustment,
                    RequestMaxDiscountAcc: SpkTaking.RequestMaxDiscountAcc,
                    RequestTotalDiscount: SpkTaking.RequestTotalDiscount,
                    PelanggaranWilayah: SpkTaking.PelanggaranWilayah
                }]);
            },
            update: function(SpkTaking) {
                for (var i in SpkTaking.ListInfoDocument) {
                    SpkTaking.ListInfoDocument[i].DocumentData = '';
                };

                SpkTaking.SpkDate = tanggal(SpkTaking.SpkDate);

                console.log("DraftSpk Update", SpkTaking);
                return $http.put('/api/sales/SAHSPKDraft', [{
                    KTPNo: SpkTaking.KTPNo,
                    ProspectId: SpkTaking.ProspectId,
                    BookingUnitId: SpkTaking.BookingUnitId,
                    KaroseriPPN: SpkTaking.KaroseriPPN,
					RequestNote: SpkTaking.RequestNote,
                    KaroseriTotalInclVAT: SpkTaking.KaroseriTotalInclVAT,
                    AjuDiscountSummary: SpkTaking.AjuDiscountSummary,
                    Discount: SpkTaking.Discount,
                    DiscountSubsidiDP: parseFloat(SpkTaking.DiscountSubsidiDP),
                    MediatorCommision: parseFloat(SpkTaking.MediatorCommision),
                    DPP: parseFloat(SpkTaking.DPP),
                    PPN: parseFloat(SpkTaking.PPN),
                    TotalBBN: parseFloat(SpkTaking.TotalBBN),
                    PPH22: parseFloat(SpkTaking.PPH22),
                    TotalInclVAT: parseFloat(SpkTaking.TotalInclVAT),
                    BookingFeeFundSourceId: SpkTaking.BookingFeeFundSourceId,
                    BookingFeeReceiveBit: SpkTaking.BookingFeeFundSourceId,
                    VehicleTypeId: SpkTaking.VehicleTypeId,
                    ProductionYear: SpkTaking.ProductionYear,
                    GrandTotal: SpkTaking.GrandTotal,
                    NoteDiscount: SpkTaking.NoteDiscount,
                    CustomerId: SpkTaking.CustomerId,
                    ToyotaId: SpkTaking.ToyotaId,
                    CustomerTypeId: SpkTaking.CustomerTypeId,
                    SalesId: SpkTaking.SalesId,
                    SpkId: SpkTaking.SpkId,
                    SpkDate: SpkTaking.SpkDate,
                    STNKDataBit: SpkTaking.STNKDataBit,
                    PenerimaSTNKBit: SpkTaking.PenerimaSTNKBit,
                    SpkStatusId: SpkTaking.SpkStatusId,
                    SpkCancelId: SpkTaking.SpkCancelId,
                    NoteCancel: SpkTaking.NoteCancel,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    ManualMatchingBit: SpkTaking.ManualMatchingBit,
                    QtyTotalUnit: SpkTaking.QtyTotalUnit,
                    KaroseriId: SpkTaking.KaroseriId,
                    KaroseriPrice: parseFloat(SpkTaking.KaroseriPrice),
                    TotalDP: SpkTaking.TotalDP,
                    SalesProgramId: SpkTaking.SalesProgramId,
                    FundSourceId: SpkTaking.FundSourceId,
                    PaymentTypeId: SpkTaking.PaymentTypeId,
                    LeasingId: SpkTaking.LeasingId,
                    LeasingLeasingTenorId: SpkTaking.LeasingLeasingTenorId,
                    LeasingSimulationId: SpkTaking.LeasingSimulationId,
                    InsuranceId: SpkTaking.InsuranceId,
                    InsuranceProductId: SpkTaking.InsuranceProductId,
                    InsuranceFundSourceId: SpkTaking.InsuranceFundSourceId,
                    InsuranceUserTypeId: SpkTaking.InsuranceUserTypeId,
                    InsuranceTypeId: SpkTaking.InsuranceTypeId,
                    WaktuPertanggungan: SpkTaking.WaktuPertanggungan,
                    PolisName: SpkTaking.WaktuPertanggungan,
                    EstimasiBiayaAsuransi: SpkTaking.EstimasiBiayaAsuransi,
                    BankId: SpkTaking.BankId,
                    TradeInBit: SpkTaking.TradeInBit,
                    Note: SpkTaking.Note,
                    VehiclePrice: SpkTaking.VehiclePrice,
                    BookingFee: SpkTaking.BookingFee,
                    UnnoticedCost: SpkTaking.UnnoticedCost,
                    StatusApprovalDiscountId: SpkTaking.StatusApprovalDiscountId,
                    Diskon: SpkTaking.Diskon,
                    TDP_DP: SpkTaking.TDP_DP,
                    GrandTotal: SpkTaking.GrandTotal,
                    ReceiverDataBit: SpkTaking.ReceiverDataBit,
                    ReceiverName: SpkTaking.ReceiverName,
                    ReceiverAddress: SpkTaking.ReceiverAddress,
                    ReceiverHP: SpkTaking.ReceiverHP,
                    SentUnitDate: SpkTaking.SentUnitDate,
                    EstimateDate: SpkTaking.EstimateDate,
                    StatusPDDId: SpkTaking.StatusPDDId,
                    DeliveryCategoryId: SpkTaking.DeliveryCategoryId,
                    OnOffTheRoadId: SpkTaking.OnOffTheRoadId,
                    PromiseDeliveryMonthId: SpkTaking.PromiseDeliveryMonthId,
                    MediatorName: SpkTaking.MediatorName,
                    MediatorPhoneNo: SpkTaking.MediatorPhoneNo,
                    MediatorKTPNo: SpkTaking.MediatorKTPNo,
                    PemakaiPembeliBit: SpkTaking.PemakaiPembeliBit,
                    PemakaiPemilikBit: SpkTaking.PemakaiPemilikBit,
                    PemakaiName: SpkTaking.PemakaiName,
                    PemakaiHP: SpkTaking.PemakaiHP,
                    PemakaiAddress: SpkTaking.PemakaiAddress,
                    DocumentCompletedBit: SpkTaking.DocumentCompletedBit,
                    URLDigitalSign: SpkTaking.URLDigitalSign,
                    BatchId: SpkTaking.BatchId,
                    PostingDate: SpkTaking.PostingDate,
                    UploadDataDigitalSign: SpkTaking.UploadDataDigitalSign,
                    ListInfoDocument: SpkTaking.ListInfoDocument,
                    ListInfoLeasing: SpkTaking.ListInfoLeasing,
                    ListInfoPelanggan: SpkTaking.ListInfoPelanggan,
                    ListInfoPerluasanJaminan: SpkTaking.ListInfoPerluasanJaminan,
                    ListInfoUnit: SpkTaking.ListInfoUnit,
                    CodeInvoiceTransactionTaxId: SpkTaking.CodeInvoiceTransactionTaxId,
                    FreePPNBit: SpkTaking.FreePPNBit,
                    FreePPnBMBit: SpkTaking.FreePPnBMBit,
                    DiscountSubsidiRate: SpkTaking.DiscountSubsidiRate,
                    PriceAfterDiscount: SpkTaking.PriceAfterDiscount,
                    BBN: SpkTaking.BBN,
                    BBNAdjustment: SpkTaking.BBNAdjustment,
                    BBNServiceAdjustment: SpkTaking.BBNServiceAdjustment,
                    RequestMaxDiscountAcc: SpkTaking.RequestMaxDiscountAcc ,
                    RequestTotalDiscount: SpkTaking.RequestTotalDiscount,
                    PelanggaranWilayah: SpkTaking.PelanggaranWilayah
                }]);
            },
            createAju: function(SpkTaking) {
                SpkTaking.SpkDate = tanggal(SpkTaking.SpkDate);

                return $http.post('/api/sales/SAHSPK', [{
                    KTPNo: SpkTaking.KTPNo,
                    ProspectId: SpkTaking.ProspectId,
                    BookingUnitId: SpkTaking.BookingUnitId,
                    KaroseriPPN: SpkTaking.KaroseriPPN,
                    KaroseriTotalInclVAT: SpkTaking.KaroseriTotalInclVAT,
                    AjuDiscountSummary: SpkTaking.AjuDiscountSummary,
                    Discount: SpkTaking.Discount,
                    RequestNote: SpkTaking.RequestNote,
                    DiscountSubsidiDP: SpkTaking.DiscountSubsidiDP,
                    MediatorCommision: parseFloat(SpkTaking.MediatorCommision),
                    DPP: parseFloat(SpkTaking.DPP),
                    PPN: parseFloat(SpkTaking.PPN),
                    TotalBBN: parseFloat(SpkTaking.TotalBBN),
                    PPH22: parseFloat(SpkTaking.PPH22),
                    TotalInclVAT: parseFloat(SpkTaking.TotalInclVAT),
                    BookingFeeFundSourceId: SpkTaking.BookingFeeFundSourceId,
                    BookingFeeReceiveBit: SpkTaking.BookingFeeFundSourceId,
                    VehicleTypeId: SpkTaking.VehicleTypeId,
                    ProductionYear: SpkTaking.ProductionYear,
                    GrandTotal: SpkTaking.GrandTotal,
                    NoteDiscount: SpkTaking.NoteDiscount,
                    CustomerId: SpkTaking.CustomerId,
                    ToyotaId: SpkTaking.ToyotaId,
                    CustomerTypeId: SpkTaking.CustomerTypeId,
                    SalesId: SpkTaking.SalesId,
                    SpkId: SpkTaking.SpkId,
                    SpkDate: SpkTaking.SpkDate,
                    STNKDataBit: SpkTaking.STNKDataBit,
                    SpkStatusId: SpkTaking.SpkStatusId,
                    SpkCancelId: SpkTaking.SpkCancelId,
                    NoteCancel: SpkTaking.NoteCancel,
                    PenerimaSTNKBit: SpkTaking.PenerimaSTNKBit,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    ManualMatchingBit: SpkTaking.ManualMatchingBit,
                    QtyTotalUnit: SpkTaking.QtyTotalUnit,
                    KaroseriId: SpkTaking.KaroseriId,
                    KaroseriPrice: parseFloat(SpkTaking.KaroseriPrice),
                    TotalDP: SpkTaking.TotalDP,
                    SalesProgramId: SpkTaking.SalesProgramId,
                    FundSourceId: SpkTaking.FundSourceId,
                    PaymentTypeId: SpkTaking.PaymentTypeId,
                    LeasingId: SpkTaking.LeasingId,
                    LeasingLeasingTenorId: SpkTaking.LeasingLeasingTenorId,
                    LeasingSimulationId: SpkTaking.LeasingSimulationId,
                    InsuranceId: SpkTaking.InsuranceId,
                    InsuranceFundSourceId: SpkTaking.InsuranceFundSourceId,
                    InsuranceProductId: SpkTaking.InsuranceProductId,
                    InsuranceUserTypeId: SpkTaking.InsuranceUserTypeId,
                    InsuranceTypeId: SpkTaking.InsuranceTypeId,
                    WaktuPertanggungan: SpkTaking.WaktuPertanggungan,
                    PolisName: SpkTaking.WaktuPertanggungan,
                    EstimasiBiayaAsuransi: SpkTaking.EstimasiBiayaAsuransi,
                    BankId: SpkTaking.BankId,
                    TradeInBit: SpkTaking.TradeInBit,
                    Note: SpkTaking.Note,
                    VehiclePrice: SpkTaking.VehiclePrice,
                    BookingFee: SpkTaking.BookingFee,
                    UnnoticedCost: SpkTaking.UnnoticedCost,
                    StatusApprovalDiscountId: SpkTaking.StatusApprovalDiscountId,
                    Diskon: SpkTaking.Diskon,
                    TDP_DP: SpkTaking.TDP_DP,
                    GrandTotal: SpkTaking.GrandTotal,
                    ReceiverDataBit: SpkTaking.ReceiverDataBit,
                    ReceiverName: SpkTaking.ReceiverName,
                    ReceiverAddress: SpkTaking.ReceiverAddress,
                    ReceiverHP: SpkTaking.ReceiverHP,
                    SentUnitDate: SpkTaking.SentUnitDate,
                    EstimateDate: SpkTaking.EstimateDate,
                    StatusPDDId: SpkTaking.StatusPDDId,
                    DeliveryCategoryId: SpkTaking.DeliveryCategoryId,
                    OnOffTheRoadId: SpkTaking.OnOffTheRoadId,
                    PromiseDeliveryMonthId: SpkTaking.PromiseDeliveryMonthId,
                    MediatorName: SpkTaking.MediatorName,
                    MediatorPhoneNo: SpkTaking.MediatorPhoneNo,
                    MediatorKTPNo: SpkTaking.MediatorKTPNo,
                    PemakaiPembeliBit: SpkTaking.PemakaiPembeliBit,
                    PemakaiPemilikBit: SpkTaking.PemakaiPemilikBit,
                    PemakaiName: SpkTaking.PemakaiName,
                    PemakaiHP: SpkTaking.PemakaiHP,
                    PemakaiAddress: SpkTaking.PemakaiAddress,
                    DocumentCompletedBit: SpkTaking.DocumentCompletedBit,
                    URLDigitalSign: SpkTaking.URLDigitalSign,
                    BatchId: SpkTaking.BatchId,
                    PostingDate: SpkTaking.PostingDate,
                    UploadDataDigitalSign: SpkTaking.UploadDataDigitalSign,
                    ListInfoDocument: SpkTaking.ListInfoDocument,
                    ListInfoLeasing: SpkTaking.ListInfoLeasing,
                    ListInfoPelanggan: SpkTaking.ListInfoPelanggan,
                    ListInfoPerluasanJaminan: SpkTaking.ListInfoPerluasanJaminan,
                    ListInfoUnit: SpkTaking.ListInfoUnit,
                    CodeInvoiceTransactionTaxId: SpkTaking.CodeInvoiceTransactionTaxId,
                    FreePPNBit: SpkTaking.FreePPNBit,
                    FreePPnBMBit: SpkTaking.FreePPnBMBit,
                    DiscountSubsidiRate: SpkTaking.DiscountSubsidiRate,
                    PriceAfterDiscount: SpkTaking.PriceAfterDiscount,
                    BBN: SpkTaking.BBN,
                    BBNAdjustment: SpkTaking.BBNAdjustment,
                    BBNServiceAdjustment: SpkTaking.BBNServiceAdjustment,
                    RequestMaxDiscountAcc: SpkTaking.RequestMaxDiscountAcc,
                    RequestTotalDiscount: SpkTaking.RequestTotalDiscount,
                    PelanggaranWilayah: SpkTaking.PelanggaranWilayah,
                    NoSPKPengganti: SpkTaking.NoSPKPengganti,
                    SPKPenggantiBit: SpkTaking.IsPengganti
                }]);
            },
            ubahAju: function(SpkTaking) {
                for (var i in SpkTaking.ListInfoDocument) {
                    SpkTaking.ListInfoDocument[i].DocumentData = '';
                };

                SpkTaking.SpkDate = tanggal(SpkTaking.SpkDate);

                return $http.put('/api/sales/SAHSPK', [{
                    ProspectId: SpkTaking.ProspectId,
                    BookingUnitId: SpkTaking.BookingUnitId,
                    KaroseriPPN: SpkTaking.KaroseriPPN,
                    AjuDiscountSummary: SpkTaking.AjuDiscountSummary,
                            KaroseriTotalInclVAT: SpkTaking.KaroseriTotalInclVAT,
                            Discount: SpkTaking.Discount,
                    RequestNote: SpkTaking.RequestNote,
                            DiscountSubsidiDP: SpkTaking.DiscountSubsidiDP,
                            MediatorCommision: parseFloat(SpkTaking.MediatorCommision),
                            DPP: parseFloat(SpkTaking.DPP),
                            PPN: parseFloat(SpkTaking.PPN),
                            TotalBBN: parseFloat(SpkTaking.TotalBBN),
                            PPH22: parseFloat(SpkTaking.PPH22),
                            TotalInclVAT: parseFloat(SpkTaking.TotalInclVAT),
                            BookingFeeFundSourceId: SpkTaking.BookingFeeFundSourceId,
                            BookingFeeReceiveBit: SpkTaking.BookingFeeFundSourceId,
                            VehicleTypeId: SpkTaking.VehicleTypeId,
                           ProductionYear: SpkTaking.ProductionYear,
                    GrandTotal: SpkTaking.GrandTotal,
                            NoteDiscount: SpkTaking.NoteDiscount,
                    CustomerId: SpkTaking.CustomerId,
                    ToyotaId: SpkTaking.ToyotaId,
                    CustomerTypeId: SpkTaking.CustomerTypeId,
                    SalesId: SpkTaking.SalesId,
                    SpkId: SpkTaking.SpkId,
                    PenerimaSTNKBit: SpkTaking.PenerimaSTNKBit,
                    FormSpkId: SpkTaking.FormSpkId,
                    SpkDate: SpkTaking.SpkDate,
                    STNKDataBit: SpkTaking.STNKDataBit,
                    SpkStatusId: SpkTaking.SpkStatusId,
                    SpkCancelId: SpkTaking.SpkCancelId,
                    NoteCancel: SpkTaking.NoteCancel,
                    SpkLostId: SpkTaking.SpkLostId,
                    NoteLost: SpkTaking.NoteLost,
                    ManualMatchingBit: SpkTaking.ManualMatchingBit,
                    QtyTotalUnit: SpkTaking.QtyTotalUnit,
                    KaroseriId: SpkTaking.KaroseriId,
                    KaroseriPrice: parseFloat(SpkTaking.KaroseriPrice),
                    TotalDP: SpkTaking.TotalDP,
                    SalesProgramId: SpkTaking.SalesProgramId,
                    FundSourceId: SpkTaking.FundSourceId,
                    PaymentTypeId: SpkTaking.PaymentTypeId,
                    LeasingId: SpkTaking.LeasingId,
                    LeasingLeasingTenorId: SpkTaking.LeasingLeasingTenorId,
                    LeasingSimulationId: SpkTaking.LeasingSimulationId,
                    InsuranceId: SpkTaking.InsuranceId,
                    InsuranceFundSourceId: SpkTaking.InsuranceFundSourceId,
                    InsuranceProductId: SpkTaking.InsuranceProductId,
                    InsuranceUserTypeId: SpkTaking.InsuranceUserTypeId,
                    InsuranceTypeId: SpkTaking.InsuranceTypeId,
                    WaktuPertanggungan: SpkTaking.WaktuPertanggungan,
                    PolisName: SpkTaking.WaktuPertanggungan,
                    EstimasiBiayaAsuransi: SpkTaking.EstimasiBiayaAsuransi,
                    BankId: SpkTaking.BankId,
                    TradeInBit: SpkTaking.TradeInBit,
                    Note: SpkTaking.Note,
                    VehiclePrice: SpkTaking.VehiclePrice,
                    BookingFee: SpkTaking.BookingFee,
                    UnnoticedCost: SpkTaking.UnnoticedCost,
                    StatusApprovalDiscountId: SpkTaking.StatusApprovalDiscountId,
                    Diskon: SpkTaking.Diskon,
                    TDP_DP: SpkTaking.TDP_DP,
                    GrandTotal: SpkTaking.GrandTotal,
                    ReceiverDataBit: SpkTaking.ReceiverDataBit,
                    ReceiverName: SpkTaking.ReceiverName,
                    ReceiverAddress: SpkTaking.ReceiverAddress,
                    ReceiverHP: SpkTaking.ReceiverHP,
                    SentUnitDate: SpkTaking.SentUnitDate,
                    EstimateDate: SpkTaking.EstimateDate,
                    StatusPDDId: SpkTaking.StatusPDDId,
                    DeliveryCategoryId: SpkTaking.DeliveryCategoryId,
                    OnOffTheRoadId: SpkTaking.OnOffTheRoadId,
                    PromiseDeliveryMonthId: SpkTaking.PromiseDeliveryMonthId,
                    MediatorName: SpkTaking.MediatorName,
                    MediatorPhoneNo: SpkTaking.MediatorPhoneNo,
                    MediatorKTPNo: SpkTaking.MediatorKTPNo,
                    PemakaiPembeliBit: SpkTaking.PemakaiPembeliBit,
                    PemakaiPemilikBit: SpkTaking.PemakaiPemilikBit,
                    PemakaiName: SpkTaking.PemakaiName,
                    PemakaiHP: SpkTaking.PemakaiHP,
                    PemakaiAddress: SpkTaking.PemakaiAddress,
                    DocumentCompletedBit: SpkTaking.DocumentCompletedBit,
                    URLDigitalSign: SpkTaking.URLDigitalSign,
                    BatchId: SpkTaking.BatchId,
                    PostingDate: SpkTaking.PostingDate,
                    UploadDataDigitalSign: SpkTaking.UploadDataDigitalSign,
                    ListInfoDocument: SpkTaking.ListInfoDocument,
                    ListInfoLeasing: SpkTaking.ListInfoLeasing,
                    ListInfoPelanggan: SpkTaking.ListInfoPelanggan,
                    ListInfoPerluasanJaminan: SpkTaking.ListInfoPerluasanJaminan,
                    ListInfoUnit: SpkTaking.ListInfoUnit,
                    CodeInvoiceTransactionTaxId: SpkTaking.CodeInvoiceTransactionTaxId,
                    FreePPNBit: SpkTaking.FreePPNBit,
                    FreePPnBMBit: SpkTaking.FreePPnBMBit,
                    DiscountSubsidiRate: SpkTaking.DiscountSubsidiRate,
                    PriceAfterDiscount: SpkTaking.PriceAfterDiscount,
                    BBN: SpkTaking.BBN,
                    BBNAdjustment: SpkTaking.BBNAdjustment,
                    BBNServiceAdjustment: SpkTaking.BBNServiceAdjustment,
                    RequestMaxDiscountAcc: SpkTaking.RequestMaxDiscountAcc ,
                    RequestTotalDiscount: SpkTaking.RequestTotalDiscount,
                    PelanggaranWilayah: SpkTaking.PelanggaranWilayah
                }]);
            },
            cekPwlwil: function(SpkTaking){
                var provinsi = null, kabupaten = null, kecamatan = null;
                if(SpkTaking.CustomerTypeId == 3){
                    provinsi = SpkTaking.ListInfoPelanggan[1].ProvinceId
                    kabupaten = SpkTaking.ListInfoPelanggan[1].CityRegencyId
                    kecamatan = SpkTaking.ListInfoPelanggan[1].DistrictId
                }else{
                    provinsi = SpkTaking.ListInfoPelanggan[2].ProvinceId
                    kabupaten = SpkTaking.ListInfoPelanggan[2].CityRegencyId
                    kecamatan = SpkTaking.ListInfoPelanggan[2].DistrictId
                }
               var res = $http.get('/api/sales/ProspectingCekPelWil/?ProvinceId=' + provinsi + '&CityRegencyId=' + kabupaten + '&DistrictId=' + kecamatan);
               return res;
            },
            freeUnitBooking: function(BookingUnitId) {
                return $http.put('/api/sales/BookingUnitBatalReserved', [{
                    BookingUnitId: BookingUnitId
                }]);
            },
            stopNhour: function(SpkTaking) {
                return $http.put('/api/sales/StopEndHour', [{
                    BookingUnitId: SpkTaking.BookingUnitId,
                    CashTransferBit: SpkTaking.CashTransferBit,
                    BookingFeeReceiveBit: SpkTaking.BookingFeeReceiveBit
                }])
            },
            bookingFeeDp: function(VehicleType) {
                var res = $http.get('/api/sales/FFinanceMasterVehicleBookingFeeDP/?vehicleTypeId=' + VehicleType);
                return res;
            },

            searchSPKpengganti: function(EmployeeId, NoSPK) {
                return $http.post('/api/sales/SAHSPK/SPKPengganti', [{
                    EmployeeId : EmployeeId,
                    FormSPKNo : NoSPK
                }])
            }


        }
    });
//ddd