angular.module('app')
    .controller('CreateSpkController', function (MaintenanceSpkFactory, ModelFactory, CreateSpkFactory, bsNotify, $rootScope, $scope, $http, CurrentUser, ComboBoxFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            $scope.select = 1;
            // $scope.formvalidation = {};
            // $scope.formvalidInfoIndividu = {};
            // $scope.formvalidPengiriman = {};
            // $scope.formRincianharga = {};
        });
        //----------------------------------
        // Initialization
        //----------------------------------;
        //IfGotProblemWithModal();
        $scope.$on('$destroy', function () {
            //angular.element('.ui.modal.signature').remove();      
            angular.element('.ui.modal.DraftSPK').remove();
            angular.element('.ui.modal.dokumentList').remove();
            angular.element('.ui.modal.dokumenSPKViewMobiles').remove();
            angular.element('.ui.modal.dokumenSPKViewMobilesDraft').remove();
            angular.element('.ui.modal.ModalApprovalDicountDraft').remove();
            angular.element('.ui.modal.ModalTambahAksesoris').remove();
            angular.element('.ui.modal.Karoseri').remove();
            angular.element('.ui.modal.signature').remove();
            angular.element('.ui.modal.ModalStopNHour').remove();
            angular.element('.ui.modal.CreateSpkAjuOffTheRoad').remove();
        });

        $scope.dataselect = [{ ValueId: 1, ValueName: 'Select1' }, { ValueId: 2, ValueName: 'Select2' }, { ValueId: 3, ValueName: 'Select3' }];
        $scope.objectSelect = {};

        $scope.changeMethod = function () {
            console.log("panggil select");
        }

        $scope.komisiMediator = function() {
            if($scope.selected_data.MediatorCommision > 0){
                bsNotify.show({
					title:"Peringatan",
					content:"Nama Mediator dan No. KTP Mediator harus diisi",
					type:"warning"
				});
            }
        }

        $scope.user = CurrentUser.user();
        $scope.date = new Date();
        $scope.null = null;
        $scope.mspkTaking = null; //Modelsss
        $scope.statusAction = null;
        $scope.searchAccesoriestype = {};
        $scope.individuPembeli = false, $scope.individuPemilik = false;
        $scope.formDataPembeliPerusahaan = false;
        $scope.formDataPenanggungJawabPerusahaan = false;
        $scope.formDataPemilikPerusahaan = false;
        $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
        $scope.selected_data = {};
        $scope.selected_data.ListInfoDocument = [];
        $scope.selected_data.ListInfoPelanggan = [];
        //$scope.selected_data.ListInfoPelanggan =[{ProvinceId:null,CityRegencyId:null,DistrictId:null},{ProvinceId:null,CityRegencyId:null,DistrictId:null},{ProvinceId:null,CityRegencyId:null,DistrictId:null}];
        $scope.selected_data.ListInfoPerluasanJaminan = [];
        $scope.selected_data.ListInfoUnit = [];
        $scope.selected_data.GrandTotal = 0;
        $scope.listspk = true;
        $scope.uploadFiles = [];
        $scope.signStatus = null;
        $scope.mspkTaking = null; //Model
        //$scope.menusekarang = "Daftar Draft SPK";
        $scope.statusKembali = 'kembali';
        $scope.xRole = { selected: [] };
        $scope.individuPembeli = false, $scope.individuPemilik = false;
        $scope.signature = { dataUrl: null };
        $scope.arrayJsData = [];
        $scope.jsData = { Title: null, HeaderInputs: { $TotalACC$: 0, $OutletId$: null, $VehicleTypeColorId$: null, $VehicleYear$: null } }
        $scope.Satuan = false;
        $scope.searchAccesories = {};
        $scope.simpan = [];
        $scope.ZeroEmpty = 0;
        $scope.CreateSpkOfftheRoadOperation = "";
        $scope.showModelInput = false;
        $scope.showCustomerInput = true;
        $scope.disabledAjuSPK = false;

        $scope.IsPengganti = false;


        // $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
        $scope.urlAccessoriesSpk = '/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
        $scope.urlAccessoriesPackageSpk = '/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1';

        //$scope.recalculate = false;

        $scope.tinggi = window.screen.availHeight;
        $scope.lebar = window.screen.availWidth;

        $scope.gambar = {
            "max-width": $scope.lebar - 40,
            "max-height": $scope.tinggi - 40
        }

        //console.log("jkshkjhd", $scope.tinggi, $scope.lebar);

        // $scope.setelahrefresh = function(){
        //     console.log("AFTER REFRESH");
        // }

        $scope.tanggalDiscount = {
            format: "dd-MM-yyyy"
        }

        $scope.dateOptionsSPK = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.bind_data = {
            data: [],
            filter: []
        };

        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            var patternRegex
            if (type == 1) {
                patternRegex = /\d|[-]/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /[a-zA-Z]|[0-9]|[ ]|[.,/-]/i; //ALPHANUMERIC ONLY
                  // if (item.includes("*")) {
                //     event.preventDefault();
                //     return false;
                // }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };
                
        $scope.allowPatternName = function(event, type, item) {
            console.log("event", event);
            var patternRegex
            if (type == 1) {
                patternRegex = /[a-zA-Z]|[ ]|[-']/i; // NAME ONLY 
            } else if (type == 2) {
                patternRegex = /[a-zA-Z]|[0-9]|[ ]|[''",.()-]|[&]/i; //ALPHANUMERIC ONLY
                // if (item.includes("*")) {
                //     event.preventDefault();
                //     return false;
                // }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };
        
        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        // $scope.saveDraft = function () {
        //     for (var i in $scope.selectedData){
        //         $scope.selectedData[i].BirthDate = $scope.changeFormatDate($scope.selectedData[i].BirthDate)
        //     }
        // }
        $scope.filterData = {
            defaultFilter: [
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'Status', value: 'SPKStatusName' },
                { name: 'Nama', value: 'CustomerName' },
                { name: 'Model', value: 'ModelType' },
                { name: 'Kategori Pelanggan', value: 'CustomerTypeDesc' },
            ],
            advancedFilter: [
                { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 1 },
                { name: 'Status', value: 'SPKStatusName' },
                { name: 'Nama', value: 'CustomerName', typeMandatory: 1 },
                { name: 'Model Tipe', value: 'VehicleModelId', typeMandatory: 0 },
                { name: 'Kategori Pelanggan', value: 'CustomerTypeDesc' },
                { name: 'Tanggal Mulai SPK', value: 'SPKStartDate' },
                { name: 'Tanggal Akhir SPK', value: 'SPKEndDate' },
            ]
        };



        CreateSpkFactory.getSignConfig().then(function (res) {
            $scope.IsSign = res.data;
            console.log("testsign", $scope.IsSign);
        })

        $scope.breadcrums = {};

        $scope.CreateSpkAkalAkalan = function (selectedFilterCreateSpk, textFilterCreateSpk) {
            if (selectedFilterCreateSpk == "FormSPKNo") {
                console.log('selectedFilterCreateSpk ====>', selectedFilterCreateSpk);
                $scope.bind_data.filter.CustomerName = null;
                $scope.bind_data.filter.FormSPKNo = textFilterCreateSpk;
                $scope.bind_data.filter.ModelType = null;
                $scope.bind_data.filter.CustomerTypeDesc = null;
                $scope.bind_data.filter.SPKStatusName = null;
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
            }
            else if (selectedFilterCreateSpk == "ModelType") {
                console.log('selectedFilterCreateSpk ====>', selectedFilterCreateSpk);
                $scope.bind_data.filter.CustomerName = null;
                $scope.bind_data.filter.FormSPKNo = null;
                $scope.bind_data.filter.ModelType = textFilterCreateSpk;
                $scope.bind_data.filter.SPKStatusName = null;
                $scope.bind_data.filter.CustomerTypeDesc = null;
                $scope.showModelInput = true;
                $scope.showCustomerInput = false;
            }
            else if (selectedFilterCreateSpk == "CustomerName") {
                console.log('selectedFilterCreateSpk ====>', selectedFilterCreateSpk);
                $scope.bind_data.filter.CustomerName = textFilterCreateSpk;
                $scope.bind_data.filter.FormSPKNo = null;
                $scope.bind_data.filter.ModelType = null;
                $scope.bind_data.filter.CustomerTypeDesc = null;
                $scope.bind_data.filter.SPKStatusName = null;
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
            }
            else if (selectedFilterCreateSpk == "SPKStatusName") {
                console.log('selectedFilterCreateSpk ====>', selectedFilterCreateSpk);
                $scope.bind_data.filter.CustomerName = null;
                $scope.bind_data.filter.SPKStatusName = textFilterCreateSpk;
                $scope.bind_data.filter.FormSPKNo = null;
                $scope.bind_data.filter.ModelType = null;
                $scope.bind_data.filter.CustomerTypeDesc = null;
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
            }
            // else if (selectedFilterCreateSpk == "CustomerTypeDesc") {
            else {
                console.log('selectedFilterCreateSpk ====>', selectedFilterCreateSpk);
                $scope.bind_data.filter.CustomerName = null;
                $scope.bind_data.filter.FormSPKNo = null;
                $scope.bind_data.filter.SPKStatusName = null;
                $scope.bind_data.filter.ModelType = null;
                $scope.bind_data.filter.CustomerTypeDesc = textFilterCreateSpk;
                $scope.showModelInput = false;
                $scope.showCustomerInput = true;
            }
            console.log('textFilterCreateSpk ===>', textFilterCreateSpk);

        }



        ModelFactory.getData().then(function (res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        $scope.CreateSpkModelSearchChange = function (selected) {

            $scope.bind_data.filter.VehicleModelId = selected.VehicleModelId;
        }

        $scope.CreateSpkIlanginSearch = function () {
            document.getElementById("DaTextFilterCreateSpk").value = "";
            document.getElementById("DaTextFilterCreateSpk").value = null;

            $scope.CreateSpkModelSearch = null;
            $scope.bind_data.filter.VehicleModelId = null;
            document.getElementById("DaModelFilterCreateSpk").value = "";
            document.getElementById("DaModelFilterCreateSpk").value = null;
        }

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2)
                return fix;
            } else {
                return null;
            }
        };

        $scope.CreateSpkChangeSpkStartDate = function (obj) {
            $scope.bind_data.filter.SPKStartDate = fixDate(obj);
        }

        $scope.CreateSpkChangeSpkEndDate = function (obj) {
            $scope.bind_data.filter.SPKEndDate = fixDate(obj);
        }

        $scope.assignDataDaftarSPK = function () {
            $scope.statusAction = 'lihat';
            $scope.breadcrums.title = "Lihat Detail";
            $scope.tinggi = window.screen.availHeight;
            $scope.lebar = window.screen.availWidth;

            $scope.gambar = {
                "max-width": $scope.lebar - 40,
                "max-height": $scope.tinggi - 40
            }
            $scope.tab = angular.copy($rootScope.breadcrumsTab);
            $scope.selected_data = $rootScope.selected_data;
            ComboBoxFactory.getDataVehicleModel().then(function (res) { $scope.modelname = res.data.Result });
            ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + $scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.warna = res.data.Result;
            });
        };

        $scope.btnkeluarLeasingdraft = function () {
            angular.element(".ui.modal.ModalApprovalDicountDraft").modal("hide");
        }

        $scope.aprovalLeasing = function () {
            angular.element(".ui.modal.ModalApprovalLeasing").modal("show");
            angular.element('.ui.modal.ModalApprovalLeasing').not(':first').remove();
        }

        $scope.aprovalDiscountDraft = function (data) {
            CreateSpkFactory.getApprovalDiscount(data.SpkId).then(function (res) {
                setTimeout(function () {
                    angular.element('.ui.modal.ModalApprovalDicountDraft').modal('refresh');
                }, 0);
                angular.element(".ui.modal.ModalApprovalDicountDraft").modal("show");
                $scope.listDicount = res.data.Result[0];
                //angular.element('.ui.modal.ModalApprovalDicountDraft').not(':first').remove();
            });
        }

        $scope.aprovalOffTheRoad = function (data) {
            CreateSpkFactory.getApprovalOffTheRoad(data.SpkId).then(function (res) {
                setTimeout(function () {
                    angular.element('.ui.modal.ModalApprovalOffTheRoadDraft').modal('refresh');
                }, 0);
                angular.element(".ui.modal.ModalApprovalOffTheRoadDraft").modal("show");
                $scope.ListOffTheRoad = res.data.Result;
                //angular.element('.ui.modal.ModalApprovalOffTheRoadDraft').not(':first').remove();
            });
        }

        $scope.btnkeluarApprovalOffTheRoad = function () {
            angular.element(".ui.modal.ModalApprovalOffTheRoadDraft").modal("hide");
        }

        $rootScope.$on("buatSPKBookingUnit", function () {
            $scope.statusAction = 'buat';
            $scope.BookingUnit = angular.copy($rootScope.BookingUnit);
            $scope.PopulateKategory();
            $scope.bookingUnitSPK();
        });


        $scope.statusUnit = 'prospect';
        $scope.bookingUnitSPK = function () {
            $scope.statusUnit = 'bookingunit';
            $scope.statusAction = 'buat';
            $scope.statusKembali = 'book';
            $scope.selected_data.SpkDate = new Date();

            console.log('test', $scope.BookingUnit);

            $scope.selected_data.EndHourStatus = true;

            if ($scope.BookingUnit.CustomerTypeId == 1) {
                $scope.selected_data.CustomerTypeId = 3;
            } else {
                $scope.selected_data.CustomerTypeId = $scope.BookingUnit.CustomerTypeId
            }

            $scope.selected_data.CustomerId = $scope.BookingUnit.CustomerId;
            $scope.selected_data.ProspectId = $scope.BookingUnit.ProspectId;
            $scope.selected_data.ProspectCode = $scope.BookingUnit.ProspectCode;
            $scope.selected_data.SalesId = $scope.BookingUnit.SalesId;
            $scope.selected_data.BookingUnitId = $scope.BookingUnit.BookingUnitId;
            $scope.selected_data.ToyotaId = $scope.BookingUnit.ToyotaId;

            $scope.selected_data.BookingFeeReceiveBit = false;

            if ($scope.BookingUnit.OffTheRoad == false || $scope.BookingUnit.OffTheRoad == null) {
                $scope.selected_data.OnOffTheRoadId = 1;
            } else {
                $scope.selected_data.OnOffTheRoadId = 2;
            }

            if ($scope.BookingUnit.DirectDeliveryPDC == false || $scope.BookingUnit.DirectDeliveryPDC == null) {
                $scope.selected_data.DeliveryCategoryId = 2;
            } else {
                $scope.selected_data.DeliveryCategoryId = 1;
            }

            //$scope.selected_data.TradeInBit = $scope.BookingUnit.TradeInBit;
            if ($scope.BookingUnit.TradeInBit == null || $scope.BookingUnit.TradeInBit == false) {
                $scope.selected_data.TradeInBit = false;
            } else {
                $scope.selected_data.TradeInBit = $scope.BookingUnit.TradeInBit
            }

            $scope.selected_data.BookingFee = $scope.BookingUnit.BookingFee || 0.0;
            $scope.selected_data.GrandTotal = $scope.BookingUnit.GrandTotal || 0.0;
            $scope.selected_data.Discount = $scope.BookingUnit.RequestDiscount || 0.0;
            $scope.selected_data.VehiclePrice = $scope.BookingUnit.VehiclePrice || 0.0;

            $scope.selected_data.TDP_DP = 0;
            $scope.selected_data.DiscountSubsidiDP = $scope.BookingUnit.DiscountSubsidiDP || 0.0;
            $scope.selected_data.DiscountSubsidiRate = $scope.BookingUnit.DiscountSubsidiRate || 0.0;
            $scope.selected_data.MediatorCommision = $scope.BookingUnit.MediatorCommision || 0.0;
            $scope.selected_data.BBNAdjustment = $scope.BookingUnit.BBNAdjustment || 0.0;
            $scope.selected_data.BBNServiceAdjustment = $scope.BookingUnit.BBNServiceAdjustment || 0.0;
            $scope.selected_data.PriceAfterDiscount = $scope.BookingUnit.PriceAfterDiscount || 0.0;
            $scope.selected_data.DPP = $scope.BookingUnit.DPP || 0.0;
            $scope.selected_data.PPN = $scope.BookingUnit.PPN || 0.0;
            $scope.selected_data.BBN = $scope.BookingUnit.BBN || 0.0;
            $scope.selected_data.TotalBBN = $scope.BookingUnit.TotalBBN || 0.0;
            $scope.selected_data.PPH22 = $scope.BookingUnit.PPH22 || 0.0;
            $scope.selected_data.TotalInclVAT = $scope.BookingUnit.TotalInclVAT || 0.0;

            $scope.selected_data.KaroseriId = $scope.BookingUnit.KaroseriId;
            $scope.selected_data.KaroseriName = $scope.BookingUnit.KaroseriName;
            $scope.selected_data.KaroseriPrice = $scope.BookingUnit.KaroseriPrice;
            $scope.selected_data.KaroseriPPN = $scope.BookingUnit.PPNKaroseri;
            $scope.selected_data.KaroseriTotalInclVAT = $scope.BookingUnit.TotalInclVATKaroseri;
            //$scope.selected_data. = $scope.BookingUnit.ListDetailBookingUnit[0].KaroseriDPP;

            $scope.selected_data.LeasingId = $scope.BookingUnit.LeasingId;
            $scope.selected_data.LeasingLeasingTenorId = $scope.BookingUnit.LeasingLeasingTenorId;

            //$scope.selected_data.TradeInBit = $scope.BookingUnit.TradeInBit;
            $scope.selected_data.FundSourceId = $scope.BookingUnit.FundSourceId;
            console.log('tipe fundsource??',$scope.selected_data.FundSourceName);
            if($scope.selected_data.FundSourceName == 'Cash'){
                console.log('masuk sini kahh??');
                $scope.statusLeasing = 'mati';
                $scope.bankstastus = false;
                $scope.selected_data.LeasingId = null;
                $scope.selected_data.LeasingLeasingTenorId = null;
            }else{
                $scope.statusLeasing = false;
                $scope.bankstastus = 'hidup';
             
            }

            if (angular.isUndefined($scope.BookingUnit.DPP) == true) {
                $scope.selected_data.DPP = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.PPH22) == true) {
                $scope.selected_data.PPH22 = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.PPN) == true) {
                $scope.selected_data.PPN = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.TotalBBN) == true) {
                $scope.selected_data.TotalBBN = 0.0;
            }

            if (angular.isUndefined($scope.BookingUnit.TotalInclVAT) == true) {
                $scope.selected_data.TotalInclVAT = 0.0;
            }

            $scope.selected_data.EstimateDate = $scope.BookingUnit.SentUnitDateEstimateDate;
            $scope.selected_data.SentUnitDate = $scope.BookingUnit.SentUnitDate;
            $scope.selected_data.StatusPDDId = $scope.BookingUnit.StatusPDDId;

            if ($scope.selected_data.CustomerTypeId == 3) {
                $scope.selected_data.ListInfoPelanggan[0] = {
                    CustomerCorrespondentId: 1,
                    CustomerName: $scope.BookingUnit.ProspectName,
                    CustomerAddress: $scope.BookingUnit.Address,
                    ProvinceId: $scope.BookingUnit.ProvinceId,
                    CityRegencyId: $scope.BookingUnit.CityRegencyId,
                    DistrictId: $scope.BookingUnit.DistrictId,
                    VillageId: $scope.BookingUnit.VillageId,
                    PostalCode: $scope.BookingUnit.PostalCode,
                    RT: $scope.BookingUnit.RT,
                    RW: $scope.BookingUnit.RW, //fitri
                    HpNumber: $scope.BookingUnit.HP,
                    Email: $scope.BookingUnit.Email,
                    PhoneNumber: $scope.BookingUnit.PhoneNumber,
                    NickName: $scope.BookingUnit.NickName,
                    BirthDate: $scope.BookingUnit.Birthdate,
                    Job: $scope.BookingUnit.WorkTitle,
                    CustomerPositionId: $scope.BookingUnit.CustomerPositionId,
                    CustomerReligionId: $scope.BookingUnit.CustomerReligionId,
                    OfficeAddress: $scope.BookingUnit.OfficeAddress,
                    OfficePhone: $scope.BookingUnit.OfficePhone,
                    InstanceName: $scope.BookingUnit.InstanceName
                }
            } else {
                $scope.selected_data.ListInfoPelanggan[0] = {
                    CustomerCorrespondentId: 1,
                    CustomerName: $scope.BookingUnit.InstanceName,
                    InstanceAddress: $scope.BookingUnit.Address,
                    ProvinceId: $scope.BookingUnit.ProvinceId,
                    CityRegencyId: $scope.BookingUnit.CityRegencyId,
                    DistrictId: $scope.BookingUnit.DistrictId,
                    VillageId: $scope.BookingUnit.VillageId,
                    PostalCode: $scope.BookingUnit.PostalCode,
                    RT: $scope.BookingUnit.RT,
                    RW: $scope.BookingUnit.RW, //fitri
                    PhoneNumber: $scope.BookingUnit.PhoneNumber,
                    OfficePhone: $scope.BookingUnit.PhoneNumber,
                    InstanceName: $scope.BookingUnit.InstanceName
                }
            }

            if ($scope.selected_data.CustomerTypeId == 3) {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            } else {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 2 });
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            }

            if ($scope.selected_data.CustomerTypeId > 3) {
                $scope.selected_data.ListInfoPelanggan[1] = {
                    CustomerCorrespondentId: 2,
                    CustomerName: $scope.BookingUnit.ProspectName,
                    HpNumber: $scope.BookingUnit.HP,
                    Email: $scope.BookingUnit.Email
                }
            }

            // $scope.selected_data.ListInfoUnit[0].NumberPlateId = $scope.BookingUnit.ListDetailBookingUnit[0].NumberPlateId;

            $scope.selected_data.ListInfoUnit[0] = {
                "VehicleTypeId": $scope.BookingUnit.VehicleTypeId,
                "Description": $scope.BookingUnit.Description,
                //"KatashikiCode" : 
                //"Suffix" :
                "VehicleModelId": $scope.BookingUnit.VehicleModelId,
                "Qty": $scope.BookingUnit.Qty,
                "ColorId": $scope.BookingUnit.ColorId,
                "ColorName": $scope.BookingUnit.ColorName,
                "ProductionYear": $scope.BookingUnit.ListDetailBookingUnit[0].AssemblyYear,
                "ListDetailUnit": []
            };

            for (var i in $scope.BookingUnit.ListDetailBookingUnit) {
                $scope.selected_data.ListInfoUnit[0].ListDetailUnit.push({
                    VehicleTypeId: $scope.BookingUnit.ListDetailBookingUnit[i].VehicleTypeId,
                    Description: $scope.BookingUnit.Description,
                    VehicleModelId: $scope.BookingUnit.VehicleModelId,
                    ColorId: $scope.BookingUnit.ColorId,
                    ColorName: $scope.BookingUnit.ColorName,
                    RRNNo: $scope.BookingUnit.ListDetailBookingUnit[i].RRNNo,
                    FrameNo: $scope.BookingUnit.ListDetailBookingUnit[i].FrameNo,
                    SpecialReqNumberPlate: $scope.BookingUnit.ListDetailBookingUnit[i].PoliceNo,
                    GrandTotalAcc: 0,
                    DPP: $scope.BookingUnit.ListDetailBookingUnit[i].DPP,
                    ListAccessories: $scope.BookingUnit.ListDetailBookingUnit[i].ListDetailAccBookingUnit,
                    ListAccessoriesPackage: $scope.BookingUnit.ListDetailBookingUnit[i].ListDetailAccBookingUnitPackage
                });

            }

            for (var i in $scope.selected_data.ListInfoUnit[0].ListDetailUnit) {
                //$scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc = 0;
                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage.length > 0) {
                    for (var j in $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc =
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc +
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessoriesPackage[j].TotalInclVAT;
                    };
                }

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories.length > 0) {
                    for (var j in $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc =
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc +
                            $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].ListAccessories[j].TotalInclVAT;
                    };
                }

            }

            for (var i in $scope.selected_data.ListInfoUnit) {
                $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = 0;
                for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {
                    $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll = 0;
                    $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll =
                        $scope.selected_data.TotalInclVAT + $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].GrandTotalAcc + $scope.selected_data.KaroseriTotalInclVAT;

                    $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = $scope.selected_data.ListInfoUnit[i].GrandTotalUnit + $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll;
                }
            }
            $scope.selected_data.ListInfoUnit[0].NumberPlateId = $scope.BookingUnit.ListDetailBookingUnit[0].NumberPlateId;

            CreateSpkFactory.getSignConfig().then(function (res) {
                $scope.IsSign = res.data;
            });

            ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + $scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.warna = res.data.Result;
            });

            CreateSpkFactory.bookingFeeDp($scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.tempTDP = angular.copy(res.data.NominalDP);
                $scope.selected_data.TDP_DP = res.data.NominalDP;
            })


        };
        //tambahan norman
        $scope.getNPWP = function (datanpwp) {
            if (datanpwp == null || datanpwp == '' || typeof datanpwp == 'undefined') {
                $scope.selectedNPWPBit = false;
            } else {
                $scope.selectedNPWPBit = true;
            }
            $scope.selected_data.recalculate = true;
        }

         $scope.statusLeasing = 'mati'; 
        $scope.CreateSpkSumberDanaChanged = function (selectedFundsource){
            console.log('selectedFundsource',selectedFundsource);
            
            $scope.dataFundSource = $scope.fundsource.filter(function (type) {
                return (selectedFundsource == type.FundSourceId);
            })
            console.log('$scope.dataFundSource',$scope.dataFundSource)
            if($scope.dataFundSource[0].FundSourceName == 'Cash'){
                $scope.selected_data.FundSourceName = 'Cash'
                $scope.statusLeasing = 'mati';
                $scope.bankstastus = false;
                $scope.selected_data.LeasingId = null;
                $scope.selected_data.LeasingLeasingTenorId = null;
            }else{
                $scope.selected_data.FundSourceName = 'Credit'
                $scope.statusLeasing = 'hidup';
                $scope.bankstastus = true;
                bsNotify.show({
                    title: "Peringatan",
                    content: "Anda memilih sumber dana kredit, pastikan anda memilih leasing dan tenor.",
                    type: 'warning'
                });
               
            }

            console.log('statusleasing',$scope.statusLeasing);
            console.log('statusacion',$scope.statusAction);
            console.log('selecteddata',$scope.selected_data.FundSourceName);

        }
        //end tambahan norman

        $scope.assignData = function () {
            $scope.prospectToSpk = {};
            $scope.statusAction = 'buat';
            $scope.statusUnit = 'prospect';
            $scope.statusKembali == 'kembali';
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.prospectToSpk = $rootScope.prospectToSpk;
            console.log("hasil ini", $scope.prospectToSpk, $rootScope.prospectToSpk)
            $scope.selected_data.recalculate = true;

            $scope.selected_data.CustomerId = $rootScope.selected_data.CustomerId;
            if ($rootScope.selected_data.CustomerTypeId == 1) {
                $scope.selected_data.CustomerTypeId = 3;
            }

            $scope.selected_data.SPKPenggantiBit = $rootScope.selected_data.SPKPenggantiBit;

            if($scope.selected_data.SPKPenggantiBit != null) {
                $scope.IsPengganti = $scope.selected_data.SPKPenggantiBit;
            }
            
            $scope.selected_data.SpkDate = new Date();
            $scope.selected_data.CustomerTypeId = $rootScope.selected_data.CustomerTypeId;
            $scope.selected_data.ProspectId = $rootScope.selected_data.ProspectId;
            $scope.selected_data.ProspectCode = $rootScope.selected_data.ProspectCode;
            $scope.selected_data.SalesId = $rootScope.selected_data.SalesId;
            $scope.selected_data.ToyotaId = $rootScope.selected_data.ToyotaId;
            $scope.selected_data.FundSourceId = $rootScope.selected_data.FundSourceId;
            $scope.selected_data.FundSourceName = $rootScope.selected_data.FundSourceName;

            if($scope.prospectToSpk.FundSourceName == 'Cash'){
                $scope.statusLeasing = 'mati';
                $scope.bankstastus = false;
                $scope.selected_data.LeasingId = null;
                $scope.selected_data.LeasingLeasingTenorId = null;
            }else{
                $scope.statusLeasing = false;
                $scope.bankstastus = 'hidup';
             
            }
            

            console.log('selected_data.ToyotaId', $scope.selected_data);
            if ($scope.selected_data.ToyotaId == null || $scope.selected_data.ToyotaId == '' || typeof $scope.selected_data.ToyotaId == 'undefined') {
                $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
            }
            else {
                $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20006&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
            }

            if ($rootScope.selected_data.TradeInBit != null) {
                $scope.selected_data.TradeInBit = $rootScope.selected_data.TradeInBit;
            } else {
                $scope.selected_data.TradeInBit = false;
            }

            $scope.selected_data.ListInfoUnit = $rootScope.selected_data.ListInfoUnit;
            $scope.selected_data.ListInfoUnit[0].NumberPlateId = 1;
            $scope.selected_data.OnOffTheRoadId = 1;
            $scope.selected_data.DeliveryCategoryId = 2;
            $scope.selected_data.Discount = 0;
            $scope.selected_data.DiscountSubsidiDP = 0;
            $scope.selected_data.MediatorCommision = 0;
            $scope.selected_data.DiscountSubsidiRate = 0;
            $scope.selected_data.BBNAdjustment = 0;
            $scope.selected_data.BBNServiceAdjustment = 0;
            $scope.selected_data.PriceAfterDiscount = 0;
            if ($scope.statusAction = 'buat') {
                $scope.selected_data.GrandTotal = 0;
            } else {
                setTimeout(function () {
                    $scope.selected_data.GrandTotal = $scope.selected_data.GrandTotal;
                }, 3000);

            }
            $scope.selected_data.ProductionYear = $rootScope.selected_data.ListInfoUnit[0].ProductionYear;
            $scope.selected_data.EstimateDate = null;
            $scope.selected_data.SentUnitDate = null;

            if ($scope.selected_data.CustomerTypeId == 3) {
                $scope.selected_data.ListInfoPelanggan[0] = {
                    CustomerCorrespondentId: 1,
                    CustomerName: $scope.prospectToSpk.ProspectName,
                    CustomerAddress: $scope.prospectToSpk.Address,
                    ProvinceId: $scope.prospectToSpk.ProvinceId,
                    CityRegencyId: $scope.prospectToSpk.CityRegencyId,
                    DistrictId: $scope.prospectToSpk.DistrictId,
                    VillageId: $scope.prospectToSpk.VillageId,
                    PostalCode: $scope.prospectToSpk.PostalCode,
                    RT: $scope.prospectToSpk.RT,
                    RW: $scope.prospectToSpk.RW,
                    HpNumber: $scope.prospectToSpk.HP,
                    Email: $scope.prospectToSpk.Email,
                    PhoneNumber: $scope.prospectToSpk.PhoneNumber,
                    NickName: $scope.prospectToSpk.NickName,
                    BirthDate: $scope.prospectToSpk.Birthdate,
                    Job: $scope.prospectToSpk.WorkTitle,
                    CustomerPositionId: $scope.prospectToSpk.CustomerPositionId,
                    CustomerReligionId: $scope.prospectToSpk.CustomerReligionId,
                    OfficeAddress: $scope.prospectToSpk.OfficeAddress,
                    OfficePhone: $scope.prospectToSpk.OfficePhone,
                    InstanceName: $scope.prospectToSpk.InstanceName
                }
            } else {
                $scope.selected_data.ListInfoPelanggan[0] = {
                    CustomerCorrespondentId: 1,
                    //CustomerName: $scope.prospectToSpk.InstanceName,
                    InstanceAddress: $scope.prospectToSpk.Address,
                    ProvinceId: $scope.prospectToSpk.ProvinceId,
                    CityRegencyId: $scope.prospectToSpk.CityRegencyId,
                    DistrictId: $scope.prospectToSpk.DistrictId,
                    VillageId: $scope.prospectToSpk.VillageId,
                    PostalCode: $scope.prospectToSpk.PostalCode,
                    RT: $scope.prospectToSpk.RT,
                    RW: $scope.prospectToSpk.RW,
                    PhoneNumber: $scope.prospectToSpk.PhoneNumber,
                    OfficePhone: $scope.prospectToSpk.PhoneNumber,
                    InstanceName: $scope.prospectToSpk.InstanceName
                }

            }

            if ($scope.selected_data.CustomerTypeId == 3) {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            } else {
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 2 });
                $scope.selected_data.ListInfoPelanggan.push({ CustomerCorrespondentId: 3 });
            }

            if ($scope.selected_data.CustomerTypeId > 3) {
                $scope.selected_data.ListInfoPelanggan[1] = {
                    CustomerCorrespondentId: 2,
                    CustomerName: $scope.prospectToSpk.ProspectName,
                    HpNumber: $scope.prospectToSpk.HP,
                    Email: $scope.prospectToSpk.Email
                }
            }

            // $scope.filterKabupaten($scope.selected_data.ListInfoPelanggan[0].ProvinceId);
            // $scope.filterKecamatan($scope.selected_data.ListInfoPelanggan[0].CityRegencyId);
            // $scope.filterKelurahan($scope.selected_data.ListInfoPelanggan[0].DistrictId);

            // $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
            // $scope.urlAccessoriesSpk = '/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
            // $scope.urlAccessoriesPackageSpk = '/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1';

            // $scope.jsData.HeaderInputs.$InputTotalAcc$ = 200000;

            CreateSpkFactory.getSignConfig().then(function (res) {
                $scope.IsSign = res.data;
            });

            CreateSpkFactory.bookingFeeDp($scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.tempTDP = angular.copy(res.data.NominalDP);
                $scope.selected_data.TDP_DP = res.data.NominalDP;

                $scope.tempBookingFee = angular.copy(res.data.NominalBookingFee);
                $scope.selected_data.BookingFee = res.data.NominalBookingFee;
            });

            ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + $scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.warna = res.data.Result;
            });

            ComboBoxFactory.getUnitYear().then(function (res) { $scope.unitYear = res.data.Result; });

            setTimeout(function () {
                $scope.selected_data.FundSourceId = $rootScope.prospectToSpk.FundSourceId;
                $scope.selected_data.ListInfoUnit[0].ProductionYear = $rootScope.selected_data.ListInfoUnit[0].ProductionYear;
            }, 0);

            // $scope.$watch("selected_data.", function(Id, oldData){
            //     if (Id != undefined) {
            //         ComboBoxFactory.getKodePajakbyId(Id).then(function (res) {
            //             $scope.KodePajak = res.data;
            //             if (($scope.KodePajak.Code == '01') || ($scope.KodePajak.Code == '08')) {
            //                 $scope.PPNbit = true;
            //                 $scope.PPNBmBit = true;
            //                 if ($scope.KodePajak.Code == '08') {
            //                     $scope.selected_data.FreePPnBMBit = false;
            //                     $scope.selected_data.FreePPNBit = false;
            //                 }
            //             } else if (($scope.KodePajak.Code != '01') || ($scope.KodePajak.Code != '08')) {
            //                 $scope.PPNbit = false;
            //                 $scope.PPNBmBit = false;
            //                 if ($scope.KodePajak.Code == '07') {
            //                     $scope.selected_data.FreePPnBMBit = false;
            //                     $scope.selected_data.FreePPNBit = true;
            //                 } else if ($scope.KodePajak.Code != '07') {
            //                     $scope.selected_data.FreePPnBMBit = false;
            //                     $scope.selected_data.FreePPNBit = false;
            //                 }
            //             }
            //         })
            //     }
            // })

            // console.log("%%%****&#!%@^$%$@%^%@%#++===>fundSource di spk", $scope.selected_data.FundSourceId);
            // console.log("spkoioioio", $scope.selected_data);
        }

        $rootScope.$on("buatSPKProspect", function () {
            //$scope.formspk = true;
            $scope.PopulateKategory();
            $scope.assignData();
        });

        $rootScope.$on("SPKdariDaftar", function () {
            //$scope.formspk = true;
            $scope.tinggi = window.screen.availHeight;
            $scope.lebar = window.screen.availWidth;

            $scope.gambar = {
                "max-width": $scope.lebar - 40,
                "max-height": $scope.tinggi - 40
            }
            $scope.PopulateKategory();
            $scope.assignDataDaftarSPK();
        });

        $scope.dataSama = function () {
            if ($scope.selected_data.STNKDataBit) {
                $scope.selected_data.STNKDataBit = true;
                $scope.selected_data.ListInfoPelanggan[1].KTPNo = angular.copy($scope.selected_data.ListInfoPelanggan[0].KTPNo);
                $scope.selected_data.ListInfoPelanggan[1].CustomerName = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerName);
                $scope.selected_data.ListInfoPelanggan[1].CustomerAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerAddress);
                $scope.selected_data.ListInfoPelanggan[1].ProvinceId = angular.copy($scope.selected_data.ListInfoPelanggan[0].ProvinceId);
                $scope.selected_data.ListInfoPelanggan[1].CityRegencyId = angular.copy($scope.selected_data.ListInfoPelanggan[0].CityRegencyId);
                $scope.selected_data.ListInfoPelanggan[1].DistrictId = angular.copy($scope.selected_data.ListInfoPelanggan[0].DistrictId);
                $scope.selected_data.ListInfoPelanggan[1].VillageId = angular.copy($scope.selected_data.ListInfoPelanggan[0].VillageId);
                $scope.selected_data.ListInfoPelanggan[1].RT= angular.copy($scope.selected_data.ListInfoPelanggan[0].RT);
                $scope.selected_data.ListInfoPelanggan[1].RW = angular.copy($scope.selected_data.ListInfoPelanggan[0].RW); //fitri
                $scope.selected_data.ListInfoPelanggan[1].VillageId = angular.copy($scope.selected_data.ListInfoPelanggan[0].VillageId);
                $scope.selected_data.ListInfoPelanggan[1].PostalCode = angular.copy($scope.selected_data.ListInfoPelanggan[0].PostalCode);
                $scope.selected_data.ListInfoPelanggan[1].Job = angular.copy($scope.selected_data.ListInfoPelanggan[0].Job);
                $scope.selected_data.ListInfoPelanggan[1].PhoneNumber = angular.copy($scope.selected_data.ListInfoPelanggan[0].PhoneNumber);
                $scope.selected_data.ListInfoPelanggan[1].HpNumber = angular.copy($scope.selected_data.ListInfoPelanggan[0].HpNumber);
                $scope.selected_data.ListInfoPelanggan[1].Email = angular.copy($scope.selected_data.ListInfoPelanggan[0].Email);
            } else {
                $scope.selected_data.STNKDataBit = false;
                $scope.selected_data.ListInfoPelanggan[1].KTPNo = "";
                $scope.selected_data.ListInfoPelanggan[1].CustomerName = "";
                $scope.selected_data.ListInfoPelanggan[1].CustomerAddress = "";
                $scope.selected_data.ListInfoPelanggan[1].ProvinceId = "";
                $scope.selected_data.ListInfoPelanggan[1].CityRegencyId = "";
                $scope.selected_data.ListInfoPelanggan[1].DistrictId = "";
                $scope.selected_data.ListInfoPelanggan[1].VillageId = "";
                $scope.selected_data.ListInfoPelanggan[1].RT = "";
                $scope.selected_data.ListInfoPelanggan[1].RW = ""; //fitri
                $scope.selected_data.ListInfoPelanggan[1].VillageId = "";
                $scope.selected_data.ListInfoPelanggan[1].PostalCode = "";
                $scope.selected_data.ListInfoPelanggan[1].Job = "";
                $scope.selected_data.ListInfoPelanggan[1].PhoneNumber = "";
                $scope.selected_data.ListInfoPelanggan[1].HpNumber = "";
                $scope.selected_data.ListInfoPelanggan[1].Email = "";
            }
        }

        $scope.dataSamainstansi = function () {
            if ($scope.selected_data.STNKDataBit) {
                $scope.selected_data.STNKDataBit = true;
                $scope.selected_data.ListInfoPelanggan[2].SIUPNo = angular.copy($scope.selected_data.ListInfoPelanggan[0].SIUPNo);
                $scope.selected_data.ListInfoPelanggan[2].TDPNo = angular.copy($scope.selected_data.ListInfoPelanggan[0].TDPNo);
                $scope.selected_data.ListInfoPelanggan[2].KTPNo = angular.copy($scope.selected_data.ListInfoPelanggan[0].KTPNo);
                $scope.selected_data.ListInfoPelanggan[2].InstanceName = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceName);
                $scope.selected_data.ListInfoPelanggan[2].InstanceAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceAddress);
                $scope.selected_data.ListInfoPelanggan[2].CustomerName = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceName);
                $scope.selected_data.ListInfoPelanggan[2].CustomerAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceAddress);
                $scope.selected_data.ListInfoPelanggan[2].ProvinceId = angular.copy($scope.selected_data.ListInfoPelanggan[0].ProvinceId);
                $scope.selected_data.ListInfoPelanggan[2].CityRegencyId = angular.copy($scope.selected_data.ListInfoPelanggan[0].CityRegencyId);
                $scope.selected_data.ListInfoPelanggan[2].DistrictId = angular.copy($scope.selected_data.ListInfoPelanggan[0].DistrictId);
                $scope.selected_data.ListInfoPelanggan[2].VillageId = angular.copy($scope.selected_data.ListInfoPelanggan[0].VillageId);
                $scope.selected_data.ListInfoPelanggan[2].RT = angular.copy($scope.selected_data.ListInfoPelanggan[0].RT);
                $scope.selected_data.ListInfoPelanggan[2].RW = angular.copy($scope.selected_data.ListInfoPelanggan[0].RW); //fitri
                $scope.selected_data.ListInfoPelanggan[2].PostalCode = angular.copy($scope.selected_data.ListInfoPelanggan[0].PostalCode); 
                $scope.selected_data.ListInfoPelanggan[2].Job = angular.copy($scope.selected_data.ListInfoPelanggan[0].Job);
                $scope.selected_data.ListInfoPelanggan[2].OfficePhone = angular.copy($scope.selected_data.ListInfoPelanggan[0].OfficePhone);
                $scope.selected_data.ListInfoPelanggan[2].PhoneNumber = angular.copy($scope.selected_data.ListInfoPelanggan[0].OfficePhone);
                $scope.selected_data.ListInfoPelanggan[2].NPWPNo = angular.copy($scope.selected_data.ListInfoPelanggan[0].NPWPNo);
                $scope.selected_data.ListInfoPelanggan[2].SectorBusinessId = angular.copy($scope.selected_data.ListInfoPelanggan[0].SectorBusinessId);
            } else {
                $scope.selected_data.STNKDataBit = false;
                $scope.selected_data.ListInfoPelanggan[2].SIUPNo = "";
                $scope.selected_data.ListInfoPelanggan[2].TDPNo = "";
                $scope.selected_data.ListInfoPelanggan[2].KTPNo = "";
                $scope.selected_data.ListInfoPelanggan[2].InstanceName = "";
                $scope.selected_data.ListInfoPelanggan[2].InstanceAddress = "";
                $scope.selected_data.ListInfoPelanggan[2].CustomerName = "";
                $scope.selected_data.ListInfoPelanggan[2].CustomerAddress = "";
                $scope.selected_data.ListInfoPelanggan[2].ProvinceId = "";
                $scope.selected_data.ListInfoPelanggan[2].CityRegencyId = "";
                $scope.selected_data.ListInfoPelanggan[2].DistrictId = "";
                $scope.selected_data.ListInfoPelanggan[2].VillageId = "";
                $scope.selected_data.ListInfoPelanggan[2].RT = "";
                $scope.selected_data.ListInfoPelanggan[2].RW = ""; //fitri
                $scope.selected_data.ListInfoPelanggan[2].PostalCode = "";
                $scope.selected_data.ListInfoPelanggan[2].Job = "";
                $scope.selected_data.ListInfoPelanggan[2].OfficePhone = "";
                $scope.selected_data.ListInfoPelanggan[2].PhoneNumber = "";
                $scope.selected_data.ListInfoPelanggan[2].NPWPNo = "";
                $scope.selected_data.ListInfoPelanggan[2].SectorBusinessId = "";
            }
        }

        $scope.equalInfoPengiriman = function () {
            if ($scope.selected_data.PenerimaSTNKBit == true) {
                $scope.selected_data.PenerimaSTNKBit = true;
                if ($scope.selected_data.CustomerTypeId > 3) {
                    $scope.selected_data.ReceiverName = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerName);
                    $scope.selected_data.ReceiverAddress = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerAddress);
                    if($scope.selected_data.ReceiverHP == undefined || $scope.selected_data.ReceiverHP == null || $scope.selected_data.ReceiverHP == "" ){
                        $scope.selected_data.ReceiverHP = angular.copy($scope.selected_data.ListInfoPelanggan[2].PhoneNumber);
                    }else{
                        $scope.selected_data.ReceiverHP = angular.copy($scope.selected_data.ReceiverHP);
                    }
                } else {
                    $scope.selected_data.ReceiverName = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerName);
                    $scope.selected_data.ReceiverAddress = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerAddress);
                    $scope.selected_data.ReceiverHP = angular.copy($scope.selected_data.ListInfoPelanggan[1].HpNumber);
                }
            } else {
                $scope.selected_data.PenerimaSTNKBit = false;
                $scope.selected_data.ReceiverName = angular.copy($scope.selected_data.ReceiverName);
                $scope.selected_data.ReceiverAddress = angular.copy($scope.selected_data.ReceiverAddress);
                $scope.selected_data.ReceiverHP = angular.copy($scope.selected_data.ReceiverHP);
            }
        }

        $scope.equalPemakaiPembeli = function () {
            if ($scope.selected_data.PemakaiPembeliBit) {
                $scope.selected_data.PemakaiPembeliBit = true;
                $scope.selected_data.PemakaiPemilikBit = false;
                if ($scope.selected_data.CustomerTypeId > 3) {
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceAddress);
                    // $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].PhoneNumber);
                    if($scope.selected_data.ListInfoPelanggan[0].HpNumber == undefined || $scope.selected_data.ListInfoPelanggan[0].HpNumber == null || $scope.selected_data.ListInfoPelanggan[0].HpNumber == "" ){
                        $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].PhoneNumber);
                    }else{
                        $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].HpNumber);
                    }
                } else {
                    $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].HpNumber);
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerAddress);
                }
            } else {
                $scope.selected_data.PemakaiPembeliBit = false;
                $scope.selected_data.PemakaiName = null;
                $scope.selected_data.PemakaiAddress = null;
                $scope.selected_data.PemakaiHP = null;
            }
        }

        $scope.equalPemakaiPemilik = function () {
            if ($scope.selected_data.PemakaiPemilikBit) {
                $scope.selected_data.PemakaiPembeliBit = false;
                $scope.selected_data.PemakaiPemilikBit = true;
                if ($scope.selected_data.CustomerTypeId > 3) {
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerAddress);
                    // $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[2].HpNumber); Awalnya ngambil dari selected_data.Pemakai HP, Kalau di uncheck / check ambil dari selected_data.Pemakai HP
                    if($scope.selected_data.ListInfoPelanggan[0].HpNumber == undefined || $scope.selected_data.ListInfoPelanggan[0].HpNumber == null || $scope.selected_data.ListInfoPelanggan[0].HpNumber == "" ){
                        $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].PhoneNumber);
                    }else{
                        $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].HpNumber);
                    }
                } else {
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerAddress);
                    $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[1].HpNumber);
                }

            } else {
                $scope.selected_data.PemakaiPemilikBit = false;
                $scope.selected_data.PemakaiName = null;
                $scope.selected_data.PemakaiAddress = null;
                $scope.selected_data.PemakaiHP = null;
            }
        }

        $scope.selectPembayaran = function (selected) {
            $scope.metodpemabayaran = selected;
            if ($scope.metodpemabayaran.PaymentTypeName == "Cash") {
                $scope.bankstastus = false;
            } else {
                $scope.bankstastus = true;
            }
        }

        $scope.$watch("selected_data.PaymentTypeId", function (newValue, oldValue) {
            var temp = {};
            for (var i in $scope.jenispembayaran) {
                if ($scope.jenispembayaran[i].PaymentTypeId == newValue) {
                    temp = $scope.jenispembayaran[i];
                }
            }
            if (temp.PaymentTypeName == "Cash") {
                $scope.bankstastus = false;
                $scope.selected_data.BankId = null;
            } else {
                $scope.bankstastus = true;
            }
        });

        $scope.tambahWarna = function () {
            $scope.selected_data.recalculate = true;

            $scope.selected_data.ListInfoUnit.push({
                ColorId: null,
                SpecialReqNumberPlate: null,
                Qty: 1,
                Description: $scope.selected_data.ListInfoUnit[0].Description,
                NumberPlateId: $scope.selected_data.ListInfoUnit[0].NumberPlateId,
                VehicleTypeId: $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                ListDetailUnit: [],
                ProductionYear: $scope.selected_data.ListInfoUnit[0].ProductionYear
            });

            var length = $scope.selected_data.ListInfoUnit.length;

            $scope.selected_data.ListInfoUnit[length - 1].ListDetailUnit.push({
                ColorId: null,
                Description: $scope.selected_data.ListInfoUnit[length - 1].Description,
                ColorName: null,
                GrandTotalAcc: 0.0,
                ListAccessories: [],
                ListAccessoriesPackage: [],
                SpecialReqNumberPlate: null,
                OutletId: $scope.user.OutletId,
                VehicleTypeId: $scope.selected_data.ListInfoUnit[length - 1].VehicleTypeId,
                VehicleTypeColorId: null,
                ProductionYear: $scope.selected_data.ListInfoUnit[length - 1].ProductionYear
            });
        }

        $scope.hapusUnit = function (list, index) {
            $scope.selected_data.recalculate = true;
            $scope.selected_data.ListInfoUnit.splice(index, 1);
        }

        $scope.tambahUnit = function (list, index) {
            $scope.selected_data.recalculate = true;
            list.Qty = list.Qty + 1;

            $scope.selected_data.ListInfoUnit[index].ListDetailUnit.push({
                ColorId: list.ColorId,
                Description: list.Description,
                ColorName: list.ColorName,
                GrandTotalAcc: null,
                ListAccessories: [],
                ListAccessoriesPackage: [],
                SpecialReqNumberPlate: null,
                OutletId: $scope.user.OutletId,
                VehicleModelId: list.VehicleModelId,
                VehicleTypeId: list.VehicleTypeId,
                VehicleTypeColorId: $scope.selected_data.ListInfoUnit[index].ListDetailUnit[0].VehicleTypeColorId,
                ProductionYear: $scope.selected_data.ListInfoUnit[0].ProductionYear
            });
        }

        $scope.rubahWarna = function (select, list) {
            console.log("warna", select);
            var warnaselect = $scope.warna.filter(function (type) {
                return (select == type.ColorId);
            })
            console.log("warna", warnaselect);
            list.ColorName = warnaselect[0].ColorName;
            list.VehicleTypeColorId = warnaselect[0].VehicleTypeColorId;
            for (var i in list.ListDetailUnit) {
                list.ListDetailUnit[i].ColorId = list.ColorId;
                list.ListDetailUnit[i].ColorName = warnaselect[0].ColorName;
                list.ListDetailUnit[i].Description = list.ListDetailUnit[i].Description;
                list.ListDetailUnit[i].GrandTotalAcc = list.ListDetailUnit[i].GrandTotalAcc;
                list.ListDetailUnit[i].ListAccessories = list.ListDetailUnit[i].ListAccessories;
                list.ListDetailUnit[i].ListAccessoriesPackage = list.ListDetailUnit[i].ListAccessoriesPackage;
                list.ListDetailUnit[i].SpecialReqNumberPlate = list.ListDetailUnit[i].SpecialReqNumberPlate;
                list.ListDetailUnit[i].OutletId = $scope.user.OutletId;
                list.ListDetailUnit[i].VehicleModelId = list.ListDetailUnit[i].VehicleModelId;
                list.ListDetailUnit[i].VehicleTypeId = list.ListDetailUnit[i].VehicleTypeId;
                list.ListDetailUnit[i].VehicleTypeColorId = warnaselect[0].VehicleTypeColorId;
                list.ListDetailUnit[i].ProductionYear = list.ListDetailUnit[i].ProductionYear;
            }
        }

        $scope.kurangUnit = function (list, index) {
            $scope.selected_data.recalculate = true;
            list.Qty = list.Qty - 1;
            var length = list.ListDetailUnit.length;
            $scope.selected_data.ListInfoUnit[index].ListDetailUnit.splice(length - 1, 1);
        }

        $scope.grandTotalUnit = { GrandTotalAll: 0 };

        $scope.recalculatechange = function (data) {
            if (data == null || data == undefined || data == 0) { data = undefined; }
            else { data = data; }
            $scope.selected_data.recalculate = true;
            return data;
        }

        $scope.setNull = function (data) { if (data == 0) { data = undefined; } else { data = data; } return data; }
        $scope.setZero = function (data) { if (data == null || data == undefined) { data = 0; } else { data = data; } return data; }

        $scope.callculateGrandTotalAll = function () {
            //$scope.selected_data.GrandTotal = 0;
            //console.log("kesini ga")
            for (var i in $scope.selected_data.ListInfoUnit) {
                $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = 0;
                for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {
                    $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc = 0;
                    $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll = 0;
                }
            }

            for (var i in $scope.selected_data.ListInfoUnit) {
                for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {

                    $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll =
                        $scope.selected_data.TotalInclVAT +
                        $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc;

                    $scope.selected_data.ListInfoUnit[i].GrandTotalUnit =
                        $scope.selected_data.ListInfoUnit[i].GrandTotalUnit +
                        $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll;
                }

                $scope.selected_data.GrandTotal =
                    // $scope.selected_data.TotalInclVAT * $scope.selected_data.QtyUnit +
                    // $scope.selected_data.TotalHargaAcc;

                    // CR 4
                    $scope.selected_data.GrandTotal = Math.floor(
                    $scope.selected_data.TotalInclVAT * $scope.selected_data.QtyUnit +
                    $scope.selected_data.TotalHargaAcc);
            }

        }

        $rootScope.$on("GrandTotalSPK", function () {
            $scope.callculateGrandTotalAll();
        });

        $scope.getdatalist = function () {
            CreateSpkFactory.getData().then(function (res) {
                $scope.bind_data.data = [];
                $scope.loading = false;
            });
        }

        $scope.buatSPK = function (data) {
            //$scope.getGlobalMediator();
            $scope.buatSPKProspect = true;
            $scope.formData = false;
            angular.element('.ui.modal.ModalProspect').modal('hide');
            $scope.selected_data.ProspectId = data.ProspectId;
            $scope.selected_data.ProspectCode = data.ProspectCode;
            $scope.selected_data.BookingUnitId = data.BookingUnitId;
            $scope.selected_data.SalesId = data.SalesId;
            $scope.selected_data.CustomerTypeId = data.CustomerTypeId;
            $scope.selected_data.CustomerId = data.CustomerId;
            $scope.selected_data.ToyotaId = data.ToyotaId;
            $scope.selected_data.ListInfoUnit = data.CProspectDetailUnitView;
            //console.log('data dari prospect',data);

            for (var i = 0; i < $scope.selected_data.ListInfoUnit[0].Qty; i++) {
                $scope.selected_data.ListInfoUnit[0].ListDetailUnit[i].push({
                    OutletId: $scope.selected_data.ListInfoUnit[0].OutletId,
                    SpkId: $scope.selected_data.ListInfoUnit[0].SpkId,
                    SpkDetailInfoUnitId: $scope.selected_data.ListInfoUnit[0].SpkDetailInfoUnitId,
                    VehicleTypeId: $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                    Description: $scope.selected_data.ListInfoUnit[0].Description,
                    KatashikiCode: $scope.selected_data.ListInfoUnit[0].KatashikiCode,
                    SuffixCode: $scope.selected_data.ListInfoUnit[0].SuffixCode,
                    ModelType: $scope.selected_data.ListInfoUnit[0].ModelType,
                    VehicleModelId: $scope.selected_data.ListInfoUnit[0].VehicleModelId,
                    ColorId: $scope.selected_data.ListInfoUnit[0].ColorId,
                    ColorName: $scope.selected_data.ListInfoUnit[0].ColorName,
                    VehicleTypeColorId: $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId,
                    RRNNo: null,
                    FrameNo: null,
                    ListAccessories: [],
                    ListAccessoriesPackage: [],
                });
            }

            if($scope.selected_data.FundSourceName == 'Cash'){
                $scope.statusLeasing = 'mati';
                $scope.bankstastus = false;
                $scope.selected_data.LeasingId = null;
                $scope.selected_data.LeasingLeasingTenorId = null;
            }else{
                $scope.statusLeasing = false;
                $scope.bankstastus = 'hidup';
             
            }

        }


        $scope.statusSimpan = false;
        $scope.back = function () {
            $scope.formspk = false;
            $scope.buatSPKProspect = false;
            $scope.formData = true;
            $scope.listspk = true;
            // $scope.selected_data = {};
            $rootScope.$emit("kembaliDaftarSPK", {});
            $rootScope.$emit("KembalidariProspect", {});
            $rootScope.$emit("KembalidariSpkBook", {});

            if ($scope.statusKembali != 'save' && $scope.statusKembali != 'book') {
                $rootScope.$emit("kembaliCekbookingList", {});
            } else if ($scope.statusKembali == 'book') {
                $rootScope.$emit("KembalidariSpkBook", {});
            } else {
                $rootScope.$emit("kembaliKebookingList", {});
            }

            // Free Booking Unit
            if ($scope.statusUnit == 'bookingunit' && $scope.statusSimpan == false) {
                CreateSpkFactory.freeUnitBooking($scope.selected_data.BookingUnitId).then(function (res) {

                });
            }

        }

        // Set Default Kode Pajak
        $scope.PPNbit = false;
        $scope.PPNBmBit = false;
        $scope.TaxCodeChange = function (Id) {
            
            if (Id != undefined) {
                ComboBoxFactory.getKodePajakbyId(Id).then(function (res) {
                    $scope.KodePajak = res.data;
                    if (($scope.KodePajak.Code == '01') || ($scope.KodePajak.Code == '08')) {
                        $scope.PPNbit = true;
                        $scope.PPNBmBit = true;
                        $scope.selected_data.FreePPnBMBit = false;
                        $scope.selected_data.FreePPNBit = false;
                    } else if (($scope.KodePajak.Code != '01') || ($scope.KodePajak.Code != '08')) {
                        $scope.PPNbit = false;
                        $scope.PPNBmBit = false;
                        if ($scope.KodePajak.Code == '07') {
                            $scope.selected_data.FreePPnBMBit = false;
                            $scope.selected_data.FreePPNBit = true;
                        } else if ($scope.KodePajak.Code != '07') {
                            $scope.selected_data.FreePPnBMBit = false;
                            $scope.selected_data.FreePPNBit = false;
                        }
                    }
                })
            }
        }

        $scope.saveDraft = function () {
            $scope.CreateSpkOfftheRoadOperation = "Draft";//kalo counternya BukanDraft
            $scope.IsSign = false;
            $scope.signStatus = "draft";
            $scope.KlikAjuSPK = true;
            $scope.klikDraft = false;

            for(var i in $scope.selected_data.ListInfoPelanggan){
                // $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate)
                if($scope.selected_data.ListInfoPelanggan[i].BirthDate != null || $scope.selected_data.ListInfoPelanggan[i].BirthDate != undefined){
                    $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate) 
                }
           
            }
             // $scope.selected_data.ListInfoPelanggan.BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan.BirthDate)

            console.log("tess==> ", $scope.selected_data.ListInfoPelanggan,  $scope.selected_data.ListInfoPelanggan.birthDate)
            
            if($scope.selected_data.TDP_DP > $scope.selected_data.GrandTotal ){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Nilai TDP/DP Tidak Boleh Lebih Besar Dari Total Harga.",
                    type: 'warning'
                });
                $scope.KlikAjuSPK = false;
                $scope.klikDraft = false;
            }else {

                if ($scope.selected_data.OnOffTheRoadId != 2) {
                    if ($scope.selected_data.BookingFee > $scope.selected_data.TDP_DP) {
                        bsNotify.show({
                            title: "Error Message",
                            content: "TDP Lebih Kecil Dari BookingFee",
                            type: 'danger'
                        });
                        setTimeout(function(){
                            $scope.KlikAjuSPK = false;
                            $scope.klikDraft = false;
                        },5000);

                       
                    } else {
                        if (($scope.selected_data.BookingUnitId != null) &&
                            $scope.selected_data.EndHourStatus == true &&
                            ($scope.selected_data.BookingFeeReceiveBit == null || $scope.selected_data.BookingFeeReceiveBit == false)
                        ) {
                            angular.element('.ui.modal.ModalStopNHour').modal('show');
                            angular.element('.ui.modal.ModalStopNHour').not(':first').remove();
                        } else {
                            $scope.saveDraftNHour();
                        }
                    }
                }
                else if ($scope.selected_data.OnOffTheRoadId == 2) {
                    $scope.selected_data.RequestNote = "";
                    angular.element('.ui.modal.CreateSpkAjuOffTheRoad').modal('show');
                    angular.element('.ui.modal.CreateSpkAjuOffTheRoad').not(':first').remove();
                }
                setTimeout(function(){
                    $scope.KlikAjuSPK = false;
                    $scope.klikDraft = false;
                },5000);


            }

            


        }

        $scope.saveDraftNHour = function () {
            CreateSpkFactory.getGlobalMediator().then(function (res) {
                $scope.mediatorbit = res.data.Result[0].mediatorbit;
                console.log('Cek Mediator', $scope.mediatorbit)

            var tempMaxdiscount = maxDiscAcc($scope.selected_data);
            var tempQtyUnit = 1;

            if (typeof $scope.selected_data.QtyUnit == 'undefined' || $scope.selected_data.QtyUnit == null || $scope.selected_data.QtyUnit == '') {
                tempQtyUnit = $scope.selected_data.QtyTotalUnit;
            } else {
                tempQtyUnit = $scope.selected_data.QtyUnit;
            }

            if (typeof $scope.selected_data.QtyTotalUnit == 'undefined' || $scope.selected_data.QtyTotalUnit == null || $scope.selected_data.QtyTotalUnit == '') {
                tempQtyUnit = $scope.selected_data.QtyUnit;
            } else {
                tempQtyUnit = $scope.selected_data.QtyTotalUnit;
            }

            console.log('ini selected data saveDraftNHour ===>', $scope.selected_data)

            var accSatuan = $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories;
            var accPackage = $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage;
            var totalDiscAcc = 0;
            var totalDiscAccPackage = 0;

            if (accSatuan != null || accSatuan != [] || accSatuan != undefined) {
                for (var i = 0; i < accSatuan.length; i++) {

                    totalDiscAcc += (accSatuan[i].Discount * accSatuan[i].Qty)
                }
                console.log('totalDiscAcc ===>', totalDiscAcc);
            }

            if (accPackage != null || accPackage != [] || accPackage != undefined) {
                for (var i = 0; i < accPackage.length; i++) {

                    totalDiscAccPackage += (accPackage[i].Discount * accPackage[i].Qty);
                }
                console.log('totalDiscAccPackage ===>', totalDiscAccPackage);
            }

            $scope.RequestMaxDiscountAcc = totalDiscAcc + totalDiscAccPackage;
            $scope.selected_data.RequestMaxDiscountAcc = $scope.RequestMaxDiscountAcc;

            if($scope.mediatorbit == 1){
                $scope.totalSumDisc = ($scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty) + tempMaxdiscount + $scope.selected_data.DiscountSubsidiRate + $scope.selected_data.DiscountSubsidiDP + $scope.selected_data.MediatorCommision + totalDiscAccPackage;
            } else {
                $scope.totalSumDisc = ($scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty) + tempMaxdiscount + $scope.selected_data.DiscountSubsidiRate + $scope.selected_data.DiscountSubsidiDP + totalDiscAccPackage;
            }
            $scope.selected_data.RequestTotalDiscount = angular.copy($scope.totalSumDisc);
            $scope.tempDiscountUnit = $scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty;
            $scope.tempDiscAcc = tempMaxdiscount + totalDiscAccPackage;
            $scope.tempDiscSubRate = $scope.selected_data.DiscountSubsidiRate;
            $scope.tempDiscSubDP = $scope.selected_data.DiscountSubsidiDP;
            $scope.tempKomisiMediator = $scope.selected_data.MediatorCommision;

            console.log(' ini hasil setelah kalkulasi saveDraftNHour ===>', $scope.selected_data);
            // console.log('$scope.selected_data.Discount',$scope.selected_data.Discount);
            // console.log('$scope.selected_data.ListInfoUnit[0].Qty',$scope.selected_data.ListInfoUnit[0].Qty);
            // console.log('tempMaxdiscount',tempMaxdiscount);
            // console.log('$scope.selected_data.DiscountSubsidiRate',$scope.selected_data.DiscountSubsidiRate);
            // console.log('$scope.selected_data.DiscountSubsidiDP',$scope.selected_data.DiscountSubsidiDP);
            // console.log('totalDiscAccPackage',totalDiscAccPackage);
            // console.log('RequestTotalDiscount',RequestTotalDiscount);




            // console.log('tempMaxdiscount', tempMaxdiscount);
            // console.log('$scope.selected_data.Discount', $scope.selected_data.Discount,$scope.selected_data.QtyUnit);
            // console.log('$scope.totalSumDisc', $scope.totalSumDisc);

            var tempAjudixcount = { AjuDiscountSummary: tempMaxdiscount };
            _.extend($scope.selected_data, tempAjudixcount);

            var param = {
                "discount": $scope.totalSumDisc,
                "vehicle": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                "year": $scope.selected_data.ListInfoUnit[0].ProductionYear,
            }

            CreateSpkFactory.getDiscount(param).then(function (res) {
                $scope.listDiscount = res.data;
                if ($scope.listDiscount.DiscountValidBit == false) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Discount melebihi batas. Maksimal discount " + $scope.listDiscount.MaxDiscount + ".",
                        type: 'danger'
                    });
                } else {
                    if ($scope.listDiscount.ListApprover.length > 0 && $scope.temphistorydiscount != $scope.totalSumDisc &&
                        ($scope.selected_data.BookingUnitId == null || angular.isUndefined($scope.selected_data.BookingUnitId))) {
                        var numLength = angular.element('.ui.small.modal.diskonapprove').length;
                        setTimeout(function () {
                            angular.element('.ui.small.modal.diskonapprove').modal('refresh');
                        }, 0);

                        if (numLength > 1) {
                            angular.element('.ui.small.modal.diskonapprove').not(':first').remove();
                        }
                        angular.element(".ui.small.modal.diskonapprove").modal("show");
                        $scope.listapprover = $scope.listDiscount.ListApprover;
                    } else {
                        if ($scope.IsSign == true && $scope.statusAction == 'buat') {
                            var numLength = angular.element('.ui.modal.signature').length;
                            setTimeout(function () {
                                angular.element('.ui.modal.signature').modal('refresh');
                            }, 0);

                            if (numLength > 1) {
                                angular.element('.ui.modal.signature').not(':first').remove();
                            }

                            angular.element('.ui.modal.signature').modal('show');
                            //angular.element('.ui.modal.signature').not(':first').remove();
                        } else {
                            $scope.oKDraftSPK();
                        }

                    }
                }
            });
        });
        
        }

        $scope.KlikAjuSPK = false;
        $scope.klikDraft = false;
        $scope.ajuSPK = function () {
            $scope.KlikAjuSPK = false;
            $scope.klikDraft = true;
            $scope.disabledAjuSPK = true;
            $scope.signStatus = "aju";
            $scope.CreateSpkOfftheRoadOperation = "BukanDraft";//kalo counternya Draft

            console.log('$scope.selected_data.OnOffTheRoadId',$scope.selected_data.OnOffTheRoadId)
            console.log('$scope.selected_data.recalculate',$scope.selected_data.recalculate)
            console.log('$scope.selected_data.BookingUnitId',$scope.selected_data.BookingUnitId)

            // CreateSpkFactory.getGlobalMediator().then(function (res) {
            //     $scope.mediatorbit = res.data.Result[0].mediatorbit;
            // });

            if($scope.selected_data.CustomerTypeId == 2 || $scope.selected_data.CustomerTypeId == 4 || $scope.selected_data.CustomerTypeId == 5 || $scope.selected_data.CustomerTypeId == 6){
                $scope.selected_data.ListInfoPelanggan[0].CustomerName = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceName);
                $scope.selected_data.ListInfoPelanggan[0].CustomerAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceAddress);
                $scope.selected_data.ListInfoPelanggan[1].InstanceName = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerName);
                $scope.selected_data.ListInfoPelanggan[2].InstanceName = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerName);
                $scope.selected_data.ListInfoPelanggan[2].InstanceAddress = angular.copy($scope.selected_data.ListInfoPelanggan[2].CustomerAddress);
            }else{
                $scope.selected_data.ListInfoPelanggan[0].InstanceName = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerName);
                $scope.selected_data.ListInfoPelanggan[0].InstanceAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerAddress);
                $scope.selected_data.ListInfoPelanggan[1].InstanceName = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerName);
                $scope.selected_data.ListInfoPelanggan[1].InstanceAddress = angular.copy($scope.selected_data.ListInfoPelanggan[1].CustomerAddress);
            }

            for(var i in $scope.selected_data.ListInfoPelanggan){
                // $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate)
                if($scope.selected_data.ListInfoPelanggan[i].BirthDate != null || $scope.selected_data.ListInfoPelanggan[i].BirthDate != undefined){
                    $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate) 
                }
            }

            if($scope.selected_data.TDP_DP > $scope.selected_data.GrandTotal ){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Nilai TDP/DP Tidak Boleh Lebih Besar Dari Total Harga.",
                    type: 'warning'
                });
                $scope.disabledAjuSPK = false;
                setTimeout(function(){
                    $scope.KlikAjuSPK = false;
                    $scope.klikDraft = false;
                },5000);
            }else {

                if ($scope.selected_data.OnOffTheRoadId != 2) {
                    if ($scope.selected_data.recalculate == true && $scope.selected_data.BookingUnitId == null) {
                        bsNotify.show({
                            title: "Peringatan",
                            content: "Harap kalkulasi ulang.",
                            type: 'warning'
                        });
                        $scope.disabledAjuSPK = false;
                        setTimeout(function(){
                            $scope.KlikAjuSPK = false;
                            $scope.klikDraft = false;
                        },5000);
                    } else {
                        if ($scope.selected_data.BookingFee > $scope.selected_data.TDP_DP) {
                            bsNotify.show({
                                title: "Error Message",
                                content: "TDP Lebih Kecil Dari BookingFee",
                                type: 'danger'
                            });
                            $scope.disabledAjuSPK = false;
                            setTimeout(function(){
                                $scope.KlikAjuSPK = false;
                                $scope.klikDraft = false;
                            },5000);
                        } else {
                            if ($scope.selected_data.BookingUnitId != null &&
                                $scope.selected_data.EndHourStatus == true &&
                                ($scope.selected_data.BookingFeeReceiveBit == null || $scope.selected_data.BookingFeeReceiveBit == false)
                            ) {
                                angular.element('.ui.modal.ModalStopNHour').modal('show');
                                angular.element('.ui.modal.ModalStopNHour').not(':first').remove();
                            } else {
                                if($scope.IsPengganti == true && $scope.selected_data.NoSPKPengganti == ''){
                                    bsNotify.show({
                                        title: "Error Message",
                                        content: "Anda memilih SPK pengganti, silahkan isi Nomor SPK Pengganti",
                                        type: 'danger'
                                    });
                                } else {
                                    $scope.ajuSPKNHour();
                                }
                            }
                            $scope.disabledAjuSPK = false;
                            setTimeout(function(){
                                $scope.KlikAjuSPK = false;
                                $scope.klikDraft = false;
                            },5000);
                        }
                    }
                }
                else if ($scope.selected_data.OnOffTheRoadId == 2) {
                    //nongol popup
                    $scope.selected_data.RequestNote = "";
                    angular.element('.ui.modal.CreateSpkAjuOffTheRoad').modal('show');
                    angular.element('.ui.modal.CreateSpkAjuOffTheRoad').not(':first').remove();
                }
                $scope.disabledAjuSPK = false;
                setTimeout(function(){
                    $scope.KlikAjuSPK = false;
                    $scope.klikDraft = false;
                },5000);

            }
             //2 artinya off the road
           


        }

        $scope.selectedOnOffTheRoad = function (selectedData) {
            if (selectedData == 2){

                bsNotify.show({
                    title: "Info",
                    content: "Anda Telah Memillih Pengiriman Unit Off The Road.",
                    type: 'info'
                });

            }else{

            }
        }

        $scope.CreateSpkCekanDiskon = function (obj) {
            console.log('inputan diskon manual ===>',obj);
            $scope.CreateSpkCekDiskon_GaAman = false;

            if (obj.Discount > obj.Price) {
                $scope.CreateSpkCekDiskon_GaAman = true;
            } 
            
            
            if (obj.Discount == obj.Price) {
                obj.IsDiscount = true;
                $scope.freeAccSatuan(obj);
            }else{
                obj.IsDiscount = false;
            }
            
            // galang
            
        }

        $scope.cekValidDiskon = function (data, dataPE, status) {
            console.log('$scope.CreateSpkCekDiskon_GaAman', $scope.CreateSpkCekDiskon_GaAman);
            console.log('status', status);
            // if ($scope.CreateSpkCekDiskon_GaAman==true) {
            //     $scope.itemAksesorisDetailPackege = {};
            //     $scope.itemAksesorisDetailPackege.Discount = 0;
            //     $scope.itemAksesorisDetail = {};
            //     $scope.itemAksesorisDetail.Discount = 0;
            // }

            if ($scope.CreateSpkCekDiskon_GaAman == true) {
                // data = 0;
                dataPE.Discount = 0;



                // if (status == true) {

                //     console.log('data', data);

                //     var param = {
                //         "Title": "AccessoriesPackage",
                //         "HeaderInputs": {
                //             "$AccPackageId$": dataPE.AccessoriesPackageId,
                //             "$QTY$": dataPE.Qty,
                //             "$Discount$": dataPE.Discount
                //         }
                //     };

                //     $http.post($scope.urlAccessoriesPackageSpk, param)
                //         .then(function (res) {
                //             dataPE.AccessoriesPackageId = dataPE.AccessoriesPackageId;
                //             dataPE.AccessoriesPackageName = dataPE.AccessoriesPackageName;
                //             dataPE.Price = dataPE.Price;
                //             dataPE.Qty = dataPE.Qty;
                //             dataPE.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                //             dataPE.PPN = parseFloat(res.data.Codes["[PPN]"]);
                //             dataPE.DPP = parseFloat(res.data.Codes["[DPP]"]);
                //             dataPE.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                //             dataPE.ListDetail = dataPE.ListDetail;
                //         });

                // } else if (status == false) {


                //     var param = {
                //         "Title": "Accessories",
                //         "HeaderInputs": {
                //             "$AccessoriesId$": dataPE.AccessoriesId,
                //             "$QTY$": dataPE.Qty,
                //             "$Discount$": data
                //         }
                //     };

                //     $http.post($scope.urlAccessoriesSpk, param)
                //         .then(function (res) {
                //             dataPE.AccessoriesId = dataPE.AccessoriesId;
                //             dataPE.AccessoriesName = dataPE.AccessoriesName;
                //             dataPE.Price = dataPE.Price;
                //             dataPE.Qty = dataPE.Qty;
                //             dataPE.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                //             dataPE.PPN = parseFloat(res.data.Codes["[PPN]"]);
                //             dataPE.DPP = parseFloat(res.data.Codes["[DPP]"]);
                //             dataPE.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                //         })


                // }
            }
            else {
                // data = data;


                if (status == true) {

                    console.log('data', data);
                    //galang
                    var param = {
                        "Title": "AccessoriesPackage",
                        "HeaderInputs": {
                            "$AccPackageId$": dataPE.AccessoriesPackageId,
                            // "$AccCode$": dataPE.AccessoriesCode,
                            "$QTY$": dataPE.Qty,
                            "$Discount$": dataPE.Discount,
                            "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                            "$OutletId$": $scope.user.OutletId,
                        }
                    };

                    $http.post($scope.urlAccessoriesPackageSpk, param)
                        .then(function (res) {
                            dataPE.AccessoriesPackageId = dataPE.AccessoriesPackageId;
                            dataPE.AccessoriesPackageName = dataPE.AccessoriesPackageName;
                            dataPE.Price = dataPE.Price;
                            dataPE.Qty = dataPE.Qty;
                            dataPE.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                            dataPE.PPN = parseFloat(res.data.Codes["[PPN]"]);
                            dataPE.DPP = parseFloat(res.data.Codes["[DPP]"]);
                            dataPE.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                            dataPE.ListDetail = dataPE.ListDetail;
                        });

                } else if (status == false) {


                    var param = {
                        "Title": "Accessories",
                        "HeaderInputs": {
                            // "$AccessoriesId$": dataPE.AccessoriesId,
                            "$AccCode$": dataPE.AccessoriesCode,
                            "$QTY$": dataPE.Qty,
                            "$Discount$": data,
                            "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                            "$OutletId$": $scope.user.OutletId,
                            "$AccessoriesCode$": dataPE.AccessoriesCode,

                        }
                    };

                    $http.post($scope.urlAccessoriesSpk, param)
                        .then(function (res) {
                            dataPE.AccessoriesId = dataPE.AccessoriesId;
                            dataPE.AccessoriesName = dataPE.AccessoriesName;
                            dataPE.Price = dataPE.Price;
                            dataPE.Qty = dataPE.Qty;
                            dataPE.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                            dataPE.PPN = parseFloat(res.data.Codes["[PPN]"]);
                            dataPE.DPP = parseFloat(res.data.Codes["[DPP]"]);
                            dataPE.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                        })


                }

                return data;
            }

        }

        $scope.CreateSpkAjuOffTheRoadKirim = function () {

            if ($scope.CreateSpkOfftheRoadOperation == "BukanDraft") {
                console.log('ini masuk Bukan draft');
                if ($scope.selected_data.recalculate == true && $scope.selected_data.BookingUnitId == null) {
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Harap kalkulasi ulang.",
                        type: 'warning'
                    });
                } else {
                    if ($scope.selected_data.BookingFee > $scope.selected_data.TDP_DP) {
                        bsNotify.show({
                            title: "Error Message",
                            content: "TDP Lebih Kecil Dari BookingFee",
                            type: 'danger'
                        });
                    } else {
                        if ($scope.selected_data.BookingUnitId != null &&
                            $scope.selected_data.EndHourStatus == true &&
                            ($scope.selected_data.BookingFeeReceiveBit == null || $scope.selected_data.BookingFeeReceiveBit == false)
                        ) {
                            angular.element('.ui.modal.ModalStopNHour').modal('show');
                            angular.element('.ui.modal.ModalStopNHour').not(':first').remove();
                        } else {
                            $scope.ajuSPKNHour();
                        }
                    }
                }
                angular.element('.ui.modal.CreateSpkAjuOffTheRoad').modal('hide');
            }
            else if ($scope.CreateSpkOfftheRoadOperation == "Draft") {
                console.log('ini masuk  draft');
                if ($scope.selected_data.BookingFee > $scope.selected_data.TDP_DP) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "TDP Lebih Kecil Dari BookingFee",
                        type: 'danger'
                    });
                } else {
                    if (($scope.selected_data.BookingUnitId != null) &&
                        $scope.selected_data.EndHourStatus == true &&
                        ($scope.selected_data.BookingFeeReceiveBit == null || $scope.selected_data.BookingFeeReceiveBit == false)
                    ) {
                        angular.element('.ui.modal.ModalStopNHour').modal('show');
                        angular.element('.ui.modal.ModalStopNHour').not(':first').remove();
                    } else {
                        $scope.saveDraftNHour();
                    }
                }
            } else {
                console.log('ini gamasuk kedua nya');
            }



        }

        $scope.CreateSpkAjuOffTheRoadBatal = function () {
            console.log('ini harusnya close modal CreateSpkAjuOffTheRoad');
            angular.element('.ui.modal.CreateSpkAjuOffTheRoad').modal('hide');
            angular.element('.ui.modal.CreateSpkAjuOffTheRoad').not(':first').remove();
        }

        // function Pelwil(data) {
        //     console.log(data);
        // }

        function maxDiscAcc(dataSPK) {
            var maxDiscAccValue = 0;
            var temDisc = 0;
            for (var i in dataSPK.ListInfoUnit) {
                for (var j in dataSPK.ListInfoUnit[i].ListDetailUnit) {
                    for (var k in dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage) {
                        // tadi dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].IsDiscount == true
                        temDisc = 0;
                        if (dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Price > 0) {
                            temdisc = dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Discount * dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Qty;
                            // maxDiscAccValue += dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Discount;
                            //maxDiscAccValue += dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Price + dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Discount;
                        }
                        maxDiscAccValue += temDisc;
                    }
                    for (var l in dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories) {
                        temDisc = 0;
                        // console.log('list acc',dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories);
                        // tadi dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].IsDiscount == true
                        if (dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Price > 0) {
                            temDisc = dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Discount * dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Qty;
                            // console.log('temDisc',temDisc);

                            //maxDiscAccValue += dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Price + dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Discount;
                        }
                        maxDiscAccValue += temDisc;

                    }
                }
            }
            console.log("First checking Max Adjust discount ===>", maxDiscAccValue);
            return maxDiscAccValue;
        }

        $scope.totalSumDisc = 0;
        $scope.ajuSPKNHour = function () {
            CreateSpkFactory.getGlobalMediator().then(function (res) {
                $scope.mediatorbit = res.data.Result[0].mediatorbit;
                console.log('Cek Mediator', $scope.mediatorbit)

            var tempMaxdiscount = maxDiscAcc($scope.selected_data);
            console.log('$scope.selected_data', $scope.selected_data);


            var tempMaxdiscount = maxDiscAcc($scope.selected_data);
            var tempQtyUnit = 1;

            if (typeof $scope.selected_data.QtyUnit == 'undefined' || $scope.selected_data.QtyUnit == null || $scope.selected_data.QtyUnit == '') {
                tempQtyUnit = $scope.selected_data.QtyTotalUnit;
            } else {
                tempQtyUnit = $scope.selected_data.QtyUnit;
            }

            if (typeof $scope.selected_data.QtyTotalUnit == 'undefined' || $scope.selected_data.QtyTotalUnit == null || $scope.selected_data.QtyTotalUnit == '') {
                tempQtyUnit = $scope.selected_data.QtyUnit;
            } else {
                tempQtyUnit = $scope.selected_data.QtyTotalUnit;
            }

            console.log('ini selected data ajuSPKNHour ===>', $scope.selected_data);

            var accSatuan = $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories;
            var accPackage = $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage;
            var totalDiscAcc = 0;
            var totalDiscAccPackage = 0;

            if (accSatuan != null || accSatuan != [] || accSatuan != undefined) {
                for (var i = 0; i < accSatuan.length; i++) {

                    totalDiscAcc += (accSatuan[i].Discount * accSatuan[i].Qty);
                }
                console.log('totalDiscAcc ===>', totalDiscAcc);
            }

            if (accPackage != null || accPackage != [] || accPackage != undefined) {
                for (var i = 0; i < accPackage.length; i++) {

                    totalDiscAccPackage += (accPackage[i].Discount * accPackage[i].Qty);
                }
                console.log('totalDiscAccPackage ===>', totalDiscAccPackage);
            }

            $scope.RequestMaxDiscountAcc = totalDiscAcc + totalDiscAccPackage;
            $scope.selected_data.RequestMaxDiscountAcc = $scope.RequestMaxDiscountAcc;
           
            if($scope.mediatorbit == 1){
                $scope.totalSumDisc = ($scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty) + tempMaxdiscount + $scope.selected_data.DiscountSubsidiRate + $scope.selected_data.DiscountSubsidiDP + $scope.selected_data.MediatorCommision + totalDiscAccPackage;
            } else {
                $scope.totalSumDisc = ($scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty) + tempMaxdiscount + $scope.selected_data.DiscountSubsidiRate + $scope.selected_data.DiscountSubsidiDP + totalDiscAccPackage;
            }
            $scope.selected_data.RequestTotalDiscount = angular.copy($scope.totalSumDisc);
            $scope.tempDiscountUnit = $scope.selected_data.Discount * $scope.selected_data.ListInfoUnit[0].Qty;
            $scope.tempDiscAcc = tempMaxdiscount + totalDiscAccPackage;
            $scope.tempDiscSubRate = $scope.selected_data.DiscountSubsidiRate;
            $scope.tempDiscSubDP = $scope.selected_data.DiscountSubsidiDP;
            $scope.tempKomisiMediator = $scope.selected_data.MediatorCommision;
            $scope.selected_data_ajuSPKNHour = angular.copy($scope.selected_data);
            console.log(' ini hasil setelah kalkulasi ajuSPKNHour ===>', $scope.selected_data_ajuSPKNHour);


            //var totalDisc = tempMaxdiscount + 
            //console.log("maxdiscount", tempMaxdiscount);
            // console.log('tempMaxdiscount', tempMaxdiscount);
            // console.log('$scope.selected_data.Discount', $scope.selected_data.Discount,$scope.selected_data.QtyUnit);
            // console.log('$scope.totalSumDisc', $scope.totalSumDisc);

            var tempAjudixcount = { AjuDiscountSummary: tempMaxdiscount };
            _.extend($scope.selected_data, tempAjudixcount);

            var param = {
                "discount": $scope.totalSumDisc,
                "vehicle": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                "year": $scope.selected_data.ListInfoUnit[0].ProductionYear,
            }

            CreateSpkFactory.getDiscount(param).then(function (res) {
                $scope.listDiscount = res.data;
                if ($scope.listDiscount.DiscountValidBit == false) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Discount melebihi batas. Maksimal discount " + $scope.listDiscount.MaxDiscount + ".",
                        type: 'danger'
                    });
                    $scope.disabledAjuSPK = false;
                } else {
                    if ($scope.listDiscount.ListApprover.length > 0 && $scope.temphistorydiscount != $scope.totalSumDisc &&
                        ($scope.selected_data.BookingUnitId == null || angular.isUndefined($scope.selected_data.BookingUnitId))) {
                        var numLength = angular.element('.ui.small.modal.diskonapprove').length;
                        setTimeout(function () {
                            angular.element('.ui.small.modal.diskonapprove').modal('refresh');
                        }, 0);
                        if (numLength > 1) {
                            angular.element('.ui.small.modal.diskonapprove').not(':first').remove();
                        }
                        angular.element(".ui.small.modal.diskonapprove").modal("show");
                        $scope.listapprover = $scope.listDiscount.ListApprover;
                        $scope.disabledAjuSPK = false;
                    } else {
                        if ($scope.IsSign == true && $scope.statusAction == 'buat') {
                            angular.element('.ui.modal.signature').modal('setting', '', '').modal('show');

                        } else {
                            $scope.selected_data = angular.copy($scope.selected_data_ajuSPKNHour)
                            $scope.oKAjuSPK();
                        }
                        $scope.disabledAjuSPK = false;

                    }
                }
            });
        });
            
            
        }

        $scope.Nhour = function (status) {
            if (status == 'Belum') {
                $scope.selected_data.BookingFeeReceiveBit = false;
            } else {
                $scope.selected_data.BookingFeeReceiveBit = true;
            }

            if ($scope.signStatus == 'aju') {
                $scope.ajuSPKNHour();
                angular.element('.ui.modal.ModalStopNHour').modal('hide');
            } else {
                $scope.saveDraftNHour();
                angular.element('.ui.modal.ModalStopNHour').modal('hide');
            }
        }

 

        $scope.simpanDariSign = function () {
            $rootScope.$emit("BuatSpkSign", {});

            $scope.selected_data.UploadDataDigitalSign = {
                "UpDocObj": $scope.signature.dataUrl,
                "FileName": $scope.CreateGuid()
            };

            $scope.TempSignature = angular.copy($scope.selected_data.UploadDataDigitalSign);

            if ($scope.signStatus == "draft") {
                $scope.oKDraftSPK();
                angular.element('.ui.modal.signature').modal('hide');
            } else {
                $scope.oKAjuSPK();
                angular.element('.ui.modal.signature').modal('hide');
            }
        }

        $scope.keluarModalSign = function () {
            angular.element(".ui.modal.signature").modal("hide");
        }

        $scope.bataldiscount = function () {
            angular.element(".ui.small.modal.diskonapprove").modal("hide");
        }

        $scope.keluardicount = function () {
            console.log("ini catetan diskonnya ===>", $scope.selected_data.NoteDiscount);
            $scope.NoteDiscountUpdate = angular.copy($scope.selected_data.NoteDiscount);

            if ($scope.IsSign == true) {
                //console.log("keluardiscountTTD");
                angular.element('.ui.modal.signature').modal('show');

            } else {
                if ($scope.signStatus == "aju") {
                    console.log('ini aju spk ===>', $scope.signStatus);

                    $scope.oKAjuSPK();
                    angular.element(".ui.small.modal.diskonapprove").modal("hide");
                } else if ($scope.signStatus == "draft") {
                    console.log('ini draft spk ===>', $scope.signStatus);

                    $scope.oKDraftSPK();
                    angular.element(".ui.small.modal.diskonapprove").modal("hide");
                }

            }
        }

        $scope.oKDraftSPK = function () {
            $scope.selected_data.VehicleTypeId = $scope.selected_data.ListInfoUnit[0].VehicleTypeId;
            $scope.selected_data.ProductionYear = $scope.selected_data.ListInfoUnit[0].ProductionYear;

            $scope.statusKembali = 'save';
            angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
                if (angular.isUndefined(Doc.UploadDataDocument) == true) {
                    Doc.UploadDataDocument = {
                        Branch: null,
                        DocumentTypeId: null,
                        FileName: null,
                        TableRefId: null,
                        UpDocObj: null
                    };
                }
            });

            //Pelwil($scope.selected_data);

            angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
                //console.log("Draft Doc.UploadDataDocument.UpDocObj", Doc.UploadDataDocument.UpDocObj);
                if ((Doc.UploadDataDocument.UpDocObj != null && Doc.UploadDataDocument.UpDocObj != "" && Doc.UploadDataDocument.UpDocObj != "bnVsbA==")) {
                    Doc.UploadDataDocument.UpDocObj = btoa(Doc.UploadDataDocument.UpDocObj);
                }
                //tambahan, aslinya ga ada
                else if (angular.isUndefined(Doc.UploadDataDocument.UpDocObj) == true || Doc.UploadDataDocument.UpDocObj == undefined || Doc.UploadDataDocument.UpDocObj == "") {
                    Doc.UploadDataDocument.UpDocObj = null;

                }
            });

            //console.log("error ga 2");
            //console.log("error ga 21" ,$scope.selected_data.UploadDataDigitalSign);
            //console.log("error ga 22", $scope.selected_data.UploadDataDigitalSign.UpDocObj);


            if ($scope.IsSign == true) {
                if (angular.isDefined($scope.selected_data.UploadDataDigitalSign) == true) {
                    $scope.selected_data.UploadDataDigitalSign.UpDocObj = btoa($scope.selected_data.UploadDataDigitalSign.UpDocObj);
                }
            } else {
                $scope.selected_data.UploadDataDigitalSign = { UpDocObj: null, FileName: null };
            }


            //console.log("error ga 3");

            if ($scope.statusAction == 'buat') {
                //$rootScope.$emit("pricingCalculateSPK", {});
                CreateSpkFactory.create($scope.selected_data).then(function (res) {
                    $scope.statusSimpan = true;
                    $rootScope.$emit("RefreshDirective", {});
                    CreateSpkFactory.stopNhour($scope.selected_data).then(function (res) {

                    })
                    $scope.back();
                    bsNotify.show({
                        title: "Success Message",
                        content: "Input SPK Sebagai Draft Berhasil",
                        type: 'succes'
                    });
                },
                    function (err) {
                        bsNotify.show({
                            title: "Error Message",
                            content: "Input SPK Sebagai Draft Gagal",
                            type: 'danger'
                        });
                    }
                );
            } else if ($scope.statusAction == 'ubah') {
                //$rootScope.$emit("pricingCalculateSPK", {});
                CreateSpkFactory.update($scope.selected_data).then(function (res) {
                    $scope.statusSimpan = true;
                    $rootScope.$emit("RefreshDirective", {});
                    CreateSpkFactory.stopNhour($scope.selected_data).then(function (res) {

                    })
                    $scope.back();
                    bsNotify.show({
                        title: "Success Message",
                        content: "Ubah SPK Draft Berhasil",
                        type: 'succes'
                    });

                    console.log('Ubah SPK Draft Berhasil');
                    $scope.CreateSpkAjuOffTheRoadBatal();

                },
                    function (err) {
                        bsNotify.show({
                            title: "Error Message",
                            content: "Update SPK Draft Gagal",
                            type: 'danger'
                        });
                    }
                );
            }

            // CreateSpkFactory.cekPwlwil($scope.selected_data).then(function (res) {
            //     $scope.pelwilobjc = { PelanggaranWilayah: null }
            //     console.log(res.data.Result[0]);
            //     if (res.data.Result[0].PelanggaranWilayahBit == true) {
            //         _.extend($scope.selected_data, $scope.pelwilobjc);
            //         $scope.selected_data.PelanggaranWilayah = true;
            //         console.log("true");
            //     } else {
            //         _.extend($scope.selected_data, $scope.pelwilobjc);
            //         $scope.selected_data.PelanggaranWilayah = false;
            //         console.log("false");
            //     }


            // })


        }

        $scope.test = function () {
            console.log("sekarang", $scope.selected_data);
            console.log("formvalidationNew", $scope.formvalidation);
            console.log("formvalidInfoIndividuNew", $scope.formvalidInfoIndividu);
            console.log("formvalidPengirimanNew", $scope.formvalidPengiriman);
            console.log("formRincianhargaNew", $scope.formRincianharga);
        }

        $scope.addDisc = {};
        $scope.oKAjuSPK = function () {
            var isPelwil = null;
            $scope.selected_data.VehicleTypeId = $scope.selected_data.ListInfoUnit[0].VehicleTypeId;
            $scope.selected_data.ProductionYear = $scope.selected_data.ListInfoUnit[0].ProductionYear;

            $scope.statusKembali = 'save';
            angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
                if (angular.isUndefined(Doc.UploadDataDocument) == true) {
                    Doc.UploadDataDocument = {
                        Branch: null,
                        DocumentTypeId: null,
                        FileName: null,
                        TableRefId: null,
                        UpDocObj: null
                    };
                }
            });

            //galang pindahin kebawah 
            // angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
            //     //console.log("Aju Doc.UploadDataDocument.UpDocObj", Doc.UploadDataDocument.UpDocObj);
            //     //if(angular.isDefined(Doc.UploadDataDocument.UpDocObj) == true){
            //     if ((Doc.UploadDataDocument.UpDocObj != null
            //         && Doc.UploadDataDocument.UpDocObj != ""
            //         && Doc.UploadDataDocument.UpDocObj != "bnVsbA==")) {
            //         Doc.UploadDataDocument.UpDocObj = btoa(Doc.UploadDataDocument.UpDocObj);
                    
            //     } // }else if(angular.isUndefined(Doc.UploadDataDocument.UpDocObj) == true){
            //     //     Doc.UploadDataDocument.UpDocObj = null;
            //     // }
            //     //}
            // });


            //if(angular.isDefined($scope.selected_data.UploadDataDigitalSign.UpDocObj) == true){

            //galang pindahin kebawah
            // if ($scope.IsSign == true && $scope.statusAction == 'buat') {
            //     console.log('betoa masuk sini ga sih?')
            //     $scope.selected_data.UploadDataDigitalSign.UpDocObj = btoa($scope.selected_data.UploadDataDigitalSign.UpDocObj);
            // } else {
            //     $scope.selected_data.UploadDataDigitalSign = { UpDocObj: null, FileName: null };
            // }

            
            // }


            ////////////Re Calculate////////////////

            // if (angular.isUndefined($scope.selected_data.KaroseriPrice) == true || $scope.selected_data.KaroseriPrice == null) {
            //     $scope.selected_data.KaroseriPrice = 0.0;
            // }

            // for (var i in $scope.selected_data.ListInfoUnit) {
            //     $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = 0;
            //     for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {
            //         $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc = 0;
            //         $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll = 0;
            //     }
            // }

            // //scope.grandtotalarray = [];
            // for (var i in $scope.selected_data.ListInfoUnit) {
            //     $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = 0;
            //     for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {
            //         $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData = {
            //             Title: null,
            //             HeaderInputs: {
            //                 $VehicleTypeColorId$: $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].VehicleTypeColorId,
            //                 $BBNAdj$: $scope.selected_data.BBNAdjustment, 
            //                 $BBNSAdj$: $scope.selected_data.BBNServiceAdjustment, 
            //                 $Diskon$: $scope.selected_data.Discount,
            //                 $DSubsidiDP$: $scope.selected_data.DiscountSubsidiDP,
            //                 $DSubsidiRate$: $scope.selected_data.DiscountSubsidiRate,
            //                 $MCommision$: $scope.selected_data.MediatorCommision,
            //                 $TotalACC$: 0.0,
            //                 $OutletId$: $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].OutletId,
            //                 $VehicleYear$: $scope.selected_data.ListInfoUnit[i].ProductionYear
            //             }
            //         }

            //         for (var k in $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage) {
            //             $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
            //                 $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;
            //             $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc =
            //                 $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc + $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;
            //         }

            //         for (var k in $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessories) {
            //             $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
            //                 $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
            //             $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc =
            //                 $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc + $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
            //         }

            //         $scope.selectedy_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
            //             $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + $scope.selected_data.KaroseriTotalInclVAT;

            //         $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc = $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc + $scope.selected_data.KaroseriTotalInclVAT;

            //         $http.post('/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1', $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j].jsData)
            //             .then(
            //                 function(res) {  

            //                     $scope.selected_data.DPP = parseFloat(res.data.Codes["[DPP]"]);  
            //                     $scope.selected_data.PPN = parseFloat(res.data.Codes["[PPN]"]);  
            //                     $scope.selected_data.BBN = parseFloat(res.data.Codes["[BBN]"])
            //                     $scope.selected_data.TotalBBN = parseFloat(res.data.Codes["[TotalBBN]"]);
            //                     $scope.selected_data.PPH22 = parseFloat(res.data.Codes["[PPH22]"]);
            //                     $scope.selected_data.TotalInclVAT = parseFloat(res.data.Codes["[TotalHarga]"]);
            //                     $scope.selected_data.VehiclePrice = parseFloat(res.data.Codes["[HargaMobil]"]);
            //                     $scope.selected_data.PriceAfterDiscount = parseFloat(res.data.Codes["[PriceAfterDiskon]"]);



            //                     //scope.trigerGrandTotal();


            //                 });
            //     }



            // }

            $scope.addDisc = {
                RequestMaxDiscountAcc: maxDiscAcc($scope.selected_data),
                RequestTotalDiscount: $scope.selected_data.Discount +
                    $scope.selected_data.DiscountSubsidiDP +
                    $scope.selected_data.DiscountSubsidiRate +
                    maxDiscAcc($scope.selected_data)
            };

            _.extend($scope.selected_data, $scope.addDisc);

            // Pelwil($scope.selected_data);

            //console.log("error ga");
            CreateSpkFactory.cekPwlwil($scope.selected_data).then(function (res) {
                $scope.pelwilobjc = { PelanggaranWilayah: null }
                console.log(res.data.Result[0]);
                if (res.data.Result[0].PelanggaranWilayahBit == true) {
                    _.extend($scope.selected_data, $scope.pelwilobjc);
                    $scope.selected_data.PelanggaranWilayah = true;
                    //console.log("true");
                } else {
                    _.extend($scope.selected_data, $scope.pelwilobjc);
                    $scope.selected_data.PelanggaranWilayah = false;
                    //console.log("false");
                }

                $scope.selected_data = angular.copy($scope.selected_data_ajuSPKNHour);

                if ($scope.NoteDiscountUpdate != null || $scope.NoteDiscountUpdate != undefined || $scope.NoteDiscountUpdate != '') {
                    $scope.selected_data.NoteDiscount = $scope.NoteDiscountUpdate;
                }


                console.log('ini finall Ubah SPK & Buat SPK ===>', $scope.selected_data);



                if ($scope.statusAction == 'buat') {

                    angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
                        //console.log("Aju Doc.UploadDataDocument.UpDocObj", Doc.UploadDataDocument.UpDocObj);
                        //if(angular.isDefined(Doc.UploadDataDocument.UpDocObj) == true){
                        if ((Doc.UploadDataDocument.UpDocObj != null
                            && Doc.UploadDataDocument.UpDocObj != ""
                            && Doc.UploadDataDocument.UpDocObj != "bnVsbA==")) {
                            Doc.UploadDataDocument.UpDocObj = btoa(Doc.UploadDataDocument.UpDocObj);

                        }else if (angular.isUndefined(Doc.UploadDataDocument.UpDocObj) == true || Doc.UploadDataDocument.UpDocObj == undefined || Doc.UploadDataDocument.UpDocObj == "") {
                            Doc.UploadDataDocument.UpDocObj = null;
                        }

                    });

                    if ($scope.IsSign == true && $scope.statusAction == 'buat') {
                        $scope.selected_data.UploadDataDigitalSign = $scope.TempSignature;
                        $scope.selected_data.UploadDataDigitalSign.UpDocObj = btoa($scope.TempSignature.UpDocObj);
                    } else {
                        $scope.selected_data.UploadDataDigitalSign = { UpDocObj: null, FileName: null };
                    }

                    console.log('ini finall Create Aju BUAT ===>', $scope.selected_data);



                    CreateSpkFactory.createAju($scope.selected_data).then(function (res) {
                        $scope.statusSimpan = true;
                        $rootScope.$emit("RefreshDirective", {});
                        CreateSpkFactory.stopNhour($scope.selected_data).then(function (res) {

                        })
                        $scope.back();

                        bsNotify.show({
                            title: "Berhasil",
                            content: "Aju SPK Berhasil",
                            type: 'succes'
                        });

                        if ($scope.selected_data.PelanggaranWilayah == true) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "SPK ini pelanggaran wilayah",
                                type: 'warning'
                            });
                        };
                    },
                        function (err) {
                            bsNotify.show({
                                title: "Error Message",
                                content: "Aju SPK Gagal",
                                type: 'danger'
                            });
                        })
                } else if ($scope.statusAction == 'ubah') {

                    angular.forEach($scope.selected_data.ListInfoDocument, function (Doc, DocIndx) {
                        //console.log("Aju Doc.UploadDataDocument.UpDocObj", Doc.UploadDataDocument.UpDocObj);
                        //if(angular.isDefined(Doc.UploadDataDocument.UpDocObj) == true){
                        if ((Doc.UploadDataDocument.UpDocObj != null
                            && Doc.UploadDataDocument.UpDocObj != ""
                            && Doc.UploadDataDocument.UpDocObj != "bnVsbA==")) {
                            Doc.UploadDataDocument.UpDocObj = btoa(Doc.UploadDataDocument.UpDocObj);

                        } else if (angular.isUndefined(Doc.UploadDataDocument.UpDocObj) == true || Doc.UploadDataDocument.UpDocObj == undefined || Doc.UploadDataDocument.UpDocObj == "") {
                            Doc.UploadDataDocument.UpDocObj = null;
                        }

                    });

                    if ($scope.IsSign == true && $scope.statusAction == 'buat') {
                        $scope.selected_data.UploadDataDigitalSign = $scope.TempSignature;
                        $scope.selected_data.UploadDataDigitalSign.UpDocObj = btoa($scope.TempSignature.UpDocObj);
                    } else {
                        $scope.selected_data.UploadDataDigitalSign = { UpDocObj: null, FileName: null };
                    }

                    console.log('ini finall Create Aju UBAH ===>', $scope.selected_data);

                    CreateSpkFactory.ubahAju($scope.selected_data).then(function (res) {
                        $scope.statusSimpan = true;
                        $rootScope.$emit("RefreshDirective", {});
                        CreateSpkFactory.stopNhour($scope.selected_data).then(function (res) {

                        })
                        $scope.back();
                        bsNotify.show({
                            title: "Success Message",
                            content: "Ubah Aju SPK Berhasil",
                            type: 'succes'
                        });

                        console.log('Ubah Aju SPK Berhasil');
                        $scope.CreateSpkAjuOffTheRoadBatal();

                        if ($scope.selected_data.PelanggaranWilayah == true) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "SPK ini pelanggaran wilayah",
                                type: 'warning'
                            });
                        };
                    },
                        function (err) {
                            var errMsg = err.data.Message.split('#')
                            bsNotify.show({
                                title: "Error Message",
                                // content: "Update SPK Gagal",
                                content: errMsg[1],
                                type: 'danger'
                            });
                        })
                }
            });
        }

        $scope.CreateGuid = function () {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        }

        $scope.selectedNoSpk = function (spk) {
            if (spk != undefined || spk != null) {
                for (var i in $scope.nospkisi) {
                    if ($scope.nospkisi[i].SpkId == spk) {
                        $scope.selected_data.FormSPKNo = $scope.nospkisi[i].FormSPKNo;
                    }
                }
            }

        }

        $scope.kembaliformspk = function () {
            $scope.signspk = false;
            $scope.formspk = true;
        }

        $scope.closeModal = function () {
            angular.element(".ui.modal.ModalApprovalDicountDraft").modal("hide");
            $scope.clear();
        }
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.Category = function () {
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.ekspansiAll = false;

        }

        $scope.individuPembelibtn = function () {
            $scope.individuPemilik = false;
            switch ($scope.individuPembeli) {
                case false:
                    $scope.individuPembeli = true;
                    $scope.ekspansiAll = false;
                    break;
                case true:
                    $scope.individuPembeli = false;
                    break;
            }
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
        }

        $scope.individuPemilikbtn = function () {
            $scope.individuPembeli = false;
            $scope.ekspansiAll = false;
            if ($scope.individuPemilik == false) {
                $scope.individuPemilik = true;
            } else if ($scope.individuPemilik == true) {
                $scope.individuPemilik = false;
            }
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
        }

        $scope.HidePenanggungJawabPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = !$scope.formDataEkspansiPenanggungJawabPerusahaan;
        }

        $scope.btnPembeliPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPembeliPerusahaan = !$scope.formDataPembeliPerusahaan;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;

        }

        $scope.btnPenangungJawabPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = !$scope.formDataPenanggungJawabPerusahaan;
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
        }

        $scope.btnPemilikPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = !$scope.formDataPemilikPerusahaan;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPembeliPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
        }

        $scope.temphistorydiscount = null;
        $scope.updateSelectedItem = function (selected_data) {
            angular.element('.ui.modal.DraftSPK').modal('hide');
            $scope.statusAction = 'ubah';
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            // $scope.selected_data = selected_data;

            $scope.selectedNPWPBit = false;

            CreateSpkFactory.getDataDetail(selected_data.SpkId).then(function (res) {
                var tempselected_data = angular.copy(res.data.Result[0]);

                // setTimeout(function(){
                //     $scope.selected_data.GrandTotal = res.data.Result[0].GrandTotal;
                //     $scope.selected_data.PemakaiPembeliBit = res.data.Result[0].PemakaiPembeliBit;
                //     $scope.selected_data.PenerimaSTNKBit = res.data.Result[0].PenerimaSTNKBit;
                //     $scope.selected_data.PemakaiPemilikBit = res.data.Result[0].PemakaiPemilikBit;
                // },0);

                console.log('$scope.selected_data',tempselected_data);
                if(tempselected_data.FundSourceName == 'Cash'){
                $scope.statusLeasing = 'mati';
                $scope.bankstastus = false;
                $scope.selected_data.LeasingId = null;
                $scope.selected_data.LeasingLeasingTenorId = null;
                }else{
                $scope.statusLeasing = 'hidup';
                $scope.bankstastus = true;
             
                }





                var tempdoc = angular.copy(tempselected_data.ListInfoDocument);
                for (var i in tempdoc) {
                    tempdoc[i].UploadDataDocument = { UpDocObj: null, FileName: null };
                    tempdoc[i].UploadDataDocument.UpDocObj = tempdoc[i].DocumentData;
                    tempdoc[i].UploadDataDocument.FileName = tempdoc[i].DocumentDataName;
                };
                tempselected_data.ListInfoDocument = tempdoc;

                CreateSpkFactory.getSignConfig().then(function (res) {
                    $scope.IsSign = res.data;
                });

                $scope.selected_data = angular.copy(tempselected_data);
                CreateSpkFactory.bookingFeeDp($scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                    $scope.tempTDP = angular.copy(res.data.NominalDP);
                    $scope.tempBookingFee = angular.copy(res.data.NominalBookingFee);
                });


                console.log('selected_data.ToyotaId', $scope.selected_data);
                if ($scope.selected_data.ToyotaId == null || $scope.selected_data.ToyotaId == '' || typeof $scope.selected_data.ToyotaId == 'undefined') {
                    $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                }
                else {
                    $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20006&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                }

                // $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                // $scope.urlAccessoriesSpk = '/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                // $scope.urlAccessoriesPackageSpk = '/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1';

                ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + $scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                });

                var tempMaxdiscount = maxDiscAcc($scope.selected_data);
                $scope.temphistorydiscount = angular.copy($scope.selected_data.Discount +
                    tempMaxdiscount +
                    $scope.selected_data.DiscountSubsidiRate +
                    $scope.selected_data.DiscountSubsidiDP);

                var tempAjudixcount = { AjuDiscountSummary: tempMaxdiscount };
                _.extend($scope.selected_data, tempAjudixcount);

                $scope.filterTenor($scope.selected_data.LeasingId);

                $scope.formspk = true;
                $scope.listspk = false;


                if ($scope.selected_data.PenerimaSTNKBit == true) {
                    console.log('ini PenerimaSTNKBitnya ==> Harusnya true ===>', $scope.selected_data.PenerimaSTNKBit);
                    $scope.equalInfoPengiriman();
                } else {
                    console.log('ini PenerimaSTNKBitnya ==> Harusnya false ===>', $scope.selected_data.PenerimaSTNKBit);
                }
                $scope.getNPWP($scope.selected_data.ListInfoPelanggan[0].NPWPNo)





            })

            

        }


        $scope.viewSelectedItem = function (data) {

            angular.element('.ui.modal.DraftSPK').modal('hide');
            $scope.statusAction = "lihat";
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;

            CreateSpkFactory.getDataDetail(data.SpkId).then(function (res) {
                $scope.selected_data = res.data.Result[0];

                $scope.tempdoc = angular.copy($scope.selected_data.ListInfoDocument);
                // setTimeout(function(){
                //     $scope.selected_data.GrandTotal = res.data.Result[0].GrandTotal;
                // },0)
                for (var i in $scope.tempdoc) {
                    $scope.tempdoc[i].UploadDataDocument = { UpDocObj: null, FileName: null };
                    $scope.tempdoc[i].UploadDataDocument.UpDocObj = $scope.tempdoc[i].DocumentData;
                    $scope.tempdoc[i].UploadDataDocument.FileName = $scope.tempdoc[i].DocumentDataName;
                }

                console.log('selected_data.ToyotaId', $scope.selected_data);
                if ($scope.selected_data.ToyotaId == null || $scope.selected_data.ToyotaId == '' || typeof $scope.selected_data.ToyotaId == 'undefined') {
                    $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20003&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                }
                else {
                    $scope.urlPricingSpk = '/api/fe/PricingEngine?PricingId=20006&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                }

                ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + $scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                });

                $scope.selected_data.ListInfoDocument = $scope.tempdoc;

                setTimeout(function () {
                    console.log($scope.selected_data, res.data.Result[0]);
                }, 3000);

                $scope.filterTenor($scope.selected_data.LeasingId);
                $scope.formspk = true;
                $scope.listspk = false;



                $scope.equalInfoPengiriman();
            });
        }

        //Kategory Pelanggan
        $scope.kategory = [];
        $scope.PopulateKategory = function () {
            ComboBoxFactory.getDataKategori().then(function (res) {
                var tempCategory = [];
                tempCategory = res.data.Result;
                $scope.kategory = tempCategory.filter(function (type) {
                    return (type.ParentId == 2 || type.CustomerTypeId == 3);
                })
            })
        }
        $scope.PopulateKategory();

        ComboBoxFactory.getDataProvince().then(function (res) { $scope.province = res.data.Result; });
        ComboBoxFactory.getDataGender().then(function (res) { $scope.jeniskelamin = res.data.Result; });
        ComboBoxFactory.getDataStatusPernikahan().then(function (res) { $scope.maritalststus = res.data.Result; });
        ComboBoxFactory.getDataAgama().then(function (res) { $scope.agama = res.data.Result; });
        ComboBoxFactory.getDataJabatan().then(function (res) { $scope.jabatan = res.data.Result; });
        ComboBoxFactory.getDataKoresponden().then(function (res) { $scope.koresponden = res.data.Result; });
        ComboBoxFactory.getDataSectorBisnis().then(function (res) { $scope.sectorbisnis = res.data.Result; });
        ComboBoxFactory.getDataDeliveryCategory().then(function (res) { $scope.delivery = res.data.Result; });
        ComboBoxFactory.getDataRoad().then(function (res) { $scope.road = res.data.Result; });
        ComboBoxFactory.getDataVehicleModel().then(function (res) { $scope.modelname = res.data.Result; });
        //ComboBoxFactory.getDataVehicleType().then(function (res) { $scope.vehicletype = res.data.Result; });
        ComboBoxFactory.getSalesProgram().then(function (res) { $scope.salesprogram = res.data.Result; });
        ComboBoxFactory.getJenisPembayaran().then(function (res) { $scope.jenispembayaran = res.data.Result; });
        ComboBoxFactory.getBank().then(function (res) { $scope.bank = res.data.Result; });
        ComboBoxFactory.getNoSPK().then(function (res) { $scope.nospk = res.data.Result; });
        ComboBoxFactory.getNoSPKIsi().then(function (res) { $scope.nospkisi = res.data.Result; });
        ComboBoxFactory.getDataKetegoriPlat().then(function (res) { $scope.kategoriplat = res.data.Result; });
        ComboBoxFactory.getDataCategoryDelivery().then(function (res) { $scope.kategorydelivery = res.data.Result; });
        ComboBoxFactory.getUnitYear().then(function (res) { $scope.unitYear = res.data.Result; });
        ComboBoxFactory.getDataDeliveryMounth().then(function (res) { $scope.deliveryMounth = res.data.Result; });
        ComboBoxFactory.getDataStatusPDD().then(function (res) { $scope.statusPDD = res.data.Result; });
        ComboBoxFactory.getDataLeasing().then(function (res) { $scope.leasing = res.data.Result; });
        // ComboBoxFactory.getDataTenor().then(function(res) { $scope.temptenor = res.data.Result; })
        ComboBoxFactory.getDataSimulationTenor().then(function (res) { $scope.simulasiltenor = res.data.Result; });
        ComboBoxFactory.getDataInsurance().then(function (res) {



            $scope.insurance = res.data.Result;
            var ulala = angular.copy($scope.insurance[0]);
            $scope.insurance.unshift(ulala);
            $scope.insurance[0].InsuranceId = null;
            $scope.insurance[0].InsuranceName = "Tidak Pakai Asuransi";
            $scope.insurance[0].ListInsuranceProductView = null;
            $scope.insurance[0].ListMProfileInsuranceExtensionView = null;


        });
        // ComboBoxFactory.getDataInsuranceProduct().then(function(res) { $scope.tempproductinsurance = res.data.Result; });
        // ComboBoxFactory.getDataInsuranceEkstension().then(function(res) { $scope.tempinsuranceextension = res.data.Result; });
        ComboBoxFactory.getFinanceFundSource().then(function (res) { $scope.FinanceFundSource = res.data.Result; });
        ComboBoxFactory.getDataInsuranceUserType().then(function (res) { $scope.insuranceusertype = res.data.Result; });
        ComboBoxFactory.getDataInsuranceType().then(function (res) { $scope.insurancetype = res.data.Result; });
        ComboBoxFactory.getDataFundSource().then(function (res) { $scope.fundsource = res.data.Result; });
        // ComboBoxFactory.getDataWarna().then(function(res){ $scope.tempwarna = res.data.Result; })

        /////


        $scope.$watch("selected_data.ListInfoPelanggan[0].ProvinceId", function (newValue, oldValue) {
            $scope.kecamatan0 = "";
            $scope.kelurahan0 = "";
            if ($scope.selected_data.ListInfoPelanggan[0].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[0].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[0].ProvinceId).then(function (res) {
                    $scope.kabupaten0 = res.data.Result;
                    // $scope.kecamatan0 = "";
                    // $scope.kelurahan0 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[0].CityRegencyId", function (newValue, oldValue) {
            $scope.kelurahan0 = "";
            if ($scope.selected_data.ListInfoPelanggan[0].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[0].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[0].CityRegencyId).then(function (res) {
                    $scope.kecamatan0 = res.data.Result;
                    // $scope.kelurahan0 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[0].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[0].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[0].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[0].DistrictId).then(function (res) {
                    $scope.kelurahan0 = res.data.Result;
                });
            };
        })
        /////

        /////
        $scope.$watch("selected_data.ListInfoPelanggan[1].ProvinceId", function (newValue, oldValue) {
            $scope.kecamatan1 = "";
            $scope.kelurahan1 = "";
            if ($scope.selected_data.ListInfoPelanggan[1].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[1].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[1].ProvinceId).then(function (res) {
                    $scope.kabupaten1 = res.data.Result;
                    // $scope.kecamatan1 = "";
                    // $scope.kelurahan1 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[1].CityRegencyId", function (newValue, oldValue) {
            $scope.kelurahan1 = "";
            if ($scope.selected_data.ListInfoPelanggan[1].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[1].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[1].CityRegencyId).then(function (res) {
                    $scope.kecamatan1 = res.data.Result;
                    // $scope.kelurahan1 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[1].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[1].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[1].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[1].DistrictId).then(function (res) {
                    $scope.kelurahan1 = res.data.Result;
                });
            };
        })
        ///

        ///
        $scope.$watch("selected_data.ListInfoPelanggan[2].ProvinceId", function (newValue, oldValue) {
            $scope.kecamatan2 = "";
            $scope.kelurahan2 = "";
            if ($scope.selected_data.ListInfoPelanggan[2].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[2].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[2].ProvinceId).then(function (res) {
                    $scope.kabupaten2 = res.data.Result;
                    // $scope.kecamatan2 = "";
                    // $scope.kelurahan2 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[2].CityRegencyId", function (newValue, oldValue) {
            $scope.kelurahan2 = "";
            if ($scope.selected_data.ListInfoPelanggan[2].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[2].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[2].CityRegencyId).then(function (res) {
                    $scope.kecamatan2 = res.data.Result;
                    // $scope.kelurahan2 = "";
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[2].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[2].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[2].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[2].DistrictId).then(function (res) {
                    $scope.kelurahan2 = res.data.Result;
                });
            };
        })
        ///

        $scope.testunit = function () {
            console.log("coba unit", $scope.selected_data);
        }

        $scope.filterTypeModel = function (setect) {
            //console.log("ulalala", setect);

            for (var i = 0; i < $scope.modelname.length; ++i) {
                if ($scope.modelname[i].VehicleModelId == $scope.selected_data.ListInfoUnit[0].VehicleModelId) {
                    $scope.selected_data.ListInfoUnit[0].VehicleModelName = $scope.modelname[i].VehicleModelName;
                }
            }

            var selected = $scope.modelname.filter(function (type) {
                return (setect == type.VehicleModelId);
            })

            //console.log("ulalalacucuc", selected);

            $scope.selected_data.recalculate = true;
            var lengthUnit = $scope.selected_data.ListInfoUnit.length;
            if (lengthUnit >= 1) {
                $scope.selected_data.ListInfoUnit.splice(1, lengthUnit - 1);
                $scope.selected_data.ListInfoUnit[0].VehicleTypeId = null;
                $scope.selected_data.ListInfoUnit[0].Description = null;
                $scope.selected_data.ListInfoUnit[0].KatashikiCode = null;
                $scope.selected_data.ListInfoUnit[0].SuffixCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorId = null;
                $scope.selected_data.ListInfoUnit[0].ColorCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorCodeName = null;
                $scope.selected_data.ListInfoUnit[0].ColorName = null;
                $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                $scope.selected_data.ListInfoUnit[0].VehiclePrice = 0;
                $scope.selected_data.ListInfoUnit[0].DPP = 0;
                $scope.selected_data.ListInfoUnit[0].Qty = 1;
                $scope.selected_data.ListInfoUnit[0].Discount = 0;
                $scope.selected_data.ListInfoUnit[0].DiscountSubsidiDP = 0;
                $scope.selected_data.ListInfoUnit[0].MediatorCommision = 0;
                $scope.selected_data.ListInfoUnit[0].PPN = 0;
                $scope.selected_data.ListInfoUnit[0].TotalBBN = 0;
                $scope.selected_data.ListInfoUnit[0].PPH22 = 0;
                $scope.selected_data.ListInfoUnit[0].TotalInclVAT = 0;
                $scope.selected_data.ListInfoUnit[0].GrandTotalUnit = 0;
                $scope.selected_data.ListInfoUnit[0].PriceAfterDiscount = 0;

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit.length >= 1) {
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit.splice(1, $scope.selected_data.ListInfoUnit[0].ListDetailUnit.length - 1);
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].Description = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].KatashikiCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].SuffixCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCodeName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAcc = 0;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAll = 0;

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories = []
                    }

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage = []
                    }
                }
                // for(var i=0; i<lengthUnit; i++){
                //     if(i==0){
                //         var lengthDetail = $scope.selected_data.ListInfoUnit[i].ListDetail.length;
                //         for(var j=0; j<lengthDetail; j++){
                //             var many = lengthDetail - j;
                //             if(j>=1){
                //                 $scope.selected_data.ListInfoUnit[i].ListDetail.splice(j,1);
                //             }
                //         }
                //     }else{
                //         $scope.selected_data.ListInfoUnit.splice(i,)
                //     }
                // }
            }

            if (setect != null || typeof setect != "undefined") {
                ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + setect).then(function (res) {
                    $scope.vehicletype = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeId = null;
                })
            }

            if (setect != null || typeof setect != "undefined") {
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                })
            }
        }

        $scope.warna = [];
        $scope.filterWarna = function (select) {
            var selected = $scope.vehicletype.filter(function (type) {
                return (select == type.VehicleTypeId);
            })
            //console.log("test", selected);
            $scope.selected_data.recalculate = true;
            // var tempunit = $scope.vehicletype.filter(function (type) {
            //     return (type.VehicleTypeId == selected);
            // });
            // console.log("test", tempunit);
            var lengthUnit = $scope.selected_data.ListInfoUnit.length;
            if (lengthUnit >= 1) {
                $scope.selected_data.ListInfoUnit.splice(1, lengthUnit - 1);
                $scope.selected_data.ListInfoUnit[0].VehicleTypeId = selected[0].VehicleTypeId;
                $scope.selected_data.ListInfoUnit[0].Description = selected[0].Description;
                $scope.selected_data.ListInfoUnit[0].KatashikiCode = selected[0].KatashikiCode;
                $scope.selected_data.ListInfoUnit[0].SuffixCode = selected[0].SuffixCode;
                $scope.selected_data.ListInfoUnit[0].ColorId = null;
                $scope.selected_data.ListInfoUnit[0].ColorCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorCodeName = null;
                $scope.selected_data.ListInfoUnit[0].ColorName = null;
                $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                $scope.selected_data.ListInfoUnit[0].VehiclePrice = 0;
                $scope.selected_data.ListInfoUnit[0].DPP = 0;
                $scope.selected_data.ListInfoUnit[0].Qty = 1;
                $scope.selected_data.ListInfoUnit[0].Discount = 0;
                $scope.selected_data.ListInfoUnit[0].DiscountSubsidiDP = 0;
                $scope.selected_data.ListInfoUnit[0].MediatorCommision = 0;
                $scope.selected_data.ListInfoUnit[0].PPN = 0;
                $scope.selected_data.ListInfoUnit[0].TotalBBN = 0;
                $scope.selected_data.ListInfoUnit[0].PPH22 = 0;
                $scope.selected_data.ListInfoUnit[0].TotalInclVAT = 0;
                $scope.selected_data.ListInfoUnit[0].GrandTotalUnit = 0;
                $scope.selected_data.ListInfoUnit[0].PriceAfterDiscount = 0;

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit.length >= 1) {
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit.splice(1, $scope.selected_data.ListInfoUnit[0].ListDetailUnit.length - 1);
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeId = selected[0].VehicleTypeId;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].Description = selected[0].Description;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].KatashikiCode = selected[0].KatashikiCode;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].SuffixCode = selected[0].SuffixCode;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCodeName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAcc = 0;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAll = 0;

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories = []
                    }

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage = []
                    }
                }
                // for(var i=0; i<lengthUnit; i++){
                //     if(i==0){
                //         var lengthDetail = $scope.selected_data.ListInfoUnit[i].ListDetail.length;
                //         for(var j=0; j<lengthDetail; j++){
                //             var many = lengthDetail - j;
                //             if(j>=1){
                //                 $scope.selected_data.ListInfoUnit[i].ListDetail.splice(j,1);
                //             }
                //         }
                //     }else{
                //         $scope.selected_data.ListInfoUnit.splice(i,)
                //     }
                // }
            }

            if (select != null || typeof select != "undefined") {
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                })
            }

            CreateSpkFactory.bookingFeeDp($scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.tempTDP = angular.copy(res.data.NominalDP);
                $scope.selected_data.TDP_DP = res.data.NominalDP;
                $scope.selected_data.recalculate == true;
            })
        };

        $scope.temptenor = [];
        $scope.filterTenor = function (selected) {
            if (selected != null || typeof selected != "undefined") {
                ComboBoxFactory.getDataTenor(selected).then(function (res) {

                    for (var i = 0; i < res.data.Result.length; ++i) {
                        console.log('selected', selected);
                        if (res.data.Result[i].LeasingId == selected) {

                            $scope.tenor = res.data.Result[i].ListLeasingTenor;
                            console.log('$scope.tenor', $scope.tenor);
                            break;
                        }
                    }


                    setTimeout(function () {
                        $scope.selected_data.LeasingLeasingTenorId = $scope.selected_data.LeasingLeasingTenorId;
                    }, 300);
                })
            }
        }

        $scope.$watch("selected_data.LeasingId", function (newValue, oldValue) {
            if (newValue != oldValue) {
                ComboBoxFactory.getDataTenor($scope.selected_data.LeasingId).then(function (res) {
                    //$scope.tenor = res.data.Result;
                    for (var i = 0; i < res.data.Result.length; ++i) {
                        if (res.data.Result[i].LeasingId == selected) {

                            console.log('$scope.tenor', $scope.tenor);
                            $scope.tenor = res.data.Result[i].ListLeasingTenor;
                            break;
                        }
                    }
                })
            }
        });

        $scope.year = function (selected) {
            if ($scope.selected_data.ListInfoUnit.length > 0) {
                for (var i in $scope.selected_data.ListInfoUnit) {
                    $scope.selected_data.ListInfoUnit[i].ProductionYear = selected.Year;
                }
            }
        }

        $scope.product = [];
        $scope.insuranceextension = [];
        $scope.filterProduct = function (selected) {
            if (selected != undefined || selected != null) {
                ComboBoxFactory.getDataInsuranceProduct(selected).then(function (res) { $scope.product = res.data.Result; });
                ComboBoxFactory.getDataInsuranceEkstension(selected).then(function (res) { $scope.insuranceextension = res.data.Result; });
            }
        }

        $scope.$watch("selected_data.InsuranceId", function (newValue, oldValue) {
            if (newValue != oldValue) {
                ComboBoxFactory.getDataInsuranceProduct($scope.selected_data.InsuranceId).then(function (res) { $scope.product = res.data.Result; });
                ComboBoxFactory.getDataInsuranceEkstension($scope.selected_data.InsuranceId).then(function (res) { $scope.insuranceextension = res.data.Result; });
            }
        });

        ComboBoxFactory.getKodePajak().then(function (res) {
            $scope.kodepajak = res.data.Result;
            if ($scope.statusAction == null || $scope.statusAction == 'buat') {
                for (var i in $scope.kodepajak) {
                    if ($scope.kodepajak[i].DefaultBit == true) {
                        $scope.selected_data.CodeInvoiceTransactionTaxId = $scope.kodepajak[i].CodeInvoiceTransactionTaxId
                    }
                }
            }

        });

        $scope.fundsourceselected = function (selected) {
            $scope.fundsourceselect = selected;
        }

        // $scope.fundsource = function () {
        //     if ($scope.fundsourceselect.FundSourceName == 'Cash') {
        //         $scope.bankstastus = false;
        //     } else {
        //         $scope.bankstastus = true;
        //     }
        // }

        $scope.loadingdoc == false;
        $scope.openDokumenlist = function () {
            $scope.loadingdoc = true;
            var numItems = angular.element('.ui.modal.dokumentList').length;
            ComboBoxFactory.getDataDocument().then(function (res) {
                $scope.dokumentlist = res.data.Result;
                setTimeout(function () {
                    angular.element('.ui.modal.dokumentList').modal('refresh');
                    $scope.loadingdoc = false;
                }, 0);
                if (numItems > 1) {
                    angular.element('.ui.modal.dokumentList').not(':first').remove();
                }
                angular.element('.ui.modal.dokumentList').modal({ observeChanges: true }).modal('show');
            })
        }

        $scope.selectedItems = [];
        $scope.toggleChecked = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedItems.push(data);
            }
        };

        $scope.ApproveData = function () {
            angular.element('.ui.modal.dokumentList').modal('hide');
            if ($scope.selected_data.ListInfoDocument.length == 0) {
                $scope.selected_data.ListInfoDocument = angular.copy($scope.selectedItems);
            } else {
                for (var i in $scope.selected_data.ListInfoDocument) {
                    for (var j in $scope.selectedItems) {
                        if ($scope.selectedItems[j].DocumentId == $scope.selected_data.ListInfoDocument[i].DocumentId) {
                            $scope.selectedItems.splice(j, 1);
                        }
                    }
                }
                for (var i in $scope.selectedItems) {
                    $scope.selected_data.ListInfoDocument.push($scope.selectedItems[i]);
                }
            }

            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 1;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];

        };

        $scope.RejectData = function () {
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 0;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];
            angular.element('.ui.modal.dokumentList').modal('hide');
        };

        $scope.removeImage = function (list, index) {
            list.UploadDataDocument.UpDocObj = null;
            list.UploadDataDocument.FileName = null;
        }

        $scope.viewImage = function (docList) {
            $scope.image = { documentname: null, viewdocument: null, viewdocumentName: null };
            if ($scope.menusekarang == 'Daftar SPK') {
                var numItems = angular.element('.ui.modal.dokumenSPKViewMobiles').length;
            } else {
                var numItems = angular.element('.ui.modal.dokumenSPKViewMobilesDraft').length;
            }

            MaintenanceSpkFactory.getImage(docList.DocumentURL).then(function (res) {
                if ($scope.menusekarang == 'Daftar SPK') {
                    setTimeout(function () {
                        angular.element('.ui.modal.dokumenSPKViewMobiles').modal('refresh');
                    }, 0);
                    $scope.image = { documentname: docList.DocumentName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                    if (numItems > 1) {
                        angular.element('.ui.modal.dokumenSPKViewMobiles').not(':first').remove();
                    }
                    angular.element('.ui.modal.dokumenSPKViewMobiles').modal('show');
                } else {
                    setTimeout(function () {
                        angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('refresh');
                    }, 0);
                    $scope.image = { documentname: docList.DocumentName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                    if (numItems > 1) {
                        angular.element('.ui.modal.dokumenSPKViewMobilesDraft').not(':first').remove();
                    }
                    angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('show');
                }
            });
        }

        $scope.removeDoc = function (index) {
            $scope.selected_data.ListInfoDocument.splice(index, 1);
        }

        $scope.keluarDokumen = function () {
            //$scope.image = { viewdocument: null, viewdocumentName: null };
            //angular.element('.ui.modal.dokumenSPKViewMobiles').not(':first').remove();
            if ($scope.menusekarang == 'Daftar SPK') {
                angular.element('.ui.modal.dokumenSPKViewMobiles').modal('hide');
            } else {
                angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('hide');
            }
        }


        //////////////////////////////////////////////////////////////////
        ///// Buat Aksesories
        /////////////////////////////////////////////////////////////////
        $scope.tipeaccesories = [{ Name: "Paket" }, { Name: "Satuan" }];

        // $scope.$watch("searchAccesoriestype", function (newValue, oldValue) {
        //     console.log(newValue,oldValue);
        //     if(oldValue == "Paket" || oldValue == "Satuan"){
        //         $scope.searchAccesoriestype = "";
        //     }
        // });

        $scope.debug = function () {
            var temp = angular.copy($scope.searchAccesoriestype);
            console.log("test", temp);

            $scope.searchAccesoriestype.Name = "";

        }

        // $scope.testModal = function () {
        //     $scope.searchAccesoriestype = null; 
        // }

        $scope.btnTambahAcessories = function (header) {
            $scope.modalAccs = header;
            $scope.searchAccesoriestype = null;
            //angular.element(document.querySelector('#categoryAccs')).prop('value','');
            //$("#categoryAccs").val("");
            $scope.Satuan = null;
            $scope.acessoriesVehicle = [];
            $scope.acessoriesVehiclesatuan = [];
            $scope.searchAccesories = {};
            $scope.selectedItemsAccsPackage = [];
            $scope.selecedItemsAccsSatuan = [];


            for (var i in $scope.modalAccs.ListAccessories) {
                if ($scope.modalAccs.ListAccessories[i].Discount != null || $scope.modalAccs.ListAccessories[i].Discount != undefined) {
                    // $scope.modalAccs.ListAccessories[i].IsDiscount = true;
                }
            }

            for (var i in $scope.modalAccs.ListAccessoriesPackage) {
                if ($scope.modalAccs.ListAccessoriesPackage[i].Discount != null || $scope.modalAccs.ListAccessoriesPackage[i].Discount != undefined) {
                    // $scope.modalAccs.ListAccessoriesPackage[i].IsDiscount = true;
                }
            }

            console.log('$scope.modalAccs.ListAccessories pas tambah ====>', $scope.modalAccs.ListAccessories);
            console.log('$scope.modalAccs.ListAccessoriesPackage pas tambah ====>', $scope.modalAccs.ListAccessoriesPackage);

            $scope.iniListAksesorisAlreadyAdded = angular.copy($scope.modalAccs.ListAccessories);
            $scope.iniListAksesorisPackageAlreadyAdded = angular.copy($scope.modalAccs.ListAccessoriesPackage);

            //var numLength = angular.element('.ui.modal.ModalTambahAksesoris').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalTambahAksesoris').modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalTambahAksesoris').modal('show');
            //if (numLength > 1) {
            angular.element('.ui.modal.ModalTambahAksesoris').not(':first').remove();
            //}

        }

        $scope.loadingAcc = false;
        $scope.AccessoriesChange = function (vehicleTypeId, tipe) {
            console.log(vehicleTypeId, tipe);
            console.log($scope.searchAccesoriestype);
            //$scope.searchAccesoriestype.Name = tipe;
            $scope.tipeAksesories = tipe;
            if (tipe == 'Paket') {
                $scope.Satuan = null;
                $scope.loadingAcc = true;
                ComboBoxFactory.getDataAccsessoriesPackage(vehicleTypeId).then(function (res) {
                    $scope.acessoriesVehicle = res.data.Result;
                    $scope.loadingAcc = false;
                    angular.element('.ui.modal.ModalTambahAksesoris').modal({ observeChanges: true }).modal('refresh');
                })
            } else if (tipe == 'Satuan') {
                $scope.Satuan = 'OK';
                $scope.acessoriesVehicle = [];
            }
        }

        $scope.AccesoriesSatuan = function (vehicleId, param) {
            $scope.loadingAcc = true;
            if (param.Classification == undefined) {
                param.Classification = false;
            }
            ComboBoxFactory.getDataAcessories(vehicleId, param.Classification, param.PartName).then(function (res) {
                $scope.acessoriesVehiclesatuan = res.data.Result;
                $scope.loadingAcc = false;
            })
        }

        //// Akesosris Package
        //////////////////////
        $scope.selectedItemsAccsPackage = [];
        //console.log('lengthacc', $scope.selectedItemsAccsPackage.length);
        $scope.toggleCheckedPackageAccs = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItemsAccsPackage.indexOf(data);
                $scope.selectedItemsAccsPackage.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedItemsAccsPackage.push(data);
            }
        }

        //// Aksesories Satuan
        //////////////////////
        $scope.selecedItemsAccsSatuan = [];
        $scope.toggleCheckedPcsAccs = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selecedItemsAccsSatuan.indexOf(data);
                $scope.selecedItemsAccsSatuan.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selecedItemsAccsSatuan.push(data);
            }
        }

        //// Aksesories Pilih
        //////////////////////

        $scope.OkAccsPackage = function () {
            $scope.selected_data.recalculate = true;
            if ($scope.tipeAksesories == 'Paket') {
                console.log('ini pakettt');

                if ($scope.modalAccs.ListAccessoriesPackage.length == 0) {
                    console.log("$scope.modalAccs.ListAccessoriesPackage.length 0 ===>", $scope.modalAccs.ListAccessoriesPackage.length);
                    angular.forEach($scope.selectedItemsAccsPackage, function (accp, accIdx) {
                        console.log('ini isi OkAccsPackage yang paket accp ===>', accp)
                        var param = {
                            "Title": "AccessoriesPackage",
                            "HeaderInputs": {
                                "$AccPackageId$": accp.AccessoriesPackageId,
                                "$QTY$": "1",
                                "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                                "$OutletId$": $scope.user.OutletId,
                            }
                        };

                        for (var i = 0; i < accp.listDetail.length; i++) {
                            accp.listDetail[i].AccessoriesName = accp.listDetail[i].PartsName;
                            accp.listDetail[i].AccessoriesCode = accp.listDetail[i].PartsCode;

                        }

                        $http.post($scope.urlAccessoriesPackageSpk, param)
                            .then(function (res) {
                                $scope.modalAccs.ListAccessoriesPackage.push({
                                    AccessoriesPackageId: accp.AccessoriesPackageId,
                                    AccessoriesPackageName: accp.AccessoriesPackageName,
                                    AccessoriesPackageCode: accp.AccessoriesPackageCode,
                                    Price: accp.Price,
                                    Qty: 1,
                                    IsDiscount: false,
                                    Discount: parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]),
                                    PPN: parseFloat(res.data.Codes["[PPN]"]),
                                    DPP: parseFloat(res.data.Codes["[DPP]"]),
                                    TotalInclVAT: parseFloat(res.data.Codes["[TotalHargaAcc]"]),
                                    ListDetail: accp.listDetail
                                });

                                // angular.forEach(accp.listDetail, function (detail, detailIdx) {
                                //     $scope.modalAccs.ListAccessoriesPackage[accIdx].ListDetail.push({
                                //         AccessoriesId: detail.PartsId,
                                //         AccessoriesCode: detail.PartsCode,
                                //         AccessoriesName: detail.PartsName,
                                //         Price: detail.RetailPrice,
                                //         UomId: detail.UomId,
                                //         UomName: detail.UomName,
                                //     });
                                // });
                            })
                    });



                } else if ($scope.modalAccs.ListAccessoriesPackage.length > 0) {
                    console.log("$scope.modalAccs.ListAccessoriesPackage.length > 0 ===>", $scope.modalAccs.ListAccessoriesPackage.length);
                    for (var i in $scope.modalAccs.ListAccessoriesPackage) {

                        for (var j in $scope.selectedItemsAccsPackage) {
                            if ($scope.modalAccs.ListAccessoriesPackage[i].AccessoriesPackageId == $scope.selectedItemsAccsPackage[j].AccessoriesPackageId) {
                                $scope.modalAccs.ListAccessoriesPackage[i].Qty++;
                                $scope.selectedItemsAccsPackage.splice(j, 1);
                            }
                        }
                    }

                    if ($scope.selectedItemsAccsPackage.length > 0) {
                        console.log("$scope.selectedItemsAccsPackage.length ===>", $scope.selectedItemsAccsPackage.length + "===>" + $scope.selectedItemsAccsPackage);


                        for (var k in $scope.selectedItemsAccsPackage) {



                            $scope.modalAccs.ListAccessoriesPackage.push({
                                AccessoriesPackageId: $scope.selectedItemsAccsPackage[k].AccessoriesPackageId,
                                AccessoriesPackageName: $scope.selectedItemsAccsPackage[k].AccessoriesPackageName,
                                Price: $scope.selectedItemsAccsPackage[k].Price,
                                Qty: 1,
                                IsDiscount: false,
                                ListDetail: $scope.selectedItemsAccsPackage[k].listDetail
                            });

                            for (var l = 0; l < $scope.selectedItemsAccsPackage[k].listDetail.length; l++) {
                                if ($scope.selectedItemsAccsPackage[k].listDetail[l].PartsName != undefined && $scope.selectedItemsAccsPackage[k].listDetail[l].PartsCode != undefined);
                                $scope.selectedItemsAccsPackage[k].listDetail[l].AccessoriesCode = $scope.selectedItemsAccsPackage[k].listDetail[l].PartsCode;
                                $scope.selectedItemsAccsPackage[k].listDetail[l].AccessoriesName = $scope.selectedItemsAccsPackage[k].listDetail[l].PartsName;

                                if ($scope.selectedItemsAccsPackage[k].listDetail.length < 1) {
                                    $scope.modalAccs.ListAccessoriesPackage[k].ListDetail.push({
                                        AccessoriesId: $scope.selectedItemsAccsPackage[k].listDetail[l].PartsId,
                                        AccessoriesCode: $scope.selectedItemsAccsPackage[k].listDetail[l].PartsCode,
                                        AccessoriesName: $scope.selectedItemsAccsPackage[k].listDetail[l].PartsName,
                                        Price: $scope.selectedItemsAccsPackage[k].listDetail[l].RetailPrice,
                                        UomId: $scope.selectedItemsAccsPackage[k].listDetail[l].UomId,
                                        UomName: $scope.selectedItemsAccsPackage[k].listDetail[l].UomName,
                                    });
                                }

                            }

                        }
                    }






                    angular.forEach($scope.modalAccs.ListAccessoriesPackage, function (accp, accIdx) {
                        // Koment Buat CR2
                        if (accp.IsDiscount == true) {

                        } else {
                            var param = {
                                "Title": "AccessoriesPackage",
                                "HeaderInputs": {
                                    "$AccPackageId$": accp.AccessoriesPackageId,
                                    "$QTY$": accp.Qty,
                                    "$Discount$": accp.Discount,
                                    "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                                    "$OutletId$": $scope.user.OutletId,
                                }
                            };

                            $http.post($scope.urlAccessoriesPackageSpk, param)
                                .then(function (res) {
                                    accp.AccessoriesPackageId = accp.AccessoriesPackageId;
                                    accp.AccessoriesPackageName = accp.AccessoriesPackageName;
                                    accp.Price = accp.Price;
                                    accp.Qty = accp.Qty;
                                    accp.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                                    accp.PPN = parseFloat(res.data.Codes["[PPN]"]);
                                    accp.DPP = parseFloat(res.data.Codes["[DPP]"]);
                                    accp.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                                    accp.ListDetail = accp.ListDetail;
                                })
                        }

                    });


                    $scope.listDetailAccPackage = angular.copy($scope.modalAccs.ListAccessoriesPackage);
                    console.log("selected_data.ListInfoUnit ===>", $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0]);
                    console.log('$scope.modalAccs.ListAccessoriesPackage pas udah selesai ====>', $scope.listDetailAccPackage);

                }

                $scope.selectedItemsAccsPackage = [];
                $scope.acessoriesVehicle = [];
                //$scope.searchAccesoriestype="";
                //$scope.searchAccesoriestype.Name = "";
                //angular.element(document.querySelector('#categoryAccs')).prop('value','');
                angular.element(".ui.modal.ModalTambahAksesoris").modal("hide");


            } else if ($scope.tipeAksesories == 'Satuan') {
                console.log('iniii satuann');
                if ($scope.modalAccs.ListAccessories.length == 0) {
                    angular.forEach($scope.selecedItemsAccsSatuan, function (acc, accIdx) {
                        console.log('ini isi OkAccsPackage yang satuan acc ===>', acc);
                        var param = {
                            "Title": "Accessories",
                            "HeaderInputs": {
                                "$AccessoriesId$": acc.PartsId,
                                "$QTY$": 1,
                                "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                                "$OutletId$": $scope.user.OutletId,
                                "$AccessoriesCode$": acc.PartsCode,
                            }
                        };

                        $http.post($scope.urlAccessoriesSpk, param)
                            .then(function (res) {
                                $scope.modalAccs.ListAccessories.push({
                                    AccessoriesId: acc.PartsId,
                                    AccessoriesName: acc.PartsName,
                                    AccessoriesCode: acc.PartsCode,
                                    Price: acc.RetailPrice,
                                    IsDiscount: false,
                                    Qty: 1,
                                    Discount: parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]),
                                    PPN: parseFloat(res.data.Codes["[PPN]"]),
                                    DPP: parseFloat(res.data.Codes["[DPP]"]),
                                    TotalInclVAT: parseFloat(res.data.Codes["[TotalHargaAcc]"]),
                                });
                            })
                    });
                } else {
                    for (var j in $scope.modalAccs.ListAccessories) {
                        for (var i in $scope.selecedItemsAccsSatuan) {
                            if ($scope.modalAccs.ListAccessories[j].AccessoriesId == $scope.selecedItemsAccsSatuan[i].PartsId) {
                                $scope.modalAccs.ListAccessories[j].Qty++;
                                $scope.selecedItemsAccsSatuan.splice(i, 1);
                            }
                        }

                    }

                    if ($scope.selecedItemsAccsSatuan.length > 0) {
                        for (var k in $scope.selecedItemsAccsSatuan) {
                            $scope.iniListAksesorisAlreadyAdded.push({
                                AccessoriesId: $scope.selecedItemsAccsSatuan[k].PartsId,
                                AccessoriesName: $scope.selecedItemsAccsSatuan[k].PartsName,
                                AccessoriesCode: $scope.selecedItemsAccsSatuan[k].PartsCode,
                                Price: $scope.selecedItemsAccsSatuan[k].RetailPrice,
                                Qty: 1,
                                IsDiscount: false
                            });
                        }
                    }


                    $scope.modalAccs.ListAccessories = angular.copy($scope.iniListAksesorisAlreadyAdded);
                    console.log('$scope.modalAccs.ListAccessories pas udah selesai ====>', $scope.modalAccs.ListAccessories);


                    angular.forEach($scope.modalAccs.ListAccessories, function (acc, accIdx) {

                        ////// Koment Buat CR 2
                        if (acc.IsDiscount == true) {

                        } else {
                            var param = {
                                "Title": "Accessories",
                                "HeaderInputs": {
                                    "$AccessoriesId$": acc.AccessoriesId,
                                    "$QTY$": acc.Qty,
                                    "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                                    "$OutletId$": $scope.user.OutletId,
                                    "$AccessoriesCode$": acc.AccessoriesCode,

                                }
                            };

                            $http.post($scope.urlAccessoriesSpk, param)
                                .then(function (res) {
                                    acc.AccessoriesId = acc.AccessoriesId;
                                    acc.AccessoriesName = acc.AccessoriesName;
                                    acc.Price = acc.Price;
                                    acc.Qty = acc.Qty;
                                    acc.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]),
                                        acc.PPN = parseFloat(res.data.Codes["[PPN]"]);
                                    acc.DPP = parseFloat(res.data.Codes["[DPP]"]);
                                    acc.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                                })
                        }
                    });
                }

                $scope.selecedItemsAccsSatuan = [];
                $scope.acessoriesVehiclesatuan = [];
                //$scope.searchAccesoriestype= "";
                //$scope.searchAccesoriestype.Name = "";
                // angular.element(document.querySelector('#categoryAccs')).prop('value','');
                angular.element(".ui.modal.ModalTambahAksesoris").modal("hide");
            }
            //$("#categoryAccs").val("");
            // angular.element(document.querySelector('#categoryAccs')).prop('value',undefined);
        };

        // $scope.getGlobalMediator = function() {
        //     CreateSpkFactory.getGlobalMediator().then(
        //          function (res) 
        //          {
        //              //$scope.selected_data.MediatorCommision = res.data.Result;
        //              $scope.mediatorbit = res.data.Result.mediatorbit;
        //              console.log('Cek Mediatorbit', $scope.mediatorbit)
        //          }
        //     );
        // }

        $scope.KalkulasiAccp = function (header, accp, index, status) {
            console.log(' ini isi KalkulasiAccp Paket accp ===>', accp);
            $scope.selected_data.recalculate = true;
            //$scope.getGlobalMediator();
            if (status == true) {
                accp.Qty = accp.Qty + 1;
                console.log('ini acc Paket Satuan +1');
            } else {
                if (accp.Qty == 1) {
                    header.ListAccessoriesPackage.splice(index, 1);
                    var temp = angular.copy(header.ListAccessoriesPackage);
                    header.ListAccessoriesPackage = [];
                    for (var i in temp) {
                        header.ListAccessoriesPackage.push(temp[i]);
                    }
                } else {
                    header.ListAccessoriesPackage[index].Qty = header.ListAccessoriesPackage[index].Qty - 1;
                    console.log('ini acc Paket Paket -1');
                }
            }

            // Komen Buat CR 2
            if (accp.IsDiscount == true) {

            } else {
                var param = {
                    "Title": "AccessoriesPackage",
                    "HeaderInputs": {
                        "$AccPackageId$": accp.AccessoriesPackageId,
                        "$QTY$": accp.Qty,
                        "$Discount$": accp.Discount,
                        "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                        "$OutletId$": $scope.user.OutletId,
                    }
                };

                $http.post($scope.urlAccessoriesPackageSpk, param)
                    .then(function (res) {
                        accp.AccessoriesPackageId = accp.AccessoriesPackageId;
                        accp.AccessoriesPackageName = accp.AccessoriesPackageName;
                        accp.Price = accp.Price;
                        accp.Qty = accp.Qty;
                        accp.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                        accp.PPN = parseFloat(res.data.Codes["[PPN]"]);
                        accp.DPP = parseFloat(res.data.Codes["[DPP]"]);
                        accp.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                        accp.ListDetail = accp.ListDetail;
                    });
            }
        }

        $scope.freeAcc = function (accp) {
            console.log('ini isi freeAcc  accp ===>', accp);
            $scope.selected_data.recalculate = true;
            if (accp.IsDiscount == true) {
                console.log('ini Free Acc Package Checklist Truee ===>', accp.IsDiscount);
                accp.Discount = accp.Price;
                accp.PPN = 0;
                accp.DPP = 0;
                accp.TotalInclVAT = 0;
            } else if (accp.IsDiscount == false) {
                console.log('ini Free Acc Package Checklist false ===>', accp.IsDiscount);
                var param = {
                    "Title": "AccessoriesPackage",
                    "HeaderInputs": {
                        "$AccPackageId$": accp.AccessoriesPackageId,
                        "$QTY$": accp.Qty,
                        "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                        "$OutletId$": $scope.user.OutletId,
                    }
                };

                $http.post($scope.urlAccessoriesPackageSpk, param)
                    .then(function (res) {
                        accp.AccessoriesPackageId = accp.AccessoriesPackageId;
                        accp.AccessoriesPackageName = accp.AccessoriesPackageName;
                        accp.Price = accp.Price;
                        accp.Qty = accp.Qty;
                        accp.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                        accp.PPN = parseFloat(res.data.Codes["[PPN]"]);
                        accp.DPP = parseFloat(res.data.Codes["[DPP]"]);
                        accp.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                        accp.ListDetail = accp.ListDetail;
                    });
            }
        }

        $scope.freeAccSatuan = function (accp) {
            console.log('ini isi freeAccSatuan accp ===>', accp);
            $scope.selected_data.recalculate = true;
            if (accp.IsDiscount == true) {
                accp.Discount = accp.Price;
                accp.PPN = 0;
                accp.DPP = 0;
                accp.TotalInclVAT = 0;
                accp.IsDiscount == true;
            } else if (accp.IsDiscount == false) {
                accp.IsDiscount == false;
                var param = {
                    "Title": "Accessories",
                    "HeaderInputs": {
                        "$AccessoriesId$": accp.AccessoriesId,
                        "$AccessoriesCode$": accp.AccessoriesCode,
                        "$QTY$": accp.Qty,
                        "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                        "$OutletId$": $scope.user.OutletId,
                    }
                };

                $http.post($scope.urlAccessoriesSpk, param)
                    .then(function (res) {
                        accp.AccessoriesId = accp.AccessoriesId;
                        accp.AccessoriesName = accp.AccessoriesName;
                        accp.Price = accp.Price;
                        accp.Qty = accp.Qty;
                        accp.Discount = parseFloat(res.data.Codes["[Discount]"]) == null ? 0 : parseFloat(res.data.Codes["[Discount]"]);
                        accp.PPN = parseFloat(res.data.Codes["[PPN]"]);
                        accp.DPP = parseFloat(res.data.Codes["[DPP]"]);
                        accp.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                    })
            }
        }

        $scope.kalkulasiAcc = function (header, accp, index, status) {
            console.log(' ini isi kalkulasiAcc Satuan accp ===>', accp);
            $scope.selected_data.recalculate = true;
            if (status == true) {
                accp.Qty = accp.Qty + 1;
                console.log('ini acc Satuan +1');
            } else {
                if (accp.Qty == 1) {
                    header.ListAccessories.splice(index, 1);
                    var temp = angular.copy(header.ListAccessories);
                    header.ListAccessories = [];
                    for (var i in temp) {
                        header.ListAccessories.push(temp[i]);
                    }
                } else {
                    header.ListAccessories[index].Qty = header.ListAccessories[index].Qty - 1;
                    console.log('ini acc Satuan -1');
                }
            }

            // Koment Buat CR2
            if (accp.IsDiscount == true) {

            } else {
                var param = {
                    "Title": "Accessories",
                    "HeaderInputs": {
                        "$AccessoriesId$": accp.AccessoriesId,
                        "$AccessoriesCode$": accp.AccessoriesCode,
                        "$QTY$": accp.Qty,
                        "$Discount$": accp.Discount,
                        "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                        "$OutletId$": $scope.user.OutletId,
                    }
                };

                $http.post($scope.urlAccessoriesSpk, param)
                    .then(function (res) {
                        accp.AccessoriesId = accp.AccessoriesId;
                        accp.AccessoriesName = accp.AccessoriesName;
                        accp.Price = accp.Price;
                        accp.Qty = accp.Qty;
                        accp.PPN = parseFloat(res.data.Codes["[PPN]"]);
                        accp.DPP = parseFloat(res.data.Codes["[DPP]"]);
                        accp.TotalInclVAT = parseFloat(res.data.Codes["[TotalHargaAcc]"]);
                    });
            }
        }

        $scope.detailaccs = false;
        $scope.opendetail = function () {
            switch ($scope.detailaccs) {
                case false:
                    $scope.detailaccs = true; break;
                case true:
                    $scope.detailaccs = false; break;
            }
        }

        // $scope.searchAccesoriestype = "";
        $scope.keluarModalAcc = function () {
            //$scope.searchAccesoriestype = "";
            // $('#categoryAccs').get().reset();
            //$scope.searchAccesoriestype = '';
            //angular.element('#categoryAccs').get().reset();
            //angular.element(document.querySelector('#categoryAccs')).val(null);

            angular.element(".ui.modal.ModalTambahAksesoris").modal("hide");
        }

        ///////////////////////////////////////////////////////////
        ////// Karoseri
        //////////////////////////////////////////////////////////
        $scope.selectKaroseri = [];
        $scope.selectedKaroseri = function (data) {
            $scope.selectKaroseri.splice(0, 1);
            $scope.selectKaroseri.push(data);
        }

        $scope.pilihKaroseri = function () {
            $scope.selected_data.recalculate = true;
            var link = '/api/fe/PricingEngine/?PricingId=20004&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
            var param = {
                "Title": "Karoseri",
                "HeaderInputs": {
                    "$KaroseriId$": $scope.selectKaroseri[0].KaroseriId,
                    "$QTY$": 1,
                    "$VehicleTypeId$": $scope.selected_data.ListInfoUnit[0].VehicleTypeId,
                    "$OutletId$": $scope.user.OutletId,
                }
            };

            $http.post(link, param)
                .then(function (res) {
                    $scope.selected_data.KaroseriId = $scope.selectKaroseri[0].KaroseriId;
                    $scope.selected_data.KaroseriName = $scope.selectKaroseri[0].KaroseriName,
                        $scope.selected_data.KaroseriPrice = parseFloat(res.data.Codes["[HargaKar]"]);
                    $scope.selected_data.KaroseriPPN = parseFloat(res.data.Codes["[PPN]"]);
                    $scope.selected_data.DPPKaroseri = parseFloat(res.data.Codes["[DPP]"]);
                    //acc.DPP = parseFloat(res.data.Codes["[DPP]"]);
                    $scope.selected_data.KaroseriTotalInclVAT = parseFloat(res.data.Codes["[TotalHargaKar]"]);
                    $scope.selectKaroseri = [];
                    angular.element('.ui.modal.Karoseri').modal('hide');
                })
        }

        $scope.btnTambahKaroseri = function (vehicleId, ColorId) {
            ComboBoxFactory.getKaroseri(vehicleId, ColorId).then(function (res) {
                $scope.Karoseri = res.data.Result;
                var numLength = angular.element('.ui.modal.Karoseri').length;
                setTimeout(function () {
                    angular.element('.ui.modal.Karoseri').modal('refresh');
                }, 0);
                if (numLength > 1) {
                    angular.element('.ui.modal.Karoseri').not(':first').remove();
                }
                angular.element('.ui.modal.Karoseri').modal('show');
            })
        }

        $scope.removeKaroseri = function () {
            $scope.selected_data.recalculate = true;
            $scope.selected_data.KaroseriId = null;
            $scope.selected_data.KaroseriPrice = null;
            $scope.selected_data.KaroseriName = null;
            $scope.selected_data.KaroseriPPN = null;
            $scope.selected_data.KaroseriTotalInclVAT = null;
        }

        $scope.keluarModalKaro = function () {
            angular.element('.ui.modal.Karoseri').modal('hide');
        }
        //////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////
        /////// Ekstnsion Insurance
        //////////////////////////////////////////////////////////////
        $scope.btnTambahperluasan = function () {
            angular.element(".ui.modal.insuranceEkstension").modal("show");
            angular.element('.ui.modal.insuranceEkstension').not(':first').remove();
        }

        $scope.selectedEkstensionInsurance = [];
        $scope.toggleSelectedExtension = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedEkstensionInsurance.indexOf(data);
                $scope.selectedEkstensionInsurance.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedEkstensionInsurance.push(data);
            }
        }

        $scope.OkSelectedExtension = function () {
            $scope.selected_data.ListInfoPerluasanJaminan = $scope.selectedEkstensionInsurance;
            angular.element(".ui.modal.insuranceEkstension").modal("hide");
            for (var i = 0; i < $scope.selectedEkstensionInsurance.length; i++) {
                $scope.selectedEkstensionInsurance[i].ApprovalFlag = 1;
                delete $scope.selectedEkstensionInsurance[i].checked;
            }
            $scope.selectedEkstensionInsurance = [];
        }

        $scope.removeExtension = function (index) {
            $scope.selected_data.ListInfoPerluasanJaminan.splice(index, 1);
        }

        $scope.checkIsPengganti = function () {

            $scope.IsPengganti = $scope.IsPengganti == true ? false : true;
            $scope.selected_data.IsPengganti = $scope.IsPengganti;
            $scope.selected_data.NoSPKPengganti = "";

            console.log("IsGanti",$scope.selected_data.IsPengganti);
        }

        $scope.searchSPKpengganti = function() {

            CreateSpkFactory.searchSPKpengganti($scope.user.EmployeeId,$scope.selected_data.NoSPKPengganti).then(function (res) {

                console.log("tes",res);
                if(res.data == true) {
                    bsNotify.show({
                        title: "Success",
                        content: "No. SPK Pengganti ("+$scope.selected_data.NoSPKPengganti+") Ditemukan",
                        type: 'success'
                    });
                }
                else {
                    bsNotify.show({
                        title: "Warning",
                        content: "No. SPK Pengganti ("+$scope.selected_data.NoSPKPengganti+") Tidak Ditemukan",
                        type: 'warning'
                    });

                    $scope.selected_data.NoSPKPengganti = "";
                }
            });
        }
        
        ///////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////
        ///// End SPK
        ////////////////////////////////////////////////////////////////////////////


    });

angular.module('signature', []);

angular.module('signature').directive('signaturePadrosa', ['$interval', '$timeout', '$window',
    function ($interval, $timeout, $window) {
        'use strict';

        var signaturePad, element, EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';

        return {
            restrict: 'EA',
            replace: true,
            template: '<div class="signature" style="width: 100%; max-width:{{width}}px; height: 100%; max-height:{{height}}px;"><canvas style="display: block; margin: 0 auto;width: 100%; max-width:{{width}}px; height: 200px; max-height:200px" ng-mouseup="onMouseup()" ng-mousedown="notifyDrawing({ drawing: true })"></canvas></div>',
            scope: {
                accept: '=?',
                clear: '=?',
                dataurl: '=?',
                height: '@',
                width: '@',
                //simpan: '@',
                notifyDrawing: '&onDrawing',
            },
            controller: [
                '$scope',
                function ($scope) {
                    $scope.accept = function () {

                        $rootScope.$emit("SPKSignOK", {});
                        return {
                            isEmpty: $scope.dataurl === EMPTY_IMAGE,
                            dataUrl: $scope.dataurl
                        };

                        //$scope.simpan();

                    };

                    $scope.onMouseup = function () {
                        $scope.updateModel();

                        // notify that drawing has ended
                        $scope.notifyDrawing({ drawing: false });
                    };

                    $scope.updateModel = function () {
                        /*
                         defer handling mouseup event until $scope.signaturePad handles
                         first the same event
                         */
                        $timeout().then(function () {
                            $scope.dataurl = $scope.signaturePad.isEmpty() ? EMPTY_IMAGE : $scope.signaturePad.toDataURL();
                        });
                    };

                    $scope.clear = function () {
                        $scope.signaturePad.clear();
                        $scope.dataurl = EMPTY_IMAGE;
                    };

                    $scope.$watch("dataurl", function (dataUrl) {
                        if (!dataUrl || $scope.signaturePad.toDataURL() === dataUrl) {
                            return;
                        }

                        $scope.setDataUrl(dataUrl);
                    });
                }
            ],
            link: function (scope, element, attrs) {
                var canvas = element.find('canvas')[0];
                var parent = canvas.parentElement;
                var scale = 0;
                var ctx = canvas.getContext('2d');

                var width = parseInt(scope.width, 10);
                var height = parseInt(scope.height, 10);

                canvas.width = width;
                canvas.height = height;

                scope.signaturePad = new SignaturePad(canvas);

                scope.setDataUrl = function (dataUrl) {
                    ctx.setTransform(1, 0, 0, 1, 0, 0);
                    ctx.scale(1, 1);

                    scope.signaturePad.clear();
                    scope.signaturePad.fromDataURL(dataUrl);

                    $timeout().then(function () {
                        ctx.setTransform(1, 0, 0, 1, 0, 0);
                        ctx.scale(1 / scale, 1 / scale);
                    });
                };

                var calculateScale = function () {
                    var scaleWidth = Math.min(parent.clientWidth / width, 1);
                    var scaleHeight = Math.min(parent.clientHeight / height, 1);

                    var newScale = Math.min(scaleWidth, scaleHeight);

                    if (newScale === scale) {
                        return;
                    }

                    var newWidth = width * newScale;
                    var newHeight = height * newScale;
                    canvas.style.height = Math.round(newHeight) + "px";
                    canvas.style.width = Math.round(newWidth) + "px";

                    scale = newScale;
                    ctx.setTransform(1, 0, 0, 1, 0, 0);
                    ctx.scale(1 / scale, 1 / scale);
                };

                var resizeIH = $interval(calculateScale, 200);
                scope.$on('$destroy', function () {
                    $interval.cancel(resizeIH);
                    resizeIH = null;
                });

                angular.element($window).bind('resize', calculateScale);
                scope.$on('$destroy', function () {
                    angular.element($window).unbind('resize', calculateScale);
                });

                calculateScale();

                element.on('touchstart', onTouchstart);
                element.on('touchend', onTouchend);

                function onTouchstart() {
                    scope.$apply(function () {
                        // notify that drawing has started
                        scope.notifyDrawing({ drawing: true });
                    });
                }

                function onTouchend() {
                    scope.$apply(function () {
                        // updateModel
                        scope.updateModel();

                        // notify that drawing has ended
                        scope.notifyDrawing({ drawing: false });
                    });
                }
            }
        };
    }
]);

// Backward compatibility
angular.module('ngSignaturePad', ['signature']);