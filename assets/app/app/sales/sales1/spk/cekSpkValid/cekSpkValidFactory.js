angular.module('app')
    .factory('CekSpkValidFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

        return {
            getDataSales: function() {
                var res = $http.get('/api/sales/MProfileEmployee/SalesmanAll');
                return res;
            },
            getData: function(filter) {
                filter.dateFrom = fixDate(filter.dateFrom);
                filter.dateTo = fixDate(filter.dateTo);
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/SPKCekSPKValid/Filter?start=1&limit=100&' + param);
                return res;
            },
            getDokumen: function(filter) {
                var res = $http.get('/api/sales/PCategoryDocument');
                return res;
            },
            getDokumenSpk: function(Spkid) {
                var res = $http.get('/api/sales/SpkInfoDocument/?SpkId=' + Spkid);
                return res;
            },
            create: function(SpkTaking) {
                console.log("test", SpkTaking);
                return $http.post('/api/sales/SPK/GenerateSOPR', SpkTaking);
            },
            update: function(SpkTaking) {
                return $http.put('/api/sales/SAHSPKLengkap', [{
                    SpkId: SpkTaking.SpkId,
                    DocumentCompletedBit: SpkTaking.DocumentCompletedBit
                }]);
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd