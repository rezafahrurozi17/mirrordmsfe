angular.module('app')
    .controller('CekSpkValidController', function ($rootScope, $stateParams, $scope, $http, CurrentUser, bsNotify, CekSpkValidFactory, CreateSpkFactory, $timeout, MobileHandling, uiGridConstants) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ValidSpkToGenerateSo = false;

        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.status = false;

            $scope.bv = MobileHandling.browserVer();
            $scope.bver = MobileHandling.getBrowserVer();
            if ($scope.bv !== "Chrome") {
                if ($scope.bv == "Unknown") {
                    if (!alert("Menu ini tidak bisa dibuka pada perangkat Mobile.")) {
                        $scope.removeTab($scope.tab.sref);
                    }
                }
            }

        });
        //----------------------------------
        // Initialization
        //----------------------------------

        //PENJAGAAN BUTTON KEMBALI
        



        //$scope.cekDokumen = false;
        $scope.rightEnableByte = null; 
        $scope.cekValidSpk = true;
        $scope.selectedRows = [];
        $scope.tempAccEmpty = [];
        //IfGotProblemWithModal();
        $scope.user = CurrentUser.user();
        $scope.tab = $stateParams.tab;
        $scope.mcekSpkValid = null; //Model
        $scope.mValidDokumen = {};
        $scope.selctedRow = [];
        $scope.dokumen = [];
        $scope.SpkId = null;

        var mount = new Date();
        var year = mount.getFullYear();
        var mounth = mount.getMonth();
        var lastdate = new Date(year, mounth + 1, 0).getDate();

        $scope.firstDate = new Date(mount.setDate("01"));
        $scope.lastDate = new Date(mount.setDate(lastdate));

        $scope.filter = { dateFrom: $scope.firstDate, dateTo: new Date(), salesId: null };

        console.log("tanggalan", $scope.filter);

        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.dateOptionLastDate = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalConfirmation').remove();
		  angular.element('.ui.modal.buatSOManual').remove();
		  angular.element('.ui.modal.dokumenSPKViewvalid').remove();
		});

        $scope.tglpengajuan ={startingDay: 1,
            format: "dd/MM/yyyy"};

        $scope.dateOptionLastDate.minDate = $scope.firstDate;

        $scope.tanggalmin = function (dt) {
            $scope.dateOptionLastDate.minDate = dt;
            if (dt == undefined || dt == null) {
                $scope.filter.dateTo = null;
            } else {
                if ($scope.filter.dateFrom < $scope.filter.dateTo) {

                } else {
                    $scope.filter.dateTo = $scope.filter.dateFrom;
                }
            }

        }


        CekSpkValidFactory.getDataSales().then(function (res) {
            $scope.sales = res.data.Result;
            return res.data;
        })

        CekSpkValidFactory.getDokumen().then(function (res) {
            $scope.dokumen = res.data.Result;
        })
        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
            // === custom ====
            $scope.RightsEnableForMenu.allowUnmatch = getMod(u, 12);
            $scope.RightsEnableForMenu.allowSuggestmatch = getMod(u, 13);
            $scope.RightsEnableForMenu.allowMatch = getMod(u, 14);
            $scope.RightsEnableForMenu.allowRetur = getMod(u, 15);
            $scope.RightsEnableForMenu.allowSO = getMod(u, 16);
            $scope.RightsEnableForMenu.allowGenerateSO = getMod(u, 17);
            $scope.RightsEnableForMenu.allowCekDokumen = getMod(u, 18);
        }

        $timeout(function () {
            console.log("rightEnableByte", $scope.rightEnableByte);
            checkBytes($scope.rightEnableByte);
            console.log("RIGHST =====>", $scope.RightsEnableForMenu);
        }, 1000)

        //----------------------------------
        // Get Data
        //----------------------------------
       
        // var gridData = [];
        $scope.getData = function () {
            console.log("filter", $scope.filter);
            if ($scope.filter.dateFrom == null || $scope.filter.dateFrom == undefined) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Isi range tanggal.",
                    type: 'warning'
                });
            } else if ($scope.filter.dateFrom != null && $scope.filter.dateTo == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Pastikan range tanggal telah terisi dengan benar",
                    type: 'warning'
                });
            }
            // else if (
            //     ($scope.filter.dateFrom != null && $scope.filter.dateTo != null && $scope.filter.salesId == null) ||
            //     ($scope.filter.dateFrom == null && $scope.filter.dateTo == null && $scope.filter.salesId == null)) {
            //     bsNotify.show({
            //         title: "Peringatan",
            //         content: "Harap pilih nama sales",
            //         type: 'warning'
            //     });
            // } 
            else {
                $scope.loading = true;
                CekSpkValidFactory.getData($scope.filter)
                    .then(
                        function (res) {
                            console.log($scope.filter);
                            $scope.grid.data = res.data.Result;
                            $scope.loading = false;
                            $scope.selectedRows = [];
                            //$scope.filter={dateForm:null, dateTo:null, salesId:null};
                        },
                        function (err) {
                            console.log("err=>", err);
                        }
                    );
            }
        };

        $scope.getData();

        $scope.backfromDokument = function () {
            $scope.dokumentcekValidSpk = false;
            $scope.cekValidSpk = true;
        }

        $scope.keluarImg = function () {
            $scope.status = false;
            angular.element('.ui.modal.dokumenSPKViewvalid').modal('hide');
        }

        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
            $scope.selectedRows = rows;
            $scope.ValidSpkToGenerateSo = false;
        };

        $scope.savingStateSO = false;
        $scope.savingStateSOPopup = false;

        $scope.generateSO = function () {
            $scope.ValidSpkToGenerateSo = false;
            $scope.savingStateSO = true;
            console.log("generate SO", $scope.selectedRows[0]);
            if ($scope.selectedRows[0].CategoryFleetName == "Fleet") {
                
                // if ($scope.selectedRows[0].fDuplikasiPelanggan == true) {
                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         timeout: 4000,
                //         title: "Duplikasi Pelanggan",
                //     });
                // }
                if ($scope.selectedRows[0].StatusApprovalDiscountName != "Disetujui" ||
                    $scope.selectedRows[0].StatusApprovalDiscountName == "Diajukan" ||
                    $scope.selectedRows[0].StatusApprovalDiscountName == "Ditolak") {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Discount belum disetujui",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].StatusApprovalDiscountName == "Disetujui" || $scope.selectedRows[0].StatusApprovalDiscountName == null) {
                    CekSpkValidFactory.create($scope.selectedRows).then(function (res) {
                        bsNotify.show({
                            title: "Berhasil",
                            content: "SO dan PR berhasil dibuat.",
                            type: 'success'
                        });
                    });
                    $scope.savingStateSO = false;
                }
            } else if ($scope.selectedRows[0].CategoryFleetName != "Fleet") {
                if (($scope.selectedRows[0].StatusApprovalDiscountName != "Disetujui" && $scope.selectedRows[0].StatusApprovalDiscountName != null) ||
                    $scope.selectedRows[0].StatusApprovalDiscountName == "Diajukan" ||
                    $scope.selectedRows[0].StatusApprovalDiscountName == "Ditolak") {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Discount belum disetujui.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].BookingFeeLunasBit == false || $scope.selectedRows[0].BookingFeeLunasBit == null) {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Bookingfee belum lunas.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].DocumentCompletedBit == false || $scope.selectedRows[0].DocumentCompletedBit == null) {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Dokumen belum lengkap.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }

                if (($scope.selectedRows[0].StatusApprovalDiscountName == "Disetujui" ||
                    $scope.selectedRows[0].StatusApprovalDiscountName == null) &&
                    $scope.selectedRows[0].BookingFeeLunasBit == true &&
                    $scope.selectedRows[0].DocumentCompletedBit == true) {
                    $scope.tempAccs = [{ OutletId: null, SpkId: null, SpkDetailInfoUnitId: null, VehicleTypeColorId: null, AccesoriesId: null, AccessoriesPackageId: null, StatusAccId: null, Qty: null, InstallationLocation: null, Description: null, AccessoriesName: null, AccessoriesCode: null, ColorName: null }];
                    $scope.tempAccs.splice(0, 1);
                    for (var i in $scope.selectedRows[0].ListUnit) {
                        if ($scope.selectedRows[0].ListUnit[i].ListAccessories.length == 0) {
                            $scope.tempAccs.push({
                                OutletId: $scope.selectedRows[0].ListUnit[i].OutletId,
                                SpkId: $scope.selectedRows[0].ListUnit[i].SpkId,
                                SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].SpkDetailInfoUnitId,
                                VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].VehicleTypeColorId,
                                AccessoriesId: null,
                                AccessoriesPackageId: null,
                                StatusAccId: null,
                                Qty: 0,
                                InstallationLocation: "PDS"
                            });
                        } else if ($scope.selectedRows[0].ListUnit[i].ListAccessories.length > 0) {
                            for (var j in $scope.selectedRows[0].ListUnit[i].ListAccessories) {
                                $scope.tempAccs.push({
                                    OutletId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].OutletId,
                                    SpkId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].SpkId,
                                    SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].SpkDetailInfoUnitId,
                                    VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].VehicleTypeColorId,
                                    AccessoriesId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesId,
                                    AccessoriesPackageId: null,
                                    StatusAccId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].StatusAccId,
                                    Qty: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].Qty,
                                    InstallationLocation: "PDS",
                                    Description: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].Description,
                                    AccessoriesName: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesName,
                                    AccessoriesCode: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesCode,
                                    ColorName: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].ColorName

                                })
                            }
                        }

                        if ($scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage.length == 0) {
                            $scope.tempAccs.push({
                                OutletId: $scope.selectedRows[0].ListUnit[i].OutletId,
                                SpkId: $scope.selectedRows[0].ListUnit[i].SpkId,
                                SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].SpkDetailInfoUnitId,
                                VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].VehicleTypeColorId,
                                AccessoriesPackageId: null,
                                AccessoriesId: null,
                                StatusAccId: null,
                                Qty: 0,
                                InstallationLocation: "PDS"

                            });
                        } else if ($scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage.length > 0) {
                            for (var k in $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage) {
                                for (var l in $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail) {
                                    $scope.tempAccs.push({
                                        OutletId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].OutletId,
                                        SpkId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].SpkId,
                                        SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].SpkDetailInfoUnitId,
                                        VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].VehicleTypeColorId,
                                        AccessoriesId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail[l].AccessoriesId,
                                        AccessoriesPackageId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].AccessoriesPackageId,
                                        StatusAccId: null,
                                        Qty: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].Qty,
                                        InstallationLocation: "PDS",
                                        Description: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].Description,
                                        AccessoriesName: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail[l].AccessoriesName,
                                        AccessoriesCode: "",
                                        ColorName: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ColorName,
                                    });
                                }
                            }
                        }

                    }

                    $scope.selectedRows[0].ListAccessories = $scope.tempAccs;

                    CekSpkValidFactory.create($scope.selectedRows).then(
                        function (res) {
                            bsNotify.show({
                                title: "Berhasil",
                                content: "SO dan PR berhasil dibuat",
                                type: 'success'
                            });
                           
                            $scope.selectedRows = [];
                            $scope.getData();
                            $scope.ValidSpkToGenerateSo = false;
                            $scope.savingStateSO = false;
                        },
                        function (err) {
                            var errMsg = "";
                            if(err.data.Message.split('#').length > 1){
                                errMsg = err.data.Message.split('#')[1];
                                errMsg = errMsg.split('Error')[0];
                            }else{
                                errMsg = err.data.Message;
                            }
                            bsNotify.show({
                                title: "Gagal",
                                content: errMsg,
                                type: 'danger'
                            });
                           
                            $scope.ValidSpkToGenerateSo = false;
                            $scope.savingStateSO = false;
                        }
                    );
                }

            }
        };


        $scope.generatePoManual = function () {
            $scope.ValidSpkToGenerateSo = false;
            $scope.savingStateSO = true;
            if ($scope.selectedRows[0].CategoryFleetName == "Fleet") {
                if ($scope.selectedRows[0].StatusApprovalDiscountName != "Disetujui") {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Discount belum disetujui.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].StatusApprovalDiscountName == "Disetujui" || $scope.selectedRows[0].StatusApprovalDiscountName == null) {
                    CekSpkValidFactory.create($scope.selectedRows).then(function (res) {
                        bsNotify.show({
                            title: "Berhasil",
                            content: "SO dan PR berhasil dibuat.",
                            type: 'success'
                        });
                        $scope.savingStateSO = false;
                    });
                };
            } else if ($scope.selectedRows[0].CategoryFleetName != "Fleet") {
                if ($scope.selectedRows[0].StatusApprovalDiscountName != "Disetujui" && $scope.selectedRows[0].StatusApprovalDiscountName != null) {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Discount belum disetujui.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].BookingFeeLunasBit == false || $scope.selectedRows[0].BookingFeeLunasBit == null) {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Bookingfee belum lunas.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }
                if ($scope.selectedRows[0].DocumentCompletedBit == false || $scope.selectedRows[0].DocumentCompletedBit == null) {
                    bsNotify.show({
                        title: "Gagal",
                        content: "Dokumen belum lengkap.",
                        type: 'danger'
                    });
                    $scope.savingStateSO = false;
                }

                if (($scope.selectedRows[0].StatusApprovalDiscountName == "Disetujui" || $scope.selectedRows[0].StatusApprovalDiscountName == null) &&
                    $scope.selectedRows[0].BookingFeeLunasBit == true &&
                    $scope.selectedRows[0].DocumentCompletedBit == true) {
                    $scope.tempAccs = [{
                        OutletId: null,
                        SpkId: null,
                        SpkDetailInfoUnitId: null,
                        VehicleTypeColorId: null,
                        AccesoriesId: null,
                        StatusAccId: null,
                        Qty: null,
                        InstallationLocation: null,
                        Description: null,
                        AccessoriesName: null,
                        AccessoriesCode: null,
                        ColorName: null
                    }];
                    $scope.tempAccs.splice(0, 1);
                    console.log("ssadasd", $scope.selectedRows[0].ListUnit);

                    for (var i in $scope.selectedRows[0].ListUnit) {
                        if ($scope.selectedRows[0].ListUnit[i].ListAccessories.length > 0) {
                            for (var j in $scope.selectedRows[0].ListUnit[i].ListAccessories) {
                                $scope.tempAccs.push({
                                    OutletId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].OutletId,
                                    SpkId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].SpkId,
                                    SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].SpkDetailInfoUnitId,
                                    VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].VehicleTypeColorId,
                                    AccessoriesId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesId,
                                    StatusAccId: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].StatusAccId,
                                    Qty: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].Qty,
									Classification: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].Classification,
                                    InstallationLocation: "PDS",
                                    Description: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].Description,
                                    AccessoriesName: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesName,
                                    AccessoriesCode: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].AccessoriesCode,
                                    ColorName: $scope.selectedRows[0].ListUnit[i].ListAccessories[j].ColorName

                                })
                            }
                        }

                        if ($scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage.length > 0) {
                            for (var k in $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage) {
                                for (var l in $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail) {
                                    $scope.tempAccs.push({
                                        OutletId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].OutletId,
                                        SpkId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].SpkId,
                                        SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].SpkDetailInfoUnitId,
                                        VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].VehicleTypeColorId,
                                        AccessoriesId: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail[l].AccessoriesId,
                                        StatusAccId: null,
                                        Qty: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].Qty,
                                        InstallationLocation: "PDS",
                                        Description: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].Description,
                                        AccessoriesName: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ListDetail[l].AccessoriesName,
                                        AccessoriesCode: "",
                                        ColorName: $scope.selectedRows[0].ListUnit[i].ListAccessoriesPackage[k].ColorName,

                                    })
                                }
                            }
                        }


                    }
                    angular.element('.ui.modal.buatSOManual').modal('show');
					angular.element('.ui.modal.buatSOManual').not(':first').remove(); 
                    $scope.gridAksesorisSO.data = $scope.tempAccs;
                    console.log("ssadasd", $scope.tempAccs);
                }
            }
        };


        $scope.soOk = function () {
            $scope.savingStateSOPopup = true;
            $scope.selectedRows[0].ListAccessories = [];
            $scope.tempAccEmpty = [];
            $scope.tempSelectedRow = angular.copy($scope.selctedRow);

            for (var i in $scope.selectedRows[0].ListUnit) {
                $scope.tempAccEmpty.push({
                    OutletId: $scope.selectedRows[0].OutletId,
                    SpkId: $scope.selectedRows[0].SpkId,
                    SpkDetailInfoUnitId: $scope.selectedRows[0].ListUnit[i].SpkDetailInfoUnitId,
                    VehicleTypeColorId: $scope.selectedRows[0].ListUnit[i].VehicleTypeColorId,
                    AccessoriesId: null,
                    StatusAccId: null,
                    Qty: 0,
                    InstallationLocation: null,
                })
            }

            if ($scope.selctedRow.length > 0) {
                for (var i in $scope.tempAccEmpty) {
                    var ctrl = 0;
                    var ctrl2 = 0;
                    for (var j in $scope.selctedRow) {
                        ctrl2++;
                        if ($scope.tempAccEmpty[i].SpkDetailInfoUnitId == $scope.selctedRow[j].SpkDetailInfoUnitId) {
                            ctrl++;
                            break;
                        }
                    };
                    if (ctrl == 0 && ctrl2 == $scope.selctedRow.length) {
                        $scope.tempSelectedRow.push($scope.tempAccEmpty[i]);
                    }
                };
                $scope.selectedRows[0].ListAccessories = $scope.tempSelectedRow;
            } else {
                $scope.selectedRows[0].ListAccessories = $scope.tempAccEmpty;
            }

            //console.log("------>",$scope.selectedRows);

            CekSpkValidFactory.create($scope.selectedRows).then(function (res) {
                angular.element('.ui.modal.buatSOManual').modal('hide');
                if ($scope.selctedRow.length > 0) {
                    bsNotify.show({
                        title: "Berhasil",
                        content: "SO dan PR berhasil dibuat.",
                        type: 'success'
                    });
                   
                    $scope.savingStateSO = false;
                    $scope.savingStateSOPopup = false;
                } else {
                    bsNotify.show({
                        title: "Berhasil",
                        content: "SO berhasil dibuat.",
                        type: 'success'
                    });
                   
                    $scope.savingStateSO = false;
                    $scope.savingStateSOPopup = false;
                }

                $scope.selctedRow = [];
                $scope.selectedRows = [];
                $scope.getData();
                $scope.ValidSpkToGenerateSo = false;
                $scope.savingStateSO = false;
                $scope.savingStateSOPopup = false;
            });
        }

        $scope.aproved = function () {
            angular.element('.ui.modal.ModalConfirmation').modal({
                onHide: function () {
                    console.log('hidden');
                }
            }).modal('show');
        };

        $scope.onHide = function () {
            angular.element('.ui.modal.ModalConfirmation').modal('hide');
        }

        $scope.keluarSOPO = function () {
            $scope.selctedRow = [];
            $scope.selectedRows[0].ListAccessories = [];
            angular.element(".ui.modal.buatSOManual").modal("hide");
            console.log("udah keluar", $scope.selectedRows[0]);
           
            $scope.savingStateSO = false;
        }

        $scope.backfromMaintain = function () {
            $scope.cekValidSpk = true;
        }


        $rootScope.$on("KembaliMintainSPKdariValid", function () {
            $scope.backfromMaintain();
        });

        $scope.CekDokumen = function (spkid, nospk, obj, comletebit) {
            $scope.gridDocument.data = [];
            $scope.tempdocs = [];
            $scope.tempdocskosong = [];

            for (var i in $scope.dokumen) {
                $scope.dokumen[i].StatusDocumentName = "Empty";
                for (var j in obj) {
                    if ($scope.dokumen[i].DocumentId == obj[j].DocumentId) {
                        $scope.dokumen.splice(i, 1);
                        obj[j].StatusDocumentName = "Upload";
                        $scope.dokumen.push(obj[j]);
                    }
                }
            }

            for (var i in $scope.dokumen) {
                if ($scope.dokumen[i].DocumentURL == null || typeof $scope.dokumen[i].DocumentURL == undefined || $scope.dokumen[i].DocumentURL == "") {
                    $scope.dokumen[i].StatusDocumentName = "Empty";
                }
            }

            $scope.gridDocument.data = $scope.dokumen;
            $scope.mValidDokumen.DocumentCompletedBit = comletebit;
            $scope.nospk = nospk;
            $scope.SpkId = spkid;
            $scope.dokumentcekValidSpk = true;
            $scope.cekValidSpk = false;
        }

        $scope.updatedokument = function () {

            if ($scope.mValidDokumen.DocumentCompletedBit == false) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Checkbox kelengkapan dokumen belum diisi.",
                    type: 'warning'
                });
            } else {
                $scope.mValidDokumen.SpkId = $scope.SpkId;
                CekSpkValidFactory.update($scope.mValidDokumen).then(function (res) {
                    $scope.dokumentcekValidSpk = false;
                    $scope.cekValidSpk = true;
                    $scope.getData();
                })

            }

        }

        $scope.CekDokumenalrt = function (row) {
            $scope.dataStream2 = null;

            CekSpkValidFactory.getImage(row.DocumentURL).then(function (res) {
                $scope.dataStream2 = res.data.UpDocObj;
                $scope.dataName = res.data.FileName;
                setTimeout(function () {
                    angular.element(".ui.modal.dokumenSPKViewvalid").modal('refresh');
                }, 0);
                angular.element(".ui.modal.dokumenSPKViewvalid").modal("Refresh").modal("show");
				angular.element('.ui.modal.dokumenSPKViewvalid').not(':first').remove(); 
            });

            //$scope.dataStream2 = row.DocumentData;

            // $scope.TheFileTipeToDownload = $scope.dataStream2.split(';')[0];

            // var blob2 = dataURItoBlob($scope.dataStream2);

            // var blob = new Blob([blob2], { type: $scope.TheFileTipeToDownload.substring(5)});
            // ////change download.pdf to the name of whatever you want your file to be
            // saveAs(blob, "download.png");
        }

        $scope.breadcrums = {};
        $scope.NoSPK = function (SpkId) {
            //$rootScope.breadcrums.title = "Lihat";
            $rootScope.SpkId = SpkId;
            $rootScope.tabSPKValid = angular.copy($stateParams.tab);
            //console.log("tesrifg",angular.copy($scope.tab));
            $rootScope.$emit("MintainSPKdariValid", {});
            $scope.lihatSPK = true;
            setDefaultTab();
            $scope.cekValidSpk = false;
        }

        $scope.lokasiPemasangan = [{ name: "PDS" }, { name: "PDC" }];

        $scope.aprovedDiscount = function (SpkId) {
            CreateSpkFactory.getApprovalDiscount(SpkId).then(function (res) {
                $scope.listDicount = res.data.Result[0];
                $scope.gridStatusDiscount.data = $scope.listDicount.ListOfSAHApprovalDiscountSPKView;
                angular.element(".ui.modal.ModalConfirmation").modal("show");
				angular.element('.ui.modal.ModalConfirmation').not(':first').remove();
            });
        }

        var btnActionCekValid2 = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px"  ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowNew" ng-click="grid.appScope.$parent.CekDokumen(row.entity.SpkId, row.entity.FormSPKNo, row.entity.ListInfoDocument, row.entity.DocumentCompletedBit)"> \
                                        <u>Cek Dokumen</u> \
                                    </p> \
                                  </a>';
        var btnActionCekValid = '<a ng-if="row.entity.StatusDocumentName!=\'Empty\'" style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.CekDokumenalrt(row.entity)"> \
                                        <u>Lihat Dokumen</u> \
                                    </p> \
                                  </a>';
        var noSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.NoSPK(row.entity.SpkId)">{{row.entity.FormSPKNo}}</u></p></span>';

        var statusSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px" > \
                                    <u ng-click="grid.appScope.$parent.aprovedDiscount(row.entity.SpkId)">{{row.entity.StatusApprovalDiscountName}}</u> \
                                </span>';

        var bookingfee = '<p style="font-size:16px;margin:5px 0 0 0" align="center"> \
                            <i ng-if="row.entity.BookingFeeLunasBit==true" class="fa fa-check-square-o"></i> \
                            <i ng-if="row.entity.BookingFeeLunasBit==false" class="fa fa-square-o"></i> \
                          </p>';
        var lengkapdocument = '<p style="font-size:16px;margin:5px 0 0 0" align="center"> \
                          <i ng-if="row.entity.DocumentCompletedBit==true" class="fa fa-check-square-o"></i> \
                          <i ng-if="row.entity.DocumentCompletedBit==false" class="fa fa-square-o"></i> \
                        </p>';
        var pelwil = '<p style="font-size:16px;margin:5px 0 0 0" align="center"> \
                            <i ng-if="row.entity.PelanggaranWilayah == true" class="fa fa-check-square-o"></i> \
                            <i ng-if="row.entity.PelanggaranWilayah == false" class="fa fa-square-o"></i> \
                          </p>';

        var listCheckedDocument = '<p style="font-size:16px;margin:5px 0 0 0" align="center"> \
                                        <i ng-if="row.entity.StatusDocumentName == \'Upload\'" class="fa fa-check-square-o"></i> \
                                        <i ng-if="row.entity.StatusDocumentName == \'Empty\'" class="fa fa-square-o"></i> \
                                    </p>'

        var vehicleUnit = '<p style="font-size:14px;margin:5px 0 0 5px"> \
                                        {{row.entity.Description}} {{row.entity.KatashikiCode}}-{{row.entity.SuffixCode}} \
                                    </p>'

        var acessories = '<p style="font-size:14px;margin:5px 0 0 5px"> \
                                        {{row.entity.AccessoriesCode}} - {{row.entity.AccessoriesName}} \
                                    </p>'
        var pemasanganAccesories = '<select ng-change="tsts()" ng-model="modelfilter.selectedFilter" class="" ng-options="item.name as item.name for item in lokasiPemasangan" placeholder="Filter"> </select>'
        //----------------------------------
        // Grid Setup
        //----------------------------------

        $scope.gridAksesorisSO = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            multiSelect: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 15,
            //data: 'dataInfokendaraan',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                {
                    name: 'type model',
                    field: 'Description',
                    enableCellEdit: false,
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>',
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                },
                { name: 'aksesoris / karoseri', width: '30%', enableCellEdit: false, cellTemplate: acessories },
                { name: 'Jenis',field: 'Classification', cellTemplate: '<p style="font-size:14px;margin:5px 0 0 5px"><label ng-show="row.treeLevel !=0">{{row.entity.Classification}}</label></p>' },
				{
                    name: 'pemasangan aksesoris pds',
                    width: '15%',
                    field: 'InstallationLocation',
                    editType: 'dropdown',
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.lokasiPemasangan,
                    editDropdownIdLabel: 'name',
                    editDropdownValueLabel: 'name'
                }
            ]
        };


        $scope.gridDocument = {
            enableSorting: true,
			enableColumnResizing: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: ' ', cellTemplate: listCheckedDocument, width: 30 },
                {
                    name: 'id',
                    field: 'DocumentId',
                    visible: false,
                    sort: { priority: 0, direction: 'asc' }
                },
                { name: 'dokumen', field: 'DocumentName', width: '40%' },
                { name: 'tipe dokumen', field: 'DocumentType' },
                { name: 'status tipe dokumen', field: 'StatusDocumentName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '10%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionCekValid
                }

            ]
        };


        $scope.gridStatusDiscount = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: 'approver', field: 'ApproverName' },
                { name: 'jabatan', field: 'ApproverRoleName' },
                { name: 'tanggal approver', field: 'ApprovalDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'status discount', field: 'StatusApprovalName' },
                // { name: 'discount reject', field:'DiscountReject' },
                { name: 'keterangan', field: 'RequestNote' },
            ]
        };

        // name: 'No GR',
        //         allowCellFocus: false,
        //         width: '15%',
        //         enableColumnMenu: false,
        //         enableSorting: false,
		// 		field:'GRCode',
        //         enableColumnResizing: true,

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnMenus: true,
            enableSelectAll: false,
            //enableVerticalScrollbar : 2,
            //enableHorizontalScrollbar : 2,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'id', field: 'Id', visible: false },
                { name: 'No SPK', displayName: 'No SPK', field:'FormSPKNo', cellTemplate: noSpkClicked, width: '12%' },
                { name: 'Tanggal SPK', displayName: 'Tanggal SPK', width: '8%', field: 'SpkDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'nama sales', width: '15%', field: 'SalesName' },
                { name: 'Nama Prospek/Pelanggan', width: '15%', field: 'ProspectName' },
                { name: 'Incoming Booking Fee', width: '11%', field: 'TotalAmountBF', cellFilter: 'currency:"Rp."'},                
                { name: 'booking fee lunas', width: '7%', cellTemplate: bookingfee },
                { name: 'status Diskon', width: '8%', cellTemplate: statusSpkClicked,},
                { name: 'Dokumen Lengkap', width: '7%', cellTemplate: lengkapdocument },
                { name: 'pelwil', width: '7%', cellTemplate: pelwil },
				{ name: 'Status Off the Road', width: '7%', field: 'StatusApprovalOffTheRoadName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '8%',
                    pinnedRight: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionCekValid2
                }
            ],
            // onRegisterApi = function(gridApi) {
            //     //set gridApi on scope
            //     var gridApi = gridApi;
            //     $interval(function(){
            //         gridApi.core.handleWindowResize();
            //     },200);
            // }

        };
		//{ name: 'status Diskon', width: '8%', cellTemplate: statusSpkClicked },

        $scope.grid.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            $interval(function () {
                self.gridApi.core.handleWindowResize();
            }, 150, 5);
        }

        $scope.gridAksesorisSO.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                var msg = 'row selected ' + row.isSelected;
                $scope.selctedRow = gridApi.selection.getSelectedRows();
                console.log("slectrowsaaa", $scope.selctedRow);
            });
           

        };

        function setDefaultTab() {
            angular.element("#infoPelanggan1_tab").addClass("active");
            angular.element("#dokumen1_tab").removeClass("active").addClass("");
            angular.element("#infoPengiriman1_tab").removeClass("active").addClass("");
            angular.element("#infoKendaraan1_tab").removeClass("active").addClass("");
            angular.element("#aksesoris1_tab").removeClass("active").addClass("");
            angular.element("#karoseri1_tab").removeClass("active").addClass("");
            angular.element("#infoLeasing1_tab").removeClass("active").addClass("");
            angular.element("#infoAsuransi1_tab").removeClass("active").addClass("");
            angular.element("#rincianHarga1_tab").removeClass("active").addClass("");
            angular.element("#infoMediator1_tab").removeClass("active").addClass("");
            angular.element("#infoPemakai1_tab").removeClass("active").addClass("");

            angular.element("#infoPelanggan1").removeClass("tab-pane fade").addClass("tab-pane fade in active");
            angular.element("#dokumen1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPengiriman1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoKendaraan1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#aksesoris1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#karoseri1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoLeasing1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoAsuransi1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#rincianHarga1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoMediator1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPemakai1").removeClass("tab-pane fade in active").addClass("tab-pane fade");
        }


    });

app.directive('imageSetAspect', function () {
    return {
        scope: false,
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('load', function () {
                // Use jquery on 'element' to grab image and get dimensions.
                scope.$apply(function () {
                    scope.imageMeta.width = $(element).width();
                    scope.imageMeta.height = $(element).height();
                    console.log(scope.$parent.imageMeta);
                });
            });
        }
    };
});