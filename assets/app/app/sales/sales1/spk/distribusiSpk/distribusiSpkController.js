angular.module('app')
    .controller('DistribusiSpkController', function(bsNotify, $stateParams, FindStockFactory, ModelFactory, $rootScope, $scope, $http, CreateSpkFactory, CurrentUser, DistribusiSpkFactory, $timeout, ComboBoxFactory) {
        //----------------------------------
        // Start-Up
        //------------------DistribusiSpkController----------------
        $scope.$on('$viewContentLoaded', function() {
            //$scope.loading = false;
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.breadcrums = {};
        $scope.tab = $stateParams.tab;
        $scope.user = CurrentUser.user();
        $scope.Role = $scope.user.RoleName;
        console.log("user", $scope.Role);
        $scope.menusekarang = "Daftar SPK";
        // $scope.selected_data.ReceiverName = null;
        $scope.mSPK_Cancel = {};
        $scope.data = {};
        $scope.select_data ={};
        $scope.formspkdraft = false;
        $scope.listdaftar = true;
        $scope.statusAction = null;
        $scope.findStock = false;
        $scope.show = { findStock: false };
        $scope.showModelInput = false;
        $scope.showCustomerInput = true;
        $scope.disabledBtnSPKCancel = false;

        $scope.bind_data = {
            data: [],
            filter: []
        };
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.batalSpk').remove();
		  angular.element('.ui.modal.loadingBatalSPK').remove();
		});

        $scope.tinggi = window.screen.availHeight;
        $scope.lebar = window.screen.availWidth;

        $scope.gambar = {
            "max-width": $scope.lebar - 40,
            "max-height": $scope.tinggi - 40
        }
		
		function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':00:00';
                return fix;
            } else {
                return null;
            }
        };

        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        $scope.filterType = function(id) {
            if(id !== undefined){
                DistribusiSpkFactory.getDataVehicleType(id).then(function(res) {
                    $scope.vehicleType = res.data.Result;
                })   
            }
        }

        $scope.filterColor = function(id) {
            DistribusiSpkFactory.getDataVehicleTypeColors(id).then(function(res) {
                $scope.color = res.data.Result;
            })
        }

        DistribusiSpkFactory.getDataArea().then(
            function(res) {
                $scope.getDataArea = res.data.Result;
                return res.data;
            }
        );

        DistribusiSpkFactory.getDataProvince().then(
            function(res) {
                $scope.getDataProvince = res.data.Result;
                return res.data;
            }
        );
		
		$scope.DistribusiSpkIlanginSearch = function () {
			document.getElementById("DaTextFilterDistribusiSpk").value="";
			document.getElementById("DaTextFilterDistribusiSpk").value=null;
			$scope.DistribusiSpkModelSearch=null;
			$scope.bind_data.filter.VehicleModelId=null;
			document.getElementById("DaModelFilterDistribusiSpk").value="";
			document.getElementById("DaModelFilterDistribusiSpk").value=null;
			
			
		}

        $scope.findStockfunct = function() {
            $scope.listdaftar = false;
            $scope.findStock = true;
            angular.element(".ui.modal.Distribution1SPK").modal("hide");
        }

        $scope.kembalifindstock = function() {
            $scope.listdaftar = true;
            $scope.findStock = false;
        }


        function filterInitialize() {
            if ($scope.Role == 'SALES') {
                $scope.filterData = {
                    defaultFilter: [
                        { name: 'No SPK', value: 'FormSPKNo' },
                        { name: 'Status', value: 'SPKStatusName' },
                        { name: 'Nama', value: 'ProspectName' },
                        { name: 'Model', value: 'ModelType' },
						{ name: 'Kategori Pelanggan', value: 'CustomerTypeDesc' },
                    ],
                    advancedFilter: [
                        { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 1 },
                        { name: 'Status', value: 'SPKStatusName' },
						{ name: 'Nama', value: 'ProspectName' },
						{ name: 'Kategori Pelanggan', value: 'CustomerTypeDesc' },
						{ name: 'Model Kendaraan', value: 'VehicleModelId' },
						{ name: 'Tanggal Mulai SPK', value: 'SPKStartDate' },
						{ name: 'Tanggal Akhir SPK', value: 'SPKEndDate' },
                    ]
                };
            } else {
                $scope.filterData = {
                    defaultFilter: [
                        { name: 'No SPK', value: 'FormSPKNo' },
                        { name: 'Nama Sales', value: 'SalesName' },
                        { name: 'Model', value: 'VehicleTypeId' },
                        { name: 'Status', value: 'SPKStatusName' },
						{ name: 'Nama', value: 'ProspectName' },
                    ],
                    advancedFilter: [
                        { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 1 },
                        { name: 'Nama Sales', value: 'SalesName' },
                        { name: 'Status', value: 'SPKStatusName' },
						{ name: 'Nama', value: 'ProspectName' },
						{ name: 'Model Kendaraan', value: 'VehicleModelId' },
						{ name: 'Tanggal Mulai SPK', value: 'SPKStartDate' },
						{ name: 'Tanggal Akhir SPK', value: 'SPKEndDate' },
                    ]
                };
            }
        };

        filterInitialize();

        $scope.filterTenor = function (selected) {
            if (selected != null || typeof selected != "undefined") {
                ComboBoxFactory.getDataTenor(selected).then(function (res) {

                    for (var i = 0; i < res.data.Result.length; ++i) {
                        console.log('selected',selected);
                        if (res.data.Result[i].LeasingId == selected) {
                           
                            $scope.tenor = res.data.Result[i].ListLeasingTenor;
                            console.log('$scope.tenor',$scope.tenor);
                            break;
                        }
                    }


                    setTimeout(function () {
                        $scope.selected_data.LeasingLeasingTenorId = $scope.selected_data.LeasingLeasingTenorId;
                    }, 300);
                })
            }
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.btnKonfirmasi = function(){
            $('#ModalKonfirmasi').modal('show');
        }

        $scope.BatalSpk = function(data) {
            DistribusiSpkFactory.getDataSPpkCancle().then(function(res) {
                $scope.alsanBatal = res.data.Result;
            })

            console.log(data)
            $scope.select_data.FormSPKNo = data.FormSPKNo;
			setTimeout(function() {
				angular.element('.ui.modal.batalSpk').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.batalSpk').not(':first').remove();  
            }, 1);
            
            $scope.data = data;
        }
        $scope.param = {}
		
		$scope.DistribusiSpkAkalAkalan = function(selectedFilterDistribusiSpk,textFilterDistribusiSpk, flag) {
            console.log('ini yang dipilih ===>', selectedFilterDistribusiSpk, textFilterDistribusiSpk);
            // New Code =====
            console.log("$scope.filter ==>", $scope.filter, $scope.bind_data);
            $scope.bind_data.filter = {}; // for filter advancedsearch
            $scope.bind_data.textFilterDistribusiSpk = null;
            if(flag === undefined){
                $scope.param = {};

            }
            // ==============
			if ($scope.Role == 'SALES') 
			{
				if(selectedFilterDistribusiSpk=="ProspectName")
				{
					$scope.bind_data.filter.ProspectName=textFilterDistribusiSpk
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.CustomerTypeDesc=null;
                    $scope.bind_data.filter.VehicleModelId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="FormSPKNo")
				{
					$scope.bind_data.filter.ProspectName=null
					$scope.bind_data.filter.FormSPKNo=textFilterDistribusiSpk;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.CustomerTypeDesc=null;
                    $scope.bind_data.filter.VehicleModelId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="SPKStatusName")
				{
					$scope.bind_data.filter.ProspectName=null
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=textFilterDistribusiSpk;
                    $scope.bind_data.filter.CustomerTypeDesc=null;
                    $scope.bind_data.filter.VehicleModelId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="CustomerTypeDesc")
				{
					$scope.bind_data.filter.ProspectName=null
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.CustomerTypeDesc=textFilterDistribusiSpk;
                    $scope.bind_data.filter.VehicleModelId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;                
                }
                else if (selectedFilterDistribusiSpk == "ModelType" ) 
                {
                    $scope.bind_data.filter.ProspectName = null
                    $scope.bind_data.filter.FormSPKNo = null;
                    $scope.bind_data.filter.SPKStatusName = null;
                    $scope.bind_data.filter.CustomerTypeDesc = null;
                    $scope.bind_data.filter.VehicleModelId = textFilterDistribusiSpk;
                    $scope.showModelInput = true;
                    $scope.showCustomerInput = false;
                }
			}
			else
			{
						
				if(selectedFilterDistribusiSpk=="ProspectName")
				{
					$scope.bind_data.filter.ProspectName=textFilterDistribusiSpk
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.SalesName=null;
                    $scope.bind_data.filter.VehicleTypeId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="FormSPKNo")
				{
					$scope.bind_data.filter.ProspectName=null;
					$scope.bind_data.filter.FormSPKNo=textFilterDistribusiSpk;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.SalesName=null;
                    $scope.bind_data.filter.VehicleTypeId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="SPKStatusName")
				{
					$scope.bind_data.filter.ProspectName=null;
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=textFilterDistribusiSpk;
                    $scope.bind_data.filter.SalesName=null;
                    $scope.bind_data.filter.VehicleTypeId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
				}
				else if(selectedFilterDistribusiSpk=="SalesName")
				{
					$scope.bind_data.filter.ProspectName=null;
					$scope.bind_data.filter.FormSPKNo=null;
					$scope.bind_data.filter.SPKStatusName=null;
                    $scope.bind_data.filter.SalesName=textFilterDistribusiSpk;
                    $scope.bind_data.filter.VehicleTypeId = null;
                    $scope.showModelInput = false;
                    $scope.showCustomerInput = true;
                }
                else if (selectedFilterDistribusiSpk =="VehicleTypeId") {
                    $scope.bind_data.filter.ProspectName = null
                    $scope.bind_data.filter.FormSPKNo = null;
                    $scope.bind_data.filter.SPKStatusName = null;
                    $scope.bind_data.filter.CustomerTypeDesc = null;
                    $scope.bind_data.filter.VehicleTypeId = textFilterDistribusiSpk;
                    $scope.showModelInput = true;
                    $scope.showCustomerInput = false;
                }
				
			}
			
		}
		
		$scope.DistribusiSpkChangeSpkStartDate = function(setan) {
			$scope.bind_data.filter.SPKStartDate=fixDate(setan);
		}
		
		$scope.DistribusiSpkChangeSpkEndDate = function(setan) {
			$scope.bind_data.filter.SPKEndDate=fixDate(setan);
        }
        

		
		$scope.DistribusiSpkModelSearchChange = function(selected) {
			
            $scope.bind_data.filter.VehicleModelId=selected.VehicleModelId;
            console.log('ini vihecleId item yg di pilih ===>', $scope.bind_data.filter.VehicleModelId);
		}

        $scope.btnSPKCancel = function() {
            $scope.disabledBtnSPKCancel = true;
			setTimeout(function() {
				angular.element('.ui.modal.loadingBatalSPK').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.loadingBatalSPK').not(':first').remove();  
            }, 0);
            $scope.mSPK_Cancel.SpkId = $scope.data.SPKId;
            DistribusiSpkFactory.suratCancel($scope.mSPK_Cancel).then(function(res) {
                
				setTimeout(function() {
    				// angular.element('.ui.modal.loadingBatalSPK').not(':first').remove();  
					angular.element(".ui.modal.loadingBatalSPK").modal('hide');
					angular.element(".ui.modal.batalSpk").modal('hide');  
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Membutuhkan approval ke KACAB.",
                        type: 'succes'
                    });
                    $scope.disabledBtnSPKCancel = false;
                    $scope.mSPK_Cancel = {};
                    $rootScope.$emit("RefreshDirective", {});
				}, 50);
               
            })
        }

        $scope.Keluarlistapproval = function (){
            angular.element(".ui.modal.ModalApprovalDicountDaftar").modal("hide");
        }

        $scope.childmethod = function() {
            $rootScope.$emit("SPKdariDaftar", {});
        }

        $scope.lihatSpk = function(data) {
           // console.log("role", $scope.Role)
            $scope.statusAction = 'Lihat';
            DistribusiSpkFactory.getDataSales(data.SPKId).then(function(res) {
                //$scope.selected_data = res.data.Result[0];
                angular.element(".ui.modal.Distribution1SPK").modal("refresh").modal("hide");
                $scope.statusAction = 'lihat';
                $scope.formspkdraft = true;
                $scope.listdaftar = false;
                var tempData = res.data.Result[0];

                tempData.MediatorKTPNo = parseInt(tempData.MediatorKTPNo);
                for (var i in tempData.ListInfoPelanggan) {
                    tempData.ListInfoPelanggan[i].KTPNo = tempData.ListInfoPelanggan[i].KTPNo;
                }

                $scope.tempdoc = angular.copy(tempData.ListInfoDocument);

                for (var i in $scope.tempdoc) {
                    $scope.tempdoc[i].UploadDataDocument = { strBase64: null, FileName: null };
                    $scope.tempdoc[i].UploadDataDocument.strBase64 = $scope.tempdoc[i].DocumentData;
                    $scope.tempdoc[i].UploadDataDocument.FileName = $scope.tempdoc[i].DocumentDataName;
                }

                tempData.ListInfoDocument = $scope.tempdoc;
                $scope.tempRole ={ RoleName : $scope.Role};
                _.extend(tempData, $scope.tempRole );
                $rootScope.selected_data = tempData;
                $rootScope.breadcrumsTemps = "Lihat Detail";
                $rootScope.breadcrumsTab = angular.copy($scope.tab);
                $scope.childmethod();

                // console.log('tempData',tempData);
                // console.log('$scope.selected_data',$scope.selected_data);
                $scope.filterTenor(tempData.LeasingId);
                //console.log("adasd", $rootScope.selected_data);

            })
        }

        $scope.btnSPKCancelBatal = function() {
            $scope.mSPK_Cancel = {};
            angular.element('.ui.modal.batalSpk').modal('hide');
            $("#ModalKonfirmasi").modal('hide');
        }

        $scope.aproval = function() {
            angular.element(".ui.modal.ModalApproval").modal("show");
        }

        $scope.aprovalLeasing = function() {
            setTimeout(function(){angular.element(".ui.modal.ModalApprovalLeasing").modal("hide"); },0)
            angular.element(".ui.modal.ModalApprovalLeasing").modal("show");
        }

        $rootScope.$on("kembaliDaftarSPK", function() {
            $scope.kembali();
        })

        $scope.aprovalDiscountDaftar = function(data) {
            CreateSpkFactory.getApprovalDiscount(data.SPKId).then(function(res) {
                $scope.listDicount = res.data.Result[0];
                angular.element(".ui.modal.ModalApprovalDicountDaftar").modal("show");
            });

        }

        $scope.closeModal = function() {
            angular.element(".ui.modal.ModalApprovalDicountDraft").modal("hide");
        }

        $scope.kembali = function() {
            $scope.formspkdraft = false;
            $scope.listdaftar = true;
        }

        $scope.openModalthis = function(item) {
            angular.element(".ui.modal.Distribution1SPK").modal("show");
        }

        $scope.batalSpk = function() {
            $scope.modalshow = true;
            angular.element('#ModalConfirmationBuatSpk').modal();
            angular.element('#Distribution1SPK').modal('hide');
        }

        $scope.btnkeluarLeasing = function (){
            setTimeout(function(){angular.element(".ui.modal.ModalApprovalLeasing").modal("hide"); },0)
        }


        //////////////////////////////////
        //// Find Stock

        $scope.findStockfunct = function() {
            $scope.show = { findStock: true };
            $scope.listdaftar = false;
            angular.element(".ui.modal.Distribution1SPK").modal("hide");

        };

        $scope.kembalidariStock = function() {
            $scope.show = { findStock: false };
            $scope.listdaftar = true;
        }

        $scope.user = CurrentUser.user();
        $scope.xRole = { selected: [] };
        enable_advsearch: '@enableAdvancedsearch',
            $scope.filter = { VehicleModelId: null, VehicleTypeId: null, ColorId: null, AreaId: null, ProvinceId: null };

        $scope.falseToggle = function() {
            $scope.advSearch = false;
            $scope.toggle = false;
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.advancedSearch = function() {
            console.log('$scope.selectedFilterDistribusiSpk ===>',$scope.selectedFilterDistribusiSpk);
            if ($scope.filter.VehicleModelId == null || $scope.filter.AreaId == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Filter Mandatory tidak boleh kosong.",
                    type: 'warning'
                });
            } else {
                $scope.loading = true;
                FindStockFactory.getDataGroupDealer($scope.filter).then(
                    function(res) {
                        $scope.stockGroupDealer = res.data.Result;
                        $scope.Qty = $scope.stockGroupDealer.length;
                        if($scope.Qty == 0){
                            $scope.isEmpty = true;
                        }else{
                            $scope.isEmpty = false;
                        }
                        $scope.loading = false;
                        return res.data;
                    }
                );
            }
        }

        $scope.Qty = 0
        $scope.QtyOther = 0;
        $scope.loading = null;
        $scope.loadingOther = null;
       // $scope.isEmpty = true;
       // $scope.isOtherEmpty = true;

       $scope.refresh = function(){
        $scope.filterOther = {};
        $scope.filter = {};
        $scope.Qty = 0
        $scope.QtyOther = 0;
        $scope.isOtherEmpty = false;
        $scope.isEmpty = false;
        $scope.loading = null;
        $scope.loadingOther = null;
        console.log('this is refresh?');
       }

        $scope.otherDealerSearch = function() {
            if ($scope.filterOther.VehicleModelId == null || $scope.filterOther.AreaId == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Filter Mandatory tidak boleh kosong.",
                    type: 'warning'
                });
            } else {
                $scope.loadingOther = true;
                FindStockFactory.getDataOthDealer($scope.filterOther).then(
                    function(res) {
                        $scope.stockOtherDealer = res.data.Result;
                        $scope.QtyOther = $scope.stockOtherDealer.length;
                        if($scope.QtyOther == 0){
                            $scope.isOtherEmpty = true;
                        }else{
                            $scope.isOtherEmpty = false;
                        }
                        $scope.loadingOther = false;
                        return res.data;
                    }
                );
            }
        }

        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return res.data;
        });

        $scope.filterModel = function(selected) {
            console.log('ini filter model');
            $scope.Model = selected;
            console.log('$scope.Model===>', $scope.Model);
            getId = "?VehicleModelId=" + $scope.Model.VehicleModelId;
            FindStockFactory.getTipeModel(getId).then(
                function(res) {
                    $scope.listTipe = res.data.Result;
                    return res.data;
                }
            );
        }

        $scope.pilihType = function(selected) {
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected).then(
                function(res) {
                    $scope.listWarna = res.data.Result;
                })
        };

        FindStockFactory.getDataArea().then(
            function(res) {
                $scope.getDataArea = res.data.Result;
                return res.data;
            }
        );


        $scope.getProvince = function(Id) {
            FindStockFactory.getDataProvincebyArea(Id).then(
                function(res) {
                    $scope.getDataProvince = res.data.Result;
                    return res.data;
                }
            );
        }


    });