angular.module('app')
    .factory('DistribusiSpkFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        var RoleName = currentUser.RoleName;
        return {
            getData: function(param) {
                if (RoleName == "SALES") {
                    var res = $http.get('/api/sales/SPKDaftarSPKSalesWithParam' + param);
                    return res;
                } else {
                    var res = $http.get('/api/sales/SPKDaftarSPKWithParam' + param);
                    return res;
                }
            },
            getDataSales: function(param) {
                var res = $http.get('/api/sales/SAHSPKDetail?SpkId=' + param);
                return res;
            },
            getDataSPpkCancle: function() {
                var res = $http.get('/api/sales/AdminHandlingSPKCancel');
                return res;
            },
            suratCancel: function(SpkTaking) {
                return $http.put('/api/sales/SAHSPKBatal', [{
                    SpkId: SpkTaking.SpkId,
                    SpkCancelId: SpkTaking.SpkCancelId,
                    NoteCancel: SpkTaking.NoteCancel,
                }]);
            },
            getDataVehicleType: function(modelId) {
                var res = $http.get('/api/sales/MUnitVehicleType?VehicleModelId=' + modelId);
                return res;
            },
            getDataVehicleTypeColors: function(vehicletypeId) {
                var res = $http.get('/api/sales/MUnitVehicleTypeColorJoin?start=1&limit=20&filterData=VehicleTypeId|' + vehicletypeId);
                return res;
            },
            getDataArea: function() {
                var res = $http.get('/api/sales/MSitesArea');
                return res;
            },
            getDataProvince: function() {
                var res = $http.get('/api/sales/MLocationProvince');
                return res;
            }
        }
    });