angular.module('app')
    .controller('DaftarSPKController', function (MaintenanceSpkFactory, CreateSpkFactory, bsNotify, $rootScope, $scope, $http, CurrentUser, ComboBoxFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            $scope.select = 1;
            // $scope.formvalidation = {};
            // $scope.formvalidInfoIndividu = {};
            // $scope.formvalidPengiriman = {};
            // $scope.formRincianharga = {};
        });
        //----------------------------------
        // Initialization
        //----------------------------------;
        //IfGotProblemWithModal();
        $scope.$on('$destroy', function () {
            //angular.element('.ui.modal.signature').remove();      
            angular.element('.ui.modal.DraftSPK').remove();
            angular.element('.ui.modal.dokumentList').remove();
            angular.element('.ui.modal.dokumenSPKViewMobiles').remove();
            angular.element('.ui.modal.dokumenSPKViewMobilesDraft').remove();
            angular.element('.ui.modal.ModalApprovalDicountDraft').remove();
            angular.element('.ui.modal.ModalTambahAksesoris').remove();
            angular.element('.ui.modal.Karoseri').remove();
            //angular.element('.ui.modal.signature').remove();
        });

        $scope.dataselect = [{ ValueId: 1, ValueName: 'Select1' }, { ValueId: 2, ValueName: 'Select2' }, { ValueId: 3, ValueName: 'Select3' }];
        $scope.objectSelect = {};

        $scope.changeMethod = function () {
            console.log("panggil select");
        }


        $scope.user = CurrentUser.user();
        $scope.date = new Date();
        $scope.mspkTaking = null; //Modelsss
        $scope.statusAction = null;
        $scope.searchAccesoriestype = {};
        $scope.individuPembeli = false, $scope.individuPemilik = false;
        $scope.formDataPembeliPerusahaan = false;
        $scope.formDataPenanggungJawabPerusahaan = false;
        $scope.formDataPemilikPerusahaan = false;
        $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
        $scope.selected_data = {};
        $scope.selected_data.ListInfoDocument = [];
        $scope.selected_data.ListInfoPelanggan = [];
        $scope.selected_data.ListInfoPerluasanJaminan = [];
        $scope.selected_data.ListInfoUnit = [];
        $scope.selected_data.GrandTotal = 0;
        $scope.listspk = true;
        $scope.uploadFiles = [];
        $scope.signStatus = null;
        $scope.mspkTaking = null; //Model
        //$scope.menusekarang = "Daftar Draft SPK";
        $scope.statusKembali = 'kembali';
        $scope.xRole = { selected: [] };
        $scope.individuPembeli = false, $scope.individuPemilik = false;
        $scope.signature = { dataUrl: null };
        $scope.arrayJsData = [];
        $scope.jsData = { Title: null, HeaderInputs: { $TotalACC$: 0, $OutletId$: null, $VehicleTypeColorId$: null, $VehicleYear$: null } }
        $scope.Satuan = false;
        $scope.searchAccesories = {};
        $scope.simpan = [];
        $scope.IsPengganti = false;

        //$scope.recalculate = false;

        $scope.tinggi = window.screen.availHeight;
        $scope.lebar = window.screen.availWidth;

        $scope.gambar = {
            "max-width": $scope.lebar - 40,
            "max-height": $scope.tinggi - 40
        }

        //console.log("jkshkjhd", $scope.tinggi, $scope.lebar);

        // $scope.setelahrefresh = function(){
        //     console.log("AFTER REFRESH");
        // }

        $scope.tanggalDiscount = {
            format: "dd-MM-yyyy"
        }

        $scope.dateOptionsSPK = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.bind_data = {
            data: [],
            filter: []
        };

        $scope.filterData = {
            defaultFilter: [
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'Model Tipe', value: 'ModelType' },
                { name: 'Nama', value: 'CustomerName' }
            ],
            advancedFilter: [
                { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 1 },
                { name: 'Model Tipe', value: 'ModelType', typeMandatory: 0 },
                { name: 'Nama', value: 'CustomerName', typeMandatory: 1 }
            ]
        };


      
       $scope.breadcrums = {};

        $rootScope.$on("SPKdariDaftar", function () {
            //$scope.formspk = true;
            $scope.tinggi = window.screen.availHeight;
            $scope.lebar = window.screen.availWidth;

            $scope.gambar = {
                "max-width": $scope.lebar - 40,
                "max-height": $scope.tinggi - 40
            }
            $scope.PopulateKategory();
            $scope.assignDataDaftarSPK();
        });

        $scope.assignDataDaftarSPK = function () {
            $scope.statusAction = 'lihat';
            $scope.breadcrums.title = "Lihat Detail";
            $scope.tinggi = window.screen.availHeight;
            $scope.lebar = window.screen.availWidth;

            $scope.gambar = {
                "max-width": $scope.lebar - 40,
                "max-height": $scope.tinggi - 40
            }
            $scope.tab = angular.copy($rootScope.breadcrumsTab);
            $scope.selected_data = $rootScope.selected_data;

            $scope.selected_data.SPKPenggantiBit = $rootScope.selected_data.SPKPenggantiBit;

            if($scope.selected_data.SPKPenggantiBit != null) {
                $scope.IsPengganti = $scope.selected_data.SPKPenggantiBit;
            }

            ComboBoxFactory.getDataVehicleModel().then(function (res){$scope.modelname = res.data.Result });
            ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" +$scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                $scope.warna = res.data.Result;
            });
        };

        // $scope.btnkeluarLeasingdraft = function () {
        //     angular.element(".ui.modal.ModalApprovalDicountDraft").modal("hide");
        // }

        // $scope.aprovalLeasing = function () {
        //     angular.element(".ui.modal.ModalApprovalLeasing").modal("show");
        //     angular.element('.ui.modal.ModalApprovalLeasing').not(':first').remove();
        // }

        // $scope.aprovalDiscountDraft = function (data) {
        //     CreateSpkFactory.getApprovalDiscount(data.SpkId).then(function (res) {
        //         setTimeout(function () {
        //             angular.element('.ui.modal.ModalApprovalDicountDraft').modal('refresh');
        //         }, 0);
        //         angular.element(".ui.modal.ModalApprovalDicountDraft").modal("show");
        //         $scope.listDicount = res.data.Result[0];
        //         //angular.element('.ui.modal.ModalApprovalDicountDraft').not(':first').remove();
        //     });
        // }


        $scope.$watch("selected_data.PaymentTypeId", function (newValue, oldValue) {
            var temp = {};
            for (var i in $scope.jenispembayaran) {
                if ($scope.jenispembayaran[i].PaymentTypeId == newValue) {
                    temp = $scope.jenispembayaran[i];
                }
            }
            if (temp.PaymentTypeName == "Cash") {
                $scope.bankstastus = false;
                $scope.selected_data.BankId = null;
            } else {
                $scope.bankstastus = true;
            }
        });


        $scope.grandTotalUnit = { GrandTotalAll: 0 };

        $scope.setNull = function (data){ if(data == 0){data = undefined;} else{data = data;} return data;}
        $scope.setZero = function (data){ if(data == null || data == undefined){data = 0;} else{data = data;} return data;}


        $rootScope.$on("GrandTotalSPK", function () {
            $scope.callculateGrandTotalAll();
        });


        $scope.statusSimpan = false;
        $scope.back = function () {
            $scope.formspk = false;
            $scope.buatSPKProspect = false;
            $scope.formData = true;
            $scope.listspk = true;
            // $scope.selected_data = {};
            $rootScope.$emit("kembaliDaftarSPK", {});
            $rootScope.$emit("KembalidariProspect", {});
            $rootScope.$emit("KembalidariSpkBook", {});

            if ($scope.statusKembali != 'save' && $scope.statusKembali != 'book') {
                $rootScope.$emit("kembaliCekbookingList", {});
            } else if ($scope.statusKembali == 'book') {
                $rootScope.$emit("KembalidariSpkBook", {});
            } else {
                $rootScope.$emit("kembaliKebookingList", {});
            }

            // Free Booking Unit
            if ($scope.statusUnit == 'bookingunit' && $scope.statusSimpan == false) {
                CreateSpkFactory.freeUnitBooking($scope.selected_data.BookingUnitId).then(function (res) {

                });
            }

        }

        // Set Default Kode Pajak
        // function Pelwil(data) {
        //     console.log(data);
        // }

        function maxDiscAcc(dataSPK) {
            var maxDiscAccValue = 0;
            for (var i in dataSPK.ListInfoUnit) {
                for (var j in dataSPK.ListInfoUnit[i].ListDetailUnit) {
                    for (var k in dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage) {
                        if (dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Price > maxDiscAccValue &&
                            dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].IsDiscount == true) {
                            maxDiscAccValue = dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessoriesPackage[k].Price;
                        }
                    }
                    for (var l in dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories) {
                        if (dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Price > maxDiscAccValue &&
                            dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].IsDiscount == true) {
                            maxDiscAccValue = dataSPK.ListInfoUnit[i].ListDetailUnit[j].ListAccessories[l].Price;
                        }
                    }
                }
            }
            return maxDiscAccValue;
        }

        $scope.totalSumDisc = 0;

        $scope.addDisc = {};
      
        $scope.selectedNoSpk = function (spk) {
            if (spk != undefined || spk != null) {
                for (var i in $scope.nospkisi) {
                    if ($scope.nospkisi[i].SpkId == spk) {
                        $scope.selected_data.FormSPKNo = $scope.nospkisi[i].FormSPKNo;
                    }
                }
            }

        }

        $scope.kembaliformspk = function () {
            $scope.signspk = false;
            $scope.formspk = true;
        }

        // $scope.closeModal = function () {
        //     angular.element(".ui.modal.ModalApprovalDicountDraft").modal("hide");
        //     $scope.clear();
        // }
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.Category = function () {
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.ekspansiAll = false;

        }

        $scope.individuPembelibtn = function () {
            $scope.individuPemilik = false;
            switch ($scope.individuPembeli) {
                case false:
                    $scope.individuPembeli = true;
                    $scope.ekspansiAll = false;
                    break;
                case true:
                    $scope.individuPembeli = false;
                    break;
            }
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
        }

        $scope.individuPemilikbtn = function () {
            $scope.individuPembeli = false;
            $scope.ekspansiAll = false;
            if ($scope.individuPemilik == false) {
                $scope.individuPemilik = true;
            } else if ($scope.individuPemilik == true) {
                $scope.individuPemilik = false;
            }
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
        }

        $scope.HidePenanggungJawabPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = !$scope.formDataEkspansiPenanggungJawabPerusahaan;
        }

        $scope.btnPembeliPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPembeliPerusahaan = !$scope.formDataPembeliPerusahaan;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;

        }

        $scope.btnPenangungJawabPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPenanggungJawabPerusahaan = !$scope.formDataPenanggungJawabPerusahaan;
            $scope.formDataPembeliPerusahaan = false;
            $scope.formDataPemilikPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
        }

        $scope.btnPemilikPerusahaan = function () {
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;
            $scope.formDataPemilikPerusahaan = !$scope.formDataPemilikPerusahaan;
            $scope.formDataPenanggungJawabPerusahaan = false;
            $scope.formDataPembeliPerusahaan = false;
            $scope.individuPembeli = false;
            $scope.individuPemilik = false;
        }

        $scope.temphistorydiscount = null;

        $scope.viewSelectedItem = function (data) {
            angular.element('.ui.modal.DraftSPK').modal('hide');
            $scope.statusAction = "lihat";
            $scope.formDataEkspansiPenanggungJawabPerusahaan = false;

            CreateSpkFactory.getDataDetail(data.SpkId).then(function (res) {
                $scope.selected_data = res.data.Result[0];
                console.log("ini===>" , $scope.selected_data)

                $scope.tempdoc = angular.copy($scope.selected_data.ListInfoDocument);
                // setTimeout(function(){
                //     $scope.selected_data.GrandTotal = res.data.Result[0].GrandTotal;
                // },0)
                for (var i in $scope.tempdoc) {
                    $scope.tempdoc[i].UploadDataDocument = { UpDocObj: null, FileName: null };
                    $scope.tempdoc[i].UploadDataDocument.UpDocObj = $scope.tempdoc[i].DocumentData;
                    $scope.tempdoc[i].UploadDataDocument.FileName = $scope.tempdoc[i].DocumentDataName;
                }

                ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" +$scope.selected_data.ListInfoUnit[0].VehicleModelId).then(function (res) { $scope.vehicletype = res.data.Result; });
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.selected_data.ListInfoUnit[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                });

                $scope.selected_data.ListInfoDocument = $scope.tempdoc;

                setTimeout(function () {
                    console.log($scope.selected_data, res.data.Result[0]);

                    //$scope.callculateGrandTotalAll();
                }, 3000);


                $scope.formspk = true;
                $scope.listspk = false;
            });
        }

        //Kategory Pelanggan
        $scope.kategory = [];
        $scope.PopulateKategory = function () {
            ComboBoxFactory.getDataKategori().then(function (res) {
                var tempCategory = [];
                tempCategory = res.data.Result;
                $scope.kategory = tempCategory.filter(function (type) {
                    return (type.ParentId == 2 || type.CustomerTypeId == 3);
                })
            })
        }
        $scope.PopulateKategory();

        ComboBoxFactory.getDataProvince().then(function (res) { $scope.province = res.data.Result; });
        ComboBoxFactory.getDataGender().then(function (res) { $scope.jeniskelamin = res.data.Result; });
        ComboBoxFactory.getDataStatusPernikahan().then(function (res) { $scope.maritalststus = res.data.Result; });
        ComboBoxFactory.getDataAgama().then(function (res) { $scope.agama = res.data.Result; });
        ComboBoxFactory.getDataJabatan().then(function (res) { $scope.jabatan = res.data.Result; });
        ComboBoxFactory.getDataKoresponden().then(function (res) { $scope.koresponden = res.data.Result; });
        ComboBoxFactory.getDataSectorBisnis().then(function (res) { $scope.sectorbisnis = res.data.Result; });
        ComboBoxFactory.getDataDeliveryCategory().then(function (res) { $scope.delivery = res.data.Result; });
        ComboBoxFactory.getDataRoad().then(function (res) { $scope.road = res.data.Result; });
        ComboBoxFactory.getDataVehicleModel().then(function (res) { $scope.modelname = res.data.Result; });
        //ComboBoxFactory.getDataVehicleType().then(function (res) { $scope.vehicletype = res.data.Result; });
        ComboBoxFactory.getSalesProgram().then(function (res) { $scope.salesprogram = res.data.Result; });
        ComboBoxFactory.getJenisPembayaran().then(function (res) { $scope.jenispembayaran = res.data.Result; });
        ComboBoxFactory.getBank().then(function (res) { $scope.bank = res.data.Result; });
        ComboBoxFactory.getNoSPK().then(function (res) { $scope.nospk = res.data.Result; });
        ComboBoxFactory.getNoSPKIsi().then(function (res) { $scope.nospkisi = res.data.Result; });
        ComboBoxFactory.getDataKetegoriPlat().then(function (res) { $scope.kategoriplat = res.data.Result; });
        ComboBoxFactory.getDataCategoryDelivery().then(function (res) { $scope.kategorydelivery = res.data.Result; });
        ComboBoxFactory.getUnitYear().then(function (res) { $scope.unitYear = res.data.Result; });
        ComboBoxFactory.getDataDeliveryMounth().then(function (res) { $scope.deliveryMounth = res.data.Result; });
        ComboBoxFactory.getDataStatusPDD().then(function (res) { $scope.statusPDD = res.data.Result; });
        ComboBoxFactory.getDataLeasing().then(function (res) { $scope.leasing = res.data.Result; });
        // ComboBoxFactory.getDataTenor().then(function(res) { $scope.temptenor = res.data.Result; })
        ComboBoxFactory.getDataSimulationTenor().then(function (res) { $scope.simulasiltenor = res.data.Result; });
        ComboBoxFactory.getDataInsurance().then(function (res) { $scope.insurance = res.data.Result; });
        // ComboBoxFactory.getDataInsuranceProduct().then(function(res) { $scope.tempproductinsurance = res.data.Result; });
        // ComboBoxFactory.getDataInsuranceEkstension().then(function(res) { $scope.tempinsuranceextension = res.data.Result; });
        ComboBoxFactory.getFinanceFundSource().then(function (res) { $scope.FinanceFundSource = res.data.Result; });
        ComboBoxFactory.getDataInsuranceUserType().then(function (res) { $scope.insuranceusertype = res.data.Result; });
        ComboBoxFactory.getDataInsuranceType().then(function (res) { $scope.insurancetype = res.data.Result; });
        ComboBoxFactory.getDataFundSource().then(function (res) { $scope.fundsource = res.data.Result; });
        // ComboBoxFactory.getDataWarna().then(function(res){ $scope.tempwarna = res.data.Result; })

        /////
        $scope.$watch("selected_data.ListInfoPelanggan[0].ProvinceId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[0].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[0].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[0].ProvinceId).then(function (res) {
                    $scope.kabupaten0 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[0].CityRegencyId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[0].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[0].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[0].CityRegencyId).then(function (res) {
                    $scope.kecamatan0 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[0].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[0].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[0].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[0].DistrictId).then(function (res) {
                    $scope.kelurahan0 = res.data.Result;
                });
            };
        })
        /////

        /////
        $scope.$watch("selected_data.ListInfoPelanggan[1].ProvinceId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[1].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[1].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[1].ProvinceId).then(function (res) {
                    $scope.kabupaten1 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[1].CityRegencyId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[1].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[1].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[1].CityRegencyId).then(function (res) {
                    $scope.kecamatan1 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[1].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[1].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[1].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[1].DistrictId).then(function (res) {
                    $scope.kelurahan1 = res.data.Result;
                });
            };
        })
        ///

        ///
        $scope.$watch("selected_data.ListInfoPelanggan[2].ProvinceId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[2].ProvinceId != null || typeof $scope.selected_data.ListInfoPelanggan[2].ProvinceId != "undefined") {
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.selected_data.ListInfoPelanggan[2].ProvinceId).then(function (res) {
                    $scope.kabupaten2 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[2].CityRegencyId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[2].CityRegencyId != null || typeof $scope.selected_data.ListInfoPelanggan[2].CityRegencyId != "undefined") {
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.selected_data.ListInfoPelanggan[2].CityRegencyId).then(function (res) {
                    $scope.kecamatan2 = res.data.Result;
                });
            };
        })

        $scope.$watch("selected_data.ListInfoPelanggan[2].DistrictId", function (newValue, oldValue) {
            if ($scope.selected_data.ListInfoPelanggan[2].DistrictId != null || typeof $scope.selected_data.ListInfoPelanggan[2].DistrictId != "undefined") {
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.selected_data.ListInfoPelanggan[2].DistrictId).then(function (res) {
                    $scope.kelurahan2 = res.data.Result;
                });
            };
        })
        ///

        $scope.filterTypeModel = function (setect){
           console.log("ulalala", setect);
            var selected = $scope.modelname.filter(function(type) {
                return (setect == type.VehicleModelId);
            })

            console.log("ulalalacucuc", selected);

            $scope.selected_data.recalculate = true;
            var lengthUnit = $scope.selected_data.ListInfoUnit.length;
            if (lengthUnit >= 1) {
                $scope.selected_data.ListInfoUnit.splice(1, lengthUnit - 1);
                $scope.selected_data.ListInfoUnit[0].VehicleTypeId = null;
                $scope.selected_data.ListInfoUnit[0].Description = null;
                $scope.selected_data.ListInfoUnit[0].KatashikiCode = null;
                $scope.selected_data.ListInfoUnit[0].SuffixCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorId = null;
                $scope.selected_data.ListInfoUnit[0].ColorCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorCodeName = null;
                $scope.selected_data.ListInfoUnit[0].ColorName = null;
                $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                $scope.selected_data.ListInfoUnit[0].VehiclePrice = 0;
                $scope.selected_data.ListInfoUnit[0].DPP = 0;
                $scope.selected_data.ListInfoUnit[0].Qty = 1;
                $scope.selected_data.ListInfoUnit[0].Discount = 0;
                $scope.selected_data.ListInfoUnit[0].DiscountSubsidiDP = 0;
                $scope.selected_data.ListInfoUnit[0].MediatorCommision = 0;
                $scope.selected_data.ListInfoUnit[0].PPN = 0;
                $scope.selected_data.ListInfoUnit[0].TotalBBN = 0;
                $scope.selected_data.ListInfoUnit[0].PPH22 = 0;
                $scope.selected_data.ListInfoUnit[0].TotalInclVAT = 0;
                $scope.selected_data.ListInfoUnit[0].GrandTotalUnit = 0;
                $scope.selected_data.ListInfoUnit[0].PriceAfterDiscount = 0;

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit.length >= 1) {
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit.splice(1, $scope.selected_data.ListInfoUnit[0].ListDetailUnit.length - 1);
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].Description = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].KatashikiCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].SuffixCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCodeName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAcc = 0;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAll = 0;

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories = []
                    }

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage = []
                    }
                }
            }

            if (setect != null || typeof setect != "undefined") {
                ComboBoxFactory.getDataVehicleType("?start=1&limit=1000&filterData=VehicleModelId|" + setect).then(function (res) {
                    $scope.vehicletype = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeId = null;
                })
            }

            if (setect != null || typeof setect != "undefined") {
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                })
            }
        }

        $scope.warna = [];
        $scope.filterWarna = function (select) {
            var selected = $scope.vehicletype.filter(function(type) {
                return (select == type.VehicleTypeId);
            })
            console.log("test", selected);
            $scope.selected_data.recalculate = true;
            // var tempunit = $scope.vehicletype.filter(function (type) {
            //     return (type.VehicleTypeId == selected);
            // });
            // console.log("test", tempunit);
            var lengthUnit = $scope.selected_data.ListInfoUnit.length;
            if (lengthUnit >= 1) {
                $scope.selected_data.ListInfoUnit.splice(1, lengthUnit - 1);
                $scope.selected_data.ListInfoUnit[0].VehicleTypeId = selected[0].VehicleTypeId;
                $scope.selected_data.ListInfoUnit[0].Description = selected[0].Description;
                $scope.selected_data.ListInfoUnit[0].KatashikiCode = selected[0].KatashikiCode;
                $scope.selected_data.ListInfoUnit[0].SuffixCode = selected[0].SuffixCode;
                $scope.selected_data.ListInfoUnit[0].ColorId = null;
                $scope.selected_data.ListInfoUnit[0].ColorCode = null;
                $scope.selected_data.ListInfoUnit[0].ColorCodeName = null;
                $scope.selected_data.ListInfoUnit[0].ColorName = null;
                $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                $scope.selected_data.ListInfoUnit[0].VehiclePrice = 0;
                $scope.selected_data.ListInfoUnit[0].DPP = 0;
                $scope.selected_data.ListInfoUnit[0].Qty = 1;
                $scope.selected_data.ListInfoUnit[0].Discount = 0;
                $scope.selected_data.ListInfoUnit[0].DiscountSubsidiDP = 0;
                $scope.selected_data.ListInfoUnit[0].MediatorCommision = 0;
                $scope.selected_data.ListInfoUnit[0].PPN = 0;
                $scope.selected_data.ListInfoUnit[0].TotalBBN = 0;
                $scope.selected_data.ListInfoUnit[0].PPH22 = 0;
                $scope.selected_data.ListInfoUnit[0].TotalInclVAT = 0;
                $scope.selected_data.ListInfoUnit[0].GrandTotalUnit = 0;
                $scope.selected_data.ListInfoUnit[0].PriceAfterDiscount = 0;

                if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit.length >= 1) {
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit.splice(1, $scope.selected_data.ListInfoUnit[0].ListDetailUnit.length - 1);
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeId = selected[0].VehicleTypeId;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].Description = selected[0].Description;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].KatashikiCode = selected[0].KatashikiCode;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].SuffixCode = selected[0].SuffixCode;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCode = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorCodeName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ColorName = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].VehicleTypeColorId = null;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAcc = 0;
                    $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].GrandTotalAll = 0;

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessories = []
                    }

                    if ($scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage.length > 0) {
                        $scope.selected_data.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage = []
                    }
                }
            }

            if (select != null || typeof select != "undefined") {
                ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + selected[0].VehicleTypeId).then(function (res) {
                    $scope.warna = res.data.Result;
                    $scope.selected_data.ListInfoUnit[0].VehicleTypeColorId = null;
                })
            }
        };

        $scope.temptenor = [];
        $scope.filterTenor = function (selected) {
            if (selected != null || typeof selected != "undefined") {
                ComboBoxFactory.getDataTenor(selected).then(function (res) {
                    $scope.tenor = res.data.Result;
                    setTimeout(function () {
                        $scope.selected_data.LeasingLeasingTenorId = $scope.selected_data.LeasingLeasingTenorId;
                    }, 300);
                })
            }
        }

        $scope.$watch("selected_data.LeasingId", function (newValue, oldValue) {
            if (newValue != oldValue) {
                ComboBoxFactory.getDataTenor($scope.selected_data.LeasingId).then(function (res) {
                    $scope.tenor = res.data.Result;
                })
            }
        });

        $scope.year = function (selected) {
            if ($scope.selected_data.ListInfoUnit.length > 0) {
                for (var i in $scope.selected_data.ListInfoUnit) {
                    $scope.selected_data.ListInfoUnit[i].ProductionYear = selected.Year;
                }
            }
        }

        $scope.product = [];
        $scope.insuranceextension = [];
        $scope.filterProduct = function (selected) {
            if (selected != undefined || selected != null) {
                ComboBoxFactory.getDataInsuranceProduct(selected).then(function (res) { $scope.product = res.data.Result; });
                ComboBoxFactory.getDataInsuranceEkstension(selected).then(function (res) { $scope.insuranceextension = res.data.Result; });
            }
        }

        $scope.$watch("selected_data.InsuranceId", function (newValue, oldValue) {
            if (newValue != oldValue) {
                ComboBoxFactory.getDataInsuranceProduct($scope.selected_data.InsuranceId).then(function (res) { $scope.product = res.data.Result; });
                ComboBoxFactory.getDataInsuranceEkstension($scope.selected_data.InsuranceId).then(function (res) { $scope.insuranceextension = res.data.Result; });
            }
        });

        ComboBoxFactory.getKodePajak().then(function (res) {
            $scope.kodepajak = res.data.Result;
            if ($scope.statusAction == null || $scope.statusAction == 'buat') {
                for (var i in $scope.kodepajak) {
                    if ($scope.kodepajak[i].DefaultBit == true) {
                        $scope.selected_data.CodeInvoiceTransactionTaxId = $scope.kodepajak[i].CodeInvoiceTransactionTaxId
                    }
                }
            }

        });

        $scope.fundsourceselected = function (selected) {
            $scope.fundsourceselect = selected;
        }

        $scope.fundsource = function () {
            if ($scope.fundsourceselect.FundSourceName == 'Cash') {
                $scope.bankstastus = false;
            } else {
                $scope.bankstastus = true;
            }
        }

        $scope.loadingdoc == false;

        $scope.removeImage = function (list, index) {
            list.UploadDataDocument.UpDocObj = null;
            list.UploadDataDocument.FileName = null;
        }

        $scope.viewImage = function (docList) {
            $scope.image = { documentname: null, viewdocument: null, viewdocumentName: null };
            if ($scope.menusekarang == 'Daftar SPK') {
                var numItems = angular.element('.ui.modal.dokumenSPKViewMobiles').length;
            } else {
                var numItems = angular.element('.ui.modal.dokumenSPKViewMobilesDraft').length;
            }

            MaintenanceSpkFactory.getImage(docList.DocumentURL).then(function (res) {
                if ($scope.menusekarang == 'Daftar SPK') {
                    setTimeout(function () {
                        angular.element('.ui.modal.dokumenSPKViewMobiles').modal('refresh');
                    }, 0);
                    $scope.image = { documentname: docList.DocumentName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                    if (numItems > 1) {
                        angular.element('.ui.modal.dokumenSPKViewMobiles').not(':first').remove();
                    }
                    angular.element('.ui.modal.dokumenSPKViewMobiles').modal('show');
                } else {
                    setTimeout(function () {
                        angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('refresh');
                    }, 0);
                    $scope.image = { documentname: docList.DocumentName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                    if (numItems > 1) {
                        angular.element('.ui.modal.dokumenSPKViewMobilesDraft').not(':first').remove();
                    }
                    angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('show');
                }
            });
        }

        $scope.keluarDokumen = function () {
            //$scope.image = { viewdocument: null, viewdocumentName: null };
            //angular.element('.ui.modal.dokumenSPKViewMobiles').not(':first').remove();
            if ($scope.menusekarang == 'Daftar SPK') {
                angular.element('.ui.modal.dokumenSPKViewMobiles').modal('hide');
            } else {
                angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('hide');
            }
        }

        
        ////////////////////////////////////////////////////////////////////////////
        ///// End SPK
        ////////////////////////////////////////////////////////////////////////////


    });

