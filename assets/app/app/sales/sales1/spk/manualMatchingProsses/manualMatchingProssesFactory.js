angular.module('app')
    .factory('ManualMatchingProssesFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/fw/Role');
                //console.log('res=>',res);
                return res;
            },
            create: function(SpkTaking) {
                return $http.post('/api/fw/Role', [{
                    NamaSales: SpkTaking.NamaSales,

                }]);
            },
            update: function(SpkTaking) {
                return $http.put('/api/fw/Role', [{
                    NoSPK: SpkTaking.NoSPK,
                    NamaSPK: SpkTaking.NamaSales,

                    //pid: role.pid,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd