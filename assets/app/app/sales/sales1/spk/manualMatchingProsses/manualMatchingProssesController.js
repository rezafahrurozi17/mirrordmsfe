angular.module('app')
    .controller('ManualMatchingProssesController', function($scope, $http, CurrentUser, ManualMatchingProssesFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;

            $scope.gridData = [];
            // $scope.gridData2 = [];


        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mmanualMatchingProssesFactory = null; //Model
        //$scope.mcekSpkValidDocument = null;


        //----------------------------------
        // Get Data
        //----------------------------------
        var dataprof = [{
                "Id": "1",
                "Nama": "Michael E",
                "Rate": "Hot",
                "NoTlp": "085673991448",
                "Type": "Fortuner VRZ 4x2w (Diesel) - Black Mica"
            },
            {
                "Id": "2",
                "Nama": "Sam Mitchell",
                "Rate": "Medium",
                "NoTlp": "085673991448",
                "Type": "Avanza G 15L - Silver Metallic"
            },
            {
                "Id": "3",
                "Nama": "Barney Ross",
                "Rate": "Low",
                "NoTlp": "085673991448",
                "Type": "Sienta Q - Super White 2"
            },

        ];
        $scope.daftar = dataprof;

        var x = [{
                "Id": 1,
                "TipeModel": "Camry 2000 M/T",
                "Warna": "Metalic",
                "NoRangka": "~",
                "NoSpk": 270000020,
                "TanggalSPK": "13-01-2013",
                "SoNo": 600000020,
                "SoDate": "15/01/2013",
                "TanggalPelunasan": "13/01/2013",
                "NamaProspect": "Amir Huda",
                "StatusMatching": "Match"
            },
            {
                "Id": 2,
                "TipeModel": "Camry 2000 M/T",
                "Warna": "Metalic",
                "NoRangka": "~",
                "NoSpk": 270000021,
                "TanggalSPK": "13-01-2013",
                "SoNo": 600000021,
                "SoDate": "15/01/2013",
                "TanggalPelunasan": "13/01/2013",
                "NamaProspect": "Esti Kurnia",
                "StatusMatching": "Outstanding"
            }

        ];

        $scope.datax = x;

        //console.log("datax",  $scope.datax);

        // var gridData = [];
        $scope.getData = function() {
            ManualMatchingProssesFactory.getData()
                .then(
                    function(res) {
                        gridData = [];
                        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);



                        // array.forEach(function(x) {
                        //     var plus = x.NoSPK + '-' + x.NoSP;
                        //     return plus;
                        // }, this);

                        //$scope.grid.data = res.data.Result;
                        $scope.grid.data = x;
                        // console.log("ActivityChecklistItemAdministrasiPembayaran=>", res.data.Result);
                        //console.log("grid data=>",$scope.grid.data);
                        //$scope.roleData = res.data;
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }


        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }



        // var btnActionEditTemplate = '<button class="ui icon inverted grey button"' +
        //     ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
        //     ' onclick="this.blur()"' +
        //     ' ng-click="grid.appScope.gridClickToggleStatus(row.entity)">' +
        //     '<i ng-class="' +
        //     '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.OpenState,' +
        //     '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.OpenState,' +
        //     '}' +
        //     '">' +
        //     '</i>' +
        //     '</button>';

        //var btnActionEditTemplate = '<a style="color:blue;" onclick=""; ng-click=""><p style="padding:5px 0 0 5px"><u>Lihat</u></p></a>';
        var btnActionCekValid2 = '<a style="color:blue;"  ng-click=""><p style="padding:5px 0 0 5px" onclick="MatchRRN()";><u>...</u></p></a>';
        // var noSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u>{{row.entity.NoSPK}}</u></p></span>';
        // var statusSpkClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px" onclick="ModalConfirmation()"><u>{{row.entity.StatusSPK}}</u></p></span>';;
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: 'tipe model', field: 'TipeModel' },
                { name: 'warna', field: 'Warna', width: '10%' },
                { name: 'no rangka', cellTemplate: btnActionCekValid2 },
                { name: 'no spk', field: 'NoSpk' },
                { name: 'tanggal spk', field: 'TanggalSPK' },
                { name: 'so no', field: 'SoNo' },
                { name: 'so date', field: 'SoDate' },
                { name: 'tanggal pelunasan', field: 'TanggalPelunasan' },
                { name: 'nama prospect/Pelanggan', field: 'NamaProspect' },
                { name: 'status matching', field: 'StatusMatching' }

            ]
        };


    });