angular.module('app')
    .factory('DistributionSpkFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    '00:' + '00';
                return fix;
            } else {
                return null;
            }
        };

        function reDate(str) {
            var date = str.substring(0, 2);
            var month = str.substring(3, 5);
            var year = str.substring(6, 10);

            return year+"-"+month+"-"+date;
        }

        return {       
            getData: function(filter) {
                filter.dateFrom = fixDate(filter.dateFrom);
                filter.dateTo = fixDate(filter.dateTo);

                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/SAHFormSPKRegister/?start=1&limit=10000&' + param + '');
                return res;
            },

            getDataSales: function() {
                var res = $http.get('/api/sales/MProfileEmployee/SalesmanAll'); // $http.get('/api/sales/MProfileEmployee/?position=salesman');
                return res;
            },

            getConfigGenerate: function (){
                var res=$http.get('/api/sales/GetSGlobalParam?ParamType=3');
                var val =[];
               
                console.log("testing", val);
                return res;
            },

            create: function(SpkTaking) {
                return $http.post('/api/sales/CreateFormSPK', SpkTaking);
            },

            createAuto: function(SpkTaking) {
                SpkTaking.ReceiveDate = fixDate(SpkTaking.ReceiveDate);
                return $http.post('/api/sales/CreateFormSPKNonUpload', [{
                    SalesId: SpkTaking.SalesId,
                    FormSPKNo: SpkTaking.FormSPKNo,
                    Qty: SpkTaking.Qty,
                    ReceiveDate: SpkTaking.ReceiveDate,
                }]);
            },

            cekNoSPK: function(Data){
                for(var i in Data){
                    Data[i].ReceiveDate = reDate(Data[i].ReceiveDate);
                }
                var res =  $http.put('/api/sales/CreateFormSPK/Check', Data);
                return res;
            }
        }
    });