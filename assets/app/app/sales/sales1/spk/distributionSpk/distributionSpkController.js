angular.module('app')
    .controller('DistributionSpkController', function ($scope, $filter, MobileHandling, $httpParamSerializer, $http, bsNotify, CurrentUser, DistributionSpkFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            $scope.loading = false;
            //$scope.isGenerate = false;
            $scope.bv = MobileHandling.browserVer();
            $scope.bver = MobileHandling.getBrowserVer();
            //console.log("browserVer=>",$scope.bv);
            //console.log("browVersion=>",$scope.bver);
            //console.log("tab: ",$scope.tab);
            if ($scope.bv !== "Chrome") {
                if ($scope.bv == "Unknown") {
                    if (!alert("Menu ini tidak bisa dibuka pada perangkat Mobile.")) {
                        $scope.removeTab($scope.tab.sref);
                    }
                }
            }

            //Get Config Generate
            DistributionSpkFactory.getConfigGenerate().then(function (res) {
                var val = {};
                for (var i in res.data.Result) {
                    if (res.data.Result[i].ParamId == 404) {
                        val = res.data.Result[i];
                    }
                    if (val.NumVal1 == 0) {
                        val.NumVal1 = false;
                    } else {
                        val.NumVal1 = true;
                    }
                }
                $scope.isGenerate = val.NumVal1;
            });
            //$scope.getData(1);
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.isGenerate = false;
        $scope.user = CurrentUser.user();
        $scope.quantity = 0, $scope.RangeSPK = [];
        $scope.uiGridPageSize = 10;
        $scope.addView = false;
        $scope.promise = false;
        var mount = new Date();
        var year = mount.getFullYear();
        var mounth = mount.getMonth();
        var lastdate = new Date(year, mounth + 1, 0).getDate();
        $scope.tempDistribusiSpkValid = [];
        $scope.tempDistribusiSpkinValid = [];
        $scope.totalvalid = 0;
        $scope.totalinvalid = 0;
        $scope.isComplete = false;
        $scope.disabledSimpan = false;

        $scope.firstDate = new Date(mount.setDate("01"));
        $scope.lastDate = new Date(mount.setDate(lastdate));

        //$scope.mintgl = null;
        $scope.mdistributionSpk = { SalesId: null, Qty: 0, ReceiveDate: new Date() }; //Model
        $scope.filter = { dateFrom: $scope.firstDate, dateTo: new Date(), salesId: null, FormSpkNo: null };
        //$scope.minDate = new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };
        $scope.dateOptionsFrom = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };
        $scope.dateOptionLastDate = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

        $scope.tanggalmin = function (dt) {
            $scope.dateOptionLastDate.minDate = dt;
            if (dt == null || dt == undefined) {
                $scope.filter.dateTo = null;
            } else {
                if ($scope.filter.dateFrom < $scope.filter.dateTo) {

                } else {
                    $scope.filter.dateTo = $scope.filter.dateFrom;
                }
            }
        }

        //----------------------------------
        // Get Data
        //----------------------------------

        DistributionSpkFactory.getDataSales().then(function (res) {
            $scope.sales = res.data.Result;
            console.log("list sales", $scope.sales);
            $scope.salescode = $filter('orderBy')($scope.sales, 'EmployeeCode', false);
            $scope.salesname = $filter('orderBy')($scope.sales, 'EmployeeName', false);
        });

        var gridData = [];
        $scope.getData = function () {
            if ($scope.filter.dateFrom == null || $scope.filter.dateFrom == undefined) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Silahkan isi range tanggal.",
                    type: 'warning'
                });
            } else if ($scope.filter.dateFrom != null && $scope.filter.dateTo == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Pastikan range tanggal telah terisi dengan benar.",
                    type: 'warning'
                });
            } else {
                $scope.loading = true;
                DistributionSpkFactory.getData($scope.filter).then(
                    function (res) {
                        $scope.loading = false;
                        $scope.grid.data = res.data.Result;
                        $scope.grid.totalItems = res.data.Total;
                        return res.data;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );
            }
        }


        $scope.simpan = function () {
            $scope.disabledSimpan = true;
            $scope.promise = true;
            if ($scope.tempDistribusiSpkinValid.length > 0) {
                bsNotify.show({
                    title: "Peringatan!",
                    content: "Data tidak valid, Terjadi Kecurangan !!!",
                    type: 'danger'
                });
                $scope.promise = false;
                $scope.disabledSimpan = false;
            } else {
                if ($scope.isGenerate == true) {
                    DistributionSpkFactory.createAuto($scope.mdistributionSpk).then(function (res) {
                        $scope.loading = true;
                        $scope.promise = false;
                        DistributionSpkFactory.getData($scope.filter).then(
                            function (res) {
                                $scope.loading = false;
                                $scope.grid.data = res.data.Result;
                            },
                            function (err) {
                            }
                        );
                        $scope.addView = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Nomor SPK berhasil di distribusikan ke Sales.",
                            type: 'success'
                        });
                        $scope.disabledSimpan = false;
                    });
                } else {
                    DistributionSpkFactory.create($scope.tempDistribusiSpkValid).then(function (res) {
                        $scope.loading = true;
                        $scope.promise = false;
                        DistributionSpkFactory.getData($scope.filter).then(
                            function (res) {
                                $scope.loading = false;
                                $scope.grid.data = res.data.Result;
                            },
                            function (err) {
                            }
                        );
                        $scope.addView = false;
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Nomor SPK berhasil di distribusikan ke Sales.",
                            type: 'success'
                        });
                        $scope.disabledSimpan = false;
                    });
                }
            }
        };

        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.test = function () {
            angular.element('.ui.modal.loadingUploadfiles').modal('show');
        }

        $scope.open = function () {
            document.getElementById("uploadNoSPK").select();
        }

        $scope.tambah = function () {
            //try{
            DistributionSpkFactory.getConfigGenerate().then(function (res) {
                var val = {};
                for (var i in res.data.Result) {
                    if (res.data.Result[i].ParamId == 404) {
                        val = res.data.Result[i];
                    }
                    if (val.NumVal1 == 0) {
                        val.NumVal1 = false;
                    } else {
                        val.NumVal1 = true;
                    }
                }
                $scope.isGenerate = val.NumVal1;
                $scope.tempDistribusiSpkValid = [];
                $scope.tempDistribusiSpkinValid = [];
                $scope.DistribusiSpk_ExcelData = [];
                $scope.FileName = null;
                $scope.totalvalid = 0;
                $scope.totalinvalid = 0;
                $scope.gridList.data = [];
                $scope.myEl = [];
                $scope.isComplete = false;
                $scope.addView = true;
                angular.element(document.querySelector('#uploadNoSPK')).prop("value", "");
                $scope.mdistributionSpk = { SalesId: null, ReceiveDate: new Date(), Qty: 1 };
                $scope.dateOptions.minDate = new Date();
                //console.log("asdsad)()*",val);
            });
            //}catch(ex){
            //}
        }

        // $scope.cekTambah = function (obj) {
        //     var loop = null;
        //     for (var i = 0; i <= obj.Quantity; i++) {
        //         loop.push();
        //     }
        //     var tgl = $scope.mdistributionSpk.TanggalTerima.toJSON();
        //     $scope.mdistributionSpk.TanggalTerima = tgl;
        // }

        // $scope.jumlah = function () {
        //     var seq = 0;
        //     for (var i = 0; i <= $scope.quantity; i++) {
        //         seq++;
        //         $scope.RangeSPK.push({ Sequence: seq });
        //     }
        // }

        // $scope.input = function (Qty) {
        //     if (Qty == 0) {
        //         $scope.mdistributionSpk.Qty = null;
        //     } else {
        //         var first = $scope.mdistributionSpk.Qty.substring(0, 1);
        //         var length = $scope.mdistributionSpk.Qty.length;
        //         var fixed = $scope.mdistributionSpk.Qty.substring(1, length);
        //         if (first == 0) {
        //             $scope.mdistributionSpk.Qty = fixed
        //         }
        //     }
        //     var asd = angular.copy($scope.mdistributionSpk.FormSPKNoFrom);
        //     $scope.mdistributionSpk.FormSPKNoTo = asd;
        // }

        //==========================================================================//
        // 							Excel Upload Begin								//
        //==========================================================================//
        $scope.DistribusiSpk_ExcelData = [];
        $scope.loadXLS = function (ExcelFile) {
            var ExcelData = [];
            $scope.myEl = [];
            $scope.tempDistribusiSpkValid = [], $scope.tempDistribusiSpkinValid = [];
            $scope.totalvalid = 0, $scope.totalinvalid = 0;
            $scope.gridList.data = [], $scope.DistribusiSpk_ExcelData = [];

            $scope.myEl = angular.element(document.querySelector('#uploadNoSPK')); //ambil elemen dari dokumen yang di-upload
            console.log("Upload Excel", $scope.myEl);

            // alert('selesai');
            $scope.FileName = $scope.myEl.context.files[0].name;
            var listExt = $scope.FileName.split(".");

            //--check double extension.
            if (listExt.length >= 3) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Nama file tidak boleh menggunakan ' . '",
                    type: 'warning'
                });
                $scope.myEl = [], $scope.FileName = null;
                angular.element(document.querySelector('#uploadNoSPK')).prop("value", "");
            } else {
                XLSXInterface.loadToJson($scope.myEl[0].files[0], function (json) {
                    //ExcelData = json[0];
                    for (var i = 0; i < json.length; i++) {
                        if (json[i].KodeSalesman != '' && json.NoSpk != '' && json.TanggalSpk != '') {
                            $scope.DistribusiSpk_ExcelData.push({
                                Baris: i + 1,
                                KodeSalesman: json[i].KodeSalesman,
                                NoSpk: json[i].NoSpk,
                                TanggalSpk: json[i].TanggalSpk,
                                valid: null
                            });
                        }
                    }
                });
            }

            angular.element('.ui.modal.loadingUploadfiles').modal('hide');
        }

        $scope.refreshExcel = function () {
            $scope.tempDistribusiSpkValid = [];
            $scope.tempDistribusiSpkinValid = [];
            $scope.FileName = null;
            $scope.totalvalid = 0;
            $scope.totalinvalid = 0;
            $scope.gridList.data = [];
            $scope.DistribusiSpk_ExcelData = [];
            $scope.isComplete = false;
            $scope.myEl = [];
            angular.element(document.querySelector('#uploadNoSPK')).prop("value", "");
        }

        $scope.uploadExcelSpk = function () {
            $scope.tempDistribusiSpkValid = []; $scope.tempDistribusiSpkinValid = [];
            for (var i = 0; i < $scope.DistribusiSpk_ExcelData.length; i++) {
                $scope.isExist = 0, $scope.isLoop = 0;
                for (var j = 0; j < $scope.sales.length; j++) {
                    $scope.isLoop++;
                    if ($scope.DistribusiSpk_ExcelData[i].KodeSalesman == $scope.sales[j].EmployeeCode) { // Cek Kode salesman
                        if ($scope.DistribusiSpk_ExcelData[i].NoSpk == '' && $scope.DistribusiSpk_ExcelData[i].TanggalSpk == '') {
                            $scope.mDistribusiSPK = {
                                Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                SalesId: $scope.sales[j].EmployeeId,
                                SalesCode: $scope.sales[j].EmployeeCode,
                                SalesName: $scope.sales[j].EmployeeName,
                                FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                Qty: 1,
                                Keterangan: 'No SPK dan Tanggal SPK tidak ada.',
                                valid: false
                            }
                            $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                        } else if ($scope.DistribusiSpk_ExcelData[i].NoSpk == '') {
                            $scope.mDistribusiSPK = {
                                Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                SalesId: $scope.sales[j].EmployeeId,
                                SalesCode: $scope.sales[j].EmployeeCode,
                                SalesName: $scope.sales[j].EmployeeName,
                                FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                Qty: 1,
                                Keterangan: 'No SPK tidak ada.',
                                valid: false
                            }
                            $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                        } else if ($scope.DistribusiSpk_ExcelData[i].TanggalSpk == '') {
                            $scope.mDistribusiSPK = {
                                Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                SalesId: $scope.sales[j].EmployeeId,
                                SalesCode: $scope.sales[j].EmployeeCode,
                                SalesName: $scope.sales[j].EmployeeName,
                                FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                Qty: 1,
                                Keterangan: 'Tanggal SPK tidak ada.',
                                valid: false
                            }
                            $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                        } else {
                            if ($scope.DistribusiSpk_ExcelData[i].NoSpk.length > 19) { // Cek panjang maksimal no SPK
                                $scope.mDistribusiSPK = {
                                    Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                    SalesId: $scope.sales[j].EmployeeId,
                                    SalesCode: $scope.sales[j].EmployeeCode,
                                    SalesName: $scope.sales[j].EmployeeName,
                                    FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                    ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                    Qty: 1,
                                    Keterangan: 'NO SPK melebihi 19 karakter.',
                                    valid: false
                                }
                                $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                            } else if ($scope.DistribusiSpk_ExcelData[i].TanggalSpk.length != 10) { // Cek panjang tanggal (format)
                                $scope.mDistribusiSPK = {
                                    Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                    SalesId: $scope.sales[j].EmployeeId,
                                    SalesCode: $scope.sales[j].EmployeeCode,
                                    SalesName: $scope.sales[j].EmployeeName,
                                    FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                    ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                    Qty: 1,
                                    Keterangan: 'Format tanggal tidak sesuai, format: dd-mm-yyyy.',
                                    valid: false
                                }
                                $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                            } else if ($scope.DistribusiSpk_ExcelData[i].TanggalSpk.length == 10) { // Cek panjang tanggal valid
                                var string = [];
                                for (var k in $scope.DistribusiSpk_ExcelData[i].TanggalSpk) {
                                    string.push({
                                        index: i + 1,
                                        str: $scope.DistribusiSpk_ExcelData[i].TanggalSpk[k]
                                    });
                                }
                                if (string[2].str != '-' || string[5].str != '-') { // Cek Sparator format tangggal 
                                    $scope.mDistribusiSPK = {
                                        Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                        SalesId: $scope.sales[j].EmployeeId,
                                        SalesCode: $scope.sales[j].EmployeeCode,
                                        SalesName: $scope.sales[j].EmployeeName,
                                        FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                        ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                        Qty: 1,
                                        Keterangan: 'Format tanggal tidak sesuai, format: dd-mm-yyyy.',
                                        valid: false
                                    }
                                    $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                                } else {
                                    var day = parseInt(string[0].str + string[1].str);
                                    var month = parseInt(string[3].str + string[4].str);
                                    var year = parseInt(string[6].str + string[7].str + string[8].str + string[9].str);
                                    var lastdate = parseInt(new Date(year, month, 0).getDate());
                                    var today = new Date(); today.setHours(0); today.setMinutes(0); today.setSeconds(0);
                                    var daying = new Date(year, month - 1, day);

                                    var dayingSubmit = $filter('date')(new Date(daying), 'M/d/yy');
                                    var TodaySubmit = $filter('date')(new Date(today), 'M/d/yy');

                                    if (month <= 0 || month > 12) { // Cek Bulan antara 1 - 12
                                        $scope.mDistribusiSPK = {
                                            Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                            SalesId: $scope.sales[j].EmployeeId,
                                            SalesCode: $scope.sales[j].EmployeeCode,
                                            SalesName: $scope.sales[j].EmployeeName,
                                            FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                            ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                            Qty: 1,
                                            Keterangan: 'Bulan lebih kecil dari 0 atau lebih besar 12.',
                                            valid: false
                                        }
                                        $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                                    } else if (day < 1 || day > lastdate) { // Cek Tanggal minimal 1 , maksimal tangal bulan ini.
                                        $scope.mDistribusiSPK = {
                                            Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                            SalesId: $scope.sales[j].EmployeeId,
                                            SalesCode: $scope.sales[j].EmployeeCode,
                                            SalesName: $scope.sales[j].EmployeeName,
                                            FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                            ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                            Qty: 1,
                                            Keterangan: 'Tanggal lebih besar dari akhir tanggal bulan ini atau kurang dari 0.',
                                            valid: false
                                        }
                                        $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                                    } else if (dayingSubmit < TodaySubmit) { // Cek Tanggal mundur. 
                                        // console.log('1',daying);
                                        // console.log('2',today);
                                        $scope.mDistribusiSPK = {
                                            Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                            SalesId: $scope.sales[j].EmployeeId,
                                            SalesCode: $scope.sales[j].EmployeeCode,
                                            SalesName: $scope.sales[j].EmployeeName,
                                            FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                            ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                            Qty: 1,
                                            Keterangan: 'Tanggal lebih kecil dari hari ini.',
                                            valid: false
                                        }
                                        $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                                    } else { // Data Valid
                                        $scope.mDistribusiSPK = {
                                            Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                            SalesId: $scope.sales[j].EmployeeId,
                                            SalesCode: $scope.sales[j].EmployeeCode,
                                            SalesName: $scope.sales[j].EmployeeName,
                                            FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                            ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                            Qty: 1,
                                            Keterangan: 'Valid.',
                                            valid: true

                                        }
                                        $scope.tempDistribusiSpkValid.push($scope.mDistribusiSPK);
                                    }
                                }
                            } else {
                                $scope.mDistribusiSPK = {
                                    Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                                    SalesId: $scope.sales[j].EmployeeId,
                                    SalesCode: $scope.sales[j].EmployeeCode,
                                    SalesName: $scope.sales[j].EmployeeName,
                                    FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                                    ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                                    Qty: 1,
                                    Keterangan: 'Valid.',
                                    valid: true

                                }
                                $scope.tempDistribusiSpkValid.push($scope.mDistribusiSPK);
                            }
                        }
                        $scope.isExist++;
                        break;
                    }
                    if ($scope.isExist == 0 && $scope.isLoop == $scope.sales.length) {
                        $scope.mDistribusiSPK = {
                            Baris: $scope.DistribusiSpk_ExcelData[i].Baris,
                            SalesId: '',
                            SalesCode: $scope.DistribusiSpk_ExcelData[i].KodeSalesman,
                            SalesName: '',
                            FormSPKNo: $scope.DistribusiSpk_ExcelData[i].NoSpk,
                            ReceiveDate: $scope.DistribusiSpk_ExcelData[i].TanggalSpk,
                            Qty: 1,
                            Keterangan: 'Sales tidak ada.',
                            valid: false
                        }
                        $scope.tempDistribusiSpkinValid.push($scope.mDistribusiSPK);
                    }
                }
            }

            var tempmergelistSpk = [];
            DistributionSpkFactory.cekNoSPK($scope.tempDistribusiSpkValid).then(function (res) {
                var temSameSPK = res.data.Result;
                if (temSameSPK.length > 0) {
                    for (var i in temSameSPK) {
                        $scope.tempDistribusiSpkinValid.push({
                            Baris: temSameSPK[i].Baris,
                            SalesId: temSameSPK[i].SalesId,
                            SalesCode: temSameSPK[i].SalesCode,
                            SalesName: temSameSPK[i].SalesName,
                            FormSPKNo: temSameSPK[i].FormSPKNo,
                            ReceiveDate: temSameSPK[i].ReceiveDate,
                            Qty: temSameSPK[i].Qty,
                            Keterangan: temSameSPK[i].Keterangan,
                            valid: false
                        });
                    }

                    for (var i in $scope.tempDistribusiSpkValid) {
                        for (var j in $scope.tempDistribusiSpkinValid) {
                            if ($scope.tempDistribusiSpkValid[i].Baris == $scope.tempDistribusiSpkinValid[j].Baris) {
                                $scope.tempDistribusiSpkValid.splice(i, 1);
                            }
                        }
                    }
                }

                //var tempDistribusiSpkValidmerge = angular.copy($scope.tempDistribusiSpkValid);
                //var tempDistribusiSpkinValidmerge = angular.copy($scope.tempDistribusiSpkinValid);
                $scope.totalvalid = $scope.tempDistribusiSpkValid.length;
                $scope.totalinvalid = $scope.tempDistribusiSpkinValid.length;

                // tempmergelistSpk = _.merge([],tempDistribusiSpkValidmerge, tempDistribusiSpkinValidmerge);

                for (var i in $scope.tempDistribusiSpkValid) {
                    tempmergelistSpk.push($scope.tempDistribusiSpkValid[i]);
                }

                for (var i in $scope.tempDistribusiSpkinValid) {
                    tempmergelistSpk.push($scope.tempDistribusiSpkinValid[i]);
                }



                $scope.tempmergelistSpkshort = $filter('orderBy')(tempmergelistSpk, 'Baris', false);
                $scope.gridList.data = angular.copy($scope.tempmergelistSpkshort);

                if ($scope.tempDistribusiSpkinValid.length > 0) {
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Data tidak valid.",
                        type: 'warning'
                    });
                    $scope.isComplete = false;
                } else {
                    $scope.isComplete = true;
                }

                console.log("valid", $scope.tempDistribusiSpkValid);
                console.log("invalid", $scope.tempDistribusiSpkinValid);

            });

        }
        //==========================================================================//
        // 							Excel Upload End								//
        //==========================================================================//

        //==========================================================================//
        // 							Excel Download Begin							//
        //==========================================================================//

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = ('0' + date.getDate()).slice(-2) + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear()
                    ;
                return fix;
            } else {
                return '';
            }
        };


        $scope.DistribusiSPK_DownloadExcelTemplate = function () {
            var excelData = [];
            var fileName = "";
            // for(var i in $scope.sales){
            //     excelData.push({
            //         "KodeSalesman": $scope.sales[i].EmployeeCode,
            //         "NamaSales" : $scope.sales[i].EmployeeName,
            //         "NoSpk": "",
            //         "TanggalSpk": new Date()
            //     });
            // }

            // for(var i in excelData){
            //     excelData[i].TanggalSpk = fixDate(excelData[i].TanggalSpk);
            // }
            excelData = [{ "KodeSalesman": "", "NoSpk": "", "TanggalSpk": new Date() }];
            excelData[0].TanggalSpk = fixDate(excelData[0].TanggalSpk);
            fileName = "DistribusiSpk_Template";

            XLSXInterface.writeToXLSX(excelData, fileName);
        }
        //==========================================================================//
        // 							Excel Download End								//
        //==========================================================================//


        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
			enableColumnResizing: true,
            // paginationPageSizes: null,
            // useCustomPagination: true,
            // useExternalPagination: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'FormSPKId', visible: false },
                { name: 'no sales', field: 'SalesCode', width: '10%' },
                { name: 'nama sales', field: 'SalesName' },
                //{ displayName:'No SPK' ,name: 'no spk', cellTemplate: '<p style="margin:5px 0 0 5px">{{row.entity.FormSPKNoFrom}} - {{row.entity.FormSPKNoTo}}</p>', width: '30%' },
                { displayName: 'No SPK', name: 'no spk', field: 'FormSPKNoFromTo', width: '30%' },
                { displayName: 'Qty. SPK', name: 'Qty. spk', field: 'QtySPK', width: '12%' },
                { name: 'tanggal terima', field: 'ReceiveDate', width: '12%', cellFilter: 'date:\'dd-MM-yyyy\'' }

            ],
            onRegisterApi: function (gridApi) {
                $scope.gridAwal = gridApi;
                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                    $scope.getData(pageNumber);
                });
            }

        };

        $scope.gridList = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            // paginationPageSizes: null,
            // useCustomPagination: true,
            // useExternalPagination: true,
            paginationPageSizes: [10, 25, 50],
            // paginationPageSize: 15,
            columnDefs: [
                { name: '', width: '5%', field: 'valid', cellTemplate: '<div style="padding-left:15px;padding-top:2px"><i ng-class="row.entity.valid == true? \'fa fa-2x fa-check-circle green\':\'fa fa-2x fa-times-circle red\'"></i></div>' },
                { name: 'kode salesman', field: 'SalesCode', width: '11%' },
                { name: 'nama sales', field: 'SalesName', width: '26%' },
                { displayName: 'No SPK', name: 'no spk', field: 'FormSPKNo', width: '15%' },
                { name: 'tanggal terima', field: 'ReceiveDate', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'keterangan', field: 'Keterangan' }
            ]

        };
    });