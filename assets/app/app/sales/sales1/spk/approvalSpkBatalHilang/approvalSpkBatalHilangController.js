angular.module('app')
.controller('ApprovalSpkBatalHilangController', function($scope, $http, CurrentUser, ApprovalSpkBatalHilangFactory,$timeout,bsNotify) {
	$scope.ApprovalSpkBatalHilangMain=true;
	IfGotProblemWithModal();
	$scope.ApprovalSpkBatalHilangMultipleReject=false;
	
	$scope.ApprovalSpkBatalHilangSearchCriteriaStore="";
	$scope.ApprovalSpkBatalHilangSearchValueStore="";
	$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
	$scope.ApprovalSpkBatalHilangHideIncrement=false;
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.optionApprovalSpkBatalHilangSearchCriteria = ApprovalSpkBatalHilangFactory.GetApprovalSpkBatalHilangSearchDropdownOptions();
		$scope.optionApprovalSpkBatalHilangSearchLimit = ApprovalSpkBatalHilangFactory.getDataIncrementLimit();
		//$scope.ApprovalSpkBatalHilangSearchTier=$scope.optionApprovalSpkBatalHilangSearchLimit[0];
		$scope.ApprovalSpkBatalHilangSearchLimit = $scope.optionApprovalSpkBatalHilangSearchLimit[0].SearchOptionId; 
		$scope.ApprovalSpkBatalHilangSearchCriteria = $scope.optionApprovalSpkBatalHilangSearchCriteria[0].SearchOptionId;
        //$scope.mApprovalSpkBatalHilang = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        ApprovalSpkBatalHilangFactory.getData().then(
			function(res){
				$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
				$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
				if(res.data.Total<=5)
				{
					$scope.ApprovalSpkBatalHilangHideIncrement=true;
				}
				return res.data;
			},
			function(err){
				console.log("err=>",err);
			}
		);
		
		$scope.ApprovalSpkBatalHilangRefresh = function () {
			$scope.ApprovalSpkBatalHilangHideIncrement=false;
			ApprovalSpkBatalHilangFactory.getDataFilter($scope.ApprovalSpkBatalHilangSearchLimit).then(
				function(res){
					
					$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
					$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
					$scope.ApprovalSpkBatalHilangSearchValue="";
					$scope.ResetSelection();
					if(res.data.Total<=5)
					{
						$scope.ApprovalSpkBatalHilangHideIncrement=true;
					}
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
		$scope.ApprovalSpkBatalHilangIncrementLimit = function () {
			//$scope.ApprovalSpkBatalHilangSearchTier++;
			//$scope.ApprovalSpkBatalHilangIncrementStart=($scope.ApprovalSpkBatalHilangSearchTier*$scope.ApprovalSpkBatalHilangSearchLimit)+1;
			$scope.ApprovalSpkBatalHilangHideIncrement=false;

			if($scope.ApprovalSpkBatalHilangSearchIsPressed==false)
			{	
				var masukin=angular.copy($scope.ApprovalSpkBatalHilangGabungan.length)+1;
				ApprovalSpkBatalHilangFactory.getDataIncrement(masukin,$scope.ApprovalSpkBatalHilangSearchLimit).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalSpkBatalHilangGabungan.push(res.data.Result[i]);
						}
						var ApprovalSpkBatalHilangDaIncrementBablas=masukin+$scope.ApprovalSpkBatalHilangSearchLimit;
						
						if(ApprovalSpkBatalHilangDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalSpkBatalHilangHideIncrement=true;
						}
						
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			if($scope.ApprovalSpkBatalHilangSearchIsPressed==true)
			{
				//console.log("ulala",$scope.ApprovalSpkBatalHilangGabungan.length)
				var masukin=angular.copy($scope.ApprovalSpkBatalHilangGabungan.length)+1;
				ApprovalSpkBatalHilangFactory.getDataSearchApprovalSpkBatalHilangIncrement(masukin,$scope.ApprovalSpkBatalHilangSearchLimit,$scope.ApprovalSpkBatalHilangSearchCriteriaStore,$scope.ApprovalSpkBatalHilangSearchValueStore).then(
					function(res){
						for(var i = 0; i < res.data.Result.length; ++i)
						{
							$scope.ApprovalSpkBatalHilangGabungan.push(res.data.Result[i]);
						}
						// $scope.ApprovalSpkBatalHilangSearchCriteriaStore=angular.copy($scope.ApprovalSpkBatalHilangSearchCriteria);
						// $scope.ApprovalSpkBatalHilangSearchValueStore=angular.copy($scope.ApprovalSpkBatalHilangSearchValue);
						
						var ApprovalSpkBatalHilangDaIncrementBablas=masukin+$scope.ApprovalSpkBatalHilangSearchLimit;
						if(ApprovalSpkBatalHilangDaIncrementBablas>=res.data.Total)
						{
							$scope.ApprovalSpkBatalHilangHideIncrement=true;
						}
						return res.data;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}
			
			
			

		};
		
		$scope.FromListSetujuApprovalSpkBatalHilang = function () {
			//console.log("kurwa",$scope.selectionApprovalSpkBatalHilang);
			
			for(var i = 0; i < $scope.selectionApprovalSpkBatalHilang.length; ++i)
			{
				var ApprovalSpkBatalHilangPaksaRequestDateMs=$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
				var ApprovalSpkBatalHilangPaksaRequestDateMsParent='0';
				
				if(ApprovalSpkBatalHilangPaksaRequestDateMs<10)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParent='00'+$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
				}
				else if(ApprovalSpkBatalHilangPaksaRequestDateMs<100)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParent='0'+$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
				}
				else
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParent=''+(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds()<'100'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds());
				}
				
				$scope.selectionApprovalSpkBatalHilang[i].RequestDate=$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getFullYear()+'-'
												  +('0' + ($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getDate()).slice(-2)+'T'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getHours()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getHours())+':'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMinutes()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMinutes())+':'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getSeconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getSeconds())+'.'
												  +ApprovalSpkBatalHilangPaksaRequestDateMsParent
												  
				for(var x = 0; x < $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView.length; ++x)
				{
					var ApprovalSpkBatalHilangPaksaChildRequestDateMs=$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
					var ApprovalSpkBatalHilangPaksaChildRequestDateMsChild='0';
					
					if(ApprovalSpkBatalHilangPaksaChildRequestDateMs<10)
					{
						ApprovalSpkBatalHilangPaksaChildRequestDateMsChild='00'+$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
					}
					else if(ApprovalSpkBatalHilangPaksaChildRequestDateMs<100)
					{
						ApprovalSpkBatalHilangPaksaChildRequestDateMsChild='0'+$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
					}
					else
					{
						ApprovalSpkBatalHilangPaksaChildRequestDateMsChild=''+(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds()<'100'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds());
					}
					
					$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate=$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getFullYear()+'-'
													  +('0' + ($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMonth()+1)).slice(-2)+'-'
													  +('0' + $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getDate()).slice(-2)+'T'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getHours()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getHours())+':'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMinutes()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMinutes())+':'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getSeconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getSeconds())+'.'
													  +ApprovalSpkBatalHilangPaksaChildRequestDateMsChild;
				}
			}
			
			ApprovalSpkBatalHilangFactory.SetujuApprovalSpkBatalHilangDariList($scope.selectionApprovalSpkBatalHilang).then(function () {
						ApprovalSpkBatalHilangFactory.getDataFilter($scope.ApprovalSpkBatalHilangSearchLimit).then(
							function(res){
							
								bsNotify.show(
									{
										title: "Message",
										content: "Data Di Approve",
										type: 'warning'
									}
								);
							
								$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
								$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalApprovalSpkBatalHilang();
								$scope.ResetSelection();
								$scope.ApprovalSpkBatalHilangHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSpkBatalHilangHideIncrement=true;
								}
								$scope.ApprovalSpkBatalHilangMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
		};
		
		$scope.FromListTidakSetujuApprovalSpkBatalHilang = function () {
			$scope.ApprovalSpkBatalHilangMultipleReject=true;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSpkBatalHilang').modal('setting',{closable:false}).modal('show');
		};
        
		$scope.TidakSetujuApprovalSpkBatalHilang = function () {
			$scope.ApprovalSpkBatalHilangMultipleReject=false;
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSpkBatalHilang').modal('setting',{closable:false}).modal('show');
		};
		
		$scope.ActionApprovalSpkBatalHilang = function (SelectedData) {
			$scope.ApprovalSpkBatalHilangMultipleReject=false;
			$scope.mApprovalSpkBatalHilang=angular.copy(SelectedData);
			angular.element('.ui.modal.ModalActionApprovalSpkBatalHilang').modal('setting',{closable:false}).modal('show');
		}
		
		$scope.BatalActionApprovalSpkBatalHilang = function () {
			angular.element('.ui.modal.ModalActionApprovalSpkBatalHilang').modal('hide');
			$scope.mApprovalSpkBatalHilang={};
		}
		
		$scope.SelectApprovalSpkBatalHilang = function () {
			
			$scope.ApprovalSpkBatalHilangMain=false;
			$scope.ApprovalSpkBatalHilangDetail=true;
			
			if($scope.mApprovalSpkBatalHilang.Batal==true)
			{
				$scope.ApprovalSpkBatalHilang_Hilang=false;
				$scope.ApprovalSpkBatalHilang_Batal=true;
				
				ApprovalSpkBatalHilangFactory.getDaRincian($scope.mApprovalSpkBatalHilang.SPKId).then(
					function(res){
						$scope.selected_data = res.data.Result[0];
						return res.data;
					}
				);
				
			}
			else if($scope.mApprovalSpkBatalHilang.Batal==false)
			{
				$scope.ApprovalSpkBatalHilang_Hilang=true;
				$scope.ApprovalSpkBatalHilang_Batal=false;
			}
			
			angular.element('.ui.modal.ModalActionApprovalSpkBatalHilang').modal('hide');
		}
		
		$scope.BatalApprovalSpkBatalHilang = function () {
			$scope.mApprovalSpkBatalHilang ={};
			$scope.ApprovalSpkBatalHilangMain=true;
			$scope.ApprovalSpkBatalHilangDetail=false;
			angular.element('.ui.modal.ModalActionApprovalSpkBatalHilang').modal('hide');
		}
		
		$scope.BatalAlasanApprovalSpkBatalHilang = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalSpkBatalHilang').modal('hide');
			//$scope.mApprovalSpkBatalHilang={};
		}
		
		$scope.SetujuApprovalSpkBatalHilang = function () {
		
				var ApprovalSpkBatalHilangPaksaRequestDateMsNotFromList=$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				var ApprovalSpkBatalHilangPaksaRequestDateMsParentNotFromList='0';
				
				if(ApprovalSpkBatalHilangPaksaRequestDateMsNotFromList<10)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParentNotFromList='00'+$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				}
				else if(ApprovalSpkBatalHilangPaksaRequestDateMsNotFromList<100)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParentNotFromList='0'+$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				}
				else
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsParentNotFromList=''+(($scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds());
				}
			
			$scope.mApprovalSpkBatalHilang.RequestDate=$scope.mApprovalSpkBatalHilang.RequestDate.getFullYear()+'-'
												  +('0' + ($scope.mApprovalSpkBatalHilang.RequestDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.mApprovalSpkBatalHilang.RequestDate.getDate()).slice(-2)+'T'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getHours()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getHours())+':'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getMinutes()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getMinutes())+':'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getSeconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getSeconds())+'.'
												  +ApprovalSpkBatalHilangPaksaRequestDateMsParentNotFromList;
			
			for(var i = 0; i < $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView.length; ++i)
			{
				var ApprovalSpkBatalHilangPaksaRequestDateMsNotFromListChild=$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
				var ApprovalSpkBatalHilangPaksaRequestDateMsChildNotFromListChild='0';
				
				if(ApprovalSpkBatalHilangPaksaRequestDateMsNotFromListChild<10)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsChildNotFromListChild='00'+$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
				}
				else if(ApprovalSpkBatalHilangPaksaRequestDateMsNotFromListChild<100)
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsChildNotFromListChild='0'+$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
				}
				else
				{
					ApprovalSpkBatalHilangPaksaRequestDateMsChildNotFromListChild=''+(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds());
				}
				
				$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate=$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getFullYear()+'-'
												  +('0' + ($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getDate()).slice(-2)+'T'
												  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getHours()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getHours())+':'
												  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMinutes()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMinutes())+':'
												  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getSeconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getSeconds())+'.'
												  +ApprovalSpkBatalHilangPaksaRequestDateMsChildNotFromListChild;
			}
			
			ApprovalSpkBatalHilangFactory.SetujuApprovalSpkBatalHilang($scope.mApprovalSpkBatalHilang).then(function () {
					ApprovalSpkBatalHilangFactory.getData().then(
						function(res){
						
							bsNotify.show(
									{
										title: "Message",
										content: "Data Di Approve",
										type: 'warning'
									}
								);
						
							$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
							$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
							$scope.loading=false;
							$scope.BatalApprovalSpkBatalHilang();
							$scope.ResetSelection();
							$scope.ApprovalSpkBatalHilangHideIncrement=false;
							if(res.data.Total<=5)
							{
								$scope.ApprovalSpkBatalHilangHideIncrement=true;
							}
							$scope.ApprovalSpkBatalHilangMultipleReject=false;
							
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				});

		};
		
		$scope.SubmitAlasanApprovalSpkBatalHilang = function () {
			if($scope.ApprovalSpkBatalHilangMultipleReject==false)
			{
				for(var x = 0; x < $scope.mApprovalSpkBatalHilang['ListOfSAHApprovalBatalHilangSPKView'].length; ++x)
				{
					$scope.mApprovalSpkBatalHilang['ListOfSAHApprovalBatalHilangSPKView'][x]['ApprovalNote']=angular.copy($scope.mApprovalSpkBatalHilang.ApprovalNote);
				}
				
				var TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsGaDariList=$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				var TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParentGaDariList='0';
				
				if(TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsGaDariList<10)
				{
					TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParentGaDariList='00'+$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				}
				else if(TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsGaDariList<100)
				{
					TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParentGaDariList='0'+$scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds();
				}
				else
				{
					TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParentGaDariList=''+(($scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getMilliseconds());
				}
				
				$scope.mApprovalSpkBatalHilang.RequestDate=$scope.mApprovalSpkBatalHilang.RequestDate.getFullYear()+'-'
												  +('0' + ($scope.mApprovalSpkBatalHilang.RequestDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.mApprovalSpkBatalHilang.RequestDate.getDate()).slice(-2)+'T'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getHours()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getHours())+':'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getMinutes()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getMinutes())+':'
												  +(($scope.mApprovalSpkBatalHilang.RequestDate.getSeconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.RequestDate.getSeconds())+'.'
												  +TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParentGaDariList;
				
				for(var i = 0; i < $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView.length; ++i)
				{
					
					var TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsGaDariList=$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
					var TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChildGaDariList='0';
					
					if(TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsGaDariList<10)
					{
						TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChildGaDariList='00'+$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
					}
					else if(TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsGaDariList<100)
					{
						TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChildGaDariList='0'+$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds();
					}
					else
					{
						TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChildGaDariList=''+(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMilliseconds());
					}
					
					$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate=$scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getFullYear()+'-'
													  +('0' + ($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMonth()+1)).slice(-2)+'-'
													  +('0' + $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getDate()).slice(-2)+'T'
													  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getHours()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getHours())+':'
													  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMinutes()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getMinutes())+':'
													  +(($scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getSeconds()<'10'?'0':'')+ $scope.mApprovalSpkBatalHilang.ListOfSAHApprovalBatalHilangSPKView[i].RequestDate.getSeconds())+'.'
													  +TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChildGaDariList;
				}
				
				ApprovalSpkBatalHilangFactory.TidakSetujuApprovalSpkBatalHilang($scope.mApprovalSpkBatalHilang).then(function () {
						ApprovalSpkBatalHilangFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Message",
										content: "Data Di Reject",
										type: 'warning'
									}
								);
							
								$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
								$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalSpkBatalHilang();
								$scope.BatalApprovalSpkBatalHilang();
								$scope.ResetSelection();
								$scope.ApprovalSpkBatalHilangHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSpkBatalHilangHideIncrement=true;
								}
								$scope.ApprovalSpkBatalHilangMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
			else if($scope.ApprovalSpkBatalHilangMultipleReject==true)
			{
			
				for(var i = 0; i < $scope.selectionApprovalSpkBatalHilang.length; ++i)
				{
					var TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMs=$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
					var TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParent='0';
					
					if(TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMs<10)
					{
						TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParent='00'+$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
					}
					else if(TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMs<100)
					{
						TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParent='0'+$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds();
					}
					else
					{
						TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParent=''+(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMilliseconds());
					}
					
					$scope.selectionApprovalSpkBatalHilang[i].RequestDate=$scope.selectionApprovalSpkBatalHilang[i].RequestDate.getFullYear()+'-'
												  +('0' + ($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getDate()).slice(-2)+'T'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getHours()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getHours())+':'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMinutes()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getMinutes())+':'
												  +(($scope.selectionApprovalSpkBatalHilang[i].RequestDate.getSeconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].RequestDate.getSeconds())+'.'
												  +TidakSetujuApprovalSpkBatalHilangPaksaRequestDateMsParent;
					
					for(var x = 0; x < $scope.selectionApprovalSpkBatalHilang[i]['ListOfSAHApprovalBatalHilangSPKView'].length; ++x)
					{
						$scope.selectionApprovalSpkBatalHilang[i]['ListOfSAHApprovalBatalHilangSPKView'][x]['ApprovalNote']=angular.copy($scope.mApprovalSpkBatalHilang.ApprovalNote);
						
						var TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMs=$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
						var TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChild='0';
						
						if(TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMs<10)
						{
							TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChild='00'+$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
						}
						else if(TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMs<100)
						{
							TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChild='0'+$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds();
						}
						else
						{
							TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChild=''+(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMilliseconds());
						}
						
						$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate=$scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getFullYear()+'-'
													  +('0' + ($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMonth()+1)).slice(-2)+'-'
													  +('0' + $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getDate()).slice(-2)+'T'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getHours()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getHours())+':'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMinutes()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getMinutes())+':'
													  +(($scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getSeconds()<'10'?'0':'')+ $scope.selectionApprovalSpkBatalHilang[i].ListOfSAHApprovalBatalHilangSPKView[x].RequestDate.getSeconds())+'.'
													  +TidakSetujuApprovalSpkBatalHilangPaksaChildRequestDateMsChild;
					}
					
					
				}
				
				
				ApprovalSpkBatalHilangFactory.TidakSetujuApprovalSpkBatalHilangDariList($scope.selectionApprovalSpkBatalHilang).then(function () {
						ApprovalSpkBatalHilangFactory.getData().then(
							function(res){
							
								bsNotify.show(
									{
										title: "Message",
										content: "Data Di Reject",
										type: 'warning'
									}
								);
							
								$scope.ApprovalSpkBatalHilangSearchIsPressed=false;
								$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalSpkBatalHilang();
								$scope.BatalApprovalSpkBatalHilang();
								$scope.ResetSelection();
								$scope.ApprovalSpkBatalHilangHideIncrement=false;
								if(res.data.Total<=5)
								{
									$scope.ApprovalSpkBatalHilangHideIncrement=true;
								}
								$scope.ApprovalSpkBatalHilangMultipleReject=false;
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
			}
		
			
		}

		$scope.ResetSelection = function () {
			$scope.selectionApprovalSpkBatalHilang=[{ OutletId: null,
											BookingUnitID: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalNegotiationDiscountId: null}];
			$scope.selectionApprovalSpkBatalHilang.splice(0, 1);
		}
		
		$scope.selectionApprovalSpkBatalHilang=[{ OutletId: null,
											BookingUnitID: null,
											ApprovalCategoryId: null,
											Seq: null,
											ApproverRoleId: null,
                                            ApprovalNegotiationDiscountId: null}];
		$scope.selectionApprovalSpkBatalHilang.splice(0, 1);
		
		$scope.toggleSelection = function toggleSelection(SelectedApprovalSpkBatalHilangFromList) {
			var idx = $scope.selectionApprovalSpkBatalHilang.indexOf(SelectedApprovalSpkBatalHilangFromList);

			// Is currently selected
			if (idx > -1) {
			  $scope.selectionApprovalSpkBatalHilang.splice(idx, 1);
			}

			// Is newly selected
			else {
			  $scope.selectionApprovalSpkBatalHilang.push(SelectedApprovalSpkBatalHilangFromList);
			}

			$scope.SelectionAman_ApprovalSpkBatalHilang=true;
			for(var i = 0; i < $scope.selectionApprovalSpkBatalHilang.length; ++i)
			{
				if($scope.selectionApprovalSpkBatalHilang[i].HeaderStatusApprovalName!="Diajukan")
				{
					$scope.SelectionAman_ApprovalSpkBatalHilang=false;
				}
			}
		  };
		  
		$scope.SearchApprovalSpkBatalHilang = function () {
			$scope.ApprovalSpkBatalHilangHideIncrement=false;
			try
			{
				if($scope.ApprovalSpkBatalHilangSearchValue!=""&& (typeof $scope.ApprovalSpkBatalHilangSearchValue!="undefined"))
				{
					
					ApprovalSpkBatalHilangFactory.getDataSearchApprovalSpkBatalHilang($scope.ApprovalSpkBatalHilangSearchLimit,$scope.ApprovalSpkBatalHilangSearchCriteria,$scope.ApprovalSpkBatalHilangSearchValue).then(function (res) {
						
						$scope.ApprovalSpkBatalHilangSearchIsPressed=true;
						$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
						if($scope.ApprovalSpkBatalHilangSearchLimit>=res.data.Total)
						{
							$scope.ApprovalSpkBatalHilangHideIncrement=true;
						}
						$scope.ResetSelection();
						$scope.ApprovalSpkBatalHilangSearchCriteriaStore=angular.copy($scope.ApprovalSpkBatalHilangSearchCriteria);
						$scope.ApprovalSpkBatalHilangSearchValueStore=angular.copy($scope.ApprovalSpkBatalHilangSearchValue);
					});
				}
				else
				{
					ApprovalSpkBatalHilangFactory.getData().then(function (res) {
						$scope.ApprovalSpkBatalHilangSearchIsPressed=true;
						$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
						
						if(res.data.Total<=5)
						{
							$scope.ApprovalSpkBatalHilangHideIncrement=true;
						}
						
						$scope.ResetSelection();
					});
				}
			}
			catch(e)
			{
				ApprovalSpkBatalHilangFactory.getData().then(function (res) {
					$scope.ApprovalSpkBatalHilangSearchIsPressed=true;
					$scope.ApprovalSpkBatalHilangGabungan = res.data.Result;
					
					if(res.data.Total<=5)
					{
						$scope.ApprovalSpkBatalHilangHideIncrement=true;
					}
					
					$scope.ResetSelection();
				});
			}
			

        }  

});
