angular.module('app')
  .factory('ApprovalSpkBatalHilangFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		var res=$http.get('/api/sales/SPKApprovalBatalHilang?start=1&limit=5&FilterData=HeaderStatusApprovalName|Diajukan');
		return res;
      },
      getDataFilter: function(limit) {
        var res=$http.get('/api/sales/SPKApprovalBatalHilang?start=1&limit='+limit+'&FilterData=HeaderStatusApprovalName|Diajukan');
        return res;
      },
	  
	  getDaRincian: function(spkId) {
		var res=$http.get('/api/sales/SAHSPKDetail/?spkId='+spkId);
		return res;
        
      },
	  
	  getDataIncrement: function(Start,Limit) {
		var res=$http.get('/api/sales/SPKApprovalBatalHilang?start='+Start+'&limit='+Limit+'&FilterData=HeaderStatusApprovalName|Diajukan');
		return res;
        
      },
	  
	  getDataSearchApprovalSpkBatalHilangIncrement: function(Start,limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/SPKApprovalBatalHilang/?start='+Start+'&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  getDataIncrementLimit: function () {
        
        var da_json = [
          { SearchOptionId: 5, SearchOptionName: "5" },
		  { SearchOptionId: 10, SearchOptionName: "10" },
		  { SearchOptionId: 20, SearchOptionName: "20" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetApprovalSpkBatalHilangSearchDropdownOptions: function () {
        
        var da_json = [
          { SearchOptionId: "FormSPKNo", SearchOptionName: "No. SPK" },
		  { SearchOptionId: "ProspectName", SearchOptionName: "Nama pelanggan" },
		  { SearchOptionId: "VehicleTypeDescription", SearchOptionName: "Model tipe" },
		  { SearchOptionId: "Salesman", SearchOptionName: "Sales" },
		  { SearchOptionId: "HeaderStatusApprovalName", SearchOptionName: "Status" }
        ];

        var res=da_json

        return res;
      },
	  
	  
	  getDataSearchApprovalSpkBatalHilang: function(limit,SearchBy,SearchValue) {
		var res=$http.get('/api/sales/SPKApprovalBatalHilang/?start=1&limit='+limit+'&FilterData='+SearchBy+'|'+SearchValue);
		return res;
        
      },
	  
	  SetujuApprovalSpkBatalHilang: function(ApprovalSpkBatalHilang){
		return $http.put('/api/sales/SPKApprovalBatalHilang/Approve', [ApprovalSpkBatalHilang]);									
      },
	  
	  SetujuApprovalSpkBatalHilangDariList: function(ApprovalSpkBatalHilang){
		return $http.put('/api/sales/SPKApprovalBatalHilang/Approve', ApprovalSpkBatalHilang);									
      },
	  
	  TidakSetujuApprovalSpkBatalHilang: function(ApprovalSpkBatalHilang){
		return $http.put('/api/sales/SPKApprovalBatalHilang/Reject', [ApprovalSpkBatalHilang]);									
      },
	  TidakSetujuApprovalSpkBatalHilangDariList: function(ApprovalSpkBatalHilang){
		return $http.put('/api/sales/SPKApprovalBatalHilang/Reject', ApprovalSpkBatalHilang);									
      },
	  
      create: function(ApprovalSpkBatalHilang) {
        return $http.post('/api/sales/SPKApprovalBatalHilang', [{
                                            SalesProgramName: ApprovalSpkBatalHilang.SalesProgramName}]);
      },
      update: function(ApprovalSpkBatalHilang){
        return $http.put('/api/sales/SPKApprovalBatalHilang', [{
                                            SalesProgramId: ApprovalSpkBatalHilang.SalesProgramId,
                                            SalesProgramName: ApprovalSpkBatalHilang.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/SPKApprovalBatalHilang',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd