angular.module('app')
    .controller('CekSpkValidDiskonController', function(MobileHandling,bsNotify, $scope, $http, CurrentUser, CekSpkValidDiskonFactory, $timeout, uiGridConstants, uiGridGroupingConstants) {
        //----------------------------------
        // Start-Up
        //-----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
              $scope.bv=MobileHandling.browserVer();
              $scope.bver=MobileHandling.getBrowserVer();
              //console.log("browserVer=>",$scope.bv);
              //console.log("browVersion=>",$scope.bver);
              //console.log("tab: ",$scope.tab);
              if($scope.bv!=="Chrome"){
              if ($scope.bv=="Unknown") {
                if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                {
                  $scope.removeTab($scope.tab.sref);
                }
              }        
              }
            
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mcekSpkValidDiskon = null; //Model
        var mount = new Date();
        var year = mount.getFullYear();
        var mounth = mount.getMonth();
        var lastdate = new Date(year, mounth+1, 0).getDate();

        $scope.firstDate = new Date( mount.setDate("01"));
        $scope.lastDate = new Date (mount.setDate(lastdate));

        $scope.filter = {SPKDateStart : $scope.firstDate, SPKDateEnd : new Date(), HeaderStatusApprovalId: null};

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date()
        };

         $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

         $scope.tanggalmin = function (tgl){
            $scope.dateOptionsEnd.minDate = tgl;
            if(tgl == null || tgl == undefined){
                $scope.filter.SPKDateEnd = null;
            }else{
                if($scope.filter.SPKDateStart < $scope.filter.SPKDateEnd){
                    //break;
                }else{
                    $scope.filter.SPKDateEnd = $scope.filter.SPKDateStart;
                }
                
            }
            
        }

        CekSpkValidDiskonFactory.getDataStatusDiscount().then(function (res){
            $scope.statusdiskon = res.data.Result;
        });

        // var gridData = [];
        $scope.getData = function() {
            if (($scope.filter.SPKDateStart == null || $scope.filter.SPKDateEnd == null) || ($scope.filter.SPKDateStart == undefined || $scope.filter.SPKDateEnd == undefined)){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Isi range tanggal.",
                    type: 'warning'
                });
            }else{
                CekSpkValidDiskonFactory.getData($scope.filter)
                .then(
                    function(res) {
                        $scope.grid.data = [];
                        // gridData = [];
                        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);

                        var temp = [];
                         temp = res.data.Result;

                        for (var i in temp){
                            for (var j in temp[i].ListOfSAHApprovalDiscountSPKView){
                                 $scope.grid.data.push(temp[i].ListOfSAHApprovalDiscountSPKView[j]);
                            }
                        }

                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
            );
            }

        }

      
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

       //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableColumnMenus: true,
            enableGroupHeaderSelection: false,
            enableRowHeaderSelection: false,
            enableSorting: true,
            //enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 30,

            columnDefs: [
                { name: 'No. spk', 
                  field: 'FormSPKNo',
                  grouping: { groupPriority: 0 },
                  sort: { priority: 0, direction: 'asc' }, 
                  width:'18%',
                  cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'tanggal pengajuan', 
                  field: 'SpkDate', width:'8%', 
                  cellFilter: 'date:\'dd-MM-yyyy\'',
                  
                  //treeAggregationType: uiGridGroupingConstants.aggregation.MAX,
                  //cellTemplate: '<div class="ui-grid-cell-contents" > <fakemax val="{{COL_FIELD}}" /></div>'
                },
                { name: 'Menyetujui', field: 'ApproverName', width:'15%' },
                { name: 'jabatan', field: 'ApproverRoleName', width:'15%' },
                { name: 'tanggal approval', field: 'ApprovalDate', width:'8%', cellFilter: 'date:\'dd-MM-yyyy\''},
                //{ name: 'status pengajuan', cellTemplate: '<input class="ui-grid-cell-contents"  type="checkbox" name="polwil" ng-disabled="true" ng-checked="row.entity.IsRejected==true">' },
                { name: 'Disetujui',width:'7%', cellTemplate: '<input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" name="approval" ng-disabled="true" ng-checked="row.entity.StatusApprovalName == \'Disetujui\'">' },
                { name: 'Ditolak',width:'7%', cellTemplate: '<input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" name="polwil" ng-disabled="true" ng-checked="row.entity.StatusApprovalName == \'Ditolak\'">' },
                { name: 'Nilai', cellTemplate: '<div style="float:right"><p style="padding:8px 5px 0 5px">{{row.entity.RequestTotalDiscount | number:0}}</p><div>'},
                // field: 'Discount' },
                { name: 'status', 
                //field: 'StatusApprovalName',
                 //aggregationHideLabel:true,
                    aggregationType: uiGridConstants.aggregationTypes.MAX,
                //     customtreeAggregationFinalizerFn: function( aggregation ) {
                //     aggregation.rendered = '{{row.entity.HeaderStatusApprovalName}}'}
                // }
                    cellTemplate: '<p style="padding:0px 0 0 5px"><b ng-if="row.groupHeader">{{row.entity.HeaderStatusApprovalName}}</b></p>'+
                                '<p style="padding:0px 0 0 5px"><b ng-if="!row.groupHeader">{{row.entity.StatusApprovalName}}</b></p>'}
                    //<div class="ui-grid-cell-contents" ng-if="row.groupHeader">{{row.entity.HeaderStatusApprovalName}}</div>'+
                   // '<div class="ui-grid-cell-contents" ng-if="!row.groupHeader">{{row.entity.StatusApprovalName}}</div> '}
               // { name: 'status diskon', field: 'StatusApprovalDiscountName' }

            ]
        };

    });

app.directive('fakemax', function () {
    return function(scope, iElement, iAttrs) {
        var value = iAttrs.val;
        // var year = new Date(value).getFullYear();
		// var mounth = ('0' + (new Date(value).getMonth() + 1)).slice(-2);
		// var date = ('0' + new Date(value).getDate()).slice(-2);
        // var full = date+'-'+mounth+'-'+year;
        // console.log("asdasds", full);
        // value = full;
        value = value.replace("max:", "");
        iElement[0].innerHTML = value;
    }
});