angular.module('app')
  .factory('TaskListFactory', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var res=$http.get('/api/as/TaskList');
        // return res;
        var defer = $q.defer();
            defer.resolve(
              {
                "data" : {
                  "Result":[{
                      "TaskId":1,
                      "GroupDealerCode":"T0A0",
                      "OutletCode":"A001",
                      "WMI":"MHK",
                      "VDS":"A4DE2F",
                      "OperationNo":"0D1A13",
                      "OperationDescription":"1.000 KM Service",
                      "ActivityTypeCode":"SB",
                      "FlatRate":1.0000,
                      "ActualRate":null,
                      "OperationUom":"H",
                      "ModelName":"Agya",
                      "FullModel":"B100LA-GMDFF",
                      "EffectiveDate":"2017/07/06",
                      "ValidThru":"2017/08/21",
                  }],
                  "Start":1,
                  "Limit":10,
                  "Total":1
                }
              }
            );
        return defer.promise;
      },
      create: function(TaskData) {        
        TaskData.TaskId = 0;
        // console.log("Data Retail : ",RetailData);
        return $http.post('/api/as/TaskList', [RetailData]);
      },
      update: function(TaskData){
        console.log("Data TaskList : ",TaskData);
        return $http.put('/api/as/TaskList', [TaskData]);
      },
      delete: function(id) {
        console.log("Data TaskList : ",id);
        return $http.delete('/api/as/TaskList',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //