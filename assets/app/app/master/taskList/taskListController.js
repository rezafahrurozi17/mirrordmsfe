angular.module('app')
    .controller('TaskListController', function($scope, $http, CurrentUser, TaskListFactory, OrgChart, GeneralMaster, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTaskList = null; //Model
    $scope.cTaskList = null; //Collection
    $scope.xTaskList = {};
    $scope.xTaskList.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    var mDataCopy;
    var outletObj = null;
    var outletData = [];
    $scope.orgData= [];

    $scope.validDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    $scope.noResults=false;

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                changeMode(processingMode);
                console.log("simpan");                
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];
                var x = new Date();
                var xdate = (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
                angular.forEach($scope.materialData, function(mtrl, mtrlIdx){
                    generatedData.push({EffectiveDate:xdate, ValidThru:xdate, PartId:mtrl.PartsId, Discount:0});
                });                
                XLSXInterface.writeToXLSX(generatedData, 'Template Task List');
            },
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                document.getElementById('uploadTask').click();
            },
            title: 'Load File',
            rightsBit: 1
        }
    ];

    function validateXLS(data){
        var result = [];
        var error = [];

        $scope.grid.data = [];
        $timeout(function(){
            for(var i=0; i<data.length; i++){     
                var dMaterial = _.find(materialData, { 'PartsCode': data[i].PartsCode });
                if(dMaterial == undefined){
                    error.push('Bahan / Part '+data[i].PartsCode+' tidak ditemukan.');
                    continue;
                }
                var newObj = {
                    EffectiveDate:data[i].EffectiveDate, 
                    ValidThru:data[i].ValidThru,
                    PartId:data[i].PartId,
                    Discount:data[i].Discount,
                };

                result.push(newObj);
            }            
        });
        
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadTask' ) );

        var a = myEl[0].files[0].name.split(".");
        var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        if (fileExt === "xlsx") {
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $timeout(function(){
                    myEl.value = '';
                    document.getElementById('uploadTask').value = '';
                    // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    $scope.grid.data = validateXLS(json);
                    mDataCopy =  angular.copy($scope.grid.data);

                    uploadMode = true;
                    changeMode(editMode);
                });
            });
        } else {
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "File yang diupload bukan format .xlsx",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );   
        }
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {        
        TaskListFactory.getData().then(
            function(res){
                console.log(res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };

    OrgChart.getDataTree().then(
        function(res){
            orgTreeData = res.data.Result;
            $scope.orgData = res.data.Result;
            if ($scope.orgData.length==1 && $scope.orgData[0].child.length==1) {
                $scope.filter.outletId = $scope.orgData[0].child[0].id;
            }
            for(i in $scope.orgData){
                for(ch in $scope.orgData[i].child){
                    outletData.push($scope.orgData[i].child[ch]);
                }
            }

            $scope.loading=false;
            return res.data;
        }
    );

    GeneralMaster.getData(1).then(
        function(res){
            $scope.UomData = res.data.Result;
            // console.log("$scope.UomData : ",$scope.UomData);
            return res.data;
        }
    );
  
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'WMI', field:'WMI' },
            { name:'VDS', field:'VDS' },
            { name:'Operation No', field:'OperationNo' },
            { name:'Description', field:'OperationDescription' },
            { name:'Tipe Pekerjaan', field:'ActivityTypeCode' },
            { name:'Flat Rate', field:'FlatRate' },
            { name:'Actual Rate', field:'ActualRate' },
            { name:'Operation UOM', displayName:'Operation UOM',field:'OperationUom' },
            { name:'Model Name', field:'ModelName' },
            { name:'Full Model', field:'FullModel' },
            { name:'Berlaku Mulai', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
        ]
    };
}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
}).filter('custCurrency', function () {
    return function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };
});
