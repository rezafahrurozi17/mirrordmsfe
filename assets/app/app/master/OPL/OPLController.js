
angular.module('app')
    .controller('OPLController', function($scope, $http, CurrentUser, OplFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });

 
    //----------------------------------
    // Initialization
    //----------------------------------

    $scope.user = CurrentUser.user();
    $scope.mOpl = null; //Model
    $scope.cOpl = null; //Collection
    $scope.xOpl = {};
    $scope.xOpl.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {        
        OplFactory.getData().then(
            function(res){
                console.log("res=>",res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
        // $scope.loading=false;
    };
    OplFactory.getVendor().then(
        function(res){
            $scope.VendorData= res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'Id', width:'7%', visible:false },
            { displayName:'Nama Pekerjaan OPL', field:'OPLWorkName' },
            { name:'Harga', field:'Price', cellFilter: 'custCurrency', cellClass: 'right-align' },
            // { name:'Id Vendor', field:'VendorId'},
            { name:'Vendor', field:'VendorName'},
            { name:'Alamat Penagihan', field:'BillingAddress' }
            
        ]
    };
}).filter('custCurrency', function () {
    return function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };
});

