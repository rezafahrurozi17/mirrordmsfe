angular.module('app')
  .factory('OplFactory', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/OPL');
        return res;
      },
      getVendor: function() {
        // var defer = $q.defer(); 
        //     defer.resolve(
        //       {
        //         "data" : {
        //           "Result":[{
        //               "idVendor":0,
        //               "Description":"Vendor A",
        //           },
        //           {
        //               "idVendor":1,
        //               "Description":"Vendor B",
        //           },
        //           {
        //               "idVendor":2,
        //               "Description":"Vendor C",
        //           }],
        //           "Start":1,
        //           "Limit":10,
        //           "Total":1
        //         }
        //       }
        //     );
        // return defer.promise;
        var res=$http.get('/api/as/Vendor');
        return res;
      },
      create: function(data) {
        data.CounterId = 0;
        data.isExternal = 0;
        return $http.post('/api/as/OPL', [data]);
      },
      update: function(data){
        return $http.put('/api/as/OPL', [data]);
      },
      delete: function(id) {
        console.log("idd=>",id);
        return $http.delete('/api/as/OPL', {data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });