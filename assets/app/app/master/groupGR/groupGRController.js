angular.module('app')
    .controller('GroupGRController', function($scope, $http, CurrentUser, GroupGRFactory, $timeout, ngDialog, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mGroupGR = null; //Model
        $scope.cGroupGR = null; //Collection
        $scope.xGroupGR = {};
        $scope.xGroupGR.selected = [];
        $scope.deletedTechnicianDetail = [];
        $scope.ButtonAdd = 1;
        $scope.formApi = {};

        $scope.typeGroupOpt = [
            { "text": "Internal", "value": 1 },
            { "text": "Vendor", "value": 2 }
        ];

        $scope.FullTechnicianData = [];
        $scope.techData = [];
        $scope.foreData = [];

        var getArrTechId = [];
        var getArrGrpTechId = [];
        var formMode;


        // var x = _.difference([1,2], [1,5,6]);
        // var y = _.difference([1,2], x);
        // var z = _.difference([1,5,6], y);
        // console.log(x);
        // console.log(y);
        // console.log(z);

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            console.log("get data..");
            GroupGRFactory.getData().then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    console.log("data=>", res.data);
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            )
        };

        GroupGRFactory.getTechnician().then(
            function(res) {
                $scope.techData = angular.copy(res.data.Result);
                for (var i in $scope.techData) {
                    $scope.techData[i].TheId = {
                        "TechnicianId": $scope.techData[i].EmployeeId,
                        "EmployeeName": $scope.techData[i].EmployeeName,
                        "Initial": $scope.techData[i].Initial,
                        "StatusCode": $scope.techData[i].StatusCode
                    };
                }

                $scope.FullTechnicianData = $scope.techData;

                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        GroupGRFactory.getForeman().then(
            function(res) {
                $scope.foreData = res.data.Result;
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        $scope.selectTechnician = function(selected) {
            console.log(selected);
            // console.log($scope.mGroupGR.TechnicianId);

            //logic untuk yang multiple
            /*
            var detData = [];
            var fromDetTbl = []; 

            if (selected!==undefined) {
                var grTId = [];
                if (formMode!="new") {
                     _.find(getArrGrpTechId, {"TechnicianId":selected[selected.length-1].TechnicianId});
                }            
                if ($scope.gridDetail.data.length<selected.length) {
                    $scope.gridDetail.data.push({ 
                        "GroupTechnicianId":(formMode=="new"?0:(typeof grTId === 'undefined'?0:grTId)),
                        "TechnicianId":selected[selected.length-1].TechnicianId,
                        "EmployeeName":selected[selected.length-1].EmployeeName,
                        "Initial":selected[selected.length-1].Initial,
                        "StatusCode":selected[selected.length-1].StatusCode
                    });
                } else {
                    for (var j in $scope.gridDetail.data) {
                        detData.push($scope.gridDetail.data[j]);
                    }
                    
                    for (var i in selected) {
                        var arrFind = _.find($scope.gridDetail.data, selected[i]);
                        fromDetTbl.push(arrFind);
                    }

                    // _.pullAllWith(detData, fromDetTbl, _.isEqual);
                    for (var i in fromDetTbl) {
                        var idx = detData.indexOf(fromDetTbl[i]);
                        if (idx > -1) {
                            detData.splice(idx, 1);
                        }
                    }

                    // _.pullAllWith($scope.gridDetail.data, detData, _.isEqual);
                    for (var i in detData) {
                        var idx = $scope.gridDetail.data.indexOf(detData[i]);
                        if (idx > -1) {
                            $scope.gridDetail.data.splice(idx, 1);
                        }
                    }                
                }
            }     
            */

        }

        $scope.guid = function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        $scope.validation = function(data) {
            var flag = 0;

            if (data.GroupName == null || data.GroupName == "") {
                flag = 1;
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Nama Group Harus Diisi Terlebih Dahulu",
                    timeout: 10000
                });
            }
            if (data.ForemanId == null || data.ForemanId === undefined) {
                flag = 1;
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Petugas Foreman Harus Diisi Terlebih Dahulu",
                    timeout: 10000
                });
            }
            if (data.Technician == null || data.Technician === undefined) {
                flag = 1;
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Teknisi Harus Diisi Terlebih Dahulu",
                    timeout: 10000
                });
            }

            if (flag == 0) {
                return true;
            } else {
                return false;
            }
        }

        $scope.addGroupTechnician = function(mData) {
            var valid = $scope.validation(mData);
            if (valid) {
                var technicianDataIndex = $scope.techData.findIndex(function(obj) {
                    return obj.TheId.TechnicianId == mData.Technician.TechnicianId;
                });

                var technicianName = $scope.techData[technicianDataIndex].EmployeeName;
                var initial = $scope.techData[technicianDataIndex].Initial;

                $scope.gridDetail.data.push({
                    GroupTechnicianId: $scope.guid(),
                    TechnicianId: mData.Technician.TechnicianId,
                    EmployeeName: technicianName,
                    Initial: initial,
                    StatusActive: 1,
                    StatusCode: 1
                });

                $scope.refreshTechnicianData();

                delete $scope.mGroupGR.Technician;
            }

        }

        $scope.refreshTechnicianData = function() {
            // $scope.techData = angular.copy($scope.FullTechnicianData);

            // for (var i in $scope.gridDetail.data) {
            //     var indexToSplice = $scope.techData.findIndex(function(obj) {
            //         return obj.EmployeeId == $scope.gridDetail.data[i].TechnicianId;
            //     });
            //     $scope.techData.splice(indexToSplice, 1);
            // }

            // ganti logic kl for, findindex dan splice di satuin nanti rancu. apa lg kl splice banyak data
            $scope.techData = [];
            for (var i=0; i<$scope.FullTechnicianData.length; i++){
                var tidakAdaYangSama = 0; // variable cek data FullTechnicianData supaya ga ada yg sama dengan yang ada di gridDetail
                for (var j=0; j<$scope.gridDetail.data.length; j++){
                    if ($scope.FullTechnicianData[i].EmployeeId  == $scope.gridDetail.data[j].TechnicianId){
                        tidakAdaYangSama++;
                    }
                }
                if (tidakAdaYangSama == 0){
                    // kalo ga ada di griddetail data nya, berarti boleh masuk ke dropdown
                    $scope.techData.push($scope.FullTechnicianData[i])
                }
            }
        }

        $scope.onShowDetail = function(mdl, mode) {
            console.log("mode showDetail : ", mode);
            $scope.gridDetail.data = [];
            if (mode == 'new') {
                $scope.mGroupGR = {};
                $scope.mGroupGR.GroupTypeId = 0;
                $scope.mGroupGR.isGR = 1;
                $scope.ButtonAdd = 1;
            } else if (mode == 'view') {
                $scope.ButtonAdd = 0;
                getDataGroupGRDetail(mdl, mode);
            } else if (mode == 'edit') {
                $scope.ButtonAdd = 1;
                getDataGroupGRDetail(mdl, mode);
            }
        }

        function getDataGroupGRDetail(row, mode) {
            // console.log("row : ",row);  
            GroupGRFactory.getDetailGroupGR(row.GroupId).then(function(res) {
                $scope.mGroupGR.TechnicianId = [];
                getArrTechId = [];
                getArrGrpTechId = [];
                if (res.data.Result.length > 0) {
                    for (var i in res.data.Result) {
                        getArrGrpTechId.push(res.data.Result[i]);
                        getArrTechId.push(res.data.Result[i].TechnicianId);
                        res.data.Result[i].disabled = (mode == 'view' ? true : false);
                    }
                    $scope.gridDetail.data = res.data.Result;                    
                    $scope.refreshTechnicianData();
                    $scope.mGroupGR.TechnicianId = res.data.Result;
                    $scope.mGroupGR.MGroup_Technician_TM = res.data.Result;
                    console.log('$scope.gridDetail.data', $scope.gridDetail.data);
                }
            });
        };

        $scope.onValidateSave = function(row, mode) {
            console.log("mode onValidateSave : ", row, mode);
            if (!$scope.gridDetail.data.length > 0) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Data Detail Harus Diisi Minimal 1 (satu)",
                    timeout: 10000
                });
                return false;
            } else if (typeof row.ForemanId === 'undefined' || row.ForemanId == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Silahkan Pilih Foreman Terlebih Dahulu!",
                    timeout: 10000
                });
                return false;
            } else {

                row.MGroup_Technician_TM_Delete = [];

                var tempInsertData = [];
                $scope.copyDataDetail = angular.copy($scope.gridDetail.data)
                for (var i in $scope.gridDetail.data) {
                    // $scope.gridDetail.data[i].GroupTechnicianId = $scope.gridDetail.data[i].GroupTechnicianId + "";
                    if ($scope.gridDetail.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
                        $scope.gridDetail.data[i].GroupTechnicianId = 0;
                        tempInsertData.push($scope.gridDetail.data[i]);
                    } else {
                        $scope.deletedTechnicianDetail.push($scope.gridDetail.data[i]);
                    }
                }

                var tech = $scope.gridDetail.data;
                _.map(tech, function(val){
                    val.StatusCode = val.StatusActive;
                });
                row.MGroup_Technician_TM = tech;
                //mdl.MGroup_Technician_TM = tempInsertData;
                row.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;

                var functionChoice = mode == "create" ? GroupGRFactory.create : GroupGRFactory.update;

                functionChoice(row).then(function(res) {
                    if (res.data == -1){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Nama Foreman Sudah Terdaftar Di Group Lain",
                        });
                        $scope.deletedTechnicianDetail = [];
                        $scope.gridDetail.data = angular.copy($scope.copyDataDetail)
                        return false;
                    } else {
                        $scope.deletedTechnicianDetail = [];
                        $scope.gridDetail.data = [];
                        $scope.mGroupGR = {};
                        $scope.refreshTechnicianData();
                        $scope.getData();
                        $scope.formApi.setMode('grid');

                        // $scope.goBack();
                        return true;

                    }
                    
                })

            }
        };

        $scope.doCustomSave = function(mdl, mode) {

            console.log(mdl);
            console.log(mode);
            //mdl.MGroup_Technician_TM = [];
            // ========================= ini di komen pindah ke validatesave ==================== start
            // mdl.MGroup_Technician_TM_Delete = [];

            // /*
            //     if (mode=='create') {
            //         if (mdl.TechnicianId.length>0) {
            //             for (var i in mdl.TechnicianId){
            //                 mdl.MGroup_Technician_TM.push({ 
            //                     "TechnicianId" : mdl.TechnicianId[i].TechnicianId, 
            //                     "StallId" : 0
            //                 });
            //             }            
            //         }
    
            //         var saveObj = angular.copy(mdl);
            //         delete saveObj.TechnicianId;
            //         console.log("saveObj : ",saveObj)
            //         GroupGRFactory.create(mdl).then(function(res){
            //             mdl = {};
            //             $scope.getData();
            //         });
            //     } else {
            //         var listTechId = [];            
            //         for (var i in mdl.TechnicianId) {
            //             listTechId.push(mdl.TechnicianId[i].TechnicianId);
            //         }
            //         var del = _.difference(getArrTechId, listTechId);
            //         var x = _.difference(getArrTechId, del);
            //         var ist = _.difference(mdl.TechnicianId, x);            
    
            //         if (ist.length>0) {
            //             for (var i in ist){
            //                 // var found = _.find($scope.techData, { "TechnicianId" : ist[i] });
            //                 // if (typeof found !== 'undefined'){
            //                     mdl.MGroup_Technician_TM.push({ 
            //                         "TechnicianId" : ist[i], 
            //                         "StallId" : 0
            //                     });
            //                 // }
            //             }            
            //         }
    
            //         if (del.length>0) {
            //             for (var i in del){
            //                 var found = _.findIndex($scope.gridDetail.data, { "TechnicianId" : del[i] });
            //                 if (typeof found !== 'undefined'){
            //                     mdl.MGroup_Technician_TM_Delete.push({
            //                         "GroupTechnicianId" : found.GroupTechnicianId
            //                     });
            //                 }
            //             }    
            //         }
    
            //         var saveObj = angular.copy(mdl);
            //         delete saveObj.TechnicianId;
            //         GroupGRFactory.update(mdl).then(function(res){
            //             mdl = {};
            //             $scope.getData();
            //         });
            //     }   
            //     */

            // var tempInsertData = [];

            // for (var i in $scope.gridDetail.data) {
            //     // $scope.gridDetail.data[i].GroupTechnicianId = $scope.gridDetail.data[i].GroupTechnicianId + "";
            //     if ($scope.gridDetail.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
            //         $scope.gridDetail.data[i].GroupTechnicianId = 0;
            //         tempInsertData.push($scope.gridDetail.data[i]);
            //     } else
            //         $scope.deletedTechnicianDetail.push($scope.gridDetail.data[i]);
            // }

            // var tech = $scope.gridDetail.data;
            // _.map(tech, function(val){
            //     val.StatusCode = val.StatusActive;
            // });
            // mdl.MGroup_Technician_TM = tech;
            // //mdl.MGroup_Technician_TM = tempInsertData;
            // mdl.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;

            // var functionChoice = mode == "create" ? GroupGRFactory.create : GroupGRFactory.update;

            // functionChoice(mdl).then(function(res) {
            //     if (res.data == -1){
            //         bsNotify.show({
            //             size: 'big',
            //             type: 'danger',
            //             title: "Nama Foreman Sudah Terdaftar Di Group Lain",
            //         });
            //         $scope.formApi.setMode('detail');
            //         return;
            //     } else {
            //         $scope.deletedTechnicianDetail = [];
            //         $scope.gridDetail.data = [];
            //         $scope.mGroupGR = {};
            //         $scope.refreshTechnicianData();
            //         $scope.getData();
            //         // $scope.goBack();
            //     }
                
            // })
            // ========================= ini di komen pindah ke validatesave ==================== end


        }

        // var statusCdTemplate =  '<bscheckbox ng-model="row.entity.StatusCode"'+
        var statusCdTemplate = '<input type="checkbox" ng-model="row.entity.StatusActive"' +
            '   ng-disabled="row.entity.disabled"' +
            '   ng-true-value = "1"' +
            '   ng-false-value = "0"' +
            '   styles="margin-top:-12px;margin-left:0px;">'; //+
            //'</bscheckbox>';

        //----------------------------------
        // Grid Setup
        //----------------------------------

        var actionTemp = '<div>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)">' +
            '<i class="fa fa-fw fa-lg fa-list-alt" title="Lihat"></i>' +
            '</button>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.gridClickEditHandler(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-pencil" title="Ubah"></i>' +
            '</button>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.$parent.deleteGroupconfirmation(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-trash" title="Hapus"></i>' +
            '</button>' +
            '</div>';

        $scope.deleteGroupconfirmation = function(row) {

            $scope.tempGroupIdDelete = row.GroupId;

            var tempNameIndex = $scope.grid.data.findIndex(function(obj) {
                return obj.GroupId == row.GroupId
            });

            $scope.tempStallNameDelete = $scope.grid.data[tempNameIndex].GroupName;

            ngDialog.openConfirm({
                template: '\
                  <p>Are you sure you want to delete stall : {{tempStallNameDelete}}?</p>\
                  <div class="ngdialog-buttons">\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteGroup();closeThisDialog(0)">Yes</button>\
                  </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope: $scope
            });
        }

        $scope.deleteGroup = function() {

            GroupGRFactory.delete($scope.tempGroupIdDelete).then(function(res) {
                // $scope.getData();
                // $scope.actRefresh();
                // $scope.refreshTechnicianData();            
                $scope.getData();

            })
        }


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Group ID', field: 'GroupId', width: '7%', visible: false },
                { name: 'Nama Group', field: 'GroupName' },
                { name: 'Foreman', field: 'ForemanName' },
                //tambahan yap
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: 250,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: actionTemp
                }
            ]
        };

        $scope.gridDetailApi = {};
        $scope.gridDetail = {
            // rowHeight: 50,
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus   
            columnDefs: [
                { name: 'EmployeeId', field: 'TechnicianId', visible: false },
                { name: 'Nama Teknisi', field: 'EmployeeName' },
                { name: 'Inisial', field: 'Initial' },
                { name: 'Status Aktif', cellTemplate: statusCdTemplate }
            ]
        };


    });