angular.module('app')
  .factory('GroupGRFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function () {
        // var res=$http.get('/api/as/GroupGR');
        var res = $http.get('/api/as/GroupGR/GetGroupGR');
        console.log("Hasil Factory : ", res);
        return res;
      },
      getTechnician: function () {
        console.log("get getTechnician");
        // var res=$http.get('/api/as/EmployeeRoles/2');
        var res = $http.get('/api/as/GroupGR/GetTechnician');
        console.log("Hasil Factory : ", res);
        return res;
      },
      getQC: function () {
        var res = $http.get('/api/as/EmployeeRoles/5');
        console.log("Hasil Factory : ", res);
        return res;
      },
      getForeman: function () {
        // var res=$http.get('/api/as/EmployeeRoles/2');
        var res = $http.get('/api/as/GroupGR/GetForeman');
        console.log("Hasil Factory : ", res);
        return res;
      },
      getGroupHead: function () {
        var res = $http.get('/api/as/EmployeeRoles/2');
        console.log("Hasil Factory : ", res);
        return res;
      },

      create: function (data) {
        console.log("Data DP : ", data);
        data.Id = 0;
        return $http.post('/api/as/GroupGR', [data]);
      },
      update: function (data) {
        console.log("Data DP : ", data);
        // return $http.put('/api/as/GroupGR', [data]); // sekarang update sama di delete disatuin pake post
        return $http.post('/api/as/GroupGR', [data]);
      },
      // delete: function (id) {
      //   console.log("Data DP : ", id);
      //   return $http.delete('/api/as/GroupGR', { data: id, headers: { 'Content-Type': 'application/json' } });
      // },
      delete: function (groupIdFront) {
        return $http.delete('/api/as/GroupGR/DeleteGroupGR?groupId=' + groupIdFront);
      },
      // deleteGroup: function (groupIdFront) {
      //   return $http.delete('/api/as/GroupGR/DeleteGroupGR?groupId=' + groupIdFront);
      // },
      getDetailGroupGR: function (groupId) {
        var res = $http.get('/api/as/GroupGR/GetDetailGroupGR', { params: { GroupId: groupId } });
        console.log("Hasil getDetailGroupGR : ", res);
        return res;
      }
    }
  });
 //