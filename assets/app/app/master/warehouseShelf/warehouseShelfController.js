
angular.module('app')
    .controller('WarehouseShelfController', function($scope, $http, CurrentUser, WarehouseShelfFactory, PartsCategory, $timeout, bsNotify, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        console.log("VIEW CONTENT LOADED_________==============");
        //get material type and service type
            //login 41 -> RoleId: 1135 -> GR
            //login 25 -> RoleId: 1122 -> GR
            //login 26 -> RoleId: 1123 -> BP
            //login 27 -> RoleId: 1125 -> bahan GR
            //login 28 -> RoleId: 1124 -> bahan BP
            if ($scope.user.RoleId == 1135) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                console.log('1135', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1122) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                console.log('1122', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1123) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 0;
                console.log('1123', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1125) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                console.log('1125', $scope.MaterialTypeId, $scope.ServiceTypeId);
                // $scope.ServiceTypeId = 1;
                // $scope.ServiceTypeId = 0;
                // $scope.FunctionGetData2(mData);
            } else if ($scope.user.RoleId == 1124) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 0;
                console.log('1124', $scope.MaterialTypeId, $scope.ServiceTypeId);
            }
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mWarehouseShelf = null; //Model
    $scope.cWarehouseShelf = null; //Collection
    $scope.xWarehouseShelf = {};
    $scope.xWarehouseShelf.selected=[];
    $scope.MaterialTypeId = null;
    $scope.ServiceTypeId = null;

    $scope.saveWHShelf = {};
    $scope.mode = '';
    $scope.bAktif = true;
    $scope.tempID = 0;

    var dateFormat='yyyy/MM/dd';

    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    $scope.ServiceTypeData = [
        {Id:0, Name:"BP"},
        {Id:1, Name:"GR"}
    ];

    //----------------------------------
    // Get Data
    //----------------------------------
    var tempObj = {};
    $scope.getData = function() {
        WarehouseShelfFactory.getData().then(
            function(res){
                console.log("data=>",res.data);
                // var temp = res.data.Result;
                // var lastArr = [];
                // if (temp.length>0) {
                //     tempObj = {};
                //     for (var i in temp) {
                //         var dateKey = new Date(temp[i]["EffectiveDate"]);
                //         var key = dateKey.getFullYear() + "/" + (dateKey.getMonth() + 1) + "/" + dateKey.getDate();
                //         var d = new Date();
                //         var cutOff = temp[i].CutOffTime.split(":");
                //         d.setHours(cutOff[0]);
                //         d.setMinutes(cutOff[1]);
                //         temp[i].CutOffTime = d;

                //         var h = new Date();
                //         var etdH = temp[i].ETDHour.split(":");
                //         h.setHours(etdH[0]);
                //         h.setMinutes(etdH[1]);
                //         temp[i].ETDHour = h;

                //         if (!tempObj[key]) {
                //             tempObj[key] = [];
                //         }
                //         tempObj[key].push(temp[i]);
                //     }   
                //     var x = _.keys(tempObj); 
                //     for (var j in x) {
                //         lastArr.push({
                //             "EffectiveDate":x[j]
                //         });
                //     }
                // }
                // $scope.grid.data = lastArr;
                var tmpRes = res.data.Result;
                _.map(tmpRes,function(val){
                    if(val.MaterialTypeId == 1){
                        val.MaterialTypeDesc = "Part"
                    }else{
                        val.MaterialTypeDesc = "Bahan"
                    }

                    if(val.ServiceTypeId == 1){
                        val.ServiceTypeDesc = "GR"
                    }else{
                        val.ServiceTypeDesc = "BP"
                    }
                })
                var tmpData = [];
                for(var i in tmpRes){
                    if(tmpRes[i].MaterialTypeId == $scope.MaterialTypeId ){
                        tmpData.push(tmpRes[i]);
                    }
                    
                }
                $scope.grid.data = tmpData;                
                $scope.loading=false;
                $scope.mode = 'grid';
                console.log('Mode Grid =>', $scope.mode);
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    PartsCategory.getData(1).then(
        function(res){
            $scope.MaterialTypeData = res.data.Result;
            console.log("data=>",res.data);
            return res.data;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    $scope.beforeNew = function(){
        $scope.mode = 'New';
        console.log('mode new ==>', $scope.mode);
        $scope.gridDetail.data = [];
        $scope.mWarehouseShelf.MaterialTypeId = $scope.MaterialTypeId;
        $scope.mWarehouseShelf.ServiceTypeId = $scope.ServiceTypeId;
        $scope.addNewRow();
        $scope.mWarehouseShelf.Active = true;
        $scope.bAktif = false;
        console.log('New bAktif=>',$scope.bAktif);
    }

    var warehouseIdTgt = null;
    $scope.beforeEdit = function(row,mode){
        $scope.mode = 'Edit';
        console.log('mode Edit ==>', $scope.mode);
        $scope.mWarehouseShelf = {};
        $scope.mWarehouseShelf = angular.copy(row);
        // $scope.saveWHShelf.WarehouseCode = $scope.mWarehouseShelf.WarehouseCode;
        // $scope.saveWHShelf.WarehouseName = $scope.mWarehouseShelf.WarehouseName;
        // $scope.saveWHShelf.Active = $scope.mWarehouseShelf.Active;
        // $scope.saveWHShelf.Notes = $scope.mWarehouseShelf.Notes;
        warehouseIdTgt = $scope.mWarehouseShelf.WarehouseId;
        $scope.bAktif = true;
        console.log('Edit bAktif=>',$scope.bAktif);
        
        $scope.gridDetail.data = $scope.mWarehouseShelf.WarehouseShelf;
        if(mode == 'edit'){
            $scope.addNewRow();
        }
        
    }

    $scope.addNewRow = function(){
        $scope.tempID += 1;
        $scope.gridDetail.data.push({     
            ShelfId: null,
            WarehouseId: null,
            ShelfCode: null,
            ShelfName: null,
            Active: true,
            TempID: $scope.tempID
        });
        console.log('ini array di dalem grid cuk==>', $scope.gridDetail.data);
    }

    $scope.removeRow = function(selected){
        var detData = [];
        console.log('removeRow==> ',selected);

        if (selected.ShelfId == null) {
            if (selected.TempID>0) {
                var flagDeleteTmp = _.findIndex($scope.gridDetail.data, {"TempId": selected.TempID} );
                console.log('selected item=>', flagDeleteTmp);
                $scope.gridDetail.data.splice(flagDeleteTmp,1);
            } else {
                var temp = _.keys(selected);
                for (var i in temp) {
                    selected[temp[i]] = null;
                }
            }
        } else {
            WarehouseShelfFactory.cekRakTerpakai(selected).then(function(res) {
                var resData = res.data;
                console.log("resData==>>",resData);
                console.log("resData.Result.length==>>",resData.Result.length);
                if (resData.Result.length == 0) {
                    if ($scope.gridDetail.data.length>1) {
                        // _.pullAllWith($scope.gridDetail.data, [selected], _.isEqual);   
                        var flagDeleteDetail = _.findIndex($scope.gridDetail.data, {"ShelfId": selected.ShelfId} );
                        console.log('selected item=>', flagDeleteDetail);
                        $scope.gridDetail.data.splice(flagDeleteDetail,1);
                    } else {
                        var temp = _.keys(selected);
                        for (var i in temp) {
                            selected[temp[i]] = null;
                        }
                    }
                } else {
                    // bsNotify.show({
                    //     title: "Tidak boleh dihapus",
                    //     content: "Sudah digunakan PartsCode "+resData.Result[0].PartsCode+" "+resData.Result[0].PartsName,
                    //     // content: "Sudah digunakan PartId "+resData.PartId,
                    //     type: 'danger'
                    // });
                    bsAlert.info("Tidak boleh dihapus", "Sudah digunakan oleh "+resData.Result[0].PartsCode+" "+resData.Result[0].PartsName);
                }
            });
        }
    }

    $scope.checkRow = function(){
        var findObj = _.find($scope.gridDetail.data, {
            ShelfId: null,
            WarehouseId: null,
            ShelfCode: null,
            ShelfName: null,
            Active: false
        });
        console.log('checkRow');
        if (findObj==undefined) {
            $scope.addNewRow();
        } 
    }

    $scope.checkAktif = function(selected){
        var detData = [];
        console.log(selected);

        if (selected.Active == false) {
            WarehouseShelfFactory.cekRakTerpakai(selected).then(function(res) {
                var resData = res.data;
                console.log("resData==>>",resData);
                console.log("resData.Result.length==>>",resData.Result.length);
                if (resData.Result.length > 0) {
                    // bsNotify.show({
                    //     title: "Tidak boleh dihapus",
                    //     content: "Sudah digunakan PartsCode "+resData.Result[0].PartsCode+" "+resData.Result[0].PartsName,
                    //     // content: "Sudah digunakan PartId "+resData.PartId,
                    //     type: 'danger'
                    // });
                    selected.Active = true;
                    bsAlert.info("Tidak boleh di-NonAktif", "Sudah digunakan oleh "+resData.Result[0].PartsCode+" "+resData.Result[0].PartsName);
                }
            });
        }
    }

    $scope.validateSave = function(data){
        var tmpData = angular.copy($scope.grid.data);
        var tmpMaterialType = '';
        var tmpServiceType = '';
        var tmpWarning = '';
        console.log("okee",$scope.grid.data);
        console.log("data.MaterialTypeId",data.MaterialTypeId);
        console.log("data.ServiceTypeId",data.ServiceTypeId);

        if ($scope.mode == 'New') {

            var tmpFlag = 0;
            for (var i in tmpData) {
                if (data.MaterialTypeId == tmpData[i].MaterialTypeId && data.ServiceTypeId == tmpData[i].ServiceTypeId) {
                    tmpFlag++;
                    if(tmpData[i].MaterialTypeId == 1) { tmpMaterialType = 'Parts' } else { tmpMaterialType = 'Bahan' };
                    if(tmpData[i].ServiceTypeId == 1) { tmpServiceType = 'GR' } else { tmpServiceType = 'BP' };
                    tmpWarning = 'Gudang '+tmpMaterialType+' '+tmpServiceType+' sudah ada !!';
                }
            }
            console.log("tmplasdfad", tmpFlag);
            if (tmpFlag > 0) {
                bsNotify.show(
                    {
                        size: 'medium',
                        type: 'warning',
                        // title: "DATA ADA YANG SAMA",
                        title: tmpWarning,
                        // content: error.join('<br>'),
                        // number: error.length
                    }
                );
                console.log("mantaaaaaaappp", tmpData[i].MaterialTypeId, tmpData[i].ServiceTypeId);
                return false
            }
            else {
                if ($scope.gridDetail.data.length == 1) {
                    bsNotify.show(
                        {
                            size: 'medium',
                            type: 'warning',
                            title: "Kode dan nama lokasi rak masih kosong.",
                            // content: error.join('<br>'),
                            // number: error.length
                        }
                    );
                    return false;
                }
                return true;
                console.log("teessss");
            }
            
        } else if ($scope.mode == 'Edit') {

            bsNotify.show(
                {
                    size: 'medium',
                    type: 'success',
                    title: "Data Berhasil di Perbarui.",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );
            return true;

        } else{
            console.log('aku rapopo mas');

            
        }
        


        console.log("tmplasdfad",tmpFlag);
        // if(data.MaterialTypeId == tmpData[i].MaterialTypeId && data.ServiceTypeId == tmpData[i].ServiceTypeId){
        //     tmpFlag = true;
        //     bsNotify.show(
        //         {
        //             size: 'medium',
        //             type: 'warning',
        //             title: "DATA ADA YANG SAMA",
        //             // content: error.join('<br>'),
        //             // number: error.length
        //         }
        //     );
        //     console.log("mantaaaaaaappp",tmpData[i].MaterialTypeId,tmpData[i].ServiceTypeId);
        //     return false
        // }else{
        //     if ($scope.gridDetail.data.length==1) {
        //         bsNotify.show(
        //             {
        //             size: 'medium',
        //             type: 'warning',
        //             title: "Kode dan nama lokasi rak masih kosong.",
        //             // content: error.join('<br>'),
        //             // number: error.length
        //             }
        //         );  
        //         return false;
        //     }
        //     tmpFlag = false;
        //     return false
        //     console.log("teessss");
        // }
        return false
        // if($scope.MaterialTypeData && $scope.ServiceTypeId){
        //     bsNotify.show(
        //         {
        //             size: 'medium',
        //             type: 'warning',
        //             title: "DATA ADA YANG SAMA",
        //             // content: error.join('<br>'),
        //             // number: error.length
        //         }
        //     );  
        //     return false;
        //   }else{
        //     if ($scope.gridDetail.data.length==1) {
        //         bsNotify.show(
        //             {
        //                 size: 'medium',
        //                 type: 'warning',
        //                 title: "Kode dan nama lokasi rak masih kosong.",
        //                 // content: error.join('<br>'),
        //                 // number: error.length
        //             }
        //         );  
        //         return false;
        //     }
        //     return true;
        //   }
    }

    $scope.doCustomSave = function(mdl,mode){
        // console.log("inside before save");
        // console.log("mdl : "+JSON.stringify(mdl));
        // console.log("mode : "+mode);
        var grdData = $scope.gridDetail.data;
        var lastArr = [];       
        console.log('grdData==>',grdData); 
        
        // for (var i=0; i<grdData.length-1; i++) {                
        for (var i=0; i<grdData.length; i++) {                
            var temp = {};
            if (grdData[i].ShelfId==null) {
                temp.ShelfId = 0;
            } else {
                temp.ShelfId = grdData[i].ShelfId;
            }
            // temp.ShelfId = 0;

            // if (grdData[i].WarehouseId==null) {
            //     temp.WarehouseId = 0;
            // } else {
            //     temp.WarehouseId = grdData[i].WarehouseId;
            // }            
            temp.WarehouseId = warehouseIdTgt;
            if (mode=='create') {
                temp.WarehouseId = 0;       
            }
            
            temp.ShelfCode = grdData[i].ShelfCode;
            temp.ShelfName = grdData[i].ShelfName;
            temp.Active = grdData[i].Active;

            if(temp.ShelfCode != null && temp.ShelfCode != '') {
                lastArr.push(temp);
            }
        }
                
        // mdl.ServiceTypeId = 1;
        // mdl.MaterialTypeId = 1;
        mdl.WarehouseShelf = lastArr;

        if (mode=='create') {
            // mdl.WarehouseId = null;
            mdl.WarehouseId = 0;
            console.log("mdl : ",mdl);
            var svObj = angular.copy(mdl);
            WarehouseShelfFactory.create(svObj).then(function(res){
                $scope.gridDetail.data = [];
                $scope.getData();   


        });
            
        } else {
            var svObj = angular.copy(mdl);
            WarehouseShelfFactory.update(svObj).then(function(res){
                $scope.gridDetail.data = [];
                $scope.getData();
            });
        }
    }

    $scope.alertAfterSave = function() {
        bsAlert.alert({
                title: "Data tersimpan",
                type: "success",
                showCancelButton: false
            }
        )
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [  
            // 1 -> BP
            // 2 -> GR
            { name:'Kode Lokasi Gudang', field:'WarehouseCode'},
            { name:'Nama Lokasi Gudang', field:'WarehouseName'},
            // { name:'Tipe Material Id', field:'MaterialTypeId', cellFilter: 'materialType', visible:false},
            { name:'Tipe Material', field:'MaterialTypeDesc'},
            // { name:'Tipe Service', field:'ServiceTypeId', cellFilter: 'serviceType'},
            { name:'Tipe Service', field:'ServiceTypeDesc'},                                    
            { name:'Aktif', cellTemplate:  '<input type="checkbox" ng-model="row.entity.Active" disabled>'},
        ]
    };

    // var numbCellTemplate =      '<div class="ui-grid-cell-contents">'+
    //                             '   <input type="number" class="form-control" name="OrderTypeId" max="3" min="1" style="height:50">'+
    //                             '</div>';
    // var dateCellTemplate =      '<div class="ui-grid-cell-contents">'+
    //                             '   <div uib-timepicker ng-model="day.timeFrom" show-spinners="false" show-meridian="false"></div>'+
    //                             '</div>';

    $scope.gridDetailApi = {};
    $scope.gridDetail = {
        // rowHeight: 50,
        enableSorting: true,
        enableSelectAll: true,
        enableCellEdit: false,
        enableCellEditOnFocus: true, // set any editable column to allow edit on focus   
        columnDefs: [            
            { name:'WarehouseShelfId',    field:'ShelfId', visible:false },            
            { name:'Kode Lokasi Rak', field: 'ShelfCode', enableCellEdit: true},
            { name:'Nama Lokasi Rak', field: 'ShelfName', enableCellEdit: true},
            { name:'Aktif', cellTemplate:  '<input type="checkbox" ng-model="row.entity.Active" ng-click="grid.appScope.checkAktif(row.entity)">'},
            // { name:'Aktif', cellTemplate:  '<input type="checkbox" ng-model="row.entity.Active" ng-disabled="grid.appScope.bAktif">'},
            // { name:'Aktif', field: 'Active', enableCellEdit: $scope.bAktif},
            { name:'TempID', field: 'TempID',visible:false },
            { name:'Action', cellTemplate:  '<div class="ui-grid-cell-contents">'+
                                                '<button class="ui icon inverted grey button"'+
                                                        'style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                                        'onclick="this.blur()"'+
                                                        'ng-click="grid.appScope.removeRow(row.entity)">'+
                                                    '<i class="fa fa-fw fa-lg fa-times"></i>'+
                                                '</button>'+
                                            '</div>'}
        ]
    };

    $scope.formApi={};

    $scope.gridDetail.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridDetailApi = gridApi;         
        console.log('grid bAktif=>', $scope.bAktif);     

        $scope.gridDetailApi.selection.on.rowSelectionChanged($scope,function(row) {
            //scope.selectedRows=null;
            $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
            // console.log("bsform selected=>",scope.selectedRows);
        });
        $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
            //scope.selectedRows=null;
            $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
        });

        $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            console.log('Inside afterCellEdit grid detail..');
            if (newValue!=oldValue) {
                $scope.checkRow();
            }
        });
    };
}).filter('materialType', function () {
    return function (x) {
        // for (var i = 0; i < map.length; i++) {
        //     console.log(map[i]);
        //     if(map[i][idField] == input){
        //         return map[i][valueField];
        //     }
        // }
        // return '';
        return (x==1?"Part":"Bahan");
    };
}).filter('serviceType', function () {
    return function (x) {
        return (x==1?"GR":"BP");
    };
});
