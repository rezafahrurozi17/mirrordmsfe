angular.module('app')
  .factory('WarehouseShelfFactory', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/Warehouse');
        return res;
      },
      create: function(WarehouseShelf) {
        console.log("Data DP : ",WarehouseShelf);        
        return $http.post('/api/as/Warehouse', [WarehouseShelf]);
      },
      update: function(WarehouseShelf){
        console.log("Data DP : ",WarehouseShelf);
        return $http.put('/api/as/Warehouse', [WarehouseShelf]);
      },
      delete: function(id) {
        console.log("Data DP : ",id);
        //return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
      cekRakTerpakai: function(data){
        console.log("Data: ",data);
        var res = $http.get('/api/as/Warehouse/CekRakTerpakai', {params: {
          WarehouseId: (data.WarehouseId), 
          ShelfId: (data.ShelfId)}
        });
        console.log('<factory> cekRakTerpakai => ', res);
        return res;
      },
    }
  });
 //