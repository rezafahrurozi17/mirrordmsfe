angular.module('app')
  .factory('RincianPembayaranLihatFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
    
    
        
    var da_json=[
        {Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 1",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 1",Nominal_Pembayaran: 1000},
        {Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 2",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 2",Nominal_Pembayaran: 1000},
        {Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 3",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 3",Nominal_Pembayaran: 1000},
        ];
    res=da_json;
    
        return res;
      },
      create: function(rincianPembayaranLihat) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: rincianPembayaranLihat.Name,
                                            Description: rincianPembayaranLihat.Description}]);
      },
      update: function(rincianPembayaranLihat){
        return $http.put('/api/fw/Role', [{
                                            Id: rincianPembayaranLihat.Id,
                                            //pid: negotiation.pid,
                                            Name: rincianPembayaranLihat.Name,
                                            Description: rincianPembayaranLihat.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });