angular.module('app')
    .controller('DpPartsController', function($scope, $http, CurrentUser, DpPartsFactory,$timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mDpParts = null; //Model
    $scope.mDpParts = {};
    $scope.cDpParts = null; //Collection
    $scope.xDpParts = {};
    $scope.xDpParts.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.validDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];

    $scope.getData = function(param) {
        console.log('asem gila1', $scope.mDpParts.startDate)
        console.log('asem gila2', $scope.mDpParts.endDate)
        console.log('param duhhh', param)
        if (param == 999){
            // artinya dari showdetail
            $scope.formApi.setMode('grid');
            if ($scope.mDpParts.startDate != undefined && $scope.mDpParts.endDate != undefined){
                if ($scope.mDpParts.startDate != null && $scope.mDpParts.endDate != null){
                    DpPartsFactory.getDataFilter($scope.mDpParts).then(
                        function(res){
                            console.log(res);
                            $scope.grid.data = res.data.Result;
                            $scope.loading=false;
                            return res.data;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
                } else {
                    $scope.grid.data = [];
                }
                
            } else {
                $scope.grid.data = []
            }
        } else {
            if (($scope.mDpParts.startDate != undefined && $scope.mDpParts.startDate != null) && 
                ($scope.mDpParts.endDate != undefined && $scope.mDpParts.endDate != null)){
                    DpPartsFactory.getDataFilter($scope.mDpParts).then(
                        function(res){
                            console.log(res);
                            $scope.grid.data = res.data.Result;
                            $scope.loading=false;
                            return res.data;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            } else {
                bsNotify.show(
                    {
                        title: "Mandatory",
                        content: "'Tanggal Berlaku Dari' dan 'Sampai' harus diisi",
                        type: 'danger'
                    }
                );  
            }
        }
        // if(($scope.mDpParts.startDate == null || $scope.mDpParts.endDate == null) || 
        //     ($scope.mDpParts.startDate == undefined || $scope.mDpParts.endDate == undefined)){
        //     bsNotify.show(
        //         {
        //             title: "Mandatory",
        //             content: "'Tanggal Berlaku Dari' dan 'Sampai' harus diisi",
        //             type: 'danger'
        //         }
        //     );  
        // } else{        
        //     DpPartsFactory.getDataFilter($scope.mDpParts).then(
        //         function(res){
        //             console.log(res);
        //             $scope.grid.data = res.data.Result;
        //             $scope.loading=false;
        //             return res.data;
        //         },
        //         function(err){
        //             console.log("err=>",err);
        //         }
        //     );
            
        // }
    };
    


    $scope.numberWithCommas = function(x) {
        console.log(x);
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    // ===== Append By faisal ======
    $scope.onShowDetail = function(data,mode){
        console.log("data",data);
        console.log("mode",mode);
        console.log("mDpParts => ",$scope.mDpParts);
        var tglAwal = $scope.mDpParts.startDate;
        var tglAkhir = $scope.mDpParts.endDate;
        $scope.mDpParts = {};
        $scope.mDpParts = angular.copy(data);
        $scope.mDpParts.startDate = tglAwal;
        $scope.mDpParts.endDate = tglAkhir;
        if(mode == 'edit' || mode == 'view'){	
            $scope.mDpParts.PriceFrom = String($scope.mDpParts.PriceFrom);
            $scope.mDpParts.PriceFrom = $scope.mDpParts.PriceFrom.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
            $scope.mDpParts.PriceTo = String($scope.mDpParts.PriceTo);
            $scope.mDpParts.PriceTo = $scope.mDpParts.PriceTo.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        }
    }
    $scope.givePatternpriceFrom = function(item, event) {
        console.log("item", item);
        if (event.which > 37 && event.which < 40) {
            event.preventDefault();
            return false;
        } else {
            $scope.mDpParts.PriceFrom = $scope.mDpParts.PriceFrom.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
            return;
        }
    };
    $scope.giveCommaOnDecimal = function(item){
    	console.log("item",item);

        return item.toString().replace(/\./g, ',');
    }
    $scope.givePatternpriceTo = function(item, event) {
        console.log("item", item);
        if (event.which > 37 && event.which < 40) {
            event.preventDefault();
            return false;
        } else {
            $scope.mDpParts.PriceTo = $scope.mDpParts.PriceTo.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
            return;
        }
    };
    $scope.onValidateSave = function(data, mode){
        console.log('data', data);
        console.log('mode', mode)
        console.log(('mDpParts=>', $scope.mDpParts));
    	var tmpPriceForm = angular.copy(data.PriceFrom);
        var tmpPriceTo = angular.copy(data.PriceTo);
        
        if (tmpPriceForm.toString().includes('.')){
            tmpPriceForm= tmpPriceForm.replace(/\./g, '');
        }
    	// console.log("tmpPriceForm",tmpPriceForm);
    	tmpPriceForm = parseInt(tmpPriceForm);
    	data.PriceFrom = tmpPriceForm;
        
        if (tmpPriceTo.toString().includes('.')){
            tmpPriceTo= tmpPriceTo.replace(/\./g, '');
        }
    	// console.log("tmpPriceForm",tmpPriceTo);
    	tmpPriceTo = parseInt(tmpPriceTo);
        data.PriceTo = tmpPriceTo;
        
        if (mode == 'create'){
            DpPartsFactory.create(data).then(
                function(res){
                    console.log('res=>',res);
                    var resData = res.data;
                    console.log('resData=>',resData);
                    if(resData.ResponseCode == 13) {
                        bsNotify.show(
                            {
                                title: "Success",
                                content: "Data Berhasil Disimpan",
                                type: 'success'
                            }
                        );  
                        $scope.getData(999);
                    } else if (resData.ResponseCode == 29) {
                        bsNotify.show(
                            {
                                title: "Warning",
                                content: "Data bersinggungan dengan data lain",
                                type: 'warning'
                            }
                        );  
                    }
            })
        } else {
            DpPartsFactory.update(data).then(
                function(res){
                    console.log('res=>',res);
                    var resData = res.data;
                    console.log('resData=>',resData);
                    if(resData.ResponseCode == 23) {
                        bsNotify.show(
                            {
                                title: "Success",
                                content: "Data Berhasil Dirubah",
                                type: 'success'
                            }
                        );  
                        $scope.getData(999);
                    } else if (resData.ResponseCode == 29) {
                        bsNotify.show(
                            {
                                title: "Warning",
                                content: "Data bersinggungan dengan data lain",
                                type: 'warning'
                            }
                        );  
                    }
            })
        }

       
        // return true
    }
    // var templatePercentage = '<div class="ui-grid-cell-contents">{{grid.appScope.giveCommaOnDecimal(row.entity.PercentDP)}}</div>'
    //==============================
  
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.onDeleteFilter = function (temp) {
        console.log("temp",temp,$scope.mDpParts);
        $scope.mDpParts={startDate:null,endDate:null};
        console.log("$scope.mDpParts",$scope.mDpParts);
      }
    
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'DP Id',field:'DpId', width:'7%', visible:false },
            { name:'Tanggal Berlaku Dari', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Tanggal Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
            { name:'Range Dari', field:'PriceFrom', cellFilter: 'custCurrency' },
            { name:'Range Sampai', field:'PriceTo', cellFilter: 'custCurrency' },
            { name:'Persentase', field:'PercentDP', cellTemplate:'<div class="ui-grid-cell-contents">{{grid.appScope.$parent.giveCommaOnDecimal(row.entity.PercentDP)}}</div>'},
      
        ]
    };
}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
}).filter('custCurrency', function () {
    return function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); // append by faisal
        // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); // before
    };
});
