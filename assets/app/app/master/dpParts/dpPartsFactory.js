angular.module('app')
  .factory('DpPartsFactory', function($http, CurrentUser, $q, $filter) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/PartsDP');
        return res;
      },
      getDataFilter: function(data){
        //var res=$http.get('api/as/PartsDPFilter/{firstDate}/{secDate}');
        
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        var res=$http.get('/api/as/PartsDPFilter/'+startDate+'/'+endDate);
      	// var res=$http.get('/api/as/PartsDPFilter/', {params: {
        //   firstDate : (data.startDate==null?"n/a":startDate),
        //   secDate : (data.endDate==null?"n/a":endDate)
    
        // } });
        return res;
      },
      create: function(DpData) {
        var EffectiveDate = DpData.EffectiveDate.getFullYear() + '-' + (DpData.EffectiveDate.getMonth() + 1) + '-' + DpData.EffectiveDate.getDate();
        var ValidThru = DpData.ValidThru.getFullYear() + '-' + (DpData.ValidThru.getMonth() + 1) + '-' + DpData.ValidThru.getDate();
        DpData.EffectiveDate = EffectiveDate;
        DpData.ValidThru = ValidThru;
        console.log("Data DP : ",DpData);
        DpData.PartPriceDPId = 0;
        return $http.post('/api/as/PartsDP', [DpData]);
      },
      update: function(DpData){
        console.log("Data DP : ",DpData);
        DpData.EffectiveDate = $filter('date')(DpData.EffectiveDate, 'yyyy-MM-dd');
        DpData.ValidThru = $filter('date')(DpData.ValidThru, 'yyyy-MM-dd');
        return $http.put('/api/as/PartsDP', [DpData]);
      },
      delete: function(id) {
        console.log("Data DP : ",id);
        return $http.delete('/api/as/PartsDP',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //