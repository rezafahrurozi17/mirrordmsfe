angular.module('app')
    .controller('SCCRetailDiscController', function($scope, $http, CurrentUser, SCCRetailDiscount, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
        $scope.dataPartsClass = [];
        $scope.getPartsClassSCC();
        $scope.DeclareFunction();
        $scope.getDataCombobox();
    });

    $scope.getDataCombobox = function(){
        $scope.getPartsClassSCC();
    }

    $scope.getPartsClassSCC = function(){
        SCCRetailDiscount.getPartsClass().then(function(res){
            $scope.dataPartsClass = res.data.Result;
        });
    };

    $scope.DeclareFunction = function(){
        // $scope.onSelectRows = function(rows){
   
        // };

        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
        };

        $scope.onGotResult = function($) {
            console.log("onGotResult=>");
        };

        $scope.selected = {};

        $scope.onSelectPartsClass = function($item, $model, $label) {
                // console.log("onSelectWork=>", idx);
                console.log("onSelectWork=>", $item);
                console.log("onSelectWork=>", $model);
                console.log("onSelectWork=>", $label);

                $scope.mSCCRetailDisc.PartsClassId = $item.PartsClassId;

                // $scope.mData.Work = $label;
                // console.log('materialArray', materialArray);
                // materialArray = [];
                // materialArray.push($item);
                // console.log('materialArray',materialArray);
            };

        $scope.getPartsClassSCCSearch = function(key) {
            console.log("ada dia");
            console.log(key);

            var tempSearch = [];

            for(var i in $scope.dataPartsClass){
                if($scope.dataPartsClass[i].toLowerCase().indexOf(key.toLowerCase()) != -1){
                    tempSearch.push($scope.dataPartsClass[i]);
                } 
            }

            return tempSearch;
        };
    }

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mSCCRetailDisc = null; //Model
    $scope.cSCCRetailDisc = null; //Collection
    $scope.xSCCRetailDisc = {};
    $scope.xSCCRetailDisc.selected=[];

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;
            
            $scope.customActionSettings[0].visible = mode==editMode;
            $scope.customActionSettings[1].visible = mode==editMode;
            $scope.customActionSettings[0].enable = mode!=processingMode;
            $scope.customActionSettings[1].enable = mode!=processingMode;
            
            $scope.customActionSettings[2].visible = mode==viewMode;
            $scope.customActionSettings[3].visible = mode==viewMode;
            $scope.grid.enableCellEditOnFocus = mode=editMode;

        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                changeMode(processingMode);
                console.log("simpan");                
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];
                var x = new Date();
                var xdate = (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
                angular.forEach($scope.materialData, function(mtrl, mtrlIdx){
                    generatedData.push({EffectiveDate:xdate, ValidThru:xdate, PartId:mtrl.PartsId, Discount:0});
                });                
                XLSXInterface.writeToXLSX(generatedData, 'Template Diskon Penjualan Material');
            },
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                document.getElementById('uploadMRD').click();
            },
            title: 'Load File',
            rightsBit: 1
        }
    ];


    function validateXLS(data){
        var result = [];
        var error = [];

        $scope.grid.data = [];
        $timeout(function(){
            for(var i=0; i<data.length; i++){     
                var dMaterial = _.find(materialData, { 'PartsCode': data[i].PartsCode });
                if(dMaterial == undefined){
                    error.push('Bahan / Part '+data[i].PartsCode+' tidak ditemukan.');
                    continue;
                }
                var newObj = {
                    EffectiveDate:data[i].EffectiveDate, 
                    ValidThru:data[i].ValidThru,
                    PartId:data[i].PartId,
                    Discount:data[i].Discount,
                };

                result.push(newObj);
            }            
        });
        
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadMRD' ) );

        var a = myEl[0].files[0].name.split(".");
        var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        if (fileExt === "xlsx") {
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $timeout(function(){
                    myEl.value = '';
                    document.getElementById('uploadMRD').value = '';
                    // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    $scope.grid.data = validateXLS(json);
                    mDataCopy =  angular.copy($scope.grid.data);

                    uploadMode = true;
                    changeMode(editMode);
                });
            });
        } else {
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "File yang diupload bukan format .xlsx",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );   
        }
    };

    var dateFormat='dd/MM/yyyy';
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1 
    };
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        SCCRetailDiscount.getData().then(
            function(res){
                console.log("res=>",res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };
    
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id', field:'PartPriceDiscountPerSCCId', width:'7%' , visible: false},
            { name:'Tanggal Berlaku', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
            // { name:'No. Material', field:'Description', width: '15%' },
            { name:'SCC', field:'SCC', width: '15%' },
            { name:'Diskon',  field: 'Discount' },
            // { name:'Tanggal Berlaku', field:'EffectiveDate' },
        ]
    };

    $scope.validDateChange = function(dt){
        console.log("change->",$scope.mMtrlRetailDisc,dt);
    };
}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
});
