angular.module('app')
  .factory('SCCRetailDiscount', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;

    localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    return {
      getData: function() {
        var res=$http.get('/api/as/SCCRetailDiscount');
        return res;
      },
      create: function(data) {
        // data.Id = 0;
        data.EffectiveDate = localeDate(data.EffectiveDate);
        data.ValidThru = localeDate(data.ValidThru);
        data.PartPriceDiscountPerSCCId = 0;
        return $http.post('/api/as/SCCRetailDiscount', [data]);
      },
      update: function(data){
        data.EffectiveDate = localeDate(data.EffectiveDate);
        data.ValidThru = localeDate(data.ValidThru);
        return $http.put('/api/as/SCCRetailDiscount', [data]);
      },
      delete: function(id) {
        return $http.delete('/api/as/SCCRetailDiscount',{data:id,headers: {'Content-Type': 'application/json'}});
      },
      getPartsClass: function() {
        // return $http.get('/api/as/SCCRetailDiscount/GetSCCRetailPartsClasses');
        return $http.get('/api/as/SCCRetailDiscount/GetPartsClass');
      }
    }
  });
 //ddd