angular.module('app')
    .controller('DefectController', function($scope, $http, CurrentUser, Defect,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mDefect = null; //Model
    $scope.cDefect = null; //Collection
    $scope.xDefect = {};
    $scope.xDefect.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {        
        Defect.getData().then(
            function(res){
                console.log(res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };
  
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'DP Id',field:'DpId', width:'7%', visible:false },
            { name:'Nama', field:'Name' }
      
        ]
    };
});
