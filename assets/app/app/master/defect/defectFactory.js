angular.module('app')
  .factory('Defect', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/Defect');
        return res;
      },
      create: function(Defect) {
        console.log("Data Defect : ",Defect);
        Defect.Id = 0;
        return $http.post('/api/as/Defect', [Defect]);
      },
      update: function(Defect){
        console.log("Data Defect : ",Defect);
        return $http.put('/api/as/Defect', [Defect]);
      },
      delete: function(id) {
        console.log("Data Defect : ",id);
        return $http.delete('/api/as/Defect',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //