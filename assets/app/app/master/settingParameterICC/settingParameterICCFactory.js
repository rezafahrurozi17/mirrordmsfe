angular.module('app')
  .factory('SettingParameterICC', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var defer = $q.defer();
        //     defer.resolve(
        //       {
        //         "data" : {
        //           "Result":[{
        //               "PartParamICCId": 3,
        //               "EffectiveDate": "2017-04-06T00:00:00",
        //               "PartsClassId": 8,
        //               "Description": "A - Very Fast Moving",
        //               "IsDeadStock": true,
        //               "FrequencyMin": 1,
        //               "FrequencyMax": 7,
        //               "QtyMin": 4,
        //               "QtyMax": 10,
        //               "QtyParamOrderCycle": 50000,
        //               "QtyParamLeadTime": 50000,
        //               "QtyParamLeadTimeSS": 12000,
        //               "QtyParamDemandSS": 13000,
        //               "QtyParamMinMIP": 42000,
        //               "UsePartMinMIP": false
        //           }],
        //           "Start":1,
        //           "Limit":10,
        //           "Total":1
        //         }
        //       }
        //     );
        // return defer.promise;
        var res=$http.get('/api/as/PartsParamICC');
        return res;
      },

      // getTypeICC: function() {
      //   // var defer = $q.defer();
      //   //     defer.resolve(
      //   //       {
      //   //         "data" : {
      //   //           "Result":[{
      //   //               "ICCId":1,
      //   //               "Description":"A - Very Fast Moving",
      //   //           },
      //   //           {
      //   //               "ICCId":2,
      //   //               "Description":"B - Fast Moving",
      //   //           },
      //   //           {
      //   //               "ICCId":3,
      //   //               "Description":"C - Medium Moving",
      //   //           },
      //   //           {
      //   //               "ICCId":4,
      //   //               "Description":"D - Slow Moving",
      //   //           },
      //   //           {
      //   //               "ICCId":5,
      //   //               "Description":"E - Very Slow Moving",
      //   //           },
      //   //           {
      //   //               "ICCId":6,
      //   //               "Description":"F - Non Moving",
      //   //           }],
      //   //           "Start":1,
      //   //           "Limit":10,
      //   //           "Total":1
      //   //         }
      //   //       }
      //   //     );
      //   // return defer.promise;
      //   var res=$http.get('/api/as/PartsClasses?ClassTypeId=8');
      //   return res;
      // },

      create: function(data) {
        console.log("AAA",data);
        data.PartParamICCId = 0;
        data.QtyParamMinMIP = 0;
        return $http.post('/api/as/PartsParamICC', [data]);
      },
      update: function(data){
        console.log("AAA",data);
        return $http.put('/api/as/PartsParamICC', [data]);
      },
      delete: function(id) {
        console.log("AAA",id);
        return $http.delete('/api/as/PartsParamICC', {data:id,headers: {'Content-Type': 'application/json'}});
      },
      getListICC: function(classTypeId,partsClassId) {
        return $http.get('/api/as/PartsParamICC/GetPartsClassesFromType?ClassTypeId='+classTypeId+'&PartsClassId='+partsClassId);
      },
    }
  });
 //ddd
