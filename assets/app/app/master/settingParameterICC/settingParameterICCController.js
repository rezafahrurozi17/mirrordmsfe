angular.module('app')
    .controller('SettingParameterICCController', function($scope, $http,PartsGlobal, CurrentUser, SettingParameterICC, PartsCategory, $timeout, $filter) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mSettingParameterICC = null; //Model
    $scope.cSettingParameterICC = null; //Collection
    $scope.xSettingParameterICC = {};
    $scope.xSettingParameterICC.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.validDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];

    // SettingParameterICC.getTypeICC().then(
    //     function(res){
    //         console.log("res=>",res);
    //         $scope.optionsTypeICC = res.data.Result;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );

    // PartsCategory.getData(2).then(
    // SettingParameterICC.getListICC(2).then(
    //     function(res){
    //         $scope.optionsTypeICC = res.data.Result;
    //         console.log("data=>",res.data);
    //         return res.data;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );

    $scope.getData = function() {
        SettingParameterICC.getData().then(
            function(res){
                console.log("res=>",res);
                $scope.gridParent.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    $scope.onBeforeNew = function(){
        $scope.mSettingParameterICC = {};
        $scope.mSettingParameterICC.UsePartMinMIP = 0;
    }

    $scope.onShowDetail = function(row, mode) {
        console.log("---> onShowDetail");

        if (mode == 'view') {
            console.log("[VIEW]");
            console.log(row);
            SettingParameterICC.getListICC(2,row.PartsClassId).then(
                function(res){
                    $scope.optionsTypeICC = res.data.Result;
                    console.log("data=>",res.data);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );        
        }         
        if (mode == 'edit') {
            console.log("[EDIT]");
            console.log(row);
            SettingParameterICC.getListICC(2,row.PartsClassId).then(
                function(res){
                    $scope.optionsTypeICC = res.data.Result;
                    console.log("data=>",res.data);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );        
        } 
        if (mode == 'new') {
            SettingParameterICC.getListICC(2,0).then(
                function(res){
                    $scope.optionsTypeICC = res.data.Result;
                    console.log("data=>",res.data);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );        
        }        
    }

    $scope.disableIpt = function(){
        // console.log("ASCAF");
        $scope.mSettingParameterICC.UsePartMinMIP = !$scope.mSettingParameterICC.UsePartMinMIP;
        if (!$scope.mSettingParameterICC.UsePartMinMIP) {
            document.getElementById("QtyParamMinMIP").disabled = true;
            $scope.mSettingParameterICC.QtyParamMinMIP = null;
        } else {
            document.getElementById("QtyParamMinMIP").disabled = false;
        }
    }

    $scope.onValidateSave=function(data,res){
      $scope.EffectiveDate=data.EffectiveDate;
      $scope.ValidThru=data.ValidThru;
      data.EffectiveDate = data.EffectiveDate.getFullYear() + '-' + (data.EffectiveDate.getMonth() +1) + '-' + data.EffectiveDate.getDate();
      data.ValidThru = data.ValidThru.getFullYear() + '-' + (data.ValidThru.getMonth() +1) + '-' + data.ValidThru.getDate();
      console.log('data effDate COntroller', data.EffectiveDate);

      var insertResponse = res.data;
      console.log("insertResponse Part=> ", res.data);
      PartsGlobal.showAlert({
          title: "Setting Parameter ICC",
          content: "Data berhasil disimpan",
          type: 'success'
      });
      return true;
    }

    var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.TAMBit == 0" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.gridParent = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id', field:'ParamICCId', width:'7%' , visible: false},
            { name:'Tanggal Berlaku', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
            { displayName:'ICC', field:'Description'},
            { name:'Kategori Dead Stock', field:'IsDeadStock', cellTemplate: '<input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="true" ng-false-value="false" disabled="disabled">', width:'20%'},
            { name: 'Action', width: 120, cellTemplate: gridActionButtonTemplate }
        ]
    };

}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
});
