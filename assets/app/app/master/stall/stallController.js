angular.module('app')
    .controller('StallMstController', function($scope, $http, $filter, CurrentUser, StallMst,bsAlert, GroupBPFactory, $timeout, bsNotify, ngDialog) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.deletedJadwalDetail = [];
            $scope.showModal_deleteStall = false;

            $scope.schedule_mode = false;

            $scope.getComboboxData();
            $scope.FilterStallSchedule();
        });

        $scope.getComboboxData = function() {
                StallMst.getStallType().then(function(res) {
                    $scope.StallTypeData = res.data.Result;
                });

                StallMst.getTechnician().then(function(res) {
                    $scope.TechnicianData = res.data.Result;
                });

            }
            //----------------------------------
            // Initialization
            //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mStallMst = null; //Model
        $scope.cStallMst = null; //Collection
        $scope.xStallMst = {};
        $scope.xStallMst.selected = [];

        // $scope.MainFilterColumnList = [{name: "Jam Dari", value: 1}, {name: "Jam Sampai", value: 2}, {name: "Tanggal Dari", value: 3},
		// 								{name: "Tanggal Sampai", value: 4}];

        // $scope.Main_Filter_Button_Clicked = function () {
        //     //$scope.gridJadwal();
        //     console.log("cari filter", $scope.gridJadwal.data);
        //     $scope.gridJadwalTableHeight();
        //     console.log("filt",$scope.MainFilterColumn);
        //     console.log("filt32",$scope.MainFilterText);
        // }
                                

        $scope.isBP = false;
        $scope.TypeData = [
            { Id: 1, Name: "BP" },
            { Id: 2, Name: "GR" },
            { Id: 3, Name: "EM" },
            { Id: 4, Name: "Pre Diagnose" }
        ];

        var dateFormat = 'dd-MM-yyyy';
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1 
        };

       
        
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.hstep = 1;
        $scope.mstep = 10;
        var gridData = [];
        $scope.getData = function() {
           
            StallMst.getData().then(function(res) {
                    for (var i in res.data.Result) {
                        res.data.Result[i]["AvailableTimePrev"]= res.data.Result[i]["AvailableTime"];
                        res.data.Result[i]["JamTersedia"]= res.data.Result[i]["AvailableTime"] + ' ' +'Jam';
                        res.data.Result[i]["OpenTimePrev"] = res.data.Result[i]["OpenTime"];
                        res.data.Result[i]["CloseTimePrev"] = res.data.Result[i]["CloseTime"];

                        // res.data.Result[i]["JamIstirahat1"] =  res.data.Result[i]["StartBreakTime1"] + ' - ' + res.data.Result[i]["EndBreakTime1"];
                        // res.data.Result[i]["JamIstirahat2"] =  res.data.Result[i]["StartBreakTime2"] + ' - ' + res.data.Result[i]["EndBreakTime2"];
                        // res.data.Result[i]["JamIstirahat3"] =  res.data.Result[i]["StartBreakTime3"] + ' - ' + res.data.Result[i]["EndBreakTime3"];


                        // switch(res.data.Result[i]["Type"])
                        // {
                        //     case 0:
                        //         res.data.Result[i]["TypeName"] = "General Repair";
                        //         break;
                        //     case 2:
                        //         res.data.Result[i]["TypeName"] = "Service Berkala";
                        //         break;
                        //     case 3:
                        //         res.data.Result[i]["TypeName"] = "Express Maintenance";
                        //         break;
                        //     case 4:
                        //         res.data.Result[i]["TypeName"] = "Pre-Diagnose";
                        //         break;
                        //     case 100:
                        //         res.data.Result[i]["TypeName"] = "Stall Washing";
                        //         break;
                        //     default:
                        //         res.data.Result[i]["TypeName"] = "n/a"
                        //         break;
                        // }

                        // var time = {
                        //     "AvailableTime":res.data.Result[i]["AvailableTime"],
                        //     "OpenTime":res.data.Result[i]["OpenTime"],
                        //     "CloseTime":res.data.Result[i]["CloseTime"],
                        //     "StartBreakTime":res.data.Result[i]["StartBreakTime"],
                        //     "EndBreakTime":res.data.Result[i]["EndBreakTime"]
                        // };
                        // for (var key in time) {
                        //     if (time[key]!==null && time[key] !== undefined) {
                        //         var tdy = new Date();
                        //         var temp = time[key].split(":");
                        //         tdy.setHours(temp[0],temp[1],temp[2]);
                        //         res.data.Result[i][key] = tdy;
                        //     }                        
                        // }
                    }
                    $scope.grid.data = res.data.Result;
                    //$scope.VehicleModelData = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }



        /*
        GroupBPFactory.getData().then(
            function(res){
                $scope.GroupBPData = res.data.Result;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
        */
       $scope.allDisabled = false;
        $scope.selectType = function(selected) {
            if (selected.Id == 1) {
                $scope.isBP = true;
            } else {
                $scope.isBP = false;
            }
        }

        $scope.onShowDetail = function(mdl, mode) {
            if (mdl.Type == 1) {
                $scope.isBP = true;
            } else {
                $scope.isBP = false;
            }

            if (mode == 'new') {
                $scope.schedule_mode = false;
                $scope.mStallMst.StallId = 0;
            } else {
                // $scope.ViewJadwalStall(mdl);
                // if($scope.schedule_mode == false)
                //if ($scope.schedule_mode == false) // comment by anita
                    $scope.getDetailStall(mdl);
            }
            if(mode == 'view'){
                $scope.formApi.setFormReadOnly(true);
                $scope.allDisabled = true;
            }else if(mode == 'edit'){
                $scope.formApi.setFormReadOnly(false);
                $scope.allDisabled = false;
            }else if(mode == 'new'){
                $scope.formApi.setFormReadOnly(false);
                $scope.allDisabled = false;
            }

        }

        $scope.onShowDetail = function(mdl, mode) {
            console.log('model getDetailStall', mdl, mode);
            $scope.mStallMst = {};
            $scope.mStallMst = angular.copy(mdl);
            if (mdl.Type == 1) {
                $scope.isBP = true;
            } else {
                $scope.isBP = false;
            }

            if (mode == 'new') {
                $scope.schedule_mode = false;
            } else {
                // $scope.ViewJadwalStall(mdl);
                // if($scope.schedule_mode == false)
                //if ($scope.schedule_mode == false) (*// anita comment * karena ubah schedule juga perlu)
                    $scope.getDetailStall($scope.mStallMst);
            }
        }

        $scope.getCal = function (item, event){
            console.log("cek item", item);
            var copyStartBreak1 = angular.copy($scope.mStallMst.StartBreakTime1);
            var copyEndBreak1 = angular.copy($scope.mStallMst.EndBreakTime1);
            var copyStartBreak2 = angular.copy($scope.mStallMst.StartBreakTime2);
            var copyEndBreak2 = angular.copy($scope.mStallMst.EndBreakTime2);
            var copyStartBreak3 = angular.copy($scope.mStallMst.StartBreakTime3);
            var copyEndBreak3 = angular.copy($scope.mStallMst.EndBreakTime3);

            console.log("copyStartBreak1",copyStartBreak1);
            console.log("copyEndBreak1",copyEndBreak1);
            console.log("copyStartBreak2",copyStartBreak2);
            console.log("copyEndBreak2",copyEndBreak2);
            console.log("copyStartBreak3",copyStartBreak3);
            console.log("copyEndBreak3",copyEndBreak3);
            
            if ((copyStartBreak1 == null || copyStartBreak1 == undefined || copyStartBreak1 == '')){
                copyStartBreak1 = 0;
            }
            if((copyEndBreak1 == null || copyEndBreak1 == undefined || copyEndBreak1 == '')){
                copyEndBreak1 = 0;
            }
            if ((copyStartBreak2 == null || copyStartBreak2 == undefined || copyStartBreak2 == '')){
                copyStartBreak2 = 0;
            }
            if((copyEndBreak2 == null || copyEndBreak2 == undefined || copyEndBreak2 == '')){
                copyEndBreak2 = 0;
            }
            if ((copyStartBreak3 == null || copyStartBreak3 == undefined || copyStartBreak3 == '')){
                copyStartBreak3 = 0;
            }
            if((copyEndBreak3 == null || copyEndBreak3 == undefined || copyEndBreak3 == '')){
                copyEndBreak3 = 0;
            }

            var totBreak1 = (copyEndBreak1 - copyStartBreak1);
            var totBreak2 = (copyEndBreak2 - copyStartBreak2);
            var totBreak3 = (copyEndBreak3 - copyStartBreak3);
            console.log("totBreak1",totBreak1);
            console.log("totBreak2",totBreak2);
            console.log("totBreak3",totBreak3);
            var calcJamTersedia = ($scope.mStallMst.DefaultCloseTime -  $scope.mStallMst.DefaultOpenTime) - (totBreak1 + totBreak2 + totBreak3)
            console.log("cek calcJamTersedia", calcJamTersedia);
            calcJamTersedia = (calcJamTersedia / (1000 * 60 * 60));
            $scope.mStallMst.AvailableTime = parseFloat(calcJamTersedia.toFixed(2));
            console.log("mStallMst.AvailableTime", $scope.mStallMst.AvailableTime);
        }

        $scope.ViewJadwalStall = function(row) {
            console.log("barisnya");
            console.log(row);
            var filterColumn = ($scope.MainFilterColumn == undefined) ? 0 : $scope.MainFilterColumn;
            var filterText = ($scope.MainFilterText == undefined) ? "" : $scope.MainFilterText;

            StallMst.getStallSchedule(row.StallId, filterColumn, filterText).then(function(res) {
                $scope.gridJadwal.data = res.data.Result;
                $scope.mStallMst.StallSchedule = res.data.Result;
            })

            $scope.schedule_mode = true;
        }

        $scope.getDetailStall = function(model) {
            console.log('model getDetailStall', model);
            //if ($scope.schedule_mode == true) //comment by anita, karna ganti schedule juga butuh 2018
            //    return;

                if(model["JamIstirahat1"] != null && model["JamIstirahat1"] != undefined && model["JamIstirahat1"] != ""){
                    var tempJamIstirahat1 = model["JamIstirahat1"].split("-");
                    model["StartBreakTime1"] = tempJamIstirahat1[0].trim();
                    model["EndBreakTime1"] = tempJamIstirahat1[1].trim();
                    model["StartBreakTime1"] = model["StartBreakTime1"].split(":");
                    model["EndBreakTime1"] = model["EndBreakTime1"].split(":");
                    model["StartBreakTime1"] = new Date(new Date(1, 1, 1970, model["StartBreakTime1"][0], model["StartBreakTime1"][1], 0))
                    model["EndBreakTime1"] = new Date(new Date(1, 1, 1970, model["EndBreakTime1"][0], model["EndBreakTime1"][1], 0))
                }

                if(model["JamIstirahat2"] != null && model["JamIstirahat2"] != undefined && model["JamIstirahat2"] != ""){
                    var tempJamIstirahat2 = model["JamIstirahat2"].split("-");
                    model["StartBreakTime2"] = tempJamIstirahat2[0].trim();
                    model["EndBreakTime2"] = tempJamIstirahat2[1].trim();
                    model["StartBreakTime2"] = model["StartBreakTime2"].split(":");
                    model["EndBreakTime2"] = model["EndBreakTime2"].split(":");
                    model["StartBreakTime2"] = new Date(new Date(1, 1, 1970, model["StartBreakTime2"][0], model["StartBreakTime2"][1], 0))
                    model["EndBreakTime2"] = new Date(new Date(1, 1, 1970, model["EndBreakTime2"][0], model["EndBreakTime2"][1], 0))
                }

                if(model["JamIstirahat3"] != null && model["JamIstirahat3"] != undefined && model["JamIstirahat3"] != ""){
                    var tempJamIstirahat3 = model["JamIstirahat3"].split("-");
                    model["StartBreakTime3"] = tempJamIstirahat3[0].trim();
                    model["EndBreakTime3"] = tempJamIstirahat3[1].trim();
                    model["StartBreakTime3"] = model["StartBreakTime3"].split(":");
                    model["EndBreakTime3"] = model["EndBreakTime3"].split(":");
                    model["StartBreakTime3"] = new Date(new Date(1, 1, 1970, model["StartBreakTime3"][0], model["StartBreakTime3"][1], 0))
                    model["EndBreakTime3"] = new Date(new Date(1, 1, 1970, model["EndBreakTime3"][0], model["EndBreakTime3"][1], 0))
                }
                
                if ($scope.schedule_mode == true){
                    if(model["DefaultOpenTime"] != null && model["DefaultOpenTime"] != undefined && model["DefaultOpenTime"] != ""){
                        model["DefaultOpenTime"] = model["DefaultOpenTime"].split(":");
                        model["DefaultOpenTime"] = new Date(new Date(1, 1, 1970, model["DefaultOpenTime"][0], model["DefaultOpenTime"][1], 0))
                    }

                    if(model["DefaultCloseTime"] != null && model["DefaultCloseTime"] != undefined && model["DefaultCloseTime"] != ""){
                        model["DefaultCloseTime"] = model["DefaultCloseTime"].split(":");
                        model["DefaultCloseTime"] = new Date(new Date(1, 1, 1970, model["DefaultCloseTime"][0], model["DefaultCloseTime"][1], 0))
                    }

                }
                

                console.log('model getDetailStall anita 4', model, $scope.schedule_mode); 

                  
                    // var tempJamIstirahat1 = model["JamIstirahat1"].split("-");
                    // var tempJamIstirahat2 = model["JamIstirahat2"].split("-");
                    // var tempJamIstirahat3 = model["JamIstirahat3"].split("-");

                    // model["StartBreakTime1"] = tempJamIstirahat1[0].trim();
                    // model["EndBreakTime1"] = tempJamIstirahat1[1].trim();
                    // model["StartBreakTime2"] = tempJamIstirahat2[0].trim();
                    // model["EndBreakTime2"] = tempJamIstirahat2[1].trim();
                    // model["StartBreakTime3"] = tempJamIstirahat3[0].trim();
                    // model["EndBreakTime3"] = tempJamIstirahat3[1].trim();

                    // model["StartBreakTime1"] = model["StartBreakTime1"].split(":");
                    // model["EndBreakTime1"] = model["EndBreakTime1"].split(":");
                    // model["StartBreakTime2"] = model["StartBreakTime2"].split(":");
                    // model["EndBreakTime2"] = model["EndBreakTime2"].split(":");
                    // model["StartBreakTime3"] = model["StartBreakTime3"].split(":");
                    // model["EndBreakTime3"] = model["EndBreakTime3"].split(":");

                if ($scope.schedule_mode == false){
                    var tempOpenTime = model["DefaultOpenTime"].split(":");
                    var tempCloseTime = model["DefaultCloseTime"].split(":");
                    model["DefaultOpenTime"] = new Date(new Date(1, 1, 1970, tempOpenTime[0], tempOpenTime[1], 0))
                    model["DefaultCloseTime"] = new Date(new Date(1, 1, 1970, tempCloseTime[0], tempCloseTime[1], 0))
                } 
                   // model["DefaultOpenTime"] = new Date(new Date(1, 1, 1970, model["DefaultOpenTime"][0], model["DefaultOpenTime"][1], 0))
                    // model["DefaultCloseTime"] = new Date(new Date(1, 1, 1970, model["DefaultCloseTime"][0], model["DefaultCloseTime"][1], 0))
                    // model["StartBreakTime1"] = new Date(new Date(1, 1, 1970, model["StartBreakTime1"][0], model["StartBreakTime1"][1], 0))
                    // model["EndBreakTime1"] = new Date(new Date(1, 1, 1970, model["EndBreakTime1"][0], model["EndBreakTime1"][1], 0))
                    // model["StartBreakTime2"] = new Date(new Date(1, 1, 1970, model["StartBreakTime2"][0], model["StartBreakTime2"][1], 0))
                    // model["EndBreakTime2"] = new Date(new Date(1, 1, 1970, model["EndBreakTime2"][0], model["EndBreakTime2"][1], 0))
                    // model["StartBreakTime3"] = new Date(new Date(1, 1, 1970, model["StartBreakTime3"][0], model["StartBreakTime3"][1], 0))
                    // model["EndBreakTime3"] = new Date(new Date(1, 1, 1970, model["EndBreakTime3"][0], model["EndBreakTime3"][1], 0))
                //}


            if (model.StatusCode === "Aktif") {
                model.StatusCode = 1;
            } else {
                model.StatusCode = 0;
            };

        

            $scope.mStallMst = model;
            $scope.mStallMst.Technician1 = model.Technician1;
            // $scope.mStallMst.Technician1 = {
            //     'TechnicianData' : model.Technician1,
            //     'EmployeeName' : model.Technician1Name
            // }
            console.log("teknisi1", $scope.mStallMst.Technician1);
            $scope.mStallMst.Technician2 = model.Technician2;
            // $scope.mStallMst.Technician2 = {
            //     'TechnicianData' : model.Technician2,
            //     'EmployeeName' : model.Technician2Name
            // }
            console.log("teknisi2", $scope.mStallMst.Technician2);
            $scope.mStallMst.Technician3 = model.Technician3;
            // $scope.mStallMst.Technician3 = {
            //     'TechnicianData' : model.Technician3,
            //     'EmployeeName' : model.Technician3Name
            // }
            console.log("teknisi3", $scope.mStallMst.Technician3);
        }

        $scope.ViewJadwalStall = function(row) {

            console.log("barisnya2");
            console.log(row);
            var filterColumn = ($scope.MainFilterColumn == undefined) ? 0 : $scope.MainFilterColumn;
            var filterText = ($scope.MainFilterText == undefined) ? "" : $scope.MainFilterText;
            console.log("filt",$scope.MainFilterColumn);
            console.log("filt32",$scope.MainFilterText);

            var model = row;
            if(model["JamIstirahat1"] != null && model["JamIstirahat1"] != undefined && model["JamIstirahat1"] != ""){
                var tempJamIstirahat1 = model["JamIstirahat1"].split("-");
                model["StartBreakTime1"] = tempJamIstirahat1[0].trim();
                model["EndBreakTime1"] = tempJamIstirahat1[1].trim();
                model["StartBreakTime1"] = model["StartBreakTime1"].split(":");
                model["EndBreakTime1"] = model["EndBreakTime1"].split(":");
                model["StartBreakTime1"] = new Date(new Date(1, 1, 1970, model["StartBreakTime1"][0], model["StartBreakTime1"][1], 0))
                model["EndBreakTime1"] = new Date(new Date(1, 1, 1970, model["EndBreakTime1"][0], model["EndBreakTime1"][1], 0))
            }

            if(model["JamIstirahat2"] != null && model["JamIstirahat2"] != undefined && model["JamIstirahat2"] != ""){
                var tempJamIstirahat2 = model["JamIstirahat2"].split("-");
                model["StartBreakTime2"] = tempJamIstirahat2[0].trim();
                model["EndBreakTime2"] = tempJamIstirahat2[1].trim();
                model["StartBreakTime2"] = model["StartBreakTime2"].split(":");
                model["EndBreakTime2"] = model["EndBreakTime2"].split(":");
                model["StartBreakTime2"] = new Date(new Date(1, 1, 1970, model["StartBreakTime2"][0], model["StartBreakTime2"][1], 0))
                model["EndBreakTime2"] = new Date(new Date(1, 1, 1970, model["EndBreakTime2"][0], model["EndBreakTime2"][1], 0))
            }

            if(model["JamIstirahat3"] != null && model["JamIstirahat3"] != undefined && model["JamIstirahat3"] != ""){
                var tempJamIstirahat3 = model["JamIstirahat3"].split("-");
                model["StartBreakTime3"] = tempJamIstirahat3[0].trim();
                model["EndBreakTime3"] = tempJamIstirahat3[1].trim();
                model["StartBreakTime3"] = model["StartBreakTime3"].split(":");
                model["EndBreakTime3"] = model["EndBreakTime3"].split(":");
                model["StartBreakTime3"] = new Date(new Date(1, 1, 1970, model["StartBreakTime3"][0], model["StartBreakTime3"][1], 0))
                model["EndBreakTime3"] = new Date(new Date(1, 1, 1970, model["EndBreakTime3"][0], model["EndBreakTime3"][1], 0))
            }
        

            StallMst.getStallSchedule(row.StallId, filterColumn, filterText).then(function(res) {
                for (var i in res.data.Result) {
                    var mData = angular.copy(res.data.Result[i]);

                    // res.data.Result[i]["FromDate"] = mData["InactiveStart"].getFullYear() + "-" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveStart"].getDate()).slice(-2);
                    // res.data.Result[i]["FromTime"] = mData["InactiveStart"].getHours() + ":" + ("0" + (mData["InactiveStart"].getMinutes() + 1)).slice(-2);
                    // res.data.Result[i]["ToDate"] = mData["InactiveEnd"].getFullYear() + "-" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveEnd"].getDate()).slice(-2);
                    // res.data.Result[i]["ToTime"] = mData["InactiveEnd"].getHours() + ":" + ("0" + (mData["InactiveEnd"].getMinutes() + 1)).slice(-2);

                    // res.data.Result[i]["FromDate"] = ("0" + mData["InactiveStart"].getDate()).slice(-2) + "-" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + "-" + mData["InactiveStart"].getFullYear() ;
                    // res.data.Result[i]["FromTime"] = ("0"+mData["InactiveStart"].getHours()).slice(-2) + ":" + ("0" + (mData["InactiveStart"].getMinutes() + 1)).slice(-2);
                    // res.data.Result[i]["ToDate"] = ("0" + mData["InactiveEnd"].getDate()).slice(-2) + "-" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + "-" + mData["InactiveEnd"].getFullYear() ;
                    // res.data.Result[i]["ToTime"] = ("0"+mData["InactiveEnd"].getHours()).slice(-2) + ":" + ("0" + (mData["InactiveEnd"].getMinutes() + 1)).slice(-2);

                    // res.data.Result[i].InactiveStart = mData["InactiveStart"].getFullYear() + "-" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveStart"].getDate()).slice(-2) + "" + " " + mData["InactiveStart"].getHours() + ":" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + ":00.000";
                    // res.data.Result[i].InactiveEnd = mData["InactiveEnd"].getFullYear() + "-" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveEnd"].getDate()).slice(-2) + " " + mData["InactiveEnd"].getHours() + ":" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + ":00.000";
                    
                    res.data.Result[i].FromDate = $filter('date')(mData.InactiveStart, 'yyyy-MM-dd')
                    res.data.Result[i].FromTime = $filter('date')(mData.InactiveStart, 'HH:mm')
                    res.data.Result[i].ToDate = $filter('date')(mData.InactiveEnd, 'yyyy-MM-dd')
                    res.data.Result[i].ToTime = $filter('date')(mData.InactiveEnd, 'HH:mm')

                    res.data.Result[i].InactiveStart = $filter('date')(mData.InactiveStart, 'yyyy-MM-dd HH:mm')
                    res.data.Result[i].InactiveEnd = $filter('date')(mData.InactiveEnd, 'yyyy-MM-dd HH:mm')


                    res.data.Result[i].JamIstirahat1 = row.JamIstirahat1
                    res.data.Result[i].JamIstirahat2 = row.JamIstirahat2
                    res.data.Result[i].JamIstirahat3 = row.JamIstirahat3
                    res.data.Result[i].DefaultOpenTime = row.DefaultOpenTime
                    res.data.Result[i].DefaultCloseTime = row.DefaultCloseTime

                }

                $scope.gridJadwal.data = res.data.Result;

                // $scope.gridJadwal.data.push({
                //         DefaultOpenTime : row.DefaultOpenTime,
                //         DefaultCloseTime : row.DefaultCloseTime,
                //         JamIstirahat1 : row.JamIstirahat1,
                //         JamIstirahat2 : row.JamIstirahat2,
                //         JamIstirahat3 : row.JamIstirahat3
                // });
                
                console.log("anita3",$scope.gridJadwal.data);
            })
            $scope.schedule_mode = true;
        }

        // $scope.acteditStatus = function(status, id){

        // }
        $scope.onValidateSave = function(row, mode) {
            console.log("OK onValidateSave before save", row, mode);
            // disini if gak terpenuhi, langsung return false
            // ===================================================  Validasi cek jam istirahat ============================================ start

            var copyBuka = angular.copy(row.DefaultOpenTime)
            var cekBuka = new Date(2021, 0, 1, copyBuka.getHours(), copyBuka.getMinutes(), 0)
            cekBuka = cekBuka.getTime()

            var copyTutup = angular.copy(row.DefaultCloseTime)
            var cekTutup = new Date(2021, 0, 1, copyTutup.getHours(), copyTutup.getMinutes(), 0)
            cekTutup = cekTutup.getTime()

            var copyStart1 = angular.copy(row.StartBreakTime1)
            var cekStart1 = new Date(2021, 0, 1, copyStart1.getHours(), copyStart1.getMinutes(), 0)
            cekStart1 = cekStart1.getTime()

            var copyEnd1 = angular.copy(row.EndBreakTime1)
            var cekEnd1 = new Date(2021, 0, 1, copyEnd1.getHours(), copyEnd1.getMinutes(), 0)
            cekEnd1 = cekEnd1.getTime()

            if (cekStart1 > cekEnd1){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Ada kesalahan pada jam istirahat 1",
                });
                return false
            } else {
                console.log('kondisi 1 bener')
            }

            if (cekBuka >= cekStart1) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Jam awal istirahat 1 tidak boleh lebih kecil atau sama dari jam buka stall",
                });
                return false
            }

            if (cekTutup <= cekEnd1) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Jam akhir istirahat 1 tidak boleh lebih besar atau sama dari jam tutup stall",
                });
                return false
            }
            

            //////////////////////////////////////////////////////////

            if ((row.StartBreakTime2 !== null && row.StartBreakTime2 !== undefined && row.StartBreakTime2 !== '') || (row.EndBreakTime2 !== null && row.EndBreakTime2 !== undefined && row.EndBreakTime2 !== '')) {
                if (row.StartBreakTime2 == null || row.StartBreakTime2 == undefined || row.StartBreakTime2 == '' || row.EndBreakTime2 == null || row.EndBreakTime2 == undefined || row.EndBreakTime2 == '') {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada kesalahan pada jam istirahat 2",
                    });
                    return false
                } else {
                    var copyStart2 = angular.copy(row.StartBreakTime2)
                    var cekStart2 = new Date(2021, 0, 1, copyStart2.getHours(), copyStart2.getMinutes(), 0)
                    cekStart2 = cekStart2.getTime()

                    var copyEnd2 = angular.copy(row.EndBreakTime2)
                    var cekEnd2 = new Date(2021, 0, 1, copyEnd2.getHours(), copyEnd2.getMinutes(), 0)
                    cekEnd2 = cekEnd2.getTime()

                    if (cekStart2 <= cekEnd1) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Jam istirahat 2 tidak boleh kurang dari Jam istirahat 1 atau bersinggungan dengan Jam istirahat 1",
                        });
                        return false
                    } else if (cekEnd2 <= cekStart2) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Ada kesalahan pada jam istirahat 2",
                        });
                        return false
                    } else {
                        console.log('kondisi 2 bener')
                        if (cekTutup <= cekEnd2) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Jam akhir istirahat 2 tidak boleh lebih besar atau sama dari jam tutup stall",
                            });
                            return false
                        } else {
                            console.log ('jam akhir 2 ok')
                        }
                    }
                } 
            } else {
                console.log ('jam istirahat 2 kosong semua')
                if ((row.StartBreakTime3 !== null && row.StartBreakTime3 !== undefined && row.StartBreakTime3 !== '') || (row.EndBreakTime3 !== null && row.EndBreakTime3 !== undefined && row.EndBreakTime3 !== '')) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon isi jam istirahat 2 terlebih dulu",
                    });
                    return false
                }
            }

            //////////////////////////////////////////////////////////
            
            if ((row.StartBreakTime3 !== null && row.StartBreakTime3 !== undefined && row.StartBreakTime3 !== '') || (row.EndBreakTime3 !== null && row.EndBreakTime3 !== undefined && row.EndBreakTime3 !== '')) {
                if (row.StartBreakTime3 == null || row.StartBreakTime3 == undefined || row.StartBreakTime3 == '' || row.EndBreakTime3 == null || row.EndBreakTime3 == undefined || row.EndBreakTime3 == '') {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada kesalahan pada jam istirahat 3",
                    });
                    return false
                } else {
                    var copyStart3 = angular.copy(row.StartBreakTime3)
                    var cekStart3 = new Date(2021, 0, 1, copyStart3.getHours(), copyStart3.getMinutes(), 0)
                    cekStart3 = cekStart3.getTime()

                    var copyEnd3 = angular.copy(row.EndBreakTime3)
                    var cekEnd3 = new Date(2021, 0, 1, copyEnd3.getHours(), copyEnd3.getMinutes(), 0)
                    cekEnd3 = cekEnd3.getTime()

                    if (cekStart3 <= cekEnd2) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Jam istirahat 3 tidak boleh kurang dari Jam istirahat 2 atau bersinggungan dengan Jam istirahat 2",
                        });
                        return false
                    } else if (cekEnd3 <= cekStart3){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Ada kesalahan pada jam istirahat 3",
                        });
                        return false
                    } else {
                        console.log('kondisi 3 bener')
                        if (cekTutup <= cekEnd3) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Jam akhir istirahat 3 tidak boleh lebih besar atau sama dari jam tutup stall",
                            });
                            return false
                        } else {
                            console.log ('jam akhir 3 ok')
                        }
                    }
                } 
            } else {
                console.log ('jam istirahat 3 kosong semua')
            }

            // ===================================================  Validasi cek jam istirahat ============================================ end

            if (mode == "update") {
                console.log("masuk gak ?");
                console.log("kai e ",$scope.schedule_mode);
                if($scope.schedule_mode == true){

                    if ($scope.gridJadwal.data.length == 0) {
                        console.log("sini masuk gak ?");
                        // bsNotify.show({
                        //     size: 'big',
                        //     type: 'danger',
                        //     title: "Mohon Input Data Terlebih Dahulu",
                        // });
                        // return false;
                        bsAlert.alert({
                            title: "Jadwal Stall Kosong. Tetap Simpan Data?",
                            text: "",
                            type: "question",
                            showCancelButton: true
                        },
                        function() {
                            // $scope.doCustomSave(row,mode)
                            var valid = $scope.doCustomSaveCloneForUpdate(row,mode)
                            if (valid){
                                return true
                            } else {
                                return false
                            }
                        },
                        function() {
                            return false
                        }
                    )
                    } else {
                        var valid = $scope.doCustomSaveCloneForUpdate(row,mode)
                        if (valid){
                            return true
                        } else {
                            return false
                        }
                    };
                }else{
                    var valid = $scope.doCustomSaveCloneForUpdate(row,mode)
                    if (valid){
                        return true
                    } else {
                        return false
                    }
                }
            } else {
                var valid = $scope.SaveCreateMode(row, mode) // validasi create di sini, krn kl di docustomesave kl gagal hrs pake setmode('detail') malah jd jelek. bolak balik tampilannya jelek
                if (valid){
                    return true;
                } else {
                    return false;
                }
            }
        };

        $scope.SaveCreateMode = function(mdlx, modex) {
            var mdl = angular.copy(mdlx)
            var mode = angular.copy(modex)

            var saveObj = angular.copy(mdl);
            saveObj["DefaultOpenTime"] = mdl["DefaultOpenTime"];
            saveObj["DefaultCloseTime"] = mdl["DefaultCloseTime"];
            
            console.log("ANITA2 TES", saveObj);

            delete saveObj["AvailableTimePrev"];
            delete saveObj["OpenTimePrev"];
            delete saveObj["CloseTimePrev"];

                var StartBreakTime2 = null;
                var StartBreakTime3 = null;
                var EndBreakTime2 = null;
                var EndBreakTime3 = null;
            
                if(saveObj["StartBreakTime2"] != null || saveObj["EndBreakTime2"] != null)
                {
                    StartBreakTime2 = new Date(saveObj["StartBreakTime2"]);
                    EndBreakTime2 = new Date(saveObj["EndBreakTime2"]);
                }

                if(saveObj["StartBreakTime3"] != null || saveObj["EndBreakTime3"] != null)
                {
                    StartBreakTime3 = new Date(saveObj["StartBreakTime3"]);
                    EndBreakTime3 = new Date(saveObj["EndBreakTime3"]);
                }

                var time = {
                    // "AvailableTime":new Date(saveObj["AvailableTime"]),
                    // "OpenTime":new Date(saveObj["OpenTime"]),
                    // "CloseTime":new Date(saveObj["CloseTime"]),
                    "DefaultOpenTime": new Date(saveObj["DefaultOpenTime"]),
                    "DefaultCloseTime": new Date(saveObj["DefaultCloseTime"]),
                    "StartBreakTime1": new Date(saveObj["StartBreakTime1"]),
                    "EndBreakTime1": new Date(saveObj["EndBreakTime1"]),
                    "StartBreakTime2": StartBreakTime2,
                    "EndBreakTime2": EndBreakTime2,
                    "StartBreakTime3": StartBreakTime3,
                    "EndBreakTime3": EndBreakTime3
                };
                console.log('data time', time);

                for (var key in time) {
                    if(time[key] != null)
                    {
                        console.log('data key', key);
                        console.log('data key in time', time[key]);
                        var hours = time[key].getHours() < 10 ? "0" + time[key].getHours() : time[key].getHours();
                        var minutes = time[key].getMinutes() < 10 ? '0' + time[key].getMinutes() : time[key].getMinutes();
                        console.log('hours minutes in time', hours, minutes);
                        saveObj[key] = (isNaN(hours) ? "0" : hours) + ":" + (isNaN(minutes) ? '0' : minutes);
                    }
                   
                };

            saveObj.StatusCode = mdl.StatusCode;
               

            console.log('saveObj11' + JSON.stringify(saveObj));

            if (mode == 'create') {
                if (saveObj.StatusCode == null || saveObj.StatusCode == undefined){
                    saveObj.StatusCode = 1;
                }
                StallMst.create(saveObj).then(function(res) {
                    if (res.data == 666){
                        bsAlert.alert({
                            title: "Nama Stall Sudah Ada",
                            text: "",
                            type: "warning",
                            showCancelButton: false
                        }, function(){
                        
                        });
                        return false
                    } else {
                        bsAlert.alert({
                            title: "Data Tersimpan",
                            text: "Data berhasil disimpan",
                            type: "success",
                            showCancelButton: false
                        }, function(){
                        
                        });
                        $scope.getData();
                        $scope.formApi.setMode('grid');
                        console.log('uhuy')
                        return true;
                    }
                    
                });
                
            }

        }

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.popup1 = {
            opened: false
        };

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.formApi = {};

        $scope.doCustomSave = function(mdl, mode) {
            console.log("inside before save", mdl, mode);
            console.log("mdl : " + JSON.stringify(mdl));
            console.log("mode : " + mode);

            console.log("anita 5", (mdl["DefaultOpenTime"]));
            console.log("anita 5", new Date(mdl["DefaultOpenTime"]));


            var saveObj = angular.copy(mdl);
            saveObj["DefaultOpenTime"] = mdl["DefaultOpenTime"];
            saveObj["DefaultCloseTime"] = mdl["DefaultCloseTime"];
            
            console.log("StartBreakTime2 : " + saveObj["StartBreakTime2"]);
            console.log("EndBreakTime2 : " + saveObj["EndBreakTime2"]);
            
            console.log("ANITA2 TES", saveObj);

            delete saveObj["AvailableTimePrev"];
            delete saveObj["OpenTimePrev"];
            delete saveObj["CloseTimePrev"];

            //if($scope.schedule_mode == false) {
                var StartBreakTime2 = null;
                var StartBreakTime3 = null;
                var EndBreakTime2 = null;
                var EndBreakTime3 = null;
            
                if(saveObj["StartBreakTime2"] != null || saveObj["EndBreakTime2"] != null)
                {
                    StartBreakTime2 = new Date(saveObj["StartBreakTime2"]);
                    EndBreakTime2 = new Date(saveObj["EndBreakTime2"]);
                }

                if(saveObj["StartBreakTime3"] != null || saveObj["EndBreakTime3"] != null)
                {
                    StartBreakTime3 = new Date(saveObj["StartBreakTime3"]);
                    EndBreakTime3 = new Date(saveObj["EndBreakTime3"]);
                }

                var time = {
                    // "AvailableTime":new Date(saveObj["AvailableTime"]),
                    // "OpenTime":new Date(saveObj["OpenTime"]),
                    // "CloseTime":new Date(saveObj["CloseTime"]),
                    "DefaultOpenTime": new Date(saveObj["DefaultOpenTime"]),
                    "DefaultCloseTime": new Date(saveObj["DefaultCloseTime"]),
                    "StartBreakTime1": new Date(saveObj["StartBreakTime1"]),
                    "EndBreakTime1": new Date(saveObj["EndBreakTime1"]),
                    "StartBreakTime2": StartBreakTime2,
                    "EndBreakTime2": EndBreakTime2,
                    "StartBreakTime3": StartBreakTime3,
                    "EndBreakTime3": EndBreakTime3
                };
                console.log('data time', time);

                for (var key in time) {
                    if(time[key] != null)
                    {
                        console.log('data key', key);
                        console.log('data key in time', time[key]);
                        var hours = time[key].getHours() < 10 ? "0" + time[key].getHours() : time[key].getHours();
                        var minutes = time[key].getMinutes() < 10 ? '0' + time[key].getMinutes() : time[key].getMinutes();
                        console.log('hours minutes in time', hours, minutes);
                        saveObj[key] = (isNaN(hours) ? "0" : hours) + ":" + (isNaN(minutes) ? '0' : minutes);
                    }
                   
                };
            //}

            saveObj.StatusCode = mdl.StatusCode;
                // if (saveObj.StatusCode == 1) {
                //     saveObj.StatusCode = 1;
                // } else {
                //     saveObj.StatusCode = 0;
                // };

            console.log('saveObj11' + JSON.stringify(saveObj));
            if (mode == 'create') {
                
                // ======================== ini di pindah ke SaveCreateMode ========================
                // StallMst.create(saveObj).then(function(res) {
                //     if (res.data == 666){
                //         bsAlert.alert({
                //             title: "Data Sudah Ada",
                //             text: "",
                //             type: "warning",
                //             showCancelButton: false
                //         }, function(){
                        
                //         });
                //     } else {
                //         // bsAlert.alert({
                //         //     title: "Data Tersimpan",
                //         //     text: "Data berhasil disimpan",
                //         //     type: "success",
                //         //     showCancelButton: false
                //         // }, function(){
                        
                //         // });
                //         // $scope.getData();
                //         console.log('uhuy')
                //     }
                    
                // });
                // ======================== ini di pindah ke SaveCreateMode ========================

                
            } else {
 
                // if ($scope.schedule_mode == true) {
                //     console.log("$scope.gridJadwal.data.length", $scope.gridJadwal.data.length);
                //     // if ($scope.gridJadwal.data.length > 0) {
                //         console.log("$sini1 saveObj", saveObj);
                //         console.log("$sini1 mdl", mdl);
                //         bsAlert.alert({
                //             title: "Update Berhasil",
                //             text: "Data berhasil di update",
                //             type: "success",
                //             showCancelButton: false
                //         }, function(){
                //         $scope.saveJadwalStall(mdl);
                //         // $scope.schedule_mode == false;
                //         // $scope.getData();
                //         StallMst.update(saveObj).then(function(res) {
                //             console.log('res', res);
                //             // $scope.schedule_mode = false;
                //             $scope.getData();
                //             $scope.formApi.setMode('grid');
                //         });
                //         return;
                //     });
                //     // } else {
                //     //     // console.log("$sini2");
                //     //     // bsNotify.show({
                //     //     //     size: 'big',
                //     //     //     type: 'danger',
                //     //     //     title: "Mohon Input Data Terlebih Dahulu",
                //     //     // });
                //     // };
                // }else{
                //     StallMst.update(saveObj).then(function(res) {
                //         console.log('res', res);
                //         bsAlert.alert({
                //             title: "Update Berhasil",
                //             text: "Data berhasil di update",
                //             type: "success",
                //             showCancelButton: false
                //         }, function(){
                        
                //         });
                //         // $scope.schedule_mode = false;
                //         $scope.getData();
                //         $scope.formApi.setMode('grid');
                //     });
                //     return;
                // }
            }
            // };
        };

        $scope.doCustomSaveCloneForUpdate = function(mdlx, modex) {
            var mdl = angular.copy(mdlx)
            var mode = angular.copy(modex)
            console.log("inside before save", mdl, mode);

            var saveObj = angular.copy(mdl);
            saveObj["DefaultOpenTime"] = mdl["DefaultOpenTime"];
            saveObj["DefaultCloseTime"] = mdl["DefaultCloseTime"];
            
            console.log("ANITA2 TES", saveObj);

            delete saveObj["AvailableTimePrev"];
            delete saveObj["OpenTimePrev"];
            delete saveObj["CloseTimePrev"];

            //if($scope.schedule_mode == false) {
                var StartBreakTime2 = null;
                var StartBreakTime3 = null;
                var EndBreakTime2 = null;
                var EndBreakTime3 = null;
            
                if(saveObj["StartBreakTime2"] != null || saveObj["EndBreakTime2"] != null)
                {
                    StartBreakTime2 = new Date(saveObj["StartBreakTime2"]);
                    EndBreakTime2 = new Date(saveObj["EndBreakTime2"]);
                }

                if(saveObj["StartBreakTime3"] != null || saveObj["EndBreakTime3"] != null)
                {
                    StartBreakTime3 = new Date(saveObj["StartBreakTime3"]);
                    EndBreakTime3 = new Date(saveObj["EndBreakTime3"]);
                }

                var time = {
                    // "AvailableTime":new Date(saveObj["AvailableTime"]),
                    // "OpenTime":new Date(saveObj["OpenTime"]),
                    // "CloseTime":new Date(saveObj["CloseTime"]),
                    "DefaultOpenTime": new Date(saveObj["DefaultOpenTime"]),
                    "DefaultCloseTime": new Date(saveObj["DefaultCloseTime"]),
                    "StartBreakTime1": new Date(saveObj["StartBreakTime1"]),
                    "EndBreakTime1": new Date(saveObj["EndBreakTime1"]),
                    "StartBreakTime2": StartBreakTime2,
                    "EndBreakTime2": EndBreakTime2,
                    "StartBreakTime3": StartBreakTime3,
                    "EndBreakTime3": EndBreakTime3
                };
                console.log('data time', time);

                for (var key in time) {
                    if(time[key] != null)
                    {
                        console.log('data key', key);
                        console.log('data key in time', time[key]);
                        var hours = time[key].getHours() < 10 ? "0" + time[key].getHours() : time[key].getHours();
                        var minutes = time[key].getMinutes() < 10 ? '0' + time[key].getMinutes() : time[key].getMinutes();
                        console.log('hours minutes in time', hours, minutes);
                        saveObj[key] = (isNaN(hours) ? "0" : hours) + ":" + (isNaN(minutes) ? '0' : minutes);
                    }
                   
                };
            //}

            saveObj.StatusCode = mdl.StatusCode;

            console.log('saveObj11' + JSON.stringify(saveObj));
            if (mode == 'create') {
                
            } else {
 
                if ($scope.schedule_mode == true) {
                    console.log("$scope.gridJadwal.data.length", $scope.gridJadwal.data.length);
                    // if ($scope.gridJadwal.data.length > 0) {
                        console.log("$sini1 saveObj", saveObj);
                        console.log("$sini1 mdl", mdl);
                        
                        // $scope.schedule_mode == false;
                        // $scope.getData();
                        StallMst.update(saveObj).then(function(res) {
                            if (res.data == 666){
                                bsAlert.alert({
                                    title: "Nama Stall Sudah Ada",
                                    text: "",
                                    type: "warning",
                                    showCancelButton: false
                                }, function(){
                                
                                });
                                return false
                            } else if (res.data == 777) {
                                bsAlert.alert({
                                    title: "Stall tidak dapat di non aktifkan, masih terdapat chip di stall tersebut.",
                                    text: "Silahkan diselesaikan terlebih dahulu.",
                                    type: "warning",
                                    showCancelButton: false
                                }, function(){
                                
                                });
                                return false
                            } else {
                                $scope.saveJadwalStall(mdl);
                                console.log('res', res);
                                bsAlert.alert({
                                    title: "Update Berhasil",
                                    text: "Data berhasil di update",
                                    type: "success",
                                    showCancelButton: false
                                }, function(){

                                })
                                // $scope.schedule_mode = false;
                                $scope.getData();
                                $scope.formApi.setMode('grid');
                            }
                            
                        });
                        return;
                    
                    // } else {
                    //     // console.log("$sini2");
                    //     // bsNotify.show({
                    //     //     size: 'big',
                    //     //     type: 'danger',
                    //     //     title: "Mohon Input Data Terlebih Dahulu",
                    //     // });
                    // };
                }else{
                    StallMst.update(saveObj).then(function(res) {
                        console.log('res', res);
                        if (res.data == 666){
                            bsAlert.alert({
                                title: "Nama Stall Sudah Ada",
                                text: "",
                                type: "warning",
                                showCancelButton: false
                            }, function(){
                            
                            });
                            return false
                        } else if (res.data == 777) {
                            bsAlert.alert({
                                title: "Stall tidak dapat di non aktifkan, masih terdapat chip di stall tersebut.",
                                text: "Silahkan diselesaikan terlebih dahulu.",
                                type: "warning",
                                showCancelButton: false
                            }, function(){
                            
                            });
                            return false
                        } else {
                            bsAlert.alert({
                                title: "Update Berhasil",
                                text: "Data berhasil di update",
                                type: "success",
                                showCancelButton: false
                            }, function(){
                            
                            });
                            // $scope.schedule_mode = false;
                            $scope.getData();
                            $scope.formApi.setMode('grid');
                        }
                        
                    });
                    return;
                }
            }
            // };
        };

        $scope.onBeforeCancel = function(mdl, mode) {
            $scope.schedule_mode = false;
        };

        $scope.saveJadwalStall = function(mdl) {
            var tempInsertData = [];

            for (var i in $scope.gridJadwal.data) {

                if ($scope.gridJadwal.data[i].StallInactiveId.toString().indexOf("-") > 0) {
                    $scope.gridJadwal.data[i].StallInactiveId = 0;
                    tempInsertData.push($scope.gridJadwal.data[i]);
                };
                // else
                //     $scope.gridJadwal.data.splice(i,1);
            };

            // mdl.MStall_Inactive_TM = $scope.gridJadwal.data;
            mdl.MStall_Inactive_TM = tempInsertData;
            mdl.MStall_Inactive_TM_Delete = $scope.deletedJadwalDetail;
            console.log('saveJadwalStall', mdl);

            StallMst.saveJadwalStall(mdl).then(function(res) {
                $scope.deletedJadwalDetail = [];
            });
        };

        $scope.guid = function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        $scope.addJadwal = function(mData) {

            // mData["FromDate"] = mData["FromDate"].split("/");
            // mData["FromTime"] = mData["FromTime"].split(":");
            // mData["ToDate"] = mData["ToDate"].split("/");
            // mData["ToTime"] = mData["ToTime"].split(":");

            // mData["FromDateCopy"] = mData["FromDate"];
            // mData["ToDateCopy"] = mData["ToDate"];
            // mData["FromTimeCopy"] = mData["FromTime"];
            // mData["ToTimeCopy"] = mData["ToTime"];

            // mData["FromDate"] = new Date(mData["FromDate"]);
            // mData["ToDate"] = new Date(mData["ToDate"]);
            // mData["FromTime"] = new Date(mData["FromTime"]);
            // mData["ToTime"] = new Date(mData["ToTime"]);

            // // mData["FromDateCopy"] = mData["FromDate"].getFullYear() + "-" + ("0" + (mData["FromDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["FromDate"].getDate()).slice(-2);
            // // mData["ToDateCopy"] = mData["FromTime"].getHours() + ":" + ("0" + (mData["FromTime"].getMonth() + 1)).slice(-2);
            // // mData["FromTimeCopy"] = mData["ToDate"].getFullYear() + "-" + ("0" + (mData["ToDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["ToDate"].getDate()).slice(-2);
            // // mData["ToTimeCopy"] = mData["ToTime"].getHours() + ":" + ("0" + (mData["ToTime"].getMonth() + 1)).slice(-2);
            // mData["FromDateCopy"] = ("0" + mData["FromDate"].getDate()).slice(-2) + "-" + ("0" + (mData["FromDate"].getMonth() + 1)).slice(-2)+"-"+ mData["FromDate"].getFullYear() ;
            // mData["FromTimeCopy"] = ("0"+mData["FromTime"].getHours()).slice(-2) + ":" + ("0" + (mData["FromTime"].getMonth() + 1)).slice(-2);
            // mData["ToDateCopy"] = ("0" + mData["ToDate"].getDate()).slice(-2) + "-" + ("0" + (mData["ToDate"].getMonth() + 1)).slice(-2) + "-" + mData["ToDate"].getFullYear();
            // mData["ToTimeCopy"] = ("0"+ mData["ToTime"].getHours()).slice(-2) + ":" + ("0" + (mData["ToTime"].getMonth() + 1)).slice(-2);


            // mData.InactiveStart = mData["FromDate"].getFullYear() + "-" + ("0" + (mData["FromDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["FromDate"].getDate()).slice(-2) + "" + " " + mData["FromTime"].getHours() + ":" + ("0" + (mData["FromTime"].getMonth() + 1)).slice(-2) + ":00.000";
            // mData.InactiveEnd = mData["ToDate"].getFullYear() + "-" + ("0" + (mData["ToDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["ToDate"].getDate()).slice(-2) + " " + mData["ToTime"].getHours() + ":" + ("0" + (mData["ToTime"].getMonth() + 1)).slice(-2) + ":00.000";
            

            mData.FromDateCopy = $filter('date')(mData.FromDate, 'yyyy-MM-dd');
            mData.FromTimeCopy = $filter('date')(mData.FromTime, 'HH:mm');
            mData.ToDateCopy = $filter('date')(mData.ToDate, 'yyyy-MM-dd');
            mData.ToTimeCopy = $filter('date')(mData.ToTime, 'HH:mm');

            mData.InactiveStart = mData.FromDateCopy + ' ' + mData.FromTimeCopy
            mData.InactiveEnd = mData.ToDateCopy + ' ' + mData.ToTimeCopy

            mData.DefaultOpenTimeCopy = $filter('date')(mData.DefaultOpenTime, 'HH:mm')
            mData.DefaultCloseTimeCopy = $filter('date')(mData.DefaultCloseTime, 'HH:mm')

            mData.StartBreakTime1Copy = $filter('date')(mData.StartBreakTime1, 'HH:mm')
            mData.EndBreakTime1Copy = $filter('date')(mData.EndBreakTime1, 'HH:mm')
            mData.StartBreakTime2Copy = $filter('date')(mData.StartBreakTime2, 'HH:mm')
            mData.EndBreakTime2Copy = $filter('date')(mData.EndBreakTime2, 'HH:mm')
            mData.StartBreakTime3Copy = $filter('date')(mData.StartBreakTime3, 'HH:mm')
            mData.EndBreakTime3Copy = $filter('date')(mData.EndBreakTime3, 'HH:mm')

            $scope.gridJadwal.data.push({
                StallInactiveId: $scope.guid(),
                StallId: mData.StallId,
                FromDate: mData["FromDateCopy"],
                ToDate: mData["ToDateCopy"],
                FromTime: mData["FromTimeCopy"],
                ToTime: mData["ToTimeCopy"],
                InactiveStart: mData.InactiveStart,
                InactiveEnd: mData.InactiveEnd,
                InactiveReason: mData.InactiveReason,
                JamIstirahat1: mData.StartBreakTime1Copy +' - '+ mData.EndBreakTime1Copy,
                JamIstirahat2: mData.StartBreakTime2Copy +' - '+ mData.EndBreakTime2Copy,
                JamIstirahat3: mData.StartBreakTime3Copy +' - '+ mData.EndBreakTime3Copy,
                DefaultOpenTime: mData.DefaultOpenTimeCopy,
                DefaultCloseTime: mData.DefaultCloseTimeCopy
            });

            $scope.mStallMst["FromDate"] = null;
            $scope.mStallMst["FromTime"] = null;
            $scope.mStallMst["ToDate"] = null;
            $scope.mStallMst["ToTime"] = null;
            $scope.mStallMst["InactiveReason"] = null;
        }

        $scope.deleteStallConfirm = function(row) {
            console.log(row);
            $scope.tempStallIdDelete = row.StallId; // karena bentuk nya array
            $scope.tempStallNameDelete = row.StallName;
            // $scope.showModal_deleteStall = true;
            ngDialog.openConfirm({
                template: '\
                  <p>Are you sure you want to delete stall : {{tempStallNameDelete}}?</p>\
                  <div class="ngdialog-buttons">\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteStall();closeThisDialog(0)">Yes</button>\
                  </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope: $scope
            });
        }

        $scope.deleteStall = function(row) {
            StallMst.delete($scope.tempStallIdDelete).then(function(res) {
                delete $scope.tempStallIdDelete;
            })

            $scope.showModal_deleteStall = false;
        }

        $scope.deleteJadwalDetail = function(row) {
            console.log(row);
            bsAlert.alert({
                title: "Apakah Anda yakin ingin menghapus ",
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            },function () {

                var rowindex = $scope.gridJadwal.data.findIndex(function(obj) {
                    return obj.StallInactiveId == row.StallInactiveId;
                });

                $scope.gridJadwal.data.splice(rowindex, 1);

                if (row.StallInactiveId.toString().indexOf("-") == -1)
                    $scope.deletedJadwalDetail.push(row);
            });
        }



        var actionTemp = '<div>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.$parent.schedule_mode=false;grid.appScope.$parent.onEdit();grid.appScope.gridClickEditHandler(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-pencil"></i>' +
            '</button>' +
            // '<button class="ui icon inverted grey button"'+
            //         'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
            //         'onclick="this.blur()"'+
            //         'ng-click="grid.appScope.$parent.deleteStallConfirm(row.entity);">'+
            //     '<i class="fa fa-fw fa-lg fa-eraser"></i>'+
            // '</button>'+
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.$parent.schedule_mode=false;grid.appScope.actView(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-list-alt"></i>' +
            '</button>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.$parent.schedule_mode=true;grid.appScope.$parent.ViewJadwalStall(row.entity);grid.appScope.gridClickEditHandler(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-table"></i>' +
            '</button>' +
            '</div>';

        var actionTemp1 = '<div>' +
            
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.$parent.schedule_mode=true;grid.appScope.$parent.deleteJadwalDetail(row.entity);grid.appScope.gridClickEditHandler(row.entity);">' +
            '<i class="fa fa-fw fa-lg fa-trash"></i>' +
            '</button>' +
            '</div>';  

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.viewData = function(row){
            $scope.formApi.setMode('detail');
            $scope.formApi.setFormReadOnly('true');
        }
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [20, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 20,
            columnDefs: [
                { name: 'Id', field: 'id', width: '7%', visible: false },
                { name: 'Jenis Stall', field: 'TypeName', width:150 },
                { name: 'Nama Stall', field: 'Name', width:120 },
                { name: 'Teknisi 1', field: 'Technician1Name', width:180 },
                { name: 'Teknisi 2', field: 'Technician2Name', width:180 },
                { name: 'Teknisi 3', field: 'Technician3Name', width:180 },
                // { name: 'Jam Tersedia', width:100, cellTemplate: '<div style="margin-left:5px">{{ row.entity.AvailableTime }} Jam</div>'},
                { name: 'Jam Tersedia', field: 'JamTersedia', width:100,},
                { name: 'Jam Istirahat 1', field: 'JamIstirahat1', width:120 },
                { name: 'Jam Istirahat 2', field: 'JamIstirahat2', width:120},
                { name: 'Jam Istirahat 3', field: 'JamIstirahat3' , width:120},
                { name: 'Jam Buka Default', field: 'DefaultOpenTime', width:140 },
                { name: 'Jam Tutup Default', field: 'DefaultCloseTime', width:140 },
                { name: 'Status', field: 'StatusCode', width:100 },
                // { name:'Nyobain', cellTemplate:"<span>{{grid.appScope.$parent.schedule_mode}}</span>"},
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: 200,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: actionTemp
                }
            ]
        };

        $scope.gridJadwal = {
            onRegisterApi: function (gridApi) {
                $scope.gridnyaAPI = gridApi;
            },
            enableFiltering: true,
            // enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'id', width: '7%', visible: false },
                { name: 'Jam Dari', field: 'FromTime'},
                { name: 'Tanggal Dari', field: 'FromDate' },
                { name: 'Jam Sampai', field: 'ToTime' },
                { name: 'Tanggal Sampai', field: 'ToDate' },
                { name: 'Catatan', field: 'InactiveReason' },
                { name: 'Jam Istirahat 1', field: 'JamIstirahat1' },
                { name: 'Jam Istirahat 2', field: 'JamIstirahat2' },
                { name: 'Jam Istirahat 3', field: 'JamIstirahat3' },
                { name: 'Jam Buka Default', field: 'DefaultOpenTime' },
                { name: 'Jam Tutup Default', field: 'DefaultCloseTime' },
                
                // { name: 'action', allowCellFocus: false, cellTemplate: "<a ng-click='grid.appScope.deleteJadwalDetail(row.entity)'><a>" }
                { 
                    name: 'action', 
                    allowCellFocus: false,
                    width: 200,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: actionTemp1  }
            ]
        };

        $scope.gridJadwalTableHeight = function() {
            var headerHeight = 30; // your header height
            if ($scope.gridJadwal.data != undefined) {
                if ($scope.gridJadwal.data.length == 0) {
                    return {
                        height: 250 + "px"
                    };
                } else if ($scope.gridJadwal.data.length < 10) {
                    return {
                        height: ($scope.gridJadwal.data.length * headerHeight) + 160 + "px"
                    };
                } else {
                    return {
                        height: 460 + "px"
                    }
                }

            }
        };

        $scope.actionButtonSettingsDetail = [{
                func: function(row, formScope) {
                    console.log('Simpan >> ', row, formScope);
                },
                title: 'Simpan',
            },
            {
                func: function(row, formScope) {
                    console.log('Kemlabi >> ', row, formScope);
                },
                title: 'Kembali',
            }
        ]

        $scope.FilterStallSchedule = function () {
            $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
            $scope.gridApiAppointment = {};
            $scope.filterColIdx = 1;
            var x = -1;
            console.log("filter",$scope.gridJadwal)
            for (var i = 0; i < $scope.gridJadwal.columnDefs.length; i++) {
                if ($scope.gridJadwal.columnDefs[i].visible == undefined && $scope.gridJadwal.columnDefs[i].name !== 'Action') {
                    x++;
                    $scope.gridCols.push($scope.gridJadwal.columnDefs[i]);
                    console.log("i")
                    $scope.gridCols[x].idx = i;
                }
                console.log("$scope.gridCols", $scope.gridCols);
                console.log("$scope.grid.columnDefs[i]", $scope.gridJadwal.columnDefs[i]);
            }
            $scope.filterBtnLabel = $scope.gridCols[0].name;

            $scope.filterBtnChange = function (col) {
                console.log("col", col);
                $scope.filterBtnLabel = col.name;
                $scope.filterColIdx = col.idx + 1;
            };
        }

        $scope.onSelectRows = function(rows){
            console.log("onSelectRows=>",rows);
            $scope.selectedRow_stall  = rows;
        }
        $scope.onBulkDelete = function() {
            console.log($scope.selectedRow_stall)

            var mdl = []
            for (var i=0; i<$scope.selectedRow_stall.length; i++) {
                mdl.push($scope.selectedRow_stall[i].StallId)
            }

            StallMst.delete(mdl).then(function(res) {
                if (res.data == 777) {
                    bsAlert.alert({
                        title: "Stall tidak dapat di non aktifkan, masih terdapat chip di stall tersebut.",
                        text: "Silahkan diselesaikan terlebih dahulu.",
                        type: "warning",
                        showCancelButton: false
                    }, function(){
                    
                    });
                    $scope.selectedRow_stall = []
                    $scope.getData()
                    return false
                } else {
                    $scope.selectedRow_stall = []
                    $scope.getData()
                }
                
            });
            // bsAlert.alert({
            //         title: "Apakah anda yakin akan menghapus data stall yang terpilih?",
            //         text: "",
            //         type: "question",
            //         showCancelButton: true
            //     },
            //     function() {
            //         console.log("yes nih");
                    
            //     },
            //     function() {
            //     }
            // );
        }


    });