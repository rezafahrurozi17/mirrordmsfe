angular.module('app')
    .factory('StallMst', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                // var defer = $q.defer();
                //     defer.resolve(
                //       {
                //         "data" : {
                //           "Result":[{
                //               "id":1,
                //               "jenisStall":"Stall 1",
                //               "namaStall":"name stall",
                //               "teknisi":"Daniel",
                //               "jamTersedia":"21",
                //               "jamBuka":"09:00",
                //               "jamTutup":"17:00"
                //           },{
                //               "id":2,
                //               "jenisStall":"Stall 2",
                //               "namaStall":"name stall",
                //               "teknisi":"Naufal",
                //               "jamTersedia":"21",
                //               "jamBuka":"09:00",
                //               "jamTutup":"17:00" 
                //           }
                //           ],
                //           "Start":1,
                //           "Limit":10,
                //           "Total":1
                //         }
                //       }
                //     );
                // return defer.promise;
                var res = $http.get('/api/as/Stall');
                console.log("Hasil Factory : ", res);
                return res;
            },
            create: function(data) {
                console.log("data==>", data);
                data.StallId = 0;
                //data.Type = 0;    //dicoment karena tidak selalu milih jenis stall general repair
                return $http.post('/api/as/Stall', [data]);
            },
            update: function(data) {
                console.log("data==>", data);
                // if (data.StatusCode === "Aktif") {
                //     data.StatusCode = 1;
                // } else {
                //     data.StatusCode = 0;
                // };
                return $http.put('/api/as/Stall/update', [data]);
            },
            delete: function(ids) {
                console.log("id==>", ids);
                return $http.delete('/api/as/Stall',{data:ids,headers: {'Content-Type': 'application/json'}});
            },
            getStallType: function() {
                return $http.get('/api/as/Stall/GetStallType');
            },
            getTechnician: function() {
                return $http.get('/api/as/Stall/GetStallTechnician');
            },
            getStallSchedule: function(StallId, FilterTeks, FilterColumn) {
                return $http.get('/api/as/Stall/getStallSchedule', {
                    params: {
                        StallId: StallId
                    }
                });
 
            },
            saveJadwalStall: function(data) {
                console.log("data saveJadwalStall==>", data);
                return $http.post('/api/as/Stall/PostStallSchedule', [data]);
            },

        }
    });