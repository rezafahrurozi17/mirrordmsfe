angular.module('app')
  .factory('VendorMst', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/Vendor');
        return res;
      },
      create: function(Vendor) {
        console.log("Data DP : ",Vendor);
        Vendor.VendorId = 0;
        return $http.post('/api/as/Vendor', [Vendor]);
      },
      update: function(Vendor){
        console.log("Data DP : ",Vendor);
        return $http.put('/api/as/Vendor', [Vendor]);
      },
      delete: function(id) {
        console.log("Data DP : ",id);
        return $http.delete('/api/as/Vendor',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //