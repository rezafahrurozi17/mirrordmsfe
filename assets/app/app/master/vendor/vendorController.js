angular.module('app')
    .controller('VendorMstController', function($scope, $http, CurrentUser, VendorMst,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVendorMst = null; //Model
    $scope.cVendorMst = null; //Collection
    $scope.xVendorMst = {};
    $scope.xVendorMst.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {        
        VendorMst.getData().then(
            function(res){
                console.log(res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };
  
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'DP Id',field:'DpId', width:'7%', visible:false },
            { name:'Tanggal Berlaku', field:'EffectiveDate' },
            { name:'Range From', field:'PriceFrom' },
            { name:'Range Thru', field:'PriceTo' },
            { name:'Percentase', field:'PercentDP' },
      
        ]
    };
});
