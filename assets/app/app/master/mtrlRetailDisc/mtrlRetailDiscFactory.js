angular.module('app')
    .factory('MaterialRetailDiscount', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/MaterialRetailDiscount');
                return res;
            },
            create: function(data) {
                if (data.length > 0)
                // data.PartPriceDiscountPerMaterialId = 0;
                return $http.post('/api/as/MaterialRetailDiscount', data);
                else
                return $http.post('/api/as/MaterialRetailDiscount', [data]);
            },
            upload: function(data) {
                //return $http.post('/api/as/MaterialRetailDiscount/MaterialRetailDiscountUpload', data);
                if (data.length > 0)
                // data.PartPriceDiscountPerMaterialId = 0;
                return $http.post('/api/as/MaterialRetailDiscount/MaterialRetailDiscountUpload', data);
                else
                return $http.post('/api/as/MaterialRetailDiscount/MaterialRetailDiscountUpload', [data]);
            },
            update: function(data) {
                return $http.put('/api/as/MaterialRetailDiscount', [data]);
            },
            delete: function(id) {
                return $http.delete('/api/as/MaterialRetailDiscount', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            searchMaterial: function(PartsClassId, kode) {
                return $http.get('/api/as/MaterialRetailDiscount/GetPartsMaterial', {
                    params: {
                        PartsClassId: PartsClassId,
                        PartsCode: kode
                    }
                })
            }
        }
    });
//ddd