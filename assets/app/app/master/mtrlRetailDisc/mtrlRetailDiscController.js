angular.module('app')
    .controller('MtrlRetailDiscController', function($scope, $http, CurrentUser, MaterialRetailDiscount, Parts, Material, PartsCategory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];

            $scope.mMtrlRetailDisc = {};
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMtrlRetailDisc = null; //Model
        $scope.cMtrlRetailDisc = null; //Collection
        $scope.xMtrlRetailDisc = {};
        $scope.xMtrlRetailDisc.selected = [];

        $scope.startMinOption = null;
        $scope.startMaxOption = null;
        $scope.endMinOption = null;
        $scope.endMaxOption = null;
        $scope.formApi = {};

        var dateFormat = 'dd/MM/yyyy';
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1 
        };

        $scope.DateFilterOptions1 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.startDateChange = function(selected) {
            console.log(selected);
            $scope.DateFilterOptions2.minDate = selected.dt;
        }
        var mDataCopy;
        var mode = 0;
        var editMode = 1;
        var viewMode = 0;
        var uploadMode = false;
        var processingMode = 2;
        var editableMode = false;

        $scope.materialData = [];
        $scope.partsData = [];
        $scope.typeSetData = [];

        function changeMode(mode) {
            $timeout(function() {
                editableMode = mode == editMode;

                $scope.customActionSettings[0].visible = mode == editMode;
                $scope.customActionSettings[1].visible = mode == editMode;

                $scope.customActionSettings[0].enable = mode != processingMode;
                $scope.customActionSettings[1].enable = mode != processingMode;

                $scope.customActionSettings[2].visible = mode == viewMode;
                $scope.customActionSettings[3].visible = mode == viewMode;

                $scope.grid.enableCellEditOnFocus = mode = editMode;

            }, 0);
        };

        //Ini buatan gua
        $scope.customActionSettings = [{
                func: function(string) {
                    changeMode(viewMode);
                    $scope.grid.data = angular.copy(mDataCopy);
                },
                title: 'Batal',
                visible: false,
                color: '#ddd'
            },
            {
                title: 'Download',
                icon: 'fas fa-file-upload',
                func: function(row, formScope) {
                    var excelData = [];
                    var fileName = "Template Diskon Penjualan Material";
                    var excelSheet2 = { "Discount": "", "Effectivedate": "", "PartCode": "", "Validthru": "" };


                    excelData.push(excelSheet2);
                    XLSXInterface.writeToXLSX(excelData, fileName);
                },
                type: 'custom' //for link
            },
            {
                title: 'Upload',
                icon: 'fa-file-upload',
                func: function(row, formScope) {
                    angular.element(document.querySelector('#UploadDiscount')).val(null);
                    angular.element('#ModalUploadDiscount').modal('show');
                }
            }

        ];
        $scope.loadXLSDiscount = function(ExcelFile) {

            var myEl = angular.element(document.querySelector('#UploadDiscount')); //ambil elemen dari dokumen yang di-upload
            XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
                $scope.ExcelData = json;
                console.log("ExcelData : ", $scope.ExcelData);
                console.log("json", json);
            });
        }

        $scope.UploadDiscountClicked = function() {
            var eachData = {};
            var inputData = [];

            angular.forEach($scope.ExcelData, function(value, key) {
                console.log('excel Data', $scope.ExcelData);
                console.log('value', value);
                console.log('key', key);
                eachData = {
                    "Discount": value.Discount,
                    "Effectivedate": value.Effectivedate,                    
                    "PartCode": value.PartCode,
                    "Validthru": value.Validthru

                }
                inputData.push(eachData);
            });
            console.log('cek data upload', eachData);
            MaterialRetailDiscount.upload(inputData).then(function(res) {
                $scope.getData();
                angular.element('#ModalUploadDiscount').modal('hide');
                angular.element(document.querySelector('#UploadDiscount')).val(null);
                bsNotify.show({
                    size: 'small',
                    type: 'success',
                    title: "Success",
                    content: "Data Berhasil Di Upload"
                });
            });
        }

        // $scope.customActionSettings = [
        //     {   
        //         func: function(string) {
        //             changeMode(viewMode);
        //             $scope.grid.data = angular.copy(mDataCopy);
        //         },
        //         title: 'Batal',
        //         visible:false,
        //         color: '#ddd'
        //     },
        //     // {
        //     //     func: function(string) {
        //     //         changeMode(processingMode);
        //     //         var saveArr = [];
        //     //         for (var i in $scope.grid.data) {
        //     //             // console.log($scope.grid.data[i]);
        //     //             saveArr.push({
        //     //                 PartPriceDiscountPerMaterialId:0,
        //     //                 EffectiveDate:$scope.grid.data[i].EffectiveDate,
        //     //                 ValidThru:$scope.grid.data[i].ValidThru,
        //     //                 PartCode:$scope.grid.data[i].PartsCode,
        //     //                 Discount:$scope.grid.data[i].Discount,
        //     //                 OutletId:$scope.user.OutletId,
        //     //                 MaterialTypeData:$scope.grid.data[i].MaterialTypeData
        //     //             });
        //     //         }
        //     //         // console.log("saveArr : ",saveArr);
        //     //         // console.log("$scope.user : ",$scope.user);
        //     //         MaterialRetailDiscount.upload(saveArr).then(function(res){
        //     //             $scope.getData();
        //     //             changeMode(viewMode);
        //     //             mode = viewMode;
        //     //         });
        //     //         console.log("simpan");                
        //     //     },
        //     //     title: 'Simpan',
        //     //     visible:false
        //     // },
        //     {
        //         // func: function(string) {
        //         //     var generatedData = [];
        //         //     var x = new Date();
        //         //     var xdate = (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
        //         //     angular.forEach($scope.materialData, function(mtrl, mtrlIdx){
        //         //         generatedData.push({EffectiveDate:xdate, ValidThru:xdate, PartsCode:mtrl.PartsCode, Discount:0});
        //         //     });
        //         //     angular.forEach($scope.partsData, function(part, partIdx){
        //         //         generatedData.push({EffectiveDate:xdate, ValidThru:xdate, PartsCode:part.PartsCode, Discount:0});
        //         //     });                
        //         //     XLSXInterface.writeToXLSX(generatedData, 'Template Diskon Penjualan Material');
        //         // },
        //         // title: 'Download Template',
        //         // // rightsBit: 1,
        //         // visible:true
        //         title: 'Download',
        //         icon: 'fas fa-file-upload',
        //         func: function(row, formScope) {
        //         var excelData = [];
        //         var fileName = "Template Diskon Penjualan Material";
        //         var excelSheet2 = {"EffectiveDate":"", "ValidThru":"", "PartsCode":"", "Discount":"","PartsId":"","PartsName":"","OutletId":""};
        //         var excelSheet = {}
        //         var excelData = [];
        //         //excelData.push(excelSheet1);
        //         excelData.push(excelSheet2);
        //         //excelData.push(excelSheet3);
        //         //console.log("excel data", excelData);

        //         //var excelSheetName = ["0", "1", "2"];

        //         XLSXInterface.writeToXLSX(excelData, fileName);
        //         //$scope.formApi.setMode('detail');
        //         },

        //         type: 'custom' //for link
        //     },{
        //         title: 'Upload',
        //       icon: 'fa-file-upload',
        //       func: function (row, formScope) {
        //         angular.element( document.querySelector( '#UploadDiscount' ) ).val(null);
        //     		angular.element('#ModalUploadDiscount').modal('show');
        //       }
        //     }
        // ];

        // // function validateXLS(data){
        // //     var result = [];
        // //     var error = [];
        // //     console.log("data excel : ",data);

        // //     $scope.grid.data = [];
        // //     $timeout(function(){
        // //         for(var i=0; i<data.length; i++){     
        // //             if (typeof data[i].PartsCode !== 'undefined') {
        // //                 var dMaterial = _.find($scope.materialData, { 'PartsCode': data[i].PartsCode });
        // //                 if(dMaterial == undefined){
        // //                     var dParts = _.find($scope.partsData, { 'PartsCode': data[i].PartsCode });
        // //                     if(dParts == undefined){
        // //                         error.push('Bahan / Part '+data[i].PartsCode+' tidak ditemukan.');
        // //                         continue;
        // //                     }
        // //                 }
        // //                 var newObj = {
        // //                     EffectiveDate:new Date(data[i].EffectiveDate), 
        // //                     ValidThru:new Date(data[i].ValidThru),
        // //                     PartId:(typeof dMaterial !== 'undefined' ? dMaterial.PartsId : dParts.PartsId),
        // //                     PartsCode:(typeof dMaterial !== 'undefined' ? dMaterial.PartsCode : dParts.PartsCode),
        // //                     PartsName:(typeof dMaterial !== 'undefined' ? dMaterial.PartsName : dParts.PartsName),
        // //                     Discount:data[i].Discount,
        // //                 };

        // //                 result.push(newObj);
        // //             }                
        // //         }            
        // //     });

        // //     if(error.length>0){
        // //         bsNotify.show(
        // //             {
        // //                 size: 'big',
        // //                 type: 'danger',
        // //                 title: "Ditemukan beberapa data yang salah",
        // //                 content: error.join('<br>'),
        // //                 number: error.length
        // //             }
        // //         );            
        // //     }
        // //     return result;
        // // }
        // // $scope.loadXLS=function(o){
        // //     var myEl = angular.element( document.querySelector( '#uploadMRD' ) );

        // //     var a = myEl[0].files[0].name.split(".");
        // //     var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        // //     if (fileExt === "xlsx") {
        // //         XLSXInterface.loadToJson(myEl[0].files[0], function(json){
        // //             $timeout(function(){
        // //                 myEl.value = '';
        // //                 document.getElementById('uploadMRD').value = '';
        // //                 // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        // //                 mDataCopy = angular.copy($scope.grid.data);
        // //                 $scope.grid.data = validateXLS(json);                    

        // //                 uploadMode = true;
        // //                 changeMode(editMode);
        // //             });
        // //         });
        // //     } else {
        // //         bsNotify.show(
        // //             {
        // //                 size: 'big',
        // //                 type: 'danger',
        // //                 title: "File yang diupload bukan format .xlsx",
        // //                 // content: error.join('<br>'),
        // //                 // number: error.length
        // //             }
        // //         );   
        // //     }
        // // };
        // $scope.loadXLSDiscount = function(ExcelFile){
        //     var myEl = angular.element( document.querySelector( '#UploadDiscount' ) ); //ambil elemen dari dokumen yang di-upload


        //     XLSXInterface.loadToJson(myEl[0].files[0], function(json){
        //         $scope.ExcelData = json;
        //         console.log("ExcelData : ", $scope.ExcelData);
        //         console.log("json", json);
        //             });
        //   }


        //   $scope.UploadDiscountClicked = function(){
        //     var eachData = {};
        //     var inputData = [];

        //         angular.forEach($scope.ExcelData, function(value, key){
        //       console.log('excel Data', $scope.ExcelData);
        //       console.log('value', value);
        //       console.log('key', key);
        //             eachData = {
        //             //"PartPriceDiscountPerMaterialId"        : 0,
        //             "EffectiveDate"                         : value.EffectiveDate,
        //             "ValidThru" 		                    : value.ValidThru,
        //             "PartCode"                              : value.PartCode,                
        //             "Discount"                              : value.Discount,
        //             "OutletId"                              : value.OutletId, 
        //             "PartsId"                               : value.PartsId,
        //             "PartsName"                             : value.PartsName             
        //             //"MaterialTypeData"                      : value.MaterialTypeData

        //         }

        //             inputData.push(eachData);
        //         });
        //     console.log('cek data upload',eachData);
        //     MaterialRetailDiscount.create(eachData).then(function (res) {
        //       angular.element('#ModalUploadDiscount').modal('hide');
        //       angular.element( document.querySelector( '#UploadDiscount' ) ).val(null);
        //       bsNotify.show({
        //         size: 'small',
        //         type: 'success',
        //         title: "Success",
        //         content: "Data Berhasil Di Upload"
        //       });
        //     })
        // //    $scope.goBack();
        //     }
        //     // $scope.goBack = function()
        //     // {
        //     //     $scope.formApi.setMode('grid');
        //     //     $scope.getData();
        //     // }
        // $scope.setType = function(selected){
        //     if (selected.PartsClassId==1) {
        //         $scope.typeSetData = angular.copy($scope.partsData);
        //     } else {
        //         $scope.typeSetData = angular.copy($scope.materialData);
        //     }
        // };

        $scope.onValidateSave = function(data) {
            $scope.EffectiveDate = data.EffectiveDate;
            $scope.ValidThru = data.ValidThru;
            data.EffectiveDate = data.EffectiveDate.getFullYear() + '-' + (data.EffectiveDate.getMonth() + 1) + '-' + data.EffectiveDate.getDate();
            data.ValidThru = data.ValidThru.getFullYear() + '-' + (data.ValidThru.getMonth() + 1) + '-' + data.ValidThru.getDate();
            console.log('data effDate COntroller', data.EffectiveDate);
            return true;
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            MaterialRetailDiscount.getData().then(
                function(res) {
                    console.log("res=>", res);
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.searchMaterial = function(partsClassId, partsCode) {
            if (!partsClassId)
                bsNotify.show({
                    title: "Search Material",
                    content: "pilih dahulu Parts Typenya",
                    type: 'danger'
                });
            else
                MaterialRetailDiscount.searchMaterial(partsClassId, partsCode).then(function(res) {
                    console.log("search part", res.data)
                    $scope.mMtrlRetailDisc.PartsName = res.data.Result[0].PartsName;
                    $scope.mMtrlRetailDisc.PartId = res.data.Result[0].PartId + "";
                });
        }

        PartsCategory.getData(1).then(
            function(res) {
                $scope.MaterialTypeData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        // unused function cuz no one
        // Parts.getData(1).then(
        //     function(res) {
        //         $scope.partsData = res.data.rs.Result;
        //         Material.getData(2).then(
        //             function(xres) {
        //                 $scope.materialData = xres.data.rs.Result;
        //                 console.log("data=>", xres.data);
        //                 $scope.loading = false;
        //                 return res.data;
        //             },
        //             function(err) {
        //                 console.log("err=>", err);
        //             }
        //         );
        //     },
        //     function(err) {
        //         console.log("err=>", err);
        //     }
        // );

        $scope.onSelectRows = function(rows) {

        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'PartPriceDiscountPerMaterialId', field: 'PartPriceDiscountPerMaterialId', width: '7%', visible: false },
                { name: 'Tanggal Berlaku', field: 'EffectiveDate', cellFilter: 'custDate' },
                { name: 'Berlaku Sampai', field: 'ValidThru', cellFilter: 'custDate' },
                { name: 'No. Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartsName', width: '30%' },
                { name: 'Diskon', field: 'Discount' },
                // { name:'Tanggal Berlaku', field:'EffectiveDate' },
            ]
        };

        $scope.validDateChange = function(dt) {
            console.log("change->", $scope.mMtrlRetailDisc, dt);
        };
    }).filter('custDate', function() {
        return function(x) {
            return (x.getDate() < 10 ? "0" + x.getDate() : x.getDate()) + "/" + ((x.getMonth() + 1) < 10 ? "0" + (x.getMonth() + 1) : (x.getMonth() + 1)) + "/" + x.getFullYear();
        };
    });