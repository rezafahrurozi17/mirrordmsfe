angular.module('app')
    .factory('Material', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(type, startPage, itemPerPage, ParamSearch) {
                var tmpFilter = angular.copy(ParamSearch)
                // var defer = $q.defer(); 
                //     defer.resolve(
                //       {
                //         "data" : {
                //           "Result":[{
                //               "PartsCode": 'P0001',
                //               "PartsName": "Part satu",
                //               "Active": true,
                //               "VendorName": "Vendor Name",
                //               "StockingPolicy": "Stok",
                //               "SCC": "Overhaul",
                //               "ICC": "A - Very Fast Moving",
                //               "Franchise": "C - Material",
                //               "Clasification": "Toyota Genuine Material",
                //               "Warehouse": "Gudang",
                //               "WarehouseShelf": "Rak",
                //               "RetailPrice": 12000,
                //               "StandardCost": 13000,
                //               "Satuan": "piece"
                //           }],
                //           "Start":1,
                //           "Limit":10,
                //           "Total":1
                //         }
                //       }
                //     );
                // return defer.promise;
                if (tmpFilter.PartsCode == null || typeof tmpFilter.PartsCode == "undefined" || tmpFilter.PartsCode == "") {
                    tmpFilter.PartsCode = "-";
                }
                if (tmpFilter.PartsName == null || typeof tmpFilter.PartsName == "undefined" || tmpFilter.PartsName == "") {
                    tmpFilter.PartsName = "-";
                }
                // var res = $http.get('/api/as/MstParts/GetMaterialPartsAll?type=' + type + '&startPage=' + startPage + '&indexPage=' + itemPerPage);
                var newres = $http.get('/api/as/MstParts/GetMaterialPartsAll?type=' + type + '&startPage=' + startPage + '&indexPage=' + itemPerPage + '&partsName=' + tmpFilter.PartsName + '&partsCode=' + tmpFilter.PartsCode);
                // console.log("Data newres : ", newres);
                return newres;
            },
            create: function(Material) {
                console.log("Data Material : ", Material);
                return $http.post('/api/as/MstParts', [Material]);
            },
            update: function(Material) {
                console.log("Data Material : ", Material);
                return $http.put('/api/as/MstParts', [Material]);
            },
            delete: function(id) {
                console.log("Data Material : ", id);
                return $http.delete('/api/as/MstParts', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            createUpload: function(uploadExcel) {
                var url = '/api/as/MstParts/UploadMaterialPart/';
                var param = JSON.stringify(uploadExcel);
                console.log("object input saveData", param);
                var res=$http.post(url, param);
                return res;
              },
            getVendor: function() {
                // return $http.get('/api/as/GetVendorByBussinessType/9')
                return $http.get('/api/as/GetVendorByBussinessMultiType/9/2')
            },
            checkNoMaterial: function(param){
                return $http.get('/api/as/MstParts/CheckNoMaterial', {
                    params: {
                        PartsCode: param,
                    }
                })
            }
        }
    });
//