angular.module('app')
    .controller('MaterialController', function($scope, $http, uiGridConstants,PartsGlobal, CurrentUser, Material, GeneralMaster, PartsCategory, WarehouseShelfFactory, VendorMst, $timeout,bsNotify,bsAlert,ngDialog) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ExcelData = [];
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            // $scope.gridData = [];

            $scope.list_PartsClassId5_disable = [40];
            // $scope.getData();
            //get material type and service type
            //login 41 -> RoleId: 1135 -> GR
            //login 25 -> RoleId: 1122 -> GR
            //login 26 -> RoleId: 1123 -> BP
            //login 27 -> RoleId: 1125 -> bahan GR
            //login 28 -> RoleId: 1124 -> bahan BP
            if ($scope.user.RoleId == 1135) {
                // / $scope.MaterialTypeId = 1;  -- change to 2 for bahan
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                console.log('1135', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1122 || $scope.user.RoleId == 1021) {
                // $scope.MaterialTypeId = 1;  -- change to 2 for bahan
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                console.log('1122', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1123) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 0;
                console.log('1123', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1125) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                console.log('1125', $scope.MaterialTypeId, $scope.ServiceTypeId);
                // $scope.ServiceTypeId = 1;
                // $scope.ServiceTypeId = 0;
                // $scope.FunctionGetData2(mData);
            } else if ($scope.user.RoleId == 1124) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 0;
                console.log('1124', $scope.MaterialTypeId, $scope.ServiceTypeId);
            }
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaterial = null; //Model$
        $scope.cMaterial = null; //Collection
        $scope.xMaterial = {};
        $scope.gridMaterialApi = {};
        $scope.formGridApi = {};
        $scope.formApi = {};
        $scope.xMaterial.selected = [];
        $scope.MaterialTypeId = null;
        $scope.ServiceTypeId = null;
        $scope.UomObj = {};
        $scope.MaterialTypeData = [];
        $scope.StockPData = [];
        $scope.SCCData = [];
        $scope.ICCData = [];
        $scope.FranchiseData = [];
        $scope.ClassifData = [];
        $scope.filter = {};

        $scope.UomData = [];
        $scope.warehouse = {
            WarehouseId: null,
            ShelfId: null
        };
        $scope.uom = {
            UomId: null,
            UomQty: null,
            UomIdTgt: null
        };

        //----------------------------------
        // Get Data
        //----------------------------------
        // $scope.getData = function() {
        //     console.log("get data..");
        // Material.getData(2).then(
        //     function(res) {
        //         console.log("===========",$scope.gridMaterialApi)
        //         // $scope.grid.data = res.rs.data.Result;
        //         $scope.grid.data = res.data.rs.Result;
        //         // console.log("data=>",res.data);
        //         console.log("data=>", res.data.rs);
        //         $scope.loading = false;
        //         // return res.rs.data;
        //         if ($scope.grid.data == "" || $scope.grid.data == undefined || $scope.grid.data == null || $scope.grid.data == []){
        //             bsNotify.show(
        //                 {
        //                     title: "Info",
        //                     content: "Data tidak ditemukan",
        //                     type: 'warning'
        //                 }
        //             );
        //           }
        //         return res.data.rs;

        //     },
        //     function(err) {
        //         console.log("err=>", err);
        //     }
        // );
        //};

        $scope.actionButtonSettings = [{
            title: 'Download',
            icon: 'fas fa-file-upload',
            func: function (row, formScope) {
                var excelData = [];
                var fileName = "MasterMaterial_Template";
                //var excelSheet2 = {"NoMaterial":"","NamaMaterial":"","DefaultVendor":"SPLD","NoMaterialLama":"","NamaMaterialLama":"","Dibuatdi":"","MaterialType":"","StockingPolicy":"","FranchiseReport":"","SSC":"","Clasification":"","ICC":"","WarehouseName":"","ShelfName":"","HargaRetail":"","DefaultSatuanBeli":"PCS","SatuanDasar":"","OrderCycle":"","LeadTime":"","SSDemand":"","SafetyStockLT":"","MinQtyMIP":"","DAD":"","MIP":""};
                var excelSheet2 =
                    // { "No Material": "", "Nama Material": "", "Default Vendor": "", "Stocking Policy": "", "Clasification": "", "Harga Retail": "", "Landed Cost": "" }
                    { "NoMaterial": "", "NamaMaterial": "", "KodeVendor": "", "NamaVendor": "", "StockingPolicy": "", "Clasification": "", "HargaRetail": "", "LandedCost": "","NamaGudang": "","NamaRak": ""}
                var excelData = [];
                excelData.push(excelSheet2);
                XLSXInterface.writeToXLSX(excelData, fileName);
            },

            type: 'custom' //for link
        }, {
            title: 'Upload',
            icon: 'fa-file-upload',
            func: function (row, formScope) {
                angular.element(document.querySelector('#UploadGroupBP')).val(null);
                angular.element('#ModalUploadGroupBP').modal('show');
            }
        }];

        $scope.loadXLSGroupBP = function(ExcelFile){
            var myEl = angular.element( document.querySelector( '#UploadGroupBP' ) ); //ambil elemen dari dokumen yang di-upload
            console.log("myEl=>",myEl);
  
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $scope.ExcelData = json;
                console.log("ExcelData : ", $scope.ExcelData);
                console.log("json", json);
                    });
          }

          $scope.getUnique = function (array){
            var uniqueArray = [];
            
            // Loop through array values
            for(i=0; i < array.length; i++){
                if(uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $scope.UploadGroupBPClicked = function(array){
            var inputData = [];
            var eachData = {};
            var countRak = 0;
            var countSama = 0;
            var ada_parts_sama = 0;
            

            angular.forEach($scope.ExcelData, function(value, key){
                console.log('excel Data', $scope.ExcelData);
                console.log('value', value);
                console.log('key', key);
            
                eachData = {
                    "PartsCode": value.NoMaterial,
                    "PartsName": value.NamaMaterial,
                    "VendorCode": value.KodeVendor,
                    "VendorName": value.NamaVendor,
                    "WarehouseId": value.NamaGudang,
                    "ShelfId": value.NamaRak,
                    "UomName": value.StockingPolicy,
                    "Clasification": value.Clasification,
                    "RetailPrice": value.HargaRetail,
                    "StandardCost": value.LandedCost
                }
                inputData.push(eachData);
                console.log('inputData123', inputData);

            });
            console.log('inputData',inputData);
            console.log('whdata',$scope.whData);
            console.log('cek data upload',eachData);

            var tmpArray = [];
            for (var i  in inputData) {
                tmpArray.push($scope.ExcelData[i].NoMaterial);
                    for (var x = 0; x < $scope.ExcelData.length; x++){
                        var ada_parts_sama = 0;
                        if ($scope.ExcelData[x].NoMaterial == inputData[i].PartsCode && (tmpArray.length != $scope.ExcelData.length)) {
                            ada_parts_sama++;
                            break;
                        }
                        tmpArray = $scope.getUnique(tmpArray);
                
                    }

                 }
                 console.log("Coba dah",tmpArray[i])
                 console.log("Coba dah1",tmpArray.length)
                 console.log("Coba dah2",tmpArray[1])
                 if(ada_parts_sama > 0){
                   
                    bsNotify.show({
                        title: "Master Material",
                        content: "Material Tidak boleh duplikat",
                        type: 'danger'
                    });
                    return
                }
            
            
            for(var z = 0; z < inputData.length; z++){
                var countSama = 0;
                    for (var zz in $scope.whData){              
                        if($scope.whData[zz].WarehouseName.toLowerCase() == inputData[z].WarehouseId.toLowerCase()){
                        countSama++;
                        break;
                    }          
                 }

            if (countSama <= 0 ) {
                bsNotify.show({
                    title: "Kode Gudang",
                    content: "tidak sesuai, mohon cek kembali",
                    type: 'danger'
                });
                return
            }
        }

            for (var zz in $scope.whData){              
                for(var z = 0; z < inputData.length; z++){
                    if($scope.whData[zz].WarehouseName.toString().toLowerCase() == inputData[z].WarehouseId.toString().toLowerCase()){
                    inputData[z].WarehouseId = angular.copy($scope.whData[zz].WarehouseId)
                    } 
                }
            }

        
        
            for(var z = 0; z < inputData.length; z++){
                var countRak = 0;
                    for (var zz in $scope.whData){         
                         for(var zzz in $scope.whData[zz].WarehouseShelf){
                    if($scope.whData[zz].WarehouseShelf[zzz].ShelfName.toLowerCase() == inputData[z].ShelfId.toLowerCase()){
                        console.log('Rak Apa', inputData[z].ShelfId)
                        countRak++;
                        break;
                     } 
                }
            }
            if (countRak <= 0) {
                bsNotify.show({
                    title: "Kode Rak",
                    content: "tidak sesuai, mohon cek kembali",
                    type: 'danger'
                });
                return     
            }
        }

            for (var zz in $scope.whData){ 
                for(var zzz in $scope.whData[zz].WarehouseShelf){

                for(var z = 0; z < inputData.length; z++){
                    if($scope.whData[zz].WarehouseShelf[zzz].ShelfName.toString().toLowerCase() == inputData[z].ShelfId.toString().toLowerCase()){
                        inputData[z].ShelfId = angular.copy($scope.whData[zz].WarehouseShelf[zzz].ShelfId)
                    } 
                }
            }
        }
           

            Material.createUpload(inputData).then(function (res) {
                var create = res.data;
                console.log("res=>",res);
                console.log("create=>",create);
                var tmpMstMaterial = res.data.ResponseMessage;
                console.log("resu Response", res.data);
                tmpMstMaterial = tmpMstMaterial.split('#');
                console.log("tmpMstMaterial=>", $scope.mData);
                $scope.dataMaterial = tmpMstMaterial[1];


                if (create.ResponseCode == 13) {
                    angular.element('#ModalUploadGroupBP').modal('hide');
                    angular.element( document.querySelector( '#UploadGroupBP' ) ).val(null);
                    bsNotify.show({
                        size: 'small',
                        type: 'success',
                        title: "Success",
                        content: "Data Berhasil Di Upload"
                    });
                } else {
                    bsAlert.alert({
                        title: $scope.dataMaterial,
                        text:'',
                        type: "error",
                        showCancelButton: false
                    },
                    function() {
                    
                    }
                    )
                } 

            })
            //$scope.getData();
            //$scope.goBack();
      }

        $scope.mData = {};
        $scope.getData = function() {
            console.log('mData', $scope.mData);
            console.log('eeeee',$scope.formGridApi)
            console.log('eeeee',$scope.formApi)
            if(($scope.filter.PartsCode === undefined || $scope.filter.PartsCode === "" ) && ($scope.filter.PartsName === undefined || $scope.filter.PartsName == "" )){
                bsNotify.show({
                    size: 'big',
                    title: 'Salah satu filter tidak boleh kosong',
                    text: 'Silahkan cek kembali',
                    timeout: 2000,
                    type: 'danger'
                });
                return
            }
            Material.getData(2, $scope.grid.paginationCurrentPage, $scope.grid.paginationPageSize, $scope.filter).then(function(res) {
                    console.log("===========", $scope.gridMaterialApi)
                        // $scope.grid.data = res.rs.data.Result;
                    $scope.grid.data = res.data.rs.Result;
                    $scope.grid.totalItems = res.data.total;
                    // console.log("data=>",res.data);
                    console.log("data=>", res.data.rs);
                    $scope.loading = false;
                    // return res.rs.data;
                    if ($scope.grid.data == "" || $scope.grid.data == undefined || $scope.grid.data == null || $scope.grid.data == []) {
                        bsNotify.show({
                            title: "Info",
                            content: "Data tidak ditemukan",
                            type: 'warning'
                        });
                    }
                    return res.data.rs;

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // $scope.grid.data = [];
            // $scope.grid.paginationPageSize = 10;
            $scope.grid.paginationPageSizes = [10, 50, 100, 250, 500, 100];
        }

        // $scope.onDeleteFilter = function(){
        //     //Pengkondisian supaya seolaholah kosong
        //     $scope.mData.MaterialTypeId = '';
        //     $scope.mData.WarehouseId = '';
        //     $scope.mData.PartsCode = '';
        //     $scope.mData.PartsName = '';
        //   }

        PartsCategory.getData(1).then(
            function(res) {
                var tmp = [];
                //  $scope.MaterialTypeData = res.data.Result;
                _.map(res.data.Result, function(val) {
                    if (val.PartsClassId == 2) {
                        tmp.push(val);
                    }
                })
                $scope.MaterialTypeData = tmp;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(2).then(
            function(res) {
                $scope.ICCData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(4).then(
            function(res) {
                $scope.StockPData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(3).then(
            function(res) {
                $scope.SCCData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(5).then(
            function(res) {
                $scope.ClassifData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(6).then(
            function(res) {
                $scope.FranchiseData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        Material.getVendor().then(
            function(res) {
                $scope.VendorData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        GeneralMaster.getData(1).then(
            function(res) {
                $scope.UomData = res.data.Result;
                $scope.gridDetail.columnDefs[0].editDropdownOptionsArray = angular.copy($scope.UomData);
                $scope.gridDetail.columnDefs[0].cellFilter = "griddropdown:this";
                $scope.gridDetail.columnDefs[2].editDropdownOptionsArray = angular.copy($scope.UomData);
                $scope.gridDetail.columnDefs[2].cellFilter = "griddropdown:this";
                $scope.gridDetail.columnDefs = angular.copy($scope.gridDetail.columnDefs);
                // console.log("$scope.UomData : ",$scope.UomData);
                return res.data;
            }
        );

        $scope.whData = [];
        $scope.shData = [];
        WarehouseShelfFactory.getData().then(
            function(res) {
                console.log("warehouse..");
                var tmpWh = []
                $scope.whData = angular.copy(res.data.Result);
                for(var i in res.data.Result){
                    if(res.data.Result[i].MaterialTypeId == $scope.MaterialTypeId){
                        tmpWh.push(res.data.Result[i]);
                    }
                }
                $scope.gridDetailLoc.columnDefs[0].editDropdownOptionsArray = angular.copy(tmpWh);
                $scope.gridDetailLoc.columnDefs[0].cellFilter = "griddropdown:this";
                $scope.gridDetailLoc.columnDefs[1].cellFilter = "griddropdownloc:this";
                $scope.gridDetailLoc.columnDefs = angular.copy($scope.gridDetailLoc.columnDefs);

                $scope.gridDetailLocView.columnDefs[0].editDropdownOptionsArray = angular.copy(tmpWh);
                $scope.gridDetailLocView.columnDefs[0].cellFilter = "griddropdown:this";
                $scope.gridDetailLocView.columnDefs[1].cellFilter = "griddropdownloc:this";
                $scope.gridDetailLocView.columnDefs = angular.copy($scope.gridDetailLocView.columnDefs);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        $scope.noResults = false;
        $scope.selected = {};

        $scope.setUomTgt = function(selected) {
            for (var i = 0; i < $scope.gridDetail.data.length; i++)
                $scope.gridDetail.data[i]["UomTgt"] = selected.MasterId;
        };

        $scope.onSelectRows = function(item) {
            console.log("onSelectRows=>", item);

        };

        // $scope.setShelf = function(selected){
        //     console.log(selected);
        //     $scope.shData = _.find($scope.whData, {
        //         WarehouseId: selected.WarehouseId,
        //         // WarehouseCode: selected.WarehouseCode,
        //         // WarehouseName: selected.WarehouseName,
        //     }).WarehouseShelf;
        //     $scope.gridDetailLoc.columnDefs[1].editDropdownOptionsArray = angular.copy($scope.shData);
        // }

        // $scope.addUom = function(){
        //     console.log($scope.uom.UomId);
        //     console.log(_.find($scope.UomData, {UomId : $scope.uom.UomId}));
        //     $scope.gridDetail.data.push({
        //         "Id": null,
        //         "PartsId": null,
        //         "UomId": $scope.uom.UomId,
        //         "UomIdName": _.find($scope.UomData, {MasterId : $scope.uom.UomId}).Name,
        //         "Qty": $scope.uom.Qty,
        //         "UomTgt": $scope.mMaterial.UomId,
        //         "UomTgtName": _.find($scope.UomData, {MasterId : $scope.mMaterial.UomId}).Name,
        //     });
        // }

        // $scope.addAlloc = function(){
        //     $scope.gridDetailLoc.data.push({
        //         "StockPositionId": null,
        //         "WarehouseId": $scope.warehouse.WarehouseId,
        //         "WarehouseName": _.find($scope.whData, {WarehouseId : $scope.warehouse.WarehouseId}).WarehouseName,
        //         "PartId": null,
        //         // "QtyOnHand": $scope.mMaterial.StockingPosition[i]["QtyOnHand"],
        //         // "QtyOnOrder": $scope.mMaterial.StockingPosition[i]["QtyOnOrder"],
        //         // "QtyOrder": $scope.mMaterial.StockingPosition[i]["QtyOrder"],
        //         // "QtyOnTransferOrder": $scope.mMaterial.StockingPosition[i]["QtyOnTransferOrder"],
        //         // "QtyReserved": $scope.mMaterial.StockingPosition[i]["QtyReserved"],
        //         // "QtyBlocked": $scope.mMaterial.StockingPosition[i]["QtyBlocked"],
        //         "ShelfId": $scope.warehouse.ShelfId,
        //         "ShelfName": _.find($scope.shData, {ShelfId : $scope.warehouse.ShelfId}).ShelfName,
        //     });
        // }

        $scope.beforeView = function(mdl, mode) {
            console.log("before beforeView", mdl, mode);
            $scope.mMaterial = angular.copy(mdl);
            $scope.mMaterial.DefaultVendorId = mdl.VendorId;

            if (mode == 'view' || mode == 'edit')
                $scope.beforeEdit();

            if (mode == 'view'){
                $scope.LocEdit = false;
            } else if (mode == 'edit'){
                $scope.LocEdit = true;
            } else if (mode == 'new'){
                $scope.LocEdit = true;
            }
        }

        $scope.beforeNew = function() {
            console.log("before new");
            $scope.gridDetail.data = [];
            $scope.parts_class_id5_edit = null;
            $scope.gridDetailLoc.data = [];
            $scope.gridDetailLocView.data = [];
            $scope.addNewRow();
            $scope.addNewLocRow();
            // $scope.mMaterial = {};
            $scope.mMaterial.PartsClassId1 = 2;
            $scope.pisahMenu = 2;
        }

        $scope.beforeEdit = function() {
            console.log("before edit",$scope.mMaterial);
            $scope.gridDetail.data = [];
            $scope.gridDetailLoc.data = [];
            $scope.gridDetailLocView.data = [];
            if ($scope.mMaterial.UomId == null || $scope.mMaterial.UomId == undefined || $scope.mMaterial.UomId == '') {
                $scope.pisahMenu = 2;
            } else {
                $scope.pisahMenu = 5;
            }
            $scope.parts_class_id5_edit = $scope.mMaterial.PartsClassId5;

            
            


            for (var i in $scope.mMaterial.PartsUoM) {
                // var UomFind = _.find($scope.UomData, {MasterId : $scope.mMaterial.PartsUoM[i]["UomId"]});
                // var UomTgtFind = _.find($scope.UomData, {MasterId : $scope.mMaterial.PartsUoM[i]["UomId"]});
                $scope.gridDetail.data.push({
                    "Id": $scope.mMaterial.PartsUoM[i]["Id"],
                    "PartsId": $scope.mMaterial.PartsUoM[i]["PartsId"],
                    "UomId": $scope.mMaterial.PartsUoM[i]["UomId"],
                    // "UomIdName": UomFind.Name,
                    "Qty": $scope.mMaterial.PartsUoM[i]["Qty"],
                    "UomTgt": $scope.mMaterial.UomId,
                    // "UomTgtName": UomTgtFind.Name,
                });
            }
            console.log("Pikonnnnnn",$scope.mMaterial.APart_StockPosition_TT);
            for (var i in $scope.mMaterial.APart_StockPosition_TT) {
                // var whFind = _.find($scope.whData, {WarehouseId : $scope.mMaterial.StockPosition[i]["WarehouseId"]});
                // console.log(whFind);
                // $scope.setShelf(whFind);
                $scope.gridDetailLoc.data.push({
                    "StockPositionId": $scope.mMaterial.APart_StockPosition_TT[i]["StockPositionId"],
                    "WarehouseId": $scope.mMaterial.APart_StockPosition_TT[i]["WarehouseId"],
                    // "WarehouseName": whFind.WarehouseName,
                    "PartId": $scope.mMaterial.APart_StockPosition_TT[i]["PartId"],
                    "QtyOnHand": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnHand"],
                    "QtyOnOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnOrder"],
                    "QtyOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOrder"],
                    "QtyOnTransferOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnTransferOrder"],
                    "QtyReserved": $scope.mMaterial.APart_StockPosition_TT[i]["QtyReserved"],
                    "QtyBlocked": $scope.mMaterial.APart_StockPosition_TT[i]["QtyBlocked"],
                    "ShelfId": $scope.mMaterial.APart_StockPosition_TT[i]["ShelfId"],
                    "ShelfList": _.find($scope.whData, {
                            WarehouseId: $scope.mMaterial.APart_StockPosition_TT[i]["WarehouseId"],
                        }).WarehouseShelf
                        
                        // "ShelfName": _.find($scope.shData, {ShelfId : $scope.mMaterial.StockPosition[i]["ShelfId"]}).ShelfName,
                });
                

                $scope.gridDetailLocView.data.push({
                    "StockPositionId": $scope.mMaterial.APart_StockPosition_TT[i]["StockPositionId"],
                    "WarehouseId": $scope.mMaterial.APart_StockPosition_TT[i]["WarehouseId"],
                    // "WarehouseName": whFind.WarehouseName,
                    "PartId": $scope.mMaterial.APart_StockPosition_TT[i]["PartId"],
                    "QtyOnHand": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnHand"],
                    "QtyOnOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnOrder"],
                    "QtyOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOrder"],
                    "QtyOnTransferOrder": $scope.mMaterial.APart_StockPosition_TT[i]["QtyOnTransferOrder"],
                    "QtyReserved": $scope.mMaterial.APart_StockPosition_TT[i]["QtyReserved"],
                    "QtyBlocked": $scope.mMaterial.APart_StockPosition_TT[i]["QtyBlocked"],
                    "ShelfId": $scope.mMaterial.APart_StockPosition_TT[i]["ShelfId"],
                    "ShelfList": _.find($scope.whData, {
                            WarehouseId: $scope.mMaterial.APart_StockPosition_TT[i]["WarehouseId"],
                        }).WarehouseShelf
                        // "ShelfName": _.find($scope.shData, {ShelfId : $scope.mMaterial.StockPosition[i]["ShelfId"]}).ShelfName,
                });
            }

            if($scope.gridDetailLoc.data == null || $scope.gridDetailLoc.data == ''){
                $scope.addNewLocRow();
            }else{
                $scope.checkLocRow();
                }
            // $scope.addNewRow();
           // $scope.addNewLocRow();
        }

        $scope.addNewRow = function() {
            $scope.gridDetail.data.push({
                "Id": null,
                "PartsId": null,
                "UomId": null,
                "Qty": null,
                "UomTgt": ($scope.mMaterial.UomId ? $scope.mMaterial.UomId : null)
            });
        }

        $scope.addNewLocRow = function() {
            $scope.gridDetailLoc.data.push({
                "StockPositionId": null,
                "PartId": null,
                "WarehouseId": null,
                "QtyOnHand": 0,
                "QtyOnOrder": 0,
                "QtyOrder": 0,
                "QtyOnTransferOrder": 0,
                "QtyReserved": 0,
                "QtyBlocked": 0,
                "ShelfId": null,
                "ShelfList": []
            });
        }

        $scope.checkRow = function(findObj) {
            var findObj = _.find($scope.gridDetail.data, {
                "Id": null,
                "PartsId": null,
                "UomId": null,
                "Qty": null,
                "UomTgt": ($scope.mMaterial.UomId ? $scope.mMaterial.UomId : null)
            });
            // if (findObj == undefined) {
            //     $scope.addNewRow();
            // }
        }

        $scope.checkLocRow = function() {
            var findObj = _.find($scope.gridDetailLoc.data, {
                "StockPositionId": 0,
                "PartId": 0,
                "WarehouseId": 0,
                "QtyOnHand": 0,
                "QtyOnOrder": 0,
                "QtyOrder": 0,
                "QtyOnTransferOrder": 0,
                "QtyReserved": 0,
                "QtyBlocked": 0,
                "ShelfId": 0,
                "ShelfList": []
            });
            // if (findObj == undefined) {
            //     $scope.addNewLocRow();
            // }
        }

        $scope.removeRow = function(gridSel, selected) {
            var detData = [];
            console.log(selected);

            var temp = (gridSel == 1 ? $scope.gridDetailLoc.data : $scope.gridDetail.data);
            // if (temp.length > 1) {
            //     _.pullAllWith(temp, [selected], _.isEqual);
            // } else {
            //     var temp = _.keys(selected);
            //     for (var i in temp) {
            //         selected[temp[i]] = null;
            //     }
            // }
            if(gridSel == 1){
                var idx = _.findIndex(temp, { "WarehouseId": selected.WarehouseId, "StockPositionId": selected.StockPositionId });
                if(idx > -1){
                    $scope.gridDetailLoc.data.splice(idx, 1);
                }
            }else{
                if (temp.length > 1) {
                    _.pullAllWith(temp, [selected], _.isEqual);
                } else {
                    var temp = _.keys(selected);
                    for (var i in temp) {
                        selected[temp[i]] = null;
                    }
                }
            }
        }
        $scope.onValidateSave = function(mdl, mode){
            console.log('===>', mdl, mode);
            if(mode === 'create'){
                Material.checkNoMaterial(mdl.PartsCode).then(function(res) {
                    if(res.data == 1){
                        // return true;
                        $scope.doCustomSave(mdl, mode);
                    }else{
                        bsNotify.show({
                            title: "Peringatan",
                            content: "No. Material sudah ada, silahkan gunakan No. Material yang lain",
                            type: 'warning'
                        });
                        return false;
                    }
                });
            }else{
                return true
            }
        }
        $scope.doCustomSave = function(mdl, mode) {
            console.log("inside before save");
            console.log("mdl : " + JSON.stringify(mdl));
            console.log("mode : " + mode);
            var grdData = $scope.gridDetail.data;
            console.log('grdData doCustomSave', grdData);
            var grdLocData = $scope.gridDetailLoc.data;
            var svObj = {
                "UsePartParamMIP": 0,
                "UsePartMinMIP": 0,
                "QtyParamOrderCycle": 0,
                "QtyParamLeadTime": 0,
                "QtyParamLeadTimeSS": 0,
                "QtyParamDemandSS": 0,
                "QtyParamMinMIP": 0,
            };
            var lastArr = [];
            var lastLocArr = [];

            for (var i = 0; i < grdData.length; i++) {
                
                if(grdData[i].UomId != undefined && grdData[i].UomId != null ){

                    var temp = {};
                    temp.Id = (grdData[i]["Id"] == null ? 0 : grdData[i]["Id"]);
                    temp.PartsId = (mode == 'create' ? 0 : mdl.PartsId);
                    temp.UomId = grdData[i].UomId;
                    temp.Qty = grdData[i].Qty;
                    lastArr.push(temp);
                }
                
            }
            console.log('grdData doCustomSave', grdData, grdLocData);
            console.log('svObj.PartsUoM doCustomSave', svObj.PartsUoM, svObj);
            for (var i = 0; i < grdLocData.length ; i++) {

                if((grdLocData[i].WarehouseId != undefined && grdLocData[i].ShelfId != undefined) && (grdLocData[i].ShelfId != null && grdLocData[i].WarehouseId != null )){
                    console.log("ayam geprek gapake sambel",grdLocData[i].StockPositionId)
                var temp = {};
        
                temp.StockPositionId = (mode == 'create' || grdLocData[i]["StockPositionId"] == null ? 0 : grdLocData[i]["StockPositionId"]);
                temp.PartId = (mode == 'create' ? 0 : mdl.PartsId);
                temp.WarehouseId = grdLocData[i].WarehouseId;
                temp.QtyOnHand = grdLocData[i].QtyOnHand;
                temp.QtyOnOrder = grdLocData[i].QtyOnOrder;
                temp.QtyOrder = grdLocData[i].QtyOrder;
                temp.QtyOnTransferOrder = grdLocData[i].QtyOnTransferOrder;
                temp.QtyReserved = grdLocData[i].QtyReserved;
                temp.QtyBlocked = grdLocData[i].QtyBlocked;
                temp.ShelfId = grdLocData[i].ShelfId;
                lastLocArr.push(temp);
            }
        }
            //
            console.log("wowo",lastArr );
            console.log("wowo",lastLocArr );

            svObj.PartsUoM = lastArr;
            svObj.APart_StockPosition_TT = lastLocArr;


            console.log('svObj.PartsUoM doCustomSave', svObj.PartsUoM, svObj);
            console.log('svObj.APart_StockPosition_TT doCustomSave', svObj.APart_StockPosition_TT, svObj);

           

            if (mode == 'create') {
                svObj.PartsId = 0;
                var lastSave = Object.assign(svObj, mdl);
                console.log('svObj', svObj);
                console.log('mdl', mdl);
                console.log("lastSave Create : ", lastSave);
                lastSave.DefaultVendorId = lastSave.VendorId;

             

                Material.create(lastSave).then(function(res) {
                    
                        PartsGlobal.showAlert({
                          title: "Master Material",
                          content: "Data berhasil disimpan",
                          type: 'success'
                          });

                         
                    

                    $scope.gridDetail.data = [];
                    $scope.gridDetailLoc.data = [];
                    $scope.gridDetailLocView.data = [];
                    $scope.formApi.setMode('grid');
                    $scope.getData();

                    
                });
            } else {
                console.log('grdData doCustomSave', grdData, grdLocData);
                var lastSave = Object.assign(svObj, mdl);

                lastSave.PartsUoM = lastArr;
                // APart_StockPosition_TT
                // lastSave.StockPositionId = lastLocArr;
                lastSave.APart_StockPosition_TT = lastLocArr;


                console.log('svObj', svObj, lastSave);
                console.log('mdl', mdl);
                console.log("lastSave Update : ", lastSave);
                lastSave.DefaultVendorId = lastSave.VendorId;
                lastSave.StatusCode = 1;
                Material.update(lastSave).then(function(res) {

                    PartsGlobal.showAlert({
                          title: "Master Material",
                          content: "Ubah Data berhasil disimpan",
                          type: 'success'
                          });

                    $scope.gridDetail.data = [];
                    $scope.gridDetailLoc.data = [];
                    $scope.gridDetailLocView.data = [];
                    $scope.formApi.setMode('grid');
                    $scope.getData();
                });
            }
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            useCustomPagination: true,
            useExternalPagination: true,
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 50, 100, 250, 500, 1000],
            // paginationPageSize: 10,
            paginationCurrentPage: 1,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode', pinnedLeft: true },
                { name: 'Nama Material', field: 'PartsName' },
                { name: 'Default Vendor', field: 'VendorName' },
                { name: 'Stocking Policy', field: 'StockingPolicy' },
                { name: 'SCC', field: 'SCC' },
                { name: 'ICC', field: 'ICC' },
                { name: 'Franchise (Report)', field: 'Franchise' },
                { name: 'Clasification', field: 'Clasification' },
                // { name : 'Lokasi Gudang', field:'Warehouse' },
                // { name : 'Lokasi Rak', field:'WarehouseShelf' },
                { name: 'Harga Retail', field: 'RetailPrice' },
                { name: 'Landed Cost', field: 'StandardCost' },
                // { name : 'Satuan Dasar', field:'Satuan' },
            ],
            // onRegisterApi: function(gridApi) {
            //     // $scope.GridApiMaterialList = gridApi;
            //     gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //         $scope.MaterialList_UIGrid_Paging(pageNumber, pageSize);
            //     });
            // }
            onRegisterApi: function(gridApi){
                $scope.formGridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                    console.log("kacauuuuu");
                    $scope.MaterialList_UIGrid_Paging(pageNumber, pageSize);

                });
            }
        };
        $timeout(function() {
            $scope.formGridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                console.log("kacauuuuu", pageNumber, pageSize);
                $timeout(function() {
                    // $scope.getTableHeight();
                    $scope.MaterialList_UIGrid_Paging(pageNumber, pageSize);
                },100);
            });
        },5000);
        // $scope.gridMaterialApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
        //     console.log('mm1',pageNumber, pageSize );
        // });  
        $scope.tmpPageSize = 10;
        $scope.MaterialList_UIGrid_Paging = function(pageNumber, pageSize) {
            console.log('aa', pageNumber, pageSize);
            if (pageSize !== $scope.tmpPageSize) {
                $scope.tmpPageSize = angular.copy(pageSize);
                $scope.grid.paginationCurrentPage = 1;
                $scope.getData();
            } else {
                $scope.getData();
            }

            // Material.getData(pageNumber, $scope.uiGridPageSize, pageSize)
            // .then(
            //     function(res) {
            //         $scope.grid.data = res.data.rs.Result;
            //         console.log("data=>", res.data.rs);
            //         $scope.loading = false;
            //         return res.data.rs;
            //     }
            // );
        }

        $scope.gridDetailApi = {};
        $scope.gridDetail = {
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus
            columnDefs: [{
                    field: 'UomId',
                    name: 'Satuan Dasar',
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'MasterId',
                    editDropdownOptionsArray: [],
                },
                {
                    name: 'Nilai Konversi',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <input type="number" class="form-control" name="ConversionVal" ng-model="row.entity.Qty" min="1" style="height:50">' +
                        '</div>'
                },
                {
                    field: 'UomTgt',
                    name: 'Satuan Konversi',
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'MasterId',
                    editDropdownOptionsArray: [],
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <button class="ui icon inverted grey button"' +
                        '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        '           onclick="this.blur()"' +
                        '              ng-click="grid.appScope.removeRow(2,row.entity)">' +
                        '       <i class="fa fa-fw fa-lg fa-times"></i>' +
                        '   </button>' +
                        '</div>'
                }
            ]
        };

        $scope.formApi = {};

        $scope.gridDetail.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridDetailApi = gridApi;

            $scope.gridDetailApi.selection.on.rowSelectionChanged($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
                // console.log("bsform selected=>",scope.selectedRows);
            });
            $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
            });

            $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                // console.log('Inside afterCellEdit grid detail..');
                $scope.checkRow();
            });
        };

        $scope.gridDetailLocApi = {};
        $scope.gridDetailLoc = {
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: true,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus
            columnDefs: [{
                    field: 'WarehouseId',
                    name: 'Gudang',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'WarehouseName',
                    editDropdownIdLabel: 'WarehouseId',
                    editDropdownOptionsArray: [],
                },
                {
                    field: 'ShelfId',
                    name: 'Rak',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'ShelfName',
                    editDropdownIdLabel: 'ShelfId',
                    // editDropdownOptionsArray: [],
                    editDropdownRowEntityOptionsArrayPath: 'ShelfList',
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <button class="ui icon inverted grey button"' +
                        '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        '           onclick="this.blur()"' +
                        '              ng-click="grid.appScope.removeRow(1,row.entity)">' +
                        '       <i class="fa fa-fw fa-lg fa-times"></i>' +
                        '   </button>' +
                        '</div>'
                }
            ]
        };

        $scope.gridDetailLocApiView = {};
        $scope.gridDetailLocView = {
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: false, // set any editable column to allow edit on focus
            columnDefs: [{
                    field: 'WarehouseId',
                    name: 'Gudang',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'WarehouseName',
                    editDropdownIdLabel: 'WarehouseId',
                    editDropdownOptionsArray: [],
                },
                {
                    field: 'ShelfId',
                    name: 'Rak',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'ShelfName',
                    editDropdownIdLabel: 'ShelfId',
                    // editDropdownOptionsArray: [],
                    editDropdownRowEntityOptionsArrayPath: 'ShelfList',
                },
                // {
                //     name: 'Action',
                //     allowCellFocus: false,
                //     enableColumnMenu: false,
                //     enableSorting: false,
                //     enableColumnResizing: false,
                //     cellTemplate: '<div class="ui-grid-cell-contents">' +
                //         '   <button class="ui icon inverted grey button"' +
                //         '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                //         '           onclick="this.blur()"' +
                //         '              ng-click="grid.appScope.removeRow(1,row.entity)">' +
                //         '       <i class="fa fa-fw fa-lg fa-times"></i>' +
                //         '   </button>' +
                //         '</div>'
                // }
            ]
        };

        $scope.gridDetailLoc.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridDetailLocApi = gridApi;

            $scope.gridDetailLocApi.selection.on.rowSelectionChanged($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailLocRows = $scope.gridDetailLocApi.selection.getSelectedRows();
                // console.log("bsform selected=>",scope.selectedRows);
            });
            $scope.gridDetailLocApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailLocRows = $scope.gridDetailLocApi.selection.getSelectedRows();
            });

            $scope.gridDetailLocApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                // console.log('Inside afterCellEdit grid detail..');
                if (colDef.name == "Gudang") {
                    // $scope.setShelf(rowEntity);
                    rowEntity.ShelfList = _.find($scope.whData, {
                        WarehouseId: rowEntity.WarehouseId,
                    }).WarehouseShelf;
                    console.log("rowEntity.ShelfList : ", rowEntity.ShelfList);
                }
                $scope.checkLocRow();
            });
        };
    }).filter('griddropdown', function() {
        return function(input, xthis) {
            console.log("filter..");
            console.log(input);
            console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        }

                    }
                }
            }

            return '';
        };
   // })
     }).filter('griddropdownloc', function() {
         return function(input, xthis) {
             console.log('griddropdownloc', xthis);
             if (xthis !== undefined) {
                 if (xthis.col !== undefined) {
                     // console.log(xthis.$parent.$parent.row.entity);
                     var xmap = xthis.col.colDef.editDropdownRowEntityOptionsArrayPath;
                     var map = xthis.$parent.$parent.row.entity[xmap];
                     var idField = xthis.col.colDef.editDropdownIdLabel;
                     var valueField = xthis.col.colDef.editDropdownValueLabel;
                     if (map !== undefined) {
                         for (var i = 0; i < map.length; i++) {
                             console.log(map[i]);
                             if (map[i][idField] == input) {
                                 return map[i][valueField];
                             }
                         }
                     } else {
                         return '';
                     }
                 }
             }

             return '';
         };
     });