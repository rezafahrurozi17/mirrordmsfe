angular.module('app')
.controller('HolidayController', function($scope, $http, CurrentUser, HolidayFactory, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.getData = function(){
        HolidayFactory.getData()
        .then(
            function(res){
                gridData = [];
                gridData=res.data.Result;
                _.map(gridData,function(e){
                    var year = new Date(e.HolidayDate);

                    e.Year = year.getFullYear();
                });
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = gridData ;
                console.log("gridData=>",gridData);
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    };
    $scope.onBeforeNewMode = function(row,mode){
        console.log("onBeforeNewMode=>",row,mode);
    };
    $scope.onShowDetail = function(row,mode){
        console.log("onShowDetail=>",row,mode);
        // $scope.mData=row;
    };
    $scope.onValidateSave=function(model){
        var date = new Date(model.HolidayDate);
        model.HolidayDate = new Date(date.setDate(date.getDate()+1));
        console.log("onValidateSave=>",model);

        return true;
    }


    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'HolidaysId',field:'HolidaysId', width:'7%', visible:false },
            { name:'Tahun', field:'Year' },
            { name:'Tanggal ', field:'HolidayDate', cellFilter:dateFilter},
            { name:'Keterangan ', field:'HolidayDesc'},
        ]
    };
});