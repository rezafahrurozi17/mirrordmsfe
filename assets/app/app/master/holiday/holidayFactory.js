angular.module('app')
.factory('HolidayFactory', function($http, CurrentUser, $q) {
  var currentUser = CurrentUser.user();

  	return {
    	getData: function() {
      	var res=$http.get('/api/as/Holidays');
     	return res;
        },
        create:function(model){
            return $http.post('/api/as/Holidays',[model]);
        },
        update:function(model){
            return $http.put('/api/as/Holidays',[model]);
        }
   	}
});