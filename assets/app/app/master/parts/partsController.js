angular.module('app')
    .controller('PartsController', function($scope, $http   ,PartsGlobal, uiGridConstants, CurrentUser, Parts, bsNotify, GeneralMaster, PartsCategory, WarehouseShelfFactory, VendorMst, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];

            $scope.flagDisableEdit = false;

            // mParts.PartsClassId5 == 36 || mParts.PartsClassId5 == 38
            // ini untuk list disable
            $scope.list_PartsClassId5_disable = [36, 38];
            $scope.disabled = true;
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mParts = {}; //Model
        $scope.cParts = null; //Collection
        $scope.xParts = {};
        $scope.xParts.selected = [];

        $scope.UomObj = {};
        $scope.PartsData = [];
        $scope.PartsTypeData = [];
        $scope.StockPData = [];
        $scope.SCCData = [];
        $scope.ICCData = [];
        $scope.FranchiseData = [];
        $scope.ClassifData = [];

        $scope.formGridApi = {};

        $scope.UomData = [];
        $scope.warehouse = {
            WarehouseId: null,
            ShelfId: null
        };
        $scope.uom = {
            UomId: null,
            UomQty: null,
            UomIdTgt: null
        };

        $scope.onSelectRows = function(item) {
            console.log("get onSelectRows..", item);

        };

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.getData = function(mParts) {
            $scope.grid.data = [];
            $scope.loading = true;
            console.log("get data..",mParts, $scope.formGridApi);
            if(($scope.mParts.PartsCode === undefined || $scope.mParts.PartsCode === "" ) && ($scope.mParts.PartsName === undefined || $scope.mParts.PartsName == "" )){
                bsNotify.show({
                    size: 'big',
                    title: 'Salah satu filter tidak boleh kosong',
                    text: 'Silahkan cek kembali',
                    timeout: 2000,
                    type: 'danger'
                });
                return
            }
            Parts.getData( $scope.grid.paginationCurrentPage, $scope.grid.paginationPageSize ,mParts).then(
                function(res) {
                    // $scope.grid.data = res.data.Result;
                    // $scope.PartsData = res.data.Result;
                    $scope.grid.data = res.data.rs.Result;
                    $scope.PartsData = res.data.rs.Result;

                    $scope.mip_raw = res.data.raw_mip.Result;
                    $scope.dad_raw = res.data.raw_dad.Result;

                    for (var i in $scope.grid.data) {
                        var tempParts = $scope.grid.data[i].PartsId;
                        var indexMIP = $scope.mip_raw.findIndex(function(obj) {
                            return obj.PartsId == tempParts;
                        });

                        var indexDAD = $scope.dad_raw.findIndex(function(obj) {
                            return obj.PartsId == tempParts;
                        });

                        $scope.grid.data[i].Mip = $scope.mip_raw[indexMIP].QtyMIP;
                        $scope.grid.data[i].Dad = $scope.dad_raw[indexDAD].QtyDAD;

                        $scope.PartsData[i].Mip = $scope.mip_raw[indexMIP].QtyMIP;
                        $scope.PartsData[i].Dad = $scope.dad_raw[indexDAD].QtyDAD;

                    }
                    $scope.grid.totalItems = res.data.total;


                    // console.log("data=>",res.rs.data);
                    console.log("data=>", res.data.rs);
                    $scope.loading = false;
                    // return res.data;
                    return res.data.rs;
                },
                function(err) {
                    console.log("err=>", err);
                    $scope.loading = false;
                }
            );
        };

        $scope.afterCancel = function() {
            $scope.loading = true;
            $scope.grid.data = [];
            $scope.getData({PartsName:null, PartsCode:null});
        }

        PartsCategory.getData(1).then(
            function(res) {
                $scope.PartsTypeData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(2).then(
            function(res) {
                $scope.ICCData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(4).then(
            function(res) {
                $scope.StockPData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(3).then(
            function(res) {
                $scope.SCCData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(5).then(
            function(res) {
                $scope.ClassifData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        PartsCategory.getData(6).then(
            function(res) {
                $scope.FranchiseData = res.data.Result;
                console.log("data=>", res.data);
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        VendorMst.getData().then(
            function(res) {
                $scope.VendorData = res.data.Result;
                console.log("data=>VendorData", res.data);
                $scope.datasatu=[];   
                for (var i in res.data.Result){
                    if (res.data.Result[i].BusinessUnitId == 9){
                        $scope.datasatu.push(res.data.Result[i]);                  
                    }
                }     
                console.log("$scope.datasatu",$scope.datasatu);                     
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        GeneralMaster.getData(1).then(
            function(res) {
                $scope.UomData = res.data.Result;
                $scope.gridDetail.columnDefs[0].editDropdownOptionsArray = angular.copy($scope.UomData);
                $scope.gridDetail.columnDefs[0].cellFilter = "griddropdown:this";
                $scope.gridDetail.columnDefs[2].editDropdownOptionsArray = angular.copy($scope.UomData);
                $scope.gridDetail.columnDefs[2].cellFilter = "griddropdown:this";
                $scope.gridDetail.columnDefs = angular.copy($scope.gridDetail.columnDefs);
                // console.log("$scope.UomData : ",$scope.UomData);
                return res.data;
            }
        );

        $scope.whData = [];
        $scope.shData = [];
        WarehouseShelfFactory.getData().then(
            function(res) {
                console.log("warehouse..");
                for (var i in res.data.Result){
                    if (res.data.Result[i].MaterialTypeId == 1){
                        $scope.whData.push(res.data.Result[i]);                  
                    }
                }     
                //$scope.whData = angular.copy(res.data.Result);
                $scope.gridDetailLoc.columnDefs[0].editDropdownOptionsArray = angular.copy($scope.whData);
                $scope.gridDetailLoc.columnDefs[0].cellFilter = "griddropdown:this";
                $scope.gridDetailLoc.columnDefs[1].cellFilter = "griddropdownloc:this";
                $scope.gridDetailLoc.columnDefs = angular.copy($scope.gridDetailLoc.columnDefs);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        $scope.noResults = false;
        $scope.selected = {};

        $scope.setUomTgt = function(selected) {
            for (var i = 0; i < $scope.gridDetail.data.length; i++)
                $scope.gridDetail.data[i]["UomTgt"] = selected.MasterId;
        }
        $scope.actionButtonSettings = [{
            enable: false,
            title: 'Download',
            icon: 'fas fa-file-upload',
            func: function(row, formScope) {
            var excelData = [];
            var fileName = "MasterPart_Template";
            //var excelSheet2 = {"NoMaterial":"","NamaMaterial":"","DefaultVendor":"SPLD","NoMaterialLama":"","NamaMaterialLama":"","Dibuatdi":"","MaterialType":"","StockingPolicy":"","FranchiseReport":"","SSC":"","Clasification":"","ICC":"","WarehouseName":"","ShelfName":"","HargaRetail":"","DefaultSatuanBeli":"PCS","SatuanDasar":"","OrderCycle":"","LeadTime":"","SSDemand":"","SafetyStockLT":"","MinQtyMIP":"","DAD":"","MIP":""};
            var excelSheet2 =
	        {"UsePartParamMIP":"","UsePartMinMIP":"","PartsId":"","UomId":"","Qty":"","StockPositionId":"","PartId":"","WarehouseId":"","QtyOnHand":"","QtyOnOrder":"","QtyOrder":"","QtyOnTransferOrder":"","QtyReserved":"","QtyBlocked":"","ShelfId":"","PartsId":"","disable_mip_dad":"","PartsCode":"","SubtitudeFromId":"","PartsName":"","MadeIn":"","DefaultVendorId":"5254","PartsClassId1":"","PartsClassId4":"","PartsClassId6":"","PartsClassId3":"","PartsClassId5":"","PartsClassId2":"","UomId":"","Active":"True","paramByParts":"True","QtyParamOrderCycle":"","QtyParamLeadTime":"","QtyParamDemandSS":"","QtyParamLeadTimeSS":"","QtyParamMinMIP":""}
            var excelData = [];
            excelData.push(excelSheet2);
            XLSXInterface.writeToXLSX(excelData, fileName);
            },

            type: 'custom' //for link
        },{
          enable: false,
          title: 'Upload',
          icon: 'fa-file-upload',
          func: function (row, formScope) {
            angular.element( document.querySelector( '#UploadGroupBP' ) ).val(null);
        		angular.element('#ModalUploadGroupBP').modal('show');
          }
        }];

        $scope.loadXLSGroupBP = function(ExcelFile){
            var myEl = angular.element( document.querySelector( '#UploadGroupBP' ) ); //ambil elemen dari dokumen yang di-upload


            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $scope.ExcelData = json;
                console.log("ExcelData : ", $scope.ExcelData);
                console.log("json", json);
                    });
          }
           
          $scope.UploadGroupBPClicked = function(){
            var eachData = {};
            var inputData = [];

            angular.forEach($scope.ExcelData, function(value, key){
            console.log('excel Data', $scope.ExcelData);
            console.log('value', value);
            console.log('key', key);

                eachData = {
                    "UsePartParamMIP":value.UsePartParamMIP,
                    "UsePartMinMIP":value.UsePartMinMIP,
                    "PartsUoM":
                        [{"Id":0,
                        "PartsId":value.PartId,
                        "UomId":value.UomId,
                        "Qty":value.Qty}],
                    "APart_StockPosition_TT":
                        [{"StockPositionId":value.StockPositionId,
                        "PartId":value.PartId,
                        "WarehouseId":value.WarehouseId,
                        "QtyOnHand":value.QtyOnHand,
                        "QtyOnOrder":value.QtyOnOrder,
                        "QtyOrder":value.QtyOrder,
                        "QtyOnTransferOrder":value.QtyOnTransferOrder,
                        "QtyReserved":value.QtyReserved,
                        "QtyBlocked":value.QtyBlocked,
                        "ShelfId":value.ShelfId}],
                    "PartsId":value.PartId,
                    "disable_mip_dad":value.disable_mip_dad,
                    "PartsCode":value.PartsCode,
                    "SubtitudeFromId":value.SubtitudeFromId,
                    "PartsName":value.PartsName,
                    "MadeIn":value.MadeIn,
                    "DefaultVendorId":5254,
                    "PartsClassId1":value.PartsClassId1,
                    "PartsClassId4":value.PartsClassId4,
                    "PartsClassId6":value.PartsClassId6,
                    "PartsClassId3":value.PartsClassId3,
                    "PartsClassId5":value.PartsClassId5,
                    "PartsClassId2":value.PartsClassId2,
                    "UomId":value.UomId,
                    "Active":value.Active,
                    "paramByParts":value.paramByParts,
                    "QtyParamOrderCycle":value.QtyParamOrderCycle,
                    "QtyParamLeadTime":value.QtyParamLeadTime,
                    "QtyParamDemandSS":value.QtyParamDemandSS,
                    "QtyParamLeadTimeSS":value.QtyParamLeadTimeSS,
                    "QtyParamMinMIP":value.QtyParamMinMIP
                     }
                    inputData.push(eachData);
                });
            console.log('cek data upload',eachData);
            Parts.create(eachData).then(function (res) {
              angular.element('#ModalUploadGroupBP').modal('hide');
              angular.element( document.querySelector( '#UploadGroupBP' ) ).val(null);
              bsNotify.show({
                size: 'small',
                type: 'success',
                title: "Success",
                content: "Data Berhasil Di Upload"
              });
            })
            }

        // $scope.setShelf = function(selected){
        //     console.log(selected);
        //     $scope.shData = _.find($scope.whData, {
        //         WarehouseId: selected.WarehouseId,
        //         // WarehouseCode: selected.WarehouseCode,
        //         // WarehouseName: selected.WarehouseName,
        //     }).WarehouseShelf;
        //     $scope.gridDetailLoc.columnDefs[1].editDropdownOptionsArray = angular.copy($scope.shData);
        // }

        // $scope.addUom = function(){
        //     console.log($scope.uom.UomId);
        //     console.log(_.find($scope.UomData, {UomId : $scope.uom.UomId}));
        //     $scope.gridDetail.data.push({
        //         "Id": null,
        //         "PartsId": null,
        //         "UomId": $scope.uom.UomId,
        //         "UomIdName": _.find($scope.UomData, {MasterId : $scope.uom.UomId}).Name,
        //         "Qty": $scope.uom.Qty,
        //         "UomTgt": $scope.mParts.UomId,
        //         "UomTgtName": _.find($scope.UomData, {MasterId : $scope.mParts.UomId}).Name,
        //     });
        // }

        // $scope.addAlloc = function(){
        //     $scope.gridDetailLoc.data.push({
        //         "StockPositionId": null,
        //         "WarehouseId": $scope.warehouse.WarehouseId,
        //         "WarehouseName": _.find($scope.whData, {WarehouseId : $scope.warehouse.WarehouseId}).WarehouseName,
        //         "PartId": null,
        //         // "QtyOnHand": $scope.mParts.StockingPosition[i]["QtyOnHand"],
        //         // "QtyOnOrder": $scope.mParts.StockingPosition[i]["QtyOnOrder"],
        //         // "QtyOrder": $scope.mParts.StockingPosition[i]["QtyOrder"],
        //         // "QtyOnTransferOrder": $scope.mParts.StockingPosition[i]["QtyOnTransferOrder"],
        //         // "QtyReserved": $scope.mParts.StockingPosition[i]["QtyReserved"],
        //         // "QtyBlocked": $scope.mParts.StockingPosition[i]["QtyBlocked"],
        //         "ShelfId": $scope.warehouse.ShelfId,
        //         "ShelfName": _.find($scope.shData, {ShelfId : $scope.warehouse.ShelfId}).ShelfName,
        //     });
        // }

        $scope.beforeNew = function() {
            console.log("before new");
            $scope.gridDetail.data = [];
            $scope.gridDetailLoc.data = [];
            $scope.mParts.disable_mip_dad = 1;
            $scope.parts_class_id5_edit = null;
            $scope.isViewKelompok = 1;
            $scope.isView = false
            $scope.addNewRow();
            $scope.addNewLocRow();
        }

        // $scope.search_mip_and_dad = function(mParts){
        //     var mipIndex = $scope.mip_raw.findIndex(function(obj){
        //         return obj.PartsId == mParts.PartsId
        //     });

        //     var dadIndex = $scope.dad_raw.findIndex(function(obj){
        //         return obj.PartsId == mParts.PartsId
        //     });

        //     return {
        //         mip : $scope.mip_raw[mipIndex].QtyMIP || 0,
        //         dad : $scope.dad_raw[dadIndex].QtyDAD || 0
        //     }
        // }

        $scope.beforeView = function(mdl, mode) {
            // BuildLocation
            if (mode == 'view' || mode == 'edit') {
                $scope.mParts = angular.copy(mdl);
                $scope.mParts.DefaultVendorId = $scope.mParts.VendorId;
                $scope.mParts.MadeIn = $scope.mParts.BuildLocation;
                $scope.beforeEdit(mode);
            }

        }

        $scope.beforeEdit = function(mode) {
            console.log("before edit",$scope.list_PartsClassId5_disable.indexOf($scope.mParts.PartsClassId5,mode));
            $scope.gridDetail.data = [];
            $scope.gridDetailLoc.data = [];

            $scope.mParts.disable_mip_dad = 1;

            // var temp_dad_mip = $scope.search_mip_and_dad($scope.mParts);

            // $scope.mParts.Mip = temp_dad_mip.mip;
            // $scope.mParts.Dad = temp_dad_mip.dad;
            if (mode == 'view') {
              $scope.isViewKelompok = 0;
              $scope.isView = true;
            }else {
              $scope.isView = true;
              $scope.isViewKelompok = 1;
            }

            $scope.parts_class_id5_edit = $scope.mParts.PartsClassId5;
            for (var i in $scope.mParts.PartsUoM) {
                // var UomFind = _.find($scope.UomData, {MasterId : $scope.mParts.PartsUoM[i]["UomId"]});
                // var UomTgtFind = _.find($scope.UomData, {MasterId : $scope.mParts.PartsUoM[i]["UomId"]});
                $scope.gridDetail.data.push({
                    "Id": $scope.mParts.PartsUoM[i]["Id"],
                    "PartsId": $scope.mParts.PartsUoM[i]["PartsId"],
                    "UomId": $scope.mParts.PartsUoM[i]["UomId"],
                    // "UomIdName": UomFind.Name,
                    "Qty": $scope.mParts.PartsUoM[i]["Qty"],
                    "UomTgt": $scope.mParts.UomId,
                    // "UomTgtName": UomTgtFind.Name,
                });
            }
            for (var i in $scope.mParts.APart_StockPosition_TT) {
                // var whFind = _.find($scope.whData, {WarehouseId : $scope.mParts.StockPosition[i]["WarehouseId"]});
                // console.log(whFind);
                // $scope.setShelf(whFind);
                $scope.gridDetailLoc.data.push({
                    "StockPositionId": $scope.mParts.APart_StockPosition_TT[i]["StockPositionId"],
                    "WarehouseId": $scope.mParts.APart_StockPosition_TT[i]["WarehouseId"],
                    // "WarehouseName": whFind.WarehouseName,
                    "PartId": $scope.mParts.APart_StockPosition_TT[i]["PartId"],
                    "QtyOnHand": $scope.mParts.APart_StockPosition_TT[i]["QtyOnHand"],
                    "QtyOnOrder": $scope.mParts.APart_StockPosition_TT[i]["QtyOnOrder"],
                    "QtyOrder": $scope.mParts.APart_StockPosition_TT[i]["QtyOrder"],
                    "QtyOnTransferOrder": $scope.mParts.APart_StockPosition_TT[i]["QtyOnTransferOrder"],
                    "QtyReserved": $scope.mParts.APart_StockPosition_TT[i]["QtyReserved"],
                    "QtyBlocked": $scope.mParts.APart_StockPosition_TT[i]["QtyBlocked"],
                    "ShelfId": $scope.mParts.APart_StockPosition_TT[i]["ShelfId"],
                    "ShelfList": _.find($scope.whData, {
                            WarehouseId: $scope.mParts.APart_StockPosition_TT[i]["WarehouseId"],
                        }).WarehouseShelf
                        // "ShelfName": _.find($scope.shData, {ShelfId : $scope.mParts.StockPosition[i]["ShelfId"]}).ShelfName,
                });
            }
            $scope.addNewRow();
            $scope.addNewLocRow();

            if($scope.mParts.PartsClassId3 != 113 && $scope.mParts.PartsClassId1 == 1){
                for(var i in $scope.SCCData){
                    if($scope.SCCData[i].PartsClassId == 113){
                        $scope.SCCData.splice(i, 1);
                        break;
                    }
                }
            }else{
                $scope.SCCData = [];
                PartsCategory.getData(3).then(
                    function(res) {
                        $scope.SCCData = res.data.Result;
                        console.log("data=>", res.data);
                        return res.data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        $scope.addNewRow = function() {
            $scope.gridDetail.data.push({
                "guid": $scope.guid(),
                "Id": null,
                "PartsId": null,
                "UomId": null,
                "Qty": null,
                "UomTgt": ($scope.mParts.UomId ? $scope.mParts.UomId : null)
            });
        }

        $scope.addNewLocRow = function() {
            $scope.gridDetailLoc.data.push({
                "StockPositionId": null,
                "PartId": null,
                "WarehouseId": null,
                "QtyOnHand": 0,
                "QtyOnOrder": 0,
                "QtyOrder": 0,
                "QtyOnTransferOrder": 0,
                "QtyReserved": 0,
                "QtyBlocked": 0,
                "ShelfId": null,
                "ShelfList": []
            });
        }

        $scope.checkRow = function() {
            var findObj = _.find($scope.gridDetail.data, {
                "Id": null,
                "PartsId": null,
                "UomId": null,
                "Qty": null,
                "UomTgt": ($scope.mParts.UomId ? $scope.mParts.UomId : null)
            });
            if (findObj == undefined) {
                $scope.addNewRow();
            }
        }

        $scope.guid = function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        $scope.checkLocRow = function() {
            var findObj = _.find($scope.gridDetailLoc.data, {
                "StockPositionId": null,
                "PartId": null,
                "WarehouseId": null,
                "QtyOnHand": 0,
                "QtyOnOrder": 0,
                "QtyOrder": 0,
                "QtyOnTransferOrder": 0,
                "QtyReserved": 0,
                "QtyBlocked": 0,
                "ShelfId": null,
                "ShelfList": []
            });
            // if (findObj == undefined) {
            //     $scope.addNewLocRow();
            // }
        }

        $scope.removeRow = function(gridSel, selected) {
            var detData = [];
            console.log(selected);

            var temp = (gridSel == 1 ? $scope.gridDetailLoc.data : $scope.gridDetail.data);
            if(gridSel == 1){
                var idx = _.findIndex(temp, { "WarehouseId": selected.WarehouseId, "StockPositionId": selected.StockPositionId });
                if(idx > -1){
                    $scope.gridDetailLoc.data.splice(idx, 1);
                }
            }else{
                if (temp.length > 1) {
                    _.pullAllWith(temp, [selected], _.isEqual);
                } else {
                    var temp = _.keys(selected);
                    for (var i in temp) {
                        selected[temp[i]] = null;
                    }
                }
            }
        }

        $scope.doCustomSave = function(mdl, mode) {
            console.log("inside before save");
            console.log("mdl : " + JSON.stringify(mdl));
            console.log("mode : " + mode);
            var grdData = $scope.gridDetail.data;
            var grdLocData = $scope.gridDetailLoc.data;
            var svObj = {
                "UsePartParamMIP": 0,
                "UsePartMinMIP": 0,
            };
            var lastArr = [];
            var lastLocArr = [];

            for (var i = 0; i < grdData.length; i++) {
                var temp = {};

                if(grdData[i].UomId != undefined && grdData[i].UomId != null ){

                    var temp = {};
                    temp.Id = (grdData[i]["Id"] == null ? 0 : grdData[i]["Id"]);
                    temp.PartsId = (mode == 'create' ? 0 : mdl.PartsId);
                    temp.UomId = grdData[i].UomId;
                    temp.Qty = grdData[i].Qty;
                    lastArr.push(temp);
            }
        }
            for (var i = 0; i < grdLocData.length; i++) {
                if((grdLocData[i].WarehouseId != undefined && grdLocData[i].ShelfId != undefined) && (grdLocData[i].ShelfId != null && grdLocData[i].WarehouseId != null )){
                    console.log("ayam geprek gapake sambel",grdLocData[i].StockPositionId)
                var temp = {};
                //temp.StockPositionId = (mode == 'create' ? 0 : grdLocData[i]["StockPositionId"]);
                temp.StockPositionId = (mode == 'create' || grdLocData[i]["StockPositionId"] == null ? 0 : grdLocData[i]["StockPositionId"]);
                //temp.PartId = (mode == 'create' ? 0 : grdLocData[i]["PartId"]) ;
                temp.PartId = (mode == 'create' ? 0 : mdl.PartsId);
                temp.WarehouseId = grdLocData[i].WarehouseId;
                temp.QtyOnHand = grdLocData[i].QtyOnHand;
                temp.QtyOnOrder = grdLocData[i].QtyOnOrder;
                temp.QtyOrder = grdLocData[i].QtyOrder;
                temp.QtyOnTransferOrder = grdLocData[i].QtyOnTransferOrder;
                temp.QtyReserved = grdLocData[i].QtyReserved;
                temp.QtyBlocked = grdLocData[i].QtyBlocked;
                temp.ShelfId = grdLocData[i].ShelfId;
                console.log('User',  $scope.user);
                temp.OutletId =  $scope.user.OutletId;
                lastLocArr.push(temp);
                }
            }
            console.log("wowo",lastArr );
            console.log("wowo",lastLocArr );

            svObj.PartsUoM = lastArr;
            svObj.APart_StockPosition_TT = lastLocArr;

            console.log('svObj.PartsUoM doCustomSave', svObj.PartsUoM, svObj);
            console.log('svObj.APart_StockPosition_TT doCustomSave', svObj.APart_StockPosition_TT, svObj);

           
            if (mode == 'create') {
                svObj.PartsId = 0;
                var lastSave = Object.assign(svObj, mdl);

                console.log('svObj', svObj);
                console.log('mdl', mdl);
                console.log("lastSave Create : ", lastSave);
                
                Parts.create(lastSave).then(function(res) {



                    $scope.gridDetail.data = [];
                    $scope.gridDetailLoc.data = [];
                    $scope.getData({PartsName:null, PartsCode:null});
                });
            } else {
                mdl.PartsUoM = lastArr;
                mdl.APart_StockPosition_TT = lastLocArr;
                var lastSave = Object.assign(svObj, mdl);
                console.log("lastSave Update : ", lastSave);
                lastSave.StatusCode = 1;
                Parts.update(lastSave).then(function(res) {

                    PartsGlobal.showAlert({
                        title: "Master Parts",
                        content: "Ubah Data berhasil disimpan",
                        type: 'success'
                        });

                    $scope.gridDetail.data = [];
                    $scope.gridDetailLoc.data = [];
                    $scope.getData({PartsName:null, PartsCode:null});
                });
            }
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            useCustomPagination: true,
            useExternalPagination: true,
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 50, 100, 250, 500, 1000],
            // paginationPageSize: 10,
            paginationCurrentPage: 1,
            columnDefs: [
            { name : 'No Part', field : 'PartsCode', width : '13%', pinnedLeft:true },
            { name : 'Nama Part', field : 'PartsName' ,  width : '20%',},
                { name: 'Aktif', field: 'Active',width : '10%' },
                { name: 'Default Vendor', field: 'VendorName', width : '18%' },
                { name: 'Stocking Policy', field: 'StockingPolicy', width : '18%' },
                { name: 'SCC', field: 'SCC', width : '13%'},
                { name: 'ICC', field: 'ICC' , width : '13%'},
                { name: 'Franchise (Report)', field: 'Franchise' ,width : '15%' },
                { name: 'Clasification', field: 'Clasification', width : '12%' },
                // { name : 'Lokasi Gudang', field:'Warehouse' },
                // { name : 'Lokasi Rak', field:'WarehouseShelf' },
                { name: 'Harga Retail', field: 'RetailPrice', width : '15%'},
                { name: 'Landed Cost', field: 'StandardCost' , width : '15%'},
                { name: 'Satuan Dasar', field: 'Satuan' , width : '15%'},
            ],
            onRegisterApi: function(gridApi){
                $scope.formGridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                    console.log("kacauuuuu");
                    $scope.MaterialList_UIGrid_Paging(pageNumber, pageSize);

                });
            }
        };
        $scope.tmpPageSize = 10;
        $timeout(function() {
            $scope.formGridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                console.log("kacauuuuu", pageNumber, pageSize);
                $timeout(function() {
                    // $scope.getTableHeight();
                    $scope.MaterialList_UIGrid_Paging(pageNumber, pageSize);
                },100);
            });
        },5000);
        $scope.MaterialList_UIGrid_Paging = function(pageNumber, pageSize) {
            console.log('aa', pageNumber, pageSize);
            if (pageSize !== $scope.tmpPageSize) {
                $scope.tmpPageSize = angular.copy(pageSize);
                $scope.grid.paginationCurrentPage = 1;
                $scope.getData($scope.mParts);
            } else {
                $scope.getData($scope.mParts);
            }

            // Material.getData(pageNumber, $scope.uiGridPageSize, pageSize)
            // .then(
            //     function(res) {
            //         $scope.grid.data = res.data.rs.Result;
            //         console.log("data=>", res.data.rs);
            //         $scope.loading = false;
            //         return res.data.rs;
            //     }
            // );
        }

        $scope.gridDetailApi = {};
        $scope.gridDetail = {
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus
            columnDefs: [{
                    field: 'UomId',
                    name: 'Satuan Dasar',
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'MasterId',
                    editDropdownOptionsArray: [],
                },
                {
                    name: 'Nilai Konversi',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <input type="number" class="form-control" name="ConversionVal" ng-model="row.entity.Qty" min="1" style="height:50">' +
                        '</div>'
                },
                {
                    field: 'UomTgt',
                    name: 'Satuan Konversi',
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'MasterId',
                    editDropdownOptionsArray: [],
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <button class="ui icon inverted grey button"' +
                        '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        '           onclick="this.blur()"' +
                        '              ng-click="grid.appScope.removeRow(2,row.entity)">' +
                        '       <i class="fa fa-fw fa-lg fa-times"></i>' +
                        '   </button>' +
                        '</div>'
                }
            ]
        };

        $scope.formApi = {};

        $scope.gridDetail.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridDetailApi = gridApi;

            $scope.gridDetailApi.selection.on.rowSelectionChanged($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
                // console.log("bsform selected=>",scope.selectedRows);
            });
            $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
            });

            $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                // console.log('Inside afterCellEdit grid detail..');
                $scope.checkRow();
            });
        };

        $scope.gridDetailLocApi = {};
        $scope.gridDetailLoc = {
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: true,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus
            columnDefs: [{
                    field: 'WarehouseId',
                    name: 'Gudang',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'WarehouseName',
                    editDropdownIdLabel: 'WarehouseId',
                    editDropdownOptionsArray: [],
                },
                {
                    field: 'ShelfId',
                    name: 'Rak',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'ShelfName',
                    editDropdownIdLabel: 'ShelfId',
                    // editDropdownOptionsArray: [],
                    editDropdownRowEntityOptionsArrayPath: 'ShelfList',
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <button class="ui icon inverted grey button"' +
                        '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        '           onclick="this.blur()"' +
                        '              ng-click="grid.appScope.removeRow(1,row.entity)">' +
                        '       <i class="fa fa-fw fa-lg fa-times"></i>' +
                        '   </button>' +
                        '</div>'
                }
            ]
        };

        $scope.gridDetailLoc.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridDetailLocApi = gridApi;

            $scope.gridDetailLocApi.selection.on.rowSelectionChanged($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailLocRows = $scope.gridDetailLocApi.selection.getSelectedRows();
                // console.log("bsform selected=>",scope.selectedRows);
            });
            $scope.gridDetailLocApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailLocRows = $scope.gridDetailLocApi.selection.getSelectedRows();
            });

            $scope.gridDetailLocApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                // console.log('Inside afterCellEdit grid detail..');
                if (colDef.name == "Gudang") {
                    // $scope.setShelf(rowEntity);
                    rowEntity.ShelfList = _.find($scope.whData, {
                        WarehouseId: rowEntity.WarehouseId,
                    }).WarehouseShelf;
                    console.log("rowEntity.ShelfList : ", rowEntity.ShelfList);
                };
                $scope.checkLocRow();
            });
        };
    }).filter('griddropdown', function() {
        return function(input, xthis) {
            console.log("filter..");
            console.log(input);
            console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        }
                    }
                }
            }

            return '';
        };
    }).filter('griddropdownloc', function() {
        return function(input, xthis) {
            console.log('griddropdownloc', xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    // console.log(xthis.$parent.$parent.row.entity);
                    var xmap = xthis.col.colDef.editDropdownRowEntityOptionsArrayPath;
                    var map = xthis.$parent.$parent.row.entity[xmap];
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    if (map !== undefined) {
                        for (var i = 0; i < map.length; i++) {
                            console.log(map[i]);
                            if (map[i][idField] == input) {
                                return map[i][valueField];
                            }
                        }
                    } else {
                        return '';
                    }
                }
            }

            return '';
        };
    });
