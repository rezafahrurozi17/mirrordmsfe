angular.module('app')
  .factory('Parts', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      // getData: function(type) {
      //   // var defer = $q.defer(); 
      //   //     defer.resolve(
      //   //       {
      //   //         "data" : {
      //   //           "Result":[{
      //   //               "PartsCode": 'P0001',
      //   //               "PartsName": "Part satu",
      //   //               "Active": true,
      //   //               "VendorName": "Vendor Name",
      //   //               "StockingPolicy": "Stok",
      //   //               "SCC": "Overhaul",
      //   //               "ICC": "A - Very Fast Moving",
      //   //               "Franchise": "C - Material",
      //   //               "Clasification": "Toyota Genuine Material",
      //   //               "Warehouse": "Gudang",
      //   //               "WarehouseShelf": "Rak",
      //   //               "RetailPrice": 12000,
      //   //               "StandardCost": 13000,
      //   //               "Satuan": "piece"
      //   //           }],
      //   //           "Start":1,
      //   //           "Limit":10,
      //   //           "Total":1
      //   //         }
      //   //       }
      //   //     );
      //   // return defer.promise;
      //   var res=$http.get('/api/as/MstParts/GetMaterialPartsAll?type='+type);
      //   return res;
      // },
      getData: function(start, itemPerPage, mParts) {
        return $http.get('/api/as/MstParts/GetMaterialPartsAll', {
          params: {
            type: 1,
            startPage: start,
            indexPage: itemPerPage,
            PartsName : ((mParts.PartsName == null || mParts.PartsName == "") ? "-" : mParts.PartsName),
            PartsCode : ((mParts.PartsCode == null || mParts.PartsCode == "") ? "-" : mParts.PartsCode)
          }
        });
      },

      getDataByString: function(param) {      
        var xparam = '/api/as/MstParts/GetMaterialPart?';
        if (param.PartsCode != '' && param.PartsCode != null) {
          xparam+='PartsCode='+param.PartsCode;
        }
        // if (param.PartsName != '' && param.PartsName != null) {
        //   xparam+='PartsName='+param.PartsName;
        // }
        var res=$http.get(xparam);
        return res;
      },
      create: function(Parts) {
        console.log("Data Parts : ",Parts);
        return $http.post('/api/as/MstParts', [Parts]);        
      },
      update: function(Parts){
        console.log("Data Parts : ",Parts);
        return $http.put('/api/as/MstParts', [Parts]);
      },
      delete: function(id) {
        console.log("Data Parts : ",id);
        return $http.delete('/api/as/MstParts',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //