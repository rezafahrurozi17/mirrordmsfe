angular.module('app')
  .factory('Region', function($http, CurrentUser, bsTransTree) {
    if(_myCache==undefined)
      var _myCache = [];
    if(_arProv==undefined)
      var _arProv = [];
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/param/Areas?start=1&limit=100');
        return res;
      },
      create: function(procat) {
        return $http.post('/master/region/create', procat);
      },
      update: function(procat){
        return $http.post('/master/region/update', procat);
      },
      delete: function(id) {
        return $http.post('/master/region/delete',{id:id});
      },
      getAreaProvince: function(){
        if(_arProv.length==0){
            res=$http.get('/api/param/Areas/GetAreaProvince/?start=1&limit=1000');
            res.then(function(res){
              _arProv = res.data.Result;
            });
        }else{
          res = {
            then:function(cbOk,cbErr){
              cbOk({data:{Result:angular.copy(_arProv)}});
            }
          }
        }
        return res;
      },
      getDataTree: function() {
        var res = null;
        if(_myCache.length==0){
            res=$http.get('/api/param/IslandCitys/');
            res.then(function(res){
              //console.log("res=>",res.data.Result);
              var treeIn = bsTransTree.translate(res.data.Result, {
                id:['IslandId','ProvinceId','CityRegencyId'],
                title:['IslandName','ProvinceName','CityRegencyName'],
                child:{isChild:true, fld:['listProvinceCity','listCityRegency']}
              });
              _myCache = treeIn;
            });
        }else{
          res = {
            then:function(cbOk,cbErr){
              cbOk({data:{Result:angular.copy(_myCache)}});
            }
          }
        }
        return res;
      },
      //area,province
      getDataArea: function() {
        // var res=$http.get('/master/region');
        var res=$http.get('/api/param/Areas?start=1&limit=100');
        return res;
      },
      getDataProvince: function() {
        var res=$http.get('/api/param/Provinces?start=1&limit=100');
        return res;
      },
      getDataCity: function() {
        var res=$http.get('/api/param/City?start=1&limit=100');
        return res;
      },
    }
  });