//agGrid.initialiseAgGridWithAngular1(angular);
angular.module('app')
    .controller('RegionController', function($scope, $http, CurrentUser, Region,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRegion = null; //Model
    $scope.xRegion = {};
    $scope.xRegion.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.cRegion = Region.getDataTree().then(
            function(res){
                console.log(res);
                $scope.grid.data = [];
                for(row in res.data.Result){
                    res.data.Result[row].$$treeLevel = 0;
                    $scope.grid.data.push(res.data.Result[row]);
                    for(child in res.data.Result[row].child){
                        res.data.Result[row].child[child].$$treeLevel = 1;
                        $scope.grid.data.push(res.data.Result[row].child[child]);
                        for(gchild in res.data.Result[row].child[child].child){
                            res.data.Result[row].child[child].child[gchild].$$treeLevel = 2;
                            $scope.grid.data.push(res.data.Result[row].child[child].child[gchild]);
                        }
                    }
                }
                // $scope.grid.data = res.data;
                console.log($scope.grid.data);
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        showTreeExpandNoChildren: true,
        enableRowHeaderSelection: false,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', visible:false },
            { name:'pid', field:'pid', visible:false },
            { name:'Region Name', field: 'title' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
