angular.module('app')
    .controller('StockingPolicyController', function($scope, $http, CurrentUser, bsNotify, StockingPolicy,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
        $scope.mFilter = [];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mStockingPolicy = null; //Model
    $scope.cStockingPolicy = null; //Collection
    $scope.xStockingPolicy = {};
    $scope.xStockingPolicy.selected=[];

    var dateFormat2='dd/MM/yyyy';
    $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];

    StockingPolicy.getTypeSCC().then(
        function(res){
            console.log("res=>",res);
            $scope.optionsTypeSCC = res.data.Result;
            $scope.date = new Date();
            if ($scope.mFilter.SCC == null || $scope.mFilter.SCC == undefined) {
                $scope.mFilter.SCC = '-';
            }
            var StartDate =  $scope.date.getFullYear() +'-'+ ( $scope.date.getMonth() + 1) +'-'+  $scope.date.getDate();
            var EndDate = $scope.date.getFullYear() +'-'+ ( $scope.date.getMonth() + 1) +'-'+  $scope.date.getDate();
            var scc = $scope.mFilter.SCC;
            StockingPolicy.getData(StartDate, EndDate, scc).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    //$scope.VehicleModelData = res.data.Result;
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        },
        function(err){
            console.log("err=>",err);
        }
    );

    $scope.getData = function() {
        $scope.date = new Date();
        if(($scope.mFilter.startDate == null) || ($scope.mFilter.startDate == undefined) || 
            ($scope.mFilter.endDate == null) || ($scope.mFilter.endDate == undefined)){
            
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Isi Filter Tanggal",
                });
        } else {
            if ($scope.mFilter.SCC == null || $scope.mFilter.SCC == undefined) {
                $scope.mFilter.SCC = '-';
            }
            var StartDate = $scope.mFilter.startDate.getFullYear() +'-'+ ($scope.mFilter.startDate.getMonth() + 1) +'-'+ $scope.mFilter.startDate.getDate();
            var EndDate = $scope.mFilter.endDate.getFullYear() +'-'+ ($scope.mFilter.endDate.getMonth() + 1) +'-'+ $scope.mFilter.endDate.getDate();
            var scc = $scope.mFilter.SCC;
            StockingPolicy.getData(StartDate, EndDate, scc).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    //$scope.VehicleModelData = res.data.Result;
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    };


    $scope.onValidateSave=function(data){
        $scope.EffectiveDate=data.EffectiveDate;
        $scope.ValidThru=data.ValidThru;
        data.EffectiveDate = data.EffectiveDate.getFullYear() + '-' + (data.EffectiveDate.getMonth() +1) + '-' + data.EffectiveDate.getDate();
        data.ValidThru = data.ValidThru.getFullYear() + '-' + (data.ValidThru.getMonth() +1) + '-' + data.ValidThru.getDate();
        if (data.MaxPrice.toString().includes(",")) {
                            data.MaxPrice = data.MaxPrice.split(',');
                            if (data.MaxPrice.length > 1) {
                                data.MaxPrice = data.MaxPrice.join('');
                            } else {
                                data.MaxPrice = data.MaxPrice[0];
                            }
                        }
                        data.MaxPrice = parseInt(data.MaxPrice);
        console.log('data...', data);

        $scope.mFilter.startDate = data.EffectiveDate;
        $scope.mFilter.endDate = data.ValidThru;
        
        console.log('data effDate COntroller', data.EffectiveDate);
        console.log('data.MaxPrice', data.MaxPrice);
        
        return true;
    }
    $scope.changeFormatDate = function(item) {
        var tmpAppointmentDate = item;
        console.log("changeFormatDate item", item);
        tmpAppointmentDate = new Date(tmpAppointmentDate);
        var finalDate
        var yyyy = tmpAppointmentDate.getFullYear().toString();
        var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
        var dd = tmpAppointmentDate.getDate().toString();
        finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        console.log("changeFormatDate finalDate", finalDate);
        return finalDate;
    }

     $scope.givePattern = function(item, event) {
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.mStockingPolicy.MaxPrice = $scope.mStockingPolicy.MaxPrice.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };

    // $scope.stockingData = [{Id:1, Name:"Stock"}];
    // $scope.sccData = [
    //     {Id:1, Name:"Overhaul"},
    //     {Id:2, Name:"Glass"},
    //     {Id:3, Name:"Body"},
    //     {Id:4, Name:"Accessories"},
    //     {Id:5, Name:"CBU"}
    // ];
    
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'id', width:'7%', visible:false },
            { name:'Tanggal Berlaku', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
            { name:'SCC', field:'Description' },
            { name:'Frekuensi', field:'MinFrequency' },
            { name:'Harga', field:'MaxPrice' }
            
        ]
    };
}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
});
 