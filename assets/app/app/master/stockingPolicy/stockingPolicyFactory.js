angular.module('app')
  .factory('StockingPolicy', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    return {
      getData: function(StartDate, EndDate, SccVal) {
        // var defer = $q.defer();
        //     defer.resolve(
        //       {
        //         "data" : {
        //           "Result":[{
                      //   "PartStockingPolicyId": 1,
                      //   "EffectiveDate": "2017-04-06T00:00:00",
                      //   "PartsClassId": 2,
                      //   "Description": "Overhaul",
                      //   "MinFrequency": 3,
                      //   "MaxPrice": 100000
                      // }
        //           ],
        //           "Start":1,
        //           "Limit":10,
        //           "Total":1
        //         }
        //       }
        //     );
        // return defer.promise;
        // var res=$http.get('/api/as/StockingPolicy'); // get all data without param comment because using advance filter
        var res=$http.get('/api/as/StockingPolicy/SCC/'+ SccVal+ '/vFrom/' + StartDate + '/vTo/' + EndDate);
        return res;
      },
      getTypeSCC: function() {
        // var res=$http.get('/api/as/PartsClasses?ClassTypeId=6');
        var res=$http.get('/api/as/PartsClasses/GetPartsClassesFromType?ClassTypeId=3');
        return res;
      },
      create: function(data) {
        console.log("data==>",data);
        data.PartStockingPolicyId = 0;
        data.OutletId = currentUser.OrgId;
        return $http.post('/api/as/StockingPolicy', [data]);
      },
      update: function(data) {        
        data.OutletId = currentUser.OrgId;
        console.log("data==>",data);
        return $http.put('/api/as/StockingPolicy', [data]);
      },
      delete: function(id) {
        console.log("id==>",id);
        return $http.delete('/api/as/StockingPolicy', {data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
