angular.module('app')
    .controller('CustomerFleetControllerLocal', function($scope, $http, CurrentUser, CustomerFleet,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mFleet = null; //Model
    $scope.cFleet = null; //Collection
    $scope.xFleet = {};
    $scope.xFleet.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.cFleet = CustomerFleet.getData().then(
            function(res){
                $scope.grid.data = res.data;
                // console.log("role=>",res.data);
                $scope.fleetData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', width:'7%' , visible: false},
            { name:'code', field:'code' },
            { name:'name', field:'name' },
            { name:'desc',  field: 'desc' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
