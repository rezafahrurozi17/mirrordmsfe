angular.module('app')
  .factory('CustomerFleetLocal', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/master/customerfleet');
        //console.log('res=>',res);
        return res;
      },
      create: function(fleet) {
        return $http.post('/master/customerfleet/create', {
                                            code: fleet.code,
                                            name: fleet.name,
                                            desc: fleet.desc});
      },
      update: function(fleet){
        return $http.post('/master/customerfleet/update', {
                                            id: fleet.id,
                                            code: fleet.code,
                                            name: fleet.name,
                                            desc: fleet.desc});
      },
      delete: function(id) {
        return $http.post('/master/customerfleet/delete',{id:id});
      },
    }
  });