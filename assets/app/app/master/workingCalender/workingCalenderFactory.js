angular.module('app')
.factory('WorkingCalenderFactory', function($http, CurrentUser, $q) {
  var currentUser = CurrentUser.user();

  	return {
    	getData: function() {
      	var res=$http.get('/api/as/WorkingCalender');
     	  return res;
      },
      getDataWorkHours: function(){
        var res=$http.get('/api/as/WorkHours');
        return res;
      },
      getDataWorkScheduleTypes: function(){
        var res=$http.get('/api/as/WorkScheduleTypes');
        return res;
      },
      getDayType: function(){
        var res=$http.get('/api/as/DayType');
        return res;
      },

      getWorkCalender: function(id) {
        var res=$http.get('/api/as/WorkingCalender/GetWorkingCalenderWeek?CalendarId='+id);
        return res;
      },

      getWorkGrid : function (id) {
        var res=$http.get('/api/as/WorkingCalender?CalendarId='+id)
        return res;
      },

      create:function(model){
        console.log("modelllllll di factory",model);
          return $http.post('/api/as/WorkingCalender/PostCalendar',[[{
                              OutletId:currentUser.OrgId,
                              WorkScheduleTypeId:model.WorkScheduleTypeId,
                              Year:model.Year,
                              StartDate:model.StartDate,
                              EndDate:model.EndDate,
                              StatusCode:1,
                              WorkingCalenderDetail:model.WorkingCalenderDetail
                            }],[{
                              Monday:model.Monday,
                              Tuesday:model.Tuesday,
                              Wednesday:model.Wednesday,
                              Thursday:model.Thursday,
                              Friday:model.Friday,
                              Saturday:model.Saturday,
                              Sunday:model.Sunday,
                            }]]);
      },
      GenerateDate:function(model){
          console.log("model",model);
          return $http.put('/api/as/WorkingCalender/GenerateDate',[model]);
      },
      update:function(model){
          return $http.put('/api/as/WorkingCalender/UpdateCalendar',[{
                              WorkingCalenderId:model.WorkingCalenderId,
                              OutletId:currentUser.OrgId,
                              WorkScheduleTypeId:model.WorkScheduleTypeId,
                              Year:model.Year,
                              StartDate:model.StartDate,
                              EndDate:model.EndDate,
                              StatusCode:1,
                              WorkingCalenderDetail:model.WorkingCalenderDetail,
                              WorkingCalenderWeek : model.WorkingCalenderWeek
                            }]);
      },
      delete:function (id) {
        // return $http.delete('/api/as/WorkingCalender/DeleteCalender?id='+id);
        return $http.put('/api/as/WorkingCalender/BulkDeleteCalender',id);

      }
   	}
});
