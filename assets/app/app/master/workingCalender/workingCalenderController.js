angular.module('app')
.controller('WorkingCalenderController', function($scope, $http,HolidayFactory,bsAlert, CurrentUser, WorkingCalenderFactory, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        $scope.getAllparameter();
        $scope.modelDay();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    $scope.minDateASB = new Date(currentDate);
    $scope.modalMode = 'new';
    // $scope.maxDate = new Date(new Date().getFullYear(), 11, 31);

    $scope.mData = {}; //Model
    $scope.mData.checked={"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"Saturday":false,"Sunday":false};
    $scope.xData = {};
    $scope.xData.selected=[];
    $scope.modal_model = {};
    $scope.dataDay = [
        {Id:1,Name:"Senin",NameEng:"Monday"},
        {Id:2,Name:"Selasa",NameEng:"Tuesday"},
        {Id:3,Name:"Rabu",NameEng:"Wednesday"},
        {Id:4,Name:"Kamis",NameEng:"Thursday"},
        {Id:5,Name:"Jumat",NameEng:"Friday"},
        {Id:6,Name:"Sabtu",NameEng:"Saturday"},
        {Id:7,Name:"Minggu",NameEng:"Sunday"}
    ]

    $scope.DataWorkScheduleTypes=[];
    $scope.DataWorkHours=[];
    $scope.DataDayType=[];
    $scope.show_modal = { show: false};
    $scope.isYear = true;
    $scope.TambahLibur = false;
    $scope.isEdit= false;
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.customButtonActionSettingsDetail = [{
                actionType: 'edit',
                title: 'Edit',
                icon: 'fa fa-fw fa-edit',
                color: 'rgb(213,51,55)'
            },
            {
                func: function(row, formScope) {
                    console.log("print");
                },
                title: 'Print',
                icon: 'fa fa-fw fa-print',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    console.log("Test Drive JobId", $scope.mData.JobId);
                    PreDiagnose.TestDrive($scope.mData.JobId)
                        .then(
                            function(res) {
                                console.log("Save");
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                },
                title: 'Test Drive',
                icon: 'fa fa-fw fa-car',
                //rightsBit: 1
            }
        ];
    $scope.DateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.DateOptionsAkhir = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    var dateFormat = 'dd/MM/yyyy';
    $scope.modelDay = function(){
        for(var i =0;i<$scope.dataDay.length;i++){
            $scope.mData.checked[$scope.dataDay[i].NameEng] = false;
            $scope.mData[$scope.dataDay[i].NameEng] = 0;
            console.log("$scope.mData[$scope.dataDay[i].Name]",$scope.mData);
        };
    };
    $scope.getData = function(){
        WorkingCalenderFactory.getData()
        .then(
            function(res){
                gridData = [];
                gridData=res.data.Result;
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = gridData ;
                console.log("role=>",res);
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    };
    $scope.selectedRows = [];

    $scope.onBeforeNewMode = function(row,mode){
        console.log("onBeforeNewMode=>",row,mode,$scope.user.OrgName);
        // ini hrs na ga kepake di pindah ke onshowdetail aja di paling bawah
        // // for(var i =0;i<$scope.dataDay.length;i++){
        // //     row.checked[$scope.dataDay[i].NameEng] = false;
        // //     row[$scope.dataDay[i].NameEng] = 0;
        // //     console.log("$scope.mData[$scope.dataDay[i].Name]",$scope.mData);
        // // };
        // $scope.showAddHoliday = false;
        // $scope.showGenerate = true;
        // $scope.isEdit = false;
        // $scope.TambahLibur = true;
        // $scope.isYear = false;
        // var dateNow = new Date()
        // $scope.mData.Year = parseInt(dateNow.getFullYear());
        // $scope.checkYear($scope.mData.Year)
        // // row.Outlet = $scope.user.OrgName;
        // // console.log("onBeforeNewMode setelah=>",row);
        // $scope.gridGenerate.data = [];
        // // $scope.mData=angular.copy(row);

    };
    $scope.onShowDetail = function(row,mode){
        
        $scope.tglawal = false;
        setTimeout(function() { 
            $scope.tglawal = true;

            if (mode == 'edit' || mode == 'new'){
                $scope.showAddHoliday = true;
                $scope.showGenerate = true;
               
            } else {
                $scope.showAddHoliday = false;
                $scope.showGenerate = false;
            }
    
            console.log("onShowDetail=>",row,mode,$scope.user.OrgName);
            row.checked={"Monday":false,"Tuesday":false,"Wednesday":false,"Thursday":false,"Friday":false,"Saturday":false,"Sunday":false};
            row.Outlet = $scope.user.OrgName;
            console.log("onShowDetail setelah=>",row);
            $scope.gridGenerate.data = [];
            $scope.mData=angular.copy(row);
    
            WorkingCalenderFactory.getWorkCalender(row.WorkingCalenderId).then(function (res) {
              var tempDays = [];
              var checked = [];
              console.log("cek >>",row.checked,$scope.mData.checked,"cek >>>",res.data);
              tempDays = Object.keys(res.data);
              console.log("$scope.mData >>",tempDays);
              for(var i in tempDays){
                console.log(tempDays[i], res.data[tempDays[i]]);
                checked.push({
                  name : tempDays[i],
                  value : res.data[tempDays[i]]
                })
                if(checked[i].value > 0){
                  $scope.mData.checked[checked[i].name] = false;
                }else{
                  $scope.mData.checked[checked[i].name] = true;
                }
                $scope.mData[tempDays[i]] = res.data[tempDays[i]];
              }
    
    
            });
    
            WorkingCalenderFactory.getWorkGrid(row.WorkingCalenderId).then(function (res) {
              var gridDetail = [];
                gridDetail = res.data[0].WorkingCalenderDetail;
              $scope.gridGenerate.data = gridDetail;
              console.log(">>>",res.data[0]);
            });
    
            // $scope.mData=row;

            if (mode == 'new'){
                $scope.showAddHoliday = false;
                $scope.showGenerate = true;
                $scope.isEdit = false;
                $scope.TambahLibur = true;
                $scope.isYear = false;
                var dateNow = new Date()
                $scope.mData.Year = parseInt(dateNow.getFullYear());
                $scope.checkYear($scope.mData.Year)
                // row.Outlet = $scope.user.OrgName;
                // console.log("onBeforeNewMode setelah=>",row);
                $scope.gridGenerate.data = [];
            }

            
        }, 300);

        
    };
    $scope.onBeforeEdit = function(row,mode){
        $scope.isEdit = true;
        $scope.TambahLibur = false;
        $scope.isYear = false;
        // $scope.DateOptions.minDate = $scope.minDateASB;
        setTimeout(function() { 
            $scope.DateOptions.minDate = row.StartDate;
            $scope.DateOptions.maxDate = new Date(row.Year, 11, 31); 
        }, 500);
        
        // $scope.mData.StartDate = row.StartDate;
        // $scope.mData.EndDate = $scope.DateOptions.maxDate;
    };
    $scope.onValidateSave=function(model){  
        console.log("-------", model)
        var countError = 0;
        if(model.WorkingCalenderId == 0 ||model.WorkingCalenderId == undefined ){
            for(var x=0;x<$scope.grid.data.length;x++){            
                if(model.WorkScheduleTypeId == $scope.grid.data[x].WorkScheduleTypeId && model.Year == $scope.grid.data[x].Year){
                    countError++;
                }
            }
        }
        if(countError > 0){
            bsAlert.warning("Data Sudah Ada");
            return false
        }else{
            model.StartDate = model.StartDate.getFullYear() + '-' + (model.StartDate.getMonth()+1) + '-' + model.StartDate.getDate();
            model.EndDate = model.EndDate.getFullYear() + '-' + (model.EndDate.getMonth()+1) + '-' + model.EndDate.getDate();
            console.log("onValidateSave=>",model, model.EndDate);
            if($scope.gridGenerate.data.length > 0){
                console.log("onValidateSave=> $scope.gridGenerate.data",$scope.gridGenerate.data);
                model.WorkingCalenderDetail = angular.copy($scope.gridGenerate.data);

                for (var i=0; i < model.WorkingCalenderDetail.length; i++) {
                    model.WorkingCalenderDetail[i].Date = model.WorkingCalenderDetail[i].Date.getFullYear() + '-' + (model.WorkingCalenderDetail[i].Date.getMonth()+1) + '-' + model.WorkingCalenderDetail[i].Date.getDate();
                }
                // $scope.isYearAkhir = true;
                var obj = {};
                obj.WorkingCalenderWeekId = model.WorkingCalenderWeekId;
                obj.WorkingCalenderId = model.WorkingCalenderId;
                obj.Sunday = model.Sunday;
                obj.Monday = model.Monday;
                obj.Tuesday = model.Tuesday;
                obj.Wednesday = model.Wednesday;
                obj.Thursday = model.Thursday;
                obj.Friday = model.Friday;
                obj.Saturday = model.Saturday;
                obj.StatusCode = 1;
                var arr = [];
                arr.push(obj);
                model.WorkingCalenderWeek = angular.copy(arr);
                return model;
            }else{
                bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                return false;
            }
        }
        
        
    };
    $scope.check = function(model,isi){
        console.log("check Model =>",model,isi);
        var tmpData = $scope.dataDay;
        if(isi !== undefined){
            for(var i=0; i<tmpData.length;i++){
                console.log("tmpData",tmpData[i],model);
                if(tmpData[i].NameEng == model ){
                    if(isi == true){
                        $scope.mData[tmpData[i].NameEng] = 0;
                        console.log("mdata yang",model,"true");
                    }else{
                        $scope.mData[tmpData[i].NameEng] = null;
                        console.log("mdata yang",model,"true");
                    }
                }
            }
        }
        console.log("mData",$scope.mData);
    };
    // $scope.unitDataGrid = {};
    $scope.getAllparameter = function(){
        WorkingCalenderFactory.getDataWorkHours().then(function(res){
            console.log("getDataWorkHours",res.data.Result);
            var tmpRes = res.data.Result;
            _.map(tmpRes, function(val) {
                console.log("valll tipe", val);
                val.finalDesc = val.WorkHourCode+' '+ val.WorkHourStart +' - '+ val.WorkHourEnd;
            })
            $scope.DataWorkHours = tmpRes;
            $scope.gridGenerate.columnDefs[3].editDropdownOptionsArray = angular.copy(tmpRes);
        });
        WorkingCalenderFactory.getDataWorkScheduleTypes().then(function(res){
            console.log("getDataWorkScheduleTypes",res.data.Result);
            $scope.DataWorkScheduleTypes = res.data.Result;
        });
        WorkingCalenderFactory.getDayType().then(function(res){
            console.log("getDayType",res.data.Result);
            var tmpRes = res.data.Result;
            var tmpData= [];
            var tmpDataa=[];
            // $scope.DataDayType = res.data.Result;
            for(var i=0;i<tmpRes.length;i++){
                if(tmpRes[i].DayTypeId !==3){
                    tmpData.push(tmpRes[i]);
                }else{
                    tmpDataa.push(tmpRes[i]);
                }
            }
            // var tmpUnit = $scope.unitData;
            // for (var i = 0; i < tmpUnit.length; i++) {
            //     // var temp = {};
            //     // temp[tmpUnit[i].MasterId] = tmpUnit[i].Name;
            //     // $scope.unitDataGrid.push(temp);
            //     if (!$scope.unitDataGrid[tmpUnit[i].DayTypeId]) {
            //         $scope.unitDataGrid[tmpUnit[i].DayTypeId] = tmpUnit[i].Name;
            //     }
            // }
            $scope.DataDayType = tmpDataa;
            $scope.gridGenerate.columnDefs[2].editDropdownOptionsArray = angular.copy(tmpData);
        });
    };
    $scope.checkYear = function(model){
        var tmpModel = String(model);
        var tmpYear = new Date().getFullYear();
        console.log("model",model,"tmpYear",tmpYear);
        if(model !== undefined){
            if(model == tmpYear){
                console.log("sama");
                $scope.isYear = false;
                $scope.isYearAkhir = true;
                $scope.DateOptions.minDate = $scope.minDateASB;
                $scope.DateOptions.maxDate = new Date(tmpYear, 11, 31);
                $scope.mData.StartDate = $scope.DateOptions.minDate;
                $scope.mData.EndDate = $scope.DateOptions.maxDate;
                console.log("$scope.maxDate",$scope.maxDate);
            }else if(model > tmpYear){
                console.log("lebih besar",model);
                $scope.isYear = true;
                $scope.isYearAkhir = true;
                $scope.DateOptions.maxDate = new Date(model.toString(), 11, 31);
                $scope.DateOptions.minDate = new Date(model, 0, 1);
                $scope.mData.StartDate = $scope.DateOptions.minDate;
                $scope.mData.EndDate = $scope.DateOptions.maxDate;
                console.log("$scope.maxDate",$scope.maxDate);
            }else{
                $scope.isYear = true;
                $scope.isYearAkhir = true;
                $scope.mData.StartDate = null;
                $scope.mData.EndDate = null;
                // return $scope.mData.Year = null
            }
        }
    };
    $scope.addNewDay = function(){
        $scope.show_modal.show = !$scope.show_modal.show;
        $scope.modal_model ={};
    };

    $scope.CounterGenerate = 0;
    $scope.generate = function(data){
        if (data.WorkScheduleTypeId == null || data.WorkScheduleTypeId == undefined || data.WorkScheduleTypeId == ''){
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Silahkan Pilih Tipe Jadwal Kerja",
            });

        } else {
            console.log("data ==== >",data);
            data.StartDate =data.StartDate.getFullYear() + '-' + (data.StartDate.getMonth()+1) + '-' + data.StartDate.getDate();
            data.EndDate =data.EndDate.getFullYear() + '-' + (data.EndDate.getMonth()+1) + '-' + data.EndDate.getDate();
    
    
            $scope.isYear = false;
            $scope.isYearAkhir = false;
            $scope.isEdit = true;
    
            var DataGridx = [];
            var TampunganGenerate1 = [];
            DataGridx = angular.copy($scope.gridGenerate.data);
            console.log('datagridx', DataGridx);
    
            WorkingCalenderFactory.GenerateDate(data).then(function(res){
                console.log("resss========>",res);
                var tmpRes=res.data;
                console.log("tmpRes",res.data);
    
                var StratNewDate = 0;
                var tglAwal = $scope.mData.StartDate.getFullYear() + '-' + ($scope.mData.StartDate.getMonth()+1) + '-' + $scope.mData.StartDate.getDate();
                var tglAkhir = $scope.mData.EndDate.getFullYear() + '-' + ($scope.mData.EndDate.getMonth()+1) + '-' + $scope.mData.EndDate.getDate();
                var posisiAwal = '';
                var posisiAkhir = '';
                console.log('tgl awal', tglAwal);
                console.log('tgl akhir', tglAkhir);
    
                if(DataGridx == null || DataGridx == "" || DataGridx == undefined || DataGridx == 0 ){
                    console.log("data Grid", DataGridx)
                    for(var z=0; z < res.data.length; z++){
                        console.log("banyak data", res.data[z])
                        if(res.data[z].DayTypeId == 3 && res.data[z].WorkHourId == 0){
                            console.log("data yang ke z", res.data[z].DayTypeId, res.data[z].WorkHourId)
                            res.data[z].DayTypeId = 1;
                            res.data[z].WorkHourId = 7;
                        }
                    }
                    
                }
    
                // if ($scope.CounterGenerate > 0) {
    
                    // for (var i=0; i < DataGridx.length; i++){
                    //     var tglInArray = DataGridx[i].Date.getFullYear() + '-' + (DataGridx[i].Date.getMonth()+1) + '-' + DataGridx[i].Date.getDate();
                    //     if (tglInArray == tglAwal){
                    //         posisiAwal = i;
                    //     }
                    //     if (tglInArray == tglAkhir){
                    //         posisiAkhir = i;
                    //     }
                    // }
    
                    // var JmlDeletedArr = tmpRes.length;
    
                    // TampunganGenerate1 = angular.copy(DataGridx);
                    // for(var i=0; i < tmpRes.length; i++){
                    //     TampunganGenerate1.splice(posisiAwal+i, 1, tmpRes[i]);
                    // }
                    // // TampunganGenerate1.splice(posisiAwal, JmlDeletedArr, tmpRes);
    
                    // console.log('posisi awal', posisiAwal);
                    // console.log('posisi akhir', posisiAkhir);
                    // console.log('Tampungan Generate 1', TampunganGenerate1);
                    // $scope.gridGenerate.data = TampunganGenerate1;
    
                // } else if ($scope.CounterGenerate == 0){
                    if(tmpRes.length > 0){
                        $scope.gridGenerate.data = tmpRes;
                    }
                // }
    
                
    
                $scope.CounterGenerate++;
                
    
            });
            console.log(" $scope.gridGenerate.data", $scope.gridGenerate.data);
        }
        
        
    };
    $scope.onDropdownChange = function(ent) {
        console.log("onDropdownChange",ent);
        if(ent.DayTypeId==2){
            return ent.WorkHourId = "";
        }else if(ent.DayTypeId==1){
            return ent.WorkHourId = 1;
        }

    }
    $scope.onDropdownChangeHour = function(ent) {
        console.log("onDropdownChangeHour",ent);

    }
    $scope.WeekEndSave = function(item) {
        console.log('itemSave', item);
        console.log('ds', $scope.mData.StartDate)
        console.log('ds', $scope.mData.EndDate)
        var tglLibur = item.HolidayDate.getTime();
        var tglStart = angular.copy($scope.mData.StartDate.getTime())
        var tglEnd = angular.copy($scope.mData.EndDate.getTime())

        if ((tglLibur >= tglStart) && (tglLibur <= tglEnd)){
            // var date = new Date(item.HolidayDate);
            // item.HolidayDate = new Date(date.setDate(date.getDate()+1));
            item.HolidayDate = item.HolidayDate.getFullYear() + '-' + (item.HolidayDate.getMonth()+1) + '-' + item.HolidayDate.getDate();
            
            HolidayFactory.create(item).then(function(res){
                console.log("ressss",res.data);
                $scope.show_modal.show =!$scope.show_modal.show;
                $scope.generate($scope.mData);
            });
        } else {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Tanggal Hari Libur Tidak Boleh Diluar Periode Kalender",
            });

        }
        
    };
    $scope.WeekEndCancel = function(item) {
        console.log('itemcancel', item);
        $scope.show_modal.show =!$scope.show_modal.show;
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    var customCellTemplate = '<div>'+
            '<select ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\">'+
            '</select>'+
            // '<label ng-if="row.entity.DayTypeId ==3">Hari Libur Nasional</label>'+
            '</div>'
    var customCellTemplatee = '<div class="ui-grid-cell-contents">'+
            '<select ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChangeHour(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\">'+
            '</select>'+
            // '<label ng-if="row.entity.DayTypeId ==3">Hari Libur Nasional</label>'+
            '</div>'
    var tesTemplate = '<div class="ui-grid-cell-contents">'+
    '<label ng-if="row.entity.DayTypeId ==3">Hari Libur Nasional  {{row.entity.DescHoliday?row.entity.DescHoliday   :row.entity.Holiday.HolidayDesc}}</label>'+
    '<label ng-if="row.entity.DayTypeId ==2">Hari Libur Biasa</label>'+
    '<label ng-if="row.entity.DayTypeId ==1">Hari Kerja</label>'+
    '</div>'

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'WorkScheduleTypeId',field:'WorkScheduleTypeId', width:'7%', visible:false },
            { name:'Tahun', field:'Year' },
            { name:'Keterangan ', field:'WorkScheduleTypeDesc'},
        ]
    };

    $scope.gridGenerate = {
        enableSorting: true,
        enableRowSelection: false,
        // multiSelect: true,
        // enableSelectAll: true,
        enableCellEdit: false,
        // showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,
        cellEditableCondition: function($scope) {
            if($scope.col.field == "DayTypeId"){
                console.log("DayTypeId..");
                if($scope.row.entity.DayTypeId !==3){
                    return true;
                }else{
                    return false;
                }
            }else if($scope.col.field == "WorkHourId"){
                if($scope.row.entity.DayTypeId == 1){
                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }
        },
        columnDefs: [
            { name:'WorkScheduleTypeId',field:'WorkScheduleTypeId', width:'7%', visible:false },
            { name:'Tanggal', field:'Date', cellFilter: dateFilter, enableCellEdit: false},
            {
                name: 'Tipe Hari',
                field: 'DayTypeId',
                // editableCellTemplate: 'ui-grid/dropdownEditor',
                enableCellEdit:true,
                editableCellTemplate: customCellTemplate,
                editDropdownValueLabel: 'DayTypeName',
                editDropdownIdLabel: 'DayTypeId',
                editDropdownOptionsArray: [],
                cellFilter: "gridGenerate:this",
                cellTemplate:tesTemplate
            },
            {
                name: 'Jam Kerja',
                field: 'WorkHourId',
                // editableCellTemplate: 'ui-grid/dropdownEditor',
                enableCellEdit:true,
                editableCellTemplate: customCellTemplatee,
                editDropdownValueLabel: 'finalDesc',
                editDropdownIdLabel: 'WorkHourId',
                editDropdownOptionsArray: [],
                cellFilter: "gridGenerate:this"
            },
        ]
    }
}).filter('gridGenerate', function() {
        return function(input, xthis) {
            // console.log("filter..");
            console.log("input",input);
            console.log("xthis",xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            console.log("map[i][idField]",map[i][idField]);
                            return map[i][valueField];
                        }
                    }
                }
            }

            return '';
        };
    });
