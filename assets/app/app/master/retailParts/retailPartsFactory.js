angular.module('app')
  .factory('RetailPartsFactory', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/PartsRetail');
        return res;
      },
      create: function(RetailData) {        
        RetailData.PartPriceRetailId = 0;
        // console.log("Data Retail : ",RetailData);
        return $http.post('/api/as/PartsRetail', [RetailData]);
      },
      update: function(RetailData){
        console.log("Data Retail : ",RetailData);
        return $http.put('/api/as/PartsRetail', [RetailData]);
      },
      delete: function(id) {
        console.log("Data Retail : ",id);
        return $http.delete('/api/as/PartsRetail',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //