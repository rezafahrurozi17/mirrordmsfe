angular.module('app')
    .controller('RetailPartsController', function($scope, $http, CurrentUser, RetailPartsFactory, Parts, GeneralMaster, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRetailParts = null; //Model
    $scope.cRetailParts = null; //Collection
    $scope.xRetailParts = {};
    $scope.xRetailParts.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.validDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    $scope.noResults=false;

    $scope.getPartsByCode = function(code) {
        console.log("ctrl getData=>",code);
        var xcode = {
            PartsCode:code
        }
        var res=Parts.getDataByString(xcode)
        // var res=User.getDataByUserName($scope.mUser.OrgId,username)     
        .then(function(xres){
            $scope.fnd = xres.data.Result.length>0;
            console.log("fnd=>",$scope.fnd);
            console.log("test get : ",xres.data.Result);
            return xres.data.Result;
        });
        return res;
    };
    $scope.onNoResult = function(){
        console.log("onNoResult=>");
    }
    $scope.onGotResult = function(){
        console.log("onGotResult=>");
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {        
        RetailPartsFactory.getData().then(
            function(res){
                console.log(res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    };

    Parts.getData(1).then(
        function(res){
            $scope.partsData = res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );

    GeneralMaster.getData(1).then(
        function(res){
            $scope.UomData = res.data.Result;
            // console.log("$scope.UomData : ",$scope.UomData);
            return res.data;
        }
    );

    $scope.selectMtrl = function(item, model, label) {
        console.log(item, model, label);
        $scope.mRetailParts.PartsName = item.PartsName;
        $scope.mRetailParts.PartsCode = item.PartsCode;
        $scope.mRetailParts.PartId = item.PartsId;
    }

    $scope.numberWithCommas = function(x) {
        console.log(x);
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
  
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Retail Id',field:'RetailId', width:'7%', visible:false },
            { name:'Berlaku Mulai', field:'EffectiveDate', cellFilter: 'custDate' },
            { name:'Berlaku Sampai', field:'ValidThru', cellFilter: 'custDate' },
            { name:'No. Material', field:'PartsCode' },
            { name:'Nama Material', field:'PartsName' },
            { name:'Satuan', field:'UomName' },
            { name:'Harga Retail Parts', field:'RetailPrice', cellFilter: 'custCurrency' },              
        ]
    };
}).filter('custDate', function () {
    return function (x) {
        return (x.getDate()<10?"0"+x.getDate():x.getDate())+"/"+((x.getMonth()+1)<10?"0"+(x.getMonth()+1):(x.getMonth()+1))+"/"+x.getFullYear();
    };
}).filter('custCurrency', function () {
    return function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };
});
