angular.module('app')
  .factory('MasterAreaDealerFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/as/AreaDealer/GetAreaDealer');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getOutlet: function(){
        console.log("get getOutlet");
        var res=$http.get('/api/as/AreaDealer/getOutlet');
        console.log("Hasil Factory : ",res);
        return res;
      },
   
      // create: function(data) {
      //   console.log("Data DP : ",data);
      //   data.Id = 0;
      //   return $http.post('/api/as/AreaDealer', [data]);
      // },
      // update: function(data){
      //   console.log("Data DP : ",data);
      //   return $http.post('/api/as/AreaDealer', [data]);
      // },
      delete: function(areaDealerId) {
         return $http.delete('/api/as/AreaDealer/DeleteAreaDealer',{
             params : {
                 AreaDealerId : areaDealerId
             }
         });
       },
      createOrUpdate: function(data) {
        console.log("Data DP : ",data);
        data.Id = 0;
        return $http.post('/api/as/AreaDealer', [data]);
      },
      getOutletPerArea: function(areaDealerId) {
        // var res=$http.get('/api/as/AreaDealer/GetAreaDealerPerId',{
        var res=$http.get('/api/as/AreaDealer/GetDetailAreaDealer',{
            params:{
                areaDealerId :  areaDealerId
            }
        });
        console.log("Hasil Factory : ",res);
        return res;
      },
    }
  });
