angular.module('app')
.controller('MasterAreaDealerController', function($scope, $http, CurrentUser, MasterAreaDealerFactory,$timeout,ngDialog,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.OutletData = [];
        $scope.FullOutletData = [];
        $scope.gridData=[];
        $scope.hideGrid = 0;
        $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Small");

        $scope.deletedOutletDetail = [];
        $scope.crud_mode = false;

        $scope.getComboboxData();
        $scope.getData();
    });

    $scope.getComboboxData = function(){
        MasterAreaDealerFactory.getOutlet().then(function(res){
            $scope.OutletData = res.data.Result;
            $scope.FullOutletData = $scope.OutletData;
        });
    }


    $scope.refreshOutletData = function(){

        MasterAreaDealerFactory.getOutlet().then(function(res){
            $scope.OutletData = res.data.Result;
            $scope.FullOutletData = $scope.OutletData;

            // $scope.OutletData = angular.copy($scope.FullOutletData);

            for(var i in $scope.gridOutlet.data){
                var indexToSplice = $scope.OutletData.findIndex(function(obj){
                    return obj.OutletId == $scope.gridOutlet.data[i].OutletId;
                });
                $scope.OutletData.splice(indexToSplice,1);
            }
        });

    }

    $scope.onSelectOutlet = function(data){
        $scope.mData.Address = data.Address;
    }
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        MasterAreaDealerFactory.getData().then(
            function(res){
                console.log("get data..", res.data.Result);
                
                $scope.grid.data = res.data.Result;
                $scope.gridDetail.data = [];
            },
            function(err){
                console.log("err=>",err);
            }
        )
    };

    $scope.onBeforeNewMode = function(){
        // $scope.crudState = "100";
        $scope.newAreaDealer();
        $scope.disableView = false;
    }

     $scope.onBeforeEditMode = function(row){
        // $scope.crudState = "100";
        // console.log('woiiiii', row);
        $scope.viewEditAreaDealer(row,true);
    }

    // $scope.onCancel = function(){
    //     console.log('coeg');
    //     $('#layoutContainer_MasterAreaDealer').removeClass("layoutContainer_MasterAreaDealer_Large");
    //     $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Small");
    // }
    $scope.disableView = false;
    $scope.onShowDetail = function(row, mode){
        $scope.hideGrid = 0;
        $scope.selectedRowsOutlet = [];
        $('#layoutContainer_MasterAreaDealer').removeClass("layoutContainer_MasterAreaDealer_Small");

        $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Large");

         console.log("onShowDetail anita", row, mode );

        $scope.flagEdit = false;

        // $scope.mData = {};
        if(mode == 'view'){
            $scope.disableView = true;
        }else{
            $scope.disableView = false;
        }
        // $scope.mData.AreaDealerId = row.AreaDealerId;
        // $scope.mData.AreaDealerName = row.AreaDealerName;
        // $scope.mData.Description = row.Description;
        if (mode != 'new') {
            MasterAreaDealerFactory.getOutletPerArea(row.AreaDealerId).then(function(res){
                $scope.gridOutlet.data = res.data.Result;
                $scope.refreshOutletData();
            });

        }
        

        $scope.crud_mode = true;
    }

    $scope.onSelectRows = function(rows) {
        $('#layoutContainer_MasterAreaDealer').removeClass("layoutContainer_MasterAreaDealer_Large");
        $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Small");
        // $scope.dis = rows.length;
        console.log("test anita");
        console.log("onSelectRows=>", rows);
        $scope.hideGrid = 1;

        // if (mode != 'new') {
            MasterAreaDealerFactory.getOutletPerArea(rows[0].AreaDealerId).then(function(res){
                $scope.gridOutlet2.data = res.data.Result;
                // $scope.refreshOutletData();
            });

        // $scope.filterBtnLabel2 = $scope.grid2Cols[0].name;
        // $scope.filterBtnChange2();

        // }
    }

    $scope.gridTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.grid.data != undefined){
            if($scope.grid.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.grid.data.length < 10){
                return {
                    height: ($scope.grid.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }

       return {height : 200 + "px"};
    };

    $scope.gridDetailTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.gridDetail.data != undefined){
            if($scope.gridDetail.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.gridDetail.data.length < 10){
                return {
                    height: ($scope.gridDetail.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }

       return {height : 200 + "px"};

    };

    $scope.gridOutletTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.gridOutlet.data != undefined){
            if($scope.gridOutlet.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.gridOutlet.data.length < 10){
                return {
                    height: ($scope.gridOutlet.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }
    };

    $scope.newAreaDealer = function(){

        $scope.flagEdit = true;
        $scope.mData = {};
        $scope.gridOutlet.data = [];
        $scope.refreshOutletData();

        $scope.mData.AreaDealerId = 0;

        $scope.crud_mode = true;
    };

    $scope.viewEditAreaDealer = function(gridData,editModeFlag){
        // console.log("viewEditAreaDealer", gridData);

        $scope.flagEdit = editModeFlag;
        $scope.mData = {};

        $scope.mData.AreaDealerId = gridData.AreaDealerId;
        $scope.mData.AreaDealerName = gridData.AreaDealerName;
        $scope.mData.Description = gridData.Description;

        MasterAreaDealerFactory.getOutletPerArea(gridData.AreaDealerId).then(function(res){
            $scope.gridOutlet.data = res.data.Result;
            $scope.refreshOutletData();
        });

        $scope.crud_mode = true;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------

    // var actionTemp =    '<div>'+    
    //                         // '<button class="ui icon inverted grey button"'+
    //                         //         'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                         //         'onclick="this.blur()"'+
    //                         //         'ng-click="grid.appScope.viewEditAreaDealer(row.entity,false)">'+
    //                         //     '<i class="fa fa-fw fa-lg fa-list-alt"></i>'+
    //                         // '</button>'+
    //                         '<button class="ui icon inverted grey button"'+
    //                                 'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 'onclick="this.blur()"'+
    //                                 'ng-click="grid.appScope.viewEditAreaDealer(row.entity,true);">'+
    //                             '<i class="fa fa-fw fa-lg fa-pencil"></i>'+
    //                         '</button>'+
    //                          '<button class="ui icon inverted grey button"'+
    //                                 'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 'onclick="this.blur()"'+
    //                                 'ng-click="grid.appScope.deleteOutletConfirmation(row.entity);">'+
    //                             '<i class="fa fa-fw fa-lg fa-eraser"></i>'+
    //                         '</button>'+
    //                     '</div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 10,
        columnDefs: [
            {name : 'Area ID', field : 'areaId', visible : false},
            { name:'Nama Area', field:'AreaDealerName' },
            /*{name :'Teknisi ID', field : 'teknisiID', width : '7%', visible : false},*/
            { name:'Jumlah Outlet', field:'TotalOutlet' },
            /*{name :'QC ID', field : 'qcIQCd', width : '7%', visible : false},*/
            // { name:'Petugas QC', field:'QC' },
            { name:'Deskripsi', field:'Description' },
            { name:'action', allowCellFocus: false, width:250, pinnedRight:true,
                enableColumnMenu:false,enableSorting: false,enableColumnResizing: false
                
            }
            // { name:'Group Head', field:'GroupHead' }, cellTemplate: actionTemp
      
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridAPIAreaDealer = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
            //     $scope.TaskListGRGrid_Paging(pageNumber);
            // });
            $scope.GridAPIAreaDealer.selection.on.rowSelectionChanged($scope,
                function(row) {
                    //$scope.mJob = row.entity;
                    var count = $scope.GridAPIAreaDealer.selection.getSelectedRows().length;
                    if(count == 0)
                        $scope.gridDetail.data = [];
                    else
                        $scope.getDataOutletAreaDealerDetail(row);
                });
        }
    };

    // var actionTemp2 =    '<div>'+    
    //                         '<button class="ui icon inverted grey button"'+
    //                                 'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 'onclick="this.blur()"'+
    //                                 'ng-click="grid.appScope.viewEditAreaDealer(row.entity,false)">'+
    //                             '<i class="fa fa-fw fa-lg fa-list-alt"></i>'+
    //                         '</button>'+
    //                         '<button class="ui icon inverted grey button"'+
    //                                 'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 'onclick="this.blur()"'+
    //                                 'ng-click="grid.appScope.viewEditAreaDealer(row.entity,true);">'+
    //                             '<i class="fa fa-fw fa-lg fa-pencil"></i>'+
    //                         '</button>'+
    //                     '</div>';

    $scope.gridDetail = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            {name : 'Outlet Area Dealer ID', field : 'OutletId', width : '7%', visible : false},
            // {name:'No', field:'GroupName' },
            {name :'Nama Outlet', field : 'OutletName'},
            { name:'Alamat', field:'OutletAddress' },
            /*
            { 
                name:'action', allowCellFocus: false, width:250, pinnedRight:true,
                enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                cellTemplate: actionTemp2
            }
            */
        ]
    };
    $scope.selectedRowsOutlet = [];
    $scope.gridOutlet = {
            enableSorting: true,
            enableRowSelection: false,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableFiltering: true,
            multiSelect: true,
            enableSelectAll: true,
            // paginationPageSizes: [10, 15, 25, 50, 75, 100],
            // paginationPageSize: 10,

            columnDefs: [
                { name: 'Id Group', field: 'OutletId', visible:false },
                { name: 'Nama Outlet', field: 'OutletName' },
                { name: 'Address', field: 'OutletAddress' },
                // { name: 'Action', cellTemplate: "<a ng-hide='grid.appScope.flagEdit == false' ng-click='grid.appScope.deleteOutletDetail(row.entity)' style='margin-left:10px;'>Delete<a>" }
            ],
            onRegisterApi: function(gridApi) {
                $scope.GridAPIOutlet = gridApi;
                $scope.GridAPIOutlet.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRowsOutlet = [];
                    $scope.selectedRowsOutlet = $scope.GridAPIOutlet.selection.getSelectedRows();
                    // if ($scope.onSelectRowsOutlet) {
                    //     $scope.onSelectRowsOutlet($scope.selectedRows);
                    // }
                });
                $scope.GridAPIOutlet.selection.on.rowSelectionChangedBatch($scope,function(row) {
                    //scope.selectedRows=null;
                    $scope.selectedRowsOutlet = [];
                    $scope.selectedRowsOutlet = $scope.GridAPIOutlet.selection.getSelectedRows();
                });
                // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                //     $scope.TaskListGRGrid_Paging(pageNumber);
                // });
                // $scope.GridAPIOutlet.selection.on.rowSelectionChanged($scope,
                //     function(row) {
                //         //$scope.mJob = row.entity;
                //         var count = $scope.GridAPIAreaDealer.selection.getSelectedRows().length;
                //         if(count == 0)
                //             $scope.gridDetail.data = [];
                //         else
                //             $scope.getDataOutletAreaDealerDetail(row);
                //     });
            },
        };

        // $scope.actDelete = function(selected){
        //     var idx = _.findIndex($scope.gridOutlet.data,function(val){
        //          if(val.OutletId == selected.OutletId){
        //              return val
        //          }
        //     });
        //     $scope.gridOutlet.data.splice(idx,1);
        //     console.log("idx",idx);
        // }
        $scope.actDelete = function(row){
            console.log(row);
            var tmpRow = row;
            for(var i in tmpRow){
                var rowindex = _.findIndex($scope.gridOutlet.data,function(obj){
                    return obj.OutletAreaDealerId == tmpRow[i].OutletAreaDealerId;
                });
                console.log("rowindex",rowindex);
                $scope.gridOutlet.data.splice(rowindex,1);
                if(tmpRow[i].OutletAreaDealerId.toString().indexOf("-") == -1){
                    $scope.deletedOutletDetail.push(tmpRow[i]);
                }
            }

            $scope.refreshOutletData();

            // if(tmpRow.OutletAreaDealerId.toString().indexOf("-") == -1)
            //     $scope.deletedOutletDetail.push(tmpRow);
        }

         $scope.gridOutlet2 = {
            enableSorting: true,
            enableRowSelection: false,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableFiltering: true,
            multiSelect: true,
            // paginationPageSizes: [10, 15, 25, 50, 75, 100],
            // paginationPageSize: 10,

            columnDefs: [
                { name: 'Id Group', field: 'OutletId', visible:false },
                { name: 'Nama Outlet', field: 'OutletName' },
                { name: 'Address', field: 'OutletAddress' }
                // { name: 'Action', cellTemplate: "<a ng-hide='grid.appScope.flagEdit == false' ng-click='grid.appScope.deleteOutletDetail(row.entity)' style='margin-left:10px;'>Delete<a>" }
            ],
            onRegisterApi: function(gridApi) {
                $scope.GridAPIOutlet2 = gridApi;
                // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                //     $scope.TaskListGRGrid_Paging(pageNumber);
                // });
                // $scope.GridAPIOutlet.selection.on.rowSelectionChanged($scope,
                //     function(row) {
                //         //$scope.mJob = row.entity;
                //         var count = $scope.GridAPIAreaDealer.selection.getSelectedRows().length;
                //         if(count == 0)
                //             $scope.gridDetail.data = [];
                //         else
                //             $scope.getDataOutletAreaDealerDetail(row);
                //     });
            },
        };
        

        $scope.deleteOutletDetail = function(row){
            console.log(row);

            var rowindex = $scope.gridOutlet.data.findIndex(function(obj){
                return obj.OutletAreaDealerId == row.OutletAreaDealerId;
            });

            $scope.gridOutlet.data.splice(rowindex,1);

            $scope.refreshOutletData();

            if(row.OutletAreaDealerId.toString().indexOf("-") == -1)
                $scope.deletedOutletDetail.push(row);
        }

        $scope.goBack = function()
        {
            console.log('balik nih');
            $('#layoutContainer_MasterAreaDealer').removeClass("layoutContainer_MasterAreaDealer_Large");
            $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Small");

            $scope.deletedOutletDetail = [];

            $scope.gridOutlet.data = [];
            $scope.refreshOutletData();
            
            $scope.mData = {};

            $scope.getData();
            $scope.crud_mode = false;

        }

        $scope.doCustomSave = function(mdl){
            $('#layoutContainer_MasterAreaDealer').removeClass("layoutContainer_MasterAreaDealer_Large");
            $('#layoutContainer_MasterAreaDealer').addClass("layoutContainer_MasterAreaDealer_Small");
            console.log('tesssss');
            // $scope.mGroupBP.MGroup_Technician_TM = $scope.gridOutlet.data;

            var tempInsertData = [];

            for(var i in $scope.gridOutlet.data){
                if($scope.gridOutlet.data[i].OutletAreaDealerId.toString().indexOf("-") > 0){
                    $scope.gridOutlet.data[i].OutletAreaDealerId = 0;
                    tempInsertData.push($scope.gridOutlet.data[i]);
                }
                // else
                //     delete $scope.gridOutlet.data[i];
            }

            // mdl.MSites_OutletAreaDealer_TR = $scope.gridOutlet.data;
            mdl.MSites_OutletAreaDealer_TR = tempInsertData;
            mdl.MSites_OutletAreaDealer_TR_Delete = $scope.deletedOutletDetail;

            MasterAreaDealerFactory.createOrUpdate(mdl).then(function(res){
                // $scope.deletedOutletDetail = [];
                // $scope.gridOutlet.data = [];
                // $scope.mData = {};
                if(res.Status !== undefined && res.Status != 200)
                    bsNotify.show({
                        title: "Area Dealer ",
                        content: "Data gagal disimpan",
                        type: 'danger'
                    });
                else
                    bsNotify.show({
                        title: "Area Dealer ",
                        content: "Data Berhasil Disimpan",
                        type: 'success'
                    });

                $scope.goBack();
            })
                
        }

        //untuk pas edit
        $scope.getDataOutletAreaDealerDetail = function(row){
            // console.log("coming soon");
            MasterAreaDealerFactory.getOutletPerArea(row.entity.AreaDealerId).then(function(res){
                $scope.gridDetail.data = res.data.Result;
            });
        }

        $scope.guid = function () {
          function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
          }
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
        }

        $scope.addOutlet = function(mData){

            var outletDataIndex = $scope.OutletData.findIndex(function(obj){
                return obj.OutletId == mData.OutletId
            });

            var outletName = $scope.OutletData[outletDataIndex].Name;

            $scope.gridOutlet.data.push({
                OutletAreaDealerId : $scope.guid(),
                OutletId: mData.OutletId ,
                OutletName : outletName ,
                OutletAddress: mData.Address 
            });

            $scope.refreshOutletData();

            delete $scope.mData.OutletId ;
            delete $scope.mData.Address ;
        }

        $scope.deleteOutletConfirmation = function(row){

            $scope.tempAreaOutletName = row[0].AreaDealerName;
            $scope.tempAreaDealerIdDelete = row[0].AreaDealerId;

            // ngDialog.openConfirm({
            //       template:'\
            //           <p>Are you sure you want to delete Area Dealer : {{tempAreaOutletName}}?</p>\
            //           <div class="ngdialog-buttons">\
            //               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //               <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteOutlet();closeThisDialog(0)">Yes</button>\
            //           </div>',
            //       plain: true,
            //       // controller: 'PartsStockOpnameController',
            //       scope: $scope
            // });
        }

        $scope.deleteOutlet = function(){
            MasterAreaDealerFactory.delete($scope.tempAreaDealerIdDelete).then(function(res){
                $scope.getData();
            })
        }
        $scope.onBulkDelete = function(data){
            console.log("daatttaaa",data);
            for(var i in data){
                MasterAreaDealerFactory.delete(data[i].AreaDealerId).then(function(res){
                    $scope.getData();
                })
            }
        }
        // =======Adding Custom Filter==============
        $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
        $scope.GridAPIOutlet = {};
        var x = -1;
        for (var i = 0; i < $scope.gridOutlet.columnDefs.length; i++) {
            if ($scope.gridOutlet.columnDefs[i].visible == undefined && $scope.gridOutlet.columnDefs[i].name !== 'Action') {
                x++;
                $scope.gridCols.push($scope.gridOutlet.columnDefs[i]);
                $scope.gridCols[x].idx = i;
            }
            console.log("$scope.gridCols", $scope.gridCols);
            console.log("$scope.grid.columnDefs[i]", $scope.gridOutlet.columnDefs[i]);
        }
        $scope.filterBtnLabel = $scope.gridCols[0].name;
        $scope.filterBtnChange = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = col.idx + 1;
        };
        $scope.filterBtnChange($scope.gridCols[0]);
        // ====================================

        // =======Adding Custom Filter==============
        $scope.grid2Cols = []; //= angular.copy(scope.grid.columnDefs);
        $scope.GridAPIOutlet2 = {};
        var x = -1;
        for (var i = 0; i < $scope.gridOutlet2.columnDefs.length; i++) {
            if ($scope.gridOutlet2.columnDefs[i].visible == undefined && $scope.gridOutlet2.columnDefs[i].name !== 'Action') {
                x++;
                $scope.grid2Cols.push($scope.gridOutlet2.columnDefs[i]);
                $scope.grid2Cols[x].idx = i;
            }
            console.log("$scope.grid2Cols", $scope.grid2Cols);
            console.log("$scope.grid.2columnDefs[i]", $scope.gridOutlet2.columnDefs[i]);
        }
        $scope.filterBtnLabel2 = $scope.grid2Cols[0].name;
        $scope.filterBtnChange2 = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel2 = col.name;
            $scope.filterCol2Idx = col.idx + 1;
        };
        $scope.filterBtnChange2($scope.grid2Cols[0]);
        // ====================================





});
