angular.module('app')
  .factory('GroupBPFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var res=$http.get('/api/as/GroupBP');// yang lama
        var res=$http.get('/api/as/GroupBP/GetGroupBP');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getForeman: function(){
        console.log("get getForeman");
        // var res=$http.get('/api/as/EmployeeRoles/2');
        var res=$http.get('/api/as/GroupBP/GetForeman');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getTechnician: function(){
        console.log("get getTechnician");
        // var res=$http.get('/api/as/EmployeeRoles/2');
        var res=$http.get('/api/as/GroupBP/GetTechnician');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getStall: function(){
        console.log("get getStall");
        var res=$http.get('/api/as/GroupBP/GetStall');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getQC: function(){
        var res=$http.get('/api/as/EmployeeRoles/5');
        console.log("Hasil Factory : ",res);
        return res;
      },
      // getForeman: function(){
      //   var res=$http.get('/api/as/EmployeeRoles/2');
      //   console.log("Hasil Factory : ",res);
      //   return res;
      // },
      getGroupHead: function(){
        var res=$http.get('/api/as/EmployeeRoles/2');
        console.log("Hasil Factory : ",res);
        return res;
      },
      getTipeGroup: function(){
        var dataTipeGroup = [{
                GroupTypeId : 0,
                GroupTypeDescription : "Eksternal"
            },
            {
                GroupTypeId : 1,
                GroupTypeDescription : "Internal"
            }]

        return dataTipeGroup;
      },
      create: function(data) {
        console.log("Data DP : ",data);
        data.Id = 0;
        return $http.post('/api/as/GroupBP', [data]);
      },
      createUpload: function(data) {
        console.log("Data DP : ",data);
        data.Id = 0;
        return $http.post('/api/as/PostGroupBPUpload', data);
      },
      update: function(data){
        console.log("Data DP : ",data);
        // return $http.put('/api/as/GroupBP', [data]); // sekarang update sama di delete disatuin pake post
        return $http.post('/api/as/GroupBP', [data]);
      },
      delete: function(groupIdFront) {
         // return $http.delete('/api/as/Stall',{data:id,headers: {'Content-Type': 'application/json'}});
         // return $http.delete('/api/as/GroupBP',{
         return $http.delete('/api/as/GroupBP/DeleteGroupBP',{
             params : {
                 groupId : groupIdFront
             }
         });
       },
      getGroupBPPerId: function(groupId) {
        // var res=$http.get('/api/as/GroupBP');// yang lama
        var res=$http.get('/api/as/GroupBP/getGroupBPPerId',{
            params:{
                id :  groupId
            }
        });
        console.log("Hasil Factory : ",res);
        return res;
      },
      getDetailGroupBP: function(groupId) {
        // var res=$http.get('/api/as/GroupBP');// yang lama
        var res=$http.get('/api/as/GroupBP/GetDetailGroupBP',{
            params:{
                GroupId :  groupId
            }
        });
        console.log("Hasil Factory : ",res);
        return res;
      },
      GetDataTechnicianAku : function()
      {
         //var res=$http.get('/api/as/GroupBP/GetGroupBP');
         var res=$http.get('/api/as/GroupBP/GetSemuaData');
         console.log("Hasil Factory GetDataTechnicianAku: ",res);
         return res;
      },
      cekGroupBP: function (GroupId, GroupName, ForemanId) {
        var url  = '/api/as/GroupBP/CekExistingGroupBP?GroupId=' + GroupId + '&GroupName=' + GroupName + '&ForemanId=' + ForemanId;
        var res = $http.get(url);
        return res;
      }
    
    }
  });
 //