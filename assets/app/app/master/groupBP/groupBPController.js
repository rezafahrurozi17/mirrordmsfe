angular.module('app')
    .controller('GroupBPController', function ($scope, $http, CurrentUser, GroupBPFactory, $timeout, ngDialog, bsNotify, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
        $scope.tipeGroupData = [];
        $scope.deletedTechnicianDetail = [];
        $scope.dropedTechnicianDetail = [];
        // $scope.gridTechnician=[];

        $scope.getComboboxData();
        $scope.FilterTechnician();
    });

    $scope.getComboboxData = function(){
        $scope.GroupTypeData = GroupBPFactory.getTipeGroup();

        GroupBPFactory.getForeman().then(function(res){
            $scope.ForemanData = res.data.Result;
        });

        GroupBPFactory.getTechnician().then(function(res){
            $scope.FullTechnicianData = res.data.Result;
            $scope.TechnicianData = res.data.Result;
        });

        GroupBPFactory.getStall().then(function(res){
            $scope.StallData = res.data.Result;
        });



    }
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mGroupBP = null; //Model
    $scope.cGroupBP = null; //Collection
    $scope.xGroupBP = {};
    $scope.xGroupBP.selected=[];
    $scope.formApi={};
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        console.log("get data..");
        GroupBPFactory.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                console.log("data=>",res.data);
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        )
          GroupBPFactory.GetDataTechnicianAku().then(
            function(res){
                $scope.gridTechnicianAku.data = res.data.Result;                
                console.log("data GetDataTechnicianAku=>",res.data);
                $scope.loading=false;       
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
          )
    };

    $scope.gridTechnicianTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.gridTechnician.data != undefined){
            if($scope.gridTechnician.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.gridTechnician.data.length < 10){
                return {
                    height: ($scope.gridTechnician.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }
    };

    $scope.gridDetailTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.gridDetail.data != undefined){
            if($scope.gridDetail.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.gridDetail.data.length < 10){
                return {
                    height: ($scope.gridDetail.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }
    };




    $scope.techData = [];
    $scope.qcData = [];
    $scope.foreData = [];
    $scope.ghData = [];

    // GroupBPFactory.getTechnician().then(
    //     function(res){
    //         // $scope.techData = res.data.Result;
    //         console.log(res);
    //         $scope.techData.push(res.data);
    //         $scope.loading=false;
    //         return res.data;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );
    // GroupBPFactory.getQC().then(
    //     function(res){
    //         // $scope.qcData = res.data.Result;
    //         $scope.qcData.push(res.data);
    //         console.log($scope.qcData);
    //         $scope.loading=false;
    //         return res.data;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );


    // GroupBPFactory.getForeman().then(
    //     function(res){
    //         // $scope.foreData = res.data.Result;
    //         $scope.foreData.push(res.data);
    //         $scope.loading=false;
    //         return res.data;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );
    // GroupBPFactory.getGroupHead().then(
    //     function(res){
    //         // $scope.ghData = res.data.Result;
    //         $scope.ghData.push(res.data);
    //         $scope.loading=false;
    //         return res.data;
    //     },
    //     function(err){
    //         console.log("err=>",err);
    //     }
    // );


    //----------------------------------
    // Grid Setup
    //----------------------------------

    var actionTemp =    '<div>'+
                            '<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)">'+
                                '<i class="fa fa-fw fa-lg fa-list-alt" title="Lihat"></i>'+
                            '</button>'+
                            '<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-click="grid.appScope.gridClickEditHandler(row.entity);">'+
                                '<i class="fa fa-fw fa-lg fa-pencil" title="Ubah"></i>'+
                            '</button>'+
                             '<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-click="grid.appScope.$parent.deleteGroupconfirmation(row.entity);">'+
                                '<i class="fa fa-fw fa-lg fa-eraser" title="Hapus"></i>'+
                            '</button>'+
                        '</div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // {name : 'Group ID', field : 'Id', visible : false},
            {name : 'Group ID', field : 'OutletId', visible : false},
            { name:'Nama Group', field:'GroupName' },
            /*{name :'Teknisi ID', field : 'teknisiID', width : '7%', visible : false},*/
            { name:'Tipe Group', field:'isInternalName' },
            /*{name :'QC ID', field : 'qcIQCd', width : '7%', visible : false},*/
            // { name:'Petugas QC', field:'QC' },
            { name:'Nama Foreman', field:'ForemanName' },
            { name:'action', allowCellFocus: false, width:250, pinnedRight:true,
                enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                cellTemplate: actionTemp
            }
            // { name:'Group Head', field:'GroupHead' },

        ]
    };

    // $scope.gridDetail = {
    //     enableSorting: true,
    //     enableRowSelection: true,
    //     multiSelect: true,
    //     enableSelectAll: true,
    //     //showTreeExpandNoChildren: true,
    //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //     // paginationPageSize: 15,
    //     columnDefs: [
    //         {name : 'Group Technician Id', field : 'Id', width : '7%', visible : false},
    //         {name : 'Group ID', field : 'OutletId', width : '7%', visible : false},
    //         { name:'Nama Group', field:'GroupName' },
    //         {name :'Nama Teknisi', field : 'EmployeeId', width : '7%', visible : false},
    //         { name:'Inisial', field:'TechInitial' },
    //         /*{name :'QC ID', field : 'qcIQCd', width : '7%', visible : false},*/
    //         { name:'Stall', field:'StallName' },
    //         { name:'StatusAktif', cellTemplate : "<input type='checkbox' ng-model='statusActive'/>" }
    //         // { name:'Stall', field:'StallName' },
    //         // { name:'Group Head', field:'GroupHead' },

    //     ]
    // };
        
    var actionTempTechnician =  '<div>' +
                                    '<button class="ui icon inverted grey button"' +
                                        'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                                        'onclick="this.blur()"' +
                                        'ng-disabled = "view"' +
                                        'ng-click="grid.appScope.deletedGroupTechnicianDetailConfirm(row.entity)">' +
                                        '<i class="fa fa-fw fa-lg fa-eraser"></i>' +
                                    '</button>' +
                                '</div>';

    $scope.gridTechnician = {
            enableSorting: true,
            enableRowSelection: false,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableFiltering: true,
            multiSelect: true,
            // paginationPageSizes: [10, 15, 25, 50, 75, 100],
            // paginationPageSize: 10,

            columnDefs: [
                { name: 'Id Group', field: 'GroupTechnicianId', visible:false },
                // { name: 'Nama Teknisi', field: 'TechnicianName' },
                { name: 'Nama Teknisi', field: 'EmployeeName' },
                { name: 'Inisial Teknisi', field: 'Initial' },
                { name: 'Nama Stall', field: 'StallName' },
                // { name: 'Gender', field: 'Sex',filter: {
                //         type: uiGridConstants.filter.SELECT,
                //         selectOptions: $scope.arrGender,
                //         condition: uiGridConstants.filter.EXACT
                //     }
                // },
                // {
                //   name: 'Birth Date',
                //   field: 'Birth_Date',
                //   cellTooltip: true,
                //   cellFilter: 'date:\'yyyy-MM-dd\'',
                //   cellTemplate: '<div class="ui-grid-cell-contents">{{COL_FIELD | date:"yyyy-MM-dd"}}</div>',
                //   filterHeaderTemplate: $scope.headertemplateuigridinstructor,
                //   width: '40%',
                //   filters: [{
                //         condition: function(term, value, row, column){
                //               if (!term) return true;
                //               var valueDate = new Date(value);
                //               return valueDate >= term;
                //       },placeholder: 'From Date'
                //       },{
                //         condition: function(term, value, row, column){
                //                 if (!term) return true;
                //                 var valueDate = new Date(value);
                //                 return valueDate <= term;
                //       },placeholder: 'To Date'
                //   }],
                //   headerCellClass: $scope.highlightFilteredHeader
                // },
                // { name: 'Inisial Teknisi', field: 'TechInitial' },
                { name: 'Status Aktif', field: 'StatusActive', cellTemplate: '<bscheckbox  ng-model="row.entity.StatusActive" true-value = "1" false-value = "2" ng-disabled="view"></bscheckbox>' },
                //{ name: 'Action', cellTemplate: "<a ng-click='grid.appScope.deleteGroupTechnicianDetail(row.entity)'>delete<a>" } 
                {    
                    name: 'Action', 
                    cellTemplate: actionTempTechnician
                }
                // {
                //     name: 'Action',
                //     cellTemplate: "<div ng-click='deleteTechnicianDetail(row.entity)'>\
                //         <span>delete</span>    \
                //     </div>"
                // }
            ]
        };


        $scope.gridTechnicianAku = {
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableFiltering: true,
            enableSelectAll: false,
            multiSelect: false,
            // paginationPageSizes: [10, 15, 25, 50, 75, 100],
            // paginationPageSize: 10,
            onRegisterApi: function (gridApi) {
                $scope.gridnyaAPI = gridApi;
            },
            columnDefs: [
                {name : 'Group ID', field : 'OutletId', visible : false},
                { name:'Nama Group', field:'GroupName' },
                { name: 'Id Group', field: 'GroupTechnicianId', visible:false },
                // { name: 'Nama Teknisi', field: 'TechnicianName' },
                { name: 'Nama Teknisi', field: 'EmployeeName' },
                { name: 'Inisial Teknisi', field: 'Initial' },
                { name: 'Nama Stall', field: 'StallName' },
                // { name: 'Gender', field: 'Sex',filter: {
                //         type: uiGridConstants.filter.SELECT,
                //         selectOptions: $scope.arrGender,
                //         condition: uiGridConstants.filter.EXACT
                //     }
                // },
                // {
                //   name: 'Birth Date',
                //   field: 'Birth_Date',
                //   cellTooltip: true,
                //   cellFilter: 'date:\'yyyy-MM-dd\'',
                //   cellTemplate: '<div class="ui-grid-cell-contents">{{COL_FIELD | date:"yyyy-MM-dd"}}</div>',
                //   filterHeaderTemplate: $scope.headertemplateuigridinstructor,
                //   width: '40%',
                //   filters: [{
                //         condition: function(term, value, row, column){
                //               if (!term) return true;
                //               var valueDate = new Date(value);
                //               return valueDate >= term;
                //       },placeholder: 'From Date'
                //       },{
                //         condition: function(term, value, row, column){
                //                 if (!term) return true;
                //                 var valueDate = new Date(value);
                //                 return valueDate <= term;
                //       },placeholder: 'To Date'
                //   }],
                //   headerCellClass: $scope.highlightFilteredHeader
                // },
                // { name: 'Inisial Teknisi', field: 'TechInitial' },
                { name: 'Status Aktif', field: 'StatusActive', cellTemplate: '<bscheckbox  ng-model="row.entity.StatusActive" true-value = "1" false-value = "2" ng-disabled="true"></bscheckbox>' },
                // { name: 'Action', cellTemplate: "<a ng-click='grid.appScope.deleteGroupTechnicianDetail(row.entity)'>delete<a>" }
                // {
                //     name: 'Action',
                //     cellTemplate: "<div ng-click='deleteTechnicianDetail(row.entity)'>\
                //         <span>delete</span>    \
                //     </div>"
                // }
            ]
        };

        $scope.deletedGroupTechnicianDetailConfirm = function(row){
            console.log('row confirm', row);

            var rowindex = $scope.gridTechnician.data.findIndex(function (obj) {
                return obj.GroupTechnicianId == row.GroupTechnicianId;
            });

            $scope.rowConfirm = row;
            $scope.tempNameIndex = $scope.gridTechnician.data[rowindex].EmployeeName;

            ngDialog.openConfirm({
                template: '\
                      <p>Apakah anda yakin ingin menghapus Teknisi : {{tempNameIndex}}?</p>\
                      <div class="ngdialog-buttons">\
                          <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                          <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteGroupTechnicianDetail(rowConfirm);closeThisDialog(0)">Yes</button>\
                      </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope: $scope
            });
        }

        $scope.deleteGroupTechnicianDetail = function(row){
            console.log('row',row);
            $scope.dropedTechnicianDetail.push(row);

            console.log('droped', $scope.dropedTechnicianDetail);

            var rowindex = $scope.gridTechnician.data.findIndex(function(obj){
                return obj.GroupTechnicianId == row.GroupTechnicianId;
            });

            $scope.gridTechnician.data.splice(rowindex,1);

            // if(row.GroupTechnicianId.indexOf("-") == -1)
                // $scope.deletedTechnicianDetail.push(row);
            
        }

        $scope.onShowDetail = function(mdl, mode){
            if (mode=="new") {
                $scope.deletedTechnicianDetail = [];
                $scope.dropedTechnicianDetail = [];
                $scope.gridTechnician.data = [];
                $scope.mGroupBP = {};
                $scope.mGroupBP.GroupTypeId = 0;
            }else{
                $scope.getDataGroupBPDetail(mdl);
            }
        }

        $scope.goBack = function()
        {
            $scope.formApi.setMode('grid');
            $scope.getData();
        }

        $scope.onSelectRows = function(row) {
            console.log('row selected', row);
            // tabel on selected row
            $scope.getDataTechnicianMain(row);
        }

        $scope.getDataTechnicianMain = function(row){
            $scope.gridTechnicianAku.data = [];
            console.log('row.group id', row[0].GroupId);
            // GroupBPFactory.getGroupBPPerId(row[0].GroupId).then(function(res){
                // mGroupBP = res.data.Result[0];
                GroupBPFactory.getDetailGroupBP(row[0].GroupId).then(function(res1){
                    for(var i in $scope.gridTechnicianAku.data)
                        $scope.gridTechnicianAku.data[i].StatusActive = $scope.gridTechnicianAku.data[i].StatusCode;

                    $scope.gridTechnicianAku.data = res1.data.Result;
                    // $scope.refreshTechnicianData();
                });
            // });
        }

        $scope.onValidateSave = function(row, mode) {
            console.log("mode onValidateSave : ", row, mode);
            if ($scope.gridTechnician.data.length == 0) {
                console.log("asu",$scope.gridTechnician.data.length)
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Data Detail Harus Diisi Minimal 1 (satu)",
                    timeout: 10000
                });
                return false;
            } else {

                row.MGroup_Technician_TM_Delete = [];
                var tempInsertData = [];
                $scope.copyDataDetail = angular.copy($scope.gridTechnician.data)
                if (row.GroupId!=null){
                    console.log("modenya apa edit");
                    GroupBPFactory.cekGroupBP(row.GroupId, row.GroupName,row.ForemanId).then(function (res) {
                        var resu = res.data
                        console.log('res >>', resu);
                        
                        if (resu[0] == "false") {
                            bsAlert.alert({
                                // title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
                                title: "Nama Group " + row.GroupName + " sudah terdaftar",
                                text: "",
                                type: "warning",
                                showCancelButton: false
                            });
                        } else {
                            for (var i in $scope.gridTechnician.data) {
                            $scope.gridTechnician.data[i].GroupTechnicianId = $scope.gridTechnician.data[i].GroupTechnicianId + "";
                            if ($scope.gridTechnician.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
                                $scope.gridTechnician.data[i].GroupTechnicianId = '0';
                                tempInsertData.push($scope.gridTechnician.data[i]);
                            } else {
                                // if ($scope.gridTechnician.data[i].StatusActive == 1) {
                                //     $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
                                // } else {
                                $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
                                // }
                            }
                            }
                        }
                    });
                }else{
                    console.log("modenya apa CREATE");
                    row.GroupId = 0
                    GroupBPFactory.cekGroupBP(row.GroupId, row.GroupName,row.ForemanId).then(function (res) {
                        var resu = res.data
                        console.log('res >>', resu);
                        
                        if (resu[0] == "false") {
                            bsAlert.alert({
                                // title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
                                title: "Nama Group " + row.GroupName + " sudah terdaftar",
                                text: "",
                                type: "warning",
                                showCancelButton: false
                            });
                        } else {
                            for (var i in $scope.gridTechnician.data) {
                            $scope.gridTechnician.data[i].GroupTechnicianId = $scope.gridTechnician.data[i].GroupTechnicianId + "";
                            if ($scope.gridTechnician.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
                                $scope.gridTechnician.data[i].GroupTechnicianId = '0';
                                tempInsertData.push($scope.gridTechnician.data[i]);
                            } else {
                                $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
                            }
                        }
                        }
                    })
                }
                
                var tech = $scope.gridTechnician.data;
                _.map(tech, function(val){
                    val.StatusCode = val.StatusActive;
                });
                for(var cc in tech){
                    tech[cc].GroupTechnicianId = '0'
                }
                row.MGroup_Technician_TM = tech;
                // mdl.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;
                row.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;

                for (var i=0; i<$scope.dropedTechnicianDetail.length; i++) {
                    for (var j=0; j<tech.length; j++) {
                        $scope.dropedTechnicianDetail[i].GroupTechnicianId = angular.copy(tech[j].GroupTechnicianId)
                    }
                }

                row.MGroup_Technician_TM_Drop = $scope.dropedTechnicianDetail;

                if (mode == 'create') {
                    row.MGroup_Technician_TM_Drop = [];
                }

                console.log("ini hasilnya");
                console.log(tech);
                console.log(row);

                var functionChoice = mode == "create" ? GroupBPFactory.create : GroupBPFactory.update;
                functionChoice(row).then(function(res) {
                    if (res.data == -1){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Nama Foreman Sudah Terdaftar Di Group Lain",
                        });
                        $scope.deletedTechnicianDetail = [];
                        $scope.gridTechnician.data = angular.copy($scope.copyDataDetail)
                        return false;
                    } else {
                        $scope.deletedTechnicianDetail = [];
                        $scope.dropedTechnicianDetail = [];
                        $scope.gridTechnician.data = [];
                        // $scope.refreshTechnicianData();
                        $scope.mGroupBP = {}
                        // $scope.refreshTechnicianData();
                        $scope.goBack();
                        $scope.formApi.setMode('grid');

                        // $scope.goBack();
                        return true;

                    }
                    
                })

            }
        };
        // --- kodingan lama -----
        // $scope.doCustomSave = function(mdl,mode){
        //     // $scope.mGroupBP.MGroup_Technician_TM = $scope.gridTechnician.data;
        //     console.log("$scope.gridTechnician.data", $scope.gridTechnician.data);
        //     $scope.deletedTechnicianDetail = [];
        //     var tempInsertData = [];

        //     if (mdl.GroupId!=null){
        //         console.log("modenya apa edit");
        //         for(var nc in $scope.grid.data){
        //             if($scope.mGroupBP.GroupName == $scope.grid.data[nc].GroupName && $scope.mGroupBP.ForemanId == $scope.grid.data[nc].ForemanId){
        //                 bsAlert.warning("Nama Group Sudah digunakan");
        //                 return false;
        //             }
        //         }

        //         for (var i in $scope.gridTechnician.data) {
        //           $scope.gridTechnician.data[i].GroupTechnicianId = $scope.gridTechnician.data[i].GroupTechnicianId + "";
        //           if ($scope.gridTechnician.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
        //             $scope.gridTechnician.data[i].GroupTechnicianId = 0;
        //             tempInsertData.push($scope.gridTechnician.data[i]);
        //           } else {
        //             // if ($scope.gridTechnician.data[i].StatusActive == 1) {
        //             //     $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
        //             // } else {
        //               $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
        //             // }
        //           }
        //         }
        //     }else{
        //         console.log("modenya apa CREATE");
        //         for(var nc in $scope.grid.data){
        //             if($scope.mGroupBP.GroupName == $scope.grid.data[nc].GroupName && $scope.mGroupBP.ForemanId == $scope.grid.data[nc].ForemanId){
        //                 bsAlert.warning("Nama Group Sudah digunakan");
        //                 return false;
        //             }
        //         }

        //         for (var i in $scope.gridTechnician.data) {
        //           $scope.gridTechnician.data[i].GroupTechnicianId = $scope.gridTechnician.data[i].GroupTechnicianId + "";
        //           if ($scope.gridTechnician.data[i].GroupTechnicianId.toString().indexOf("-") > 0) {
        //             $scope.gridTechnician.data[i].GroupTechnicianId = 0;
        //             tempInsertData.push($scope.gridTechnician.data[i]);
        //           } else
        //             $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
        //         }
                
        //     }



        //     // for(var i in $scope.gridTechnician.data){
        //     //     $scope.gridTechnician.data[i].GroupTechnicianId = $scope.gridTechnician.data[i].GroupTechnicianId + "";
        //     //     if($scope.gridTechnician.data[i].GroupTechnicianId.toString().indexOf("-") > 0){
        //     //         $scope.gridTechnician.data[i].GroupTechnicianId = 0;
        //     //         tempInsertData.push($scope.gridTechnician.data[i]);
        //     //     }
        //     //     else
        //     //         $scope.deletedTechnicianDetail.push($scope.gridTechnician.data[i]);
        //     // }
        //             // delete $scope.gridTechnician.data[i];

        //     // mdl.MGroup_Technician_TM = $scope.gridTechnician.data;
        //     mdl.MGroup_Technician_TM = tempInsertData;
        //     // mdl.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;
        //     mdl.MGroup_Technician_TM_Delete = $scope.deletedTechnicianDetail;
        //     mdl.MGroup_Technician_TM_Drop = $scope.dropedTechnicianDetail;
        //     console.log("ini hasilnya");
        //     console.log($scope.mGroupBP);
        //     console.log(mdl);

        //     var functionChoice = mode == "create" ? GroupBPFactory.create : GroupBPFactory.update;
        //     functionChoice(mdl).then(function(res){
        //         $scope.deletedTechnicianDetail = [];
        //         $scope.dropedTechnicianDetail = [];
        //         $scope.gridTechnician.data = [];
        //         // $scope.refreshTechnicianData();
        //         $scope.mGroupBP = {}
        //         $scope.goBack();
        //     })
        // }

        //untuk pas edit
        $scope.getDataGroupBPDetail = function(row){
            // console.log("coming soon");
            GroupBPFactory.getGroupBPPerId(row.GroupId).then(function(res){
                mGroupBP = res.data.Result[0];
                console.log("get data ", row, mGroupBP, res);
                var stallactive = [];
                GroupBPFactory.getDetailGroupBP(row.GroupId).then(function(res1){
                    console.log("get data detail", row, res1);
                        // for(var i in $scope.gridTechnician.data){
                            // $scope.gridTechnician.data[i].StatusActive = $scope.gridTechnician.data[i].StatusCode;
                            for(var j=0; j<res1.data.Result.length;j++){
                               if(res1.data.Result[j].StatusActive > 0){
                                    stallactive.push(res1.data.Result[j])
                                    $scope.gridTechnician.data = stallactive;
                                    console.log("statuscode", $scope.gridTechnician.data, stallactive)
                               } 
                            }
                            
                        // }
                    // $scope.refreshTechnicianData();
                });
            });
        }

        $scope.guid = function () {
          function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
          }
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
        }

        $scope.addGroupTechnician = function(mData){

            var technicianDataIndex = $scope.TechnicianData.findIndex(function(obj){
                return obj.EmployeeId == mData.TechnicianId;
            });

            var stallDataIndex = $scope.StallData.findIndex(function(obj){
                return obj.StallId == mData.StallId;
            });
            
            var technicianName = $scope.TechnicianData[technicianDataIndex].EmployeeName;
            var stallName = $scope.StallData[stallDataIndex].Name;

            var countStallGroupBP = 0
            for(var k=0;k<$scope.gridTechnician.data.length;k++){
                if(technicianName == $scope.gridTechnician.data[k].EmployeeName && mData.StallId == $scope.gridTechnician.data[k].StallId){
                    countStallGroupBP++;
                }
            }
            if(countStallGroupBP >0){
                bsAlert.warning("Nama Teknisi Sudah digunakam")
                return false;
            }


            $scope.gridTechnician.data.push({
                GroupTechnicianId : $scope.guid() ,
                StallId: mData.StallId ,
                // EmployeeId: mData.EmployeeId ,
                GroupName : mData.GroupName ,
                TechnicianId : mData.TechnicianId ,
                // TechnicianName : technicianName ,
                StallName : stallName,
                EmployeeName : technicianName ,
                // TechInitial: mData.TechInitial ,
                Initial: mData.TechInitial ,
                StatusActive : 1
            });

            // $scope.refreshTechnicianData();

            delete $scope.mGroupBP.TechnicianId;
            delete $scope.mGroupBP.StallId;
            delete $scope.mGroupBP.TechInitial;

            console.log("$scope.gridTechnician.data", $scope.gridTechnician.data);
        }

        //ini fungsi untuk merefresh data di combobox technician supaya tidak terjadi double
        // $scope.refreshTechnicianData = function(){
        //     $scope.TechnicianData = angular.copy($scope.FullTechnicianData);

        //     for(var i in $scope.gridTechnician.data){
        //         var indexToSplice = $scope.TechnicianData.findIndex(function(obj){
        //             return obj.EmployeeId == $scope.gridTechnician.data[i].TechnicianId
        //         })
        //         $scope.TechnicianData.splice(indexToSplice,1);
        //     }
        // }

    // commented by : yap
    // ini button berfungsi untuk tombol delete di gridTechnician
    // dulu ada button delete di gridTechnician sekarang tidak ada lagi
    /*
            $scope.deleteTechnicianDetail = function(row){
                var indexPoppedData = $scope.gridTechnician.data.findIndex(function(obj){
                    return obj.GroupTechnicianId == row.GroupTechnicianId
                });

                var tempData = $scope.gridTechnician.data[indexPoppedData];
                $scope.gridTechnician.data.splice(indexPoppedData,1);
            }
    */
        $scope.getTechnicianInitial = function(selected){
            var tempIndex = $scope.TechnicianData.findIndex(function(obj){
                return obj.EmployeeId == selected.EmployeeId;
            });

            $scope.mGroupBP.TechInitial = $scope.TechnicianData[tempIndex].Initial || "n/a";
        }

        $scope.deleteGroupconfirmation = function(row){

            $scope.tempGroupIdDelete = row.GroupId;

            var tempNameIndex = $scope.grid.data.findIndex(function(obj){
                return obj.GroupId == row.GroupId
            });

            $scope.tempStallNameDelete = $scope.grid.data[tempNameIndex].GroupName;

            bsAlert.alert({
                title: "Apakah Anda yakin ingin menghapus " + $scope.tempStallNameDelete + "?",
                type: "question",
                showCancelButton: true
            },
                function () {
                    $scope.deleteGroup();
                },
                function () { }
            )
            // ngDialog.openConfirm({
            //       template:'\
            //           <p>Are you sure you want to delete stall : {{tempStallNameDelete}}?</p>\
            //           <div class="ngdialog-buttons">\
            //               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //               <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteGroup();closeThisDialog(0)">Yes</button>\
            //           </div>',
            //       plain: true,
            //       // controller: 'PartsStockOpnameController',
            //       scope: $scope
            // });
        }

        $scope.deleteGroup = function(){

            GroupBPFactory.delete($scope.tempGroupIdDelete).then(function(res){
            $scope.getData();
            })
        }
        //=======add 2018-07-10
        //$scope.formApi = {};
        $scope.actionButtonSettings = [{
            title: 'Download',
            icon: 'fas fa-file-upload',
            func: function(row, formScope) {
            var excelData = [];
            var fileName = "GroupBP_Template";
            // var excelSheet2 = {"GroupName":"", "ForemanName":"", "IsGR":"", "GroupType":"", "IsInternal": "", "TechnicianName":"", "StallName":""};
                var excelSheet2 = { "Nama_Grup": "Varchar(50)", "NIP_Foreman": "Varchar(250)", "Tipe_Grup": "External/Internal", "NIP_Teknisi":"Varchar(250)", "Nama_Stall":"Varchar(32)"};
            var excelSheet = {}
            var excelData = [];
            //excelData.push(excelSheet1);
            excelData.push(excelSheet2);
            //excelData.push(excelSheet3);
            //console.log("excel data", excelData);

            //var excelSheetName = ["0", "1", "2"];

            XLSXInterface.writeToXLSX(excelData, fileName);
            //$scope.formApi.setMode('detail');
            },

            type: 'custom' //for link
        },{
          title: 'Upload',
          icon: 'fa-file-upload',
          func: function (row, formScope) {
            angular.element( document.querySelector( '#UploadGroupBP' ) ).val(null);
        		angular.element('#ModalUploadGroupBP').modal('show');
          }
        }];


        $scope.loadXLSGroupBP = function(ExcelFile){
          var myEl = angular.element( document.querySelector( '#UploadGroupBP' ) ); //ambil elemen dari dokumen yang di-upload


          XLSXInterface.loadToJson(myEl[0].files[0], function(json){
              $scope.ExcelData = json;
              console.log("ExcelData : ", $scope.ExcelData);
              console.log("json", json);
                  });
        }


        $scope.UploadGroupBPClicked = function(){
      		var eachData = {};
            var inputData = [];
            var count = 0;

      		angular.forEach($scope.ExcelData, function(value, key){
            console.log('excel Data', $scope.ExcelData);
            console.log('value', value);
            console.log('key', key);
                if (count > 0){
                    eachData = {
                        "Id": 0,
                        "ForemanCode": value.NIP_Foreman,
                        "GroupName": value.Nama_Grup,
                        "GroupTypeName": value.Tipe_Grup,
                        "isGr": 0,
                        "MGroup_Technician_TM":
                            [{
                                "GroupTechnicianId": 0,
                                "StallName": value.Nama_Stall,
                                "TechnicianCode": value.NIP_Teknisi
                            }],
                        "MGroup_Technician_TM_Delete": []
                    }
                    inputData.push(eachData);
                }
      			count++;
              }, inputData);
              console.log('inputData',inputData);
          console.log('cek data upload',eachData);
          GroupBPFactory.createUpload(inputData).then(function (res) {
            angular.element('#ModalUploadGroupBP').modal('hide');
            angular.element( document.querySelector( '#UploadGroupBP' ) ).val(null);
            bsNotify.show({
              size: 'small',
              type: 'success',
              title: "Success",
              content: "Data Berhasil Di Upload"
            });
          })
        //   $scope.getData();
          $scope.goBack();
        }
        
        $scope.FilterTechnician = function () {
            //FILTER TECHNICIAN
            $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
            $scope.gridApiAppointment = {};
            $scope.filterColIdx = 1;
            var x = -1;
            for (var i = 0; i < $scope.gridTechnicianAku.columnDefs.length; i++) {
                if ($scope.gridTechnicianAku.columnDefs[i].visible == undefined && $scope.gridTechnicianAku.columnDefs[i].name !== 'Action') {
                    x++;
                    $scope.gridCols.push($scope.gridTechnicianAku.columnDefs[i]);
                    $scope.gridCols[x].idx = i;
                }
                console.log("$scope.gridCols", $scope.gridCols);
                console.log("$scope.grid.columnDefs[i]", $scope.gridTechnicianAku.columnDefs[i]);
            }
            $scope.filterBtnLabel = $scope.gridCols[0].name;

            $scope.filterBtnChange = function (col) {
                console.log("col", col);
                $scope.filterBtnLabel = col.name;
                $scope.filterColIdx = col.idx + 1;
            };
        }
        
});
