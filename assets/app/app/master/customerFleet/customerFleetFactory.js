angular.module('app')
  .factory('CustomerFleet', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/CustomerFleet/?start=1&limit=100');
        //var res=$http.get('/master/customerfleet');
        //console.log('res=>',res);
        return res;
      },
      create: function(fleet) {
        console.log("create : "+fleet);
        var res=$http.post('/api/ds/CustomerFleet/Multiple', [{
          CustomerCode: fleet.CustomerCode,
          CustomerName: fleet.CustomerName
        }]);
        console.log(res);
        return res;
        // return $http.post('/master/customerfleet/create', {
        //                                     code: fleet.code,
        //                                     name: fleet.name,
        //                                     desc: fleet.desc});
      },
      update: function(fleet){
        console.log("update : "+fleet);
        var res=$http.put('/api/ds/CustomerFleet/Multiple', [{
          Id: fleet.Id,
          CustomerCode: fleet.CustomerCode,
          CustomerName: fleet.CustomerName
        }]);
        return res;
        // return $http.post('/master/customerfleet/update', {
        //                                     id: fleet.id,
        //                                     code: fleet.code,
        //                                     name: fleet.name,
        //                                     desc: fleet.desc});
      },
      delete: function(id) {
        // var res=$http.delete('/api/ds/CustomerFleet/Multiple',[{Id:id}]);
        var aa = [];
        var bb = [];
        
        for (var i = 0; i < id.length ; i++) {
          var cc={};
          cc.Id=id[i];
          aa.push(cc);
        };

        if (aa.length>0) {
          bb=aa;
        } else {
          var cc={};
           cc.Id = id;
           bb.push(cc);
        } 

        return $http.delete('/api/ds/CustomerFleet/Multiple', {data:bb,headers: {'Content-Type': 'application/json'}});
        return res;
        // return $http.post('/master/customerfleet/delete',{id:id});
      },
    }
  });