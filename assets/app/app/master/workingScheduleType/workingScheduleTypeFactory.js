angular.module('app')
.factory('WorkingScheduleTypeFactory', function($http, CurrentUser, $q) {
  var currentUser = CurrentUser.user();

  	return {
    	getData: function() {
      	var res=$http.get('/api/as/WorkScheduleTypes');
     	return res;
        },
        create:function(model){
            return $http.post('/api/as/WorkScheduleTypes',[model]);
        },
        update:function(model){
            return $http.put('/api/as/WorkScheduleTypes',[model]);
        },
        check:function(obj) {
            return $http.put('/api/as/WorkingScheduleType/check',obj);
        },
        delete:function (obj) {
            return $http.put('/api/as/WorkingScheduleType/delete',obj);
        },
        CekIsDefaultSchedule:function (WorkScheduleTypeId) {
            return $http.get('/api/as/WorkingScheduleType/CekdataIsDefaultSchedule/?WorkScheduleTypeId='+ WorkScheduleTypeId );
        }
   	}
});