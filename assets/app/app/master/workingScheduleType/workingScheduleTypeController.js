angular.module('app')
.controller('WorkingScheduleTypeController', function($scope, $http, CurrentUser, WorkingScheduleTypeFactory, $timeout, bsNotify, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected=[];
    $scope.selectedRows=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.dataIsDefaultSchedule = [{ Id: 1, Desc: "Ya" },
            { Id: 0, Desc: "Tidak" }
        ];

    $scope.getData = function(){
        WorkingScheduleTypeFactory.getData()
        .then(
            function(res){
                gridData = [];
                for (var i=0; i<res.data.Result.length; i++){
                    if (res.data.Result[i].isDefaultSchedule == null || res.data.Result[i].isDefaultSchedule == 0 || res.data.Result[i].isDefaultSchedule == undefined){
                        res.data.Result[i].DescIsDefaultSchedule = "Tidak"
                        res.data.Result[i].isDefaultSchedule = 0;
                    } else {
                        res.data.Result[i].DescIsDefaultSchedule = "Ya"
                    }
                }
                gridData=res.data.Result;
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = gridData ;
                console.log("role=>",res);
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    };
    $scope.onBeforeNewMode = function(row,mode){
        console.log("onBeforeNewMode=>",row,mode);
    };
    $scope.onShowDetail = function(row,mode){
        console.log("onShowDetail=>",row,mode);
        $scope.mData=angular.copy(row);
        if ($scope.mData.isDefaultSchedule == undefined || $scope.mData.isDefaultSchedule == null || $scope.mData.isDefaultSchedule == ''){
            $scope.mData.isDefaultSchedule = 0;
            console.log('ga punya')
        }
        $scope.copyisDefaultSchedule = angular.copy($scope.mData.isDefaultSchedule)
    };
    $scope.onValidateSave=function(model, mode){
        console.log("onValidateSave=>",model);
        console.log("onValidateSave=>",mode); //create update
        if (model.isDefaultSchedule == undefined || model.isDefaultSchedule == null || model.isDefaultSchedule == ''){
            model.isDefaultSchedule = 0;
            console.log('ga milih nih')
        }
        var udaAdaDefault = 0;
        for (var i=0; i<$scope.grid.data.length; i++){
            if ($scope.grid.data[i].isDefaultSchedule == 1){
                udaAdaDefault = 1;
            }
        }

        model.isDefaultSchedule = parseInt(model.isDefaultSchedule);
        if (mode == 'create'){
            if (udaAdaDefault == 0 && model.isDefaultSchedule == 0){
                bsAlert.alert({
                    title: "Belum Ada Jadwal Kerja Default",
                    text: "Tetap Simpan?",
                    type: "question",
                    showCancelButton: true
                }, function () {
                    console.log('nasuk sini nih bro bro true 1');
                    WorkingScheduleTypeFactory.create(model).then(function(res){
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data berhasil disimpan"
                                // ,
                                // content: error.join('<br>'),
                                // number: error.length
                        });
                        $scope.getData();
                        $scope.formApi.setMode('grid');
                    })
                }, function () {
                    console.log('nasuk sini nih bro bro false');
                });
            } else {
                WorkingScheduleTypeFactory.create(model).then(function(res){
                    bsNotify.show({
                        size: 'small',
                        type: 'success',
                        title: "Data berhasil disimpan"
                            // ,
                            // content: error.join('<br>'),
                            // number: error.length
                    });
                    $scope.getData();
                    $scope.formApi.setMode('grid');
                })
            }
        } else {
            if (udaAdaDefault == 0 && model.isDefaultSchedule == 0){
                bsAlert.alert({
                    title: "Belum Ada Jadwal Kerja Default",
                    text: "Tetap Simpan?",
                    type: "question",
                    showCancelButton: true
                }, function () {
                    console.log('nasuk sini nih bro bro true 1');
                    WorkingScheduleTypeFactory.update(model).then(function(res){
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data berhasil diubah"
                                // ,
                                // content: error.join('<br>'),
                                // number: error.length
                        });
                        $scope.getData();
                        $scope.formApi.setMode('grid');
                    })
                }, function () {
                    console.log('nasuk sini nih bro bro false');
                });
            } else {
                if($scope.copyisDefaultSchedule != $scope.mData.isDefaultSchedule){
                    WorkingScheduleTypeFactory.CekIsDefaultSchedule(model.WorkScheduleTypeId)
                     .then(
                    function (res) {
                        if (res.data[0] == 'True') {
                            bsAlert.warning("Tidak Dapat Edit", "Harus Ada Default Jadwal Kerja Aktif")
                        }else{
                            WorkingScheduleTypeFactory.update(model).then(function(res){
                                bsNotify.show({
                                    size: 'small',
                                    type: 'success',
                                    title: "Data berhasil diubah"
                                        // ,
                                        // content: error.join('<br>'),
                                        // number: error.length
                                });
                                $scope.getData();
                                $scope.formApi.setMode('grid');
                                })
                            }
                        })
                // WorkingScheduleTypeFactory.update(model).then(function(res){
                //     bsNotify.show({
                //         size: 'small',
                //         type: 'success',
                //         title: "Data berhasil diubah"
                //             // ,
                //             // content: error.join('<br>'),
                //             // number: error.length
                //     });
                //     $scope.getData();
                //     $scope.formApi.setMode('grid');
                // })
                }else{
                    WorkingScheduleTypeFactory.update(model).then(function(res){
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data berhasil diubah"
                                // ,
                                // content: error.join('<br>'),
                                // number: error.length
                        });
                        $scope.getData();
                        $scope.formApi.setMode('grid');
                        })
                }
            }
        }    
        // bsNotify.show({
        //     size: 'small',
        //     type: 'success',
        //     title: "Data berhasil disimpan"
        //         // ,
        //         // content: error.join('<br>'),
        //         // number: error.length
        // });
        // return true;
    };

    $scope.bulkDelete=function(rows) {
        console.log("tes bulk",rows);
        WorkingScheduleTypeFactory.check(rows).then(function(rs) {
            if(rs.data==true){
                bsNotify.show({
                  size: 'small',
                  type: 'warning',
                  title: "Data tidak dapat dihapus",
                  // ,
                  content: "data ini dipakai di tempat lain",
                  // number: error.length
                });
            }else{
                if(rows[0].isDefaultSchedule == 1){
                    bsAlert.warning("Tidak Dapat Hapus" , "Karena Tidak Ada Default Jadwal Kerja Yang Aktif ")
                }else{
                    WorkingScheduleTypeFactory.delete(rows).then(function(res){
                        if (res.data.ResponseCode==33){
                            bsNotify.show({
                              size: 'small',
                              type: 'success',
                              title: "Data berhasil dihapus"
                            });
                        }else{
                            bsNotify.show({
                              size: 'small',
                              type: 'danger',
                              title: "Data gagal dihapus"
                            });
                        }
                        $scope.getData();
                    });
                }
                
            }
        });
    }


    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'WorkScheduleTypeId',field:'WorkScheduleTypeId', width:'7%', visible:false },
            { name:'Kode Tipe Jadwal Kerja', field:'WorkScheduleTypeCode' },
            { name:'Keterangan ', field:'WorkScheduleTypeDesc'},
            { name:'Default Jadwal Kerja ', field:'DescIsDefaultSchedule'},
        ]
    };
});