angular.module('app')
.factory('WorkHourFactory', function($http, CurrentUser, $q) {
  var currentUser = CurrentUser.user();

  	return {
    	getData: function() {
      	var res=$http.get('/api/as/WorkHours');
     	return res;
        },
        create:function(model){
            return $http.post('/api/as/WorkHours',[model]);
        },
        update:function(model){
            return $http.put('/api/as/WorkHours',[model]);
        },
        delete:function(id) {
            return $http.get('/api/as/workhour/deleteWorkHour?id='+id);
        }
   	}
});