angular.module('app')
.controller('WorkHourController', function($scope, $http, CurrentUser, WorkHourFactory, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected=[];
    $scope.formApi = {};

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.toTime = function(){}

    $scope.getData = function(){
        WorkHourFactory.getData()
        .then(
            function(res){
                gridData = [];
                gridData=res.data.Result;
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                console.log("gridData=>",gridData);
                var today= new Date();
                _.map(gridData,function(e){
                    // e.WorkHourStart = ;
                    var workHurStart = new Date("Wed Jan 18 2017 "+e.WorkHourStart+" GMT+0700 (SE Asia Standard Time)");
                    var workHurEnd = new Date("Wed Jan 18 2017 "+e.WorkHourEnd+" GMT+0700 (SE Asia Standard Time)");
                    var workHurStartStr=e.WorkHourStart;
                    var workHurEndStr=e.WorkHourEnd;
                    e.WorkHourStart = workHurStart
                    e.WorkHourEnd = workHurEnd
                    e.WorkHour = workHurStartStr +" - "+workHurEndStr;
                });
                $scope.grid.data = gridData ;
                console.log("role=>",res);
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    };
    $scope.onBeforeNewMode = function(row,mode){
        console.log("onBeforeNewMode=>",row,mode);
    };
    $scope.onShowDetail = function(row,mode){
        console.log("onShowDetail=>",row,mode);
        // $scope.mData=row;
    };
    $scope.onValidateSave=function(model, mode){
        var copyStart = angular.copy(model.WorkHourStart)
        var copyEnd = angular.copy(model.WorkHourEnd)

        if (mode == 'create'){
            model.WorkDay=1;
            var WorkHourStart = new Date(model.WorkHourStart);
            model.WorkHourStart = WorkHourStart.getHours()+":"+WorkHourStart.getMinutes()+":00";

            var WorkHourEnd = new Date(model.WorkHourEnd);
            model.WorkHourEnd = WorkHourEnd.getHours()+":"+WorkHourEnd.getMinutes()+":00";

            console.log("onValidateSave=>",model);

            WorkHourFactory.create(model).then(function(res){
                if (res.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Data Kode Jam Kerja Tersebut Sudah Ada",
                    });
                    model.WorkHourStart = angular.copy(copyStart)
                    model.WorkHourEnd = angular.copy(copyEnd)
                    $scope.formApi.setMode('detail');
                    return false
                } else {
                    bsNotify.show({
                        title: "Success",
                        content: "Data berhasil disimpan",
                        type: 'success'
                    });
                    $scope.formApi.setMode('grid');
                    $scope.getData();
                    return true;
                }
            });  
        } else {
            model.WorkDay=1;
            var WorkHourStart = new Date(model.WorkHourStart);
            model.WorkHourStart = WorkHourStart.getHours()+":"+WorkHourStart.getMinutes()+":00";

            var WorkHourEnd = new Date(model.WorkHourEnd);
            model.WorkHourEnd = WorkHourEnd.getHours()+":"+WorkHourEnd.getMinutes()+":00";

            console.log("onValidateSave=>",model);

            WorkHourFactory.update(model).then(function(res){
                // if (res.data == 666){
                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         title: "Data Kode Jam Kerja Tersebut Sudah Ada",
                //     });
                //     model.WorkHourStart = angular.copy(copyStart)
                //     model.WorkHourEnd = angular.copy(copyEnd)
                //     return false
                // } else {
                    bsNotify.show({
                        title: "Success",
                        content: "Data berhasil diperbaharui",
                        type: 'success'
                    });
                    $scope.formApi.setMode('grid');
                    $scope.getData();
                    return true;
                // }
            });  

        }
    }
    $scope.fFrom = function(){
        var d = new Date($scope.mData.WorkHourEnd);
        d.getHours();
        d.getMinutes();
        $scope.maxFrom = d;
        if($scope.mData.WorkHourStart > $scope.mData.WorkHourEnd){
            $scope.mData.WorkHourStart =  $scope.mData.WorkHourEnd;
        }
    }
    $scope.fThrough =function(){
        var z = new Date($scope.mData.WorkHourStart);
        z.getHours();
        z.getMinutes();
        $scope.minThru = z;
        if($scope.mData.WorkHourEnd < $scope.mData.WorkHourStart){
            $scope.mData.WorkHourEnd = $scope.mData.WorkHourStart;
        }
    }


    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'WorkHourId',field:'WorkHourId', width:'7%', visible:false },
            { name:'Kode Jam Kerja', field:'WorkHourCode' },
            { name:'Jam Kerja', field:'WorkHour' },
        ]
    };
});