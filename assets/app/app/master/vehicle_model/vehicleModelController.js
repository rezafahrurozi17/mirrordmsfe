angular.module('app')
    .controller('VehicleModelControllerLocal', function($scope, $http, CurrentUser, VehicleModel,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVehicleModel = null; //Model
    $scope.cVehicleModel = null; //Collection
    $scope.xVehicleModel = {};
    $scope.xVehicleModel.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        VehicleModel.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                console.log("data=>",res.data);
                $scope.VehicleModelData = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    // var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
    //                                 ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 ' onclick="this.blur()"'+
    //                                 '>'+
    //                                 '<i ng-class="'+
    //                                                   '{ \'fa fa-fw fa-lg fa-toggle-on\' : (row.entity.SpotOrder),'+
    //                                                   '  \'fa fa-fw fa-lg fa-toggle-off\': (!row.entity.SpotOrder),'+
    //                                                   '}'+
    //                                 '">'+
    //                                 '</i>'+
    //                             '</button>';



    var btnActionEditTemplate = '<bscheckbox ng-model="row.entity.SpotOrder"'+
                                            ' ng-disabled="true"'+
                                            ' true-value = "true"'+
                                            ' false-value = "false"'+
                                            ' styles="margin-top:-10px;margin-left:8px;">'+
                                '</bscheckbox>';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Model Id',    field:'VehicleModelId', width:'7%' , visible: false},
            { name:'Model Code', field:'VehicleModelCode', width: '15%' },
            { name:'Model Name', field:'VehicleModelName' },
            { name:'BrandName',  field: 'BrandName' },
            // { name:'Spot Order',  field: 'SpotOrder' },
            { name:'Spot Order', allowCellFocus: false,
                                  cellTemplate: btnActionEditTemplate
            },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
