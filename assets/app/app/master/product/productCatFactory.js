angular.module('app')
  .factory('ProductCat', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/master/productcat');
        return res;
      },
      create: function(procat) {
        return $http.post('/master/productcat/create', procat);
      },
      update: function(procat){
        return $http.post('/master/productcat/update', procat);
      },
      delete: function(id) {
        return $http.post('/master/productcat/delete',{id:id});
      },
    }
  });