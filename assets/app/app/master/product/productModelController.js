angular.module('app')
    .controller('ProductModelController', function($scope, $http, CurrentUser, ProductModel,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPrdModel = null; //Model
    $scope.xPrdModel = {};
    $scope.xPrdModel.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];
    $scope.getData = function() {
        ProductModel.getData().then(
            function(res){
                $scope.grid.data = [];
                for(row in res.data){
                    res.data[row].$$treeLevel = 0;
                    $scope.grid.data.push(res.data[row]);
                    for(child in res.data[row].child){
                        res.data[row].child[child].$$treeLevel = 1;
                        $scope.grid.data.push(res.data[row].child[child]);
                        for(gchild in res.data[row].child[child].child){
                            res.data[row].child[child].child[gchild].$$treeLevel = 2;
                            $scope.grid.data.push(res.data[row].child[child].child[gchild]);
                        }
                    }
                }
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableRowHeaderSelection: false,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', visible:false },
            { name:'pid', field:'pid', visible:false },
            { name:'Model Name', width:'30%', field: 'name' },
            { name:'Description', field: 'desc' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
});
