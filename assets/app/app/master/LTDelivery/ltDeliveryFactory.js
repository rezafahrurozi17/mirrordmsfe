angular.module('app')
    .factory('OrderLeadTime', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/OrderLeadTime');
                return res;
            },
            create: function(OrderLeadTime) {
                console.log("Data DP : ", OrderLeadTime);
                // OrderLeadTime.PartPriceDPId = 0;
                return $http.post('/api/as/OrderLeadTime', OrderLeadTime);
            },
            update: function(OrderLeadTime) {
                console.log("Data DP : ", OrderLeadTime);
                return $http.put('/api/as/OrderLeadTime', OrderLeadTime);
            },
            delete: function(id) {
                console.log("Data DP : ", id);
                return $http.delete('/api/as/OrderLeadTime', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//