angular.module('app')
    .controller('OrderLeadTimeController', function($scope, $http, CurrentUser, OrderLeadTime, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mOrderLeadTime = null; //Model
        $scope.cOrderLeadTime = null; //Collection
        $scope.xOrderLeadTime = {};
        $scope.xOrderLeadTime.selected = [];
        $scope.formApi = {};

        OrderLeadTime.formApi = $scope.formApi;

        $scope.saveObj = {};
        $scope.saveEffDate = { EffectiveDate: null, ValidThru: null };

        var dateFormat = 'yyyy/MM/dd';

        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.POType = [
            {"id":1,"desc":"1"},
            {"id":2,"desc":"2"},
            {"id":3,"desc":"3"},
            {"id":6,"desc":"T"},
            {"id":7,"desc":"C"},
        ];


        //----------------------------------
        // Get Data
        //----------------------------------
        var tempObj = {};
        $scope.getData = function() {
            OrderLeadTime.getData().then(
                function(res) {
                    console.log("data=>", res.data);
                    var temp = res.data.Result;
                    var lastArr = [];
                    if (temp.length > 0) {
                        tempObj = {};
                        for (var i in temp) {
                            var dateKey = new Date(temp[i]["EffectiveDate"]);
                            var key = dateKey.getFullYear() + "/" + (dateKey.getMonth() + 1) + "/" + dateKey.getDate();
                            var dateThruKey = new Date(temp[i]["ValidThru"]);
                            var keyThru = dateThruKey.getFullYear() + "/" + (dateThruKey.getMonth() + 1) + "/" + dateThruKey.getDate();

                            var d = new Date();
                            var cutOff = temp[i].CutOffTime.split(":");
                            d.setHours(cutOff[0]);
                            d.setMinutes(cutOff[1]);
                            temp[i].CutOffTime = d;

                            var h = new Date();
                            var etdH = temp[i].ETDHour.split(":");
                            h.setHours(etdH[0]);
                            h.setMinutes(etdH[1]);
                            temp[i].ETDHour = h;

                            if (!tempObj[key + "-" + keyThru]) {
                                tempObj[key + "-" + keyThru] = [];
                            }
                            tempObj[key + "-" + keyThru].push(temp[i]);
                        }
                        var x = _.keys(tempObj);
                        for (var j in x) {
                            var keyArr = x[j].split("-");
                            lastArr.push({
                                "EffectiveDate": keyArr[0],
                                "ValidThru": keyArr[1],
                            });
                        }
                    }
                    console.log("lastArr : ", lastArr);
                    $scope.grid.data = lastArr;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.beforeNew = function() {
            console.log("before new");
            $scope.gridDetail.data = [];
            $timeout(function () {
                $('.OrderTypeId').attr('ng-disabled', false).attr('skip-enable', false).removeAttr('disabled', 'disabled');
              // }
              // $('.editOPL').hide();
              // $('#rowCustDisableCompleted').find('*').attr('skip-enable', true).attr('disabled', 'disabled').removeAttr('skip-disable');
            }, 100);
            $scope.addNewRow();
        }

        $scope.beforeEdit = function() {
            // $scope.saveEffDate.EffectiveDate = $scope.mOrderLeadTime["EffectiveDate"];
            // $scope.saveEffDate.ValidThru = $scope.mOrderLeadTime["ValidThru"];
            // $scope.gridDetail.data = [];
            // $scope.gridDetail.data = tempObj[$scope.mOrderLeadTime["EffectiveDate"]+"-"+$scope.mOrderLeadTime["ValidThru"]];
            // $scope.addNewRow();
        }
        $scope.onShowDetail = function(model, mode) {
            console.log("Model,mode", model, mode);
            $scope.gridDetail.data.splice(0);
            $scope.mOrderLeadTime = {};
            $scope.saveEffDate = {};
            $scope.saveEffDate.EffectiveDate = model.EffectiveDate;
            $scope.saveEffDate.ValidThru = model.ValidThru;
            $scope.gridDetail.data = angular.copy(tempObj[model.EffectiveDate + "-" + model.ValidThru]);
            console.log("$scope.gridDetail.data", $scope.gridDetail.data);
            if (mode == 'edit') {
                $timeout(function () {
                    $('.OrderTypeId').attr('ng-disabled', false).attr('skip-enable', false).removeAttr('disabled', 'disabled');
                  // }
                  // $('.editOPL').hide();
                  // $('#rowCustDisableCompleted').find('*').attr('skip-enable', true).attr('disabled', 'disabled').removeAttr('skip-disable');
                }, 100);
                $scope.addNewRow();
            }
            // $scope.addNewRow();
        }

        $scope.addNewRow = function() {
            $scope.gridDetail.data.push({
                OrderLeadTimeId: null,
                OrderTypeId: null,
                CutOffTime: null,
                ETDDayPlus: null,
                ETDHour: null,
                LeadTimeArrival: null
            });
        }

        $scope.removeRow = function(selected) {
            var detData = [];
            console.log(selected);

            if ($scope.gridDetail.data.length > 1) {
                _.pullAllWith($scope.gridDetail.data, [selected], _.isEqual);
            } else {
                var temp = _.keys(selected);
                for (var i in temp) {
                    selected[temp[i]] = null;
                }
            }
        }

        $scope.checkRow = function() {
            console.log("ini", $scope.gridDetail.data);
            var findObj = _.find($scope.gridDetail.data, {
                OrderLeadTimeId: null,
                OrderTypeId: null,
                CutOffTime: null,
                ETDDayPlus: null,
                ETDHour: null,
                LeadTimeArrival: null
            });
            if (findObj == undefined) {
                $scope.addNewRow();
            }
            // else {
            //     var temp = _.keys($scope.gridDetail.data[0]);
            //     var x = 0;
            //     for (var i in temp) {
            //         if (temp[i] != null) x++;
            //     }
            //     if (x==0) 
            // }
        };

        $scope.onValidateSave = function(data, mode) {
            console.log("inside onValidateSave save");
            console.log("data : ", data);
            console.log("mode : ", mode);
            var grdDataX = angular.copy($scope.gridDetail.data);
            grdDataX.pop();
            if (grdDataX.length == 0) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Isi Data Master LT Delivery Terlebih Dahulu",
                    timeout: 2000,

                    // content: error.join('<br>'),
                    // number: error.length
                });
                // $scope.formApi.setMode('detail');
                return false;
            } else {
                var flag = 0
                _.map(grdDataX, function(val) {
                    if (val.CutOffTime == null) {
                        flag++;
                    }
                    if (val.OrderTypeId == null) {
                        flag++;
                    }
                    if (val.LeadTimeArrival == null || val.LeadTimeArrival == undefined ) {
                        flag++;
                    }
                    // if (val.LeadTimeArrival == null) {
                    //     flag++;
                    // } boleh 0 loh
                    if (val.ETDHour == null) {
                        flag++;
                    }
                    if (val.ETDDayPlus == null) {
                        flag++;
                    }

                });

                if (flag == 0) {
                    return true;
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Lengkapi Data Master LT Delivery Terlebih Dahulu",
                        timeout: 2000,

                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    return false;
                }
            }

        };

        $scope.doCustomSave = function(mdl, mode) {
            console.log("inside before save");
            console.log("mdl : " + JSON.stringify(mdl));
            console.log("mdl :", mdl);
            console.log("mode : " + mode);
            console.log("$scope.gridDetail.data", $scope.gridDetail.data);
            var grdData = angular.copy($scope.gridDetail.data);
            var lastArr = [];
            $scope.gridDetail.data.pop();
            // if ($scope.gridDetail.data.length > 0) {
            for (var i = 0; i < grdData.length - 1; i++) {
                var temp = {};
                var cutTime = new Date(grdData[i].CutOffTime);
                var time = new Date(grdData[i].ETDHour);
                temp.EffectiveDate = $scope.changeFormatDate($scope.saveEffDate.EffectiveDate);
                temp.ValidThru = $scope.changeFormatDate($scope.saveEffDate.ValidThru);
                console.log(grdData[i].OrderLeadTimeId);
                // temp.OrderLeadTimeId = (grdData[i].OrderLeadTimeId==null?0:grdData[i].OrderLeadTimeId);
                if (grdData[i].OrderLeadTimeId == null || grdData[i].OrderLeadTimeId == undefined) {
                    temp.OrderLeadTimeId = 0;
                } else {
                    temp.OrderLeadTimeId = grdData[i].OrderLeadTimeId;
                };
                temp.OrderTypeId = grdData[i].OrderTypeId;
                temp.CutOffTime = (cutTime.getHours() < 10 ? "0" + cutTime.getHours() : cutTime.getHours()) + ":" + (cutTime.getMinutes() < 10 ? '0' + cutTime.getMinutes() : cutTime.getMinutes());
                // temp.CutOffTime = grdData[i].CutOffTime;
                // temp.ETDHourPlus = grdData[i].ETDHourPlus;
                //yap
                temp.ETDDayPlus = grdData[i].ETDDayPlus;
                temp.ETDHour = (time.getHours() < 10 ? "0" + time.getHours() : time.getHours()) + ":" + (time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes());
                // temp.ETDHour = grdData[i].ETDHour;
                temp.LeadTimeArrival = grdData[i].LeadTimeArrival;
                lastArr.push(temp);
            };
            console.log("lastArr : ", lastArr)
            var flagSave = 0;
            for (var i in lastArr) {
                if (lastArr[i].OrderLeadTimeId == 0) {
                    flagSave++;
                }
            };
            console.log("savenya ngikut", flagSave);
            if (flagSave == 0) {
                OrderLeadTime.create(lastArr).then(function(res) {
                    $scope.gridDetail.data = [];
                    $scope.getData();
                });
            } else {
                OrderLeadTime.update(lastArr).then(function(res) {
                    $scope.gridDetail.data = [];
                    $scope.getData();
                });
            };
            // alert('Data berhasil di simpan');
            bsNotify.show({
                size: 'small',
                type: 'success',
                title: "Data Berhasil Disimpan",
                // content: error.join('<br>'),
                // number: error.length
            });
            // } else {
            //     bsNotify.show({
            //         size: 'big',
            //         type: 'danger',
            //         title: "Mohon Isi Data Master LT Delivery Terlebih Dahulu",
            //         // content: error.join('<br>'),
            //         // number: error.length
            //     });
            //     $scope.formApi.setMode('detail');
            //     return false;

            // };
        }

        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'OrderLeadTimeId', field: 'OrderLeadTimeId', visible: false },
                { name: 'Tanggal Berlaku', field: 'EffectiveDate' },
                { name: 'Berlaku Sampai', field: 'ValidThru' },
            ]
        };

        // var numbCellTemplate =      '<div class="ui-grid-cell-contents">'+
        //                             '   <input type="number" class="form-control" name="OrderTypeId" max="3" min="1" style="height:50">'+
        //                             '</div>';
        // var dateCellTemplate =      '<div class="ui-grid-cell-contents">'+
        //                             '   <div uib-timepicker ng-model="day.timeFrom" show-spinners="false" show-meridian="false"></div>'+
        //                             '</div>';

        $scope.gridDetailApi = {};
        $scope.gridDetail = {
            // rowHeight: 50,
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus   
            columnDefs: [
                { name: 'OrderLeadTimeId', field: 'OrderLeadTimeId', visible: false },
                // { name:'Tipe Order', field: 'OrderTypeId', type: 'number', enableCellEdit: true},
                {
                    name: 'Tipe Order',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents" >'+
                    ' <select class="form-control OrderTypeId" name="OrderTypeId" ng-change="grid.appScope.checkRow()" ng-model="row.entity.OrderTypeId">' +
                        '<option ng-value="1" ng-selected="row.entity.OrderTypeId==1">1</option>' +
                        '<option ng-value="2" ng-selected="row.entity.OrderTypeId==2">2</option>' +
                        '<option ng-value="3" ng-selected="row.entity.OrderTypeId==3">3</option>' +
                        '<option ng-value="6" ng-selected="row.entity.OrderTypeId==6">T</option>' +
                        '<option ng-value="7" ng-selected="row.entity.OrderTypeId==7">C</option>' +
                    ' </select>'+
                    '</div>'
                },

                // { name:'Waktu Cut Off Order', field: 'CutOffTime', type: 'time', enableCellEdit: true},
                {
                    name: 'Waktu Cut Off Order',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <div uib-timepicker ng-change="grid.appScope.checkRow()" ng-model="row.entity.CutOffTime" show-spinners="false" show-meridian="false"></div>' +
                        '</div>'
                },

                // { name:'H+', field: 'ETDHourPlus', type: 'number', enableCellEdit: true },
                {
                    name: 'H+',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        // '   <input type="number" class="form-control" name="ETDHourPlus" ng-change="grid.appScope.checkRow()" ng-model="row.entity.ETDHourPlus" min="0" style="height:50">'+
                        '   <input type="number" class="form-control" name="ETDDayPlus" ng-change="grid.appScope.checkRow()" ng-model="row.entity.ETDDayPlus" min="0" style="height:50">' +
                        '</div>'
                },

                // { name:'Jam', field: 'ETDHour', type: 'number', enableCellEdit: true },
                {
                    name: 'Jam',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <div uib-timepicker ng-change="grid.appScope.checkRow()" ng-model="row.entity.ETDHour" show-spinners="false" show-meridian="false"></div>' +
                        '</div>'
                },

                // { name:'Lead Time Arrival', field: 'LeadTimeArrival', type: 'number', enableCellEdit: true },
                {
                    name: 'Lead Time Arrival',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <input type="number" step="1" class="form-control" name="LeadTimeArrival" ng-change="grid.appScope.checkRow()" ng-model="row.entity.LeadTimeArrival" min="0" style="height:50">' +
                        '</div>'
                },

                {
                    name: 'Action',
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '<button class="ui icon inverted grey button"' +
                        'style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        'onclick="this.blur()"' +
                        'ng-click="grid.appScope.removeRow(row.entity)">' +
                        '<i class="fa fa-fw fa-lg fa-times"></i>' +
                        '</button>' +
                        '</div>'
                }
            ]
        };

        $scope.gridDetail.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridDetailApi = gridApi;

            $scope.gridDetailApi.selection.on.rowSelectionChanged($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
                // console.log("bsform selected=>",scope.selectedRows);
            });
            $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                //scope.selectedRows=null;
                $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
            });

            $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                // console.log('Inside afterCellEdit grid detail..');
                // if (newValue<0 || newValue==null) {
                //     newValue = 0;
                //     rowEntity[colDef.field] = newValue;
                // }
            });
        };
    });