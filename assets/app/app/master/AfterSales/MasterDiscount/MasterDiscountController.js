angular.module('app')
    .controller('MasterDiscountController', function ($scope, $http, CurrentUser, MasterDiscountFactory, $timeout, bsNotify, bsAlert, $q) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.formApi = {};
        
        
        
        $scope.xRole = { selected: [] };
        console.log('$scope.user', $scope.user);

        if ($scope.user.OutletId < 2000000) {
            $scope.OutletGR = $scope.user.OutletId;
        } else if ($scope.user.OutletId >= 2000000){ 
            $scope.OutletBP = $scope.user.OutletId;
        }

        //----------------------------------
        // Get Data test
        //----------------------------------
        $scope.getData = function () {
            MasterDiscountFactory.getData().then(
                function (res) {
                    
                        

                    $scope.MasterDiskon = angular.copy(res.data.Result);
                    $scope.MasterDiskonActive = [];
                    $scope.MasterDiskonNotActive = [];
                    for (var i = 0; i < $scope.MasterDiskon.length; i++) {
                        if($scope.MasterDiskon[i].Category == 1){
                            $scope.MasterDiskon[i].CategoryStr = 'Jasa'
                        } else if($scope.MasterDiskon[i].Category == 2){
                            $scope.MasterDiskon[i].CategoryStr = 'Parts'
                        }
                        if ($scope.MasterDiskon[i].IsActive == true) {
                            //ini yang ditampilin di grid default
                            if ($scope.user.OutletId == $scope.OutletGR) {
                                $scope.MasterDiskonActive.push($scope.MasterDiskon[i]);
                            } else if ($scope.user.OutletId == $scope.OutletBP){
                                $scope.MasterDiskonActive.push($scope.MasterDiskon[i]);
                            }
                        } else if ($scope.MasterDiskon[i].IsActive == false){
                            //ini yang statusnya non aktif
                            if ($scope.user.OutletId == $scope.OutletGR) {
                                $scope.MasterDiskonNotActive.push($scope.MasterDiskon[i]);
                            } else if ($scope.user.OutletId == $scope.OutletBP) {
                                $scope.MasterDiskonNotActive.push($scope.MasterDiskon[i]);
                            }
                        }
                    }

                    var jmlDataDisplay = 10;
                    var jmlMasterDiskonNotActive = $scope.MasterDiskonNotActive.length;
                    var jmlDataDihapus = 0;

                    if(jmlMasterDiskonNotActive > jmlDataDisplay ){
                        jmlDataDihapus = jmlMasterDiskonNotActive - jmlDataDisplay;
                        for (var i = 0; i < jmlDataDihapus; i++) {
                            $scope.MasterDiskonNotActive.pop(10,i);
                        }
                    }


                    $scope.grid.data = $scope.MasterDiskonActive;
                    $scope.disableCategory = true;

                    console.log('ini all data   =======>', $scope.MasterDiskon);
                    console.log('ini data aktif =======>', $scope.MasterDiskonActive);
                    console.log('ini data non aktif ===>', $scope.MasterDiskonNotActive );
                    console.log('$scope.toggleShowData ====>',$scope.toggleShowData);
                    $scope.showAllData($scope.toggleShowData);

                    $scope.loading = false;
                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "gagal",
                            content: "Data tidak ditemukan.",
                            type: 'danger'
                        }
                    );
                }
            );
        }

        //----------------------------------
        //  Master Discount Controller By Galang
        //----------------------------------
        $scope.getData();
        $scope.toggleShowData = true;
        $scope.mDiscount = {}; //Model
        $scope.mDiscount.isActive = false;

        $scope.DiscountCategory = [
            {
                Category: 1,
                CategoryName: "Jasa"
            },
            {
                Category: 2,
                CategoryName: "Parts"
            }
        ];

        $scope.showAllData = function (showAll) {
            $scope.toggleShowData = showAll;
            console.log('$scope.toggleShowData ====>',$scope.toggleShowData);
            if (showAll == true) {
                $scope.grid.data = $scope.MasterDiskonActive;
            }else{
                console.log('$scope.toggleShowData ====>',$scope.toggleShowData);
                $scope.grid.data = $scope.MasterDiskonNotActive
            }
        }

        $scope.onSwitchActive = function(data){
            console.log('switch ===>',data);
        }
        


        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
        $scope.minDateASB = new Date(currentDate);
        $scope.DateOptionsASB = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };


        $scope.actEdit = function (selectedMasterDiscount) {
            $scope.ONmode ='edit';
            // $scope.formApi.setMode('edit');
            console.log('ini pas klik edit',selectedMasterDiscount);
            $scope.disableCategory = true;
            
            if (selectedMasterDiscount.Category == 1) {
                $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" }];
            }else{
                $scope.DiscountCategory = [{ Category: 2, CategoryName: "Parts" }];
            }

            if ($scope.MasterDiskonActive.length > 0) {
                if($scope.MasterDiskonActive.length == 1){

                    for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                        if (($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) || ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP)) {
                            $scope.DiscountCategory = [{ Category: 2, CategoryName: "Parts" }];
                        } else {
                            $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" }];
                        }
                    }
                }else{
                    
                    $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" },{ Category: 2, CategoryName: "Parts" }];                    
                }

            }else{
                $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" },{ Category: 2, CategoryName: "Parts" }];
            }
        }
        

        $scope.actView = function (selectedMasterDiscount) {
            $scope.ONmode ='view';
            // $scope.formApi.setMode('view');
            console.log('ini pas klik view', selectedMasterDiscount);
            if (selectedMasterDiscount.Category == 1) {
                $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" }];
            } else {
                $scope.DiscountCategory = [{ Category: 2, CategoryName: "Parts" }];
            }
            
        }

        $scope.actNew = function () {
            $scope.ONmode ='add';
            $scope.formApi.setMode('new');
            $scope.mDiscount.IsActive = true;
            console.log('ini pas klik tmabah');
            $scope.disableCategory = false;
            $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" }, { Category: 2, CategoryName: "Parts" }];
            console.log("$scope.MasterDiskonActive",$scope.MasterDiskonActive);
            if ($scope.MasterDiskonActive.length > 0) {
                if($scope.MasterDiskonActive.length == 1){

                    for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                        if (($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) || ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP)) {
                            $scope.DiscountCategory = [{ Category: 2, CategoryName: "Parts" }];
                        } else {
                            $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" }];
                        }
                    }
                }else{
                    
                    $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" },{ Category: 2, CategoryName: "Parts" }];                    
                }

            }else{
                $scope.DiscountCategory = [{ Category: 1, CategoryName: "Jasa" },{ Category: 2, CategoryName: "Parts" }];
            }
            
        }
   
        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Appointment Master Diskon',
                text: text || '',
                type: "question",
                showCancelButton: true
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        $scope.formatDate = function (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        };

        $scope.onTypeDiscount = function (DiscountTyping) {
            console.log('typing discount value ===>', DiscountTyping);
            $scope.mDiscount.Discount = angular.copy(DiscountTyping);
        }


        $scope.onToggleSwitch = function (bukanTrue) {
            console.log('$scope.mDiscount.isActive ===>', bukanTrue);
            if (bukanTrue != true) {
                $scope.mDiscount.isActive == false;
            }else{
                $scope.mDiscount.isActive == true;
            }
        }

        
        $scope.onValidateSave = function (data,mode) {
            console.log('ini isinya apa dah? ===>',data);
            if (data.IsActive != true) {
                data.IsActive = false;
                console.log('IsActive harusnya false ===>', data.IsActive);
            }else {
                console.log('IsActive harusnya true ====>', data.IsActive);
            }

            var diskonParts = 0;
            var diskonJasa = 0;
            for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                if ($scope.MasterDiskonActive[i].Category == 1) {
                    diskonJasa += 1;
                } else if ($scope.MasterDiskonActive[i].Category == 2) {
                    diskonParts += 1;
                }
            }
            
            
            for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                var dateStart = $scope.MasterDiskonActive[i].DateFrom
                var dateEnd = $scope.MasterDiskonActive[i].DateTo

                if(mode == 'create'){
                    if ($scope.MasterDiskonActive[i].Category == 1 && data.Category == 1 && $scope.MasterDiskonActive[i].IsActive == true && data.IsActive == true){
                    
                        if(data.DateFrom >= dateStart && data.DateFrom <= dateEnd ){
    
                        bsNotify.show({
                            size: 'big',
                            title: "Master Booking Discount",
                            content: "Terdapat data master diskon booking aktif dengan masa berlaku beririsan. Silakan lakukan perubahan masa berlaku diskon terlebih dahulu, dan pastikan sudah tidak beririsan.",
                            type: 'danger'
                        });
                        return false
                        }
                    }
    
                    if ($scope.MasterDiskonActive[i].Category == 2 && data.Category == 2 && $scope.MasterDiskonActive[i].IsActive == true && data.IsActive == true){
                        
                        if(data.DateFrom >= dateStart && data.DateFrom <= dateEnd ){
    
                        bsNotify.show({
                            size: 'big',
                            title: "Master Booking Discount",
                            content: "Terdapat data master diskon booking aktif dengan masa berlaku beririsan. Silakan lakukan perubahan masa berlaku diskon terlebih dahulu, dan pastikan sudah tidak beririsan.",
                            type: 'danger'
                        });
                        return false
                        }
                    }
                }
               
            }
            console.log('diskon parts sebelumnya ada brp? ===>', diskonParts);
            console.log('diskon Jasa sebelumnya ada brp? ===>', diskonJasa);


            if (data.Category == 1 && diskonJasa == 0 && data.IsActive == true){ //ini kalo diskon jasa nya blm ada
                $scope.dependencySave = "directSave"
                console.log('onValidateSave direct Jasa | category ==>', data.Category + ' diskon jasa ==>', diskonJasa + ' IsActive ==>',data.IsActive);
                return true;
            } else if (data.Category == 2 && diskonParts == 0 && data.IsActive == true) { //ini kalo diskon parts nya blm ada
                $scope.dependencySave = "directSave"
                console.log('onValidateSave direct Parts | category ==>', data.Category + ' diskon parts ==>', diskonParts + ' IsActive ==>',data.IsActive);
                return true;
            } else if (data.Category == 1 && diskonJasa >= 1 && data.IsActive == true){ // ini kalo sebelumnya udah ada diskon jasa
                $scope.dependencySave = "dependentJasa";
                console.log('onValidateSave dependentJasa | category ==>', data.Category + ' diskon jasa ==>', diskonJasa + ' IsActive ==>',data.IsActive);
                return true;
            } else if (data.Category == 2 && diskonParts >= 1 && data.IsActive == true) { // ini kalo sebelumnya udah ada diskon parts
                $scope.dependencySave = "dependentParts";
                console.log('onValidateSave dependentParts | category ==>', data.Category + ' diskon parts ==>', diskonParts + ' IsActive ==>',data.IsActive);
                return true;
            }else{
                console.log('ini ga masuk mana mana, category ==>',data.Category + ' diskon jasa ==>',diskonJasa + 'diskon parts ==>', diskonParts + ' IsActive ==>',data.IsActive);
                $scope.dependencySave = "directSave";
                return true
            }
        }


        $scope.directSave = function (model) {
            MasterDiscountFactory.create(model).then(
                function (res) {
                    $scope.getData();
                    bsNotify.show({
                        title: "Appointment Master Diskon",
                        content: "Data berhasil di simpan",
                        type: 'success'
                    });
                },
                function (err) {
                    // console.log('ini pesan error create ===>', err);
                    bsNotify.show(
                        {
                            type: 'danger',
                            title: "Data gagal diperbaharui!",
                            content: err.data.Message
                        }
                    );
                }
            );
        }

        $scope.directUpdate = function (model) {
            console.log("wew",model);
            MasterDiscountFactory.update(model).then(
                function (res) {
                    $scope.getData();
                    $scope.isUpdated = true;
                    bsNotify.show({
                        title: "Appointment Master Diskon",
                        content: "Data berhasil diperbaharui",
                        type: 'success'
                    });

                },
                function (err) {
                    $scope.isUpdated = false;
                    bsNotify.show(
                        {
                            type: 'danger',
                            title: "Data gagal diperbaharui!",
                            content: err.data.Message
                        }
                    );
                    // console.log('ini pesan error update ===>', err);
                }
            );
            
        }


        $scope.doCustomSave = function (mDiscountObj, mode) {

            console.log('mode  ===>', mode);//create update
            mDiscountObj.DateTo = $scope.formatDate($scope.mDiscount.DateTo);
            mDiscountObj.DateFrom = $scope.formatDate($scope.mDiscount.DateFrom);
            mDiscountObj.Discount = $scope.mDiscount.Discount;
            if (mDiscountObj.IsActive != true) {
                mDiscountObj.IsActive = false;
            }else{
                mDiscountObj.IsActive = true;
            }

            var alreadyMdiscountJasa=[];
            var alreadyMdiscountParts = [];
            for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                if ($scope.user.OutletId == $scope.OutletGR ) {
                    if ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) {// data jasa yang sudah ada di GR
                        if(mode == "create"){
                            $scope.MasterDiskonActive[i].IsActive = true;
                        } else {
                            $scope.MasterDiskonActive[i].IsActive = false;
                        }
                        alreadyMdiscountJasa.push($scope.MasterDiskonActive[i]);
                    } else if ($scope.MasterDiskonActive[i].Category == 2 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) { //data parts yang sudah ada GR
                        if(mode == "create"){
                            $scope.MasterDiskonActive[i].IsActive = true;
                        } else {
                            $scope.MasterDiskonActive[i].IsActive = false;
                        }
                        alreadyMdiscountParts.push($scope.MasterDiskonActive[i]);
                    }
                }else{
                    if ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP) {// data jasa yang sudah ada di BP
                        if(mode == "create"){
                            $scope.MasterDiskonActive[i].IsActive = true;
                        } else {
                            $scope.MasterDiskonActive[i].IsActive = false;
                        }
                        alreadyMdiscountJasa.push($scope.MasterDiskonActive[i]);
                    } else if ($scope.MasterDiskonActive[i].Category == 2 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP) { //data parts yang sudah ada BP
                        if(mode == "create"){
                            $scope.MasterDiskonActive[i].IsActive = true;
                        } else {
                            $scope.MasterDiskonActive[i].IsActive = false;
                        }
                        alreadyMdiscountParts.push($scope.MasterDiskonActive[i]);
                    }
                }
            }
            
            console.log('model ===>', mDiscountObj);
            if (mode == "create") {
                if ($scope.dependencySave == "dependentJasa") {
                    $scope.directUpdate([mDiscountObj, alreadyMdiscountJasa[0]]);
                    // $scope.bsAlertConfirm('Diskon Jasa Sudah ada', 'Apakah anda ingin mengaktifkan diskon ini dan me-non-aktifkan diskon jasa yang sudah ada sebelumnya?').then(function (res) {
                    //     if (res) {
                    //         $scope.directUpdate([mDiscountObj, alreadyMdiscountJasa[0]]);
                    //     }else{
                    //         $scope.cancelPopUp()
                    //         console.log("ini kalo klik cancle", res);
                    //     }
                    // });
                    
                } else if ($scope.dependencySave == "dependentParts"){
                    $scope.directUpdate([mDiscountObj, alreadyMdiscountParts[0]]);
                    // $scope.bsAlertConfirm('Diskon Parts Sudah ada', 'Apakah anda ingin mengaktifkan diskon ini dan me-non-aktifkan diskon parts yang sudah ada sebelumnya?').then(function (res) {
                    //     if (res) {
                    //         $scope.directUpdate([mDiscountObj, alreadyMdiscountParts[0]]);
                    //     } else {
                    //         $scope.cancelPopUp()
                    //         console.log("ini kalo klik cancle", res);
                    //     }
                    // });
                } else{
                    console.log('ini direct Save');
                    $scope.directSave([mDiscountObj]);
                }
            }else if (mode == "update") {
                console.log('mode update harusnya ===>', mode);
                console.log('mDiscountObj yang mau di update ===>', mDiscountObj);
                
                var diskonParts = 0;
                var diskonJasa = 0;
                for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                    if ($scope.MasterDiskonActive[i].Category == 1) {
                        diskonJasa += 1;
                    } else if ($scope.MasterDiskonActive[i].Category == 2) {
                        diskonParts += 1;
                    }
                }

                if (mDiscountObj.Category == 1 && diskonJasa == 0 && mDiscountObj.IsActive == true) { //ini kalo diskon jasa nya blm ada
                    $scope.dependencyUpdate = "directUpdate"
                    console.log('onValidateUpdate direct Jasa Update | category ==>', mDiscountObj.Category + ' diskon jasa ==>', diskonJasa + ' IsActive ==>', mDiscountObj.IsActive);
                    $scope.directUpdate([mDiscountObj]);
                    
                } else if (mDiscountObj.Category == 2 && diskonParts == 0 && mDiscountObj.IsActive == true) { //ini kalo diskon parts nya blm ada
                    $scope.dependencyUpdate = "directUpdate"
                    console.log('onValidateUpdate direct Parts Update | category ==>', mDiscountObj.Category + ' diskon parts ==>', diskonParts + ' IsActive ==>', mDiscountObj.IsActive);
                    $scope.directUpdate([mDiscountObj]);
                    
                } else if (mDiscountObj.Category == 1 && diskonJasa >= 1 && mDiscountObj.IsActive == true) { // ini kalo sebelumnya udah ada diskon jasa
                    $scope.dependencyUpdate = "dependentJasaUpdate";
                    console.log('onValidateUpdate dependentJasaUpdate | category ==>', mDiscountObj.Category + ' diskon jasa ==>', diskonJasa + ' IsActive ==>', mDiscountObj.IsActive);
                    $scope.bsAlertConfirm('Diskon Jasa Sudah ada', 'Apakah anda ingin mengaktifkan diskon ini dan update diskon jasa yang sudah ada sebelumnya?').then(function (res) {
                        if (res) {
                            if(mDiscountObj.AppointmentDiscountId == alreadyMdiscountJasa[0].AppointmentDiscountId){

                                $scope.directUpdate([mDiscountObj]);
                            }else{
                                $scope.directUpdate([mDiscountObj, alreadyMdiscountJasa[0]]);   
                            }
                            // $scope.directUpdate([mDiscountObj, alreadyMdiscountJasa[0]]);
                        } else {
                            $scope.cancelPopUp()
                            console.log("ini kalo klik cancle", res);
                        }
                    });
                    
                } else if (mDiscountObj.Category == 2 && diskonParts >= 1 && mDiscountObj.IsActive == true) { // ini kalo sebelumnya udah ada diskon parts
                    $scope.dependencyUpdate = "dependentPartsUpdate";
                    console.log('onValidateUpdate dependentPartsUpdate | category ==>', mDiscountObj.Category + ' diskon parts ==>', diskonParts + ' IsActive ==>', mDiscountObj.IsActive);
                    $scope.bsAlertConfirm('Diskon Parts Sudah ada', 'Apakah anda ingin mengaktifkan diskon ini dan update diskon parts yang sudah ada sebelumnya?').then(function (res) {
                        if (res) {
                            console.log("mDiscountObj, alreadyMdiscountParts[0]",mDiscountObj, alreadyMdiscountParts[0]);
                            // $scope.directUpdate([mDiscountObj, alreadyMdiscountParts[0]]);
                            if(mDiscountObj.AppointmentDiscountId == alreadyMdiscountParts[0].AppointmentDiscountId){

                                $scope.directUpdate([mDiscountObj]);
                            }else{
                                $scope.directUpdate([mDiscountObj, alreadyMdiscountParts[0]]);   
                            }
                        } else {
                            $scope.cancelPopUp()
                            console.log("ini kalo klik cancle", res);
                        }
                    });
                    
                } else {
                    console.log('ini ga masuk mana mana, category ==>', mDiscountObj.Category + ' diskon jasa ==>', diskonJasa + 'diskon parts ==>', diskonParts + ' IsActive ==>', mDiscountObj.IsActive);
                    $scope.dependencyUpdate = "directUpdate";
                    $scope.directUpdate([mDiscountObj]);
                }       
                    
            }


            $scope.formApi.setMode("grid");
        }

        $scope.cancelPopUp = function (){
            for (var i = 0; i < $scope.MasterDiskonActive.length; i++) {
                if ($scope.user.OutletId == $scope.OutletGR ) {
                    if ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) {// data jasa yang sudah ada di GR
                        $scope.MasterDiskonActive[i].IsActive = true;
                        // alreadyMdiscountJasa.push($scope.MasterDiskonActive[i]);
                    } else if ($scope.MasterDiskonActive[i].Category == 2 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletGR) { //data parts yang sudah ada GR
                        $scope.MasterDiskonActive[i].IsActive = true;
                        // alreadyMdiscountParts.push($scope.MasterDiskonActive[i]);
                    }
                }else{
                    if ($scope.MasterDiskonActive[i].Category == 1 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP) {// data jasa yang sudah ada di BP
                        $scope.MasterDiskonActive[i].IsActive = true;
                        // alreadyMdiscountJasa.push($scope.MasterDiskonActive[i]);
                    } else if ($scope.MasterDiskonActive[i].Category == 2 && $scope.MasterDiskonActive[i].OutletId == $scope.OutletBP) { //data parts yang sudah ada BP
                        $scope.MasterDiskonActive[i].IsActive = true;
                        // alreadyMdiscountParts.push($scope.MasterDiskonActive[i]);
                    }
                }
            }
        }

        $scope.selectRole = function (rows) {
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function (rows) {
            console.log('selected rows===>',rows);
        }

        


        
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            // columnDefs: [
            //     { displayName:'Appointment Diskon Id',name:'AppointmentDiscountId', field:'AppointmentDiscountId', visible: false },
            //     { displayName:'Category',name:'Category',field:'Category', visible: true, cellTemplate:'<p ng-if="row.entity.Category==1">&nbsp; Jasa</><p ng-if="row.entity.Category==2">&nbsp; Parts</>'},
            //     { displayName:'Persentage Diskon',name:'Discount',field:'Discount',visible:true,cellTemplate:'<p>&nbsp;{{row.entity.Discount}}%</p>'},
            //     { displayName:'Outlet ID',name:'OutletId',field:'OutletId',visible:true,},
            //     { displayName:'Tanggal Mulai',name:'DateFrom',field:'DateFrom',visible:true,cellFilter: 'date:\"dd-MM-yyyy\"' },
            //     { displayName:'Tanggal Berakhir',name:'DateTo', field:'DateTo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
            //     { displayName:'Aktif',name: 'Aktif', width: '7%', field:'IsActive', enableCellEdit: false, cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.IsActive == true" ng-click="grid.appScope.switchDiskonActive(row.entity)" ng-disabled="true"></p>' },
            // ]
            columnDefs: [
                { displayName:'Appointment Diskon Id',name:'AppointmentDiscountId', field:'AppointmentDiscountId', visible: false },
                { displayName:'Kategori Diskon',name:'Kategori Diskon',field:'CategoryStr',cellTemplate:'<p ng-if="row.entity.Category==1" style="margin-top:5px;">&nbsp; Jasa</><p ng-if="row.entity.Category==2" style="margin-top:5px;">&nbsp; Parts</>'},
                { displayName:'Persentase Diskon',name:'Persentase Diskon',field:'Discount',cellTemplate:'<p style="margin:5px 0 0 4px">&nbsp;{{row.entity.Discount}}%</p>'},
                { displayName:'Tanggal Mulai',name:'Tanggal Mulai',field:'DateFrom', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
                { displayName:'Tanggal Berakhir',name:'Tanggal Berakhir', field:'DateTo', visible: true,  cellFilter: 'date:\"dd-MM-yyyy\"' },
                { displayName:'Aktif',name: 'Aktif', width: '7%', field:'IsActive', visible: true, cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.IsActive == true" ng-click="grid.appScope.switchDiskonActive(row.entity)" ng-disabled="true"></p>' },
            ]
        };
    });


