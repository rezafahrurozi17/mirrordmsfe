angular.module('app')
    .factory('MasterDiscountFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/MAppointmentDiscount/Get/?start=1&limit=100000');                
                return res;
            },

            getDataAktif: function() { // ini yg blh di pake di menu2 lain
                var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                return res;
            },
            //ga kepake nih karna 
            create: function(data) {
                console.log('ini factory post data ===>',data)   
                return $http.post('/api/as/MAppointmentDiscount/SubmitData', data);
                // return $http.post('/api/as/MasterDiskon', [data]);
            },
            update: function(data) {
                console.log("ini factory update data ===>", data);
                // return $http.put('/api/as/MasterDiskon', [data]); // sekarang update sama di delete disatuin pake post
                return $http.put('/api/as/MAppointmentDiscount/UpdateData', data);
            },
            updateWODiscount: function(dataTask,dataParts) {
                console.log("dataTask difac",dataTask);
                console.log("dataParts difac",dataParts);
                return $http.put('/api/as/DiskonUpdateWO/Update', [{
                    JobTask : dataTask,
                    JobParts : dataParts
                }]);
            },
            // untuk discount yg aktive di wo dataBookingActive
            // -----------------------------------------------------
            // delete: function(id) {
            //     console.log("Data DP : ", id);
            //     return $http.delete('/api/as/MasterDiskon/DeleteMasterDiscount?DiscountId=' + id);
            // },
            
        }
    });
//