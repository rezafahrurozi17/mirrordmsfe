angular.module('app')
    .factory('BPCenterSateliteFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            getDataDealer: function() {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetDataDealer';
                console.log("url Get data Dealer : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataOutletCenter: function(dealerId, flag) {
                // Flag = 0 BP Center,
                // Flag = 1 BP Satelite
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetDataFreeOutlet/?DealerId=' + dealerId + '&Flag=' + flag;
                console.log("url Get data Outlet Center : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataSA: function(outletid) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetEmployeeData/?OutletId=' + outletid;
                // var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetEmployeeData/?OutletId=' + outletid;
                // var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetEmployeeData/?OutletId=' + user.OrgId;
                console.log("url Get data SA Employee : ", url);
                var res = $http.get(url);
                return res;
            },

            geListDataCenter: function(start, limit, dealerId) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetListDataCenter/?DealerId=' + dealerId + '&start=' + start + '&limit=' + limit;
                console.log("url Get data list Center : ", url);
                var res = $http.get(url);
                return res;
            },

            geListDataSatelite: function(start, limit, centerId) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/GetListDataSatellite/?CenterId=' + centerId + '&start=' + start + '&limit=' + limit;
                console.log("url Get data list Satellite : ", url);
                var res = $http.get(url);
                return res;
            },

            saveData: function(inputData) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/SubmitData/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            saveDataSatelite: function(inputData) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/SubmitDataSatellite/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            deleteData: function(centerId, satelliteId) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/Delete/?CenterId=' + centerId + '&satelliteId=' + satelliteId;
                console.log("url Delete data : ", url);
                var res = $http.delete(url);
                return res;
            },
            CreateNewData: function(inputData) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/CreateNewData/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },
            CekDataCenter: function(inputData) {
                // api/as/CekDataCenter
                var url = '/api/as/CekDataCenter/';
                var param = JSON.stringify(inputData);
                console.log("object cek saveData", param);
                var res = $http.put(url, param);
                return res;
            },

            
            CreateNewDataSatellite: function(data) {
                return $http.post('/api/as/AfterSalesMasterBPCenterSatellite/CreateNewDataSatellite', data);
            },
            CekDataSatellite: function(data) {
                // api/as/CekDataSatellite
                return $http.put('/api/as/CekDataSatellite', data);
            },

            
            GetAllDataBPCenter: function() {
                return $http.get('/api/as/AfterSalesMasterBPCenterSatellite/GetBPCenterData');
            },
            DeleteCenterSatellite: function(param, id) {
                return $http.put('/api/as/AfterSalesMasterBPCenterSatellite/DeleteDataCenterSatellite/' + param + '/' + id);
            }
        }
    });