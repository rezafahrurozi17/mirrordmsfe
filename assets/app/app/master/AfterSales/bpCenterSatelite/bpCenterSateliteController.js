var app = angular.module('app');
app.controller('BPCenterSateliteController', function($scope, $http, $filter, CurrentUser, BPCenterSateliteFactory, uiGridConstants, $timeout, bsNotify) {
    var user = CurrentUser.user();
    $scope.uiGridPageSize = 10;

    $scope.MainBPCenterSatelite_Show = true;
    $scope.TambahBPCenterSatelite_SateliteData = [];
    $scope.BPCenterSatelite_SelectedCenterId = 0;
    $scope.BPCenterSatelite_ListDealer = [];
    $scope.ModalSatelliteBPDelete_Data = '';
    $scope.ModalSatelliteBPDelete_DataId = 0;
    $scope.dataBawaan = {};
    $scope.DataAkhirBPCenter = {};
    $scope.user = CurrentUser.user();

    //$scope.BPCenterSatellite_DealerId = user.GroupDealerId;

    // $scope.GetAllDataBPCenter = function() {
    //     BPCenterSateliteFactory.GetAllDataBPCenter().then(function(resu) {
    //         console.log("GetAllDataBPCenter : ", resu.data.Result);
    //         var newData = resu.data.Result;
    //     });
    // };
    // $scope.GetAllDataBPCenter();
    $scope.MainBPCenterSatelite_InitiateDealer = function() {
        BPCenterSateliteFactory.getDataDealer()
            .then(
                function(res) {
                    var dataList = [];
                    $scope.BPCenterSatelite_ListDealer = res.data.Result;
                    console.log("user : ", user);
                    console.log("res : ", res.data.Result);
                    // $scope.BPCenterSatelite_ListDealer.some(function(obj, i){
                    // if(obj.GroupDealerId == user.GroupDealerId){
                    // $scope.MainBPCenterSatellite_Name = obj.GroupDealerName;
                    // }
                    // });

                    // angular.forEach(res.data.Result, function(value, key){
                    // dataList.push({"name":value.GroupDealerName, "value":{"dealerId" : value.GroupDealerId, "dealerName" : value.GroupDealerName}});
                    // });

                    // $scope.optionsMainBPCenterSatelite_Dealer = dataList;
                    $scope.BPCenterSatellite_DealerId = res.data.Result[0].GroupDealerId;
                    $scope.MainBPCenterSatellite_Name = res.data.Result[0].GroupDealerName;
                    $scope.MainBPCenterSatelite_InitiateOutletCenter($scope.BPCenterSatellite_DealerId)
                    $scope.BPCenter_UIGrid_Paging(1);

                }
            );
    }

    console.log("MainBPCeneterSatelite tambah : $scope.BPCenterSatellite_DealerId : ", $scope.BPCenterSatellite_DealerId);

    $scope.MainBPCenterSatelite_InitiateOutletCenter = function() {
        //BPCenterSateliteFactory.getDataOutletCenter($scope.MainBPCenterSatelite_Dealer.dealerId)
        BPCenterSateliteFactory.getDataOutletCenter($scope.BPCenterSatellite_DealerId, 0)
            .then(
                function(res) {
                    console.log('gondola2')
                    var dataList = [];
                    console.log("res initiate outlet center", res);
                    angular.forEach(res.data.Result, function(value, key) {
                        var sudahAda = 0
                        angular.forEach($scope.BPCenter_UIGrid.data, function(value2, key2) {
                            if (value.OutletId == value2.OutletId){
                                sudahAda++;
                            }
                        })

                        if (sudahAda == 0){
                            dataList.push({ "name": value.Name, "value": value });
                        }
                    });

                    $scope.optionsTambahBPCenterSatelite_Center_BPCenter = dataList;
                    console.log('isi data', $scope.optionsTambahBPCenterSatelite_Center_BPCenter);
                }
            );
    }

    $scope.TambahBPCenterSatelite_InitiateSA = function() {
        BPCenterSateliteFactory.getDataSA()
            .then(
                function(res) {
                    var dataList = [];
                    angular.forEach(res.data.Result, function(value, key) {
                        dataList.push({ "name": value.EmployeeName, "value": { "EmployeeName": value.EmployeeName, "EmployeeId": value.EmployeeId } });
                    });

                    $scope.optionsTambahBPCenterSatelite_Satelite_SASatelite = dataList;
                }
            );
    }

    $scope.EditBPCenterSatelite_InitiateOutletSatelite = function() {
        //BPCenterSateliteFactory.getDataOutletCenter($scope.MainBPCenterSatelite_Dealer.dealerId)
        $scope.sudahAdaAlias = true;
        $scope.TambahBPCenterSatelite_SateliteName = ''
        BPCenterSateliteFactory.getDataOutletCenter($scope.BPCenterSatellite_DealerId, 1)
            .then(
                function(res) {
                    console.log('gondola1')
                    console.log('main menu grid atas',$scope.BPCenter_UIGrid.data)
                    var dataList = [];
                    angular.forEach(res.data.Result, function(value, key) {

                        var sudahterdaftar = 0

                        angular.forEach($scope.BPCenter_UIGrid.data, function(value2, key2) {
                            if (value2.OutletId != $scope.BPCenterOutletId){
                                angular.forEach(value2.MSites_OutletBPCenters, function(value3, key3) {
                                    if (value3.OutletIdSatellite == value.OutletId) {
                                        sudahterdaftar++
                                    }
                                })
                            }
                        })

                        if (sudahterdaftar == 0){
                            dataList.push({ "name": value.Name, "value": value });
                        }
                    });

                    $scope.optionsTambahBPCenterSatelite_Satelite_BPSatelite = dataList;

                }
            );
    }

    $scope.TambahBPCenterSatelite_Center_BPCenter_Changed = function(data) {
        console.log('data', data);
        if (data !== undefined){
            $scope.TambahBPCenterSatelite_Center_Alamat = $scope.TambahBPCenterSatelite_Center_BPCenter.Address;
        }
    };

    $scope.TambahBPCenterSatelite_Center_Simpan_Clicked = function() {
        var inputData = [{
            "OutletType": 1,
            "OutletCenterId": $scope.TambahBPCenterSatelite_Center_BPCenter.OutletId
        }];
        BPCenterSateliteFactory.CekDataCenter(inputData).then(function (res1){ 
            if (res1.data == 666){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "BP Center sudah terdaftar, silahkan lakukan refresh"
                });
            } else {
                BPCenterSateliteFactory.CreateNewData(inputData).then(function(res) {
                    // alert("Data BP Center berhasil disimpan");
                    bsNotify.show({
                        size: 'small',
                        type: 'success',
                        title: "Success",
                        content: "Data BP Center berhasil disimpan"
                    });
                    $scope.BPCenter_UIGrid_Paging(1);
                    $scope.TambahBPCenterSatelite_Center_Kembali_Clicked();
                });
            }
        })

        
    }

    $scope.TambahBPCenterSatelite_Center_Kembali_Clicked = function() {
        $scope.TambahBPCenterSatelite_Center_ClearFields();
        $scope.TambahBPCenterSatelite_Center_Show = false;
        $scope.MainBPCenterSatelite_Show = true;
    }

    $scope.TambahBPCenterSatelite_Center_ClearFields = function() {
        $scope.TambahBPCenterSatelite_Center_DealerName = "";
        $scope.TambahBPCenterSatelite_Center_BPCenter = undefined;
        $scope.TambahBPCenterSatelite_Center_Alamat = "";
    }


    $scope.MainBPCenterSatelite_Dealer_Changed = function() {
        if (!angular.isUndefined($scope.MainBPCenterSatelite_Dealer)) {
            $scope.BPCenter_UIGrid_Paging(1);
        }
    }
    
    $scope.disableBtn = true;
    $scope.EditBPCenterData = function(row) {
        $scope.disableBtn = true;
        $scope.TambahBPCenterSatelite_Satelite_Show = true;
        $scope.MainBPCenterSatelite_Show = false;
        $scope.TambahBPCenterSatelite_Satelite_CenterName = row.entity.OutletName;
        $scope.BPCenterSatelite_SelectedCenterId = row.entity.OutletId;

        $scope.disableSASatellite = true;
        $scope.TambahBPCenterSatelite_Satelite_SASatelite = []

        // $scope.BPCenterSatelite_SelectedCenterId = row.entity.BPCenterId;
        $scope.BPCenterOutletId = row.entity.OutletId;
        $scope.EditBPCenterSatelite_InitiateOutletSatelite();
        // $scope.TambahBPCenterSatelite_InitiateSA();
        $scope.BPSateliteBaru_UIGrid_Paging(1);
    }

    $scope.HapusBPSateliteMainData = function(row) {
        $scope.ModalSatelliteBPDelete_Data = row.entity.BPSatellite;
        $scope.ModalSatelliteBPDelete_DataId = row.entity.SatelliteId;
        $scope.DeleteSatelliteData = row.entity;
        angular.element('#ModalSatelliteBPDelete').modal('show');
    }

    $scope.ModalSatelliteBPDelete_Ya = function(row) {
        console.log('ModalSatelliteBPDelete_Ya', row);
        BPCenterSateliteFactory.DeleteCenterSatellite(2, row.Id)
            .then(
                function(res) {
                    alert("Data berhasil dihapus");
                    angular.element('#ModalSatelliteBPDelete').modal('hide');
                    $scope.BPSatelite_UIGrid.data = [];
                    // $scope.BPSatelite_UIGrid_Paging(1);
                    // $scope.ModalSatelliteBPDelete_Tidak();
                    $scope.BPCenter_UIGrid_Paging(1);

                },
                function(err) {
                    alert(err.data.ExceptionMessage);
                }
            );
    }

    $scope.ModalSatelliteBPDelete_Tidak = function() {
        $scope.ModalSatelliteBPDelete_Data = "";
        $scope.ModalSatelliteBPDelete_DataId = 0;
        angular.element('#ModalSatelliteBPDelete').modal('hide');
    }

    $scope.HapusBPCenterData = function(row) {
        console.log('EditBPCenterData', row);
        BPCenterSateliteFactory.DeleteCenterSatellite(1, row.BPCenterId)
            .then(
                function(res) {
                    alert("Data berhasil dihapus");
                    $scope.BPCenter_UIGrid_Paging(1);
                    $scope.BPSatelite_UIGrid.data = [];
                },
                function(err) {
                    alert(err.data.ExceptionMessage);
                }
            );
    }

    $scope.TambahBPCenterSatelite_Satelite_Tambah_Clicked = function() {
       
        console.log("isi SA List", $scope.TambahBPCenterSatelite_Satelite_BPSatelite);
        var dataInput = {
            "SatelliteId": $scope.TambahBPCenterSatelite_Satelite_BPSatelite.OutletId,
            "BPSatellite": $scope.TambahBPCenterSatelite_Satelite_BPSatelite.Name,
            "Address": $scope.TambahBPCenterSatelite_Satelite_BPSatelite.Address,
            "SAName": $scope.TambahBPCenterSatelite_Satelite_SASatelite.EmployeeName,
            "SAId": $scope.TambahBPCenterSatelite_Satelite_SASatelite.EmployeeId,
            "CenterId": $scope.BPCenterSatelite_SelectedCenterId,
            // "OutletId": user.OrgId
            "NamaSatellite": $scope.TambahBPCenterSatelite_SateliteName.toUpperCase(),
            "OutletId": $scope.BPCenterOutletId
        };

        console.log("data input tambah : ", dataInput);
        $scope.TambahBPCenterSatelite_SateliteData.push(dataInput);
        $scope.BPSateliteBaru_UIGrid.data = $scope.TambahBPCenterSatelite_SateliteData;
        $scope.TambahBPCenterSatelite_Satelite_ClearFields();

    }

    var flagDis = 0;
    var dataEdit;
    $scope.TambahBPCenterSatelite_Satelite_ClearFields = function() {
        $scope.TambahBPCenterSatelite_Satelite_SASatelite = [];
        $scope.TambahBPCenterSatelite_Satelite_BPSatelite = [];
        $scope.TambahBPCenterSatelite_Satelite_Alamat = "";
        $scope.TambahBPCenterSatelite_SateliteName = '';
        $scope.sudahAdaAlias = false
        if(flagDis < dataEdit.length){
            $scope.disableBtn = false;
        }

        console.log("flagDis",flagDis ,'=',dataEdit.length);
    }

    $scope.TambahBPCenterSatelite_Satelite_Kembali_Clicked = function() {
        $scope.TambahBPCenterSatelite_Satelite_ClearFields();
        $scope.TambahBPCenterSatelite_SateliteData = [];
        $scope.TambahBPCenterSatelite_Satelite_Show = false;
        $scope.MainBPCenterSatelite_Show = true;

    }

    $scope.TambahBPCenterSatelite_Satelite_Simpan_Clicked = function() {
        if ($scope.TambahBPCenterSatelite_SateliteData.length == 0) {
            alert("Tidak ada data yang akan disimpan");
            return;
        } else {
            console.log("Data yang disimpan : ", $scope.TambahBPCenterSatelite_SateliteData);

            var newData = [];

            _.map($scope.TambahBPCenterSatelite_SateliteData, function(val) {
                if (val.Id == null || val.Id === undefined) {
                    var obj = {};
                    obj.OutletIdSatellite = val.SatelliteId;
                    obj.SaId = val.SAId;
                    obj.OutletIdBPCenter = val.CenterId;
                    obj.SAName = val.SAName;
                    obj.OutletId = $scope.BPCenterOutletId
                    obj.NamaSatellite = val.NamaSatellite
                    newData.push(obj);
                };
            });

            // BPCenterSateliteFactory.CreateNewDataSatelitte($scope.TambahBPCenterSatelite_SateliteData).then(function(res) {

            BPCenterSateliteFactory.CekDataSatellite(newData).then(function(res1) {
                if (res1.data == 666) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "BP Satellite sudah terdaftar, silahkan lakukan refresh"
                    });
                } else {
                    BPCenterSateliteFactory.CreateNewDataSatellite(newData).then(function(res) {
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Success",
                            content: "Data Berhasil Di Simpan"
                        });
                            $scope.BPCenter_UIGrid_Paging(1);
                            $scope.TambahBPCenterSatelite_InitiateSA();
                            $scope.TambahBPCenterSatelite_Satelite_Kembali_Clicked();
                        },
                        function(err) {
                            alert(err.data.ExceptionMessage);
                        }
                    );

                }
            })

            
        }
    }

    $scope.HapusBPSateliteData = function(row) {
        var index = $scope.TambahBPCenterSatelite_SateliteData.indexOf(row.entity);
        $scope.TambahBPCenterSatelite_SateliteData.splice(index, 1);
    }
    $scope.disableSASatellite = true;
    $scope.TambahBPCenterSatelite_Satelite_BPSatelite_Changed = function(data) {
        if (data !== undefined){
            console.log('datatata', data);
            var tmpOutletId = $scope.user.OutletId;
            var outletidsatelite = data.OutletId

            // if (data.Name !== undefined && data.Name !== null && data.Name !== ''){
            //     $scope.sudahAdaAlias = true
            //     $scope.TambahBPCenterSatelite_SateliteName = data.Name
            // } else {
            //     $scope.sudahAdaAlias = false
            // }

            $scope.sudahAdaAlias = false
            for ( var i=0; i<$scope.BPSateliteBaru_UIGrid.data.length; i++){
                if (data.Name === $scope.BPSateliteBaru_UIGrid.data[i].BPSatellite){
                    if ($scope.BPSateliteBaru_UIGrid.data[i].NamaSatellite !== null && $scope.BPSateliteBaru_UIGrid.data[i].NamaSatellite !== undefined && $scope.BPSateliteBaru_UIGrid.data[i].NamaSatellite !== ''){
                        $scope.sudahAdaAlias = true
                        $scope.TambahBPCenterSatelite_SateliteName = $scope.BPSateliteBaru_UIGrid.data[i].NamaSatellite
                    }
                }
            }

            // BPCenterSateliteFactory.getDataSA(tmpOutletId).then(function(resu) {
            BPCenterSateliteFactory.getDataSA(outletidsatelite).then(function(resu) {
                console.log('resu data', resu.data);
                $scope.disableSASatellite = false;
                var dataList = [];
                angular.forEach(resu.data.Result, function(value, key) {
                    dataList.push({ "name": value.EmployeeName, "value": { "EmployeeName": value.EmployeeName, "EmployeeId": value.EmployeeId } });
                });
                $scope.optionsTambahBPCenterSatelite_Satelite_SASatelite = dataList;
            });
    
            if (!angular.isUndefined($scope.TambahBPCenterSatelite_Satelite_BPSatelite))
                $scope.TambahBPCenterSatelite_Satelite_Alamat = $scope.TambahBPCenterSatelite_Satelite_BPSatelite.Address;
        }
        
    }

    ///BP CENTER MAIN
    $scope.BPCenter_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        rowSelection: true,
        enableFiltering: true,
        multiSelect: false,
        columnDefs: [
            { name: "CenterId", field: "BPCenterId", visible: false },
            { name: "BP Center", field: "OutletName" },
            { name: "Alamat", field: "Address" },
            { name: "Jumlah Satellite", field: "SateliteCount" },
            {
                name: "Action",
                cellTemplate: '<a class="fa fa-fw fa-lg fa-trash ng-scope" style="box-shadow:none!important;color:#777;margin:1px 4px 0px 2px;padding:0.5em" title="Hapus" ng-click="grid.appScope.HapusBPCenterData(row.entity)"></a>&nbsp;&nbsp;&nbsp;<a class="fa fa-fw fa-lg fa-plus ng-scope" style="box-shadow:none!important;color:#777;" title="Add Satellite" ng-click="grid.appScope.EditBPCenterData(row)"></a>'
            },
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiBPCenter = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //     if (pageNumber > 1)
            //         $scope.BPCenter_UIGrid_Paging(pageNumber);
            // });
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.OnSelectRowCenter(row.entity);
                $scope.BPCenterSatelite_SelectedCenterId = row.entity.BPCenterId;
                // $scope.BPSatelite_UIGrid_Paging(1);
            });
        }
    };

    $scope.OnSelectRowCenter = function(data) {
        console.log("OnSelectRowCenter : ", data);
        $scope.BPSatelite_UIGrid.data = data.MSites_OutletBPCenters;
        $scope.BPCenterId = data.BPCenterId;
    };

    $scope.BPCenter_UIGrid_Paging = function(pageNumber) {
        //BPCenterSateliteFactory.geListDataCenter(pageNumber, $scope.uiGridPageSize, $scope.MainBPCenterSatelite_Dealer.dealerId)
        // console.log("$scope.BPCenterSatellite_DealerId di uigridpaging", $scope.BPCenterSatellite_DealerId);
        // BPCenterSateliteFactory.geListDataCenter(pageNumber, $scope.uiGridPageSize, $scope.BPCenterSatellite_DealerId).then(function(res) {
        //     console.log("res =>", res.data.Result);
        //     $scope.BPCenter_UIGrid.data = res.data.Result;
        //     $scope.BPCenter_UIGrid.totalItems = res.data.Total;
        //     $scope.BPCenter_UIGrid.paginationPageSize = $scope.uiGridPageSize;
        //     $scope.BPCenter_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
        // });
        BPCenterSateliteFactory.GetAllDataBPCenter().then(function(resu) {
            var lengthData = resu.data.Result.length;
            console.log("GetAllDataBPCenter : ", resu.data.Result);
            var newData = resu.data.Result;
            var dataAkhir = resu.data.Result[lengthData - 1];
            $scope.BPCenter_UIGrid.data = newData;
            $scope.dataBawaan = newData;
            $scope.DataAkhirBPCenter = dataAkhir;
            console.log('data akhir', dataAkhir);
        });
    }

    ///BP SATELLITE MAIN
    $scope.BPSatelite_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        rowSelection: true,
        enableFiltering: true,
        multiSelect: false,
        columnDefs: [
            { name: "SatelliteId", field: "Id", visible: false },
            { name: "BP Satellite", field: "OutletName" },
            { name: "Alamat", field: "Address" },
            { name: "Nama SA Satellite", field: "SAName" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.HapusBPSateliteMainData(row)">Hapus</a>'
            },
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiBPSatelite = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //     if (pageNumber > 1)
            //         $scope.BPSatelite_UIGrid_Paging(pageNumber);
            // });
        }
    };

    $scope.BPSatelite_UIGrid_Paging = function(pageNumber) {
        BPCenterSateliteFactory.geListDataSatelite(pageNumber, $scope.uiGridPageSize, $scope.BPCenterSatelite_SelectedCenterId).then(function(res) {
            console.log("res =>", res.data.Result);
            $scope.BPSatelite_UIGrid.data = res.data.Result;
            // $scope.BPSatelite_UIGrid.totalItems = res.data.Total;
            // $scope.BPSatelite_UIGrid.paginationPageSize = $scope.uiGridPageSize;
            // $scope.BPSatelite_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
        });
    }

    ///BP SATELLITE BARU
    $scope.BPSateliteBaru_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        rowSelection: true,
        enableFiltering: true,
        multiSelect: false,
        columnDefs: [
            { name: "SatelliteId", field: "SatelliteId", visible: false },
            { name: "SAId", field: "SAId", visible: false },
            { name: "CenterId", field: "CenterId", visible: false },
            { name: "OutletId", field: "OutletId", visible: false },
            { name: "BP Satellite", field: "BPSatellite" },
            { name: "Nama Satellite", field: "NamaSatellite" },
            { name: "Alamat", field: "Address" },
            { name: "Nama SA Satellite", field: "SAName" },
            //{name:"Action",
            //		cellTemplate:'<a ng-click="grid.appScope.HapusBPSateliteData(row)">Hapus</a>'},
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiBPSateliteBaru = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
                if (pageNumber > 1)
                    $scope.BPSateliteBaru_UIGrid_Paging(pageNumber);
            });
        }
    };

    $scope.BPSateliteBaru_UIGrid_Paging = function(pageNumber) {
        BPCenterSateliteFactory.geListDataSatelite(pageNumber, $scope.uiGridPageSize, $scope.BPCenterSatelite_SelectedCenterId)
            .then(
                function(res) {
                    console.log("res =>", res.data.Result);
                    $scope.TambahBPCenterSatelite_SateliteData = res.data.Result;
                    $scope.BPSateliteBaru_UIGrid.data = $scope.TambahBPCenterSatelite_SateliteData;
                    $scope.BPSateliteBaru_UIGrid.totalItems = res.data.Total;

                    flagDis = res.data.Result.length;
                    dataEdit = $scope.BPSateliteBaru_UIGrid.data;
                    console.log("asdf",flagDis,dataEdit);
                    // $scope.BPSateliteBaru_UIGrid.paginationPageSize = $scope.uiGridPageSize;
                    // $scope.BPSateliteBaru_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
                }
            );
    }


    $scope.MainBPCenterSatelite_Tambah_Clicked = function() {
        console.log("isi combobox : ", $scope.MainBPCenterSatelite_Dealer);
        //if(angular.isUndefined($scope.MainBPCenterSatelite_Dealer))
        console.log("MainBPCeneterSatelite tambah : $scope.BPCenterSatellite_DealerId : ", $scope.BPCenterSatellite_DealerId);
        if ($scope.BPCenterSatellite_DealerId == 0)
            return;

        $scope.TambahBPCenterSatelite_Center_Show = true;
        $scope.MainBPCenterSatelite_Show = false;
        //$scope.TambahBPCenterSatelite_Center_DealerName = $scope.MainBPCenterSatelite_Dealer.dealerName;
        //$scope.MainBPCenterSatelite_InitiateOutletCenter($scope.MainBPCenterSatelite_Dealer.dealerId);
        $scope.TambahBPCenterSatelite_Center_DealerName = $scope.MainBPCenterSatellite_Name;
        $scope.MainBPCenterSatelite_InitiateOutletCenter($scope.BPCenterSatellite_DealerId);
    }



    $scope.MainBPCenterSatelite_InitiateDealer();
    $scope.MainBPCenterSatelite_DownloadExcelTemplate = function() {
        var excelData = [];
        var excelSheet1 = [];
        var fileName = "BP_Center_Satellite_Template";
        var fileName2 = "DataSupport";

        excelData = [{ "BPCenter": "", "BPSatellite": "" }];
        // console.log("cek", $scope.dataBawaan);
        // for (var i = 0; i < $scope.dataBawaan.length; i++) {
        //   console.log("log", $scope.dataBawaan[i]);
        //   excelSheet1 = [{"BP Center": $scope.dataBawaan[i].OutletName}];
        // }


        XLSXInterface.writeToXLSX(excelData, fileName);
    }

    $scope.MainBPCenterSatelite_Upload_Clicked = function() {
        angular.element(document.querySelector('#UploadbpCenterSatelitte')).val(null);
        angular.element('#ModalUploadbpCenterSatelitte').modal('show');
    }
    $scope.loadXLSBpCenterSatellite = function(ExcelFile) {
        var myEl = angular.element(document.querySelector('#UploadbpCenterSatelitte')); //ambil elemen dari dokumen yang di-upload


        XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
            $scope.ExcelData = json;
            console.log("ExcelData : ", $scope.ExcelData);
            console.log("json", json);
        });
    }

    $scope.UploadMasterVendor_Upload_Clicked = function() {
        var eachData = {};
        var inputData = [];

        angular.forEach($scope.ExcelData, function(value, key) {
            eachData = {
                "OutletType": 1,
                "OutletCenterId": value.BPCenter,
                "BPSatellite": value.BPSatellite
            }
            inputData.push(eachData);
        });
        console.log('cek data upload', inputData);
        $scope.updateUpload(inputData);


        // console.log("data input sbelum masuk factory :", inputData);
        // BPCenterSateliteFactory.CreateNewData(inputData)
        // .then(
        // 	function(res){
        // 		alert("Data berhasil disimpan");
        // 		angular.element('#ModalUploadbpCenterSatelitte').modal('hide');
        // 		angular.element( document.querySelector( '#UploadbpCenterSatelitte' ) ).val(null);
        // 		console.log("input data coba", inputData);
        // 	},
        // 	function(err){
        // 		console.log("err : ", err);
        // 	}
        // );
    }

    $scope.updateUpload = function(dataInput) {
        console.log('masuk uy', dataInput);
        var inputDataDB = [];
        var isiOutlet = [];
        var idSatellite = 0;
        var dataSatellite = [];
        var bpSatelliteData = {};


        isiOutlet = $scope.optionsTambahBPCenterSatelite_Center_BPCenter;
        console.log('masuk outlet', isiOutlet, inputDataDB, dataInput);

        for (var i = 0; i < dataInput.length; i++) {
            console.log('data ke -', [i], dataInput[i], dataInput.length);
            for (var j = 0; j < isiOutlet.length; j++) {
                console.log('masuk', dataInput[i], isiOutlet[j].name);
                if (dataInput[i].BPSatellite == isiOutlet[j].value.Name) {
                    idSatellite = isiOutlet[j].value.OutletId;
                    console.log('satelite', dataInput[i].BPSatellite, idSatellite);
                }
                if (dataInput[i].OutletCenterId == isiOutlet[j].value.Name) {
                    dataInput[i].OutletCenterId = isiOutlet[j].value.OutletId;
                    console.log('ini outlet Id setelah bandingan', dataInput[i].OutletCenterId);
                    inputDataDB.push(dataInput[i]);
                    BPCenterSateliteFactory.CreateNewData(inputDataDB)
                        .then(
                            function(res) {
                                $scope.BPCenter_UIGrid_Paging(1);
                                bpSatelliteData = {
                                    "SatelliteId": idSatellite,
                                    "BPSatellite": $scope.DataAkhirBPCenter.OutletName,
                                    "Address": $scope.DataAkhirBPCenter.Address,
                                    "SAName": 0,
                                    "SAId": 0,
                                    "CenterId": $scope.DataAkhirBPCenter.BPCenterId,
                                    "OutletId": user.OrgId
                                }
                                dataSatellite.push(bpSatelliteData);

                                BPCenterSateliteFactory.CreateNewDataSatellite(dataSatellite).then(function(res) {
                                        console.log('berhasil save satellite', dataSatellite);
                                    },
                                    function(err) {
                                        alert(err.data.ExceptionMessage);
                                    }
                                );
                                console.log('data ke -', [i], bpSatelliteData);
                                $scope.TambahBPCenterSatelite_Center_Kembali_Clicked();
                                console.log("input data coba", res);
                            },
                            function(err) {
                                console.log("err : ", err);
                            }
                        );
                }
            }
        }

        angular.element('#ModalUploadbpCenterSatelitte').modal('hide');
        angular.element(document.querySelector('#UploadbpCenterSatelitte')).val(null);
        bsNotify.show({
            size: 'small',
            type: 'success',
            title: "Success",
            content: "Data Berhasil Di Upload"
        });

    }


});