var app = angular.module('app');
app.controller('MasterVendorController', function ($scope, $http, $filter, bsAlert, CurrentUser, MasterVendorFactory, $timeout, bsNotify) {
    var user = CurrentUser.user();
    $scope.optionsMainVendorNamaKolom = [{ name: "Kode Vendor", value: "Kode Vendor" }, { name: "Nama Vendor", value: "Nama Vendor" }, { name: "Tipe Vendor", value: "Tipe Vendor" }];
    $scope.optionsTambahVendorJenisVendor = [{ name: "AfCo", value: 1 }, { name: "3rd Party", value: 0 }];
    $scope.optionsTambahVendor_TipeJasa = [{ name: "Eksternal", value: 1 }, { name: "Internal", value: 0 }];
    $scope.PaymentMethodData = [{ PaymentMethodId: 1, Name: "Tagihan" }, { PaymentMethodId: 2, Name: "Tunai" }];

    $scope.uiGridPageSize = 10;
    $scope.MasterVendorUoMList = [];
    $scope.ExcelData = [];
    $scope.MasterVendor_BusinessUnitList = [];
    $scope.MasterVendor_VendorOutlet = [];
    $scope.MasterVendor_VendorOutletModel = [];
    $scope.MasterVendorSubletList = [];
    $scope.MasterVendorKaroseriTypeList = [];
    $scope.MasterVendorJasaData = [];
    $scope.MasterVendorUoMData = [];
    $scope.TambahVendor_ServiceId = 0;
    $scope.MasterVendorJasaJBData = [];
    $scope.TambahVendor_ServiceJBId = 0;
    $scope.PartListData = [];
    $scope.PartListDataJB = [];
    $scope.VendorAccBankListData = [];
    $scope.MasterVendorVModelList = [];
    $scope.TambahVendorKaroseriDataList = [];
    $scope.TambahVendorKaroseriJBDataList = [];
    $scope.mode = "new";
    $scope.modelDataCheckParts = [];
    $scope.modelDataCheckDate = [];
    $scope.mData = {}
    $scope.Form = {} // ini di tambahin biar ng-model di dalem uib-tabset bisa kebaca

    $scope.ModalTambahVendorSearchJasa = false;
    $scope.ModalUploadMasterVendor = false;
    $scope.ModalTambahVendorSearchParts = false;
    $scope.ModalTambahVendorPartsUoMConversion = false;
	
	$scope.showPKP = false;

    $scope.dateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        // disableWeekend: 1
    };

	$scope.optionsPKPType = [{ value: 0, name: 'PKP' }, { value: 1, name: 'Non PKP' }];

    // $scope.ToPaymentMethodId = function(PaymentMethodId) {
    //     var namePaymentMethodId;
    //     switch (PaymentMethodId) {
    //         case 1:
    //             namePaymentMethodId = "Kredit";
    //             break;
    //         case 2:
    //             namePaymentMethodId = "Tunai";
    //             break;
    //     }
    //     return namePaymentMethodId;
    // }

    $scope.MasterVendorGetBusType = function () {
        MasterVendorFactory.getDataTipeBisnis()
            .then(
                function (res) {
                    angular.forEach(res.data.Result, function (value, key) {
                        $scope.MasterVendor_BusinessUnitList.push({ name: value.BusinessUnitName, value: value.BusinessUnitId });
                    });
                    $scope.optionsTambahVendorBusinessUnit = $scope.MasterVendor_BusinessUnitList;
                }
            );
    }

    $scope.MasterVendorGetVendorOutlet = function () {
        MasterVendorFactory.getDataVendorOutlet()
            .then(
                function (res) {
                    $scope.MasterVendor_VendorOutletModel = res.data.Result;
                    angular.forEach(res.data.Result, function (value, key) {
                        $scope.MasterVendor_VendorOutlet.push({ name: value.OutletName, value: value.OutletName });
                    });
                    $scope.optionsTambahVendorOutlet = $scope.MasterVendor_VendorOutlet;
                }
            );
    }

    $scope.MasterVendorGetSublet = function () {
        MasterVendorFactory.getDataSubletVendor()
            .then(
                function (res) {
                    angular.forEach(res.data.Result, function (value, key) {
                        $scope.MasterVendorSubletList.push({ name: value.SubletCode, value: value.SubletId });
                    });
                    //console.log("sublet list :", $scope.MasterVendorSubletList);
                    $scope.optionsTambahVendorTipeSublet = $scope.MasterVendorSubletList;
                }
            );
    }

    $scope.MasterVendorGetUoM = function () {
        MasterVendorFactory.getDataUoMList()
            .then(
                function (res) {
                    console.log('MasterVendorGetUoM', res.data.Result);
                    angular.forEach(res.data.Result, function (value, key) {
                        if (value.Name.toUpperCase() == 'PCS') {
                            $scope.Form.TambahVendor_DefaultSatuan = value.MasterId.toString();
                        }
                        // $scope.MasterVendorUoMList.push({ name: value.UOMName, value: value.UOMId });
                        $scope.MasterVendorUoMList.push({ name: value.Name, value: value.MasterId });
                    });
                    console.log('MasterVendorUoMList >', $scope.MasterVendorUoMList);
                }
            );
    }

    $scope.MasterVendorGetDataVendor = function () {
        MasterVendorFactory.getDataVendor()
            .then(
                function (res) {
                    console.log('MasterVendorGetDataVendor', res.data.Result);
                    $scope.DataVendor = res.data.Result
                    // angular.forEach(res.data.Result, function (value, key) {
                    //     if (value.Name.toUpperCase() == 'PCS') {
                    //         $scope.Form.TambahVendor_DefaultSatuan = value.MasterId.toString();
                    //     }
                    //     // $scope.MasterVendorUoMList.push({ name: value.UOMName, value: value.UOMId });
                    //     $scope.MasterVendorUoMList.push({ name: value.Name, value: value.MasterId });
                    // });
                    console.log('Data Vendor >', $scope.DataVendor);
                }
            );
    }

    $scope.selectTypeSatuan = function (item) {
        console.log("selectTypeSatuan list :", item);
    };

    $scope.selectTypeSatuanDefault = function (item) {
        console.log("selectTypeSatuanDefault list :", item);
    };

    $scope.MasterVendorGetCity = function () {
        if (!angular.isUndefined($scope.TambahVendor_Provinsi)) {
            MasterVendorFactory.getDataKotaVendor($scope.TambahVendor_Provinsi)
                .then(
                    function (res) {
                        var City = [];
                        angular.forEach(res.data.Result, function (value, key) {
                            City.push({ name: value.CityRegencyName, value: value.CityRegencyId });
                        });
                        $scope.optionsTambahVendorKota = City;
                    }
                );
        } else {
            $scope.optionsTambahVendorKota = [];
        }
    }

    $scope.TambahVendor_Provinsi_Changed = function () {
        $scope.MasterVendorGetCity();
    }

    $scope.valueP = function () {
        $scope.TambahVendor_Kota = null;
    }

    $scope.TambahVendor_NamaVendor_Changed = function (data) {
        // console.log('masuk', $scope.optionsTambahVendorOutlet,$scope.MasterVendor_VendorOutletModel );
        _.map($scope.MasterVendor_VendorOutletModel, function (obj) {
            if (obj.OutletName === data) {
                $scope.TambahVendor_KodeVendor = obj.OutletCode;
                $scope.TambahVendor_AlamatKantor = obj.OutletAddress;
                $scope.TambahVendor_Telepon = obj.OutletPhone;
                $scope.TambahVendor_Fax = obj.OutletFax;
                $scope.TambahVendor_VendorOutlet = obj.OutletId;
                $scope.TambahVendor_NamaNPWP = obj.OutletName;
                $scope.TambahVendor_NPWPAddress = obj.OutletAddress;
            }
        });
    }

    $scope.MasterVendorGetKaroseriType = function () {
        MasterVendorFactory.getDataKaroseriType()
            .then(
                function (res) {
                    // //console.log("res.data.Result faisal",res.data.Result);
                    // console.log("res.data.Result faisal",res.data.Result);
                    angular.forEach(res.data.Result, function (value, key) {
                        // $scope.MasterVendorKaroseriTypeList.push({name:value.KaroseriTypeName, value:value.KaroseriTypeId});
                        $scope.MasterVendorKaroseriTypeList.push({ name: value.KaroseriName, value: value.KaroseriId });
                    });
                    $scope.optionsTambahVendorTipeKaroseri = $scope.MasterVendorKaroseriTypeList;
                    
                }
            );
    }

    $scope.MasterVendorGetVModel = function () {
        var VModelList = [];
        var VModelListModelName = [];
        MasterVendorFactory.getDataVModel()
            .then(
                function (res) {
                    $scope.MasterVendorVModelList = res.data.Result;
                    angular.forEach(res.data.Result, function (value, key) {
                        VModelList.push({ name: value.VehicleModelCode, value: value.VehicleModelId });
                        VModelListModelName.push({ name: value.VehicleModelName, value: value.VehicleModelId });
                    });
                    $scope.optionsTambahVendorKaroseriModel = VModelList;
                    $scope.optionsTambahVendorModel = VModelListModelName;
                }
            );
    }
    $scope.tmpNameModel = "";
    $scope.tmpNameFullModel = "";
    $scope.value = function (data) {
        console.log("data full model", data);
        if (data !== undefined || data.name !== undefined) {
            console.log("data full model", data);
            $scope.tmpNameFullModel = angular.copy(data.name);
            //console.log("datatatatat",data);
        }
    };

    $scope.selectedGroup = function (item) {
        console.log("data selectedGroup", item);
    };

    $scope.MasterVendorGetVFullModel = function (dataModel) {
        //console.log("dataModel",dataModel);
        var VFullModelList = [];
        var VFullModelListModelName = [];
        $scope.optionsTambahVendorKaroseriFullModel = null;
        $scope.Form.TambahVendor_KaroseriFullModel = "";
        $scope.tmpNameModel = angular.copy(dataModel.name);
        MasterVendorFactory.getDataVFullModel(dataModel.value)
            .then(
                function (res) {
                    $scope.MasterVendorVFullModelList = res.data.Result;
                    angular.forEach(res.data.Result, function (value, key) {
                        VFullModelList.push({ name: value.FullModel, value: value.VehicleTypeId });
                        // VFullModelListModelName.push({name:value.VehicleModelName, value:value.VehicleModelId});
                    });
                    $scope.optionsTambahVendorKaroseriFullModel = VFullModelList;
                    console.log('ppp', $scope.optionsTambahVendorKaroseriFullModel);
                    // $scope.optionsTambahVendorModel = VModelListModelName;
                }
            );
    }

    $scope.MasterVendorGetProvince = function () {
        MasterVendorFactory.getDataPropinsiVendor()
            .then(
                function (res) {
                    var Province = [];
                    angular.forEach(res.data.Result, function (value, key) {
                        Province.push({ name: value.ProvinceName, value: value.ProvinceId });
                    });
                    $scope.optionsTambahVendorProvinsi = Province;
                }
            );
    }
    $scope.MasterVendorType = function () {
        MasterVendorFactory.getDataTipeVendor()
            .then(
                function (res) {
                    var tipeVendor = [];
                    angular.forEach(res.data.Result, function (value, key) {
                        tipeVendor.push({ name: value.Name, value: value.VendorTypeId });
                    });
                    $scope.optionsMainVendorJenisVendor = tipeVendor;
                    $scope.TambahVendor_JenisVendor = tipeVendor.value
                }
            );
    }

    $scope.getVendorTypeId = function (desc) {
        var returnVal = 0;
        // $scope.optionsMainVendorJenisVendor.map(function (val) {
        //     if(val.Name.toLowerCase() == desc.toLowerCase()){
        //         returnVal = val.VendorTypeId;
        //         break;
        //     }
        // });
        console.log('aaaa >>', $scope.optionsMainVendorJenisVendor);
        for (var index = 0; index < $scope.optionsMainVendorJenisVendor.length; index++) {
            console.log('aaaa >>', $scope.optionsMainVendorJenisVendor[index]);
            if ($scope.optionsMainVendorJenisVendor[index].name.toString().toLowerCase() == desc.toString().toLowerCase()) {
                returnVal = $scope.optionsMainVendorJenisVendor[index].value;
                break;
            }

        }

        return returnVal;
    };

    MasterVendorFactory.getDataTipeBisnis()
        .then(
            function (res) {
                var tipeBisnis = [];
                angular.forEach(res.data.Result, function (value, key) {
                    tipeBisnis.push({ name: value.BusinessUnitName, value: value.BusinessUnitId });
                });
                $scope.optionsMainVendorBusinessType = tipeBisnis;
            }
        );

    MasterVendorFactory.getDataNamaBank()
        .then(
            function (res) {
                var namaBank = [];
                angular.forEach(res.data.Result, function (value, key) {
                    namaBank.push({ name: value.BankName, value: value.BankId });

                    $scope.optionsTambahVendorNamaBank = namaBank;
                });
            }
        )

    $scope.MasterVendor_GetDataPartsList = function (VendorId) {
        MasterVendorFactory.getDataPartsList(VendorId)
            .then(
                function (res) {
                    console.log('MasterVendor_GetDataPartsList >>', res.data.Result);
                    var inputData = {
                        "PartsId": $scope.TambahVendor_PartsId,
                        "MaterialCode": $scope.TambahVendor_KodeMaterial,
                        "MaterialName": $scope.Form.TambahVendor_NamaMaterial,
                        // "BaseUOM": $scope.Form.TambahVendor_SatuanDasar,
                        "BaseUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                        // "UOM": _.result(_.find($scope.MasterVendorUoMList, function(o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                        "BaseUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'value'),
                        // "DefaultUOM": $scope.Form.TambahVendor_DefaultSatuan,
                        "DefaultUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'name'),
                        "DefaultUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'value'),
                    };
                    var obj = {};
                    if (res.data.Result.length > 0) {
                        _.map(res.data.Result, function (a) {
                            console.log("obj aa", a)
                            obj = {};
                            obj.PartsId = a.PartsId;
                            obj.MaterialCode = a.MaterialCode;
                            obj.MaterialName = a.MaterialName;
                            obj.BaseUOMId = a.BaseUOMId;
                            obj.DefaultUOMId = a.DefaultUOMId;
                            obj.BaseUOM = _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == a.BaseUOMId) { return o.name } }), 'name');
                            obj.DefaultUOM = _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == a.DefaultUOMId) { return o.name } }), 'name');

                            $scope.PartListData.push(obj);
                            console.log("obj", obj)
                        });
                        console.log("data parts", $scope.PartListData, inputData)
                        // $scope.PartListData = res.data.Result;
                        $scope.PartsList_UIGrid.data = $scope.PartListData;

                        for (var i = 0; i < res.data.Result.length; i++) {
                            $scope.modelDataCheckParts = res.data.Result;
                            console.log("modalllll", res.data.Result, res.data.Result[i].MaterialCode);
                        }
                        console.log("meeeedeeelll", $scope.modelDataCheckParts);
                    }
                }
            );
    }

    $scope.MasterVendor_getDataPartsJBList = function (VendorId) {
        MasterVendorFactory.getDataPartsJBList(VendorId)
            .then(
                function (res) {
                    console.log("partsJB", res)
                    $scope.PartListDataJB = res.data.Result;
                    $scope.PartsJBList_UIGrid.data = $scope.PartListDataJB;
                    // $scope.MasterVendorUoMList = res.data.Result;
                }
            );
    }

    $scope.MasterVendor_getDataKaroseriList = function (VendorId) {
        MasterVendorFactory.getDataKaroseriList(VendorId)
            .then(
                function (res) {
                    $scope.TambahVendorKaroseriDataList = res.data.Result;
                    $scope.KaroseriList_UIGrid.data = $scope.TambahVendorKaroseriDataList;
                    $scope.MasterVendor_setDataKaroseriList($scope.TambahVendorKaroseriDataList);
                }
            );
    }

    $scope.MasterVendor_getDataKaroseriJBList = function (VendorId) {
        MasterVendorFactory.getDataKaroseriJBList(VendorId)
            .then(
                function (res) {
                    for(var cc in res.data.Result){
                        res.data.Result[cc].DateStart = $scope.changeFormatDate(res.data.Result[cc].DateStart)
                        res.data.Result[cc].DateEnd = $scope.changeFormatDate(res.data.Result[cc].DateEnd)
                    }
                    $scope.TambahVendorKaroseriJBDataList = res.data.Result;
                    $scope.KaroseriJBList_UIGrid.data = $scope.TambahVendorKaroseriJBDataList;
                    $scope.MasterVendor_setDataKaroseriList($scope.TambahVendorKaroseriDataList);
                }
            );
    }

    $scope.MasterVendor_setDataKaroseriList = function (KaroseriList) {
        var DataComboBox = [];
        console.log("setDataKaroseriList", KaroseriList);
        angular.forEach(KaroseriList, function (value, key) {
            console.log("value.KaroseriCode", value);
            DataComboBox.push({ name: value.KaroseriType, value: value.KaroseriTypeId,ValueModel:value.ModelId, ValueFullModelId:value.FullModelId });
        });

        $scope.optionsTambahVendorKodeKaroseriJB = DataComboBox;
        
    }

    $scope.TambahVendor_KodeKaroseriJB_Changed = function () {
        $scope.TambahVendorKaroseriDataList.some(function (obj, i) {
            //console.log("$scope.TambahVendor_KodeKaroseriJB", $scope.TambahVendor_KodeKaroseriJB);
            console.log("obj.KaroseriCode", obj, $scope.Form.TambahVendor_KodeKaroseriJB,$scope.Form.TambahVendor_KaroseriModelIdJB,$scope.optionsTambahVendorKodeKaroseriJB.ValueModel);
            if (obj.KaroseriTypeId === $scope.Form.TambahVendor_KodeKaroseriJB && obj.ModelId === $scope.Form.TambahVendor_KaroseriModelIdJB){
                $scope.Form.TambahVendor_TipeKaroseriJB = obj.KaroseriName;
                $scope.Form.TambahVendor_KaroseriModelJB = obj.ModelName;
                $scope.Form.TambahVendor_KaroseriModelIdJB = obj.ModelId;
                $scope.Form.TambahVendor_KaroseriFullModelIdJB = obj.FullModelId;

            }
                //$scope.Form.TambahVendor_TipeKaroseriJB = obj.KaroseriType;
            
        });
    }

    $scope.MasterVendor_GetDataPartsListUOMConv = function (VendorId) {
        MasterVendorFactory.getDataPartsListUOMConv(VendorId)
            .then(
                function (res) {
                    $scope.MasterVendorUoMData = res.data.Result;
                }
            );
    }

    $scope.MasterVendor_getDataAccBankList = function (VendorId, BusinessUnitId) {
        MasterVendorFactory.getDataAccBankList(VendorId, BusinessUnitId)
            .then(
                function (res) {
                    console.log("data bank", res)
                    //console.log('res vendoraccbanklistdata', res);
                    $scope.VendorAccBankListData = res.data.Result;
                    //console.log('$scope.VendorAccBankListData', $scope.VendorAccBankListData);
                    $scope.BankAccountList_UIGrid.data = $scope.VendorAccBankListData;
                }
            );
    }

    $scope.MasterVendor_getDataJasaList = function (VendorId) {
        MasterVendorFactory.getDataJasaList(VendorId)
            .then(
                function (res) {
                    $scope.MasterVendorJasaData = res.data.Result;
                    $scope.JasaList_UIGrid.data = $scope.MasterVendorJasaData;
                }
            );
        //console.log('I thing', MasterVendorJasaData);
    }

    $scope.MasterVendor_getDataJasaJBList = function (VendorId) {
        MasterVendorFactory.getDataJasaJBList(VendorId)
            .then(
                function (res) {
                    for( var c in res.data.Result){
                        res.data.Result[c].DateStart = $scope.changeFormatDate(res.data.Result[c].DateStart)
                        res.data.Result[c].DateEnd = $scope.changeFormatDate(res.data.Result[c].DateEnd) 
                    }
                    $scope.MasterVendorJasaJBData = res.data.Result;
                    $scope.JasaJBList_UIGrid.data = $scope.MasterVendorJasaJBData;
                    console.log("get data res",res.data.Result)
                }
            );
    }

    $scope.MasterGroupList = function () {
        MasterVendorFactory.getDataGroup().then(function (resu) {
            $scope.optionsTambahVendorGroupList = resu.data.Result;
        });
    };

    
    $scope.MainVendor_Show = true;
    $scope.MasterVendorGetBusType();
    $scope.MasterVendorType();
    $scope.MasterVendorGetProvince();
    $scope.MasterVendorGetUoM();
    $scope.MasterVendorGetDataVendor();
    $scope.MasterVendorGetSublet();
    $scope.MasterVendorGetKaroseriType();
    $scope.MasterVendorGetVModel();
    $scope.MasterVendorGetVendorOutlet();
    $scope.MasterGroupList();
    // $scope.getDataTipeVendor();

    $scope.MainVendor_New_Clicked = function (mode) {
        $scope.mode = "new";
		$scope.modeN = "new";
		$scope.showPKP = false;
        $scope.TambahVendor_isDisable_ViewMode = false;
        $scope.TambahVendor_Show = true;
        $scope.MainVendor_Show = false;
        $scope.TambahVendor_Satuan_Show = true;
        $scope.TambahVendorNotInternal_Show = true;
        $scope.TambahVendor_Jasa_Show = false;
        $scope.TambahVendor_Bank_Show = false;
        $scope.TambahVendor_SatuanJB_Show = false;
        $scope.TambahVendor_JasaJB_Show = false;
        $scope.TambahVendor_Karoseri_Show = false;
        $scope.TambahVendor_KaroseriJB_Show = false;

        $scope.Form.TambahVendor_TipeKaroseri = "";
        $scope.Form.TambahVendor_KaroseriFullModel = "";
        $scope.Form.TambahVendor_KaroseriModel = "";
        $scope.Form.TambahVendor_KaroseriDeskripsi = "";
        $scope.Form.TambahVendor_KaroseriCode = "";
        $scope.KaroseriList_UIGrid.data = [];

        $scope.Form.TambahVendor_NamaBank = "";
        $scope.Form.TambahVendor_AtasNamaBank = "";
        $scope.Form.TambahVendor_NomorRekening = "";
        $scope.BankAccountList_UIGrid.data = "";

        $scope.Form.TambahVendor_KodeKaroseriJB = "";
        $scope.Form.TambahVendor_HargaBeliKaroseri = "";
        $scope.Form.TambahVendor_TipeKaroseriJB = "";
        $scope.Form.TambahVendor_HargaJualKaroseri = "";
        $scope.Form.TambahVendor_TanggalAwalKaroseri = undefined;
        $scope.Form.TambahVendor_TanggalAkhirKaroseri = undefined;
        $scope.KaroseriJBList_UIGrid.data = [];
        $scope.PartListDataJB = [];

		$scope.buatBaru = true;
		$scope.newModeView = false;
		$scope.PKPType = false;
		
		$scope.showBox = true;
		$scope.showIcon = false;
    }

    $scope.TambahVendor_TambahParts_Clicked = function () {

        if (
            ($scope.TambahVendor_KodeMaterial == null || $scope.TambahVendor_KodeMaterial == undefined || $scope.TambahVendor_KodeMaterial == '') ||
            ($scope.Form.TambahVendor_NamaMaterial == null || $scope.Form.TambahVendor_NamaMaterial == undefined || $scope.Form.TambahVendor_NamaMaterial == '') ||
            ($scope.Form.TambahVendor_SatuanDasar == null || $scope.Form.TambahVendor_SatuanDasar == undefined || $scope.Form.TambahVendor_SatuanDasar == '') ||
            ($scope.Form.TambahVendor_DefaultSatuan == null || $scope.Form.TambahVendor_DefaultSatuan == undefined || $scope.Form.TambahVendor_DefaultSatuan == '')
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {
            var inputData = {};
            var countPartsCodeList = 0;
            var cekstring = $scope.modelDataCheckParts.toString();
            console.log('cekstring', cekstring)
            if (cekstring === "") {
                console.log('cekstring', cekstring)

                //$scope.TambahVendor_PartsId = $scope.TambahVendor_PartsId - 1;

                var inputData = {
                    "PartsId": $scope.TambahVendor_PartsId,
                    "MaterialCode": $scope.TambahVendor_KodeMaterial,
                    "MaterialName": $scope.Form.TambahVendor_NamaMaterial,
                    // "BaseUOM": $scope.Form.TambahVendor_SatuanDasar,
                    "BaseUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                    "BaseUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'value'),
                    "DefaultUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'name'),
                    "DefaultUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'value'),
                    // "DefaultUOM": $scope.Form.TambahVendor_DefaultSatuan,
                };

                $scope.modelDataCheckParts.push($scope.TambahVendor_KodeMaterial);
                console.log('$scope.modelDataCheckParts', $scope.modelDataCheckParts)
                
                
                console.log("data Parts InputData", inputData, $scope.PartListDataJB)
                for (var x = 0; x < $scope.PartListData.length; x++) {
                    if (inputData.MaterialCode == $scope.PartListData[x].MaterialCode) {
                        countPartsCodeList++
                    }
                }
                if (countPartsCodeList > 0) {
                    bsAlert.warning("Part Sudah Digunakan");
                    return false;
                }

                $scope.PartListData.push(inputData);
                $scope.PartsList_UIGrid.data = $scope.PartListData;
                console.log('abc', $scope.PartsList_UIGrid.data, $scope.PartListData)

                $scope.TambahVendor_PartsId = "";
                $scope.TambahVendor_KodeMaterial = "";
                $scope.Form.TambahVendor_NamaMaterial = "";
                $scope.Form.TambahVendor_SatuanDasar = "";

            } else {
                console.log('cekstring1', cekstring, $scope.TambahVendor_KodeMaterial)
                if (cekstring.includes($scope.TambahVendor_KodeMaterial)) {
                    bsNotify.show({
                        title: "Tambah Parts",
                        content: "Data Parts " + $scope.TambahVendor_KodeMaterial + " Sudah Ada",
                        type: 'danger'
                    });
                    console.log("data parts", $scope.TambahVendor_KodeMaterial, cekstring)
                    return -1;
                } else {
                    //$scope.TambahVendor_PartsId = $scope.TambahVendor_PartsId - 1;

                    var inputData = {
                        "PartsId": $scope.TambahVendor_PartsId,
                        "MaterialCode": $scope.TambahVendor_KodeMaterial,
                        "MaterialName": $scope.Form.TambahVendor_NamaMaterial,
                        // "BaseUOM": $scope.Form.TambahVendor_SatuanDasar,
                        "BaseUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                        "BaseUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'value'),
                        "DefaultUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'name'),
                        "DefaultUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_DefaultSatuan) { return o.name } }), 'value'),
                        // "DefaultUOM": $scope.Form.TambahVendor_DefaultSatuan,

                    };

                    $scope.modelDataCheckParts.push($scope.TambahVendor_KodeMaterial);
                    console.log('$scope.modelDataCheckParts', $scope.modelDataCheckParts)
                }

                
                console.log("data Parts InputData", inputData, $scope.PartListDataJB)
                for (var x = 0; x < $scope.PartListData.length; x++) {
                    if (inputData.MaterialCode == $scope.PartListData[x].MaterialCode) {
                        countPartsCodeList++
                    }
                }
                if (countPartsCodeList > 0) {
                    bsAlert.warning("Part Sudah Digunakan");
                    return false;
                }

                $scope.PartListData.push(inputData);
                $scope.PartsList_UIGrid.data = $scope.PartListData;
                console.log('abc', $scope.PartsList_UIGrid.data, $scope.PartListData)

                $scope.TambahVendor_PartsId = "";
                $scope.TambahVendor_KodeMaterial = "";
                $scope.Form.TambahVendor_NamaMaterial = "";
                $scope.Form.TambahVendor_SatuanDasar = "";
                // $scope.Form.TambahVendor_DefaultSatuan = "";
            }
            $scope.modelDataCheckParts = [];
        }
    }

    $scope.TambahVendor_TambahBank_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_NamaBank == null || $scope.Form.TambahVendor_NamaBank == undefined || $scope.Form.TambahVendor_NamaBank == '') ||
            ($scope.Form.TambahVendor_AtasNamaBank == null || $scope.Form.TambahVendor_AtasNamaBank == undefined || $scope.Form.TambahVendor_AtasNamaBank == '') ||
            ($scope.Form.TambahVendor_NomorRekening == null || $scope.Form.TambahVendor_NomorRekening == undefined || $scope.Form.TambahVendor_NomorRekening == '')
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {
            var bankName = "";

            angular.forEach($scope.optionsTambahVendorNamaBank, function (value, key) {
                //console.log("value : " + value.value + " Nama : " + value.name);
                if (value.value == $scope.Form.TambahVendor_NamaBank)
                    bankName = value.name;
            });
            for(var im in $scope.BankAccountList_UIGrid.data){
                if(bankName == $scope.BankAccountList_UIGrid.data[im].BankName && $scope.Form.TambahVendor_NomorRekening == $scope.BankAccountList_UIGrid.data[im].AccountNo){
                    bsAlert.warning("Data Sudah Ada")
                    return false;
                }
            }

            var inputData = {
                "BankName": bankName,
                "AccountNo": $scope.Form.TambahVendor_NomorRekening,
                "AccountName": $scope.Form.TambahVendor_AtasNamaBank,
                "BankId": $scope.Form.TambahVendor_NamaBank,
            };
            //$scope.BankAccountList_UIGrid.data.push(inputData);
            $scope.VendorAccBankListData.push(inputData);
            $scope.BankAccountList_UIGrid.data = $scope.VendorAccBankListData;
            //console.log("index of ", $scope.optionsTambahVendorNamaBank.indexOf($scope.Form.TambahVendor_NamaBank));

            $scope.Form.TambahVendor_NamaBank = "";
            $scope.Form.TambahVendor_NomorRekening = "";
            $scope.Form.TambahVendor_AtasNamaBank = "";

        }
    }

    $scope.eventKey = function (event) {
        console.log('eventkey <>><<<>>', event);
        return (event.ctrlKey || event.altKey ||
            (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) ||
            (95 < event.keyCode && event.keyCode < 106) || (event.keyCode == 8) ||
            (event.keyCode == 9) || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46))

    };

    $scope.allowPattern = function (event, type, item) {
        console.log('masuk sini ga?');
        // if (event.charCode == 13) {
        //     $scope.getData();
        // }

        var patternRegex
        if (type == 1) {
            patternRegex = /\d/i; //NUMERIC ONLY
        } else if (type == 2) {
            patternRegex = /\d|[a-z]|[*]|[/]|[-]/i; //ALPHANUMERIC ONLY
            if (item.includes("*")) {
                event.preventDefault();
                return false;
            }
        }
        console.log("event", event);
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            console.log("allowPattern jin2", item);
            event.preventDefault();
            return false;
        }
    };

    $scope.TambahVendor_TambahPartsJB_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_NoMaterialJB == null || $scope.Form.TambahVendor_NoMaterialJB == undefined || $scope.Form.TambahVendor_NoMaterialJB == '') ||
            ($scope.Form.TambahVendor_HargaBeli == null || $scope.Form.TambahVendor_HargaBeli == undefined || $scope.Form.TambahVendor_HargaBeli == '') ||
            ($scope.Form.TambahVendor_NamaMaterialJB == null || $scope.Form.TambahVendor_NamaMaterialJB == undefined || $scope.Form.TambahVendor_NamaMaterialJB == '') ||
            ($scope.Form.TambahVendor_HargaJual == null || $scope.Form.TambahVendor_HargaJual == undefined || $scope.Form.TambahVendor_HargaJual == '') ||
            ($scope.Form.TambahVendor_TanggalAwal == null || $scope.Form.TambahVendor_TanggalAwal == undefined || $scope.Form.TambahVendor_TanggalAwal == 'Invalid Date') ||
            ($scope.Form.TambahVendor_TanggalAkhir == null || $scope.Form.TambahVendor_TanggalAkhir == undefined || $scope.Form.TambahVendor_TanggalAkhir == 'Invalid Date')
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {

            inputData = {
                "PartsJBId": $scope.TambahVendor_PartsJBId,
                "PartsId": $scope.TambahVendor_PartsIdJB,
                "DateStart": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAwal),
                "DateEnd": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAkhir),
                // "DateStart": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAwal).toISOString(), "dd/MM/yyyy"),
                // "DateEnd": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAkhir).toISOString(), "dd/MM/yyyy"),
                "MaterialNo": $scope.Form.TambahVendor_NoMaterialJB,
                "MaterialName": $scope.Form.TambahVendor_NamaMaterialJB,
                //"UOM":
                "BuyPrice": $scope.Form.TambahVendor_HargaBeli,
                "SellPrice": $scope.Form.TambahVendor_HargaJual,
                "BaseUOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                "UOM": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'name'),
                "BaseUOMId": _.result(_.find($scope.MasterVendorUoMList, function (o) { if (o.value == $scope.Form.TambahVendor_SatuanDasar) { return o.name } }), 'value'),
            };

            var countPartsCode = 0;
            console.log("data Parts InputData", inputData, $scope.PartListDataJB)
            for (var x = 0; x < $scope.PartListDataJB.length; x++) {
                if (inputData.MaterialNo == $scope.PartListDataJB[x].MaterialNo) {
                    countPartsCode++
                }
            }
            if (countPartsCode > 0) {
                bsAlert.warning("Part Sudah Digunakan");
                return false;
            }
            if (inputData.SellPrice < inputData.BuyPrice) {
                bsAlert.warning("Harga jual tidak boleh kurang dari harga beli");
                return false;
            } else if (inputData.DateStart > inputData.DateEnd) {
                bsAlert.warning("Tanggal awal tidak boleh lebih dari tanggal akhir");
                return false;
            }


            $scope.PartListDataJB.push(inputData);
            $scope.PartsJBList_UIGrid.data = $scope.PartListDataJB;

            $scope.TambahVendor_PartsJBId = 0;
            $scope.TambahVendor_PartsIdJB = 0;
            $scope.Form.TambahVendor_TanggalAwal = undefined;
            $scope.Form.TambahVendor_TanggalAkhir = undefined;
            $scope.Form.TambahVendor_NoMaterialJB = "";
            $scope.Form.TambahVendor_NamaMaterialJB = "";
            $scope.Form.TambahVendor_HargaBeli = "";
            $scope.Form.TambahVendor_HargaJual = "";

        }


    }

    $scope.TambahVendor_TambahKaroseri_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_TipeKaroseri == null || $scope.Form.TambahVendor_TipeKaroseri == undefined ) ||
            ($scope.Form.TambahVendor_KaroseriFullModel == null || $scope.Form.TambahVendor_KaroseriFullModel == undefined || $scope.Form.TambahVendor_KaroseriFullModel == '') ||
            ($scope.Form.TambahVendor_KaroseriModel == null || $scope.Form.TambahVendor_KaroseriModel == undefined || $scope.Form.TambahVendor_KaroseriModel == '') ||
            ($scope.Form.TambahVendor_KaroseriDeskripsi == null || $scope.Form.TambahVendor_KaroseriDeskripsi == undefined || $scope.Form.TambahVendor_KaroseriDeskripsi == '') ||
            ($scope.Form.TambahVendor_KaroseriCode == null || $scope.Form.TambahVendor_KaroseriCode == undefined )
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {
            var DataInput = {};
            var KaroseriTypeName = '';
            var ModelName = '';
            var StatusCode = 0;

            console.log("MasterVendorKaroseriTypeList==>", $scope.MasterVendorKaroseriTypeList, $scope.optionsTipeKaroseri, $scope.Form.TambahVendor_TipeKaroseri);
            // console.log("scope==>",$scope);
            console.log('$scope.MasterVendorVModelList', $scope.MasterVendorVModelList);

            $scope.optionsTipeKaroseri.some(function (obj, i) {
                console.log("masuuuk obj", obj);
                console.log("$scope.Form.TambahVendor_TipeKaroseri", $scope.Form.TambahVendor_TipeKaroseri);
                if (obj.value === $scope.Form.TambahVendor_TipeKaroseri) {
                    KaroseriTypeName = obj.name;
                    console.log('masuk sini', KaroseriTypeName);
                }
            });

            // $scope.optionsTipeKaroseri.some(function(obj, i) {
            //   if (obj.KaroseriId === $scope.optionsTipeKaroseri){
            //       KaroseriTypeName = obj.KaroseriName;
            //       console.log('masuk sini', KaroseriTypeName);
            //   }
            // })
            _.map($scope.optionsTipeKaroseri, function (obj) {
                if (obj.KaroseriId === $scope.Form.TambahVendor_TipeKaroseri) {
                    KaroseriTypeName = obj.KaroseriName;
                    console.log('masuk sini', KaroseriTypeName);
                }
            });
            //  console.log('$scope.MasterVendorVModelList',$scope.MasterVendorVModelList);
            $scope.MasterVendorVModelList.some(function (obj, i) {
                if (obj.VehicleModelId == $scope.Form.TambahVendor_KaroseriModel) {
                    ModelName = obj.VehicleModelName;
                    console.log('masuk sini', ModelName, obj);
                }
            });

            $scope.optionsTambahVendorKaroseriFullModel.some(function (obj, i) {
                if (obj.value === $scope.Form.TambahVendor_KaroseriFullModel) {
                    $scope.tmpNameFullModel = obj.name;
                    console.log('masuk sini', $scope.tmpNameFullModel);

                }
            });

            // $scope.MasterVendorGetVFullModel.some(function(obj, i) {
            // 	if (obj.VehicleTypeId == $scope.Form.TambahVendor_KaroseriFullModel){
            // 		FullModelName = obj.FullModel;
            // 	}
            // });
            console.log("KaroseriTypeName", KaroseriTypeName);
            console.log("$scope.tmpNameModel", $scope.tmpNameFullModel);
            $scope.KaroseriId = 0;
            DataInput = {
                "KaroseriId": $scope.KaroseriId,
                "KaroseriTypeId": $scope.Form.TambahVendor_TipeKaroseri,
                "KaroseriCode": $scope.Form.TambahVendor_KaroseriCode,
                "KaroseriType": KaroseriTypeName,
                "ModelName": $scope.tmpNameModel,
                "ModelId": $scope.Form.TambahVendor_KaroseriModel,
                "FullModelName": $scope.tmpNameFullModel,
                "FullModelId": $scope.Form.TambahVendor_KaroseriFullModel,
                "Description": $scope.Form.TambahVendor_KaroseriDeskripsi
            };

            
            console.log("data input", DataInput)
            if ($scope.KaroseriList_UIGrid.data.length != 0) {
                //console.log('data form', $scope.Form.TambahVendor_KaroseriCode)

                for (i = 0; i < $scope.KaroseriList_UIGrid.data.length; i++) {
                    console.log('masuk loop',$scope.KaroseriList_UIGrid.data);
                    if ($scope.Form.TambahVendor_KaroseriCode == $scope.KaroseriList_UIGrid.data[i].KaroseriCode && $scope.Form.TambahVendor_KaroseriModel == $scope.KaroseriList_UIGrid.data[i].ModelId && $scope.Form.TambahVendor_KaroseriFullModel == $scope.KaroseriList_UIGrid.data[i].FullModelId) {
                        StatusCode = 1;

                    }
                }
            }

            if (StatusCode == 0) {
                $scope.TambahVendorKaroseriDataList.push(DataInput);
                $scope.KaroseriList_UIGrid.data = $scope.TambahVendorKaroseriDataList;
                //console.log('cek this',$scope.KaroseriList_UIGrid.data);
                $scope.MasterVendor_setDataKaroseriList($scope.TambahVendorKaroseriDataList);
            } else {
                alert('Karoseri Code Sudah Ada');
            }

        }
        $scope.TambahVendor_ClearFields1();



    }

    $scope.TambahVendor_TambahKaroseriJB_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_KodeKaroseriJB == null || $scope.Form.TambahVendor_KodeKaroseriJB == undefined || $scope.Form.TambahVendor_KodeKaroseriJB == '') ||
            ($scope.Form.TambahVendor_HargaBeliKaroseri == null || $scope.Form.TambahVendor_HargaBeliKaroseri == undefined || $scope.Form.TambahVendor_HargaBeliKaroseri == '') ||
            ($scope.Form.TambahVendor_TipeKaroseriJB == null || $scope.Form.TambahVendor_TipeKaroseriJB == undefined || $scope.Form.TambahVendor_TipeKaroseriJB == '') ||
            ($scope.Form.TambahVendor_HargaJualKaroseri == null || $scope.Form.TambahVendor_HargaJualKaroseri == undefined || $scope.Form.TambahVendor_HargaJualKaroseri == '') ||
            ($scope.Form.TambahVendor_TanggalAwalKaroseri == null || $scope.Form.TambahVendor_TanggalAwalKaroseri == undefined || $scope.Form.TambahVendor_TanggalAwalKaroseri == 'Invalid Date') ||
            ($scope.Form.TambahVendor_TanggalAkhirKaroseri == null || $scope.Form.TambahVendor_TanggalAkhirKaroseri == undefined || $scope.Form.TambahVendor_TanggalAkhirKaroseri == 'Invalid Date')
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {
            var DataInput = {};
            for(var cc in $scope.MasterVendorVModelList){
                if($scope.Form.TambahVendor_KaroseriModelIdJB == $scope.MasterVendorVModelList[cc].VehicleModelId){
                    $scope.Form.TambahVendor_KaroseriModelJB = $scope.MasterVendorVModelList[cc].VehicleModelCode
                    break;
                }
            } 
            DataInput = {
                "KaroseriCode": $scope.Form.TambahVendor_KodeKaroseriJB,
                "ModelName": $scope.Form.TambahVendor_KaroseriModelJB,
                "ModelId" :  $scope.Form.TambahVendor_KaroseriModelIdJB,
                "FullModelId": $scope.Form.TambahVendor_KaroseriFullModelIdJB, 
                "BuyPrice": $scope.Form.TambahVendor_HargaBeliKaroseri,
                "SellPrice": $scope.Form.TambahVendor_HargaJualKaroseri,
                "DateStart": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAwalKaroseri),
                "DateEnd": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAkhirKaroseri),
                "KaroseriType": $scope.Form.TambahVendor_TipeKaroseriJB
            }

            if (DataInput.SellPrice < DataInput.BuyPrice) {
                bsAlert.warning("Harga jual tidak boleh kurang dari harga beli");
                return false;
            } else if (DataInput.DateStart > DataInput.DateEnd) {
                bsAlert.warning("Tanggal awal tidak boleh lebih dari tanggal akhir");
                return false;
            }

            console.log("data karoserijb", DataInput)
            var countKaroseriJB = 0
            for (i = 0; i < $scope.KaroseriJBList_UIGrid.data.length; i++) {
                console.log('masuk loop',$scope.KaroseriJBList_UIGrid.data);
                if ($scope.Form.TambahVendor_KodeKaroseriJB == $scope.KaroseriJBList_UIGrid.data[i].KaroseriCode && $scope.Form.TambahVendor_KaroseriModelIdJB == $scope.KaroseriJBList_UIGrid.data[i].ModelId && $scope.Form.TambahVendor_KaroseriFullModelIdJB == $scope.KaroseriJBList_UIGrid.data[i].FullModelId) {
                    countKaroseriJB++;

                }
            }

            if(countKaroseriJB > 0){
                bsAlert.warning("Karoseri Code Sudah digunakan")
                return false;
            }

            $scope.TambahVendorKaroseriJBDataList.push(DataInput);
            $scope.KaroseriJBList_UIGrid.data = $scope.TambahVendorKaroseriJBDataList;
            $scope.TambahVendor_ClearFieldsKaroseriJB();

        }


    }

    $scope.TambahVendor_Kembali_Clicked = function () {
        $scope.TambahVendor_VendorId = undefined;
        $scope.MainVendor_Show = true;
        $scope.TambahVendor_Show = false;
        $scope.modelDataCheckParts = [];
        $scope.TambahVendor_ClearFields();
        $scope.TambahVendor_ClearFields1();
        $scope.TambahVendor_ClearFieldsKaroseriJB();
        $scope.MasterVendorGetDataVendor();
		$scope.showPKP = false;
    }
	
	// CR5 #10
	$scope.onChangeCBPKP = function(data){
		console.log('$scope.TambahVendor_JenisVendor at onChangeCBPKP stv : ', $scope.TambahVendor_JenisVendor);
		
		$scope.DisPKP = false;
		
		if($scope.modeN == "new"){
			if(data == 0){
				$scope.TambahVendor_NamaNPWP = $scope.TambahVendor_NamaNPWPState;
				$scope.TambahVendor_NPWP = $scope.TambahVendor_NPWPState;
				$scope.TambahVendor_NPWPAddress = $scope.TambahVendor_NPWPAddressState;
				$scope.TambahVendor_KodePos = $scope.TambahVendor_KodePosState;
			}else if(data == 1){
				$scope.TambahVendor_NamaNPWPState = $scope.TambahVendor_NamaNPWP;
				$scope.TambahVendor_NPWPState = $scope.TambahVendor_NPWP;
				$scope.TambahVendor_NPWPAddressState = $scope.TambahVendor_NPWPAddress;
				$scope.TambahVendor_KodePosState = $scope.TambahVendor_KodePos;
				
				$scope.TambahVendor_NamaNPWP = "Non PKP";
				$scope.TambahVendor_NPWP = "00.000.000.0-000.000";
				$scope.TambahVendor_NPWPAddress = "Non PKP";
				$scope.TambahVendor_KodePos = "";
			}
		}
		
		/*if(data == 0){
			$scope.TambahVendor_NamaNPWP = $scope.TambahVendor_NamaNPWPState;
			$scope.TambahVendor_NPWP = $scope.TambahVendor_NPWPState;
			$scope.TambahVendor_NPWPAddress = $scope.TambahVendor_NPWPAddressState;
			$scope.TambahVendor_KodePos = $scope.TambahVendor_KodePosState;

			$scope.DisPKP = false;
		}else if(data == 1){
			$scope.TambahVendor_NamaNPWPState = $scope.TambahVendor_NamaNPWP;
			$scope.TambahVendor_NPWPState = $scope.TambahVendor_NPWP;
			$scope.TambahVendor_NPWPAddressState = $scope.TambahVendor_NPWPAddress;
			$scope.TambahVendor_KodePosState = $scope.TambahVendor_KodePos;
			
			$scope.TambahVendor_NamaNPWP = "Non PKP";
			$scope.TambahVendor_NPWP = "00.000.000.0-000.000";
			$scope.TambahVendor_NPWPAddress = "Non PKP";
			$scope.TambahVendor_KodePos = "";

			//$scope.DisPKP = true;
			$scope.DisPKP = false; //Always false -> return to normal
		}*/
		console.log('$scope.PKPType at changCB : ', $scope.PKPType);
	}
	// CR5 #10

    $scope.TambahVendor_Simpan_Clicked = function () {
        //console.log("Data ", $scope.VendorAccBankListData);
        var TermOfPayment = 0;
        var countEdit = 0;
        console.log("tipe vendor ", $scope.TambahVendor_JenisVendor);

        if (!angular.isUndefined($scope.TambahVendor_TermofPayment))
            TermOfPayment = $scope.TambahVendor_TermofPayment;

        if ($scope.TambahVendor_KodeVendor == undefined || $scope.TambahVendor_KodeVendor == null || $scope.TambahVendor_KodeVendor == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Kode Vendor belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_NamaVendor == undefined || $scope.TambahVendor_NamaVendor == null || $scope.TambahVendor_NamaVendor == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Nama Vendor belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_AlamatKantor == undefined || $scope.TambahVendor_AlamatKantor == null || $scope.TambahVendor_AlamatKantor == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Alamat Vendor belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_Provinsi == undefined || $scope.TambahVendor_Provinsi == null || $scope.TambahVendor_Provinsi == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Provinsi belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_Kota == undefined || $scope.TambahVendor_Kota == null || $scope.TambahVendor_Kota == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Kota belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_BusinessUnit == undefined || $scope.TambahVendor_BusinessUnit == null || $scope.TambahVendor_BusinessUnit == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Business Unit belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_JenisVendor == undefined || $scope.TambahVendor_JenisVendor == null) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Tipe Vendor belum diisi",
                type: 'danger'
            });
            return;
        }
        

        if ($scope.TambahVendor_JenisPayment == undefined || $scope.TambahVendor_JenisPayment == null || $scope.TambahVendor_JenisPayment == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Metode Pembayaran belum diisi",
                type: 'danger'
            });
            return;
        }
		
        /*if ($scope.TambahVendor_NamaNPWP == undefined || $scope.TambahVendor_NamaNPWP == null || $scope.TambahVendor_NamaNPWP == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Nama NPWP belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_NPWP == undefined || $scope.TambahVendor_NPWP == null || $scope.TambahVendor_NPWP == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Nomor NPWP belum diisi",
                type: 'danger'
            });
            return;
        }

        if ($scope.TambahVendor_NPWPAddress == undefined || $scope.TambahVendor_NPWPAddress == null || $scope.TambahVendor_NPWPAddress == "") {
            bsNotify.show({
                title: "Master Vendor",
                content: "Alamat NPWP belum diisi",
                type: 'danger'
            });
            return;
        }*/
		
		// CR5 #10
		if($scope.PKPType == true){
			$scope.PKPTypeTemp = 1;
		}else if($scope.PKPType == false){
			if ($scope.TambahVendor_NamaNPWP == undefined || $scope.TambahVendor_NamaNPWP == null || $scope.TambahVendor_NamaNPWP == "") {
				bsNotify.show({
					title: "Master Vendor",
					content: "Nama NPWP belum diisi",
					type: 'danger'
				});
				return;
			}

			if ($scope.TambahVendor_NPWP == undefined || $scope.TambahVendor_NPWP == null || $scope.TambahVendor_NPWP == "") {
				bsNotify.show({
					title: "Master Vendor",
					content: "Nomor NPWP belum diisi",
					type: 'danger'
				});
				return;
			}

			if ($scope.TambahVendor_NPWPAddress == undefined || $scope.TambahVendor_NPWPAddress == null || $scope.TambahVendor_NPWPAddress == "") {
				bsNotify.show({
					title: "Master Vendor",
					content: "Alamat NPWP belum diisi",
					type: 'danger'
				});
				return;
			}
			$scope.PKPTypeTemp = 0;
		}
		// CR5 #10
		
        for (var g in $scope.PartListDataJB) {
            $scope.PartListDataJB[g].DateStart = $scope.changeFormatDate($scope.PartListDataJB[g].DateStart)
            $scope.PartListDataJB[g].DateEnd = $scope.changeFormatDate($scope.PartListDataJB[g].DateEnd)
        }

        if($scope.TambahVendor_JenisVendor == null){
            $scope.TambahVendor_JenisVendor = 0
        }

        if($scope.TambahVendor_GroupList == 0){
            $scope.TambahVendor_GroupList = ""
        }

		/*if($scope.PKPType == true){
			$scope.PKPTypeTemp = 1;
		}else if($scope.PKPType == false){
			$scope.PKPTypeTemp = 0;
		}*/

        var inputData = [{
            "OutletId": user.OutletId,
            "VendorId": $scope.TambahVendor_VendorId,
            "VendorCode": $scope.TambahVendor_KodeVendor,
            "Name": $scope.TambahVendor_NamaVendor,
            "Address": $scope.TambahVendor_AlamatKantor,
            "ProvinceId": $scope.TambahVendor_Provinsi,
            "CityRegencyId": $scope.TambahVendor_Kota,
            "NPWP": $scope.TambahVendor_NPWP,
            "PostalCode": $scope.TambahVendor_KodePos,
            "NPWPName": $scope.TambahVendor_NamaNPWP,
            "NPWPAddress": $scope.TambahVendor_NPWPAddress,
            "Email": $scope.TambahVendor_Email,
            "Phone": $scope.TambahVendor_Telepon,
            "Fax": $scope.TambahVendor_Fax,
            "ContactPerson": $scope.TambahVendor_KontakPerson,
            "CPTelephone": $scope.TambahVendor_NomorKontakPerson,
            "BusinessUnitId": $scope.TambahVendor_BusinessUnit,
            "isAfco": $scope.TambahVendor_JenisVendor,
            "TermofPayment": TermOfPayment,
            "PerformanceBit": $scope.TambahVendor_isPerformanceEvaluation ? 1 : 0,
            "BlockingBit": $scope.TambahVendor_isBlocking ? 1 : 0,
            "isSPLD": $scope.TambahVendor_isSPLD,
            "SubletId": $scope.TambahVendor_TipeSublet,
            "VendorPartsList": $scope.PartListData,
            "VendorPartsJBList": $scope.PartListDataJB,
            "VendorAccBankList": $scope.VendorAccBankListData,
            "VendorUoMList": $scope.MasterVendorUoMData,
            "VendorServiceList": $scope.MasterVendorJasaData,
            "VendorServiceJBList": $scope.MasterVendorJasaJBData,
            "VendorKaroseriList": $scope.TambahVendorKaroseriDataList,
            "VendorKaroseriJBList": $scope.TambahVendorKaroseriJBDataList,
            "PaymentMethodId": $scope.TambahVendor_JenisPayment,
            "VendorOutletId": $scope.TambahVendor_VendorOutlet ? $scope.TambahVendor_VendorOutlet : null,
            "Prosentase": $scope.TambahVendor_Prosentase,
            "GroupId": $scope.TambahVendor_GroupList,
            "VendorTypeId": $scope.TambahVendor_JenisVendor,
			"IsNONPKP": $scope.PKPTypeTemp
        }];
        console.log("input Data", inputData, $scope.PartListData, $scope.PartListDataJB, $scope.MasterVendorJasaJBData, $scope.MasterVendorJasaData)
        var countharga = 0
        var tmpArrayC = [];
        for (var x in $scope.PartListDataJB) {
            console.log("data parts", $scope.PartListDataJB[x], $scope.PartListDataJB)
            for (var z in $scope.PartListData) {
                if ($scope.PartListDataJB[x].PartsId == $scope.PartListData[z].PartsId) {
                    tmpArrayC.push($scope.PartListDataJB[x].PartsId)
                }
            }
        }

        console.log("bebek", tmpArrayC);
        console.log("$scope.PartListDataJB", $scope.PartListDataJB);
        console.log("$scope.PartListData", $scope.PartListData);
        if ($scope.PartListData.length !== tmpArrayC.length) {
            countharga++
        } else if ($scope.PartListDataJB.length !== $scope.PartListData.length) {
            bsAlert.warning("Tidak dapat hapus Data Part & bahan harus hapus part dan harga jual beli")
            return false;
        }
        if (countharga > 0) {
            bsAlert.warning("Data part & bahan tidak akan tersimpan karena harga belum diinput");
            return false;
        }

        var CountKaroseri = 0
        var tmpArrayK = [];
        for (var x in $scope.TambahVendorKaroseriJBDataList) {
            for (var z in $scope.TambahVendorKaroseriDataList) {   
                if ($scope.TambahVendorKaroseriJBDataList[x].KaroseriCode == $scope.TambahVendorKaroseriDataList[z].KaroseriCode && $scope.TambahVendorKaroseriJBDataList[x].ModelId == $scope.TambahVendorKaroseriDataList[z].ModelId && $scope.TambahVendorKaroseriJBDataList[x].FullModelId == $scope.TambahVendorKaroseriDataList[z].FullModelId) {
                    tmpArrayK.push($scope.TambahVendorKaroseriJBDataList[x].KaroseriCode)
                }
            }
        }
        if ($scope.TambahVendorKaroseriDataList.length !== tmpArrayK.length) {
            CountKaroseri++
        } else if ($scope.TambahVendorKaroseriJBDataList.length !== $scope.TambahVendorKaroseriDataList.length) {
            bsAlert.warning("Tidak dapat hapus Data Karoseri harus hapus harga jual beli")
            return false;
        }
        if (CountKaroseri > 0) {
            bsAlert.warning("Data Karoseri tidak akan tersimpan karena harga belum diinput");
            return false;
        }

        var counthargaJasa = 0
        var tmpArrayJasa = [];
        for (var x in $scope.MasterVendorJasaJBData) {
            console.log("data jasa", $scope.MasterVendorJasaJBData[x], $scope.MasterVendorJasaJBData)
            for (var c in $scope.MasterVendorJasaData) {
                if ($scope.MasterVendorJasaJBData[x].ServiceId == $scope.MasterVendorJasaData[c].ServiceId) {
                    tmpArrayJasa.push($scope.MasterVendorJasaJBData[x].ServiceId)
                }
            }
        }

        console.log("jasa opl jasa", $scope.MasterVendorJasaData.length, "===>", $scope.MasterVendorJasaJBData.length)
        if ($scope.MasterVendorJasaData.length !== tmpArrayJasa.length) {
            counthargaJasa++
        } else if ($scope.MasterVendorJasaData.length !== $scope.MasterVendorJasaJBData.length) {
            console.log("jasa opl", $scope.MasterVendorJasaData.length, "===>", $scope.MasterVendorJasaJBData.length)
            bsAlert.warning("Tidak dapat hapus Jasa harus hapus Jasa dan harga jual beli")
            return false;
        }
        if (counthargaJasa > 0) {
            bsAlert.warning("Jasa tidak akan tersimpan karena harga belum diinput");
            return false;
        }
        console.log("mode",$scope.mode)
        if ($scope.mode == "new") {
            if ($scope.TambahVendor_VendorId == undefined) {
                $scope.TambahVendor_VendorId = 0
            }else if($scope.TambahVendor_VendorId != undefined){
                $scope.TambahVendor_VendorId = $scope.TambahVendor_VendorId
            }
                console.log("vendorId",$scope.TambahVendor_VendorId)
                MasterVendorFactory.CekExistingVendor($scope.TambahVendor_VendorId, $scope.TambahVendor_KodeVendor, $scope.TambahVendor_BusinessUnit)
                    .then(
                        function (res) {
                            if (res.data[0] == 'true') {
                                var countData = 0
                                for(var i in $scope.DataVendor){
                                    if($scope.DataVendor[i].BusinessUnitId == 9 && $scope.DataVendor[i].VendorTypeId == 4){
                                        countData++
                                        break;
                                    }
                                }
                                if(countData > 0){
                                    for(var j in inputData){
                                        if(inputData[j].BusinessUnitId == 9 && inputData[j].VendorTypeId == 4){
                                            bsAlert.warning("Vendor Parts & Tipe TAM Sudah Terdaftar")
                                            return false
                                        }
                                    }
                                }
                                if($scope.TambahVendor_GroupList == undefined || $scope.TambahVendor_GroupList == "NULL" || $scope.TambahVendor_GroupList == ""){
                                    $scope.TambahVendor_GroupList = 0
                                }
                                MasterVendorFactory.CekExistingVendorGroup($scope.TambahVendor_VendorId,$scope.TambahVendor_GroupList)
                                .then(
                                    function (res) {
                                        if (res.data[0] == 'true') {
                                            if($scope.TambahVendor_GroupList == 0){
                                                $scope.TambahVendor_GroupList = ""
                                            }
                                            MasterVendorFactory.create(inputData)
                                            .then(
                                                function (res) {
                                                    //alert("Data berhasil disimpan");
                                                    console.log('datavendor', res)
                                                    bsAlert.alert({
                                                        title: "Sukses",
                                                        text: "Data berhasil di Disimpan",
                                                        type: "success",
                                                        showCancelButton: false
                                                    });
                                                    $scope.VendorList_UIGrid_Paging(1);
                                                    $scope.TambahVendor_Kembali_Clicked();
                                                    //console.log("coba", inputData);
                                                },
                                                function (err) {
                                                    alert(err.data.ExceptionMessage);
                                                }
                                            );
                                        } else {
                                            bsNotify.show({
                                                title: "Master Vendor",
                                                content: "Vendor Lain Sudah Terdaftar di Group ini" ,
                                                type: 'danger'
                                            });
                                        }
                                    });
                            } else {
                                bsNotify.show({
                                    title: "Master Vendor",
                                    content: "Kode Vendor & Business Unit  sudah terdaftar",
                                    type: 'danger'
                                });
                            }
                        });
            
            $scope.mData.TambahVendor_KodeMaterial = "";
            $scope.Form.TambahVendor_SatuanDasar = "";
            $scope.Form.TambahVendor_NoMaterialJB = "";
            $scope.Form.TambahVendor_KodeJasaJB = "",
                $scope.Form.TambahVendor_NamaJasaJB = "",
                $scope.Form.TambahVendor_ModelJB = "",
                $scope.Form.TambahVendor_HargaJualJasa = "",
                $scope.Form.TambahVendor_HargaBeliJasa = "",
                $scope.Form.TambahVendor_TanggalAwalJasaJB = "",
                $scope.Form.TambahVendor_TanggalAkhirJasaJB = ""
        } else {
            MasterVendorFactory.CekExistingVendor($scope.TambahVendor_VendorId, $scope.TambahVendor_KodeVendor, $scope.TambahVendor_BusinessUnit)
                .then(
                    function (res) {
                        if (res.data[0] == 'true') {
                            if($scope.TambahVendor_GroupList == undefined || $scope.TambahVendor_GroupList == "NULL" || $scope.TambahVendor_GroupList == ""){
                                $scope.TambahVendor_GroupList = 0
                            }
                            MasterVendorFactory.CekExistingVendorGroup($scope.TambahVendor_VendorId,$scope.TambahVendor_GroupList)
                                .then(
                                    function (res) {
                                        if (res.data[0] == 'true') {
                                            if($scope.TambahVendor_GroupList == 0){
                                                $scope.TambahVendor_GroupList = ""
                                            }
                                            MasterVendorFactory.create(inputData)
                                            .then(
                                                function (res) {
                                                    //alert("Data berhasil disimpan");
                                                    console.log('datavendor', res)
                                                    bsAlert.alert({
                                                        title: "Sukses",
                                                        text: "Data berhasil di Disimpan",
                                                        type: "success",
                                                        showCancelButton: false
                                                    });
                                                    $scope.VendorList_UIGrid_Paging(1);
                                                    $scope.TambahVendor_Kembali_Clicked();
                                                    //console.log("coba", inputData);
                                                },
                                                function (err) {
                                                    alert(err.data.ExceptionMessage);
                                                }
                                            );
                                        } else {
                                            bsNotify.show({
                                                title: "Master Vendor",
                                                content: "Vendor Lain Sudah Terdaftar di Group ini" ,
                                                type: 'danger'
                                            });
                                        }
                                    });
                        } else {
                            bsNotify.show({
                                title: "Master Vendor",
                                content: "Kode Vendor & Business Unit  sudah terdaftar",
                                type: 'danger'
                            });
                        }
                    });

            $scope.mData.TambahVendor_KodeMaterial = "";
            $scope.Form.TambahVendor_SatuanDasar = "";
            $scope.Form.TambahVendor_NoMaterialJB = "";
            $scope.Form.TambahVendor_KodeJasaJB = "",
                $scope.Form.TambahVendor_NamaJasaJB = "",
                $scope.Form.TambahVendor_ModelJB = "",
                $scope.Form.TambahVendor_HargaJualJasa = "",
                $scope.Form.TambahVendor_HargaBeliJasa = "",
                $scope.Form.TambahVendor_TanggalAwalJasaJB = "",
                $scope.Form.TambahVendor_TanggalAkhirJasaJB = ""
        }

    }

    $scope.TambahVendorPartsUOMConversion = function (row) {
        var UOMConversionData = {};
        $scope.ModalTambahVendorPartsUoMConversion_KodeParts = row.entity.MaterialCode;
        $scope.ModalTambahVendorPartsUoMConversion_PartsId = row.entity.PartsId;
        $scope.ModalTambahVendorPartsUoMConversionUIGrid.data = [];
        angular.forEach($scope.MasterVendorUoMData, function (value, key) {
            if (value.PartsId == row.entity.PartsId) {
                UOMConversionData = {
                    "PartsId": value.PartsId,
                    "Satuan": value.Satuan,
                    "NilaiKonversi": value.NilaiKonversi,
                    "SatuanKonversi": value.SatuanKonversi
                }
                $scope.ModalTambahVendorPartsUoMConversionUIGrid.data.push(UOMConversionData);
            }
        });
        $scope.ModalTambahVendorPartsUoMConversion = true;
        angular.element('#ModalTambahVendorPartsUoMConversion').modal('show');
    }

    $scope.ModalTambahVendorPartsUoMConversion_Kembali = function () {
        angular.element('#ModalTambahVendorPartsUoMConversion').modal('hide');
        $scope.ModalTambahVendorPartsUoMConversion = false;
    }

    $scope.ModalTambahVendorPartsUoMConversion_Tambah_Clicked = function () {
        $scope.ModalTambahVendorPartsUoMConversionUIGrid.data.push({ PartsId: $scope.ModalTambahVendorPartsUoMConversion_PartsId });
    }

    $scope.ModalTambahVendorPartsUoMConversion_Simpan = function () {
        var rowIndex = -1;

        //console.log("isi : ", $scope.ModalTambahVendorPartsUoMConversionUIGrid);

        angular.forEach($scope.ModalTambahVendorPartsUoMConversionUIGrid.data, function (value, key) {
            $scope.MasterVendorUoMData.some(function (obj, i) {
                return obj.PartsId === value.PartsId ? rowIndex = i : false;
            });
            //rowIndex = $scope.MasterVendorUoMData.indexOf(value.PartsId);
            //console.log("row index ", rowIndex);
            //console.log("value.PartsId", value.PartsId);

            if (rowIndex != -1) {
                //console.log("masuk splice dengan row index : ", rowIndex);
                //console.log("Master Vendor UOM Data sebelum splice : ", $scope.MasterVendorUoMData);
                $scope.MasterVendorUoMData.splice(rowIndex, 1);
                //console.log("Master Vendor UOM Data sesudah splice : ", $scope.MasterVendorUoMData);
                rowIndex = -1;
            }

        });

        angular.forEach($scope.ModalTambahVendorPartsUoMConversionUIGrid.data, function (value, key) {
            $scope.MasterVendorUoMData.push({
                "PartsId": value.PartsId,
                "Satuan": value.Satuan,
                "NilaiKonversi": value.NilaiKonversi,
                "SatuanKonversi": value.SatuanKonversi
            });
        });

        $scope.ModalTambahVendorPartsUoMConversionUIGrid.data = [];
        $scope.ModalTambahVendorPartsUoMConversion_Kembali();
        //console.log("master vendor uom data : ", $scope.MasterVendorUoMData);
    }

    $scope.MainVendor_DownloadTemplate_Clicked = function () {
        $scope.MainVendor_DownloadExcelTemplate();
    }

    $scope.MainVendor_DownloadExcelTemplate = function () {
        var excelData = [];
        var excelCity = [];
        var excelBusinesUnit = [];
        var excelTypeVendor = [];
        var excelAlldata = [];
        var fileName = "Vendor_Template";

        var excelSheetFormat = {
            "Kode_Vendor": "varchar(50)",
            "Nama_Vendor": "varchar(50)",
            "Provinsi": "varchar(50)",
            "Alamat_Kantor": "text",
            "Kota": "varchar(50)",
            "Nama_NPWP": "varchar(50)",
            "Nomor_NPWP": "Numerik(15)",
            "Kode_Pos": "Numerik",
            "Alamat_NPWP": "text",
            "Email": "varchar(50)",
            "Telepon": "Numerik",
            "Nomor_Kontak_Person": "Numerik",
            "Fax": "Numerik",
            "Nama_Kontak_Person": "varchar(50)",
            "Business_Unit": "varchar(50)",
            "Tipe_Vendor": "varchar(50)",
            "Term_of_Payment": "Numerik",
            "Metode_Pembayaran": "varchar(10)",
            "Performance_Evaluation": "Bolean",
            "Blocking_Status": "Bolean"
        };

        var excelSheetExample = {
            "Kode_Vendor": "9110000294",
            "Nama_Vendor": "PT. PADA MAKMUR BARU",
            "Provinsi": "Lampung",
            "Alamat_Kantor": "Jln.Raya bandar Jaya Lampung",
            "Kota": "Bandar Lampung, Kota",
            "Nama_NPWP": "PT. PADA MAKMUR BARU",
            "Nomor_NPWP": "999999999999999",
            "Kode_Pos": "16770",
            "Alamat_NPWP": "Tunas Toyota Bandar Jaya Lampung",
            "Email": "test@gmail.com",
            "Telepon": "025163728193",
            "Nomor_Kontak_Person": "084726371892",
            "Fax": "9",
            "Nama_Kontak_Person": "Ahmad Nurdin",
            "Business_Unit": "Parts",
            "Tipe_Vendor": "Aff Co",
            "Term_of_Payment": "2",
            "Metode_Pembayaran": "Tunai/Tagihan",
            "Performance_Evaluation": "1",
            "Blocking_Status": "0"
        };

        var excelSheetSeparator = {
            "Kode_Vendor": "-",
            "Nama_Vendor": "-",
            "Provinsi": "-",
            "Alamat_Kantor": "-",
            "Kota": "-",
            "Nomor_NPWP": "",
            "Kode_Pos": "ISI",
            "Alamat_NPWP": "SETELAH",
            "Email": "BARIS",
            "Telepon": "INI",
            "Nomor_Kontak_Person": "-",
            "Fax": "-",
            "Nama_Kontak_Person": "-",
            "Business_Unit": "-",
            "Tipe_Vendor": "-",
            "Term_of_Payment": "-",
            "Metode_Pembayaran": "-",
            "Performance_Evaluation": "-",
            "Blocking_Status": "-"
        };

        var excelProvinsi = {"Provinsi":""};
        excelCity.push(excelProvinsi)
        for(var n=0; n< $scope.optionsTambahVendorProvinsi.length;n++){
			console.log("Provinsi", $scope.optionsTambahVendorProvinsi[n].name)
			
			var excelProvinsi1 = {"Provinsi":$scope.optionsTambahVendorProvinsi[n].name};	
			excelCity.push(excelProvinsi1);	
        }

        var excelBisnis = {"Bisnis Unit":""};
        excelBusinesUnit.push(excelBisnis)
        for(var n=0; n< $scope.optionsTambahVendorBusinessUnit.length;n++){
			console.log("Bisnis Unit", $scope.optionsTambahVendorBusinessUnit[n].name)
			
			var excelBisnis1 = {"Bisnis Unit":$scope.optionsTambahVendorBusinessUnit[n].name};	
			excelBusinesUnit.push(excelBisnis1);	
        }

        var excelTipeVendor = {"Tipe Vendor":""};
        excelTypeVendor.push(excelTipeVendor)
        for(var n=0; n< $scope.optionsMainVendorJenisVendor.length;n++){
			console.log("Tipe Vendor", $scope.optionsMainVendorJenisVendor[n].name)
			
			var excelTipeVendor1 = {"Tipe Vendor":$scope.optionsMainVendorJenisVendor[n].name};	
			excelTypeVendor.push(excelTipeVendor1);	
        }

        
        
        excelData.push(excelSheetFormat);
        excelData.push(excelSheetExample);
        excelData.push(excelSheetSeparator);

        excelAlldata.push(excelData);
        excelAlldata.push(excelCity);
        excelAlldata.push(excelBusinesUnit);
        excelAlldata.push(excelTypeVendor);

        XLSXInterface.writeToXLSX(excelAlldata, fileName, [fileName, "Provinsi", "Bisnis Unit", "Tipe Vendor"]);
    }

    $scope.MainVendor_Upload_Clicked = function () {
        angular.element(document.querySelector('#UploadMasterVendor')).val(null);
        angular.element('#ModalUploadMasterVendor').modal('show');
        $scope.ModalUploadMasterVendor = true;
    }

    $scope.loadXLSMasterVendor = function (ExcelFile) {
        //console.log("Debug", debug)
        var myEl = angular.element(document.querySelector('#UploadMasterVendor')); //ambil elemen dari dokumen yang di-upload

        console.log("myEl", myEl[0].files[0]);
        XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
            ExcelItem = json.Vendor_Template[4];
            exTmp = json.Vendor_Template;
            $scope.ExcelData = $scope.ExcelData.slice(3)
            console.log("data", $scope.ExcelData)
            for (var y = 0; y < exTmp.length; y++) {
                var formatKey = Object.keys(exTmp[y]);
                var tmpReformatKey = angular.copy(formatKey);
                console.log("formatkey", formatKey,exTmp[y])
                if ((exTmp[y].Kode_Vendor != null && exTmp[y].Kode_Vendor != "" && exTmp[y].Kode_Vendor != undefined)||
                    (exTmp[y].Nama_Vendor != null && exTmp[y].Nama_Vendor != "" && exTmp[y].Nama_Vendor != undefined)) {
                    console.log("extmp", exTmp[y], json);
                    $scope.ExcelData.push(exTmp[y]);
                }
            }
            console.log("ExcelData : ", $scope.ExcelData);
            console.log("Extmp : ", exTmp);
            console.log("myEl : ", myEl);
            console.log("json : ", json);
        });

    }

    $scope.setAddress = function(data,i){
		if(i > (data.length - 1) ){
			return
		}
        var row = data[i];
		MasterVendorFactory.getDataKotaVendor(row.ProvinceId).then(function(resu) {
            $scope.Kota = resu.data.Result;
            var itungKota = 0;
			for(var x in $scope.Kota){
                if ($scope.Kota[x].CityRegencyName != undefined){
                    $scope.Kota[x].CityRegencyName = $scope.Kota[x].CityRegencyName.toUpperCase()
                    var real_kota = angular.copy($scope.Kota[x].CityRegencyName)
                    if($scope.Kota[x].CityRegencyName.includes('.')){
                        var tmpCityRegencyName = []
                        if ($scope.Kota[x].CityRegencyName.includes('KAB')){
                            tmpCityRegencyName = $scope.Kota[x].CityRegencyName.split("KAB. ");                    
                        } else if ($scope.Kota[x].CityRegencyName.includes('KOTA')) {
                            tmpCityRegencyName = $scope.Kota[x].CityRegencyName.split("KOTA. ");   
                        } else {
                            tmpCityRegencyName = $scope.Kota[x].CityRegencyName.split(". ");  
                        }
                        tmpCityRegencyName = tmpCityRegencyName.slice(1)
                        $scope.Kota[x].CityRegencyName = tmpCityRegencyName[0].trim()
                        console.log('Kab ===>',tmpCityRegencyName, $scope.Kota[x].CityRegencyName);
                    }else{
                        if ($scope.Kota[x].CityRegencyName.includes("KOTA")) {
                            tmpCityRegencyName = $scope.Kota[x].CityRegencyName.split("KOTA ");
                        } else {
                            tmpCityRegencyName = $scope.Kota[x].CityRegencyName
                        }
                        tmpCityRegencyName = tmpCityRegencyName.slice(1)
                        $scope.Kota[x].CityRegencyName = tmpCityRegencyName[0]
                        console.log('Kota ===>',tmpCityRegencyName, $scope.Kota[x].CityRegencyName);
                    }   
                    console.log("oppo",$scope.Kota[x])
                    
                    if(row.CityRegencyName.toLowerCase() == $scope.Kota[x].CityRegencyName.toLowerCase() || row.CityRegencyName.toLowerCase() == real_kota.toLowerCase()){
                        row.CityRegencyId = $scope.Kota[x].CityRegencyId
                        break;
                    }else{
                        itungKota++
                    }
    
                    if(itungKota == $scope.Kota.length ){
                        bsAlert.warning("Format Data Ada yang Salah","Kota Tidak Ditemukan")
                        console.log("kota11", row.CityRegencyName.toLowerCase(), "==", $scope.Kota[x].CityRegencyName.toLowerCase() )
                        return false;
                    }
                }
                
            }
					data[i] = row;
                    console.log("data",data);
                    var CountCekData = 0
                    for(var rr in $scope.DataVendor){
                        for(var yy in data){
                            if(data[yy].VendorCode == $scope.DataVendor[rr].VendorCode){
                                CountCekData++
                                break;
                            }
                        }
                    }
                    if(CountCekData >0){
                        bsAlert.warning("Code Vendor Sudah Ada")
                        return false;
                    }
                    
					// ===== End Section of Row
					if (i < (data.length - 1)) {
						$scope.setAddress(data, i + 1);
						
						$scope.ariavalue += parseInt(80/data.length)
						$scope.ariavaluecss = parseInt(80/data.length) + '%';
	
					} else if ((data.length - 1) == i) {
                        // return false;
						MasterVendorFactory.create(data)
                            .then(
                                function (res) {

                                    // angular.element('#UploadMasterVendor').modal('hide');
                                    // angular.element(document.querySelector('#UploadMasterVendor')).val(null);
                                    // angular.element('#UploadMasterVendor').modal('hide');
                                    angular.element(document.querySelector('#UploadMasterVendor')).val(null);
                                    //alert("Data berhasil disimpan");
                                    bsAlert.alert({
                                        title: "Upload Berhasil",
                                        text: "Data berhasil di upload",
                                        type: "success",
                                        showCancelButton: false
                                    });

                                    $scope.VendorList_UIGrid_Paging(1);
                                    $scope.ModalUploadMasterVendor = false;
                                    //console.log("input data coba", inputData);
                                },
                                function (err) {
                                    //console.log("err : ", err);
                                }
                            );
						$scope.ariavalue = 100;
						$scope.ariavaluecss = '100%';
						$timeout(function() {
							// ngDialog.closeAll();
							// // bsAlert.alert({
							// // 	title: "Upload Berhasil",
							// // 	text: "Data berhasil di upload",
							// // 	type: "success",
							// // 	showCancelButton: false
							// // },
							// // 	function () {
							// // 		$scope.showUploadCUstomer = false;
							// // 	},
							// // );
							// $scope.showUploadCUstomer = false;
						}, 500);
					}
				});
    }
    
    $scope.CancelUploadFile = function(){
		$scope.ModalUploadMasterVendor = false;
		$scope.ExcelData = [];
	}

    $scope.UploadMasterVendor_Upload_Clicked = function () {
        var inputData = [];
        var eachData = {};
        var counter = 0;
        var tmpIdx = 1;
        var tmpResult = []
        var tmpResult1 = []
        var countError = 0

        $scope.ExcelData = $scope.ExcelData.slice(3)
        console.log("$scope.ExcelData", $scope.ExcelData);
        var lengthTmp = angular.copy($scope.ExcelData.length)
        // ======
        var TmpMatch = [];
        // var match = _.find($scope.ExcelData, {Kode_Vendor:row.idx});
        // if(match !== -1){
        //     TmpMatch.push(match);
        // }
        // for(var gc=0;gc<TmpMatch.length;gc++){
        //     if(TmpMatch > 1){
        //         bsAlert.warning("lala")
        //     }
        // }
        
        var sortByProperty = function (property) {
            return function (x, y) {
                return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1)); //sorting data
            };
        };
        var tmpExcelData = angular.copy($scope.ExcelData.sort(sortByProperty('Kode_Vendor')));
        var tmpExcelTipeVendor = angular.copy($scope.ExcelData.sort(sortByProperty('Kode_Vendor')));
        var tmpExcelTipePembayaran = angular.copy($scope.ExcelData.sort(sortByProperty('Kode_Vendor')));
        var tmpExcelBisnisUnit = angular.copy($scope.ExcelData.sort(sortByProperty('Kode_Vendor')));
        var checkDuplicateInObject = function(propertyName, inputArray) {
            var seenDuplicate = false, // ini Flag default buat ada duplikat atau nggak
                testObject = {}; // ini variable object buat nampung temporary yg mau dicek ex:'kode vendor'
            
            _.map(inputArray, function(item) { //  ini looping map atau mapping ulang
              var itemPropertyName = item[propertyName]; 
              if (itemPropertyName in testObject) { // cek jika value loop terkini itu ada di temporary
                testObject[itemPropertyName].duplicate = true; // otomatis ngasih flag ini gk terlalu penting sih tapi klo mau dpake nantinya boleh
                item.duplicate = true; // ini juga gk terlalu penting
                seenDuplicate = true; // ini otomatis flag bakal true, klo ada yg duplikat
              }
              else {
                testObject[itemPropertyName] = item; // masukin value dari propertinya ke temporary
                delete item.duplicate;
              }
            });
            
            return seenDuplicate;
          }
          if(checkDuplicateInObject('Kode_Vendor', tmpExcelData )){
                bsAlert.warning("Kode Vendor duplikat, silahkan cek kembali")
                return false;
          }  
        
        // ====
        var lookup = {};
          for(var item, idx = 0; item = tmpExcelData[idx++];){
              var name = item.Provinsi;
              console.log("kambing",name)
              if(!(name in lookup))	{
                  lookup[name] = 1;
                  tmpResult.push(name);
              }
  
          }
          tmpExcelData = tmpResult;
          console.log('=====>>',tmpResult, tmpExcelData);
          if(tmpExcelData != undefined && tmpExcelData != "" && tmpExcelData != 0){
              for(var xx in tmpExcelData){
                  var itungModel =1;
                  tmpExcelData[xx] = (tmpExcelData[xx]).toLowerCase();
                  for(var x in $scope.optionsTambahVendorProvinsi){
                      if(tmpExcelData[xx] == $scope.optionsTambahVendorProvinsi[x].name.toLowerCase() ){
                        //   tmpDataModelId.push($scope.optionsTambahVendorProvinsi[x].value);
                          console.log("modelid", tmpExcelData[xx], $scope.optionsTambahVendorProvinsi[x].name.toLowerCase() )
                          break;
                      }else{
                          itungModel++
                      }
                      if(itungModel == $scope.optionsTambahVendorProvinsi.length ){
                          console.log("itungan", itungModel, $scope.optionsTambahVendorProvinsi.length)
                          bsAlert.warning("Format Data Ada yang Salah","Provinsi Tidak Ditemukan")
                          ngDialog.closeAll();
                          return false;
                      }
                  }
              }
          }

            var lookup1 = {};
		for(var item, idx = 0; item = tmpExcelTipeVendor[idx++];){
            var name1 = item.Tipe_Vendor;
            console.log("kucing",name1)
			if(!(name1 in lookup1))	{
				lookup1[name1] = 1;
				tmpResult1.push(name1);
			}

		}
		tmpExcelTipeVendor = tmpResult1;
		console.log("List Satuan ==>",tmpExcelTipeVendor, tmpResult1)
		if(tmpExcelTipeVendor != undefined && tmpExcelTipeVendor != "" && tmpExcelTipeVendor != 0){
			for(var cc in tmpExcelTipeVendor){
				var itungModel1 =0;
				tmpExcelTipeVendor[cc] = (tmpExcelTipeVendor[cc]).toLowerCase();
				for(var a in $scope.optionsMainVendorJenisVendor){
                    console.log("Satuan123abc", tmpExcelTipeVendor[cc], $scope.optionsMainVendorJenisVendor[a].name.toLowerCase() )
					if(tmpExcelTipeVendor[cc] == $scope.optionsMainVendorJenisVendor[a].name.toLowerCase() ){
						var tmpVendorTypeId = $scope.optionsMainVendorJenisVendor[a].value;
						console.log("Satuan123", tmpExcelTipeVendor[cc], $scope.optionsMainVendorJenisVendor[a].name.toLowerCase() )
						break;
					}else{
						itungModel1++
					}
					console.log("satuan", tmpExcelTipeVendor[cc], itungModel1)
					if(itungModel1 == $scope.optionsMainVendorJenisVendor.length ){
						console.log("itungan1", itungModel1, $scope.optionsMainVendorJenisVendor.length)
						bsAlert.warning("Format Data Ada yang Salah", "Tipe Vendor Tidak Ditemukan")
						ngDialog.closeAll();
						return false;
					}
				}
			}
        }
        
        var lookup2 = {};
        var tmpResult2 = [];
		for(var item, idx = 0; item = tmpExcelTipePembayaran[idx++];){
            var name2 = item.Metode_Pembayaran;
            console.log("kucing",name2)
			if(!(name2 in lookup2))	{
				lookup2[name2] = 1;
				tmpResult2.push(name2);
			}

		}
		tmpExcelTipePembayaran = tmpResult2;
		console.log("List Pembayaran ==>",tmpExcelTipePembayaran, tmpResult2)
		if(tmpExcelTipePembayaran != undefined && tmpExcelTipePembayaran != "" && tmpExcelTipePembayaran != 0){
			for(var pp in tmpExcelTipePembayaran){
				var itungModel2 =0;
				tmpExcelTipePembayaran[pp] = (tmpExcelTipePembayaran[pp]).toLowerCase();
				for(var b in $scope.PaymentMethodData){
                    console.log("Pembayaran123abc", tmpExcelTipePembayaran[pp], $scope.PaymentMethodData[b].Name.toLowerCase() )
					if(tmpExcelTipePembayaran[pp] != $scope.PaymentMethodData[b].Name.toLowerCase() ){
						// var tmpVendorTypeId = $scope.PaymentMethodData[b].value;
						// console.log("Pembayaran123", tmpExcelTipePembayaran[pp], $scope.PaymentMethodData[b].Name.toLowerCase() )
                        // break;
                        itungModel2++
					}
					console.log("Pembayaran", tmpExcelTipeVendor[pp], itungModel2)
					if(itungModel2 == $scope.PaymentMethodData.length ){
						console.log("itungan1", itungModel2, $scope.PaymentMethodData.length)
						bsAlert.warning("Format Data Ada yang Salah", "Tipe Pembayaran Tidak Ditemukan")
						ngDialog.closeAll();
						return false;
					}
				}
			}
        }

        var lookup3 = {};
        var tmpResult3 = [];
		for(var item, idx = 0; item = tmpExcelBisnisUnit[idx++];){
            var name3 = item.Business_Unit;
            console.log("kucing",name3)
			if(!(name3 in lookup3))	{
				lookup3[name3] = 1;
				tmpResult3.push(name3);
			}

		}
		tmpExcelBisnisUnit = tmpResult3;
		console.log("List Bisnis ==>",tmpExcelBisnisUnit, tmpResult3)
		if(tmpExcelBisnisUnit != undefined && tmpExcelBisnisUnit != "" && tmpExcelBisnisUnit != 0){
			for(var hh in tmpExcelBisnisUnit){
				var itungModel3 =0;
				tmpExcelBisnisUnit[hh] = (tmpExcelBisnisUnit[hh]).toLowerCase();
				for(var d in $scope.optionsTambahVendorBusinessUnit){
                    console.log("Bisnis123abc", tmpExcelBisnisUnit[hh], $scope.optionsTambahVendorBusinessUnit[d].name.toLowerCase() )
					if(tmpExcelBisnisUnit[hh] != $scope.optionsTambahVendorBusinessUnit[d].name.toLowerCase() ){
                        itungModel3++
						// var tmpVendorTypeId = $scope.optionsTambahVendorBusinessUnit[d].value;
						// console.log("Bisnis123", tmpExcelBisnisUnit[hh], $scope.optionsTambahVendorBusinessUnit[d].name.toLowerCase() )
						// break;
					}
					console.log("Bisnis", tmpExcelBisnisUnit[hh], itungModel3)
					if(itungModel3 == $scope.optionsTambahVendorBusinessUnit.length ){
						console.log("itungan1", itungModel3, $scope.optionsTambahVendorBusinessUnit.length)
						bsAlert.warning("Format Data Ada yang Salah", "Bisnis unit Tidak Ditemukan")
						ngDialog.closeAll();
						return false;
					}
				}
			}
        }
        
          angular.forEach($scope.ExcelData, function (value, key) {
            if((value.Kode_Vendor == null || value.Kode_Vendor == "" || value.Kode_Vendor == undefined)||
            (value.Nama_Vendor == null || value.Nama_Vendor == "" || value.Nama_Vendor == undefined)||
            (value.Provinsi == null || value.Provinsi == "" || value.Provinsi == undefined)||
            (value.Business_Unit == null || value.Business_Unit == "" || value.Business_Unit == undefined)||
            (value.Alamat_Kantor == null || value.Alamat_Kantor == "" || value.Alamat_Kantor == undefined)||
            (value.Kota == null || value.Kota == "" || value.Kota == undefined)||
            (value.Tipe_Vendor == null || value.Tipe_Vendor == "" || value.Tipe_Vendor == undefined)||
            (value.Nama_NPWP == null || value.Nama_NPWP == "" || value.Nama_NPWP == undefined)||
            (value.Nomor_NPWP == null || value.Nomor_NPWP == "" || value.Nomor_NPWP == undefined)||
            (value.Alamat_NPWP == null || value.Alamat_NPWP == "" || value.Alamat_NPWP == undefined)||
            (value.Metode_Pembayaran == null || value.Metode_Pembayaran == "" || value.Metode_Pembayaran == undefined)
            ){
                bsAlert.warning("Data Masih Ada Yang Kosong")
                return false;
            }else{
                for(var z in $scope.PaymentMethodData){
                    if(value.Metode_Pembayaran.toLowerCase() == $scope.PaymentMethodData[z].Name.toLowerCase()){
                        value.Metode_Pembayaran = $scope.PaymentMethodData[z].PaymentMethodId
                        break;
                    }
                }
                
                for(var xx in $scope.optionsTambahVendorProvinsi){
                    if(value.Provinsi.toLowerCase() == $scope.optionsTambahVendorProvinsi[xx].name.toLowerCase()){
                        value.ProvinceId = $scope.optionsTambahVendorProvinsi[xx].value
                        break;
                    }
                }
                
                for(var cc in $scope.optionsTambahVendorBusinessUnit){
                    if(value.Business_Unit.toLowerCase() == $scope.optionsTambahVendorBusinessUnit[cc].name.toLowerCase()){
                        value.BusinessUnitId = $scope.optionsTambahVendorBusinessUnit[cc].value
                        break;
                    }
                }

                var new_tipeVendor = null
                for(var pp in $scope.optionsMainVendorJenisVendor){
                    console.log("lala",$scope.optionsMainVendorJenisVendor)
                    if(value.Tipe_Vendor.toLowerCase() == $scope.optionsMainVendorJenisVendor[pp].name.toLowerCase()){
                        // var tmpVendorTypeId = $scope.optionsMainVendorJenisVendor[pp].value
                        new_tipeVendor = $scope.optionsMainVendorJenisVendor[pp].value
                        break;
                    }
                }
                
                row = value;
            // if (counter > 2) {
                eachData = {
                    "OutletId": user.OutletId,
                    "VendorCode": value.Kode_Vendor,
                    "Name": value.Nama_Vendor,
                    "Address": value.Alamat_Kantor,
                    "ProvinceId": value.ProvinceId,
                    "ProvinceName": value.Provinsi,
                    "CityRegencyId": 0,
                    "CityRegencyName": value.Kota,
                    "NPWPName": value.Nama_NPWP,
                    "NPWP": value.Nomor_NPWP,
                    "PostalCode": value.Kode_Pos,
                    "NPWPAddress": value.Alamat_NPWP,
                    "Email": value.Email,
                    "Phone": value.Telepon,
                    "Fax": value.Fax,
                    "ContactPerson": value.Nama_Kontak_Person,
                    "CPTelephone": value.Nomor_Kontak_Person,
                    "BusinessTypeId": value.BusinessUnitId,
                    "BusinessUnitName": value.Business_Unit,
                    "isAfcoText": value.Tipe_Vendor,
                    "TermofPayment": value.Term_of_Payment,
                    "PaymentMethodId": value.Metode_Pembayaran,
                    "PerformanceBit": value.Performance_Evaluation ? 1 : 0,
                    "BlockingBit": value.Blocking_Status ? 1 : 0,
                    "isSPLD": 0,
                    "VendorPartsList": "",
                    "VendorAccBankList": "",
                    "VendorUoMList": "",
                    // "VendorTypeId": $scope.getVendorTypeId(value.Tipe_Vendor),
                    // "VendorTypeId": tmpVendorTypeId,
                    "VendorTypeId": new_tipeVendor,
                    "idx": tmpIdx

                }

                inputData.push(eachData);
            // };
            // counter = counter + 1;
            tmpIdx += 1;
            console.log("data  :", inputData, inputData.VendorCode);
            console.log("eachData", eachData,$scope.VendorList_UIGrid.data);
            console.log("data input sbelum masuk factory :", inputData, inputData.VendorCode);
            };
        });


        $scope.setAddress(inputData, 0)
        // return false;
        // MasterVendorFactory.create(inputData)
        //     .then(
        //         function (res) {

        //             // angular.element('#UploadMasterVendor').modal('hide');
        //             // angular.element(document.querySelector('#UploadMasterVendor')).val(null);
        //             // angular.element('#UploadMasterVendor').modal('hide');
        //             angular.element(document.querySelector('#UploadMasterVendor')).val(null);
        //             //alert("Data berhasil disimpan");
        //             bsAlert.alert({
        //                 title: "Upload Berhasil",
        //                 text: "Data berhasil di upload",
        //                 type: "success",
        //                 showCancelButton: false
        //             });

        //             $scope.VendorList_UIGrid_Paging(1);
        //             $scope.ModalUploadMasterVendor = false;
        //             //console.log("input data coba", inputData);
        //         },
        //         function (err) {
        //             //console.log("err : ", err);
        //         }
        //     );
    }

    $scope.MainVendor_Filter_Clicked = function () {
        $scope.VendorList_UIGrid_Paging(1);
    }

    $scope.MainVendorEditRow = function (row) {
        $scope.mode = "edit";
		$scope.modeN = "edit";
		$scope.showPKP = false;
		
		$scope.showBox = true;
		$scope.showIcon = false;

        console.log('MainVendorEditRow >> ', row);
        // var Afco = 0;
        // if (row.entity.isAfco)
        //     Afco = 1;

		// CR5 #10
		$scope.newModeView = false;
		$scope.buatBaru = false;

		if(row.entity.IsNONPKP == 0){
			$scope.PKPType = false;
			$scope.DisPKP = false;
		}else if(row.entity.IsNONPKP == 1){
			$scope.PKPType = true;
			//$scope.DisPKP = true;
			$scope.DisPKP = false; //Always false -> return to normal
		}
		console.log('$scope.PKPType at edit : ', $scope.PKPType);
		// CR5 #10

        $scope.TambahVendor_isDisable_ViewMode = false;
        $scope.TambahVendor_KodeVendor = row.entity.VendorCode;
        $scope.TambahVendor_VendorId = row.entity.VendorId;
        $scope.TambahVendor_NamaVendor = row.entity.Name;
        $scope.TambahVendor_Provinsi = row.entity.ProvinceId;
        $scope.TambahVendor_BusinessUnit = row.entity.BusinessUnitId;
        $scope.TambahVendor_AlamatKantor = row.entity.Address;
        $scope.TambahVendor_Kota = row.entity.CityRegencyId;
        $scope.TambahVendor_JenisVendor = row.entity.isAfco;
        $scope.TambahVendor_NPWP = row.entity.NPWP;
        $scope.TambahVendor_KodePos = row.entity.PostalCode;
        $scope.TambahVendor_TermofPayment = row.entity.TermofPayment;
        $scope.TambahVendor_NPWPAddress = row.entity.NPWPAddress;
        $scope.TambahVendor_NamaNPWP = row.entity.NPWPName;
        $scope.TambahVendor_Email = row.entity.Email;
        $scope.TambahVendor_Telepon = row.entity.Phone;
        $scope.TambahVendor_NomorKontakPerson = row.entity.CPTelephone;
        $scope.TambahVendor_Fax = row.entity.Fax;
        $scope.TambahVendor_KontakPerson = row.entity.ContactPerson;
        $scope.TambahVendor_TipeSublet = row.entity.SubletId;
        $scope.TambahVendor_JenisVendor = row.entity.VendorTypeId;
        $scope.TambahVendor_isPerformanceEvaluation = row.entity.PerformanceBit;
        $scope.TambahVendor_isBlocking = row.entity.BlockingBit;
        $scope.TambahVendor_JenisPayment = row.entity.PaymentMethodId;
        $scope.TambahVendor_Prosentase = row.entity.Prosentase;
        $scope.TambahVendor_GroupList = row.entity.GroupId;
        $scope.TambahVendor_JenisVendor = row.entity.VendorTypeId;

        if(row.entity.BusinessUnitName == "OPL"){
            $scope.MasterVendor_getDataJasaList($scope.TambahVendor_VendorId);
            console.log("tambah jasa", $scope.MasterVendor_getDataJasaList);
            $scope.MasterVendor_getDataJasaJBList($scope.TambahVendor_VendorId);
            console.log("tambah harga jasa", $scope.MasterVendor_getDataJasaJBList);
        }else if(row.entity.BusinessUnitName == "Karoseri"){
            $scope.MasterVendor_getDataKaroseriList($scope.TambahVendor_VendorId);
            $scope.MasterVendor_getDataKaroseriJBList($scope.TambahVendor_VendorId);
        }else{
            $scope.MasterVendor_GetDataPartsList($scope.TambahVendor_VendorId);
            $scope.MasterVendor_GetDataPartsListUOMConv($scope.TambahVendor_VendorId);
            $scope.MasterVendor_getDataPartsJBList($scope.TambahVendor_VendorId);
        }
            

        // $scope.MasterVendor_GetDataPartsList($scope.TambahVendor_VendorId);
        // $scope.MasterVendor_GetDataPartsListUOMConv($scope.TambahVendor_VendorId);
        // $scope.MasterVendor_getDataAccBankList($scope.TambahVendor_VendorId, $scope.TambahVendor_BusinessUnit);
        // $scope.MasterVendor_getDataPartsJBList($scope.TambahVendor_VendorId);
        // $scope.MasterVendor_getDataKaroseriList($scope.TambahVendor_VendorId);
        // $scope.MasterVendor_getDataKaroseriJBList($scope.TambahVendor_VendorId);
        // $scope.MasterVendor_getDataJasaList($scope.TambahVendor_VendorId);
        // console.log("tambah jasa", $scope.MasterVendor_getDataJasaList);
        // $scope.MasterVendor_getDataJasaJBList($scope.TambahVendor_VendorId);
        // console.log("tambah Part", $scope.MasterVendor_getDataJasaJBList);

        $scope.MasterVendor_getDataAccBankList($scope.TambahVendor_VendorId, $scope.TambahVendor_BusinessUnit);

        $scope.MainVendor_Show = false;
        $scope.TambahVendor_Show = true;
        $scope.mData.TambahVendor_KodeMaterial = "";
        $scope.Form.TambahVendor_SatuanDasar = "";
		
		console.log('$scope.TambahVendor_JenisVendor at stv : ', $scope.TambahVendor_JenisVendor);
    };


    $scope.MainVendorDeleteRow = function (row) {
        bsAlert.alert({
            title: "Apakah Anda Yakin Menghapus Vendor " + row.entity.Name + " ?",
            content: "",
            type: "question",
            showCancelButton: true
        },
            function () {
                MasterVendorFactory.HapusVendorOPL(row.entity.VendorId).then(function (res) {
                    if (res.data == 666){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Tidak bisa hapus vendor karena sudah ada transaksi",
                        });
                    } else {
                        bsNotify.show({
                            size: 'big',
                            title: 'Hapus Vendor Berhasil',
                            text: '',
                            // timeout: 5000,
                            type: 'success'
                        });
                        $scope.VendorList_UIGrid_Paging(1);
                    }
                })
            }
        );
    }


    $scope.TambahVendor_BusinessUnit_Changed = function () {

        var BusinessUnitName = '';
        if (!isNaN($scope.TambahVendor_BusinessUnit)) {
            angular.forEach($scope.MasterVendor_BusinessUnitList, function (value, key) {
                //console.log("value.BusinessUnitId :", value.value);
                if (value.value == $scope.TambahVendor_BusinessUnit) {
                    BusinessUnitName = value.name;
                    console.log("nama bisnis", BusinessUnitName, $scope.TambahVendor_BusinessUnit, value.value)
                }
            });
            //console.log("BusinessUnitName :", BusinessUnitName);
            if (BusinessUnitName == 'OPL') {
                //console.log("masuk OPL");
                $scope.BusinessUnitNameTMP = '';
                $scope.TambahVendor_Satuan_Show = false;
                $scope.TambahVendor_SatuanJB_Show = false;
                $scope.TambahVendor_Karoseri_Show = false;
                $scope.TambahVendor_Jasa_Show = true;
                $scope.TambahVendor_JasaJB_Show = true;
                $scope.TambahVendor_Bank_Show = true;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = false;
                $scope.activeJustified = 1;
            } else if (BusinessUnitName == 'Internal') {
                $scope.BusinessUnitNameTMP = '';
                $scope.TambahVendor_Satuan_Show = true;
                $scope.TambahVendor_SatuanJB_Show = true;
                $scope.TambahVendor_Karoseri_Show = false;
                $scope.TambahVendor_Jasa_Show = false;
                $scope.TambahVendor_JasaJB_Show = false;
                $scope.TambahVendor_Bank_Show = false;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = false;
                $scope.activeJustified = 0;
            } else if (BusinessUnitName == 'Mediator' || BusinessUnitName == 'General Transaction' || BusinessUnitName == 'Bank') {
                
                $scope.BusinessUnitNameTMP = '';
                $scope.TambahVendor_Satuan_Show = false;
                $scope.TambahVendor_SatuanJB_Show = false;
                $scope.TambahVendor_Karoseri_Show = false;
                $scope.TambahVendor_Jasa_Show = false;
                $scope.TambahVendor_JasaJB_Show = false;
                $scope.TambahVendor_Bank_Show = true;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = false;
            }
            else if( BusinessUnitName == 'Accrued Free Service'){
                $scope.BusinessUnitNameTMP = 'Accrued Free Service';
                // console.log("$scope.optionsMainVendorJenisVendor",$scope.optionsMainVendorJenisVendor);
                // var filtered = $scope.optionsMainVendorJenisVendor.filter(function(item) { 
                //                    return item.value == 4;  
                // });
                // console.log("filtered",filtered);
                $scope.TambahVendor_JenisVendor = 3;
                $scope.TambahVendor_Satuan_Show = false;
                $scope.TambahVendor_SatuanJB_Show = false;
                $scope.TambahVendor_Karoseri_Show = false;
                $scope.TambahVendor_Jasa_Show = false;
                $scope.TambahVendor_JasaJB_Show = false;
                $scope.TambahVendor_Bank_Show = true;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = false; 
            }
            //show karoseri
            else if (BusinessUnitName == 'Karoseri') {
               
                $scope.BusinessUnitNameTMP = '';
                $scope.TambahVendor_Satuan_Show = false;
                $scope.TambahVendor_SatuanJB_Show = false;
                $scope.TambahVendor_Karoseri_Show = true;
                $scope.TambahVendor_Jasa_Show = false;
                $scope.TambahVendor_JasaJB_Show = false;
                $scope.TambahVendor_Bank_Show = true;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = true;
                //$scope.getKaroseri();
            } else {
                
                $scope.BusinessUnitNameTMP = '';
                $scope.TambahVendor_Satuan_Show = true;
                $scope.TambahVendor_SatuanJB_Show = true;
                $scope.TambahVendor_Karoseri_Show = false;
                $scope.TambahVendor_Jasa_Show = false;
                $scope.TambahVendor_JasaJB_Show = false;
                $scope.TambahVendor_Bank_Show = true;
                $scope.TambahVendorNotInternal_Show = true;
                $scope.TambahVendor_KaroseriJB_Show = false;

				console.log('$scope.mode at business unit changed: ', $scope.mode);
				$scope.showPKP = false;
				if($scope.modeN == 'new'){
					$scope.TambahVendor_NamaNPWP = "";
					$scope.TambahVendor_NPWP = "";
					$scope.TambahVendor_NPWPAddress = "";
					$scope.TambahVendor_KodePos = "";
				}
				if($scope.TambahVendor_BusinessUnit == 9){
					if($scope.TambahVendor_JenisVendor == 1){
						$scope.showPKP = true;
					}else{
						$scope.PKPType = false;

						//if($scope.mode == 'new'){
						/*if($scope.modeN == 'new' || $scope.modeN == 'edit'){
							$scope.TambahVendor_NamaNPWP = "";
							$scope.TambahVendor_NPWP = "";
							$scope.TambahVendor_NPWPAddress = "";
							$scope.TambahVendor_KodePos = "";
						}*/
					}
				}else{
					$scope.PKPType = false;

					//if($scope.mode == 'new'){
					/*if($scope.modeN == 'new' || $scope.modeN == 'edit'){
						$scope.TambahVendor_NamaNPWP = "";
						$scope.TambahVendor_NPWP = "";
						$scope.TambahVendor_NPWPAddress = "";
						$scope.TambahVendor_KodePos = "";
					}*/
				}
            }

            console.log("nomor ", $scope.TambahVendor_BusinessUnit);
        }
    }

    $scope.TambahVendor_TambahJasa_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_KodeJasa == null || $scope.Form.TambahVendor_KodeJasa == undefined || $scope.Form.TambahVendor_KodeJasa == '') ||
            ($scope.Form.TambahVendor_Model == null || $scope.Form.TambahVendor_Model == undefined || $scope.Form.TambahVendor_Model == '') ||
            ($scope.Form.TambahVendor_NamaPekerjaan == null || $scope.Form.TambahVendor_NamaPekerjaan == undefined || $scope.Form.TambahVendor_NamaPekerjaan == '') ||
            ($scope.Form.TambahVendor_TipeJasa == null || $scope.Form.TambahVendor_TipeJasa == undefined)
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        } else {
            var ModelName = '';
            var isEksternal = false;

            $scope.MasterVendorVModelList.some(function (obj, i) {
                if (obj.VehicleModelId == $scope.Form.TambahVendor_Model) {
                    ModelName = obj.VehicleModelName;
                }
            });

            if ($scope.Form.TambahVendor_TipeJasa == 1)
                isEksternal = true;

            var inputData = {
                "ServiceId": $scope.TambahVendor_ServiceId,
                "ServiceCode": $scope.Form.TambahVendor_KodeJasa,
                "ServiceName": $scope.Form.TambahVendor_NamaPekerjaan,
                "isExternal": isEksternal,
                "ModelId": $scope.Form.TambahVendor_Model,
                "ModelName": ModelName
                //"ModelName": $scope.Form.TambahVendor_Model
            };

            var countServiceCode = 0;
            for (var c = 0; c < $scope.MasterVendorJasaData.length; c++) {
                if (inputData.ServiceCode + inputData.ModelName == $scope.MasterVendorJasaData[c].ServiceCode + $scope.MasterVendorJasaData[c].ModelName) {
                    countServiceCode++
                }
            };
    
            if (countServiceCode > 0) {
                bsAlert.warning("Jasa Sudah Digunakan");
                return false;
            };

            $scope.MasterVendorJasaData.push(inputData);
            $scope.JasaList_UIGrid.data = $scope.MasterVendorJasaData;
            $scope.TambahVendor_ServiceId = $scope.TambahVendor_ServiceId + 1;
            $scope.Form.TambahVendor_KodeJasa = "";
            $scope.Form.TambahVendor_NamaPekerjaan = "";
            $scope.Form.TambahVendor_Model = undefined;
            $scope.Form.TambahVendor_TipeJasa = undefined;

        }


    }

    $scope.TambahVendor_TambahJasaJB_Clicked = function () {

        if (
            ($scope.Form.TambahVendor_KodeJasaJB == null || $scope.Form.TambahVendor_KodeJasaJB == undefined || $scope.Form.TambahVendor_KodeJasaJB == '') ||
            ($scope.Form.TambahVendor_HargaBeliJasa == null || $scope.Form.TambahVendor_HargaBeliJasa == undefined || $scope.Form.TambahVendor_HargaBeliJasa == '') ||
            ($scope.Form.TambahVendor_NamaJasaJB == null || $scope.Form.TambahVendor_NamaJasaJB == undefined || $scope.Form.TambahVendor_NamaJasaJB == '') ||
            ($scope.Form.TambahVendor_HargaJualJasa == null || $scope.Form.TambahVendor_HargaJualJasa == undefined || $scope.Form.TambahVendor_HargaJualJasa == '') ||
            ($scope.Form.TambahVendor_TanggalAwalJasaJB == null || $scope.Form.TambahVendor_TanggalAwalJasaJB == undefined || $scope.Form.TambahVendor_TanggalAwalJasaJB == 'Invalid Date') ||
            ($scope.Form.TambahVendor_TanggalAkhirJasaJB == null || $scope.Form.TambahVendor_TanggalAkhirJasaJB == undefined || $scope.Form.TambahVendor_TanggalAkhirJasaJB == 'Invalid Date')
        ) {
            bsNotify.show({
                title: "Master Vendor",
                content: "Data masih ada yang kosong!",
                type: 'danger'
            });
            return false;
        }

        var inputData = {
            "ServiceId": $scope.TambahVendor_ResultSearchServiceId,
            "ServiceCode": $scope.Form.TambahVendor_KodeJasaJB,
            "ServiceName": $scope.Form.TambahVendor_NamaJasaJB,
            "ModelName": $scope.Form.TambahVendor_ModelJB,
            "SellPrice": $scope.Form.TambahVendor_HargaJualJasa,
            "BuyPrice": $scope.Form.TambahVendor_HargaBeliJasa,
            // "DateStart": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAwalJasaJB.toLocaleString()), 'yyyy-MM-dd'),
            // "DateEnd": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAkhirJasaJB.toLocaleString()), 'yyyy-MM-dd')
            "DateStart": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAwalJasaJB),
            "DateEnd": $scope.changeFormatDate($scope.Form.TambahVendor_TanggalAkhirJasaJB),
            // "DateStart": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAwalJasaJB.toISOString()), 'dd/MM/yyyy'),
            // "DateEnd": $filter('date')(new Date($scope.Form.TambahVendor_TanggalAkhirJasaJB.toISOString()), 'dd/MM/yyyy')
        }
        console.log('tambah inputdata xxx', inputData);
        console.log('t$scope.MasterVendorJasaJBData', $scope.MasterVendorJasaJBData);
        var countServiceCode = 0;
        for (var c = 0; c < $scope.MasterVendorJasaJBData.length; c++) {
            if (inputData.ServiceCode + inputData.ModelName == $scope.MasterVendorJasaJBData[c].ServiceCode + $scope.MasterVendorJasaJBData[c].ModelName) {
                countServiceCode++
            }
        }
        // for (var c = 0; c < $scope.MasterVendorJasaJBData.length; c++) {
        //     if (inputData.ServiceCode == $scope.MasterVendorJasaJBData[c].ServiceCode) {
        //         countServiceCode++
        //     }
        // }

        if (countServiceCode > 0) {
            bsAlert.warning("Jasa Sudah Digunakan");
            return false;
        }
        if (inputData.SellPrice < inputData.BuyPrice) {
            bsAlert.warning("Harga jual tidak boleh kurang dari harga beli");
            return false;
        } else if (inputData.DateStart > inputData.DateEnd) {
            bsAlert.warning("Tanggal awal tidak boleh lebih dari tanggal akhir");
            return false;
        }


        console.log('tambah kodejasa', $scope.MasterVendorJasaJBData, $scope.Form.TambahVendor_KodeJasaJB)
        console.log('tambah inputdata', inputData)
        $scope.MasterVendorJasaJBData.push(inputData);
        $scope.JasaJBList_UIGrid.data = $scope.MasterVendorJasaJBData;
        $scope.TambahVendor_ServiceJBId = $scope.TambahVendor_ServiceJBId + 1;
        $scope.Form = {};

    }
    $scope.changeFormatDate = function (item) {
        var tmpDate = item;
        tmpDate = new Date(tmpDate);
        var finalDate
        var yyyy = tmpDate.getFullYear().toString();
        var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
        var dd = tmpDate.getDate().toString();
        finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
        console.log("changeFormatDate finalDate", finalDate);
        return finalDate;
    }


    $scope.TambahVendor_ClearFields = function () {
        $scope.MasterVendorJasaData = [];
        $scope.MasterVendorUoMData = [];
        $scope.TambahVendor_ServiceId = 0;
        $scope.MasterVendorJasaJBData = [];
        $scope.TambahVendor_ServiceJBId = 0;
        $scope.PartListData = [];
        $scope.VendorAccBankListData = [];
        $scope.TambahVendorKaroseriJBDataList = [];
        $scope.TambahVendorKaroseriDataList = [];
        $scope.PartsList_UIGrid.data = $scope.PartListData;
        $scope.JasaList_UIGrid.data = $scope.MasterVendorJasaData;
        $scope.BankAccountList_UIGrid.data = $scope.VendorAccBankListData;
        $scope.PartsJBList_UIGrid.data = [];
        $scope.JasaJBList_UIGrid.data = $scope.MasterVendorJasaJBData;
        // $scope.KaroseriJBList_UIGrid.data = [];
        // $scope.KaroseriList_UIGrid.data = [];
        

        $scope.TambahVendor_KodeVendor = '';
        $scope.TambahVendor_NamaVendor = '';
        $scope.TambahVendor_Provinsi = undefined;
        $scope.TambahVendor_BusinessUnit = undefined;
        $scope.TambahVendor_AlamatKantor = '';
        $scope.TambahVendor_Kota = undefined;
        $scope.TambahVendor_JenisVendor = undefined;
        $scope.TambahVendor_NPWP = '';
        $scope.TambahVendor_TermofPayment = '';
        $scope.TambahVendor_NPWPAddress = '';
        $scope.TambahVendor_NamaNPWP = '';
        $scope.TambahVendor_Email = '';
        $scope.TambahVendor_Telepon = '';
        $scope.TambahVendor_NomorKontakPerson = '';
        $scope.TambahVendor_Fax = '';
        $scope.TambahVendor_TipeSublet = undefined;
        $scope.TambahVendor_KontakPerson = '';
        $scope.TambahVendor_KodePos = '';
        $scope.TambahVendor_isPerformanceEvaluation = '';
        $scope.TambahVendor_isBlocking = '';
        $scope.TambahVendor_KodeMaterial = '';
        $scope.Form.TambahVendor_SatuanDasar = '';
        $scope.Form.TambahVendor_NamaMaterial = '';

    }

    $scope.TambahVendor_ClearFields1 = function () {
        $scope.Form.TambahVendor_KaroseriModel = '';
        $scope.Form.TambahVendor_TipeKaroseri = '';
        $scope.Form.TambahVendor_KaroseriFullModel = '';
        $scope.Form.TambahVendor_KaroseriDeskripsi = '';
        $scope.Form.TambahVendor_KaroseriCode = '';
    }

    $scope.TambahVendor_ClearFieldsKaroseriJB = function(){
        $scope.Form.TambahVendor_TipeKaroseriJB ='';
        $scope.Form.TambahVendor_KodeKaroseriJB = '';
        $scope.Form.TambahVendor_TanggalAwalKaroseri= '';
        $scope.Form.TambahVendor_TanggalAkhirKaroseri= '';
        $scope.Form.TambahVendor_HargaBeliKaroseri = '';
        $scope.Form.TambahVendor_HargaJualKaroseri = '';
    }

    $scope.MainVendor_HapusFilter_Clicked = function () {
        $scope.MainVendor_KodeVendor = '';
        $scope.MainVendor_NamaVendor = '';
        $scope.MainVendor_JenisVendor = undefined;
        $scope.MainVendor_BusinessType = undefined;
		$scope.PKPTypeF = undefined;
    }

    $scope.TambahVendorPartsHapus = function (row) {
        //console.log("$scope.MasterVendorUoMData", $scope.MasterVendorUoMData);
        //console.log("$scope.PartsListData", $scope.PartListData);
        if (!angular.isUndefined($scope.MasterVendorUoMData)) {
            $scope.MasterVendorUoMData.some(function (obj, i) {
                if (obj.PartsId === row.entity.PartsId) {
                    $scope.MasterVendorUoMData.splice(i, 1);
                }
            });
        }

        if (!angular.isUndefined($scope.PartListData)) {
            $scope.PartListData.some(function (obj, i) {
                if (obj.PartsId === row.entity.PartsId) {
                    $scope.PartListData.splice(i, 1);
                }
                if ($scope.modelDataCheckParts[i] == row.entity.MaterialCode) {
                    delete $scope.modelDataCheckParts[i];
                }
            });
        }

        //console.log("after $scope.MasterVendorUoMData", $scope.MasterVendorUoMData);
        //console.log("after $scope.PartsListData", $scope.PartListData);

        if (!angular.isUndefined($scope.PartListData)) {
            $scope.PartsList_UIGrid.data = $scope.PartListData;
        } else {
            $scope.PartsList_UIGrid.data = [];
        }


    }

    $scope.TambahVendorPartsJasaDelete = function (row) {
        var selectedRowtoDelete = $scope.GridApiMasterVendorJasaList.selection.getSelectedRows();
        console.log('nah ini sel', selectedRowtoDelete, $scope.MasterVendorJasaData)
        if (selectedRowtoDelete.length == 0) {
            bsAlert.alert({
                title: "Pilih Data Terlebih Dahulu",
                text: "",
                type: "error",
                showCancelButton: false
            })
        } else {
            bsAlert.alert({
                title: "Apakah Anda Yakin Menghapus " + selectedRowtoDelete.length + " Jasa?",
                content: "",
                type: "question",
                showCancelButton: true
            },
                function () {
                    for (var i = 0; i < selectedRowtoDelete.length; i++) {
                        for (var j = 0; j < $scope.MasterVendorJasaData.length; j++) {
                            if (selectedRowtoDelete[i].ServiceId == $scope.MasterVendorJasaData[j].ServiceId) {
                                $scope.MasterVendorJasaData.splice(j, 1);
                            }
                        }
                    }
                    $scope.JasaList_UIGrid.data = $scope.MasterVendorJasaData;
                    bsNotify.show({
                        size: 'big',
                        title: 'Hapus Jasa Berhasil',
                        text: '',
                        // timeout: 5000,
                        type: 'success'
                    });
                }
            );
        }
        // if (!angular.isUndefined($scope.MasterVendorJasaData)) {
        //     $scope.MasterVendorJasaData.some(function(obj, i) {
        //         if (obj.ServiceCode === row.entity.ServiceCode) {
        //             $scope.MasterVendorJasaData.splice(i, 1);
        //             billlllllllllllllllllll
        //         }
        //     });
        //     $scope.JasaList_UIGrid.data = $scope.MasterVendorJasaData;
        // }
    }

    $scope.TambahVendorPartsJBDelete = function (row) {
        if (!angular.isUndefined($scope.PartListDataJB)) {
            $scope.PartListDataJB.some(function (obj, i) {
                if (obj.PartsId === row.entity.PartsId) {
                    console.log('bbb', row, obj)
                    $scope.PartListDataJB.splice(i, 1);
                }
            });
            $scope.PartsJBList_UIGrid.data = $scope.PartListDataJB;
        }
    }


    $scope.TambahVendorKaroseriDelete = function (row) {
        if (!angular.isUndefined($scope.TambahVendorKaroseriDataList)) {
            $scope.TambahVendorKaroseriDataList.some(function (obj, i) {
                if (obj.KaroseriCode === row.entity.KaroseriCode && obj.ModelId === row.entity.ModelId && obj.FullModelId === row.entity.FullModelId) {
                    $scope.TambahVendorKaroseriDataList.splice(i, 1);
                    console.log('hapus cuy', $scope.TambahVendorKaroseriDataList)
                }
            });
        }
    }

    $scope.TambahVendorKaroseriJBDelete = function (row) {
        if (!angular.isUndefined($scope.TambahVendorKaroseriJBDataList)) {
            $scope.TambahVendorKaroseriJBDataList.some(function (obj, i) {
                if (obj.KaroseriCode === row.entity.KaroseriCode && obj.ModelId === row.entity.ModelId && obj.FullModelId === row.entity.FullModelId) {
                    $scope.TambahVendorKaroseriJBDataList.splice(i, 1);
                }
            });
        }
    }

    $scope.TambahVendorServiceJBDelete = function (row) {
        var selectedRowtoDelete1 = $scope.GridApiPartsJBList1.selection.getSelectedRows();
        console.log('nah ini sel', selectedRowtoDelete1, $scope.MasterVendorJasaJBData)
        if (selectedRowtoDelete1.length == 0) {
            bsAlert.alert({
                title: "Pilih Data Terlebih Dahulu",
                text: "",
                type: "error",
                showCancelButton: false
            })
        } else {
            bsAlert.alert({
                title: "Apakah Anda Yakin Menghapus " + selectedRowtoDelete1.length + " Data?",
                content: "",
                type: "question",
                showCancelButton: true
            },
                function () {
                    for (var i = 0; i < selectedRowtoDelete1.length; i++) {
                        for (var j = 0; j < $scope.MasterVendorJasaJBData.length; j++) {
                            if (selectedRowtoDelete1[i].ServiceJBId == $scope.MasterVendorJasaJBData[j].ServiceJBId) {
                                $scope.MasterVendorJasaJBData.splice(j, 1);
                            }
                        }
                    }
                    $scope.JasaJBList_UIGrid.data = $scope.MasterVendorJasaJBData;
                    bsNotify.show({
                        size: 'big',
                        title: 'Hapus Data Berhasil',
                        text: '',
                        // timeout: 5000,
                        type: 'success'
                    });
                }
            );
        }
        // if (!angular.isUndefined($scope.MasterVendorJasaJBData)) {
        //     $scope.MasterVendorJasaJBData.some(function(obj, i) {
        //         if (obj.ServiceCode === row.entity.ServiceCode) {
        //             console.log('aaa',row , obj)
        //             $scope.MasterVendorJasaJBData.splice(i, 1);
        //         }
        //     });
        //     $scope.JasaJBList_UIGrid.data = $scope.MasterVendorJasaJBData;
        // }
    }

    $scope.MainVendorLihatRow = function (row) {
        console.log('woooowwwww', row)
        $scope.MainVendorEditRow(row);
        $scope.TambahVendor_isDisable_ViewMode = true;
		$scope.modeN = "view";
		
		$scope.showBox = false;
		$scope.showIcon = false;
		
		$scope.showPKP = false;
		if($scope.TambahVendor_BusinessUnit == 9){
			if($scope.TambahVendor_JenisVendor == 1){
				$scope.showPKP = true;
			}
		}
		
		// CR5 #10
		$scope.buatBaru = false;
		$scope.newModeView = true;
		//$scope.DisPKP = true;
		$scope.DisPKP = false; //Always false -> return to normal
		
		if(row.entity.IsNONPKP == 0){
			console.log('masuk stv');
			$scope.PKPType = false;
			
			$scope.showBox = true;
			$scope.showIcon = false;
		}else if(row.entity.IsNONPKP == 1){
			console.log('masuk0 stv');
			$scope.PKPType = true;
			
			$scope.showBox = false;
			$scope.showIcon = true;
		}
		console.log('$scope.PKPType at view : ', $scope.PKPType);
		// CR5 #10

    }
    /*==================================================================================================*/
    /*										UIGRID SECTION BEGIN										*/
    /*==================================================================================================*/
    $scope.VendorList_UIGrid = {
        paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        enableColumnResizing: true,
        multiSelect: false,
        columnDefs: [
            { name: "VendorId", field: "VendorId", visible: false },
            { name: "ProvinceId", field: "ProvinceId", visible: false },
            { name: "Address", field: "Address", visible: false },
            { name: "CityRegencyId", field: "CityRegencyId", visible: false },
            { name: "NPWP", field: "NPWP", visible: false },
            { name: "PostalCode", field: "PostalCode", visible: false },
            { name: "TermofPayment", field: "TermofPayment", visible: false },
            { name: "NPWPName", field: "NPWPName", visible: false },
            { name: "NPWPAddress", field: "NPWPAddress", visible: false },
            { name: "Email", field: "Email", visible: false },
            { name: "Phone", field: "Phone", visible: false },
            { name: "CPTelephone", field: "CPTelephone", visible: false },
            { name: "Fax", field: "Fax", visible: false },
            { name: "ContactPerson", field: "ContactPerson", visible: false },
            { name: "Kode Vendor", field: "VendorCode", width: 180 },
            { name: "Nama Vendor", field: "Name", width: 180 },
			{ name: "PKP / Non PKP",displayName:'PKP / Non PKP', field: "IsNONPKPName", width: 180 },
			{ name: "PKP / Non PKP Kode", field: "IsNONPKP", visible: false },
            { name: "Business Type", field: "BusinessUnitName", width: 180 },
            { name: "BusinessUnitId", field: "BusinessUnitId", visible: false },
            { name: "Tipe Vendor", field: "isAfcoText", width: 180 },
            { name: "isAfco", field: "isAfco", visible: false },
            { name: "PerformanceBit", field: "PerformanceBit", visible: false },
            { name: "BlockingBit", field: "BlockingBit", visible: false },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.MainVendorLihatRow(row)" class="fa fa-list-alt fa-fw fa-lg" title="Lihat" style="box-shadow:none!important;color:#777;margin:1px 7px 0px 2px;padding:0.5em" /a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainVendorEditRow(row)" class="fa fa-pencil fa-fw fa-lg" title="Edit" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>&nbsp;&nbsp;&nbsp;<a ng-show="row.entity.BusinessUnitId == 10" ng-click="grid.appScope.MainVendorDeleteRow(row)" class="fa fa-trash fa-fw fa-lg" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiVendorList = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.VendorList_UIGrid_Paging(pageNumber,pageSize);
            });
        }
    };

    // =======Adding Custom Filter==============
    $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    var x = -1;
    for (var i = 0; i < $scope.VendorList_UIGrid.columnDefs.length; i++) {
        if ($scope.VendorList_UIGrid.columnDefs[i].visible == undefined && $scope.VendorList_UIGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.VendorList_UIGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
        }
        //console.log("$scope.gridCols", $scope.gridCols);
        //console.log("$scope.grid.columnDefs[i]", $scope.VendorList_UIGrid.columnDefs[i]);
    }
    $scope.filterBtnLabel = $scope.gridCols[0].name;
    $scope.filterBtnChange = function (col) {
        //console.log("col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };


    $scope.VendorList_UIGrid_Paging = function (pageNumber, pageSize) {
        if (angular.isUndefined($scope.MainVendor_KodeVendor))
            $scope.MainVendor_KodeVendor = '';

        if (angular.isUndefined($scope.MainVendor_NamaVendor))
            $scope.MainVendor_NamaVendor = '';

        if (angular.isUndefined($scope.MainVendor_JenisVendor))
            $scope.MainVendor_JenisVendor = 0;

        if (angular.isUndefined($scope.MainVendor_BusinessType))
            $scope.MainVendor_BusinessType = 0;
		
		if (angular.isUndefined($scope.PKPTypeF))
            $scope.PKPTypeF = 2;

        if(pageSize == undefined){
			pageSize = $scope.uiGridPageSize;
		}
        MasterVendorFactory.getDataMainVendor(pageNumber, pageSize, $scope.MainVendor_KodeVendor, $scope.MainVendor_NamaVendor, $scope.MainVendor_JenisVendor, $scope.MainVendor_BusinessType, $scope.PKPTypeF)
            .then(
                function (res) {
                    console.log('get vendor', res,$scope.VendorList_UIGrid)
                    if (!angular.isUndefined(res.data.Result)) {
                        //console.log("res =>", res.data.Result);
						// CR5 #10
						var temptPKPName = '';
						$scope.VendorList_UIGridTemp = [];
						for (var i = 0; i < res.data.Result.length; i++) {
							$scope.VendorList_UIGridTemp.push(res.data.Result[i]);
							if(res.data.Result[i].IsNONPKP == 0){
								temptPKPName = 'PKP';
							}else{
								temptPKPName = 'Non PKP';
							}
							$scope.VendorList_UIGridTemp[i]["IsNONPKPName"] = temptPKPName;
						}
						$scope.VendorList_UIGrid.data = $scope.VendorList_UIGridTemp;
						// CR5 #10

						//$scope.VendorList_UIGrid.data = res.data.Result;
                        $scope.VendorList_UIGrid.totalItems = res.data.Total;
                        $scope.VendorList_UIGrid.paginationCurrentPage = pageNumber
                        // $scope.VendorList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
                        // $scope.VendorList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
                    }

                }
            );
    }

    $scope.VendorList_UIGrid_Paging(1);

    ///PARTS LIST
    $scope.PartsList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        data: $scope.PartsListData,
        columnDefs: [
            { name: "PartsId", field: "PartsId", visible: false },
            { name: "Kode Material", field: "MaterialCode" },
            { name: "Nama Material", field: "MaterialName" },
            { name: "Satuan Dasar", field: "BaseUOM" },
            { name: "Default Satuan Beli", field: "DefaultUOM" },
            { name: "BaseUOMId", field: "BaseUOMId", visible: false },
            { name: "DefaultUOMId", field: "DefaultUOMId", visible: false },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorPartsUOMConversion(row)">Nilai Konversi</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahVendorPartsHapus(row)">Hapus</a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiPartsList = gridApi;
        }
    };

    $scope.ModalTambahVendorPartsUoMConversionUIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: false,
        multiSelect: false,
        data: $scope.PartsUoMConversionData,
        columnDefs: [
            { name: "PartsId", field: "PartsId", visible: false },
            { name: "Satuan", field: "Satuan", enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownOptionsArray: $scope.MasterVendorUoMList, editDropdownIdLabel: 'name', editDropdownValueLabel: 'name' },
            { name: "Nilai Konversi", field: "NilaiKonversi", enableCellEdit: true },
            { name: "Satuan Konversi", field: "SatuanKonversi", enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownOptionsArray: $scope.MasterVendorUoMList, editDropdownIdLabel: 'name', editDropdownValueLabel: 'name' },
            { name: "UOMId", field: "", visible: false },
            { name: "ConversionUOMId", field: "", visible: false },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorPartsUoMConversionHapus(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiPartsUoMConversionList = gridApi;
        }
    };

    ///BANK ACCOUNT LIST
    $scope.BankAccountList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "BankAccountId", field: "BankAccountId", visible: false },
            { name: "Nama Bank", field: "BankName" },
            { name: "Nomor Rekening", field: "AccountNo" },
            { name: "Atas Nama Bank", field: "AccountName" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorBankDelete(row.entity)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiBankAccountList = gridApi;
        }
    };
    $scope.TambahVendorBankDelete = function (row) {
        //console.log(row);

        // var rowindex = $scope.BankAccountList_UIGrid.data.findIndex(function (obj) {
        //     console.log("row.BankAccountId", row.AccountBankId, rowindex)
        //     return obj.AccountBankId == row.AccountBankId;
        // });
        // console.log("delete bank", row, rowindex, $scope.BankAccountList_UIGrid.data)
        // $scope.BankAccountList_UIGrid.data.splice(row, 1);

        // $scope.refreshOutletData();

        if (!angular.isUndefined($scope.VendorAccBankListData)) {
            $scope.VendorAccBankListData.some(function (obj, i) {
                if (obj.BankName === row.BankName) {
                    $scope.VendorAccBankListData.splice(i, 1);
                }
            });
        }
        $scope.BankAccountList_UIGrid.data = $scope.VendorAccBankListData

        // if(row.KaroseriId.toString().indexOf("-") == -1)
        //     $scope.deletedOutletDetail.push(row);
    }

    ///PARTS JB LIST
    $scope.PartsJBList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "PartsJBId", field: "PartsJBId", visible: false },
            { name: "PartsId", field: "PartsId", visible: false },
            { name: "Tanggal Berlaku dari", field: "DateStart", cellFilter: 'date:\"dd/MM/yyyy\"' },
            { name: "Tanggal Berlaku sampai", field: "DateEnd", cellFilter: 'date:\"dd/MM/yyyy\"' },
            { name: "No Material", field: "MaterialNo" },
            { name: "Nama Material", field: "MaterialName" },
            { name: "Satuan", field: "UOM" },
            //{ name: "Satuan", field: "UOM" },
            { name: "BaseUOMId", field: "BaseUOMId", visible: false },
            { name: "Harga Beli", field: "BuyPrice", cellFilter: "Newcurrency" },
            { name: "Harga Jual", field: "SellPrice", cellFilter: "Newcurrency" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorPartsJBDelete(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiPartsJBList = gridApi;
        }
    };

    ///KAROSERI LIST
    $scope.KaroseriList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "KaroseriId", field: "KaroseriId", visible: false },
            { name: "KaroseriTypeId", field: "KaroseriTypeId", visible: false },
            { name: "Kode Karoseri", field: "KaroseriCode" },
            { name: "Tipe Karoseri", field: "KaroseriType" },
            { name: "Model", field: "ModelName" },
            { name: "ModelId", field: "ModelNameId", visible: false },
            { name: "FullModel", field: "FullModelName", displayName: "Tipe Kendaraan" },
            { name: "FullModelId", field: "FullModelId", visible: false },
            { name: "Deskripsi", field: "Description" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorKaroseriDelete(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMasterVendorKaroseriList = gridApi;
        }
    };

    // $scope.TambahVendorKaroseriDelete = function (row) {
    //     //console.log(row);

    //     var rowindex = $scope.KaroseriList_UIGrid.data.findIndex(function (obj) {
    //         return obj.KaroseriId == row.KaroseriId;
    //     });

    //     $scope.KaroseriList_UIGrid.data.splice(rowindex, 1);

    //     // $scope.refreshOutletData();

    //     // if(row.KaroseriId.toString().indexOf("-") == -1)
    //     //     $scope.deletedOutletDetail.push(row);
    // }

    ///Karoseri JB LIST
    $scope.KaroseriJBList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "KaroseriJBId", field: "KaroseriJBId", visible: false },
            { name: "KaroseriId", field: "KaroseriId", visible: false },
            { name: "Kode Karoseri", field: "KaroseriCode" },
            { name: "Model", field: "ModelName" },
            { name: "Harga Beli", field: "BuyPrice", cellFilter: "Newcurrency" },
            { name: "Harga Jual", field: "SellPrice", cellFilter: "Newcurrency" },
            { name: "Tanggal Berlaku dari", field: "DateStart", cellFilter: 'date:\"dd/MM/yyyy\"' },
            { name: "Tanggal Berlaku sampai", field: "DateEnd", cellFilter: 'date:\"dd/MM/yyyy\"' },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorKaroseriJBDelete(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiPartsJBList = gridApi;
        }
    }
    ///JASA LIST
    $scope.JasaList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: true,
        columnDefs: [
            { name: "ServiceId", field: "ServiceId", visible: false },
            { name: "Kode Jasa", field: "ServiceCode" },
            { name: "Nama Jasa", field: "ServiceName" },
            { name: "Model", field: "ModelName" },
            { name: "Eksternal", field: "isExternal", type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isExternal">' },
            { name: "ModelId", field: "ModelId", visible: false },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorPartsJasaDelete(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMasterVendorJasaList = gridApi;
        }
    };

    ///JASA JB LIST
    $scope.JasaJBList_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: true,
        columnDefs: [
            { name: "ServiceJBId", field: "ServiceJBId", visible: false },
            { name: "ServiceId", field: "ServiceId", visible: false },
            { name: "Kode Jasa", field: "ServiceCode" },
            { name: "Nama Jasa", field: "ServiceName" },
            { name: "Model", field: "ModelName", enableFiltering: true },
            { name: "Harga Beli", field: "BuyPrice", cellFilter: 'Newcurrency' },
            { name: "Harga Jual", field: "SellPrice", cellFilter: 'Newcurrency' },
            { name: "Tanggal Berlaku Dari", field: "DateStart", cellFilter: 'date:\"dd/MM/yyyy\"' },
            { name: "Tanggal Berlaku Sampai", field: "DateEnd", cellFilter: 'date:\"dd/MM/yyyy\"' },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahVendorServiceJBDelete(row)" class="fa fa-fw fa-lg fa-trash" title="Hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" /a>'
            },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiPartsJBList1 = gridApi;
        }
    };

    //==============================================================================================//
    //										Search Parts Begin										//
    //==============================================================================================//
    $scope.TambahVendor_PartsCari_fromModule = 0;

    $scope.TambahVendor_partsCari_DataParts = function (KodeParts) {
        if ($scope.TambahVendor_PartsCari_fromModule != 2) {
            MasterVendorFactory.getDataParts(KodeParts)
                .then(
                    function (res) {
						$('ModalTambahVendorSearchParts').modal('show');
						$scope.ModalTambahVendorSearchParts = true;

                        $scope.ModalTambahVendorSearchParts_UIGrid.data = res.data.Result;
                        //if (res.data.Result.length>0)
                        //console.log("sample",res.data.Result[0]);
                    }
                );
        } else {
            $scope.ModalTambahVendorSearchParts_UIGrid.data = $scope.PartListData;
        }
    }
	
	$scope.TambahVendor_partsCari_DataParts_With_BusinessUnit = function (KodeParts, BusinessUnit) {
		if ($scope.TambahVendor_PartsCari_fromModule != 2) {
            MasterVendorFactory.getDataPartsWithBusinessUnit(KodeParts, BusinessUnit)
                .then(
                    function (res) {
						if(res.data.Result){
							$('ModalTambahVendorSearchParts').modal('show');
							$scope.ModalTambahVendorSearchParts = true;

							$scope.ModalTambahVendorSearchParts_UIGrid.data = res.data.Result;
							//if (res.data.Result.length>0)
							//console.log("sample",res.data.Result[0]);
						}else{
							bsNotify.show({
								title: "Master Vendor",
								content: res.data,
								type: 'warning'
							});
							return;
						}
                    }
                );
        } else {
            $scope.ModalTambahVendorSearchParts_UIGrid.data = $scope.PartListData;
        }
    }

    $scope.TambahVendor_PartsCari_Clicked = function () {
        $scope.TambahVendor_PartsCari_fromModule = 1;
        // $scope.mData.TambahVendor_KodeMaterial = ;
        //$('ModalTambahVendorSearchParts').modal('show');
        //$scope.ModalTambahVendorSearchParts = true;
        console.log('masuk sini ngga sih?');
		
		if($scope.TambahVendor_BusinessUnit == '' || $scope.TambahVendor_BusinessUnit == undefined || $scope.TambahVendor_BusinessUnit == null){
			bsNotify.show({
                title: "Master Vendor",
                content: "Business Unit belum diisi",
                type: 'warning'
            });
            return;
		}else{
			if($scope.TambahVendor_BusinessUnit == 16){
				$scope.TambahVendor_partsCari_DataParts_With_BusinessUnit($scope.mData.TambahVendor_KodeMaterial, $scope.TambahVendor_BusinessUnit);
			}else{
				$scope.TambahVendor_partsCari_DataParts($scope.mData.TambahVendor_KodeMaterial);
			}
		}
        //$scope.TambahVendor_partsCari_DataParts($scope.mData.TambahVendor_KodeMaterial);

        // angular.element('#ModalTambahVendorSearchParts').modal('show');
    }

    $scope.TambahVendor_CariPartsJB_Clicked = function () {
        $scope.TambahVendor_PartsCari_fromModule = 2;
        // $scope.TambahVendor_partsCari_DataParts($scope.Form.TambahVendor_NoMaterialJB);
        $scope.ModalTambahVendorSearchParts = true;
        angular.element('#ModalTambahVendorSearchParts').modal('show');
        var ab = _.differenceBy($scope.PartListData, $scope.PartListDataJB, 'PartsId');
        console.log('diff', ab);
        $scope.ModalTambahVendorSearchParts_UIGrid.data = angular.copy(ab);
    }

    $scope.ModalTambahVendorSearchParts_Batal = function () {
        angular.element('#ModalTambahVendorSearchParts').modal('hide');
        $scope.ModalTambahVendorSearchParts = false;
    }

    $scope.ModalTambahVendorSearchParts_UIGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: null,
        columnDefs: [
            { name: 'PartsId', field: 'PartsId', visible: false },
            { name: 'Kode Material', field: 'MaterialCode' },
            { name: 'Nama Material', field: 'MaterialName', enableFiltering: true },
            {
                name: 'Action',
                enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchTambahVendorSearchPartsPilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        }
    };

    $scope.SearchTambahVendorSearchPartsPilihRow = function (row) {
        console.log("masuk sini", row.entity,$scope.TambahVendor_PartsCari_fromModule);
        // kalau ada nilai yg ga masuk ke model nya, mungkin karena .Form nya itu.. cek aja html nya dan controller nya hrs nya pakai .Form atau ngga
        if ($scope.TambahVendor_PartsCari_fromModule == 1) {
            $scope.Form.TambahVendor_NamaMaterial = row.entity.MaterialName;
            $scope.TambahVendor_KodeMaterial = row.entity.MaterialCode;
            $scope.TambahVendor_PartsId = row.entity.PartsId;
        } else {
            $scope.Form.TambahVendor_NamaMaterialJB = row.entity.MaterialName;
            $scope.Form.TambahVendor_NoMaterialJB = row.entity.MaterialCode;
            $scope.TambahVendor_PartsIdJB = row.entity.PartsId;
            $scope.Form.TambahVendor_SatuanDasar = row.entity.BaseUOMId;
        }

        $scope.ModalTambahVendorSearchParts_Batal();
    }
    //==============================================================================================//
    //										Search Parts End										//
    //==============================================================================================//

    //==============================================================================================//
    //										Search Jasa Begin										//
    //==============================================================================================//
    $scope.TambahVendor_JasaCari_DataJasa = function (KodeJasa) {
        // $scope.ModalTambahVendorSearchJasa_UIGrid.data = $scope.MasterVendorJasaData;
        console.log('MasterVendorJasaData', $scope.MasterVendorJasaData);
        console.log('MasterVendorJasaJBData', $scope.MasterVendorJasaJBData);
        var ax = _.differenceBy($scope.MasterVendorJasaData, $scope.MasterVendorJasaJBData, 'ServiceId');
        console.log('diff', ax);
        $scope.ModalTambahVendorSearchJasa_UIGrid.data = angular.copy(ax);
        // console.log('MasterVendorJasaData', $scope.MasterVendorJasaData);
    }

    $scope.TambahVendor_CariJasaJB_Clicked = function (filterColIdx) {
        $scope.TambahVendor_JasaCari_DataJasa($scope.Form.TambahVendor_KodeJasa);
        angular.element('#ModalTambahVendorSearchJasa').modal('show');
        // $scope.mData = 
        $scope.ModalTambahVendorSearchJasa = true;

    }

    $scope.ModalTambahVendorSearchJasa_Batal = function () {
        angular.element('#ModalTambahVendorSearchJasa').modal('hide');
        $scope.ModalTambahVendorSearchJasa = false;
    }

    $scope.ModalTambahVendorSearchJasa_UIGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: null,
        columnDefs: [
            { name: 'ServiceId', field: 'ServiceId', visible: false },
            { name: 'Kode Jasa', field: 'ServiceCode' },
            { name: 'Nama Jasa', field: 'ServiceName', enableFiltering: true },
            { name: 'Model', field: 'ModelName', enableFiltering: true },
            {
                name: 'Action',
                enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchTambahVendorSearchJasaPilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid3Api = gridApi;
        }
    };
    // =======Adding Custom Filter Grid Pilih Jasa==============
    $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    // $scope.JasaList_UIGrid = {};
    $scope.filterColIdx = 2;
    $scope.test = function (param) {
        console.log("param ===", param, $scope.grid3Api.grid.columns[$scope.filterColIdx]);
    }
    var x = -1;
    for (var i = 0; i < $scope.ModalTambahVendorSearchJasa_UIGrid.columnDefs.length; i++) {
        if ($scope.ModalTambahVendorSearchJasa_UIGrid.columnDefs[i].visible == undefined && $scope.ModalTambahVendorSearchJasa_UIGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.ModalTambahVendorSearchJasa_UIGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
        }
    }
    $scope.filterBtnLabel = $scope.gridCols[0].name;
    $scope.filterBtnChange = function (col) {
        console.log("col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };

    $scope.SearchTambahVendorSearchJasaPilihRow = function (row) {
        //console.log("masuk sini");
        // kalau ada nilai yg ga masuk ke model nya, mungkin karena .Form nya itu.. cek aja html nya dan controller nya hrs nya pakai .Form atau ngga
        $scope.Form.TambahVendor_NamaJasaJB = row.entity.ServiceName;
        $scope.Form.TambahVendor_KodeJasaJB = row.entity.ServiceCode;
        $scope.TambahVendor_ResultSearchServiceId = row.entity.ServiceId;
        $scope.Form.TambahVendor_ModelJB = row.entity.ModelName;
        $scope.ModalTambahVendorSearchJasa_Batal();
    }
    //==============================================================================================//
    //										Search Jasa End										//
    //==============================================================================================//

    // +++++++++++++++++++++++++++++++
    // get Karoseri
    $scope.getKaroseri = function () {
        //$scope.optionsTipeKaroseri = [];
        MasterVendorFactory.getKaroseri().then(function (rs) {
            console.log("Karoseri = ", rs);
            $scope.optionsTipeKaroseri = [];
            angular.forEach(rs.data.Result, function (value, key) {
                $scope.optionsTipeKaroseri.push({ name: value.KaroseriTypeName, value: value.KaroseriTypeId });
            });
            
            // for (var i = 0; i < rs.data.length; i++) {
            //     if (rs.data[i].KaroseriName != null) {
            //         console.log('aa', rs.data[i].KaroseriName);
            //         $scope.optionsTipeKaroseri.push(rs.data[i]);
            //     }
            // }
            console.log("optionsTipeKaroseri = ", $scope.optionsTipeKaroseri);
        });
    }
    $scope.valueKaroseri = function(item){
        console.log('selectedType value karoseri >>>', item);
        $scope.getKaroseri();
        $scope.Form.TambahVendor_KaroseriDeskripsi = item.name;
        $scope.Form.TambahVendor_KaroseriCode = item.value;
    }

    $scope.valueKaroseriJb = function(item){
        console.log('selectedType value karoseriJB >>>', item);
        $scope.getKaroseri();
        $scope.Form.TambahVendor_KodeKaroseriJB = item.value;
        $scope.Form.TambahVendor_TipeKaroseriJB = item.name;
        $scope.Form.TambahVendor_KaroseriModelIdJB = item.ValueModel;
        $scope.Form.TambahVendor_KaroseriFullModelIdJB = item.ValueFullModelId
        
    }

    $scope.value = function (item) {
        console.log('selectedType value >>>', item);
        $scope.getKaroseri();

		/*$scope.showPKP = false;
		if($scope.TambahVendor_BusinessUnit == 9){
			if($scope.TambahVendor_JenisVendor == 1){
				$scope.showPKP = true;
			}else{
				$scope.PKPType = false;

				$scope.TambahVendor_NamaNPWP = "";
				$scope.TambahVendor_NPWP = "";
				$scope.TambahVendor_NPWPAddress = "";
				$scope.TambahVendor_KodePos = "";
			}
		}else{
			$scope.PKPType = false;

			$scope.TambahVendor_NamaNPWP = "";
			$scope.TambahVendor_NPWP = "";
			$scope.TambahVendor_NPWPAddress = "";
			$scope.TambahVendor_KodePos = "";
		}*/

        // $scope.Form.TambahVendor_KaroseriDeskripsi = item.Description;
        // $scope.Form.TambahVendor_KaroseriCode = item.KaroseriCode;
        // console.log('aabb', $scope.Form.TambahVendor_KaroseriDeskripsi);
    }
	
	$scope.valueN = function (item) {
        console.log('selectedType value >>>', item);
        $scope.getKaroseri();

		$scope.showPKP = false;
		if($scope.TambahVendor_BusinessUnit == 9){
			if($scope.TambahVendor_JenisVendor == 1){
				$scope.showPKP = true;
			}else{
				$scope.PKPType = false;

				$scope.TambahVendor_NamaNPWP = "";
				$scope.TambahVendor_NPWP = "";
				$scope.TambahVendor_NPWPAddress = "";
				$scope.TambahVendor_KodePos = "";
			}
		}else{
			$scope.PKPType = false;

			$scope.TambahVendor_NamaNPWP = "";
			$scope.TambahVendor_NPWP = "";
			$scope.TambahVendor_NPWPAddress = "";
			$scope.TambahVendor_KodePos = "";
		}
        // $scope.Form.TambahVendor_KaroseriDeskripsi = item.Description;
        // $scope.Form.TambahVendor_KaroseriCode = item.KaroseriCode;
        // console.log('aabb', $scope.Form.TambahVendor_KaroseriDeskripsi);
    }
	
    $scope.typeId = function (item) {
        console.log('selectedType >>>', item);
		console.log('$scope.TambahVendor_JenisVendor >>>', $scope.TambahVendor_JenisVendor);
		
		$scope.showPKP = false;
		if($scope.TambahVendor_BusinessUnit == 9){
			if($scope.TambahVendor_JenisVendor == 1){
				$scope.showPKP = true;
			}
		}

		$scope.DisPKP = false;
		$scope.PKPType = false;
		$scope.buatBaru = false;
		$scope.newModeView = false;

		$scope.TambahVendor_NamaNPWP = "";
		$scope.TambahVendor_NPWP = "";
		$scope.TambahVendor_NPWPAddress = "";
		$scope.TambahVendor_KodePos = "";
    }


});


app.filter('rupiahC', function () {
    return function (val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
        }
        return val;
    };
});

app.filter('Newcurrency', function(){
    console.log('Cek masuk ke sini gak')
    return function(nil){
        console.log("Cek nil nih", nil);
        if(nil.toString().includes('.')){
            console.log("Cek nil 222222", nil);
            tmpNil = String(nil).split('.');
            console.log("Cek Hasil Split", tmpNil);
            nil = String(tmpNil[0]).replace(/(\d{3}\B)/g, "$1.") + ',' + tmpNil[1];
            console.log("Cek Hasil Separator", tmpNil);
        }else{
            while (/(\d+)(\d{3})/.test(nil.toString())) {
                nil = nil.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
            }
            console.log("Cek nil 333333", nil);
        }
        console.log("Cek nil 44444", nil);
        return nil;
    }
})