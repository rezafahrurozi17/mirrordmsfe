angular.module('app')
    .factory('MasterVendorFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            getDataTipeVendor: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorType';
                console.log("url VendorType : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataTipeBisnis: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorBusType';
                console.log("url VendorBusType : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataVendorOutlet: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorOutlet/';
                console.log("url VendorOutlet : ", url);
                var res = $http.get(url);
                return res;
            },

            // getDataKotaVendor: function(ProvinceId) {
            //     var url = '/api/as/AfterSalesMasterVendor/GetVendorCity/?ProvinceId=' + ProvinceId;
            //     console.log("url GetVendorCity : ", url);
            //     var res = $http.get(url);
            //     return res;
            // },

            // getDataPropinsiVendor: function() {
            //     var url = '/api/as/AfterSalesMasterVendor/GetVendorProvince/';
            //     console.log("url GetVendorProvince : ", url);
            //     var res = $http.get(url);
            //     return res;
            // },

            getDataPropinsiVendor: function() {
                return $http.get('/api/sales/MLocationProvince');
            },

            getDataKotaVendor: function(id) {
                return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
            },

            getDataSubletVendor: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorSublet';
                console.log("url GetVendorSublet : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataKaroseriType: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorKaroseriType';
                console.log("url GetVendorKaroseriType : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataVModel: function() {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorVModel';
                console.log("url GetVendorVModel : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataVFullModel: function(modelId) {
                // var url = '/api/as/AfterSalesMasterVendor/GetVendorFullModel';
                var res = $http.get('/api/as/AfterSalesMasterVendor/GetVendorFullModel', {
                    params: {
                        VehicleModelId: modelId
                    }
                });

                // console.log("url GetVendorFullModel : ", url);

                return res;
            },

            getDataGroup: function() {
                return $http.get('/api/as/GroupBPList');
            },


            getDataMainVendor: function(start, limit, VendorCode, VendorName, VendorTypeId, BusinessTypeid, PKPTypeid) {
                //var url = '/api/as/AfterSalesMasterVendor/GetVendorList/?start=' + start + '&limit=' + limit + '&outletid=' + user.OutletId + '&VendorCode=' + VendorCode + '&VendorName=' + VendorName + '&VendorTypeId=' + VendorTypeId +'&BusinessTypeid=' + BusinessTypeid;
				var url = '/api/as/AfterSalesMasterVendor/GetVendorList/?start=' + start + '&limit=' + limit + '&outletid=' + user.OutletId + '&VendorCode=' + VendorCode + '&VendorName=' + VendorName + '&VendorTypeId=' + VendorTypeId +'&BusinessTypeid=' + BusinessTypeid + '&IsNONPKP=' + PKPTypeid;
                console.log("url VendorList : ", url);
                var res = $http.get(url);
                return res;

            },

            getDataParts: function(partsCode) {
                var url = '/api/as/AfterSalesMasterVendorParts/GetPartsByCode/?PartsCode=' + partsCode + '&outletid=' + user.OutletId;
                console.log("url VendorBusType : ", url);
                var res = $http.get(url);
                return res;
            },
			
			getDataPartsWithBusinessUnit: function(partsCode, businessUnit) {
                var url = '/api/as/AfterSalesMasterVendorParts/GetPartsByCodeAccessoriesStandard/?PartsCode=' + partsCode + '&outletid=' + user.OutletId + '&BusinessUnitId=' + businessUnit;
                console.log("url VendorBusType : ", url);
                var res = $http.get(url);
                return res;
            },

            // getDataJasa : function(serviceCode){
            // var url = '/api/as/AfterSalesMasterVendor/GetServiceByCode/?ServiceCode='+ partsCode + '&outletid='+user.OutletId;
            // console.log("url ServiceList : ", url);
            // var res=$http.get(url);
            // return res;
            // },

            getDataNamaBank: function(partsCode) {
                var url = '/api/as/AfterSalesMasterVendor/GetBankList/';
                console.log("url VendorBank : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataUoMList: function(partsCode) {
                // var url = '/api/as/AfterSalesMasterVendor/GetDataUOM/';
                // console.log("url UOM : ", url);
                // var res=$http.get(url);
                // return res;
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            create: function(inputData) {
                var url = '/api/as/AfterSalesMasterVendor/Create/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            delete: function(id) {
                var url = '/api/as/AfterSalesFIRAnswers/deletedata/?id=' + id;
                console.log("url delete Data : ", url);
                var res = $http.delete(url);
                return res;
            },

            getDataPartsList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorPartsList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url PartsList by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataPartsListUOMConv: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorPartsListUOMConv/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url PartsList UOM Conv by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataAccBankList: function(VendorId, BusinessUnitId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorAccBankList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId + '&BusinessTypeId=' + BusinessUnitId;
                console.log("url Acc Bank List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataPartsJBList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorPartsJBList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url Parts JB List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataKaroseriList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorKaroseriList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url Karoseri List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataKaroseriJBList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorKaroseriJBList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url Karoseri JB List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataJasaList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorServiceList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url Service List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataJasaJBList: function(VendorId) {
                var url = '/api/as/AfterSalesMasterVendor/GetVendorServiceJBList/?VendorId=' + VendorId + '&OutletId=' + user.OutletId;
                console.log("url Service JB List by Vendor : ", url);
                var res = $http.get(url);
                return res;
            },
            getKaroseri: function() {
                // return $http.put('/api/as/AfterSalesMasterVendor/getKaroseri/?VehicleTypeId=' + VehicleTypeId);
                var url = '/api/as/AfterSalesMasterVendor/getKaroseri/';
                console.log("url Karoseri : ", url);
                var res = $http.get(url);
                 return res;
            },

            getDataVendor: function() {
                // return $http.put('/api/as/AfterSalesMasterVendor/getKaroseri/?VehicleTypeId=' + VehicleTypeId);
                var url = '/api/as/AfterSalesMasterVendor/GetDataVendor/?OutletId=' + user.OutletId;
                console.log("url Vendor Data : ", url);
                var res = $http.get(url);
                 return res;
            },

            CekExistingVendor: function(VendorId, VendorCode, BusinessUnitId) {
                var url = '/api/as/AfterSalesMasterVendor/CekExistingVendor/?VendorId=' + VendorId + '&VendorCode=' + VendorCode + '&BusinessUnitId=' + BusinessUnitId  ;
                var res = $http.get(url);
                return res;
            },

            CekExistingVendorGroup: function(VendorId, GroupId) {
                var url = '/api/as/AfterSalesMasterVendor/CekExistingVendorGroup/?VendorId=' + VendorId + '&GroupId=' + GroupId ;
                var res = $http.get(url);
                return res;
            },

            HapusVendorOPL: function(VendorId) {
                //api/as/Vendor/HapusVendorOPL/{VendorId}
                var res = $http.put('/api/as/Vendor/HapusVendorOPL/' + VendorId);
                
                return res;
            },

        }
    });