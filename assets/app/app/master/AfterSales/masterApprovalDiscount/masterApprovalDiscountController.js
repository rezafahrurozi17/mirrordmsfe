angular.module('app')
    .controller('MasterApprovalDiscountController', function($scope, bsAlert, $http, CurrentUser, MasterApprovalDiscountFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log('user >> ', $scope.user);
        $scope.mMasterApprovalDiscount = null; //Model
        $scope.cMasterApprovalDiscount = null; //Collection
        $scope.xMasterApprovalDiscount = {};
        $scope.xMasterApprovalDiscount.selected = [];
        $scope.RoleData = null;
        $scope.isView = false;
        $scope.modex = null;
        //-----------------------------------
        //Min Discount Range
        //-----------------------------------
        $scope.MinimalDiscount = [{ id: 1, name: '1%' }, { id: 6, name: '6%' }, { id: 11, name: '11%' }, { id: 16, name: '16%' }, { id: 21, name: '21%' }, { id: 26, name: '26%' },
            { id: 31, name: '31%' }, { id: 36, name: '36%' }, { id: 41, name: '41%' }, { id: 46, name: '46%' }, { id: 51, name: '51%' }, { id: 56, name: '56%' }, { id: 61, name: '61%' },
            { id: 66, name: '66%' }, { id: 71, name: '71%' }, { id: 76, name: '76%' }, { id: 81, name: '81%' }, { id: 86, name: '86%' }, { id: 91, name: '91%' }, { id: 96, name: '96%' }
        ];
        //-----------------------------------
        //Max Discount Range
        //-----------------------------------
        $scope.MaximalDiscount = [{ id: 5, name: '5%' }, { id: 10, name: '10%' }, { id: 15, name: '15%' }, { id: 20, name: '20%' }, { id: 25, name: '25%' },
            { id: 30, name: '30%' }, { id: 35, name: '35%' }, { id: 40, name: '40%' }, { id: 45, name: '45%' }, { id: 50, name: '50%' }, { id: 55, name: '55%' }, { id: 61, name: '60%' },
            { id: 65, name: '65%' }, { id: 70, name: '70%' }, { id: 75, name: '75%' }, { id: 80, name: '80%' }, { id: 85, name: '85%' }, { id: 90, name: '90%' }, { id: 95, name: '95%' },
            { id: 100, name: '100%' }
        ];
        //-----------------------------------
        //IsGRBP
        //-----------------------------------
        $scope.IsGRBP = [{ id: 1, name: 'GR' }, { id: 0, name: 'BP' }];
        //-----------------------------------
        //countDiscount
        //-----------------------------------
        $scope.countDiscount = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }, { id: 8 }, { id: 9 }, { id: 10 }];


        $scope.getData = function() {
            console.log('user getData >> ', $scope.user);
            MasterApprovalDiscountFactory.getData().then(
                function(res) {
                    $scope.grid.data = res.data;
                    console.log('cv', res);
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            MasterApprovalDiscountFactory.getDataRole().then(
                function(res) {
                    $scope.RoleData = res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }


        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.onBulkDelete = function(data) {
            console.log("daatttaaa", data);
            for (var i in data) {
                data[i].StatusCode = 0;
            }
            MasterApprovalDiscountFactory.update(data).then(function(res) {
                $scope.alertAfterDelete();
                $scope.getData();
            });
            console.log("daatttaaa dua dua", data);
        }


        //Function ketika memencet tombol 'Tambah'
        $scope.beforeNew = function() {
            console.log("before new");
            $scope.isView = false;
            $scope.mMasterApprovalDiscount.MaximalDiscount = null;
            $scope.mMasterApprovalDiscount.MinimalDiscount = null;
        }

        //Function untuk edit data di Grid
        $scope.beforeEdit = function(row, mode) {

            if (mode == 'edit') {
                $scope.isView = false;
                console.log('masuk edit bos', $scope.isView);
                $scope.modex = 'edit'
            } else if (mode == 'view') {
                $scope.isView = true;
                console.log('masuk view bos', $scope.isView);
                $scope.modex = 'view'
            } else {
                console.log('ini new');
                $scope.modex = 'new'
            }

        }

        $scope.validateSave = function(data, mode) {
            console.log("okee", data, mode);
            var cekRole = 0;
            var cekSeq = 0;
            var cekMin = 0;
            var cekMax = 0;

            if ($scope.modex == 'new') {
                for(var i=0; i<$scope.grid.data.length; i++){
                    if (data.RoleId == $scope.grid.data[i].RoleId && $scope.grid.data[i].StatusCode == 1){
                        cekRole = 1 // uda ada nih
                    }
                    if (data.Sequence == $scope.grid.data[i].Sequence && $scope.grid.data[i].StatusCode == 1){
                        cekSeq = 1 // uda ada nih
                    }

                    if (data.MinimalDiscount >= $scope.grid.data[i].MinimalDiscount && data.MinimalDiscount <= $scope.grid.data[i].MaximalDiscount && $scope.grid.data[i].StatusCode == 1) {
                        cekMin = 1
                    }

                    if (data.MaximalDiscount >= $scope.grid.data[i].MinimalDiscount && data.MaximalDiscount <= $scope.grid.data[i].MaximalDiscount && $scope.grid.data[i].StatusCode == 1) {
                        cekMax = 1
                    }


                }

                if (cekRole == 1) {
                    bsAlert.alert({
                        title: "Jabatan yang dipilih sudah ada",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                if (cekSeq == 1) {
                    bsAlert.alert({
                        title: "Approval Diskon ke " + data.Sequence + " sudah ada, harap dicek kembali.",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                if (cekMax == 1 || cekMin == 1) {
                    bsAlert.alert({
                        title: "Range diskon beririsan dengan data yang sudah ada, harap dicek kembali",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                data.OutletId = $scope.user.OutletId
                // MasterApprovalDiscountFactory.CekMasterApprovalDiskon([data]).then(function(res) {
                //     if (res.data == 666) {
                //         bsAlert.alert({
                //             title: "Data approval diskon bersinggungan dengan data yang sudah ada, silahkan refresh data pada halaman utama",
                //             text: '',
                //             type: "warning",
                //             showCancelButton: false
                //         })
                //         return false
                //     } else {
                        return true
                //     }
                // });


            } else if ($scope.modex == 'edit') {
                for(var i=0; i<$scope.grid.data.length; i++){
                    if (data.RoleId == $scope.grid.data[i].RoleId && $scope.grid.data[i].StatusCode == 1 && data.MasterApprovalDiscountId != $scope.grid.data[i].MasterApprovalDiscountId){
                        cekRole = 1 // uda ada nih
                    }
                    if (data.Sequence == $scope.grid.data[i].Sequence && $scope.grid.data[i].StatusCode == 1 && data.MasterApprovalDiscountId != $scope.grid.data[i].MasterApprovalDiscountId){
                        cekSeq = 1 // uda ada nih
                    }

                    if (data.MinimalDiscount >= $scope.grid.data[i].MinimalDiscount && data.MinimalDiscount <= $scope.grid.data[i].MaximalDiscount && $scope.grid.data[i].StatusCode == 1 && data.MasterApprovalDiscountId != $scope.grid.data[i].MasterApprovalDiscountId) {
                        cekMin = 1
                    }

                    if (data.MaximalDiscount >= $scope.grid.data[i].MinimalDiscount && data.MaximalDiscount <= $scope.grid.data[i].MaximalDiscount && $scope.grid.data[i].StatusCode == 1 && data.MasterApprovalDiscountId != $scope.grid.data[i].MasterApprovalDiscountId) {
                        cekMax = 1
                    }


                }

                if (cekRole == 1) {
                    bsAlert.alert({
                        title: "Jabatan yang dipilih sudah ada",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                if (cekSeq == 1) {
                    bsAlert.alert({
                        title: "Approval Diskon ke " + data.Sequence + " sudah ada, harap dicek kembali.",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                if (cekMax == 1 || cekMin == 1) {
                    bsAlert.alert({
                        title: "Range diskon beririsan dengan data yang sudah ada, harap dicek kembali",
                        text: '',
                        type: "warning",
                        showCancelButton: false
                    })
                    return false
                }

                data.OutletId = $scope.user.OutletId
                // MasterApprovalDiscountFactory.CekMasterApprovalDiskon([data]).then(function(res) {
                //     if (res.data == 666) {
                //         bsAlert.alert({
                //             title: "Data approval diskon bersinggungan dengan data yang sudah ada, silahkan refresh data pada halaman utama",
                //             text: '',
                //             type: "warning",
                //             showCancelButton: false
                //         })
                //         return false
                //     } else {
                        return true
                //     }
                // });

            }


            // for(var i=0; i<$scope.grid.data.length; i++){
            //     if (data.RoleId == $scope.grid.data[i].RoleId){
            //         cekRole = 1 // uda ada nih
            //         break;
            //     }
            // }
            // if (cekRole == 1 && $scope.modex == 'new'){
            //     bsAlert.alert({
            //         title: "Jabatan yang dipilih sudah ada",
            //         type: "warning",
            //         showCancelButton: false
            //     })
            // } else {
            //     return true;
            // }

        }


        $scope.doCustomSave = function(mdl, mode) {
            // if (mode == 'create') {
            if ($scope.modex == 'new') {
                // mdl.MasterApprovalDiscountId = 0;
                //dikomen dulu, nanti di pakai lagi kalau udah ada role adh gr/bp
                // if ($scope.user.RoleName.includes("GR")) {
                //     mdl.isGR = 1;
                // } else if ($scope.user.RoleName.includes("BP")) {
                //     mdl.isGR = 0;
                // }
                var svObj = angular.copy(mdl);
                console.log('svObj', svObj);
                MasterApprovalDiscountFactory.CekMasterApprovalDiskon([svObj]).then(function(res) {
                    if (res.data == 666) {
                        bsAlert.alert({
                            title: "Data approval diskon bersinggungan dengan data yang sudah ada, silahkan refresh data pada halaman utama",
                            text: '',
                            type: "warning",
                            showCancelButton: false
                        })
                        return false
                    } else {
                        MasterApprovalDiscountFactory.create(svObj).then(function(res) {
                            $scope.getData();
                            $scope.alertAfterSave();
                        });
                    }
                });
                

            } else {
                var svObj = angular.copy(mdl);
                console.log('svObj', svObj);
                MasterApprovalDiscountFactory.CekMasterApprovalDiskon([svObj]).then(function(res) {
                    if (res.data == 666) {
                        bsAlert.alert({
                            title: "Data approval diskon bersinggungan dengan data yang sudah ada, silahkan refresh data pada halaman utama",
                            text: '',
                            type: "warning",
                            showCancelButton: false
                        })
                        return false
                    } else {
                        MasterApprovalDiscountFactory.update([svObj]).then(function(res) {
                            console.log('cek message', res);
                            if (res.data == 666) {
                                $scope.alertErrorSaveEdit();
                                $scope.getData();
                            } else {
                                $scope.getData();
                                $scope.alertAfterSave();
                            }
                        });
                    }
                });
                
            }
        }

        $scope.alertAfterSave = function() {
            bsAlert.alert({
                title: "Data tersimpan",
                type: "success",
                showCancelButton: false
            })
        }

        $scope.alertErrorSaveEdit = function() {
            bsAlert.alert({
                title: "Role Id tidak boleh duplikat",
                type: "warning",
                showCancelButton: false
            })
        }

        $scope.alertAfterDelete = function() {
            bsAlert.alert({
                title: "Data Berhasil dihapus",
                type: "success",
                showCancelButton: false
            })
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'Jabatan', field: 'Jabatan' },
                { name: 'Range Diskon Minimal', field: 'MinimalDiscount' },
                { name: 'Range Diskon Maximal', field: 'MaximalDiscount' },
                { name: 'Approval Ke', field: 'Sequence' },
                { name: 'IsGR', field: 'IsGR', visible: false }
            ]
        };




    })