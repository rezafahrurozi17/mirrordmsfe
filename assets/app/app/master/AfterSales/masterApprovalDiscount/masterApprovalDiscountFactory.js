angular.module('app')
    .factory('MasterApprovalDiscountFactory', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/MasterApprovalDiscount/GetApprovalDiscount/');
                // var res = $http.get('/api/as/MasterApprovalDiscount/GetApprovalDiscount/' + isGR);
                return res;
            },
            getDataRole: function() {
                var res = $http.get('/api/as/MasterApprovalDiscount/GetApprovalDiscountRole');
                return res;
            },
            create: function(MasterApprovalDiscount) {
                console.log("Data DP : ", MasterApprovalDiscount);
                return $http.post('/api/as/MasterApprovalDiscount', [MasterApprovalDiscount]);
            },
            update: function(MasterApprovalDiscount) {
                console.log("Data DP : ", MasterApprovalDiscount);
                return $http.put('/api/as/MasterApprovalDiscount', MasterApprovalDiscount);
            },
            delete: function(id) {
                console.log("Data DP : ", id);
                //return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
            },
            CekMasterApprovalDiskon: function(MasterApprovalDiscount) {
                // api/as/CekMasterApprovalDiskon/

                console.log("Data DP : ", MasterApprovalDiscount);
                return $http.put('/api/as/CekMasterApprovalDiskon', MasterApprovalDiscount);
            },
        }
    });