var app = angular.module('app');
app.controller('MasterCounterGRController', function ($scope, $http, $filter, CurrentUser, bsAlert, MasterCounterGRFactory, $timeout, bsNotify) {
	var user = CurrentUser.user();
	$scope.MasterCounterGR_EmployeeList = [];
	
	$scope.MasterCounterGRMain_Start = function(){
		$scope.Show_MasterCounterGR_Main = true;
		$scope.Show_MasterCounterGR_Tambah = false;
	}
	
	$scope.MasterCounterGRMain_Tambah_Clicked = function(){
		$scope.MasterCounterGRTambah_GetEmployeeList();
		$scope.Show_MasterCounterGR_Main = false;
		$scope.Show_MasterCounterGR_Tambah = true;
		$scope.masterCounterGR_StatusCode = 1;
		$scope.masterCounterGR_ShowCode = 1;
	}
	
	$scope.MasterCounterGRTambah_Batal_Clicked = function(){
		$scope.MasterCounterGRTambah_ClearFields();
		$scope.Show_MasterCounterGR_Main = true;
		$scope.Show_MasterCounterGR_Tambah = false;
	}

	$scope.MasterCounterGRTambah_GetEmployeeList = function(mode, data){
		var ComboData = [];
		MasterCounterGRFactory.getDataEmployeeNameType('')
		.then(
			function(res){
				$scope.MasterCounterGR_EmployeeList = res.data.Result;
				if (mode != 'edit'){
					angular.forEach($scope.MasterCounterGR_EmployeeList, function(value, key){
						if ($scope.CounterGRGrid.data.length > 0){
							var adaSamaAktif = 0;
							angular.forEach($scope.CounterGRGrid.data, function(valueGrid, key){
								if (valueGrid.EmployeeId == value.EmployeeId && valueGrid.StatusCode == 1){
									adaSamaAktif = 1;
								}
							})
							if (adaSamaAktif == 0){
								ComboData.push({name:value.Name, value:value.EmployeeId})
							}
						} else {
							ComboData.push({name:value.Name, value:value.EmployeeId})
						}
						// ComboData.push({name:value.Name, value:value.EmployeeId})
					});
					
					$scope.optionsTambahCounterGRSAName = ComboData;
					if (ComboData.length == 0){
						bsNotify.show({
							size: 'big',
							title: 'Semua SA Sudah Terdaftar / Tidak Tersedia',
							text: '',
							type: 'danger'
						});
					}
				} else {
					if (data.entity.StatusCode != 1){
						var SATerpakai = 0;
						var NamaCntrSama = '';
						for (var i=0; i<$scope.CounterGRGrid.data.length; i++){
							if ($scope.CounterGRGrid.data[i].EmployeeId == data.entity.EmployeeId && $scope.CounterGRGrid.data[i].StatusCode == 1){
								SATerpakai = 1;
								NamaCntrSama = $scope.CounterGRGrid.data[i].CounterName
								break;
							}
						}
						
						if (SATerpakai == 1){
							$scope.MasterCounterGRTambah_Batal_Clicked();
							bsNotify.show({
								size: 'big',
								title: 'SA Sudah Terdaftar di Counter "'+ NamaCntrSama +'"',
								text: '',
								type: 'danger'
							});
							return false
						}

					}
					angular.forEach($scope.MasterCounterGR_EmployeeList, function(value, key){
						if (data.entity.EmployeeId == value.EmployeeId){
							ComboData.push({name:value.Name, value:value.EmployeeId})
						} else {
							var adaSamaAktif = 0;
							angular.forEach($scope.CounterGRGrid.data, function(valueGrid, key){
								if (valueGrid.EmployeeId == value.EmployeeId && valueGrid.StatusCode == 1){
									adaSamaAktif = 1;
								}
							})
							if (adaSamaAktif == 0){
								ComboData.push({name:value.Name, value:value.EmployeeId})
							}
						}
					})
					$scope.optionsTambahCounterGRSAName = ComboData;

				}
				
			}
		);
	}
	
	
	$scope.TambahCounterGR_SAName_Changed = function(){
		$scope.MasterCounterGR_EmployeeList.some(function(obj, i){
			if(obj.EmployeeId == $scope.TambahCounterGR_SAName)
				$scope.masterCounterGR_InitialSAName = obj.InitialName;
		});
	}

	
	$scope.MasterCounterGRTambah_CariSA_Clicked = function(){
		$scope.TambahCounterGR_EmployeeCari_DataEmployee($scope.masterCounterGR_SAName);
        angular.element('#ModalCounterGRTambahSearchEmployee').modal('show');
	}
	
	$scope.masterCounterGR_SAName_LostFocus = function(){
		var OldEmployeeId = $scope.masterCounterGR_EmployeeId;
		var OldEmployeeSAName = $scope.masterCounterGR_InitialSAName;
		MasterCounterGRFactory.getDataEmployee($scope.masterCounterGR_SAName)
		.then(
			function(res){
				if(res.data.Result.length > 0){
					$scope.masterCounterGR_InitialSAName = res.data.Result[0].Initial;
					$scope.masterCounterGR_EmployeeId = res.data.Result[0].EmployeeId;
				}
				else{
					
					alert("Data karyawan " + $scope.MasterCounterGR_SAName + " tidak ditemukan");
					$scope.masterCounterGR_InitialSAName = OldEmployeeSAName;
					$scope.masterCounterGR_EmployeeId = OldEmployeeId;
				}
			}
		);
	}
	
	$scope.MasterCounterGRTambah_Simpan_Clicked = function(){

		if ($scope.masterCounterGR_CounterName == null || $scope.masterCounterGR_CounterName == undefined || $scope.masterCounterGR_CounterName == '' || 
			$scope.TambahCounterGR_SAName == null || $scope.TambahCounterGR_SAName == undefined || $scope.TambahCounterGR_SAName == '') {
			bsNotify.show({
				size: 'big',
				title: 'Silahkan isi Nama Counter dan Nama SA',
				text: '',
				type: 'danger'
			});
			return;
		} else {
			console.log("employee id Pada saat save : ", $scope.masterCounterGR_EmployeeId);

			var indexSAName = $scope.optionsTambahCounterGRSAName.findIndex(function(obj){
				return obj.value == $scope.TambahCounterGR_SAName;
			})

			if(indexSAName != -1)
				$scope.SAName = $scope.optionsTambahCounterGRSAName[indexSAName].name;

			var CounterData = {
				CounterId : $scope.MasterCounterGR_CounterId,
				CounterName : $scope.masterCounterGR_CounterName,
				EmployeeId : $scope.masterCounterGR_EmployeeId,			
				// EmployeeId : $scope.TambahCounterGR_SAName,//$scope.masterCounterGR_EmployeeId, // di validasi supaya ga di update : yap
				InitialSAName : $scope.masterCounterGR_InitialSAName,//$scope.masterCounterGR_EmployeeId,
				SAName : $scope.SAName,//$scope.masterCounterGR_EmployeeId,
				OutletId : user.OutletId,
				CounterType : "GR",
				isSBI: $scope.masterCounterGR_isSBI,
				isSBE: $scope.masterCounterGR_isSBE,
				isEM: $scope.masterCounterGR_isEM,
				isBooking: $scope.masterCounterGR_isBooking,
				isWalkIn: $scope.masterCounterGR_isWalkIn,
				isOther: $scope.masterCounterGR_isOther,
				isGR : 1, // karena modul GR
				StatusCode :$scope.masterCounterGR_StatusCode,
				isShowBoard :$scope.masterCounterGR_ShowCode
				// StatusCode : 1			
			};

			//yap
			//kalau dia cari indexof di datanya ga ada berarti ga di assign untuk di update
			//reason : 
			//karena optionnya di ambil datanya sudah di filter bahwa kalau sudah di pakai tidak bisa di pakai lagi
			//alhasil ketika mau di cari namanya tidak ketemu
			if(indexSAName != -1)
				CounterData.EmployeeId = $scope.TambahCounterGR_SAName;//$scope.masterCounterGR_EmployeeId,

			
			console.log("Data di-save : ", CounterData);
			
			MasterCounterGRFactory.create(CounterData)
			.then(
				function(res){
					angular.element('#ModalHapusMasterCounterGR').modal('hide');
					//alert("Data berhasil disimpan");
					bsAlert.alert({
						title: "Upload Berhasil",
						text: "Data berhasil di upload",
						type: "success",
						showCancelButton: false
					});
					$scope.CounterGRGrid_Paging(1);
					$scope.MasterCounterGRTambah_Batal_Clicked();
				},
				function(err){
					//alert("Data gagal disimpan");
					bsAlert.alert({
						title: "Upload Gagal",
						text: "Data gagal di upload",
						type: "Warning",
						showCancelButton: false
					});
				}
			);

		}
		
	}
	
	$scope.MasterCounterGRMainDeleteRow = function(row){
		$scope.MainCounterGRHapus_CounterName = row.entity.CounterName;
		$scope.MasterCounterGR_CounterId = row.entity.CounterId;
		console.log("$scope.MasterCounterGR_CounterId"), $scope.MasterCounterGR_CounterId;
		angular.element('#ModalHapusMasterCounterGR').modal('show');
	}
	
	$scope.Hapus_MainCounterGR = function(){
		MasterCounterGRFactory.deleteData($scope.MasterCounterGR_CounterId)
		.then(
			function(res){
				alert("Data berhasil dihapus");
				$scope.Batal_MainCounterGR();
			},
			function(err){
				alert("Data gagal dihapus");
				console.log(err);
			}
		);
			
	}
	
	$scope.Batal_MainCounterGR = function(){
		console.log("masuk batal modal");
		$scope.MainCounterGRHapus_CounterName = "";
		$scope.MasterCounterGR_CounterId = 0;
		angular.element('#ModalHapusMasterCounterGR').modal('hide');
		$scope.CounterGRGrid_Paging(1);
	}
	
	$scope.MasterCounterGRMain_Start();
	
	$scope.MasterCounterGRTambah_ClearFields = function(){
		$scope.MasterCounterGR_CounterId = 0;
		$scope.masterCounterGR_EmployeeId = 0;
		$scope.masterCounterGR_CounterName = "";
		$scope.masterCounterGR_SAName = "";
		$scope.masterCounterGR_InitialSAName = "";
		$scope.masterCounterGR_isEM = false;
		$scope.masterCounterGR_isSBI = false;
		$scope.masterCounterGR_isSBE = false;
		$scope.masterCounterGR_isOther = false;
		$scope.masterCounterGR_isWalkIn = false;
		$scope.masterCounterGR_isBooking = false;
		$scope.TambahCounterGR_SAName = undefined;
		$scope.masterCounterGR_StatusCode = undefined;
		$scope.masterCounterGR_ShowCode = undefined;
	}
	$scope.MasterCounterGRMainEditRow = function(rowSelected){
		// debugger;
		console.log('cek this',rowSelected);
		$scope.MasterCounterGRTambah_GetEmployeeList('edit', rowSelected);
		$scope.MasterCounterGR_EmployeeList.push({name:rowSelected.entity.SAName, value:rowSelected.entity.EmployeeId});
		// $scope.optionsTambahCounterGRSAName = $scope.MasterCounterGR_EmployeeList;
		$scope.MasterCounterGR_CounterId = rowSelected.entity.CounterId;
		$scope.masterCounterGR_CounterName = rowSelected.entity.CounterName;
		//$scope.masterCounterGR_SAName = rowSelected.entity.SAName;
		$scope.TambahCounterGR_SAName = rowSelected.entity.EmployeeId;
		$scope.MasterCounterGR_SANameCurrent = $scope.masterCounterGR_SAName;
		$scope.masterCounterGR_InitialSAName = rowSelected.entity.InitialSAName;
		$scope.masterCounterGR_isEM = rowSelected.entity.isEM;
		$scope.masterCounterGR_isSBI = rowSelected.entity.isSBI;
		$scope.masterCounterGR_isSBE = rowSelected.entity.isSBE;
		$scope.masterCounterGR_isOther = rowSelected.entity.isOther;
		$scope.masterCounterGR_isWalkIn = rowSelected.entity.isWalkIn;
		$scope.masterCounterGR_isBooking = rowSelected.entity.isBooking;
		$scope.masterCounterGR_EmployeeId = rowSelected.entity.EmployeeId;
		$scope.masterCounterGR_StatusCode = rowSelected.entity.StatusCode;
		$scope.masterCounterGR_ShowCode = rowSelected.entity.isShowBoard;
		console.log("SAName : ", $scope.masterCounterGR_SAName);
		console.log("Employee Id Edit : ", $scope.masterCounterGR_EmployeeId);
		$scope.Show_MasterCounterGR_Main = false;
		$scope.Show_MasterCounterGR_Tambah = true;
		if ($scope.masterCounterGR_ShowCode == null || $scope.masterCounterGR_ShowCode == undefined) {
			$scope.masterCounterGR_ShowCode = 1;
		}
		
	}
	
	$scope.uiGridPageSize = 10;
	$scope.CounterGRGrid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: false,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'id', field: 'CounterId', visible: false },
			{ name: 'EmployeeId', field: 'EmployeeId', visible: false },
			{ name: 'Nama Counter', field: 'CounterName', width:120,enableCellEdit : false },
			{ name: 'Nama SA', displayName: 'Nama SA',width:100,field: 'SAName', enableCellEdit : false },
			{ name: 'Inisial SA', displayName: 'Inisial SA', field: 'InitialSAName', enableCellEdit : false },
			{ name: 'EM On Time ', displayName: 'EM On Time', width:100, field: 'isSBI', type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isSBI">', enableCellEdit : false },
			{ name: 'EM ', field: 'isEM' , displayName: 'EM',type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isEM">', enableCellEdit : false },
			{ name: 'Booking On Time ', field: 'isSBE' ,width:100, type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isSBE">', enableCellEdit : false },
			
			{ name: 'Booking', field: 'isBooking' , type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isBooking">', enableCellEdit : false },
			{ name: 'Walk In', field: 'isWalkIn' , type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isWalkIn">', enableCellEdit : false },
			{ name: 'Other', field: 'isOther' , type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isOther">', enableCellEdit : false },
			{ name: 'Status Aktif', field: 'StatusCode', width:100, type: 'boolean',cellTemplate: '<input ng-true-value=1 ng-false-value=0 type="checkbox" ng-disabled="true" ng-model="row.entity.StatusCode">', enableCellEdit : false },
			{ name: 'Show Board', field: 'isShowBoard', cellFilter: 'isShowBoard' },			
			{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.MasterCounterGRMainEditRow(row)" class="fa fa-pencil fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" title="Edit"/a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MasterCounterGRMainDeleteRow(row)" class="fa fa-fw fa-lg fa-trash" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" title="Hapus"/a>'},
							// <a ng-click="grid.appScope.MasterCounterGRMainDeleteRow(row)">Delete</a>
		],
		onRegisterApi: function(gridApi) {
			$scope.GridAPICounterGRGrid = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// $scope.CounterGRGrid_Paging(pageNumber);
			// });
		}
	};
	
	$scope.CounterGRGrid_Paging = function(pageNumber){

		MasterCounterGRFactory.getData(pageNumber, $scope.uiGridPageSize)
		.then(
			function(res)
			{
				var tmpData = res.data.Result;
				// for (var i in res.data.Result){
				// 	if (res.data.Result[i].StatusCode != 0){
				// 		tmpData.push(res.data.Result[i]);
				// 	}
				// }
				console.log("res =>", res.data.Result);
				// $scope.CounterGRGrid.data = res.data.Result;
				// $scope.CounterGRGrid.totalItems = res.data.Total;
				// $scope.CounterGRGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.CounterGRGrid.paginationPageSizes = [$scope.uiGridPageSize];
				console.log("ress =>", tmpData);
				$scope.CounterGRGrid.data = tmpData;
				$scope.CounterGRGrid.totalItems = res.data.Total;
				$scope.CounterGRGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.CounterGRGrid.paginationPageSizes = [$scope.uiGridPageSize];
				
			}
			
		);
	}
	
	$scope.CounterGRGrid_Paging(1);
	
	//==============================================================================================//
	//										Search Employee Begin										//
	//==============================================================================================//
	
	$scope.TambahCounterGR_EmployeeCari_DataEmployee = function(EmployeeName){
		MasterCounterGRFactory.getDataEmployeeNameType(EmployeeName)
		.then(
			function(res){

				$scope.ModalCounterGRTambahSearchEmployee_UIGrid.data = res.data.Result;
				if (res.data.Result.length>0)
					console.log("sample",res.data.Result[0]);
			}
		); 
	}
	
	$scope.ModalCounterGRTambahSearchEmployee_Batal = function(){
		angular.element('#ModalCounterGRTambahSearchEmployee').modal('hide');
	}
	
	$scope.ModalCounterGRTambahSearchEmployee_UIGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true, 
		paginationPageSizes: [10,20], 
		columnDefs: [ 
			{ name:'EmployeeId', field:'EmployeeId', visible:false},
			{ name:'Nama Karyawan', field:'Name', enableFiltering: true },
			{ name:'Inisial', field:'InitialName'}, 
			{ name: 'Action', enableFiltering: false, 
				cellTemplate:'<a ng-click="grid.appScope.SearchTambahCounterGREmployeePilihRow(row)">Pilih</a>'}
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        }
	};
	
	$scope.SearchTambahCounterGREmployeePilihRow = function(row){
			$scope.masterCounterGR_SAName = row.entity.Name;
			$scope.masterCounterGR_InitialSAName = row.entity.InitialName;
			$scope.masterCounterGR_EmployeeId = row.entity.EmployeeId;
		
		$scope.ModalCounterGRTambahSearchEmployee_Batal();
	}
	//==============================================================================================//
	//										Search Employee End										//
	//==============================================================================================//
}).filter('isShowBoard', function() {
    return function(x) {
        if (x == 1) {
            return 'Show';
        } else if (x == 0) {
            return 'No Show';
        } else {
            return 'Show';
        }
    };
});