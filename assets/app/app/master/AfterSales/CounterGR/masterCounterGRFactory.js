angular.module('app')
    .factory('MasterCounterGRFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        console.log("url user : ", user);
        return {
            getData: function(pageNumber, pageSize) {
                // var url = '/api/as/AfterSalesMasterCounter/?start=' + pageNumber + '&limit=' + pageSize + '&CounterType=GR&outletid=' + user.OutletId;// + user.OutletId;
                var url = '/api/as/AfterSalesMasterCounter/?CounterType=GR&outletid=' + user.OrgId; // + user.OutletId;
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },

            create: function(counterGRData) {
                var url = '/api/as/AfterSalesMasterCounter/submitdata/';
                var ArrayCounterGRData = [counterGRData];
                var param = JSON.stringify(ArrayCounterGRData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            getDataEmployee: function(employeeName) {
                var url = '/api/as/AfterSalesEmployeeData/?EmployeeName=' + employeeName;
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataEmployeeNameType: function(employeeName) {
                var url = '/api/as/AfterSalesEmployeeData/GetByNameType/?EmployeeName=' + employeeName + '&TypeId=7&outletid=' + user.OrgId;
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },

            deleteData: function(id) {
                var url = '/api/as/AfterSalesMasterCounter/deletedata/?id=' + id;
                console.log("url delete Data : ", url);
                var res = $http.delete(url);
                return res;
            }
        }
    });