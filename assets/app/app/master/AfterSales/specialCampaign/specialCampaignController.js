var app = angular.module('app');
app.controller('SpecialCampaignController', function ($scope, $http, $filter, CurrentUser, SpecialCampaignFactory, uiGridConstants, $timeout, bsNotify, bsAlert) {
    var user = CurrentUser.user();
    var uiGridPageSize = 10;
    $scope.MainSpecialCampaign_Data = [];
    $scope.TambahSpecialCampaign_DataModel = [];
    $scope.TambahSpecialCampaignJasa_DataModel = [];
    $scope.TambahSpecialCampaignJasa_DataModelView = [];
    $scope.TambahSpecialCampaignParts_DataModel = [];
    $scope.TambahSpecialCampaignParts_DataModelView = [];
    $scope.TambahSpecialCampaign_DiscountTypeId = 0;
    $scope.TambahSpecialCampaign_TypeTaskListId = 0;
    $scope.TambahSpecialCampaign_TypePartsId = 0;
    $scope.CampaignDiscountTypePartId = 0;
    $scope.TambahSpecialCampaign_CampaignDiscountId = 0;
    $scope.TambahSpecialCampaign_ColumnDef = [];
    $scope.EditSpecialCampaignModel_isMain = 0;
    $scope.TambahSpecialCampaignItems_TaskId = 0;
    $scope.TambahSpecialCampaignItems_TaskName = '';
    $scope.TambahSpecialCampaignItems_NoParts = '';
    $scope.TambahSpecialCampaignItems_DeskripsiJasa = '';
    $scope.ModalEditSpecialCampaignItemsJasa_TaskListId = 0;
    $scope.ModalEditSpecialCampaignItemsParts_PartsId = 0;
    $scope.ModalEditSpecialCampaignItemsJasa_DiscountJasa = 0;
    $scope.ModalEditSpecialCampaignItemsParts_DiscountParts = 0;
    $scope.TambahSpecialCampaignItems_DeskripsiJasa_isDisabled = true;
    $scope.modelDataCheck = [];
    $scope.modelDataCheckJasa = [];
    $scope.modelDataCheckParts = [];

    $scope.dataJasaYangDihapus = [];
    $scope.dataPartsYangDihapus = [];
    $scope.dataJasaYangDitambah = [];
    $scope.dataPartsYangDitambah = [];
    $scope.dataJasaYangDiedit = [];
    $scope.dataPartsYangDiedit = [];
    $scope.modelDataCheckJasa = [];
    $scope.modelDataCheckParts = [];

    $scope.dateOptionsYear = {
        startingDay: 1,
        format: 'yyyy',

        datepickerMode: 'year',
        minMode: 'year',
        minDate: "minDate",
        showWeeks: "false",
    };
    $scope.dateOptionsMonth = {
        startingDay: 1,
        format: ' MMM',
    };

    $scope.dateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        disableWeekend: 1
    };

    $scope.model_type = [];
    $scope.listExcel_model_katashiki = []
    SpecialCampaignFactory.GetCVehicleModelandType().then(function(ress){
        $scope.model_type = ress.data.Result;

        if ($scope.model_type.length > 0) {
            for (var i=0; i<$scope.model_type.length; i++) {

                if ($scope.model_type[i].ListKatashikiCode.length > 0) {
                    
                    for (var j=0; j<$scope.model_type[i].ListKatashikiCode.length; j++) {
                        var obj = {}
                        obj.ModelName = $scope.model_type[i].ModelName
                        obj.VehicleTypeName = $scope.model_type[i].ListKatashikiCode[j].VehicleTypeName

                        obj.Katashiki = $scope.model_type[i].ListKatashikiCode[j].KatashikiCode
                        obj.Suffix = $scope.model_type[i].ListKatashikiCode[j].Suffix


                        $scope.listExcel_model_katashiki.push(obj)
                    }
                } else {
                    var obj = {}
                    obj.ModelName = $scope.model_type[i].ModelName
                    obj.VehicleTypeName = ''

                    obj.Katashiki = ''
                    obj.Suffix = ''


                    $scope.listExcel_model_katashiki.push(obj)
                }
            }
        }

        console.log('jadi apa yaa', $scope.listExcel_model_katashiki)
    })

    $scope.TambahSpecialCampaign_GetModelData = function () {
        var dataResult = [];

        SpecialCampaignFactory.getDataModel()
            .then(
                function (res) {
                    angular.forEach(res.data.Result, function (value, key) {
                        dataResult.push({ "name": value.VehicleModelName, "value": value });
                    });

                    $scope.optionsTambahSpecialCampaign_Model = dataResult;
                }

            );
    }

    $scope.TambahSpecialCampaign_GetFullModelData = function () {
        var dataResult = [];
        if (angular.isUndefined($scope.TambahSpecialCampaign_Model))
            return;

        SpecialCampaignFactory.getDataFullModel(
            $scope.TambahSpecialCampaign_Model.VehicleModelId,
            $scope.TambahSpecialCampaign_CampaignDiscountId || null) // ga di lempar kalau lagi new campaign discount : yap
            .then(
                function (res) {
                    angular.forEach(res.data.Result, function (value, key) {
                        dataResult.push({ "name": value.VehicleDesc, "value": value });
                    });
                    console.log("data result full model : ", dataResult);
                    $scope.optionsTambahSpecialCampaign_FullModel = dataResult;
                }
            );
    }
    var tmpKatashikiCode = null;
    var tmpCampaignDiscountId = null;

    $scope.TambahSpecialCampaign_GetJasaData = function (KatashikiCode) {
        tmpKatashikiCode = angular.copy(KatashikiCode);
        tmpCampaignDiscountId = angular.copy($scope.TambahSpecialCampaign_CampaignDiscountId ? null : $scope.TambahSpecialCampaign_CampaignDiscountId);
        // var dataResult = [];
        // if (angular.isUndefined(KatashikiCode))
        //     return;

        // console.log("masuk TambahSpecialCampaign_GetJasaData");
        // //SpecialCampaignFactory.getDataServicebyKatashiki($scope.TambahSpecialCampaign_FullModel.KatashikiCode)
        // SpecialCampaignFactory.getDataServicebyKatashiki(KatashikiCode, $scope.TambahSpecialCampaign_CampaignDiscountId || null)
        //     .then(
        //         function(res) {
        //             angular.forEach(res.data.Result, function(value, key) {
        //                 dataResult.push({ "name": value.TaskCode, "value": value });
        //             });
        //             console.log("data result full model : ", dataResult);
        //             $scope.optionsTambahSpecialCampaignItems_Openo = dataResult;
        //         }
        //     );

    }
    $scope.noResults = false;
    $scope.getWork = function (key) {
        if (tmpKatashikiCode !== undefined) {
            // if (Katashiki != null && catg != null) {
            var ress = SpecialCampaignFactory.getDataServicebyKatashiki(tmpKatashikiCode, tmpCampaignDiscountId, key).then(function (resTask) {
                return resTask.data.Result;
            });
            console.log("ress", ress);
            return ress
            // }
        } else {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Data Mobil Terlebih Dahulu",
            });
        }
    };
    ///MAIN SPECIAL CAMPAIGN
    $scope.MainSpecialCampaign_Show = true;



    $scope.MainSpecialCampaign_Tambah_Clicked = function () {
        $scope.TambahSpecialCampaign_ClearFields();
        $scope.MainSpecialCampaign_Show = false;
        $scope.TambahSpecialCampaign_Show = true;
        $scope.TambahSpecialCampaign_ViewMode = false;
        $scope.TambahSpecialCampaign_ViewMode_Show = true;
        $scope.disFullModel = true;
        $scope.TambahSpecialCampaign_ColumnDef = [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "KatashikiCode", field: "KatashikiCode", visible: false },
            { name: "VehicleTypeId", field: "VehicleTypeId", visible: false },
            { name: "Model", field: "VehicleModelName" },
            { name: "Deskripsi", field: "Description" },
            { name: "Full Model", field: "VehicleDesc" },
            { name: "Diskon Item Parts", field: "TotalParts" },
            { name: "Diskon Item Jasa", field: "TotalTaskLists" },
            // {
            //     name: "Action",
            //     cellTemplate: '<div><a ng-click="grid.appScope.TambahSpecialCampaignTambahDiskon(row)">Tambah Diskon</a>&nbsp;&nbsp;<a ng-click="grid.appScope.TambahSpecialCampaignHapus(row)">Delete</a></div>'
            // },
            { name: "Action", cellTemplate: '<a ng-click="grid.appScope.TambahSpecialCampaignTambahDiskon(row)">&nbsp;&nbsp;&nbsp;<i class="fa fa-plus" aria-hidden="true" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Tambah Diskon"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahSpecialCampaignHapus(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="box-shadow:none!important; color:#777; font-size:16px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ];
        $scope.TambahSpecialCampaign_UIGrid.columnDefs = $scope.TambahSpecialCampaign_ColumnDef;
        $scope.TambahSpecialCampaign_GetModelData();
    }

    $scope.TambahSpecialCampaignHapus = function (row) {
        console.log('data grid', $scope.TambahSpecialCampaign_DataModel);
        $scope.TambahSpecialCampaign_DataModel.some(function (obj, i) {
            console.log("obj.VehicleModelName", obj.VehicleModelName);
            console.log("obj nih", obj);
            console.log("row nih", row);
            console.log("row.entity.VehicleModelName", $scope.SpecialCampaignDelete_DataId);
            // if (obj.VehicleModelName == $scope.SpecialCampaignDelete_DataId) {
            if (obj.VehicleDesc == row.entity.VehicleDesc) {
                $scope.TambahSpecialCampaign_DataModel.splice(i, 1);
                $scope.TambahSpecialCampaign_UIGrid.data = $scope.TambahSpecialCampaign_DataModel;
                $scope.SpecialCampaignDelete_DataId = 0;
            }

            // var hapusdata = $scope.modelDataCheck.split(',');
            if ($scope.modelDataCheck[i] === row.entity.KatashikiCode) {
                delete $scope.modelDataCheck[i];
            }
        });
        var dataJasaAfterDeleteModel = [];
        var dataPartsAfterDeleteModel = [];
        for (var i = 0; i < $scope.TambahSpecialCampaignJasa_DataModel.length; i++) {
            for (var j = 0; j < $scope.TambahSpecialCampaign_UIGrid.data.length; j++) {
                if ($scope.TambahSpecialCampaignJasa_DataModel[i].VehicleTypeId == $scope.TambahSpecialCampaign_UIGrid.data[j].VehicleTypeId) {
                    dataJasaAfterDeleteModel.push($scope.TambahSpecialCampaignJasa_DataModel[i]);
                }
            }
        }
        for (var i = 0; i < $scope.TambahSpecialCampaignParts_DataModel.length; i++) {
            for (var j = 0; j < $scope.TambahSpecialCampaign_UIGrid.data.length; j++) {
                if ($scope.TambahSpecialCampaignParts_DataModel[i].VehicleTypeId == $scope.TambahSpecialCampaign_UIGrid.data[j].VehicleTypeId) {
                    dataPartsAfterDeleteModel.push($scope.TambahSpecialCampaignParts_DataModel[i]);
                }
            }
        }
        $scope.TambahSpecialCampaignJasa_DataModel = dataJasaAfterDeleteModel
        $scope.TambahSpecialCampaignParts_DataModel = dataPartsAfterDeleteModel

        console.log('cek list jasa', $scope.TambahSpecialCampaignJasa_DataModel)
        console.log('cek list parts', $scope.TambahSpecialCampaignParts_DataModel)
    }

    // $scope.ModalSpecialCampaignModelDelete_Ya = function() {
    //     SpecialCampaignFactory.deleteDataModel($scope.SpecialCampaignModelDelete_DataId)
    //         .then(
    //             function(res) {
    //                 alert("data berhasil dihapus");
    //                 $scope.MainSpecialCampaignModel_UIGrid_Paging(1);
    //                 $scope.MainSpecialCampaignParts_UIGrid.data = [];
    //                 $scope.MainSpecialCampaignJasa_UIGrid.data = [];
    //                 $scope.SpecialCampaignModelDelete_DataId = 0;
    //             },
    //             function(err) {
    //                 alert(err.data.ExceptionMessage);
    //             }
    //         );

    //     $scope.ModalSpecialCampaignModelDelete_Tidak();
    // }

    // $scope.ModalSpecialCampaignModelDelete_Tidak = function() {
    //     $scope.ModalSpecialCampaignModelDelete_Data = '';
    //     $scope.SpecialCampaignModelDelete_DataId = 0;
    //     angular.element('#ModalSpecialCampaignModelDelete').modal('hide');
    // }

    $scope.MainSpecialCampaignJasaDelete = function (row) {
        bsAlert.alert({
            title: "Apakah Anda yakin ingin menghapus OPE No " + row.entity.TaskCode + " ?",
            text: "",
            type: "question",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        },
            function () {
                SpecialCampaignFactory.deleteDataTasks(row.entity.CampaignDiscountTypeTaskListId)
                    .then(
                        function (res) {
                            bsAlert.success("Data berhasil dihapus");
                            $scope.MainSpecialCampaignJasa_UIGrid_Paging(1);
                        },
                        function (err) {
                            alert(err.data.ExceptionMessage);
                        }
                    );
            },
            function () { }
        )

        // $scope.ModalSpecialCampaignJasaDelete_DataId = row.entity.CampaignDiscountTypeTaskListId;
        // $scope.ModalSpecialCampaignJasaDelete_Data = row.entity.TaskCode;
        // angular.element('#ModalSpecialCampaignJasaDelete').modal('show');
    }

    // $scope.ModalSpecialCampaignJasaDelete_Ya = function() {
    //     SpecialCampaignFactory.deleteDataTasks($scope.ModalSpecialCampaignJasaDelete_DataId)
    //         .then(
    //             function(res) {
    //                 alert("data berhasil dihapus");
    //                 $scope.MainSpecialCampaignJasa_UIGrid_Paging(1);
    //             },
    //             function(err) {
    //                 alert(err.data.ExceptionMessage);
    //             }
    //         );

    //     $scope.ModalSpecialCampaignJasaDelete_Tidak();
    // }

    // $scope.ModalSpecialCampaignJasaDelete_Tidak = function() {
    //     $scope.ModalSpecialCampaignJasaDelete_DataId = 0;
    //     $scope.ModalSpecialCampaignJasaDelete_Data = '';
    //     angular.element('#ModalSpecialCampaignJasaDelete').modal('hide');
    // }

    $scope.MainSpecialCampaignPartsDelete = function (row) {
        bsAlert.alert({
            title: "Apakah Anda yakin ingin menghapus Parts No " + row.entity.PartsNo + " ?",
            text: "",
            type: "question",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        },
            function () {
                SpecialCampaignFactory.deleteDataParts(row.entity.CampaignDiscountTypePartsId)
                    .then(
                        function (res) {
                            bsAlert.success("Data berhasil dihapus");
                            $scope.MainSpecialCampaignParts_UIGrid_Paging(1);
                        },
                        function (err) {
                            alert(err.data.ExceptionMessage);
                        }
                    );
            },
            function () { }
        )

        // $scope.ModalSpecialCampaignPartsDelete_DataId = row.entity.CampaignDiscountTypePartsId;
        // $scope.ModalSpecialCampaignPartsDelete_Data = row.entity.PartsName;
        // angular.element('#ModalSpecialCampaignPartsDelete').modal('show');

    }

    // $scope.ModalSpecialCampaignPartsDelete_Ya = function() {
    //     SpecialCampaignFactory.deleteDataParts($scope.ModalSpecialCampaignPartsDelete_DataId)
    //         .then(
    //             function(res) {
    //                 alert("data berhasil dihapus");
    //                 $scope.MainSpecialCampaignParts_UIGrid_Paging(1);
    //             },
    //             function(err) {
    //                 alert(err.data.ExceptionMessage);
    //             }
    //         );

    //     $scope.ModalSpecialCampaignPartsDelete_Tidak();
    // }

    // $scope.ModalSpecialCampaignPartsDelete_Tidak = function() {
    //     $scope.ModalSpecialCampaignPartsDelete_DataId = 0;
    //     $scope.ModalSpecialCampaignPartsDelete_Data = '';
    //     angular.element('#ModalSpecialCampaignPartsDelete').modal('hide');
    // }


    $scope.MainSpecialCampaignModelDelete = function (row) {
        // console.log('row',row);
        bsAlert.alert({
            title: "Apakah Anda yakin ingin menghapus " + row.entity.VehicleModelName + " " + row.entity.VehicleDesc + " ?",
            text: "",
            type: "question",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        },
            function () {
                SpecialCampaignFactory.deleteDataModel(row.entity.CampaignDiscountTypeId)
                    .then(
                        function (res) {
                            bsAlert.success("data berhasil dihapus");
                            $scope.MainSpecialCampaignModel_UIGrid_Paging(1);
                            $scope.MainSpecialCampaignParts_UIGrid.data = [];
                            $scope.MainSpecialCampaignJasa_UIGrid.data = [];
                            $scope.SpecialCampaignModelDelete_DataId = 0;
                        },
                        function (err) {
                            alert(err.data.ExceptionMessage);
                        }
                    );
            },
            function () { }
        )

        // $scope.ModalSpecialCampaignModelDelete_Data = row.entity.VehicleModelName;
        // $scope.SpecialCampaignModelDelete_DataId = row.entity.CampaignDiscountTypeId;
        // angular.element('#ModalSpecialCampaignModelDelete').modal('show');
    }

    $scope.MainSpecialCampaignDelete = function (row) {
        bsAlert.alert({
            title: "Apakah Anda yakin ingin menghapus " + row.entity.CampaignName + " ?",
            text: "",
            type: "question",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        },
            function () {
                SpecialCampaignFactory.deleteDataMain(row.entity.CampaignDiscountId)
                    .then(
                        function (res) {
                            bsAlert.success("Data berhasil dihapus");
                            $scope.MainSpecialCampaign_UIGrid_Paging(1);
                            $scope.MainSpecialCampaignModel_UIGrid.data = [];
                            $scope.MainSpecialCampaignParts_UIGrid.data = [];
                            $scope.MainSpecialCampaignJasa_UIGrid.data = [];
                            $scope.SpecialCampaignDelete_Data = '';
                            $scope.SpecialCampaignDelete_DataId = 0;
                        },
                        function (err) {
                            alert(err.data.ExceptionMessage);
                        }
                    );
            },
            function () { }
        )

        // $scope.ModalSpecialCampaignMainDelete_Data = row.entity.CampaignName;
        // $scope.SpecialCampaignDelete_DataId = row.entity.CampaignDiscountId;
        // angular.element('#ModalSpecialCampaignMainDelete').modal('show');
    }

    // $scope.ModalSpecialCampaignMainDelete_Ya = function() {
    //     SpecialCampaignFactory.deleteDataMain($scope.SpecialCampaignDelete_DataId)
    //         .then(
    //             function(res) {
    //                 alert("data berhasil dihapus");
    //                 $scope.MainSpecialCampaign_UIGrid_Paging(1);
    //                 $scope.MainSpecialCampaignModel_UIGrid.data = [];
    //                 $scope.MainSpecialCampaignParts_UIGrid.data = [];
    //                 $scope.MainSpecialCampaignJasa_UIGrid.data = [];
    //                 $scope.SpecialCampaignDelete_Data = '';
    //                 $scope.SpecialCampaignDelete_DataId = 0;
    //             },
    //             function(err) {
    //                 alert(err.data.ExceptionMessage);
    //             }
    //         );

    //     $scope.ModalSpecialCampaignMainDelete_Tidak();
    // }

    // $scope.ModalSpecialCampaignMainDelete_Tidak = function() {
    //     angular.element('#ModalSpecialCampaignMainDelete').modal('hide');
    // }

    $scope.MainSpecialCampaignEdit = function (row) {
        $scope.disFullModel = true;
        $scope.TambahSpecialCampaign_ClearFields();
        $scope.TambahSpecialCampaign_GetModelData();
        $scope.TambahSpecialCampaign_NamaCampaign_isDisabled = true;
        $scope.TambahSpecialCampaign_RangeKMAwal_isDisabled = true;
        $scope.TambahSpecialCampaign_RangeKMAkhir_isDisabled = true;
        $scope.TambahSpecialCampaign_ViewMode = false;
        $scope.TambahSpecialCampaign_ViewMode_Show = true;
        $scope.TambahSpecialCampaign_CampaignDiscountId = row.entity.CampaignDiscountId;
        $scope.TambahSpecialCampaign_NamaCampaign = row.entity.CampaignName;
        $scope.TambahSpecialCampaign_RangeKMAwal = row.entity.RangeKMStart;
        $scope.TambahSpecialCampaign_RangeKMAkhir = row.entity.RangeKMEnd;
        $scope.TambahSpecialCampaign_TahunRakitAwal = new Date(row.entity.AssemblyYearStart + '-01-01');
        $scope.TambahSpecialCampaign_TahunRakitAkhir = new Date(row.entity.AssemblyYearEnd + '-01-01');
        $scope.TambahSpecialCampaign_PeriodeAwal = row.entity.DateStart;
        $scope.TambahSpecialCampaign_PeriodeAkhir = row.entity.DateEnd;

        $scope.GetAllDataParts($scope.TambahSpecialCampaign_CampaignDiscountId);
        $scope.GetAllDataJasa($scope.TambahSpecialCampaign_CampaignDiscountId);

        $scope.TambahSpecialCampaign_ColumnDef = [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "KatashikiCode", field: "KatashikiCode", visible: false },
            { name: "VehicleTypeId", field: "VehicleTypeId", visible: false },
            { name: "Model", field: "VehicleModelName" },
            { name: "Deskripsi", field: "Description" },
            { name: "Full Model", field: "VehicleDesc" },
            { name: "Diskon Item Parts", field: "TotalParts" },
            { name: "Diskon Item Jasa", field: "TotalTaskLists" },
            // {
            //     name: "Action",
            //     cellTemplate: '<div><a ng-click="grid.appScope.TambahSpecialCampaignTambahDiskon(row)">Tambah Diskon</a>&nbsp;&nbsp;<a ng-click="grid.appScope.TambahSpecialCampaignHapus(row)">Delete</a></div>'
            // },
            { name: "Action", cellTemplate: '<a ng-click="grid.appScope.TambahSpecialCampaignTambahDiskon(row)">&nbsp;&nbsp;&nbsp;<i class="fa fa-plus" aria-hidden="true" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Tambah Diskon"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahSpecialCampaignHapus(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ];

        $scope.TambahSpecialCampaign_UIGrid.columnDefs = $scope.TambahSpecialCampaign_ColumnDef;
        $scope.TambahSpecialCampaign_UIGrid.data = [];
        SpecialCampaignFactory.getDataMainModel(1, 100, $scope.TambahSpecialCampaign_CampaignDiscountId)
            .then(
                function (res) {
                    $scope.TambahSpecialCampaign_DataModel = res.data.Result
                    $scope.TambahSpecialCampaign_UIGrid.data = $scope.TambahSpecialCampaign_DataModel;
                }
            );

        $scope.MainSpecialCampaign_Show = false;
        $scope.TambahSpecialCampaign_Show = true;

    }

    $scope.MainSpecialCampaignView = function (row) {
        $scope.disFullModel = true;
        $scope.TambahSpecialCampaign_ClearFields();
        $scope.TambahSpecialCampaign_NamaCampaign_isDisabled = true;
        $scope.TambahSpecialCampaign_RangeKMAwal_isDisabled = true;
        $scope.TambahSpecialCampaign_RangeKMAkhir_isDisabled = true;
        $scope.TambahSpecialCampaign_CampaignDiscountId = row.entity.CampaignDiscountId;
        $scope.TambahSpecialCampaign_NamaCampaign = row.entity.CampaignName;
        $scope.TambahSpecialCampaign_RangeKMAwal = row.entity.RangeKMStart;
        $scope.TambahSpecialCampaign_RangeKMAkhir = row.entity.RangeKMEnd;
        $scope.TambahSpecialCampaign_TahunRakitAwal = new Date(row.entity.AssemblyYearStart + '-01-01');
        $scope.TambahSpecialCampaign_TahunRakitAkhir = new Date(row.entity.AssemblyYearEnd + '-01-01');
        $scope.TambahSpecialCampaign_PeriodeAwal = row.entity.DateStart;
        $scope.TambahSpecialCampaign_PeriodeAkhir = row.entity.DateEnd;

        $scope.GetAllDataParts($scope.TambahSpecialCampaign_CampaignDiscountId);
        $scope.GetAllDataJasa($scope.TambahSpecialCampaign_CampaignDiscountId);

        $scope.TambahSpecialCampaign_ColumnDef = [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "KatashikiCode", field: "KatashikiCode", visible: false },
            { name: "VehicleTypeId", field: "VehicleTypeId", visible: false },
            { name: "Model", field: "VehicleModelName" },
            { name: "Deskripsi", field: "Description" },
            { name: "Full Model", field: "VehicleDesc" },
            { name: "Diskon Item Parts", field: "TotalParts" },
            { name: "Diskon Item Jasa", field: "TotalTaskLists" },
            // {
            //     name: "Action",
            //     cellTemplate: '<a ng-click="grid.appScope.TambahSpecialCampaignLihat(row)">View</a>'
            // },
            { name: "Action", width: 150, cellTemplate: '<a ng-click="grid.appScope.TambahSpecialCampaignLihat(row)"><i class="fa fa-list-alt fa-lg" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Lihat"></i></a>' },
        ];

        $scope.TambahSpecialCampaign_UIGrid.columnDefs = $scope.TambahSpecialCampaign_ColumnDef;

        $scope.TambahSpecialCampaign_UIGrid.data = [];

        SpecialCampaignFactory.getDataMainModel(1, 100, $scope.TambahSpecialCampaign_CampaignDiscountId)
            .then(
                function (res) {
                    $scope.GetAllDataParts($scope.TambahSpecialCampaign_CampaignDiscountId);
                    $scope.GetAllDataJasa($scope.TambahSpecialCampaign_CampaignDiscountId);

                    $scope.MainSpecialCampaign_Show = false;
                    $scope.TambahSpecialCampaign_Show = true;
                    $scope.TambahSpecialCampaign_ViewMode = true;
                    $scope.TambahSpecialCampaign_ViewMode_Show = false;
                    $scope.TambahSpecialCampaignItems_DiscountJasa_isDisabled = true;
                    $scope.TambahSpecialCampaignItems_DeskripsiJasa_isDisabled = true
                    $scope.TambahSpecialCampaignItems_NamaParts_isDisabled = true;
                    $scope.TambahSpecialCampaignItems_DiscountParts_isDisabled = true;
                    $scope.DeskripsiModel_isDisabled = true;

                    $scope.MainSpecialCampaign_Show = false;
                    $scope.TambahSpecialCampaign_Show = true;

                    $scope.TambahSpecialCampaign_DataModel = res.data.Result;
                    $scope.TambahSpecialCampaign_UIGrid.data = $scope.TambahSpecialCampaign_DataModel;
                    console.log("$scope.TambahSpecialCampaign_UIGrid.data", $scope.TambahSpecialCampaign_UIGrid.data);
                }
            );
    }

    $scope.GetAllDataParts = function (CampaignDiscountId) {
        SpecialCampaignFactory.GetAllDataSelectedParts(CampaignDiscountId)
            .then(
                function (res) {
                    console.log("res getAllDataParts : ", res);
                    $scope.TambahSpecialCampaignParts_DataModel = res.data.Result;

                    for (var i = 0; i < res.data.Result.length; i++) {
                        $scope.modelDataCheckParts.push(res.data.Result[i].PartsName);
                    }
                    console.log("modelDataCheckParts", $scope.modelDataCheckParts);
                }
            );
    }

    $scope.GetAllDataJasa = function (CampaignDiscountId) {
        SpecialCampaignFactory.GetAllDataSelectedJasa(CampaignDiscountId)
            .then(
                function (res) {
                    console.log("res getAllDataJasa : ", res);
                    $scope.TambahSpecialCampaignJasa_DataModel = res.data.Result;

                    for (var i = 0; i < res.data.Result.length; i++) {
                        $scope.modelDataCheckJasa.push(res.data.Result[i].TaskListName);
                    }
                    console.log("modelDataCheckJasa", $scope.modelDataCheckJasa);
                }
            );
    }
    ///TAMBAH SPECIAL CAMPAIGN

    $scope.TambahSpecialCampaign_ClearFields = function () {
        $scope.TambahSpecialCampaign_CampaignDiscountId = 0;
        $scope.TambahSpecialCampaign_NamaCampaign = '';
        $scope.TambahSpecialCampaign_RangeKMAwal = '';
        $scope.TambahSpecialCampaign_RangeKMAkhir = '';
        $scope.TambahSpecialCampaign_TahunRakitAwal = undefined;
        $scope.TambahSpecialCampaign_TahunRakitAkhir = undefined;
        $scope.TambahSpecialCampaign_PeriodeAwal = undefined;
        $scope.TambahSpecialCampaign_PeriodeAkhir = undefined;
        $scope.TambahSpecialCampaign_Model = undefined;
        $scope.TambahSpecialCampaign_FullModel = undefined;
        $scope.TambahSpecialCampaign_DeskripsiModel = '';
        $scope.TambahSpecialCampaign_UIGrid.data = [];
        $scope.TambahSpecialCampaign_DataModel = [];
        $scope.TambahSpecialCampaignParts_DataModel = [];
        $scope.TambahSpecialCampaignJasa_DataModel = [];
        $scope.MainSpecialCampaignModel_UIGrid.data = [];
        $scope.MainSpecialCampaignJasa_UIGrid.data = [];
        $scope.MainSpecialCampaignParts_UIGrid.data = [];
    }

    $scope.TambahSpecialCampaign_Kembali_Clicked = function () {
        $scope.TambahSpecialCampaign_ClearFields();
        $scope.MainSpecialCampaign_Show = true;
        $scope.TambahSpecialCampaign_Show = false;
        $scope.TambahSpecialCampaign_NamaCampaign_isDisabled = false;
        $scope.TambahSpecialCampaign_RangeKMAwal_isDisabled = false;
        $scope.TambahSpecialCampaign_RangeKMAkhir_isDisabled = false;
        $scope.TambahSpecialCampaign_ViewMode = false;
        $scope.TambahSpecialCampaign_ViewMode_Show = true;
        $scope.TambahSpecialCampaignItems_DiscountJasa_isDisabled = false;
        $scope.TambahSpecialCampaignItems_NamaParts_isDisabled = false;
        $scope.TambahSpecialCampaignItems_DiscountParts_isDisabled = false;
        $scope.DeskripsiModel_isDisabled = false;

        $scope.modelDataCheck = [];
        $scope.modelDataCheckJasa = [];
        $scope.modelDataCheckParts = [];
    }

    $scope.TambahSpecialCampaign_Model_Changed = function () {
        $scope.TambahSpecialCampaign_GetFullModelData();
        $scope.disFullModel = false;
    }

    $scope.TambahSpecialCampaign_FullModel_Changed = function () {
        if (!angular.isUndefined($scope.TambahSpecialCampaign_FullModel)) {
            $scope.TambahSpecialCampaign_DeskripsiModel = $scope.TambahSpecialCampaign_FullModel.Description;
            $scope.TambahSpecialCampaign_FullModel_KastashikiCode = $scope.TambahSpecialCampaign_FullModel.KatashikiCode;
            console.log("$scope.TambahSpecialCampaign_FullModel_KastashikiCode", $scope.TambahSpecialCampaign_FullModel_KastashikiCode);
        }
    }

    $scope.TambahSpecialCampaign_Tambah_Clicked = function () {
        var modelData = {};

        var cekstring = $scope.modelDataCheck.toString();
        if (cekstring === "") {
            var dataduplicate = 0
            for (var i = 0; i < $scope.TambahSpecialCampaign_UIGrid.data.length; i++) {
                if ($scope.TambahSpecialCampaign_UIGrid.data[i].KatashikiCode == $scope.TambahSpecialCampaign_FullModel.KatashikiCode) {
                    dataduplicate++;
                }
            }
            if (dataduplicate == 0) {
                $scope.TambahSpecialCampaign_DiscountTypeId = $scope.TambahSpecialCampaign_DiscountTypeId - 1;

                modelData = {
                    "CampaignDiscountTypeId": $scope.TambahSpecialCampaign_DiscountTypeId,
                    "VehicleModelName": $scope.TambahSpecialCampaign_Model.VehicleModelName,
                    "VehicleTypeId": $scope.TambahSpecialCampaign_FullModel.VehicleTypeId,
                    "Description": $scope.TambahSpecialCampaign_DeskripsiModel,
                    "VehicleDesc": $scope.TambahSpecialCampaign_FullModel.VehicleDesc,
                    "TotalParts": 0,
                    "TotalTaskLists": 0,
                    "KatashikiCode": $scope.TambahSpecialCampaign_FullModel.KatashikiCode
                };
                // $scope.modelDataCheck += "," + $scope.TambahSpecialCampaign_FullModel.KatashikiCode;
                $scope.modelDataCheck.push($scope.TambahSpecialCampaign_FullModel.KatashikiCode);
            } else {
                bsNotify.show({
                    type: 'danger',
                    title: "Full Model " + $scope.TambahSpecialCampaign_FullModel.KatashikiCode + " tidak boleh sama",
                    timeout: 10000
                });
                return -1;
            }

        } else if (cekstring.includes($scope.TambahSpecialCampaign_FullModel_KastashikiCode)) {
            bsNotify.show({
                type: 'danger',
                title: "Full Model " + $scope.TambahSpecialCampaign_FullModel.KatashikiCode + " tidak boleh sama",
                timeout: 10000
            });
            return -1;
        } else {
            $scope.TambahSpecialCampaign_DiscountTypeId = $scope.TambahSpecialCampaign_DiscountTypeId - 1;

            modelData = {
                "CampaignDiscountTypeId": $scope.TambahSpecialCampaign_DiscountTypeId,
                "VehicleModelName": $scope.TambahSpecialCampaign_Model.VehicleModelName,
                "VehicleTypeId": $scope.TambahSpecialCampaign_FullModel.VehicleTypeId,
                "Description": $scope.TambahSpecialCampaign_DeskripsiModel,
                "VehicleDesc": $scope.TambahSpecialCampaign_FullModel.VehicleDesc,
                "TotalParts": 0,
                "TotalTaskLists": 0,
                "KatashikiCode": $scope.TambahSpecialCampaign_FullModel.KatashikiCode
            };
            // $scope.modelDataCheck += "," + $scope.TambahSpecialCampaign_FullModel.KatashikiCode;
            $scope.modelDataCheck.push($scope.TambahSpecialCampaign_FullModel.KatashikiCode);
        }


        $scope.TambahSpecialCampaign_DataModel.push(modelData);
        $scope.TambahSpecialCampaign_UIGrid.data = $scope.TambahSpecialCampaign_DataModel;

        $scope.TambahSpecialCampaign_Model = undefined;
        $scope.TambahSpecialCampaign_FullModel = undefined;
        $scope.TambahSpecialCampaign_DeskripsiModel = '';
    }

    $scope.TambahSpecialCampaignItemsJasaFilter = function (DiscountTypeId) {
        // debugger;
        $scope.TambahSpecialCampaignJasa_DataModelView = [];
        var jasa = []
        angular.forEach($scope.TambahSpecialCampaignJasa_DataModel, function (value, key) {
            if (value.VehicleTypeId === DiscountTypeId)
                // $scope.TambahSpecialCampaignJasa_DataModelView.push(value);
                jasa.push(value);
        });
        $scope.TambahSpecialCampaignJasa_DataModelView = angular.copy(jasa);

        $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;

    }

    $scope.TambahSpecialCampaignItemsPartsFilter = function (DiscountTypeId) {
        $scope.TambahSpecialCampaignParts_DataModelView = [];
        var part = []

        angular.forEach($scope.TambahSpecialCampaignParts_DataModel, function (value, key) {
            if (value.VehicleTypeId === DiscountTypeId)
                // $scope.TambahSpecialCampaignParts_DataModelView.push(value);
                part.push(value)
        });
        $scope.TambahSpecialCampaignParts_DataModelView = angular.copy(part);
        $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;

    }

    $scope.TambahSpecialCampaignItems_ClearFields = function () {
        $scope.TambahSpecialCampaignItems_NamaCampaign = '';
        $scope.TambahSpecialCampaignItems_PeriodeAwal = undefined;
        $scope.TambahSpecialCampaignItems_PeriodeAkhir = undefined;
        $scope.TambahSpecialCampaignItems_RangeKMAwal = '';
        $scope.TambahSpecialCampaignItems_RangeKMAkhir = '';
        $scope.TambahSpecialCampaignItems_Model = '';
        $scope.TambahSpecialCampaignItems_TahunRakitAwal = undefined;
        $scope.TambahSpecialCampaignItems_TahunRakitAkhir = undefined;
        $scope.TambahSpecialCampaignItems_DiscountTypeId = 0;
        $scope.TambahSpecialCampaignItems_KatashikiCode = '';
        $scope.TambahSpecialCampaignItems_VehicleTypeId = 0;
        $scope.TambahSpecialCampaignJasa_DataModelView = [];
        $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;
        $scope.TambahSpecialCampaignParts_DataModelView = [];
        $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;
    }

    $scope.TambahSpecialCampaignTambahDiskon = function (row) {
        $scope.dataJasaYangDihapus = [];
        $scope.dataPartsYangDihapus = [];
        $scope.dataJasaYangDitambah = [];
        $scope.dataPartsYangDitambah = [];
        $scope.dataJasaYangDiedit = [];
        $scope.dataPartsYangDiedit = [];
        $scope.modelDataCheckJasa = [];
        $scope.modelDataCheckParts = [];

        $scope.TambahSpecialCampaignItems_ClearFields();
        $scope.TambahSpecialCampaignItems_Show = true;
        $scope.TambahSpecialCampaign_Show = false;
        console.log(" tahun rakit awal : ", new Date($scope.TambahSpecialCampaign_TahunRakitAwal + '-01-01'));

        $scope.TambahSpecialCampaignItems_NamaCampaign = $scope.TambahSpecialCampaign_NamaCampaign;
        $scope.TambahSpecialCampaignItems_PeriodeAwal = $scope.TambahSpecialCampaign_PeriodeAwal;
        $scope.TambahSpecialCampaignItems_PeriodeAkhir = $scope.TambahSpecialCampaign_PeriodeAkhir;
        $scope.TambahSpecialCampaignItems_RangeKMAwal = $scope.TambahSpecialCampaign_RangeKMAwal;
        $scope.TambahSpecialCampaignItems_RangeKMAkhir = $scope.TambahSpecialCampaign_RangeKMAkhir;
        $scope.TambahSpecialCampaignItems_Model = row.entity.VehicleModelName;
        $scope.TambahSpecialCampaignItems_TahunRakitAwal = new Date($scope.TambahSpecialCampaign_TahunRakitAwal + '-01-01');
        $scope.TambahSpecialCampaignItems_TahunRakitAkhir = new Date($scope.TambahSpecialCampaign_TahunRakitAkhir + '-01-01');

        $scope.TambahSpecialCampaignItems_DiscountTypeId = row.entity.CampaignDiscountTypeId;
        console.log("tambah diskon", $scope.TambahSpecialCampaign_CampaignDiscountId);
        console.log("vehicletypeid edit : ", row.entity.VehicleTypeId);
        $scope.TambahSpecialCampaignItems_KatashikiCode = row.entity.KatashikiCode;
        $scope.TambahSpecialCampaignItems_VehicleTypeId = row.entity.VehicleTypeId;
        $scope.TambahSpecialCampaign_GetJasaData($scope.TambahSpecialCampaignItems_KatashikiCode);
        $scope.TambahSpecialCampaignItemsJasaFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);
        $scope.TambahSpecialCampaignItemsPartsFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);
    }

    $scope.TambahSpecialCampaignLihat = function (row) {
        $scope.TambahSpecialCampaignItems_ClearFields();
        $scope.TambahSpecialCampaignItems_Show = true;
        $scope.TambahSpecialCampaign_Show = false;
        $scope.TambahSpecialCampaign_ViewMode = true;
        $scope.TambahSpecialCampaign_ViewMode_Show = false;
        console.log(" tahun rakit awal : ", new Date($scope.TambahSpecialCampaign_TahunRakitAwal + '-01-01'));

        $scope.TambahSpecialCampaignItems_NamaCampaign = $scope.TambahSpecialCampaign_NamaCampaign;
        $scope.TambahSpecialCampaignItems_PeriodeAwal = $scope.TambahSpecialCampaign_PeriodeAwal;
        $scope.TambahSpecialCampaignItems_PeriodeAkhir = $scope.TambahSpecialCampaign_PeriodeAkhir;
        $scope.TambahSpecialCampaignItems_RangeKMAwal = $scope.TambahSpecialCampaign_RangeKMAwal;
        $scope.TambahSpecialCampaignItems_RangeKMAkhir = $scope.TambahSpecialCampaign_RangeKMAkhir;
        $scope.TambahSpecialCampaignItems_Model = row.entity.VehicleModelName;
        $scope.TambahSpecialCampaignItems_TahunRakitAwal = new Date($scope.TambahSpecialCampaign_TahunRakitAwal + '-01-01');
        $scope.TambahSpecialCampaignItems_TahunRakitAkhir = new Date($scope.TambahSpecialCampaign_TahunRakitAkhir + '-01-01');
        console.log("row lihat : ", row);

        $scope.TambahSpecialCampaignItems_KatashikiCode = row.entity.KatashikiCode;
        $scope.TambahSpecialCampaignItems_VehicleTypeId = row.entity.VehicleTypeId;

        $scope.TambahSpecialCampaign_GetJasaData($scope.TambahSpecialCampaignItems_KatashikiCode);
        $scope.TambahSpecialCampaignItemsJasaFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);
        $scope.TambahSpecialCampaignItemsPartsFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);
    }

    $scope.TambahSpecialCampaign_Simpan_Clicked = function () {

        if ($scope.TambahSpecialCampaign_UIGrid.data.length > 0) {
            var dataInput = [{
                "CampaignDiscountId": $scope.TambahSpecialCampaign_CampaignDiscountId,
                "CampaignName": $scope.TambahSpecialCampaign_NamaCampaign,
                "RangeKMStart": $scope.TambahSpecialCampaign_RangeKMAwal,
                "RangeKMEnd": $scope.TambahSpecialCampaign_RangeKMAkhir,
                "AssemblyYearStart": $filter('date')($scope.TambahSpecialCampaign_TahunRakitAwal, 'yyyy'),
                "AssemblyYearEnd": $filter('date')($scope.TambahSpecialCampaign_TahunRakitAkhir, 'yyyy'),
                "DateStart": $filter('date')($scope.TambahSpecialCampaign_PeriodeAwal, 'yyyy-MM-dd'),
                "DateEnd": $filter('date')($scope.TambahSpecialCampaign_PeriodeAkhir, 'yyyy-MM-dd'),
                "ModelListModel": $scope.TambahSpecialCampaign_DataModel,
                "PartsListModel": $scope.TambahSpecialCampaignParts_DataModel,
                "TaskListModel": $scope.TambahSpecialCampaignJasa_DataModel
            }];

            console.log('isi data ke create', dataInput);

            SpecialCampaignFactory.createSpecialCampaign(dataInput)
                .then(
                    function (res) {
                        bsAlert.success("Data berhasil disimpan");
                        $scope.MainSpecialCampaign_UIGrid_Paging(1);
                        $scope.MainSpecialCampaign_UIGrid.paginationCurrentPage = 1
                        $scope.TambahSpecialCampaign_Kembali_Clicked();
                    }
                );
        } else {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Data Special Campaign Discount Tidak Boleh Kosong",
                timeout: 10000
            });
        }

        $scope.modelDataCheck = [];
        $scope.modelDataCheckJasa = [];
        $scope.modelDataCheckParts = [];
    };

    ///TAMBAH SPECIAL CAMPAIGN ITEMS
    $scope.TambahSpecialCampaignItems_Kembali_Clicked = function () {
        $scope.TambahSpecialCampaignItems_ClearFields();
        $scope.TambahSpecialCampaignItems_Show = false;
        if ($scope.EditSpecialCampaignModel_isMain == 0)
            $scope.TambahSpecialCampaign_Show = true;
        else {
            $scope.MainSpecialCampaign_Show = true;
            $scope.EditSpecialCampaignModel_isMain = 0;
        }
    }

    $scope.TambahSpecialItems_TambahJasa_Clicked = function () {
        //console.log("ok :",$scope.TambahSpecialCampaignItems_Openo);

        if ((_.isUndefined($scope.TambahSpecialCampaignItems_Openo)) || ((_.isNull($scope.TambahSpecialCampaignItems_Openo))) || ($scope.TambahSpecialCampaignItems_Openo == '') ||
            (_.isUndefined($scope.TambahSpecialCampaignItems_DeskripsiJasa)) || (_.isNull($scope.TambahSpecialCampaignItems_DeskripsiJasa)) || ($scope.TambahSpecialCampaignItems_DeskripsiJasa == '')) {

            bsNotify.show({
                title: "Special Campaign",
                content: "Tasklist harus di isi",
                type: 'danger',
                timeout: 800
            });

        } else {
            var dataInput = {};

            var cekstring = $scope.modelDataCheckJasa.toString();
            if (cekstring === "") {

                if ((_.isNull($scope.TambahSpecialCampaignItems_DiscountJasa) ||
                    (_.isUndefined($scope.TambahSpecialCampaignItems_DiscountJasa)) ||
                    ($scope.TambahSpecialCampaignItems_DiscountJasa == ''))) {

                    $scope.TambahSpecialCampaign_TypeTaskListId = $scope.TambahSpecialCampaign_TypeTaskListId - 1;

                    dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypeTaskListId": $scope.TambahSpecialCampaign_TypeTaskListId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "TaskListName": $scope.TambahSpecialCampaignItems_TaskName, //$scope.TambahSpecialCampaignItems_Openo.TaskName,
                        "TaskId": $scope.TambahSpecialCampaignItems_TaskId, //$scope.TambahSpecialCampaignItems_Openo.TaskId,
                        "TaskDescription": $scope.TambahSpecialCampaignItems_DeskripsiJasa,
                        "TaskDiscount": 0
                    }
                    $scope.modelDataCheckJasa.push($scope.TambahSpecialCampaignItems_TaskName);

                } else {

                    $scope.TambahSpecialCampaign_TypeTaskListId = $scope.TambahSpecialCampaign_TypeTaskListId - 1;

                    dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypeTaskListId": $scope.TambahSpecialCampaign_TypeTaskListId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "TaskListName": $scope.TambahSpecialCampaignItems_TaskName, //$scope.TambahSpecialCampaignItems_Openo.TaskName,
                        "TaskId": $scope.TambahSpecialCampaignItems_TaskId, //$scope.TambahSpecialCampaignItems_Openo.TaskId,
                        "TaskDescription": $scope.TambahSpecialCampaignItems_DeskripsiJasa,
                        "TaskDiscount": $scope.TambahSpecialCampaignItems_DiscountJasa
                    }
                    $scope.modelDataCheckJasa.push($scope.TambahSpecialCampaignItems_TaskName);

                }
                // } else if (cekstring.includes($scope.TambahSpecialCampaignItems_TaskName)) {

                //     bsNotify.show({
                //         type: 'danger',
                //         title: "Task List " + $scope.TambahSpecialCampaignItems_TaskName + " tidak boleh sama",
                //         timeout: 10000
                //     });
                //     return -1;

            } else {

                for (var i = 0; i < $scope.TambahSpecialCampaignJasa_UIGrid.data.length; i++) {
                    if ($scope.TambahSpecialCampaignJasa_UIGrid.data[i].TaskId == $scope.TambahSpecialCampaignItems_TaskId) {
                        bsNotify.show({
                            type: 'danger',
                            title: "Task List " + $scope.TambahSpecialCampaignItems_TaskName + " tidak boleh sama",
                            timeout: 10000
                        });
                        return -1;
                    }
                }

                if ((_.isNull($scope.TambahSpecialCampaignItems_DiscountJasa) ||
                    (_.isUndefined($scope.TambahSpecialCampaignItems_DiscountJasa)) ||
                    ($scope.TambahSpecialCampaignItems_DiscountJasa == ''))) {

                    $scope.TambahSpecialCampaign_TypeTaskListId = $scope.TambahSpecialCampaign_TypeTaskListId - 1;

                    dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypeTaskListId": $scope.TambahSpecialCampaign_TypeTaskListId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "TaskListName": $scope.TambahSpecialCampaignItems_TaskName, //$scope.TambahSpecialCampaignItems_Openo.TaskName,
                        "TaskId": $scope.TambahSpecialCampaignItems_TaskId, //$scope.TambahSpecialCampaignItems_Openo.TaskId,
                        "TaskDescription": $scope.TambahSpecialCampaignItems_DeskripsiJasa,
                        "TaskDiscount": 0
                    }
                    $scope.modelDataCheckJasa.push($scope.TambahSpecialCampaignItems_TaskName);

                } else {

                    $scope.TambahSpecialCampaign_TypeTaskListId = $scope.TambahSpecialCampaign_TypeTaskListId - 1;

                    dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypeTaskListId": $scope.TambahSpecialCampaign_TypeTaskListId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "TaskListName": $scope.TambahSpecialCampaignItems_TaskName, //$scope.TambahSpecialCampaignItems_Openo.TaskName,
                        "TaskId": $scope.TambahSpecialCampaignItems_TaskId, //$scope.TambahSpecialCampaignItems_Openo.TaskId,
                        "TaskDescription": $scope.TambahSpecialCampaignItems_DeskripsiJasa,
                        "TaskDiscount": $scope.TambahSpecialCampaignItems_DiscountJasa
                    }
                    $scope.modelDataCheckJasa.push($scope.TambahSpecialCampaignItems_TaskName);

                }
            }

            console.log("data input jasa : ", dataInput);
            $scope.dataJasaYangDitambah.push(dataInput)
            // $scope.TambahSpecialCampaignJasa_DataModel.push(dataInput);
            $scope.TambahSpecialCampaignJasa_DataModelView.push(dataInput);
            $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;

            $scope.TambahSpecialCampaignItems_TaskId = 0;
            $scope.TambahSpecialCampaignItems_TaskName = '';
            $scope.TambahSpecialCampaignItems_Openo = '';
            //$scope.TambahSpecialCampaignItems_Openo.TaskName = '';
            //$scope.TambahSpecialCampaignItems_Openo.TaskId = 0;
            $scope.TambahSpecialCampaignItems_DeskripsiJasa = '';
            $scope.TambahSpecialCampaignItems_DiscountJasa = '';
        }
    }
    $scope.onSelectWork = function (item, model, label) {
        if (!angular.isUndefined(item)) {
            $scope.TambahSpecialCampaignItems_TaskId = item.TaskId;
            $scope.TambahSpecialCampaignItems_TaskName = item.TaskName;
            $scope.TambahSpecialCampaignItems_DeskripsiJasa = item.TaskName;
            console.log('onSelectWork', $scope.TambahSpecialCampaignItems_DeskripsiJasa);
        }
    };
    $scope.onGotResult = function (log) {
        console.log(log);
    };
    $scope.onNoResult = function (log) {
        console.log(log);
    }

    $scope.TambahSpecialCampaignItems_Openo_Changed = function () {
        if (!angular.isUndefined($scope.TambahSpecialCampaignItems_Openo)) {
            $scope.TambahSpecialCampaignItems_DeskripsiJasa = $scope.TambahSpecialCampaignItems_Openo.TaskName;
        }
    }

    $scope.ModalSpecialCampaignSearchParts_Batal = function () {
        angular.element('#ModalSpecialCampaignSearchParts').modal('hide');
    }

    $scope.TambahSpecialItems_CariParts_Clicked = function () {
        $scope.ShowModalSearchParts();
    }

    $scope.TambahSpecialItems_TambahParts_Clicked = function () {

        // if ((_.isNull($scope.TambahSpecialCampaignItems_NoParts)) || 
        //     (_.isUndefined($scope.TambahSpecialCampaignItems_NoParts)) ||
        //     ($scope.TambahSpecialCampaignItems_NoParts == '')) {
        if ((_.isNull($scope.TambahSpecialCampaignItems_PartsId)) || (_.isUndefined($scope.TambahSpecialCampaignItems_PartsId)) || ($scope.TambahSpecialCampaignItems_PartsId == '') || ($scope.TambahSpecialCampaignItems_PartsId == 0)) {
            bsNotify.show({
                title: "Special Campaign",
                content: "Silahkan Pilih Parts Dari List Pencarian",
                type: 'danger'
            });

        } else {
            var dataInput = {};

            var cekstring = $scope.modelDataCheckParts.toString();
            if (cekstring === "") {

                if ((_.isNull($scope.TambahSpecialCampaignItems_DiscountParts)) ||
                    (_.isUndefined($scope.TambahSpecialCampaignItems_DiscountParts)) ||
                    ($scope.TambahSpecialCampaignItems_DiscountParts == '')) {

                    $scope.CampaignDiscountTypePartId = $scope.CampaignDiscountTypePartId - 1;

                    var dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypePartId": $scope.CampaignDiscountTypePartId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "PartsId": $scope.TambahSpecialCampaignItems_PartsId,
                        "PartsName": $scope.TambahSpecialCampaignItems_NamaParts,
                        "PartsNo": $scope.TambahSpecialCampaignItems_NoParts,
                        "PartsDesc": $scope.TambahSpecialCampaignItems_DeskripsiParts,
                        "PartsDiscount": 0
                    }
                    $scope.modelDataCheckParts.push($scope.TambahSpecialCampaignItems_NamaParts);

                } else {

                    $scope.CampaignDiscountTypePartId = $scope.CampaignDiscountTypePartId - 1;

                    var dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypePartId": $scope.CampaignDiscountTypePartId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "PartsId": $scope.TambahSpecialCampaignItems_PartsId,
                        "PartsName": $scope.TambahSpecialCampaignItems_NamaParts,
                        "PartsNo": $scope.TambahSpecialCampaignItems_NoParts,
                        "PartsDesc": $scope.TambahSpecialCampaignItems_DeskripsiParts,
                        "PartsDiscount": $scope.TambahSpecialCampaignItems_DiscountParts
                    }
                    $scope.modelDataCheckParts.push($scope.TambahSpecialCampaignItems_NamaParts);

                }
                // } else if (cekstring.includes($scope.TambahSpecialCampaignItems_NamaParts)) {

                //     bsNotify.show({
                //         type: 'danger',
                //         title: "Nama Parts " + $scope.TambahSpecialCampaignItems_NamaParts + " tidak boleh sama",
                //         timeout: 10000
                //     });
                //     return -1;

            } else {

                for (var i = 0; i < $scope.TambahSpecialCampaignParts_UIGrid.data.length; i++) {
                    if ($scope.TambahSpecialCampaignParts_UIGrid.data[i].PartsId == $scope.TambahSpecialCampaignItems_PartsId) {
                        bsNotify.show({
                            type: 'danger',
                            title: "Nama Parts " + $scope.TambahSpecialCampaignItems_NamaParts + " tidak boleh sama",
                            timeout: 10000
                        });
                        return -1;
                    }
                }

                if ((_.isNull($scope.TambahSpecialCampaignItems_DiscountParts)) ||
                    (_.isUndefined($scope.TambahSpecialCampaignItems_DiscountParts)) ||
                    ($scope.TambahSpecialCampaignItems_DiscountParts == '')) {

                    $scope.CampaignDiscountTypePartId = $scope.CampaignDiscountTypePartId - 1;

                    var dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypePartId": $scope.CampaignDiscountTypePartId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "PartsId": $scope.TambahSpecialCampaignItems_PartsId,
                        "PartsName": $scope.TambahSpecialCampaignItems_NamaParts,
                        "PartsNo": $scope.TambahSpecialCampaignItems_NoParts,
                        "PartsDesc": $scope.TambahSpecialCampaignItems_DeskripsiParts,
                        "PartsDiscount": 0
                    }
                    $scope.modelDataCheckParts.push($scope.TambahSpecialCampaignItems_NamaParts);

                } else {

                    $scope.CampaignDiscountTypePartId = $scope.CampaignDiscountTypePartId - 1;

                    var dataInput = {
                        "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                        "CampaignDiscountTypePartId": $scope.CampaignDiscountTypePartId,
                        "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
                        "PartsId": $scope.TambahSpecialCampaignItems_PartsId,
                        "PartsName": $scope.TambahSpecialCampaignItems_NamaParts,
                        "PartsNo": $scope.TambahSpecialCampaignItems_NoParts,
                        "PartsDesc": $scope.TambahSpecialCampaignItems_DeskripsiParts,
                        "PartsDiscount": $scope.TambahSpecialCampaignItems_DiscountParts
                    }
                    $scope.modelDataCheckParts.push($scope.TambahSpecialCampaignItems_NamaParts);

                }

            }

            console.log("data input parts : ", dataInput);
            $scope.dataPartsYangDitambah.push(dataInput)
            // $scope.TambahSpecialCampaignParts_DataModel.push(dataInput);
            $scope.TambahSpecialCampaignParts_DataModelView.push(dataInput);
            $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;

            // console.log('dataInput', dataInput);
            // console.log('TambahSpecialCampaignItems_NoParts', $scope.TambahSpecialCampaignItems_NoParts);
            $scope.TambahSpecialCampaignItems_NamaParts = '';
            $scope.TambahSpecialCampaignItems_NoParts = '';
            $scope.TambahSpecialCampaignItems_DeskripsiParts = '';
            $scope.TambahSpecialCampaignItems_DiscountParts = '';
            $scope.TambahSpecialCampaignItems_PartsId = 0;
        }
    }

    $scope.TambahSpecialCampaignItems_Simpan_Clicked = function () {
        console.log("$scope.EditSpecialCampaignModel_isMain SIMPAN", $scope.EditSpecialCampaignModel_isMain);
        if ($scope.EditSpecialCampaignModel_isMain == 0) {
            var TotalPartsDiscount = 0;
            var DiscountTypeId = 0;
            $scope.TambahSpecialCampaignParts_DataModelView.some(function (obj, i) {
                DiscountTypeId = obj.VehicleTypeId;
                TotalPartsDiscount = parseInt(TotalPartsDiscount) + 1;
            });

            var TotalJasaDiscount = 0;
            $scope.TambahSpecialCampaignJasa_DataModelView.some(function (obj, i) {
                DiscountTypeId = obj.VehicleTypeId;
                TotalJasaDiscount = parseInt(TotalJasaDiscount) + 1;
            });

            if (DiscountTypeId == 0) {
                if ($scope.dataJasaYangDihapus.length > 0) {
                    DiscountTypeId = $scope.dataJasaYangDihapus[0].VehicleTypeId;
                } else if ($scope.dataJasaYangDitambah.length > 0) {
                    DiscountTypeId = $scope.dataJasaYangDitambah[0].VehicleTypeId;
                } else if ($scope.dataPartsYangDihapus.length > 0) {
                    DiscountTypeId = $scope.dataPartsYangDihapus[0].VehicleTypeId;
                } else if ($scope.dataPartsYangDitambah.length > 0) {
                    DiscountTypeId = $scope.dataPartsYangDitambah[0].VehicleTypeId;
                } else {
                    console.log('ga ad perubahan jumlah task / part')
                }
            }

            console.log("TotalPartsDiscount : ", TotalPartsDiscount);
            console.log("TotalJasaDiscount : ", TotalJasaDiscount);
            console.log("DiscountTypeId : ", DiscountTypeId);
            console.log("data model : ", $scope.TambahSpecialCampaign_DataModel);
            $scope.TambahSpecialCampaign_DataModel.some(function (obj, i) {
                console.log("Cari obj.DiscountTypeId : ", obj.VehicleTypeId);
                if (obj.VehicleTypeId == DiscountTypeId) {
                    console.log("data ketemu");
                    $scope.TambahSpecialCampaign_DataModel[i].TotalParts = TotalPartsDiscount;
                    $scope.TambahSpecialCampaign_DataModel[i].TotalTaskLists = TotalJasaDiscount;
                }
            });



            if ($scope.dataJasaYangDihapus.length > 0) {

                var dataJasaAfterSave = [];
                for (var i = 0; i < $scope.dataJasaYangDihapus.length; i++) {
                    for (var j = 0; j < $scope.TambahSpecialCampaignJasa_DataModel.length; j++) {
                        if ($scope.dataJasaYangDihapus[i].CampaignDiscountTypeTaskListId == $scope.TambahSpecialCampaignJasa_DataModel[j].CampaignDiscountTypeTaskListId) {
                            $scope.TambahSpecialCampaignJasa_DataModel[j].isDeleted = 1;
                        }

                    }
                }
                for (var k = 0; k < $scope.TambahSpecialCampaignJasa_DataModel.length; k++) {
                    if ($scope.TambahSpecialCampaignJasa_DataModel[k].isDeleted != 1) {
                        dataJasaAfterSave.push($scope.TambahSpecialCampaignJasa_DataModel[k])
                    }
                }
                $scope.TambahSpecialCampaignJasa_DataModel = dataJasaAfterSave

            }
            if ($scope.dataJasaYangDiedit.length > 0) {
                for (var i = 0; i < $scope.TambahSpecialCampaignJasa_DataModel.length; i++) {
                    for (var j = 0; j < $scope.dataJasaYangDiedit.length; j++) {
                        if ($scope.TambahSpecialCampaignJasa_DataModel[i].TaskId == $scope.dataJasaYangDiedit[j].TaskId) {
                            $scope.TambahSpecialCampaignJasa_DataModel[i].TaskDiscount = $scope.dataJasaYangDiedit[j].TaskDiscount
                        }
                    }
                }
            }
            if ($scope.dataJasaYangDitambah.length > 0) {
                for (var i = 0; i < $scope.dataJasaYangDitambah.length; i++) {
                    $scope.dataJasaYangDitambah[i].CampaignDiscountTypeTaskListId = -1;
                    $scope.dataJasaYangDitambah[i].CampaignDiscountTypeId = -1;
                    $scope.TambahSpecialCampaignJasa_DataModel.push($scope.dataJasaYangDitambah[i])
                }
            }

            if ($scope.dataPartsYangDihapus.length > 0) {
                var dataPartsAfterSave = [];
                for (var i = 0; i < $scope.dataPartsYangDihapus.length; i++) {
                    for (var j = 0; j < $scope.TambahSpecialCampaignParts_DataModel.length; j++) {
                        if ($scope.dataPartsYangDihapus[i].CampaignDiscountTypePartsId == $scope.TambahSpecialCampaignParts_DataModel[j].CampaignDiscountTypePartsId) {
                            $scope.TambahSpecialCampaignParts_DataModel[j].isDeleted = 1;
                        }

                    }
                }
                for (var k = 0; k < $scope.TambahSpecialCampaignParts_DataModel.length; k++) {
                    if ($scope.TambahSpecialCampaignParts_DataModel[k].isDeleted != 1) {
                        dataPartsAfterSave.push($scope.TambahSpecialCampaignParts_DataModel[k])
                    }
                }
                $scope.TambahSpecialCampaignParts_DataModel = dataPartsAfterSave
            }
            if ($scope.dataPartsYangDiedit.length > 0) {
                for (var i = 0; i < $scope.TambahSpecialCampaignParts_DataModel.length; i++) {
                    for (var j = 0; j < $scope.dataPartsYangDiedit.length; j++) {
                        if ($scope.TambahSpecialCampaignParts_DataModel[i].PartsNo == $scope.dataPartsYangDiedit[j].PartsNo) {
                            $scope.TambahSpecialCampaignParts_DataModel[i].PartsDiscount = $scope.dataPartsYangDiedit[j].PartsDiscount
                        }
                    }
                }
            }
            if ($scope.dataPartsYangDitambah.length > 0) {
                for (var i = 0; i < $scope.dataPartsYangDitambah.length; i++) {
                    $scope.dataPartsYangDitambah[i].CampaignDiscountTypePartsId = -1;
                    $scope.dataPartsYangDitambah[i].CampaignDiscountTypePartId = -1;
                    $scope.dataPartsYangDitambah[i].CampaignDiscountTypeId = -1;
                    $scope.TambahSpecialCampaignParts_DataModel.push($scope.dataPartsYangDitambah[i])
                }
            }



            $scope.TambahSpecialCampaignItems_Kembali_Clicked();

        } else {
            var dataInput = [{
                "CampaignDiscountId": $scope.TambahSpecialCampaign_CampaignDiscountId,
                "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
                // "PartsListModel": $scope.TambahSpecialCampaignParts_DataModel,
                "PartsListModel": $scope.TambahSpecialCampaignParts_UIGrid.data,
                // "TaskListModel" : $scope.TambahSpecialCampaignJasa_DataModel
                "TaskListModel": $scope.TambahSpecialCampaignJasa_UIGrid.data


            }];
            SpecialCampaignFactory.createSpecialCampaignModel(dataInput)
                .then(
                    function (res) {
                        alert("Data berhasil disimpan");
                        $scope.MainSpecialCampaignJasa_UIGrid.data = [];
                        $scope.MainSpecialCampaignParts_UIGrid.data = [];
                        $scope.MainSpecialCampaignModel_UIGrid_Paging(1);
                        $scope.TambahSpecialCampaignItems_Kembali_Clicked();
                    }
                );
        }
    }

    $scope.MainSpecialCampaignModelEdit = function (row) {
        $scope.dataJasaYangDihapus = [];
        $scope.dataPartsYangDihapus = [];
        $scope.dataJasaYangDitambah = [];
        $scope.dataPartsYangDitambah = [];
        $scope.dataJasaYangDiedit = [];
        $scope.dataPartsYangDiedit = [];
        $scope.modelDataCheckJasa = [];
        $scope.modelDataCheckParts = [];
        console.log("row in some", row);
        var MainDataRow = [];
        $scope.TambahSpecialCampaignItems_ClearFields();
        $scope.MainSpecialCampaign_Data.some(function (obj, i) {
            console.log("obj.CampaignDiscountId", obj.CampaignDiscountId);
            console.log("row.entity.CampaignDiscountId", row.entity.CampaignDiscountId);
            if (obj.CampaignDiscountId == row.entity.CampaignDiscountId) {
                MainDataRow = obj;
                console.log("MainDataRow in some", MainDataRow);
            }
        });


        console.log(" tahun rakit awal : ", new Date($scope.TambahSpecialCampaign_TahunRakitAwal + '-01-01'));
        console.log("MainDataRow", MainDataRow);
        $scope.TambahSpecialCampaign_CampaignDiscountId = MainDataRow.CampaignDiscountId;
        $scope.TambahSpecialCampaignItems_NamaCampaign = MainDataRow.CampaignName;
        $scope.TambahSpecialCampaignItems_PeriodeAwal = MainDataRow.DateStart;
        $scope.TambahSpecialCampaignItems_PeriodeAkhir = MainDataRow.DateEnd;
        $scope.TambahSpecialCampaignItems_RangeKMAwal = MainDataRow.RangeKMStart;
        $scope.TambahSpecialCampaignItems_RangeKMAkhir = MainDataRow.RangeKMEnd;
        $scope.TambahSpecialCampaignItems_TahunRakitAwal = new Date(MainDataRow.AssemblyYearStart + '-01-01');
        $scope.TambahSpecialCampaignItems_TahunRakitAkhir = new Date(MainDataRow.AssemblyYearEnd + '-01-01');

        $scope.TambahSpecialCampaignItems_Model = row.entity.VehicleModelName;
        $scope.TambahSpecialCampaignItems_DiscountTypeId = row.entity.CampaignDiscountTypeId;
        $scope.TambahSpecialCampaignItems_KatashikiCode = row.entity.KatashikiCode;
        $scope.TambahSpecialCampaignItems_VehicleTypeId = row.entity.VehicleTypeId;
        $scope.TambahSpecialCampaign_GetJasaData($scope.TambahSpecialCampaignItems_KatashikiCode);

        $scope.TambahSpecialCampaignItemsJasaFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);
        $scope.TambahSpecialCampaignItemsPartsFilter($scope.TambahSpecialCampaignItems_VehicleTypeId);

        console.log("$scope.TambahSpecialCampaignItems_Show", $scope.TambahSpecialCampaignItems_Show);
        $scope.TambahSpecialCampaignItems_Show = true;
        $scope.MainSpecialCampaign_Show = false;
        $scope.TambahSpecialCampaign_ViewMode = false;
        $scope.TambahSpecialCampaign_ViewMode_Show = true;
        $scope.EditSpecialCampaignModel_isMain = 1;
    }

    $scope.TambahSpecialCampaignPartsDelete = function (row) {
        //here
        var deleteId = -1;

        $scope.TambahSpecialCampaignParts_DataModel.some(function (obj, i) {
            console.log('obj', obj);
            console.log('row', row);

            // if (obj.PartsNo == row.entity.PartsNo){
            //     deleteId = i;
            // }
            // if (obj.PartsId == row.entity.PartsId) {
            //     $scope.TambahSpecialCampaignParts_DataModel.splice(i, 1);
            //     $scope.TambahSpecialCampaignParts_DataModelView.splice(i, 1);
            //     $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;
            //     // $scope.SpecialCampaignDelete_DataId = 0;
            // }

            if ($scope.modelDataCheckParts[i] === row.entity.PartsName) {
                delete $scope.modelDataCheckParts[i];
            }

        });

        console.log('alam', $scope.dataPartsYangDitambah)
        for (var i = 0; i < $scope.dataPartsYangDitambah.length; i++) {
            if ($scope.dataPartsYangDitambah[i].PartsId == row.entity.PartsId) {
                $scope.dataPartsYangDitambah.splice(i, 1);
            }
        }
        for (var i = 0; i < $scope.dataPartsYangDiedit.length; i++) {
            if ($scope.dataPartsYangDiedit[i].PartsId == row.entity.PartsId) {
                $scope.dataPartsYangDiedit.splice(i, 1);
            }
        }
        $scope.TambahSpecialCampaignParts_DataModelView.some(function (obj, i) {
            console.log('obj', obj);

            if (obj.PartsId == row.entity.PartsId) {
                $scope.dataPartsYangDihapus.push(obj)
                $scope.TambahSpecialCampaignParts_DataModelView.splice(i, 1);
                $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;
            }
        });

        // $scope.TambahSpecialCampaignParts_DataModel.splice(deleteId, 1);
        // $scope.TambahSpecialCampaignParts_DataModelView.splice(deleteId, 1);
        // $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;
    }

    ///TambahSpecialCampaign_UIGrid
    $scope.TambahSpecialCampaign_UIGrid = {
        paginationPageSizes: null,
        enableSelectAll: false,
        multiSelect: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: $scope.TambahSpecialCampaign_ColumnDef,
        onRegisterApi: function (gridApi) {
            $scope.GridApiTambahSpecialCampaign = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.TambahSpecialCampaign_UIGrid_Paging(pageNumber);
            });
        }
    };

    // $scope.TambahSpecialCampaign_UIGrid_Paging = function(pageNumber) {
    //     SpecialCampaignFactory.getDataMainEachCampaign(pageNumber, uiGridPageSize)
    //         .then(
    //             function(res) {
    //                 console.log("Get data nomor pajak", res.data.result);
    //                 $scope.TambahSpecialCampaign_DataModel = res.data.Result;
    //                 $scope.TambahSpecialCampaign_UIGrid.data = $scope.TambahSpecialCampaign_DataModel;
    //                 $scope.TambahSpecialCampaign_UIGrid.totalItems = res.data.Total;
    //                 $scope.TambahSpecialCampaign_UIGrid.paginationPageSize = uiGridPageSize;
    //                 $scope.TambahSpecialCampaign_UIGrid.paginationPageSizes = [uiGridPageSize];

    //             }

    //         );
    // }

    ///TambahSpecialCampaignJasa_UIGrid
    $scope.TambahSpecialCampaignJasa_UIGrid = {
        paginationPageSizes: null,
        multiSelect: false,
        enableSelectAll: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "CampaignDiscountTypeTaskListId", field: "CampaignDiscountTypeTaskListId", visible: false },
            { name: "VehicleTypeId", field: "VehicleTypeId", visible: false },
            { name: "TaskId", field: "TaskId", visible: false },
            { name: "Nama Jasa", field: "TaskListName" },
            { name: "Deskripsi", field: "TaskDescription" },
            { name: "Discount", field: "TaskDiscount" },
            // {
            //     name: "Action",
            //     cellTemplate: '<div><a ng-click="grid.appScope.TambahSpecialCampaignJasaDelete(row)">Delete</a></div>'
            // },
            { name: "Action", cellTemplate: '<a ng-click="grid.appScope.EditSpecialCampaignItemsJasa(row)"><i class="fa fa-pencil" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Ubah"></i></a><a ng-click="grid.appScope.TambahSpecialCampaignJasaDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiTambahSpecialCampaignJasa = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.TambahSpecialCampaignJasa_UIGrid_Paging(pageNumber);
            });
        }
    };

    $scope.TambahSpecialCampaignJasaDelete = function (row) {
        //here 
        var deleteId = -1;
        $scope.TambahSpecialCampaignJasa_DataModel.some(function (obj, i) {
            console.log('obj', obj);

            // if (obj.PartsNo == row.entity.PartsNo){
            //     deleteId = i;
            // }

            // if (obj.TaskId == row.entity.TaskId) {
            //     $scope.TambahSpecialCampaignJasa_DataModel.splice(i, 1);
            //     $scope.TambahSpecialCampaignJasa_DataModelView.splice(i, 1);
            //     $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;
            //     // $scope.SpecialCampaignDelete_DataId = 0;
            // }

            if ($scope.modelDataCheckJasa[i] === row.entity.TaskListName) {
                delete $scope.modelDataCheckJasa[i];
            }

        });

        console.log('alam', $scope.dataJasaYangDitambah)
        for (var i = 0; i < $scope.dataJasaYangDitambah.length; i++) {
            if ($scope.dataJasaYangDitambah[i].TaskId == row.entity.TaskId) {
                $scope.dataJasaYangDitambah.splice(i, 1);
            }
        }
        for (var i = 0; i < $scope.dataJasaYangDiedit.length; i++) {
            if ($scope.dataJasaYangDiedit[i].TaskId == row.entity.TaskId) {
                $scope.dataJasaYangDiedit.splice(i, 1);
            }
        }
        $scope.TambahSpecialCampaignJasa_DataModelView.some(function (obj, i) {
            console.log('obj', obj);
            if (obj.TaskId == row.entity.TaskId) {
                $scope.dataJasaYangDihapus.push(obj);
                $scope.TambahSpecialCampaignJasa_DataModelView.splice(i, 1);
                $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;
            }
        });

        // $scope.TambahSpecialCampaignJasa_DataModel.splice(deleteId, 1);
        // $scope.TambahSpecialCampaignJasa_DataModelView.splice(deleteId, 1);
        // $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;
    }

    $scope.TambahSpecialCampaignJasa_UIGrid_Paging = function (pageNumber) {
        SpecialCampaignFactory.getDataMainJasa(pageNumber, uiGridPageSize, $scope.TambahSpecialCampaignItems_DiscountTypeId)
            .then(
                function (res) {
                    console.log("Get data nomor pajak", res.data.result);
                    $scope.TambahSpecialCampaignJasa_DataModel = res.data.Result;
                    $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModel;
                    $scope.TambahSpecialCampaignJasa_UIGrid.totalItems = res.data.Total;
                    $scope.TambahSpecialCampaignJasa_UIGrid.paginationPageSize = uiGridPageSize;
                    $scope.TambahSpecialCampaignJasa_UIGrid.paginationPageSizes = [uiGridPageSize];

                }

            );
    }

    ///TambahSpecialCampaignParts_UIGrid
    $scope.TambahSpecialCampaignParts_UIGrid = {
        paginationPageSizes: null,
        multiSelect: false,
        enableSelectAll: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "CampaignDiscountTypePartsId", field: "CampaignDiscountTypePartsId", visible: false },
            { name: "PartsId", field: "PartsId", visible: false },
            { name: "VehicleTypeId", field: "VehicleTypeId", visible: false },
            { name: "Nama Parts", field: "PartsName" },
            { name: "Nomor Parts", field: "PartsNo" },
            { name: "Deskripsi", field: "PartsDesc", visible: false },
            { name: "Discount", field: "PartsDiscount" },
            // {
            //     name: "Action",
            //     cellTemplate: '<div><a ng-click="grid.appScope.TambahSpecialCampaignPartsDelete(row)">Delete</a></div>'
            // },
            { name: "Action", cellTemplate: '<a ng-click="grid.appScope.EditSpecialCampaignItemsParts(row)"><i class="fa fa-pencil" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Ubah"></i></a><a ng-click="grid.appScope.TambahSpecialCampaignPartsDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiTambahSpecialCampaignParts = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.TambahSpecialCampaignParts_UIGrid_Paging(pageNumber);
            });
        }
    };

    $scope.TambahSpecialCampaignParts_UIGrid_Paging = function (pageNumber) {
        SpecialCampaignFactory.getDataMainParts(pageNumber, uiGridPageSize, $scope.TambahSpecialCampaignItems_DiscountTypeId)
            .then(
                function (res) {
                    console.log("Get dataTambahSpecialCampaignParts_UIGrid_Paging", res.data.Result);
                    $scope.TambahSpecialCampaignParts_DataModel = res.data.Result;
                    $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModel;
                    $scope.TambahSpecialCampaignParts_UIGrid.totalItems = res.data.Total;
                    $scope.TambahSpecialCampaignParts_UIGrid.paginationPageSize = uiGridPageSize;
                    $scope.TambahSpecialCampaignParts_UIGrid.paginationPageSizes = [uiGridPageSize];

                }

            );
    }

    ///MainSpecialCampaign_UIGrid
    $scope.MainSpecialCampaign_UIGrid = {
        // paginationPageSizes: null,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        useCustomPagination: true,
        useExternalPagination: true,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        enableSorting: true,
        columnDefs: [
            // { name: "CampaignDiscountId", field: "CampaignDiscountId", visible: false },
            { name: "Nama Campaign", field: "CampaignName", width: "25%" },
            { name: "Range KM Awal", field: "RangeKMStart", width: "12.5%" },
            { name: "Range KM Akhir", field: "RangeKMEnd", width: "12.5%" },
            { name: "Tahun Rakit Awal", field: "AssemblyYearStart", width: "10%" },
            { name: "Tahun Rakit Akhir", field: "AssemblyYearEnd", width: "10%" },
            { name: "Periode Awal", field: "DateStart", type: 'date', width: "10%", cellFilter: 'date:\'dd/MM/yyyy\'' },
            { name: "Periode End", field: "DateEnd", type: 'date', width: "10%", cellFilter: 'date:\'dd/MM/yyyy\'' },
            // {
            //     name: "Action", width: 150,
            //     cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignView(row)">View</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignEdit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignDelete(row)">Delete</a>'
            // },
            { name: "Action", width: "10%", cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignEdit(row)">&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil" aria-hidden="true" style="color:#777; font-size:16px; margin-top:6px; margin-left:10px; margin-right:5px" title="Ubah"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignView(row)"><i class="fa fa-list-alt fa-lg" aria-hidden="true" style="color:#777; font-size:16px; margin-top:6px; margin-right:5px" title="Lihat"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-top:6px; margin-right:5px" title="Hapus"></i></a>' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMainSpecialCampaign = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.MainSpecialCampaign_UIGrid_Paging(pageNumber, pageSize);
            });

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.MainSpecialCampaign_CampaignDiscountId = row.entity.CampaignDiscountId;
                $scope.NamaCampaign = row.entity.CampaignName;
                console.log("isi discount id : ", row.entity);
                $scope.MainSpecialCampaignModel_UIGrid_Paging(1);
            });
        }
    };

    $scope.MainSpecialCampaign_UIGrid_Paging = function (pageNumber, pageSize) {
        if (pageSize == null || pageSize == undefined) {
            pageSize = 10;
        }
        SpecialCampaignFactory.getDataMain(pageNumber, pageSize)
            .then(
                function (res) {
                    console.log("Get data nomor pajak", res.data.Result);
                    $scope.MainSpecialCampaign_Data = res.data.Result;
                    $scope.MainSpecialCampaign_UIGrid.data = $scope.MainSpecialCampaign_Data;
                    // $scope.MainSpecialCampaign_UIGrid.totalItems = res.data.Result[0].Total;
                    $scope.MainSpecialCampaign_UIGrid.totalItems = res.data.Result[0].TotalData;
                    // $scope.MainSpecialCampaign_UIGrid.paginationPageSize = uiGridPageSize;
                    // $scope.MainSpecialCampaign_UIGrid.paginationPageSizes = [uiGridPageSize];

                }

            );
    }


    $scope.MainSpecialCampaign_UIGrid_Paging(1);

    ///MainSpecialCampaignModel_UIGrid
    $scope.MainSpecialCampaignModel_UIGrid = {
        // paginationPageSizes: null,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        multiSelect: false,
        enableSelectAll: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: [
            { name: "CampaignDiscountId", field: "CampaignDiscountId", visible: false },
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "Nama Campaign", field: "CampaignName" },
            { name: "List Model", field: "VehicleModelName" },
            { name: "Full Model", field: "Description" },
            { name: "Deskripsi", field: "VehicleDesc" },
            // {
            //     name: "Action",
            //     cellTemplate: '<div><a ng-click="grid.appScope.MainSpecialCampaignModelEdit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignModelDelete(row)">Delete</a></div>'
            // },
            // { name: "Action", cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignModelEdit(row)">&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil" aria-hidden="true" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Ubah"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainSpecialCampaignModelDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMainSpecialModelCampaign = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.MainSpecialCampaignModel_UIGrid_Paging(pageNumber, pageSize);
            });

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.MainSpecialCampaign_CampaignDiscountTypeId = row.entity.CampaignDiscountTypeId;
                $scope.NamaModel = row.entity.VehicleModelName;
                $scope.MainSpecialCampaignJasa_UIGrid_Paging(1);
                $scope.MainSpecialCampaignParts_UIGrid_Paging(1);
            });
        }
    };

    $scope.MainSpecialCampaignModel_UIGrid_Paging = function (pageNumber, pageSize) {
        if (pageSize == null || pageSize == undefined) {
            pageSize = 10;
        }
        SpecialCampaignFactory.getDataMainModel(pageNumber, pageSize, $scope.MainSpecialCampaign_CampaignDiscountId)
            .then(
                function (res) {
                    console.log("MainSpecialCampaignModel_UIGrid", res.data.Result);
                    $scope.MainSpecialCampaignModel_UIGrid.data = res.data.Result;
                    $scope.MainSpecialCampaignModel_UIGrid.totalItems = res.data.Total;
                    // $scope.MainSpecialCampaignModel_UIGrid.paginationPageSize = uiGridPageSize;
                    // $scope.MainSpecialCampaignModel_UIGrid.paginationPageSizes = [uiGridPageSize];

                    $scope.GetAllDataParts($scope.MainSpecialCampaign_CampaignDiscountId);
                    $scope.GetAllDataJasa($scope.MainSpecialCampaign_CampaignDiscountId);
                }

            );
    }

    ///MainSpecialCampaignJasa_UIGrid
    $scope.MainSpecialCampaignJasa_UIGrid = {
        // paginationPageSizes: null,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        multiSelect: false,
        enableSelectAll: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "CampaignDiscountTypeTaskListId", field: "CampaignDiscountTypeTaskListId", visible: false },
            { name: "OPE No", field: "TaskCode" },
            { name: "Deskripsi", field: "TaskDescription" },
            { name: "Diskon", field: "TaskDiscount" },
            // {
            //     name: "Action",
            //     cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignJasaDelete(row)">Delete</a></div>'
            // },
            // { name: "Action", cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignJasaDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },            
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMainSpecialCampaignJasa = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.MainSpecialCampaignJasa_UIGrid_Paging(pageNumber, pageSize);
            });

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {

            });
        }
    };

    $scope.MainSpecialCampaignJasa_UIGrid_Paging = function (pageNumber, pageSize) {
        if (pageSize == null || pageSize == undefined) {
            pageSize = 10;
        }
        SpecialCampaignFactory.getDataMainJasa(pageNumber, pageSize, $scope.MainSpecialCampaign_CampaignDiscountTypeId)
            .then(
                function (res) {
                    console.log("MainSpecialCampaignJasa_UIGrid", res.data.Result);
                    $scope.MainSpecialCampaignJasa_UIGrid.data = res.data.Result;
                    $scope.MainSpecialCampaignJasa_UIGrid.totalItems = res.data.Total;
                    // $scope.MainSpecialCampaignJasa_UIGrid.paginationPageSize = uiGridPageSize;
                    // $scope.MainSpecialCampaignJasa_UIGrid.paginationPageSizes = [uiGridPageSize];

                }

            );
    }

    ///MainSpecialCampaignParts_UIGrid
    $scope.MainSpecialCampaignParts_UIGrid = {
        // paginationPageSizes: null,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        multiSelect: false,
        enableSelectAll: false,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        columnDefs: [
            { name: "CampaignDiscountTypeId", field: "CampaignDiscountTypeId", visible: false },
            { name: "CampaignDiscountTypePartsId", field: "CampaignDiscountTypePartsId", visible: false },
            { name: "Parts No", field: "PartsNo" },
            { name: "Nama Parts", field: "PartsName" },
            { name: "Diskon", field: "PartsDiscount" },
            // {
            //     name: "Action",
            //     cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignPartsDelete(row)">Delete</a></div>'
            // },
            // { name: "Action", cellTemplate: '<a ng-click="grid.appScope.MainSpecialCampaignPartsDelete(row)"><i class="fa fa-fw fa-lg fa-trash" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Hapus"></i></a>' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridApiMainSpecialCampaignParts = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                $scope.MainSpecialCampaignParts_UIGrid_Paging(pageNumber, pageSize);
            });

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {

            });
        }
    };

    $scope.MainSpecialCampaignParts_UIGrid_Paging = function (pageNumber, pageSize) {
        if (pageSize == null || pageSize == undefined) {
            pageSize = 10;
        }
        SpecialCampaignFactory.getDataMainParts(pageNumber, pageSize, $scope.MainSpecialCampaign_CampaignDiscountTypeId)
            .then(
                function (res) {
                    console.log("MainSpecialCampaignJasa_UIGrid", res.data.Result);
                    $scope.MainSpecialCampaignParts_UIGrid.data = res.data.Result;
                    $scope.MainSpecialCampaignParts_UIGrid.totalItems = res.data.Total;
                    // $scope.MainSpecialCampaignParts_UIGrid.paginationPageSize = uiGridPageSize;
                    // $scope.MainSpecialCampaignParts_UIGrid.paginationPageSizes = [uiGridPageSize];

                }

            );
    }

    ///SEARCH PARTS
    $scope.ShowModalSearchParts = function () {
        SpecialCampaignFactory.getDataPartsSearch(1, 10000, $scope.TambahSpecialCampaignItems_NamaParts, $scope.TambahSpecialCampaignItems_NoParts)
            .then(
                function (res) {

                    $scope.SearchSpecialCampaignPartsGrid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );

        angular.element('#ModalSpecialCampaignSearchParts').modal('show');
    }

    $scope.ModalSearchProspect_Batal = function () {
        angular.element('#ModalSpecialCampaignSearchParts').modal('hide');
    }
    $scope.SearchPartsPilihRow = function (row) {
        //RegisterCheque_Data.Name
        console.log("ProspectCustomer Selected", row.entity)

        $scope.TambahSpecialCampaignItems_NamaParts = row.entity.PartsName;
        $scope.TambahSpecialCampaignItems_DeskripsiParts = row.entity.PartsName;
        $scope.TambahSpecialCampaignItems_NoParts = row.entity.PartsCode;
        $scope.TambahSpecialCampaignItems_PartsId = row.entity.PartsId;

        angular.element('#ModalSpecialCampaignSearchParts').modal('hide');
    }
    $scope.SearchSpecialCampaignPartsGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10, 20],
        columnDefs: [
            { name: 'PartsId', field: 'PartsId', visible: false },
            { name: 'PartsName', displayName: 'Nama Part', field: 'PartsName', enableFiltering: true },
            { name: 'PartsCode', displayName: 'Kode', field: 'PartsCode', enableFiltering: true },
            {
                name: 'Action',
                enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchPartsPilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        }
    };
    // =======Adding Custom Filter==============
    $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    $scope.gridApiAppointment = {};
    $scope.filterColIdx = 1;
    var x = -1;
    for (var i = 0; i < $scope.MainSpecialCampaign_UIGrid.columnDefs.length; i++) {
        if ($scope.MainSpecialCampaign_UIGrid.columnDefs[i].visible == undefined && $scope.MainSpecialCampaign_UIGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.MainSpecialCampaign_UIGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
        }
        console.log("$scope.gridCols", $scope.gridCols);
        console.log("$scope.grid.columnDefs[i]", $scope.MainSpecialCampaign_UIGrid.columnDefs[i]);
    }
    $scope.filterBtnLabel = $scope.gridCols[0].name;
    $scope.filterBtnChange = function (col) {
        console.log("col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };

    // ================= cusotm filter parts =============================
    $scope.gridColsParts = []; //= angular.copy(scope.grid.columnDefs);
    $scope.filterColIdxParts = 2;
    var xParts = -1;
    for (var i = 0; i < $scope.SearchSpecialCampaignPartsGrid.columnDefs.length; i++) {
        if ($scope.SearchSpecialCampaignPartsGrid.columnDefs[i].visible == undefined && $scope.SearchSpecialCampaignPartsGrid.columnDefs[i].name !== 'Action') {
            xParts++;
            $scope.gridColsParts.push($scope.SearchSpecialCampaignPartsGrid.columnDefs[i]);
            $scope.gridColsParts[xParts].idx = i;
        }
        console.log("$scope.gridCols", $scope.gridColsParts);
        console.log("$scope.grid.columnDefs[i]", $scope.SearchSpecialCampaignPartsGrid.columnDefs[i]);
    }
    $scope.filterBtnLabelParts = $scope.gridColsParts[0].name;
    $scope.filterBtnChangeParts = function (col) {
        console.log("col", col);
        $scope.filterBtnLabelParts = col.name;
        $scope.filterColIdxParts = col.idx + 1;
    };

    $scope.test = function (data, a) {
        console.log(data, arguments)
        console.log($scope.grid2Api)
    }
    // ================= cusotm filter parts =============================


    //==========add 2018-07-10
    $scope.MainSpecialCampaign_Download_Clicked = function () {
        var excelData = [];
        var fileName = "Special_Campaign_Template";

        var excelSheetFormat = {
            "CampaignDiscountName": "varchar(50)", "KMStart": "Numerik", "KMEnd": "Numerik", "AssemblyYearStart": "Numerik(YYYY)", "AssemblyYearEnd": "Numerik(YYYY)", "StartPeriod": "Date(YYYY/mm/DD)", "EndPeriod": "Date(YYYY/mm/DD)",
            "KatashikiCode": "varchar(18)", "TaskCode": "varchar(16) (wajib untuk GR saja)", "TaskName": "varchar(50)", "Discount": "Numerik(Tanpa \%\ /%/)", "PartsNo": "varchar(32)", "DiscountPerMaterial": "Numerik(Tanpa \%\ /%/)"
        };
        var excelSheetExample = {
            "CampaignDiscountName": "Example Special Campaign", "KMStart": "1000", "KMEnd": "2000", "AssemblyYearStart": "2017", "AssemblyYearEnd": "2018", "StartPeriod": "1900/01/01", "EndPeriod": "1999/01/01",
            "KatashikiCode": "AGH30R-NFXLK", "TaskCode": "TGD21H", "TaskName": "FRONT POWER SEAT MOTOR  1 PC", "Discount": "15", "PartsNo": "04002-52112", "DiscountPerMaterial": "10"
        };
        var excelSheetSeparator = {
            "CampaignDiscountName": "-", "KMStart": "-", "KMEnd": "-", "AssemblyYearStart": "ISI", "AssemblyYearEnd": "SETELAH", "StartPeriod": "BARIS", "EndPeriod": "INI",
            "KatashikiCode": "-", "TaskCode": "-", "TaskName": "-", "Discount": "-", "PartsNo": "-", "DiscountPerMaterial": "-"
        };
        var excelData = [];
        excelData.push(excelSheetFormat);
        excelData.push(excelSheetExample);
        excelData.push(excelSheetSeparator);
        //excelData.push(excelSheet3);
        console.log("excel data", excelData);


        // var excelSheetFormatx1 = {
        //     "ModelName": "Avanza",
        //     "Katashiki": "XXX-XXX",

        // };
        // var excelSheetFormatx2 = {
        //     "ModelName": "Agya",
        //     "Katashiki": "XXX-XXX",
        // };
        // var excelDataSheet2 = [];
        // excelDataSheet2.push(excelSheetFormatx1);
        // excelDataSheet2.push(excelSheetFormatx2);



        var finalData = []
        finalData.push(excelData)
        finalData.push($scope.listExcel_model_katashiki)


        //var excelSheetName = ["0", "1", "2"];

        // XLSXInterface.writeToXLSX(excelData, fileName);
        XLSXInterface.writeToXLSX(finalData, fileName, [fileName, "Model"]);


    }

    // FUNCTION WHERE, BUTTON UPLOAD ON HOME SCREEN CLICKED, THEN SHOW A MODAL UPLOAD
    $scope.MainSpecialCampaign_Upload_Clicked = function () {
        angular.element(document.querySelector('#UploadSpecialCampaignDiscount')).val(null);
        angular.element('#ModalUploadSpecialCampaignDiscount').modal('show');
    }

    // FUCNTION FOR LOAD DATA IN EXCEL TO JSON FORMAT DATA
    $scope.loadXLSSpecialCampaignDiscount = function (ExcelFile) {
        var myEl = angular.element(document.querySelector('#UploadSpecialCampaignDiscount')); //ambil elemen dari dokumen yang di-upload

        $timeout(function () {

            XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
                console.log('ema', json.length)
                if (json.length == undefined){
                    $scope.ExcelData = json.Special_Campaign_Template
                } else {
                    $scope.ExcelData = json;
                }
                // $scope.ExcelData = json;
                console.log("ExcelData : ", $scope.ExcelData);
                console.log("json", json);
            });
        }, 500);
    }

    // FUNCTION AFTER BUTTON UPLOAD CLICKED
    $scope.UploadSpecialCampaignDiscount = function () {
        var eachData = {};
        var inputData = [];
        var counter = 0;

        angular.forEach($scope.ExcelData, function (value, key) {
            if (counter > 2) {
                console.log('excel Data', $scope.ExcelData);
                console.log('value', value);
                console.log('key', key);

                // cek mandatory
                if ((value.CampaignDiscountName !== null && value.CampaignDiscountName !== undefined && value.CampaignDiscountName !== "") &&
                    (value.KMStart !== null && value.KMStart !== undefined && value.KMStart !== "") &&
                    (value.KMEnd !== null && value.KMEnd !== undefined && value.KMEnd !== "") &&
                    (value.AssemblyYearStart !== null && value.AssemblyYearStart !== undefined && value.AssemblyYearStart !== "") &&
                    (value.AssemblyYearEnd !== null && value.AssemblyYearEnd !== undefined && value.AssemblyYearEnd !== "") &&
                    (value.StartPeriod !== null && value.StartPeriod !== undefined && value.StartPeriod !== "") &&
                    (value.EndPeriod !== null && value.EndPeriod !== undefined && value.EndPeriod !== "") &&
                    (value.KatashikiCode !== null && value.KatashikiCode !== undefined && value.KatashikiCode !== "")) {

                        eachData = {
                            "AssemblyYearStart": value.AssemblyYearStart,
                            "AssemblyYearEnd": value.AssemblyYearEnd,
                            "CampaignDiscountId": 0, // valuenya di set 0 karena ini create data baru
                            "CampaignName": value.CampaignDiscountName,
                            "DateStart": value.StartPeriod,
                            "DateEnd": value.EndPeriod,
                            "ModelListModel": [{
                                "CampaignDiscountTypeId": -1,
                                "VehicleModelName": value.Model,
                                "VehicleTypeId": "",//saat ini HardCode id, value ini bisa di get By VehicleModelName sesuai data dari DB.
                                "VehicleDesc": value.FullModel, //DATA INI JUGA BISA DDI DAPAT KETIKA DATA VEHICLE SUDAH ADA
                                "KatashikiCode": value.KatashikiCode, //DATA INI JUGA BISA DDI DAPAT KETIKA DATA VEHICLE SUDAH ADA
                                "Description": "",
                                "TotalParts": 1,
                                "TotalTaskLists": 1,
                                "VehicleTypeId": -1
                            }],
                            "TaskListModel": [{
                                // "TotalData"                         : 1,
                                "KatashikiCode": value.KatashikiCode,
                                "CmpaignDiscountTypeId": -1,
                                "CmpaignDiscountTypeTaskListId": -1,//saat ini HardCode id, value ini bisa di get By VehicleModelName sesuai data dari DB.
                                "TaskListName": "", //DATA INI JUGA BISA DDI DAPAT KETIKA DATA VEHICLE SUDAH ADA
                                "TaskCode": value.TaskCode, //DATA INI JUGA BISA DDI DAPAT KETIKA DATA VEHICLE SUDAH ADA
                                "TaskDescription": value.TaskName,
                                "TaskDiscount": value.Discount,
                                "TaskId": -1,
                                "VehicleTypeId": -1
                            }],
                            "PartsListModel": [{
                                "KatashikiCode": value.KatashikiCode,
                                "CampaignDiscountTypeId": -1,
                                "CampaignDiscountTypePartsId": -1,
                                "PartsDesc": "",
                                "PartsDiscount": value.DiscountPerMaterial,
                                "PartsId": -1, // DATA INI HARUSNYA GET ID BY PARTSNAME,SEHINGGA DAPAT GET DATA Part
                                "PartsNo": value.PartsNo,
                                "PartsName": "",
                                "VehicleTypeId": -1
                            }],
                            "RangeKMStart": value.KMStart,
                            "RangeKMEnd": value.KMEnd,
                            "TasListModel": [
        
                            ]
                        }
                        inputData.push(eachData);
                    }
                
            }
            counter = counter + 1;
        });
        console.log('cek data upload', inputData);

        // mari kita grouping base on nama kempeng, model mobil, range km, tahun rakit, periode campaign, nama pekerjaan, nah nanti part terkahir
        var hasilGrouping = [];
        for (var i=0; i<inputData.length; i++){
            if (hasilGrouping.length == 0){
                hasilGrouping.push(inputData[i])
            } else {
                // for (var j=0; j<hasilGrouping.length; j++){
                    var idxCampName = _.findIndex(hasilGrouping, { 'CampaignName': inputData[i].CampaignName });

                    if (idxCampName != -1){
                    // if (hasilGrouping[j].CampaignName == inputData[i].CampaignName){

                        // kl nama kempeng sama cek ke variasi mobil nya
                        if (hasilGrouping[idxCampName].AssemblyYearStart == inputData[i].AssemblyYearStart &&
                            hasilGrouping[idxCampName].AssemblyYearEnd == inputData[i].AssemblyYearEnd &&
                            hasilGrouping[idxCampName].DateStart == inputData[i].DateStart &&
                            hasilGrouping[idxCampName].DateEnd == inputData[i].DateEnd &&
                            hasilGrouping[idxCampName].RangeKMStart == inputData[i].RangeKMStart &&
                            hasilGrouping[idxCampName].RangeKMEnd == inputData[i].RangeKMEnd ){

                                // kl variasi mobil nya sama semua berarti 1 kempeng, cek ke model mobil na
                                var idxModel = _.findIndex(hasilGrouping[idxCampName].ModelListModel, { 'KatashikiCode': inputData[i].ModelListModel[0].KatashikiCode });
                                if (idxModel != -1){
                                    // var idxTaskbyKatasiki = _.findIndex(hasilGrouping[idxCampName].TaskListModel, { 'KatashikiCode': inputData[i].ModelListModel[0].KatashikiCode });
                                    var idxTask = _.findIndex(hasilGrouping[idxCampName].TaskListModel, { 'TaskCode': inputData[i].TaskListModel[0].TaskCode });


                                    if (idxTask != -1){
                                        hasilGrouping[idxCampName].PartsListModel.push(inputData[i].PartsListModel[0])
                                    } else {
                                        hasilGrouping[idxCampName].TaskListModel.push(inputData[i].TaskListModel[0])
                                        hasilGrouping[idxCampName].PartsListModel.push(inputData[i].PartsListModel[0])
                                    }
                                    
                                } else {
                                    hasilGrouping[idxCampName].ModelListModel.push(inputData[i].ModelListModel[0])
                                    hasilGrouping[idxCampName].TaskListModel.push(inputData[i].TaskListModel[0])
                                    hasilGrouping[idxCampName].PartsListModel.push(inputData[i].PartsListModel[0])
                                }
                            } else {
                                hasilGrouping.push(inputData[i])
                            }
                    } else {
                        hasilGrouping.push(inputData[i])
                    }
                // }
            }
        }

        console.log('grouping coy', hasilGrouping)



        //SAVE UPLOAD DATA FROM EXCEL TO BACKEND
        SpecialCampaignFactory.createSpecialCampaignUploadDataOnly(hasilGrouping).then(function (res) {

            //AFTER DATA SAVED, THEN HIDE MODAL UPLOAD
            angular.element('#ModalUploadSpecialCampaignDiscount').modal('hide');
            angular.element(document.querySelector('#UploadSpecialCampaignDiscount')).val(null);

            //POP UP SUCCES
            bsNotify.show({
                size: 'small',
                type: 'success',
                title: "Success",
                content: "Data Berhasil Di Upload"
            });

            //REFRESH DATA GRID
            $scope.MainSpecialCampaign_UIGrid_Paging(1);
            $scope.TambahSpecialCampaign_Kembali_Clicked();
        })
    }

    $scope.EditSpecialCampaignItemsJasa = function (row) {
        angular.element('#ModalEditSpecialCampaignItemsJasa').modal('show');

        $scope.ModalEditSpecialCampaignItemsJasa_TaskListId = row.entity.CampaignDiscountTypeTaskListId;
        $scope.ModalEditSpecialCampaignItemsJasa_NamaJasa = row.entity.TaskListName;
        $scope.ModalEditSpecialCampaignItemsJasa_DeskripsiJasa = row.entity.TaskListName;
        $scope.ModalEditSpecialCampaignItemsJasa_DiscountJasa = row.entity.TaskDiscount;

        console.log('CampaignDiscountTypeTaskListId', row.entity.CampaignDiscountTypeTaskListId);

        //------------------------------

        $scope.TambahSpecialCampaignItems_DiscountTypeId = row.entity.CampaignDiscountTypeId;
        $scope.TambahSpecialCampaign_TypeTaskListId = row.entity.CampaignDiscountTypeTaskListId;
        $scope.TambahSpecialCampaignItems_VehicleTypeId = row.entity.VehicleTypeId;
        $scope.TambahSpecialCampaignItems_TaskName = row.entity.TaskListName;
        $scope.TambahSpecialCampaignItems_TaskId = row.entity.TaskId;
        $scope.TambahSpecialCampaignItems_DeskripsiJasa = row.entity.TaskDescription;
        $scope.TambahSpecialCampaignItems_DiscountJasa = row.entity.TaskDiscount;

    }

    $scope.ModalEditSpecialCampaignItemsJasa_Batal = function () {
        angular.element('#ModalEditSpecialCampaignItemsJasa').modal('hide');
    }

    $scope.ModalEditSpecialCampaignItemsJasa_Simpan = function () {

        console.log($scope.TambahSpecialCampaignJasa_DataModel);
        var TaskId = $scope.TambahSpecialCampaignItems_TaskId;
        for (var i = 0; i < $scope.TambahSpecialCampaignJasa_DataModelView.length; i++) {
            if ($scope.TambahSpecialCampaignJasa_DataModelView[i].TaskId == TaskId) {
                $scope.TambahSpecialCampaignJasa_DataModelView[i].TaskDiscount = $scope.ModalEditSpecialCampaignItemsJasa_DiscountJasa
                if ($scope.TambahSpecialCampaignJasa_DataModelView[i].CampaignDiscountTypeTaskListId != undefined && $scope.TambahSpecialCampaignJasa_DataModelView[i].CampaignDiscountTypeTaskListId > 0) {
                    // masukin yg uda ada id nya aja buat cek. kl yg baru di tambah lgsg di edit uda ga usah di cek nanti uda ikut diskonnya pake yg baru
                    $scope.dataJasaYangDiedit.push($scope.TambahSpecialCampaignJasa_DataModelView[i])
                }
            }
        }

        //   var id = $scope.ModalEditSpecialCampaignItemsJasa_TaskListId;
        //   var discount = $scope.ModalEditSpecialCampaignItemsJasa_DiscountJasa;

        //   console.log('id', id);
        //   console.log('discount', discount);

        // //   SpecialCampaignFactory.putTasklistDiscount(id, discount).then(function(res){

        // //       bsNotify.show({
        // //           size: 'small',
        // //           type: 'success',
        // //           title: "Data berhasil diubah",
        // //       });

        // //       angular.element('#ModalEditSpecialCampaignItemsJasa').modal('hide');
        // //     //   $scope.GetAllDataParts($scope.TambahSpecialCampaign_CampaignDiscountId);
        // //     //   $scope.GetAllDataJasa($scope.TambahSpecialCampaign_CampaignDiscountId);
        // //       $scope.TambahSpecialCampaignJasa_UIGrid_Paging(1);

        // //   });

        //   $scope.TambahSpecialCampaignJasa_DataModelView.some(function(obj, i){
        //       console.log('obj jasa', obj);

        //       if (obj.CampaignDiscountTypeTaskListId == $scope.TambahSpecialCampaign_TypeTaskListId) {
        //           $scope.TambahSpecialCampaignJasa_DataModel.splice(i,1);
        //           $scope.TambahSpecialCampaignJasa_DataModelView.splice(i, 1);
        //           $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;

        //       }
        //   });

        //   var dataInput = {
        //       "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
        //       "CampaignDiscountTypeTaskListId": $scope.TambahSpecialCampaign_TypeTaskListId,
        //       "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
        //       "TaskListName": $scope.TambahSpecialCampaignItems_TaskName, //$scope.TambahSpecialCampaignItems_Openo.TaskName,
        //       "TaskId": $scope.TambahSpecialCampaignItems_TaskId, //$scope.TambahSpecialCampaignItems_Openo.TaskId,
        //       "TaskDescription": $scope.TambahSpecialCampaignItems_DeskripsiJasa,
        //       "TaskDiscount": discount
        //   }

        //   $scope.TambahSpecialCampaignJasa_DataModel.push(dataInput);
        //   $scope.TambahSpecialCampaignJasa_DataModelView.push(dataInput);
        $scope.TambahSpecialCampaignJasa_UIGrid.data = $scope.TambahSpecialCampaignJasa_DataModelView;

        bsNotify.show({
            size: 'small',
            type: 'success',
            title: "Data berhasil diubah",
        });

        angular.element('#ModalEditSpecialCampaignItemsJasa').modal('hide');

        $scope.TambahSpecialCampaignItems_TaskName = '';
        $scope.TambahSpecialCampaignItems_Openo = '';
        $scope.TambahSpecialCampaignItems_DeskripsiJasa = '';
        $scope.TambahSpecialCampaignItems_DiscountJasa = '';
    }

    $scope.EditSpecialCampaignItemsParts = function (row) {
        angular.element('#ModalEditSpecialCampaignItemsParts').modal('show');

        $scope.ModalEditSpecialCampaignItemsParts_PartsId = row.entity.CampaignDiscountTypePartsId;
        $scope.ModalEditSpecialCampaignItemsParts_NamaParts = row.entity.PartsName;
        $scope.ModalEditSpecialCampaignItemsParts_NoParts = row.entity.PartsNo;
        $scope.ModalEditSpecialCampaignItemsParts_DeskripsiParts = row.entity.PartsDesc;
        $scope.ModalEditSpecialCampaignItemsParts_DiscountParts = row.entity.PartsDiscount

        console.log('CampaignDiscountTypePartsId', row.entity.CampaignDiscountTypePartsId);

        //-----------------------------

        $scope.TambahSpecialCampaignItems_DiscountTypeId = row.entity.CampaignDiscountTypeId;
        $scope.CampaignDiscountTypePartId = row.entity.CampaignDiscountTypePartsId;
        $scope.TambahSpecialCampaignItems_VehicleTypeId = row.entity.VehicleTypeId;
        $scope.TambahSpecialCampaignItems_PartsId = row.entity.PartsId;
        $scope.TambahSpecialCampaignItems_NamaParts = row.entity.PartsName;
        $scope.TambahSpecialCampaignItems_NoParts = row.entity.PartsNo;
        $scope.TambahSpecialCampaignItems_DeskripsiParts = row.entity.PartsDesc;
        $scope.TambahSpecialCampaignItems_DiscountParts = row.entity.PartsDiscount;
    }

    $scope.ModalEditSpecialCampaignItemsParts_Batal = function () {
        angular.element('#ModalEditSpecialCampaignItemsParts').modal('hide');
    }

    $scope.ModalEditSpecialCampaignItemsParts_Simpan = function () {

        console.log($scope.TambahSpecialCampaignParts_DataModel);
        var noParts = $scope.ModalEditSpecialCampaignItemsParts_NoParts;
        for (var i = 0; i < $scope.TambahSpecialCampaignParts_DataModelView.length; i++) {
            if ($scope.TambahSpecialCampaignParts_DataModelView[i].PartsNo == noParts) {
                $scope.TambahSpecialCampaignParts_DataModelView[i].PartsDiscount = $scope.ModalEditSpecialCampaignItemsParts_DiscountParts
                if ($scope.TambahSpecialCampaignParts_DataModelView[i].CampaignDiscountTypePartsId != undefined && $scope.TambahSpecialCampaignParts_DataModelView[i].CampaignDiscountTypePartsId > 0) {
                    // masukin yg uda ada id nya aja buat cek. kl yg baru di tambah lgsg di edit uda ga usah di cek nanti uda ikut diskonnya pake yg baru
                    $scope.dataPartsYangDiedit.push($scope.TambahSpecialCampaignParts_DataModelView[i]);
                }
            }
        }


        // var id = $scope.ModalEditSpecialCampaignItemsParts_PartsId;
        // var discount = $scope.ModalEditSpecialCampaignItemsParts_DiscountParts;

        // console.log('id', id);
        // console.log('discount', discount);

        // // SpecialCampaignFactory.putPartsDiscount(id, discount).then(function (res) {

        // //     bsNotify.show({
        // //         size: 'small',
        // //         type: 'success',
        // //         title: "Data berhasil diubah",
        // //     });

        // //     angular.element('#ModalEditSpecialCampaignItemsParts').modal('hide');
        // //     // $scope.GetAllDataParts($scope.TambahSpecialCampaign_CampaignDiscountId);
        // //     // $scope.GetAllDataJasa($scope.TambahSpecialCampaign_CampaignDiscountId);
        // //     $scope.TambahSpecialCampaignParts_UIGrid_Paging(1);

        // // });

        // $scope.TambahSpecialCampaignParts_DataModelView.some(function (obj, i) {
        //     console.log('obj parts', obj);

        //     if (obj.CampaignDiscountTypePartsId == $scope.CampaignDiscountTypePartId) {
        //         $scope.TambahSpecialCampaignParts_DataModel.splice(i, 1);
        //         $scope.TambahSpecialCampaignParts_DataModelView.splice(i, 1);
        //         $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;

        //     }
        // });

        // var dataInput = {
        //     "CampaignDiscountTypeId": $scope.TambahSpecialCampaignItems_DiscountTypeId,
        //     "CampaignDiscountTypePartId": $scope.CampaignDiscountTypePartId,
        //     "VehicleTypeId": $scope.TambahSpecialCampaignItems_VehicleTypeId,
        //     "PartsId": $scope.TambahSpecialCampaignItems_PartsId,
        //     "PartsName": $scope.TambahSpecialCampaignItems_NamaParts,
        //     "PartsNo": $scope.TambahSpecialCampaignItems_NoParts,
        //     "PartsDesc": $scope.TambahSpecialCampaignItems_DeskripsiParts,
        //     "PartsDiscount": discount
        // }

        // $scope.TambahSpecialCampaignParts_DataModel.push(dataInput);
        // $scope.TambahSpecialCampaignParts_DataModelView.push(dataInput);
        $scope.TambahSpecialCampaignParts_UIGrid.data = $scope.TambahSpecialCampaignParts_DataModelView;

        bsNotify.show({
            size: 'small',
            type: 'success',
            title: "Data berhasil diubah",
        });

        angular.element('#ModalEditSpecialCampaignItemsParts').modal('hide');

        $scope.TambahSpecialCampaignItems_NamaParts = '';
        $scope.TambahSpecialCampaignItems_NoParts = '';
        $scope.TambahSpecialCampaignItems_DeskripsiParts = '';
        $scope.TambahSpecialCampaignItems_DiscountParts = '';
    }

});
