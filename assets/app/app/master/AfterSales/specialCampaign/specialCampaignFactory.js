angular.module('app')
    .factory('SpecialCampaignFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();

        return {
            getDataModel: function() {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataModel';
                console.log("check Model data : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataFullModel: function(vehicleModel) {
                // var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataFullModel/?VehicleModel=' + vehicleModel;
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataFullModel/?VehicleModel=' + vehicleModel + '&CampaignDiscountId=' + 0;
                console.log("check Vehicle Model data : ", url);
                var res = $http.get(url);
                return res;
            },

            // getDataServicebyKatashiki: function(katashikiCode, campaignDiscountTypeId) {
            //     // var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataTaskList/?KatashikiCode=' + katashikiCode +'&CampaignDiscountTypeId='+campaignDiscountTypeId;
            //     // console.log("check service data : ", url);
            //     // var res=$http.get(url);
            //     var res = $http.get('/api/as/AfterSalesMasterSpecialCampaign/GetDataTaskList', {
            //         params: {
            //             KatashikiCode: katashikiCode,
            //             CampaignDiscountTypeId: campaignDiscountTypeId
            //         }
            //     });
            //     return res;
            // },
            getDataServicebyKatashiki: function(katashikiCode, campaignDiscountTypeId, TaskName) {
                // var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataTaskList/?KatashikiCode=' + katashikiCode +'&CampaignDiscountTypeId='+campaignDiscountTypeId;
                // console.log("check service data : ", url);
                // var res=$http.get(url);
                
                var res = $http.get('/api/as/AfterSalesMasterSpecialCampaign/NewGetDataTaskList', {
                    params: {
                        TaskName : TaskName,
                        KatashikiCode: katashikiCode,
                        CampaignDiscountTypeId: campaignDiscountTypeId
                    }
                });
                return res
                // var res = $http.post('/api/as/AfterSalesMasterSpecialCampaign/NewGetDataTaskList?KatashikiCode='+katashikiCode+'&TaskName='+TaskName+'&OutletId='++'&CampaignDiscountTypeId='+campaignDiscountTypeId);
                // return res;
            },



            getDataPartsSearch: function(start, limit, partsName, partsCode) {
                if (partsName == null || partsName == undefined || partsName == ''){
                    partsName = '-'
                }
                if (partsCode == null || partsCode == undefined || partsCode == ''){
                    partsCode = '-'
                }
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetPartsDatabyName/?start=' + start + '&limit=' + limit + '&PartsName=' + partsName + '&PartsCode=' + partsCode + '&outletid=' + user.OrgId;
                console.log("check Vehicle Model data : ", url);
                var res = $http.get(url);
                return res;
            },

            createSpecialCampaign: function(inputData) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/SubmitUploadData/';
                var param = JSON.stringify(inputData);
                console.log("object input Special Campaign ", param);
                var res = $http.post(url, param);
                return res;
            },

            createSpecialCampaignUploadDataOnly: function(inputData) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/SubmitUploadDataOnly/';
                var param = JSON.stringify(inputData);
                console.log("object input Special Campaign ", param);
                var res = $http.post(url, param);
                return res;
            },

            createSpecialCampaignModel: function(inputData) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/SubmitModelData/';
                var param = JSON.stringify(inputData);
                console.log("object input Special Campaign Model ", param);
                var res = $http.post(url, param);
                return res;
            },

            getDataMain: function(start, limit) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataMain/?start=' + start + '&limit=' + limit + '&outletid=' + user.OrgId;
                console.log("check data main: ", url);
                var res = $http.get(url);
                return res;
            },

            getDataMainModel: function(start, limit, campaignDiscountId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataMainModel/?start=' + start + '&limit=' + limit + '&campaigndiscountid=' + campaignDiscountId + '&outletid=' + user.OrgId;
                console.log("check data main model: ", url);
                var res = $http.get(url);
                return res;
            },

            getDataMainJasa: function(start, limit, campaignDiscountTypeId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataMainJasa/?start=' + start + '&limit=' + limit + '&CampaignDiscountTypeId=' + campaignDiscountTypeId + '&outletid=' + user.OrgId;
                console.log("check data main Jasa: ", url);
                var res = $http.get(url);
                return res;
            },

            GetAllDataSelectedParts: function(CampaignDiscountId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetAllDataSelectedParts/?CampaignDiscountId=' + CampaignDiscountId + '&OutletId=' + user.OrgId;
                console.log("get all data selected parts : ", url);
                var res = $http.get(url);
                return res;
            },

            GetAllDataSelectedJasa: function(CampaignDiscountId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetAllDataSelectedJasa/?CampaignDiscountId=' + CampaignDiscountId + '&OutletId=' + user.OrgId;
                console.log("get all data selected Jasa : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataMainParts: function(start, limit, campaignDiscountTypeId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/GetDataMainParts/?start=' + start + '&limit=' + limit + '&CampaignDiscountTypeId=' + campaignDiscountTypeId + '&outletid=' + user.OrgId;
                console.log("check data main Parts: ", url);
                var res = $http.get(url);
                return res;
            },

            deleteDataParts: function(discountPartsId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/DeleteDataParts/?DiscountPartsId=' + discountPartsId;
                console.log("url Delete data parts : ", url);
                var res = $http.delete(url);
                return res;
            },

            deleteDataTasks: function(discountTasksId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/DeleteDataTasks/?DiscountTaskId=' + discountTasksId;
                console.log("url Delete data parts : ", url);
                var res = $http.delete(url);
                return res;
            },

            deleteDataModel: function(discountModelId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/DeleteDataModel/?DiscountModelId=' + discountModelId;
                console.log("url Delete data Model : ", url);
                var res = $http.delete(url);
                return res;
            },

            deleteDataMain: function(discountId) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/DeleteDataMain/?DiscountId=' + discountId;
                console.log("url Delete data Model : ", url);
                var res = $http.delete(url);
                return res;
            },

            putTasklistDiscount: function(id, discount) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/UpdateTaskListDiscount/?CampaignDiscountTypeTaskListId=' + id + '&TaskDicount=' + discount;
                console.log("url Update data Model : ", url);
                var res = $http.put(url);
                return res;
            },

            putPartsDiscount: function (id, discount) {
                var url = '/api/as/AfterSalesMasterSpecialCampaign/UpdatePartsDiscount/?CampaignDiscountTypePartsId=' + id + '&PartsDiscount=' + discount;
                console.log("url Update data Model : ", url);
                var res = $http.put(url);
                return res;
            },
            
            GetCVehicleModelandType: function(start, limit, campaignDiscountTypeId) {
                // api/crm/GetCVehicleModelandType/
                var url = '/api/crm/GetCVehicleModelandType/'
                console.log("check data main Parts: ", url);
                var res = $http.get(url);
                return res;
            },
        }
    });