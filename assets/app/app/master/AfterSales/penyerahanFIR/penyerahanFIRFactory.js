angular.module('app')
	.factory('PenyerahanFIRFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/as/AfterSalesFIRQuestions/?start=1&limit=100';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(questionFIRData){
			var url = '/api/as/AfterSalesFIRQuestions/create/';
			var ArrayquestionFIRData = [questionFIRData];
			var param = JSON.stringify(ArrayquestionFIRData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(questionFIRData){
			var url = '/api/as/AfterSalesFIRQuestions/Update/';
			var ArrayquestionFIRData = [questionFIRData];
			var param = JSON.stringify(ArrayquestionFIRData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/as/AfterSalesFIRQuestions/deletedata/?id=' + id;
			console.log("url delete Data : ", url);
			var res=$http.delete(url);
			// var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});