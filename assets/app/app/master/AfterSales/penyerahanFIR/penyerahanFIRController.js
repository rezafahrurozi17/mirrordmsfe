var app = angular.module('app');
app.controller('PenyerahanFIRController', function ($scope, $http, $filter, CurrentUser, PenyerahanFIRFactory, $timeout, bsNotify) {
	
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function() {
		//$scope.loading = true;
		$scope.gridData = [];
	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mPenyerahanFIR = null; //Model
	$scope.xRole = { selected: [] };

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOption = { format: dateFormat }


	$scope.cekObject = function() {
		console.log($scope.mPenyerahanFIR);
	}

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	$scope.getData = function() {
		PenyerahanFIRFactory.getData()
		.then(
			function(res){
				$scope.grid.data = res.data.Result;
			}
		);
	}

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}

	$scope.CheckOutlet = function(rows)
	{
		console.log("onSelectRows=>", rows.OutletId);		
		if(rows.FlagEdit == 1){
			PenyerahanFIRFactory.delete(rows.QuestionId).then(function(res){
				$scope.getData();
			})
		}else{
			bsNotify.show({
				title: "Pertanyaan FIR",
				content: "Data tidak boleh di hapus karena berasal dari TAM",
				type : "Danger"
			})
		}
		// if(rows[0].FlagEdit == 1)
		// 		PenyerahanFIRFactory.delete(rows[0].QuestionId)
		// 		.then(
		// 			function(res){
		// 				$scope.getData();
		// 			}
		// 		);
		// 	else
		// 		bsNotify.show({
        //           title: "Pertanyaan FIR",
        //           content: "Data tidak boleh di delete karena berasal dari TAM",
        //           type: 'danger'
        // });
	}

	$scope.selectRole = function(rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function(rows) {
			console.log("onSelectRows=>", rows);
		}
	//----------------------------------
	// Grid Setup
	//----------------------------------

	var actionTemp =    '<div>'+    
                            '<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" title="Lihat">'+
                                '<i class="fa fa-fw fa-lg fa-list-alt"></i>'+
                            '</button>'+
                            '<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-if=" row.entity.FlagEdit == 1 "'+
                                    'ng-click="grid.appScope.gridClickEditHandler(row.entity);" title="Edit">'+
                                    // '{{row.entity}}'+
                                '<i class="fa fa-fw fa-lg fa-pencil"></i>'+
							'</button>'+
							'<button class="ui icon inverted grey button"'+
                                    'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    'onclick="this.blur()"'+
                                    'ng-if=" row.entity.FlagEdit == 1 "'+
                                    'ng-click="grid.appScope.$parent.CheckOutlet(row.entity);" title="Hapus">'+
                                    // '{{row.entity}}'+
                                '<i class="fa fa-fw fa-lg fa-trash"></i>'+
                            '</button>'+
                        '</div>';

	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: false,
		enableSelectAll: true,

		columnDefs: [
			{ name: 'id', field: 'QuestionId', visible: false },
			{ name: 'Nama Penjelasan', field: 'Description' },
			{ name:'action', allowCellFocus: false, width:250, pinnedRight:true,
                enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                cellTemplate: actionTemp
            }
		]
	};
});