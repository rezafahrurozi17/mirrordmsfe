angular.module('app')
    .factory('MasterWACFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                // var res=$http.get('/api/as/MasterWAC');
                var res = $http.get('/api/as/GlobalMasterWAC');
                console.log("Hasil Factory : ", res);
                return res;
            },

            create: function(data) {
                console.log("Data create : ", data);
                data.Id = 0;
                data.ItemWAC = angular.copy(data.Item_WAC);
                return $http.post('/api/as/GlobalMasterWAC/CreateNewWAC', [data]);
                // return $http.post('/api/as/GlobalMasterWAC', [data]);
            },
            update: function(data) {
                console.log("Data update : ", data);
                // return $http.put('/api/as/GlobalMasterWAC', [data]); // sekarang update sama di delete disatuin pake post
                return $http.post('/api/as/GlobalMasterWAC', [data]);
            },
            delete: function(id) {
                console.log("Data DP : ", id);
                return $http.delete('/api/as/GlobalMasterWAC/DeleteGlobalMasterGroupWAC?WACId=' + id);
            },
            getDetailMasterWAC: function(wacId) {
                var res = $http.get('/api/as/GlobalMasterWAC/GetGlobalMasterWACItem', { params: { wacId: wacId } });
                console.log("Hasil getDetailMasterWAC : ", res);
                return res;
            }
        }
    });
//