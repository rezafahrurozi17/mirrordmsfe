angular.module('app')
    .controller('MasterWACController', function($scope, $http, CurrentUser, MasterWACFactory, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMasterWAC = null; //Model
        $scope.cMasterWAC = null; //Collection
        $scope.xMasterWAC = {};
        $scope.xMasterWAC.selected = [];

        $scope.saveObj = {};

        var dateFormat = 'yyyy/MM/dd';

        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        var getArrItemId = [];
        var getArrDetailId = [];

        //----------------------------------
        // Get Data
        //----------------------------------
        var tempObj = {};
        $scope.getData = function() {
            MasterWACFactory.getData().then(
                function(res) {
                    var tempdata = [];
                    for (var i in res.data.Result) {
                        if (res.data.Result[i].StatusCode != 0 & res.data.Result[i].Id != 0) {
                            tempdata.push(res.data.Result[i]);
                        }

                    }
                    $scope.grid.data = tempdata;

                    console.log("data=>", res.data);
                    //$scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.beforeNew = function() {
            console.log("before new");
            $scope.gridDetail.data = [];
            $scope.addNewRow();
        }

        $scope.beforeEdit = function(mdl) {

        }

        $scope.onShowDetail = function(mdl, mode) {
            console.log("mode showDetail : ", mode);
            $scope.gridDetail.data = [];
            if (mode == 'new') {
                $scope.mMasterWAC = {};
            } else {
                getDataMasterWACDetail(mdl);
            }
            if (mode == 'view') {
                $scope.isDisabled = true;

            } else {
                $scope.isDisabled = false;
            }
            formMode = mode;
        }

        var getDataMasterWACDetail = function(row) {
            // console.log(row);
            MasterWACFactory.getDetailMasterWAC(row.Id).then(function(res) {
                // console.log(res.data.Result);
                getArrItemId = [];
                getArrDetailId = [];
                if (res.data.Result.length > 0) {
                    for (var i in res.data.Result) {
                        getArrItemId.push(res.data.Result[i].ItemId);
                        getArrDetailId.push(res.data.Result[i]);
                        getArrDetailId[i].strCompare = res.data.Result[i].ItemName.toLowerCase().replace(/ /g, '');
                    }
                    $scope.gridDetail.data = res.data.Result;
                }
                $scope.addNewRow();
            });
        }

        $scope.addNewRow = function() {
            $scope.gridDetail.data.push({
                ItemId: null,
                GroupId: null,
                ItemName: null
            });
        }

        $scope.removeRow = function(selected) {
            var detData = [];
            console.log(selected);

            if ($scope.gridDetail.data.length > 1) {
                // _.pullAllWith($scope.gridDetail.data, [selected], _.isEqual);
                var idx = $scope.gridDetail.data.indexOf(selected);
                if (idx > -1) {
                    $scope.gridDetail.data.splice(idx, 1);
                }
            } else {
                var temp = _.keys(selected);
                for (var i in temp) {
                    selected[temp[i]] = null;
                }
            }
        }

        $scope.checkRow = function() {
            var findObj = _.find($scope.gridDetail.data, {
                ItemId: null,
                GroupId: null,
                ItemName: null
            });
            if (findObj == undefined) {
                $scope.addNewRow();
            }
            // else {
            //     var temp = _.keys($scope.gridDetail.data[0]);
            //     var x = 0;
            //     for (var i in temp) {
            //         if (temp[i] != null) x++;
            //     }
            //     if (x==0)
            // }
        }

        $scope.doCustomSave = function(mdl, mode) {
            console.log(mdl);
            console.log(mode);
            mdl.Item_WAC = [];
            mdl.Item_WAC_Delete = [];

            var idx = _.findIndex($scope.gridDetail.data, {
                ItemId: null,
                GroupId: null,
                ItemName: null
            });
            if (idx > -1) {
                $scope.gridDetail.data.splice(idx, 1);
            }

            if (mode == 'create') {
                if ($scope.gridDetail.data.length > 0) {
                    for (var i in $scope.gridDetail.data) {
                        mdl.Item_WAC.push({
                            "ItemId": 0,
                            "ItemName": $scope.gridDetail.data[i].ItemName,
                            "GroupId": 0
                        });
                    }
                }

                var saveObj = angular.copy(mdl);
                console.log("saveObj : ", saveObj);
                MasterWACFactory.create(mdl).then(function(res) {
                    mdl = {};
                    $scope.getData();
                });
            } else {
                var listItemId = [];
                console.log('$scope.gridDetail.data', $scope.gridDetail.data);
                var tempNewData = angular.copy($scope.gridDetail.data);
                for (var i in $scope.gridDetail.data) {
                    listItemId.push($scope.gridDetail.data[i].ItemId);
                }
                // console.log(getArrItemId);
                // console.log(listItemId);

                var del = _.difference(getArrItemId, listItemId);
                console.log('deldeldeldel', del);

                if ($scope.gridDetail.data.length > 0) {
                    for (var i in $scope.gridDetail.data) {
                        if ($scope.gridDetail.data[i].ItemId == null) {
                            var found = _.findIndex(getArrDetailId, { "strCompare": $scope.gridDetail.data[i].ItemName.toLowerCase().replace(/ /g, '') });
                            if (found < 0) {
                                mdl.Item_WAC.push({
                                    "ItemId": 0,
                                    "ItemName": $scope.gridDetail.data[i].ItemName,
                                    "GroupId": 0
                                });
                            }
                        }
                    }
                };

                mdl.ItemWAC = [];
                _.map(tempNewData, function(a) {
                    if (a.ItemId != null) {
                        mdl.ItemWAC.push(a);
                    }
                });

                if (del.length > 0) {
                    for (var i in del) {
                        var found = _.findIndex(getArrDetailId, { "ItemId": del[i] });
                        // console.log(found);
                        if (found > -1) {
                            mdl.Item_WAC_Delete.push({
                                "ItemId": del[i]
                            });
                        }
                    }
                }

                var saveObj = angular.copy(mdl);
                console.log("saveObj : ", saveObj);
                MasterWACFactory.update(mdl).then(function(res) {
                    mdl = {};
                    $scope.getData();
                });
            }
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'MasterWACId', field: 'MasterWACId', visible: false },
                { name: 'Nama', field: 'GroupName' }
            ]
        };

        // var numbCellTemplate =      '<div class="ui-grid-cell-contents">'+
        //                             '   <input type="number" class="form-control" name="OrderTypeId" max="3" min="1" style="height:50">'+
        //                             '</div>';
        // var dateCellTemplate =      '<div class="ui-grid-cell-contents">'+
        //                             '   <div uib-timepicker ng-model="day.timeFrom" show-spinners="false" show-meridian="false"></div>'+
        //                             '</div>';

        $scope.gridDetailApi = {};
        $scope.gridDetail = {
            // rowHeight: 50,
            enableSorting: true,
            enableSelectAll: true,
            enableCellEdit: false,
            enableCellEditOnFocus: true, // set any editable column to allow edit on focus
            columnDefs: [
                { name: 'ItemId', field: 'ItemId', visible: false },
                // { name:'Nama Item', field: 'ItemName'},
                {
                    name: 'Nama Item',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '   <input type="text" class="form-control" name="ItemName" ng-change="grid.appScope.checkRow()" ng-disabled="grid.appScope.isDisabled" ng-model="row.entity.ItemName" style="height:50">' +
                        '</div>'
                },

                {
                    name: 'Action',
                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '<button class="ui icon inverted grey button"' +
                        'style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                        'onclick="this.blur()" ng-disabled="grid.appScope.isDisabled" ' +
                        'ng-click="grid.appScope.removeRow(row.entity)">' +
                        '<i class="fa fa-fw fa-lg fa-times"></i>' +
                        '</button>' +
                        '</div>',
                    width: "10%",
                    enableCellEdit: false
                }
            ]
        };

        $scope.formApi = {};

        // $scope.gridDetail.onRegisterApi = function(gridApi) {
        //     // set gridApi on scope
        //     $scope.gridDetailApi = gridApi;

        //     $scope.gridDetailApi.selection.on.rowSelectionChanged($scope,function(row) {
        //         //scope.selectedRows=null;
        //         $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
        //         // console.log("bsform selected=>",scope.selectedRows);
        //     });
        //     $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
        //         //scope.selectedRows=null;
        //         $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
        //     });

        //     $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
        //         // console.log('Inside afterCellEdit grid detail..');
        //         // if (newValue<0 || newValue==null) {
        //         //     newValue = 0;
        //         //     rowEntity[colDef.field] = newValue;
        //         // }
        //     });
        // };
    });