var app = angular.module('app');
app.controller('CustomerDiscountController', function (WOBP, $q, $scope, $http, bsAlert, $filter, CurrentUser, CustomerDiscountFactory, uiGridConstants,ngDialog, $timeout) {
	var user = CurrentUser.user();
	var uiGridPageSize = 15;
	$scope.modeCrud = '';

	$scope.dateOptions = {
		startingDay: 1,
		format: 'dd-MM-yyyy'
	};
	// =========== STANDARIZATION ==============

	// TRIGGER WHEN INIT CURRENT MENU
	$scope.$on("$viewContentLoaded",function(){
		// $scope.getDataCombobox();
	})

	//  INITIALIZE VARIABLE AND OTHERS
	$scope.mData = {};
	$scope.formApi = {};
	$scope.optionsTambahCustomerDiscount_GRBP = [{name:"GR", value:"GR"}, {name:"BP", value:"BP"}];
	$scope.lmModelAddress = {};
	$scope.TambahCustomerDiscount_GroupCustomerId = 0;
	$scope.TambahCustomerDiscount_CustomerList = [];
	$scope.TambahCustomerDiscount_ColumnDefs = [];
	$scope.loading = true;
	$scope.actionButtonCaption = {new:'Tambah Group Customer'};
	$scope.buttonSettingUpload = {save:{
		text:'Upload',
		show:false
	}}
	$scope.modeModalCustomerUpload = 'new';
	$scope.showTambahCustomer = false;
	$scope.showUploadCUstomer = false;
	$scope.dataCustomer = {}
	$scope.selectedRows = [];
	$scope.dataUpload = {};
	$scope.modeModalCustomer = 'new';


	// TRIGGER WHEN CURRENT MENU IS READY, IF NOT USING ADVANCE SEARCH
	$scope.getData = function(){
		$scope.MainCustomerDiscountList_UIGrid_Paging(1);
	}

	// TRIGGER WHEN BEFORE GO TO DETAIL FORM
	$scope.onShowDetail = function(row, mode){
		console.log(row);
		$scope.TambahCustomerGrid.data = [];
		if(mode !== 'new'){
			CustomerDiscountFactory.getDataGroupCustomerList(row.GroupCustomerId).then(function(res){
				$scope.TambahCustomerGrid.data = res.data.Result;
				var idx = 0;
				for (var i in $scope.TambahCustomerGrid.data){
					$scope.TambahCustomerGrid.data[i].idx = idx;
					idx++
				}
			});
		}

	}

	// TRIGGER WHEN CLICK ADD BUTTON ON GRID FORM
	$scope.onBeforeNewMode = function(){

	}

	// TRIGGER WHEN CLICK SAVE BUTTON ON FORM AND WILL RETURN 'TRUE' OR 'FALSE'
	$scope.onValidateSave = function(data){
		console.log('onvalidate save ==>', data);
		var inputData = [{
			"GroupCustomerId": data.GroupCustomerId,
			"GroupCustomerName": data.GroupCustomerName,
			"Category": data.Category,
			"DiscountJasa": data.DiscountJasa,
			"DiscountParts": data.DiscountParts,
			"DiscountOPL":data.DiscountOPL,
			"CustomerList"  :$scope.TambahCustomerGrid.data
		}];
		console.log('onvalidate save ==>', inputData);
		return inputData
	}
	// 
	$scope.doCustomSave = function(row, mode){
		row.CustomerList  = $scope.TambahCustomerGrid.data;
		console.log('roww',row);
		if (mode == 'create') {
			CustomerDiscountFactory.create(row).then(
			function(res){
				console.log("res", res);
				if(res.data.ResponseCode == 13){
					bsAlert.success("Data berhasil disimpan");
					$scope.MainCustomerDiscountList_UIGrid_Paging(1);
				}
				else if(res.data.ResponseCode == 98)
				{
					bsAlert.warning(res.data.ResponseMessage)
				}
				else{
					bsAlert.warning(res.data.ResponseMessage);
				}
			});
		} else {
			CustomerDiscountFactory.create(row).then(
				function(res){
				console.log("res", res);
				if(res.data.ResponseCode == 13){
					bsAlert.success("Data berhasil disimpan");
					$scope.MainCustomerDiscountList_UIGrid_Paging(1);
				}
				else if(res.data.ResponseCode == 98)
				{
					bsAlert.warning(res.data.ResponseMessage)
				}
				else{
					bsAlert.warning(res.data.ResponseMessage);
				}
			});
		}
	}

	// TRIGGER WHEN CLICK CHECK BUTTON ON GRID
	$scope.onSelectRows = function(selected){
		console.log('selected Rows ==> ', selected);
	}

	// GRID OPTION
	$scope.grid = {
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination : true,
		multiSelect : false,
		enableFiltering: true,
		enableColumnResizing: true,
		columnDefs: [
			{name:"GroupCustomerId",field:"GroupCustomerId",visible:false},
			{name:"Group Customer Name",field:"GroupCustomerName"},
			{name:"Diskon Jasa",field:"DiscountJasa"},
			{name:"Diskon Parts",field:"DiscountParts"},
			{name:"Diskon OPL",displayName:"Diskon OPL",field:"DiscountOPL"},
			{name:"Kategori Diskon",field:"Category"},
		],
		// onRegisterApi: function(gridApi) {
		// 	$scope.GridApiMainCustomerDiscountList = gridApi;
		// 	gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
		// 		$scope.MainCustomerDiscountList_UIGrid_Paging(pageNumber);
		// 	});
		// }
	};
	// TEMPLATE CUSTOM ACTION COLUMN IN GRID, IF USING 'grid-custom-action-button-template'
	$scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
	<a ng-click="grid.appScope.actView(row.entity)" class="fa fa-list-alt fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Lihat"/a> \
	<a ng-click="grid.appScope.actEdit(row.entity)" class="fa fa-pencil fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Ubah" ng-if="row.entity.TamBit == 0" /a> \
	<a ng-click="grid.appScope.$parent.actDeleteTask(row.entity)" class="fa fa-fw fa-lg fa-trash" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Hapus" ng-if="row.entity.TamBit == 0" /a> \
	</div>';

	// Declaration Grid API
	$timeout(function(){
		console.log('$scope.formGridApi ===>',$scope.formGridApi);
		$scope.formGridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			$scope.MainCustomerDiscountList_UIGrid_Paging(pageNumber);
		});
	},1000)

	// CUSTOM FUNCTION
	$scope.MainCustomerDiscountList_UIGrid_Paging = function(pageNumber){
		CustomerDiscountFactory.getDataMain(pageNumber, uiGridPageSize)
		.then(
			function(res)
			{
				console.log("Get data main", res.data.Result);

				$scope.grid.data = res.data.Result;
				$scope.grid.totalItems = res.data.Total;
				$scope.grid.paginationPageSize = uiGridPageSize;
				$scope.grid.paginationPageSizes = [uiGridPageSize];
				$scope.loading = false;
				$scope.getAddressList();

			}
	  );
	}

	$scope.getAddressList = function() {
		return $q.resolve(
				WOBP.getMLocationProvince().then(function(res) {
				console.log("List Province====>", res.data.Result);
				$scope.provinsiData = res.data.Result;
			})
		);
	};
	$scope.cancelTambahCustomer = function() {
		$scope.dataCustomer = {};
		$scope.showTambahCustomer = false;
	};
	$scope.saveCustomer = function(data, mode) {
		console.log("saveCustomer mode", mode);
		console.log("saveCustomer data", data);
		var BirthDate = '';
		var ValidTo = '';
		// if(angular.isUndefined(data.TanggalLahir) || data.TanggalLahir == null)
		// 	BirthDate = null;
		// else
		// 	BirthDate = $filter('date')(new Date(data.TanggalLahir.toLocaleString()), "yyyy-MM-dd");

		// if(angular.isUndefined(data.BerlakuSampai) || data.BerlakuSampai == null)
		// 	ValidTo = null;
		// else
		// 	ValidTo = $filter('date')(new Date(data.BerlakuSampai.toLocaleString()), "yyyy-MM-dd");
		var tmpIdx = 0;
		if(data.idx !== undefined){
			tmpIdx = data.idx;
		}else{
			var lengthGrid = $scope.TambahCustomerDiscount_CustomerList.length - 1;
			if($scope.TambahCustomerGrid.data.length == 0){
				tmpIdx = 0;
			}else{
				tmpIdx = $scope.TambahCustomerGrid.data[$scope.TambahCustomerGrid.data.length - 1].idx + 1;
			}
			console.log('birthData ==>',BirthDate);
			console.log('ValidTo ==>',ValidTo);

		}
		var DataInput = {
			"CustomerName" : data.CustomerName,
			"PhoneNumber" : data.PhoneNumber,
			"BirthDate" : $scope.changeFormatDate(data.BirthDate),
			"IdNumber" : data.IdNumber,
			"Npwp" : data.Npwp,
			"Address" : data.Address,
			"ValidTo" : $scope.changeFormatDate(data.ValidTo),
			"ProvinceName" : $scope.findValueFromArray($scope.provinsiData,'ProvinceId','ProvinceName',data.ProvinceId),
			"DistrictName" : $scope.findValueFromArray($scope.kecamatanData,'DistrictId','DistrictName',data.DistrictId),
			"CityRegencyName" : $scope.findValueFromArray($scope.kabupatenData,'CityRegencyId','CityRegencyName',data.CityRegencyId),
			"VillageName" : $scope.findValueFromArray($scope.kelurahanData,'VillageId','VillageName',data.VillageId),
			"PostalCode" : data.PostalCode ? data.PostalCode : 0,
			"VillageId" : data.VillageId,
			"DistrictId" : data.DistrictId,
			"CityRegencyId" : data.CityRegencyId,
			"ProvinceId" : data.ProvinceId,
			"idx": tmpIdx
		};
		console.log('=======>',DataInput, $scope.TambahCustomerGrid.data);
		console.log("data", data.idx,data, mode)


		var match = _.find($scope.TambahCustomerGrid.data, {idx:tmpIdx});
		if(match){
			var index = _.indexOf($scope.TambahCustomerGrid.data, _.find($scope.TambahCustomerGrid.data, {idx:tmpIdx}));
			// $scope.TambahCustomerGrid.data.splice(data.idx, 1,DataInput);
			$scope.TambahCustomerGrid.data.splice(index, 1, DataInput);
		}else{
			$scope.TambahCustomerGrid.data.push(DataInput);
		}
		// if(data.idx !== undefined){
		// 	$scope.TambahCustomerGrid.data.splice(data.idx, 1,DataInput);
		// }else{
		// 	$scope.TambahCustomerGrid.data.push(DataInput);
		// }
		$scope.showTambahCustomer = false;
	};
	$scope.tambahCustomerBaru = function(){
		$scope.modeModalCustomer = 'new';
		$scope.dataCustomer = {};
		$scope.showTambahCustomer = true;
	}

	$scope.selectProvince = function(row, data) {
		console.log('data province', data);
		console.log('row selectProvince', row);
		// $scope.dataCustomer.CityRegencyId = null;
		// $scope.dataCustomer.DistrictId = null;
		// $scope.dataCustomer.VillageId = null;
		// $scope.dataCustomer.PostalCode = null;
		if (data == undefined || data == null) {
			$scope.dataCustomer.CityRegencyId = null;
			$scope.dataCustomer.DistrictId = null;
			$scope.dataCustomer.VillageId = null;
			$scope.dataCustomer.PostalCode = null;
		}
		WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
			$scope.kabupatenData = resu.data.Result;
			console.log('Kabupaten', $scope.kabupatenData);
			if (data !== undefined) {
				$scope.selectRegency(row, $scope.kabupatenData);
			};
		});
		$scope.selectedProvince = angular.copy(row);
	};
 
	$scope.selectRegency = function(row, data) {
		console.log('data regency', data);
		console.log('row selectRegency', row);
		// $scope.dataCustomer.DistrictId = null;
		// $scope.dataCustomer.VillageId = null;
		// $scope.dataCustomer.PostalCode = null;
		if (data == undefined || data == null) {
			$scope.dataCustomer.DistrictId = null;
			$scope.dataCustomer.VillageId = null;
			$scope.dataCustomer.PostalCode = null;
		}
		$scope.kecamatanData = [];
		WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
			$scope.kecamatanData = resu.data.Result;
			if (data !== undefined) {
				$scope.selectDistrict(row, $scope.kecamatanData);
			};
		});
		// $scope.kecamatanData = row.MDistrict;
		$scope.selectedRegency = angular.copy(row);
	};
 
	$scope.selectDistrict = function(row, data) {
		console.log('row selectDistrict', row);
		if (data == undefined || data == null) {
			$scope.dataCustomer.VillageId = null;
			$scope.dataCustomer.PostalCode = null;
		}
		$scope.kelurahanData = []
		WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
			$scope.kelurahanData = resu.data.Result;
			if (data !== undefined) {
				$scope.selectVillage(row, $scope.kelurahanData);
			};
		});
		$scope.selectedDistrict = angular.copy(row);
	};
 
	$scope.selectVillage = function(row, data) {
		// $scope.dataCustomer.VillageId = null;
		// $scope.dataCustomer.PostalCode = null;
		console.log('row selectVillage', row, data);
		if (data == undefined || data == null) {
			$scope.dataCustomer.VillageId = null;
			$scope.dataCustomer.PostalCode = null;
		}
		$scope.dataCustomer.VillageId = row.VillageId;
		$scope.selectedVillage = angular.copy(row);
		$scope.dataCustomer.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
		console.log('row selectVillage $scope.dataCustomer.PostalCode', $scope.dataCustomer.PostalCode);
	};
	$scope.setAddress = function(data,i){
		if(i > (data.length - 1) ){
			return
		}
		var row = data[i];
		WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
			$scope.kabupatenData = resu.data.Result;
			for(var x in $scope.kabupatenData){
				$scope.kabupatenData[x].CityRegencyName = $scope.kabupatenData[x].CityRegencyName.split(',')[0];
				if(row.CityRegencyName.toLowerCase() == $scope.kabupatenData[x].CityRegencyName.toLowerCase()){
					row.CityRegencyId = $scope.kabupatenData[x].CityRegencyId
				}
			}
			WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
				$scope.kecamatanData = resu.data.Result;
				for(var x in $scope.kecamatanData){
					if(row.DistrictName.toLowerCase() == $scope.kecamatanData[x].DistrictName.toLowerCase()){
						row.DistrictId = $scope.kecamatanData[x].DistrictId
					}
				}
				WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
					$scope.kelurahanData = resu.data.Result;
					for(var x in $scope.kelurahanData){
						if(row.VillageName.toLowerCase() == $scope.kelurahanData[x].VillageName.toLowerCase()){
							row.VillageId = $scope.kelurahanData[x].VillageId
							row.PostalCode = $scope.kelurahanData[x].PostalCode
						}
					}
					data[i] = row;
					console.log("data");
					// ===== End Section of Row
					if (i < (data.length - 1)) {
						$scope.setAddress(data, i + 1);
						
						$scope.ariavalue += parseInt(80/data.length)
						$scope.ariavaluecss = parseInt(80/data.length) + '%';
	
					} else if ((data.length - 1) == i) {
						for(var zz in data){
							$scope.TambahCustomerGrid.data.push(data[zz]);
						}
						$scope.ariavalue = 100;
						$scope.ariavaluecss = '100%';
						$timeout(function() {
							ngDialog.closeAll();
							// bsAlert.alert({
							// 	title: "Upload Berhasil",
							// 	text: "Data berhasil di upload",
							// 	type: "success",
							// 	showCancelButton: false
							// },
							// 	function () {
							// 		$scope.showUploadCUstomer = false;
							// 	},
							// );
							$scope.showUploadCUstomer = false;
						}, 500);
					}
				});
			});
		});
	}

	$scope.changeFormatDate = function(item) {
		var tmpDate = item;
		console.log("changeFormatDate item", item);
		tmpDate = new Date(tmpDate);
		var finalDate
		var yyyy = tmpDate.getFullYear().toString();
		var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpDate.getDate().toString();
		finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	}

	// arr = for array
	$scope.findValueFromArray = function(arr, key, value, param){
		for(var i in arr){
			console.log('arr ====>', arr[i][key] ,key, param);
			if(arr[i][key] == param ){
				return arr[i][value]
			}
		}
	}

	$scope.TambahCustomerGrid = {
		useCustomPagination: true,
		useExternalPagination : true,
		multiSelect : false,
		enableFiltering: false,
		enableSorting: false,
		enableColumnResizing: true,
		columnDefs:[
			{
			  name: "No",
			  displayName: "No",
			  cellTemplate: '<span style="text-align:center">{{rowRenderIndex+1}}</span>',
			  visible: true,
			}, {
			  name: "CustomerGroupId",
			  field: "CustomerGroupId",
			  visible: false
			}, {
			  name: "CustomerId",
			  field: "CustomerId",
			  visible: false
			}, {
			  width:250,
			  name: "Nama",
			  field: "CustomerName"
			}, {
			  width:180,
			  name: "Tanggal Lahir",
			  field: "BirthDate",
			  cellFilter: 'date:\"dd-MM-yyyy\"'
			}, {
			  width:180,
			  name: "Alamat",
			  field: "Address"
			}, {
			  width:180,
			  name: "Provinsi",
			  field: "ProvinceName"
			}, {
			  width:180,
			  name: "Kota/Kab",
			  field: "CityRegencyName"
			}, {
			  width:180,
			  name: "Kecamatan",
			  field: "DistrictName"
			}, {
			  width:180,
			  name: "Kelurahan",
			  field: "VillageName"
			}, {
			  width:180,
			  name: "Kode Pos",
			  field: "PostalCode"
			}, {
			  width:180,
			  name: "Telepon",
			  field: "PhoneNumber"
			}, {
			  width:180,
			  name: "Nomor KTP",
			  field: "IdNumber"
			}, {
			  width:180,
			  name: "NPWP",
			  field: "Npwp"
			}, {
			  width:180,
			  name: "Berlaku sampai",
			  field: "ValidTo",
			  cellFilter: 'date:\"dd-MM-yyyy\"'
			}, {
			  name: 'VillageId',
			  field: 'VillageId',
			  visible: false
			}, {
			  name: 'CityRegencyId',
			  field: 'CityRegencyId',
			  visible: false
			}, {
			  name: 'DistrictId',
			  field: 'DistrictId',
			  visible: false
			}, {
			  name: 'ProvinceId',
			  field: 'ProvinceId',
			  visible: false
			}, {
			  width:150,
			  pinnedRight:true,
			  name: "Action",
			  cellTemplate: '<div class="ui-grid-cell-contents"> \
			  <a class="fa fa-list-alt fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" ng-click="grid.appScope.viewCustomer(row.entity)" title="Lihat"/a> \
			  <a class="fa fa-pencil fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" ng-click="grid.appScope.editCustomer(row.entity)" title="Edit"/a> \
			  <a class="fa fa-trash fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" ng-click="grid.appScope.deleteCustomer(row.entity)" title="Hapus"/a>'
			}
		],
		onRegisterApi: function(gridApi) {
			$scope.GridApiTambahCustomerDiscountList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// $scope.TambahCustomerDiscountList_UIGrid_Paging(pageNumber);
			});
		}
	}
	$scope.TambahCustomerDiscountList_UIGrid_Paging = function(pageNumber){
		CustomerDiscountFactory.getDataMain(pageNumber, uiGridPageSize)
		.then(
			function(res)
			{
				console.log("Get data nomor pajak", res.data.Result);
				$scope.TambahCustomerGrid.data = res.data.Result;
				$scope.TambahCustomerGrid.totalItems = res.data.Total;
				$scope.TambahCustomerGrid.paginationPageSize = uiGridPageSize;
				$scope.TambahCustomerGrid.paginationPageSizes = [uiGridPageSize];

			}

	  );
	}
	$scope.viewCustomer = function(row){
		$scope.dataCustomer = angular.copy(row);
		$scope.modeModalCustomer = 'view';
		$scope.showTambahCustomer = true;
		$scope.selectProvince(row, 1);
	}
	$scope.editCustomer = function(row){
		console.log("row ===>", row);
		$scope.dataCustomer = angular.copy(row);
		$scope.modeModalCustomer = 'edit';
		$scope.showTambahCustomer = true;
		$scope.selectProvince(row, 1);
	}
	$scope.deleteCustomer = function(row){
		bsAlert.alert({
            title: "Apakah anda yakin ingin menghapus data Customer ini ?",
            text: "data yang sudah dihapus tidak dapat dikembalikan",
            type: "question",
			showCancelButton: true,
			
        },function() {
			// $scope.TambahCustomerGrid.data.splice(row.idx,1);
			var match = _.find($scope.TambahCustomerGrid.data, {idx:row.idx});
			if(match){
				var index = _.indexOf($scope.TambahCustomerGrid.data, _.find($scope.TambahCustomerGrid.data, {idx:row.idx}));
				// $scope.TambahCustomerGrid.data.splice(data.idx, 1,DataInput);
				$scope.TambahCustomerGrid.data.splice(index, 1);
			}
		},function() {

		});
	}

	$scope.tambahUpload = function(){
		$scope.dataUpload = {};
		$scope.showUploadCUstomer = true;
		$('#UploadMasterCustomerDiscount').val("");
	}

	$scope.uploadExcel = function(row){
		var DataInput = {};
		var counter = 0;
		var tmpArrayCust = [];
		var tmpIdx = 1;
		console.log("data data", row)
		ngDialog.openConfirm({
			template: 	'<div>\
						<p class="loading-message">Mohon menunggu sedang memproses file...</p>\
						<div class="sk-three-bounce"><div id="loading-bar-spinner"><div class="spinner-icon"></div></div></div>\
						<div class="progress">\
							<div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="{{ariavalue}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ariavaluecss}}; background-color:#d53337;">{{ariavalue}}%</div>\
							</div>\
						</div>',
			plain: true,
			showClose: false,
			// controller: 'WOController',
			scope: $scope
		}).then(function(value) {}, function(value) {});
		$timeout(function() {
			$scope.ariavalue = 10;
			$scope.ariavaluecss = '10%';
				angular.forEach($scope.MasterCustomerDiscount_ExcelData, function(value, key){

				if (counter > 1) {

					//ini untuk ngubah format yang di upload by rizma
					var BirthDate = value.Tanggal_Lahir;
					// console.log("changeFormatDate item", item);
					BirthDate = new Date(BirthDate);
					var finalBirthDate
					var yyyyBirthDate = BirthDate.getFullYear().toString();
					var mmBirthDate = (BirthDate.getMonth() + 1).toString(); // getMonth() is zero-based
					var ddBirthDate = BirthDate.getDate().toString();
					// finalBirthDate = (ddBirthDate[1] ? ddBirthDate : "0" + ddBirthDate[0]) + '/' + (mmBirthDate[1] ? mmBirthDate : "0" + mmBirthDate[0]) + '/' + yyyyBirthDate;
					finalBirthDate =  yyyyBirthDate + '-' + (mmBirthDate[1] ? mmBirthDate : "0" + mmBirthDate[0]) + '-' + (ddBirthDate[1] ? ddBirthDate : "0" + ddBirthDate[0]);
					console.log("changeFormatDate finalDate", finalBirthDate);

					var ValidTo = value.Berlaku_Sampai;
					// console.log("changeFormatDate item", item);
					ValidTo = new Date(ValidTo);
					var finalValidTo
					var yyyyValidTo = ValidTo.getFullYear().toString();
					var mmValidTo = (ValidTo.getMonth() + 1).toString(); // getMonth() is zero-based
					var ddValidTo = ValidTo.getDate().toString();
					// finalValidTo = (ddValidTo[1] ? ddValidTo : "0" + ddValidTo[0]) + '/' + (mmValidTo[1] ? mmValidTo : "0" + mmValidTo[0]) + '/' + yyyyValidTo;
					finalValidTo = yyyyValidTo + '-' + (mmValidTo[1] ? mmValidTo : "0" + mmValidTo[0]) + '-' + (ddValidTo[1] ? ddValidTo : "0" + ddValidTo[0]) ;
					console.log("changeFormatDate finalDate", finalValidTo);
					
					console.log("data", row, value)
					for(var x in $scope.provinsiData){
						console.log("data Provinsi", $scope.provinsiData)
						if(value.Provinsi.toLowerCase() == $scope.provinsiData[x].ProvinceName.toLowerCase()){
							console.log("Nama Provinsi", value.Provinsi.toLowerCase(),"------", $scope.provinsiData[x].ProvinceName.toLowerCase())
							value.ProvinceId = $scope.provinsiData[x].ProvinceId
						}
					}

					row = value
					console.log("asem", row)
					DataInput = {
						"CustomerName" : value.Nama,
						"PhoneNumber" : value.Telepon,
						"BirthDate" : finalBirthDate,
						"IdNumber" : value.KTP,
						"Npwp" : value.NPWP,
						"Address" : value.Alamat,
						"ValidTo" :finalValidTo,
						"ProvinceName" : value.Provinsi,
						"DistrictName" : value.Kecamatan,
						"CityRegencyName" : value.Kota,
						"VillageName" : value.Kelurahan,
						"PostalCode" : value.Kode_Pos,
						"ProvinceId" : value.ProvinceId,
						'idx':tmpIdx
					};
					tmpArrayCust.push(DataInput);
					
					// for(var c in $scope.kabupatenData){
					// 	console.log("data Kabupaten", $scope.kabupatenData)
					// 	if(value.Kota.toLowerCase() == $scope.kabupatenData[c].CityRegencyName.split(",")[0].toLowerCase()){
					// 		console.log("Nama Kabupaten", value.Kota.toLowerCase(),"------", $scope.kabupatenData[c].CityRegencyName.split(",")[0].toLowerCase())
					// 		value.CityRegencyId = $scope.kabupatenData[c].CityRegencyId
					// 	}
					// }

					// for(var a in $scope.kecamatanData){
					// 	console.log("data Kecamatan", $scope.kecamatanData)
					// 	if(value.Kecamatan.toLowerCase() == $scope.kecamatanData[a].DistrictName.toLowerCase()){
					// 		console.log("Nama Kecamatan", value.Kecamatan.toLowerCase(),"------", $scope.kecamatanData[a].DistrictName.toLowerCase())
					// 		value.DistrictId = $scope.kecamatanData[a].DistrictId
					// 	}
					// }

					// for(var b in $scope.kelurahanData){
					// 	console.log("data Kelurahan", $scope.kelurahanData)
					// 	if(value.Kelurahan.toLowerCase() == $scope.kelurahanData[b].VillageName.toLowerCase()){
					// 		console.log("Nama Kelurahan", value.Kelurahan.toLowerCase(),"------", $scope.kelurahanData[b].VillageName.toLowerCase())
					// 		value.VillageId = $scope.kelurahanData[b].VillageId
					// 	}
					// }

						// console.log("Value", value)
						// DataInput = {
						// 	"CustomerName" : value.Nama,
						// 	"PhoneNumber" : value.Telepon,
						// 	"BirthDate" : finalBirthDate,
						// 	"IdNumber" : value.KTP,
						// 	"Npwp" : value.NPWP,
						// 	"Address" : value.Alamat,
						// 	"ValidTo" :finalValidTo,
						// 	"ProvinceName" : value.Provinsi,
						// 	"DistrictName" : value.Kecamatan,
						// 	"CityRegencyName" : value.Kota,
						// 	"VillageName" : value.Kelurahan,
						// 	"PostalCode" : value.Kode_Pos,
						// 	"VillageId" : value.VillageId,
						// 	"DistrictId" : value.DistrictId,
						// 	"CityRegencyId" : value.CityRegencyId,
						// 	"ProvinceId" : value.ProvinceId,
						// 	'idx':counter
						// };
						// console.log("Data Input", DataInput)
						// $scope.TambahCustomerGrid.data.push(DataInput);
					}
					tmpIdx += 1;
					counter = counter + 1;

				});
				for(var c in tmpArrayCust){
					if(tmpArrayCust[c].CustomerName.length > 50){
						bsAlert.warning('Tidak Dapat Upload Karena Nama CustomerName Terlalu Panjang' )
						ngDialog.closeAll();
						return false;
					}
				}
				$scope.setAddress(tmpArrayCust, 0)
		
		}, 500); // WAIT 1 second
		// angular.element('#ModalUploadMasterCustomerDiscount').modal('hide');
        // bsAlert.alert({
        //     title: "Upload Berhasil",
        //     text: "Data berhasil di upload",
        //     type: "success",
        //     showCancelButton: false
        // });
	}

	// INITALIZE AUTO COMPLETE
	$scope.noResults = true;
	$scope.loadingCustomer = false;
	$scope.onSelectCust = function($item, $model, $label){
		console.log("onSelectCust=>", $item);
		console.log("onSelectCust=>", $model);
		console.log("onSelectCust=>", $label);
		if($scope.dataCustomer.idx){
			$item.idx = $scope.dataCustomer.idx;
		}
		$scope.dataCustomer = $item;
		$scope.selectProvince($item, 1);
	}
	
	$scope.getCustomer = function(key){
		$scope.loadingCustomer = true;
		var ress = CustomerDiscountFactory.getDataCustomer(key).then(
			function(res){
				$scope.loadingCustomer = false;
				return res.data.Result;
			}
		);
		return ress
	}

	$scope.onGotResult = function() {
		console.log("onGotResult=>");
	};
	$scope.onNoResult = function() {

	}
	// ==========================
	$scope.actDeleteTask = function(row){
		$scope.tam
		bsAlert.alert({
			title: "Apakah Anda yakin akan menghapus 1 pekerjaan ?",
			text: "",
			type: "question",
			showCancelButton: true
		},
			function() {
				CustomerDiscountFactory.deleteData(row.GroupCustomerId)
				.then(
					function(res){				
						bsAlert.success("Data berhasil dihapus");	
						$scope.MainCustomerDiscountList_UIGrid_Paging(1)
					});		
					console.log("hapus data ==>>", row.GroupCustomerId);
				},
					function() {

				});
	}

	$scope.TambahCustomerDiscount_ColumnDefs = [
						{ name: "No", displayName: "No", cellTemplate: '<span>{{rowRenderIndex+1}}</span>', visible: true },
						{name:"CustomerGroupId",field:"CustomerGroupId",visible:false},
						{name:"CustomerId",field:"CustomerId",visible:false},
						{name:"Nama",field:"CustomerName"},
						{name:"Tanggal Lahir",field:"BirthDate", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Alamat",field:"Address"},
						{name:"Provinsi",field:"ProvinceName"},
						{name:"Kota/Kab",field:"CityRegencyName"},
						{name:"Kecamatan",field:"DistrictName"},
						{name:"Kelurahan",field:"VillageName"},
						{name:"Kode Pos",field:"PostalCode"},
						{name:"Telepon",field:"PhoneNumber"},
						{name:"Nomor KTP",field:"IdNumber"},
						{name:"NPWP",field:"Npwp"},
						{name:"Berlaku sampai",field:"ValidTo", cellFilter:'date:\"dd-MM-yyyy\"'},
						{ name:'VillageId', field:'VillageId', visible:false},
						{ name:'CityRegencyId', field:'CityRegencyId', visible:false},
						{ name:'DistrictId', field:'DistrictId', visible:false},
						{ name:'ProvinceId', field:'ProvinceId', visible:false},
						{name:"Action",
							cellTemplate:'<a class="fa fa-pencil fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" ng-click="grid.appScope.TambahCustomerDiscountListEdit(row)" title="Edit"/a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahCustomerDiscountListView(row)" class="fa fa-list-alt fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Lihat"/a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahCustomerDiscountListDelete(row)" class="fa fa-fw fa-lg fa-trash" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Hapus"/a>'},
								],

	console.log("user : ", user);
	///MAIN CUSTOMER DISCOUNT
	$scope.MainCustomerDiscount_Show = true;
	$scope.TambahCustomerData_Show = false;
	$scope.TambahCustomerDiscount_Show = false;

	$scope.disableAddButton = false;
	$scope.value = function(param){
		console.log("param1",param);
		// if (param.value.TamBit==0)
		if (param.value.TamBit==1)
		{
			$scope.disableAddButton = true;

		}
		else
		{

			$scope.disableAddButton = false;
		}

		console.log("param2",param);
	}

	$scope.MainCustomerDiscount_Tambah_Clicked = function(){
		$scope.StatusCrud_MainPage = 'Tambah';
		$scope.MainCustomerDiscount_Show = false;
		$scope.TambahCustomerDiscount_Show = true;
		$scope.TambahCustomerDiscount_View_Show = true;
		$scope.TambahCustomerDiscount_View_Disable = false;
	}

	$scope.MainCustomerDiscountListEdit = function(row){
		$scope.StatusCrud_MainPage = 'Edit';
		$scope.TambahCustomerDiscount_View_Show = true;
		$scope.TambahCustomerDiscount_View_Disable = false;
		$scope.TambahCustomerDiscount_GroupCustomerId = row.entity.GroupCustomerId;
		$scope.TambahCustomerDiscount_GroupCustomer = row.entity.GroupCustomerName;
		$scope.TambahCustomerDiscount_Jasa = row.entity.DiscountJasa;
		$scope.TambahCustomerDiscount_Parts = row.entity.DiscountParts;
		$scope.TambahCustomerDiscount_OPL = row.entity.DiscountOPL;
		$scope.TambahCustomerDiscount_Total = row.entity.TotalDiscount;
		$scope.TambahCustomerDiscount_GRBP = row.entity.Category;
		$scope.TambahCustomerDiscount_ColumnDefs = [
						{ name: "No", displayName: "No", cellTemplate: '<span>{{rowRenderIndex+1}}</span>', visible:true},
						{name:"CustomerGroupId",field:"CustomerGroupId",visible:false},
						{name:"CustomerId",field:"CustomerId",visible:false},
						{name:"Nama",field:"CustomerName"},
						{name:"Tanggal Lahir",field:"BirthDate", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Alamat",field:"Address"},
						{name:"Provinsi",field:"ProvinceName"},
						{name:"Kota/Kab",field:"CityRegencyName"},
						{name:"Kecamatan",field:"DistrictName"},
						{name:"Kelurahan",field:"VillageName"},
						{name:"Kode Pos",field:"PostalCode"},
						{name:"Telepon",field:"PhoneNumber"},
						{name:"Nomor KTP",field:"IdNumber"},
						{name:"NPWP",field:"Npwp"},
						{name:"Berlaku sampai",field:"ValidTo", cellFilter:'date:\"dd-MM-yyyy\"'},
						{ name:'VillageId', field:'VillageId', visible:false},
						{ name:'CityRegencyId', field:'CityRegencyId', visible:false},
						{ name:'DistrictId', field:'DistrictId', visible:false},
						{ name:'ProvinceId', field:'ProvinceId', visible:false},
						{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.TambahCustomerDiscountListEdit(row)" class="fa fa-pencil fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Edit"/a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.TambahCustomerDiscountListDelete(row)" class="fa fa-fw fa-lg fa-trash" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" title="Hapus"/a>'},
					],
		$scope.TambahCustomerDiscountList_UIGrid.columnDefs = $scope.TambahCustomerDiscount_ColumnDefs;
		$scope.TambahCustomerDiscountList_UIGrid.data = [];
		CustomerDiscountFactory.getDataGroupCustomerList(row.entity.GroupCustomerId)
		.then(
			function(res){
				$scope.TambahCustomerDiscount_CustomerList = res.data.Result;
				var idx = 0;
				for (var i in $scope.TambahCustomerDiscount_CustomerList){
					$scope.TambahCustomerDiscount_CustomerList[i].idx = idx;
					idx++
				}
				console.log("$scope.TambahCustomerDiscount_CustomerList ===", $scope.TambahCustomerDiscount_CustomerList);
				$scope.TambahCustomerDiscountList_UIGrid.data = $scope.TambahCustomerDiscount_CustomerList;
			}
		);

		$scope.MainCustomerDiscount_Show = false;
		$scope.TambahCustomerDiscount_Show = true;
	}

	$scope.TambahCustomerDiscountListView = function(row){
		$scope.modeCrud = 'view';

		var DateBirth = '';
		var ValidTo = '';

		// if(row.entity.BirthDate != null)
		// 	DateBirth = row.entity.BirthDate.substring(6, 10) + '-' + row.entity.BirthDate.substring(3,5)+ '-' + row.entity.BirthDate.substring(0,2);

		// if(row.entity.ValidTo != null)
		// 	ValidTo = row.entity.ValidTo.substring(6, 10) + '-' + row.entity.ValidTo.substring(3,5)+ '-' + row.entity.ValidTo.substring(0,2);
		$scope.lmModelAddress = angular.copy(row.entity);
		console.log("$scope.lmModelAddress11", $scope.lmModelAddress);

		$scope.selectProvince( row.entity, $scope.provinsiData);
		console.log("DateBirth : ", DateBirth);
		$scope.TambahCustomerDiscount_CustomerId = row.entity.CustomerId;
		$scope.TambahCustomerData_NamaPemilik = row.entity.CustomerName;
		$scope.TambahCustomerData_NomorTelepon = row.entity.PhoneNumber;
		$scope.TambahCustomerData_TanggalLahir = row.entity.BirthDate;
		$scope.TambahCustomerData_NomorKTP = row.entity.IdNumber;
		$scope.TambahCustomerData_NomorNPWP = row.entity.Npwp;
		$scope.TambahCustomerData_Alamat = row.entity.Address;
		$scope.TambahCustomerData_Provinsi = row.entity.ProvinceName;
		$scope.TambahCustomerData_Kecamatan = row.entity.DistrictName;
		$scope.TambahCustomerData_KotaKab = row.entity.CityRegencyName;
		$scope.TambahCustomerData_Kelurahan = row.entity.VillageName;
		$scope.TambahCustomerData_KodePos = row.entity.PostalCode;
		$scope.TambahCustomerData_VillageId = row.entity.VillageId;
		$scope.TambahCustomerData_DistrictId = row.entity.DistrictId;
		$scope.TambahCustomerData_ProvinceId = row.entity.ProvinceId;
		$scope.TambahCustomerData_CityRegencyId = row.entity.CityRegencyId;
		$scope.TambahCustomerData_BerlakuSampai = row.entity.ValidTo;

		$scope.MainCustomerDiscount_Show = false;
		$scope.TambahCustomerDiscount_Show = false;
		$scope.TambahCustomerData_Show = true;
		$scope.TambahCustomerDiscount_View_Show = false;
		$scope.TambahCustomerDiscount_View_Disable = true;
	}

	$scope.TambahCustomerDiscount_Upload_Clicked = function(){
		angular.element('#ModalUploadMasterCustomerDiscount').modal('show');
		$('#UploadMasterCustomerDiscount').val("")
	}

	$scope.loadXLSMasterCustomerDiscount = function(ExcelFile){
		var ExcelData = [];
		var myEl = angular.element( document.querySelector( '#UploadMasterCustomerDiscount' ) ); //ambil elemen dari dokumen yang di-upload

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				ExcelData = json[0];
				console.log("ExcelFile : ",ExcelFile);
				console.log("ExcelData : ", ExcelData);
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterCustomerDiscount_ExcelData = json;
            });
	}

	$scope.UploadMasterCustomerDiscount_Upload_Clicked = function(){
		var DataInput = {};
		var counter = 0;
		console.log("$scope.TambahCustomerDiscountList_UIGrid.data",$scope.TambahCustomerDiscountList_UIGrid.data);
		// counter = $scope.TambahCustomerDiscountList_UIGrid.data.length + 1;
		angular.forEach($scope.MasterCustomerDiscount_ExcelData, function(value, key){

			if (counter > 1) {

		//ini untuk ngubah format yang di upload by rizma
		var BirthDate = value.Tanggal_Lahir;
        // console.log("changeFormatDate item", item);
        BirthDate = new Date(BirthDate);
        var finalBirthDate
        var yyyyBirthDate = BirthDate.getFullYear().toString();
        var mmBirthDate = (BirthDate.getMonth() + 1).toString(); // getMonth() is zero-based
        var ddBirthDate = BirthDate.getDate().toString();
        // finalBirthDate = (ddBirthDate[1] ? ddBirthDate : "0" + ddBirthDate[0]) + '/' + (mmBirthDate[1] ? mmBirthDate : "0" + mmBirthDate[0]) + '/' + yyyyBirthDate;
        finalBirthDate =  yyyyBirthDate + '-' + (mmBirthDate[1] ? mmBirthDate : "0" + mmBirthDate[0]) + '-' + (ddBirthDate[1] ? ddBirthDate : "0" + ddBirthDate[0]);
        console.log("changeFormatDate finalDate", finalBirthDate);

		var ValidTo = value.Berlaku_Sampai;
        // console.log("changeFormatDate item", item);
        ValidTo = new Date(ValidTo);
        var finalValidTo
        var yyyyValidTo = ValidTo.getFullYear().toString();
        var mmValidTo = (ValidTo.getMonth() + 1).toString(); // getMonth() is zero-based
        var ddValidTo = ValidTo.getDate().toString();
        // finalValidTo = (ddValidTo[1] ? ddValidTo : "0" + ddValidTo[0]) + '/' + (mmValidTo[1] ? mmValidTo : "0" + mmValidTo[0]) + '/' + yyyyValidTo;
        finalValidTo = yyyyValidTo + '-' + (mmValidTo[1] ? mmValidTo : "0" + mmValidTo[0]) + '-' + (ddValidTo[1] ? ddValidTo : "0" + ddValidTo[0]) ;
        console.log("changeFormatDate finalDate", finalValidTo);

			DataInput = {
				"CustomerName" : value.Nama,
				"PhoneNumber" : value.Telepon,
				"BirthDate" : finalBirthDate,
				"IdNumber" : value.KTP,
				"Npwp" : value.NPWP,
				"Address" : value.Alamat,
				"ValidTo" :finalValidTo,
				"ProvinceName" : value.Provinsi,
				"DistrictName" : value.Kecamatan,
				"CityRegencyName" : value.Kota,
				"VillageName" : value.Kelurahan,
				"PostalCode" : value.Kode_Pos,
				"VillageId" : 0,
				"DistrictId" : 0,
				"CityRegencyId" : 0,
				"ProvinceId" : 0
			};

			$scope.TambahCustomerDiscount_CustomerList.push(DataInput);
			$scope.TambahCustomerDiscountList_UIGrid.data.push(DataInput);
		}
			counter = counter + 1;

		});

		angular.element('#ModalUploadMasterCustomerDiscount').modal('hide');
        bsAlert.alert({
            title: "Upload Berhasil",
            text: "Data berhasil di upload",
            type: "success",
            showCancelButton: false
        });
	}

	$scope.TambahCustomerDiscount_Download_Clicked = function(){
		var excelData = [];
		var fileName = "";

		var datahead = {"Nama":"Varchar(50) (Ex. BUDI)", "Tanggal_Lahir":"(yyyy/MM/dd) (Ex. 1980/07/03)", "Alamat":"Varchar(100) (Ex. JAKARTA)", "Telepon":"Varchar(20) (Ex. 089774839274)"
					, "Provinsi":"Varchar(100) (Ex. DKI JAKARTA)", "Kota":"Varchar(100) (Ex. JAKARTA PUSAT)", "Kecamatan":"Varchar(100) (Ex. GAMBIR)", "Kelurahan":"Varchar(100) (Ex. GAMBIR)", "Kode_Pos":"int (Ex. 14360)", "KTP":"Varchar(20) (Ex. 324332944895348942223)","NPWP":"Varchar(20) (Ex. 76.066.862.4-044.000)", "Berlaku_Sampai":"(yyyy/MM/dd) (Ex. 2020/07/01)"};
		fileName = "Customer_Discount_Data_Template";

		var excelSheetSeparator = {
		    "Nama": "-", "Tanggal_Lahir": "-", "KMEnd": "-", "Alamat": "ISI", "Telepon": "SETELAH", "Provinsi": "BARIS", "Kota": "INI",
		    "Kecamatan": "-", "Kelurahan": "-", "Kode_Pos" : "-", "KTP" : "-", "NPWP" : "-", "Berlaku_Sampai" : "-"
		};
		excelData.push(datahead);
		excelData.push(excelSheetSeparator);
		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.MainCustomerDiscountListView = function(row){
		$scope.StatusCrud_MainPage = 'View';
		$scope.TambahCustomerDiscount_View_Show = false;
		$scope.TambahCustomerDiscount_View_Disable = true;

		$scope.TambahCustomerDiscount_GroupCustomerId = row.entity.GroupCustomerId;
		$scope.TambahCustomerDiscount_GroupCustomer = row.entity.GroupCustomerName;
		$scope.TambahCustomerDiscount_Jasa = row.entity.DiscountJasa;
		$scope.TambahCustomerDiscount_Parts = row.entity.DiscountParts;
		$scope.TambahCustomerDiscount_OPL = row.entity.DiscountOPL;
		$scope.TambahCustomerDiscount_Total = row.entity.TotalDiscount;
		$scope.TambahCustomerDiscount_GRBP = row.entity.Category;

		$scope.TambahCustomerDiscount_ColumnDefs = [
						{ name: "No", displayName: "No", cellTemplate: '<span>{{rowRenderIndex+1}}</span>', visible: true,  },
						{name:"CustomerGroupId",field:"CustomerGroupId",visible:false},
						{name:"CustomerId",field:"CustomerId",visible:false},
						{name:"Nama",field:"CustomerName"},
						{name:"Tanggal Lahir",field:"BirthDate", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Alamat",field:"Address"},
						{name:"Provinsi",field:"ProvinceName"},
						{name:"Kota/Kab",field:"CityRegencyName"},
						{name:"Kecamatan",field:"DistrictName"},
						{name:"Kelurahan",field:"VillageName"},
						{name:"Kode Pos",field:"PostalCode"},
						{name:"Telepon",field:"PhoneNumber"},
						{name:"Nomor KTP",field:"IdNumber"},
						{name:"NPWP",field:"Npwp"},
						{name:"Berlaku sampai",field:"ValidTo", cellFilter:'date:\"dd-MM-yyyy\"'},
						{ name:'VillageId', field:'VillageId', visible:false},
						{ name:'CityRegencyId', field:'CityRegencyId', visible:false},
						{ name:'DistrictId', field:'DistrictId', visible:false},
						{ name:'ProvinceId', field:'ProvinceId', visible:false},
						{name:"Action",
							 cellTemplate:'<a class="fa fa-list-alt fa-fw fa-lg" style="color:#777; font-size:16px; margin-top:8px; margin-left:10px; margin-right:5px" ng-click="grid.appScope.TambahCustomerDiscountListView(row)" title="Lihat"/a>'}
					],
		$scope.TambahCustomerDiscountList_UIGrid.columnDefs = $scope.TambahCustomerDiscount_ColumnDefs;
		$scope.TambahCustomerDiscountList_UIGrid.data = [];
		CustomerDiscountFactory.getDataGroupCustomerList(row.entity.GroupCustomerId)
		.then(
			function(res){
				$scope.TambahCustomerDiscount_CustomerList = res.data.Result;
				$scope.TambahCustomerDiscountList_UIGrid.data = $scope.TambahCustomerDiscount_CustomerList;
				$scope.counter++;
			}
		);

		$scope.MainCustomerDiscount_Show = false;
		$scope.TambahCustomerDiscount_Show = true;
	}

	///TAMBAH CUSTOMER DISCOUNT
	$scope.TambahCustomerDiscount_Tambah_Clicked = function(){
		$scope.TambahCustomerDiscount_Show = false;
		$scope.TambahCustomerData_Show = true;
		$scope.tmpIdx = 0;
	}

	$scope.TambahCustomerDiscount_Batal_Clicked = function(){
		$scope.TambahCustomerDiscount_ClearFields();
		$scope.MainCustomerDiscount_Show = true;
		$scope.TambahCustomerDiscount_Show = false;
		$scope.TambahCustomerDiscount_View_Show = true;
		$scope.TambahCustomerDiscount_View_Disable = false;
	}

	$scope.TambahCustomerDiscount_Simpan_Clicked = function(){
		// var BirthDate = '';
		// var ValidTo = '';
		// $scope.TambahCustomerDiscount_CustomerList.some(function(obj, i){

		// 	// DateBirth = obj.BirthDate.substring(6, 10) + '-' + obj.BirthDate.substring(3,5)+ '-' + obj.BirthDate.substring(0,2);
		// 	// ValidTo = obj.ValidTo.substring(6, 10) + '-' + obj.ValidTo.substring(3,5)+ '-' + obj.ValidTo.substring(0,2);

		// 	DateBirth = obj.BirthDate;
		// 	ValidTo = obj.ValidTo;

		// 	$scope.TambahCustomerDiscount_CustomerList[i].BirthDate = $filter('date')(new Date(DateBirth), "dd-MM-yyyy");
		// 	$scope.TambahCustomerDiscount_CustomerList[i].ValidTo = $filter('date')(new Date(ValidTo), "dd-MM-yyyy");
		// });

		var inputData = [{
			"GroupCustomerId": $scope.TambahCustomerDiscount_GroupCustomerId,
			"GroupCustomerName": $scope.TambahCustomerDiscount_GroupCustomer,
			"Category": $scope.TambahCustomerDiscount_GRBP,
			"DiscountJasa": $scope.TambahCustomerDiscount_Jasa,
			"DiscountParts": $scope.TambahCustomerDiscount_Parts,
			"DiscountOPL":$scope.TambahCustomerDiscount_OPL,
			"CustomerList"  :$scope.TambahCustomerDiscount_CustomerList
		}];

		CustomerDiscountFactory.create(inputData)
		.then(
			function(res){
				console.log("res", res);
				if(res.data.Result.length == 0){
					alert("Data berhasil disimpan");
					$scope.TambahCustomerDiscount_Batal_Clicked();
					$scope.MainCustomerDiscountList_UIGrid_Paging(1);
				}
				else{
					alert(res.data.Result[0].Message);
				}
			}
		);
	}

	$scope.TambahCustomerDiscount_ClearFields = function(){
		$scope.TambahCustomerDiscount_GroupCustomerId = 0;
		$scope.TambahCustomerDiscount_GroupCustomer = '';
		$scope.TambahCustomerDiscount_GRBP = undefined;
		$scope.TambahCustomerDiscount_Jasa = '';
		$scope.TambahCustomerDiscount_Parts = '';
		$scope.TambahCustomerDiscount_OPL = '';
		$scope.TambahCustomerDiscount_Total = '';
		$scope.TambahCustomerDiscountList_UIGrid.data = [];
		$scope.TambahCustomerDiscount_CustomerList = [];
	}

	$scope.MainCustomerDiscountListDelete = function(row){
		$scope.TambahCustomerDiscount_GroupCustomerId = row.entity.GroupCustomerId;
		$scope.MainCustomerDiscountHapus_CounterName = row.entity.GroupCustomerName;
		//angular.element('#ModalHapusMasterCustomerDiscount').modal('show');
		bsAlert.alert({
			title: "Apakah Anda yakin ingin menghapus " + $scope.MainCustomerDiscountHapus_CounterName,
			text: "",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak",
		}, function(){
	//}

	// $scope.Batal_MainCustomerDiscount = function(){
	// 	$scope.TambahCustomerDiscount_GroupCustomerId = 0;
	// 	$scope.MainCustomerDiscountHapus_CounterName = '';
	// 	angular.element('#ModalHapusMasterCustomerDiscount').modal('hide');
	// }

	// $scope.Hapus_MainCustomerDiscount = function(){
		CustomerDiscountFactory.deleteData($scope.TambahCustomerDiscount_GroupCustomerId)
		.then(
			function(res){
				//alert("Data berhasil dihapus");
				$scope.MainCustomerDiscountList_UIGrid_Paging(1);
			}
		);
		//$scope.Batal_MainCustomerDiscount();
		});
	}

	$scope.TambahCustomerData_Simpan_Clicked = function(){
		console.log('hadir');
		var BirthDate = '';
		var ValidTo = '';
		console.log("$scope.TambahCustomerData_BerlakuSampai", $scope.TambahCustomerData_BerlakuSampai);
		if(angular.isUndefined($scope.TambahCustomerData_TanggalLahir) || $scope.TambahCustomerData_TanggalLahir == null)
			BirthDate = null;
		else
			BirthDate = $filter('date')(new Date($scope.TambahCustomerData_TanggalLahir.toLocaleString()), "yyyy-MM-dd");

		if(angular.isUndefined($scope.TambahCustomerData_BerlakuSampai) || $scope.TambahCustomerData_BerlakuSampai == null)
			ValidTo = null;
		else
			ValidTo = $filter('date')(new Date($scope.TambahCustomerData_BerlakuSampai.toLocaleString()), "yyyy-MM-dd");

		var lengthGrid = $scope.TambahCustomerDiscount_CustomerList.length - 1;
		var tmpIdx = 0;
		console.log('length',lengthGrid);

		// if($scope.tmpIdx == 0){
		// 	if ($scope.TambahCustomerDiscount_CustomerList.length > 0) {
		// 		tmpIdx = $scope.TambahCustomerDiscount_CustomerList[lengthGrid].idx + 1;
		// 		console.log(tmpIdx);
		// 	} else {
		// 		tmpIdx = 1;
		// 		console.log(tmpIdx);

		// 	}
		// }else{
		// 	tmpIdx = $scope.tmpIdx;
		// 	// var index = _.indexOf($scope.TambahCustomerDiscount_CustomerList, _.find($scope.TambahCustomerDiscount_CustomerList, { 'idx':tmpIdx}));
		// 	// $scope.TambahCustomerDiscount_CustomerList.splice(index);
		// 	//remove array object by position below
		// 	$scope.TambahCustomerDiscount_CustomerList = $scope.TambahCustomerDiscount_CustomerList.filter(function(el) {
		// 		return el.idx !== tmpIdx;
		// 	});
		// }

		// console.log('idx ===>', tmpIdx);
		// console.log('idx ===>', $scope.tmpIdx);
		$scope.TambahCustomerData_ProvinceId = $scope.selectedProvince.ProvinceId;
		$scope.TambahCustomerData_Provinsi = $scope.selectedProvince.ProvinceName;

		$scope.TambahCustomerData_CityRegencyId = $scope.selectedRegency.CityRegencyId;
		$scope.TambahCustomerData_KotaKab = $scope.selectedRegency.CityRegencyName;

		$scope.TambahCustomerData_DistrictId = $scope.selectedDistrict.DistrictId;
		$scope.TambahCustomerData_Kecamatan = $scope.selectedDistrict.DistrictName;

		$scope.TambahCustomerData_VillageId = $scope.selectedVillage.VillageId;
		$scope.TambahCustomerData_Kelurahan = $scope.selectedVillage.VillageName;

		$scope.TambahCustomerData_KodePos = $scope.selectedVillage.PostalCode;

		var DataInput = {
			"CustomerName" : $scope.TambahCustomerData_NamaPemilik,
			"PhoneNumber" : $scope.TambahCustomerData_NomorTelepon,
			"BirthDate" : BirthDate,
			"IdNumber" : $scope.TambahCustomerData_NomorKTP,
			"Npwp" : $scope.TambahCustomerData_NomorNPWP,
			"Address" : $scope.TambahCustomerData_Alamat,
			"ValidTo" : ValidTo,
			"ProvinceName" : $scope.TambahCustomerData_Provinsi,
			"DistrictName" : $scope.TambahCustomerData_Kecamatan,
			"CityRegencyName" : $scope.TambahCustomerData_KotaKab,
			"VillageName" : $scope.TambahCustomerData_Kelurahan,
			"PostalCode" : $scope.TambahCustomerData_KodePos,
			"VillageId" : $scope.TambahCustomerData_VillageId,
			"DistrictId" : $scope.TambahCustomerData_DistrictId,
			"CityRegencyId" : $scope.TambahCustomerData_CityRegencyId,
			"ProvinceId" : $scope.TambahCustomerData_ProvinceId,
			"idx": $scope.TambahCustomerData_idx
		};
			// var key = {};
			// key[tmpIdx] = $scope.TambahCustomerDiscount_CustomerList[vm.dId];
			// var match = _.find($scope.TambahCustomerDiscount_CustomerList, tmpIdx);
			// var xitem = angular.copy(item);
			// if(match){

		// var findIdx = _.findIndex($scope.TambahCustomerDiscount_CustomerList, { idx: $scope.TambahCustomerData_idx})
		// $scope.TambahCustomerDiscount_CustomerList.splice(findIdx,1);
		// $scope.TambahCustomerDiscount_CustomerList.push(DataInput);
		// $scope.TambahCustomerDiscount_CustomerList.sort(function (a, b) {
		// 	return a.idx - b.idx;
		//   });
		console.log("$scope.TambahCustomerDiscount_CustomerList", $scope.TambahCustomerDiscount_CustomerList);
		// $scope.TambahCustomerDiscountList_UIGrid.data = $scope.TambahCustomerDiscount_CustomerList;
		$scope.TambahCustomerDiscountList_UIGrid.data.push(DataInput);
		$scope.TambahCustomerData_Batal_Clicked();
	}

	$scope.onBeforeSaveAddress = function(row) {
		console.log('masuk sini onbeforesaveaddress', row);
		var AddressCategory = {};
		if ($scope.getCustIdBySearch) {
			row.CustomerId = $scope.CustomerId;
		} else {
			row.CustomerId = 0;
		};
		_.map($scope.categoryContact, function(val) {
			if (val.AddressCategoryId == row.AddressCategoryId) {
				AddressCategory = val;
			}
		});
		row.AddressCategory = AddressCategory;
		row.ProvinceData = $scope.selectedProvince;
		row.CityRegencyData = $scope.selectedRegency;
		row.DistrictData = $scope.selectedDistrict;
		row.VillageData = $scope.selecedtVillage;
		console.log('masuk sini onbeforesaveaddress NEWDATA', row);
	};


	$scope.TambahCustomerData_Batal_Clicked = function(){
		console.log('statusCrud_MainPage', $scope.StatusCrud_MainPage)

			if ($scope.StatusCrud_MainPage == 'View'){
				$scope.TambahCustomerDiscount_Customer_ClearFields();
				$scope.TambahCustomerDiscount_View_Show = false;
				$scope.TambahCustomerDiscount_View_Disable = true;
				$scope.TambahCustomerDiscount_Show = true;
				$scope.TambahCustomerData_Show = false;
			} else if ($scope.StatusCrud_MainPage == 'Edit') {
				$scope.TambahCustomerDiscount_Customer_ClearFields();
				$scope.TambahCustomerDiscount_Show = true;
				$scope.TambahCustomerData_Show = false;
				$scope.TambahCustomerDiscount_View_Show = true;
				$scope.TambahCustomerDiscount_View_Disable = false;
			} else {
				console.log('masuk sini ga?')
				$scope.TambahCustomerDiscount_Customer_ClearFields();
				$scope.TambahCustomerDiscount_Show = true;
				$scope.TambahCustomerData_Show = false;
				$scope.TambahCustomerDiscount_View_Show = true;
				$scope.TambahCustomerDiscount_View_Disable = false;
			}

		//ini yang lama, lumayan buat jadi patokan=================
		// if ($scope.modeCrud == 'view'){
		// 	$scope.TambahCustomerDiscount_Customer_ClearFields();
		// 	$scope.TambahCustomerDiscount_View_Show = false;
		// 	$scope.TambahCustomerDiscount_View_Disable = true;
		// 	$scope.TambahCustomerDiscount_Show = true;
		// 	$scope.TambahCustomerData_Show = false;
		// } else if ($scope.modeCrud == 'edit') {
		// 	$scope.TambahCustomerDiscount_Customer_ClearFields();
		// 	$scope.TambahCustomerDiscount_Show = true;
		// 	$scope.TambahCustomerData_Show = false;
		// 	$scope.TambahCustomerDiscount_View_Show = true;
		// 	$scope.TambahCustomerDiscount_View_Disable = false;
		// } else {
		// 	console.log('masuk sini ga?')
		// 	$scope.TambahCustomerDiscount_Customer_ClearFields();
		// 	$scope.TambahCustomerDiscount_Show = true;
		// 	$scope.TambahCustomerData_Show = false;
		// 	$scope.TambahCustomerDiscount_View_Show = true;
		// 	$scope.TambahCustomerDiscount_View_Disable = false;
		// }
		//ini yang lama, lumayan buat jadi patokan=================


	}


	$scope.TambahCustomerDiscount_Customer_ClearFields = function(){

		delete $scope.TambahCustomerData_NamaPemilik;
		delete $scope.TambahCustomerData_NomorTelepon;
		delete $scope.TambahCustomerData_NomorKTP;
		delete $scope.TambahCustomerData_NomorNPWP;
		delete $scope.TambahCustomerData_Alamat;
		delete $scope.TambahCustomerData_Provinsi;
		delete $scope.TambahCustomerData_Kecamatan;
		delete $scope.TambahCustomerData_KotaKab;
		delete $scope.TambahCustomerData_Kelurahan;
		delete $scope.lmModelAddress.PostalCode;
		delete $scope.lmModelAddress.VillageId;
		delete $scope.lmModelAddress.DistrictId;
		delete $scope.lmModelAddress.CityRegencyId;
		delete $scope.lmModelAddress.ProvinceId;
		delete $scope.TambahCustomerData_BerlakuSampai;
		delete $scope.TambahCustomerData_TanggalLahir;
	}

	// $scope.tmpIdx = 0;
	$scope.TambahCustomerDiscountListEdit = function(row){
		$scope.modeCrud = 'edit';
		console.log("ROW--->",row);
		console.log('row.entity.BirthDate',row.entity.BirthDate);
		console.log('row.entity.ValidTo',row.entity.ValidTo);
		// var DateBirth = row.entity.BirthDate.substring(6, 10) + '-' + row.entity.BirthDate.substring(3,5)+ '-' + row.entity.BirthDate.substring(0,2);
		//var ValidTo = row.entity.ValidTo.substring(6, 10) + '-' + row.entity.ValidTo.substring(3,5)+ '-' + row.entity.ValidTo.substring(0,2);
		// console.log("DateBirth : ", DateBirth);
		$scope.lmModelAddress = angular.copy(row.entity);
		console.log("$scope.lmModelAddress11", $scope.lmModelAddress);

		$scope.TambahCustomerDiscount_CustomerId = row.entity.CustomerId;
		$scope.TambahCustomerData_NamaPemilik = row.entity.CustomerName;
		$scope.TambahCustomerData_NomorTelepon = row.entity.PhoneNumber;
		$scope.TambahCustomerData_TanggalLahir = row.entity.BirthDate;
		$scope.TambahCustomerData_NomorKTP = row.entity.IdNumber;
		$scope.TambahCustomerData_NomorNPWP = row.entity.Npwp;
		$scope.TambahCustomerData_Alamat = row.entity.Address;
		$scope.TambahCustomerData_Provinsi = row.entity.ProvinceName;
		$scope.TambahCustomerData_Kecamatan = row.entity.DistrictName;
		$scope.TambahCustomerData_KotaKab = row.entity.CityRegencyName;
		$scope.TambahCustomerData_Kelurahan = row.entity.VillageName;
		$scope.TambahCustomerData_KodePos = row.entity.PostalCode;
		$scope.TambahCustomerData_VillageId = row.entity.VillageId;
		$scope.TambahCustomerData_DistrictId = row.entity.DistrictId;
		$scope.TambahCustomerData_ProvinceId = row.entity.ProvinceId;
		$scope.TambahCustomerData_CityRegencyId = row.entity.CityRegencyId;
		$scope.TambahCustomerData_BerlakuSampai = row.entity.ValidTo;
		$scope.TambahCustomerData_idx =row.entity.idx


		$scope.TambahCustomerDiscount_Show = false;
		$scope.TambahCustomerData_Show = true;
	}

	$scope.TambahCustomerDiscountListDelete = function(row){
		var SelectedIndex = 0;
		console.log("row", row);
		$scope.TambahCustomerDiscount_CustomerList.some(function(obj, i){
			console.log("obj.CustomerId", obj.CustomerName);
			console.log("row.entity.CustomerId", row.entity.CustomerName);
			if(obj.CustomerName === row.entity.CustomerName)
				$scope.TambahCustomerDiscount_CustomerList.splice(i, 1);
				$scope.TambahCustomerDiscountList_UIGrid.data = $scope.TambahCustomerDiscount_CustomerList;
		});


	}
	///TambahCustomerDiscountList_UIGrid
	// $scope.TambahCustomerDiscountList_UIGrid = {
	// 	useCustomPagination: true,
	// 	useExternalPagination : true,
	// 	multiSelect : false,
	// 	enableFiltering: true,
	// 	enableColumnResizing: true,
	// 	columnDefs: $scope.TambahCustomerDiscount_ColumnDefs,
	// 	onRegisterApi: function(gridApi) {
	// 		$scope.GridApiTambahCustomerDiscountList = gridApi;
	// 		gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	// 			$scope.TambahCustomerDiscountList_UIGrid_Paging(pageNumber);
	// 		});
	// 	}
	// };

	// $scope.TambahCustomerDiscountList_UIGrid_Paging = function(pageNumber){
	// 	CustomerDiscountFactory.getDataMain(pageNumber, uiGridPageSize)
	// 	.then(
	// 		function(res)
	// 		{
	// 			console.log("Get data nomor pajak", res.data.Result);
	// 			$scope.TambahCustomerDiscountList_UIGrid.data = res.data.Result;
	// 			$scope.TambahCustomerDiscountList_UIGrid.totalItems = res.data.Total;
	// 			$scope.TambahCustomerDiscountList_UIGrid.paginationPageSize = uiGridPageSize;
	// 			$scope.TambahCustomerDiscountList_UIGrid.paginationPageSizes = [uiGridPageSize];

	// 		}

	//   );
	// }

	//==============================================================================================//
	//									Search Customer Begin										//
	//==============================================================================================//

	$scope.TambahCustomerDiscount_CustomerData = function(CustomerName){
		CustomerDiscountFactory.getDataCustomer(CustomerName)
		.then(
			function(res){
				$scope.ModalTambahCustomerDiscountSearchCustomer_UIGrid.data = res.data.Result;
				if (res.data.Result.length>0)
					console.log("sample",res.data.Result[0]);
			}
		);
	}

	$scope.TambahCustomerData_Cari_Clicked = function()
    {
		$scope.TambahCustomerDiscount_CustomerData($scope.TambahCustomerData_NamaPemilik);
        angular.element('#ModalTambahCustomerDiscountSearchCustomer').modal('show');
    }

	$scope.ModalTambahCustomerDiscountSearchCustomer_Batal = function(){
		angular.element('#ModalTambahCustomerDiscountSearchCustomer').modal('hide');
	}

	$scope.ModalTambahCustomerDiscountSearchCustomer_UIGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10,20],
		columnDefs: [
			{ name:'CustomerId', field:'CustomerId', visible:false},
			{ name:'Nama Customer', field:'CustomerName'},
			{ name:'BirthDate', field:'BirthDate', visible:false},
			{ name:'Address', field:'Address', visible:false},
			{ name:'PhoneNumber', field:'PhoneNumber', visible:false},
			{ name:'IdNumber', field:'IdNumber', visible:false},
			{ name:'Npwp', field:'Npwp', visible:false},
			{ name:'PostalCode', field:'PostalCode', visible:false},
			{ name:'VillageId', field:'VillageId', visible:false},
			{ name:'VillageName', field:'VillageName', visible:false},
			{ name:'CityRegencyId', field:'CityRegencyId', visible:false},
			{ name:'CityRegencyName', field:'CityRegencyName', visible:false},
			{ name:'DistrictId', field:'DistrictId', visible:false},
			{ name:'DistrictName', field:'DistrictName', visible:false},
			{ name:'ProvinceId', field:'ProvinceId', visible:false},
			{ name:'ProvinceName', field:'ProvinceName', visible:false},
			{ name:'isPersonal', field:'isPersonal', visible:false},
			{ name: 'Action', enableFiltering: false,
				cellTemplate:'<a ng-click="grid.appScope.SearchCustomerDiscountSearchCustomerPilihRow(row)">Pilih</a>'}
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        }
	};

	$scope.SearchCustomerDiscountSearchCustomerPilihRow = function(row){
		console.log("masuk sini");
		$scope.TambahCustomerDiscount_CustomerId = row.entity.CustomerId;
		$scope.TambahCustomerData_NamaPemilik = row.entity.CustomerName;
		$scope.TambahCustomerData_NomorTelepon = row.entity.PhoneNumber;
		$scope.TambahCustomerData_TanggalLahir = row.entity.BirthDate;
		$scope.TambahCustomerData_NomorKTP = row.entity.IdNumber;
		$scope.TambahCustomerData_NomorNPWP = row.entity.Npwp;
		$scope.TambahCustomerData_Alamat = row.entity.Address;
		$scope.TambahCustomerData_Provinsi = row.entity.ProvinceName;
		$scope.TambahCustomerData_Kecamatan = row.entity.DistrictName;
		$scope.TambahCustomerData_KotaKab = row.entity.CityRegencyName;
		$scope.TambahCustomerData_Kelurahan = row.entity.VillageName;
		$scope.TambahCustomerData_KodePos = row.entity.PostalCode;
		$scope.TambahCustomerData_VillageId = row.entity.VillageId;
		$scope.TambahCustomerData_DistrictId = row.entity.DistrictId;
		$scope.TambahCustomerData_ProvinceId = row.entity.ProvinceId;
		$scope.TambahCustomerData_CityRegencyId = row.entity.CityRegencyId;

		$scope.ModalTambahCustomerDiscountSearchCustomer_Batal();
	}
	//==============================================================================================//
	//										Search Customer End										//
	//==============================================================================================//
});
