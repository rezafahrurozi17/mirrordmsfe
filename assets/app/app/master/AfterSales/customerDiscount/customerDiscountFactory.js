angular.module('app')
	.factory('CustomerDiscountFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		//ini untuk data paging
		getDataMain : function(start, limit){
			var url = '/api/as/AfterSalesMasterCustomerDiscount/getDataMain/?Start=' + start + '&limit=' + limit + '&OutletId=' + user.OutletId;
			console.log("url Data Main : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataCustomer : function(customerName){
			var url = '/api/as/AfterSalesMasterCustomerDiscount/GetDataCustomer/?CustomerName=' + customerName + '&OutletId=' + user.OutletId;
			console.log("url Data Customer : ", url);
			var res=$http.get(url);
			return res;
		},
		
		//ini untuk ambil data satuan
		getDataGroupCustomerList : function(GroupCustomerId){
			var url = '/api/as/AfterSalesMasterCustomerDiscount/GetDataCustomerList/?Start=1&limit=100&OutletId=' + user.OutletId + '&GroupCustomerId=' + GroupCustomerId;
			console.log("url Data Group Customer List : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(inputData){
			var url = '/api/as/AfterSalesMasterCustomerDiscount/SubmitData/';
			// var param = JSON.stringify(inputData);
			var param = inputData;
			console.log("object input saveData", param);
			var res=$http.post(url, [param]);
			return res;
		},
		
		deleteData:function(id){
			var url = '/api/as/AfterSalesMasterCustomerDiscount/DeleteDataMain/?GroupCustomerId=' + id;
			console.log("url delete Data : ", url);
			var res=$http.delete(url);
			return res;
		},
		getMLocationProvince: function() {
                return $http.get('/api/sales/MLocationProvince');
            },
            getMLocationCityRegency: function(id) {
                return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
            },
            getMLocationKecamatan: function(id) {
                return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + id);
            },
            getMLocationKelurahan: function(id) {
                return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + id)
			},		
		}
});