var app = angular.module('app');
app.controller('TaskListBPController', function ($scope, $http, $filter, CurrentUser, TaskListBPFactory, $timeout, bsAlert, ngDialog, bsNotify) {
	var user = CurrentUser.user();
	
	$scope.uiGridPageSize = 1000;
	$scope.TaskListBPMain_Show = true;
	$scope.ExcelData = [];
	$scope.TmpSuccces = [];
	$scope.BPEditlistUOM = [];
	$scope.TaskListBP_ListCategory = [];
	$scope.TaskListBPEdit = {};
	$scope.TaskListBPMain = {};
	$scope.formApi = {};
	$scope.dataExcel = {};
	$scope.showUploadFileTaskBP = false;
	$scope.modeModalUploadExcelBP = 'new';
	$scope.settingButton = {save:{text:'Upload'}};
	$scope.viewButton = {save:{text:'Ok'}};
	$scope.showUploadedError = false;
	$scope.dataError = [];

	$scope.flagModel = true;
	$scope.flagSatuan = true;
	$scope.flagArea = true;

	$scope.isStatusVehicleId = true;
	$scope.$on("$viewContentLoaded",function(){
		$scope.getDataCombobox();
	})
	$scope.getData = function(){
		TaskListBPFactory.getDataVehicleModel().then(function(res){
			var tempData = [];
			angular.forEach(res.data.Result,function(value,key){
				// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
				tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
			});
			$scope.optionsTaskListBPMain_ModelKendaraan = tempData;
		});
		$scope.TaskListBPGrid_Paging(1);
	}
	$scope.onShowDetail = function(row, mode){
		console.log('UOOOOM',row);
		if(mode == 'edit' || mode == 'view'){

			$scope.TaskListBPEdit = row;
			$scope.TaskListBPEdit.TaskId = row.TaskListBPId;
			$scope.TaskListBPEdit.OutletId = row.OutletId;
			$scope.TaskListBPEdit.OutletCode = '';
			$scope.TaskListBPEdit.WMI = row.WMI;
			$scope.TaskListBPEdit.VDS = row.VDS;
			$scope.TaskListBPEdit.Model = row.VehicleModelId;
			$scope.TaskListBPEdit.OperationNo = row.OperationNo; 
			$scope.TaskListBPEdit.TipePekerjaan = row.ActivityTypeId; 
			$scope.TaskListBPEdit.TaskName = row.TaskName;
			$scope.TaskListBPEdit.OperationUOM = row.UOMId;
			$scope.TaskListBPEdit.ServiceRate = row.ServiceRate; 
			$scope.TaskListBPEdit.WarrantyRate = row.WarrantyRate;
			$scope.TaskListBPEdit.EstimasiBody = row.BodyEstimationMinute;
			$scope.TaskListBPEdit.EstimasiPutty = row.PuttyEstimationMinute;
			$scope.TaskListBPEdit.EstimasiSurfacer = row.SurfacerEstimationMinute;
			$scope.TaskListBPEdit.EstimasiSpraying = row.SprayingEstimationMinute;
			$scope.TaskListBPEdit.EstimasiPolishing = row.PolishingEstimationMinute;
			$scope.TaskListBPEdit.EstimasiReassembly = row.ReassemblyEstimationMinute; 
			$scope.TaskListBPEdit.EstimasiFI = row.FIEstimationMinute; 
			$scope.TaskListBPEdit.TotalEstimasi = row.TotalEstimationMinute; 
			$scope.TaskListBPEdit.TanggalAwal = row.ValidFrom; 
			$scope.TaskListBPEdit.TanggalAkhir = row.ValidThru;
			$scope.TaskListBPEdit.PointId = row.PointId;
			$scope.TaskListBPEdit.AreaId = row.AreaId;
			$scope.TaskListBPEdit.StatusVehicleId = row.StatusVehicleId;
				if($scope.TaskListBPEdit.StatusVehicleId == 2442){
					$scope.isStatusVehicleId = false;
				}else{
					$scope.isStatusVehicleId = true;
					// $scope.TaskListBPEdit.PointId = null;
				}
			console.log("statusvehiscle ==",$scope.TaskListBPEdit.StatusVehicleId)
			TaskListBPFactory.getVehicleTypeById(row.VehicleModelId).then(function(res){
				var tempData = [];
				angular.forEach(res.data.Result,function(value,key){
					// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
					tempData.push({name:value.Description,value:value.VehicleTypeId})
				});
				$scope.optionsTaskListBPMain_FullModel = tempData;
			});
			$scope.TaskListBPEdit.FullModel = row.VehicleTypeId;
			// setTimeout(function(){ }, 3000);		
	
		} else if (mode == 'new') {
			// kl mode new set uom default nya jadi panel
			setTimeout(function() { 
				$scope.TaskListBPEdit.OperationUOM = 2428;
			}, 20);

		}
		
			
		$scope.TaskListBPMain_Show = false;
		$scope.TaskListBPEdit_Show = true;

		console.log($scope.TaskListBPEdit.OperationUOM);
		
	}
	$scope.onValidateSave = function(data){
		var ValidFrom = '';
		var ValidTo = '';
		console.log("asu", data)
		if(data.TaskListBPId != null){
			TaskListBPFactory.CekTasklistBP(data.TaskName, data.TaskListBPId,data.Model,data.FullModel).then(function (res) {
				var resu = res.data
				console.log('res >>', resu);
				
				if (resu[0] == "false") {
					bsAlert.alert({
						// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						title: "Nama Pekerjaan " + data.TaskName + " sudah terdaftar",
						text: "",
						type: "warning",
						showCancelButton: false
					});
				}else{
					if(!angular.isUndefined(data.TanggalAwal))
						ValidFrom = $filter('date')(new Date(data.TanggalAwal.toLocaleString()), "yyyy-MM-dd");
					
					if(!angular.isUndefined(data.TanggalAkhir))
						ValidTo = $filter('date')(new Date(data.TanggalAkhir.toLocaleString()), "yyyy-MM-dd");
					
					if (data.StatusVehicleId == 2442){
						data.PointId = data.PointId
					}else if(data.StatusVehicleId != 2442){
						data.PointId = 0
					}
					var inputData=[{
							"TaskListBPId" 				: data.TaskId,
							"OutletId" 					: data.OutletId,//user.OutletId,
							"OutletCode" 				: data.OutletCode,
							"WMI" 						: data.WMI,
							"VDS" 						: data.VDS,
							// "VehicleTypeName"			: data.TypeName, 
							"VehicleTypeId"				: data.FullModel, 
							"VehicleModelId" 			: data.Model, 
							// "ModelName" 				: data.ModelName, 
							"OperationNo"				: data.OperationNo, 
							"ActivityTypeId" 			: data.TipePekerjaan, 
							"TaskName" 					: data.TaskName, 
							"UOMId" 					: data.OperationUOM, 
							"ServiceRate"				: data.ServiceRate, 
							"WarrantyRate"				: data.WarrantyRate, 
							"BodyEstimationMinute"		: data.EstimasiBody, 
							"PuttyEstimationMinute"		: data.EstimasiPutty, 
							"SurfacerEstimationMinute"	: data.EstimasiSurfacer, 
							"SprayingEstimationMinute"	: data.EstimasiSpraying, 
							"PolishingEstimationMinute"	: data.EstimasiPolishing, 
							"ReassemblyEstimationMinute": data.EstimasiReassembly, 
							"FIEstimationMinute"		: data.EstimasiFI, 
							"TotalEstimationMinute"		: data.TotalEstimasi, 
							"ValidFrom"					: data.TanggalAwal,
							"ValidThru"					: data.TanggalAkhir,
							"PointId"					: data.PointId,
							"AreaId"					: data.AreaId,
							"StatusVehicleId"			: data.StatusVehicleId,
							"Qty"						: 1,
							// "ValidFrom"					: ValidFrom,
							// "ValidThru"					: ValidTo,
							"UploadDate" 				: undefined, 
							"PreparedDate" 				: undefined
							
					}];
					
					TaskListBPFactory.create(inputData)
					.then(
						function(res){
							bsAlert.success("Data berhasil disimpan");
							$scope.TaskListBP_Edit_Batal_Clicked();
							$scope.TaskListBPGrid_Paging(1);
							$scope.formApi.setMode('grid');
						},
						function(err){
							bsAlert.error("Input data Harus benar");
							return false
						}
					);
				}
			});
		}else{
			data.TaskId = 0
			TaskListBPFactory.CekTasklistBP(data.TaskName, data.TaskId,data.Model,data.FullModel).then(function (res) {
				var resu = res.data
				console.log('res >>', resu);
				
				if (resu[0] == "false") {
					bsAlert.alert({
						// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						title: "Nama Pekerjaan " + data.TaskName + " sudah terdaftar",
						text: "",
						type: "warning",
						showCancelButton: false
					});
				}else{
					if(!angular.isUndefined(data.TanggalAwal))
						ValidFrom = $filter('date')(new Date(data.TanggalAwal.toLocaleString()), "yyyy-MM-dd");
					
					if(!angular.isUndefined(data.TanggalAkhir))
						ValidTo = $filter('date')(new Date(data.TanggalAkhir.toLocaleString()), "yyyy-MM-dd");
					
					if (data.StatusVehicleId == 2442){
						data.PointId = data.PointId
					}else if(data.StatusVehicleId != 2442){
						data.PointId = 0
					}
					var inputData=[{
							"TaskListBPId" 				: data.TaskId,
							"OutletId" 					: data.OutletId,//user.OutletId,
							"OutletCode" 				: data.OutletCode,
							"WMI" 						: data.WMI,
							"VDS" 						: data.VDS,
							// "VehicleTypeName"			: data.TypeName, 
							"VehicleTypeId"				: data.FullModel, 
							"VehicleModelId" 			: data.Model, 
							// "ModelName" 				: data.ModelName, 
							"OperationNo"				: data.OperationNo, 
							"ActivityTypeId" 			: data.TipePekerjaan, 
							"TaskName" 					: data.TaskName, 
							"UOMId" 					: data.OperationUOM, 
							"ServiceRate"				: data.ServiceRate, 
							"WarrantyRate"				: data.WarrantyRate, 
							"BodyEstimationMinute"		: data.EstimasiBody, 
							"PuttyEstimationMinute"		: data.EstimasiPutty, 
							"SurfacerEstimationMinute"	: data.EstimasiSurfacer, 
							"SprayingEstimationMinute"	: data.EstimasiSpraying, 
							"PolishingEstimationMinute"	: data.EstimasiPolishing, 
							"ReassemblyEstimationMinute": data.EstimasiReassembly, 
							"FIEstimationMinute"		: data.EstimasiFI, 
							"TotalEstimationMinute"		: data.TotalEstimasi, 
							"ValidFrom"					: data.TanggalAwal,
							"ValidThru"					: data.TanggalAkhir,
							"PointId"					: data.PointId,
							"AreaId"					: data.AreaId,
							"StatusVehicleId"			: data.StatusVehicleId,
							"Qty"						: 1,
							// "ValidFrom"					: ValidFrom,
							// "ValidThru"					: ValidTo,
							"UploadDate" 				: undefined, 
							"PreparedDate" 				: undefined
							
					}];
					
					TaskListBPFactory.create(inputData)
					.then(
						function(res){
							bsAlert.success("Data berhasil disimpan");
							$scope.TaskListBP_Edit_Batal_Clicked();
							$scope.TaskListBPGrid_Paging(1);
							$scope.formApi.setMode('grid');
						},
						function(err){
							bsAlert.error("Input data Harus benar");
							return false
						}
					);
				}
			});
		}

		

	}
	$scope.onBeforeNewMode = function(){

	}
	$scope.actDeleteTask = function(row){
		bsAlert.alert({
			title: "Apakah Anda yakin akan menghapus 1 pekerjaan ?",
			text: "",
			type: "question",
			showCancelButton: true
		},
			function() {
				TaskListBPFactory.DeleteDataBP(row.TaskListBPId)
				.then(
					function(res){				
						bsAlert.success("Data berhasil dihapus");	
						$scope.Grid_Paging(1);					
					}
				);		
			},
			function() {

			}
		)
	}
	$scope.actionButtonSettingsDetail = [
		{
			func: function(row, formScope) {
				// $scope.TaskListBPMain_Upload_Clicked()
				$scope.showUploadFileTaskBP = true;
				$('#UploadTaskListBP').val("");
			},
			title: 'Upload Dari Excel',
			icon: '',
		},
		{
			func: function(row, formScope) {
				$scope.TaskListBPMain_DownloadExcelTemplate();
			},
			title: 'Download Template',
			icon: '',
		}
	];
	$scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
        <a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-click="grid.appScope.$parent.actDeleteTask(row.entity)" uib-tooltip="Hapus" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-trash" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        </div>';
	$scope.getDataCombobox = function(){
		$scope.getDataVehicleModel();	
		$scope.optionsTaskListBPMain_TypeSearch = TaskListBPFactory.getDataTypeSearch();
	}
	$scope.uploadExcel = function(){
		console.log('Uploaded excel', $scope.ExcelData);
		if($scope.ExcelData.length > 0){
			$scope.ariavalue = 0;
			$scope.ariavaluecss = '0%';
			$scope.message = 'Mohon menunggu sedang memuat file...'
			ngDialog.openConfirm({
				template: 	'<div>\
							<p class="loading-message">{{message}}</p>\
							<div class="sk-three-bounce"><div id="loading-bar-spinner"><div class="spinner-icon"></div></div></div>\
							<div class="progress">\
								<div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="{{ariavalue}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ariavaluecss}}; background-color:#d53337;">{{ariavalue}}%</div>\
								</div>\
							</div>',
				plain: true,
				showClose: false,
				// controller: 'WOController',
				scope: $scope
			}).then(function(value) {}, function(value) {});
			$timeout(function() {
				$scope.ariavalue = 10;
				$scope.ariavaluecss = '10%';
				$timeout(function() {
					$scope.UploadTaskListBP_Upload_Clicked();
				}, 500); // WAIT 1 seconds
			}, 500); // WAIT 1 second
		}else{
			bsAlert.warning("Pililh file terlebih dahulu");
		}
		

	}
	$scope.cancelUploadExcel = function(){
		$scope.showUploadFileTaskBP = false;
		$scope.ExcelData = [];
	}
	$scope.TaskListBPMain_Tambah_Clicked = function(){
		$scope.TaskListBPMain_Show = false;
		$scope.TaskListBPEdit_Show = true;
	}

	$scope.getDataVehicleModel = function(){
		TaskListBPFactory.getDataVehicleModel().then(function(res){
			var tempData = [];
			angular.forEach(res.data.Result,function(value,key){
				// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
				tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
			});
			$scope.optionsTaskListBPMain_ModelKendaraan = tempData;
		});
	};

	$scope.onSelectModel = function(selected){
		// getVehicleTypeById
		// TaskListBPFactory.getFullModelFromModel(selected.value).then(function(res){
		TaskListBPFactory.getVehicleTypeById(selected.value).then(function(res){
			var tempData = [];
			angular.forEach(res.data.Result,function(value,key){
				// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
				tempData.push({name:value.Description,value:value.VehicleTypeId})
			});
			$scope.optionsTaskListBPMain_FullModel = tempData;
		});
	}

	$scope.calculateTotalTime = function(){
		var value = 0;
		var parameter = [
			"EstimasiBody",
			"EstimasiPutty",
			"EstimasiSurfacer",
			"EstimasiSpraying",
			"EstimasiPolishing",
			"EstimasiReassembly",
			"EstimasiFI"
		]

		for(var i in parameter){
			console.log("$scope[parameter[i]] ====>",$scope.TaskListBPEdit[parameter[i]],parameter[i]);
			value = value + ($scope.TaskListBPEdit[parameter[i]] || 0);
		}

		$scope.TaskListBPEdit.TotalEstimasi = value;
	}
	$scope.grid = {
		// paginationPageSizes: null,
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination : true,
		enableSorting: true,
		multiSelect: false,
		enableFiltering: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: 'TaskListBPId', field: 'TaskListBPId', visible: false },
			{ name: 'OutletId', field: 'OutletId', visible:false },
			{ name: 'WMI', width: '10%', field: 'WMI'},
			{ name: 'VDS', width: '10%', field: 'VDS'},
			{ name: 'Model Name', width: '10%', field: 'ModelName'},
			{ name: 'Full Model', width: '10%', field: 'VehicleTypeName'},
			{ name: 'Operation No', width: '10%', field: 'OperationNo'},
			{ name: 'Nama Pekerjaan', width: '10%', field: 'TaskName'},
			{ name: 'Tipe Pekerjaan', width: '10%', field: 'ActivityTypeName'},
			{ name: 'ActivityTypeId', width: '10%', field: 'ActivityTypeId', visible: false},
			{ name: 'Jumlah', width: '10%', field: 'Qty'},
			{ name: 'Satuan', width: '10%', field: 'UOM'},
			{ name: 'UOMId', field: 'UOMId', visible:false},
			{ name: 'Service Rate', width: '10%', field: 'ServiceRate'},
			{ name: 'Standard Actual Rate', width: '10%', field: 'StandardActualRate'},
			{ name: 'Waranty Rate', width: '10%', field: 'WarrantyRate'},
			{ name: 'Body Time In', width: '10%', field: 'BodyEstimationMinute'},
			{ name: 'Putty Time In', width: '10%', field: 'PuttyEstimationMinute'},
			{ name: 'Surfacer Time In', width: '10%', field: 'SurfacerEstimationMinute'},
			{ name: 'Painting Time In', width: '10%', field: 'SprayingEstimationMinute'},
			{ name: 'Polishing Time In', width: '10%', field: 'PolishingEstimationMinute'},
			{ name: 'Final Inspection Time In', width: '10%', field: 'FIEstimationMinute'},
			{ name: 'Total Time In', width: '10%', field: 'TotalEstimationMinute'},
			{ name: 'Upload Date', width: '10%', field: 'UploadDate', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Valid From', width: '10%', field: 'ValidFrom', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Valid To', width: '10%', field: 'ValidThru', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Prepared By', width: '10%', field: 'PreparedBy'},
			{ name: 'Prepared Date', width: '10%', field: 'PreparedDate', cellFilter:'date:\"dd-MM-yyyy\"'},
			// { name:"Action", width:100, 
			// 				cellTemplate:'<a ng-click="grid.appScope.MainTaskListBP_Edit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainTaskListBP_Delete(row)">Delete</a>'}
		],
		// onRegisterApi: function(gridApi) {
		// 	$scope.gridApiTasklist = gridApi;
		// 	gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
		// 		console.log('nah ini pagenumber dan page size', pageNumber)
		// 		console.log('nah ini pagenumber dan page size', pageSize)
		// 		// $scope.TaskListBPGrid_Paging(pageNumber, pageSize);
		// 	});
		// }
	};
	$timeout(function(){
		console.log('$scope.formGridApi ===>',$scope.formGridApi);
		$scope.Grid_Paging(1);
		$scope.formGridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			console.log('nah ini pagenumber dan page size', pageNumber)
			console.log('nah ini pagenumber dan page size', pageSize)
			$scope.Grid_Paging(pageNumber, pageSize);
		});
	},1000)
	$scope.Grid_Paging = function(pageNumber, pageSize){
		var ModelKendaraan = '';
		var OperationDescription = '';
		var TipePekerjaan = 0;
		if (pageSize == null || pageSize == undefined){
			$scope.uiGridPageSize = 10;
		} else {
			$scope.uiGridPageSize = pageSize;
		}
		
		if (!angular.isUndefined($scope.TaskListBPMain.ModelKendaraan))
			ModelKendaraan = $scope.TaskListBPMain.ModelKendaraan;
		
		if(!angular.isUndefined($scope.TaskListBPMain.OperationDescription))
			OperationDescription = $scope.TaskListBPMain.OperationDescription;
		
		if(!angular.isUndefined($scope.TaskListBPMain.ActivityType))
			TipePekerjaan = $scope.TaskListBPMain.ActivityType;
			// console.log("TipePekerjaan",TipePekerjaan);
			// console.log("TipePekerjaan",$scope.TaskListBPMain.ActivityType);
		
		console.log("$scope.TaskListBPMain.FreeSearch",$scope.TaskListBPMain.ModelKendaraan);
		console.log("$scope.TaskListBPMain.TypeSearch",$scope.TaskListBPMain.OperationDescription);
		console.log("$scope.TaskListBPMain.TypeSearch",$scope.TaskListBPMain.ActivityType);
		TaskListBPFactory.getTaskListBPData(
			pageNumber,
			$scope.uiGridPageSize,
			ModelKendaraan,
			OperationDescription,
			TipePekerjaan,
			$scope.TaskListBPMain.FreeSearch,
			$scope.TaskListBPMain.TypeSearch
		)
		  .then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				$scope.grid.data = res.data.Result;
				$scope.grid.totalItems =  res.data.Result[0].TotalData;
			}
			
		);
	}

	// $scope.formGridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	// 			console.log('nah ini pagenumber dan page size', pageNumber)
	// 			console.log('nah ini pagenumber dan page size', pageSize)
	// 			// $scope.TaskListBPGrid_Paging(pageNumber, pageSize);
	// 		});
	
	$scope.TaskListBPGrid = {
		// paginationPageSizes: null,
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination : true,
		enableSorting: true,
		multiSelect: false,
		enableFiltering: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: 'TaskListBPId', field: 'TaskListBPId', visible: false },
			{ name: 'OutletId', field: 'OutletId', visible:false },
			{ name: 'WMI', width: '10%', field: 'WMI'},
			{ name: 'VDS', width: '10%', field: 'VDS'},
			{ name: 'Model Name', width: '10%', field: 'ModelName'},
			{ name: 'Full Model', width: '10%', field: 'VehicleTypeName'},
			{ name: 'Operation No', width: '10%', field: 'OperationNo'},
			{ name: 'Nama Pekerjaan', width: '10%', field: 'TaskName'},
			{ name: 'Tipe Pekerjaan', width: '10%', field: 'ActivityTypeName'},
			{ name: 'ActivityTypeId', width: '10%', field: 'ActivityTypeId', visible: false},
			{ name: 'Jumlah', width: '10%', field: 'Qty'},
			{ name: 'Satuan', width: '10%', field: 'UOM'},
			{ name: 'UOMId', field: 'UOMId', visible:false},
			{ name: 'Service Rate', width: '10%', field: 'ServiceRate'},
			{ name: 'Standard Actual Rate', width: '10%', field: 'StandardActualRate'},
			{ name: 'Waranty Rate', width: '10%', field: 'WarrantyRate'},
			{ name: 'Body Time In', width: '10%', field: 'BodyEstimationMinute'},
			{ name: 'Putty Time In', width: '10%', field: 'PuttyEstimationMinute'},
			{ name: 'Surfacer Time In', width: '10%', field: 'SurfacerEstimationMinute'},
			{ name: 'Painting Time In', width: '10%', field: 'SprayingEstimationMinute'},
			{ name: 'Polishing Time In', width: '10%', field: 'PolishingEstimationMinute'},
			{ name: 'Final Inspection Time In', width: '10%', field: 'FIEstimationMinute'},
			{ name: 'Total Time In', width: '10%', field: 'TotalEstimationMinute'},
			{ name: 'Upload Date', width: '10%', field: 'UploadDate', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Valid From', width: '10%', field: 'ValidFrom', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Valid To', width: '10%', field: 'ValidThru', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name: 'Prepared By', width: '10%', field: 'PreparedBy'},
			{ name: 'Prepared Date', width: '10%', field: 'PreparedDate', cellFilter:'date:\"dd-MM-yyyy\"'},
			{ name:"Action", width:100, 
							cellTemplate:'<a ng-click="grid.appScope.MainTaskListBP_Edit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainTaskListBP_Delete(row)">Delete</a>'}
		],
		onRegisterApi: function(gridApi) {
			$scope.GridAPITaskListGRGrid = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log('nah ini pagenumber dan page size', pageNumber)
				console.log('nah ini pagenumber dan page size', pageSize)
				$scope.TaskListBPGrid_Paging(pageNumber, pageSize);
			});
		}
	};
	
	$scope.TaskListBPGrid_Paging = function(pageNumber, pageSize){
		var ModelKendaraan = '';
		var OperationDescription = '';
		var TipePekerjaan = 0;
		if (pageSize == null || pageSize == undefined){
			$scope.uiGridPageSize = 10;
		} else {
			$scope.uiGridPageSize = pageSize;
		}
		
		if (!angular.isUndefined($scope.TaskListBPMain.ModelKendaraan))
			ModelKendaraan = $scope.TaskListBPMain.ModelKendaraan;
		
		if(!angular.isUndefined($scope.TaskListBPMain.OperationDescription))
			OperationDescription = $scope.TaskListBPMain.OperationDescription;
		
		if(!angular.isUndefined($scope.TaskListBPMain.ActivityType))
			TipePekerjaan = $scope.TaskListBPMain.ActivityType;
			// console.log("TipePekerjaan",TipePekerjaan);
			// console.log("TipePekerjaan",$scope.TaskListBPMain_ActivityType);
		
		console.log("$scope.TaskListBPMain_FreeSearch", $scope.TaskListBPMain.ModelKendaraan);
		console.log("$scope.TaskListBPMain_TypeSearch",$scope.TaskListBPMain.ActivityType);
		TaskListBPFactory.getTaskListBPData(
			pageNumber,
			$scope.uiGridPageSize,
			ModelKendaraan,
			OperationDescription,
			TipePekerjaan,
			$scope.TaskListBPMain.FreeSearch,
			$scope.TaskListBPMain.TypeSearch
		)
		  .then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				// $scope.TaskListBPGrid.data = res.data.Result;
				// $scope.TaskListBPGrid.totalItems =  res.data.Result[0].TotalData;
				$scope.grid.data = res.data.Result;
				$scope.grid.totalItems =  res.data.Result[0].TotalData;
				// $scope.TaskListBPGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.TaskListBPGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			
		);
	}
	
	// $scope.MainTaskListBP_Edit = function(row){
	// 	console.log('UOOOOM',row.entity.VehicleTypeId);
	// 	$scope.TaskListBPEdit_TaskId = row.entity.TaskListBPId;
	// 	$scope.TaskListBPEdit_OutletId = row.entity.OutletId;
	// 	$scope.TaskListBPEdit_OutletCode = '';
	// 	$scope.TaskListBPEdit_WMI = row.entity.WMI;
	// 	$scope.TaskListBPEdit_VDS = row.entity.VDS;
	// 	$scope.TaskListBPEdit_Model = row.entity.VehicleModelId;
		
	// 	$scope.TaskListBPEdit_OperationNo = row.entity.OperationNo; 
	// 	$scope.TaskListBPEdit_TipePekerjaan = row.entity.ActivityTypeId; 
	// 	$scope.TaskListBPEdit_TaskName = row.entity.TaskName;
	// 	$scope.TaskListBPEdit_OperationUOM = row.entity.UOMId;
	// 	$scope.TaskListBPEdit_ServiceRate = row.entity.ServiceRate; 
	// 	$scope.TaskListBPEdit_WarrantyRate = row.entity.WarrantyRate;
	// 	$scope.TaskListBPEdit_EstimasiBody = row.entity.BodyEstimationMinute;
	// 	$scope.TaskListBPEdit_EstimasiPutty = row.entity.PuttyEstimationMinute;
	// 	$scope.TaskListBPEdit_EstimasiSurfacer = row.entity.SurfacerEstimationMinute;
	// 	$scope.TaskListBPEdit_EstimasiSpraying = row.entity.SprayingEstimationMinute;
	// 	$scope.TaskListBPEdit_EstimasiPolishing = row.entity.PolishingEstimationMinute;
	// 	$scope.TaskListBPEdit_EstimasiReassembly = row.entity.ReassemblyEstimationMinute; 
	// 	$scope.TaskListBPEdit_EstimasiFI = row.entity.FIEstimationMinute; 
	// 	$scope.TaskListBPEdit_TotalEstimasi = row.entity.TotalEstimationMinute; 
	// 	$scope.TaskListBPEdit_TanggalAwal = row.entity.ValidFrom; 
	// 	$scope.TaskListBPEdit_TanggalAkhir = row.entity.ValidThru;
	// 	$scope.TaskListBPEdit_PointId = row.entity.PointId;
	// 	$scope.TaskListBPEdit_AreaId = row.entity.AreaId;
	// 	$scope.TaskListBPEdit_StatusVehicleId = row.entity.StatusVehicleId;
	// 	TaskListBPFactory.getFullModelFromModel(row.entity.VehicleModelId).then(function(res){
	// 		var tempData = [];
	// 		angular.forEach(res.data.Result,function(value,key){
	// 			// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
	// 			tempData.push({name:value.VehicleTypeName,value:value.VehicleTypeId})
	// 		});
	// 		$scope.optionsTaskListBPMain_FullModel = tempData;
	// 	});
	// 	$scope.TaskListBPEdit_FullModel = row.entity.VehicleTypeId;
	// 	// setTimeout(function(){ }, 3000);		
		
	// 	$scope.TaskListBPMain_Show = false;
	// 	$scope.TaskListBPEdit_Show = true;

	// 	console.log($scope.TaskListBPEdit_OperationUOM);
	// }
	
	$scope.MainTaskListBP_Delete = function(row){
		TaskListBPFactory.DeleteDataBP(row.entity.TaskListBPId)
		.then(
			function(res){				
				alert("Data berhasil dihapus");	
				$scope.TaskListBPGrid_Paging(1);					
			}
		);
	}
	
	$scope.TaskListBPEdit_SetActivityTypeCode = function(){
		TaskListBPFactory.getDataCategory()
		.then(
			function(res){
				
				var eachCategory = {};
				angular.forEach(res.data.Result, function(value, key){
					// eachCategory = {"name":value.Name, "value":value.MasterId};
					eachCategory = {"name":value.ActivityTypeName, "value":value.ActivityTypeId};
					$scope.TaskListBP_ListCategory.push(eachCategory);
				});
				 $scope.optionsTaskListBPEdit_TipePekerjaan = $scope.TaskListBP_ListCategory;
				 $scope.optionsTaskListBPMain_ActivityType = $scope.TaskListBP_ListCategory;
				 console.log("$scope.optionsTaskListBPMain_ActivityType",$scope.optionsTaskListBPMain_ActivityType);
				 console.log("$scope.optionsTaskListBPEdit_TipePekerjaan",$scope.optionsTaskListBPEdit_TipePekerjaan);
			}
		);
	}
	
	$scope.TaskListBPEdit_SetUOM = function(){
		TaskListBPFactory.getDataUOM()
		.then(
			function(res){
				
				var eachUOM = {};
				angular.forEach(res.data.Result, function(value, key){
					eachUOM = {"name":value.UOMName, "value":value.UOMId};
					$scope.BPEditlistUOM.push(eachUOM);
				});
				 $scope.optionsTaskListBPEdit_OperationUOM = $scope.BPEditlistUOM;
			}
		);
	}

	$scope.TaskListBPEdit_SetPointId = function(){
		TaskListBPFactory.getMasterCategoryId()
		.then(
			function(res){
				
				// var eachPointId = {};
				// angular.forEach(res.data.Result, function(value, key){
				// 	eachPointId = {"name":value.Name, "value":value.MasterId};
				// 	$scope.BPEditlistPointId.push(eachPointId);
				// });
				//  $scope.optionsTaskListBPEdit_OperationPointId = $scope.BPEditlistPointId;
					$scope.optionsTaskListBPEdit_OperationPointId = res.data.Result;
				
			}
		);
	} 
	$scope.cekMetodePerbaikan = function(item){
		if(item.MasterId == 2442){
			$scope.isStatusVehicleId = false;
		}else{
			$scope.isStatusVehicleId = true;
			$scope.TaskListBPEdit.PointId = null;
		}
		
	}
	$scope.TaskListBPEdit_SetStatusVehicleId = function(){
		TaskListBPFactory.getMasterCategoryId1()
		.then(
			function(res){
				
				// var eachPointId = {};
				// angular.forEach(res.data.Result, function(value, key){
				// 	eachPointId = {"name":value.Name, "value":value.MasterId};
				// 	$scope.BPEditlistPointId.push(eachPointId);
				// });
				//  $scope.optionsTaskListBPEdit_OperationPointId = $scope.BPEditlistPointId;
				 $scope.optionsTaskListBPEdit_OperationStatusVehicleId = res.data.Result;
			});
			
	}


	$scope.TaskListBPEdit_SetAreaId = function(){
		// TaskListBPFactory.getMasterCategoryId1()
		// .then(
		// 	function(res){
				
				// var eachPointId = {};
				// angular.forEach(res.data.Result, function(value, key){
				// 	eachPointId = {"name":value.Name, "value":value.MasterId};
				// 	$scope.BPEditlistPointId.push(eachPointId);
				// });
				//  $scope.optionsTaskListBPEdit_OperationPointId = $scope.BPEditlistPointId;
				var AreaId = [];
				for (var i=1; i<49; i++){
					AreaId.push(
						{id:i,Name:i}
					)
				}
				$scope.optionsTaskListBPEdit_OperationAreaId = AreaId;
				console.log ("lala",$scope.optionsTaskListBPEdit_OperationAreaId);
			}
	// 	);
	// }

	
	// $scope.TaskListBPGrid_Paging(1);
	$scope.TaskListBPEdit_SetUOM();
	$scope.TaskListBPEdit_SetPointId();
	$scope.TaskListBPEdit_SetStatusVehicleId(); 
	$scope.TaskListBPEdit_SetAreaId();
	$scope.TaskListBPEdit_SetActivityTypeCode();
	
	$scope.TaskListBPMain_Cari_Clicked = function(){
		$scope.TaskListBPGrid_Paging(1);
	}
	
	function createHeaderStyle(obj){
        var res = [];
        for(var i=0; i<2; i++){
            var newObj = {};
            for(var key in obj){
                newObj[key] = '';
            }
            res.push(newObj);
        }
        return res;
    }
	

	$scope.TaskListBPMain_DownloadExcelTemplate = function(){
		var excelData = [];
		var fileName = "Task_list_BP_Template";
		
		var excelSheet1 = {"Dealer_Code":"", "OutletId":"", "WMI":"", "VDS":"", "Full_Model_Code": "", "Model_Kendaraan":"", "Service_Rate":"", 
							"Warranty_Rate":"","Upload_Date":"", "Valid_From":"","Valid_To":"", "Prepared_By":"", "Prepared_Date":""};
		var excelSheet2 = { "":"","Operation_No":"", "Warranty_Rate":"", "Valid_From":"", "Valid_To":"", "UOM":"", "Model_Kendaraan":"", "Full_Model": "", 
							"Nama_Pekerjaan":"", "Harga_Jasa":"", "No_Area":"","Metode_Perbaikan":"","Tingkat_Kerusakan":"", 
							"Body_Time":"", "Putty_Time":"", "Surfacer_Time":"", "Spraying_Time":"", "Polishing_Time":"", "Reassembly_Time":"", "Final_Inspection_Time":"", "Total_Time":"" };
		var excelSheetSeparator = { "":"Format","Operation_No":"Number", "Warranty_Rate":"Jam", "Valid_From*":"yyyy/mm/dd", "Valid_To*":"yyyy/mm/dd", "UOM*":"List Satuan", "Model_Kendaraan*":"List Model", 
							"Nama_Pekerjaan*":"Text", "Harga_Jasa*":"Rupiah Tanpa Titik (Include PPN)", "No_Area*":"Area ID Gambar","Metode_Perbaikan*":"List Metode Perbaikan","Tingkat_Kerusakan":"List Tingkat Kerusakan", 
							"Body_Time*":"Waktu Proses Body", "Putty_Time*":"Waktu Proses Putty", "Surfacer_Time*":"Waktu Proses Surfacer", "Spraying_Time*":"Waktu Prosves Spraying", "Polishing_Time*":"Waktu Proses Polishing",
							"Reassembly_Time*":"Waktu Proses Reassembly", "Final_Inspection_Time*":"Waktu Proses FI", "Total_Time*":"Total Waktu Proses Perbaikan" };
		var excelSheet4 = { "":"contoh","Operation_No":"123456", "Warranty_Rate":"3", "Valid_From":"2019/12/12", "Valid_To":"2025/12/12", "UOM":"Panel", "Model_Kendaraan":"Avanza", 
							"Nama_Pekerjaan":"Bumper Belakang - REPAIR A", "Harga_Jasa":"704000", "No_Area":"46","Metode_Perbaikan":"Repair","Tingkat_Kerusakan":"A", 
							"Body_Time":"60", "Putty_Time":"60", "Surfacer_Time":"75", "Spraying_Time":"60", "Polishing_Time":"30", "Reassembly_Time":"30", "Final_Inspection_Time":"7", "Total_Time":"322" };
		var excelSheet5 = { "":"contoh","Operation_No":"NULL", "Warranty_Rate":"NULL", "Valid_From":"2019/12/12", "Valid_To":"2025/12/12", "UOM":"Panel", "Model_Kendaraan":"Avanza", 
							"Nama_Pekerjaan":"Bumper Belakang - Replace", "Harga_Jasa":"704000", "No_Area":"46","Metode_Perbaikan":"Replace","Tingkat_Kerusakan":"", 
							"Body_Time":"90", "Putty_Time":"90", "Surfacer_Time":"105", "Spraying_Time":"60", "Polishing_Time":"30", "Reassembly_Time":"30", "Final_Inspection_Time":"7", "Total_Time":"412" };
		var excelSheet6 = { "":"contoh","Operation_No":"NULL", "Warranty_Rate":"NULL", "Valid_From":"2019/12/12", "Valid_To":"2025/12/12", "UOM":"Panel", "Model_Kendaraan":"Avanza", 
							"Nama_Pekerjaan":"Bumper Belakang - Remove & Reinstall", "Harga_Jasa":"704000", "No_Area":"46","Metode_Perbaikan":"Remove & Reinstall","Tingkat_Kerusakan":"", 
							"Body_Time":"15", "Putty_Time":"15", "Surfacer_Time":"55", "Spraying_Time":"60", "Polishing_Time":"30", "Reassembly_Time":"30", "Final_Inspection_Time":"7", "Total_Time":"212" };
		var excelSheet7 = { "":"contoh","Operation_No":"NULL", "Warranty_Rate":"NULL", "Valid_From":"2019/12/12", "Valid_To":"2025/12/12", "UOM":"Panel", "Model_Kendaraan":"Avanza", 
							"Nama_Pekerjaan":"Bumper Belakang - REPLACE", "Harga_Jasa":"682000", "No_Area":"46","Metode_Perbaikan":"Repair","Tingkat_Kerusakan":"B", 
							"Body_Time":"20", "Putty_Time":"0", "Surfacer_Time":"0", "Spraying_Time":"60", "Polishing_Time":"30", "Reassembly_Time":"30", "Final_Inspection_Time":"7", "Total_Time":"222" };
							"Mohon tidak menghapus dan mengubah data baris di atas dan silahkan melakukan pengisian takslist mulai dari baris di bawah ini"
		var excelSheet8 = { "":"-","Operation_No":"-", "Warranty_Rate":"-", "Valid_From":"Mohon", "Valid_To":"Tidak", "UOM":"Menghapus", "Model_Kendaraan":"dan", 
							"Nama_Pekerjaan":"Mengubah Data", "Harga_Jasa":"Baris", "No_Area":"di Atas","Metode_Perbaikan":"dan","Tingkat_Kerusakan":"Silahkan", 
							"Body_Time":"Melakukan", "Putty_Time":"Pengisian", "Surfacer_Time":"Tasklist", "Spraying_Time":"Mulai", "Polishing_Time":"dari", "Reassembly_Time":"Baris", "Final_Inspection_Time":"di Bawah ini", "Total_Time":"-" };							
		var excelSheet10 = {"Dealer_Code":"", "OutletId":"", "WMI":"", "VDS":"", "Operation_No":"","Material_No":"", "Qty":"", "Satuan":"", "Full_Model_Code": "", 
							"Model_Kendaraan":"", "Upload_Date":"", "Valid_From":"", "Valid_To":"", "Prepared_By":"", "Prepared_Date":""};
		
		var excelListMPerbaikan = {"":"Metode_Perbaikan"," ":"Repair"};
		var excelListMPerbaikan1 = {"":"Metode_Perbaikan"," ":"Replace"};
		var excelListMPerbaikan2 = {"":"Metode_Perbaikan"," ":"Remove & Reinstall"};

		var excelListKTkerusakan = {"":"Hanya Di Isi Bila Metode Perbaikan Repair", };
		var excelListKTkerusakan1 = {"":"Tingkat_Kerusakan"," ":"A"};
		var excelListKTkerusakan2 = {"":"Tingkat_Kerusakan"," ":"B"};
		var excelListKTkerusakan3 = {"":"Tingkat_Kerusakan"," ":"C"};
		// XLSXInterface.insert_image('AE2', 'images/gambarWAC.png')
		var excelSheet = {}					
		var excelData = [];
		var excelPerbaikan = [];
		var excelKerusakan = [];
		var excelTypeKendaraan = [];
		var excelUom = [];
		var finaldata = [];
		//excelData.push(excelSheet1);
		// excelData.push(excelSheet2);
		excelData.push(excelSheetSeparator);
		excelData.push(excelSheet4);
		excelData.push(excelSheet5);
		excelData.push(excelSheet6);
		excelData.push(excelSheet7);
		excelData.push(excelSheet8);
		//================ List Metode Perbaikan ==================
		excelPerbaikan.push(excelListMPerbaikan)
		excelPerbaikan.push(excelListMPerbaikan1)
		excelPerbaikan.push(excelListMPerbaikan2)

		//================ List Tingkat Kerusakan ==================
		excelKerusakan.push(excelListKTkerusakan);
		excelKerusakan.push(excelListKTkerusakan1);
		excelKerusakan.push(excelListKTkerusakan2);
		excelKerusakan.push(excelListKTkerusakan3);

		//============== List Model & Tipe ============================
		var excelType = {"VehicleModelName":"", 'KatashikiCode':""};
		var tmpDataModelId = [];
		var tmpDataUom = [];
		var tmpArrayTypeCar = [];
		
		excelTypeKendaraan.push(excelType);

		finaldata.push(excelData)
		finaldata.push(excelPerbaikan)
		finaldata.push(excelKerusakan)
		finaldata.push(excelUom)
		
		// finaldata.push(excelTypeKendaraan)
		// =====================
		var excelSatuan = {"UOM":""};
		excelUom.push(excelSatuan);
		for(var n=0; n< $scope.optionsTaskListBPEdit_OperationUOM.length;n++){
			console.log("uom ", $scope.optionsTaskListBPEdit_OperationUOM[n].name)
			
			var excelsatuan1 = {"UOM":$scope.optionsTaskListBPEdit_OperationUOM[n].name};
			// for(var cc == name1){

		// }	
			excelUom.push(excelsatuan1);	
		}

		for(var i=0; i< $scope.optionsTaskListBPMain_ModelKendaraan.length;i++){
			console.log("bababa ", $scope.optionsTaskListBPMain_ModelKendaraan[i])
			
			// var excelType1 = {"VehicleModelName":$scope.optionsTaskListBPMain_ModelKendaraan[i].name};
			// for(var cc == name1){

		// }	
			tmpDataModelId.push({
				nama:$scope.optionsTaskListBPMain_ModelKendaraan[i].name,
				Id:$scope.optionsTaskListBPMain_ModelKendaraan[i].value,
			});
			// excelTypeKendaraan.push(excelType1);	
		}
		var findFullModel = function(data, x){
			if(x > (tmpDataModelId.length-1)){
				return
			}
			TaskListBPFactory.getFullModelFromModel(data[x].Id).then(function(res){
				tmpArrayTypeCar[data[x].nama] = [];
				var jsonData = [];
				angular.forEach(res.data.Result,function(value,key){
					console.log('tmpDataModelId[x] ====>', data[x]);
					if(value.VehicleTypeName != null){
						jsonData.push({name:value.VehicleTypeName.split(' ')[0],value:value.VehicleTypeId})
					}else{
						jsonData.push({name:"-",value:value.VehicleTypeId})
					}
					
				});
				tmpArrayTypeCar[data[x].nama] = jsonData;
				// if(count == (data.length )){
				// 	$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
				// }

				if(x < (tmpDataModelId.length-1)){
					findFullModel(data, x + 1);
					
				}else if((tmpDataModelId.length - 1) == x){
					$scope.changeFormatJSONDownload(excelTypeKendaraan, tmpArrayTypeCar, finaldata);
				}
			});
		}
		findFullModel(tmpDataModelId, 0)
		console.log("final data", finaldata);
	}
	
	$scope.changeFormatJSONDownload = function(data, dataVehicle, allData){
		var finalCarTypeModel = [];
		var fileName = "Task_list_BP_Template";
		var objKeys = Object.keys(dataVehicle);
		for(var z = 0; z < objKeys.length; z++){
			// for(var zz = 0; zz < dataVehicle[objKeys[z]].length; zz++ ){
				finalCarTypeModel.push({
					VehicleModelName:objKeys[z],
					// KatashikiCode:dataVehicle[objKeys[z]][zz].name
				})

			// }
		}
		allData.push(finalCarTypeModel);
		console.log('===========>>', finalCarTypeModel);
		XLSXInterface.writeToXLSX(allData, fileName, [fileName, "List Metode Perbaikan", "List Tingkat Kerusakan", "Satuan", "List Model"]);
		// =========
		var link = document.createElement('a');
				link.href = '/media/TemplateMapping_MasterTasklist_BP.pdf';
				link.download="TemplateMapping_MasterTasklist_BP.pdf";
				link.click();
	}

	$scope.TaskListBPMain_Download_Clicked = function(){
		$scope.TaskListBPMain_DownloadExcelTemplate();
		
	}
	$scope.area = function(){
		var tmpArray = []
		for(var i=1; i<49; i++){
			tmpArray.push(
				{Id:i, Name:i}
			)
		}
		$scope.apa = tmpArray

	}
	
	$scope.TaskListBPMain_Upload_Clicked = function(){
		angular.element('#ModalUploadTaskListBP').modal('show');
		$('#UploadTaskListBP').val("");
	}

	//===================== type file upload ========================
	//===============================================================
	
	$scope.loadXLSTaskListBP = function(ExcelFile){
		var ExcelItem = [];
		var exTmp = [];
		// var myEl = $('UploadTaskListBP').val();
		$timeout(function(){
			// console.log("ExcelFile : ",myEl, $('UploadTaskListBP').find());
			// console.log("ExcelFile : ", angular.element( document.querySelector( '#UploadTaskListBP' ) ), ExcelFile);
			var myEl = angular.element( document.querySelector( '#UploadTaskListBP' ) ); //ambil elemen dari dokumen yang di-upload 
			console.log("element dari upload =>>>", myEl)
			if(myEl[0].files.length == 0){
				myEl = $(document.activeElement);
			}
			if(myEl[0].files[0] == 0 || myEl[0].files[0] == undefined){
				return false;
			}
			$scope.ariavalue = 0;
			$scope.ariavaluecss = '0%';

			
			var name = myEl[0].files[0].name;
			var lastDot = name.lastIndexOf('.');
			var ext = name.substring(lastDot + 1);
			console.log("ayaayawae ==>",ext);

			if(ext != "xlsx"){
				bsAlert.warning("Format File Anda Salah!", "Harus Menggunakan File Excel.XLSX")
				ngDialog.closeAll();
				return false;
			}

			ngDialog.openConfirm({
				template: 	'<div>\
							<p class="loading-message">Mohon menunggu sedang memuat file...</p>\
							<div class="sk-three-bounce"><div id="loading-bar-spinner"><div class="spinner-icon"></div></div></div>\
							<div class="progress">\
								<div class="progress-bar progress-bar-animated" role="progressbar" aria-valuenow="{{ariavalue}}" aria-valuemin="0" aria-valuemax="100" style="width:{{ariavaluecss}}; background-color:#d53337;">{{ariavalue}}%</div>\
								</div>\
							</div>',
				plain: true,
				showClose: false,
				// controller: 'WOController',
				scope: $scope
			}).then(function(value) {
				console.log("Data", data);
				// $scope.doClockOnWoAfterPause(ModelDta,JobId);
			}, function(value) {
				//Do something
				//console.log("else",value);
			});
			var $progressBar = $('.progress-bar');
			$timeout(function() {
				$scope.ariavalue = 10;
				$scope.ariavaluecss = '10%';
				$timeout(function() {
					$scope.ariavalue = 30;
					$scope.ariavaluecss = '30%';
					$timeout(function() {
						$scope.ariavalue = 60;
						$scope.ariavaluecss = '60%';
						$timeout(function() {
							console.log("file",myEl[0].files[0].name)
							console.log("accept",ExcelFile.accept, ExcelFile.type, myEl[0].files[0].type)
							XLSXInterface.loadToJson(myEl[0].files[0], function(json){
								console.log("fileJson",json)
								ExcelItem = json.Task_list_BP_Template[5];
								exTmp = json.Task_list_BP_Template;
								$scope.ExcelData = $scope.ExcelData.slice(6);
								for(var y =0; y <exTmp.length; y++){
									var formatKey = Object.keys(exTmp[y]);
									var tmpReformatKey=angular.copy(formatKey);
									for(var idz in formatKey){
										var tmpKey = tmpReformatKey[idz];
										if(tmpKey.includes('*')){
											tmpReformatKey[idz] = tmpKey.split('*')[0];
											exTmp[y][tmpReformatKey[idz]] = exTmp[y][formatKey[idz]];
											delete exTmp[y][formatKey[idz]];
											// console.log ("lagi ==>", formatKey[idz])
											// console.log ("lagi kan==>",  exTmp[y][formatKey[idz]])
										}
									}
									if(exTmp[y].Model_Kendaraan != null && exTmp[y].Model_Kendaraan != "" && exTmp[y].Model_Kendaraan != undefined){
										console.log("extmp", exTmp[y], json);
										$scope.ExcelData.push(exTmp[y]);
										
									}
								}
								console.log("ExcelFile : ",ExcelFile);
								console.log("ExcelData : ", ExcelItem);
								console.log("myEl : ", myEl);
								console.log("json : ", json);
								$timeout(function() {
									$scope.ariavalue = 100;
									$scope.ariavaluecss = '100%';
									$timeout(function() {
										ngDialog.closeAll();
									}, 500); // WAIT 1 seconds
								}, 1000); // WAIT 1 seconds
							},function(err){
								console.log('anjay',err)
							});
						},1000);
					}, 500); // WAIT 1 seconds
				}, 500); // WAIT 1 seconds
			}, 500); // WAIT 1 second
		}, 500);
		
		// var myEl = angular.element( document.querySelector( '#UploadTaskListBP' ) ); //ambil elemen dari dokumen yang di-upload 
	}
	//=====================change format date================
	$scope.changeFormatDate = function(item) {
		var tmpAppointmentDate = item;
		console.log("changeFormatDate item", item);
		tmpAppointmentDate = new Date(tmpAppointmentDate);
		var finalDate
		if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
			var yyyy = tmpAppointmentDate.getFullYear().toString();
			var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = tmpAppointmentDate.getDate().toString();
			finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
		} else {
			finalDate = '';
		}
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	};

	//============================
	$scope.changeFormatJSON = function(data , dataVehicle){
		console.log('inputData', dataVehicle, dataVehicle.length);
		var inputData = [];
		var eachData = {};
		var countStatusVehicle = 0;
		var CaountErrValid = 0
		var CountErrHarga = 0
		// ========= OPTION ERROR =====
		// var ObjectError = {};
		// var keyBaruInt = ['Harga_Jasa','Body_Time', 'Putty_Time', 'Surfacer_Time', 'Spraying_Time', 'Polishing_Time', 'Reassembly_Time',  'Final_Inspection_Time', 'Total_Time'];
		// var keyTanggal = ['Valid_From', 'Valid_To'];
		// var numbers = /^[0-9]+$/;
	    // var tgl = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
		// ===========================
		console.log("data excel", data);

		console.log("data excel", data,data.length);
		
		if(data.length > 1000){
			bsAlert.warning("Data Melebihi Maximum Upload")
			ngDialog.closeAll();
			return false;
		}
		angular.forEach(data, function(value, key){
			// var ValidFrom = new Date(value.Valid_From);
			// var ValidTo = new Date(value.Valid_To);

			for(var i in $scope.optionsTaskListBPMain_ModelKendaraan){
				if(value.Model_Kendaraan.toLowerCase() == $scope.optionsTaskListBPMain_ModelKendaraan[i].name.toLowerCase() ){
					value.VehicleModelId = $scope.optionsTaskListBPMain_ModelKendaraan[i].value;
					break;
				}
			}

			
			for (var v in $scope.optionsTaskListBPEdit_OperationStatusVehicleId){
				console.log('statusVehicle', value.Metode_Perbaikan.toLowerCase(), $scope.optionsTaskListBPEdit_OperationStatusVehicleId[v].Name.toLowerCase(),"=====",value.StatusVehicleId , $scope.optionsTaskListBPEdit_OperationStatusVehicleId[v].MasterId);	
				
				if(value.Metode_Perbaikan != " " && value.Metode_Perbaikan != "null" && value.Metode_Perbaikan != null && value.Metode_Perbaikan != undefined){
					if(value.Metode_Perbaikan.toLowerCase() == $scope.optionsTaskListBPEdit_OperationStatusVehicleId[v].Name.toLowerCase() ){
						value.StatusVehicleId =  $scope.optionsTaskListBPEdit_OperationStatusVehicleId[v].MasterId;
						break;
					}
				}else{
					value.StatusVehicleId = 0 ;
				}	
			}
			console.log('statusVehicle', value.StatusVehicleId);	

			if(value.StatusVehicleId == 2442 ){
				for (var p in $scope.optionsTaskListBPEdit_OperationPointId){
					console.log('PointId', value.Tingkat_Kerusakan, $scope.optionsTaskListBPEdit_OperationPointId[p].Name);
					if(value.Tingkat_Kerusakan != " " && value.Tingkat_Kerusakan != "NULL" && value.Tingkat_Kerusakan != 0 && value.Tingkat_Kerusakan != undefined){
						if(value.Tingkat_Kerusakan.toLowerCase() == $scope.optionsTaskListBPEdit_OperationPointId[p].Name.toLowerCase() ){
							value.PointId =  $scope.optionsTaskListBPEdit_OperationPointId[p].MasterId;
							break;
						}
					}else if(value.Tingkat_Kerusakan == " " || value.Tingkat_Kerusakan == "NULL" || value.Tingkat_Kerusakan == 0 || value.Tingkat_Kerusakan == undefined){
						value.PointId = 0 ;
					}
				}
					console.log('PointId', value.PointId);
			}else{
				
				if(value.Tingkat_Kerusakan != " " && value.Tingkat_Kerusakan != "NULL" && value.Tingkat_Kerusakan != 0 && value.Tingkat_Kerusakan != undefined){
					value.PointId = 0;
				}
				
			}
			console.log("StatusVehicleID ==>>>", value.StatusVehicleId)
			// ======== VALIDASI & Sorting Data AreaId=====
			var lastIdxArea = $scope.optionsTaskListBPEdit_OperationAreaId.length - 1
			$scope.optionsTaskListBPEdit_OperationAreaId.sort();
			if(value.No_Area > $scope.optionsTaskListBPEdit_OperationAreaId[lastIdxArea].id){
				countStatusVehicle++;
			}
			console.log("areaid ke ===>>>>>", value.No_Area,$scope.optionsTaskListBPEdit_OperationAreaId.sort(),$scope.optionsTaskListBPEdit_OperationAreaId[lastIdxArea].id)

			if(value.Warranty_Rate != " " && value.Warranty_Rate != "NULL" && value.Warranty_Rate != 0 && value.Warranty_Rate != undefined){
				value.WarrantyRate = value.Warranty_Rate 
			}else if(value.Warranty_Rate == " " || value.Warranty_Rate == "NULL" || value.Warranty_Rate == 0 || value.Warranty_Rate == undefined){
				value.WarrantyRate = 0
			}
			console.log('WaranntyRate', value.WarrantyRate);

			if(value.Operation_No != " " && value.Operation_No != "NULL" && value.Operation_No != undefined && value.Operation_No != 0 && value.Operation_No != '0'){
				value.OperationNo = value.Operation_No;
			}else if(value.Operation_No == "" || value.Operation_No == "NULL" || value.Operation_No == undefined || value.Operation_No == 0 || value.Operation_No == "0"){
				value.OperationNo = undefined;
			}
			console.log("OperationNo", value.Operation_No, value.OperationNo);

			if(value.No_Area != " " && value.No_Area != "NULL" && value.No_Area != 0 && value.No_Area != undefined){
				value.NoArea = value.No_Area.trim()
			}else if(value.No_Area == " " || value.No_Area == "NULL" || value.No_Area == 0 || value.No_Area == undefined){
				value.NoArea = 0
			}
			// ------------------Body Time DLL --------------------------

			if(value.Body_Time != " " && value.Body_Time != "NULL" && value.Body_Time != 0 && value.Body_Time != undefined){
				value.BodyTime = value.Body_Time 
			}else if(value.Body_Time == " " || value.Body_Time == "NULL" || value.Body_Time == 0 || value.Body_Time == undefined || value.Body_Time == null){
				value.BodyTime = 0
				value.Body_Time = "0"
			}
			

			if(value.Putty_Time != " " && value.Putty_Time != "NULL" && value.Putty_Time != 0 && value.Putty_Time != undefined){
				value.PuttyTime = value.Putty_Time 
			}else if(value.Putty_Time == " " || value.Putty_Time == "NULL" || value.Putty_Time == 0 || value.Putty_Time == undefined || value.Putty_Time == null){
				value.PuttyTime = 0
				value.Putty_Time = "0"
			}
			

			if(value.Surfacer_Time != " " && value.Surfacer_Time != "NULL" && value.Surfacer_Time != 0 && value.Surfacer_Time != undefined){
				value.SurfacerTime = value.Surfacer_Time 
			}else if(value.Surfacer_Time == " " || value.Surfacer_Time == "NULL" || value.Surfacer_Time == 0 || value.Surfacer_Time == undefined || value.Surfacer_Time == null){
				value.SurfacerTime = 0
				value.Surfacer_Time  = "0";
			}
			

			if(value.Spraying_Time != " " && value.Spraying_Time != "NULL" && value.Spraying_Time != 0 && value.Spraying_Time != undefined){
				value.SprayingTime = value.Spraying_Time 
			}else if(value.Spraying_Time == " " || value.Spraying_Time == "NULL" || value.Spraying_Time == 0 || value.Spraying_Time == undefined || value.Spraying_Time == null ){
				value.SprayingTime = 0
				value.Spraying_Time = "0"
			}
			

			if(value.Polishing_Time != " " && value.Polishing_Time != "NULL" && value.Polishing_Time != 0 && value.Polishing_Time != undefined){
				value.PolishingTime = value.Polishing_Time 
			}else if(value.Polishing_Time == " " || value.Polishing_Time == "NULL" || value.Polishing_Time == 0 || value.Polishing_Time == undefined || value.Polishing_Time == null){
				value.PolishingTime = 0
				value.Polishing_Time = "0"
			}
			

			if(value.Reassembly_Time != " " && value.Reassembly_Time != "NULL" && value.Reassembly_Time != 0 && value.Reassembly_Time != undefined){
				value.ReassemblyTime = value.Reassembly_Time 
			}else if(value.Reassembly_Time == " " || value.Reassembly_Time == "NULL" || value.Reassembly_Time == 0 || value.Reassembly_Time == undefined || value.Reassembly_Time == null){
				value.ReassemblyTime = 0
				value.Reassembly_Time = "0";
			}
			

			if(value.Final_Inspection_Time != " " && value.Final_Inspection_Time != "NULL" && value.Final_Inspection_Time != 0 && value.Final_Inspection_Time != undefined){
				value.FinalInspectionTime = value.Final_Inspection_Time 
			}else if(value.Final_Inspection_Time == " " || value.Final_Inspection_Time == "NULL" || value.Final_Inspection_Time == 0 || value.Final_Inspection_Time == undefined){
				value.FinalInspectionTime = 0
				value.Final_Inspection_Time = "0";
			}
			

			if(value.Total_Time != " " && value.Total_Time != "NULL" && value.Total_Time != 0 && value.Total_Time != undefined){
				value.TotalTime = value.Total_Time 
			}else if(value.Total_Time == " " || value.Total_Time == "NULL" || value.Total_Time == 0 || value.Total_Time == undefined){
				value.TotalTime = 0
				value.Total_Time = "0";
			}

			if(value.Valid_From == "" || value.Valid_From == "Null" || value.Valid_From == undefined 
			|| value.Valid_To == "" || value.Valid_To == "Null" || value.Valid_To == undefined  ){
				CaountErrValid++
			}
			console.log('tgl awal', value.Valid_From);
			console.log('tgl akhir', value.Valid_To);

			if(value.Harga_Jasa == "" || value.Harga_Jasa == "Null" || value.Harga_Jasa == undefined ){
				CountErrHarga++
			}

			// ============== New Validation ===========
			// for(var loopIdx in keyBaruInt){
			// 	if(!value[keyBaruInt[loopIdx]].match(numbers)){
			// 		ObjectError[keyBaruInt[loopIdx]] = 1;
			// 	}
			// }

			// for(var loopIdxx in keyTanggal){
			// 	if(!value[keyTanggal[loopIdxx]].match(tgl)){
			// 		ObjectError[keyTanggal[loopIdxx]] = 1;
			// 	}
			// }
			
			console.log('BodyTime', value.BodyTime);
			console.log('PuttyTime', value.PuttyTime);
			console.log('SurfacerTime', value.SurfacerTime);
			console.log('SprayingTime', value.SprayingTime);
			console.log('PolishingTime', value.PolishingTime);
			console.log('ReassemblyTime', value.ReassemblyTime);
			console.log('FinalInspectionTime', value.FinalInspectionTime);
			console.log('TotalTime', value.TotalTime);
			
			// value.StatusVehicleId = parseInt(value.StatusVehicleId);
			// value.PointId = parseInt(value.PointId);
			eachData = {
				"TaskListBPId" : $scope.TaskListBPEdit.TaskId,
				"OutletId" : $scope.TaskListBPEdit.OutletId,
				"DealerCode" : value.Dealer_Code, 
				"OutletCode" : value.Outlet_Code, 
				"WMI": value.WMI, 
				"VDS": value.VDS, 
				// "VehicleTypeName": value.Full_Model, 
				// "ModelName" : value.Model_Kendaraan, 
				"VehicleModelId" : value.VehicleModelId, 
				// "VehicleTypeId" : value.VehicleTypeId, 
				"OperationNo": value.OperationNo, 
				"ActivityTypeId" : value.Tipe_Perkerjaan, 
				"TaskName" : value.Nama_Pekerjaan, 
				"Qty" : value.Jumlah = 1, 
				"UOM" : value.UOM, 
				"HourAmount" : value.Jumlah_Hour, 
				"ServiceRate": value.Harga_Jasa, 
				"WarrantyRate":value.WarrantyRate, 
				// "DamageLevel" : value.Tingkat_Kerusakan, 
				"BodyEstimationMinute": parseInt(value.BodyTime), 
				"PuttyEstimationMinute": parseInt(value.PuttyTime), 
				"SurfacerEstimationMinute": parseInt(value.SurfacerTime), 
				"SprayingEstimationMinute": parseInt(value.SprayingTime), 
				"PolishingEstimationMinute": parseInt(value.PolishingTime), 
				"ReassemblyEstimationMinute": parseInt(value.ReassemblyTime), 
				"FIEstimationMinute": parseInt(value.FinalInspectionTime), 
				"TotalEstimationMinute": parseInt(value.TotalTime), 
				"UploadDate": value.UploadDate, 
				"ValidFrom": $filter('date')(new Date(value.Valid_From), "yyyy-MM-dd"),
				"ValidThru": $filter('date')(new Date(value.Valid_To), "yyyy-MM-dd"),
				"PreparedBy": value.Prepared_By, 
				"PreparedDate": value.Prepared_Date,
				"PointId": value.PointId,
				"AreaId": value.NoArea,
				"StatusVehicleId": value.StatusVehicleId
			}
			
			inputData.push(eachData);
			console.log('inputData123', inputData);
		});

		console.log('inputData', inputData);
		if(countStatusVehicle > 0){
			bsAlert.warning("Data Area Tidak Ditemukan")
			ngDialog.closeAll();
			return false;
		}

		
		if(CaountErrValid > 0){
			bsAlert.warning("Valid Form Atau Valid To Masih ada Yang Kosong")
			ngDialog.closeAll();
			return false;
		}

		
		if(CountErrHarga > 0){
			bsAlert.warning("Harga Jasa Masih ada Yang Kosong")
			ngDialog.closeAll();
			return false;
		}
		
		$scope.ariavalue = 80;
		$scope.ariavaluecss = '80%';
		$scope.message = 'Mohon menunggu sedang memeriksa data..'
		// ======= NEW VALIDATION =====
		// var countError = 0;
		// for(var loopIndex in keyBaruInt){
		// 	if(ObjectError[keyBaruInt[loopIndex]] == 1){
		// 		countError++;
		// 		var msg = angular.copy(keyBaruInt[loopIndex]);
		// 		msg = msg.replace(/_/g, ' ');
		// 		bsNotify.show({
		// 			size: 'big',
		// 			type: 'danger',
		// 			title: "Format Data Ada yang Salah",
		// 			content:'<b>'+msg+'</b>'+ ' harus diisi dengan angka.'
		// 		});
		// 	}
		// }
		// if(countError > 0){
		// 	ngDialog.closeAll();
		// 	return false;
		// }

		// var countErrorTgl = 0;
		// for(var loopIndexx in keyTanggal){
		// 	if(ObjectError[keyTanggal[loopIndexx]] == 1){
		// 		countErrorTgl++;
		// 		var msg = angular.copy(keyTanggal[loopIndexx]);
		// 		msg = msg.replace(/_/g, ' ');
		// 		bsNotify.show({
		// 			size: 'big',
		// 			type: 'danger',
		// 			title: "Format Data Ada yang Salah",
		// 			content:'<b>'+msg+'</b>'+ ' harus diisi dengan angka.'
		// 		});
		// 	}
		// }	
		// if(countErrorTgl > 0){
		// 	ngDialog.closeAll();
		// 	return false;
		// }
		// if(CountUpload > 0){
		// 	bsAlert.warning("Ada Data yang Sudah terdaftar")
		// 	ngDialog.closeAll();
		// 	return false;
		// }
		var tmpDeletedTask = [];
		var findCekData = function(data, x){
			if(x > (inputData.length-1)){
				return
			}
			console.log("asuasu", data[x],data)
			// for(var gt in inputData){
				tmpCekData = [];
				$scope.TaskListBPEdit.TaskId = 0
				TaskListBPFactory.CekTasklistBPUpload(data[x].TaskName, $scope.TaskListBPEdit.TaskId,data[x].VehicleModelId).then(function (res) {
					var resu = res.data
					console.log('res >>', resu);
					
					if (resu[0] == "false") {
						// bsAlert.alert({
						// 	// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						// 	title: "Nama Pekerjaan " + data[x].TaskName + " sudah terdaftar",
						// 	text: "",
						// 	type: "warning",
						// 	showCancelButton: false
						// });
						// data = data.slice(1)
						// ngDialog.closeAll();
						tmpDeletedTask.push(data[x]);
						console.log('ola',data)
					}
					console.log("AU",tmpDeletedTask)

					if(x < (inputData.length-1)){
						findCekData(data, x + 1);
						
					}else if((inputData.length - 1) == x){
						$scope.changeFormatJSONUpload(data, tmpDeletedTask);
					}
				});
			// }
			
			// TaskListBPFactory.getFullModelFromModel(data[x].Id).then(function(res){
			// 	tmpArrayTypeCar[data[x].nama] = [];
			// 	var jsonData = [];
			// 	angular.forEach(res.data.Result,function(value,key){
			// 		console.log('inputData[x] ====>', data[x]);
			// 		if(value.VehicleTypeName != null){
			// 			jsonData.push({name:value.VehicleTypeName.split(' ')[0],value:value.VehicleTypeId})
			// 		}else{
			// 			jsonData.push({name:"-",value:value.VehicleTypeId})
			// 		}
					
			// 	});
			// 	tmpArrayTypeCar[data[x].nama] = jsonData;
			// 	// if(count == (data.length )){
			// 	// 	$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
			// 	// }

			// 	if(x < (inputData.length-1)){
			// 		findFullModel(data, x + 1);
					
			// 	}else if((inputData.length - 1) == x){
			// 		$scope.changeFormatJSONDownload(excelTypeKendaraan, tmpArrayTypeCar, finaldata);
			// 	}
			// });
		}
		findCekData(inputData, 0)

		
		// var CountUpload = 0
		
		// var TmpSuccces = '';
		// console.log("data input sbelum masuk factory :", eachData);
		// TaskListBPFactory.UploadData(inputData)
		// .then(
		// 	function(res){
		// 		$scope.TmpSuccces.push(res);
		// 		console.log("lala",res,TmpSuccces)
		// 		$scope.ariavalue = 100;
		// 		$scope.ariavaluecss = '100%';
		// 		ngDialog.closeAll();
		// 		$timeout(function(){
		// 			bsAlert.success("Data Berhasil Disimpan");
		// 			// angular.element('#ModalUploadTaskListBP').modal('hide');
		// 			$scope.showUploadFileTaskBP = false;
		// 			angular.element( document.querySelector( '#UploadTaskListBP' ) ).val(null);
		// 		},500)
		// 	},
		// 	function(err){
		// 		bsAlert.warning("Format Data Ada yang Salah");
		// 		ngDialog.closeAll();
		// 		return false;
		// 	},
		// 	console.log("bagus11", $scope.TmpSuccces)
		// );

		// console.log("bagus22", $scope.TmpSuccces)

		// $scope.ExcelData = [];
		// console.log("excel data ke  ==>>>>>>>", $scope.ExcelData)

		// console.log("bagus33", $scope.TmpSuccces)
		// if(isValid == 1)
		// {
		// console.log("data input sbelum masuk factory :", eachData);
		// return false;
			
			
	}
	$scope.changeFormatJSONUpload = function(data, deletedData){
		console.log("data ===>", data);
		console.log("deletedData ===>", deletedData);
		var tmpCopyData = angular.copy(data);
		var tmpCopyDeleted = angular.copy(deletedData);
		if(tmpCopyData.length <= tmpCopyDeleted.length ){
			bsAlert.alert({
				// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
				title: tmpCopyDeleted.length+ " dari "+ tmpCopyData.length + " data pekerjaan tidak valid, duplikat data",
				text: "",
				type: "warning",
				showCancelButton: false
			});
			ngDialog.closeAll();
			return false
		}else {
			$scope.ariavalue = 100;
			$scope.ariavaluecss = '100%';
			ngDialog.closeAll();

			if(deletedData.length > 0){
				bsAlert.alert({
					// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
					title: tmpCopyDeleted.length+ " dari "+ tmpCopyData.length + " data pekerjaan tidak valid, duplikat data",
					// text: "Apakah anda yakin akan menyimpan "+(da,
					text:"",
					type: "warning",
					showCancelButton: true
				},
				function() {
					// yes button
					for(var i in tmpCopyDeleted){
						tmpCopyData.splice(_.findIndex(tmpCopyData, { "TaskName": tmpCopyDeleted[i].TaskName,"AreaId": tmpCopyDeleted[i].AreaId,"VehicleModelId": tmpCopyDeleted[i].VehicleModelId }), 1)
					}
					console.log("data ==>", tmpCopyData);
                    TaskListBPFactory.UploadData(tmpCopyData)
						.then(
							function(res){
								// $scope.ariavalue = 100;
								// $scope.ariavaluecss = '100%';
								// ngDialog.closeAll();
								$timeout(function(){
									// bsAlert.success("Data Berhasil Disimpan");
									bsAlert.success("data yang berhasil di upload akan mulai aktif dan dapat di gunakan di H+1 dari tanggal upload");
									// angular.element('#ModalUploadTaskListBP').modal('hide');
									$scope.showUploadFileTaskBP = false;
									angular.element( document.querySelector( '#UploadTaskListBP' ) ).val(null);
								},500)
								
							},
							function(err){
								bsAlert.warning("Format Data Ada yang Salah");
								ngDialog.closeAll();
								return false;
							}
						);
						$scope.ExcelData = [];
                },
                function() {
					// cancel button
                }
				);
			}else{
				TaskListBPFactory.UploadData(tmpCopyData)
						.then(
							function(res){
								// $scope.ariavalue = 100;
								// $scope.ariavaluecss = '100%';
								// ngDialog.closeAll();
								$timeout(function(){
									// bsAlert.success("Data Berhasil Disimpan");
									bsAlert.success("data yang berhasil di upload akan mulai aktif dan dapat di gunakan di H+1 dari tanggal upload");
									// angular.element('#ModalUploadTaskListBP').modal('hide');
									$scope.showUploadFileTaskBP = false;
									angular.element( document.querySelector( '#UploadTaskListBP' ) ).val(null);
								},500)
								
							},
							function(err){
								bsAlert.warning("Format Data Ada yang Salah");
								ngDialog.closeAll();
								return false;
							}
						);
						$scope.ExcelData = [];
			}
		// 	TaskListBPFactory.UploadData(inputData)
		// 	.then(
		// 		function(res){
		// 			$scope.ariavalue = 100;
		// 			$scope.ariavaluecss = '100%';
		// 			ngDialog.closeAll();
		// 			$timeout(function(){
		// 				bsAlert.success("Data Berhasil Disimpan");
		// 				// angular.element('#ModalUploadTaskListBP').modal('hide');
		// 				$scope.showUploadFileTaskBP = false;
		// 				angular.element( document.querySelector( '#UploadTaskListBP' ) ).val(null);
		// 			},500)
		// 		},
		// 		function(err){
		// 			bsAlert.warning("Format Data Ada yang Salah");
		// 			ngDialog.closeAll();
		// 			return false;
		// 		},
		// $scope.ExcelData = [];
		// console.log("excel data ke  ==>>>>>>>", $scope.ExcelData)
		}
	}

	$scope.checkError = function(dataArr, param, type){
		// id + string
		// M = Model
		// S = Satuan
		// V = valid From / to
		// T = 'No_Area', 'Harga_Jasa','Body_Time', 'Putty_Time', 'Surfacer_Time', 'Spraying_Time', 'Polishing_Time', 'Reassembly_Time',  'Final_Inspection_Time', 'Total_Time'
		// P = Perbaikan
		// D = Duplikasi

		
		var errorCount = 0;
		if(type == 1){
				// === identifically array ==
				for(var idxArr = 0; idxArr < dataArr.length; idxArr++){
					dataArr[idxArr].rowsNumber = idxArr;
				}
				tmpExcelDataDuplicate = angular.copy(dataArr)
				errorCount = 0
				// var tmpDuplicate = {}
				var tmpDuplicate = []
				var sortByProperty = function (property) {
					return function (x, y) {
						return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1)); //sorting data
					};
				};
				var tmpExcelDataFilter = angular.copy(tmpExcelDataDuplicate.sort(sortByProperty('Nama_Pekerjaan')));

				console.log(' excel filter ===>',  tmpExcelDataFilter);
				var checkDuplicateInObject = function(propertyModel,propertyName, dataArr) {
					var seenDuplicate = false, // ini Flag default buat ada duplikat atau nggak
						testObject = {}; // ini variable object buat nampung temporary yg mau dicek ex:'kode vendor'
					
					_.map(dataArr, function(item) { //  ini looping map atau mapping ulang
						var itemPropertyName  = []
						itemPropertyName.push(item[propertyModel])
						itemPropertyName.push(item[propertyName]);
						if (itemPropertyName in testObject) { // cek jika value loop terkini itu ada di temporary
							testObject[itemPropertyName].duplicate = true; // otomatis ngasih flag ini gk terlalu penting sih tapi klo mau dpake nantinya boleh
							item.duplicate = true; // ini penting
							seenDuplicate = true; // ini otomatis flag bakal true, klo ada yg duplikat
						}
						else {
							testObject[itemPropertyName] = item; // masukin value dari propertinya ke temporary
							delete item.duplicate;
						}
					});
					_.map(dataArr, function(item, i){
						if(item.duplicate){
							tmpDuplicate.push({
								rowsNumber:item.rowsNumber,
								field:item.Nama_Pekerjaan,
								ModelKendaraan:item.Model_Kendaraan,
								id:'D' + i
							})
						}
					});
					
					return seenDuplicate;
				}
				if(checkDuplicateInObject('Model_Kendaraan','Nama_Pekerjaan', tmpExcelDataFilter )){

					// value concat
					var findValueGroup = {};
					var valueGroup = [];
					for(var item, idx = 0; item = tmpDuplicate[idx++];){
						var name1 = item.field;
						if(!(name1 in findValueGroup))	{
							findValueGroup[name1] = 1;
							valueGroup.push(name1);
						}
					}
					for(var i in tmpDuplicate){
						for(var j in valueGroup){
							if(tmpDuplicate[i].field === valueGroup[j]){
								$scope.allError.push({
									id:tmpDuplicate[i].id,
									msg:'Nama Pekerjaan duplikat',
									row:(tmpDuplicate[i].rowsNumber+8),
									field:tmpDuplicate[i].ModelKendaraan + ' - ' + valueGroup[j]
								})
								break;
							}
						}
			
					}
					// _.map(tmpDuplicate, function(item, i){
					// 	if(item.field){

					// 	}
					// 	$scope.allError.push({
					// 		id:null,
					// 		msg:'Nama Pekerjaan duplikat',
					// 		row:null,
					// 		field:tmpDuplicate[1]
					// 	})
					// });
				} 
			
		}else if(type == 2){
			for(var i = 0; i < dataArr.length; i++){
				errorCount = 0
				for(var ii in param){
					if(dataArr[i].Model_Kendaraan.toLowerCase() == param[ii].toLowerCase()){
						break;
					}else{
						errorCount++;
					}
					if(errorCount == param.length){
						$scope.allError.push({
							id:i+'M',
							msg:'Model Tidak Ditemukan',
							row:(i+8),
							field:'Model_Kendaraan'
						})
						// bsAlert.warning("Format Data Ada yang Salah","Terjadi kesalahan di baris "+(i+8)+" ( Model Tidak Ditemukan )");
						// return false
					}
				}
			}
			console.log("==sukses mobil");
			return true
		}else if(type == 3){
			for(var i = 0; i < dataArr.length; i++){
				errorCount = 0;
				for(var ii in param){
					if(dataArr[i].UOM.toLowerCase() == param[ii].toLowerCase()){
						break;
					}else{
						errorCount++;
					}
					if(errorCount == param.length){
						$scope.allError.push({
							id:i+'S',
							msg:'Satuan Tidak Ditemukan',
							row:(i+8),
							field:'UOM'
						})
						// bsAlert.warning("Format Data Ada yang Salah","Terjadi kesalahan di baris "+(i+8)+" ( Satuan Tidak Ditemukan )")
						// return false
					}
				}
			}
			console.log("==sukses satuan");
			return true
		}else if(type == 4){
			var keyTanggal = ['Valid_From', 'Valid_To'];
			var tgl = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
			var ObjectError = {};
			for(var i = 0; i < dataArr.length; i++){
				for(var loopIdxx in keyTanggal){
					if(!dataArr[i][keyTanggal[loopIdxx]].match(tgl)){
						ObjectError[keyTanggal[loopIdxx]] = 1;

						$scope.allError.push({
							id:i+'V',
							msg:'Harus diisi dengan format tanggl YYYY/MM/DD.',
							row:(i+8),
							field:keyTanggal[loopIdxx]
						})
						// var msg = angular.copy(keyTanggal[loopIdxx]);
						// msg = msg.replace(/_/g, ' ');
						// bsNotify.show({
						// 	size: 'big',
						// 	type: 'danger',
						// 	title: "Format Data Ada yang Salah,Terjadi kesalahan di baris " + (i+8) + " Format Data Ada yang Salah",
						// 	content:'<b>'+msg+'</b>'+ ' Harus diisi dengan format tanggl YYYY/MM/DD.'
						// });
						// return false;
					}
				}
			}
			console.log("==sukses validfrom valid to");
			return true
		}else if(type == 5){
			var keyBaruInt = [ 'Harga_Jasa','Body_Time', 'Putty_Time', 'Surfacer_Time', 'Spraying_Time', 'Polishing_Time', 'Reassembly_Time',  'Final_Inspection_Time', 'Total_Time'];
			var numbers = /^[0-9]+$/;
			var ObjectError = {};
			for(var i = 0; i < dataArr.length; i++){
				for(var loopIdx in keyBaruInt){
					if(!dataArr[i][keyBaruInt[loopIdx]].match(numbers)){
						ObjectError[keyBaruInt[loopIdx]] = 1;
						var msg = angular.copy(keyBaruInt[loopIdx]);
						msg = msg.replace(/_/g, ' ');
						// bsNotify.show({
						// 	size: 'big',
						// 	type: 'danger',
						// 	title: "Format Data Ada yang Salah,Terjadi kesalahan di baris " + (i+8) + " Format Data Ada yang Salah",
						// 	content:'<b>'+msg+'</b>'+ ' harus diisi dengan angka.'
						// });
						// return false;
						$scope.allError.push({
							id:i+'T',
							msg:'Harus diisi dengan angka.',
							row:(i+8),
							field:keyBaruInt[loopIdx]
						})
					}
				}
			}
			console.log("==sukses integer");
			return true
		}else if(type == 6){
			for(var i = 0; i < dataArr.length; i++){
				errorCount = 0;
				for(var ii in param){
					if(dataArr[i].Metode_Perbaikan.toLowerCase() == param[ii].toLowerCase()){
						break;
					}else{
						errorCount++;
					}
					if(errorCount == param.length){
						$scope.allError.push({
							id:i+'P',
							msg:'Metode Perbaikan Tidak Ditemukan',
							row:(i+8),
							field:'Metode Perbaikan'
						})
					}
				}
			}
			console.log("== sukses Metode Perbaikan");
			return true
		}else if(type == 7){
			var keyBaruInt = ['No_Area'];
			var numbers = /[0-9]+$(\.[0-9][0-9]$)?/;
			var ObjectError = {};
			for(var i = 0; i < dataArr.length; i++){
				for(var loopIdx in keyBaruInt){
					if(!dataArr[i][keyBaruInt[loopIdx]].match(numbers)){
						ObjectError[keyBaruInt[loopIdx]] = 1;
						var msg = angular.copy(keyBaruInt[loopIdx]);
						msg = msg.replace(/_/g, ' ');
						// bsNotify.show({
						// 	size: 'big',
						// 	type: 'danger',
						// 	title: "Format Data Ada yang Salah,Terjadi kesalahan di baris " + (i+8) + " Format Data Ada yang Salah",
						// 	content:'<b>'+msg+'</b>'+ ' harus diisi dengan angka.'
						// });
						// return false;
						$scope.allError.push({
							id:i+'T',
							msg:'Harus diisi dengan angka.',
							row:(i+8),
							field:keyBaruInt[loopIdx]
						})
					}
				}
			}
			console.log("== suksess No_Area");
			return true
		}
	}
	$scope.showMessageAllerror = function(data){
		var findValueGroup = {};
		var valueGroup = [];
		var msgGroup = {};
		var tmpmsgGroup = {};
		for(var item, idx = 0; item = data[idx++];){
			var name1 = item.field;
			if(!(name1 in findValueGroup))	{
				findValueGroup[name1] = 1;
				valueGroup.push(name1);
			}
		}
		for(var ii in valueGroup){
			tmpmsgGroup[valueGroup[ii]] = {};
			tmpmsgGroup[valueGroup[ii]]['row'] = [];
		}
		for(var i in data){
			for(var j in valueGroup){
				if(data[i].field === valueGroup[j]){
					tmpmsgGroup[data[i].field]['row'].push(data[i].row);
					tmpmsgGroup[data[i].field]['message'] = data[i].msg;
				}
			}

		}
		var keys = Object.keys(tmpmsgGroup);
		var PrintError = [];
		
		console.log('====>', keys, tmpmsgGroup);
		for(var zz in keys){
			var msg = angular.copy(keys[zz]);
			msg = msg.replace(/_/g, ' ');
			// ===== Multiple Notify ========
			// bsNotify.show({
			// 	size: 'big',
			// 	type: 'danger',
			// 	title: "Format Data Ada yang Salah,Terjadi kesalahan di baris " + tmpmsgGroup[keys[zz]]['row'].join(', '),
			// 	content:'<b>'+msg+'</b> '+ tmpmsgGroup[keys[zz]]['message']
			// });
			PrintError.push({
				title: "Format Data Ada yang Salah,Terjadi kesalahan di baris " + tmpmsgGroup[keys[zz]]['row'].join(', '),
				content:'<b>'+msg+'</b> '+ tmpmsgGroup[keys[zz]]['message']
			})
		}
		$scope.dataError = PrintError;

		return false 
		
	}
	$scope.onOkerror = function(){
		$scope.showUploadedError = false;
		$scope.ExcelData = []
	}
	$scope.onCancelError = function(){
		$scope.showUploadedError = false;
		$scope.ExcelData = []
	}
	$scope.UploadTaskListBP_Upload_Clicked = function(){
		var eachData = {};
		var isValid = 1;
		var tmpExcelData = [];
		var tmpExcelDataDuplicate = [];
		var tmpExcelSatuan = [];
		var tmpExcelStatusVehicle = [];
		var tmpDataModelId = [];
		var tmpDataUOM = [];
		var tmpDataStatusVehicleId = [];
		var tmpVehicleType = {};
		var count = 0;
		var tmpResult = [];
		var tmpResult1 = [];
		var tmpResult2 = [];
		var tmpexcelarray= [];
		$scope.allError = [];
		$scope.ExcelData = $scope.ExcelData.slice(6);

		if(!$scope.checkError($scope.ExcelData,null,1)){
			// ngDialog.closeAll();
			// return false
		}

		console.log("excel Data ==>", $scope.ExcelData );
		tmpExcelData = angular.copy($scope.ExcelData);
		tmpExcelSatuan = angular.copy($scope.ExcelData);
		tmpExcelStatusVehicle = angular.copy($scope.ExcelData);
		// ================ NEW CODE =====
		
		// var lengthTmp = angular.copy($scope.ExcelData.length)
		// for(var i = 0; i<lengthTmp; i++){ 
		// 	if($scope.ExcelData[i].No_Area.includes(',')){
		// 		$scope.ExcelData[i].idx = i;
		// 		console.log("==== > koma", $scope.ExcelData[i].No_Area)
		// 		var tmpRows = $scope.ExcelData[i];
		// 		var tmpArea = [];
		// 		tmpArea = $scope.ExcelData[i].No_Area.split(',');
		// 		console.log('tmpArea ===>',tmpArea , tmpRows);
		// 		for(var z = 0; z < tmpArea.length; z++){
		// 			var dataRows = {};
		// 			dataRows = angular.copy(tmpRows);
		// 			delete dataRows.idx;
		// 			dataRows.No_Area = angular.copy(tmpArea[z]);
		// 			console.log('tmpRows[z] ===>',dataRows.No_Area);
		// 			$scope.ExcelData.push(dataRows);
		// 		}
				
		// 	}else if($scope.ExcelData[i].No_Area.includes('.')){
		// 		$scope.ExcelData[i].idx = i; 
		// 		console.log("==== > titik", $scope.ExcelData[i].No_Area)
		// 		var tmpRows = $scope.ExcelData[i]
		// 		var tmpArea = [];
		// 		tmpArea = $scope.ExcelData[i].No_Area.split('.');
		// 		console.log('tmpArea ===>',tmpArea , tmpRows);
		// 		for(var z = 0; z < tmpArea.length; z++){
		// 			var dataRows = {};
		// 			dataRows = angular.copy(tmpRows);
		// 			delete dataRows.idx;
		// 			dataRows.No_Area = angular.copy(tmpArea[z]);
		// 			console.log('tmpRows[z] ===>',dataRows.No_Area);
		// 			$scope.ExcelData.push(dataRows);
		// 		}
		// 	}
		// 	console.log('index excel ke ===>',_.findIndex($scope.ExcelData, { "idx":i }));
		// }
		for(var z = 0 ; z< $scope.ExcelData.length; z++ ){
			if($scope.ExcelData[z].idx == undefined ){
				tmpexcelarray.push($scope.ExcelData[z]);
			}	
			
		}

		for(var ar in tmpexcelarray){
			tmpexcelarray[ar].No_Area = tmpexcelarray[ar].No_Area.trim()
		}

		$scope.ExcelData = [];
		$scope.ExcelData = tmpexcelarray;
		var modelCompare = [];
		var unitCompare = [];
		console.log("ngawur=======> ", $scope.ExcelData)
		// ===============================
		// tmpExcelData = _.uniq(tmpExcelData, 'Model_Kendaraan');
		var lookup = {};
		for(var item, idx = 0; item = tmpExcelData[idx++];){
			var name = item.Model_Kendaraan;
			if(!(name in lookup))	{
				lookup[name] = 1;
				tmpResult.push(name);
			}

		}
		tmpExcelData = tmpResult;
		console.log('=====>>',tmpResult, tmpExcelData);
		if(tmpExcelData != undefined && tmpExcelData != "" && tmpExcelData != 0){
			for(var xx in tmpExcelData){
				var itungModel =1;
				tmpExcelData[xx] = (tmpExcelData[xx]).toLowerCase();
				for(var x in $scope.optionsTaskListBPMain_ModelKendaraan){
					if(tmpExcelData[xx] == $scope.optionsTaskListBPMain_ModelKendaraan[x].name.toLowerCase() ){
						tmpDataModelId.push($scope.optionsTaskListBPMain_ModelKendaraan[x].value);
						modelCompare.push($scope.optionsTaskListBPMain_ModelKendaraan[x].name);
						break;
					}else{
						itungModel++
					}
					if(itungModel == $scope.optionsTaskListBPMain_ModelKendaraan.length ){
						modelCompare.push($scope.optionsTaskListBPMain_ModelKendaraan[x].name);
						// console.log("itungan", itungModel, $scope.optionsTaskListBPMain_ModelKendaraan.length)
						// bsAlert.warning("Format Data Ada yang Salah","Model Tidak Ditemukan")
						// ngDialog.closeAll();
						// return false;
					}
				}
			}
		}else{
			bsAlert.warning("Model Kendaraan Masih ada Yang Kosong")
			ngDialog.closeAll();
			return false;
		}

		if(!$scope.checkError($scope.ExcelData,modelCompare,2)){
			// ngDialog.closeAll();
			// return false
		}

		//----------Satuan------------
		var lookup1 = {};
		for(var item, idx = 0; item = tmpExcelSatuan[idx++];){
			var name1 = item.UOM;
			if(!(name1 in lookup1))	{
				lookup1[name1] = 1;
				tmpResult1.push(name1);
			}

		}
		tmpExcelSatuan = tmpResult1;
		console.log("List Satuan ==>",tmpExcelSatuan, tmpResult1)
		if(tmpExcelSatuan != undefined && tmpExcelSatuan != "" && tmpExcelSatuan != 0){
			for(var cc in tmpExcelSatuan){
				var itungModel1 =1;
				tmpExcelSatuan[cc] = (tmpExcelSatuan[cc]).toLowerCase();
				for(var a in $scope.optionsTaskListBPEdit_OperationUOM){
					if(tmpExcelSatuan[cc] == $scope.optionsTaskListBPEdit_OperationUOM[a].name.toLowerCase() ){
						tmpDataUOM.push($scope.optionsTaskListBPEdit_OperationUOM[a].name);
						unitCompare.push($scope.optionsTaskListBPEdit_OperationUOM[a].name);
						break;
					}else{
						itungModel1++
					}
					if(itungModel1 == $scope.optionsTaskListBPEdit_OperationUOM.length ){
						unitCompare.push($scope.optionsTaskListBPEdit_OperationUOM[a].name);
						// console.log("itungan", itungModel, $scope.optionsTaskListBPMain_ModelKendaraan.length)
						// bsAlert.warning("Format Data Ada yang Salah","Model Tidak Ditemukan")
						// ngDialog.closeAll();
						// return false;
					}
				}
			}
		}else{
			bsAlert.warning("UOM Masih Ada Yang Kosong")
			ngDialog.closeAll();
			return false;
		}

		var lookup2 = {};
		for(var item, idx = 0; item = tmpExcelStatusVehicle[idx++];){
			var name2 = item.Metode_Perbaikan;
			if(!(name2 in lookup2))	{
				lookup2[name2] = 1;
				tmpResult2.push(name2);
			}

		}
		tmpExcelStatusVehicle = tmpResult2;
		if(tmpExcelStatusVehicle != undefined && tmpExcelStatusVehicle != "" && tmpExcelStatusVehicle != 0){
			for(var cc in tmpExcelStatusVehicle){
				var itungModel11 =1;
				tmpExcelStatusVehicle[cc] = (tmpExcelStatusVehicle[cc]).toLowerCase();
				for(var a in $scope.optionsTaskListBPEdit_OperationStatusVehicleId){
					if(tmpExcelStatusVehicle[cc] == $scope.optionsTaskListBPEdit_OperationStatusVehicleId[a].Name.toLowerCase() ){
						tmpDataStatusVehicleId.push($scope.optionsTaskListBPEdit_OperationStatusVehicleId[a].Name);
						break;
					}else{
						itungModel11++
					}
					if(itungModel11 == $scope.optionsTaskListBPEdit_OperationStatusVehicleId.length ){
						tmpDataStatusVehicleId.push($scope.optionsTaskListBPEdit_OperationStatusVehicleId[a].Name);
						// console.log("itungan", itungModel, $scope.optionsTaskListBPMain_ModelKendaraan.length)
						// bsAlert.warning("Format Data Ada yang Salah","Model Tidak Ditemukan")
						// ngDialog.closeAll();
						// return false;
					}
				}
			}
		}else{
			bsAlert.warning("UOM Masih Ada Yang Kosong")
			ngDialog.closeAll();
			return false;
		}

		if(!$scope.checkError($scope.ExcelData,unitCompare,3)){
			// ngDialog.closeAll();
			// return false
		}

		if(!$scope.checkError($scope.ExcelData,null,4)){
			// ngDialog.closeAll();
			// return false
		}

		if(!$scope.checkError($scope.ExcelData,null,5)){
			// ngDialog.closeAll();
			// return false
		}

		if(!$scope.checkError($scope.ExcelData,tmpDataStatusVehicleId,6)){
			// ngDialog.closeAll();
			// return false
		}
		if(!$scope.checkError($scope.ExcelData,null,7)){
			// ngDialog.closeAll();
			// return false
		}

		if($scope.allError.length > 0){
			console.log('===== this fkn error ==', $scope.allError );
			if(!$scope.showMessageAllerror($scope.allError)){
				$scope.showUploadedError = true;
				ngDialog.closeAll();
				return false
			}else{
				$scope.showUploadedError = false;
			}
		}
		// tmpDataModelId.sort();
		console.log('tmpmodelid',tmpDataModelId);
		var findDataByModel = function(data, x){
			if(x > (tmpDataModelId.length-1)){
				return
			}
			if(data[x] == undefined){
				bsAlert.warning("Model Kendaraan Tidak Ditemukan")
				ngDialog.closeAll();
				return false;
			}
			TaskListBPFactory.getFullModelFromModel(data[x]).then(function(res){
				tmpVehicleType[data[x]] = [];
				var jsonData = [];
				angular.forEach(res.data.Result,function(value,key){
					console.log('tmpDataModelId[xxx] ====>', data[x]);
					jsonData.push({name:value.VehicleTypeName.split(' ')[0],value:value.VehicleTypeId})
				});
				tmpVehicleType[data[x]] = jsonData;
				// if(count == (data.length )){
				// 	$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
				// }
				if($scope.ariavalue < 80){
					$scope.ariavalue += 2 + x;
					var valueLoading = angular.copy($scope.ariavalue);
					$scope.ariavaluecss = (valueLoading).toString() + '%';
				}else{
					// $scope.ariavaluecss = ($scope.ariavalue).toString() + '%';
				}
				if(x < (tmpDataModelId.length-1)){
					findDataByModel(data, x + 1);
					
				}else if((tmpDataModelId.length - 1) == x){
					$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType, tmpexcelarray);
				}
			});
		}
		findDataByModel(tmpDataModelId, 0)
		// for(var xxx = 0; xxx <= tmpDataModelId.length; xxx++){
		// 	TaskListBPFactory.getFullModelFromModel(tmpDataModelId[xxx]).then(function(res){
		// 		tmpVehicleType[tmpDataModelId[count]] = [];
		// 		var jsonData = [];
		// 		angular.forEach(res.data.Result,function(value,key){
		// 			console.log('tmpDataModelId[xxx] ====>', tmpDataModelId[count]);
		// 			jsonData.push({name:value.VehicleTypeName.split(' ')[0],value:value.VehicleTypeId})
		// 		});
		// 		tmpVehicleType[tmpDataModelId[count]] = jsonData;
		// 		count++
		// 		if(count == (tmpDataModelId.length )){
		// 			$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
		// 		}
		// 	});
		// }
		// angular.forEach($scope.ExcelData, function(value, key){
		// 	var ValidFrom = new Date(value.Valid_From);
		// 	var ValidTo = new Date(value.Valid_To);
			
		// 	// if(isNaN(ValidFrom) || isNaN(ValidTo)){
		// 	// 	isValid = 0;
		// 	// }
		// 	for(var i in $scope.optionsTaskListBPMain_ModelKendaraan){
		// 		console.log('======>>', value.Model_Kendaraan, '==>', $scope.optionsTaskListBPMain_ModelKendaraan[i].name)
		// 		if(value.Model_Kendaraan.toLowerCase() == $scope.optionsTaskListBPMain_ModelKendaraan[i].name.toLowerCase() ){
		// 			value.VehicleModelId = $scope.optionsTaskListBPMain_ModelKendaraan[i].value;
		// 			break;
		// 		}
		// 	}
		// 	// optionsTaskListBPMain_FullModel
		// 	// TaskListBPFactory.getFullModelFromModel(value.VehicleModelId).then(function(res){
		// 	// 	var tempData = [];
		// 	// 	angular.forEach(res.data.Result,function(value,key){
		// 	// 		// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
		// 	// 		tempData.push({name:value.VehicleTypeName,value:value.VehicleTypeId})
		// 	// 	});
		// 	// 	$scope.optionsTaskListBPMain_FullModel = tempData;
		// 	// });
		// 	// for(var j in $scope.optionsTaskListBPMain_FullModel){

		// 	// }

		// 	eachData = {
		// 		"TaskListBPId" : $scope.TaskListBPEdit.TaskId,
		// 		"OutletId" : $scope.TaskListBPEdit.OutletId,
		// 		"DealerCode" : value.Dealer_Code, 
		// 		"OutletCode" : value.Outlet_Code, 
		// 		"WMI": value.WMI, 
		// 		"VDS": value.VDS, 
		// 		"VehicleTypeName": value.Full_Model, 
		// 		"ModelName" : value.Model_Kendaraan, 
		// 		"VehicleModelId" : value.VehicleModelId, 
		// 		"VehicleTypeId" : value.VehicleTypeId, 
		// 		"OperationNo": value.Operation_No, 
		// 		"ActivityTypeId" : value.Tipe_Perkerjaan, 
		// 		"TaskName" : value.Nama_Pekerjaan, 
		// 		"Qty" : value.Jumlah, 
		// 		"UOM" : value.UOM, 
		// 		"HourAmount" : value.Jumlah_Hour, 
		// 		"ServiceRate": value.Harga_Jasa, 
		// 		"WarrantyRate":value.Warranty_Rate, 
		// 		"DamageLevel" : value.Tingkat_Kerusakan, 
		// 		"BodyEstimationMinute": value.Body_Time, 
		// 		"PuttyEstimationMinute": value.Putty_Time, 
		// 		"SurfacerEstimationMinute": value.Surfacer_Time, 
		// 		"SprayingEstimationMinute": value.Spraying_Time, 
		// 		"PolishingEstimationMinute": value.Polishing_Time, 
		// 		"ReassemblyEstimationMinute": value.Reassembly_Time, 
		// 		"FIEstimationMinute": value.Final_Inspection_Time, 
		// 		"TotalEstimationMinute": value.Total_Time, 
		// 		"UploadDate": value.UploadDate, 
		// 		"ValidFrom": value.Valid_From,
		// 		"ValidThru": value.Valid_To, 
		// 		"PreparedBy": value.Prepared_By, 
		// 		"PreparedDate": value.Prepared_Date,
		// 		"PointId": value.Tingkat_Kerusakan,
		// 		"AreaId": value.No_Area,
		// 		"StatusVehicleId": value.Metode_Perbaikan
		// 	}
			
		// 	inputData.push(eachData);
		// });
		// console.log('tempData', tmpExcelData);
		// console.log('inputData', inputData);
		// console.log('tmpDataModelId', tmpDataModelId);
		return false
		
	}
	
	$scope.TaskListBPEdit_ClearFields = function(){
		$scope.TaskListBPEdit.TaskId = 0;
		$scope.TaskListBPEdit.OutletId = 0;
		$scope.TaskListBPEdit.OutletCode = '';
		$scope.TaskListBPEdit.WMI = '';
		$scope.TaskListBPEdit.VDS = '';
		// $scope.TaskListBPEdit.TypeName = ''; 
		$scope.TaskListBPEdit.FullModel = 0; 
		// $scope.TaskListBPEdit.ModelName = '';
		$scope.TaskListBPEdit.Model = 0;
		$scope.TaskListBPEdit.OperationNo = ''; 
		$scope.TaskListBPEdit.TipePekerjaan = ''; 
		$scope.TaskListBPEdit.TaskName = '';
		$scope.TaskListBPEdit.OperationUOM = '';
		$scope.TaskListBPEdit.ServiceRate = ''; 
		$scope.TaskListBPEdit.WarrantyRate = '';
		$scope.TaskListBPEdit.EstimasiBody = '';
		$scope.TaskListBPEdit.EstimasiPutty = '';
		$scope.TaskListBPEdit.EstimasiSurfacer = '';
		$scope.TaskListBPEdit.EstimasiSpraying = '';
		$scope.TaskListBPEdit.EstimasiPolishing = '';
		$scope.TaskListBPEdit.EstimasiReassembly = ''; 
		$scope.TaskListBPEdit.EstimasiFI = ''; 
		$scope.TaskListBPEdit.TotalEstimasi = ''; 
		$scope.TaskListBPEdit.TanggalAwal = ''; 
		$scope.TaskListBPEdit.TanggalAkhir = '';
		$scope.TaskListBPEdit.PointId = '';
		$scope.TaskListBPEdit.AreaId = '';
		$scope.TaskListBPEdit.StatusVehicleId = '';
	}
	
	$scope.TaskListBP_Edit_Simpan_Clicked = function(){
		var ValidFrom = '';
		var ValidTo = '';
		
		if(!angular.isUndefined($scope.TaskListBPEdit.TanggalAwal))
			ValidFrom = $filter('date')(new Date($scope.TaskListBPEdit.TanggalAwal.toLocaleString()), "yyyy-MM-dd");
		
		if(!angular.isUndefined($scope.TaskListBPEdit.TanggalAkhir))
			ValidTo = $filter('date')(new Date($scope.TaskListBPEdit.TanggalAkhir.toLocaleString()), "yyyy-MM-dd");
		
		var inputData=[{
				"TaskListBPId" 				: $scope.TaskListBPEdit.TaskId,
				"OutletId" 					: $scope.TaskListBPEdit.OutletId,//user.OutletId,
				"OutletCode" 				: $scope.TaskListBPEdit.OutletCode,
				"WMI" 						: $scope.TaskListBPEdit.WMI,
				"VDS" 						: $scope.TaskListBPEdit.VDS,
				// "VehicleTypeName"			: $scope.TaskListBPEdit.TypeName, 
				"VehicleTypeId"				: $scope.TaskListBPEdit.FullModel, 
				"VehicleModelId" 			: $scope.TaskListBPEdit.Model, 
				// "ModelName" 				: $scope.TaskListBPEdit.ModelName, 
				"OperationNo"				: $scope.TaskListBPEdit.OperationNo, 
				"ActivityTypeId" 			: $scope.TaskListBPEdit.TipePekerjaan, 
				"TaskName" 					: $scope.TaskListBPEdit.TaskName, 
				"UOMId" 					: $scope.TaskListBPEdit.OperationUOM, 
				"ServiceRate"				: $scope.TaskListBPEdit.ServiceRate, 
				"WarrantyRate"				: $scope.TaskListBPEdit.WarrantyRate, 
				"BodyEstimationMinute"		: $scope.TaskListBPEdit.EstimasiBody, 
				"PuttyEstimationMinute"		: $scope.TaskListBPEdit.EstimasiPutty, 
				"SurfacerEstimationMinute"	: $scope.TaskListBPEdit.EstimasiSurfacer, 
				"SprayingEstimationMinute"	: $scope.TaskListBPEdit.EstimasiSpraying, 
				"PolishingEstimationMinute"	: $scope.TaskListBPEdit.EstimasiPolishing, 
				"ReassemblyEstimationMinute": $scope.TaskListBPEdit.EstimasiReassembly, 
				"FIEstimationMinute"		: $scope.TaskListBPEdit.EstimasiFI, 
				"TotalEstimationMinute"		: $scope.TaskListBPEdit.TotalEstimasi, 
				"ValidFrom"					: $scope.TaskListBPEdit.TanggalAwal,
				"ValidThru"					: $scope.TaskListBPEdit.TanggalAkhir,
				"PointId"					: $scope.TaskListBPEdit.PointId,
				"AreaId"					: $scope.TaskListBPEdit.AreaId,
				"StatusVehicleId"			: $scope.TaskListBPEdit.StatusVehicleId,
				"Qty"						: 1,
				// "ValidFrom"					: ValidFrom,
				// "ValidThru"					: ValidTo,
				"UploadDate" 				: undefined, 
				"PreparedDate" 				: undefined
				
		}];
		console.log($scope.TaskListBPEdit.OperationUOM);
		
		TaskListBPFactory.create(inputData)
		.then(
			function(res){
				alert("Data berhasil disimpan");
				$scope.TaskListBP_Edit_Batal_Clicked();
				$scope.TaskListBPGrid_Paging(1);
			},
			function(err){
				alert("Input data Harus benar");
			}
		);
	}
	
	$scope.TaskListBP_Edit_Batal_Clicked = function(){
		$scope.TaskListBPEdit_ClearFields();
		$scope.TaskListBPMain_Show = true;
		$scope.TaskListBPEdit_Show = false;
	}

	// =======Adding Custom Filter==============
	$scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
	var x = -1;
	for (var i = 0; i < $scope.TaskListBPGrid.columnDefs.length; i++) {
	    if ($scope.TaskListBPGrid.columnDefs[i].visible == undefined && $scope.TaskListBPGrid.columnDefs[i].name !== 'Action') {
	        x++;
	        $scope.gridCols.push($scope.TaskListBPGrid.columnDefs[i]);
	        $scope.gridCols[x].idx = i;
	    }
	    console.log("$scope.gridCols", $scope.gridCols);
	    console.log("$scope.grid.columnDefs[i]", $scope.TaskListBPGrid.columnDefs[i]);
	}
	$scope.filterBtnLabel = $scope.gridCols[0].name;
	$scope.filterBtnChange = function(col) {
	    console.log("col", col);
	    $scope.filterBtnLabel = col.name;
	    $scope.filterColIdx = col.idx + 1;
	};
	// $scope.TaskListBPEditRow = function(row){
		// $scope.TaskListBPEdit_CheckId = row.entity.CheckId;
		// $scope.TaskListBPEdit_Detail = row.entity.CheckDesc;
		// $scope.TaskListBPMain_Show = false;
		// $scope.TaskListBPEdit_Show = true;
	// }
	
	// $scope.TaskListBP_Edit_Batal_Clicked = function(){
		// var inputData = [
		// {
			// "CheckId":$scope.TaskListBPEdit_WMI,
			// "CheckDesc": $scope.TaskListBPEdit_VDS,
			// TaskListBPEdit_OperationNo,
			// TaskListBPEdit_TaskName,
			
			
		// }];
		
		// TaskListBPFactory.create(inputData)
		// .then(
			// function(res){
				// alert("Data berhasil disimpan");
				// $scope.TaskListBPMain_GetAllData();
				// $scope.TaskListBPMain_Show = true;
				// $scope.TaskListBPEdit_Show = false;
			// },
			// function(err){
				// alert(err.data.ExceptionMessage);
			// }
		// );

	// }
	
	// $scope.TaskListBP_Edit_Simpan_Clicked = function(){
		// $scope.TaskListBP_Edit_Batal_Clicked();
	// }
});