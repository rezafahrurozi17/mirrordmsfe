angular.module('app')
	.factory('TaskListBPFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
	return{
		getTaskListBPData: function(start, limit, modelKendaraan, operationDescription, activityTypeId, NameSearch, NamaModel){
			// var url = '/api/as/AfterSalesMasterTaskListBP/GetListDataTaskListBP/?Start=' + start + '&limit=' + limit + '&ModelKendaraan=' + modelKendaraan
			// 			+ '&OperationDescription=' + operationDescription + '&OutletId=' + user.OutletId + '&ActivityTypeId =' + activityTypeId;
			// console.log("url TaxType : ", url);
			// var res=$http.get(url);
			var param = {
				start : start ,
				limit : limit ,
				VehicleModelId : modelKendaraan || "-" ,
				ActivityTypeId : activityTypeId || '0' ,
				// OperationDescription : operationDescription,
				TaskName : operationDescription || "-" ,
				// NamaModel : VehicleModelNameSearch || "" ,
				// VehicleModelNameSearch : VehicleModelNameSearch || "" ,
				// ActivityTypeNameSearch : modelKendaraan || "" ,
				// TaskNameSearch : modelKendaraan || "" ,
				// FullModelSearch : modelKendaraan || "" ,
				OutletId : user.OutletId 
				// CategoryId : categoryId
			}

			if(NamaModel)
				param[NamaModel] = NameSearch || "";

			var res = $http.get('/api/as/AfterSalesMasterTaskListBP/GetListDataTaskListBP',{
				params : param
			});

			return res;
		},

		getDataTypeSearch : function(){
			var data = [
				{
					name : "Model Kendaraan",
					value : "VehicleModelNameSearch"
				},
				{
					name : "Nama Pekerjaan",
					value : "ActivityTypeNameSearch"
				},
				{
					name : "Activity Type",
					value : "TaskNameSearch"
				},
				{
					name : "Full Model",
					value : "FullModelSearch"
				}
				
			];

			return data;
		},
		
		create:function(inputData){
			var url = '/api/as/AfterSalesMasterTaskListBP/SaveData/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},

		UploadData:function(uploadExcel){
			var url = '/api/as/AfterSalesMasterTaskListBP/UploadData/';
			var param = JSON.stringify(uploadExcel);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		getDataUOM: function(){
			var url = '/api/as/AfterSalesMasterTaskListBP/GetDataUOM';
			console.log("url UOM List : ", url);
			var res=$http.get(url);
			return res;
			
		},
		getMasterCategoryId: function() {
			var url = '/api/as/GlobalMaster?CategoryId=2037&Flag=1';
			console.log("url PointId List : ", url);
			var res=$http.get(url);
			return res;
		},
		getMasterCategoryId1: function() {
			var url = '/api/as/GlobalMaster?CategoryId=2036&Flag=1';
			console.log("url PointId List : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataCategory: function(){
			var url = '/api/as/AfterSalesMasterTaskListBP/GetDataCategory';
			console.log("url Category List : ", url);
			var res=$http.get(url);
			return res;
			
		},

		changeFormatDate: function(item) {
			var tmpAppointmentDate = item;
			console.log("changeFormatDate item", item);
			var finalDate
			if (item == null || item == undefined) {
				finalDate = null;
			} else {
				tmpAppointmentDate = new Date(tmpAppointmentDate);

				if (tmpAppointmentDate !== null && tmpAppointmentDate !== undefined) {
					var yyyy = tmpAppointmentDate.getFullYear().toString();
					var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
					var dd = tmpAppointmentDate.getDate().toString();
					finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
				} else {
					finalDate = '';
				}
			}

			console.log("changeFormatDate finalDate", finalDate);
			return finalDate;
		},
		
		DeleteDataBP: function(taskListId){
			var url = '/api/as/AfterSalesMasterTaskListBP/DeleteData/?TaskListId=' + taskListId;
			console.log("url Category List : ", url);
			var res=$http.delete(url);
			return res;
		},

		getDataVehicleModel: function(){
			return $http.get("/api/as/AfterSalesMasterTaskListBP/GetDataModel");
		},
		getFullModelFromModel: function(modelId){
			return $http.get("/api/as/AfterSalesMasterTaskListBP/GetDataFullModel",{
				params: {
					VehicleModelId : modelId
				}
			});
		},
		getVehicleTypeById: function(id) {
			var url = '/api/crm/GetCVehicleTypeById/' + id
			var res = $http.get(url);
			return res;
		},

		CekTasklistBP: function (TaskName, TasklistBPId,VehicleModelId,VehicleTypeId) {
			var url  = '/api/as/AfterSalesMasterTaskListBP/CekTasklistBP?TaskName=' + TaskName + '&TasklistBPId=' + TasklistBPId + '&VehicleModelId='+ VehicleModelId + '&VehicleTypeId=' + VehicleTypeId;
			var res = $http.get(url);
			return res;
		}, 
		CekTasklistBPUpload: function (TaskName, TasklistBPId,VehicleModelId) {
			var url  = '/api/as/AfterSalesMasterTaskListBP/CekTasklistBPUpload?TaskName=' + TaskName + '&TasklistBPId=' + TasklistBPId + '&VehicleModelId='+ VehicleModelId;
			var res = $http.get(url);
			return res;
		}
	}
});