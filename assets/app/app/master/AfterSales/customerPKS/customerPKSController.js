var app = angular.module('app');
app.controller('CustomerPKSController', function ($scope, $http, $filter, CurrentUser, bsNotify, CustomerPKSFactory, $timeout, uiGridConstants, bsAlert) {
	var user = CurrentUser.user();
	var outletId = user.OutletId;

	//general
	$scope.uiGridPageSize = 10;
	$scope.uiGridTotalData = 0;
	$scope.rowIndexToBeRemoved = undefined;

	//##### MAIN PAGE #####
	$scope.CPKS_Main_Body_Show = true;
	$scope.CPKS_Main_IsNewData = true;
	$scope.CPKS_AddDisableGroupType = false;
	$scope.CPKS_TotalData = 0;
	$scope.CPKS_Main_RowIndexToBeRemoved = undefined;
	$scope.CPKS_MainFilterColumnList = [{name: "Tipe Group", value: 1}, {name: "Toyota ID", value: 2}, {name: "No PKS", value: 3},
										{name: "Nama Perusahaan", value: 4}, {name: "Alamat Penagihan", value: 5}, {name: "NPWP", value: 6},
										{name: "Alamat NPWP", value: 7}, {name: "Contact", value: 8}, {name: "Periode Mulai", value: 9},
										{name: "Periode Berakhir", value: 10}, {name: "Top", value: 11}, {name: "Saldo", value: 12}];

	$scope.DateOptions = { // ini huruf gede kecil nya ngefek.. yg atas ga dihapus takut kepake
		startingDay: 1,
		// firstDay: 1,
		format: 'dd/MM/yyyy',
		//disableWeekend: 1
	};
										
	$scope.gridPKSTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.CPKS_Main_UIGrid.data != undefined){
            if($scope.CPKS_Main_UIGrid.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.CPKS_Main_UIGrid.data.length < 10){
                return {
                    height: ($scope.CPKS_Main_UIGrid.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }
    };


	$scope.numberWithCommas = function(x) {
        console.log(x);
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}
	
	//MAIN (UI GRID)
	$scope.CPKS_Main_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		useCustomPagination: true,
        useExternalPagination: true,
		enableFiltering:true,
		rowSelection : true,
		multiSelect : false,
		enableHorizontalScrollbar: 1,
	    paginationPageSizes: [5,10,15,20,25,50],
        paginationPageSize: 10,
		columnDefs: [
						{name:"Customer PKS ID",field:"customerPKSID",visible:false},
						{name:"Tipe Group ID",field:"groupTypeId",visible:false},
						{name:"Tipe Group", width:"15%", field:"groupType",enableCellEdit:false},
						{name:"Toyota ID", displayName: "Toyota ID" , width:"15%", field:"toyotaID",enableCellEdit:false},
						{name:"No PKS", displayName:"No PKS", width:"15%", field:"PKSNum",enableCellEdit:false},
						{name:"Nama Perusahaan", width:"15%",  field:"companyName", enableCellEdit:false},
						{name:"Alamat Penagihan", width:"15%", field:"billingAddress", enableCellEdit:false},
						{name:"NPWP", displayName:"NPWP" , width:"15%", field:"npwp", enableCellEdit:false},
						{name:"Alamat NPWP", displayName:"Alamat NPWP",width:"15%", field:"npwpAddress", enableCellEdit:false},
						{name:"Contact", width:"15%", field:"contactPerson", enableCellEdit:false},
						{name: "Periode Mulai", cellFilter: 'date:\"dd-MM-yyyy\"', width:"15%", field:"startPeriod", enableCellEdit:false},
						{name: "Periode Berakhir", cellFilter: 'date:\"dd-MM-yyyy\"', width:"15%", field:"endPeriod", enableCellEdit:false},
						{name:"Top", displayName:"TOP",width:"15%", field:"top", enableCellEdit:false, cellFilter: 'CurrencyTitik'},
						{name:"Saldo", width:"15%", field:"balance", enableCellEdit:false, cellFilter: 'CurrencyTitik'},
						{name:"Action", width:"15%",
							cellTemplate:'<a class="fa fa-pencil fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 4px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Main_Edit_Button_Clicked(row)" title="Ubah"/a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="fa fa-list-alt fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 7px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Main_View_Button_Clicked(row)" title="Lihat"/a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="fa fa-fw fa-lg fa-trash" style="box-shadow:none!important;color:#777;margin:1px 4px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Main_Delete_Button_Clicked(row)" title="Hapus"/a>'}
					],
		onRegisterApi: function(gridApi) {
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.CPKS_Main_UIGrid_Paging(pageNumber,pageSize);
			});
		}
	};

	$scope.CPKS_Main_UIGrid_Paging = function(pageNumber,pageSize){
		var filterColumn = ($scope.CPKS_MainFilterColumn == undefined) ? 0 : $scope.CPKS_MainFilterColumn;
		var filterText = ($scope.CPKS_MainFilterText == undefined) ? "" : $scope.CPKS_MainFilterText;

		if(pageSize == undefined){
			pageSize = $scope.uiGridPageSize;
		}
		CustomerPKSFactory.getTotalData_CustomerPKS(filterColumn, filterText)
		.then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					angular.forEach(res.data.Result, function(value, key){
						$scope.CPKS_TotalData = value.TotalData;
					});
				}

				CustomerPKSFactory.getData_CustomerPKS(pageNumber, pageSize, filterColumn, filterText)
				.then(
					function(res)
					{
						if(!angular.isUndefined(res.data.Result)){
							var grid_data = [];
							angular.forEach(res.data.Result, function(value, key){
								var tempTOP;
								var tempBalance;
								if (value.PaymentMethod.toLowerCase() == 'top')
								{
									tempTOP = value.TOP;
									tempBalance = 0;
								}
								else if (value.PaymentMethod.toLowerCase() == 'saldo')
								{
									tempTOP = 0;
									tempBalance = value.Balance;
								}
								grid_data.push({
									"customerPKSID"		: value.CustomerPKSId,
									"groupTypeId"		: value.GroupTypeId,
									"groupType"			: value.GroupType,
									"toyotaID"			: value.ToyotaId,
									"PKSNum"			: value.NoPKS,
									"companyName"		: value.CompanyName,
									"billingAddress"	: value.BillingAddress,
									"npwp"				: value.NPWP,
									"npwpAddress"		: value.NPWPAddress,
									"contactPerson"		: value.PICName,
									"startPeriod"		: $filter('date')(new Date(value.StartPeriod), "yyyy-MM-dd"),
									"endPeriod"			: $filter('date')(new Date(value.EndPeriod), "yyyy-MM-dd"),
									"top"				: tempTOP,
									"balance"			: tempBalance,
									"PaymentMethod"		: value.PaymentMethod
								});
							});

							$scope.CPKS_Main_UIGrid.data = grid_data;
							// $scope.CPKS_Main_UIGrid.totalItems = ($scope.CPKS_TotalData<res.data.Total)? res.data.Total : $scope.CPKS_TotalData;
							// $scope.CPKS_Main_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							// $scope.CPKS_Main_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
							$scope.CPKS_Main_UIGrid.paginationCurrentPage = pageNumber;
							$scope.CPKS_Main_UIGrid.totalItems = res.data.Result[0].TotalData;
							$scope.CPKS_GetFullData();
						}
					}
				);
			}
		);
		
	};

	$scope.CPKS_Main_UIGrid_Paging(1);

	$scope.CPKS_Main_Filter_Button_Clicked = function ()
	{
		$scope.CPKS_Main_UIGrid_Paging(1);
	}

	$scope.CPKS_Main_Add_Button_Clicked = function ()
	{
		$scope.CPKS_Add_ResetVariable();
		$scope.CPKS_AddDisableGroupType = false;
		$scope.CPKS_AddDisableField = false;
		$scope.CPKS_AddPaymentMethod = 'saldo';
		$scope.CPKS_AddDisableTOPNum = true;
		$scope.CPKS_AddDisableToyotaID = false;
		$scope.CPKS_AddSaveCancel_Button_Show = true;
		$scope.CPKS_AddBack_Button_Show = false;
		$scope.CPKS_Main_Body_Show = false;
		$scope.CPKS_Add_Body_Show = true;
		$scope.CPKS_Main_IsNewData = true;
		$scope.CPKS_AddDisableBalanceText = false;
		$scope.CPKS_AddButtons_Show = true;
		$scope.CPKS_Add_UIGrid.data = [];
		$scope.CPKS_Add_Current_CustomerPKSID = -1;
		$scope.disableNamaAlamat = false;
		$scope.CPKS_AddDisableTOPNum1 = false;
		$scope.pks_edit = true;
		$scope.pks_hapus = true;
		// $scope.CPKS_AddDisableTOPNum = false;
	}

	$scope.CPKS_Main_Edit_Button_Clicked = function(row) {
		var index = $scope.CPKS_Main_UIGrid.data.indexOf(row.entity);
		$scope.CPKS_Add_Current_CustomerPKSID = $scope.CPKS_Main_UIGrid.data[index].customerPKSID;
		$scope.CPKS_Add_ResetVariable();
		$scope.CPKS_AddDisableGroupType = true;
		$scope.CPKS_AddDisableField = false;
		$scope.CPKS_AddDisableToyotaID = true;
		$scope.CPKS_AddSaveCancel_Button_Show = true;
		$scope.CPKS_AddBack_Button_Show = false;
		$scope.CPKS_Main_IsNewData = false;
		$scope.CPKS_AddDisableBalanceText = false;
		$scope.CPKS_AddDisableTOPNum = false;
		$scope.CPKS_AddButtons_Show = true;
		$scope.disableNamaAlamat = false;
		$scope.pks_edit = true;
		$scope.pks_hapus = true;
		

		var startPeriod;
		var endPeriod;
		var pksNum;
		var paymentMethod;
		var top;
		var balance;

		paymentMethod = $scope.CPKS_Main_UIGrid.data[index].PaymentMethod.toLowerCase();
		if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_Main_UIGrid.data[index].groupTypeId].toLowerCase() == 'pks') //PKS
		{
			startPeriod = $scope.CPKS_Main_UIGrid.data[index].startPeriod
			endPeriod = $scope.CPKS_Main_UIGrid.data[index].endPeriod;
			pksNum = $scope.CPKS_Main_UIGrid.data[index].PKSNum;

			top = $scope.CPKS_Main_UIGrid.data[index].top;
			balance = $scope.CPKS_Main_UIGrid.data[index].balance;

			if (paymentMethod == 'saldo'){
				if (top == undefined || top == 0)
				{
					top = 0;
					$scope.CPKS_AddDisableTOPNum = true;
				}
			}
			if(paymentMethod == 'top'){
				if (balance == undefined || balance == 0)
				{
					balance = 0;
					$scope.CPKS_AddDisableTOPNum = false;
				}
			}

			// if (top == undefined || top == 0)
			// {
			// 	top = 0;
			// 	paymentMethod = "saldo";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
			// else if (balance == undefined || balance == 0)
			// {
			// 	balance = 0;
			// 	paymentMethod = "top";
			// 	$scope.CPKS_AddDisableTOPNum = false;
			// }
		}
		else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_Main_UIGrid.data[index].groupTypeId].toLowerCase() == 'affiliated company') //Afco
		{
			// startPeriod = "";
			// endPeriod = "";
			// pksNum = "";
			// paymentMethod="";
			// top = "";
			// balance = "";
			startPeriod = $scope.CPKS_Main_UIGrid.data[index].startPeriod
			endPeriod = $scope.CPKS_Main_UIGrid.data[index].endPeriod;
			pksNum = $scope.CPKS_Main_UIGrid.data[index].PKSNum;

			top = $scope.CPKS_Main_UIGrid.data[index].top;
			balance = $scope.CPKS_Main_UIGrid.data[index].balance;

			if (paymentMethod == 'saldo'){
				if (top == undefined || top == 0)
				{
					top = 0;
					$scope.CPKS_AddDisableTOPNum = true;
				}
			}
			if(paymentMethod == 'top'){
				if (balance == undefined || balance == 0)
				{
					balance = 0;
					$scope.CPKS_AddDisableTOPNum = false;
				}
			}

			// if (top == undefined || top == 0)
			// {
			// 	top = 0;
			// 	paymentMethod = "saldo";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
			// else if (balance == undefined || balance == 0)
			// {
			// 	balance = 0;
			// 	paymentMethod = "top";
			// 	$scope.CPKS_AddDisableTOPNum = false;
			// }
		}

		$scope.CPKS_AddGroupType = $scope.CPKS_Main_UIGrid.data[index].groupTypeId;
		$scope.CPKS_AddToyotaIDText = $scope.CPKS_Main_UIGrid.data[index].toyotaID;
		$scope.CPKS_AddContactPersonText = $scope.CPKS_Main_UIGrid.data[index].contactPerson;
		$scope.CPKS_AddPKSNumText = pksNum;
		$scope.CPKS_AddCompanyNameText = $scope.CPKS_Main_UIGrid.data[index].companyName;
		$scope.CPKS_AddStartPeriod = new Date(startPeriod);
		$scope.CPKS_AddEndPeriod = new Date(endPeriod);
		$scope.CPKS_AddBillingAddress = $scope.CPKS_Main_UIGrid.data[index].billingAddress;
		$scope.CPKS_AddPaymentMethod = paymentMethod;
		$scope.CPKS_AddBalanceText = balance;
		$scope.CPKS_AddTOPNum = top;
		$scope.CPKS_AddNPWPText = $scope.CPKS_Main_UIGrid.data[index].npwp;
		$scope.CPKS_AddNPWPAddress = $scope.CPKS_Main_UIGrid.data[index].npwpAddress;
		$scope.CPKS_AddFilterText = "";
		$scope.CPKS_AddFilterColumn = undefined;
		$scope.CPKS_Add_UIGrid_Paging(1);

		$scope.CPKS_Main_Body_Show = false;
		$scope.CPKS_Add_Body_Show = true;
	};

	$scope.CPKS_Main_View_Button_Clicked = function(row) {
		var index = $scope.CPKS_Main_UIGrid.data.indexOf(row.entity);
		$scope.CPKS_Add_Current_CustomerPKSID = $scope.CPKS_Main_UIGrid.data[index].customerPKSID
		$scope.CPKS_Add_ResetVariable();
		$scope.CPKS_AddDisableGroupType = true;
		$scope.CPKS_AddDisableField = true;
		$scope.CPKS_AddDisableToyotaID = true;
		$scope.CPKS_AddSaveCancel_Button_Show = false;
		$scope.CPKS_AddBack_Button_Show = true;
		$scope.CPKS_AddDisableBalanceText = true;
		$scope.CPKS_AddDisableTOPNum = true;
		$scope.CPKS_AddDisableTOPNum1 = true;
		$scope.CPKS_AddButtons_Show = false;
		$scope.disableNamaAlamat = true;
		$scope.pks_edit = false;
		$scope.pks_hapus = false;
		
		var startPeriod;
		var endPeriod;
		var pksNum;
		var paymentMethod;
		var top;
		var balance;

		paymentMethod = $scope.CPKS_Main_UIGrid.data[index].PaymentMethod.toLowerCase();
		if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_Main_UIGrid.data[index].groupTypeId].toLowerCase() == 'pks') //PKS
		{
			startPeriod = $scope.CPKS_Main_UIGrid.data[index].startPeriod
			endPeriod = $scope.CPKS_Main_UIGrid.data[index].endPeriod;
			pksNum = $scope.CPKS_Main_UIGrid.data[index].PKSNum;

			top = $scope.CPKS_Main_UIGrid.data[index].top;
			balance = $scope.CPKS_Main_UIGrid.data[index].balance;

			if (paymentMethod == 'saldo'){
				if (top == undefined || top == 0)
				{
					top = 0;
					$scope.CPKS_AddDisableTOPNum = true;
				}
			}
			if(paymentMethod == 'top'){
				if (balance == undefined || balance == 0)
				{
					balance = 0;
					$scope.CPKS_AddDisableTOPNum = false;
				}
			}

			// if (top == undefined || top == 0)
			// {
			// 	top = 0;
			// 	paymentMethod = "saldo";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
			// else if (balance == undefined || balance == 0)
			// {
			// 	balance = 0;
			// 	paymentMethod = "top";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
		}
		else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_Main_UIGrid.data[index].groupTypeId].toLowerCase() == 'affiliated company') //Afco
		{
			// startPeriod = "";
			// endPeriod = "";
			// pksNum = "";
			// paymentMethod="";
			// top = "";
			// balance = "";
			startPeriod = $scope.CPKS_Main_UIGrid.data[index].startPeriod
			endPeriod = $scope.CPKS_Main_UIGrid.data[index].endPeriod;
			pksNum = $scope.CPKS_Main_UIGrid.data[index].PKSNum;

			top = $scope.CPKS_Main_UIGrid.data[index].top;
			balance = $scope.CPKS_Main_UIGrid.data[index].balance;

			if (paymentMethod == 'saldo'){
				if (top == undefined || top == 0)
				{
					top = 0;
					$scope.CPKS_AddDisableTOPNum = true;
				}
			}
			if(paymentMethod == 'top'){
				if (balance == undefined || balance == 0)
				{
					balance = 0;
					$scope.CPKS_AddDisableTOPNum = false;
				}
			}

			// if (top == undefined || top == 0)
			// {
			// 	top = 0;
			// 	paymentMethod = "saldo";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
			// else if (balance == undefined || balance == 0)
			// {
			// 	balance = 0;
			// 	paymentMethod = "top";
			// 	$scope.CPKS_AddDisableTOPNum = true;
			// }
		}

		$scope.CPKS_AddGroupType = $scope.CPKS_Main_UIGrid.data[index].groupTypeId;
		$scope.CPKS_AddToyotaIDText = $scope.CPKS_Main_UIGrid.data[index].toyotaID;
		$scope.CPKS_AddContactPersonText = $scope.CPKS_Main_UIGrid.data[index].contactPerson;
		$scope.CPKS_AddPKSNumText = pksNum;
		$scope.CPKS_AddCompanyNameText = $scope.CPKS_Main_UIGrid.data[index].companyName;
		$scope.CPKS_AddStartPeriod = new Date(startPeriod);
		$scope.CPKS_AddEndPeriod = new Date(endPeriod);
		$scope.CPKS_AddBillingAddress = $scope.CPKS_Main_UIGrid.data[index].billingAddress;
		$scope.CPKS_AddPaymentMethod = paymentMethod;
		$scope.CPKS_AddBalanceText = balance;
		$scope.CPKS_AddTOPNum = top;
		$scope.CPKS_AddNPWPText = $scope.CPKS_Main_UIGrid.data[index].npwp;
		$scope.CPKS_AddNPWPAddress = $scope.CPKS_Main_UIGrid.data[index].npwpAddress;
		$scope.CPKS_AddFilterText = "";
		$scope.CPKS_AddFilterColumn = undefined;
		$scope.CPKS_Add_UIGrid_Paging(1);

		$scope.CPKS_Main_Body_Show = false;
		$scope.CPKS_Add_Body_Show = true;
	};

	$scope.CPKS_Main_Delete_Button_Clicked = function(row) {
		$scope.CPKS_Main_RowIndexToBeRemoved = $scope.CPKS_Main_UIGrid.data.indexOf(row.entity);
		//angular.element('#CPKS_Main_DeleteQuestion_Modal').modal('show');
		bsAlert.alert({
			title: "Apakah Anda yakin ingin menghapus ",
			text: "",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak",
		},
		function () {
			CustomerPKSFactory.delete_CustomerPKS($scope.CPKS_Main_UIGrid.data[$scope.CPKS_Main_RowIndexToBeRemoved].customerPKSID)
			.then(
				function(res){
					$scope.CPKS_Main_UIGrid.data.splice($scope.CPKS_Main_RowIndexToBeRemoved, 1);
					$scope.CPKS_TotalData -= 1;
					$scope.CPKS_Main_UIGrid.totalItems = $scope.CPKS_TotalData;
					$scope.CPKS_Main_UIGrid_Paging(1);
				}
			);
		},
		function () {

		}
		
		)
	};

	// $scope.CPKS_Main_No_Delete_Button_Clicked = function () {
	// 	$scope.CPKS_Main_RowIndexToBeRemoved = undefined;
	// 	angular.element('#CPKS_Main_DeleteQuestion_Modal').modal('hide');
	// }

	// $scope.CPKS_Main_Yes_Delete_Button_Clicked = function () {
		
	// 	if ($scope.CPKS_Main_RowIndexToBeRemoved != undefined)
	// 	{
			
	// 		CustomerPKSFactory.delete_CustomerPKS($scope.CPKS_Main_UIGrid.data[$scope.CPKS_Main_RowIndexToBeRemoved].customerPKSID)
	// 		.then(
	// 			function(res){
	// 				$scope.CPKS_Main_UIGrid.data.splice($scope.CPKS_Main_RowIndexToBeRemoved, 1);
	// 				$scope.CPKS_TotalData -= 1;
	// 				$scope.CPKS_Main_UIGrid.totalItems = $scope.CPKS_TotalData;
	// 				$scope.CPKS_Main_UIGrid_Paging();
	// 			}
	// 		);

	// 		$scope.CPKS_Main_RowIndexToBeRemoved = undefined;
	// 	}
		
	// 	angular.element('#CPKS_Main_DeleteQuestion_Modal').modal('hide');
	// }

	//##### END MAIN PAGE #####

	//##### ADD CUSTOMER PKS PAGE #####
	$scope.CPKS_Add_Body_Show = false;
	$scope.CPKS_Add_DoPagination = true;
	$scope.CPKS_Add_GridApi;
	$scope.CPKS_Add_Current_CustomerPKSID = -1;
	$scope.CPKS_AddUnitPKSTotalData = 0;
	$scope.CPKS_AddGroupTypeDefaultValue = -1;
	$scope.CPKS_Add_RowIndexToBeRemoved = undefined;
	$scope.CPKS_AddGroupTypeList = [];
	$scope.CPKS_AddGroupTypeOption = [];
	$scope.CPKS_AddFilterColumnOption = [{name: "Nomor Polisi", value: 1}, {name: "Model", value: 2}, {name: "VIN", value: 3},
										{name: "Nama Penanggung Jawab", value: 4}, {name: "Alamat", value: 5}, {name: "No HP", value: 6},
										{name: "Berlaku Sampai", value: 7}];
	angular.element('#CPKS_ToyotaID_Modal').modal('hide');

	$scope.CPKS_Add_PKSGrid = [
						{name:"ID",field:"customerPKSVehicleID",visible:false},
						{name:"Nomor Polisi",field:"licenseNum",enableCellEdit:false},
						{name:"Model ID",field:"modelID",visible:false},
						{name:"Model",field:"model",enableCellEdit:false},
						{name:"VIN",displayName:"VIN",field:"vin",enableCellEdit:false},
						{name:"Nama Penanggung Jawab",field:"personInCharge",enableCellEdit:false},
						{name:"Alamat", field:"address", enableCellEdit:false},
						{name:"No HP", displayName:"No HP",field:"phoneNum", enableCellEdit:false},
						{name: "Berlaku Sampai", cellFilter: 'date:\"dd-MM-yyyy\"', field:"validUntil", enableCellEdit:false},
						{name:"Action",
							cellTemplate:'<a class="fa fa-pencil fa-fw fa-lg" ng-show="grid.appScope.pks_edit"  style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Add_Edit_Button_Clicked(row)" title="Ubah"/a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="fa fa-fw fa-lg fa-trash" ng-show="grid.appScope.pks_hapus" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Add_Delete_Button_Clicked(row)" title="Hapus"/a>'}
					];

	$scope.CPKS_Add_AfcoGrid = [
						{name:"ID",field:"customerPKSVehicleID",visible:false},
						{name:"Nomor Polisi",field:"licenseNum",enableCellEdit:false},
						{name:"Model ID",field:"modelID",visible:false},
						{name:"Model",field:"model",enableCellEdit:false},
						{name:"VIN",displayName:"VIN",field:"vin",enableCellEdit:false},
						{name:"Nama Penanggung Jawab",field:"personInCharge",enableCellEdit:false},
						{name:"Alamat", field:"address", enableCellEdit:false},
						{name:"No HP",displayName:"No HP", field:"phoneNum", enableCellEdit:false},
						{name: "Berlaku Sampai", cellFilter: 'date:\"dd-MM-yyyy\"', field:"validUntil", enableCellEdit:false},
						{name:"Action",
							cellTemplate:'<a class="fa fa-pencil fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Add_Edit_Button_Clicked(row)" title="Ubah"/a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="fa fa-fw fa-lg fa-trash" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" ng-click="grid.appScope.CPKS_Add_Delete_Button_Clicked(row)" title="Hapus"/a>'}
					];

	$scope.CPKS_Add_GetGroupTypeList = function(text) {
		CustomerPKSFactory.get_GroupTypeList()
		.then(
			function(res){
				angular.forEach(res.data.Result, function(value, key){
					$scope.CPKS_AddGroupTypeOption.push({name: value.GroupType, value: value.GroupTypeId});
					$scope.CPKS_AddGroupTypeList[value.GroupTypeId] = value.GroupType;
					if ($scope.CPKS_AddGroupTypeDefaultValue == -1)
					{
						$scope.CPKS_AddGroupTypeDefaultValue = value.GroupTypeId;
					}
				});
			}
		);
	};
	$scope.CPKS_GetFullData = function() {
		CustomerPKSFactory.getFullData()
		.then(
			function(res){
				$scope.GetFullData = res.data.Result
			}
		);
	};

	$scope.CPKS_Add_GetGroupTypeList();
	$scope.CPKS_GetFullData();

	$scope.CPKS_Add_FindToyotaID_Button_Clicked = function(text) {
		$scope.CPKS_ToyotaID_UIGrid_Paging(1);
		angular.element('#CPKS_ToyotaID_Modal').modal('show');
	};

	$scope.CPKS_Add_PaymentMethod_Radio_Changed = function ()
	{
		console.log("radio button changed");
		if ($scope.CPKS_AddPaymentMethod == 'top')
		{
			$scope.CPKS_AddDisableBalanceText = true;
			$scope.CPKS_AddDisableTOPNum = false;
			$scope.CPKS_AddTOPNum = 0;
			$scope.CPKS_AddBalanceText = 0;
		}
		else if($scope.CPKS_AddPaymentMethod == 'saldo')
		{
			$scope.CPKS_AddDisableTOPNum = true;
			$scope.CPKS_AddDisableBalanceText = false;
			$scope.CPKS_AddTOPNum = 0;
			$scope.CPKS_AddBalanceText = 0;
		}
	}

	//ADD MENU (UI GRID)
	$scope.CPKS_Add_UIGrid = {
		//paginationPageSizes: null,
		paginationPageSizes: [10,20,30,40,50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination : true,
		enableFiltering:true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: $scope.CPKS_Add_PKSGrid,
		onRegisterApi: function(gridApi) {
			$scope.CPKS_Add_GridApi = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log('dodol masuk')
				if ($scope.CPKS_Add_DoPagination == true)
				{
					$scope.CPKS_Add_UIGrid_Paging(pageNumber, pageSize);
				}
				else
				{
					$scope.CPKS_Add_DoPagination = true;
				}
			});
		}
	};

	$scope.CPKS_Add_UIGrid_Paging = function(pageNumber, pageSize){
		var filterColumn = ($scope.CPKS_AddFilterColumn == undefined) ? 0 : $scope.CPKS_AddFilterColumn;
		var filterText = ($scope.CPKS_AddFilterText == undefined) ? "" : $scope.CPKS_AddFilterText;
		if (pageSize == null || pageSize == undefined){
			pageSize = 10;
		}

		CustomerPKSFactory.getTotalData_Vehicle($scope.CPKS_Add_Current_CustomerPKSID,filterColumn, filterText)
		.then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					angular.forEach(res.data.Result, function(value, key){
						$scope.CPKS_AddUnitPKSTotalData = value.TotalData;
					});
				}

				// CustomerPKSFactory.getData_Vehicle($scope.CPKS_Add_Current_CustomerPKSID,pageNumber, $scope.uiGridPageSize, filterColumn, filterText)
				CustomerPKSFactory.getData_Vehicle($scope.CPKS_Add_Current_CustomerPKSID,pageNumber, pageSize, filterColumn, filterText)
				.then(
					function(res)
					{
						if(!angular.isUndefined(res.data.Result)){
							var grid_data = [];
							if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks')
							{
								angular.forEach(res.data.Result, function(value, key){
									for(var xx in $scope.CPKS_AddUnitPKSModelOption) {
										if(value.VehicleModelName.toLowerCase() == $scope.CPKS_AddUnitPKSModelOption[xx].name.toLowerCase()){
											value.VehicleModelId = $scope.CPKS_AddUnitPKSModelOption[xx].value
										}
									}
									grid_data.push({
										"customerPKSVehicleID" 	: value.CustomerPKSVehicleId,
										"licenseNum" 			: value.LicensePlate,
										"VehicleId"				: value.VehicleId,
										"modelID"				: value.VehicleModelId,
										"model" 				: value.VehicleModelName,
										"vin" 					: value.VIN,
										"personInCharge" 		: value.PICName,
										"address" 				: value.Address,
										"phoneNum"				: value.Phone,
										"validUntil"			: $filter('date')(new Date(value.ValidThru), "yyyy-MM-dd")
									});
								});
							}
							else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company')
							{
								angular.forEach(res.data.Result, function(value, key){
									grid_data.push({
										"customerPKSVehicleID" 	: value.CustomerPKSVehicleId,
										"VehicleId"				: value.VehicleId,
										"licenseNum" 			: value.LicensePlate,
										"modelID"				: value.VehicleModelId,
										"model" 				: value.VehicleModelName,
										"vin" 					: value.VIN,
										"personInCharge" 		: value.PICName,
										"address" 				: value.Address,
										"phoneNum"				: value.Phone,
										"validUntil"			: $filter('date')(new Date(value.ValidThru), "yyyy-MM-dd")
									});
								});
							}
							$scope.CPKS_Add_UIGrid.data = grid_data;
							$scope.CPKS_Add_UIGrid.totalItems = ($scope.CPKS_AddUnitPKSTotalData<res.data.Total)? res.data.Total : $scope.CPKS_AddUnitPKSTotalData;
							// $scope.CPKS_Add_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							// $scope.CPKS_Add_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
							$scope.CPKS_Add_UIGrid.paginationCurrentPage = pageNumber;
						}
					}
				);
			}
		);
	};

	$scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    $scope.gridApiAppointment = {};
    $scope.filterColIdx = 1;
    var x = -1;
    for (var i = 0; i < $scope.CPKS_Add_UIGrid.columnDefs.length; i++) {
        if ($scope.CPKS_Add_UIGrid.columnDefs[i].visible == undefined && $scope.CPKS_Add_UIGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.CPKS_Add_UIGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
		}
		
        console.log("$scope.gridCols", $scope.gridCols);
        console.log("$scope.grid.columnDefs[i]", $scope.CPKS_Add_UIGrid.columnDefs[i]);
    }
	$scope.filterBtnLabel = $scope.gridCols[0].name;
	
    $scope.filterBtnChange = function(col) {
        console.log("aku col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };
	$scope.filterBtnChange($scope.gridCols[0]);

	$scope.CPKS_AddGroupTypeChange = function ()
	{
		var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType]!=undefined)?$scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";

		if (groupType == 'pks' ) //PKS
		{
			$scope.CPKS_Add_StartPeriod_Show = true;
			$scope.CPKS_Add_EndPeriod_Show = true;
			$scope.CPKS_Add_PaymentMethod_Show = true;
			$scope.CPKS_AddUnitPKS_ValidUntil_Show = true;
			$scope.CPKS_Add_NoPKS_Show = true;
			$scope.CPKS_Add_NOAFCO_Show = false;
			

			if ($scope.CPKS_Add_UIGrid != undefined)
			{
				$scope.CPKS_Add_UIGrid.columnDefs = $scope.CPKS_Add_PKSGrid;
				$scope.CPKS_Add_UIGrid.data = [];
				$scope.CPKS_Add_UIGrid_Paging(1);
			}
		}
		else if (groupType == 'affiliated company') //Affiliated Company
		{
			$scope.CPKS_Add_StartPeriod_Show = true;
			$scope.CPKS_Add_EndPeriod_Show = true;
			$scope.CPKS_Add_PaymentMethod_Show = true;
			$scope.CPKS_AddUnitPKS_ValidUntil_Show = true;
			$scope.CPKS_Add_NoPKS_Show = false;
			$scope.CPKS_Add_NOAFCO_Show = true;

			if ($scope.CPKS_Add_UIGrid != undefined)
			{
				$scope.CPKS_Add_UIGrid.columnDefs = $scope.CPKS_Add_AfcoGrid;
				$scope.CPKS_Add_UIGrid.data = [];
				$scope.CPKS_Add_UIGrid_Paging(1);
			}
		}
		else
		{
			$scope.CPKS_AddGroupType = $scope.CPKS_AddGroupTypeDefaultValue;
		}
	};

	$scope.CPKS_Add_Filter_Button_Clicked = function ()
	{
		$scope.CPKS_Add_UIGrid_Paging_Filter(1);
	}

	$scope.CPKS_Add_Edit_Button_Clicked = function (row)
	{
		$scope.mode = "edit"
		$scope.CPKS_AddUnitPKSResetVariable();
		var index = $scope.CPKS_Add_UIGrid.data.indexOf(row.entity);
		$scope.CPKS_AddUnitPKSCurrentRow = index;
		$scope.CPKS_AddUnitPKSCustomerPKSID = $scope.CPKS_Add_UIGrid.data[index].CustomerPKSId
		$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = $scope.CPKS_Add_UIGrid.data[index].customerPKSVehicleID;
		$scope.CPKS_AddUnitPKSLicenseNumText = $scope.CPKS_Add_UIGrid.data[index].licenseNum;
		$scope.CPKS_AddUnitPKSLicensePlateText = $scope.CPKS_Add_UIGrid.data[index].licenseNum;
		$scope.CPKS_AddUnitPKSModel = $scope.CPKS_Add_UIGrid.data[index].modelID;
		$scope.CPKS_AddUnitPKSVINText = $scope.CPKS_Add_UIGrid.data[index].vin;
		$scope.CPKS_AddUnitPKSPICText = $scope.CPKS_Add_UIGrid.data[index].personInCharge;
		$scope.CPKS_AddUnitPKSPhoneNumText = $scope.CPKS_Add_UIGrid.data[index].phoneNum;
		$scope.CPKS_AddUnitPKSPICAddress = $scope.CPKS_Add_UIGrid.data[index].address;

		if($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks')
		{
			$scope.CPKS_AddUnitPKSValidUntil = new Date($scope.CPKS_Add_UIGrid.data[index].validUntil);
		}
		if($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company')
		{
			$scope.CPKS_AddUnitPKSValidUntil = new Date($scope.CPKS_Add_UIGrid.data[index].validUntil);
		}

		$scope.CPKS_Add_Body_Show = false;
		$scope.CPKS_AddUnitPKS_Show = true;

	}

	$scope.phoneChange = function(string,model){
	   $scope[model] = string.replace(new RegExp("[^0-9]","g"),"");
	}

	$scope.DeleteVehiclePKS = []
	$scope.CPKS_Add_Delete_Button_Clicked = function (row)
	{	
		$scope.CPKS_Add_RowIndexToBeRemoved = $scope.CPKS_Add_UIGrid.data.indexOf(row.entity);
		//angular.element('#CPKS_Add_DeleteQuestion_Modal').modal('show');
		bsAlert.alert({
			title: "Apakah Anda yakin ingin menghapus ",
			text: "",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak",
		},
		function () {
			// CustomerPKSFactory.delete_Vehicle($scope.CPKS_Add_UIGrid.data[$scope.CPKS_Add_RowIndexToBeRemoved].customerPKSVehicleID)
			// 	.then(
			// 		function(res){
					// for(var i in $scope.CPKS_Add_UIGrid.data){
					// 	$scope.DeleteVehiclePKS.push($scope.CPKS_Add_UIGrid.data[i]) 
					// }
					$scope.DeleteVehiclePKS.push($scope.CPKS_Add_UIGrid.data[$scope.CPKS_Add_RowIndexToBeRemoved]);
					$scope.CPKS_Add_UIGrid.data.splice($scope.CPKS_Add_RowIndexToBeRemoved, 1);
					$scope.CPKS_AddUnitPKSTotalData -= 1;
					$scope.CPKS_Add_DoPagination = false;
					$scope.CPKS_Add_UIGrid.totalItems = $scope.CPKS_AddUnitPKSTotalData;
			
					
				
		},
		function () {

		}
		)
	}

	// $scope.CPKS_Add_No_Delete_Button_Clicked = function () {
	// 	$scope.CPKS_Add_RowIndexToBeRemoved = undefined;
	// 	angular.element('#CPKS_Add_DeleteQuestion_Modal').modal('hide');
	// }

	// $scope.CPKS_Add_Yes_Delete_Button_Clicked = function () {
	// 	if ($scope.CPKS_Add_RowIndexToBeRemoved != undefined)
	// 	{
	// 		if ($scope.CPKS_Add_UIGrid.data[$scope.CPKS_Add_RowIndexToBeRemoved].customerPKSVehicleID == undefined)
	// 		{
	// 			$scope.CPKS_Add_UIGrid.data.splice($scope.CPKS_Add_RowIndexToBeRemoved, 1);
	// 			$scope.CPKS_AddUnitPKSTotalData -= 1;
	// 			//$scope.CPKS_Add_UIGrid.totalItems = $scope.CPKS_AddUnitPKSTotalData;
	// 		}
	// 		else
	// 		{
	// 			CustomerPKSFactory.delete_Vehicle($scope.CPKS_Add_UIGrid.data[$scope.CPKS_Add_RowIndexToBeRemoved].customerPKSVehicleID)
	// 			.then(
	// 				function(res){
	// 					$scope.CPKS_Add_UIGrid.data.splice($scope.CPKS_Add_RowIndexToBeRemoved, 1);
	// 					$scope.CPKS_AddUnitPKSTotalData -= 1;
	// 					$scope.CPKS_Add_DoPagination = false;
	// 					$scope.CPKS_Add_UIGrid.totalItems = $scope.CPKS_AddUnitPKSTotalData;
	// 				}
	// 			);

	// 		}

	// 		$scope.CPKS_Add_RowIndexToBeRemoved = undefined;
	// 	}

	// 	angular.element('#CPKS_Add_DeleteQuestion_Modal').modal('hide');
	// }

	$scope.CPKS_Add_Cancel_Button_Clicked = function ()
	{
		$scope.CPKS_Add_Current_CustomerPKSID = -1;
		$scope.CPKS_Add_ResetVariable();
		$scope.CPKS_AddDisableField = false;
		$scope.CPKS_AddBack_Button_Show = false;

		$scope.CPKS_Main_Body_Show = true;
		$scope.CPKS_Add_Body_Show = false;
	}

	$scope.CPKS_Add_AddUnitPKS_Button_Clicked = function ()
	{
		$scope.mode = "new"
		$scope.CPKS_AddUnitPKSResetVariable();
		$scope.CPKS_Add_Body_Show = false;
		$scope.CPKS_AddUnitPKS_Show = true;
		$scope.CPKS_AddUnitPKSCurrentRow = -1;
	}

	$scope.CPKS_Add_ResetVariable = function ()
	{
		$scope.CPKS_AddGroupType = 1;
		$scope.CPKS_AddToyotaIDText = ""
		$scope.CPKS_AddContactPersonText = "";
		$scope.CPKS_AddPKSNumText = "";
		$scope.CPKS_AddCompanyNameText = "";
		$scope.CPKS_AddStartPeriod = undefined;
		$scope.CPKS_AddEndPeriod = undefined;
		$scope.CPKS_AddBillingAddress = "";
		$scope.CPKS_AddPaymentMethod = "saldo";
		$scope.CPKS_AddBalanceText = "";
		$scope.CPKS_AddTOPNum = "";
		$scope.CPKS_AddNPWPText = "";
		$scope.CPKS_AddNPWPAddress = "";
		$scope.CPKS_AddFilterText = "";
		$scope.CPKS_AddFilterColumn = undefined;
		$scope.CPKS_Add_UIGrid.data = [];
		$scope.CPKS_AddBalanceText = 0;
		$scope.CPKS_AddTOPNum = 0;
	};

	$scope.CPKS_Add_UploadExcel_Button_Clicked = function ()
	{
		angular.element('#CPKS_Add_Upload_Modal').modal('show');
		$('#CPKS_Add_UploadVehicleList').val("")
		$scope.ExcelData = [];
	}

	$scope.CPKS_Add_DownloadTemplate_Button_Clicked = function ()
	{
		var excelData = [];
		var fileName = "CustomerPKS_AddVehicle_Template";

		excelData = [{"No_Polisi":"varchar(50) (ex.B1234PP)", "Model":"varchar(50) (ex.Avanza)", "VIN":"varchar(50) (ex.MR053HY9399023697)", "Nama_PIC":"varchar(50) (ex.Raflizal)", "Alamat":"varchar(50) (ex.Jl.ABC)", "Handphone":"numerik(20) (ex.08666666666)", "Valid_To":" 'yyyy/MM/dd (ex.'2019/12/30)"}];

		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.CPKS_Add_Save_Button_Clicked = function()
	{
		if ($scope.CPKS_AddPaymentMethod == 'saldo'){
			bsNotify.show({
				size: 'big',
				type: 'danger',
				title: "Tidak bisa simpan data, payment method harus TOP"
			});
			return;
		}
		if (($scope.CPKS_AddGroupType == null || $scope.CPKS_AddGroupType == undefined || $scope.CPKS_AddGroupType =="") ||
			($scope.CPKS_AddPKSNumText == null || $scope.CPKS_AddPKSNumText == undefined || $scope.CPKS_AddPKSNumText == "") ||
			($scope.CPKS_AddCompanyNameText == null || $scope.CPKS_AddCompanyNameText == undefined || $scope.CPKS_AddCompanyNameText == "") ||
			($scope.CPKS_AddBillingAddress == null || $scope.CPKS_AddBillingAddress == undefined || $scope.CPKS_AddBillingAddress == "") ||
			($scope.CPKS_AddNPWPText == null || $scope.CPKS_AddNPWPText == undefined || $scope.CPKS_AddNPWPText == "") ||
			($scope.CPKS_AddNPWPAddress == null || $scope.CPKS_AddNPWPAddress == undefined || $scope.CPKS_AddNPWPAddress == "") ||
			($scope.CPKS_AddStartPeriod == null || $scope.CPKS_AddStartPeriod == undefined) ||
			($scope.CPKS_AddEndPeriod == null || $scope.CPKS_AddEndPeriod == undefined) ||
			($scope.CPKS_AddPaymentMethod == null || $scope.CPKS_AddPaymentMethod == undefined)
		){
			bsNotify.show({
				size: 'big',
				type: 'danger',
				title: "Mohon Lengkapi Data Terlebih Dahulu"
			});

		} else if ($scope.CPKS_Main_IsNewData == true)
		{
			var customerPKSId = -1;
			$scope.CPKS_AddPaymentMethod = ($scope.CPKS_AddPaymentMethod == undefined)? 'saldo' : $scope.CPKS_AddPaymentMethod;
			var top = ($scope.CPKS_AddPaymentMethod == 'top')? $scope.CPKS_AddTOPNum : 0;
			var balance = ($scope.CPKS_AddPaymentMethod == 'saldo')? $scope.CPKS_AddBalanceText : 0;
			$scope.CPKS_Add_Current_CustomerPKSID = 0
			CustomerPKSFactory.CekCustomerPKS($scope.CPKS_AddPKSNumText, $scope.CPKS_Add_Current_CustomerPKSID,$scope.CPKS_AddGroupType).then(function (res) {
				var resu = res.data
				console.log('res >>', resu);
				
				if (resu[0] == "false") {
					bsAlert.alert({
						// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						title: "PKS dengan No " + $scope.CPKS_AddPKSNumText + " sudah terdaftar",
						text: "",
						type: "warning",
						showCancelButton: false
					});
				}else{
					if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks')
					{
						inputCustomerPKS = [{
							"OutletId" 			: outletId,
							"GroupTypeId" 		: $scope.CPKS_AddGroupType,
							"ToyotaId" 			: $scope.CPKS_AddToyotaIDText,
							"NoPKS" 			: $scope.CPKS_AddPKSNumText,
							"CompanyName"		: $scope.CPKS_AddCompanyNameText,
							"BillingAddress"	: $scope.CPKS_AddBillingAddress,
							"NPWP"				: $scope.CPKS_AddNPWPText,
							"NPWPAddress"		: $scope.CPKS_AddNPWPAddress,
							"StartPeriod"		: $filter('date')(new Date($scope.CPKS_AddStartPeriod), "yyyy-MM-dd"),
							"EndPeriod"			: $filter('date')(new Date($scope.CPKS_AddEndPeriod), "yyyy-MM-dd"),
							"ContactPerson"		: $scope.CPKS_AddContactPersonText,
							"PaymentMethod"		: $scope.CPKS_AddPaymentMethod,
							"TOP"				: top,
							"Balance"			: balance
						}];
					}
					else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company')
					{
						inputCustomerPKS = [{
							"OutletId" 			: outletId,
							"GroupTypeId" 		: $scope.CPKS_AddGroupType,
							"ToyotaId" 			: $scope.CPKS_AddToyotaIDText,
							"NoPKS" 			: $scope.CPKS_AddPKSNumText,
							"CompanyName"		: $scope.CPKS_AddCompanyNameText,
							"BillingAddress"	: $scope.CPKS_AddBillingAddress,
							"NPWP"				: $scope.CPKS_AddNPWPText,
							"NPWPAddress"		: $scope.CPKS_AddNPWPAddress,
							"StartPeriod"		: $filter('date')(new Date($scope.CPKS_AddStartPeriod), "yyyy-MM-dd"),
							"EndPeriod"			: $filter('date')(new Date($scope.CPKS_AddEndPeriod), "yyyy-MM-dd"),
							"ContactPerson"		: $scope.CPKS_AddContactPersonText,
							"PaymentMethod"		: $scope.CPKS_AddPaymentMethod,
							"TOP"				: top,
							"Balance"			: balance
						}];
					}

					var status;
					var statusMessage;
					console.log("inputCustomerPKS",inputCustomerPKS)
					CustomerPKSFactory.saveData_CustomerPKS(inputCustomerPKS)
					.then(
						function(res){
							angular.forEach(res.data.Result, function(value, key){
								status = value.Status;
								statusMessage = value.StatusMessage;
								customerPKSId = value.CustomerPKSId;
							});

							/*
							if (status == 1)
							{
								// sebelumnya code insert yang terdapat di bawah
							}
							else
							{
								alert("Data sudah terdaftar sebagai PKS, gunakan Edit Data");
							}
							*/

							//code insertnya
							var inputData = [];
							var eachData = {};

							angular.forEach($scope.CPKS_Add_UIGrid.data, function(value, key){						
								if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks') {
									eachData = {
										"OutletID": outletId,
										"CustomerPKSId": customerPKSId,
										"VehicleId": value.VehicleId,
										"LicenseNum": value.licenseNum,
										"VIN": value.vin,
										"PICName": value.personInCharge,
										"Address": value.address,
										"Phone": value.phoneNum,
										"ValidThru": $filter('date')(new Date(value.validUntil), "yyyy-MM-dd"),
										// "LicensePlate": value.licenseNum,
										"LicensePlate": value.licensePlate,
										"VehicleModelName": value.model
									};

								}
								else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company') {
									eachData = {
										"OutletID": outletId,
										"CustomerPKSId": customerPKSId,
										"VehicleId": value.VehicleId,
										"LicenseNum": value.licenseNum,
										"VIN": value.vin,
										"PICName": value.personInCharge,
										"Address": value.address,
										"Phone": value.phoneNum,
										"ValidThru": $filter('date')(new Date(value.validUntil), "yyyy-MM-dd"),
										// "LicensePlate": value.licenseNum,
										"LicensePlate": value.licensePlate,
										"VehicleModelName": value.model
									};
								}


								console.log("data input sebelum masuk factory :", eachData);
								inputData.push(eachData);
							});

							console.log("data input sebelum masuk factory :", inputData);
							CustomerPKSFactory.saveData_Vehicle(inputData)
							.then(
								function(res){
									$scope.CPKS_Main_UIGrid_Paging(1);
									$scope.CPKS_Main_Body_Show = true;
									$scope.CPKS_Add_Body_Show = false;
									bsAlert.success("Data berhasil disimpan");
								},
								function(err){
									console.log("err : ", err);
								}
							);


						},
						function(err){
							alert('Gagal simpan data');
							console.log("err : ", err);
						}
					);
					// $scope.CPKS_GetFullData();
				}
			})
		}
		else
		{
			$scope.CPKS_AddPaymentMethod = ($scope.CPKS_AddPaymentMethod == undefined)? 'saldo' : $scope.CPKS_AddPaymentMethod;
			var top = ($scope.CPKS_AddPaymentMethod == 'top')? $scope.CPKS_AddTOPNum : 0;
			var balance = ($scope.CPKS_AddPaymentMethod == 'saldo')? $scope.CPKS_AddBalanceText : 0;
			$scope.CPKS_Add_Current_CustomerPKSID = $scope.CPKS_Add_Current_CustomerPKSID
			CustomerPKSFactory.CekCustomerPKS($scope.CPKS_AddPKSNumText, $scope.CPKS_Add_Current_CustomerPKSID,$scope.CPKS_AddGroupType).then(function (res) {
				var resu = res.data
				console.log('res >>', resu);
				if (resu[0] == "false") {
					bsAlert.alert({
						// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						title: "PKS dengan No " + $scope.CPKS_AddPKSNumText + " sudah terdaftar",
						text: "",
						type: "warning",
						showCancelButton: false
					});
				}else{
					if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks')
					{
						inputCustomerPKS = [{
							"OutletId" 			: outletId,
							"GroupTypeId" 		: $scope.CPKS_AddGroupType,
							"CustomerPKSId" 	: $scope.CPKS_Add_Current_CustomerPKSID,
							"ToyotaId" 			: $scope.CPKS_AddToyotaIDText,
							"NoPKS" 			: $scope.CPKS_AddPKSNumText,
							"CompanyName"		: $scope.CPKS_AddCompanyNameText,
							"BillingAddress"	: $scope.CPKS_AddBillingAddress,
							"NPWP"				: $scope.CPKS_AddNPWPText,
							"NPWPAddress"		: $scope.CPKS_AddNPWPAddress,
							"StartPeriod"		: $filter('date')(new Date($scope.CPKS_AddStartPeriod), "yyyy-MM-dd"),
							"EndPeriod"			: $filter('date')(new Date($scope.CPKS_AddEndPeriod), "yyyy-MM-dd"),
							"ContactPerson"		: $scope.CPKS_AddContactPersonText,
							"PaymentMethod"		: $scope.CPKS_AddPaymentMethod,
							"TOP"				: top,
							"Balance"			: balance
						}];
						console.log('data pks', inputCustomerPKS);
					}
					else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company')
					{
						
						inputCustomerPKS = [{
							"OutletId" 			: outletId,
							"CustomerPKSId" 	: $scope.CPKS_Add_Current_CustomerPKSID,
							"GroupTypeId" 		: $scope.CPKS_AddGroupType,
							"ToyotaId" 			: $scope.CPKS_AddToyotaIDText,
							"NoPKS" 			: $scope.CPKS_AddPKSNumText,
							"CompanyName"		: $scope.CPKS_AddCompanyNameText,
							"BillingAddress"	: $scope.CPKS_AddBillingAddress,
							"NPWP"				: $scope.CPKS_AddNPWPText,
							"NPWPAddress"		: $scope.CPKS_AddNPWPAddress,
							"StartPeriod"		: $filter('date')(new Date($scope.CPKS_AddStartPeriod), "yyyy-MM-dd"),
							"EndPeriod"			: $filter('date')(new Date($scope.CPKS_AddEndPeriod), "yyyy-MM-dd"),
							"ContactPerson"		: $scope.CPKS_AddContactPersonText,
							"PaymentMethod"		: $scope.CPKS_AddPaymentMethod,
							"TOP"				: top,
							"Balance"			: balance
						}];
					}
					var status;
					var statusMessage;
					console.log('data affiliated company', inputCustomerPKS);
					CustomerPKSFactory.editData_CustomerPKS(inputCustomerPKS)
					.then(
						function(res){
							var status;
							var statusMessage;
							angular.forEach(res.data.Result, function(value, key){
								status = value.Status;
								statusMessage = value.StatusMessage;
							});
							if (status == 1)
							{
								var inputData = [];
								var eachData = {};

								angular.forEach($scope.CPKS_Add_UIGrid.data, function(value, key){
								console.log("value q",value);
									
									if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'pks')
									{

										eachData = {
											"OutletID" 			: outletId,
											"CustomerPKSId" 	: $scope.CPKS_Add_Current_CustomerPKSID,								
											"VehicleId" 		: value.VehicleId,
											"LicenseNum" 		: value.licenseNum,
											"VIN" 				: value.vin,
											"PICName" 			: value.personInCharge,
											"Address" 			: value.address,
											"Phone" 			: value.phoneNum,
											"ValidThru"			: $filter('date')(new Date(value.validUntil), "yyyy-MM-dd"),
											// "LicensePlate"		: value.licenseNum,
											"LicensePlate"		: value.licensePlate,
											"VehicleModelName" 	: value.model
											
										};

									}
									else if ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() == 'affiliated company')
									{
										eachData = {
											"OutletID" 			: outletId,
											"CustomerPKSId" 	: $scope.CPKS_Add_Current_CustomerPKSID,
											"VehicleId" 		: value.VehicleId,
											"LicenseNum" 		: value.licenseNum,
											"VIN" 				: value.vin,
											"PICName" 			: value.personInCharge,
											"Address" 			: value.address,
											"Phone" 			: value.phoneNum,
											"ValidThru"			: $filter('date')(new Date(value.validUntil), "yyyy-MM-dd"),
											// "LicensePlate"		: value.licenseNum,
											"LicensePlate"		: value.licensePlate,
											"VehicleModelName" 	: value.model
										};
									}

									inputData.push(eachData);
								});

								console.log("data input sebelum masuk factory :", inputData);
								CustomerPKSFactory.editData_Vehicle(inputData)
								.then(
									function(res){
										$scope.CPKS_Main_UIGrid_Paging(1);
										$scope.CPKS_Main_Body_Show = true;
										$scope.CPKS_Add_Body_Show = false;
										bsAlert.success("Data berhasil disimpan");
									},
									function(err){
										console.log("err : ", err);
									}
								);
							}
							else
							{
								alert("Data belum terdaftar sebagai PKS, gunakan Tambah Data");
							}


						},
						function(err){
							alert('Gagal simpan data');
							console.log("err : ", err);
						}
					);
					// $scope.CPKS_GetFullData();
				}
			})
		}
			if($scope.DeleteVehiclePKS.length != 0){
				for(var rr in $scope.DeleteVehiclePKS){
					CustomerPKSFactory.delete_Vehicle($scope.DeleteVehiclePKS[rr].customerPKSVehicleID)
					.then(
						function(res){
					})
				}
			}
			
	}

	//##### END ADD CUSTOMER PKS PAGE #####

	//##### ADD UNIT PKS PAGE #####
	$scope.CPKS_AddUnitPKS_Show = false;
	$scope.CPKS_AddUnitPKSModelList = [];
	$scope.CPKS_AddUnitPKSModelList2 = [];
	$scope.CPKS_AddUnitPKSModelOption = [];
	$scope.CPKS_AddUnitPKSCurrentRow = -1;
	angular.element('#CPKS_LicenseNum_Modal').modal('hide');

	$scope.CPKS_AddUnitPKS_GetModelList = function(text) {
		CustomerPKSFactory.get_ModelList()
		.then(
			function(res){
				angular.forEach(res.data.Result, function(value, key){
					$scope.CPKS_AddUnitPKSModelOption.push({name: value.VehicleModelName, value: value.VehicleModelId});
					$scope.CPKS_AddUnitPKSModelList[value.VehicleModelId] = value.VehicleModelName;
					$scope.CPKS_AddUnitPKSModelList2[value.VehicleModelName] = value.VehicleModelId;
				});
			}
		);
	};

	$scope.CPKS_AddUnitPKS_GetModelList();

	$scope.CPKS_AddUnitPKS_Find_Button_Clicked = function ()
	{
		$scope.CPKS_LicenseNum_UIGrid_Paging(1);
		angular.element('#CPKS_LicenseNum_Modal').modal('show');
	}

	$scope.CPKS_AddUnitPKS_Save_Button_Clicked = function () {
		console.log("$scope.mode >>", $scope.mode);
		var TmpDataKendaraan = [];
		if($scope.mode == "new"){
			$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = 0;
			CustomerPKSFactory.cekLicensePlateAvailable($scope.CPKS_AddUnitPKSLicenseNumText, $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,$scope.CPKS_AddUnitPKSVINText).then(function (res) {
			var resu = res.data
			console.log('res >>', resu);
			
			if (resu[0] == "false") {
				bsAlert.alert({
					// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
					title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar",
					text: "",
					type: "warning",
					showCancelButton: false
				});
			} else {
				// $scope.CPKS_LicenseNum_UIGrid_Paging(1);
				// angular.element('#CPKS_LicenseNum_Modal').modal('show');

				console.log("$scope.CPKS_AddUnitPKSLicensePlateText",$scope.CPKS_AddUnitPKSLicensePlateText)
				console.log("$scope.CPKS_AddUnitPKSValidUntil", $scope.CPKS_AddUnitPKSValidUntil)
				if (($scope.CPKS_AddUnitPKSValidUntil == null || $scope.CPKS_AddUnitPKSValidUntil == undefined || $scope.CPKS_AddUnitPKSValidUntil == "Invalid Date") ||
					($scope.CPKS_AddUnitPKSLicenseNumText == null || $scope.CPKS_AddUnitPKSLicenseNumText == undefined || $scope.CPKS_AddUnitPKSLicenseNumText == "") ||
					($scope.CPKS_AddUnitPKSModel == null || $scope.CPKS_AddUnitPKSModel == undefined || $scope.CPKS_AddUnitPKSModel == "") ||
					($scope.CPKS_AddUnitPKSVINText == null || $scope.CPKS_AddUnitPKSVINText == undefined || $scope.CPKS_AddUnitPKSVINText == "") ||
					($scope.CPKS_AddUnitPKSPICText == null || $scope.CPKS_AddUnitPKSPICText == undefined || $scope.CPKS_AddUnitPKSPICText == "") ||
					// ($scope.CPKS_AddUnitPKSPhoneNumText == null || $scope.CPKS_AddUnitPKSPhoneNumText == undefined || $scope.CPKS_AddUnitPKSPhoneNumText == "") ||
					($scope.CPKS_AddUnitPKSPICAddress == null || $scope.CPKS_AddUnitPKSPICAddress == undefined || $scope.CPKS_AddUnitPKSPICAddress == "") 

				) {
					bsNotify.show({
						size: 'big',
						type: 'danger',
						title: "Mohon Lengkapi Data Terlebih Dahulu"
					});
				} else {
					var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType] != undefined) ? $scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";
					var row_data;
					console.log("licenseNum",$scope.CPKS_AddUnitPKSLicenseNumText)
					console.log("licensePlate",$scope.CPKS_AddUnitPKSLicensePlateText)
					console.log("modelID",$scope.CPKS_AddUnitPKSModel)
					console.log("model",$scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel])
					console.log("vin",$scope.CPKS_AddUnitPKSVINText)
					console.log("personInCharge",$scope.CPKS_AddUnitPKSPICText)
					console.log("address", $scope.CPKS_AddUnitPKSPICAddress)
					console.log("phoneNum",$scope.CPKS_AddUnitPKSPhoneNumText)
					console.log("validUntil",$scope.CPKS_AddUnitPKSValidUntil)
					if (groupType == 'pks') //PKS
					{
						row_data = {
							"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
							"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,
							"modelID": $scope.CPKS_AddUnitPKSModel,
							"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
							"vin": $scope.CPKS_AddUnitPKSVINText,
							"personInCharge": $scope.CPKS_AddUnitPKSPICText,
							"address": $scope.CPKS_AddUnitPKSPICAddress,
							"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
							"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
						};
					}
					else if (groupType == 'affiliated company') //Affiliated Company
					{
						row_data = {
							"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
							"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,
							"modelID": $scope.CPKS_AddUnitPKSModel,
							"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
							"vin": $scope.CPKS_AddUnitPKSVINText,
							"personInCharge": $scope.CPKS_AddUnitPKSPICText,
							"address": $scope.CPKS_AddUnitPKSPICAddress,
							"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
							"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
						};
					}

					if ($scope.CPKS_AddUnitPKSCurrentRow != -1) {
						$scope.CPKS_Add_UIGrid.data[$scope.CPKS_AddUnitPKSCurrentRow] = row_data;
					}
					else {
						$scope.CPKS_Add_UIGrid.data.push(row_data);
						TmpDataKendaraan.push(row_data);
					}

					$scope.CPKS_AddUnitPKSResetVariable();
					$scope.pks_edit = true;
					$scope.pks_hapus = true;
					$scope.CPKS_AddUnitPKS_Show = false;
					$scope.CPKS_Add_Body_Show = true;
					console.log("data new", row_data)
				}
			};

		});
		}
		else
		{
			if($scope.CPKS_AddUnitPKSCustomerPKSVehicleID == undefined){
				$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = 0
				console.log("kampret",$scope.CPKS_AddUnitPKSCustomerPKSVehicleID)
				CustomerPKSFactory.cekLicensePlateAvailable($scope.CPKS_AddUnitPKSLicenseNumText, $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,$scope.CPKS_AddUnitPKSVINText).then(function (res) {
					var resu = res.data
					console.log('res >>', resu);
					
					if (resu[0] == "false") {
						bsAlert.alert({
							// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
							title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar",
							text: "",
							type: "warning",
							showCancelButton: false
						});
					} else {
						// $scope.CPKS_LicenseNum_UIGrid_Paging(1);
						// angular.element('#CPKS_LicenseNum_Modal').modal('show');
		
		
						console.log("$scope.CPKS_AddUnitPKSLicensePlateText",$scope.CPKS_AddUnitPKSLicensePlateText)
						console.log("$scope.CPKS_AddUnitPKSValidUntil", $scope.CPKS_AddUnitPKSValidUntil)
						if (($scope.CPKS_AddUnitPKSValidUntil == null || $scope.CPKS_AddUnitPKSValidUntil == undefined || $scope.CPKS_AddUnitPKSValidUntil == "Invalid Date") ||
							($scope.CPKS_AddUnitPKSLicenseNumText == null || $scope.CPKS_AddUnitPKSLicenseNumText == undefined || $scope.CPKS_AddUnitPKSLicenseNumText == "") ||
							($scope.CPKS_AddUnitPKSModel == null || $scope.CPKS_AddUnitPKSModel == undefined || $scope.CPKS_AddUnitPKSModel == "") ||
							($scope.CPKS_AddUnitPKSVINText == null || $scope.CPKS_AddUnitPKSVINText == undefined || $scope.CPKS_AddUnitPKSVINText == "")
							// ($scope.CPKS_AddUnitPKSPICText == null || $scope.CPKS_AddUnitPKSPICText == undefined || $scope.CPKS_AddUnitPKSPICText == "") ||
							// ($scope.CPKS_AddUnitPKSPhoneNumText == null || $scope.CPKS_AddUnitPKSPhoneNumText == undefined || $scope.CPKS_AddUnitPKSPhoneNumText == "") ||
							// ($scope.CPKS_AddUnitPKSPICAddress == null || $scope.CPKS_AddUnitPKSPICAddress == undefined || $scope.CPKS_AddUnitPKSPICAddress == "") 
		
						) {
							bsNotify.show({
								size: 'big',
								type: 'danger',
								title: "Mohon Lengkapi Data Terlebih Dahulu"
							});
						} else {
							var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType] != undefined) ? $scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";
							var row_data;
							if (groupType == 'pks') //PKS
							{
								console.log("data licenseplate",$scope.CPKS_AddUnitPKSLicensePlateText)
								row_data = {
									"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
									// "licensePlate": $scope.CPKS_AddUnitPKSLicenseNumText,
									"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,	"modelID": $scope.CPKS_AddUnitPKSModel,
									"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
									"vin": $scope.CPKS_AddUnitPKSVINText,
									"personInCharge": $scope.CPKS_AddUnitPKSPICText,
									"address": $scope.CPKS_AddUnitPKSPICAddress,
									"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
									"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
								};
							}
							else if (groupType == 'affiliated company') //Affiliated Company
							{
								row_data = {
									"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
									// "licensePlate": $scope.CPKS_AddUnitPKSLicenseNumText,
									"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,"modelID": $scope.CPKS_AddUnitPKSModel,
									"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
									"vin": $scope.CPKS_AddUnitPKSVINText,
									"personInCharge": $scope.CPKS_AddUnitPKSPICText,
									"address": $scope.CPKS_AddUnitPKSPICAddress,
									"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
									"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
								};
							}
		
							if ($scope.CPKS_AddUnitPKSCurrentRow != -1) {
								$scope.CPKS_Add_UIGrid.data[$scope.CPKS_AddUnitPKSCurrentRow] = row_data;
							}
							else {
								$scope.CPKS_Add_UIGrid.data.push(row_data);
							}
		
							$scope.CPKS_AddUnitPKSResetVariable();
							$scope.CPKS_AddUnitPKS_Show = false;
							$scope.CPKS_Add_Body_Show = true;
							console.log("data edit", row_data)
						}
					};
		
				});
			}else{
				$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = $scope.CPKS_AddUnitPKSCustomerPKSVehicleID
				CustomerPKSFactory.cekLicensePlateAvailable($scope.CPKS_AddUnitPKSLicenseNumText, $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,$scope.CPKS_AddUnitPKSVINText).then(function (res) {
					var resu = res.data
					console.log('res >>', resu);
					
					if (resu[0] == "false") {
						bsAlert.alert({
							// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
							title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar",
							text: "",
							type: "warning",
							showCancelButton: false
						});
					} else {
						// $scope.CPKS_LicenseNum_UIGrid_Paging(1);
						// angular.element('#CPKS_LicenseNum_Modal').modal('show');
		
		
						console.log("$scope.CPKS_AddUnitPKSLicensePlateText",$scope.CPKS_AddUnitPKSLicensePlateText)
						console.log("$scope.CPKS_AddUnitPKSValidUntil", $scope.CPKS_AddUnitPKSValidUntil)
						if (($scope.CPKS_AddUnitPKSValidUntil == null || $scope.CPKS_AddUnitPKSValidUntil == undefined || $scope.CPKS_AddUnitPKSValidUntil == "Invalid Date") ||
							($scope.CPKS_AddUnitPKSLicenseNumText == null || $scope.CPKS_AddUnitPKSLicenseNumText == undefined || $scope.CPKS_AddUnitPKSLicenseNumText == "") ||
							($scope.CPKS_AddUnitPKSModel == null || $scope.CPKS_AddUnitPKSModel == undefined || $scope.CPKS_AddUnitPKSModel == "") ||
							($scope.CPKS_AddUnitPKSVINText == null || $scope.CPKS_AddUnitPKSVINText == undefined || $scope.CPKS_AddUnitPKSVINText == "")
							// ($scope.CPKS_AddUnitPKSPICText == null || $scope.CPKS_AddUnitPKSPICText == undefined || $scope.CPKS_AddUnitPKSPICText == "") ||
							// ($scope.CPKS_AddUnitPKSPhoneNumText == null || $scope.CPKS_AddUnitPKSPhoneNumText == undefined || $scope.CPKS_AddUnitPKSPhoneNumText == "") ||
							// ($scope.CPKS_AddUnitPKSPICAddress == null || $scope.CPKS_AddUnitPKSPICAddress == undefined || $scope.CPKS_AddUnitPKSPICAddress == "") 
		
						) {
							bsNotify.show({
								size: 'big',
								type: 'danger',
								title: "Mohon Lengkapi Data Terlebih Dahulu"
							});
						} else {
							var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType] != undefined) ? $scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";
							var row_data;
							if (groupType == 'pks') //PKS
							{	
								console.log("data licenseplate",$scope.CPKS_AddUnitPKSLicensePlateText)
								row_data = {
									"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
									"customerPKSVehicleID": $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,
									"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,"modelID": $scope.CPKS_AddUnitPKSModel,
									"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
									"vin": $scope.CPKS_AddUnitPKSVINText,
									"personInCharge": $scope.CPKS_AddUnitPKSPICText,
									"address": $scope.CPKS_AddUnitPKSPICAddress,
									"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
									"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
								};
							}
							else if (groupType == 'affiliated company') //Affiliated Company
							{
								row_data = {
									"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
									"customerPKSVehicleID": $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,
									"licensePlate": $scope.CPKS_AddUnitPKSLicensePlateText === undefined || $scope.CPKS_AddUnitPKSLicensePlateText == "" ? $scope.CPKS_AddUnitPKSLicenseNumText : $scope.CPKS_AddUnitPKSLicensePlateText,"modelID": $scope.CPKS_AddUnitPKSModel,
									"modelID": $scope.CPKS_AddUnitPKSModel,
									"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined) ? $scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] : "",
									"vin": $scope.CPKS_AddUnitPKSVINText,
									"personInCharge": $scope.CPKS_AddUnitPKSPICText,
									"address": $scope.CPKS_AddUnitPKSPICAddress,
									"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
									"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined) ? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd") : ""
								};
							}
		
							if ($scope.CPKS_AddUnitPKSCurrentRow != -1) {
								$scope.CPKS_Add_UIGrid.data[$scope.CPKS_AddUnitPKSCurrentRow] = row_data;
							}
							else {
								$scope.CPKS_Add_UIGrid.data.push(row_data);
							}
		
							$scope.CPKS_AddUnitPKSResetVariable();
							$scope.CPKS_AddUnitPKS_Show = false;
							$scope.CPKS_Add_Body_Show = true;
							console.log("data edit", row_data,$scope.CPKS_AddUnitPKSCustomerPKSVehicleID)
						}
					};
		
				});
			}
			
		}
	};
	//---------------- koding Lama -------------------------------
	// $scope.CPKS_AddUnitPKS_Save_Button_Clicked = function ()
	// {
	// 	console.log("$scope.CPKS_AddUnitPKSValidUntil",$scope.CPKS_AddUnitPKSValidUntil)
	// 	if (($scope.CPKS_AddUnitPKSValidUntil == null || $scope.CPKS_AddUnitPKSValidUntil == undefined || $scope.CPKS_AddUnitPKSValidUntil =="Invalid Date") ||
	// 		($scope.CPKS_AddUnitPKSLicenseNumText == null || $scope.CPKS_AddUnitPKSLicenseNumText == undefined || $scope.CPKS_AddUnitPKSLicenseNumText == "") ||
	// 		($scope.CPKS_AddUnitPKSModel == null || $scope.CPKS_AddUnitPKSModel == undefined || $scope.CPKS_AddUnitPKSModel == "") ||
	// 		($scope.CPKS_AddUnitPKSVINText == null || $scope.CPKS_AddUnitPKSVINText == undefined || $scope.CPKS_AddUnitPKSVINText == "")
	// 		// ($scope.CPKS_AddUnitPKSPICText == null || $scope.CPKS_AddUnitPKSPICText == undefined || $scope.CPKS_AddUnitPKSPICText == "") ||
	// 		// ($scope.CPKS_AddUnitPKSPhoneNumText == null || $scope.CPKS_AddUnitPKSPhoneNumText == undefined || $scope.CPKS_AddUnitPKSPhoneNumText == "") ||
	// 		// ($scope.CPKS_AddUnitPKSPICAddress == null || $scope.CPKS_AddUnitPKSPICAddress == undefined || $scope.CPKS_AddUnitPKSPICAddress == "") 
			
	// 	){
	// 		bsNotify.show({
	// 			size: 'big',
	// 			type: 'danger',
	// 			title: "Mohon Lengkapi Data Terlebih Dahulu"
	// 		});
	// 	} else {
	// 		var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType]!=undefined)?$scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";
	// 		var row_data;
	// 		if (groupType == 'pks') //PKS
	// 		{
	// 			row_data = {
	// 				"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
	// 				"modelID":$scope.CPKS_AddUnitPKSModel,
	// 				"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined)?$scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel]:"",
	// 				"vin":$scope.CPKS_AddUnitPKSVINText,
	// 				"personInCharge": $scope.CPKS_AddUnitPKSPICText,
	// 				"address": $scope.CPKS_AddUnitPKSPICAddress,
	// 				"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText,
	// 				"validUntil": ($scope.CPKS_AddUnitPKSValidUntil != undefined)? $filter('date')(new Date($scope.CPKS_AddUnitPKSValidUntil), "yyyy-MM-dd"): ""
	// 			};
	// 		}
	// 		else if (groupType == 'affiliated company') //Affiliated Company
	// 		{
	// 			row_data = {
	// 				"licenseNum": $scope.CPKS_AddUnitPKSLicenseNumText,
	// 				"modelID":$scope.CPKS_AddUnitPKSModel,
	// 				"model": ($scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel] != undefined)?$scope.CPKS_AddUnitPKSModelList[$scope.CPKS_AddUnitPKSModel]:"",
	// 				"vin":$scope.CPKS_AddUnitPKSVINText,
	// 				"personInCharge": $scope.CPKS_AddUnitPKSPICText,
	// 				"address": $scope.CPKS_AddUnitPKSPICAddress,
	// 				"phoneNum": $scope.CPKS_AddUnitPKSPhoneNumText
	// 			};
	// 		}

	// 		if ($scope.CPKS_AddUnitPKSCurrentRow != -1)
	// 		{
	// 			$scope.CPKS_Add_UIGrid.data[$scope.CPKS_AddUnitPKSCurrentRow] = row_data;
	// 		}
	// 		else
	// 		{
	// 			$scope.CPKS_Add_UIGrid.data.push(row_data);
	// 		}

	// 		$scope.CPKS_AddUnitPKSResetVariable();
	// 		$scope.CPKS_AddUnitPKS_Show = false;
	// 		$scope.CPKS_Add_Body_Show = true;
	// 	}
		
	// };

	$scope.CPKS_AddUnitPKS_Cancel_Button_Clicked = function ()
	{
		$scope.CPKS_AddUnitPKS_Show = false;
		$scope.CPKS_Add_Body_Show = true;
		$scope.CPKS_AddUnitPKSResetVariable();
		$scope.disableModelVin = false;
		$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = "";
		$scope.CPKS_AddUnitPKSCustomerPKSID = "";
	};

	$scope.CPKS_AddUnitPKSResetVariable = function ()
	{
		$scope.CPKS_AddUnitPKSLicenseNumText = "";
		$scope.CPKS_AddUnitPKSLicensePlateText = ""
		$scope.CPKS_AddUnitPKSModel = undefined;
		$scope.CPKS_AddUnitPKSVINText = "";
		$scope.CPKS_AddUnitPKSPICText = "";
		$scope.CPKS_AddUnitPKSPhoneNumText = "";
		$scope.CPKS_AddUnitPKSPICAddress = "";
		$scope.CPKS_AddUnitPKSValidUntil = "";
	};

	//##### END UNIT PKS PAGE #####

	//##### TOYOTA ID MODAL #####
	$scope.CPKS_ToyotaID_TotalData = 0;
	$scope.CPKS_ToyotaID_UIGrid = {
		paginationPageSizes: [10,20,30,40,50],
		paginationPageSize: 10,
		enableHorizontalScrollbar: 1,
		useCustomPagination: true,
		useExternalPagination : true,
		enableFiltering:true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: [
						{name:"Toyota ID",field:"toyotaID",displayName:"Toyota ID",enableCellEdit:false},
						{name:"Nama",field:"name",enableCellEdit:false},
						{name:"NPWP",field:"npwp",visible:false},
						{name:"Nama Penanggung Jawab",field:"personInCharge",visible:false},
						{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.CPKS_ToyotaID_Select_Button_Clicked(row)">Pilih</a>'}
					],
		onRegisterApi: function(gridApi) {
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.CPKS_ToyotaID_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.CPKS_ToyotaID_UIGrid_Paging = function(pageNumber){
		$scope.CPKS_AddToyotaIDText = ($scope.CPKS_AddToyotaIDText == undefined)? "" : $scope.CPKS_AddToyotaIDText;
		CustomerPKSFactory.getTotalData_ToyotaID($scope.CPKS_AddToyotaIDText)
		.then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					angular.forEach(res.data.Result, function(value, key){
						$scope.CPKS_ToyotaID_TotalData = value.TotalData;
					});
				}

				console.log("griddd",$scope.CPKS_ToyotaID_UIGrid)
				CustomerPKSFactory.getData_ToyotaID($scope.CPKS_AddToyotaIDText, pageNumber, $scope.CPKS_ToyotaID_UIGrid.paginationPageSize)
				.then(
					function(res)
					{
						if(!angular.isUndefined(res.data.Result)){
							var grid_data = [];
							angular.forEach(res.data.Result, function(value, key){
								grid_data.push({
									"toyotaID" 			: value.ToyotaId,
									"name" 				: value.Name,
									"npwp"				: value.Npwp,
									"billingAddress"	: value.BillingAddress,
									"VehicleId"			: value.VehicleId,
									"personInCharge"	: value.PICName
								});
							});

							$scope.CPKS_ToyotaID_UIGrid.data = grid_data;
							// $scope.CPKS_ToyotaID_UIGrid.totalItems = ($scope.CPKS_ToyotaID_TotalData<res.data.Total)? res.data.Total : $scope.CPKS_ToyotaID_TotalData;
							// $scope.CPKS_ToyotaID_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							// $scope.CPKS_ToyotaID_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
							$scope.CPKS_ToyotaID_UIGrid.paginationCurrentPage = pageNumber;
							$scope.CPKS_ToyotaID_UIGrid.totalItems = res.data.Result[0].TotalData;
						}
					}
				);
			}
		);
	};

	$scope.CPKS_ToyotaID_Select_Button_Clicked = function (row) {
		var index = $scope.CPKS_ToyotaID_UIGrid.data.indexOf(row.entity);

		$scope.disableNamaAlamat = false;
		$scope.CPKS_AddToyotaIDText = $scope.CPKS_ToyotaID_UIGrid.data[index].toyotaID;
		$scope.CPKS_AddCompanyNameText = $scope.CPKS_ToyotaID_UIGrid.data[index].name;
		$scope.CPKS_AddNPWPText = $scope.CPKS_ToyotaID_UIGrid.data[index].npwp;
		$scope.CPKS_AddBillingAddress = $scope.CPKS_ToyotaID_UIGrid.data[index].billingAddress;
		$scope.CPKS_AddContactPersonText = $scope.CPKS_ToyotaID_UIGrid.data[index].personInCharge;

		angular.element('#CPKS_ToyotaID_Modal').modal('hide');
		if ($scope.CPKS_ToyotaID_UIGrid.data[index].toyotaID == $scope.CPKS_AddToyotaIDText){
			$scope.disableNamaAlamat = true;
		} else {
			$scope.disableNamaAlamat = false;
		}
	}
	//##### END TOYOTA ID MODAL #####

	//##### LICENSE NUMBER MODAL #####
	$scope.CPKS_LicenseNum_TotalData = 0;
	$scope.CPKS_LicenseNum_UIGrid = {
		//paginationPageSizes: null,
		paginationPageSizes: [10,20,30,40,50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination : true,
		enableFiltering:true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: [
						{name:"Nomor Polisi",field:"licenseNum",enableCellEdit:false},
						{name:"Vehicle ID",field:"vehicleID",visible:false},
						{name:"Model ID",field:"modelID",visible:false},
						{name:"Model Name",field:"modelName",visible:false},
						{name:"VIN",field:"vin",visible:false},
						{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.CPKS_LicenseNum_Select_Button_Clicked(row)">Pilih</a>'}
					],
		onRegisterApi: function(gridApi) {
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.CPKS_LicenseNum_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.CPKS_LicenseNum_UIGrid_Paging = function(pageNumber){
		CustomerPKSFactory.getTotalData_LicenseNum($scope.CPKS_AddUnitPKSLicenseNumText)
		.then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					angular.forEach(res.data.Result, function(value, key){
						$scope.CPKS_LicenseNum_TotalData = value.TotalData;
					});
				}

				CustomerPKSFactory.getData_LicenseNum($scope.CPKS_AddUnitPKSLicenseNumText, pageNumber, $scope.CPKS_LicenseNum_UIGrid.paginationPageSize)
				.then(
					function(res)
					{
						if(!angular.isUndefined(res.data.Result)){
							var grid_data = [];
							angular.forEach(res.data.Result, function(value, key){
								grid_data.push({
									"licenseNum" 		: value.LicensePlate,
									"VehicleID"			: value.VehicleId,
									"modelID"			: value.VehicleModelId,
									"modelName"			: value.VehicleModelName,
									"vin"				: value.VIN
								});
							});

							$scope.CPKS_LicenseNum_UIGrid.data = grid_data;
							$scope.CPKS_LicenseNum_UIGrid.totalItems = ($scope.CPKS_LicenseNum_TotalData<res.data.Total)? res.data.Total : $scope.CPKS_LicenseNum_TotalData;
							// $scope.CPKS_LicenseNum_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							// $scope.CPKS_LicenseNum_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
							$scope.CPKS_LicenseNum_UIGrid.paginationCurrentPage = pageNumber;
						}
					}
				);
			}
		);
	};

	$scope.CPKS_LicenseNum_Select_Button_Clicked = function (row) {
		var index = $scope.CPKS_LicenseNum_UIGrid.data.indexOf(row.entity);

		$scope.disableModelVin = false;
		$scope.CPKS_AddUnitPKSLicenseNumText = $scope.CPKS_LicenseNum_UIGrid.data[index].licenseNum;
		// $scope.CPKS_AddUnitPKSLicensePlateText = $scope.CPKS_LicenseNum_UIGrid.data[index].licenseNum;
		$scope.CPKS_AddUnitPKSModel = $scope.CPKS_LicenseNum_UIGrid.data[index].modelID;
		$scope.CPKS_AddUnitPKSVINText = $scope.CPKS_LicenseNum_UIGrid.data[index].vin;
		angular.element('#CPKS_LicenseNum_Modal').modal('hide');
		console.log("1")
		if ($scope.CPKS_LicenseNum_UIGrid.data[index].licenseNum == $scope.CPKS_AddUnitPKSLicenseNumText){
			$scope.disableModelVin = true;
		} else {
			$scope.disableModelVin = false;
		}
	}
	//##### END LICENSE NUMBER MODAL #####

	//UPLOAD
	$scope.ExcelData = [];
	$scope.getJsDateFromExcel = function(excelDate)
	{
		var utc_days  = Math.floor(excelDate - 25569);
		var utc_value = utc_days * 86400;
		var date_info = new Date(utc_value * 1000);

		return date_info;
	}

	$scope.loadXLS = function(ExcelFile){
		var myEl = angular.element( document.querySelector( '#CPKS_Add_UploadVehicleList' ) ); //ambil elemen dari dokumen yang di-upload

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
			// $scope.ExcelData = json;

			ExcelData = json;
            ExcelItem = json[2];
            exTmp = json;
            $scope.ExcelData = $scope.ExcelData.slice(1);
            console.log("data", $scope.ExcelData)
            for (var y = 0; y < exTmp.length; y++) {
                var formatKey = Object.keys(exTmp[y]);
                var tmpReformatKey = angular.copy(formatKey);
                console.log("formatkey", formatKey,exTmp[y])
				if ((exTmp[y].No_Polisi != null && exTmp[y].No_Polisi != "" && exTmp[y].No_Polisi != undefined)||
					(exTmp[y].Model != null && exTmp[y].Model != "" && exTmp[y].Model != undefined)) {
                    console.log("extmp", exTmp[y], json);
                    $scope.ExcelData.push(exTmp[y]);
                }

            }

			console.log("ExcelData : ", $scope.ExcelData);
			console.log("ExcelData : ", ExcelItem);
            console.log("Extmp : ", exTmp);
            console.log("myEl : ", myEl);
            console.log("json : ", json);
		});
	}

	$scope.CPKS_Add_Upload_Button_Clicked = function ()
	{
		// var count = 0;
		var tmpResult = [];
		// var countNotif = 0
		$scope.ExcelData = $scope.ExcelData.slice(1)
		tmpExcelData = angular.copy($scope.ExcelData);

		var lookup = {};
		for(var item, idx = 0; item = tmpExcelData[idx++];){
			var name = item.Model;
			if(!(name in lookup))	{
				lookup[name] = 1;
				tmpResult.push(name.trim());
			}

		}
		tmpExcelData = tmpResult;
		console.log('=====>>',tmpResult, tmpExcelData);
		if(tmpExcelData != undefined && tmpExcelData != "" && tmpExcelData != 0){
			for(var xx in tmpExcelData){
				var itungModel =1;
				tmpExcelData[xx] = (tmpExcelData[xx]).toLowerCase();
				for(var x in $scope.CPKS_AddUnitPKSModelOption){
					if(tmpExcelData[xx] == $scope.CPKS_AddUnitPKSModelOption[x].name.toLowerCase() ){
						// var TmpModel = $scope.CPKS_AddUnitPKSModelOption[x].name;
						// var TmpModelId = $scope.CPKS_AddUnitPKSModelOption[x].value;
						console.log("modelid", tmpExcelData[xx], $scope.CPKS_AddUnitPKSModelOption[x].name.toLowerCase() )
						break;
					}else{
						itungModel++
					}
					if(itungModel == $scope.CPKS_AddUnitPKSModelOption.length ){
						console.log("itungan", itungModel, $scope.CPKS_AddUnitPKSModelOption.length)
						bsAlert.warning("Format Data Ada yang Salah","Model Tidak Ditemukan")
						ngDialog.closeAll();
						return false;
					}
				}
			}
		}
		angular.forEach($scope.ExcelData, function(value, key){
			$scope.CPKS_AddUnitPKSCustomerPKSVehicleID = 0
			CustomerPKSFactory.cekLicensePlateAvailable(value.No_Polisi, $scope.CPKS_AddUnitPKSCustomerPKSVehicleID,value.VIN).then(function (res) {
				var resu = res.data
				console.log('cek res >>', resu);
				
				if (resu[0] == "false") {
					bsAlert.alert({
						// title: "Kendaraan dengan No. Polisi " + $scope.CPKS_AddUnitPKSLicenseNumText + " sudah terdaftar di group PKS " + resu[1],
						title: "Kendaraan dengan No. Polisi " + value.No_Polisi + " sudah terdaftar",
						text: "",
						type: "warning",
						showCancelButton: false
					});
				}else{ 
					var groupType = ($scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType]!=undefined)?$scope.CPKS_AddGroupTypeList[$scope.CPKS_AddGroupType].toLowerCase() : "";
					var row_data;
					var modelID = $scope.CPKS_AddUnitPKSModelList2[value.Model];
					
					// x
					angular.forEach($scope.CPKS_AddUnitPKSModelOption, function(value1, key1){
						value.Model = value.Model.trim();
						if (value.Model.toLowerCase() == value1.name.toLowerCase())
						{
							modelID = value1.value;
						}
					});
					for(var yy in $scope.CPKS_Add_UIGrid.data){
						if( value.No_Polisi == $scope.CPKS_Add_UIGrid.data[yy].licenseNum && value.Model.toLowerCase() == $scope.CPKS_Add_UIGrid.data[yy].model.toLowerCase() && value.VIN.toLowerCase() == $scope.CPKS_Add_UIGrid.data[yy].vin.toLowerCase()){
							bsAlert.warning("Data Sudah Ada")
							return false;
						}
					}

					
					if((value.No_Polisi == 0 || value.No_Polisi == null || value.No_Polisi == undefined || value.No_Polisi == '')|| 
					(value.Model == 0 || value.Model == null || value.Model == undefined || value.Model == '') ||
					(value.VIN == 0 || value.VIN == null || value.VIN == undefined || value.VIN == '') ||
					(value.Nama_PIC == 0 || value.Nama_PIC == null || value.Nama_PIC == undefined || value.Nama_PIC == '') ||
					(value.Alamat == 0 || value.Alamat == null || value.Alamat == undefined || value.Alamat == '') ||
					(value.Valid_To == 0 || value.Valid_To == null || value.Valid_To == undefined || value.Valid_To == '')){

						bsAlert.warning("Ada Data yang Masih Kosong")
						return false;
					}
					
					if (groupType == 'pks') //PKS
					{
						row_data = {
							"licenseNum": value.No_Polisi,
							"modelID":modelID,
							"model": value.Model,
							"vin":value.VIN,
							"personInCharge": value.Nama_PIC,
							"address": value.Alamat,
							"phoneNum": value.Handphone,
							// "validUntil": $filter('date')(new Date($scope.getJsDateFromExcel(value.Valid_To)), "yyyy-MM-dd")
							"validUntil": $filter('date')(new Date(value.Valid_To), "yyyy-MM-dd")
						};
						// countNotif++
						// bsNotify.show({
						// 		title: "Success",
						// 		content: "Data berhasil diupload",
						// 		type: 'success'
						// });
						// console.log("Tanggal dari excel : "+value.Valid_To);
					}
					else if (groupType == 'affiliated company') //Affiliated Company
					{
						row_data = {
							"licenseNum": value.No_Polisi,
							"modelID":modelID,
							"model": value.Model,
							"vin":value.VIN,
							"personInCharge": value.Nama_PIC,
							"address": value.Alamat,
							"phoneNum": value.Handphone,
							"validUntil": $filter('date')(new Date(value.Valid_To), "yyyy-MM-dd")
						};
						// countNotif++
					}
					$scope.CPKS_Add_UIGrid.data.push(row_data);
					}
					
				})
				
		},$scope.CPKS_Add_UIGrid.data);
		angular.element('#CPKS_Add_Upload_Modal').modal('hide');
	}

	$scope.changeFormatDate = function(item) {
		var tmpDate = item;
		console.log("changeFormatDate item", item);
		tmpDate = new Date(tmpDate);
		var finalDate
		var yyyy = tmpDate.getFullYear().toString();
		var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpDate.getDate().toString();
		finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	}

	$scope.alphaNumericOnly = function(string,model){
		$scope[model] = string.replace(/[^a-zA-Z0-9]/g,'');
	 }




}).filter('CurrencyTitik', function () {
    return function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };
});
