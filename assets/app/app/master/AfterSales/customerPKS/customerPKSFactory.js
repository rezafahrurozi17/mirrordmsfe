angular.module('app')
    .factory('CustomerPKSFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        var outletId = user.OutletId === undefined ? user.OrgId : user.OutletId;
        return {
            getTotalData_CustomerPKS: function(FilterColumn, FilterTeks) {
                var url;
                if (FilterTeks == undefined || FilterTeks.trim().length == 0) {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataCustomerPKS?filterColumn=' + FilterColumn + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataCustomerPKS?filterColumn=' + FilterColumn + '&filterTeks=' + FilterTeks + '&outletId=' + outletId;
                }

                console.log("url GetTotalDataCustomerPKS : ", url);
                var response = $http.get(url);
                return response;
            },
            getData_CustomerPKS: function(Start, Limit, FilterColumn, FilterTeks) {
                var url;
                if (FilterTeks == undefined || FilterTeks.trim().length == 0) {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataCustomerPKS?start=' + Start + '&limit=' + Limit + '&filterColumn=' + FilterColumn + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataCustomerPKS?start=' + Start + '&limit=' + Limit + '&filterColumn=' + FilterColumn + '&filterTeks=' + FilterTeks + '&outletId=' + outletId;
                }

                console.log("url GetDataCustomerPKS : ", url);
                var response = $http.get(url);
                return response;
            },
            getData_CustomerPKSById: function(customerPKSID) {
                var url = '/api/as/AfterSalesCustomerPKS/GetCustomerPKSByID?outletId=' + outletId + '&customerPKSID=' + customerPKSID;
                console.log("url GetCustomerPKSByID : ", url);
                var res = $http.get(url);
                return res;
            },
            getFullData: function() {
                var url = '/api/as/AfterSalesCustomerPKS/GetFullData?outletId=' + outletId ;
                console.log("url GetFullData : ", url);
                var res = $http.get(url);
                return res;
            },
            delete_CustomerPKS: function(customerPKSID) {
                var url = '/api/as/AfterSalesCustomerPKS/GetDeleteCustomerPKS?outletId=' + outletId + '&customerPKSID=' + customerPKSID;
                console.log("url GetDeleteCustomerPKS : ", url);
                var res = $http.get(url);
                return res;
            },
            get_GroupTypeList: function() {
                var url = '/api/as/AfterSalesCustomerPKS/GetGroupTypeList?outletId=' + outletId;
                console.log("url GetGroupTypeList : ", url);
                var res = $http.get(url);
                return res;
            },
            get_ModelList: function() {
                var url = '/api/as/AfterSalesCustomerPKS/GetModelList?outletId=' + outletId;
                console.log("url GetModelList : ", url);
                var res = $http.get(url);
                return res;
            },
            getTotalData_Vehicle: function(CustomerPKSId, FilterColumn, FilterTeks) {
                var url;
                CustomerPKSId = (CustomerPKSId == undefined) ? "" : CustomerPKSId;
                if (FilterTeks == undefined || FilterTeks.trim().length == 0) {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataVehicle?customerPKSID=' + CustomerPKSId + '&filterColumn=' + FilterColumn + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataVehicle?customerPKSID=' + CustomerPKSId + '&filterColumn=' + FilterColumn + '&filterTeks=' + FilterTeks + '&outletId=' + outletId;
                }

                console.log("url GetTotalDataVehicle : ", url);
                var response = $http.get(url);
                return response;
            },
            getData_Vehicle: function(CustomerPKSId, Start, Limit, FilterColumn, FilterTeks) {
                var url;
                CustomerPKSId = (CustomerPKSId == undefined) ? "" : CustomerPKSId;
                if (FilterTeks == undefined || FilterTeks.trim().length == 0) {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataVehicle?customerPKSID=' + CustomerPKSId + '&start=' + Start + '&limit=' + Limit + '&filterColumn=' + FilterColumn + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataVehicle?customerPKSID=' + CustomerPKSId + '&start=' + Start + '&limit=' + Limit + '&filterColumn=' + FilterColumn + '&filterTeks=' + FilterTeks + '&outletId=' + outletId;
                }

                console.log("url GetDataVehicle : ", url);
                var response = $http.get(url);
                return response;
            },
            delete_Vehicle: function(customerPKSVehicleID) {
                var url = '/api/as/AfterSalesCustomerPKS/GetDeleteVehicle?customerPKSVehicleID=' + customerPKSVehicleID + '&outletId=' + outletId;
                console.log("url GetDeleteVehicle : ", url);
                var res = $http.get(url);
                return res;
            },
            getData_VehicleById: function(customerPKSVehicleID) {
                var url = '/api/as/AfterSalesCustomerPKS/GetDataVehicleByID?outletId=' + outletId + '&customerPKSVehicleID=' + customerPKSVehicleID;
                console.log("url GetDataVehicleByID : ", url);
                var res = $http.get(url);
                return res;
            },
            getTotalData_ToyotaID: function(toyotaID) {
                var url;

                if (toyotaID == undefined || toyotaID.trim() == "") {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataToyotaID?outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataToyotaID?toyotaID=' + toyotaID + '&outletId=' + outletId;
                }

                console.log("url GetTotalDataToyotaID : ", url);
                var response = $http.get(url);
                return response;
            },
            getData_ToyotaID: function(toyotaID, Start, Limit) {
                var url;

                if (toyotaID == undefined || toyotaID.trim() == "") {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataToyotaID?start=' + Start + '&limit=' + Limit + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataToyotaID?toyotaID=' + toyotaID + '&start=' + Start + '&limit=' + Limit + '&outletId=' + outletId;
                }

                console.log("url GetDataToyotaID : ", url);
                var response = $http.get(url);
                return response;
            },
            getTotalData_LicenseNum: function(licenseNum) {
                var url;

                if (licenseNum == undefined || licenseNum.trim() == "") {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataLicenseNum?outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetTotalDataLicenseNum?licenseNum=' + licenseNum + '&outletId=' + outletId;
                }

                console.log("url GetTotalDataLicenseNum : ", url);
                var response = $http.get(url);
                return response;
            },
            getData_LicenseNum: function(licenseNum, Start, Limit) {
                var url;

                if (licenseNum == undefined || licenseNum.trim() == "") {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataLicenseNum?start=' + Start + '&limit=' + Limit + '&outletId=' + outletId;
                } else {
                    url = '/api/as/AfterSalesCustomerPKS/GetDataLicenseNum?licenseNum=' + licenseNum + '&start=' + Start + '&limit=' + Limit + '&outletId=' + outletId;
                }

                console.log("url GetDataLicenseNum : ", url);
                var response = $http.get(url);
                return response;
            },
            saveData_CustomerPKS: function(InputData) {
                var url = '/api/as/AfterSalesCustomerPKS/PostSaveData/';
                var param = JSON.stringify(InputData);
                console.log("object input PostSaveData", param);
                var res = $http.post(url, param);
                return res;
            },
            editData_CustomerPKS: function(InputData) {
                var url = '/api/as/AfterSalesCustomerPKS/PostEditData/';
                var param = JSON.stringify(InputData);
                console.log("object input PostEditData", param);
                var res = $http.post(url, param);
                return res;
            },
            saveData_Vehicle: function(InputData) {
                var url = '/api/as/AfterSalesCustomerPKS/PostSaveDataVehicle/';
                var param = JSON.stringify(InputData);
                console.log("object input PostSaveDataVehicle", param);
                var res = $http.post(url, param);
                return res;
            },
            editData_Vehicle: function(InputData) {
                var url = '/api/as/AfterSalesCustomerPKS/PostEditDataVehicle/';
                var param = JSON.stringify(InputData);
                console.log("object input PostEditDataVehicle", param);
                var res = $http.post(url, param);
                return res;
            },
            // cekLicensePlateAvailable: function (licensePlate) {
            //     return $http.get('/api/as/CekPoliceNumberPKS/' + licensePlate)
            // }
            cekLicensePlateAvailable: function (licensePlate, customerPKSVehicleID, Vin) {
                var url  = '/api/as/CekPoliceNumberPKS?LicensePlate=' + licensePlate + '&CustomerPKSVehicleId=' + customerPKSVehicleID + '&Vin=' + Vin + '&OutletId=' + outletId;
                var res = $http.get(url);
                return res;
            },
            CekCustomerPKS: function (NoPKS, CustomerPKSId,GroupTypeId) {
                var url  = '/api/as/CekCustomerPKS?NoPKS=' + NoPKS + '&CustomerPKSId=' + CustomerPKSId + '&GroupTypeId='+ GroupTypeId + '&OutletId=' + outletId;
                var res = $http.get(url);
                return res;
            }
        }
    });