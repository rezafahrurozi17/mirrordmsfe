angular.module('app')
    .factory('TaskListGRFactory', function ($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            // getData: function(){
            // var url = '/api/as/AfterSalesCheck/GetCheckData/?TypeId=2';
            // console.log("url TaxType : ", url);
            // var res=$http.get(url);
            // return res;
            // },
            getListTaskListGRData: function (start, limit, modelKendaraan, operationDescription, operationNumber, categoryId) {
                // var url = '/api/as/AfterSalesMasterTaskListGR/GetListDataTaskListGR/?start=' + start + '&limit=' + limit + '&ModelKendaraan=' + modelKendaraan
                // 			+ '&OperationDescription=' + operationDescription + '&OperationNumber=' + operationNumber + '&OutletId=' + user.OutletId + '&CategoryId=' + categoryId;
                // console.log("url TaskListGR : ", url);
                // var res=$http.get(url);
                modelKendaraan = modelKendaraan == null ? '-' : modelKendaraan;
                operationDescription = operationDescription == null ? '-' : operationDescription;
                var res = $http.get('/api/as/AfterSalesMasterTaskListGR/GetListDataTaskListGR', {
                    params: {
                        start: start,
                        limit: limit,
                        ModelKendaraan: modelKendaraan,
                        OperationDescription: operationDescription,
                        OperationNumber: operationNumber,
                        OutletId: user.OutletId,
                        CategoryId: categoryId
                    }
                });

                return res;

            },
            getListTaskListGR: function (start, limit, modelKendaraan, operationDescription, OperationNumber, categoryId) {
                modelKendaraan = modelKendaraan == null ? '-' : modelKendaraan;
                operationDescription = operationDescription == null ? '-' : operationDescription;
                categoryId = categoryId == "" ? 0 : categoryId;
                return $http.get('/api/as/AfterSalesMasterTaskListGR/NewGetListDataTaskListGR?start=' + start + '&limit=' + limit + '&ModelKendaraan=' + modelKendaraan + '&OperationDescription=' + operationDescription + '&OperationNumber=' + OperationNumber + '&CategoryId=' + categoryId)
            },
            getDataVehicleModel: function () {
                return $http.get("/api/as/AfterSalesMasterTaskListGR/getVehicleModel"
                );
            },

            getDataOperationNumber: function () {
                return $http.get("/api/as/AfterSalesMasterTaskListGR/getOperationNumber");
            },

            getDataOperationDescription: function (modelId) {
                return $http.get("/api/as/AfterSalesMasterTaskListGR/getOperationDescription/" + modelId);
            },

            getDataCategory: function () {
                var url = '/api/as/AfterSalesMasterTaskListGR/GetDataCategory';
                console.log("url Category List : ", url);
                var res = $http.get(url);
                return res;

            },
            getDataVehicleModel1: function () {
                return $http.get("/api/as/AfterSalesMasterTaskListGR/GetDataModel");
            },

            getDataUOM: function () {
                var url = '/api/as/AfterSalesMasterTaskListGR/GetDataUOM';
                console.log("url UOM List : ", url);
                var res = $http.get(url);
                return res;

            },

            create: function (inputData) {
                var url = '/api/as/AfterSalesMasterTaskListGR/SaveData';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res; 
            },
            UploadData: function (uploadExcel) {
                var url = '/api/as/AfterSalesMasterTaskListGR/UploadData/';
                var param = JSON.stringify(uploadExcel);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },
            CreateParts: function (data) {
                var url = '/api/as/PostTaskList_Parts';
                var res = $http.post(url, data);
                return res
            },
            getUnitMeasurement: function () {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                // api/as/GetTaskList_Parts/{taskid}
                return res;
            },
            GetDataPartsByTaskId: function (taskid) {
                var res = $http.get('/api/as/GetTaskList_Parts/' + taskid);
                console.log('resnya pause=>', res);
                return res;
            },
            getDataParts: function (Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getFullModelFromModel: function(modelId){
                return $http.get("/api/as/AfterSalesMasterTaskListGR/GetDataFullModel",{
                    params: {
                        VehicleModelId : modelId
                    }
                });
            },
            getVehicleTypeById: function (id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            DeleteDataGR: function (taskId) {
                var url = '/api/as/AfterSalesMasterTaskListGR/DeleteData/?TaskId=' + taskId;
                console.log("url Category List : ", url);
                var res = $http.delete(url);
                return res;
            },
        }
    });