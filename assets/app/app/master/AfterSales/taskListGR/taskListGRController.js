var app = angular.module('app');
app.controller('TaskListGRController', function ($scope, $http, $filter, CurrentUser, TaskListGRFactory, $timeout, bsNotify, bsAlert, ngDialog) {
    var user = CurrentUser.user();

    $scope.uiGridPageSize = 10;
    $scope.uiGridPageNumber = 1;
    $scope.TaskListGRMain_Show = true;
    $scope.TaskListGREdit_Show = false;
    $scope.ExcelData = [];
    $scope.TaskListGR_ListCategory = [];
    $scope.optionsTaskListGRMain_OperationDescription = [];
    $scope.listUOM = [];

    $scope.$on('$viewContentLoaded', function () {
        $scope.loading = false;
        $scope.gridData = [];

        $scope.getDataCombobox();
    });

    $scope.getDataVehicleModel = function () {
        TaskListGRFactory.getDataVehicleModel().then(function (res) {
            var tempData = [];
            angular.forEach(res.data.Result, function (value, key) {
                // tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
                tempData.push({ name: value.VehicleModelName, value: value.VehicleModelId })
            });
            $scope.optionsTaskListGRMain_ModelKendaraan = tempData;
        });
    };

    $scope.selectedModelFilter = function (item) {
        console.log("selectedModelFilter", item);
        if (angular.isUndefined(item)) {
            $scope.optionsTaskListGRMain_OperationDescription = [];
            $scope.TaskListGRMain_OperationDescription = null;
        } else {
            TaskListGRFactory.getDataOperationDescription(item.value).then(function (res) {
                var tempData = [];
                angular.forEach(res.data.Result, function (value, key) {
                    tempData.push({ name: value.OperationDescription, value: value.OperationDescription })
                });
                $scope.optionsTaskListGRMain_OperationDescription = tempData;
                console.log("taskname", $scope.optionsTaskListGRMain_OperationDescription)
            });
        };
    };

    // $scope.onChangeModelFilter = function(item){
    //     console.log("onChangeModelFilter", item);

    // };

    $scope.getData = function () {
        TaskListGRFactory.getDataVehicleModel1().then(function (res) {
            var tempData1 = [];
            angular.forEach(res.data.Result, function (value, key) {
                // tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
                tempData1.push({ name: value.VehicleModelName, value: value.VehicleModelId })
            });
            $scope.optionsTaskListGRMain1_ModelKendaraan = tempData1;
            console.log("modelName", $scope.optionsTaskListGRMain1_ModelKendaraan)
        });
    };

    //==
    // $scope.onSelectModel = function(selected){
    // 	TaskListBPFactory.getFullModelFromModel(selected.value).then(function(res){
    // 		var tempData = [];
    // 		angular.forEach(res.data.Result,function(value,key){
    // 			// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
    // 			tempData.push({name:value.VehicleTypeName,value:value.VehicleTypeId})
    // 		});
    // 		$scope.optionsTaskListGRMain_FullModel = tempData;
    // 	});
    // }
    //==========================new=========================
    $scope.Grid_Paging = function (pageNumber, pageSize) {
        var ModelKendaraan = '';
        var OperationDescription = '';
        var TipePekerjaan = 0;
        if (pageSize == null || pageSize == undefined) {
            $scope.uiGridPageSize = 10;
        } else {
            $scope.uiGridPageSize = pageSize;
        }

        if (!angular.isUndefined($scope.TaskListBPMain.ModelKendaraan))
            ModelKendaraan = $scope.TaskListBPMain.ModelKendaraan;

        if (!angular.isUndefined($scope.TaskListBPMain.OperationDescription))
            OperationDescription = $scope.TaskListBPMain.OperationDescription;

        if (!angular.isUndefined($scope.TaskListBPMain.ActivityType))
            TipePekerjaan = $scope.TaskListBPMain.ActivityType;
        // console.log("TipePekerjaan",TipePekerjaan);
        // console.log("TipePekerjaan",$scope.TaskListBPMain.ActivityType);

        console.log("$scope.TaskListBPMain.FreeSearch", $scope.TaskListBPMain.ModelKendaraan);
        console.log("$scope.TaskListBPMain.TypeSearch", $scope.TaskListBPMain.OperationDescription);
        console.log("$scope.TaskListBPMain.TypeSearch", $scope.TaskListBPMain.ActivityType);
        TaskListBPFactory.getListTaskListGR(
            pageNumber,
            $scope.uiGridPageSize,
            ModelKendaraan,
            OperationDescription,
            TipePekerjaan,
            $scope.TaskListBPMain.FreeSearch,
            $scope.TaskListBPMain.TypeSearch
        )
            .then(
                function (res) {
                    console.log("res =>", res.data.Result);
                    $scope.grid.data = res.data.Result;
                    $scope.grid.totalItems = res.data.Result[0].TotalData;
                }

            );
    }

    //======================================================


    $scope.getDataOperationNumber = function () {
        TaskListGRFactory.getDataOperationNumber().then(function (res) {
            var tempData = [];
            angular.forEach(res.data.Result, function (value, key) {
                tempData.push({ name: value.OperationNumber, value: value.OperationNumber })
            });
            $scope.optionsTaskListGRMain_OperationNumber = tempData;
            console.log("taskcode", $scope.optionsTaskListGRMain_OperationNumber)
        });
    };

    // $scope.getDataOperationDescription = function () {
    //     TaskListGRFactory.getDataOperationDescription().then(function (res) {
    //         var tempData = [];
    //         angular.forEach(res.data.Result, function (value, key) {
    //             tempData.push({ name: value.OperationDescription, value: value.OperationDescription })
    //         });
    //         $scope.optionsTaskListGRMain_OperationDescription = tempData;
    //         console.log("taskname", $scope.optionsTaskListGRMain_OperationDescription)
    //     });
    // };

    $scope.getDataCombobox = function () {
        // $scope.getDataVehicleModel();
        $scope.getData();
        // $scope.getDataVehicleType();
        $scope.getDataOperationNumber();
        // $scope.getDataOperationDescription();
    }

    $scope.getDataCombobox();

    $scope.TaskListGRMain_Tambah_Clicked = function () {
        $scope.TaskListGRMain_Show = false;
        $scope.TaskListGREdit_Show = true;
        $scope.mData = {};
        $scope.TaskListGREdit_TaskId = 0; //Default ketika klik tambah task baru by UI, taskid di default 0;
        $scope.gridPartsDetail.data = [];
    }
    var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
        '<a href="#" ng-click="grid.appScope.MainTaskListGR_Edit(row)" ng-hide="row.entity.TaskId == null" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" title="Edit" style="color:#777; font-size:16px; margin-top:5px; margin-right:5px"></i></a>' +
        '<a href="#" ng-click="grid.appScope.MainTaskListGR_Delete(row)" ng-hide="row.entity.TaskId == null" uib-tooltip="Hapus" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-trash" title="Hapus" style="color:#777; font-size:16px; margin-top:5px; margin-right:5px"></i></a>' +
        '</div>';

    $scope.TaskListGRGrid = {
        // paginationPageSizes: null,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        useCustomPagination: true,
        useExternalPagination: true,
        enableSorting: true,
        multiSelect: false,
        enableFiltering: true,
        enableColumnResizing: true,
        enableRowSelection: true,
        columnDefs: [
            { name: 'TaskId', field: 'TaskId', visible: false },
            { name: 'OutletId', field: 'OutletId', visible: false },
            // { name: 'OutletCode', field: 'OutletCode', width: 180, visible: false },
            { name: 'No', displayName: "No", field: 'Nomor', width: 60 },
            { name: 'WMI', displayName: "WMI", field: 'WMI', width: 180 },
            { name: 'VDS', displayName: "VDS", field: 'VDS', width: 180 },
            { name: 'KatashikiCode', field: 'KatashikiCode', width: 180, visible: false },
            //{ name: 'Suffix', field: 'Suffix', width:40},
            { name: 'Operation No', field: 'OperationNo', width: 180 },
            { name: 'Operation Description', field: 'OperationDescription', width: 180 },
            { name: 'Activity Type Code', field: 'ActivityTypeCode', width: 180 },
            { name: 'ActivityTypeId', field: 'ActivityTypeId', visible: false },
            { name: 'Service Rate', field: 'FlatRate', width: 180 },
            { name: 'Standard Actual Rate', field: 'StandardActualRate', width: 180 },
            // { name: 'Operation UOM Code', field: 'OperationUOMCode', width:180},
            { name: 'Operation UOM Code', displayName: "Operation UOM Code", field: 'OperationUOMCode', width: 180 },
            { name: 'OperationUOMId', field: 'OperationUOMId', visible: false },
            { name: 'Model Name', field: 'ModelName', width: 180 },
            { name: 'Full Model', field: 'FullModel', width: 180 },
            { name: 'Kilometer', field: 'Kilometer', width: 180 },
            { name: 'Upload Date', field: 'UploadDate', cellFilter: 'date:\"dd-MM-yyyy\"', width: 180 },
            { name: 'Valid From', field: 'ValidFrom', cellFilter: 'date:\"dd-MM-yyyy\"', width: 180 },
            { name: 'Valid To', field: 'ValidTo', cellFilter: 'date:\"dd-MM-yyyy\"', width: 180 },
            // { name: 'Prepared By', field: 'PreparedBy', width: 180 },
            // { name: 'Prepared Date', field: 'PreparedDate', cellFilter: 'date:\"dd-MM-yyyy\"', width: 180 },
            {
                name: "Action",
                width: 100,
                cellTemplate: gridActionButtonTemplate
            },
            // { name:"Action", width:100, 
            //   cellTemplate:'<a ng-click="grid.appScope.MainTaskListBP_Edit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainTaskListBP_Delete(row)">Delete</a>'}
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridAPITaskListGRGrid = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                console.log('nah ini pagenumber dan page size', pageNumber)
                console.log('nah ini pagenumber dan page size', pageSize)
                $scope.uiGridPageSize = pageSize;
                $scope.uiGridPageNumber = pageNumber;
                $scope.TaskListGRGrid_Paging(pageNumber, pageSize);

            });

            gridApi.selection.on.rowSelectionChanged($scope,
                function (row) {
                    $scope.dataTask = row.entity;
                    console.log('selected row=>', row.entity);
                    $scope.dataTask.length = 0;
                    if (row.isSelected) {
                        TaskListGRFactory.GetDataPartsByTaskId($scope.dataTask.TaskId).then(function (resu) {
                            console.log("form onSelectRows getJobParts=>4", resu.data.Result);
                            var data = resu.data.Result;
                            _.map(data, function (val) {
                                val.satuanName = val.Satuan;
                            });
                            // $scope.gridPartsDetail.data = data;
                            $scope.gridPartsFront.data = data;

                        })
                    } else {
                        $scope.gridPartsFront.data = [];
                    }
                });
            if ($scope.gridApi.selection.selectRow) {
                $scope.gridApi.selection.selectRow($scope.TaskListGRGrid.data[0]);
            }
        }

        // onRegisterApi: function(gridApi) {
        //     $scope.GridAPITaskListGRGrid = gridApi;
        //     $scope.GridAPITaskListGRGrid.selection.on.rowSelectionChanged($scope, function(row) {
        //         $scope.selectedRows = $scope.GridAPITaskListGRGrid.selection.getSelectedRows();
        //         // console.log("selected=>",$scope.selectedRows);
        //         if ($scope.onSelectRows) {
        //             $scope.onSelectRows($scope.selectedRows);
        //         }
        //     });
        //     // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
        //     //     $scope.TaskListGRGrid_Paging(pageNumber);
        //     // });
        // }
    }
    $scope.onSelectRows = function (rows) {
        console.log("form onSelectRows=>", rows);
        if (rows.length != 0) {
            TaskListGRFactory.GetDataPartsByTaskId(rows[0].TaskId).then(function (resu) {
                console.log("form onSelectRows getJobParts=>1", resu.data.Result);
                var data = resu.data.Result;
                for (var i in data) {
                    if (data[i].ETA == "Mon Jan 01 1900 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                        data[i].ETA = "-";
                    }
                }
                // $scope.gridPartsFront.data = data;
                $scope.gridPartsDetail.data = data;
            })
        }
    };

    $scope.TaskListGRGrid_Paging = function (pageNumber, pageSize) {
        var ModelKendaraan = '-';
        var OperationDescription = '-';
        var TipePekerjaan = 0;
        var OperationNumber = '-';
        console.log("TaskListGRMain_TipePekerjaan =>", $scope.TaskListGRMain_TipePekerjaan);

        if (!angular.isUndefined($scope.TaskListGRMain_ModelKendaraan)) {
            ModelKendaraan = $scope.TaskListGRMain_ModelKendaraan;
            console.log('kendaraan', ModelKendaraan)
            for (var x in $scope.optionsTaskListGRMain1_ModelKendaraan) {
                if (ModelKendaraan == $scope.optionsTaskListGRMain1_ModelKendaraan[x].value) {
                    ModelKendaraan = $scope.optionsTaskListGRMain1_ModelKendaraan[x].name;
                    console.log("model Kendaraan", $scope.optionsTaskListGRMain1_ModelKendaraan[x].name, $scope.optionsTaskListGRMain1_ModelKendaraan[x].value)
                    break;
                }
            }
        }

        if (!angular.isUndefined($scope.TaskListGRMain_OperationDescription))
            OperationDescription = $scope.TaskListGRMain_OperationDescription;

        if (!angular.isUndefined($scope.TaskListGRMain_TipePekerjaan))
            TipePekerjaan = $scope.TaskListGRMain_TipePekerjaan === undefined ? 0 : $scope.TaskListGRMain_TipePekerjaan;

        if (!angular.isUndefined($scope.TaskListGRMain_OperationNumber))
            OperationNumber = $scope.TaskListGRMain_OperationNumber;

        TaskListGRFactory.getListTaskListGR($scope.uiGridPageNumber, $scope.uiGridPageSize, ModelKendaraan, OperationDescription, OperationNumber, TipePekerjaan)
            .then(
                function (res) {
                    console.log("res =>", res.data.Result);
                    if (res.data.Result.length == 0 || res.data.Result == "" || res.data.Result == 0 || res.data.Result == null || res.data.Result == undefined) {
                        bsAlert.warning("Data Tidak Tersedia")
                        return false;
                    } else {
                        angular.forEach(res.data.Result, function (value, key) {
                            //value.FlatRate = toFixed(1)(val.FlatRate)
                            var FlatRate = value.FlatRate;
                            value.FlatRate = FlatRate.toFixed(1);

                            for (var d = 0; d < $scope.TaskListGREdit_SetActivityTypeCode.length; d++) {
                                value.Activity_TypeCode = $scope.TaskListGREdit_SetActivityTypeCode[d].name;
                                console.log("activity code", value.Activity_TypeCode, $scope.TaskListGREdit_SetActivityTypeCode[d].name)
                            }

                            var StandardActualRate = value.StandardActualRate;
                            value.StandardActualRate = StandardActualRate.toFixed(1);

                            console.log("upload date : ", $filter('date')(new Date(value.UploadDate.toLocaleString()), "yyyyMMdd"));
                            // if ($filter('date')(new Date(value.UploadDate.toLocaleString()), "yyyyMMdd") == '20011231') {
                            //     value.UploadDate = undefined;
                            // }

                            // if ($filter('date')(new Date(value.ValidFrom.toLocaleString()), "yyyyMMdd") == '20011231') {
                            //     value.ValidFrom = undefined;
                            // }

                            // if ($filter('date')(new Date(value.ValidTo.toLocaleString()), "yyyyMMdd") == '20011231') {
                            //     value.ValidTo = undefined;
                            // }

                            // if ($filter('date')(new Date(value.PreparedDate.toLocaleString()), "yyyyMMdd") == '20011231') {
                            //     value.PreparedDate = undefined;
                            // }

                        });
                        $scope.TaskListGRGrid.data = res.data.Result;
                        console.log("data upload =>", $scope.TaskListGRGrid.data)
                        // $scope.TaskListGRGrid.totalItems = res.data.Total;
                        // $scope.TaskListGRGrid.paginationPageSize = $scope.uiGridPageSize;
                        // $scope.TaskListGRGrid.paginationPageSizes = [$scope.uiGridPageSize];
                    }
                }

            );
    };

    $scope.TaskListGRMain_Cari_Clicked = function (item) {
        $scope.gridPartsDetail.data = [];
        $scope.gridPartsFront.data = [];
        console.log('TaskListGRMain_Cari_Clicked', item);
        // if (typeof item === 'undefined') {
        //     bsNotify.show({
        //         size: 'big',
        //         type: 'danger',
        //         title: "Mohon input filter model terlebih dahulu",
        //     });
        // } else {
        $scope.TaskListGRGrid_Paging();
        //$scope.gridPartsDetail();
        $scope.disVehic = true;
        // };
    }

    $scope.MainTaskListGR_Delete = function (row) {
        console.log("row ke ==>", row)
        bsAlert.alert({
            title: "Apakah Anda yakin akan menghapus 1 pekerjaan ?",
            text: "",
            type: "question",
            showCancelButton: true
        },
            function () {
                TaskListGRFactory.DeleteDataGR(row.entity.TaskId)
                    .then(
                        function (res) {
                            bsAlert.success("Data berhasil dihapus");
                            $scope.TaskListGRGrid_Paging();
                            $scope.gridPartsFront.data = [];
                        }
                    );
            })

    }

    $scope.MainTaskListGR_Edit = function (row) {
        $scope.TaskListGRMain_Show = false;
        $scope.TaskListGREdit_Show = true;
        $scope.TaskListGREdit_TaskId = row.entity.TaskId;
        console.log('$scope.TaskListGREdit_TaskId', $scope.TaskListGREdit_TaskId);
        console.log('$scope.TaskListGREdit_TaskId2', row.entity);
        $scope.TaskListGREdit_KatashikiCode = row.entity.KatashikiCode;
        $scope.TaskListGREdit_WMI = row.entity.WMI;
        $scope.TaskListGREdit_VDS = row.entity.VDS;
        // $scope.TaskListGREdit_FullModelCode = row.entity.FullModel;
        //$scope.TaskListGREdit_Suffix = row.entity.Suffix;
        // if(typeof row.entity.OperationNo == 'string'){
        //     $scope.TaskListGREdit_OperationNumber = parseInt(row.entity.OperationNo);
        // }else{
        $scope.TaskListGREdit_OperationNumber = row.entity.OperationNo;
        // }

        $scope.TaskListGREdit_OperationDescription = row.entity.OperationDescription;
        //$scope.TaskListGREdit_ActivityTypeCode = row.entity.ActivityTypeCode;

        _.map($scope.optionsTaskListGREdit_ActivityTypeCode, function (val) {
            console.log("model typeCode", val)
            if (row.entity.ActivityTypeCode == val.name || row.entity.ActivityTypeCodeName == val.name) {
                $scope.TaskListGREdit_ActivityTypeCode = val.value;
            }
        })

        //$scope.TaskListGREdit_FlatRate = row.entity.FlatRate;
        if (typeof row.entity.FlatRate == 'string') {
            $scope.TaskListGREdit_FlatRate = parseFloat(row.entity.FlatRate);
        } else {
            $scope.TaskListGREdit_FlatRate = row.entity.FlatRate;
        }

        //$scope.TaskListGREdit_StandardActualRate = row.entity.StandardActualRate;
        if (typeof row.entity.StandardActualRate == 'string') {
            $scope.TaskListGREdit_StandardActualRate = parseFloat(row.entity.StandardActualRate);
        } else {
            $scope.TaskListGREdit_StandardActualRate = row.entity.StandardActualRate;
        }

        _.map($scope.optionsTaskListGREdit_OperationUOM, function (val) {
            if (row.entity.OperationUOMCode == val.name) {
                $scope.TaskListGREdit_OperationUOM = val.value;
            }
        })

        _.map($scope.optionsTaskListGRMain1_ModelKendaraan, function (val) {
            console.log("model modelan", val, row.entity.ModelName)
            if (row.entity.ModelName == val.name || row.entity.ModelName1 == val.name) {
                $scope.TaskListGREdit_ModelName = val.value;
                // $scope.TaskListGREdit_FullModelCode = 
            }
        })
        TaskListGRFactory.getVehicleTypeById(row.entity.ModelId).then(function (res) {
            console.log("form onSelectRows getType=>", res.data.Result);
            var dataType = res.data.Result;
            var tempDataType = [];
            angular.forEach(res.data.Result, function (value, key) {
                // tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
                tempDataType.push({ name: value.Description, value: value.VehicleTypeId, katashiki: value.KatashikiCode, VehicleModelTypeId: value.VehicleModelId })
            });
            $scope.optionsTaskListGRMain_VehicleType = tempDataType;
            _.map(dataType, function (val) {
                console.log("model modelan", val, row.entity.KatashikiCode, val.KatashikiCode, val.VehicleTypeId)
                if (row.entity.KatashikiCode == val.KatashikiCode) {
                    $scope.TaskListGREdit_FullModelCode = val.VehicleTypeId;
                }
            })
        });
        console.log("fullmodel2", $scope.TaskListGREdit_FullModelCode)

        console.log('$scope.optionsTaskListGREdit_OperationUOM', $scope.TaskListGREdit_OperationUOM, $scope.TaskListGREdit_FullModelCode);
        console.log('$scope', $scope);
        console.log('row==>', row.entity, row.entity.ModelName);

        //EDIT HERE
        var y = $scope.optionsTaskListGRMain1_ModelKendaraan;
        console.log("y", y);
        var item = y.find(function (x) { return x.name == row.entity.ModelName; });
        console.log("y1", item);
        // $scope.TaskListGREdit_ModelName = item.name;

        // var uniqType = _.uniq(item.name, 'KatashikiCode');
        // console.log("uniqType",uniqType);

        // var typeItem = uniqType.find(function (y) {return y.KatashikiCode == row.entity.KatashikiCode});
        // console.log("typeItem",typeItem);
        // $scope.optionsTaskListGRMain_VehicleType = row.entity.ModelName;



        //$scope.TaskListGREdit_FullModelCode = 12238;
        // _.map($scope.optionsTaskListGRMain_TypeKendaraan,function(val){
        //     if (row.entity.FullModel == val.name){
        //         $scope.TaskListGREdit_FullModelCode = val.value;
        //     }
        // })

        //$scope.TaskListGREdit_Kilometer = row.entity.Kilometer;
        if (typeof row.entity.FlatRate == 'string') {
            $scope.TaskListGREdit_Kilometer = parseInt(row.entity.Kilometer);
        } else {
            $scope.TaskListGREdit_Kilometer = row.entity.Kilometer;
        }

        $scope.TaskListGREdit_TanggalAwal = row.entity.ValidFrom;
        $scope.TaskListGREdit_TanggalAkhir = row.entity.ValidTo;
        $scope.TaskListGREdit_OutletId = row.entity.OutletId;
        // $scope.TaskListGREdit_OutletCode = row.entity.OutletCode;

        TaskListGRFactory.GetDataPartsByTaskId(row.entity.TaskId).then(function (resu) {
            console.log("form onSelectRows getJobParts=>3", resu.data.Result);
            var data = resu.data.Result;
            _.map(data, function (val) {
                val.satuanName = val.Satuan;
                val.SatuanId = val.UomId;
            });
            $scope.gridPartsDetail.data = data;

        })

        //=====
        // TaskListBPFactory.getFullModelFromModel(row.entity.VehicleModelId).then(function(res){
        // 	var tempData = [];
        // 	angular.forEach(res.data.Result,function(value,key){
        // 		// tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
        // 		tempData.push({name:value.VehicleTypeName,value:value.VehicleTypeId})
        // 	});
        // 	$scope.optionsTaskListGRMain_FullModel = tempData;
        // });
        //$scope.TaskListBPEdit_FullModel = row.entity.VehicleTypeId;
        //=====

    }

    // --------------------------- get vehicletype---------------------------------
    $scope.getDataVehicleType = function (row) {
        TaskListGRFactory.getVehicleTypeById(row.VehicleModelId).then(function (res) {
            var tempDataType = [];
            angular.forEach(res.data.Result, function (value, key) {
                // tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
                tempDataType.push({ name: value.Description, value: value.VehicleTypeId, katashiki: value.KatashikiCode, VehicleModelTypeId: value.VehicleModelId })
            });
            $scope.optionsTaskListGRMain_VehicleType = tempDataType;
            console.log('vehicletypeName', $scope.optionsTaskListGRMain_VehicleType)
        });
    };

    $scope.TaskListGREdit_SetActivityTypeCode = function () {
        TaskListGRFactory.getDataCategory()
            .then(
                function (res) {

                    var eachCategory = {};
                    angular.forEach(res.data.Result, function (value, key) {
                        eachCategory = { "name": value.Name, "value": value.MasterId };
                        $scope.TaskListGR_ListCategory.push(eachCategory);
                    });
                    $scope.optionsTaskListGREdit_ActivityTypeCode = $scope.TaskListGR_ListCategory;
                    $scope.optionsTaskListGRMain_TipePekerjaan = $scope.TaskListGR_ListCategory;
                }
            );
    }

    $scope.TaskListGREdit_SetUOM = function () {
        TaskListGRFactory.getDataUOM()
            .then(
                function (res) {

                    var eachUOM = {};
                    angular.forEach(res.data.Result, function (value, key) {
                        eachUOM = { "name": value.UOMName, "value": value.UOMId };
                        $scope.listUOM.push(eachUOM);
                    });
                    $scope.optionsTaskListGREdit_OperationUOM = $scope.listUOM;
                }
            );
    }
    $scope.disVehic = true;

    $scope.selectedModel = function (item) {
        $scope.disVehic = false;
        console.log("selectedModel", item, $scope.optionsTaskListGRMain_VehicleType);
        // $scope.TaskListGREdit_KatashikiCode = item.VehicleType;
        var uniqType = _.uniq(item, 'KatashikiCode');
        console.log("uniqType", uniqType, item);
        TaskListGRFactory.getVehicleTypeById(item.value).then(function (res) {
            var tempDataType = [];
            angular.forEach(res.data.Result, function (value, key) {
                // tempData.push({name:value.VehicleModelName,value:value.VehicleModelId})
                tempDataType.push({ name: value.Description, value: value.VehicleTypeId, katashiki: value.KatashikiCode, VehicleModelTypeId: value.VehicleModelId })
            });
            $scope.optionsTaskListGRMain_VehicleType = tempDataType;
            console.log('vehicletypeName', $scope.optionsTaskListGRMain_VehicleType)

            $scope.optionsTaskListGRMain_TypeKendaraan = $scope.optionsTaskListGRMain_VehicleType;
            console.log("model kendaraan ", $scope.optionsTaskListGRMain_TypeKendaraan)
            $scope.TaskListGREdit_FullModelCode = $scope.optionsTaskListGRMain_VehicleType.name;
        });


    };

    $scope.selectedUOM = function (item) {

        console.log("selectedModel", item);
        console.log("selectedModel", $scope.TaskListGREdit_OperationUOM);
    };


    $scope.selectedType = function (item) {
        console.log("selectedType", item);
        $scope.TaskListGREdit_KatashikiCode = item.katashiki;

    };

    $scope.TaskListGREdit_SetActivityTypeCode();
    $scope.TaskListGREdit_SetUOM();

    // $scope.TaskListGRGrid_Paging();

    $scope.TaskListGRMain_Download_Clicked = function () {
        $scope.TaskListGRMain_DownloadExcelTemplate();
    }

    $scope.TaskListGRMain_DownloadExcelTemplate = function () {
        var excelData = [];
        var excelDesc = [];
        var excelWMI = [];
        var excelTypeKendaraan = [];
        var excelUom = [];
        var excelTypeCode = [];
        var finalData = [];
        var fileName = "Task_list_GR_Template";

        var ExcelSheet1 = {
            "": "Contoh Task Tanpa Default parts", "WMI": "MHK", "VDS": "M1BA1J", "Katashiki_Code": "F653RM-Gxxxx", "Operation_No": "141011K", "Operation_Description": "10,000 KM Service",
            "Activity_Type_Code": "SBE", "Flat_Rate": "1.5", "Standard_Actual_Rate": "1.5", "Operation_UOM_Code": "Hour", "Model_Name": "AGYA", "Valid_From": "'2020/01/01", "Valid_To": "'2020/12/31",
            "Kilometer": "0", "PartsCode": "0", "Quantity": ""
        };
        var ExcelSheet2 = {
            "": "Contoh Task Dengan Satu Default Parts", "WMI": "MHK", "VDS": "M1BA2J", "Katashiki_Code": "F653RM-Gxxxx", "Operation_No": "141011K", "Operation_Description": "10,000 KM Service",
            "Activity_Type_Code": "SBE", "Flat_Rate": "1.5", "Standard_Actual_Rate": "1.5", "Operation_UOM_Code": "Hour", "Model_Name": "AGYA", "Valid_From": "'2020/01/01", "Valid_To": "'2020/12/31",
            "Kilometer": "0", "PartsCode": "43512-BZ270", "Quantity": "1"
        };
        var ExcelSheet3 = {
            "": "Contoh Task dengan default parts lebih dari satu", "WMI": "MHK", "VDS": "M1BA3J", "Katashiki_Code": "F653RM-Gxxxx", "Operation_No": "141011K", "Operation_Description": "10,000 KM Service",
            "Activity_Type_Code": "SBE", "Flat_Rate": "1.5", "Standard_Actual_Rate": "1.5", "Operation_UOM_Code": "Hour", "Model_Name": "AGYA", "Valid_From": "'2020/01/01", "Valid_To": "'2020/12/31",
            "Kilometer": "0", "PartsCode": "43512-BZ270", "Quantity": "2"
        };
        var ExcelSheet4 = {
            "": "", "WMI": "MHK", "VDS": "M1BA3J", "Katashiki_Code": "F653RM-Gxxxx", "Operation_No": "141011K", "Operation_Description": "10,000 KM Service",
            "Activity_Type_Code": "SBE", "Flat_Rate": "1.5", "Standard_Actual_Rate": "1.5", "Operation_UOM_Code": "Hour", "Model_Name": "AGYA", "Valid_From": "'2020/01/01", "Valid_To": "'2020/12/31",
            "Kilometer": "0", "PartsCode": "ACCU-PROV", "Quantity": "1"
        };
        // var ExcelSheet5 = {}                        
        var ExcelSheet6 = { "": "Mulai Disini" }

        //============Sheet 1=============================
        excelData.push(ExcelSheet1)
        excelData.push(ExcelSheet2)
        excelData.push(ExcelSheet3)
        excelData.push(ExcelSheet4)
        // excelData.push(ExcelSheet5)
        excelData.push(ExcelSheet6)

        //=============Sheet 2===========================
        // var ExcelDesc1 = {"No": "1","Item": "OutletCode","Deskripsi Item": "Kode outlet pada setiap Dealer di DMS","Contoh": "280","Keterangan": "Mandatory item"};
        var ExcelDesc2 = { "No": "2", "Item": "WMI", "Deskripsi Item": "3 Digit awal pada No Rangka", "Contoh": "MHK", "Keterangan": "Mandatory item" };
        var ExcelDesc3 = { "No": "3", "Item": "VDS", "Deskripsi Item": "6 Digit setelah WMI pada No. Rangka", "Contoh": "M1BA3J", "Keterangan": "Mandatory item" };
        var ExcelDesc4 = { "No": "4", "Item": "Katashiki_Code", "Deskripsi Item": "Model kendaraan", "Contoh": "F653RM-GMMFJJ", "Keterangan": "Mandatory item" };
        var ExcelDesc5 = { "No": "5", "Item": "Operation_No", "Deskripsi Item": "Kode urutan penomoran Tasklist", "Contoh": "141011K", "Keterangan": "Boleh isi Free text" };
        var ExcelDesc6 = { "No": "6", "Item": "Operation_Description", "Deskripsi Item": "Deskripsi pekerjaan yang akan dibuatkan Tasklist", "Contoh": "10,000 KM Service", "Keterangan": "Boleh isi Free text" };
        var ExcelDesc7 = { "No": "7", "Item": "Activity_Type_Code", "Deskripsi Item": "Tipe Job yang akan dipilih saat pilih Tasklist pada WO", "Contoh": "SBE", "Keterangan": "Pilihan ada di sheet sebelah" };
        var ExcelDesc8 = { "No": "8", "Item": "Flat_Rate", "Deskripsi Item": "Harga per jam sesuai Model yang akan dibebankan ke pelanggan", "Contoh": "1.5", "Keterangan": "Jika desimal maka dengan titik ( . )" };
        var ExcelDesc9 = { "No": "9", "Item": "Standard_Actual_Rate", "Deskripsi Item": "Aktual jam kerja yang berpengaruh ke panjang Chip JPCB", "Contoh": "1.5", "Keterangan": "Jika desimal maka dengan titik ( . )" };
        var ExcelDesc10 = { "No": "10", "Item": "Operation_UOM_Code", "Deskripsi Item": "Satuan yang dipilih atas Flat Rate", "Contoh": "Hour", "Keterangan": "Pilihan ada di sheet sebelah" };
        var ExcelDesc11 = { "No": "11", "Item": "Model_Name", "Deskripsi Item": "Pilihan nama model kendaraan", "Contoh": "AVANZA", "Keterangan": "Pilihan ada di sheet sebelah" };
        var ExcelDesc12 = { "No": "12", "Item": "Valid_From", "Deskripsi Item": "Batas validasi mulai masa berlakunya Tasklist", "Contoh": "2020/01/01", "Keterangan": "Formatnya tahun/bulan/tanggal" };
        var ExcelDesc13 = { "No": "13", "Item": "Valid_To", "Deskripsi Item": "Batas validasi akhir masa berlakunya Tasklist", "Contoh": "2020/12/31", "Keterangan": "Formatnya tahun/bulan/tanggal" };
        var ExcelDesc14 = { "No": "14", "Item": "Kilometer", "Deskripsi Item": "Untuk Job tipe SBI & SBE yang sejenis saja", "Contoh": "10000", "Keterangan": "Selain SBE/SBI sejenisnya boleh diisi 0" };
        var ExcelDesc15 = { "No": "15", "Item": "PartsCode", "Deskripsi Item": "Nomor parts yang terdapat pada Tasklist", "Contoh": "08880-83575", "Keterangan": "Format Part No dan boleh diisi No. bahan juga" };
        var ExcelDesc16 = { "No": "16", "Item": "Quantity", "Deskripsi Item": "Quantity parts yang di input pada Tasklist", "Contoh": "4", "Keterangan": "Format angka" };

        // excelDesc.push(ExcelDesc1)
        excelDesc.push(ExcelDesc2)
        excelDesc.push(ExcelDesc3)
        excelDesc.push(ExcelDesc4)
        excelDesc.push(ExcelDesc5)
        excelDesc.push(ExcelDesc6)
        excelDesc.push(ExcelDesc7)
        excelDesc.push(ExcelDesc8)
        excelDesc.push(ExcelDesc9)
        excelDesc.push(ExcelDesc10)
        excelDesc.push(ExcelDesc11)
        excelDesc.push(ExcelDesc12)
        excelDesc.push(ExcelDesc13)
        excelDesc.push(ExcelDesc14)
        excelDesc.push(ExcelDesc15)
        excelDesc.push(ExcelDesc16)

        //=================Sheet 3===================
        var ExcelWMI1 = { "WMI": "VDS", "3 Digit awal No Rangka": "6 Digit Setelah nya" }
        // var ExcelWMI2 = {}
        var ExcelWMI3 = { "WMI": "Contoh No. Rangka", "3 Digit awal No Rangka": "MHKA4DB3Jxxxxxxx" }
        // var ExcelWMI4 = {}
        var ExcelWMI5 = { "WMI": "WMI", "3 Digit awal No Rangka": "MHK" }
        var ExcelWMI6 = { "WMI": "VDS", "3 Digit awal No Rangka": "A4DB3J" }

        excelWMI.push(ExcelWMI1)
        // excelWMI.push(ExcelWMI2)
        excelWMI.push(ExcelWMI3)
        // excelWMI.push(ExcelWMI4)
        excelWMI.push(ExcelWMI5)
        excelWMI.push(ExcelWMI6)

        //list model 

        var excelType = { "VehicleModelName": "", 'KatashikiCode': "" };
        var tmpDataModelId = [];
        var tmpArrayTypeCar = [];

        excelTypeKendaraan.push(excelType);

        finalData.push(excelData)
        finalData.push(excelDesc)
        finalData.push(excelWMI)
        finalData.push(excelTypeCode)
        finalData.push(excelUom)

        var excelActivity = { "Activity Type Code": "" };
        excelTypeCode.push(excelActivity);
        for (var n = 0; n < $scope.optionsTaskListGREdit_ActivityTypeCode.length; n++) {
            console.log("Activity_Type_Code ", $scope.optionsTaskListGREdit_ActivityTypeCode[n].name)

            var excelActivity1 = { "Activity Type Code": $scope.optionsTaskListGREdit_ActivityTypeCode[n].name };
            excelTypeCode.push(excelActivity1);
        }

        var excelSatuan = { "UOM": "" };
        excelUom.push(excelSatuan);
        for (var n = 0; n < $scope.optionsTaskListGREdit_OperationUOM.length; n++) {
            console.log("uom ", $scope.optionsTaskListGREdit_OperationUOM[n].name)

            var excelsatuan1 = { "UOM": $scope.optionsTaskListGREdit_OperationUOM[n].name };
            excelUom.push(excelsatuan1);
        }
        for (var i = 0; i < $scope.optionsTaskListGRMain1_ModelKendaraan.length; i++) {
            console.log("cek model kendaraan ", $scope.optionsTaskListGRMain1_ModelKendaraan[i])
            tmpDataModelId.push({
                nama: $scope.optionsTaskListGRMain1_ModelKendaraan[i].name,
                Id: $scope.optionsTaskListGRMain1_ModelKendaraan[i].value,
            });
        }
        var findFullModel = function (data, x) {
            if (x > (tmpDataModelId.length - 1)) {
                return
            }
            TaskListGRFactory.getFullModelFromModel(data[x].Id).then(function (res) {
                tmpArrayTypeCar[data[x].nama] = [];
                var jsonData = [];
                angular.forEach(res.data.Result, function (value, key) {
                    console.log('tmpDataModelId[x] ====>', data[x]);
                    if (value.VehicleTypeName != null) {
                        jsonData.push({ name: value.VehicleTypeName.split(' ')[0], value: value.VehicleTypeId })
                    } else {
                        jsonData.push({ name: "-", value: value.VehicleTypeId })
                    }

                });
                tmpArrayTypeCar[data[x].nama] = jsonData;
                // if(count == (data.length )){
                // 	$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
                // }

                if (x < (tmpDataModelId.length - 1)) {
                    findFullModel(data, x + 1);

                } else if ((tmpDataModelId.length - 1) == x) {
                    $scope.changeFormatJSONDownload(excelTypeKendaraan, tmpArrayTypeCar, finalData);
                }
            });
        }
        findFullModel(tmpDataModelId, 0)
        console.log("final data", finalData);
        // XLSXInterface.writeToXLSX(fv, fileName);
    }

    $scope.changeFormatJSONDownload = function (data, dataVehicle, allData) {
        var finalCarTypeModel = [];
        var fileName = "Task_list_GR_Template";
        var objKeys = Object.keys(dataVehicle);
        for (var z = 0; z < objKeys.length; z++) {
            // for(var zz = 0; zz < dataVehicle[objKeys[z]].length; zz++ ){
            finalCarTypeModel.push({
                VehicleModelName: objKeys[z],
                // KatashikiCode:dataVehicle[objKeys[z]][zz].name
            })

            // }
        }
        allData.push(finalCarTypeModel);
        console.log('===========>>', finalCarTypeModel, allData);
        XLSXInterface.writeToXLSX(allData, fileName, [fileName, "Description", "WMI", "Activity Type Code", "Operation Uom Code", "List Model"]);
    }

    $scope.TaskListGRMain_Upload_Clicked = function () {
        angular.element(document.querySelector('#UploadTaskListGR')).val(null);
        angular.element('#ModalUploadTaskListGR').modal('show');
        $scope.ExcelData = [];
    }
    // UPLOAD EXCEL
    $scope.uploadExcel = function () {
        console.log('Uploaded excel', $scope.ExcelData);
        if ($scope.ExcelData.length > 0) {
            console.log("data", $scope.ExcelData.length)
            $scope.UploadTaskListGR_Upload_Clicked();
        } else {
            bsAlert.warning("Pililh file terlebih dahulu");
        }


    }
    //

    $scope.loadXLS = function (ExcelFile) {

        var ExcelItem = [];
        var exTmp = [];
        var myEl = angular.element(document.querySelector('#UploadTaskListGR')); //ambil elemen dari dokumen yang di-upload 

        if (myEl[0].files[0] == 0 || myEl[0].files[0] == undefined) {
            return false;
        }

        XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
            ExcelData = json;
            ExcelItem = json.Task_list_GR_Template[4];
            exTmp = json.Task_list_GR_Template;
            $scope.ExcelData = $scope.ExcelData.slice(4);
            console.log("data", $scope.ExcelData)
            for (var y = 0; y < exTmp.length; y++) {
                var formatKey = Object.keys(exTmp[y]);
                var tmpReformatKey = angular.copy(formatKey);
                console.log("formatkey", formatKey)
                if (exTmp[y].Katashiki_Code != null && exTmp[y].Katashiki_Code != "" && exTmp[y].Katashiki_Code != undefined) {
                    console.log("extmp", exTmp[y], json);
                    $scope.ExcelData.push(exTmp[y]);

                }

            }
            console.log("ExcelData : ", $scope.ExcelData);
            console.log("ExcelFile : ", ExcelFile);
            console.log("ExcelData : ", ExcelItem);
            console.log("Extmp : ", exTmp);
            console.log("myEl : ", myEl);
            console.log("json : ", json);
            // $scope.ExcelData = json;
        });
    }

    //---- ChangeFormatJsonUpload ----
    $scope.changeFormatJSON = function (data, dataVehicle) {
        console.log('inputData ke ', dataVehicle, dataVehicle.length);
        var inputData = [];
        var gridDataUpload = [];
        var eachData = {};
        var gridObject = {};
        var dataParts = [];
        var countStatusVehicle = 0;
        var countKtashiki = 0;
        // ========= OPTION ERROR =====
        var ObjectError = {};
        var keyBaruInt = ['Flat_Rate', 'Standard_Actual_Rate', 'Kilometer'];
        // var keyTanggal = ['Valid_From', 'Valid_To'];
        var numbers = /[+-]?([0-9]*[.])?[0-9]+/;
        console.log("data Excel ===>", $scope.ExcelData);
        angular.forEach($scope.ExcelData, function (value, key) {
            for (var i in $scope.optionsTaskListGREdit_ActivityTypeCode) {
                if (value.Activity_Type_Code.toLowerCase() == $scope.optionsTaskListGREdit_ActivityTypeCode[i].name.toLowerCase()) {
                    value.Activity_TypeCode = $scope.optionsTaskListGREdit_ActivityTypeCode[i].value;
                    value.Activity_TypeCodeName = $scope.optionsTaskListGREdit_ActivityTypeCode[i].name;
                    break;
                }
            }

            // upload model id
            for (var i in $scope.optionsTaskListGRMain1_ModelKendaraan) {
                value.Model_Name = value.Model_Name.trim()
                if (value.Model_Name.toLowerCase() == $scope.optionsTaskListGRMain1_ModelKendaraan[i].name.toLowerCase()) {
                    value.ModelName1 = $scope.optionsTaskListGRMain1_ModelKendaraan[i].value;
                    value.ModelName = $scope.optionsTaskListGRMain1_ModelKendaraan[i].name;
                    console.log("cek vehicle model", value.Model_Name.toLowerCase(), value.ModelName1, value.ModelName)
                    break;
                }
            }
            // upload vehicle / katashikicode
            console.log("modelname", dataVehicle[value.ModelName1], value.Katashiki_Code)
            for (var k in dataVehicle[value.ModelName1]) {
                console.log('======>>', value.Katashiki_Code, '==>', dataVehicle[value.ModelName1][k].katashiki)
                if (value.Katashiki_Code != " " && value.Katashiki_Code != "NULL" && value.Katashiki_Code != 0 && value.Katashiki_Code != undefined && value.Katashiki_Code != "") {
                    if (value.Katashiki_Code == dataVehicle[value.ModelName1][k].katashiki) {
                        value.VehicleTypeId = dataVehicle[value.ModelName1][k].value;
                        value.FullModel = dataVehicle[value.ModelName1][k].name;
                        value.KatashikiCode = dataVehicle[value.ModelName1][k].katashiki;
                        console.log("katashikicode", dataVehicle[value.ModelName1][k].katashiki)
                        break;
                    }
                }
            }

            if (value.Kilometer != " " && value.Kilometer != "NULL" && value.Kilometer != 0 && value.Kilometer != undefined && value.Kilometer != "") {
                value.Kilometer = value.Kilometer
                console.log("KM", value.Kilometer_kendaraan, value.Kilometer)
            } else if (value.Kilometer == " " || value.Kilometer == "NULL" || value.Kilometer == 0 || value.Kilometer == undefined || value.Kilometer != "") {
                value.Kilometer = 0
                console.log("KM1", value.Kilometer_kendaraan, value.Kilometer)
            }

            if (value.Flat_Rate != " " && value.Flat_Rate != "NULL" && value.Flat_Rate != 0 && value.Flat_Rate != undefined && value.Flat_Rate != "") {
                value.Flat_Rate = value.Flat_Rate
                console.log("FR", value.FlateRate, value.Flat_Rate)
            } else if (value.Flat_Rate == " " || value.Flat_Rate == "NULL" || value.Flat_Rate == 0 || value.Flat_Rate == undefined || value.Flat_Rate != "") {
                value.Flat_Rate = 0
                console.log("FR1", value.FlateRate, value.Flat_Rate)
            }

            if (value.Standard_Actual_Rate != " " && value.Standard_Actual_Rate != "NULL" && value.Standard_Actual_Rate != 0 && value.Standard_Actual_Rate != undefined && value.Standard_Actual_Rate != "") {
                value.Standard_Actual_Rate = value.Standard_Actual_Rate
                console.log("SAR", value.Standard_ActualRate, value.Standard_Actual_Rate)
            } else if (value.Standard_Actual_Rate == " " || value.Standard_Actual_Rate == "NULL" || value.Standard_Actual_Rate == 0 || value.Standard_Actual_Rate == undefined || value.Standard_Actual_Rate != "") {
                value.Standard_Actual_Rate = 0
                console.log("SAR1", value.Standard_ActualRate, value.Standard_Actual_Rate)
            }

            if (value.PartsCode != " " && value.PartsCode != "NULL" && value.PartsCode != 0 && value.PartsCode != undefined && value.PartsCode != "") {
                value.PartsCode = value.PartsCode
                console.log("parts", value.PartsCode)
            } else if (value.PartsCode == " " || value.PartsCode == "NULL" || value.PartsCode == 0 || value.PartsCode == undefined || value.PartsCode != "") {
                value.PartsCode = "null"
                console.log("parts1", value.PartsCode)
            }
            if (value.Quantity != " " && value.Quantity != "NULL" && value.Quantity != 0 && value.Quantity != undefined && value.Quantity != "") {
                value.Quantity = value.Quantity
                console.log("qty", value.Quantity)
            } else if (value.Quantity == " " || value.Quantity == "NULL" || value.Quantity == 0 || value.Quantity == undefined || value.Quantity != "") {
                value.Quantity = 0
                console.log("qty1", value.Quantity)
            }


            // ============== New Validation ===========
            for (var loopIdx in keyBaruInt) {
                console.log("number", loopIdx, keyBaruInt[loopIdx])
                if (value[keyBaruInt[loopIdx]] == null || value[keyBaruInt[loopIdx]] == undefined) {
                    ObjectError[keyBaruInt[loopIdx]] = 1;
                }
            }

            var objParts = {};
            TaskListGRFactory.getDataParts(value.PartsCode, 1, 1).then(function (resparts) {
                // for (var a = 0; a < resparts.data.Result.length; a++) {

                //     dataParts.push(resparts.data.Result[a]);
                // }

                // TaskListGRFactory.getUnitMeasurement().then(function (res) {

                // });

                // $scope.unitData2 = res.data.Result;
                // console.log("$scope.unitData2", $scope.unitData2);
                if (resparts.data.Result.length > 0) {
                    objParts = resparts.data.Result[0];
                    objParts.Qty = value.Quantity;

                    var filtered = $scope.unitData.filter(function (item) {
                        return item.MasterId == objParts.UomId;
                    });
                    console.log("filtered", filtered[0]);
                    objParts.satuanName = filtered[0].Name;
                    dataParts.push(objParts);
                    console.log("dataParts", dataParts);
                    // $scope.gridPartsDetail.data = dataParts;
                    $scope.gridPartsFront.data = dataParts;
                }
            });

            // $scope.onSelectParts(value.PartsCode,value.PartsCode,value.PartsCode);

            var ValidFrom = new Date(value.Valid_From);
            var ValidTo = new Date(value.Valid_To);

            if (isNaN(ValidFrom) || isNaN(ValidTo)) {
                isValid = 0;
                message = "Format tanggal harus yyyy-MM-dd.";
            }
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;
            eachData = {
                //inputData = {
                "OutletId": user.OutletId,
                // "OutletCode": parseFloat(value.OutletCode),
                "WMI": value.WMI,
                "VDS": value.VDS,
                "KatashikiCode": value.KatashikiCode,
                //"Suffix" : value.Suffix,
                "OperationNo": value.Operation_No,
                "OperationDescription": value.Operation_Description,
                "ActivityTypeCode": value.Activity_TypeCode,
                "ActivityTypeCodeName": value.Activity_TypeCodeName,
                "FlatRate": parseFloat(value.Flat_Rate),
                "StandardActualRate": parseFloat(value.Standard_Actual_Rate),
                "OperationUOMCode": value.Operation_UOM_Code,
                "ModelId": value.ModelName1,
                "ModelName": value.ModelName,
                "vehicleType": value.vehicleTypeid,
                "FullModel": value.FullModel,
                "ValidFrom": $filter('date')(new Date(value.Valid_From), "yyyy-MM-dd"),
                "ValidTo": $filter('date')(new Date(value.Valid_To), "yyyy-MM-dd"),
                "Kilometer": parseFloat(value.Kilometer),
                "PartsCode": value.PartsCode,
                "Quantity": value.Quantity,
                "UploadDate": today,
                // "PreparedBy":value.PreparedBy,
                // "PreparedDate":$filter('date')(new Date(value.PreparedDate), "yyyy-MM-dd")

            };

            gridObject = {
                //inputData = {
                "OutletId": user.OutletId,
                // "OutletCode": parseFloat(value.OutletCode),
                "WMI": value.WMI,
                "VDS": value.VDS,
                "KatashikiCode": value.KatashikiCode,
                //"Suffix" : value.Suffix,
                "OperationNo": value.Operation_No,
                "OperationDescription": value.Operation_Description,
                "ActivityTypeCode": value.Activity_TypeCode,
                "ActivityTypeCodeName": value.Activity_TypeCodeName,
                "FlatRate": parseFloat(value.Flat_Rate),
                "StandardActualRate": parseFloat(value.Standard_Actual_Rate),
                "OperationUOMCode": value.Operation_UOM_Code,
                "ModelId": value.ModelName1,
                "ModelName": value.ModelName,
                "vehicleType": value.vehicleTypeid,
                "FullModel": value.FullModel,
                "ValidFrom": $filter('date')(new Date(value.Valid_From), "yyyy-MM-dd"),
                "ValidTo": $filter('date')(new Date(value.Valid_To), "yyyy-MM-dd"),
                "Kilometer": parseFloat(value.Kilometer),
                // "PartsCode":value.PartsCode,
                // "Quantity":value.Quantity,
                "UploadDate": today,
                // "PreparedBy":value.PreparedBy,
                // "PreparedDate":$filter('date')(new Date(value.PreparedDate), "yyyy-MM-dd")

            };

            inputData.push(eachData);
            gridDataUpload.push(gridObject);


        }, inputData);
        //Kodingan Lama
        // $scope.TaskListGRGrid.data = inputData;
        //==========================
        for (var x in gridDataUpload) {
            console.log("jumlah string opno == >", gridDataUpload, gridDataUpload[x].OperationNo.length)
            if (gridDataUpload[x].OperationNo.length > 16) {
                bsAlert.warning("Operation Number Lebih dari 16")
                return false;
            }
        }


        for (var x = 0; x < inputData.length; x++) {
            console.log('katashiki mana aja', inputData[x].KatashikiCode)
            if (inputData[x].KatashikiCode == null || inputData[x].KatashikiCode == undefined) {
                console.log('katashiki mana', inputData[x].KatashikiCode)
                countKtashiki++;
            }
        }

        if (countKtashiki > 0) {
            bsAlert.warning("KatashikiCode Anda Salah");
            ngDialog.closeAll();
            return false;
        }

        //Kodingan Baru
        var lookup = {};
        var newData = [];
        for (var item, idx = 0; item = gridDataUpload[idx++];) {
            var name = item.KatashikiCode;
            console.log("data", gridDataUpload[idx])
            if (!(name in lookup)) {
                lookup[name] = 1;
                newData.push(item);
                console.log("data x", gridDataUpload[idx], lookup[name], lookup, name)
            }

        }

        $scope.TaskListGRGrid.data = newData;
        console.log("newData", newData);
        console.log("gridDataUpload", gridDataUpload);
        console.log(" $scope.TaskListGRGrid.data >>>", $scope.TaskListGRGrid.data);
        //=============================
        console.log("data excel", $scope.TaskListGRGrid.data, inputData, eachData)
        console.log("inputData", inputData)
        angular.element('#ModalUploadTaskListGR').modal('hide');
        // bsAlert.alert({
        //     title: "Upload Berhasil",
        //     text: "Data berhasil di upload",
        //     type: "success",
        //     showCancelButton: false
        // });
        // ======= NEW VALIDATION =====
        var countError = 0;
        for (var loopIndex in keyBaruInt) {
            if (ObjectError[keyBaruInt[loopIndex]] == 1) {
                countError++;
                var msg = angular.copy(keyBaruInt[loopIndex]);
                msg = msg.replace(/_/g, ' ');
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Format Data ada yang salah!",
                    content: '<b>' + msg + '</b>' + ' harus diisi dengan angka!!'
                });
            }
        }
        if (countError > 0) {
            ngDialog.closeAll();
            return false;
        }


        console.log("data input sbelum masuk factory :", inputData);
        TaskListGRFactory.UploadData(inputData)
            .then(
                function (res) {
                    bsAlert.success("Data berhasil disimpan");
                    $scope.optionsTaskListGRMain_OperationDescription = [];
                    $scope.TaskListGRMain_ModelKendaraan = null;
                    $scope.TaskListGRMain_OperationDescription = null;
                    //     var tmpData = angular.copy($scope.gridPartsDetail.data);
                    //     for(var i in tmpData){
                    //         tmpData[i].TaskId = res.data.Result[0].TaskId;
                    //         tmpData[i].UomId = tmpData[i].SatuanId;
                    //         tmpData[i].StatusCode = 1;
                    //     }
                    //     TaskListGRFactory.CreateParts(tmpData).then(function(res){
                    //         // alert("Data berhasil disimpan");
                    //         angular.element('#ModalUploadTaskListGR').modal('hide');
                    //         angular.element(document.querySelector('#UploadTaskListGR')).val(null);
                    //         $scope.getDataCombobox();
                    //     })
                },
                function (err) {
                    bsAlert.warning("Format Data Ada yang Salah");
                    ngDialog.closeAll();
                    return false;
                }
            );
        $scope.ExcelData = [];

    }
    //--------------------------------

    $scope.UploadTaskListGR_Upload_Clicked = function () {
        //var inputData = {};
        //--- New Upload ---
        var eachData = {};
        var isValid = 1;
        var tmpExcelData = [];
        var tmpExcelSatuan = [];
        var tmpDataModelId = [];
        var tmpDataActivity_code = [];
        var tmpDataUOM = [];
        var tmpVehicleType = {};
        var count = 0;
        var tmpResult = [];
        var tmpResult1 = [];
        var tmpexcelarray = [];
        $scope.ExcelData = $scope.ExcelData.slice(4);
        console.log("excel Data ==>", $scope.ExcelData);
        tmpExcelData = angular.copy($scope.ExcelData);
        tmpExcelSatuan = angular.copy($scope.ExcelData);
        // ================ NEW CODE =====
        var lengthTmp = angular.copy($scope.ExcelData.length);
        // /\r?\n/
        for (var i = 0; i < lengthTmp; i++) {
            if ($scope.ExcelData[i].PartsCode.includes(',')) {
                $scope.ExcelData[i].idx = i;
                console.log("==== > koma", $scope.ExcelData[i].PartsCode)
                var tmpRows = $scope.ExcelData[i];
                var tmpParts = [];
                tmpParts = $scope.ExcelData[i].PartsCode.split(',');
                console.log('tmpParts ===>', tmpParts, tmpRows);
                for (var z = 0; z < tmpParts.length; z++) {
                    var dataRows = {};
                    dataRows = angular.copy(tmpRows);
                    delete dataRows.idx;
                    dataRows.PartsCode = angular.copy(tmpParts[z]);
                    console.log('tmpRows[z] ===>', dataRows.PartsCode);
                    $scope.ExcelData.push(dataRows);
                }
                // $scope.ExcelData.splice($scope.ExcelData[i].idx);
            } else if ($scope.ExcelData[i].PartsCode.includes('.')) {
                $scope.ExcelData[i].idx = i;
                console.log("==== > titik", $scope.ExcelData[i].PartsCode)
                var tmpRows = $scope.ExcelData[i]
                var tmpParts = [];
                tmpParts = $scope.ExcelData[i].PartsCode.split('.');
                console.log('tmpParts ===>', tmpParts, tmpRows);
                for (var z = 0; z < tmpParts.length; z++) {
                    var dataRows = {};
                    dataRows = angular.copy(tmpRows);
                    delete dataRows.idx;
                    dataRows.PartsCode = angular.copy(tmpParts[z]);
                    console.log('tmpRows[z] ===>', dataRows.PartsCode);
                    $scope.ExcelData.push(dataRows);
                }
                // console.log('babi ===>',_.findIndex($scope.ExcelData, { "idx":i }));
                // $scope.ExcelData.splice(_.findIndex($scope.ExcelData, { "idx":i }), 1);
            } else if ($scope.ExcelData[i].PartsCode.match("[\\n\\r]+")) {
                $scope.ExcelData[i].idx = i;
                console.log("==== > titik", $scope.ExcelData[i].PartsCode)
                var tmpRows = $scope.ExcelData[i]
                var tmpParts = [];
                tmpParts = $scope.ExcelData[i].PartsCode.split(/\r?\n/);
                console.log('tmpParts ===>', tmpParts, tmpRows);
                for (var z = 0; z < tmpParts.length; z++) {
                    var dataRows = {};
                    dataRows = angular.copy(tmpRows);
                    delete dataRows.idx;
                    dataRows.PartsCode = angular.copy(tmpParts[z]);
                    console.log('tmpRows[z] ===>', dataRows.PartsCode);
                    $scope.ExcelData.push(dataRows);
                }
                // console.log('babi ===>',_.findIndex($scope.ExcelData, { "idx":i }));
                // $scope.ExcelData.splice(_.findIndex($scope.ExcelData, { "idx":i }), 1);

            }


            console.log('parts ===>', _.findIndex($scope.ExcelData, { "idx": i }));
        }
        for (var z = 0; z < $scope.ExcelData.length; z++) {
            if ($scope.ExcelData[z].idx == undefined) {
                tmpexcelarray.push($scope.ExcelData[z])
                console.log("====>> tmp", tmpexcelarray)
            }
        }

        $scope.ExcelData = [];
        $scope.ExcelData = tmpexcelarray;
        console.log("ngawur=======> ", $scope.ExcelData)
        // ===============================
        // tmpExcelData = _.uniq(tmpExcelData, 'Model_Kendaraan');
        var lookup = {};
        for (var item, idx = 0; item = tmpExcelData[idx++];) {
            var name = item.Model_Name;
            if (!(name in lookup)) {
                lookup[name] = 1;
                tmpResult.push(name.trim());
            }

        }
        tmpExcelData = tmpResult;
        console.log('=====>> modelid', tmpResult, tmpExcelData);
        if (tmpExcelData != undefined && tmpExcelData.length > 0) {
            for (var xx in tmpExcelData) {
                var itungModel1 = 1;
                tmpExcelData[xx] = (tmpExcelData[xx]).toLowerCase();
                for (var x in $scope.optionsTaskListGRMain1_ModelKendaraan) {
                    if (tmpExcelData[xx] == $scope.optionsTaskListGRMain1_ModelKendaraan[x].name.toLowerCase()) {
                        console.log("nama model ", tmpExcelData[xx], $scope.optionsTaskListGRMain1_ModelKendaraan[x].name.toLowerCase())
                        tmpDataModelId.push($scope.optionsTaskListGRMain1_ModelKendaraan[x].value);
                        console.log("model id", tmpExcelData[xx], $scope.optionsTaskListGRMain1_ModelKendaraan[x].name.toLowerCase(), tmpDataModelId)
                        break;
                    } else {
                        itungModel1++
                    }
                    if (itungModel1 == $scope.optionsTaskListGRMain1_ModelKendaraan.length) {
                        console.log("itungan model", itungModel, $scope.optionsTaskListGRMain1_ModelKendaraan.length)
                        bsAlert.warning("Format Data Ada yang Salah", "Model Tidak Ditemukan")
                        ngDialog.closeAll();
                        return false;
                    }

                }
            }
        } else {
            bsAlert.warning("Ada Data yang Masih Kosong")
            ngDialog.closeAll();
            return false;
        }
        //----------Satuan------------
        var lookup1 = {};
        for (var item, idx = 0; item = tmpExcelSatuan[idx++];) {
            var name1 = item.Activity_Type_Code;
            if (!(name1 in lookup1)) {
                lookup1[name1] = 1;
                tmpResult1.push(name1);
            }

        }
        tmpExcelSatuan = tmpResult1
        console.log('=====>>', tmpResult1, tmpExcelSatuan);
        if (tmpExcelSatuan != undefined && tmpExcelSatuan.length > 0) {
            for (var xx in tmpExcelSatuan) {
                var itungModel = 1;
                tmpExcelSatuan[xx] = (tmpExcelSatuan[xx]).toLowerCase();
                for (var x in $scope.optionsTaskListGREdit_ActivityTypeCode) {
                    if (tmpExcelSatuan[xx] == $scope.optionsTaskListGREdit_ActivityTypeCode[x].name.toLowerCase()) {
                        tmpDataActivity_code.push($scope.optionsTaskListGREdit_ActivityTypeCode[x].value);
                        console.log("satuan", tmpExcelSatuan[xx], $scope.optionsTaskListGREdit_ActivityTypeCode[x].name.toLowerCase())
                        break;
                    } else {
                        itungModel++
                    }
                    if (itungModel == $scope.optionsTaskListGREdit_ActivityTypeCode.length) {
                        console.log("itungan", itungModel, $scope.optionsTaskListGREdit_ActivityTypeCode.length)
                        bsAlert.warning("Format Data Ada yang Salah", "Model Tidak Ditemukan")
                        ngDialog.closeAll();
                        return false;
                    }
                }
            }
        } else {
            bsAlert.warning("Ada Data yang Masih Kosong")
            ngDialog.closeAll();
            return false;
        }


        // tmpDataModelId.sort();
        console.log('tmpmodelid', tmpDataModelId);
        var findDataByModel = function (data, x) {
            if (x > (tmpDataModelId.length - 1)) {
                return
            }
            if (data[x] == undefined) {
                bsAlert.warning("Model Kendaraan Tidak Ditemukan")
                ngDialog.closeAll();
                return false;
            }
            console.log("vehiclemodelid", data[x])
            TaskListGRFactory.getVehicleTypeById(data[x]).then(function (res) {
                tmpVehicleType[data[x]] = [];
                var jsonData = [];
                angular.forEach(res.data.Result, function (value, key) {
                    console.log('tmpDataModelId[xxx] ====>', data[x]);
                    jsonData.push({ name: value.Description, value: value.VehicleTypeId, katashiki: value.KatashikiCode })
                });
                console.log("json type by model id", jsonData)
                tmpVehicleType[data[x]] = jsonData;
                // if(count == (data.length )){
                // 	$scope.changeFormatJSON($scope.ExcelData, tmpVehicleType);
                // }
                if ($scope.ariavalue < 80) {
                    $scope.ariavalue += 2 + x;
                    var valueLoading = angular.copy($scope.ariavalue);
                    $scope.ariavaluecss = (valueLoading).toString() + '%';
                } else {
                    // $scope.ariavaluecss = ($scope.ariavalue).toString() + '%';
                }
                if (x < (tmpDataModelId.length - 1)) {
                    findDataByModel(data, x + 1);

                } else if ((tmpDataModelId.length - 1) == x) {
                    $scope.changeFormatJSON($scope.ExcelData, tmpVehicleType, tmpexcelarray);
                }
            });
        }
        findDataByModel(tmpDataModelId, 0)

    }

    var dateFormat = 'dd/MM/yyyy';
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat
        //disableWeekend: 1
    };

    $scope.changeFormatDate = function (item) {
        var tmpAppointmentDate = item;
        console.log("changeFormatDate item", item);
        tmpAppointmentDate = new Date(tmpAppointmentDate);
        var finalDate
        var yyyy = tmpAppointmentDate.getFullYear().toString();
        var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = tmpAppointmentDate.getDate().toString();
        finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        console.log("changeFormatDate finalDate", finalDate);
        return finalDate;
    }

    $scope.TaskListGR_Edit_Simpan_Clicked = function () {
        $scope.TaskListGREdit_TanggalAwal = $scope.TaskListGREdit_TanggalAwal != null ? $scope.changeFormatDate($scope.TaskListGREdit_TanggalAwal) : $scope.TaskListGREdit_TanggalAwal;
        $scope.TaskListGREdit_TanggalAkhir = $scope.TaskListGREdit_TanggalAkhir != null ? $scope.changeFormatDate($scope.TaskListGREdit_TanggalAkhir) : $scope.TaskListGREdit_TanggalAkhir;
        $scope.TaskListGREdit_StandardActualRate = $scope.TaskListGREdit_StandardActualRate >= 0 ? $scope.TaskListGREdit_StandardActualRate.toString() : $scope.TaskListGREdit_StandardActualRate;
        console.log("TaskListGREdit_TanggalAwal finalDate", $scope.TaskListGREdit_TanggalAwal);
        console.log("TaskListGREdit_TanggalAkhir finalDate", $scope.TaskListGREdit_TanggalAkhir);
        if (($scope.TaskListGREdit_WMI == null || $scope.TaskListGREdit_WMI == undefined || $scope.TaskListGREdit_WMI == "") ||
            ($scope.TaskListGREdit_VDS == null || $scope.TaskListGREdit_VDS == undefined || $scope.TaskListGREdit_VDS == "") ||
            ($scope.TaskListGREdit_OperationNumber == null || $scope.TaskListGREdit_OperationNumber == undefined || $scope.TaskListGREdit_OperationNumber == "") ||
            ($scope.TaskListGREdit_OperationDescription == null || $scope.TaskListGREdit_OperationDescription == undefined || $scope.TaskListGREdit_OperationDescription == "") ||
            ($scope.TaskListGREdit_ActivityTypeCode == null || $scope.TaskListGREdit_ActivityTypeCode == undefined || $scope.TaskListGREdit_ActivityTypeCode == "") ||
            ($scope.TaskListGREdit_FlatRate == null || $scope.TaskListGREdit_FlatRate == undefined || $scope.TaskListGREdit_FlatRate == "") ||
            ($scope.TaskListGREdit_StandardActualRate == null || $scope.TaskListGREdit_StandardActualRate == undefined || $scope.TaskListGREdit_StandardActualRate == "" || $scope.TaskListGREdit_StandardActualRate < 0) ||
            ($scope.TaskListGREdit_OperationUOM == null || $scope.TaskListGREdit_OperationUOM == undefined || $scope.TaskListGREdit_OperationUOM == "") ||
            ($scope.TaskListGREdit_ModelName == null || $scope.TaskListGREdit_ModelName == undefined || $scope.TaskListGREdit_ModelName == "") ||
            ($scope.TaskListGREdit_FullModelCode == null || $scope.TaskListGREdit_FullModelCode == undefined || $scope.TaskListGREdit_FullModelCode == "") ||
            ($scope.TaskListGREdit_KatashikiCode == null || $scope.TaskListGREdit_KatashikiCode == undefined || $scope.TaskListGREdit_KatashikiCode == "") ||
            ($scope.TaskListGREdit_TanggalAwal == null || $scope.TaskListGREdit_TanggalAwal == undefined || $scope.TaskListGREdit_TanggalAwal == "") ||
            ($scope.TaskListGREdit_TanggalAkhir == null || $scope.TaskListGREdit_TanggalAkhir == undefined || $scope.TaskListGREdit_TanggalAkhir == "")

        ) {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Lengkapi Data Terlebih Dahulu"
            });
        } else {
            if ($scope.TaskListGREdit_Kilometer == null || $scope.TaskListGREdit_Kilometer == undefined) {
                $scope.TaskListGREdit_Kilometer = 0;
            }
            if ($scope.TaskListGREdit_FlatRate == null || $scope.TaskListGREdit_FlatRate == undefined || $scope.TaskListGREdit_FlatRate == "") {
                $scope.TaskListGREdit_FlatRate = 0;
            }
            if ($scope.TaskListGREdit_StandardActualRate == null || $scope.TaskListGREdit_StandardActualRate == undefined || $scope.TaskListGREdit_StandardActualRate == "") {
                $scope.TaskListGREdit_StandardActualRate = 0;
            }


            for (var i in $scope.optionsTaskListGREdit_ActivityTypeCode) {
                if ($scope.TaskListGREdit_ActivityTypeCode == $scope.optionsTaskListGREdit_ActivityTypeCode[i].value) {
                    $scope.TaskListGREdit_ActivityTypeCodeName = $scope.optionsTaskListGREdit_ActivityTypeCode[i].name;
                    break;
                }
            }
            for (var b in $scope.optionsTaskListGREdit_OperationUOM) {
                if ($scope.TaskListGREdit_OperationUOM == $scope.optionsTaskListGREdit_OperationUOM[b].value) {
                    $scope.TaskListGREdit_OperationUOMName = $scope.optionsTaskListGREdit_OperationUOM[b].name;
                    break;
                }
            }
            for (var c in $scope.optionsTaskListGRMain1_ModelKendaraan) {
                if ($scope.TaskListGREdit_ModelName == $scope.optionsTaskListGRMain1_ModelKendaraan[c].value) {
                    $scope.TaskListGREdit_ModelNameKendaraan = $scope.optionsTaskListGRMain1_ModelKendaraan[c].name;
                    break;
                }
            }
            for (var d in $scope.optionsTaskListGRMain_VehicleType) {
                if ($scope.TaskListGREdit_FullModelCode == $scope.optionsTaskListGRMain_VehicleType[d].value) {
                    $scope.TaskListGREdit_FullModelCodeName = $scope.optionsTaskListGRMain_VehicleType[d].name;
                    break;
                }
            }


            console.log("masuk sini :", $scope.TaskListGREdit_ActivityTypeCode);
            var inputData = [{
                "TaskId": $scope.TaskListGREdit_TaskId,
                "OutletId": $scope.TaskListGREdit_OutletId, //user.OutletId,
                // "OutletCode": $scope.TaskListGREdit_OutletCode,
                "KatashikiCode": $scope.TaskListGREdit_KatashikiCode,
                "WMI": $scope.TaskListGREdit_WMI,
                "VDS": $scope.TaskListGREdit_VDS,
                //"Suffix" : $scope.TaskListGREdit_Suffix,
                "OperationNo": $scope.TaskListGREdit_OperationNumber,
                "OperationDescription": $scope.TaskListGREdit_OperationDescription,
                "ActivityTypeId": $scope.TaskListGREdit_ActivityTypeCode,
                "ActivityTypeCode": $scope.TaskListGREdit_ActivityTypeCodeName,
                "FlatRate": parseFloat($scope.TaskListGREdit_FlatRate),
                "StandardActualRate": parseFloat($scope.TaskListGREdit_StandardActualRate),
                "OperationUOMId": $scope.TaskListGREdit_OperationUOM,
                "OperationUOMCode": $scope.TaskListGREdit_OperationUOMName,
                "ModelId": $scope.TaskListGREdit_ModelName,
                "ModelName": $scope.TaskListGREdit_ModelNameKendaraan,
                "FullModelId": $scope.TaskListGREdit_FullModelCode,
                "FullModel": $scope.TaskListGREdit_FullModelCodeName,
                "ValidFrom": $filter('date')(new Date($scope.TaskListGREdit_TanggalAwal), "yyyy-MM-dd"),
                "ValidTo": $filter('date')(new Date($scope.TaskListGREdit_TanggalAkhir), "yyyy-MM-dd"),
                "Kilometer": parseFloat($scope.TaskListGREdit_Kilometer)
            }];
            console.log("data edit & input", inputData)
            TaskListGRFactory.create(inputData)
                .then(
                    function (res) {
                        console.log("create", res.data.Result, inputData);
                        if ($scope.gridPartsDetail.data.length > 0) {
                            var tmpData = angular.copy($scope.gridPartsDetail.data);
                            console.log("data parts detail", tmpData, res.data.Result)
                            for (var i in tmpData) {
                                tmpData[i].TaskId = res.data.Result[0].TaskId;
                                tmpData[i].UomId = tmpData[i].SatuanId;
                                tmpData[i].StatusCode = 1;
                            }


                            setTimeout(function () {
                                TaskListGRFactory.CreateParts(tmpData).then(function (res) {
                                    console.log("masuk part", res)
                                    alert("Data berhasil disimpan");
                                    $scope.TaskListGRGrid_Paging();
                                    $scope.TaskListGR_Edit_Batal_Clicked();
                                })
                            }, 1000);

                        } else {
                            alert("Data berhasil disimpan");
                            $scope.TaskListGRGrid_Paging();
                            $scope.TaskListGR_Edit_Batal_Clicked();
                        }
                    },
                    function (err) {
                        alert(err.data.ExceptionMessage);
                    });
            $scope.TaskListGRGrid.data = inputData
            $scope.gridPartsFront.data = [];
            // $scope.TaskListGRMain_ModelKendaraan = "";
            // $scope.TaskListGRMain_OperationDescription = "";
            // $scope.TaskListGRMain_TipePekerjaan = "";
            // $scope.TaskListGRMain_OperationNumber = "";


        }
    }

    $scope.TaskListGR_Edit_Batal_Clicked = function () {
        $scope.TaskListGREdit_ClearFields();
        $scope.TaskListGRMain_Show = true;
        $scope.TaskListGREdit_Show = false;
    }

    $scope.TaskListGREdit_ClearFields = function () {
        $scope.TaskListGREdit_KatashikiCode = '';
        $scope.TaskListGREdit_OutletId = 0;
        // $scope.TaskListGREdit_OutletCode = '';
        //$scope.TaskListGREdit_Suffix = '';
        $scope.TaskListGREdit_WMI = '';
        $scope.TaskListGREdit_VDS = '';
        $scope.TaskListGREdit_OperationNumber = '';
        $scope.TaskListGREdit_OperationDescription = '';
        $scope.TaskListGREdit_ActivityTypeCode = '';
        $scope.TaskListGREdit_FlatRate = '';
        $scope.TaskListGREdit_StandardActualRate = '';
        $scope.TaskListGREdit_OperationUOM = '';
        $scope.TaskListGREdit_ModelName = '';
        $scope.TaskListGREdit_FullModelCode = '';
        $scope.TaskListGREdit_TanggalAwal = undefined;
        $scope.TaskListGREdit_TanggalAkhir = undefined;



    }
    $scope.mData = {}
    $scope.noResults = true;
    $scope.selected = {};
    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };
    $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
    TaskListGRFactory.getUnitMeasurement().then(function (res) {
        $scope.unitData = res.data.Result;
    });
    $scope.selectTypeUnitParts = function (data) {
        console.log("selectTypeUnitParts", data);
    };
    $scope.onSelectParts = function ($item, $model, $label) {
        console.log("onSelectParts=>", $item);
        console.log("onSelectParts=>", $model);
        console.log("onSelectParts=>", $label);
        $scope.mData.PartsName = $item.PartsName;
        $scope.mData.SatuanId = $item.UomId;
        var tmpUomName = _.find($scope.unitData, function (val) {
            if (val.MasterId == $item.UomId) {
                return val
            }
        });
        $scope.mData.satuanName = tmpUomName.Name;
        $scope.mData.PartId = $item.PartId;
        $scope.mData.Qty = 1;
    };
    $scope.onNoPartResult = function () {
        console.log("onGotResult=>");
        //$scope.mData = {};
    };
    $scope.onGotResult = function () {
        console.log("onGotResult=>");
    };
    $scope.getParts = function (key) {
        var kategori = $scope.mData.MaterialTypeId;
        var isGr = 1;
        if (kategori !== undefined) {
            var ress = TaskListGRFactory.getDataParts(key, isGr, kategori).then(function (resparts) {
                return resparts.data.Result;
            });
            console.log("ress", ress);
            return ress
        }
        // }
    };
    $scope.addParts = function (data) {
        console.log("data add to grid", data);
        var tmpData = angular.copy(data);
        var cekData = _.find($scope.gridPartsDetail.data, function (val) {
            console.log("val====", val);
            if (val.PartsCode == data.PartsCode) {
                return val;
            }
        });
        console.log("cekData", cekData);
        if (cekData == undefined) {
            $scope.gridPartsDetail.data.push(tmpData);
            $scope.mData = {};
        } else {
            bsAlert.warning("Data parts sudah ada", "isi kembali dengan data yang berbeda");
        }
    }
    $scope.gridPartsDetail = {
        enableRowSelection: false,
        columnDefs: [
            { name: "PartsCode", field: "PartsCode", displayName: "Nomor Part", width: '35%' },
            { name: "Nama Parts", field: "PartsName", width: '25%' },
            { name: "Qty", field: "Qty", width: '15%' },
            { name: "Satuan", field: "satuanName", width: '25%' },
            {
                name: 'Action',
                field: 'Action',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                    '   <button class="ui icon inverted grey button"' +
                    '           style="font-size:0.8em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
                    '           onclick="this.blur()"' +
                    '             title="Hapus" ng-click="grid.appScope.removeRow(row.entity)">' +
                    '       <i class="fa fa-fw fa-lg fa-trash"></i>' +
                    '   </button>' +
                    '</div>',
                width: 120,
                pinnedRight: true
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridAPITaskListGRGrid = gridApi;
            $scope.GridAPITaskListGRGrid.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedRows = $scope.GridAPITaskListGRGrid.selection.getSelectedRows();
                console.log("selected=>", $scope.selectedRows);
                // if ($scope.onSelectRows) {
                //     $scope.onSelectRows($scope.selectedRows);
                // }
            });
            // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //     $scope.TaskListGRGrid_Paging(pageNumber);
            // });
        }
    };
    $scope.gridPartsFront = {

        columnDefs: [
            { name: "PartsCode", field: "PartsCode", displayName: "Nomor Part", width: '25%' },
            { name: "Nama Parts", field: "PartsName", width: '25%' },
            { name: "Qty", field: "Qty", width: '15%' },
            { name: "Satuan", field: "satuanName", width: '35%' },
        ],
        onRegisterApi: function (gridApi) {
            $scope.GridAPITaskListGRGrid = gridApi;
            $scope.GridAPITaskListGRGrid.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedRows = $scope.GridAPITaskListGRGrid.selection.getSelectedRows();
                // console.log("selected=>",$scope.selectedRows);
                // if ($scope.onSelectRows) {
                //     $scope.onSelectRows($scope.selectedRows);
                // }
            });
            // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //     $scope.TaskListGRGrid_Paging(pageNumber);
            // });
        }
    };

    $scope.removeRow = function (selected) {
        var idx = _.findIndex($scope.gridPartsDetail.data, function (val) {
            if (val.TaskPartId !== '' && val.TaskPartId !== null && val.TaskPartId !== undefined) {
                if (val.TaskPartId == selected.TaskPartId) {
                    return val
                }
            } else {
                if (val.PartsCode == selected.PartsCode) {
                    return val
                }
            }
        });
        console.log("idx", idx);
        $scope.gridPartsDetail.data.splice(idx, 1);
    }
});