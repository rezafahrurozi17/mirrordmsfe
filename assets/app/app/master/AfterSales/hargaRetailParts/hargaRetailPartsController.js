var app = angular.module('app');
app.controller('HargaRetailPartsController', function($scope, $http, $filter, CurrentUser, HargaRetailPartsFactory, uiGridConstants, $timeout) {
    var user = CurrentUser.user();
    $scope.uiGridPageSize = 10;
    $scope.UploadHargaRetail_FileUploadId = 0;
    $scope.MainHargaRetailParts_Show = true;
    $scope.UploadHargaRetail_DataTemp = [];

    ///MAIN HARGA RETAIL PARTS
    $scope.MainHargaRetailParts_Tambah_Clicked = function() {
        $scope.MainHargaRetailParts_Show = false;
        $scope.TambahHargaRetailParts_Show = true;
        $scope.TambahHargaRetailParts_TanggalAwal_isDisabled = false;
        $scope.TambahHargaRetailParts_TanggalAkhir_isDisabled = false;
        $scope.TambahHargaRetail_PartsNoMaterial_isDisabled = false;
        $scope.TambahHargaRetailParts_Cari_isDisabled = false;
        $scope.TambahHargaRetail_Harga_isDisabled = false;
        $scope.TambahHargaRetailParts_Cari_Show = true;
        $scope.TambahHargaRetailParts_Simpan_Show = true;
        $scope.TambahHargaRetailParts_PartPriceRetailId = 0;

        $scope.UploadHargaRetailParts_ClearFields();
    }

    $scope.MainHargaRetailParts_Upload_Clicked = function() {
        $scope.MainHargaRetailParts_Show = false;
        $scope.UploadHargaRetailParts_Show = true;
    }

    $scope.MainHargaRetailParts_Download_Clicked = function() {
        var excelData = [];
        var fileName = "";

        excelData = [{ "No_Material": "", "Nama_Material": "", "Satuan": "", "Tanggal_Berlaku_Awal": "", "Tanggal_Berlaku_Akhir": "", "Harga_Retail": "" }];
        fileName = "HargaRetailPartsTemplate";

        XLSXInterface.writeToXLSX(excelData, fileName);
    }

    $scope.MainHargaRetailParts_Cari_Clicked = function() {
        $scope.MainHargaRetailParts_UIGrid_Paging();
    }

    $scope.MainHargaRetailParts_Hapus_Clicked = function() {
        $scope.MainHargaRetailParts_TanggalAwal = "";
        $scope.MainHargaRetailParts_TanggalAkhir = "";
        $scope.MainHargaRetail_PartsNoMaterial = "";
        $scope.MainHargaRetail_PartsNamaMaterial = "";

    }

    $scope.MainHargaRetailPartsLihat = function(row) {
        $scope.MainHargaRetailParts_Show = false;
        $scope.TambahHargaRetailParts_Show = true;
        $scope.TambahHargaRetailParts_TambahShow = false;
        $scope.TambahHargaRetailParts_TanggalAwal = row.entity.EffectiveDate;
        $scope.TambahHargaRetailParts_TanggalAkhir = row.entity.ValidThru;
        $scope.TambahHargaRetail_PartsNoMaterial = row.entity.PartNo;
        $scope.TambahHargaRetail_PartsNamaMaterial = row.entity.PartName;
        $scope.TambahHargaRetail_Harga = row.entity.RetailPrice;
        $scope.TambahHargaRetailParts_TanggalAwal_isDisabled = true;
        $scope.TambahHargaRetailParts_TanggalAkhir_isDisabled = true;
        $scope.TambahHargaRetail_PartsNoMaterial_isDisabled = true;
        $scope.TambahHargaRetail_Harga_isDisabled = true;
        $scope.TambahHargaRetailParts_Cari_Show = false;
        $scope.TambahHargaRetailParts_Simpan_Show = false;
    }

    $scope.MainHargaRetailPartsEdit = function(row) {
        $scope.MainHargaRetailParts_Show = false;
        $scope.TambahHargaRetailParts_Show = true;
        $scope.TambahHargaRetailParts_TambahShow = true;
        $scope.TambahHargaRetailParts_PartPriceRetailId = row.entity.PartPriceRetailId;
        $scope.TambahHargaRetailParts_TanggalAwal = row.entity.EffectiveDate;
        $scope.TambahHargaRetailParts_TanggalAkhir = row.entity.ValidThru;
        $scope.TambahHargaRetail_PartsNoMaterial = row.entity.PartNo;
        $scope.TambahHargaRetail_PartsNamaMaterial = row.entity.PartName;
        $scope.TambahHargaRetail_Harga = row.entity.RetailPrice;
        $scope.TambahHargaRetailParts_TanggalAwal_isDisabled = false;
        $scope.TambahHargaRetailParts_TanggalAkhir_isDisabled = false;
        $scope.TambahHargaRetail_PartsNoMaterial_isDisabled = true;
        $scope.TambahHargaRetail_Harga_isDisabled = false;
        $scope.TambahHargaRetailParts_Cari_Show = false;
        $scope.TambahHargaRetailParts_Simpan_Show = true;
    }

    ///TAMBAH HARGA RETAIL PARTS
    $scope.TambahHargaRetailParts_Kembali_Clicked = function() {
        $scope.MainHargaRetailParts_Show = true;
        $scope.TambahHargaRetailParts_Show = false;
        $scope.MainHargaRetailParts_UIGrid_Paging();
    }

    $scope.TambahHargaRetailParts_Simpan_Clicked = function() {
        var inputData = [{
            "PartPriceRetailId": $scope.TambahHargaRetailParts_PartPriceRetailId,
            "PartId": $scope.TambahHargaRetailParts_PartId,
            "OutletId": user.OutletId,
            "RetailPrice": $scope.TambahHargaRetail_Harga,
            "EffectiveDate": $filter('date')(new Date($scope.TambahHargaRetailParts_TanggalAwal.toLocaleString()), "yyyy-MM-dd"),
            "ValidThru": $filter('date')(new Date($scope.TambahHargaRetailParts_TanggalAkhir.toLocaleString()), "yyyy-MM-dd")
        }];

        HargaRetailPartsFactory.submitData(inputData)
            .then(
                function(res) {
                    alert("Data berhasil disimpan");
                    $scope.TambahHargaRetailParts_Kembali_Clicked();
                }
            );
    }

    $scope.TambahHargaRetailParts_Cari_Clicked = function() {
        $scope.ShowModalSearchParts();
    }

    ///UPLOAD HARGA RETAIL PARTS
    $scope.UploadHargaRetailParts_ClearFields = function() {
        angular.element(document.querySelector('#UploadHargaRetail')).val(null);
        $scope.UploadHargaRetailParts_UIGrid.data = [];
        $scope.UploadHargaRetail_DataTemp = [];

        $scope.TambahHargaRetailParts_TanggalAwal = null;
        $scope.TambahHargaRetailParts_TanggalAkhir = null;
        $scope.TambahHargaRetail_PartsNoMaterial = null;
        $scope.TambahHargaRetail_PartsNamaMaterial = null;
        $scope.TambahHargaRetail_Harga = null;
    }

    $scope.UploadHargaRetailParts_Kembali_Clicked = function() {
        $scope.MainHargaRetailParts_Show = true;
        $scope.UploadHargaRetailParts_Show = false;
        $scope.UploadHargaRetailParts_ClearFields();
    }

    $scope.UploadHargaRetailPartsHapus = function(row) {
        var index = $scope.UploadHargaRetail_DataTemp.indexOf(row.entity);
        $scope.UploadHargaRetail_DataTemp.splice(index, 1);
        $scope.UploadHargaRetailParts_UIGrid.data = $scope.UploadHargaRetail_DataTemp;
    }

    $scope.UploadHargaRetailParts_Simpan_Clicked = function() {
        var inputData = [];
        angular.forEach($scope.UploadHargaRetail_DataTemp, function(value, key) {
            inputData.push({
                "PartId": value.PartId,
                "FileId": $scope.UploadHargaRetail_FileUploadId,
                "OutletId": user.OutletId,
                "RetailPrice": value.RetailPrice,
                "EffectiveDate": value.EffectiveDate,
                "ValidThru": value.ValidThru
            });
        });

        HargaRetailPartsFactory.submitData(inputData)
            .then(
                function(res) {
                    alert("Data berhasil disimpan");
                    $scope.UploadHargaRetailParts_Kembali_Clicked();
                }
            );
    }

    $scope.loadXLS = function(ExcelFile) {
        var ExcelData = [];

        var myEl = angular.element(document.querySelector('#UploadHargaRetail'));

        XLSXInterface.loadToJson(myEl[0].files[0], function(json) {
            ExcelData = json[0];
            console.log("ExcelData : ", ExcelData);
            console.log("myEl : ", myEl);
            console.log("json : ", json);
            $scope.UploadHargaRetailParts_ExcelData = json;
            $scope.UploadHargaRetailParts_FileName_Current = myEl[0].files[0].name;
        });
    }

    $scope.UploadHargaRetailParts_Upload_Clicked = function() {
        var inputData = [];

        angular.forEach($scope.UploadHargaRetailParts_ExcelData, function(value, key) {

            inputData.push({
                "OutletId": user.OutletId,
                "FileName": $scope.UploadHargaRetailParts_FileName_Current,
                "PartName": value.Nama_Material,
                "PartNo": value.No_Material,
                "EffectiveDate": value.Tanggal_Berlaku_Awal.substr(6, 4) + '-' + value.Tanggal_Berlaku_Awal.substr(3, 2) + '-' + value.Tanggal_Berlaku_Awal.substr(0, 2),
                "ValidThru": value.Tanggal_Berlaku_Akhir.substr(6, 4) + '-' + value.Tanggal_Berlaku_Akhir.substr(3, 2) + '-' + value.Tanggal_Berlaku_Akhir.substr(0, 2),
                "RetailPrice": value.Harga_Retail
            });
        });

        HargaRetailPartsFactory.checkData(inputData)
            .then(
                function(res) {
                    console.log("FILE ID : ", res.data.Result);
                    alert("Data sudah dicek");
                    $scope.UploadHargaRetail_FileUploadId = res.data.Result[0].FileId;
                    $scope.UploadHargaRetailParts_UIGrid_Paging(1);
                }
            );
    }

    ///MAIN HARGA RETAIL PARTS UIGRID
    $scope.MainHargaRetailParts_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "PartPriceRetailId", field: "PartPriceRetailId", visible: false },
            { name: "Tanggal Berlaku Dari", field: "EffectiveDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
            { name: "Tanggal Berlaku Sampai", field: "ValidThru", cellFilter: 'date:\"dd-MM-yyyy\"' },
            { name: "No Material", field: "PartNo" },
            { name: "Nama Material", field: "PartName" },
            { name: "Harga Retail Parts", field: "RetailPrice" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.MainHargaRetailPartsLihat(row)">Lihat</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.MainHargaRetailPartsEdit(row)">Ubah</a>'
            }
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiMainHargaRetailParts = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
            // 	$scope.MainHargaRetailParts_UIGrid_Paging(pageNumber);
            // });
        }
    };

    $scope.MainHargaRetailParts_UIGrid_Paging = function() {
        var dateStart = '';
        var dateEnd = '';

        if (!angular.isUndefined($scope.MainHargaRetailParts_TanggalAwal)) {
            console.log("masuk tanggal awal : ", $scope.MainHargaRetailParts_TanggalAwal);
            dateStart = $filter('date')($scope.MainHargaRetailParts_TanggalAwal, "yyyy-MM-dd");
        }

        if (!angular.isUndefined($scope.MainHargaRetailParts_TanggalAkhir)) {
            dateEnd = $filter('date')($scope.MainHargaRetailParts_TanggalAkhir, "yyyy-MM-dd");
        }

        if (angular.isUndefined($scope.MainHargaRetail_PartsNoMaterial)) {
            $scope.MainHargaRetail_PartsNoMaterial = '';
        }

        if (angular.isUndefined($scope.MainHargaRetail_PartsNamaMaterial)) {
            $scope.MainHargaRetail_PartsNamaMaterial = '';
        }

        HargaRetailPartsFactory.getDataAll( //pageNumber, $scope.uiGridPageSize,
                dateStart,
                dateEnd,
                $scope.MainHargaRetail_PartsNoMaterial,
                $scope.MainHargaRetail_PartsNamaMaterial)
            .then(
                function(res) {
                    if (!angular.isUndefined(res.data.Result)) {
                        console.log("res =>", res.data.Result);
                        $scope.MainHargaRetailParts_UIGrid.data = res.data.Result;
                        $scope.MainHargaRetailParts_UIGrid.totalItems = res.data.Total;
                        // $scope.MainHargaRetailParts_UIGrid.paginationPageSize = $scope.uiGridPageSize;
                        // $scope.MainHargaRetailParts_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
                    }
                }
            );
    }

    ///UPLOAD HARGA RETAIL PARTS UIGRID
    $scope.UploadHargaRetailParts_UIGrid = {
        paginationPageSizes: null,
        useCustomPagination: true,
        useExternalPagination: true,
        enableFiltering: true,
        rowSelection: true,
        multiSelect: false,
        columnDefs: [
            { name: "PartId", field: "PartId", visible: false },
            { name: "Tanggal Berlaku Dari", field: "EffectiveDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
            { name: "Tanggal Berlaku Sampai", field: "ValidThru", cellFilter: 'date:\"dd-MM-yyyy\"' },
            { name: "No Material", field: "PartNo" },
            { name: "Nama Material", field: "PartName" },
            { name: "Satuan", field: "" },
            { name: "Harga Retail Parts", field: "RetailPrice" },
            { name: "Pesan Error", field: "ErrorMessage" },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.UploadHargaRetailPartsHapus(row)">Hapus</a>'
            }
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridApiUploadHargaRetailParts = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
                $scope.UploadHargaRetailParts_UIGrid_Paging(pageNumber);
            });
        }
    };

    $scope.UploadHargaRetailParts_UIGrid_Paging = function(pageNumber) {
        HargaRetailPartsFactory.getDataTemp(pageNumber, $scope.uiGridPageSize,
                $scope.UploadHargaRetail_FileUploadId)
            .then(
                function(res) {
                    if (!angular.isUndefined(res.data.Result)) {
                        console.log("res =>", res.data.Result);
                        $scope.UploadHargaRetail_DataTemp = res.data.Result;
                        $scope.UploadHargaRetailParts_UIGrid.data = $scope.UploadHargaRetail_DataTemp;
                        $scope.UploadHargaRetailParts_UIGrid.totalItems = res.data.Total;
                        $scope.UploadHargaRetailParts_UIGrid.paginationPageSize = $scope.uiGridPageSize;
                        $scope.UploadHargaRetailParts_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
                    }
                }

            );
    }

    ///SEARCH PARTS
    $scope.ShowModalSearchParts = function() {
        HargaRetailPartsFactory.getDataParts($scope.TambahHargaRetail_PartsNoMaterial)
            .then(
                function(res) {

                    $scope.SearchPartsGrid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );

        angular.element('#ModalSearchParts').modal('setting', { closeable: false }).modal('show');
    }
    $scope.ModalSearchParts_Batal = function() {
        angular.element('#ModalSearchParts').modal('hide');
    }
    $scope.SearchPartsPilihRow = function(row) {
        //RegisterCheque_Data.Name
        console.log("ProspectCustomer Selected", row.entity)

        $scope.TambahHargaRetailParts_PartId = row.entity.PartsId;
        $scope.TambahHargaRetail_PartsNoMaterial = row.entity.PartsCode;
        $scope.TambahHargaRetail_PartsNamaMaterial = row.entity.PartsName;

        angular.element('#ModalSearchParts').modal('hide');
    }

    $scope.SearchPartsGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10, 20],
        columnDefs: [
            { name: 'PartsId', field: 'PartsId', visible: false },
            { name: 'PartsCode', displayName: 'Kode Part', field: 'PartsCode', enableFiltering: true },
            { name: 'PartsName', displayName: 'Nama Part', field: 'PartsName', enableFiltering: true },
            {
                name: 'Action',
                enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchPartsPilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function(gridApi) {
            $scope.grid2Api = gridApi;
        }
    };
});
