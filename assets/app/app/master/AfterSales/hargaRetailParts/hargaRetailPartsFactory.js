angular.module('app')
    .factory('HargaRetailPartsFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        var outletId = user.OutletId === undefined ? user.OrgId : user.OutletId;
        return {
            checkData: function(inputData) {
                var url = '/api/as/AfterSalesMasterRetailPriceParts/SubmitUploadTempData';
                var param = JSON.stringify(inputData);
                console.log("check price data : ", url);
                var res = $http.post(url, param);
                return res;
            },

            getDataTemp: function(start, limit, fileId) {
                var url = '/api/as/AfterSalesMasterRetailPriceParts/GetTempData/?start=' + start + '&limit=' + limit + '&FileId=' + fileId;
                console.log("url Get data temp retail price : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataAll: function(dateStart, dateEnd, partCode, partName) {
                var url = '/api/as/AfterSalesMasterRetailPriceParts/GetData/?OutletId=' + outletId + '&DateStart=' + dateStart + '&DateEnd=' + dateEnd + '&partcode=' + partCode + '&partName=' + partName;
                console.log("url Get data retail price : ", url);
                var res = $http.get(url);
                return res;
            },

            submitData: function(inputData) {
                var url = '/api/as/AfterSalesMasterRetailPriceParts/SubmitUploadData';
                var param = JSON.stringify(inputData);
                console.log("submit price data : ", url);
                var res = $http.post(url, param);
                return res;
            },

            getDataParts: function(partsCode) {
                var url = '/api/as/AfterSalesMasterRetailPriceParts/GetPartsData/?start=1&limit=100&PartsCode=' + partsCode + '&OutletId=' + outletId;
                console.log("url Get data parts : ", url);
                var res = $http.get(url);
                return res;
            },

            deleteData: function(centerId, satelliteId) {
                var url = '/api/as/AfterSalesMasterBPCenterSatellite/Delete/?CenterId=' + centerId + '&satelliteId=' + satelliteId;
                console.log("url Delete data : ", url);
                var res = $http.delete(url);
                return res;
            }
        }
    });