var app = angular.module('app');
app.controller('AsuransiRekananController', function ($scope, $http, $filter, CurrentUser, AsuransiRekananFactory, $timeout, bsNotify, ngDialog) {

	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function() {
		//$scope.loading = true;
		$scope.gridData = [];
	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mAsuransiRekanan = null; //Model
	$scope.xRole = { selected: [] };
	$scope.isHo = false;
	var OutletLogin = $scope.user.OutletId;
	console.log("oke anita", $scope.OutletLogin);

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOption = { format: dateFormat }


	$scope.cekObject = function() {
		console.log($scope.mAsuransiRekanan);
	}

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	$scope.getData = function() {
		AsuransiRekananFactory.getData()
		.then(
			function(res){
				$scope.grid.data = res.data.Result;
			}
		);
	}

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function(rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function(rows) {
			console.log("onSelectRows=>", rows);
		}

	$scope.doCustomSave = function(mdl,mode){
		if (mode=='create') {
            AsuransiRekananFactory.create(mdl).then(function(res){
				if (res.data){
					// $scope.gridDetail.data = [];
					$scope.getData();
					return true;
				} else {
					bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Data Gagal Ditambahkan, Data Duplicate",
                        timeout: 2000
					});
					// $scope.formApi.setMode('detail');
					return false;

				}
            });
        } else {
            AsuransiRekananFactory.update(mdl).then(function(res){
                if (res.data){
					// $scope.gridDetail.data = [];
					$scope.getData();
					return true;
				} else {
					bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Data Gagal Diupdate, Data Duplicate",
                        timeout: 2000
					});
					return false;
				}
            });
        }
	}

	// $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
	// <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-if="!row.groupHeader" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	// <a href="" style="color:#777;" ng-if="(row.entity.OutletId == OutletLogin)" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-if="grid.appScope.allowEdit &amp;&amp; !grid.appScope.hideEditButton &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)" ng-click="grid.appScope.gridClickEditHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	// <a href="" ng-if="(row.entity.OutletId == 280)" ng-click="grid.appScope.$parent.deleteConfirm(row.entity.InsuranceId)" uib-tooltip="Delete" tooltip-placement="bottom" onclick="this.blur()" style="color:#777;"><i class="fa fa-fw fa-lg fa-trash" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	// </div>';

	$scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
	<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-if="!row.groupHeader" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-if="grid.appScope.allowEdit &amp;&amp; !grid.appScope.hideEditButton &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)" ng-click="grid.appScope.gridClickEditHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	<a href="" ng-click="grid.appScope.$parent.deleteConfirm(row.entity.InsuranceId)" uib-tooltip="Delete" tooltip-placement="bottom" onclick="this.blur()" style="color:#777;"><i class="fa fa-fw fa-lg fa-trash" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
	</div>';

$scope.deleteConfirm = function (data) {

	console.log('masuk button', data);
	$scope.asuransiId = data;
	ngDialog.openConfirm ({
		template:'\
								 <div align="center" class="ngdialog-buttons">\
								 <p><b>Konfirmasi</b></p>\
								 <p>Anda yakin akan menghapus data ini?</p>\
								 <div class="ngdialog-buttons" align="center">\
									 <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
									 <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="deleteAsuransi(asuransiId)">Ya</button>\
								 </div>\
								 </div>',
		plain: true,
		scope : $scope,
	 });
}
$scope.onBulkDelete = function(row){
	console.log("row",row);
}
$scope.deleteAsuransi = function (dataId) {
	var idArray = [];
	idArray.push(dataId);
	$scope.ngDialog.close();
	console.log('masuk delete', idArray);
	AsuransiRekananFactory.delete(idArray).then(function (res) {
	console.log('masuk delete Factory', res);
		if (res.data){
			// $scope.gridDetail.data = [];
			bsNotify.show({
										size: 'small',
										type: 'succes',
										title: "Data berhasil didelete",
										timeout: 2000
			});
			$scope.getData();
			return true;
		} else {
			bsNotify.show({
										size: 'big',
										type: 'danger',
										title: "Data gagal didelete",
										timeout: 2000
			});
			// $scope.formApi.setMode('detail');
			return false;

		}
		$scope.getData();
	});

}




	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,

		columnDefs: [
			{ name: 'id', field: 'InsuranceId', visible: false },
			{ name: 'Nama Asuransi', field: 'InsuranceName' },
			{ name: 'Alamat Penagihan', field: 'BillAddress' },
			{ name: 'NPWP ', field: 'NPWP' },
			{name: 'Diskon Part', field: 'DiscountPart' },
			{name: 'Diskon Jasa', field: 'DiscountJasa' },
			{name: 'OutletId', field: 'OutletId', visible: false }

		]
	};

	$scope.allowPatternFilter = function(event) {
        console.log("event", event);
        patternRegex = /[a-z]|\s/i; //ALPHAbeth Only
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            event.preventDefault();
            return false;
        }
	};
	
	$scope.allowPatternFilter2 = function(event) {
        console.log("event", event);
        patternRegex = /\d|[a-z]|\s/i; //ALPHANUMERIC ONLY
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            event.preventDefault();
            return false;
        }
	};
	
	$scope.allowPatternFilter3 = function(event) {
        console.log("event", event);
        patternRegex = /[0-9]/i; //ALPHANUMERIC ONLY
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            event.preventDefault();
            return false;
        }
    };




});
