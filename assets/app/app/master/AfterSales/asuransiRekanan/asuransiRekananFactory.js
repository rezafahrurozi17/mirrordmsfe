angular.module('app')
	.factory('AsuransiRekananFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getData: function(){
			var url = '/api/as/AfterSalesMasterInsurance/?start=1&limit=100&outletId=' + user.OutletId;
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},

		create:function(AsuransiRekananData){
			var url = '/api/as/AfterSalesMasterInsurance/create/';
			var ArrayAsuransiRekananData = [AsuransiRekananData];
			var param = JSON.stringify(ArrayAsuransiRekananData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},

		update:function(AsuransiRekananData){
			var url = '/api/as/AfterSalesMasterInsurance/Update/';
			var ArrayAsuransiRekananData = [AsuransiRekananData];
			var param = JSON.stringify(ArrayAsuransiRekananData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},

		delete:function(id){
			var url = '/api/as/AfterSalesMasterInsurance/deletedata/';
			console.log("url delete Data : ", url, id);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});
