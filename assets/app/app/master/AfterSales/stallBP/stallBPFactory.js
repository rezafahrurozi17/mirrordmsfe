angular.module('app')
	.factory('StallBPFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getDataBPJobCategory: function(start, limit){
			var url = '/api/as/AfterSalesMasterStallBP/GetDataBPJobCategory';
			console.log("url BPJobCategory : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataTeknisiBP: function(teknisiBP){
			var url = '/api/as/AfterSalesMasterStallBP/GetDataBPTechnician/?TechName=' + teknisiBP + '&outletid=' + user.OutletId;
			console.log("url Teknisi BP : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataGrupBP: function(grupBP){
			var url = '/api/as/AfterSalesMasterStallBP/GetDataBPGroup/?GroupName='+grupBP+'&outletid=' + user.OutletId;
			console.log("url Grup BP : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataWOBP: function(){
			var url = '/api/as/AfterSalesMasterStallBP/GetDataBPWO/';
			console.log("url WO BP : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getData: function(start, limit){
			// var url = '/api/as/AfterSalesMasterStallBP/GetData/?start=' + start + '&limit=' + limit + '&outletid='+user.OutletId;
			var url = '/api/as/AfterSalesMasterStallBP/GetData';
			console.log("url Data BP All : ", url);

			var obj = {
				params : {
					Start : start,
					Limit : limit
				}
			};

			var res=$http.get(url,obj);
			return res;

			// https://dms-sit.toyota.astra.co.id/api/as/AfterSalesMasterStallBP/GetData
		},
		
		create:function(inputData){
			var url = '/api/as/AfterSalesMasterStallBP/SaveData/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/as/AfterSalesMasterStallBP/DeleteData/?id=' + id + '&outletId=' + user.OutletId;
			console.log("url delete Data : ", url);
			var res=$http.delete(url);
			return res;
		},

		getStallSchedule : function(StallId){
	        return $http.get('/api/as/AfterSalesMasterStallBP/getStallBPSchedule',{
	            params : {
	              StallId :  StallId
	            }
	        });
	    },
	    saveJadwalStall: function(data) {
	        return $http.post('/api/as/AfterSalesMasterStallBP/PostStallBPSchedule', [data]);
	    }
	}
});