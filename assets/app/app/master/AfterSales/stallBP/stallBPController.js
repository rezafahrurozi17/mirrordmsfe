var app = angular.module('app');
app.controller('StallBPController', function ($scope, $http, $filter, CurrentUser, StallBPFactory, uiGridConstants, $timeout, bsAlert) {
	var user = CurrentUser.user();
	$scope.uiGridPageSize = 10;

	$scope.MainMasterStallBP_Show = true;
	$scope.JadwalStallBP_Show = false;
	$scope.ViewMasterStallBP_Show = false;

	$scope.ViewJadwalStall = function(row){

		$scope.mStallMst = {};
		$scope.mStallMst.StallId = row.StallId;
		$scope.mStallMst.StallName = row.StallName;

        $scope.mStallMst["FromDate"] = "";
        $scope.mStallMst["FromTime"] = "";
        $scope.mStallMst["ToDate"] = "";
        $scope.mStallMst["ToTime"] = "";
        $scope.mStallMst["InactiveReason"] = "";

        $scope.gridJadwal.data = [];
        $scope.deletedJadwalDetail = [];

        StallBPFactory.getStallSchedule(row.StallId).then(function(res){
        	for(var i in res.data.Result){
        		var mData = angular.copy(res.data.Result[i]);

        		res.data.Result[i]["FromDate"] = mData["InactiveStart"].getFullYear() + "-" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveStart"].getDate()).slice(-2);
		        res.data.Result[i]["FromTime"] = mData["InactiveStart"].getHours() + ":" + ("0" + (mData["InactiveStart"].getMinutes())).slice(-2);
		        res.data.Result[i]["ToDate"] = mData["InactiveEnd"].getFullYear() + "-" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveEnd"].getDate()).slice(-2) ;
		        res.data.Result[i]["ToTime"] = mData["InactiveEnd"].getHours() + ":" + ("0" + (mData["InactiveEnd"].getMinutes())).slice(-2);

		        res.data.Result[i].InactiveStart = mData["InactiveStart"].getFullYear() + "-" + ("0" + (mData["InactiveStart"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveStart"].getDate()).slice(-2) + "" + " " + mData["InactiveStart"].getHours() + ":" + ((mData["InactiveStart"].getMinutes())) + ":00.000";
		        res.data.Result[i].InactiveEnd = mData["InactiveEnd"].getFullYear() + "-" + ("0" + (mData["InactiveEnd"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["InactiveEnd"].getDate()).slice(-2) + " " + mData["InactiveEnd"].getHours() + ":" + ((mData["InactiveEnd"].getMinutes())) + ":00.000";
        	}

            $scope.gridJadwal.data = res.data.Result;
        });

		$scope.MainMasterStallBP_Show = false;
        // $scope.TambahMasterStallBP_Show = false;
        $scope.JadwalStallBP_Show = true;
    }

    $scope.addJadwal = function(mData){

        mData["FromDate"] = new Date(mData["FromDate"]);
        mData["ToDate"] = new Date(mData["ToDate"]);
        mData["FromTime"] = new Date(mData["FromTime"]);
        mData["ToTime"] = new Date(mData["ToTime"]);

        mData["FromDateCopy"] = mData["FromDate"].getFullYear() + "-" + ("0" + (mData["FromDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["FromDate"].getDate()).slice(-2);
        mData["FromTimeCopy"] = mData["FromTime"].getHours() + ":" + ((mData["FromTime"].getMinutes()));
        mData["ToDateCopy"] = mData["ToDate"].getFullYear() + "-" + ("0" + (mData["ToDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["ToDate"].getDate()).slice(-2) ;
        mData["ToTimeCopy"] = mData["ToTime"].getHours() + ":" + ((mData["ToTime"].getMinutes()));

        mData.InactiveStart = mData["FromDate"].getFullYear() + "-" + ("0" + (mData["FromDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["FromDate"].getDate()).slice(-2) + "" + " " + mData["FromTime"].getHours() + ":" + ((mData["FromTime"].getMinutes())) + ":00.000";
        mData.InactiveEnd = mData["ToDate"].getFullYear() + "-" + ("0" + (mData["ToDate"].getMonth() + 1)).slice(-2) + "-" + ("0" + mData["ToDate"].getDate()).slice(-2) + " " + mData["ToTime"].getHours() + ":" + ((mData["ToTime"].getMinutes())) + ":00.000";

        $scope.gridJadwal.data.push({
            StallInactiveId : $scope.guid() ,
            StallId : mData.StallId,
            FromDate : mData["FromDateCopy"],
            ToDate : mData["ToDateCopy"],
            FromTime : mData["FromTimeCopy"],
            ToTime : mData["ToTimeCopy"],
            InactiveStart: mData.InactiveStart ,
            InactiveEnd : mData.InactiveEnd ,
            InactiveReason : mData.InactiveReason
        });

        $scope.mStallMst["FromDate"] = "";
        $scope.mStallMst["FromTime"] = "";
        $scope.mStallMst["ToDate"] = "";
        $scope.mStallMst["ToTime"] = "";
        $scope.mStallMst["InactiveReason"] = "";
    }

    $scope.guid = function () {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }

    $scope.deleteJadwalDetail = function(row){
        console.log(row);

        var rowindex = $scope.gridJadwal.data.findIndex(function(obj){
            return obj.StallInactiveId == row.StallInactiveId;
        });

        $scope.gridJadwal.data.splice(rowindex,1);

        if(row.StallInactiveId.toString().indexOf("-") == -1)
            $scope.deletedJadwalDetail.push(row);
    }

    $scope.saveJadwalStall = function(){
    	var mdl = {};
    	var tempInsertData = [];

        for(var i in $scope.gridJadwal.data){
        	// $scope.gridJadwal.data[i].StallInactiveId = $scope.gridJadwal.data[i].StallInactiveId + "";

            if($scope.gridJadwal.data[i].StallInactiveId.toString().indexOf("-") > 0){
                $scope.gridJadwal.data[i].StallInactiveId = 0;
                tempInsertData.push($scope.gridJadwal.data[i]);
            }
            // else
            //     tempInsertData.push($scope.gridJadwal.data[i])
            //     // $scope.gridJadwal.data.splice(i,1);
        }

        // mdl.MStall_Inactive_TM = $scope.gridJadwal.data;
        mdl.MStall_Inactive_TM = tempInsertData;
        mdl.MStall_Inactive_TM_Delete = $scope.deletedJadwalDetail;

        StallBPFactory.saveJadwalStall(mdl).then(function(res){
            $scope.deletedJadwalDetail = [];
            $scope.backFromJadwalStall();
        })
    }

    $scope.backFromJadwalStall = function(){
        $scope.MainMasterStallBP_Show = true;
		$scope.JadwalStallBP_Show = false;
    }

	$scope.TambahMasterStallBP_BPJobCategory = function(){
		StallBPFactory.getDataBPJobCategory()
		.then(
			function(res){
				var resultData = [];
				angular.forEach(res.data.Result, function(value, key){
					resultData.push({"name":value.JobBPName, "value":value.CategoryId});
				});

				$scope.optionsTambahMasterStallBP_JobStall = resultData;


				var dataJenisStall = [
					{
						value : 1,
						name : "TPS"
					},
					{
						value : 2,
						name : "Non-TPS"
					}
				];

				$scope.optionsTambahMasterStallBP_JenisStall = dataJenisStall;
			}
		);
	}

	$scope.TambahMasterStallBP_BPWO = function(){
		StallBPFactory.getDataWOBP()
		.then(
			function(res){
				var resultData = [];
				angular.forEach(res.data.Result, function(value, key){
					resultData.push({"name":value.Name, "value":value.MasterId});
				});

				$scope.optionsTambahMasterStallBP_KategoriWO = resultData;
			}
		);
	}

	$scope.TambahMasterStallBP_BPJobCategory();
	$scope.TambahMasterStallBP_BPWO();

	$scope.MainMasterStallBP_Tambah_Clicked = function(){
		$scope.ViewMode = false;

		$scope.MainMasterStallBP_Show = false;
		$scope.TambahMasterStallBP_Show = true;
		$scope.TambahMasterStallBP_StallId = 0;
		$scope.TambahMasterStallBP_JobStall = null;
		$scope.TambahMasterStallBP_JenisStall = null;
		$scope.TambahMasterStallBP_NamaStall = null;
		$scope.TambahMasterStallBP_EmployeeId = null;
		$scope.TambahMasterStallBP_TeknisiBP = null;
		$scope.TambahMasterStallBP_GrupBPId = null;
		$scope.TambahMasterStallBP_GrupBP = null;
		$scope.TambahMasterStallBP_KategoriWO = null;
		$scope.TambahMasterStallBP_JamTersedia = null;
		$scope.TambahMasterStallBP_JamBukaDefaultJam = null;
		$scope.TambahMasterStallBP_JamBukaDefaultMenit = null;
		$scope.TambahMasterStallBP_JamTutupDefaultJam = null;
		$scope.TambahMasterStallBP_JamTutupDefaultMenit = null;
		$scope.TambahMasterStallBP_Istirahat1JamAwal = null;
		$scope.TambahMasterStallBP_Istirahat1MenitAwal = null;
		$scope.TambahMasterStallBP_Istirahat1JamAkhir = null;
		$scope.TambahMasterStallBP_Istirahat1MenitAkhir = null;
		$scope.TambahMasterStallBP_Istirahat2JamAwal = null;
		$scope.TambahMasterStallBP_Istirahat2MenitAwal = null;
		$scope.TambahMasterStallBP_Istirahat2JamAkhir = null;
		$scope.TambahMasterStallBP_Istirahat2MenitAkhir =null;
		$scope.TambahMasterStallBP_Istirahat3JamAwal = null;
		$scope.TambahMasterStallBP_Istirahat3MenitAwal = null;
		$scope.TambahMasterStallBP_Istirahat3JamAkhir = null;
		$scope.TambahMasterStallBP_Istirahat3MenitAkhir = null;

		$scope.TambahStatusCode = 0;
	}
	$scope.TambahMasterStallBP_Kembali_Clicked = function(){
		$scope.MainMasterStallBP_Show = true;
		$scope.TambahMasterStallBP_Show = false;
	}

	$scope.DeleteMasterStallBP = function(row){
		StallBPFactory.delete(row.entity.StallId)
		.then(
			function(res){
				alert("Data berhasil dihapus");
				$scope.StallBP_List_UIGrid_Paging(1, 250);
			},
			function(err){
				alert(err.data.ExceptionMessage);
			}
		);
	}

	//Jika jam istirahat 2 null
	if (_.isUndefined($scope.TambahMasterStallBP_Istirahat2JamAwal) || 
	_.isNull($scope.TambahMasterStallBP_Istirahat2JamAwal)){
		
		$scope.StartBreakTime2 = null;
		$scope.EndBreakTime2 = null;
	} else {
		$scope.StartBreakTime2 = $scope.TambahMasterStallBP_Istirahat2JamAwal + ":" + $scope.TambahMasterStallBP_Istirahat2MenitAwal;
		$scope.EndBreakTime2 = $scope.TambahMasterStallBP_Istirahat2JamAkhir + ":" + $scope.TambahMasterStallBP_Istirahat2MenitAkhir;
	}

	//Jika jam istirahat 3 null
	if (_.isUndefined($scope.TambahMasterStallBP_Istirahat3JamAwal) || 
		_.isNull($scope.TambahMasterStallBP_Istirahat3JamAwal)){
			
		$scope.StartBreakTime3 = null;
		$scope.EndBreakTime3 = null;
	} else {
		$scope.StartBreakTime3 = $scope.TambahMasterStallBP_Istirahat3JamAwal + ":" + $scope.TambahMasterStallBP_Istirahat3MenitAwal;
		$scope.EndBreakTime3 = $scope.TambahMasterStallBP_Istirahat3JamAkhir + ":" + $scope.TambahMasterStallBP_Istirahat3MenitAkhir;
	}

	$scope.ViewMasterStallBP = function(row){
		console.log("wo id view: ", row.entity.BPWOId);
		console.log("row.entity",row.entity);
		
		$scope.TambahMasterStallBP_StallId = row.entity.StallId;
		// $scope.TambahMasterStallBP_JenisStall = row.entity.StallTypeId;
		$scope.TambahMasterStallBP_JobStall = row.entity.Type;
		$scope.TambahMasterStallBP_JenisStall = row.entity.CategoryId;
		$scope.TambahMasterStallBP_NamaStall = row.entity.StallName;
		$scope.TambahMasterStallBP_EmployeeId = row.entity.EmployeeId;
		$scope.TambahMasterStallBP_TeknisiBP = row.entity.EmployeeName;
		$scope.TambahMasterStallBP_GrupBPId = row.entity.BPGroupId;
		$scope.TambahMasterStallBP_GrupBP = row.entity.BPGroupName;
		$scope.TambahMasterStallBP_KategoriWO = row.entity.BPWOId;
		$scope.TambahMasterStallBP_JamTersedia = row.entity.AvailableTime;
		$scope.TambahMasterStallBP_JamBukaDefaultJam = parseInt(row.entity.DefaultOpenTime.substring(0,2));
		$scope.TambahMasterStallBP_JamBukaDefaultMenit = parseInt(row.entity.DefaultOpenTime.substring(3,5));
		$scope.TambahMasterStallBP_JamTutupDefaultJam = parseInt(row.entity.DefaultCloseTime.substring(0,2));
		$scope.TambahMasterStallBP_JamTutupDefaultMenit = parseInt(row.entity.DefaultCloseTime.substring(3,5));
		$scope.TambahMasterStallBP_Istirahat1JamAwal = parseInt(row.entity.StartBreakTime1.substring(0,2));
		$scope.TambahMasterStallBP_Istirahat1MenitAwal = parseInt(row.entity.StartBreakTime1.substring(3,5));
		$scope.TambahMasterStallBP_Istirahat1JamAkhir = parseInt(row.entity.EndBreakTime1.substring(0,2));
		$scope.TambahMasterStallBP_Istirahat1MenitAkhir = parseInt(row.entity.EndBreakTime1.substring(3,5));

		if(row.entity.StartBreakTime2 != null){
			$scope.TambahMasterStallBP_Istirahat2JamAwal = parseInt(row.entity.StartBreakTime2.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat2MenitAwal = parseInt(row.entity.StartBreakTime2.substring(3,5));
			$scope.TambahMasterStallBP_Istirahat2JamAkhir = parseInt(row.entity.EndBreakTime2.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat2MenitAkhir = parseInt(row.entity.EndBreakTime2.substring(3,5));
		}

		if(row.entity.StartBreakTime3 != null){
			$scope.TambahMasterStallBP_Istirahat3JamAwal = parseInt(row.entity.StartBreakTime3.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat3MenitAwal = parseInt(row.entity.StartBreakTime3.substring(3,5));
			$scope.TambahMasterStallBP_Istirahat3JamAkhir = parseInt(row.entity.EndBreakTime3.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat3MenitAkhir = parseInt(row.entity.EndBreakTime3.substring(3,5));
		}

		$scope.MainMasterStallBP_Show = false;
		$scope.TambahMasterStallBP_Show = true;
		$scope.ViewMode = true;

		$scope.TambahStatusCode = row.entity.StatusCode;
		console.log("TambahStatusCode", $scope.TambahStatusCode)
	}


	$scope.EditMasterStallBP = function(row){
		console.log("wo id : ", row.entity.BPWOId);
		console.log("row.entity",row.entity);
		$scope.TambahMasterStallBP_StallId = row.entity.StallId;
		// $scope.TambahMasterStallBP_JenisStall = row.entity.StallTypeId;
		$scope.TambahMasterStallBP_JobStall = row.entity.Type;
		$scope.TambahMasterStallBP_JenisStall = row.entity.CategoryId;
		$scope.TambahMasterStallBP_NamaStall = row.entity.StallName;
		$scope.TambahMasterStallBP_EmployeeId = row.entity.EmployeeId;
		$scope.TambahMasterStallBP_TeknisiBP = row.entity.EmployeeName;
		$scope.TambahMasterStallBP_GrupBPId = row.entity.BPGroupId;
		$scope.TambahMasterStallBP_GrupBP = row.entity.BPGroupName;
		$scope.TambahMasterStallBP_KategoriWO = row.entity.BPWOId;
		$scope.TambahMasterStallBP_JamTersedia = row.entity.AvailableTime;
		$scope.TambahMasterStallBP_JamBukaDefaultJam = parseInt(row.entity.DefaultOpenTime.substring(0,2));
		$scope.TambahMasterStallBP_JamBukaDefaultMenit = parseInt(row.entity.DefaultOpenTime.substring(3,5));
		$scope.TambahMasterStallBP_JamTutupDefaultJam = parseInt(row.entity.DefaultCloseTime.substring(0,2));
		$scope.TambahMasterStallBP_JamTutupDefaultMenit = parseInt(row.entity.DefaultCloseTime.substring(3,5));
		$scope.TambahMasterStallBP_Istirahat1JamAwal = parseInt(row.entity.StartBreakTime1.substring(0,2));
		$scope.TambahMasterStallBP_Istirahat1MenitAwal = parseInt(row.entity.StartBreakTime1.substring(3,5));
		$scope.TambahMasterStallBP_Istirahat1JamAkhir = parseInt(row.entity.EndBreakTime1.substring(0,2));
		$scope.TambahMasterStallBP_Istirahat1MenitAkhir = parseInt(row.entity.EndBreakTime1.substring(3,5));

		if(row.entity.StartBreakTime2 != null){
			$scope.TambahMasterStallBP_Istirahat2JamAwal = parseInt(row.entity.StartBreakTime2.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat2MenitAwal = parseInt(row.entity.StartBreakTime2.substring(3,5));
			$scope.TambahMasterStallBP_Istirahat2JamAkhir = parseInt(row.entity.EndBreakTime2.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat2MenitAkhir = parseInt(row.entity.EndBreakTime2.substring(3,5));
		}

		if(row.entity.StartBreakTime3 != null){
			$scope.TambahMasterStallBP_Istirahat3JamAwal = parseInt(row.entity.StartBreakTime3.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat3MenitAwal = parseInt(row.entity.StartBreakTime3.substring(3,5));
			$scope.TambahMasterStallBP_Istirahat3JamAkhir = parseInt(row.entity.EndBreakTime3.substring(0,2));
			$scope.TambahMasterStallBP_Istirahat3MenitAkhir = parseInt(row.entity.EndBreakTime3.substring(3,5));
		}

		$scope.MainMasterStallBP_Show = false;
		$scope.TambahMasterStallBP_Show = true;
		$scope.ViewMode = false;
		$scope.TambahStatusCode = row.entity.StatusCode;
		
	}

	$scope.TambahMasterStallBP_Simpan_Clicked = function(){
		if ($scope.TambahMasterStallBP_JamTutupDefaultJam > 22) {
			bsAlert.warning("Batas waktu jam tutup Stall max 22:59");
		} else {
			console.log($scope.optionsTambahMasterStallBP_JenisStall);
			var categoryNameIndex = $scope.optionsTambahMasterStallBP_JenisStall.findIndex(function(obj){
				return obj.value == $scope.TambahMasterStallBP_JenisStall
			});
			console.log("categoryNameIndex",categoryNameIndex,$scope.optionsTambahMasterStallBP_JenisStall.length,$scope.TambahMasterStallBP_JenisStall);
			var categoryName = $scope.optionsTambahMasterStallBP_JenisStall[categoryNameIndex].name;
	
			var inputData = [{
				"CategoryId" : $scope.TambahMasterStallBP_JenisStall,
				"CategoryName" : categoryName,
				"StallId" : $scope.TambahMasterStallBP_StallId,
				// "OutletId" : user.OutletId,
				"StallTypeId" : $scope.TambahMasterStallBP_JobStall,
				"StallName" : $scope.TambahMasterStallBP_NamaStall,
				// "EmployeeId" : $scope.TambahMasterStallBP_EmployeeId,
				// "BPGroupId" : $scope.TambahMasterStallBP_GrupBPId,
				// "BPWOId" : $scope.TambahMasterStallBP_KategoriWO,
				// "VendorName" : $scope.TambahMasterStallBP_NamaVendor,
				"StatusCode" : $scope.TambahStatusCode,
				"AvailableTime" : $scope.TambahMasterStallBP_JamTersedia,
				"DefaultOpenTime" : $scope.TambahMasterStallBP_JamBukaDefaultJam + ":" + $scope.TambahMasterStallBP_JamBukaDefaultMenit,
				"DefaultCloseTime" : $scope.TambahMasterStallBP_JamTutupDefaultJam + ":" + $scope.TambahMasterStallBP_JamTutupDefaultMenit,
				"StartBreakTime1" : $scope.TambahMasterStallBP_Istirahat1JamAwal + ":" + $scope.TambahMasterStallBP_Istirahat1MenitAwal,
				"EndBreakTime1" : $scope.TambahMasterStallBP_Istirahat1JamAkhir + ":" + $scope.TambahMasterStallBP_Istirahat1MenitAkhir,
				"StartBreakTime2" : $scope.StartBreakTime2,
				"EndBreakTime2" : $scope.EndBreakTime2,
				"StartBreakTime3" : $scope.StartBreakTime3,
				"EndBreakTime3" : $scope.EndBreakTime3,
			}];
	
			var fieldNotMandatory = [
				"2",
				// "EndBreakTime2",
				"3"
				// "EndBreakTime3"
			];
	
			for(var i in fieldNotMandatory){
	
				var string = inputData[0]["StartBreakTime"+fieldNotMandatory[i]] + ' - ' + inputData[0]["EndBreakTime"+fieldNotMandatory[i]];
				var count = (string.match(/undefined/g) || []).length;
				if(count == 4){
					inputData[0]["StartBreakTime"+fieldNotMandatory[i]] = null;
					inputData[0]["EndBreakTime"+fieldNotMandatory[i]] = null;
				}
			}
	
			console.log("data to save : ", inputData);
			StallBPFactory.create(inputData)
			.then(
				function(res){
					bsAlert.success("Data berhasil disimpan");
					$scope.TambahMasterStallBP_ClearFields();
					$scope.TambahMasterStallBP_Kembali_Clicked();
					$scope.StallBP_List_UIGrid_Paging(1, 250);
				},
				function(err){
	
				}
			);
		}

		
	}

	$scope.TambahMasterStallBP_ClearFields = function(){
		$scope.TambahMasterStallBP_JenisStall = '';
		$scope.TambahMasterStallBP_EmployeeId = 0;
		$scope.TambahMasterStallBP_GrupBPId = 0;
		$scope.TambahMasterStallBP_KategoriWO = '';
		$scope.TambahMasterStallBP_NamaVendor = '';
		$scope.TambahMasterStallBP_JamTersedia = '';
		$scope.TambahMasterStallBP_JamBukaDefaultJam = '';
		$scope.TambahMasterStallBP_JamBukaDefaultMenit = '';
		$scope.TambahMasterStallBP_JamTutupDefaultJam = '';
		$scope.TambahMasterStallBP_JamTutupDefaultMenit = '';
		$scope.TambahMasterStallBP_Istirahat1JamAwal  = '';
		$scope.TambahMasterStallBP_Istirahat1JamAkhir  = '';
		$scope.TambahMasterStallBP_Istirahat2JamAwal  = '';
		$scope.TambahMasterStallBP_Istirahat2JamAkhir  = '';
		$scope.TambahMasterStallBP_Istirahat3JamAwal  = '';
		$scope.TambahMasterStallBP_Istirahat3JamAkhir  = '';
		$scope.TambahMasterStallBP_Istirahat1MenitAwal = '';
		$scope.TambahMasterStallBP_Istirahat1MenitAkhir = '';
		$scope.TambahMasterStallBP_Istirahat2MenitAwal = '';
		$scope.TambahMasterStallBP_Istirahat2MenitAkhir = '';
		$scope.TambahMasterStallBP_Istirahat3MenitAwal = '';
		$scope.TambahMasterStallBP_Istirahat3MenitAkhir = '';
	}

	$scope.StallBP_List_UIGrid = {
		// paginationPageSizes: [100, 20, 50],
		// useCustomPagination: true,
		// useExternalPagination : true,
		// rowSelection : true,
		// multiSelect : false,
		enableFiltering: true,
		columnDefs:[
					// {name:"StallId",field:"StallId",visible:false},
					// {name:"Jenis Stall",field:"StallTypeName"},
					{name:"Jenis Stall", width:120, field:"xStatus"},
					{name:"Nama Stall", width:160, field:"StallName"},
					{name:"EmployeeId", field:"EmployeeId", visible:false},
					// {name:"Teknisi", field:"EmployeeName"},
					// {name:"BPGroupId", field:"BPGroupId", visible:false},
					// {name:"Group BP", field:"BPGroupName"},
					// {name:"BPWOId", field:"BPWOId", visible:false},
					{name:"Jam Tersedia", width:80, field:"AvailableTime"},
					// {name:"Jam Istirahat 1", field:"BreakTime1"},
					// {name:"Jam Istirahat 2", field:"BreakTime2"},
					// {name:"Jam Istirahat 3", field:"BreakTime3"},
					{name:"Jam Istirahat 1", width:150, field:"JamIstirahat1"},
					{name:"Jam Istirahat 2", width:150, field:"JamIstirahat2"},
					{name:"Jam Istirahat 3", width:150, field:"JamIstirahat3"},
					{name:"Jam Buka Default", width:100, field:"DefaultOpenTime"},
					{name:"Jam Tutup Default", width:100, field:"DefaultCloseTime"},
					{name:"Status Aktif", width:110, field:"StatusCodeName"},
					// {name:"Status Aktif", field:"StatusCode", visible:false},
					{name:"Action", width:120, cellTemplate:'<a ng-click="grid.appScope.EditMasterStallBP(row)">&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil" aria-hidden="true" style="color:#000; font-size:16px; margin-top:5px; margin-left:10px; margin-right:5px" title="Ubah"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.ViewMasterStallBP(row)"><i class="fa fa-table" aria-hidden="true" style="color:#000; font-size:16px; margin-top:5px; margin-right:5px" title="Lihat"></i></a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.ViewJadwalStall(row.entity)"><i class="fa fa-calendar" aria-hidden="true" style="color:#000; font-size:16px; margin-top:5px; margin-right:5px" title="Jadwal"></i></a>'},
								// cellTemplate:'<a ng-click="grid.appScope.EditMasterStallBP(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.DeleteMasterStallBP(row)">Hapus</a>'},
					{name:"StartBreakTime1", field:"StartBreakTime1", visible:false},
					{name:"EndBreakTime1", field:"EndBreakTime1", visible:false},
					{name:"StartBreakTime2", field:"StartBreakTime2", visible:false},
					{name:"EndBreakTime2", field:"EndBreakTime2", visible:false},
					{name:"StartBreakTime3", field:"StartBreakTime3", visible:false},
					{name:"EndBreakTime3", field:"EndBreakTime3", visible:false}
				],
		onRegisterApi: function(gridApi) {
			$scope.GridApiStallBP = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.StallBP_List_UIGrid_Paging(pageNumber, pageSize);
			});
		}
	};

	$scope.StallBP_List_UIGrid_Paging = function(pageNumber, pageSize){
		  StallBPFactory.getData(pageNumber, pageSize)
		  .then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				for (var i = 0; i < res.data.Result.length; i++) {
				switch (res.data.Result[i].CategoryId) {
					case 1:
						res.data.Result[i].xStatus = "TPS";
						break;
					case 2:
						res.data.Result[i].xStatus = "NON-TPS";
						break;
					}
				}
				$scope.StallBP_List_UIGrid.data = res.data.Result;
				$scope.StallBP_List_UIGrid.totalItems = res.data.Total;
				// $scope.StallBP_List_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.StallBP_List_UIGrid.paginationPageSizes = [$scope.uiGridPageSize, 20, 50];

				// console.log('totalItems', $scope.StallBP_List_UIGrid.totalItems);
				// console.log('paginationPageSize', $scope.StallBP_List_UIGrid.paginationPageSize);
				// console.log('paginationPageSizes', $scope.StallBP_List_UIGrid.paginationPageSizes);
			}

		);
	}

	$scope.StallBP_List_UIGrid_Paging(1, 250);

	///SEARCH TEKNISI BP
	$scope.TambahMasterStallBP_CariTeknisi_Clicked = function(){
		$scope.ShowModalSearchTeknisiBP();
	}

	$scope.ShowModalSearchTeknisiBP = function()
    {
		StallBPFactory.getDataTeknisiBP($scope.TambahMasterStallBP_TeknisiBP)
		.then(
			function(res){
                $scope.TeknisiBPGrid.data = res.data.Result;
			}
        );

        angular.element('#ModalSearchTeknisiBP').modal('setting',{closeable:false}).modal('show');
    }
    $scope.ModalSearchTeknisiBP_Batal = function()
    {
        angular.element('#ModalSearchTeknisiBP').modal('hide');
    }
    $scope.SearchTeknisiBPPilihRow = function(row)
    {
        //RegisterCheque_Data.Name
        console.log("ProspectCustomer Selected", row.entity)

        $scope.TambahMasterStallBP_EmployeeId = row.entity.EmployeeId;
        $scope.TambahMasterStallBP_TeknisiBP = row.entity.EmployeeName;

        angular.element('#ModalSearchTeknisiBP').modal('hide');
    }
    $scope.TeknisiBPGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10,20],
		columnDefs: [
            { name:'EmployeeId', field:'EmployeeId', visible:false},
            { name:'EmployeeName', displayName:'Nama teknisi', field:'EmployeeName', enableFiltering: true },
            { name:'ManPowerTypeCode', displayName:'Kode Tipe ManPower', field:'ManPowerTypeCode', enableFiltering: true },
            { name:'ManPowerTypeName', displayName:'Nama Tipe ManPower', field:'ManPowerTypeName', enableFiltering: true },
            { name: 'Action', enableFiltering: false,
            cellTemplate:'<a ng-click="grid.appScope.SearchTeknisiBPPilihRow(row)">Pilih</a>'}
        ],
        onRegisterApi: function (gridApi) {
            $scope.gridApiTeknisiBP = gridApi;
        }
	};

	///SEARCH Group BP
	$scope.TambahMasterStallBP_CariGrupBP_Clicked = function(){
		$scope.ShowModalSearchGrupBP();
	}

	$scope.ShowModalSearchGrupBP = function()
    {
		StallBPFactory.getDataGrupBP($scope.TambahMasterStallBP_GrupBP)
		.then(
			function(res){
                $scope.GrupBPGrid.data = res.data.Result;
			}
        );

        angular.element('#ModalSearchGrupBP').modal('setting',{closeable:false}).modal('show');
    }
    $scope.ModalSearchGrupBP_Batal = function()
    {
        angular.element('#ModalSearchGrupBP').modal('hide');
    }
    $scope.SearchGrupBPPilihRow = function(row)
    {
        $scope.TambahMasterStallBP_GrupBP = row.entity.GroupName;
        $scope.TambahMasterStallBP_GrupBPId = row.entity.GroupId;

        angular.element('#ModalSearchGrupBP').modal('hide');
    }
    $scope.GrupBPGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10,20],
		columnDefs: [
            { name:'GroupId', field:'GroupId', visible:false},
            { name:'GroupName', displayName:'Nama Grup', field:'GroupName', enableFiltering: true },
            { name: 'Action', enableFiltering: false,
            cellTemplate:'<a ng-click="grid.appScope.SearchGrupBPPilihRow(row)">Pilih</a>'}
        ],
        onRegisterApi: function (gridApi) {
            $scope.gridApiGrupBP = gridApi;
        }
	};

	$scope.gridJadwal = {
        // enableSorting: true,
        // enableRowSelection: true,
        // multiSelect: true,
        // enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Id',field:'id', width:'7%', visible:false },
            { name:'Jam Dari', field:'FromTime'},
            { name:'Tanggal Dari', field:'FromDate'},
            { name:'Jam Sampai', field:'ToTime'},
            { name:'Tanggal Sampai', field:'ToDate'},
            { name:'Catatan', field:'InactiveReason'},
            { name:'action', allowCellFocus: false, cellTemplate: "<a ng-click='grid.appScope.deleteJadwalDetail(row.entity)'>delete<a>"}
        ]
    };

    $scope.gridJadwalTableHeight = function() {
       var headerHeight = 30; // your header height
       if($scope.gridJadwal.data != undefined){
            if($scope.gridJadwal.data.length == 0){
                return {
                    height: 250 + "px"
                };
            }else if($scope.gridJadwal.data.length < 10){
                return {
                    height: ($scope.gridJadwal.data.length * headerHeight) + 160 + "px"
                };
            }else{
                return {
                    height: 460 + "px"
                }
            }

       }
    };


		//----------------------Filter-------------------------------
		$scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    $scope.gridApiAppointment = {};
    $scope.filterColIdx = 1;
    var x = -1;
    for (var i = 0; i < $scope.StallBP_List_UIGrid.columnDefs.length; i++) {
        if ($scope.StallBP_List_UIGrid.columnDefs[i].visible == undefined && $scope.StallBP_List_UIGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.StallBP_List_UIGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
        }
        console.log("$scope.gridCols", $scope.gridCols);
        console.log("$scope.grid.columnDefs[i]", $scope.StallBP_List_UIGrid.columnDefs[i]);
    }
    $scope.filterBtnLabel = $scope.gridCols[0].name;

		$scope.filterBtnChange = function(col) {
        console.log("col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };

});
