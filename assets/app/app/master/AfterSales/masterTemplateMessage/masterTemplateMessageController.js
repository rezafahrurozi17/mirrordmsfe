var app = angular.module('app');
app.controller('MasterTemplateMessageController', function ($scope, $rootScope, $element, $http, $filter, CurrentUser, MasterTemplateMessageFactory, $timeout, bsNotify) {
	var user = CurrentUser.user();
	$scope.TemplateMessageEdit_TypeList = [];
	$scope.uiGridPageSize = 10;
	$scope.TemplateMessageMain_Show = true;
	$scope.tempString = '';

	function checkRbyte(rb,b){
		var p = rb & Math.pow(2,b);
		return (   ( p == Math.pow(2,b)) );
	}
	
	var rByte = JSON.parse($scope.tab.item).ByteEnable;
	var allowEdit = checkRbyte(rByte,4)
	
	$scope.TemplateMessageEdit_GetMessageTypeList = function(){
		MasterTemplateMessageFactory.getDataMessageTypeList()
		.then(
			function(res){
				var ComboData = [];
				$scope.TemplateMessageEdit_TypeList = res.data.Result;
				angular.forEach($scope.TemplateMessageEdit_TypeList, function(value, key){
					ComboData.push({name:value.TypeName, value:value.TemplateTypeId});
				});
				
				$scope.optionsTemplateMessageEdit_MessageType = ComboData;
				$scope.TemplateMessageEdit_MessageType = $scope.TemplateMessageEdit_TypeList[0].TemplateTypeId;
			}
		);
	}
	
	$scope.TemplateMessageEdit_MessageType_Changed = function(){
		if($scope.TemplateMessageEdit_MessageType == 1){
			$scope.hideSmsPsfu = true;
		}else{
			$scope.hideSmsPsfu = false;
		}
		
		
		$scope.TemplateMessageMain_SMS = '';
		// $scope.TemplateMessageMain_Email = '';
		$scope.TemplateMessageMain_SMSMessageType = 0;
		$scope.TemplateMessageMain_SMSMessageId = 0;

		console.log('anita tes =>', $scope.TemplateMessageEdit_MessageType);
		
		if(!angular.isUndefined($scope.TemplateMessageEdit_MessageType)){
			MasterTemplateMessageFactory.getData($scope.TemplateMessageEdit_MessageType)
			.then(
				function(res){
					console.log('anita tes 2 =>', res);
					$scope.TemplateMessageMain_SMS = res.data.Result[0].MessageDesc;
					$scope.copyTemplateSMS = angular.copy(res.data.Result[0].MessageDesc)
					$scope.TemplateMessageMain_SMSMessageType = res.data.Result[0].MessageType;
					$scope.TemplateMessageMain_SMSMessageId = res.data.Result[0].MessageId;
				}
			);
		}

		// $scope.ShowDataMain();
	}
	
	$scope.TemplateMessageEdit_GetMessageTypeList();
	
	$scope.TemplateMessageParam_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination : true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: [
						{name:"ParamId",field:"ParamId",visible:false} ,	
						{name:"ParamTag",field:"ParamTag",visible:false},
						{name:"Nama Parameter",field:"ParamName"}
					],
		onRegisterApi: function(gridApi) {
			$scope.gridApiTemplateMessage = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function(row) {
				var textArea = angular.element("#TemplateMessageEdit_Message");
				console.log("posisi :", textArea.prop("selectionStart"));
			});
		}
	};
	
	$scope.GetListParam = function(){
		MasterTemplateMessageFactory.getDataParam($scope.TemplateMessageEdit_Id)
		.then(
			function(res){
				$scope.TemplateMessageParam_UIGrid.data = res.data.Result;
			}
		);
	}
	
	
  
	$scope.ShowDataMain = function(){
		console.log('anita tes => showDataMain');
		MasterTemplateMessageFactory.getData(0)
		.then(
			function(res){
				$scope.TemplateMessageMain_Email = res.data.Result[0].MessageDesc;
				$scope.TemplateMessageMain_EmailMessageType = res.data.Result[0].MessageType;
				$scope.TemplateMessageMain_EmailMessageId = res.data.Result[0].MessageId;
			}
		);
		
		$scope.TemplateMessageEdit_MessageType_Changed();
	
	}
	
	$scope.ShowDataMain();
	
	$scope.TemplateMessageMain_EditSMS_Clicked = function(){
		$scope.tempString = 'SMS'
		$scope.TemplateMessageMain_Show = false;
		$scope.TemplateMessageEdit_Show = true;
		$scope.TemplateMessageEdit_ShowButton = allowEdit;
		$scope.TemplateMessageEdit_SMSType = $scope.TemplateMessageEdit_MessageType;
		$scope.TemplateMessageEdit_Message = $scope.TemplateMessageMain_SMS;
		$scope.TemplateMessageEdit_Id = $scope.TemplateMessageMain_SMSMessageId;
		$scope.TemplateMessageEdit_Type = $scope.TemplateMessageMain_SMSMessageType;
		$scope.currentEditisSMS = 1;
		$scope.GetListParam();
	}
	$scope.currentEditisSMS = 1;
	
	$scope.TemplateMessageMain_EditEmail_Clicked = function(){
		$scope.tempString = 'E-mail'
		$scope.TemplateMessageMain_Show = false;
		$scope.TemplateMessageEdit_Show = true;
		$scope.TemplateMessageEdit_ShowButton = allowEdit;
		$scope.TemplateMessageEdit_SMSType = 0;
		$scope.TemplateMessageEdit_Message = $scope.TemplateMessageMain_Email;
		$scope.TemplateMessageEdit_Id = $scope.TemplateMessageMain_EmailMessageId;
		$scope.TemplateMessageEdit_Type = $scope.TemplateMessageMain_EmailMessageType;
		$scope.currentEditisSMS = 0;
		$scope.GetListParam();
	}
	
	$scope.TemplateMessageEdit_Batal_Clicked = function(){
		$scope.TemplateMessageMain_Show = true;
		$scope.TemplateMessageEdit_Show = false;
		$scope.TemplateMessageEdit_Message = '';
	}

	$scope.TemplateMessageEdit_DefaultTemplate_Clicked = function(){
		MasterTemplateMessageFactory.getDefaultTemplate($scope.currentEditisSMS).then(function(res){
			if(!res)
				bsNotify.show({
                  title: "Default Message",
                  content: "Default Message untuk Outlet anda belum tersedia",
                  type: 'warning'
                });
			else
				$scope.TemplateMessageEdit_Message = res.data.Result[0].MessageDesc;
		});
	}

	// $scope.TemplateDefaultHO_Clicked = function(){
	// 	$scope.TemplateMessageEdit_Message = '';
	// }
	
	$scope.TemplateMessageEdit_Insert_Clicked = function(){
		$rootScope.$broadcast('insert', $scope.gridApiTemplateMessage.selection.getSelectedRows()[0].ParamTag);
	}
	
	$scope.TemplateMessageEdit_Outlet_Clicked = function(){
		$rootScope.$broadcast('insert', '#Outlet#');
	}
	
	$scope.TemplateMessageEdit_NomorHPCustomer_Clicked = function(){
		$rootScope.$broadcast('insert', '#NomorHPCustomer#');
	}

	$scope.TemplateMessageEdit_NomorPolisi_Clicked = function(){
		$rootScope.$broadcast('insert', '#NomorPolisi#');
	}
	
	$scope.TemplateMessageEdit_TanggalJatuhTempo_Clicked = function(){
		$rootScope.$broadcast('insert', '#TanggalJatuhTempo#');
	}
	
	$scope.TemplateMessageEdit_JenisSB_Clicked = function(){
		$rootScope.$broadcast('insert', '#JenisSB#');
	}
	
	$scope.TemplateMessageEdit_WarnaKendaraan_Clicked = function(){
		$rootScope.$broadcast('insert', '#WarnaKendaraan#');
	}
	
	$scope.TemplateMessageEdit_NomorPolisi_Clicked = function(){
		$rootScope.$broadcast('insert', '#NomorPolisi#');
	}
	
	$scope.TemplateMessageEdit_ModelKendaraan_Clicked = function(){
		$rootScope.$broadcast('insert', '#ModelKendaraan#');
	}
	
	$scope.TemplateMessageEdit_NamaCustomer_Clicked = function(){
		$rootScope.$broadcast('insert', '#NamaCustomer#');
	}
	
	$scope.TemplateMessageEdit_Simpan_Clicked = function(){
		console.log('anita tes TemplateMessageEdit_MessageType', $scope.TemplateMessageEdit_MessageType);
		var inputData = [
		{
			"MessageDesc" : $element[0].value,//$scope.TemplateMessageEdit_Message,
			"MessageId" : $scope.TemplateMessageEdit_Id,
			"MessageType" : $scope.TemplateMessageEdit_SMSType,
			"OutletId" : user.OutletId
		}];
		
		MasterTemplateMessageFactory.create(inputData)
		.then(
			function(res){
				alert("Data berhasil disimpan");
				$scope.ShowDataMain();
				$scope.TemplateMessageEdit_Batal_Clicked();
			},
			function(err){
				alert(err.data.Message);
			}
		);
		
	}
});

app.directive('myText', ['$rootScope', function($rootScope) {
  return {
    link: function(scope, element, attrs) {
      $rootScope.$on('insert', function(e, val) {
        var domElement = element[0];
		console.log("element", element);
        if (document.selection) {
			  domElement.focus();
			  var sel = document.selection.createRange();
			  sel.text = val;
			  domElement.focus();
        } else if (domElement.selectionStart || domElement.selectionStart === 0) {
			  var startPos = domElement.selectionStart;
			  var endPos = domElement.selectionEnd;
			  var scrollTop = domElement.scrollTop;
			  domElement.value = domElement.value.substring(0, startPos) + val + domElement.value.substring(endPos, domElement.value.length);
			  domElement.focus();
			  domElement.selectionStart = startPos + val.length;
			  domElement.selectionEnd = startPos + val.length;
			  domElement.scrollTop = scrollTop;
        } else {
			  domElement.value += val;
			  domElement.focus();
        }
      });
    },
	controller: 'MasterTemplateMessageController'
  }
}]);