angular.module('app')
    .factory('MasterTemplateMessageFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        var OultetId = user.OutletId === undefined ? user.OrgId : user.OutletId;
        return {
            getData: function(messageType) {
                var url = '/api/as/AfterSalesTemplateMessage/GetMessage/?MessageType=' + messageType + '&OutletId=' + OultetId;
                console.log("url TemplateMessage : ", url);
                var res = $http.get(url);
                return res;
            },

            create: function(inputData) {
                var url = '/api/as/AfterSalesTemplateMessage/SubmitData/';
                var param = JSON.stringify(inputData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            getDefaultTemplate: function(msgtype) {
                var url = '/api/as/AfterSalesTemplateMessage/GetDefaultMessage/?typemsg=' + msgtype;
                console.log("url TemplateMessageParam : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataParam: function(messageId) {
                var url = '/api/as/AfterSalesTemplateMessage/GetMessageParam/?MessageId=' + messageId + '&OutletId=' + OultetId;
                console.log("url TemplateMessageParam : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataMessageTypeList: function() {
                var url = '/api/as/AfterSalesTemplateMessage/GetMessageType';
                console.log("url GetMessageType : ", url);
                var res = $http.get(url);
                return res;
            }
        }
    });