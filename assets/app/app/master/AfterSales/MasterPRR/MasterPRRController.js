var app = angular.module('app');
app.controller('MasterPRRController', function ($scope, $http, $filter, bsNotify, CurrentUser, MasterPRRFactory, $timeout) {
	

	$scope.optionsComboBulkAction_PRR = [{ name: "Delete", value: "Delete" }];

	$scope.optionsComboFilterGrid_PRR = [
		{ name: 'Outlet Code', value: 'OutletCode' },
		{ name: 'Outlet Name', value: 'OutletName' },
		{ name: 'PWR', value: 'PWR' },
		{ name: 'PWR_2', value: 'PWR_2' },
		{ name: 'PWR_3', value: 'PWR_3' },
		{ name: 'PWR Battery 1', value: 'PWR_BATTERY_1' },
		{ name: 'PWR Battery 2', value: 'PWRBattery2' },
		{ name: 'PWR Battery 3', value: 'PWRBattery3' },
		{ name: 'Effective Date From', value: 'EffectiveDateFrom'},
		{ name: 'Effective Date To', value: 'EffectiveDateTo'},


	];

	$scope.isDisable = true;
	$scope.btnUpload = "hide";
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'MasterPRRWarranty';
	angular.element('#PRRModal').modal('hide');
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.cekKolom = '';
	$scope.ShowFieldGenerate_PRR = true;
	$scope.FilterPrr = {};

	$scope.grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'OutletCode', displayName: 'OutletCode', field: 'OutletCode' },
			{ width: '25%', enableCellEdit: false, enableHiding: false, name: 'OutletName', displayName: 'OutletName', field: 'OutletName', },
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'PWR', displayName: 'PWR', field: 'PWR' },
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'PWR_2', displayName: 'PWR_2', field: 'PWR_2' },
			{ width: '10%', enableCellEdit: false, enableHiding: false, name: 'PWR_3', displayName: 'PWR_3', field: 'PWR_3' },
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'PWR_BATTERY_1', displayName: 'PWR_BATTERY_1', field: 'PWR_BATTERY_1' },
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'PWR_BATTERY_2', displayName: 'PWR_BATTERY_2', field: 'PWR_BATTERY_2' },
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'PWR_BATTERY_3', displayName: 'PWR_BATTERY_3', field: 'PWR_BATTERY_3' },
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'Effective Date From', displayName: 'Effective Date From',cellFilter: 'date:\"dd-MM-yyyy\"', field: 'DateFrom' ,visible: true},
			{ width: '15%', enableCellEdit: false, enableHiding: false, name: 'Effective Date To', displayName: 'Effective Date To', cellFilter: 'date:\"dd-MM-yyyy\"', field: 'DateTo', visible: true },
			{ width: '25%', enableCellEdit: false, enableHiding: false, name: 'Remarks', displayName: 'Remarks', field: 'Remarks' },
		],

		onRegisterApi: function (gridApi) {
			$scope.gridAPI = gridApi;
		}
	};



	//New Galang
	$scope.selectedGridFilter = '';
	$scope.onFilterGridSelected = function (selectedGridFilter) {
		console.log('selectedGridFilter ===>',selectedGridFilter);
		$scope.selectedGridFilter = selectedGridFilter;
	}
	//End New Galang

	$scope.FilterGridMasterPRR = function (textFilterMasterPRR,selectedGridFilter) {
		var inputfilter = textFilterMasterPRR;
		console.log('$scope.textFilterMasterPRR ===>',textFilterMasterPRR);

		var tempGrid = angular.copy($scope.grid.dataTemp);
		var objct = '{"' + selectedGridFilter + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.grid.data = $scope.grid.dataTemp;
			console.log('$scope.grid.data if ===>', $scope.grid.data)

		} else {
			$scope.grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.grid.data else ===>', $scope.grid.data)
		}
	};


	$scope.fixDate = function (date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() + "-" +
				('0' + (date.getMonth() + 1)).slice(-2) + "-" +
				('0' + date.getDate()).slice(-2)
			return fix;
		} else {
			return null;
		}
	};

	$scope.fixDateGapakeSparatoryyyymmdd = function (date) {
		// yyyy-mm-dd ====> yyyymmdd
		date = date.split('-');
		var fix = date[0] + '/' + date[1] + '/' + date[2];
		return fix;
	};

	$scope.MasterSelling_PRR_Generate_Clicked = function () {

		$scope.isDisable = true;
		var DateFrom;
		var DateTo;
		var namaCabang;
		if($scope.FilterPrr.TanggalFilterStart == null || $scope.FilterPrr.TanggalFilterStart == undefined || $scope.FilterPrr.TanggalFilterStart == ""){
			DateFrom = "2001-01-01";
		}else{
			DateFrom = $scope.fixDate($scope.FilterPrr.TanggalFilterStart);
		}
		if($scope.FilterPrr.TanggalFilterEnd == null || $scope.FilterPrr.TanggalFilterEnd == undefined || $scope.FilterPrr.TanggalFilterEnd == "" ){
			DateTo = "9999-09-09";
		}else{
			DateTo = $scope.fixDate($scope.FilterPrr.TanggalFilterEnd);
		}

		if($scope.FilterPrr.OutletCode == "" || $scope.FilterPrr.OutletCode == null || $scope.FilterPrr.OutletCode == undefined ){
			namaCabang= "-";
		}else{
			namaCabang = $scope.FilterPrr.OutletCode;
		}



		console.log('OutletCode ===>', $scope.FilterPrr.OutletCode);
		console.log('date_from ===>', DateFrom);
		console.log('date_to ===>', DateTo);
		console.log('FilterPrr ===>', $scope.FilterPrr);

		var filter = "&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "&NamaCabang=" + namaCabang;


		MasterPRRFactory.getData(1, 10000, filter)
			// MasterPRRFactory.getData(1, 10000, UrlParameter)
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;            //Data hasil dari WebAPI

					for (var i in $scope.grid.data) {
						if($scope.grid.data[i].StatusCode == 1) { 
							$scope.grid.data[i].DateFrom = $scope.fixDate($scope.grid.data[i].DateFrom);
							$scope.grid.data[i].DateTo = $scope.fixDate($scope.grid.data[i].DateTo);
						}
					}
					console.log("coba adib",$scope.grid.data[i].StatusCode)
					$scope.grid.totalItems = res.data.Total;
					$scope.grid.dataTemp = angular.copy($scope.grid.data);

				},
				function (err) {
					bsNotify.show(
						{
							type: 'danger',
							title: "Data gagal disimpan!",
							content: err.data.Message,
						}
					);
				}
			);
	}

	$scope.changeFormatDate = function(item) {
		var tmpDate = item;
		console.log("changeFormatDate item", item);
		tmpDate = new Date(tmpDate);
		var finalDate
		var yyyy = tmpDate.getFullYear().toString();
		var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
		var dd = tmpDate.getDate().toString();
		finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function (ExcelFile) {
		console.log('ExcelFile ===>',ExcelFile);
		$scope.isDisable = false;
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element(document.querySelector('#uploadSellingFile_PRR')); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);
		console.log("myEl filename ====>", myEl.value);	



		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			exTmp = json
			for (var y = 0; y < exTmp.length; y++) {
                var formatKey = Object.keys(exTmp[y]);
                var tmpReformatKey = angular.copy(formatKey);
                console.log("formatkey", formatKey)
                if (exTmp[y].OutletCode != null && exTmp[y].OutletCode != "" && exTmp[y].OutletCode != undefined) {
                    console.log("extmp", exTmp[y], json);
                    $scope.MasterSelling_ExcelData.push(exTmp[y]);
                }
			}
			console.log('data asli yang di upload ===>', json.EffectiveDateFrom, json.EffectiveDateTo);
			for (i = 0; i < json.length; i++) {
				var tglUpload = json[i].EffectiveDateFrom //format 20191231
				// var thn = tglUpload.substring(0, 4);
				// var bln = tglUpload.substring(4, 7);
				// var tgl = tglUpload.substring(7, 9);
				// var tgl_jadi = tgl + "-" + bln + "-" + thn;
				json[i].EffectiveDateFrom = $scope.changeFormatDate(tglUpload);
				console.log("tgl awal >>>>",json[i].EffectiveDateFrom)
				

				var tglUploadTo = json[i].EffectiveDateTo //format 20191231
				// var thnTo = tglUploadTo.substring(0, 4);
				// var blnTo = tglUploadTo.substring(4, 6);
				// var tglTo = tglUploadTo.substring(6, 8);
				// var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
				json[i].EffectiveDateTo = $scope.changeFormatDate(tglUploadTo);
				console.log("tgl akhir> >>>", json[i].EffectiveDateTo)
				
			}

			$scope.btnUpload = "hide";
			console.log("myEl : ", myEl);
			console.log("json : ", json);
			// $scope.MasterSelling_ExcelData = json;
			$scope.MasterSelling_ExcelData.splice(0,1);
			$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
			$scope.MasterSelling_PRR_Upload_Clicked();
			// myEl.val('');
		});
		
	}

	

	$scope.validateColumn = function (inputExcelData) {
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.OutletCode)) {
			$scope.cekKolom = $scope.cekKolom + ' OutletCode \n';
		}

		// if (angular.isUndefined(inputExcelData.OutletName)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' OutletName \n';
		// }

		if (angular.isUndefined(inputExcelData.PWR)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR \n';
		}

		if (angular.isUndefined(inputExcelData.PWR_2)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR_2 \n';
		}

		if (angular.isUndefined(inputExcelData.PWR_3)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR_3 \n';
		}

		if (angular.isUndefined(inputExcelData.PWR_BATTERY_1)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR_BATTERY_1 \n';
		}

		if (angular.isUndefined(inputExcelData.PWR_BATTERY_2)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR_BATTERY_2 \n';
		}

		if (angular.isUndefined(inputExcelData.PWR_BATTERY_3)) {
			$scope.cekKolom = $scope.cekKolom + ' PWR_BATTERY_3 \n';
		}

		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' DateFrom \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' DateTo \n';
		}
		// if (angular.isUndefined(inputExcelData.Remarks)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		// }

		if (
			angular.isUndefined(inputExcelData.OutletCode) || 
			// angular.isUndefined(inputExcelData.OutletName) ||
			angular.isUndefined(inputExcelData.PWR) ||
			angular.isUndefined(inputExcelData.PWR_2) ||
			angular.isUndefined(inputExcelData.PWR_3) ||
			angular.isUndefined(inputExcelData.PWR_BATTERY_1) ||
			angular.isUndefined(inputExcelData.PWR_BATTERY_2) ||
			angular.isUndefined(inputExcelData.PWR_BATTERY_3) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) ||
			angular.isUndefined(inputExcelData.EffectiveDateTo) 
			// angular.isUndefined(inputExcelData.Remarks)
			) {
			return (false);
		}

		return (true);
	}

	$scope.ComboBulkAction_PRR_Changed = function () {
		if ($scope.ComboBulkAction_PRR == "Delete") {
			//var index;
			// $scope.gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.grid.data.indexOf(row.entity);
			// 	$scope.grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.grid.data.splice($scope.grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				angular.element('#PRRModal').modal('show');
			}
		}
	}


	$scope.MasterSelling_PRR_Download_Clicked = function () {
		console.log('$scope.grid.data ===>',$scope.grid.data);
		var excelData = [];
		var fileName = "";
		fileName = "MasterPRRWarranty";

		if ($scope.grid.data.length == 0) {
			excelData.push({
				OutletCode: "TUTXXXXXX",
				OutletName: "Nama Cabang",
				PWR: "000",
				PWR_2: "000",
				PWR_3: "000",
				PWR_BATTERY_1: "000",
				PWR_BATTERY_2: "000",
				PWR_BATTERY_3: "000",
				EffectiveDateFrom: "'yyyy/mm/dd",
				EffectiveDateTo: "'yyyy/mm/dd",
				Remarks: "Jangan Hapus Baris Template Ini"
			});
			fileName = "MasterPRRTemplate";
		}
		else {

			var excelDownloadData = angular.copy($scope.grid.data);

			



			for (i = 0; i < excelDownloadData.length; i++) {
				excelDownloadData[i].DateFrom = $scope.fixDateGapakeSparatoryyyymmdd(excelDownloadData[i].DateFrom);
				excelDownloadData[i].DateTo = $scope.fixDateGapakeSparatoryyyymmdd(excelDownloadData[i].DateTo);
			}



			for (var i = 0; i < excelDownloadData.length; i++) {

				if (excelDownloadData[i].OutletCode == null) {
					excelDownloadData[i].OutletCode = " ";
				}
				if (excelDownloadData[i].OutletName == null) {
					excelDownloadData[i].OutletName = " ";
				}
				if (excelDownloadData[i].PWR == null) {
					excelDownloadData[i].PWR = " ";
				}
				if (excelDownloadData[i].PWR_2 == null) {
					excelDownloadData[i].PWR_2 = " ";
				}
				if (excelDownloadData[i].PWR_3 == null) {
					excelDownloadData[i].PWR_3 = " ";
				}
				if (excelDownloadData[i].PWR_BATTERY_1 == null) {
					excelDownloadData[i].PWR_BATTERY_1 = " ";
				}
				if (excelDownloadData[i].PWR_BATTERY_2 == null) {
					excelDownloadData[i].PWR_BATTERY_2 = " ";
				}
				if (excelDownloadData[i].PWR_BATTERY_3 == null) {
					excelDownloadData[i].PWR_BATTERY_3 = " ";
				}
				if (excelDownloadData[i].EffectiveDateFrom == null) {
					excelDownloadData[i].EffectiveDateFrom = " ";
				}
				if (excelDownloadData[i].EffectiveDateTo == null) {
					excelDownloadData[i].EffectiveDateTo = " ";
				}
				if (excelDownloadData[i].Remarks == null) {
					excelDownloadData[i].Remarks = " ";
				}
			}

			excelDownloadData.unshift({
				OutletCode: "TUTXXXXXX",
				OutletName: "Nama Cabang",
				PWR: "000",
				PWR_2: "000",
				PWR_3: "000",
				PWR_BATTERY_1: "000",
				PWR_BATTERY_2: "000",
				PWR_BATTERY_3: "000",
				DateFrom: "'yyyy/mm/dd",
				DateTo: "'yyyy/mm/dd",
				Remarks: "Jangan Hapus Baris Template Ini"
			});

			excelDownloadData.forEach(function (row) {
				excelData.push({
					OutletCode       : row.OutletCode,
					OutletName       : row.OutletName,
					PWR              : row.PWR,
					PWR_2            : row.PWR_2,
					PWR_3            : row.PWR_3,
					PWR_BATTERY_1    : row.PWR_BATTERY_1,
					PWR_BATTERY_2    : row.PWR_BATTERY_2,
					PWR_BATTERY_3    : row.PWR_BATTERY_3,
					EffectiveDateFrom: row.DateFrom,
					EffectiveDateTo  : row.DateTo,
					Remarks          : row.Remarks
				});
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.MasterSelling_PRR_Upload_Clicked = function () {
		if ($scope.MasterSelling_ExcelData.length == 0) {
			alert("file excel kosong !");
			return;
		}

		if (!$scope.validateColumn($scope.MasterSelling_ExcelData[0])) {
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}


		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);

		for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
			var DateStart = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom
			var to_submit = DateStart
			$scope.MasterSelling_ExcelData[i].DateFrom = to_submit;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

			var DateEnd = $scope.MasterSelling_ExcelData[i].EffectiveDateTo
			var to_submit2 = DateEnd
			$scope.MasterSelling_ExcelData[i].DateTo = to_submit2;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);

			delete $scope.MasterSelling_ExcelData[i]['EffectiveDateFrom'];
			delete $scope.MasterSelling_ExcelData[i]['EffectiveDateTo'];
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.gridAPI.grid.clearAllFilters();
		MasterPRRFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				$scope.grid.data = res.data.Result;			//Data hasil dari WebAPI

				for(i in $scope.grid.data){
					$scope.grid.data[i].DateFrom = $scope.fixDate($scope.grid.data[i].DateFrom);
					$scope.grid.data[i].DateTo = $scope.fixDate($scope.grid.data[i].DateTo);
					$scope.grid.data[i].CreatedUserDate = $scope.fixDate(new Date());
					$scope.grid.data[i].CreatedUserId = null;
					$scope.grid.data[i].LastModifiedUserId = null;
					$scope.grid.data[i].LastModifiedUserDate =$scope.fixDate(new Date());
				}

				var dataValid = undefined;
				for (i = 0; i < $scope.grid.data.length; i++) {
					if ($scope.grid.data[i].Remarks != "") {
						dataValid = false;
					}
				}

				if (dataValid == false) {
					bsNotify.show(
						{
							type: 'warning',
							title: "Data tidak valid!",
							content: "Cek detail kesalahan di kolom Remarks pada tabel",
						}
					);
				}


				$scope.grid.totalItems = res.data.Total;
			},
			function (err) {
				bsNotify.show(
					{
						type: 'danger',
						title: "Data gagal di Verify",
						content: "Format data salah, Isi data sesuai format pada Template yang sudah diberikan"
					}
				);
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked = function () {
		console.log('sebelum data masuk [grid] ===>',$scope.grid.data);
		var isSave = true;
		
		for(i in $scope.grid.data){
			if($scope.grid.data[i].Remarks.length > 0 ){
				isSave = false;
				console.log('ga aman ===>',$scope.grid.data[i].Remarks);
			}else{
				console.log('aman ===>',$scope.grid.data[i].Remarks );
			}
		}
		
		var Grid;
		Grid = JSON.stringify($scope.grid.data);
		if(isSave == true){
			MasterPRRFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
				function (res) {
					bsNotify.show({
						title: "Customer Data",
						content: "Data berhasil di simpan",
						type: 'success'
					});
					$scope.MasterSelling_PRR_Generate_Clicked();
				},
				function (err) {
					bsNotify.show(
						{
							type: 'danger',
							title: "Data gagal disimpan!",
							content: err.data.Message,
						}
					);
				}
			);
		}else{
			bsNotify.show(
				{
					type: 'warning',
					title: "Data Tidak Valid",
					content: 'Cek kolom remarks pada tabel untuk detail data yang tidak valid',
				}
			);
		}


	}

	$scope.MasterSelling_Batal_Clicked = function () {
		$scope.grid.data = [];
		$scope.gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_PRR = "";
		$scope.TextFilterSatu_PRR = "";

		var myEl = angular.element(document.querySelector('#uploadSellingFile_PRR'));
		myEl.val('');
	}



	$scope.MasterSelling_PRR_Cari_Clicked = function () {
		var value = $scope.TextFilterGrid_PRR;
		$scope.gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_PRR != "") {
			$scope.gridAPI.grid.getColumn($scope.ComboFilterGrid_PRR).filters[0].term = value;
		}
		// else {
		// 	$scope.gridAPI.grid.clearAllFilters();

	}

});