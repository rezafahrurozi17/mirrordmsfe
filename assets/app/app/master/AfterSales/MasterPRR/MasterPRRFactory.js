angular.module('app')
	.factory('MasterPRRFactory', function ($http, CurrentUser) {
 var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
		
	
	return {
		VerifyData : function (KlasifikasiPE, IsiGrid, TypePE) {
			
			//var url = '/api/fe/IncomingInstruction';
			var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
			var url = '/api/as/AfterSalesMasterPRRWarranty/Verify';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		},

		getData : function (start, limit, Url) {
			var url = '/api/as/AfterSalesMasterPRRWarranty/Get?start=' + start + '&limit=' + limit + Url;
			var res = $http.get(url);
			return res;
		},

		Submit : function (KlasifikasiPE, IsiGrid, TypePE) {
			// api/as/AfterSalesMasterPRRWarranty/SaveData
			var url = '/api/as/AfterSalesMasterPRRWarranty/SaveData/';
			var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		},

		getDaVehicleType: function (param) {
			var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
			return res;
		},

		getDataModel: function () {
			var res = $http.get('/api/sales/MUnitVehicleModelTomas');
			return res;
		},



	}
});