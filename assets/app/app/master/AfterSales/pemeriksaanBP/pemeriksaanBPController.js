var app = angular.module('app');
app.controller('PemeriksaanBPController', function ($scope, $http, $filter, CurrentUser, PemeriksaanBPFactory, $timeout) {
	var user = CurrentUser.user();
	
	$scope.uiGridPageSize = 10;
	$scope.PemeriksaanBPMain_Show = true;
	
	$scope.PemeriksaanBPGrid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: false,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'id', field: 'CheckId', visible: false },
			{ name: 'Nama Penjelasan', field: 'CheckDesc'},
			{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.PemeriksaanBPEditRow(row)" class="fa fa-pencil fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" title="Edit"/a>'},
		],
		onRegisterApi: function(gridApi) {
			$scope.GridAPIPemeriksaanBPGrid = gridApi;
		}
	};
	
	$scope.PemeriksaanBPMain_GetAllData = function(){
		PemeriksaanBPFactory.getData()
		.then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				$scope.PemeriksaanBPGrid.data = res.data.Result;
				$scope.PemeriksaanBPGrid.totalItems = res.data.Total;
				$scope.PemeriksaanBPGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.PemeriksaanBPGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
		);
	}
	
	$scope.PemeriksaanBPMain_GetAllData();
	
	$scope.PemeriksaanBPEditRow = function(row){
		$scope.PemeriksaanBPEdit_CheckId = row.entity.CheckId;
		$scope.PemeriksaanBPEdit_Detail = row.entity.CheckDesc;
		$scope.PemeriksaanBPMain_Show = false;
		$scope.PemeriksaanBPEdit_Show = true;
	}
	
	$scope.PemeriksaanBP_Edit_Batal_Clicked = function(){
		$scope.PemeriksaanBPMain_Show = true;
		$scope.PemeriksaanBPEdit_Show = false;
	}
	
	$scope.PemeriksaanBP_Edit_Simpan_Clicked = function(){
		var inputData = [
		{
			"CheckId":$scope.PemeriksaanBPEdit_CheckId,
			"CheckDesc": $scope.PemeriksaanBPEdit_Detail,
		}];
		
		PemeriksaanBPFactory.create(inputData)
		.then(
			function(res){
				alert("Data berhasil disimpan");
				$scope.PemeriksaanBPMain_GetAllData();
				$scope.PemeriksaanBP_Edit_Batal_Clicked();
			},
			function(err){
				alert(err.data.ExceptionMessage);
			}
		);
	}
});