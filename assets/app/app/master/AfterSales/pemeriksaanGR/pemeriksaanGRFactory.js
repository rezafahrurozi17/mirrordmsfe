angular.module('app')
	.factory('PemeriksaanGRFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
	return{
		getData: function(){
			var url = '/api/as/AfterSalesCheck/GetCheckData/?TypeId=1';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(PemeriksaanGRData){
			var url = '/api/as/AfterSalesCheck/Update/';
			var param = JSON.stringify(PemeriksaanGRData);
			console.log("object input saveData", param);
			var res=$http.put(url, param);
			return res;
		},
	}
});