var app = angular.module('app');
app.controller('PemeriksaanGRController', function ($scope, $http, $filter, CurrentUser, PemeriksaanGRFactory, $timeout) {
	var user = CurrentUser.user();
	
	$scope.uiGridPageSize = 10;
	$scope.PemeriksaanGRMain_Show = true;
	
	$scope.PemeriksaanGRGrid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: false,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'id', field: 'CheckId', visible: false },
			{ name: 'Nama Penjelasan', field: 'CheckDesc'},
			{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.PemeriksaanGREditRow(row)" class="fa fa-pencil fa-fw fa-lg" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" title="Edit"/a>'},
		],
		onRegisterApi: function(gridApi) {
			$scope.GridAPIPemeriksaanGRGrid = gridApi;
		}
	};
	
	$scope.PemeriksaanGRMain_GetAllData = function(){
		PemeriksaanGRFactory.getData()
		.then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				$scope.PemeriksaanGRGrid.data = res.data.Result;
				$scope.PemeriksaanGRGrid.totalItems = res.data.Total;
				$scope.PemeriksaanGRGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.PemeriksaanGRGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
		);
	}
	
	$scope.PemeriksaanGRMain_GetAllData();
	
	$scope.PemeriksaanGREditRow = function(row){
		$scope.PemeriksaanGREdit_CheckId = row.entity.CheckId;
		$scope.PemeriksaanGREdit_Detail = row.entity.CheckDesc;
		$scope.PemeriksaanGRMain_Show = false;
		$scope.PemeriksaanGREdit_Show = true;
	}
	
	$scope.PemeriksaanGR_Edit_Batal_Clicked = function(){
		$scope.PemeriksaanGRMain_Show = true;
		$scope.PemeriksaanGREdit_Show = false;
	}
	
	$scope.PemeriksaanGR_Edit_Simpan_Clicked = function(){
		var inputData = [
		{
			"CheckId":$scope.PemeriksaanGREdit_CheckId,
			"CheckDesc": $scope.PemeriksaanGREdit_Detail,
		}];
		
		PemeriksaanGRFactory.create(inputData)
		.then(
			function(res){
				alert("Data berhasil disimpan");
				$scope.PemeriksaanGRMain_GetAllData();
				$scope.PemeriksaanGR_Edit_Batal_Clicked();
			},
			function(err){
				alert(err.data.ExceptionMessage);
			}
		);
		
	}
});