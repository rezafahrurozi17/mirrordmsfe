var app = angular.module('app');
app.controller('MasterCounterBPController', function($scope, $http, $filter, CurrentUser, MasterCounterBPFactory, $timeout,bsAlert) {
    var user = CurrentUser.user();
    console.log('user', user);
    $scope.MasterCounterBP_EmployeeList = [];

    $scope.MasterCounterBP_CounterId = 0;
    $scope.nData = [];
    $scope.CounterCat = [];
    $scope.DataSA_BP = [];
    // $scope.GetCounterCategory = function() {
    //     MasterCounterBPFactory.getCounterCategory().then(function(resu) {
    //         console.log('data CounterCategory', resu.data.Result);
    //         $scope.CounterCat = resu.data.Result;
    //         _.map($scope.CounterCat, function(value, key) {
    //             value.Checked = 0;
    //         });

    //         $scope.CounterCatTemp = angular.copy($scope.CounterCat);
    //         console.log('data CounterCatTemp', $scope.CounterCatTemp);
    //     });
    // };

    // $scope.checkCategory = function(index, item) {
    //     console.log('data checkCategory', index, item);
    //     console.log('data checkCategory $scope.nData', $scope.nData);
    //     _.map($scope.nData, function(value, key) {
    //         if (key == index[0]) {
    //             value.QCategoryId = item;
    //         }
    //     });
    //     console.log('data checkCategory $scope.nData after', $scope.nData);
    // }

    $scope.checkCategory = function(item) {
        console.log('data checkCategory', item);
        console.log('data checkCategory $scope.nData after', $scope.CounterCatTemp);


    }

    $scope.MasterCounterBPMain_Start = function() {
        $scope.Show_MasterCounterBP_Main = true;
        $scope.Show_MasterCounterBP_Tambah = false;
    }

    $scope.MasterCounterBPMain_Tambah_Clicked = function() {
        $scope.masterCounterBP_StatusCode = 1;
        $scope.Show_MasterCounterBP_Main = false;
        $scope.Show_MasterCounterBP_Tambah = true;
        $scope.MasterCounterBPTambah_GetEmployeeList();
        $scope.masterCounterBP_ShowCode = 1;
    }

    $scope.MasterCounterBPTambah_Batal_Clicked = function() {
        $scope.MasterCounterBPTambah_ClearFields();
        $scope.Show_MasterCounterBP_Main = true;
        $scope.Show_MasterCounterBP_Tambah = false;
    }

    $scope.MasterCounterBPTambah_GetEmployeeList = function() {
        var ComboData = [];
        MasterCounterBPFactory.getDataSA('')
            .then(
                function(res) {
                    var tmpData = angular.copy(res.data.Result);
                    var tmpDataCombo = [];
                    // var dataCombo = [];
                    console.log("tmpData", tmpData);
                    if (tmpData.length > 0) {
                        console.log("$scope.CounterBPGrid.data", $scope.CounterBPGrid.data);
                        for (var i in $scope.CounterBPGrid.data) {
                            // tmpdataCombo = _.find(tmpData,{ "EmployeeId": $scope.CounterBPGrid.data[i].EmployeeId });
                            var tmpIdx = _.findIndex(tmpData, function(val) {
                                if ($scope.CounterBPGrid.data[i].StatusCode == 1) {
                                    return $scope.CounterBPGrid.data[i].EmployeeId == val.EmployeeId
                                }
                            });
                            console.log('tmpIdxtmpIdxtmpIdx', tmpIdx, tmpData);
                            if (tmpIdx > -1) {
                                tmpData.splice(tmpIdx, 1);
                            }

                            // if(tmpdataCombo === undefined){
                            //     dataCombo.push(tmpdataCombo);
                            // }
                        }
                        console.log("hasil tmpData", tmpData);
                        // console.log("dataCombo",dataCombo);

                    }
                    // $scope.MasterCounterBP_EmployeeList = res.data.Result;
                    // angular.forEach($scope.MasterCounterBP_EmployeeList, function(value, key) {
                    //     ComboData.push({ name: value.Name, value: value.EmployeeId })
                    // });

                    $scope.optionsTambahCounterBPSAName = tmpData;
                }
            );
    }
    $scope.MasterCounterBPedit_GetEmployeeList = function(param) {
        var ComboData = [];
        MasterCounterBPFactory.getDataSA('')
            .then(
                function(res) {
                    var tmpData2 = angular.copy(res.data.Result);
                    var tmpData = res.data.Result;
                    var tmpDataCombo = [];
                    var dataCombo = [];
                    if (tmpData.length > 0) {
                        for (var i in $scope.CounterBPGrid.data) {
                            var tmpIdx = _.findIndex(tmpData, function(val) {
                                return $scope.CounterBPGrid.data[i].EmployeeId == val.EmployeeId
                            });
                            if (tmpIdx > -1) {
                                tmpData.splice(tmpIdx, 1);
                            }
                        }
                        dataCombo = tmpData;
                        for (var k in tmpData2) {
                            if (tmpData2[k].EmployeeId == param) {
                                dataCombo.push(tmpData2[k]);
                            }
                        }
                    }

                    $scope.optionsTambahCounterBPSAName = dataCombo;
                }
            );
    }
    $scope.mData = {};

    $scope.GetNameSABP = function() {
        console.log('GetNameSABP');

        MasterCounterBPFactory.getSA_BP()
            .then(
                function(res) {
                    if (res.data.Result.length > 0) {
                        $scope.DataSA_BP = res.data.Result;
                        console.log('$scope.DataSA_BP', $scope.DataSA_BP);

                    }
                }
            );
    }
    $scope.TambahCounterBP_SAName_Changed = function(item) {
        console.log("item", item);
        if (item !== undefined && item !== null) {
            $scope.mData.InitialName = item.InitialName;
            $scope.mData.SAName = item.Name;
            $scope.masterCounterBP_InitialSAName = item.InitialName;
            $scope.DataSA_BP.some(function(obj, i) {

                console.log("change SA", obj);

                if (obj.EmployeeId == $scope.TambahCounterBP_SAName)
                    $scope.masterCounterBP_InitialSAName = obj.InitialName;
            });
        };
    }

    $scope.MasterCounterBPTambah_CariSA_Clicked = function() {
        console.log('$scope.masterCounterBP_SAName', $scope.masterCounterBP_SAName);
        $scope.TambahCounterBP_EmployeeCari_DataEmployee($scope.masterCounterBP_SAName);
        angular.element('#ModalCounterBPTambahSearchEmployee').modal('show');
    }

    $scope.GetNameSABP = function() {
        console.log('GetNameSABP');

        MasterCounterBPFactory.getSA_BP()
            .then(
                function(res) {
                    if (res.data.Result.length > 0) {
                        $scope.DataSA_BP = res.data.Result;
                        console.log('$scope.DataSA_BP', $scope.DataSA_BP);

                    }
                }
            );
    }

    $scope.masterCounterBP_SAName_LostFocus = function() {
        var OldEmployeeId = $scope.masterCounterBP_EmployeeId;
        var OldEmployeeSAName = $scope.masterCounterBP_InitialSAName;
        MasterCounterBPFactory.getDataEmployee($scope.masterCounterBP_SAName)
            .then(
                function(res) {
                    if (res.data.Result.length > 0) {
                        $scope.masterCounterBP_InitialSAName = res.data.Result[0].Initial;
                        $scope.masterCounterBP_EmployeeId = res.data.Result[0].EmployeeId;
                    } else {
                        alert("Data karyawan " + $scope.MasterCounterBP_SAName + " tidak ditemukan");
                        $scope.masterCounterBP_InitialSAName = OldEmployeeSAName;
                        $scope.masterCounterBP_EmployeeId = OldEmployeeId;
                    }
                }
            );
    }

    $scope.MasterCounterBPTambah_Update_Clicked = function() {
        // billlllllllllllllllllllllll
        var finalCategoryCreate = [];
        for (var i=0; i<$scope.CounterCatTemp.length; i++){
            if ($scope.CounterCatTemp[i].Name == 'Booking'){
                if ($scope.masterCounterBP_isBooking == true){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                } else {
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 0;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.CounterCatTemp[i].Name == 'Walk In'){
                if ($scope.masterCounterBP_isWalkIn == true){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                } else {
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 0;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.CounterCatTemp[i].Name == 'Others'){
                if ($scope.masterCounterBP_isOther == true){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                } else {
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 0;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.CounterCatTemp[i].Name == 'Pengambilan'){
                if ($scope.masterCounterBP_isTakeOut == true){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                } else {
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    $scope.CounterCatTemp[i].Checked = 0;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            
        }
        var countError = 0
        if(($scope.masterCounterBP_isBooking == false || $scope.masterCounterBP_isBooking == undefined)&&
            ($scope.masterCounterBP_isWalkIn == false || $scope.masterCounterBP_isWalkIn == undefined)&&
            ($scope.masterCounterBP_isOther == false || $scope.masterCounterBP_isOther == undefined)&&
            ($scope.masterCounterBP_isTakeOut == false || $scope.masterCounterBP_isTakeOut == undefined) && 
            $scope.masterCounterBP_StatusCode == 1){
            countError++
        }
        var CounterData = {
            CounterId: $scope.MasterCounterBP_CounterId,
            CounterName: $scope.masterCounterBP_CounterName,
            EmployeeId: $scope.TambahCounterBP_SAName, //$scope.masterCounterBP_EmployeeId,
            SAName: $scope.mData.SAName, //$scope.masterCounterBP_EmployeeId,
            // InitialSAName: $scope.mData.InitialName, //$scope.masterCounterBP_EmployeeId,
            InitialSAName: $scope.masterCounterBP_InitialSAName, //$scope.masterCounterBP_EmployeeId,
            OutletId: user.OutletId,
            CounterType: "BP",
            // CounterCategory: $scope.CounterCatTemp,
            CounterCategory: finalCategoryCreate,
            // isBooking: $scope.masterCounterBP_isBooking,
            // isBookingOnTime: $scope.masterCounterBP_isBookingOnTime,
            // isWalkIn: $scope.masterCounterBP_isWalkIn,
            // isOther: $scope.masterCounterBP_isOther,
            // isTakeOut: $scope.masterCounterBP_isTakeOut,
            StatusCode: $scope.masterCounterBP_StatusCode,
            isShowBoard: $scope.masterCounterBP_ShowCode,
            isGR: 0
        };
        if(countError > 0){
            bsAlert.warning("Kategori Tidak Boleh Kosong")
            return false;
        }
        MasterCounterBPFactory.update(CounterData).then(function(resu) {
                angular.element('#ModalHapusMasterCounterBP').modal('hide');
                alert("Data berhasil diubah");
                $scope.CounterBPGrid_Paging(1);
                $scope.MasterCounterBPTambah_Batal_Clicked();
            },
            function(err) {
                alert("Data gagal disimpan");
            });
    }

    $scope.MasterCounterBPTambah_Simpan_Clicked = function() {
        console.log("employee id Pada saat save : ", $scope.masterCounterBP_EmployeeId);
        console.log("$scope.TambahCounterBP_SAName", $scope.TambahCounterBP_SAName);
        console.log("$scope.optionsTambahCounterBPSAName", $scope.optionsTambahCounterBPSAName);
        console.log('checkbox masterCounterBP_isBooking', $scope.masterCounterBP_isBooking)
        console.log('checkbox masterCounterBP_isBooking', $scope.masterCounterBP_isBookingOnTime)
        console.log('checkbox masterCounterBP_isBooking', $scope.masterCounterBP_isWalkIn)
        console.log('checkbox masterCounterBP_isBooking', $scope.masterCounterBP_isOther)
        console.log('checkbox masterCounterBP_isBooking', $scope.masterCounterBP_isTakeOut)


        // var indexSAName = $scope.optionsTambahCounterBPSAName.findIndex(function(obj) {
        //     console.log("$scope.TambahCounterBP_SAName", $scope.TambahCounterBP_SAName);
        //     return obj.value == $scope.TambahCounterBP_SAName;
        // })
        // console.log("indexSAName", indexSAName);
        // if (indexSAName != -1) {
        //     $scope.SAName = $scope.optionsTambahCounterBPSAName[indexSAName].name;
        // } else if (indexSAName == -1) {
        //     $scope.SAName = null;
        //     $scope.TambahCounterBP_SAName = null;
        //     $scope.masterCounterBP_InitialSAName = null;
        // };

        // 25	2000280	Booking
        // 26	2000280	Walk In
        // 27	2000280	Others
        // 28	2000280	WI BP Take
        // 29	2000280	Booking On Time
        var finalCategoryCreate = [];
        for (var i=0; i<$scope.CounterCatTemp.length; i++){
            if ($scope.masterCounterBP_isBooking == true){
                if ($scope.CounterCatTemp[i].Name == 'Booking'){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.masterCounterBP_isWalkIn == true){
                if ($scope.CounterCatTemp[i].Name == 'Walk In'){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.masterCounterBP_isOther == true){
                if ($scope.CounterCatTemp[i].Name == 'Others'){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
            if ($scope.masterCounterBP_isTakeOut == true){
                if ($scope.CounterCatTemp[i].Name == 'Pengambilan'){
                    $scope.CounterCatTemp[i].StatusCode = 1;
                    finalCategoryCreate.push($scope.CounterCatTemp[i]);
                }
            }
        }

        var CounterData = {
            CounterId: $scope.MasterCounterBP_CounterId,
            CounterName: $scope.masterCounterBP_CounterName,
            EmployeeId: $scope.TambahCounterBP_SAName, //$scope.masterCounterBP_EmployeeId,
            SAName: $scope.mData.SAName, //$scope.masterCounterBP_EmployeeId,
            InitialSAName: $scope.mData.InitialName, //$scope.masterCounterBP_EmployeeId,
            OutletId: user.OutletId,
            CounterType: "BP",
            // CounterCategory: $scope.CounterCatTemp,
            CounterCategory: finalCategoryCreate,
            // isBooking: $scope.masterCounterBP_isBooking,
            // isBookingOnTime: $scope.masterCounterBP_isBookingOnTime,
            // isWalkIn: $scope.masterCounterBP_isWalkIn,
            // isOther: $scope.masterCounterBP_isOther,
            // isTakeOut: $scope.masterCounterBP_isTakeOut,
            isBooking: true,
            isWalkIn: true,
            isOther: true,
            isTakeOut: true,
            StatusCode: $scope.masterCounterBP_StatusCode,
            isShowBoard: $scope.masterCounterBP_ShowCode,
            isGR: 0
        };

        console.log("Data di-save : ", CounterData);
        if(CounterData.CounterCategory == '' || CounterData.CounterCategory == 0 || CounterData.CounterCategory == undefined){
            bsAlert.warning("Isi Kategori Terlebih dahulu")
            return false;
        }
        MasterCounterBPFactory.create(CounterData)
            .then(
                function(res) {
                    angular.element('#ModalHapusMasterCounterBP').modal('hide');
                    alert("Data berhasil disimpan");
                    $scope.CounterBPGrid_Paging(1);
                    $scope.MasterCounterBPTambah_Batal_Clicked();
                },
                function(err) {
                    alert("Data gagal disimpan");
                }
            );
    }

    $scope.MasterCounterBPMainDeleteRow = function(row) {
        $scope.MainCounterBPHapus_CounterName = row.entity.CounterName;
        $scope.MasterCounterBP_CounterId = row.entity.CounterId;
        angular.element('#ModalHapusMasterCounterBP').modal('show');
    }

    $scope.Hapus_MainCounterBP = function() {
        MasterCounterBPFactory.deleteData($scope.MasterCounterBP_CounterId)
            .then(
                function(res) {
                    alert("Data berhasil dihapus");
                    $scope.Batal_MainCounterBP();
                },
                function(err) {
                    alert("Data gagal dihapus");
                    console.log(err);
                }
            );

    }

    $scope.Batal_MainCounterBP = function() {
        console.log("masuk batal modal");
        $scope.MainCounterBPHapus_CounterName = "";
        $scope.MasterCounterBP_CounterId = 0;
        angular.element('#ModalHapusMasterCounterBP').modal('hide');
        $scope.CounterBPGrid_Paging(1);
    }

    $scope.MasterCounterBPMain_Start();

    $scope.MasterCounterBPTambah_ClearFields = function() {
        $scope.MasterCounterBP_CounterId = 0;
        $scope.masterCounterBP_EmployeeId = 0;
        $scope.masterCounterBP_CounterName = "";
        $scope.masterCounterBP_SAName = "";
        $scope.masterCounterBP_InitialSAName = "";
        $scope.masterCounterBP_isTakeOut = false;
        $scope.masterCounterBP_isOther = false;
        $scope.masterCounterBP_isWalkIn = false;
        $scope.masterCounterBP_isBooking = false;
        $scope.masterCounterBP_isBookingOnTime = false;
        $scope.masterCounterBP_isPengambilan = false;
        $scope.TambahCounterBP_SAName = undefined;
        $scope.mData = {};
        $scope.CounterCatTemp = angular.copy($scope.CounterCat);
        $scope.CounterCatTemp = $scope.CounterCatTemp.splice(3, 4);
    };

    $scope.MasterCounterBPMainEditRow = function(rowSelected) {
        console.log("rowSelected : ", rowSelected.entity);
        $scope.MasterCounterBPedit_GetEmployeeList(rowSelected.entity.EmployeeId);
        $scope.MasterCounterBP_EmployeeList.push({ name: rowSelected.entity.SAName, value: rowSelected.entity.EmployeeId });
        $scope.optionsTambahCounterBPSAName = $scope.MasterCounterBP_EmployeeList;

        $scope.MasterCounterBP_CounterId = rowSelected.entity.CounterId;
        $scope.masterCounterBP_CounterName = rowSelected.entity.CounterName;
        //$scope.masterCounterBP_SAName = rowSelected.entity.SAName;
        $scope.TambahCounterBP_SAName = rowSelected.entity.EmployeeId;
        $scope.MasterCounterBP_SANameCurrent = $scope.masterCounterBP_SAName;
        $scope.masterCounterBP_InitialSAName = rowSelected.entity.SAInitial;
        $scope.mData.InitialName = rowSelected.entity.SAInitial;
        $scope.mData.SAName = rowSelected.entity.SAName;
        $scope.masterCounterBP_isTakeOut = rowSelected.entity.isPengambilan;
        $scope.masterCounterBP_isOther = rowSelected.entity.isOther;
        $scope.masterCounterBP_isWalkIn = rowSelected.entity.isWalkIn;
        $scope.masterCounterBP_isBooking = rowSelected.entity.isBooking;
        // $scope.masterCounterBP_isBookingOnTime = rowSelected.entity.isBookingOnTime;
        $scope.masterCounterBP_EmployeeId = rowSelected.entity.EmployeeId;
        _.map($scope.CounterCatTemp, function(val) {
            // if (val.Name == "EM On Time") {
            //     if (rowSelected.entity.isEMOT) {
            //         val.Checked = 1;
            //     }
            // } else if (val.Name == "EM") {
            //     if (rowSelected.entity.isEM) {
            //         val.Checked = 1;
            //     }
            // } else if (val.Name == "Booking On Time") {
            //     if (rowSelected.entity.isBOT) {
            //         val.Checked = 1;
            //     }
            // } else
            if (val.Name == "Booking") {
                if (rowSelected.entity.isBooking) {
                    val.Checked = 1;
                }
            } else if (val.Name == "Walk In") {
                if (rowSelected.entity.isWalkIn) {
                    val.Checked = 1;
                }
            } else if (val.Name == "Others") {
                if (rowSelected.entity.isOther) {
                    val.Checked = 1;
                }
            } else if (val.Name == "Pengambilan") {
                if (rowSelected.entity.isPengambilan) {
                    val.Checked = 1;
                }
            };

        });
        console.log('rowsel', rowSelected,$scope.CounterCatTemp);
        $scope.masterCounterBP_StatusCode = rowSelected.entity.StatusCode;
        $scope.masterCounterBP_ShowCode = rowSelected.entity.isShowBoard;
        console.log('status code', $scope.masterCounterBP_StatusCode);
        console.log("SAName : ", $scope.masterCounterBP_SAName);
        console.log("Employee Id Edit : ", $scope.masterCounterBP_EmployeeId);
        $scope.Show_MasterCounterBP_Main = false;
        $scope.Show_MasterCounterBP_Tambah = true;
        if ($scope.masterCounterBP_ShowCode == null || $scope.masterCounterBP_ShowCode == undefined) {
			$scope.masterCounterBP_ShowCode = 1;
		}

    };

    $scope.uiGridPageSize = 10;
    $scope.CounterBPGrid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: true,
        enableFiltering: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
        paginationPageSize: 10,
        columnDefs: [
            { name: 'id', field: 'CounterId', visible: false },
            { name: 'EmployeeId', field: 'EmployeeId', visible: false },
            { name: 'Nama Counter', field: 'CounterName', enableCellEdit: false },
            { name: 'Nama SA', field: 'SAName', enableCellEdit: false },
            { name: 'Inisial SA', field: 'SAInitial', enableCellEdit: false },
            // { name: 'EM On Time', field: 'isEMOT', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isEMOT">', enableCellEdit: false, visible: false },
            // { name: 'EM', field: 'isEM', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isEM">', enableCellEdit: false, visible: false },
            // { name: 'Booking On Time', field: 'isBOT', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isBOT">', enableCellEdit: false, visible: false },
            { name: 'Booking', field: 'isBooking', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isBooking">', enableCellEdit: false },
            { name: 'Walk In', field: 'isWalkIn', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isWalkIn">', enableCellEdit: false },
            { name: 'Others', field: 'isOther', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isOther">', enableCellEdit: false },
            { name: 'Pengambilan', field: 'isPengambilan', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isPengambilan">', enableCellEdit: false },
            { name: 'Status', field: 'StatusCode', cellFilter: 'StatusCode' },
            { name: 'Show Board', field: 'isShowBoard', cellFilter: 'isShowBoard' },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.MasterCounterBPMainEditRow(row)"><i class="fa fa-pencil" aria-hidden="true" style="color:#777; font-size:16px; margin-left:10px; margin-top:8px; margin-right:5px" title="Ubah"></i></a>'
            },
        ],
        onRegisterApi: function(gridApi) {
            $scope.GridAPICounterBPGrid = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
            //     $scope.CounterBPGrid_Paging(pageNumber);
            // });
        }
    };
    // cellTemplate: '<a ng-click="grid.appScope.MasterCounterBPMainEditRow(row)">Edit</a>&nbsp;&nbsp;<a ng-click="grid.appScope.MasterCounterBPMainDeleteRow(row)">Delete</a>'

    $scope.CounterBPGrid_Paging = function(pageNumber) {
        MasterCounterBPFactory.getData(pageNumber, $scope.uiGridPageSize).then(function(res) {
                var data = res.data.Result;
                $scope.MasterCounterBPTambah_GetEmployeeList();
                console.log("res =>", res.data.Result);
                MasterCounterBPFactory.getCounterCategory().then(function(resu) {
                    console.log('data CounterCategory', resu.data.Result);
                    $scope.CounterCat = resu.data.Result;
                    _.map($scope.CounterCat, function(value, key) {
                        value.Checked = 0;
                    });
                    // for(var i in $scope.CounterCat){
                    //    var testIdx = _.findIndex($scope.CounterCat,function(value){
                    //        if(value.Name == "EM On Time" || value.Name == "EM" || value.Name == "Booking On Time"){
                    //            return $scope.CounterCat[i].Name == value.Name;
                    //        }
                    //    });
                    //    if(testIdx > -1){
                    //       $scope.CounterCat.splice(testIdx,1);      
                    //    }
                    // }
                    
                    $scope.CounterCatTemp = angular.copy($scope.CounterCat);
                    //$scope.CounterCatTemp = $scope.CounterCatTemp.splice(3, 4);

                    console.log('data CounterCatTemp', $scope.CounterCatTemp);
                    console.log("$scope.CounterCat",$scope.CounterCat);

                    _.map(data, function(resux) {
                        console.log("resux =>", resux);

                        _.map(resux.CounterQCategory, function(catQue) {
                            console.log("catQue =>", catQue);
                            // if (catQue.StatusCode != 0) {
                                _.map($scope.CounterCatTemp, function(nasterCat) {
                                    // console.log("nasterCat =>", nasterCat);
                                    console.log("nasterCat.QCategoryId == catQue.QCategoryId",nasterCat.QCategoryId,"==", catQue.QCategoryId);
                                    if (nasterCat.QCategoryId == catQue.QCategoryId) {
                                        // resux[nasterCat.Name]
                                        console.log("nasterCat QCategoryId =>", nasterCat);
                                        // if (nasterCat.Name == "EM On Time") {
                                        //     resux.isEMOT = true;
                                        //     nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        // } else if (nasterCat.Name == "EM") {
                                        //     resux.isEM = true;
                                        //     nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        // } else if (nasterCat.Name == "Booking On Time") {
                                        //     resux.isBOT = true;
                                        //     nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        // } else 
                                        if (nasterCat.Name == "Booking") {
                                            if(catQue.StatusCode == 1){
                                                resux.isBooking = true;
                                            }else{
                                                resux.isBooking = false;
                                            }
                                            nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        } else if (nasterCat.Name == "Walk In") {
                                            if(catQue.StatusCode == 1){
                                                resux.isWalkIn = true;
                                            }else{
                                                resux.isWalkIn = false;
                                            }
                                            nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        } else if (nasterCat.Name == "Others") {
                                            if(catQue.StatusCode == 1){
                                                resux.isOther = true;
                                            }else{
                                                resux.isOther = false;
                                            }
                                            nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        } else if (nasterCat.Name == "Pengambilan") {
                                            // resux.isPengambilan = true;
                                            if(catQue.StatusCode == 1){
                                                resux.isPengambilan = true;
                                            }else{
                                                resux.isPengambilan = false;
                                            }
                                            nasterCat.CounterQCategoryId = catQue.CounterQCategoryId;
                                        };
                                    };
                                });
                            // };
                        });
                    });
                    $scope.CounterBPGrid.data = data;
                });

                // $scope.CounterBPGrid.totalItems = res.data.Total;
                // $scope.CounterBPGrid.paginationPageSize = $scope.uiGridPageSize;
                // $scope.CounterBPGrid.paginationPageSizes = [$scope.uiGridPageSize];
            }

        );
    }

    $scope.CounterBPGrid_Paging(1);
    MasterCounterBPFactory.getDataSA().then(function(ress) {
        console.log("ress", ress);
    });

    //==============================================================================================//
    //										Search Employee Begin										//
    //==============================================================================================//

    $scope.TambahCounterBP_EmployeeCari_DataEmployee = function(EmployeeName) {
        MasterCounterBPFactory.getDataSA(EmployeeName)
            .then(
                function(res) {

                    $scope.ModalCounterBPTambahSearchEmployee_UIGrid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );
    }

    $scope.ModalCounterBPTambahSearchEmployee_Batal = function() {
        angular.element('#ModalCounterBPTambahSearchEmployee').modal('hide');
    }

    $scope.ModalCounterBPTambahSearchEmployee_UIGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10, 20],
        columnDefs: [
            { name: 'EmployeeId', field: 'EmployeeId', visible: false },
            { name: 'Nama Karyawan', field: 'Name', enableFiltering: true },
            { name: 'Inisial', field: 'InitialName' },
            {
                name: 'Action',
                enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchTambahCounterBPEmployeePilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function(gridApi) {
            $scope.grid2Api = gridApi;
        }
    };

    $scope.SearchTambahCounterBPEmployeePilihRow = function(row) {
        $scope.masterCounterBP_SAName = row.entity.Name;
        $scope.masterCounterBP_InitialSAName = row.entity.InitialName;
        $scope.masterCounterBP_EmployeeId = row.entity.EmployeeId;

        $scope.ModalCounterBPTambahSearchEmployee_Batal();
    }

    // =======Adding Custom Filter==============
    $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
    $scope.gridApiAppointment = {};
    $scope.filterColIdx = 1;
    var x = -1;
    for (var i = 0; i < $scope.CounterBPGrid.columnDefs.length; i++) {
        if ($scope.CounterBPGrid.columnDefs[i].visible == undefined && $scope.CounterBPGrid.columnDefs[i].name !== 'Action') {
            x++;
            $scope.gridCols.push($scope.CounterBPGrid.columnDefs[i]);
            $scope.gridCols[x].idx = i;
        }
        console.log("$scope.gridCols", $scope.gridCols);
        console.log("$scope.grid.columnDefs[i]", $scope.CounterBPGrid.columnDefs[i]);
    }
    $scope.filterBtnLabel = $scope.gridCols[0].name;
    $scope.filterBtnChange = function(col) {
        console.log("aku col", col);
        $scope.filterBtnLabel = col.name;
        $scope.filterColIdx = col.idx + 1;
    };
    $scope.filterBtnChange($scope.gridCols[0]);
    //==============================================================================================//
    //										Search Employee End										//
    //==============================================================================================//
    $scope.allowPatternFilter = function(event) {
        console.log("event", event);
        patternRegex = /\d|[a-z]|\s/i; //ALPHANUMERIC ONLY
        var keyCode = event.which || event.keyCode;
        var keyCodeChar = String.fromCharCode(keyCode);
        if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            event.preventDefault();
            return false;
        }
    };

}).filter('StatusCode', function() {
    return function(x) {
        if (x == 1) {
            return 'Aktif';
        } else {
            return 'Tidak Aktif';
        }
    };
}).filter('isShowBoard', function() {
    return function(x) {
        if (x == 1) {
            return 'Show';
        } else if (x == 0) {
            return 'No Show';
        } else {
            return 'Show';
        }
    };
});