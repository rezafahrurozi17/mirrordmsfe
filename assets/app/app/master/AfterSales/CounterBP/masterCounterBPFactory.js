angular.module('app')
    .factory('MasterCounterBPFactory', function($http, CurrentUser) {
        var user = CurrentUser.user();
        return {
            // getData: function(pageNumber, pageSize) {
            //     var url = '/api/as/AfterSalesMasterCounter/?start=' + pageNumber + '&limit=' + pageSize + '&CounterType=BP&outletid=' + user.OrgId; // + user.OrgId;
            //     console.log("url TaxType : ", url);
            //     var res = $http.get(url);
            //     return res;
            // },
            getData: function(pageNumber, pageSize) {
                return $http.get('/api/as/AfterSalesMasterCounter/GetAllData/BP');
            },
            // create: function(counterGRData) {
            //     var url = '/api/as/AfterSalesMasterCounter/submitdata/';
            //     var ArrayCounterGRData = [counterGRData];
            //     var param = JSON.stringify(ArrayCounterGRData);
            //     console.log("object input saveData", param);
            //     var res = $http.post(url, param);
            //     return res;
            // },
            create: function(counterGRData) {
                var url = '/api/as/AfterSalesMasterCounter/CreateNewData/';
                var ArrayCounterGRData = [counterGRData];
                if (ArrayCounterGRData[0].isBooking === true) {
                    ArrayCounterGRData[0].isBooking = 1
                } else {
                    ArrayCounterGRData[0].isBooking = 0
                }
                if (ArrayCounterGRData[0].isOther === true) {
                    ArrayCounterGRData[0].isOther = 1
                } else {
                    ArrayCounterGRData[0].isOther = 0
                }
                if (ArrayCounterGRData[0].isTakeOut === true) {
                    ArrayCounterGRData[0].isTakeOut = 1
                } else {
                    ArrayCounterGRData[0].isTakeOut = 0
                }
                if (ArrayCounterGRData[0].isWalkIn === true) {
                    ArrayCounterGRData[0].isWalkIn = 1
                } else {
                    ArrayCounterGRData[0].isWalkIn = 0
                }
                var param = JSON.stringify(ArrayCounterGRData);
                console.log("object input saveData", param);
                var res = $http.post(url, param);
                return res;
            },

            update: function(data) {
                return $http.put('/api/as/AfterSalesMasterCounter/UpdateData', [data])
            },

            getDataEmployee: function(employeeName) {
                var url = '/api/as/AfterSalesEmployeeData/?EmployeeName=' + employeeName;
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },

             getSA_BP: function() {
                var url = '/api/as/AfterSalesEmployeeData/Get_SA_BP';
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },

            getDataEmployeeNameType: function(employeeName) {
                var url = '/api/as/AfterSalesEmployeeData/GetByNameType/?EmployeeName=' + employeeName + '&TypeId=13&outletid=' + user.OrgId;
                console.log("url TaxType : ", url);
                var res = $http.get(url);
                return res;
            },
            // getSA_BP: function() { 
            //     var url = '/api/as/AfterSalesEmployeeData/Get_SA_BP'; 
            //     console.log("url TaxType : ", url); 
            //     var res = $http.get(url); 
            //     return res; 
            // }, 
            
            deleteData: function(id) {
                var url = '/api/as/AfterSalesMasterCounter/deletedata/?id=' + id;
                console.log("url delete Data : ", url);
                var res = $http.delete(url);
                return res;
            },
            getDataSA: function(){
                return $http.get('/api/as/AfterSalesEmployeeData/Get_SA_BP');
            },
            getCounterCategory: function() {
                return $http.get('/api/as/AfterSalesMasterCounter/GetCounterCategory');
            }
        }
    });