var app = angular.module('app');
app.controller('JawabanFIRController', function ($scope, $http, $filter, CurrentUser, JawabanFIRFactory, $timeout, bsNotify, bsAlert) {
	var user = CurrentUser.user();
	$scope.optionsJawabanFIRMain_BulkAction = [{name:"hapus", value:"hapus"}];

	$scope.JawabanFIRMain_GetQuestionData = function(){
		JawabanFIRFactory.getDataQuestions()
		.then(
			function(res){
				var QuestionsList = [];
				angular.forEach(res.data.Result, function(value,key){
					QuestionsList.push({name:value.Description, value:{QuestionId:value.QuestionId, Description:value.Description, FlagEdit:value.FlagEdit}});
				});
				
				$scope.optionsJawabanFIRMain_DaftarPertanyaan = QuestionsList;
			}
		);
	}
	$scope.GridAPIJawabanFIRGrid = {};
	$scope.JawabanFIRMain_Start = function(){
		$scope.JawabanFIRMain_GetQuestionData();
		$scope.Show_JawabanFIR_Main = true;
		$scope.Show_JawabanFIR_Tambah = false;
	}
	
	$scope.JawabanFIRMain_Start();
	
	$scope.JawabanFIRMain_Tambah_Clicked = function(){
		if(angular.isUndefined($scope.JawabanFIRMain_DaftarPertanyaan)){
			alert("Pertanyaan FIR wajib dipilih");
			return;
		}
		$scope.JawabanFIRTambah_JawabanFIRId = 0;
		$scope.JawabanFIRTambah_PertanyaanFIR = $scope.JawabanFIRMain_DaftarPertanyaan.Description;
		$scope.JawabanFIRTambah_PertanyaanFIRId = $scope.JawabanFIRMain_DaftarPertanyaan.QuestionId;
		$scope.Show_JawabanFIR_Main = false;
		$scope.Show_JawabanFIR_Tambah = true;
		$scope.JawabanFIRTambah_ShowTambahButton = true;
		$scope.JawabanFIRTambah_ShowKembaliButton = false;
	}
	$scope.disableAddButton = false;
	$scope.value = function(param){
		console.log("param",param);
		if (param.value.FlagEdit==0)
		{
			$scope.disableAddButton = true

		}
		else
		{

			$scope.disableAddButton = false
		}
		
		console.log("param",param);
	}
	$scope.JawabanFIRTambah_Simpan_Clicked = function(){
		if($scope.JawabanFIRTambah_JawabanFIR == ""){
			alert("Jawaban harus diisi");
			return;
		}
		
		var saveDataFIR = { 
			"AnswerId" : $scope.JawabanFIRTambah_JawabanFIRId,
			"QuestionId" : $scope.JawabanFIRTambah_PertanyaanFIRId,
			"Description" : $scope.JawabanFIRTambah_JawabanFIR,
			"IsNegative" : $scope.JawabanFIRTambah_IsNegativeFIR
		};

		JawabanFIRFactory.create(saveDataFIR)
		.then(
			function(res){
				bsAlert.alert({
					title: "Data Berhasil di Simpan",
					text: "",
					type: "success",
					showCancelButton: false
				});
				$scope.JawabanFIRMain_Search_Clicked();
				$scope.JawabanFIRTambah_Batal_Clicked();
				
			},
			function(err){
				alert("Data gagal disimpan");
			}
		);
		
	}
	
	$scope.JawabanFIRTambah_Batal_Clicked = function(){
		$scope.JawabanFIRTambah_ClearFields();
		$scope.Show_JawabanFIR_Main = true;
		$scope.Show_JawabanFIR_Tambah = false;
	}
	
	$scope.JawabanFIRTambah_Kembali_Clicked = function(){
		$scope.JawabanFIRTambah_Batal_Clicked();
	}
	
	$scope.JawabanFIRMain_Search_Clicked = function(){
		$scope.JawabanFIRGrid_GetData();
	}
	

	$scope.JawabanFIRTambah_ClearFields = function(){
		$scope.JawabanFIRTambah_PertanyaanFIRId = 0;
		$scope.JawabanFIRTambah_PertanyaanFIR = "";
		$scope.JawabanFIRTambah_JawabanFIR = "";
		$scope.JawabanFIRTambah_JawabanFIRId = 0;
		$scope.JawabanFIRTambah_JawabanFIR_isDisabled = false;
		
	}
	
	$scope.JawabanFIRMain_BulkAction_Changed = function(){
		$scope.JawabanFIRMain_BulkActionCounter=0;
		
		if(angular.isUndefined($scope.GridAPIJawabanFIRGrid))
			return;
		
		if($scope.JawabanFIRMain_BulkAction == "hapus")
			$scope.JawabanFIRMain_BulkActionChange=1;
		
		$scope.JawabanFIRMain_BulkActionTotalData = $scope.GridAPIJawabanFIRGrid.selection.getSelectedRows().length;
		
		angular.forEach($scope.GridAPIJawabanFIRGrid.selection.getSelectedRows(), function(value, key){
			console.log("ini value",value,key)
			if(value.FlagEdit == 1)
				JawabanFIRFactory.delete(value.AnswerId, value.QuestionId)
				.then(
					function(res){
						$scope.JawabanFIRMain_BulkActionCounter = $scope.JawabanFIRMain_BulkActionCounter + 1;
					}
				);
			else
				bsNotify.show({
                  title: "Jawaban FIR",
                  content: "Data tidak boleh di delete karena berasal dari TAM",
                  type: 'danger'
              	});
		});
	}
	
	$scope.$watch("JawabanFIRMain_BulkActionCounter", function(newValue, oldValue){
		if ($scope.JawabanFIRMain_BulkActionCounter == $scope.JawabanFIRMain_BulkActionTotalData && $scope.JawabanFIRMain_BulkActionChange == 1){
			$scope.JawabanFIRMain_BulkActionChange = 0;
			alert("data berhasil dihapus");
			$scope.JawabanFIRGrid_GetData();
			$scope.JawabanFIRMain_BulkAction = "";
		}
	});
	
	$scope.JawabanFIRMainEditRow = function(row){
		$scope.JawabanFIRTambah_PertanyaanFIR = row.entity.QuestionDesc;
		$scope.JawabanFIRTambah_PertanyaanFIRId = row.entity.QuestionId;
		$scope.JawabanFIRTambah_JawabanFIR = row.entity.Description;
		$scope.JawabanFIRTambah_JawabanFIRId = row.entity.AnswerId;
		$scope.JawabanFIRTambah_ShowTambahButton = true;
		$scope.JawabanFIRTambah_ShowKembaliButton = false;
		$scope.Show_JawabanFIR_Main = false;
		$scope.Show_JawabanFIR_Tambah = true;
		$scope.JawabanFIRTambah_IsNegativeFIR = row.entity.IsNegative;
	}
	
	$scope.JawabanFIRMainLihatRow = function(row){
		$scope.JawabanFIRTambah_PertanyaanFIR = row.entity.QuestionDesc;
		$scope.JawabanFIRTambah_PertanyaanFIRId = row.entity.QuestionId;
		$scope.JawabanFIRTambah_JawabanFIR = row.entity.Description;
		$scope.JawabanFIRTambah_JawabanFIRId = row.entity.AnswerId;
		$scope.JawabanFIRTambah_JawabanFIR_isDisabled = true;
		$scope.JawabanFIRTambah_ShowTambahButton = false;
		$scope.JawabanFIRTambah_ShowKembaliButton = true;
		$scope.Show_JawabanFIR_Main = false;
		$scope.Show_JawabanFIR_Tambah = true;
		$scope.JawabanFIRTambah_IsNegativeFIR = row.entity.IsNegative;
	}
	
	$scope.uiGridPageSize = 10;
	$scope.selectedRow = [];
	$scope.JawabanFIRGrid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		columnDefs: [
			{ name: 'id', field: 'AnswerId', visible: false },
			{ name: 'QuestionId', field: 'QuestionId', visible: false },
			{ name: 'QuestionDesc', field: 'QuestionDesc', visible: false },
			{ name: 'Detail Jawaban', field: 'Description', enableCellEdit : false },
			{ name: 'Kategori', field: 'IsNegativeDesc', enableCellEdit : false },
			{name:"Action",
							cellTemplate:'<a class="fa fa-fw fa-lg fa-list-alt" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" ng-click="grid.appScope.JawabanFIRMainLihatRow(row)" title="Lihat"/>&nbsp;&nbsp;<a class="fa fa-fw fa-lg fa-pencil" ng-if="row.entity.FlagEdit == 1" ng-click="grid.appScope.JawabanFIRMainEditRow(row)" style="box-shadow:none!important;color:#777;margin:1px 1px 0px 2px;padding:0.5em" title="Edit"/>'},
		],
		onRegisterApi: function(gridApi) {
			$scope.GridAPIJawabanFIRGrid = gridApi;
			$scope.GridAPIJawabanFIRGrid.selection.on.rowSelectionChanged($scope, function(row){
				$scope.selectedRow = row.entity;
				console.log($scope.selectedRow);
			});
			if ($scope.GridAPIJawabanFIRGrid.selection.selectRow){
				console.log('selected',$scope.selectedRow);
				$scope.GridAPIJawabanFIRGrid.selection.selectRow($scope.JawabanFIRGrid.data[0]);
			}
		}
	};
	
	$scope.JawabanFIRGrid_GetData = function(){
		if(angular.isUndefined($scope.JawabanFIRMain_DaftarPertanyaan)){
			return;
		}
		
		JawabanFIRFactory.getData($scope.JawabanFIRMain_DaftarPertanyaan.QuestionId)
		.then(
			function(res)
			{
				// $scope.JawabanFIRGrid.data = res.data.Result;
				_.map(res.data.Result,function(val){
					if(val.IsNegative == 0){
						val.IsNegativeDesc = "Positif"
					}
					else
					{
						val.IsNegativeDesc = "Negatif"		
					}
				});
				$scope.JawabanFIRGrid.data = res.data.Result;
			}
			
		);
	}

	$scope.test = function(val){
		console.log("vall",val,$scope.GridAPIJawabanFIRGrid.grid.columns[$scope.filterColIdx].filters[0].term);
		console.log("valll",$scope.filterColIdx);
	}

	$scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
	$scope.gridApiAppointment = {};
	var x = -1;
	$scope.filterColIdx = 4;
	for (var i = 0; i < $scope.JawabanFIRGrid.columnDefs.length; i++) {
		if ($scope.JawabanFIRGrid.columnDefs[i].visible == undefined && $scope.JawabanFIRGrid.columnDefs[i].name !== 'Action') {
			x++;
			$scope.gridCols.push($scope.JawabanFIRGrid.columnDefs[i]);
			$scope.gridCols[x].idx = i;
		}
		console.log("$scope.gridCols", $scope.gridCols);
		console.log("$scope.grid.columnDefs[i]", $scope.JawabanFIRGrid.columnDefs[i]);
	}
	$scope.filterBtnLabel = $scope.gridCols[0].name;
	$scope.filterBtnChange = function(col) {
		console.log("col", col);
		$scope.filterBtnLabel = col.name;
		$scope.filterColIdx = col.idx + 1;
	};

});