angular.module('app')
	.factory('JawabanFIRFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
		console.log('CurrentUser', CurrentUser);
	return{
		getDataQuestions: function(){
			var url = '/api/as/AfterSalesFIRQuestions/?start=1&limit=100';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getData: function(questionId){
			var url = '/api/as/AfterSalesFIRAnswers/?QuestionsId=' + questionId + '&OutletId='+ user.OutletId;
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(firAnswerData){
			var url = '/api/as/AfterSalesFIRAnswers/Create/';
			var ArrayfirAnswerData = [firAnswerData];
			var param = JSON.stringify(ArrayfirAnswerData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		delete:function(id, questionId){
			var url = '/api/as/AfterSalesFIRAnswers/deletedata/?id=' + id + '&QuestionId=' + questionId;
			console.log("url delete Data : ", url);
			var res=$http.delete(url);
			return res;
		}

	}
});