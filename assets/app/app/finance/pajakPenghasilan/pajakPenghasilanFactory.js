/*
Edit by : 
	20170618, eric, initialize
*/
angular.module('app')
    .factory('PajakPenghasilanFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        var factory = {};
        var debugMode = true;

        factory.getDataPajakPenjualan = function (Start, Limit, DateStart, DateEnd, nomorBilling, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
            Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak) {
            var url = '/api/fe/FinanceVatpphPenjualan/?start='+Start+'&limit='+Limit+'&datestart=' + DateStart + '&dateend=' + DateEnd 
            + '&outletid=' + OutletId
            + '&BillingNo=' + nomorBilling
            + '&BuktiPotong='+buktiPotongNo
            + '&DateStartReceipt=' + tanggalFakturAwal
            + '&DateEndReceipt=' + tanggalFakturAkhir
            + '&Name=' + Nama
            + '&NPWP=' + NPWP
            + '&DownloadStatus=' + StatusDownload
            + '&TaxMonth='+MasaPajak 
            + '&TaxYear='+TahunPajak
            + '&ObjekPajak='+ObjekPajak;
            console.log('get data pajak keluaran : ', url);
            return $http.get(url);
            }	
        factory.getCSVPenjualan = function(customerId, billingList, TaxDocDate, OutletId){
                //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
                var url = '/api/fe/FinanceManualTaxPenjualan/Cetak/'
                console.log("getCSV : ", url);
                // var billingList = [{
                // 	"billingid":BillingId,
                // 	"billingcode":BillingCode
                // }];
                var inputData = [{
                    "TaxDataList":billingList
    
                }];
                var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
                return res;
            }
            factory.getDataPajakPembelian = function (Start, Limit, DateStart, DateEnd,DateInvStart, DateInvEnd, nomorPayment,nomorInvoice, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
                Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak,JenisPajak,TipeInvoice) {
                var url = '/api/fe/FinanceVatpphPembelian/?start='+Start+'&limit='+Limit+'&datestart=' + DateStart + '&dateend=' + DateEnd+'&dateinvstart=' + DateInvStart + '&dateinvend=' + DateInvEnd 
                + '&outletid=' + OutletId
                + '&PaymentNo=' + nomorPayment
                + '&InvoiceNo=' + nomorInvoice
                + '&BuktiPotong='+buktiPotongNo
                + '&DateStartReceipt=' + tanggalFakturAwal
                + '&DateEndReceipt=' + tanggalFakturAkhir
                + '&Name=' + Nama
                + '&NPWP=' + NPWP
                + '&DownloadStatus=' + StatusDownload
                + '&TaxMonth='+MasaPajak 
                + '&TaxYear='+TahunPajak
                + '&ObjekPajak='+ObjekPajak
                + '&JenisPajak='+JenisPajak
                + '&TipeInvoice='+TipeInvoice
                ;
                console.log('get data pajak keluaran : ', url);
                return $http.get(url);
                }	
        factory.getCSVPPH23 = function(customerId, billingList, TaxDocDate, OutletId){
                //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
                var url = '/api/fe/FinanceManualTaxPembelianPPH23/Cetak/'
                console.log("getCSV : ", url);
                // var billingList = [{
                // 	"billingid":BillingId,
                // 	"billingcode":BillingCode
                // }];
                var inputData = [{
                    "TaxDataList":billingList
    
                }];
                var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
                return res;
        }

        factory.getCSVPPH4 = function(customerId, billingList, TaxDocDate, OutletId){
            //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
            var url = '/api/fe/FinanceManualTaxPembelianPPH4/Cetak/'
            console.log("getCSV : ", url);
            // var billingList = [{
            // 	"billingid":BillingId,
            // 	"billingcode":BillingCode
            // }];
            var inputData = [{
                "TaxDataList":billingList

            }];
            var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
            return res;
        }
        
        factory.getCSVPPH26 = function(customerId, billingList, TaxDocDate, OutletId){
            //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
            var url = '/api/fe/FinanceManualTaxPembelianPPH26/Cetak/'
            console.log("getCSV : ", url);
            // var billingList = [{
            // 	"billingid":BillingId,
            // 	"billingcode":BillingCode
            // }];
            var inputData = [{
                "TaxDataList":billingList

            }];
            var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
            return res;
        }

    factory.getCSVPPH15 = function(customerId, billingList, TaxDocDate, OutletId){
    //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
    var url = '/api/fe/FinanceManualTaxPembelianPPH15/Cetak/'
    console.log("getCSV : ", url);
    // var billingList = [{
    // 	"billingid":BillingId,
    // 	"billingcode":BillingCode
    // }];
    var inputData = [{
        "TaxDataList":billingList

    }];
    var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
    return res;
    }
    factory.getCSVPPH22 = function(customerId, billingList, TaxDocDate, OutletId){
        //var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
        var url = '/api/fe/FinanceManualTaxPembelianPPH22/Cetak/'
        console.log("getCSV : ", url);
        // var billingList = [{
        // 	"billingid":BillingId,
        // 	"billingcode":BillingCode
        // }];
        var inputData = [{
            "TaxDataList":billingList

        }];
        var res=$http.post(url,inputData, {responseType: 'arraybuffer'});
        return res;
    }

        factory.getDataBranchComboBox = function (orgId) {
            var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
            var res = $http.get(url);
            return res;
        }
		
		factory.getInvoiceMasukType = function(){
			var url = '/api/fe/InvoiceMasuk/GetInvoiceMasukType/';
			console.log("url getTaxList : ", url);
			var res=$http.get(url);
			return res;
		}

        factory.getTaxList = function () {
            var url = '/api/fe/financetaxtype/getalldata';
            console.log("url getTaxList : ", url);
            var res = $http.get(url);
            return res;
        }

        factory.getTaxDescList = function () {
            var url = '/api/fe/VatPPH/GetPasalPPH';
            console.log("url getTaxList : ", url);
            var res = $http.get(url);
            return res;
        }

      
        return factory;

    });