var app = angular.module('app');
app.controller('PajakPenghasilanController', function ($scope, $http, CurrentUser, $filter, PajakPenghasilanFactory, $timeout, uiGridConstants, bsNotify) {
    $scope.optionsPajakPenghasilan_PilihanPPh = [
        { name: "Pembelian", value: "Pembelian" },
        { name: "Penjualan", value: "Penjualan" }
    ];

    $scope.PajakPenghasilan_PilihanPPh = "";

    $scope.optionsPajakPenghasilan_StatusDownloadCSV = [
        { name: "Sudah", value: 2 },
        { name: "Belum", value: 1 }
    ];

	var paginationOptions_PajakPenjualan = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
    }
    
    $scope.PajakPenghasilan_JenisInvoiceData = [];
    $scope.optionsPajakPenghasilan_TipeInvoice = [];

    $scope.PajakPenghasilan_JenisPajakData = [];
    $scope.optionsPajakPenghasilan_JenisPajak = [];
    $scope.optionsPajakPenghasilan_JenisPajakDesc = [];

    $scope.dateOptions = {
        format: "dd/MM/yyyy"
    };

    $scope.dateOptionsMonth = {
        format: "MM"
    };

    $scope.dateOptionsYear = {
        format: "yyyy"
    };

    $scope.optionsPajakPenghasilan_Pembelian_SelectKolom = [
        { name: "Nama Branch", value: "Nama Branch" },
        { name: "Nomor Invoice dan Vendor", value: "Nomor Invoice dan Vendor" },
        { name: "Nomor Instruksi Pembayaran", value: "Nomor Instruksi Pembayaran" },
        { name: "Nomor Bukti Potong", value: "Nomor Bukti Potong" },
        { name: "Nama Vendor", value: "Nama Vendor" },
        { name: "Nama NPWP", value: "Nama NPWP" },
        { name: "Status Download CSV", value: "Status Download CSV" }
    ];

    $scope.optionsPajakPenghasilan_Penjualan_SelectKolom = [
        { name: "Nama Branch", value: "Nama Branch" },
        { name: "Nomor Billing", value: "Nomor Billing" },
        { name: "Nomor Bukti Potong", value: "Nomor Bukti Potong" },
        { name: "Nama Pelanggan", value: "Nama Pelanggan" },
        { name: "Nama NPWP", value: "Nama NPWP" },
        { name: "Status Download CSV", value: "Status Download CSV" }
    ];

    var user = CurrentUser.user();

    if (user.OrgCode.substring(3, 9) == '000000')
        $scope.PajakPenghasilanMain_HO_Show = false;
    else
        $scope.PajakPenghasilanMain_HO_Show = true;

    $scope.PajakPenghasilan_getDataBranch = function () {
        PajakPenghasilanFactory.getDataBranchComboBox(user.OrgId)
            .then
            (
            function (res) {
                $scope.optionsPajakPenghasilanMain_SelectBranch = res.data.Result;

                $scope.PajakPenghasilanMain_SelectBranch = user.OrgId;

                // res.data.some(function (obj, i) {
                //     if (obj.value == user.OutletId)
                //         $scope.PajakPenghasilanMain_NamaBranch = obj.name;
                // });
            },
            function (err) {
                console.log("err=>", err);
            }
            );
    };

    $scope.PajakPenghasilan_getDataBranch();

    $scope.PajakPenghasilan_PilihanPPh_Selected_Changed = function () {
        $scope.PajakPenghasilan_FilterForm_Show = true;
        $scope.PajakPenghasilan_TipeInvoice_Show = false;
        var ddlPPh = angular.isUndefined($scope.PajakPenghasilan_PilihanPPh) ? "" : $scope.PajakPenghasilan_PilihanPPh;

        if ($scope.PajakPenghasilan_PilihanPPh == "Pembelian") {
            $scope.PajakPenghasilan_TipeInvoice_Show = true;

            if ($scope.PajakPenghasilan_JenisInvoiceData.length == 0) {
                PajakPenghasilanFactory.getInvoiceMasukType()
                    .then(
                        function (res) {
                            angular.forEach(res.data.Result, function (value, key) {
                                $scope.PajakPenghasilan_JenisInvoiceData.push({ "name": value.Label, "value": { "InvoiceType": value.Label, "InvoiceId": value.Value } });
                            });
                            $scope.optionsPajakPenghasilan_TipeInvoice = $scope.PajakPenghasilan_JenisInvoiceData;
                        }
                    );
            }

            $scope.PajakPenghasilan_PembelianForm_Show = true;
            $scope.PajakPenghasilan_PenjualanForm_Show = false;
        }
        else if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan") {
            $scope.PajakPenghasilan_PembelianForm_Show = false;
            $scope.PajakPenghasilan_PenjualanForm_Show = true;
           
        }

        if (ddlPPh == "") {
            $scope.PajakPenghasilan_FilterForm_Show = false;
        }
        else {
            $scope.PajakPenghasilan_JenisPajakData = [];
            $scope.PajakPenghasilan_JenisPajakDataDesc = [];
                PajakPenghasilanFactory.getTaxList()
                    .then(
                        function (res) {
                            angular.forEach(res.data.Result, function (value, key) {
                                $scope.PajakPenghasilan_JenisPajakData.push({ "name": value.TaxType, "value": { "TaxType": value.TaxType, "TaxTypeId": value.TaxTypeId, "DC": value.DC, "NPWPAmount": value.NPWPAmount, "NonNPWPAmount": value.NonNPWPAmount, "isDocTax": value.isDocTax, "TaxDesc": value.TaxDesc } });//
                            });
                            if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan")
                            {
                                $scope.optionsPajakPenghasilan_JenisPajak = $filter('filter')($scope.PajakPenghasilan_JenisPajakData, function(obj) {
                                    return obj.name == 'PPh 22 5% - Pajak Penjualan Barang Sangat Mewah' &&  !obj.value.TaxDesc.includes('PPN') ;
                                },true);
                            }
                            else{
                                //$scope.optionsPajakPenghasilan_JenisPajak = $scope.PajakPenghasilan_JenisPajakData;
                                $scope.optionsPajakPenghasilan_JenisPajak = $filter('filter')($scope.PajakPenghasilan_JenisPajakData, function(obj) {
                                    return obj.name != 'PPh 22 5% - Pajak Penjualan Barang Sangat Mewah'  &&  !obj.value.TaxDesc.includes('PPN') ;
                                },true);
                            }
                            
                        }
                    );

                    PajakPenghasilanFactory.getTaxDescList()
                    .then(
                        function (res) {
                            angular.forEach(res.data.Result, function (value, key) {
                                $scope.PajakPenghasilan_JenisPajakDataDesc.push({ "name": value.taxDesc, "value": value.taxDesc});//
                            });
                            if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan")
                            {
                                $scope.optionsPajakPenghasilan_JenisPajakDesc =
                                $filter('filter')($scope.PajakPenghasilan_JenisPajakDataDesc, function(obj) {
                                    return obj.value == 'PPh Pasal 22 - Payable' ;
                                },true); 
                            }
                            else{
                                $scope.optionsPajakPenghasilan_JenisPajakDesc =
                                $filter('filter')($scope.PajakPenghasilan_JenisPajakDataDesc, function(obj) {
                                    return !obj.value.includes('PPN') && obj.value != 'PPh Pasal 22 - Payable' ;
                                },true); 
                            }
                            
                            
                        }
                    );
            
        }

        $scope.PajakPenghasilan_UIGrid_Show = false;
    }

    $scope.PajakPenghasilan_Pembelian_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: false,
		useExternalPagination: false,
		enableSelectAll: false,
		enableColumnResizing: true,
		enableRowSelection: true,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ displayName: "Nama Branch", field: "OutletName", width: 170 },
			{ displayName: "Jenis Pajak", field: "TaxType", width: 170 },
			{ displayName: "Jenis Pasal", field: "ObjekPajak", width: 170 },
            { displayName: "Tanggal Invoice Masuk", field: "InvoiceDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 170 },
            { displayName: "Nomor Invoice dari Vendor", field: "InvoiceNo", width: 170 },
            { displayName: "Tipe Invoice Masuk", field: "InvoiceType", width: 170 },
            { displayName: "Tanggal Instruksi Pembayaran", field: "PaymentDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 170 },
            { displayName: "Nomor Instruksi Pembayaran", field: "PaymentNo", width: 170 },
            { displayName: "Nama Vendor", field: "VendorName" , width: 170},
            { displayName: "Nama NPWP", field: "VendorName" , width: 170},
            { displayName: "NPWP", field: "Npwp", width: 170 },
            { displayName: "Alamat NPWP", field: "Address", width: 170 },
            { displayName: "Tanggal Bukti Potong", field: "PaymentDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 170 },
            { displayName: "Nomor Bukti Potong", field: "BuktiPotongNo", width: 170 },
            { displayName: "Masa Pajak", field: "TaxMonth", width: 170 },
            { displayName: "Tahun Pajak", field: "TaxYear", width: 170 },
            
            { displayName: "Akumulasi DPP PPH", field: "AkumulasiDPP", width: 170 },
            { displayName: "Nominal DPP", field: "DPP", cellFilter: 'rupiahPPH', width: 170 },
            { displayName: "Tarif (%)", field: "TaxPercent", width: 170 },
            { displayName: "Nominal pph", field: "PPH", cellFilter: 'rupiahPPH', width: 170 },
            { displayName: "Nomor GL Account", field: "GLAccountCode" , width: 170},
            { displayName: "Status Download CSV", field: "DownloadCSVStatus", width: 170 }
		],

		onRegisterApi: function (gridApi) {
			$scope.PajakPenghasilan_Pembelian_gridAPI = gridApi;
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
				if (gridApi.selection.getSelectedRows().length == 0) {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = true;
                    $scope.PajakPenghasilan_DownloadCSV_Disabled = true;
				}
				else {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = false;
					$scope.PajakPenghasilan_DownloadCSV_Disabled = false;
				}
            });
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (gridApi.selection.getSelectedRows().length == 0) {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = true;
                    $scope.PajakPenghasilan_DownloadCSV_Disabled = true;
				}
				else {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = false;
					$scope.PajakPenghasilan_DownloadCSV_Disabled = false;
				}
            });
		}
    };
    
    $scope.PajakPenghasilan_Penjualan_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: false,
		useExternalPagination: false,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableRowSelection: true,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ displayName: "Nama Branch", field: "OutletName", width: 170 },
			{ displayName: "Jenis Pajak", field: "TaxType", width: 170 },
			{ displayName: "Jenis Pasal", field: "ObjekPajak", width: 170 },
            { displayName: "Tanggal Billing", field: "BillingDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 170 },
            { displayName: "Nomor Billing", field: "BillingCode", width: 170 },
            { displayName: "Nama Pelanggan", field: "CustomerName" , width: 170},
            { displayName: "Nama NPWP", field: "CustomerName" , width: 170},
            { displayName: "NPWP", field: "Npwp", width: 170 },
            { displayName: "Alamat NPWP", field: "Address", width: 170 },
            { displayName: "Tanggal Bukti Potong", field: "BillingDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 170 },
            { displayName: "Nomor Bukti Potong", field: "BuktiPotongNo", width: 170 },
            { displayName: "Masa Pajak", field: "TaxMonth", width: 170 },
            { displayName: "Tahun Pajak", field: "TaxYear", width: 170 },
            { displayName: "Nominal DPP", field: "DPP", cellFilter: 'rupiahPPH', width: 170 },
            { displayName: "Tarif (%)", field: "TaxPercent", width: 170 },
            { displayName: "Nominal pph", field: "PPH", cellFilter: 'rupiahPPH', width: 170 },
            { displayName: "Nomor GL Account", field: "GLAccountCode" , width: 170},
            { displayName: "Status Download CSV", field: "DownloadCSVStatus", width: 170 }
		],

		onRegisterApi: function (gridApi) {
            $scope.PajakPenghasilan_Penjualan_gridAPI = gridApi;
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
				if (gridApi.selection.getSelectedRows().length == 0) {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = true;
                    $scope.PajakPenghasilan_DownloadCSV_Disabled = true;
				}
				else {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = false;
					$scope.PajakPenghasilan_DownloadCSV_Disabled = false;
				}
            });
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (gridApi.selection.getSelectedRows().length == 0) {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = true;
                    $scope.PajakPenghasilan_DownloadCSV_Disabled = true;
				}
				else {
                    $scope.PajakPenghasilan_DownloadExcel_Disabled = false;
					$scope.PajakPenghasilan_DownloadCSV_Disabled = false;
				}
            });
		}
    };

    $scope.PajakPenghasilan_Cari_Clicked = function () {
        var taxType = '';
        var invoiceType = '';
       if(angular.isUndefined($scope.PajakPenghasilan_JenisPajak))
       {
        taxType = 'undefined';
       } 
       else{
        taxType = $scope.PajakPenghasilan_JenisPajak.TaxTypeId;
       }

       if(angular.isUndefined($scope.PajakPenghasilan_TipeInvoice))
       {
        invoiceType = 'undefined';
       } 
       else{
        invoiceType = $scope.PajakPenghasilan_TipeInvoice.InvoiceType;
       }

       if(($scope.PajakPenghasilan_TanggalInstruksiPembayaran == undefined && $scope.PajakPenghasilan_TanggalInstruksiPembayaran == null)){
           $scope.PajakPenghasilan_TanggalInstruksiPembayaran = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalInstruksiPembayaran = $filter('date')(new Date($scope.PajakPenghasilan_TanggalInstruksiPembayaran), 'yyyy-MM-dd');
       }

       if(($scope.PajakPenghasilan_TanggalInstruksiPembayaranTo == undefined && $scope.PajakPenghasilan_TanggalInstruksiPembayaranTo == null)){
            $scope.PajakPenghasilan_TanggalInstruksiPembayaranTo = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalInstruksiPembayaranTo = $filter('date')(new Date($scope.PajakPenghasilan_TanggalInstruksiPembayaranTo), 'yyyy-MM-dd');
       }
       
       if(($scope.PajakPenghasilan_TanggalInvoiceMasuk == undefined && $scope.PajakPenghasilan_TanggalInvoiceMasuk == null)){
            $scope.PajakPenghasilan_TanggalInvoiceMasuk = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalInvoiceMasuk = $filter('date')(new Date($scope.PajakPenghasilan_TanggalInvoiceMasuk), 'yyyy-MM-dd');
       }
       
       if(($scope.PajakPenghasilan_TanggalInvoiceMasukTo == undefined && $scope.PajakPenghasilan_TanggalInvoiceMasukTo == null)){
            $scope.PajakPenghasilan_TanggalInvoiceMasukTo = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalInvoiceMasukTo = $filter('date')(new Date($scope.PajakPenghasilan_TanggalInvoiceMasukTo), 'yyyy-MM-dd');
       }
       
       if(($scope.PajakPenghasilan_TanggalBuktiPotong == undefined && $scope.PajakPenghasilan_TanggalBuktiPotong == null)){
            $scope.PajakPenghasilan_TanggalBuktiPotong = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalBuktiPotong = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotong), 'yyyy-MM-dd');
       }
       
       if(($scope.PajakPenghasilan_TanggalBuktiPotongTo == undefined && $scope.PajakPenghasilan_TanggalBuktiPotongTo == null)){
            $scope.PajakPenghasilan_TanggalBuktiPotongTo = undefined;
       }else{
            $scope.PajakPenghasilan_TanggalBuktiPotongTo = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotongTo), 'yyyy-MM-dd');
       }

        if(($scope.PajakPenghasilan_MasaPajak == undefined && $scope.PajakPenghasilan_MasaPajak == null)){
             $scope.PajakPenghasilan_MasaPajak = undefined;
        }else{
            $scope.PajakPenghasilan_MasaPajak = $filter('date')(new Date($scope.PajakPenghasilan_MasaPajak), 'MM');
        }

        if(($scope.PajakPenghasilan_TahunPajak == undefined && $scope.PajakPenghasilan_TahunPajak == null)){
            $scope.PajakPenghasilan_TahunPajak = undefined;
        }else{
            $scope.PajakPenghasilan_TahunPajak = $filter('date')(new Date($scope.PajakPenghasilan_TahunPajak), 'yyyy');
        }

        $scope.PajakPenghasilan_UIGrid_Show = true;
        $scope.PajakPenghasilan_DownloadExcel_Disabled = true;
        $scope.PajakPenghasilan_DownloadCSV_Disabled = true;

        if ($scope.PajakPenghasilan_PilihanPPh == "Pembelian") {
            $scope.PajakPenghasilan_Pembelian_UIGrid.data = [];
            $scope.getDataPajakPembelian(1, 10, 
                $scope.PajakPenghasilan_TanggalInstruksiPembayaran,
                $scope.PajakPenghasilan_TanggalInstruksiPembayaranTo,
                $scope.PajakPenghasilan_TanggalInvoiceMasuk,
                $scope.PajakPenghasilan_TanggalInvoiceMasukTo,
                $scope.PajakPenghasilan_NomorInstruksiPembayaran,
                $scope.PajakPenghasilan_NomorInvoicedanVendor,
                $scope.PajakPenghasilan_NomorBuktiPotong,
                $scope.PajakPenghasilan_TanggalBuktiPotong,
                $scope.PajakPenghasilan_TanggalBuktiPotongTo,
                $scope.PajakPenghasilan_NamaVendor,
                $scope.PajakPenghasilan_NPWP,
                $scope.PajakPenghasilan_StatusDownloadCSV, 
                $scope.PajakPenghasilanMain_SelectBranch,
                $scope.PajakPenghasilan_KeteranganObjekPajak,
                // $filter('date')(new Date($scope.PajakPenghasilan_MasaPajak), "MM"),
                // $filter('date')(new Date($scope.PajakPenghasilan_TahunPajak), "yyyy"),
                $scope.PajakPenghasilan_MasaPajak,
                $scope.PajakPenghasilan_TahunPajak,
                taxType,
                invoiceType
            );
        }
        else if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan") {
           
            if(($scope.PajakPenghasilan_TanggalBilling == undefined && $scope.PajakPenghasilan_TanggalBilling == null)){
                $scope.PajakPenghasilan_TanggalBilling = undefined;
            }else{
                $scope.PajakPenghasilan_TanggalBilling = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBilling), 'yyyy-MM-dd');
            }
    
            if(($scope.PajakPenghasilan_TanggalBillingTo == undefined && $scope.PajakPenghasilan_TanggalBillingTo == null)){
                $scope.PajakPenghasilan_TanggalBillingTo = undefined;
            }else{
                $scope.PajakPenghasilan_TanggalBillingTo = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBillingTo), 'yyyy-MM-dd');
            }
    
            if(($scope.PajakPenghasilan_TanggalBuktiPotong == undefined && $scope.PajakPenghasilan_TanggalBuktiPotong == null)){
                $scope.PajakPenghasilan_TanggalBuktiPotong = undefined;
            }else{
                $scope.PajakPenghasilan_TanggalBuktiPotong = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotong), 'yyyy-MM-dd');
            }
    
            if(($scope.PajakPenghasilan_TanggalBuktiPotongTo == undefined && $scope.PajakPenghasilan_TanggalBuktiPotongTo == null)){
                $scope.PajakPenghasilan_TanggalBuktiPotongTo = undefined;
            }else{
                $scope.PajakPenghasilan_TanggalBuktiPotongTo = $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotongTo), 'yyyy-MM-dd');
            }


            $scope.PajakPenghasilan_Penjualan_UIGrid.data = [];              
                    $scope.getDataPajakPenjualan(1, 10, 
                        $scope.PajakPenghasilan_TanggalBilling,
                        $scope.PajakPenghasilan_TanggalBillingTo,
                        $scope.PajakPenghasilan_NomorBilling,
                        $scope.PajakPenghasilan_NomorBuktiPotong,
                        $scope.PajakPenghasilan_TanggalBuktiPotong,
                        $scope.PajakPenghasilan_TanggalBuktiPotongTo,
                        $scope.PajakPenghasilan_NamaVendor,
                        $scope.PajakPenghasilan_NPWP,
                        $scope.PajakPenghasilan_StatusDownloadCSV, 
                        $scope.PajakPenghasilanMain_SelectBranch,
                        $scope.PajakPenghasilan_KeteranganObjekPajak,
                        $scope.PajakPenghasilan_MasaPajak,
                        $scope.PajakPenghasilan_TahunPajak
                    );
               
        }

        // if (angular.isUndefined($scope.PajakPenghasilan_KeteranganObjekPajak))
        // {$scope.PajakPenghasilan_DownloadCSV_Disabled = true;}
        // else
        // {$scope.PajakPenghasilan_DownloadCSV_Disabled = false;}

        // console.log(angular.isUndefined($scope.PajakPenghasilan_KeteranganObjekPajak));
    }

    $scope.PajakPenghasilan_KeteranganObjekPajak_changed = function(){
        // $scope.PajakPenghasilan_DownloadCSV_Disabled = true;
    }

    $scope.getDataPajakPenjualan = function (Start, Limit, DateStart, DateEnd, nomorBilling, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
		 Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak) {
		console.log("ouletid :", $scope.PajakKeluaranMain_SelectBranch);
		PajakPenghasilanFactory.getDataPajakPenjualan(Start, Limit, DateStart, DateEnd, nomorBilling, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
            Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak)
			.then(
				function (res) {
					console.log(res);
					$scope.PajakPenghasilan_Penjualan_UIGrid.data = res.data.Result;
					$scope.PajakPenghasilan_Penjualan_UIGrid.totalItems = res.data.Total;
					$scope.PajakPenghasilan_Penjualan_UIGrid.paginationPageSize = paginationOptions_PajakPenjualan.pageSize;
					$scope.PajakPenghasilan_Penjualan_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.PajakPenghasilan_Penjualan_UIGrid.paginationCurrentPage = paginationOptions_PajakPenjualan.pageNumber;
				},
				function (err) {
					console.log("err=>", err);
				}
			);

    }

    $scope.getDataPajakPembelian = function (Start, Limit, DateStart, DateEnd,DateInvStart, DateInvEnd, nomorPayment,nomorInvoice, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
        Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak,JenisPajak,TipeInvoice) {
       console.log("ouletid :", $scope.PajakKeluaranMain_SelectBranch);
       PajakPenghasilanFactory.getDataPajakPembelian(Start, Limit, DateStart, DateEnd, DateInvStart, DateInvEnd, nomorPayment,nomorInvoice, buktiPotongNo, tanggalFakturAwal, tanggalFakturAkhir, 
           Nama, NPWP, StatusDownload,OutletId,ObjekPajak,MasaPajak,TahunPajak,JenisPajak,TipeInvoice)
           .then(
               function (res) {
                   console.log(res);
                   $scope.PajakPenghasilan_Pembelian_UIGrid.data = res.data.Result;
                   $scope.PajakPenghasilan_Pembelian_UIGrid.totalItems = res.data.Total;
                   $scope.PajakPenghasilan_Pembelian_UIGrid.paginationPageSize = paginationOptions_PajakPenjualan.pageSize;
                   $scope.PajakPenghasilan_Pembelian_UIGrid.paginationPageSizes = [10, 25, 50];
                   $scope.PajakPenghasilan_Pembelian_UIGrid.paginationCurrentPage = paginationOptions_PajakPenjualan.pageNumber;
               },
               function (err) {
                   console.log("err=>", err);
               }
           );

   }
    
    
    $scope.PPHPenjualan_Download = function () {
		var billingList = [];

		angular.forEach($scope.PajakPenghasilan_Penjualan_gridAPI.selection.getSelectedRows(), function (value, key) {

			billingList.push({
			"TaxDataId":value.TaxDataId
			})
			


        });
		$scope.BuatPenghasilanPenjualan_BuatCSV(0, billingList, '', 0);
    }

    $scope.PPHPembelian_Download = function () {
        
        var members = $scope.PajakPenghasilan_Pembelian_gridAPI.selection.getSelectedRows();
        console.log('Selected rows:', members);

        var groups = members.reduce(function(obj,item){
            obj[item.ObjekPajak] = obj[item.ObjekPajak] || [];
            obj[item.ObjekPajak].push(item.TaxDataId);
            return obj;
        }, {});

        var myArray = Object.keys(groups).map(function(key){
            return {TaxDataId: groups[key], ObjekPajak: key};
        });
        console.log('Grouped by ObjekPajak:', myArray);

        var promise = $timeout();
        angular.forEach(myArray, function(value){
            promise = promise.then(function() {
                var billingList = [];
                //$scope.PajakPenghasilan_KeteranganObjekPajak = value.ObjekPajak;
                angular.forEach(value.TaxDataId, function(res){
                    billingList.push({
                        "TaxDataId":res
                    });
                });
                console.log('Buat CSV:', value.ObjekPajak + ', billing list', billingList);
                $scope.BuatPenghasilanPembelian_BuatCSV(value.ObjekPajak, 0, billingList, '', 0);
                //$scope.PajakPenghasilan_KeteranganObjekPajak = undefined;
                return $timeout(500);
            });
        });
    }
    

    $scope.BuatPenghasilanPenjualan_BuatCSV = function (CustomerId, billingList, TaxDocDate, OutletId) {

		PajakPenghasilanFactory.getCSVPenjualan(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh 22 Sangat Mewah - Penjualan' + '.csv');
					//$scope.PajakKeluaran_Cari_Clicked();
				},
				function (err) {
					console.log("err=>", err);
				}
			);
    }

    $scope.BuatPenghasilanPembelian_BuatCSV = function (ObjekPajak, CustomerId, billingList, TaxDocDate, OutletId) {

        if (ObjekPajak != undefined && ObjekPajak != ''){
            JenisPasal = ObjekPajak;
        }
        else {
            JenisPasal = $scope.PajakPenghasilan_KeteranganObjekPajak;
        }

        if (JenisPasal=="PPh Pasal 4 ayat 2")
        {
            PajakPenghasilanFactory.getCSVPPH4(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh 4 Ayat 2 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }
        else if (JenisPasal=="PPh Pasal 15")
        {
            PajakPenghasilanFactory.getCSVPPH15(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'SPT PPh 15 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }
		else if (JenisPasal=="PPh Pasal 21")
        {
            PajakPenghasilanFactory.getCSVPPH23(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh Pasal 21 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }									
        else if (JenisPasal=="PPh Pasal 22 - Prepaid")
        {
            PajakPenghasilanFactory.getCSVPPH22(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh Pasal 22 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }
        else if (JenisPasal=="PPh Pasal 23")
        {
            PajakPenghasilanFactory.getCSVPPH23(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh Pasal 23 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }
        else if (JenisPasal=="PPh Pasal 26")
        {
            PajakPenghasilanFactory.getCSVPPH26(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'PPh Pasal 26 - Pembelian' + '.csv');
				},
				function (err) {
					console.log("err=>", err);
				});
        }
		
    }

    $scope.PPHPenjualan_DownloadDaftar_Clicked = function () {
		var excelData = [];
		var fileName = "";
		var eachData = {};
		PajakPenghasilanFactory.getDataPajakPenjualan(1, 10, 
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBilling), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBillingTo), 'yyyyMMdd'),
            $scope.PajakPenghasilan_NomorBilling,
            $scope.PajakPenghasilan_NomorBuktiPotong,
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotong), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotongTo), 'yyyyMMdd'),
            $scope.PajakPenghasilan_NamaPelanggan,
            $scope.PajakPenghasilan_NPWP,
            $scope.PajakPenghasilan_StatusDownloadCSV, 
            $scope.PajakPenghasilanMain_SelectBranch,
            $scope.PajakPenghasilan_KeteranganObjekPajak,
            $scope.PajakPenghasilan_MasaPajak,
            $scope.PajakPenghasilan_TahunPajak)
			.then(
				function (res) {
					console.log(res);
					dataAll = res.data.Result;
					var TaxIdArray = [];
					if (dataAll.length > 0) {
						angular.forEach($scope.PajakPenghasilan_Penjualan_gridAPI.selection.getSelectedRows(), function (value, key) {
							TaxIdArray.push(value.TaxDataId);
						});
						//$scope.getAllData();
						angular.forEach(dataAll, function (value, key) {
							console.log("value : ", value);
			
							// eachData = {"Nomor Billing":value.BillingCode, "Tanggal Billing":value.BillingDate, "Nomor Faktur Pajak":value.TaxInvoiceNo, 
							// 		"Tanggal Faktur Pajak":value.UsedDate, "Tipe Bisnis":value.BusinesTypeName, "Kode Jenis Transaksi":value.TranCodeName, "Nama":value.CustomerName, 
							// 		"Alamat":value.Address, "NPWP":value.Npwp, "DPP":value.DPP, "PPN":value.PPN, "Faktur Pajak Pengganti":value.isTaxSubstitute, 
							// 		"Tanggal Pembuatan Faktur Pajak":value.ProcessDate, "Status Download CSV":value.DownloadCSVStatus};
							if (TaxIdArray.includes(value.TaxDataId))
							{
								eachData = {
									"Nama Branch": value.OutletName|| '', "Jenis Pajak": value.TaxType|| '', "Jenis Pajak": value.ObjekPajak|| '',
									"Tanggal Billing": value.BillingDate|| '', "Nomor Billing": value.BillingCode || '', "Nama Pelanggan": value.CustomerName || '', "Nama NPWP": value.CustomerName|| '',
									"NPWP": value.Npwp || '', "Alamat Vendor": value.Address||'', "Tanggal Bukti Potong": value.ProcessDate, "Nomor Bukti Potong": value.BuktiPotongNo|| '', "Masa Pajak": value.TaxMonth|| '',
									"Tahun Pajak": value.TaxYear|| '', "Nominal DPP": value.DPP || '', "Tarif (%)": value.TaxPercent || '', "Nominal pph": value.PPH || '', "Nomor GL Account": value.GLAccountCode || '', "Status Download CSV": value.DownloadCSVStatus || ''
								};
								excelData.push(eachData);
							}

			
						});
						console.log("excel Data :", excelData);
						fileName = "Daftar Pajak Penghasilan - Penjualan";
			
						XLSXInterface.writeToXLSX(excelData, fileName);
					}
					else {
                        eachData = {
                            "Nama Branch": "-", "Jenis Pajak":"-", "Jenis Pajak": "-",
                            "Tanggal Billing": "-", "Nomor Billing": "-", "Nama Pelanggan": "-", "Nama NPWP": "-",
                            "NPWP": "-", "Alamat Vendor": "-", "Tanggal Bukti Potong": "-", "Nomor Bukti Potong": "-", "Masa Pajak": "-",
                            "Tahun Pajak": "-", "Nominal DPP":"-", "Tarif ( % )":"-", "Nominal pph": "-", "Nomor GL Account":"-", "Status Download CSV": "-"
                        };
						excelData.push(eachData);
						console.log("excel Data :", excelData);
						fileName = "Daftar Pajak Penghasilan - Penjualan";
			
						XLSXInterface.writeToXLSX(excelData, fileName);
					}
				},
				function (err) {
					console.log("err=>", err);
					bsNotify.show({
						title: "Error",
						content: err,
						type: "danger"
					});

				}
			);




    }

    
    
    $scope.PPHPembelianDownloadDaftar_Clicked = function () {
		var excelData = [];
		var fileName = "";
        var eachData = {};
        var taxType = '';
        var invoiceType = '';
       if(angular.isUndefined($scope.PajakPenghasilan_JenisPajak))
       {
        taxType = 'undefined';
       } 
       else{
        taxType = $scope.PajakPenghasilan_JenisPajak.TaxTypeId;
       }

       if(angular.isUndefined($scope.PajakPenghasilan_TipeInvoice))
       {
        invoiceType = 'undefined';
       } 
       else{
        invoiceType = $scope.PajakPenghasilan_TipeInvoice.InvoiceType;
       }
		PajakPenghasilanFactory.getDataPajakPembelian(1, 10, 
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalInstruksiPembayaran), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalInstruksiPembayaranTo), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalInvoiceMasuk), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalInvoiceMasukTo), 'yyyyMMdd'),
            $scope.PajakPenghasilan_NomorInstruksiPembayaran,
            $scope.PajakPenghasilan_NomorInvoicedanVendor,
            $scope.PajakPenghasilan_NomorBuktiPotong,
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotong), 'yyyyMMdd'),
            $filter('date')(new Date($scope.PajakPenghasilan_TanggalBuktiPotongTo), 'yyyyMMdd'),
            $scope.PajakPenghasilan_NamaPelanggan,
            $scope.PajakPenghasilan_NPWP,
            $scope.PajakPenghasilan_StatusDownloadCSV, 
            $scope.PajakPenghasilanMain_SelectBranch,
            $scope.PajakPenghasilan_KeteranganObjekPajak,
            $scope.PajakPenghasilan_MasaPajak,
            $scope.PajakPenghasilan_TahunPajak,
            taxType,
            invoiceType)
			.then(
				function (res) {
					console.log(res);
					dataAll = res.data.Result;
					var TaxIdArray = [];
					if (dataAll.length > 0) {
						angular.forEach($scope.PajakPenghasilan_Pembelian_gridAPI.selection.getSelectedRows(), function (value, key) {
							TaxIdArray.push(value.TaxDataId);
						});
						//$scope.getAllData();
						angular.forEach(dataAll, function (value, key) {
							console.log("value : ", value);
			
							// eachData = {"Nomor Billing":value.BillingCode, "Tanggal Billing":value.BillingDate, "Nomor Faktur Pajak":value.TaxInvoiceNo, 
							// 		"Tanggal Faktur Pajak":value.UsedDate, "Tipe Bisnis":value.BusinesTypeName, "Kode Jenis Transaksi":value.TranCodeName, "Nama":value.CustomerName, 
							// 		"Alamat":value.Address, "NPWP":value.Npwp, "DPP":value.DPP, "PPN":value.PPN, "Faktur Pajak Pengganti":value.isTaxSubstitute, 
							// 		"Tanggal Pembuatan Faktur Pajak":value.ProcessDate, "Status Download CSV":value.DownloadCSVStatus};
							if (TaxIdArray.includes(value.TaxDataId))
							{
								eachData = {
									"Nama Branch": value.OutletName|| '', "Jenis Pajak": value.TaxType|| '', "Jenis Pasal": value.ObjekPajak|| '', "Tanggal Invoice Masuk": value.InvoiceDate||'',"Nomor Invoice dari Vendor":value.InvoiceNo||'',"Tipe Invoice Masuk": value.InvoiceType || '',"Tanggal Instruksi Pembayaran":value.PaymentDate||'',"Nomor Instruksi Pembayaran":value.PaymentNo||'',
									"Nama Vendor": value.VendorName || '', "Nama NPWP": value.VendorName|| '',
									"NPWP": value.Npwp || '', "Alamat Vendor": value.Address||'', "Tanggal Bukti Potong": value.ProcessDate, "Nomor Bukti Potong": value.BuktiPotongNo|| '', "Masa Pajak": value.TaxMonth|| '',
									"Tahun Pajak": value.TaxYear|| '', "Nominal DPP": value.DPP || '', "Tarif (%)": value.TaxPercent || '', "Nominal pph": value.PPH || '', "Nomor GL Account": value.GLAccountCode || '', "Status Download CSV": value.DownloadCSVStatus || ''
								};
								excelData.push(eachData);
							}

			
						});
						console.log("excel Data :", excelData);
						fileName = "Daftar Pajak Penghasilan - Pembelian";
			
						XLSXInterface.writeToXLSX(excelData, fileName);
					}
					else {
                        eachData = {
                            "Nama Branch":  "-", "Jenis Pajak":  "-", "Jenis Pajak": "-", "Tanggal Invoice Masuk":  "-","Nomor Invoice dari Vendor": "-","Tipe Invoice Masuk":  "-","Tanggal Instruksi Pembayaran": "-","Nomor Instruksi Pembayaran": "-",
                            "Nama Vendor":  "-", "Nama NPWP": "-",
                            "NPWP":  "-", "Alamat Vendor":  "-", "Tanggal Bukti Potong":  "-", "Nomor Bukti Potong":  "-", "Masa Pajak":  "-",
                            "Tahun Pajak":  "-", "Nominal DPP":  "-", "Tarif (%)":  "-", "Nominal pph":  "-", "Nomor GL Account":  "-", "Status Download CSV": "-"
                        };
						excelData.push(eachData);
						console.log("excel Data :", excelData);
						fileName = "Daftar Pajak Penghasilan - Pembelian";
			
						XLSXInterface.writeToXLSX(excelData, fileName);
					}
				},
				function (err) {
					console.log("err=>", err);
					bsNotify.show({
						title: "Error",
						content: err,
						type: "danger"
					});

				}
			);
	}
    
    $scope.PajakPenghasilan_DownloadExcel_Clicked = function() {
        if ($scope.PajakPenghasilan_PilihanPPh == "Pembelian") {
            $scope.PPHPembelianDownloadDaftar_Clicked();
        }
        else if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan") {
            $scope.PPHPenjualan_DownloadDaftar_Clicked();
        }
    }
    
    $scope.PajakPenghasilan_DownloadCSV_Clicked = function() {
        if ($scope.PajakPenghasilan_PilihanPPh == "Pembelian") {
           
                $scope.PPHPembelian_Download();
            
        }
        else if ($scope.PajakPenghasilan_PilihanPPh == "Penjualan") {
            
                $scope.PPHPenjualan_Download();
            
        }
    }
});

app.filter('rupiahPPH', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
});