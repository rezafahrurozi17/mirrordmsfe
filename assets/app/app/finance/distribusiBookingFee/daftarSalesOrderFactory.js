angular.module('app')
  .factory('DaftarSalesOrderFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[
				{Id: "1",  NomorSo: "so1",  TanggalSo: new Date("2/9/2017"),  DistribusiBookingFee: 10},
				{Id: "2",  NomorSo: "so2",  TanggalSo: new Date("2/9/2017"),  DistribusiBookingFee: 10},
				{Id: "3",  NomorSo: "so3",  TanggalSo: new Date("2/9/2017"),  DistribusiBookingFee: 10},
			  ];
		res=da_json;
		alert('ini quick fix di DaftarSalesOrderFactory cari solusi bener nanti');
        return res;
      },
      create: function(daftarSalesOrderFactory) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: daftarSalesOrderFactory.Name,
                                            Description: daftarSalesOrderFactory.Description}]);
      },
      update: function(daftarSalesOrderFactory){
        return $http.put('/api/fw/Role', [{
                                            Id: daftarSalesOrderFactory.Id,
                                            //pid: negotiation.pid,
                                            Name: daftarSalesOrderFactory.Name,
                                            Description: daftarSalesOrderFactory.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });