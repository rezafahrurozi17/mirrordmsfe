angular.module('app')
.controller('DistribusiBookingFeeController', function($scope, $http, CurrentUser, DistribusiBookingFeeFactory,DaftarSalesOrderFactory,$timeout) {
    
	$scope.DistribusiBookingFee_MainPage=true;
	
	$scope.DistribusiBookingFee_Cari = function () {

		//$scope.DistribusiBookingFee_TanggalIncomingPayment;
		//$scope.DistribusiBookingFee_NamaBank;
		$scope.List_DistribusiBookingFee_To_Repeat=DistribusiBookingFeeFactory.getData();
		$scope.DistribusiBookingFee_Tabel=true;
		$scope.optionsDistribusiBookingFee_DaftarSpkSearchCategory=[{ name: "Nomor SPK", value: "Nomor SPK" }, { name: "Tanggal SPK", value: "Tanggal SPK" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }, { name: "Model", value: "Model" }, { name: "Tipe", value: "Tipe" }, { name: "Warna", value: "Warna" }];
		
		
	};
	
	$scope.ToDistribusiDetail = function (Distribusi) {

		//alert(Distribusi.NomorSpk);
		$scope.DistribusiBookingFee_MainPage=false;
		$scope.DistribusiBookingFee_Detail=true;
		
		$scope.DistribusiBookingFee_TanggalDistribusiBookingFee=Distribusi.TanggalDistribusiBookingFee;
		$scope.DistribusiBookingFee_NomorSpk=Distribusi.NomorSpk;
		$scope.DistribusiBookingFee_Model=Distribusi.Model;
		$scope.DistribusiBookingFee_Tipe=Distribusi.Tipe;
		$scope.DistribusiBookingFee_Warna=Distribusi.Warna;
		$scope.DistribusiBookingFee_NomorPelanggan=Distribusi.NomorPelanggan;
		$scope.DistribusiBookingFee_NamaPelanggan=Distribusi.NamaPelanggan;
		$scope.DistribusiBookingFee_Alamat=Distribusi.Alamat;
		$scope.DistribusiBookingFee_KabupatenKota=Distribusi.KabupatenKota;
		$scope.DistribusiBookingFee_HandphoneTelepon=Distribusi.HandphoneTelepon;
		$scope.DistribusiBookingFee_TanggalSpk=Distribusi.TanggalSpk;
		$scope.DistribusiBookingFee_NominalBookingFee=Distribusi.NominalBookingFee;
		$scope.List_DaftarSalesOrder_To_Repeat=DaftarSalesOrderFactory.getData();
		
		var TotalBookingFee=0;
		for(var i = 0; i < $scope.List_DaftarSalesOrder_To_Repeat.length; ++i)
		{
			TotalBookingFee+=$scope.List_DaftarSalesOrder_To_Repeat[i].DistribusiBookingFee;
		}
		$scope.DistribusiBookingFee_TotalDistribusiBookingFee=TotalBookingFee;
		
	};
	
	$scope.DistribusiBookingFee_PengajuanDistribusi = function () {

		alert('pengajuan distribusi');
		$scope.DistribusiBookingFee_Detail=false;
		$scope.DistribusiBookingFee_MainPage=true;
		//nanti clear isi textboxnya
		
	};
	
	$scope.DistribusiBookingFee_Kembali = function () {

		$scope.DistribusiBookingFee_Detail=false;
		$scope.DistribusiBookingFee_MainPage=true;
		//nanti clear isi textboxnya
	};
	
	$scope.List_DaftarSalesOrder_RecalculateTotalBookingFee = function () {
		//$scope.DistribusiBookingFee_TotalDistribusiBookingFee=0;
		var TotalBookingFee=0;
		for(var i = 0; i < $scope.List_DaftarSalesOrder_To_Repeat.length; ++i)
		{
			TotalBookingFee+=parseFloat($scope.List_DaftarSalesOrder_To_Repeat[i].DistribusiBookingFee);
		}
		$scope.DistribusiBookingFee_TotalDistribusiBookingFee=TotalBookingFee;
		
	};
	

	
	
});
