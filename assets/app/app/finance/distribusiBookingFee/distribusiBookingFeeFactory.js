angular.module('app')
  .factory('DistribusiBookingFeeFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[
				{Id: "1",  NomorSpk: "SPK1",TanggalSpk: new Date("2/9/2017"),NamaPelanggan: "didit1",NomorPelanggan: "p1",Model: "Avanza",Tipe: "1.5 E A/T",Warna: "Red",TanggalDistribusiBookingFee: new Date("2/9/2017"),Alamat: "alamat1",KabupatenKota: "Jakarta",HandphoneTelepon: "1234",NominalBookingFee: 1000},
				{Id: "2",  NomorSpk: "SPK2",TanggalSpk: new Date("2/9/2017"),NamaPelanggan: "didit2",NomorPelanggan: "p2",Model: "Avanza",Tipe: "1.5 E A/T",Warna: "Red",TanggalDistribusiBookingFee: new Date("2/9/2017"),Alamat: "alamat2",KabupatenKota: "Jakarta",HandphoneTelepon: "1234",NominalBookingFee: 1000},
				{Id: "3",  NomorSpk: "SPK3",TanggalSpk: new Date("2/9/2017"),NamaPelanggan: "didit3",NomorPelanggan: "p3",Model: "Avanza",Tipe: "1.5 E A/T",Warna: "Red",TanggalDistribusiBookingFee: new Date("2/9/2017"),Alamat: "alamat3",KabupatenKota: "Jakarta",HandphoneTelepon: "1234",NominalBookingFee: 1000},
				{Id: "4",  NomorSpk: "SPK4",TanggalSpk: new Date("2/9/2017"),NamaPelanggan: "didit4",NomorPelanggan: "p4",Model: "Avanza",Tipe: "1.5 E A/T",Warna: "Red",TanggalDistribusiBookingFee: new Date("2/9/2017"),Alamat: "alamat4",KabupatenKota: "Jakarta",HandphoneTelepon: "1234",NominalBookingFee: 1000},
				{Id: "5",  NomorSpk: "SPK5",TanggalSpk: new Date("2/9/2017"),NamaPelanggan: "didit5",NomorPelanggan: "p5",Model: "Avanza",Tipe: "1.5 E A/T",Warna: "Red",TanggalDistribusiBookingFee: new Date("2/9/2017"),Alamat: "alamat5",KabupatenKota: "Jakarta",HandphoneTelepon: "1234",NominalBookingFee: 1000},
			  ];
		res=da_json;
		
        return res;
      },
      create: function(distribusiBookingFee) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: distribusiBookingFee.Name,
                                            Description: distribusiBookingFee.Description}]);
      },
      update: function(distribusiBookingFee){
        return $http.put('/api/fw/Role', [{
                                            Id: distribusiBookingFee.Id,
                                            //pid: negotiation.pid,
                                            Name: distribusiBookingFee.Name,
                                            Description: distribusiBookingFee.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });