var app = angular.module('app');
app.controller('MasterPricingDealerToBranchPriceLinkDIOController', function ($scope, $http, $filter, bsAlert, CurrentUser, MasterPricingDealerToBranchPriceLinkDIOFactory, $timeout) {
	$scope.optionsKolomFilterSatu_PriceLinkDIO = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_PriceLinkDIO = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_PriceLinkDIO = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_PriceLinkDIO = [
			{name:"Kode Paket DIO", value:"DIOPacketCode"}, {name:"Nama Paket DIO", value:"DIOPacketName"},
			{name:"Kode DIO", value:"DIOCode"}, {name:"Nama DIO", value:"DIOName"},
			{name:"Effective Date From", value:"EffectiveDateFrom"},
			{name:"Effective Date To", value:"EffectiveDateTo"}, {name:"Remarks", value:"Remarks"}
	];
	angular.element('#PriceLinkDioModal').modal('hide');
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'PriceLinkDIO';
	$scope.cekKolom = '';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_PriceLinkDIO = false;

	$scope.MasterSellingPriceFU_PriceLinkDIO_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],	
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'DIOPacketCode', displayName: 'Kode Paket DIO', field: 'DIOPacketCode',enableCellEdit: false , enableHiding: false},
			{ name: 'DIOPacketName', displayName: 'Nama Paket DIO', field: 'DIOPacketName',enableCellEdit: false, enableHiding: false },			
			{ name: 'DIOCode', displayName: 'Kode DIO', field: 'DIOCode',enableCellEdit: false, enableHiding: false},
			{ name: 'DIOName', displayName: 'Nama DIO', field: 'DIOName',enableCellEdit: false, enableHiding: false },
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date From', field: 'EffectiveDateFrom',enableCellEdit: false, enableHiding: false },
			{ name: 'EffectiveDateTo', displayName: 'Effective Date To', field: 'EffectiveDateTo',enableCellEdit: false, enableHiding: false },  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks',enableCellEdit: true, enableHiding: false }, 
			{ name: 'DIOId',displayName:'Id', field: 'DIOId', visible: false, enableHiding: false },		
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceLinkDIO' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				$scope.MasterSelling_PriceLinkDIO_Upload_Clicked();
				myEl.val('');
				
            });
	}

	$scope.validateColumn = function(inputExcelData){
		// ini harus di rubah
		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.DIOCode)) {
			$scope.cekKolom = $scope.cekKolom + ' DIOCode \n';
		}
		
		if (angular.isUndefined(inputExcelData.DIOPacketName)) {
			$scope.cekKolom = $scope.cekKolom + ' DIOPacketName \n';
		}
		if 	(angular.isUndefined(inputExcelData.DIOPacketCode)){
			$scope.cekKolom = $scope.cekKolom + ' DIOPacketCode \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}		
		if ( angular.isUndefined(inputExcelData.DIOCode) || 
			angular.isUndefined(inputExcelData.DIOPacketName) ||
			angular.isUndefined(inputExcelData.DIOPacketCode) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.ComboBulkAction_PriceLinkDIO_Changed = function()
	{
		if ($scope.ComboBulkAction_PriceLinkDIO == "Delete") {
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				// angular.element('#AreaPricingModal').modal('show');
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
					function() {
						$scope.OK_Button_Clicked(); 
					},
					function() {
		
					}
				);
	
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_PriceLinkDIO_grid.data.splice($scope.MasterSellingPriceFU_PriceLinkDIO_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_PriceLinkDIO = "";
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_PriceLinkDIO = "";
		angular.element('#PriceLinkDioModal').modal('hide');
	}

	$scope.MasterSelling_PriceLinkDIO_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingPriceLinkDIO";

		if ($scope.MasterSellingPriceFU_PriceLinkDIO_grid.data.length == 0) {
				excelData.push({ 
						DIOPacketCode : "",
						DIOPacketName : "",
						DIOCode: "",
		//				DIOName: row.DIOName,
		//				SalesPrice: row.SalesPrice,
						EffectiveDateFrom: "",
						EffectiveDateTo: "",
						Remarks: "" });
				fileName = "MasterSellingPriceLinkDIOTemplate";
		}
		else {
		//$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_PriceLinkDIO_grid.data.forEach(function(row) {		
				excelData.push({ 
						DIOPacketCode : row.DIOPacketCode,
						DIOPacketName : row.DIOPacketName,
						DIOCode: row.DIOCode,
		//				DIOName: row.DIOName,
		//				SalesPrice: row.SalesPrice,
						EffectiveDateFrom: row.EffectiveDateFrom,
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks:row.Remarks });
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_PriceLinkDIO_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.grid.clearAllFilters();
		MasterPricingDealerToBranchPriceLinkDIOFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_PriceLinkDIO_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceLinkDIO_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_PriceLinkDIO_grid.data);		
		MasterPricingDealerToBranchPriceLinkDIOFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_PriceLinkDIO_grid.data = [];
		$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_PriceLinkDIO = "";
		$scope.TextFilterSatu_PriceLinkDIO = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceLinkDIO' ) );
		myEl.val('');	
	}

	$scope.MasterSelling_PriceLinkDIO_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_PriceLinkDIO_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_PriceLinkDIO_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_PriceLinkDIO_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_PriceLinkDIO_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_PriceLinkDIO_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_PriceLinkDIO_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_PriceLinkDIO,
			Text1 : $scope.TextFilterSatu_PriceLinkDIO,
			AndOr : $scope.Filter_PriceLinkDIO_AndOr,
			Filter2 : $scope.KolomFilterDua_PriceLinkDIO,
			Text2 : $scope.TextFilterDua_PriceLinkDIO,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingDealerToBranchPriceLinkDIOFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_PriceLinkDIO_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceLinkDIO_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_PriceLinkDIO_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_PriceLinkDIO;
		$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_PriceLinkDIO != "") {
			$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.grid.getColumn($scope.ComboFilterGrid_PriceLinkDIO).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_PriceLinkDIO_gridAPI.grid.clearAllFilters();

	}

});