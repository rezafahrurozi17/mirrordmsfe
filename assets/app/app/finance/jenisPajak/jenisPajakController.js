var app = angular.module('app');
app.controller('JenisPajakController', function ($scope, $http, $filter, CurrentUser, bsNotify, JenisPajakFactory, $timeout) {

	$scope.optionsJenisPajakDebetKredit = [{ name: "Debet", value: "D" }, { name: "Kredit", value: "K" }];
	//$("<br>").insertBefore(".layout_top");
	//$scope.optionsJenisPajakGL = [{name:"1", value:"1"}, {name:"2", value:"2"}];
	var GLobj = [];
	JenisPajakFactory.getDataGLTaxList()
		.then(
			function (res) {
				console.log("rees", res);
				var DataResult = [];
				//res = {"Result":[{"GLAccountId":6,"GLAccountCode":"001PJK","GLAccountName":"GL Pajak 1","GLDesc":"GL Pajak 1","GLPurposeId":3804,"LastModifiedDate":null,"LastModifiedUserId":null}],"Start":0,"Limit":1,"Total":1};
				console.log("res.result", res.data.Result);
				angular.forEach(res.data.Result, function (value, key) {
					DataResult.push({ name: value.GLAccountCode, value: value.GLAccountId });
					GLobj.push({ id: value.GLAccountId, name: value.GLAccountName });
					console.log("DataResult : ", DataResult);
				});
				$scope.optionsJenisPajakGL = DataResult;
				console.log("$scope.optionsJenisPajakGL : ", $scope.optionsJenisPajakGL);
			}
		);
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading = true;
		$scope.gridData = [];
	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mJenisPajak = null; //Model
	$scope.xRole = { selected: [] };

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOption = { format: dateFormat }


	$scope.cekObject = function () {
		console.log($scope.mJenisPajak);
	}

	$scope.gridActionTemplate = '<div align="center" class="ui-grid-cell-contents">\
    <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Ubah" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
	</div>';
	

	// Saat Edit Jenis Pajak
	$scope.OnShowDetail = function(model,mode){
		console.log("OnShowDetail",model)
		if(mode == "new"){
			$scope.Show_mJenisPajak_Tambah = true;
			$scope.Show_mJenisPajak_Edit = false;
		}else{
			$scope.Show_mJenisPajak_Tambah = false;
			$scope.Show_mJenisPajak_Edit = true;
		}
	}
	// end
	// Notif ---------------------
	$scope.onValidateSave = function(model,mode){
		console.log("cek notif>>>>",model)
		if(mode == "create"){
			$scope.creat(model);
			bsNotify.show({
				title: "Berhasil",
				content: "Data Berhasil disimpan",
				type: 'success'
			});
			$scope.formApi.setMode('grid');
			$timeout(function(){
				$scope.getData();
			},100)
		}else{
			$scope.update(model);
			bsNotify.show({
				title: "Berhasil",
				content: "Data Berhasil diupdate",
				type: 'success'
			});
			$scope.formApi.setMode('grid');
			$timeout(function(){
				$scope.getData();
			},100)
		}
		
	}

	$scope.creat = function(model){
		JenisPajakFactory.create(model)
		.then(
			function(res){
				// $scope.grid.data = res.data.Result;
			}
		);
	}
	$scope.update = function(model){
		JenisPajakFactory.update(model)
		.then(
			function(res){
				// $scope.grid.data = res.data.Result;
			}
		);
	}

	$scope.optionsJenisPajakGLChange = function (t) {
		if (t != null) {
			$.each(GLobj, function (index, value) {
				//console.log(index + '   ' + value.id + '   ' + value.name);
				if (value.id == t) {
					$scope.mJenisPajak.GLName = value.name;
				}
			});
		}
		else {
			$scope.mJenisPajak.GLName = "";
		}
	}
	$scope.bsformAboveGrid = '<br><br>test eetseyasd asjdhga ksdg aksjdh aksd';
	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	$scope.getData = function () {
		$scope.grid.data = [];
		//$scope.grid.data = JenisPajakFactory.getData();
		JenisPajakFactory.getData()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) { 
						value.NPWPAmount = value.NPWPAmount.toString().replace('.', ',');
						value.NonNPWPAmount = value.NonNPWPAmount.toString().replace('.', ',');
					});
					$scope.grid.data = res.data.Result;
				}
			);
		// .then(
		//     function(res){
		//         gridData = [];
		//         //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
		//          = res.data.Result;
		//         console.log("AssetBrosur=>",res.data.Result);
		//         //console.log("grid data=>",$scope.grid.data);
		//         //$scope.roleData = res.data;
		//         $scope.loading=false;
		//     },
		//     function(err){
		//         console.log("err=>",err);
		//     }
		// );
	}

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function (rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function (rows) {
		console.log("onSelectRows=>", rows);
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		//showTreeExpandNoChildren: true,
		// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
		// paginationPageSize: 15,

		columnDefs: [
			{ name: 'id', field: 'TaxTypeId', visible: false },
			{ name: 'Jenis Pajak', field: 'TaxType' },
			{ name: 'Keterangan Objek Pajak', field: 'TaxDesc' },
			{ name: 'Tarif Pajak (% dengan NPWP)', field: 'NPWPAmount' },
			{ name: 'Tarif Pajak (% tanpa NPWP)', field: 'NonNPWPAmount' },
			{ name: 'Debet / Kredit', field: 'DC', visible: false },
			{ name: 'Debet / Kredit', field: 'DCText' },
			{ name: 'Nama GL Account Pajak', field: 'GLName' },
			{ name: 'Faktur Pajak Masukan', field: 'isEFaktur', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isEFaktur">' },
			{ name: 'Dokumen Pajak', field: 'isDocTax', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isDocTax">' },
		]
	};

}).directive('validPercent', function () {  
    return {  
        restrict: 'A',  
        link: function (scope, elm, attrs, ctrl) {  
            elm.on('keydown', function (event) {  
                var $input = $(this);  
                var value = $input.val();  
                value = value.replace(/[^0-9\.]/g, '')  
                var findsDot = new RegExp(/\./g)  
                var containsDot = value.match(findsDot)  
                if (containsDot != null && ([46, 110, 190].indexOf(event.which) > -1)) {  
                    event.preventDefault();  
                    return false;  
                }  
                $input.val(value);  
                if (event.which == 64 || event.which == 16) {  
                    // numbers  
                    return false;  
                } if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {  
                    // backspace, enter, escape, arrows  
                    return true;  
                } else if (event.which >= 48 && event.which <= 57) {  
                    // numbers  
                    return true;  
                } else if (event.which >= 96 && event.which <= 105) {  
                    // numpad number  
                    return true;  
                } else if ([46, 110, 190].indexOf(event.which) > -1) {  
                    // dot and numpad dot  
                    return true;  
                } else {  
                    event.preventDefault();  
                    return false;  
                }  
            });  
        }  
    }  
});  