angular.module('app')
	.factory('JenisPajakFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/FinanceTaxType/start/1/limit/100';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(TaxTypeData){
			var url = '/api/fe/FinanceTaxType/create/';
			TaxTypeData.TaxType = window.encodeURI(TaxTypeData.TaxType);
			TaxTypeData.NPWPAmount = TaxTypeData.NPWPAmount.toString().replace(',','.');
			TaxTypeData.NonNPWPAmount = TaxTypeData.NonNPWPAmount.toString().replace(',','.');
			var ArrayTaxTypeData = [TaxTypeData];
			var param = JSON.stringify(ArrayTaxTypeData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(TaxTypeData){
			var url = '/api/fe/FinanceTaxType/Update/';
			TaxTypeData.TaxType = window.encodeURI(TaxTypeData.TaxType);
			TaxTypeData.NPWPAmount = TaxTypeData.NPWPAmount.toString().replace(',','.');
			TaxTypeData.NonNPWPAmount = TaxTypeData.NonNPWPAmount.toString().replace(',','.');
			var ArrayTaxTypeData = [TaxTypeData];
			var param = JSON.stringify(ArrayTaxTypeData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/fe/FinanceTaxType/deletedata/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		},
		
		getDataGLTaxList:function(){
			var url = '/api/fe/FinanceCOA/GetDataByPurpose/?strGLType=GL Pajak';
			console.log("url List GL Tax : ", url);
			var res=$http.get(url);
			return res;
		}
	}
});