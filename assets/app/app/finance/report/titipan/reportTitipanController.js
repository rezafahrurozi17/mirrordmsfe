angular.module('app')
.controller('ReportTitipanController', function ($scope, $http, $filter, CurrentUser, ReportTitipanFactory, $timeout,bsNotify) {
	var user = CurrentUser.user();
	console.log("data user", user);
	$scope.dateNow = Date.now();
	$scope.ReportTitipan_Tanggal = new Date();
	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};
	
	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.is_HO = 0;
	$scope.ReportTitipan_GetDataBranchByOutletId = function(){
		// ReportTitipanFactory.getBranchByOutletId(user.OutletId)
		// .then(
		// 	function(res){
		// 		console.log("res data BranchName : ", res.data.Result[0]);
		// 		$scope.ReportTitipan_Branch = res.data.Result[0].BranchName;
		// 	}
		// );

		if ($scope.right(user.OrgCode, 6) == '000000') {
			$scope.is_HO = 1;

			// kiriman 0, di samakan dengan menu instruksi pembayaran
			ReportTitipanFactory.getDataBranchComboBox(0).then(function(res){
				console.log("res data BranchName : ", res.data.Result[0]);
				$scope.data_branch = res.data.Result
				// $scope.ReportTitipan_Branch = res.data.Result[0].BranchName;

				// if (user.RoleId == 1137) {
				// 	//finance ho
				// 	$scope.ReportTitipan_Branch_isDisabled = false
				// } else {
				// 	$scope.ReportTitipan_Branch_isDisabled = true
				// }
			});

		} else {
			$scope.is_HO = 0;
			ReportTitipanFactory.getBranchByOutletId(user.OutletId).then(function(res){
					console.log("res data BranchName : ", res.data.Result[0]);
					$scope.ReportTitipan_Branch = res.data.Result[0].BranchName;
			});
		}
		

	}
	
	$scope.ReportTitipan_GetDataBranchByOutletId();


	$scope.selectBranch = function(selected) {
		$scope.ReportTitipan_Branch = selected.BranchName
		$scope.data_outletid = selected.BranchId

	}

	
	$scope.optionsReportFakturPajak_Status = [{name:"Semua", value:"Semua"}, {name:"Tersedia", value:"Tersedia"}, {name:"Digunakan", value:"Digunakan"}];
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.Report_Excel_Clicked=function() {
		var isUnit = 0, isService = 0, isPart = 0, isUnknown = 0;

		var outlet_id = user.OutletId

		if ($scope.is_HO == 1) {
			outlet_id = $scope.data_outletid
		}

		if ($scope.ReportTitipan_Branch == null || $scope.ReportTitipan_Branch == undefined || $scope.ReportTitipan_Branch == '') {
			bsNotify.show({
				title:"Warning",
				content:"Nama Branch harus diisi.",
				type:"warning"
			});
			return;
		}

		if (((isNaN($scope.ReportTitipan_Tanggal) == true) ||  
					 (angular.isUndefined($scope.ReportTitipan_Tanggal) == true) ||
					 ( $scope.ReportTitipan_Tanggal==null))) 
		{
					bsNotify.show({
						title:"Warning",
						content:"Tanggal harus diisi / format tanggal salah.",
						type:"warning"
					});
		}
		else if((new Date($scope.ReportTitipan_Tanggal) > new Date())){
			bsNotify.show({
				title:"Warning",
				content:"Tanggal tidak bisa lebih besar dari hari ini.",
				type:"warning"
			});
		}
		else
		{

			if($scope.ReportTitipan_isUnit)
				isUnit = 1;
			
			if($scope.ReportTitipan_isService)
				isService = 1;
			
			if($scope.ReportTitipan_isPart)
				isPart = 1;
			
			if($scope.ReportTitipan_isUnknown)
				isUnknown = 1;
			
			ReportTitipanFactory.getExcelReportTitipanFactory($filter('date')(new Date($scope.ReportTitipan_Tanggal), "yyyyMMdd"),
															$scope.ReportTitipan_NamaPelanggan,
															isUnit, isService, isPart, isUnknown, outlet_id)
			.then(
				function(res){

					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);

					saveAs(blob, 'Report_Titipan' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
	}

});