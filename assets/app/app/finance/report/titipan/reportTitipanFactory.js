angular.module('app')
	.factory('ReportTitipanFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;


	factory.getExcelReportTitipanFactory=function(date, customerName, isUnit, isService, isPart, isUnknown, outlet_id) {
        var url = '/api/fe/FinanceRptCustomerDeposit/Excel/?date=' + date + '&customername=' + customerName + '&isUnit=' + isUnit + '&isservice=' + isService + '&isparts=' + isPart + '&isunknown=' + isUnknown + '&Outletid=' + outlet_id;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};
	
	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};

	factory.getDataBranchComboBox = function (orgId) {
		console.log("factory.getDataBranchComboBox");
		//var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ orgId;
		var res = $http.get(url);
		return res;
	};
	
	return factory;
});