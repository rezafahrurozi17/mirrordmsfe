angular.module('app')
	.controller('ReportDailyCashBankController', function ($scope, bsNotify, $http, $filter, CurrentUser, ReportDailyCashBankFactory, $timeout) {
		var user = CurrentUser.user();
		if (user.OrgCode.substring(3, 9) == '000000')
			$scope.ReportDailyCashBank_Branch_isDisabled = true;
		else
			$scope.ReportDailyCashBank_Branch_isDisabled = false;


		// $scope.ReportDailyCashBankTanggalOK = true;
		// $scope.ReportDailyCashBankTanggalChanged = function () {

		// 	console.log($scope.ReportDailyCashBankTanggalOK);
		// 	var dt = $scope.ReportDailyCashBank_Tanggal;
		// 	var now = new Date();
		// 	 if (dt > now) {
		// 		$scope.errReportDailyCashBankTangga = "Tanggal tidak boleh lebih dari tanggal hari ini";
		// 		$scope.ReportDailyCashBankTanggalOK = false;			
		// 	}
		// 	else {
		// 		$scope.ReportDailyCashBankTanggalOK = true;
		// 	}
		// 	console.log('asdasd ' + $scope.ReportDailyCashBankTanggalOK);
		// }
		//$scope.ReportDailyCashBank_GetDataBranchByOutletId = function(){
		ReportDailyCashBankFactory.getBranchByOutletId(user.OutletId)
			.then(
				function (res) {
					//console.log("res data BranchName : ", res.data.Result[0]);

					$scope.optionsReportDailyCashBank_NamaBranch = res.data.Result;
					$scope.ReportDailyCashBank_Branch = res.data.Result[0].BranchId;
				}
			);
		//}
		// $scope.ReportDailyCashBank_GetDataBranchByOutletId();

		var dataCOA = [];
		ReportDailyCashBankFactory.getCOACashBankList()
			.then(
				function (res) {
					console.log("res COA Cash Bank List", res);
					dataCOA = res.data.Result;
					var dataOption = [];
					angular.forEach(res.data.Result, function (value, key) {
						dataOption.push({ "name": value.GLAccountName, "value": value.GLAccountId });
					});
					$scope.optionsReportDailyCashBankNamaGLAccount = dataOption;
				}
			);

		$scope.KonfirmasiNamaGL = "NaN";
		$scope.AccountId = 0;
		$scope.ReportDailyCashBank_NamaGLAccount_Changed = function () {
			$scope.KonfirmasiNamaGL = "NaN";
			console.log("masuk ke NamaGLAccountChange");

			angular.forEach(dataCOA, function (value, key) {
				if (value.GLAccountId == $scope.ReportDailyCashBank_NamaGLAccount) {
					$scope.AccountId = value.AccountId;
					$scope.KonfirmasiNamaGL = value.GLAccountCode;
					$scope.ReportDailyCashBank_NomorGLAccount = value.GLAccountCode;
				}
			});
		}

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		// $scope.Report_Cetak_Clicked = function() {         
		// var pdfFile="";  
		// var endDate="";

		// ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd/MM/yyyy"),
		// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
		// .then(
		// function (res) {
		// var file = new Blob([res.data], {type: 'application/pdf'});
		// var fileURL = URL.createObjectURL(file);

		// console.log("pdf", fileURL);


		// printJS(fileURL);
		// },
		// function(err){
		// console.log("err=>",err);
		// }
		// );
		// };
		$scope.ReportDailyCashBank_TipeReport = 1;

		$scope.ModalKonfirmasiClosing_Tidak = function () {
			angular.element('#ModalKonfirmasiClosing').modal('hide');
		}

		$scope.ModalKonfirmasiClosing_Ya = function () {
			$scope.CetakReportClosing();
		}

		$scope.Report_Excel_Clicked = function () {
			if (!$scope.ReportDailyCashBank_Close) {
				angular.element('#ModalKonfirmasiClosing').modal('show');
			}
			else {
				$scope.CetakReportClosing();
			}
		}

		$scope.CetakReportClosing = function () {
			var isClosing = 0;
			var Lanjut = 1;
			if (new Date($scope.ReportDailyCashBank_Tanggal) > new Date()) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});

			}
			else if (angular.isUndefined($scope.ReportDailyCashBank_Tanggal) == true) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal Pembayaran harus diisi",
					type: "warning"
				});
			}
			else if (angular.isUndefined($scope.ReportDailyCashBank_NamaGLAccount) == true) {
				bsNotify.show({
					title: "Warning",
					content: "Nama GL Account harus diisi",
					type: "warning"
				});
			}
			else {
				if (!$scope.ReportDailyCashBank_Close) {
					console.log('Closing');
					isClosing = 1

					var data =
						[{
							GLId: $scope.ReportDailyCashBank_NamaGLAccount,
							TranDate: $filter('date')(new Date($scope.ReportDailyCashBank_Tanggal), "yyyy-MM-dd")
			
						}]

					ReportDailyCashBankFactory.submitClosing(data)
						.then
						(
						function (res) {
							var Msg = 'Closing ' + $scope.ReportDailyCashBank_NomorGLAccount + ' Tanggal ' + $filter('date')(new Date($scope.ReportDailyCashBank_Tanggal), "dd/MM/yyyy") + ' Berhasil';
							bsNotify.show(
								{
									title: "Berhasil",
									content: Msg,
									type: 'success'
								} 
							);
							Lanjut = 1;

							if (Lanjut == 1) {
								ReportDailyCashBankFactory.getPDFReporDailyCashBankSummary($filter('date')(new Date($scope.ReportDailyCashBank_Tanggal), "yyyyMMdd"),
									$scope.ReportDailyCashBank_NamaGLAccount, isClosing, $scope.ReportDailyCashBank_TipeReport,$scope.AccountId, $scope.ReportDailyCashBank_Branch).then(
										function (res) {
											debugger;
											var contentType = 'application/pdf';
											var blob = new Blob([res.data], { type: contentType });
											saveAs(blob, 'Report_DailyCashBank.pdf');
										},
										function (err) {
											console.log("err=>", err);
										}
									);
							}
						},
						function (err) {
							if (err != null) {
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
						}
						)
				}
				else {
					ReportDailyCashBankFactory.getPDFReporDailyCashBankSummary($filter('date')(new Date($scope.ReportDailyCashBank_Tanggal), "yyyyMMdd"),
						$scope.ReportDailyCashBank_NamaGLAccount, isClosing, $scope.ReportDailyCashBank_TipeReport,$scope.AccountId,  $scope.ReportDailyCashBank_Branch).then(
							function (res) {
								debugger;
								var contentType = 'application/pdf';
								var blob = new Blob([res.data], { type: contentType });
								saveAs(blob, 'Report_DailyCashBank.pdf');
							},
							function (err) {
								console.log("err=>", err);
							}
						);
				}
			}
		}


	});