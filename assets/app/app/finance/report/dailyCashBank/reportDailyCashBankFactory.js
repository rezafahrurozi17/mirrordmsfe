angular.module('app')
	.factory('ReportDailyCashBankFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		

	factory.getExcelReporDailyCashBankSummary=function(date, glAccountName, isClosing, reportType) {
        var url = '/api/fe/FinanceRptDailyCashBank/ExcelSummary/?Date='+date+'&GLAccountName='+glAccountName+'&isClosing='+isClosing+'&ReportType=' + reportType;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};
 
	factory.getPDFReporDailyCashBankSummary=function(date, glAccountName, isClosing, reportType,AccountId, branch) {
        var url = '/api/fe/FinanceRptDailyCashBank/Cetak/?Date='+date+'&GLAccountName='+glAccountName+'&isClosing='+isClosing+'&ReportType=' + reportType+'&AccountId='+AccountId + '&OutletId='+branch;
        console.log('get --> ', url);
        var res=$http.get(url, { responseType: 'arraybuffer' });
        return res;
	};	

	factory.getCOACashBankList=function(){
		var url = '/api/fe/FinanceCOA/GetDataByPurpose/?StrGlType=GL Cash Bank&isHO=-3';
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};

	factory.submitClosing = function (inputData) {
		var url = '/api/fe/FinanceRptDailyCashBank/SubmitData/';
		var param = JSON.stringify(inputData);

		if (debugMode) { console.log('Masuk ke Closing') };
		if (debugMode) { console.log('url :' + url); };
		if (debugMode) { console.log('Parameter POST :' + param) };
		return $http.post(url, param);
	}


	return factory;
});