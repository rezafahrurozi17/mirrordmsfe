angular.module('app')
	.factory('ReportIncomingEscrowAccountFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataBranchRptIncomingEscrow=function() {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
      var res=$http.get(url);  
      return res;			
    };

	factory.getExcelReportIncomingEscrowAccount=function(startdate,enddate, branch) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/FFinanceRptIncomingEscrowAct/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&outletId=' +  branch;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getCetakReportIncomingEscrowAccount=function(startdate,enddate, branch) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/FFinanceRptIncomingEscrowAct/Cetak?StartDate=' + startdate + '&EndDate=' + enddate + '&outletId=' +  branch;
        var res=$http.get(url , {responseType: 'arraybuffer'});

        return res;
	};

	return factory;
});