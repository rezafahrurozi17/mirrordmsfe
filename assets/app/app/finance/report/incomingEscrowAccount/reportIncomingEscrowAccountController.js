angular.module('app')
	.controller('ReportIncomingEscrowAccountController', function ($scope, $http, $filter, CurrentUser, ReportIncomingEscrowAccountFactory, $timeout, bsNotify) {
		$scope.user = CurrentUser.user();
		$scope.ReportIncomingEscrowAccount_Show = true;
		$scope.dateNow = Date.now();
		$scope.ReportIncomingEscrowAccount_TanggalStart = new Date();
		$scope.ReportIncomingEscrowAccount_TanggalEnd = new Date();

		$scope.SetComboBranch = function () {
			ReportIncomingEscrowAccountFactory.getDataBranchRptIncomingEscrow().then(
				function (res) {
					$scope.optionsReportIncomingEscrowAccount_NamaBranch = res.data.Result;
					if ($scope.user.OrgCode.substring(3, 9) == '000000') {
						$scope.Main_ReportIncomingEscrowAccount_EnableDisable = true;
					}
					$scope.ReportIncomingEscrowAccount_NamaBranch = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		$scope.SetComboBranch();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		}


		//di hilangkan dari report. karna tidak perlu cetak
		// $scope.reportIncomingEscrowAccount_buttonCetak_Clicked = function() {         
		// 	var pdfFile="";  
		// 	var enddate = "";

		// 	if ( (isNaN($scope.ReportIncomingEscrowAccount_TanggalStart) == true) ||  
		// 		(angular.isUndefined($scope.ReportIncomingEscrowAccount_TanggalStart) == true) ||
		// 		( $scope.ReportIncomingEscrowAccount_TanggalStart==null)  ) 
		// 	{
		// 		alert('Tanggal harus di isi !');
		// 	}
		// 	else 
		// 	{
		// 		if ( (isNaN($scope.ReportIncomingEscrowAccount_TanggalEnd) == true) ||  
		// 		(angular.isUndefined($scope.ReportIncomingEscrowAccount_TanggalEnd) == true) ||
		// 		( $scope.ReportIncomingEscrowAccount_TanggalEnd==null)  ) 
		// 		{
		// 			enddate = ""; 
		// 		}			
		// 		else 
		// 		{
		// 			enddate = $scope.formatDate ( $scope.ReportIncomingEscrowAccount_TanggalEnd);
		// 		}

		// 		ReportIncomingEscrowAccountFactory.getCetakReportIncomingEscrowAccount(
		// 			$scope.formatDate($scope.ReportIncomingEscrowAccount_TanggalStart) 
		// 				,enddate, $scope.ReportIncomingEscrowAccount_NamaBranch ).then(
		// 				function (res) {
		// 					var file = new Blob([res.data], {type: 'application/pdf'});
		// 					var fileURL = URL.createObjectURL(file);
		// 					console.log("pdf", fileURL);
		// 					printJS(fileURL);
		// 					//printJS({printable: file,showModal:true,type: 'pdf'})
		// 				},
		// 				function(err){
		// 						console.log("err=>",err);
		// 				}
		// 		);
		// 	}
		// };

		$scope.reportIncomingEscrowAccount_buttonExcel_Clicked = function () {
			var enddate = "";
			if (((isNaN($scope.ReportIncomingEscrowAccount_TanggalStart) == true) ||
				(angular.isUndefined($scope.ReportIncomingEscrowAccount_TanggalStart) == true) ||
				($scope.ReportIncomingEscrowAccount_TanggalStart == null)) || ((isNaN($scope.ReportIncomingEscrowAccount_TanggalEnd) == true) ||
					(angular.isUndefined($scope.ReportIncomingEscrowAccount_TanggalEnd) == true) ||
					($scope.ReportIncomingEscrowAccount_TanggalEnd == null))) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal harus diisi / format tanggal salah.",
					type: "warning"
				});
			}
			else if ((new Date($scope.ReportIncomingEscrowAccount_TanggalStart) > new Date()) ||
				(new Date($scope.ReportIncomingEscrowAccount_TanggalEnd) > new Date())) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if (new Date($scope.ReportIncomingEscrowAccount_TanggalStart) > new Date($scope.ReportIncomingEscrowAccount_TanggalEnd)) {

				bsNotify.show({
					title: "Warning",
					content: "Tanggal awal tidak boleh lebih dari tanggal akhir.",
					type: "warning"
				});
			}
			else {

				enddate = $scope.formatDate($scope.ReportIncomingEscrowAccount_TanggalEnd);
				ReportIncomingEscrowAccountFactory.getExcelReportIncomingEscrowAccount(
					$scope.formatDate($scope.ReportIncomingEscrowAccount_TanggalStart), enddate,
					$scope.ReportIncomingEscrowAccount_NamaBranch).then(
						function (res) {
							var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
							var b64Data = res.data;

							var blob = b64toBlob(b64Data, contentType);
							var blobUrl = URL.createObjectURL(blob);
							// window.open(blobUrl);
							//$scope.sm_show2=false;
							saveAs(blob, 'Report_IncomingEscrow' + '.xlsx');

							return res;
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
		}

	});