angular.module('app')
.controller('ReportPenggunaanNomorFakturPajakController', function ($scope, $http, $filter, CurrentUser, ReportPenggunaanNomorFakturPajakFactory, $timeout) {
	user = CurrentUser.user();
	$scope.optionsReportFakturPajak_Status = [{name:"Semua", value:"Semua"}, {name:"Tersedia", value:"Tersedia"}, {name:"Digunakan", value:"Digunakan"}];
	
	var user = CurrentUser.user();
	if(user.OrgCode.substring(3, 9) == '000000')
		$scope.ReportFakturPajak_Branch_isDisabled = true;
	else
		$scope.ReportFakturPajak_Branch_isDisabled = false;


	$scope.ReportPenggunaanNomorFakturPajakFactory_GetDataBranchByOutletId = function(){
		ReportPenggunaanNomorFakturPajakFactory.getBranchByOutletId(user.OutletId)
		.then(
			function(res){
				console.log("res data BranchName : ", res.data.Result[0]);
				$scope.optionsMainPenggunaanNomorFakturPajak_SelectBranch = res.data.Result;
				// $scope.ReportFakturPajak_Branch = res.data.Result[0].BranchId;
				console.log("res data Branc ", 	$scope.ReportFakturPajak_Branch);
			}
		);
	}

	$scope.RegistrasiDokumenPajak_getDataBranch = function () {
		ReportPenggunaanNomorFakturPajakFactory.getDataBranchComboBox()
			.then
			(
			function (res) {
				
				$scope.optionsMainPenggunaanNomorFakturPajak_SelectBranch = res.data;
				$scope.ReportFakturPajak_Branch = user.OutletId;

				res.data.some(function (obj, i){
					if(obj.value == user.OutletId)
						$scope.ReportFakturPajak_Branch = obj.value;	
						console.log("cek Branch", $scope.ReportFakturPajak_Branch)
				});

				

			},
			function (err) {
				console.log("err=>", err);
			}
			);
	};

	$scope.RegistrasiDokumenPajak_getDataBranch();

	$scope.ReportFakturPajakTanggalAwalOK = true;
	$scope.ReportFakturPajakTanggalAwalChanged = function () {
		
		console.log($scope.ReportFakturPajakTanggalAwalOK);
		var dt = $scope.ReportFakturPajak_TanggalAwal;
		var now = new Date();
		 if (dt > now) {
			$scope.errReportFakturPajakTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.ReportFakturPajakTanggalAwalOK = false;			
		}
		else {
			$scope.ReportFakturPajakTanggalAwalOK = true;
		}
		console.log('asdasd ' + $scope.ReportFakturPajakTanggalAwalOK);
	}
	$scope.ReportFakturPajakTanggalAkhirOK = true;
	$scope.ReportFakturPajakTanggalAkhirChanged = function () {
		
		console.log($scope.ReportFakturPajakTanggalAkhirOK);
		var dt = $scope.ReportFakturPajak_TanggalAkhir;
		var now = new Date();
		 if (dt > now) {
			$scope.errReportFakturPajakTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.ReportFakturPajakTanggalAkhirOK = false;			
		}
		else {
			$scope.ReportFakturPajakTanggalAkhirOK = true;
		}
		console.log('asdasd ' + $scope.ReportFakturPajakTanggalAkhirOK);
	}
	
	$scope.ReportPenggunaanNomorFakturPajakFactory_GetDataBranchByOutletId();
	
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.Report_Excel_Clicked=function() {
		var isCanceled = 0;
		
		if($scope.ReportFakturPajak_PernahBatal)
			isCanceled = 1;
		
		ReportPenggunaanNomorFakturPajakFactory.getExcelReportReportPenggunaanNomorFakturPajak($filter('date')(new Date($scope.ReportFakturPajak_TanggalAwal), "yyyyMMdd"),
				$filter('date')(new Date($scope.ReportFakturPajak_TanggalAkhir), "yyyyMMdd"), $scope.ReportFakturPajak_Status, isCanceled,$scope.ReportFakturPajak_Branch)
		.then(
			function(res){

				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
				var b64Data = res.data;

				var blob = b64toBlob(b64Data, contentType);
				var blobUrl = URL.createObjectURL(blob);

				saveAs(blob, 'Report_PenggunaanFakturPajak' + '.xlsx');

				return res;
			},
			function(err){
				console.log("err=>",err);
			}
		);
	}

});