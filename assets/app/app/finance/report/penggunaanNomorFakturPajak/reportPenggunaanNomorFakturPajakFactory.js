angular.module('app')
	.factory('ReportPenggunaanNomorFakturPajakFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;


	factory.getExcelReportReportPenggunaanNomorFakturPajak=function(dateStart,dateEnd, status, isCanceled,OutletId) {
        var url = '/api/fe/FinanceRptTaxInvoice/Excel/?DateStart=' + dateStart + '&dateend=' + dateEnd + '&status=' + status + '&iscanceled=' + isCanceled+'&OutletId='+OutletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getDataBranchComboBox=function () {
		console.log("factory.getDataBranchComboBox");
		var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
		var res = $http.get(url);
		return res;
	},
	
	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};
	
	return factory;
});