angular.module('app')
.controller('ReportPenjualanUnitController', function ($scope, $http, $filter, CurrentUser, ReportPenjualanUnitFactory, $timeout,bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.MainReportPenjualanUnit_Show = true;
	$scope.ReportPenjualanUnit_PelangganFleetOptions = [{name : "Semua", value : "Semua"},{name : "Fleet", value : "Fleet"}, {name :"NON Fleet", value:"NON Fleet"}];
	$scope.ReportPenjualanUnit_MetodePembayaran = null;

	var user = CurrentUser.user();
	if(user.OrgCode.substring(3, 9) == '000000')
		$scope.Main_NamaBranch_EnableDisable = true;
	else
		$scope.Main_NamaBranch_EnableDisable = false;

	$scope.SetComboBranch=function() {
		ReportPenjualanUnitFactory.getDataBranchRptPenjualanUnit().then(
			function(res) {
				$scope.optionsReportPenjualanUnit_NamaBranch = res.data.Result;	
				$scope.ReportPenjualanUnit_NamaBranch = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};
	
	// $scope.TanggalPenjualanAwalOK = true;
	// $scope.TanggalPenjualanAwalChanged = function () {
		
	// 	console.log($scope.TanggalPenjualanAwalOK);
	// 	var dt = $scope.ReportPenjualanUnit_TanggalStart;
	// 	var now = new Date();
	// 	 if (dt > now) {
	// 		$scope.errTanggalPenjualanAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
	// 		$scope.TanggalPenjualanAwalOK = false;
	// 	}
	// 	else {
	// 		$scope.TanggalPenjualanAwalOK = true;
	// 	}
	// 	console.log('asdasd ' + $scope.TanggalPenjualanAwalOK);
	// }

	// $scope.TanggalPenjualanAkhirOK = true;
	// $scope.TanggalPenjualanAkhirChanged = function () {
		
	// 	console.log($scope.TanggalPenjualanAkhirOK);
	// 	var dt = $scope.ReportPenjualanUnit_TanggalEnd;
	// 	var now = new Date();
	// 	 if (dt > now) {
	// 		$scope.errTanggalPenjualanAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
	// 		$scope.TanggalPenjualanAkhirOK = false;			
	// 	}
	// 	else {
	// 		$scope.TanggalPenjualanAkhirOK = true;
	// 	}
	// 	console.log('asdasd ' + $scope.TanggalPenjualanAkhirOK);
	// }
	$scope.ReportPenjualanUnit_PelangganFleet = "Semua"
	$scope.SetComboBranch();

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.ReportPenjualanUnit_Cetak_Clicked = function() {         
			var pdfFile="";  
			var endDate="";

			if ( !(
				(isNaN($scope.ReportPenjualanUnit_TanggalStart) == true) ||  
					 (angular.isUndefined($scope.ReportPenjualanUnit_TanggalStart) == true) ||
					 ( $scope.ReportPenjualanUnit_TanggalStart==null)  )
			&& ( ! ((  (angular.isUndefined($scope.ReportPenjualanUnit_PelangganFleet) == true) ||
					 ( $scope.ReportPenjualanUnit_PelangganFleet==null)  ) )
			)) 	
			{
				if	 ( (isNaN($scope.ReportPenjualanUnit_TanggalEnd) != true) && 
				(angular.isUndefined($scope.ReportPenjualanUnit_TanggalEnd) != true) )
				{
					endDate = $scope.formatDate($scope.ReportPenjualanUnit_TanggalEnd);
				}
				else {
					endDate = $scope.formatDate(new Date());
				}

				ReportPenjualanUnitFactory.getCetakReportPenjualanUnit(
					$scope.formatDate($scope.ReportPenjualanUnit_TanggalStart), 
					endDate, $scope.ReportPenjualanUnit_NamaBranch, $scope.ReportPenjualanUnit_PelangganFleet ).then(
					function (res) {
					var file = new Blob([res.data], {type: 'application/pdf'});
					var fileURL = URL.createObjectURL(file);
					//window.open(fileURL);

					console.log("pdf", fileURL);
					//$scope.content = $sce.trustAsResourceUrl(fileURL);
					//pdfFile = fileURL;

					printJS(fileURL);
					//printJS({printable: file,showModal:true,type: 'pdf'})
				},
				function(err){
						console.log("err=>",err);
						if (err != null) 
						{
							alert(err.data.Message);
						}	
						else {
							alert('Gagal membuat laporan !');
						}
					}
				);
			}
			else {
				alert('Data harus lengkap di isi !');
			}
	};
	$scope.ReportPenjualanUnit_PelangganFleet_Changed=function(){
		if(angular.isUndefined($scope.ReportPenjualanUnit_PelangganFleet))
		{
			$scope.ReportPenjualanUnit_PelangganFleet="Semua";
		}
	}


	$scope.ReportPenjualanUnit_Excel_Clicked=function() {
			var endDate="";
			if (angular.isUndefined($scope.ReportPenjualanUnit_TanggalStart)==true && (angular.isUndefined($scope.ReportPenjualanUnit_TanggalEnd)==true))
		{
			bsNotify.show({
				title:"Warning",
				content:"Tanggal harus diisi / format tanggal salah.",
				type:"warning"
			});
		}
		else if(new Date($scope.ReportPenjualanUnit_TanggalStart) > new Date()){
			bsNotify.show({
				title:"Warning",
				content:"Tanggal tidak bisa lebih besar dari hari ini.",
				type:"warning"
			});
		}
		else if (new Date($scope.ReportPenjualanUnit_TanggalEnd) > new Date())
		{
			bsNotify.show({
				title:"Warning",
				content:"Tanggal tidak bisa lebih besar dari hari ini.",
				type:"warning"
			});
		}		
		else if (new Date($scope.ReportPenjualanUnit_TanggalStart) > new Date($scope.ReportPenjualanUnit_TanggalEnd))
		{
			bsNotify.show({
				title:"Warning",
				content:"Tangga lawal tidak bisa lebih besar dari Tanggal Akhir.",
				type:"warning"
			});
		}
		else if (angular.isUndefined($scope.ReportPenjualanUnit_NamaBranch)==true)
		{
			bsNotify.show({
				title:"Warning",
				content:"Branch harus diisi",
				type:"warning"
			});
		}
		else{

			ReportPenjualanUnitFactory.getExcelReportPenjualanUnit($scope.formatDate( $scope.ReportPenjualanUnit_TanggalStart), 
			$scope.formatDate( $scope.ReportPenjualanUnit_TanggalEnd) , $scope.ReportPenjualanUnit_NamaBranch, $scope.ReportPenjualanUnit_PelangganFleet ).then(
					function(res){
						var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
						var b64Data = res.data;

						var blob = b64toBlob(b64Data, contentType);
						var blobUrl = URL.createObjectURL(blob);
						// window.open(blobUrl);
						//$scope.sm_show2=false;
						saveAs(blob, 'Report_PenjualanUnit' + '.xlsx');

						return res;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}			

		// 	if ( !(
		// 		(isNaN($scope.ReportPenjualanUnit_TanggalStart) == true) ||  
		// 			 (angular.isUndefined($scope.ReportPenjualanUnit_TanggalStart) == true) ||
		// 			 ( $scope.ReportPenjualanUnit_TanggalStart==null)  )
		// 	&& ( ! ((  (angular.isUndefined($scope.ReportPenjualanUnit_PelangganFleet) == true) ||
		// 			 ( $scope.ReportPenjualanUnit_PelangganFleet==null)  ) )
		// 	)) 	
		// 	{
		// 		if	 ( (isNaN($scope.ReportPenjualanUnit_TanggalEnd) != true) && 
		// 		(angular.isUndefined($scope.ReportPenjualanUnit_TanggalEnd) != true) )
		// 		{
		// 			endDate = $scope.formatDate($scope.ReportPenjualanUnit_TanggalEnd);
		// 		}
		// 		else {
		// 			endDate = $scope.formatDate(new Date());
		// 		}

		// 		ReportPenjualanUnitFactory.getExcelReportPenjualanUnit($scope.formatDate( $scope.ReportPenjualanUnit_TanggalStart), 
		// 			endDate , $scope.ReportPenjualanUnit_NamaBranch, $scope.ReportPenjualanUnit_PelangganFleet ).then(
		// 			function(res){
		// 				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
		// 				var b64Data = res.data;

		// 				var blob = b64toBlob(b64Data, contentType);
		// 				var blobUrl = URL.createObjectURL(blob);
		// 				// window.open(blobUrl);
		// 				//$scope.sm_show2=false;
		// 				saveAs(blob, 'Report_PenjualanUnit' + '.xlsx');

		// 				return res;
		// 			},
		// 			function(err){
		// 				console.log("err=>",err);
		// 			}
		// 		);
		// 	}
		//  else { 
		//  	alert('Data harus lengkap di isi !');
		//  }
	}

});