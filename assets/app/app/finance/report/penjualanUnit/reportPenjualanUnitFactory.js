angular.module('app')
	.factory('ReportPenjualanUnitFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchRptPenjualanUnit=function() {
        //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
        var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
        var res=$http.get(url);  
        return res;			
        };

        factory.getExcelReportPenjualanUnit=function(startdate,enddate,branch,fleet ) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenjualanUnit/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&Fleet=' + 
                        fleet + '&outletId=' +  branch ;
                var res=$http.get(url);
                return res;
	};

	factory.getCetakReportPenjualanUnit=function(startdate,enddate,branch,fleet) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenjualanUnit/Cetak?StartDate=' + startdate + '&EndDate=' + enddate + '&Fleet=' + 
                        fleet + '&outletId=' +  branch ;
                var res=$http.get(url , {responseType: 'arraybuffer'});
                return res;
	};

	return factory;
});