angular.module('app')
.controller('DaftarRincianKendaraanController', function ($scope, $http, $filter, CurrentUser, DaftarRincianKendaraanFactory, $timeout) {
	var	masaPajak = [];
	for(var i = 1; i < 13; i++) {
		masaPajak[i] = {name : i, value : i};
	}

	$scope.user = CurrentUser.user();
	var orgId = $scope.user.OrgId;
	$scope.MainReportRincianKB_Show = true;
	$scope.optionsFinanceReportRincianKBMasaPajak = masaPajak;
	$scope.SetComboBranch=function() {
		DaftarRincianKendaraanFactory.getDataBranchDaftarRincianKendaraan(orgId).then(
			function(res) {
				$scope.optionsFinanceReportRincianKB_NamaBranch = res.data.Result;	
				$scope.FinanceReportRincianKB_NamaBranch = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};
	
	
	$scope.SetComboBranch();

	$scope.FinanceReportRincianKB_NamaBranch_change = function(){
		if(isUndefined( $scope.FinanceReportRincianKB_NamaBranch)){
			$scope.FinanceReportRincianKB_NamaBranch = "";
		}
	}

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.ReportPenjualanUnit_Cetak_Clicked = function() {         
			var pdfFile="";  
			var endDate="";

			if ( !(
				(isNaN($scope.ReportPenjualanUnit_TanggalStart) == true) ||  
					 (angular.isUndefined($scope.ReportPenjualanUnit_TanggalStart) == true) ||
					 ( $scope.ReportPenjualanUnit_TanggalStart==null)  )
			&& ( ! ((  (angular.isUndefined($scope.ReportPenjualanUnit_PelangganFleet) == true) ||
					 ( $scope.ReportPenjualanUnit_PelangganFleet==null)  ) )
			)) 	
			{
				if	 ( (isNaN($scope.ReportPenjualanUnit_TanggalEnd) != true) && 
				(angular.isUndefined($scope.ReportPenjualanUnit_TanggalEnd) != true) )
				{
					endDate = $scope.formatDate($scope.ReportPenjualanUnit_TanggalEnd);
				}
				else {
					endDate = $scope.formatDate(new Date());
				}

				ReportPenjualanUnitFactory.getCetakReportPenjualanUnit(
					$scope.formatDate($scope.ReportPenjualanUnit_TanggalStart), 
					endDate, $scope.ReportPenjualanUnit_NamaBranch, $scope.ReportPenjualanUnit_PelangganFleet ).then(
					function (res) {
					var file = new Blob([res.data], {type: 'application/pdf'});
					var fileURL = URL.createObjectURL(file);
					//window.open(fileURL);

					console.log("pdf", fileURL);
					//$scope.content = $sce.trustAsResourceUrl(fileURL);
					//pdfFile = fileURL;

					printJS(fileURL);
					//printJS({printable: file,showModal:true,type: 'pdf'})
				},
				function(err){
						console.log("err=>",err);
					}
				);
			}
			else {
				alert('Data harus lengkap di isi !');
			}
	};

	$scope.FinanceReportRincianKB_Excel_Clicked=function() {

		DaftarRincianKendaraanFactory.getExcelReportRincianKB($scope.FinanceReportRincianKB_MasaPajak, $scope.FinanceReportRincianKB_TahunPajak, $scope.FinanceReportRincianKB_NamaBranch,
			$scope.FinanceReportRincianKB_NomorFakturPajakAwal, $scope.FinanceReportRincianKB_NomorFakturPajakAkhir, $scope.FinanceReportRincianKB_NamaPembeli,
			$scope.FinanceReportRincianKB_NPWPPembeli, $scope.FinanceReportRincianKB_NomorRangka, $scope.FinanceReportRincianKB_NomorMesin, $scope.FinanceReportRincianKB_MerkTipe,
			$scope.FinanceReportRincianKB_NomorBilling)
			.then(
				function(res){
					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);
					// window.open(blobUrl);
					//$scope.sm_show2=false;
					saveAs(blob, 'Report_DaftarRincianKendaraan' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
			}
		);
	}

});