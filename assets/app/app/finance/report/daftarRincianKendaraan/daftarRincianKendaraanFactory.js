angular.module('app')
	.factory('DaftarRincianKendaraanFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchDaftarRincianKendaraan=function(orgId) {
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
			var res=$http.get(url);  
			return res;			
        };

        factory.getExcelReportRincianKB=function(TaxMonth,TaxDate,Branch,StartTaxNumber, EndTaxNumber, CustomerName, CustomerNPWP, FrameNo, EngineNo, Model, BillingNo ) {
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var url = '/api/fe/FinanceRptVehicleDetail/Excel?TaxMonth=' + TaxMonth + '&TaxDate=' + TaxDate + '&OutletId=' + Branch + '&StartTaxNumber='+ StartTaxNumber +
								'&EndTaxNumber=' + EndTaxNumber + '&CustomerName=' + CustomerName + '&CustomerNPWP=' + CustomerNPWP + '&FrameNo=' + FrameNo +
								'&EngineNo=' + EngineNo + '&BillingNo=' + BillingNo;
			var res=$http.get(url);
			return res;
		};

	return factory;
});