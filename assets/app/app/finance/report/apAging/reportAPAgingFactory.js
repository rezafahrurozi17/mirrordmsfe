angular.module('app')
	.factory('ReportAPAgingFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataBranchRptAPAging=function(orgId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+orgId;
      var res=$http.get(url);   
      return res;			
    };
 
	factory.getExcelReportAPAging=function(startdate,branch) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/FFinanceRPTAPAging/Excel?StartDate=' + startdate + '&outletId=' +  branch;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getCetakReportAPAging=function(startdate,branch) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/FFinanceRPTAPAging/Cetak?StartDate=' + startdate + '&outletId=' +  branch;
        var res=$http.get(url , {responseType: 'arraybuffer'});

        return res;
	};

	return factory;
});