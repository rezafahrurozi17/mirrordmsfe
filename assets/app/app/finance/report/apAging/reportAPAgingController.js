angular.module('app')
	.controller('ReportAPAgingController', function ($scope, $http, $filter, bsNotify, CurrentUser, ReportAPAgingFactory, $timeout) {
		$scope.user = CurrentUser.user();
		$scope.reportAPAging_TanggalStart = new Date();
		$scope.MainreportAPAging_Show = true;
		$scope.reportAPAging_NamaBranch_EnableDisable = true;

		$scope.right = function (str, chr) {
			    return str.slice(str.length - chr, str.length);
			  }

		$scope.SetComboBranch = function () {
			
			var ho;
			    if ($scope.right($scope.user.OrgCode, 6) == '000000') {
				      $scope.reportAPAging_NamaBranch_EnableDisable = true;
				      ho = $scope.user.OrgId;
			    }
			    else {
				      $scope.reportAPAging_NamaBranch_EnableDisable = false;
				      ho = 0;
			    }
			
			ReportAPAgingFactory.getDataBranchRptAPAging(ho).then(
				function (res) {
					$scope.optionsReportAPAging_NamaBranch = res.data.Result;
					$scope.ReportAPAging_NamaBranch = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		$scope.SetComboBranch();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.reportAPAging_ButtonCetak_Clicked = function () {
			var pdfFile = "";
			if ((isNaN($scope.reportAPAging_TanggalStart) == true) ||
				(angular.isUndefined($scope.reportAPAging_TanggalStart) == true) ||
				($scope.reportAPAging_TanggalStart == null)) {
				alert('Tanggal harus di isi !');
			}
			else {
				ReportAPAgingFactory.getCetakReportAPAging($scope.formatDate($scope.reportAPAging_TanggalStart),
					$scope.ReportAPAging_NamaBranch).then(
						function (res) {
							var file = new Blob([res.data], { type: 'application/pdf' });
							var fileURL = URL.createObjectURL(file);
							console.log("pdf", fileURL);
							printJS(fileURL);
							//printJS({printable: file,showModal:true,type: 'pdf'})
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
		};

		$scope.reportAPAging_ButtonExcel_Clicked = function () {
			if ((isNaN($scope.reportAPAging_TanggalStart) == true) ||
				(angular.isUndefined($scope.reportAPAging_TanggalStart) == true) ||
				($scope.reportAPAging_TanggalStart == null)) {
				// alert('Tanggal harus di isi !');
				bsNotify.show(
					{
						title: "Warning",
						content: "Tanggal harus di isi !",
						type: 'warning'
					}
				);
			}else if($scope.ReportAPAging_NamaBranch == null || $scope.ReportAPAging_NamaBranch == undefined || $scope.ReportAPAging_NamaBranch == ""){
				bsNotify.show(
					{
						title: "Warning",
						content: "Nama Branch Harus di pilih",
						type: 'warning'
					}
				);
				return false;
			}
			else {
				ReportAPAgingFactory.getExcelReportAPAging($scope.formatDate($scope.reportAPAging_TanggalStart),
					$scope.ReportAPAging_NamaBranch).then(
						function (res) {
							var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
							var b64Data = res.data;

							var blob = b64toBlob(b64Data, contentType);
							var blobUrl = URL.createObjectURL(blob);
							// window.open(blobUrl);
							//$scope.sm_show2=false;
							saveAs(blob, 'Report_APAging' + '.xlsx');

							return res;
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
		}

		$scope.tanggalOK = true;
		$scope.tanggalOKLenght = 0;
		$scope.tanggal_Changed = function () {
			var dt = $scope.reportAPAging_TanggalStart;

			var now = new Date();

			if (dt > now) {
				$scope.errTanggal = "Tanggal tidak boleh lebih besar dari tanggal hari ini";
				$scope.tanggalOK = false;
				$scope.tanggalOKLenght = 0;
			}
			else {
				$scope.tanggalOK = true;
				$scope.tanggalOKLenght = 1;
			}
		}

	});