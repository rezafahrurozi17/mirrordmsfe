angular.module('app')
.controller('ReportArAgingController', function ($scope, $http, $filter, CurrentUser, ReportArAgingFactory, $timeout,bsNotify) {
	var user = CurrentUser.user();
	$scope.optionsReportArAgingTipeBisnis = [{name:"Unit - New Unit", value:"Unit- New Unit"}, {name:"Unit - Purna Jual", value:"Unit - Purna Jual"}, {name:"Unit - OPD", value:"Unit - OPD"}, {name:"Unit - AR Others", value:"Unit - AR Others"}, {name:"Service", value:"Service"}, {name:"Parts", value:"Parts"} ];
	$scope.optionsReportArAgingKategoriPelanggan = [{name:"Semua", value:"Semua"}, {name:"Individu", value:"Individu"}, {name:"Perusahaan", value:"Perusahaan"}, {name:"Pemerintah", value:"Pemerintah"}, {name:"Yayasan", value:"Yayasan"}];
	$scope.optionsReportArAgingPelangganPKS = [{name:"Semua", value:"Semua"}, {name:"PKS", value:"PKS"}, {name:"Non PKS", value:"NonPKS"}];
	$scope.dateNow = Date.now();
	$scope.ReportArAging_Tanggal = new Date();
	$scope.ReportArAging_HariDalamSetahun = 360;
	$scope.ReportArAging_Penalti = 0;
	$scope.ReportArAging_Due1 = 30;
	$scope.ReportArAging_Due2 = 60;
	$scope.ReportArAging_Due3 = 90;
	$scope.ReportArAging_Due4 = 120;
	$scope.ReportArAging_Due5 = 150;	
	$scope.Branch_HO_Show = true;
	$scope.ReportArAging_Branch_isDisabled = true;
	// if (user.OrgCode.substring(3, 9) == '000000'){
	// 	$scope.Branch_HO_Show = true;
	// 	$scope.NamaBranch = false;
	// }
	// else{
	// 	$scope.Branch_HO_Show = false;
	// 	$scope.NamaBranch = true;
	// }
	//Get Branch 
		$scope.getDataBranch = function () {
			ReportArAgingFactory.getDataBranchComboBox() 
				.then
				(
				function (res) {
					$scope.optionsMainARAging_SelectBranch = res.data;
					console.log("user.OutletId", user.OutletId);
					console.log("res data branch", res.data);
					console.log(res.data[0].value);
					console.log('di atas gan');
					// res.data.some(function(obj, i){
					// if(obj.value == user.OutletId)
					// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
					// });
					$timeout(function(){
						$scope.ReportArAging_Branch = user.OutletId.toString();
					},100)
					console.log("$scope.MainInvoiceMasuk_SelectBranch : ", $scope.ReportArAging_Branch);
					res.data.some(function (obj, i) {
						if (obj.value == user.OutletId)
							$scope.ReportArAging_Branch = obj.name;
					});
				},
				function (err) {
					console.log("err=>", err);
				}
				);

			if (user.OrgCode.substring(3, 9) == '000000') {
				$scope.ReportArAging_Branch_isDisabled = true;
			}
			else {
				$scope.ReportArAging_Branch_isDisabled = false;
				$scope.Main_NamaBranch_EnableDisable = false;
			}
		};
		$scope.getDataBranch();

	$scope.ReportArAging_GetDataBranchByOutletId = function(){
		ReportArAgingFactory.getBranchByOutletId(user.OutletId)
		.then(
			function(res){
				console.log("res data BranchName : ", res.data.Result[0]);
				$scope.ReportArAging_Branch = res.data.Result[0].BranchName;
			} 
		);
	}
	
	$scope.ReportArAging_TipeBisnis_Changed = function(){
		if($scope.ReportArAging_TipeBisnis == "Service" || $scope.ReportArAging_TipeBisnis == "Parts")
			$scope.ReportArAging_PelangganPKS_isDisabled = false;
		else
			$scope.ReportArAging_PelangganPKS_isDisabled = true;
	}
	
	$scope.ReportArAging_GetDataBranchByOutletId();
	
	function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	// $scope.Report_Cetak_Clicked = function() {         
			// var pdfFile="";  
			// var endDate="";

            // ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
				// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			// .then(
				// function (res) {
                // var file = new Blob([res.data], {type: 'application/pdf'});
                // var fileURL = URL.createObjectURL(file);

                // console.log("pdf", fileURL);


				// printJS(fileURL);
            // },
			// function(err){
					// console.log("err=>",err);
				// }
			// );
	// };
	
	$scope.ReportArAging_MandatoryChecked = function(){
		var result = false;
		if(angular.isUndefined($scope.ReportArAging_Tanggal)){
			bsNotify.show({
				title:"Warning",
				content:"Tanggal harus diisi / format tanggal salah.",
				type:"warning"
			});
			return(result);
		}

		if((new Date($scope.ReportArAging_Tanggal) > new Date())){
				bsNotify.show({
					title:"Warning",
					content:"Tanggal tidak bisa lebih besar dari hari ini.",
					type:"warning"
				});
			return(result);
		}
		
		if(angular.isUndefined($scope.ReportArAging_TipeBisnis)){
			bsNotify.show({
				title:"Warning",
				content:"Tipe bisnis harus dipilih.",
				type:"warning"
			});
			return(result);
		}
		
		if(angular.isUndefined($scope.ReportArAging_TipeReport)){
			bsNotify.show({
				title:"Warning",
				content:"Tipe report harus dipilih.",
				type:"warning"
			});
			return(result);
		}
		console.log("Jumlah hari dalam setahun : ", $scope.ReportArAging_HariDalamSetahun);
		if($scope.ReportArAging_HariDalamSetahun <= 0 || angular.isUndefined($scope.ReportArAging_HariDalamSetahun)){
			bsNotify.show({
				title:"Warning",
				content:"Jumlah hari dalam setahun harus lebih besar dari 0.",
				type:"warning"
			});
			return(result);
		}
		
		if(angular.isUndefined($scope.ReportArAging_Due1))
			$scope.ReportArAging_Due1 = 0;
		if(angular.isUndefined($scope.ReportArAging_Due2))
			$scope.ReportArAging_Due2 = 0;
		if(angular.isUndefined($scope.ReportArAging_Due3))
			$scope.ReportArAging_Due3 = 0;
		if(angular.isUndefined($scope.ReportArAging_Due4))
			$scope.ReportArAging_Due4 = 0;
		if(angular.isUndefined($scope.ReportArAging_Due5))
			$scope.ReportArAging_Due5 = 0;
		
		if($scope.ReportArAging_Due1 <= 0){
			//alert("Due date 1 harus lebih besar dari 0");
			bsNotify.show({
				title:"Warning",
				content:"Due date 1 harus lebih besar dari 0.",
				type:"warning"
			});
			return(result);
		}
		
		if($scope.ReportArAging_Due2 == 0 || parseInt($scope.ReportArAging_Due2, 10) <= parseInt($scope.ReportArAging_Due1, 10)){
			//alert("Due date 2 harus lebih besar dari 0 dan due date 1");
			bsNotify.show({
				title:"Warning",
				content:"Due date 3 harus lebih besar dari 0 dan due date 2.",
				type:"warning"
			});
			return(result);
		}
		
		if($scope.ReportArAging_Due3 == 0 || parseInt($scope.ReportArAging_Due3, 10) <= parseInt($scope.ReportArAging_Due2, 10)){
			//alert("Due date 3 harus lebih besar dari 0 dan due date 2");
			bsNotify.show({
				title:"Warning",
				content:"Due date 3 harus lebih besar dari 0 dan due date 2.",
				type:"warning"
			});
			return(result);
		}
		console.log("due 4", $scope.ReportArAging_Due4);
		if($scope.ReportArAging_Due4 == 0 || parseInt($scope.ReportArAging_Due4, 10) <= parseInt($scope.ReportArAging_Due3, 10)){
			//alert("Due date 4 harus lebih besar dari 0 dan due date 3");
			bsNotify.show({
				title:"Warning",
				content:"Due date 4 harus lebih besar dari 0 dan due date 3.",
				type:"warning"
			});
			return(result);
		}
		
		if($scope.ReportArAging_Due5 == 0 || parseInt($scope.ReportArAging_Due5, 10) <= parseInt($scope.ReportArAging_Due4, 10)){
			//alert("Due date 5 harus lebih besar dari 0 dan due date 4");
			bsNotify.show({
				title:"Warning",
				content:"Due date 5 harus lebih besar dari 0 dan due date 4.",
				type:"warning"
			});
			return(result);
		}
		
		result = true;
		return(result);
	}
	
	$scope.Report_Excel_Clicked=function() {
		if(!$scope.ReportArAging_MandatoryChecked())
			return;
		
		if ($scope.ReportArAging_TipeReport == 1){
			ReportArAgingFactory.getExcelReportARAgingSummary($filter('date')(new Date($scope.ReportArAging_Tanggal), "yyyyMMdd"),
					$scope.ReportArAging_TipeBisnis, $scope.ReportArAging_Penalti, $scope.ReportArAging_HariDalamSetahun, 
					$scope.ReportArAging_Due1, $scope.ReportArAging_Due2, $scope.ReportArAging_Due3, $scope.ReportArAging_Due4, $scope.ReportArAging_Due5,
					$scope.ReportArAging_KategoriPelanggan, $scope.ReportArAging_PelangganPKS,$scope.ReportArAging_Branch)
			.then(
				function(res){

					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);

					saveAs(blob, 'Report_ARAgingSummary' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
					alert(err.data.ExceptionMessage);
				}
			);
		}
		else{
			ReportArAgingFactory.getExcelReportARAgingDetail($filter('date')(new Date($scope.ReportArAging_Tanggal), "yyyyMMdd"),
					$scope.ReportArAging_TipeBisnis, $scope.ReportArAging_Penalti, $scope.ReportArAging_HariDalamSetahun, 
					$scope.ReportArAging_Due1, $scope.ReportArAging_Due2, $scope.ReportArAging_Due3, $scope.ReportArAging_Due4, $scope.ReportArAging_Due5,
					$scope.ReportArAging_KategoriPelanggan, $scope.ReportArAging_PelangganPKS,$scope.ReportArAging_Branch)
			.then(
				function(res){

					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);

					saveAs(blob, 'Report_ARAgingDetail' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
	}

});