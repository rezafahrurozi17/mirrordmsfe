angular.module('app')
	.factory('ReportArAgingFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		
	
	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};
	
	factory.getExcelReportARAgingSummary=function(date, businessTypeId, penalty, dayOfYear, dueDate1, dueDate2, dueDate3, dueDate4, dueDate5, customerCategory, pks, outletId) {
        var url = '/api/fe/FinanceRptARAging/ExcelSummary/?Date='+date+'&BusinessType='+businessTypeId+'&Penalty='+penalty+'&DayofYear='+dayOfYear+'&duedate1='+dueDate1+'&duedate2='+dueDate2+'&duedate3='+dueDate3+'&duedate4='+dueDate4+'&duedate5='+dueDate5+'&customercategory=' + customerCategory + '&pks=' + pks + '&OutletId=' + outletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getExcelReportARAgingDetail=function(date, businessTypeId, penalty, dayOfYear, dueDate1, dueDate2, dueDate3, dueDate4, dueDate5, customerCategory, pks, outletId) {
        var url = '/api/fe/FinanceRptARAging/ExcelDetail/?Date='+date+'&BusinessType='+businessTypeId+'&Penalty='+penalty+'&DayofYear='+dayOfYear+'&duedate1='+dueDate1+'&duedate2='+dueDate2+'&duedate3='+dueDate3+'&duedate4='+dueDate4+'&duedate5='+dueDate5+'&customercategory=' + customerCategory + '&pks=' + pks + '&OutletId=' + outletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getDataBranchComboBox=function () {
		console.log("factory.getDataBranchComboBox");
		var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
		var res = $http.get(url);
		return res;
	};
	
	return factory;
});