angular.module('app')
	.factory('ReportGrossProfitFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		

	factory.getExcelReportGrossProfit=function(dateStart,dateEnd,modelName,fleetStatus,billingCanceled, reportType, outletId) {
        var url = '/api/fe/FinanceRptGrossProfit/Excel/?DateStart='+dateStart+'&DateEnd='+dateEnd+'&ModelName='+modelName+'&FleetStatus='+fleetStatus+'&BillingCanceled=' + billingCanceled + '&ReportType=' + reportType + '&OutletId=' + outletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};

	factory.getDataBranchReportGrossProfitByOrg=function(orgId) {
		//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ orgId;
		var res=$http.get(url);
		
		return res;			
		};

	factory.getDataModel = function(){
		var url = '/api/sales/MUnitVehicleModelTomas?start=' + 1 + '&limit=' + 10000 + '&filterData=';
		//console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};

	factory.getDataBranchReportGrossProfit=function() {
		//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
		var res=$http.get(url);  
		return res;			
		};

	return factory;
});