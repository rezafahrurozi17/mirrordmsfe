angular.module('app')
	.controller('ReportGrossProfitController', function ($scope, $http, $filter, CurrentUser, ReportGrossProfitFactory, $timeout, bsNotify) {
		$scope.user = CurrentUser.user();		
		$scope.ReportGrossProfit_EnableDisable = true;
		//$scope.optionsReportGrossProfitModel = null;
		//$scope.optionsReportGrossProfitModel = [{ name: "semua", value: "semua" }];
		$scope.optionsReportGrossProfitStatusFleet = [{ name: "semua", value: "semua" }, { name: "Fleet", value: "Fleet" }, { name: "Non Fleet", value: "NonFleet" }];
		$scope.optionsReportGrossProfitStatusBilling = [{ name: "semua", value: "semua" }, { name: "Batal", value: "Batal" }, { name: "Tidak Batal", value: "TidakBatal" }];
		$scope.ReportGrossProfit_StatusBilling = 'semua';
		$scope.ReportGrossProfit_StatusFleet = 'semua';
		$scope.right = function (str, chr) {
			return str.slice(str.length - chr, str.length);
		}
		
		$scope.ReportGrossProfit_GetDataBranchByOutletId = function () {
			debugger;
			if ($scope.right($scope.user.OrgCode, 6) == '000000') {
				$scope.Main_NamaBranch_EnableDisable = true;
			}
			else {
				$scope.Main_NamaBranch_EnableDisable = false;
			}

			ReportGrossProfitFactory.getDataBranchReportGrossProfitByOrg($scope.user.OrgId).then(
					function (res) {
						// console.log("res data BranchName : ", res.data.Result[0]);
						// $scope.ReportGrossProfit_Branch = res.data.Result[0].BranchName;	
						$scope.optionsReportGrossProfit_NamaBranch = res.data.Result;
						$scope.ReportGrossProfit_Branch = $scope.user.OrgId;

					},
					function (err) {
						console.log('error -->', err)
					}
				);
		}
		$scope.ReportGrossProfit_GetDataBranchByOutletId();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.ReportGrossProfitModelVehicle = function () {
			ReportGrossProfitFactory.getDataModel()
				.then(
					function (res) {
						var ModelVehicle = [];
						angular.forEach(res.data.Result, function (value, key) {
							ModelVehicle.push({ "name": value.VehicleModelName, "value": value.VehicleModelName });
						});
						//console.log("res data Model : ", ModelVehicle);
						$scope.optionsReportGrossProfitModel = ModelVehicle;
					}
				);
		}

		$scope.ReportGrossProfitModelVehicle();

		// $scope.Report_Cetak_Clicked = function() {         
		// var pdfFile="";  
		// var endDate="";

		// ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
		// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
		// .then(
		// function (res) {
		// var file = new Blob([res.data], {type: 'application/pdf'});
		// var fileURL = URL.createObjectURL(file);

		// console.log("pdf", fileURL);


		// printJS(fileURL);
		// },
		// function(err){
		// console.log("err=>",err);
		// }
		// );
		// };

		$scope.Report_Excel_Clicked = function () {
			var dNow = new Date();

			if($scope.ReportGrossProfit_TanggalAwal > dNow){
				// alert("Tanggal awal tidak boleh lebih besar dari tanggal hari ini");
				bsNotify.show(
					{
						title: "Warning",
						content: "Tanggal awal tidak boleh lebih besar dari tanggal hari ini.",
						type: 'warning'
					}
				);
			}
			else if($scope.ReportGrossProfit_TanggalAkhir > dNow){			
				// alert("Tanggal akhir tidak boleh lebih besar dari tanggal hari ini");
				bsNotify.show(
					{
						title: "Warning",
						content: "Tanggal akhir tidak boleh lebih besar dari tanggal hari ini.",
						type: 'warning'
					}
				);
			}
			else if($scope.ReportGrossProfit_TanggalAwal > $scope.ReportGrossProfit_TanggalAkhir){			
				// alert("Tanggal awal tidak boleh lebih besar dari tanggal akhir");
				bsNotify.show(
					{
						title: "Warning",
						content: "Tanggal awal tidak boleh lebih besar dari tanggal akhir.",
						type: 'warning'
					}
				);
			}
			else{
					ReportGrossProfitFactory.getExcelReportGrossProfit($filter('date')(new Date($scope.ReportGrossProfit_TanggalAwal), "yyyyMMdd"),
							$filter('date')(new Date($scope.ReportGrossProfit_TanggalAkhir), "yyyyMMdd"),
							$scope.ReportGrossProfit_Model,
							$scope.ReportGrossProfit_StatusFleet,
							$scope.ReportGrossProfit_StatusBilling,
							$scope.ReportGrossProfit_TipeReport,
							$scope.ReportGrossProfit_Branch
						)
							.then(
								function (res) {

									var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
									var b64Data = res.data;

									var blob = b64toBlob(b64Data, contentType);
									var blobUrl = URL.createObjectURL(blob);

									saveAs(blob, 'Report_GrossProfit' + '.xlsx');

									return res;
								},
								function (err) {
									console.log("err=>", err);
								}
							);
				}

		}

		$scope.tanggalBillingOK = true;
		$scope.tanggalBilling_Changed = function () {
			var dt = $scope.ReportGrossProfit_TanggalAwal;
			var dtTo = $scope.ReportGrossProfit_TanggalAkhir;

			var now = new Date();

			// if (dt > dtTo) {
			// 	$scope.errTanggalBilling = "Tanggal awal tidak boleh lebih besar dari tanggal akhir";
			// 	$scope.tanggalBillingOK = false;
			// 	$scope.ReportGrossProfit_EnableDisable = false;
			// }
			// else if (dt > now || dtTo > now) {
			// 	$scope.errTanggalBilling = "Tanggal tidak boleh lebih besar dari tanggal hari ini";
			// 	$scope.tanggalBillingOK = false;				
			// 	$scope.ReportGrossProfit_EnableDisable = false;
			// }
			// else {
			// 	$scope.tanggalBillingOK = true;				
			// 	$scope.ReportGrossProfit_EnableDisable = true;
			// }
		}

		$scope.NamaBranchOK = true;
		$scope.NamaBranch_Changed = function () {
			var dtaNamaBranch = $scope.ReportGrossProfit_Branch;
	
			if(dtaNamaBranch != undefined){
				$scope.NamaBranchOK = true;
				$scope.ReportGrossProfit_EnableDisable = true;
			}
			else{			
				$scope.errNamaBranch = "Field ini wajib diisi.";
				$scope.NamaBranchOK = false;
				$scope.ReportGrossProfit_EnableDisable = false;
			}
		}

	});