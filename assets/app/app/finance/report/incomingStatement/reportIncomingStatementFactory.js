angular.module('app')
	.factory('ReportIncomingStatementFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		
	
	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ outletId;
		console.log("url getBranchByOutletId : ", url);
		var res=$http.get(url);
		return res;
	};
	
	factory.getExcelReportIncomingStatement=function(datePeriod,incomingStatementTypeId,OutletId) {
        var url = '/api/fe/FinanceRptIncomeStatement/Excel/?MTDPeriod=' + datePeriod + '&IncomingStatementTypeId=' + incomingStatementTypeId+'&OutletId='+OutletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};
	
	return factory;
});