angular.module('app')
.controller('ReportIncomingStatementController', function ($scope, $http, $filter, bsNotify, CurrentUser, ReportIncomingStatementFactory, $timeout) {
	var user = CurrentUser.user();
	console.log("Cek User >>>>", user)
	$scope.Report_Excel_isDisabled = true;
	
	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	if(user.RoleName == "Kepala Bengkel GR" || user.RoleName == "Kepala Bengkel BP" || user.RoleName == "Koordinator Suboutlet"){
		$scope.Show_Unit = false;
		$scope.ReportIncomingStatement_UnitorParts = 3;
	}else{
		$scope.Show_Unit = true;
		$scope.ReportIncomingStatement_UnitorParts = 2;
	}

	$scope.ReportIncomingStatement_GetDataBranchByOutletId = function(){
		if ($scope.right(user.OrgCode, 6) == '000000') {
			$scope.ReportIncomingStatement_Branch_isDisabled = true;
		}
		else {
			$scope.ReportIncomingStatement_Branch_isDisabled = false;
		}
		// console.log("res data User : ", user.OrgCode);

		ReportIncomingStatementFactory.getBranchByOutletId(user.OutletId)
		.then(
			function(res){
				// console.log("res data BranchName : ", res.data.Result[0]);
				$scope.optionsReportIncomingStatement_Branch = res.data.Result;
				$scope.ReportIncomingStatement_Branch = res.data.Result[0].BranchId;
			}
		);
	}	
	$scope.ReportIncomingStatement_GetDataBranchByOutletId();
	
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	// $scope.Report_Cetak_Clicked = function() {         
			// var pdfFile="";  
			// var endDate="";

            // ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
				// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			// .then(
				// function (res) {
                // var file = new Blob([res.data], {type: 'application/pdf'});
                // var fileURL = URL.createObjectURL(file);

                // console.log("pdf", fileURL);


				// printJS(fileURL);
            // },
			// function(err){
					// console.log("err=>",err);
				// }
			// );
	// };

	// $scope.ReportIncomingStatement_UnitorParts = 2;
	$scope.Report_Excel_Clicked=function() {
		// console.log("Unit or Parts", $scope.ReportIncomingStatement_UnitorParts);
		var dNow = new Date();
		if(($scope.ReportIncomingStatement_Period == null || $scope.ReportIncomingStatement_Period == undefined) || ($scope.ReportIncomingStatement_Branch == null || $scope.ReportIncomingStatement_Branch == undefined)){
			bsNotify.show(
				{
					title: "Warning",
					content: "Periode dan Nama Branch Harus diisi",
					type: 'warning'
				}
			);
			return;
		}

		if($scope.ReportIncomingStatement_Period > dNow){
			alert("Periodel tidak boleh lebih besar dari tanggal hari ini");
		}
		else{
			ReportIncomingStatementFactory.getExcelReportIncomingStatement($filter('date')(new Date($scope.ReportIncomingStatement_Period), "yyyyMM"),
					$scope.ReportIncomingStatement_UnitorParts,$scope.ReportIncomingStatement_Branch)
			.then(
				function(res){

					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);

					saveAs(blob, 'Report_IncomeStatement' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
	}
	
	$scope.tanggalPeriodeOK = true;
	$scope.tanggalPeriode_Changed = function () {
		var dt = $scope.ReportIncomingStatement_Period;
		var now = new Date();

		if (dt > now) {
			$scope.errTanggalPeriode = "Periode tidak boleh lebih besar dari bulan ini";
			$scope.tanggalPeriodeOK = false;
			$scope.Report_Excel_isDisabled = false;
		}
		else {
			$scope.tanggalPeriodeOK = true;			
			$scope.Report_Excel_isDisabled = true;
		}
	}

});