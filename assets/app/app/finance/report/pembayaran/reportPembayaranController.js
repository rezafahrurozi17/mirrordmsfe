angular.module('app')
.controller('ReportPembayaranController', function ($scope, $http, $filter, CurrentUser, bsNotify, ReportPembayaranFactory, $timeout) {
	$scope.user = CurrentUser.user();
	$scope.MainReportPembayaran_Show = true;
	$scope.reportPembayaran_MetodePembayaranOptions = [{name : "Bank Transfer", value : "Bank Transfer"}, {name :"Cash", value:"Cash"}];
	$scope.reportPembayaran_MetodePembayaran = {};
	$scope.ReportPembayaran_PilihNamaVendor = "";
	$scope.right=function (str, chr)
  	{
		return str.slice(str.length-chr,str.length);
 	}

	var dateFormat2='MM/yyyy';
	var dateFormat='dd/MM/yyyy';
	var dateFilter='date:"dd/MM/yyyy"';
	var periode=new Date();
	$scope.SdateOptions = {
			startingDay: 1,
			format: dateFormat,
	};
	$scope.EdateOptions = {
			startingDay: 1,
			format: dateFormat,
	};
	// $scope.dateOptions2 = {
	// 		startingDay: 1,
	// 		format: dateFormat2,
	// };
	
	$scope.SetComboBranch=function() {
		// var ho;
		if ($scope.right($scope.user.OrgCode,6) == '000000')
		{
			$scope.Main_NamaBranch_EnableDisable = true;
			// ho = $scope.user.OrgId;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = true;
			// ho = 0;
		}
		ReportPembayaranFactory.getDataBranchRptPembayaran($scope.user.OrgId).then(

			function(res) {
				$scope.optionsReportPembayaran_NamaBranch = res.data.Result;	
				$scope.ReportPembayaran_NamaBranch = $scope.user.OutletId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};

	$scope.ReportPembayaran_Vendor_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Id", displayName:"Id", field:"Id", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"DataName", displayName:"Name", field:"Name", enableCellEdit: false, enableHiding: false},
				{name:"Outlet", displayName:"Outlet", field:"Outlet", enableCellEdit: false, enableHiding: false}
			],
		onRegisterApi: function(gridApi) {
			$scope.ReportPembayaran_Vendor_UIGrid_gridAPI = gridApi;
		}
	};
	
	$scope.ReportPembayaran_PilihNamaVendor_Click=function() {
			ReportPembayaranFactory.getVendorListRptPembayaran( 'reportPembayaran' , '').then(
				function(res) {
					$scope.ReportPembayaran_Vendor_UIGrid.data = res.data.Result;
					angular.element('#Modal_ReportPembayaran_PilihVendor').modal('setting',{closeable:false}).modal('show');
				},
				function(err) {
					console.log("err=>", err);
				} 
			);	
	};

	$scope.Pilih_ReportPembayaran_Vendor = function() {
		if ($scope.ReportPembayaran_Vendor_UIGrid_gridAPI.selection.getSelectedCount() > 0)
		{
			$scope.ReportPembayaran_Vendor_UIGrid_gridAPI.selection.getSelectedRows().forEach(function(row) {	
				$scope.ReportPembayaran_PilihNamaVendor =  row.Name;
				$scope.ReportPembayaran_PilihIdVendor = row.Id;
			});
			
			angular.element('#Modal_ReportPembayaran_PilihVendor').modal('hide');	
		}
		else 
		{
			alert('Belum ada data yang dipilih');
		}			
	}

	$scope.Batal_ReportPembayaran_Vendor=function() {
		$scope.ReportPembayaran_PilihNamaVendor =  "";
		$scope.ReportPembayaran_PilihIdVendor = "";
		angular.element('#Modal_ReportPembayaran_PilihVendor').modal('hide');	
	}

	$scope.SetComboBranch();

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.ReportPembayaran_Cetak_Clicked = function() {        
		if(  ( (isNaN($scope.reportPembayaran_TanggalStart) != true) &&  
			 (angular.isUndefined($scope.reportPembayaran_TanggalStart) != true) &&
			  ($scope.reportPembayaran_TanggalStart != null)
			 ) &&
			   ( (isNaN($scope.ReportPembayaran_NamaBranch) != true) && 
			  (angular.isUndefined($scope.ReportPembayaran_NamaBranch) != true) )&&
			 (angular.isUndefined($scope.reportPembayaran_MetodePembayaran) != true) &&  

			// ( ( angular.isUndefined($scope.ReportPembayaran_PilihIdVendor) != true)  &&
			// 	(isNaN($scope.ReportPembayaran_PilihIdVendor) != true) && 
			// 	($scope.ReportPembayaran_PilihIdVendor != "") ) && 
				(  (angular.isUndefined($scope.ReportPembayaran_PilihNamaVendor) != true) &&
					($scope.ReportPembayaran_PilihNamaVendor != "") ) && 
				
				  ( (isNaN($scope.reportPembayaran_TanggalEnd) != true) && 
					 (angular.isUndefined($scope.reportPembayaran_TanggalEnd) != true) &&
					 ($scope.reportPembayaran_TanggalEnd != null)
					 )
			)
		{		 
			var pdfFile="";  
			// var endDate="";

			// if	 ( (isNaN($scope.reportPembayaran_TanggalEnd) != true) && 
			//  (angular.isUndefined($scope.reportPembayaran_TanggalEnd) != true) )
			//  {
			// 	endDate = $scope.formatDate($scope.reportPembayaran_TanggalEnd);
			//  }
            ReportPembayaranFactory.getCetakReportPembayaran($scope.formatDate($scope.reportPembayaran_TanggalStart), 
				 $scope.formatDate($scope.reportPembayaran_TanggalEnd), $scope.ReportPembayaran_NamaBranch, $scope.ReportPembayaran_PilihNamaVendor , 
				$scope.reportPembayaran_MetodePembayaran ).then(
				function (res) {
                var file = new Blob([res.data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                //window.open(fileURL);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                //pdfFile = fileURL;

				printJS(fileURL);
				//printJS({printable: file,showModal:true,type: 'pdf'})
            },
			function(err){
					console.log("err=>",err);
				}
			);
		}
		else {
			alert('Data parameter tidak lengkap diisi !');
		}
	};

	$scope.ReportPembayaran_Excel_Clicked=function() {
		//Penjagaan Jika tanggal lebih dari 90 hari
		var date1 = new Date($scope.reportPembayaran_TanggalStart);
		var date2 = new Date($scope.reportPembayaran_TanggalEnd);
		var t = date2.getTime() - date1.getTime(); 
 
		var DaysDiff = t / (1000 * 3600 * 24);
		
		console. log("tanggal 1>>>", date1);
		console. log("tanggal 2>>>", date2);
		if($scope.ReportPembayaran_PilihNamaVendor == null || $scope.ReportPembayaran_PilihNamaVendor == undefined || $scope.ReportPembayaran_PilihNamaVendor == ""){
			$scope.ReportPembayaran_PilihNamaVendor = undefined
		}else{
			$scope.ReportPembayaran_PilihNamaVendor = $scope.ReportPembayaran_PilihNamaVendor;
		}

		if(DaysDiff > 90){
			bsNotify.show({
				title:"Warning",
				content:"Tanggal tidak boleh lebih dari 90 Hari.",
				type: "warning"
			});
	 }else if(  ( (isNaN($scope.reportPembayaran_TanggalStart) != true) &&  
			 (angular.isUndefined($scope.reportPembayaran_TanggalStart) != true) &&
			  ($scope.reportPembayaran_TanggalStart != null)
			 ) &&
			   ( (isNaN($scope.ReportPembayaran_NamaBranch) != true) && 
			 (angular.isUndefined($scope.ReportPembayaran_NamaBranch) != true) )&&
			 (angular.isUndefined($scope.reportPembayaran_MetodePembayaran) != true) &&  

			// ( ( angular.isUndefined($scope.ReportPembayaran_PilihIdVendor) != true)  &&
			// 	(isNaN($scope.ReportPembayaran_PilihIdVendor) != true) && 
			// 	($scope.ReportPembayaran_PilihIdVendor != "") ) && 
				// (  (angular.isUndefined($scope.ReportPembayaran_PilihNamaVendor) != true) &&
				// 	($scope.ReportPembayaran_PilihNamaVendor != "") ) && 

				  ( (isNaN($scope.reportPembayaran_TanggalEnd) != true) && 
					 (angular.isUndefined($scope.reportPembayaran_TanggalEnd) != true) &&
					 ($scope.reportPembayaran_TanggalEnd != null)
					 )
			)
		{
			// var endDate="";
			console.log('isi vendor', $scope.ReportPembayaran_PilihNamaVendor);
			// if	 ( (isNaN($scope.reportPembayaran_TanggalEnd) != true) && 
			//  (angular.isUndefined($scope.reportPembayaran_TanggalEnd) != true) )
			//  {
			// 	endDate = $scope.formatDate($scope.reportPembayaran_TanggalEnd);
			//  }

			ReportPembayaranFactory.getExcelReportPembayaran($scope.formatDate( $scope.reportPembayaran_TanggalStart), 
				$scope.formatDate($scope.reportPembayaran_TanggalEnd) , $scope.user.OrgId, $scope.ReportPembayaran_PilihNamaVendor, 
				$scope.reportPembayaran_MetodePembayaran  + '|' + $scope.ReportPembayaran_NamaBranch).then(
				function(res){
					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);
					saveAs(blob, 'Report_Pembayaran' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		 }
		 else {
		 	alert('Data parameter tidak lengkap diisi !');
		 }
	}

	$scope.tanggalOK = true;
	// OLD
	// $scope.tanggal_Changed = function () {
	// 	var dt = $scope.reportPembayaran_TanggalStart;
	// 	var dtTo = $scope.reportPembayaran_TanggalEnd;

	// 	var now = new Date();

	// 	if (dt > dtTo) {
	// 		$scope.errTanggalAwalAkhir = "Tanggal awal tidak boleh lebih besar dari tanggal akhir";
	// 		$scope.tanggalOK = false;
	// 	}
	// 	else if (dt > now || dtTo > now) {
	// 		$scope.errTanggalAwalAkhir = "Tanggal tidak boleh lebih besar dari tanggal hari ini";
	// 		$scope.tanggalOK = false;
	// 	}
	// 	else {
	// 		$scope.tanggalOK = true;
	// 	}
	// }
	//  =========================== New Validasi Date Start ==========================================
	var today = new Date();
	$scope.dateNow = new Date(today.getFullYear(),today.getMonth(),today.getDate());  
	$scope.dateMin = new Date(today.getFullYear(),today.getMonth(),today.getDate());                         


	$scope.tanggal_Changed = function(tgl){
			var today = new Date();
			var dayOne = new Date($scope.reportPembayaran_TanggalStart);
			var dayTwo = new Date($scope.reportPembayaran_TanggalEnd);
			 
			$scope.EdateOptions.minDate = $scope.reportPembayaran_TanggalStart;

			// if (tgl == null || tgl == undefined){
			// 		$scope.reportPembayaran_TanggalEnd = null;
			// } else {
			// 		if ($scope.reportPembayaran_TanggalStart < $scope.reportPembayaran_TanggalEnd){
			// 				if((dayTwo.getMonth() - dayOne.getMonth()) > 2 || dayOne.getFullYear() != dayTwo.getFullYear()){
			// 					// $scope.reportPembayaran_TanggalEnd = $scope.reportPembayaran_TanggalStart; 
			// 					$scope.EdateOptions.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth()+3,dayOne.getDate());
			// 					if ($scope.EdateOptions.maxDate > today) {
			// 							$scope.EdateOptions.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());                    
			// 					}                                   
			// 				}

			// 		} else {
			// 				if (dayOne > today){
			// 						$scope.reportPembayaran_TanggalStart = today;
			// 						// $scope.reportPembayaran_TanggalEnd = $scope.reportPembayaran_TanggalStart;                    
			// 				} else {
			// 				// $scope.reportPembayaran_TanggalEnd = $scope.reportPembayaran_TanggalStart;
			// 				$scope.EdateOptions.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth()+3,dayOne.getDate());
			// 				if ($scope.EdateOptions.maxDate > today) {
			// 						$scope.EdateOptions.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());                    
			// 				}    
			// 		}                                                
			// 		}
			// }
	}

	$scope.tanggal_Changed_End = function(tgl){
			var today = new Date();
			var dayOne = new Date($scope.reportPembayaran_TanggalStart);
			var dayTwo = new Date($scope.reportPembayaran_TanggalEnd);

			if (dayTwo > today){
					// $scope.EdateOptions.minDate = $scope.reportPembayaran_TanggalStart;
					$scope.reportPembayaran_TanggalEnd = today;
			}
	}
//  =========================== New Validasi Date End ==========================================

	$scope.metodePembayranOK = true;
	$scope.reportPembayaran_MetodePembayaran_Changed = function () {
		var dta = $scope.reportPembayaran_MetodePembayaran;

		if(dta != undefined){
			$scope.errmetodePembayaran = "Dropdown ini wajib di pilih";
			$scope.metodePembayranOK = true;
		}
		else{
			$scope.metodePembayranOK = false;
		}
	}

	// $scope.namaBranchOK = true;
	// $scope.reportPembayaran_NamaBranch_Changed = function () {
	// 	var dta = $scope.ReportPembayaran_NamaBranch;

	// 	if(dta != undefined){
	// 		$scope.errnamaBranch = "Dropdown ini wajib di pilih";
	// 		$scope.namaBranchOK = true;
	// 	}
	// 	else{
	// 		$scope.namaBranchOK = false;
	// 	}
	// }

	$scope.NamaBranchOK = true;
	$scope.NamaBranch_Changed = function () {
		var dtaNamaBranch = $scope.ReportPembayaran_NamaBranch;

		if(dtaNamaBranch != undefined){
			$scope.NamaBranchOK = true;
			$scope.tanggalOK = true;
		}
		else{			
			$scope.errNamaBranch = "Field ini wajib dipilih.";
			$scope.NamaBranchOK = false;
			$scope.tanggalOK = false;
		}
	}
});