angular.module('app')
	.factory('ReportPembayaranFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataBranchRptPembayaran=function(orgId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ orgId;
      var res=$http.get(url);
      
      return res;			
    };
    
    factory.getNewDataBranchRptPembayaran=function(orgId) {
        //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
        var url = '/api/fe/Branch/SelectDataNew?start=0&limit=0&FilterData='+ orgId;
        var res=$http.get(url);
        
        return res;			
    };

    factory.getVendorListRptPembayaran= function(tipe, name) {
        var FilterData = [{PIType : tipe, VendorName : name, ShowAll : 1}];
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData));
        return res;
    };   

	factory.getExcelReportPembayaran=function(startdate,enddate,branch,vendor,metode) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/PembayaranReport/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&VendorName=' + 
                vendor + '&PaymentMethod=' + metode + '&outletId=' +  branch;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getCetakReportPembayaran=function(startdate,enddate,branch,vendor,metode) {
        //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
        var url = '/api/fe/PembayaranReport/Cetak?StartDate=' + startdate + '&EndDate=' + enddate + '&VendorName=' + 
                vendor + '&PaymentMethod=' + metode + '&outletId=' +  branch;
        var res=$http.get(url , {responseType: 'arraybuffer'});

        return res;
	};

	return factory;
});