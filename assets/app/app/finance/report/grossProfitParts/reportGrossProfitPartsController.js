angular.module('app')
.controller('ReportGrossProfitPartsController', function ($scope, $http, $filter, CurrentUser, ReportGrossProfitPartsFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.optionsReportGrossProfitPartsTipeMaterial = [{name:"semua", value:"semua"}, {name:"Parts", value:"Parts"}, {name:"Bahan", value:"Bahan"}];
	$scope.optionsReportGrossProfitPartsPKS = [{name:"semua", value:"semua"}, {name:"PKS", value:"PKS"}, {name:"Non PKS", value:"NonPKS"}];
	$scope.optionsReportGrossProfitPartsStatusBilling  = [{name:"semua", value:"semua"}, {name:"Batal", value:"Batal"}, {name:"Tidak Batal", value:"TidakBatal"}];
	// $scope.optionsReportGrossProfitParts_TipeReport = [{name:"Parts yang dijual via service", value:"1"},{name:"Parts yang dijual secara langsung", value:"2"}]
	$scope.Main_NamaBranch_EnableDisable = false;
	$scope.Report_Excel_EnableDisable = true;

	function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	// $scope.Report_Cetak_Clicked = function() {         
			// var pdfFile="";  
			// var endDate="";

            // ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
				// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			// .then(
				// function (res) {
                // var file = new Blob([res.data], {type: 'application/pdf'});
                // var fileURL = URL.createObjectURL(file);

                // console.log("pdf", fileURL);


				// printJS(fileURL);
            // },
			// function(err){
					// console.log("err=>",err);
				// }
			// );
	// };

	$scope.Report_Excel_Clicked=function() {
		var dNow = new Date();
		var status = "";
		var reportType = 0;
		if($scope.ReportGrossProfitParts_TipeReport1 == true)
		{
			status += "Service";
			reportType = 1;
		}
		if($scope.ReportGrossProfitParts_TipeReport2 == true)
		{
			status += "Langsung";			
			reportType = 1;
		}

		if($scope.ReportGrossProfitParts_TipeMaterial == null || $scope.ReportGrossProfitParts_TipeMaterial == undefined || $scope.ReportGrossProfitParts_TipeMaterial == ""){
			$scope.ReportGrossProfitParts_TipeMaterial = "semua";
		}

		if($scope.ReportGrossProfitParts_TanggalAwal > dNow){
			// alert("Tanggal awal tidak boleh lebih besar dari tanggal hari ini");
			bsNotify.show(
				{
					title: "Warning",
					content: "Tanggal awal tidak boleh lebih besar dari tanggal hari ini.",
					type: 'warning'
				}
			);
		}
		else if($scope.ReportGrossProfitParts_TanggalAkhir > dNow){			
			// alert("Tanggal akhir tidak boleh lebih besar dari tanggal hari ini");
			bsNotify.show(
				{
					title: "Warning",
					content: "Tanggal akhir tidak boleh lebih besar dari tanggal hari ini.",
					type: 'warning'
				}
			);
		}
		else if($scope.ReportGrossProfitParts_TanggalAwal > $scope.ReportGrossProfitParts_TanggalAkhir){			
			// alert("Tanggal awal tidak boleh lebih besar dari tanggal akhir");
			bsNotify.show(
				{
					title: "Warning",
					content: "Tanggal awal tidak boleh lebih besar dari tanggal akhir.",
					type: 'warning'
				}
			);
		}
		else{
			ReportGrossProfitPartsFactory.getExcelReportGrossProfitParts($filter('date')(new Date($scope.ReportGrossProfitParts_TanggalAwal), "yyyyMMdd"), 
				$filter('date')(new Date($scope.ReportGrossProfitParts_TanggalAkhir), "yyyyMMdd"),
				$scope.ReportGrossProfitParts_TipeMaterial,
				status,//$scope.ReportGrossProfitParts_PKS,
				$scope.ReportGrossProfitParts_StatusBilling,
				reportType,
				$scope.ReportGrossProfitParts_NamaBranch,
				$scope.user.EmployeeId
			)
			.then(
				function(res){
					
				debugger;
				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
				var b64Data = res.data;

				var blob = b64toBlob(b64Data, contentType);
				var blobUrl = URL.createObjectURL(blob);

				saveAs(blob, 'Report_GrossProfitParts' + '.xlsx');

				return res;
				},
				function(err){
				console.log("err=>",err);
				}
			);
		}
	}

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.SetComboBranch=function() {
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.Main_NamaBranch_EnableDisable = true;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
		}

		ReportGrossProfitPartsFactory.getDataBranchReportGrossProfitPartsByOrg($scope.user.OrgId).then(
			function(res) {
				$scope.optionsReportGrossProfitParts_NamaBranch = res.data.Result;	
				$scope.ReportGrossProfitParts_NamaBranch = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};
	$scope.SetComboBranch();

	$scope.tanggalPeriodeOK = true;
	$scope.tanggalPeriode_Changed = function () {
		var dt = $scope.ReportGrossProfitParts_TanggalAwal;
		var dtTo = $scope.ReportGrossProfitParts_TanggalAkhir;

		var now = new Date();

		// if (dt > dtTo) {
		// 	$scope.errTanggalPeriode = "Tanggal awal tidak boleh lebih besar dari tanggal akhir";
		// 	$scope.tanggalPeriodeOK = false;
		// 	$scope.Report_Excel_EnableDisable = false;
		// }
		// else if (dt > now || dtTo > now) {
		// 	$scope.errTanggalPeriode = "Tanggal tidak boleh lebih besar dari tanggal hari ini";
		// 	$scope.tanggalPeriodeOK = false;
		// 	$scope.Report_Excel_EnableDisable = false;
		// }
		// else {
		// 	$scope.tanggalPeriodeOK = true;
		// 	$scope.Report_Excel_EnableDisable = true;
		// }
	}

	$scope.NamaBranchOK = true;
	$scope.NamaBranch_Changed = function () {
		var dtaNamaBranch = $scope.ReportGrossProfitParts_NamaBranch;

		if(dtaNamaBranch != undefined){
			$scope.NamaBranchOK = true;
			$scope.Report_Excel_EnableDisable = true;
		}
		else{			
			$scope.errNamaBranch = "Field ini wajib dipilih.";
			$scope.NamaBranchOK = false;
			$scope.Report_Excel_EnableDisable = false;
		}
	}

});