angular.module('app')
	.factory('ReportGrossProfitPartsFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		

	factory.getExcelReportGrossProfitParts=function(dateStart,dateEnd,MaterialType,PKSStatus,billingCanceled, reportType, outletId, pICId) {
		debugger;
        var url = '/api/fe/FinanceRptGrossProfitParts/ExcelProfit/?DateStart='+dateStart+'&DateEnd='+dateEnd+'&MaterialType='+MaterialType+'&PKSStatus='+PKSStatus+'&BillingCanceled=' + billingCanceled + '&ReportType=' + reportType + '&OutletId=' + outletId +'&PICId='+pICId;
		console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getDataBranchReportGrossProgitParts=function() {
		//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
		var res=$http.get(url);  
		return res;			
		};
	
	factory.getDataBranchReportGrossProfitPartsByOrg=function(orgId) {
		//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ orgId;
		var res=$http.get(url);
		
		return res;			
		};

	return factory;
});