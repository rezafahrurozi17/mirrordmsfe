angular.module('app')
	.factory('BatalBillingFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchBatalBilling=function(orgId) {
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
			var res=$http.get(url);  
			return res;			
		};

        factory.getExcelReportBatalBilling = function(NomorBilling, Branch, TanggalBillingAwal, TanggalBillingAkhir, NomorFakturPajak, TanggalFakturAwal, TipeBisnis,
														TanggalFakturAkhir, TanggalBatalAwal, TanggalBatalAkhir, TipeCetakanBatal, NomorCetakanBatal, NamaPelanggan, NPWP ) {
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var url = '/api/fe/FinanceRptBillingCanceled/Excel?BillingNo=' + NomorBilling 
															+ '&OutletId=' + Branch 
															+ '&DateBillStart=' + TanggalBillingAwal 
															+ '&DateBillEnd='+ TanggalBillingAkhir 
															+ '&TaxInvoiceNo=' + NomorFakturPajak 
															+ '&DateTaxInvoiceStart=' + TanggalFakturAwal 
															+ '&DateTaxInvoiceEnd=' + TanggalFakturAkhir 
															+ '&DateCanceledStart=' + TanggalBatalAwal 
															+ '&DateCanceledEnd=' + TanggalBatalAkhir 
															+ '&PrintCanceledType=' + TipeCetakanBatal
															+ '&PrintCanceledNo=' + NomorCetakanBatal
															+ '&CustomerName=' + NamaPelanggan
															+ '&NPWP=' + NPWP
															+ '&BusinessType=' + TipeBisnis
															;
			var res=$http.get(url);
			return res;
		};

	return factory;
});