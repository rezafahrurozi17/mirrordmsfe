angular.module('app')
	.controller('BatalBillingController', function ($scope, $http, $filter, CurrentUser, BatalBillingFactory, $timeout) {
		$scope.user = CurrentUser.user();
		$scope.MainReportBatalBilling_Show = true;
		$scope.optionsFinanceReportBatalBilling_TipeCetakanBatal = [{ name: "Nota Batal", value: "1" }, { name: "Nota Retur", value: "2" }, { name: "Berita Acara Pembatalan", value: "3" }];
		$scope.optionsFinanceReportBatalBilling_TipeBisnis = [{ name: "Unit", value: "1" }, { name: "Service", value: "2" }, { name: "Parts", value: "3" }];

		$scope.SetComboBranch = function () {
			BatalBillingFactory.getDataBranchBatalBilling($scope.user.OrgId).then(
				function (res) {
					$scope.optionsFinanceReportBatalBilling_NamaBranch = res.data.Result;
					$scope.FinanceReportBatalBilling_NamaBranch = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};


		$scope.SetComboBranch();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.FinanceReportBatalBilling_Excel_Clicked = function () {
			//formatDateReport

			var startBilling = formatDateReport($scope.FinanceReportBatalBilling_TanggalBillingAwal);
			var endBilling = formatDateReport($scope.FinanceReportBatalBilling_TanggalBillingAkhir);
			var startFaktur = formatDateReport($scope.FinanceReportBatalBilling_TanggalFakturAwal);
			var endFaktur = formatDateReport($scope.FinanceReportBatalBilling_TanggalFakturAkhir);
			var startBatal = formatDateReport($scope.FinanceReportBatalBilling_TanggalBatalAwal);
			var endBatal = formatDateReport($scope.FinanceReportBatalBilling_TanggalBatalAkhir);

			if($scope.FinanceReportBatalBilling_TipeBisnis == undefined){
				$scope.FinanceReportBatalBilling_TipeBisnis = 0;
			}

			BatalBillingFactory.getExcelReportBatalBilling(
				$scope.FinanceReportBatalBilling_NomorBilling,
				$scope.FinanceReportBatalBilling_NamaBranch,
				startBilling,
				endBilling,
				$scope.FinanceReportBatalBilling_NomorFakturPajak,
				startFaktur,
				$scope.FinanceReportBatalBilling_TipeBisnis,
				endFaktur,
				startBatal,
				endBatal,
				$scope.FinanceReportBatalBilling_TipeCetakanBatal,
				$scope.FinanceReportBatalBilling_NomorCetakanBatal,
				$scope.FinanceReportBatalBilling_NamaPelanggan,
				$scope.FinanceReportBatalBilling_NPWP
			)
				.then(
					function (res) {
						var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
						var b64Data = res.data;

						var blob = b64toBlob(b64Data, contentType);
						var blobUrl = URL.createObjectURL(blob);
						// window.open(blobUrl);
						//$scope.sm_show2=false;
						saveAs(blob, 'Report_BatalBilling' + '.xlsx');

						return res;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

	});

function formatDateReport(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return year + month + day;
}