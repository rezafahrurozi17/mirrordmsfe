angular.module('app')
	.factory('ReportPenjualanServicePartFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchRptPenjualanServicePart=function() {
        //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
        var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
        var res=$http.get(url);  
        return res;			
        };

        factory.getExcelReportPenjualanServicePart=function(startdate,enddate,branch,tipe, pks ) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenjualanServicePart/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&Tipe=' + 
                        tipe + '&outletId=' +  branch + '&pks=' +  pks ;
                console.log('get --> ', url);
                var res=$http.get(url);
                return res;
	};

	factory.getCetakReportPenjualanServicePart=function(startdate,enddate,branch,tipe, pks) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenjualanServicePart/Cetak?StartDate=' + startdate + '&EndDate=' + enddate + '&Tipe=' + 
                        tipe + '&outletId=' +  branch + '&pks=' +  pks ;
                var res=$http.get(url , {responseType: 'arraybuffer'});

                return res;
	};

	return factory;
});