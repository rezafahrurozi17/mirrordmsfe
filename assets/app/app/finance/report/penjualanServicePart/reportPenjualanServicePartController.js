angular.module('app')
	.controller('ReportPenjualanServicePartController', function ($scope, bsNotify, $http, $filter, CurrentUser, ReportPenjualanServicePartFactory, $timeout) {
		$scope.user = CurrentUser.user();
		$scope.MainReportPenjualanServicePart_Show = true;
		$scope.ReportPenjualanServicePart_PelangganPKSOptions = [{ name: "Semua", value: "Semua" }, { name: "PKS", value: "PKS" }, { name: "NON PKS", value: "NON PKS" }];
		//$scope.ReportPenjualanServicePart_MetodePembayaran = null;

		var user = CurrentUser.user();
		if (user.OrgCode.substring(3, 9) == '000000')
			$scope.Main_NamaBranch_EnableDisable = true;
		else
			$scope.Main_NamaBranch_EnableDisable = false;
		$scope.SetComboBranch = function () {
			ReportPenjualanServicePartFactory.getDataBranchRptPenjualanServicePart().then(
				function (res) {
					$scope.optionsReportPenjualanServicePart_NamaBranch = res.data.Result;
					$scope.ReportPenjualanServicePart_NamaBranch = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};
		// $scope.TanggalPenjualanAwalOK = true;
		// $scope.TanggalPenjualanAwalChanged = function () {

		// 	console.log($scope.TanggalPenjualanAwalOK);
		// 	var dt = $scope.ReportPenjualanServicePart_TanggalStart;
		// 	var now = new Date();
		// 	 if (dt > now) {
		// 		$scope.errTanggalPenjualanAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
		// 		$scope.TanggalPenjualanAwalOK = false;
		// 	}
		// 	else {
		// 		$scope.TanggalPenjualanAwalOK = true;
		// 	}
		// 	console.log('asdasd ' + $scope.TanggalPenjualanAwalOK);
		// }

		// $scope.TanggalPenjualanAkhirOK = true;
		// $scope.TanggalPenjualanAkhirChanged = function () {

		// 	console.log($scope.TanggalPenjualanAkhirOK);
		// 	var dt = $scope.ReportPenjualanServicePart_TanggalEnd;
		// 	var now = new Date();
		// 	 if (dt > now) {
		// 		$scope.errTanggalPenjualanAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
		// 		$scope.TanggalPenjualanAkhirOK = false;			
		// 	}
		// 	else {
		// 		$scope.TanggalPenjualanAkhirOK = true;
		// 	}
		// 	console.log('asdasd ' + $scope.TanggalPenjualanAkhirOK);
		// }
		$scope.ReportPenjualanServicePart_PelangganPKS = "Semua";
		$scope.ReportPenjualanServicePart_PelangganPKS_Changed = function () {
			if (angular.isUndefined($scope.ReportPenjualanServicePart_PelangganPKS)) {
				$scope.ReportPenjualanServicePart_PelangganPKS = "Semua";
			}
		}

		$scope.SetComboBranch();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.ReportPenjualanServicePart_Cetak_Clicked = function () {
			var pdfFile = "";
			var endDate = "";

			if (!(
				(isNaN($scope.ReportPenjualanServicePart_TanggalStart) == true) ||
				(angular.isUndefined($scope.ReportPenjualanServicePart_TanggalStart) == true) ||
				($scope.ReportPenjualanServicePart_TanggalStart == null))
				&& (!(((angular.isUndefined($scope.ReportPenjualanServicePart_PelangganPKS) == true) ||
					($scope.ReportPenjualanServicePart_PelangganPKS == null)))
				)) {
				if ((isNaN($scope.ReportPenjualanServicePart_TanggalEnd) != true) &&
					(angular.isUndefined($scope.ReportPenjualanServicePart_TanggalEnd) != true)) {
					endDate = $scope.formatDate($scope.ReportPenjualanServicePart_TanggalEnd);
				}
				else {
					endDate = $scope.formatDate(new Date());
				}

				ReportPenjualanServicePartFactory.getCetakReportPenjualanServicePart(
					$scope.formatDate($scope.ReportPenjualanServicePart_TanggalStart),
					endDate, $scope.ReportPenjualanServicePart_NamaBranch, $scope.ReportPenjualanServicePart_TipeReport1, $scope.ReportPenjualanServicePart_TipeReport2,
					$scope.ReportPenjualanServicePart_PelangganPKS).then(
						function (res) {
							var file = new Blob([res.data], { type: 'application/pdf' });
							var fileURL = URL.createObjectURL(file);
							//window.open(fileURL);

							console.log("pdf", fileURL);
							//$scope.content = $sce.trustAsResourceUrl(fileURL);
							//pdfFile = fileURL;

							printJS(fileURL);
							//printJS({printable: file,showModal:true,type: 'pdf'})
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
			else {
				alert('Data harus lengkap di isi !');
			}
		};

		$scope.ReportPenjualanServicePart_Excel_Clicked = function () {
			var endDate = "";

			if (angular.isUndefined($scope.ReportPenjualanServicePart_TanggalStart) == true && (angular.isUndefined($scope.ReportPenjualanServicePart_TanggalEnd) == true)) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal harus diisi / format tanggal salah.",
					type: "warning"
				});
			}
			else if (new Date($scope.ReportPenjualanServicePart_TanggalStart) > new Date()) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if (new Date($scope.ReportPenjualanServicePart_TanggalEnd) > new Date()) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if (new Date($scope.ReportPenjualanServicePart_TanggalStart) > new Date($scope.ReportPenjualanServicePart_TanggalEnd)) {
				bsNotify.show({
					title: "Warning",
					content: "Tangga lawal tidak bisa lebih besar dari Tanggal Akhir.",
					type: "warning"
				});
			}
			else if (angular.isUndefined($scope.ReportPenjualanServicePart_NamaBranch) == true) {
				bsNotify.show({
					title: "Warning",
					content: "Branch harus diisi",
					type: "warning"
				});
			}
			// else if (angular.isUndefined($scope.ReportPenjualanServicePart_TipeReport1) == false && angular.isUndefined($scope.ReportPenjualanServicePart_TipeReport2) == false) {


			// 	bsNotify.show({
			// 		title: "Warning",
			// 		content: "Tipe report harus di isi !",
			// 		type: "warning"
			// 	});
			// }
			else{
				ReportPenjualanServicePartFactory.getExcelReportPenjualanServicePart($scope.formatDate( $scope.ReportPenjualanServicePart_TanggalStart), 
			$scope.formatDate( $scope.ReportPenjualanServicePart_TanggalEnd) , $scope.ReportPenjualanServicePart_NamaBranch,($scope.ReportPenjualanServicePart_TipeReport1+ $scope.ReportPenjualanServicePart_TipeReport2),
					$scope.ReportPenjualanServicePart_PelangganPKS ).then(
					function(res){
						var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
						var b64Data = res.data;

						var blob = b64toBlob(b64Data, contentType);
						var blobUrl = URL.createObjectURL(blob);
						// window.open(blobUrl);
						//$scope.sm_show2=false;
						saveAs(blob, 'Report_PenjualanServicePart' + '.xlsx');

						return res;
					},
					function(err){
						console.log("err=>",err);
					}
				);
			}


			// 	if ( !(
			// 		(isNaN($scope.ReportPenjualanServicePart_TanggalStart) == true) ||  
			// 			 (angular.isUndefined($scope.ReportPenjualanServicePart_TanggalStart) == true) ||
			// 			 ( $scope.ReportPenjualanServicePart_TanggalStart==null)  )
			// 	&& ( ! ((  (angular.isUndefined($scope.ReportPenjualanServicePart_PelangganPKS) == true) ||
			// 			 ( $scope.ReportPenjualanServicePart_PelangganPKS==null)  ) )
			// 	)) 	
			// 	{
			// 		if	 ( (isNaN($scope.ReportPenjualanServicePart_TanggalEnd) != true) && 
			// 		(angular.isUndefined($scope.ReportPenjualanServicePart_TanggalEnd) != true) )
			// 		{
			// 			endDate = $scope.formatDate($scope.ReportPenjualanServicePart_TanggalEnd);
			// 		}
			// 		else {
			// 			endDate = $scope.formatDate(new Date());
			// 		}

			// 		ReportPenjualanServicePartFactory.getExcelReportPenjualanServicePart($scope.formatDate( $scope.ReportPenjualanServicePart_TanggalStart), 
			// 			endDate , $scope.ReportPenjualanServicePart_NamaBranch,$scope.ReportPenjualanServicePart_TipeReport1, $scope.ReportPenjualanServicePart_TipeReport2,
			// 			$scope.ReportPenjualanServicePart_PelangganPKS ).then(
			// 			function(res){
			// 				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
			// 				var b64Data = res.data;

			// 				var blob = b64toBlob(b64Data, contentType);
			// 				var blobUrl = URL.createObjectURL(blob);
			// 				// window.open(blobUrl);
			// 				//$scope.sm_show2=false;
			// 				saveAs(blob, 'Report_PenjualanServicePart' + '.xlsx');

			// 				return res;
			// 			},
			// 			function(err){
			// 				console.log("err=>",err);
			// 			}
			// 		);
			// 	}
			//  else {
			//  	alert('Data harus lengkap di isi !');
			//  }
		}

	});