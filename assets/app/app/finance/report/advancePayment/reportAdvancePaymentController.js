angular.module('app')
.controller('ReportAdvancePaymentController', function ($scope, $http, $filter, CurrentUser, ReportAdvancePaymentFactory, $timeout,bsNotify) {
	var user = CurrentUser.user();
	$scope.optionsReportAdvancePayment_Status = [{name:"Semua", value:"Semua"}, {name:"Open", value:"Open"}, {name:"Close", value:"Close"}];
	$scope.dateNow = Date.now();
	$scope.ReportAdvancePayment_Tanggal = new Date();
	$scope.ReportAdvancePayment_GetDataBranchByOutletId = function(){
		ReportAdvancePaymentFactory.getBranchByOutletId(user.OutletId)
		.then(
			function(res){
				console.log("res data BranchName : ", res.data.Result[0]);
				$scope.ReportAdvancePayment_Branch = res.data.Result[0].BranchName;
			}
		);
	}
	
	$scope.ReportAdvancePayment_GetDataBranchByOutletId();
	
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	// $scope.Report_Cetak_Clicked = function() {         
			// var pdfFile="";  
			// var endDate="";

            // ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
				// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			// .then(
				// function (res) {
                // var file = new Blob([res.data], {type: 'application/pdf'});
                // var fileURL = URL.createObjectURL(file);

                // console.log("pdf", fileURL);


				// printJS(fileURL);
            // },
			// function(err){
					// console.log("err=>",err);
				// }
			// );
	// };

	$scope.Report_Excel_Clicked=function() {
		if (angular.isUndefined($scope.ReportAdvancePayment_Tanggal)==true)
		{
			bsNotify.show({
				title:"Warning",
				content:"Tanggal harus diisi / format tanggal salah.",
				type:"warning"
			});
		}
		else if(new Date($scope.ReportAdvancePayment_Tanggal) > new Date()){
			bsNotify.show({
				title:"Warning",
				content:"Tanggal tidak bisa lebih besar dari hari ini.",
				type:"warning"
			});
		}
		else if (angular.isUndefined($scope.ReportAdvancePayment_NamaVendor)==true)
		{
			bsNotify.show({
				title:"Warning",
				content:"Vendor harus diisi.",
				type:"warning"
			});
		}
		else if (angular.isUndefined($scope.ReportAdvancePayment_Status)==true)
		{
			bsNotify.show({
				title:"Warning",
				content:"Status harus dipilih.",
				type:"warning"
			});
		}
		else{

			ReportAdvancePaymentFactory.getExcelReportAdvancePayment($filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "yyyyMMdd"),
				$scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			.then(
				function(res){

					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);

					saveAs(blob, 'Report_AdvancePayment' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);

		}
		
	}

}).directive('lettersOnly', function () {  
	return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/[^a-zA-Z]/g, '');
            //console.log(transformedInput);
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      }
});