angular.module('app')
	.factory('ReportPenerimaanFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchRptPenerimaan=function(orgId) {
        //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
        var url = '/api/fe/Branch/SelectDataNew?start=0&limit=0&FilterData='+ orgId;
        var res=$http.get(url);  
        return res;			
        }; 

        factory.getMesinEDC = function(start, limit,idEDC){
                //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
                // api/fe/FinanceEDC/GetMasterData/start/1/limit/10/Branch/280
                var url = '/api/fe/FinanceEDC/GetMasterData/start/'+start+'/limit/'+limit+'/Branch/'+idEDC;
                var res=$http.get(url);  
                return res;   
        }

        factory.getDataAccount= function(id) {
                //var url = '/api/fe/BankAccount/SelectData/start/0/limit/0/filterData/' + id;
                var url = '/api/fe/BankAccount/SelectData?start=0&limit=0&filterData=' + id;
                var res=$http.get(url);  
                return res;
        };   
        factory.getDataAccount2= function(id,OutletId) {
                //var url = '/api/fe/BankAccount/SelectData/start/0/limit/0/filterData/' + id;
                var url = '/api/fe/BankAccount/SelectData2?start=0&limit=0&filterData=' + id +'&OutletId='+OutletId;
                var res=$http.get(url);  
                return res;
        }; 

        factory.getExcelReportPenerimaan=function(startdate,enddate,branch,AccountId,metode) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenerimaan/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&AccountId=' + 
                        AccountId + '&PaymentMethod=' + metode + '&outletId=' +  branch;
                console.log('get --> ', url);
                var res=$http.get(url);
                return res;
	};

	factory.getCetakReportPenerimaan=function(startdate,enddate,branch,AccountId,metode) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptPenerimaan/Cetak?StartDate=' + startdate + '&EndDate=' + enddate + '&AccountId=' + 
                        AccountId + '&PaymentMethod=' + metode + '&outletId=' +  branch;
                var res=$http.get(url , {responseType: 'arraybuffer'});

                return res;
	};

	return factory;
});