angular.module('app')
	.controller('ReportPenerimaanController', function ($scope, $http, $filter, CurrentUser, ReportPenerimaanFactory, $timeout, bsNotify) {
		$scope.user = CurrentUser.user();
		$scope.MainReportPenerimaan_Show = true;
		$scope.ReportPenerimaan_NomorRekening_Show = true;
		$scope.DataRekening = [];
		$scope.DataMesinEDC = [];
		$scope.limit = 1000;
		$scope.ReportPenerimaan_NomorRekening_EnableDisable = true;
		$scope.ReportPenerimaan_MetodePembayaranOptions = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash Collection", value: "Cash" }, { name: "Cash Operation", value: "Cash Operation" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }, { name: "Interbranch", value: "Interbranch" }];
		$scope.dateNow = Date.now();
		$scope.ReportPenerimaan_TanggalStart = new Date();
		$scope.ReportPenerimaan_TanggalEnd = new Date();
		if ($scope.user.OrgCode.substring(3, 9) == '000000') {
			Enable_Radio();
			$scope.Main_NamaBranch_EnableDisable = true;
		}
		else {
			Enable_BtnPDF();
			$scope.Main_NamaBranch_EnableDisable = false;
		}


		function Enable_BtnExcel() {
			$scope.Show_DownloadExcel = true;
		}

		function Enable_BtnPDF() {
			$scope.Show_DownloadPdf = true;
		}

		function Enable_Radio() {
			$scope.Show_RadioBtnRpt = true;
		}

		function DisableAll_Btn() {
			$scope.Show_DownloadPdf = false;
			$scope.Show_DownloadExcel = false;
		}

		$scope.FormatDownloadRptPenerimaan_SearchBy_ValueChanged = function () {
			DisableAll_Btn();
			console.log($scope.FormatDownloadRptPenerimaan_SearchBy);
			if ($scope.FormatDownloadRptPenerimaan_SearchBy == "1") {
				Enable_BtnExcel();
			}
			else {
				Enable_BtnPDF();
			}
		}

		$scope.ReportPenerimaan_NamaBranch = $scope.user.OrgId;
		//$scope.ReportPenerimaan_MetodePembayaran = null;
		$scope.SetComboBranch = function () {
			ReportPenerimaanFactory.getDataBranchRptPenerimaan($scope.user.OrgId).then(
				function (res) {
					$scope.optionsReportPenerimaan_NamaBranch = res.data.Result;
					$scope.ReportPenerimaan_NamaBranch = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		// Get Mesin EDC Start
		$scope.SetMesinEDC = function () {
			ReportPenerimaanFactory.getMesinEDC(1,$scope.limit,$scope.ReportPenerimaan_NamaBranch).then(
				function (res) {
					$scope.ReportPenerimaan_MesinEDCOptions = res.data.Result;
					$scope.DataMesinEDC = res.data.Result;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};
		// get mesin EDC End

		$scope.SetComboAccount = function () {
			ReportPenerimaanFactory.getDataAccount(0).then(
				function (res) {
					$scope.ReportPenerimaan_NomorRekeningOptions = res.data.Result;
					$scope.DataRekening = res.data.Result;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		$scope.SetComboAccount2 = function () {
			ReportPenerimaanFactory.getDataAccount2(0, $scope.ReportPenerimaan_NamaBranch).then(
				function (res) {
					$scope.ReportPenerimaan_NomorRekeningOptions = res.data.Result;
					$scope.DataRekening = res.data.Result;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		$scope.SetComboAccount();
		$scope.SetComboBranch();
		$scope.SetMesinEDC();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.ReportPenerimaan_NamaBranch_Changed = function(){
			$scope.ReportPenerimaan_MesinEDC = "";
			$scope.ReportPenerimaan_NomorRekening = "";
			$scope.ReportPenerimaan_NamaBank = "";
			$scope.SetMesinEDC();
			$scope.SetComboAccount2();
		}
		$scope.ReportPenerimaan_MetodePembayaran_Changed = function () {
			if ($scope.ReportPenerimaan_MetodePembayaran == "Bank Transfer") {
				$scope.ReportPenerimaan_NomorRekening_EnableDisable = false;
				$scope.ReportPenerimaan_NomorRekening_Show = true;
				$scope.ReportPenerimaan_MesinEDC_Show = false;
			}else if($scope.ReportPenerimaan_MetodePembayaran == "Credit/Debit Card"){
				$scope.ReportPenerimaan_MesinEDC_Show = true;
				$scope.ReportPenerimaan_NomorRekening_Show = false;
				$scope.ReportPenerimaan_MesinEDC_EnableDisable = false;
			}
			else {
				$scope.ReportPenerimaan_NomorRekening_EnableDisable = true;
				$scope.ReportPenerimaan_NomorRekening = "";
				$scope.ReportPenerimaan_MesinEDC_Show = false;
			}
		}

		$scope.ReportPenerimaan_NomorRekening_Changed = function () {
			$scope.ReportPenerimaan_NamaBank = "";
			for (i = 0; i < $scope.DataRekening.length; i++) {
				if ($scope.DataRekening[i].value == $scope.ReportPenerimaan_NomorRekening) {
					$scope.ReportPenerimaan_NamaBank = $scope.DataRekening[i].BankName;
				}
			}
		}

		$scope.ReportPenerimaan_MesinEDC_Changed = function () {
			$scope.ReportPenerimaan_NamaBank = "";
			for (i = 0; i < $scope.DataMesinEDC.length; i++) {
				if ($scope.DataMesinEDC[i].EDCId == $scope.ReportPenerimaan_MesinEDC) {
					$scope.ReportPenerimaan_NamaBank = $scope.DataMesinEDC[i].AccountNo.split("-")[1];
				}
			}
		}

		$scope.ReportPenerimaan_Cetak_Clicked = function () {
			var pdfFile = "";
			var endDate = "";

			if (!(
				(isNaN($scope.ReportPenerimaan_TanggalStart) == true) ||
				(angular.isUndefined($scope.ReportPenerimaan_TanggalStart) == true) ||
				($scope.ReportPenerimaan_TanggalStart == null))
				&& (!(((angular.isUndefined($scope.ReportPenerimaan_MetodePembayaran) == true) ||
					($scope.ReportPenerimaan_MetodePembayaran == null)))
				)
				&& (!(((angular.isUndefined($scope.ReportPenerimaan_MesinEDC) == true) ||
					($scope.ReportPenerimaan_MesinEDC == null)))
				)
				&& (!(
					($scope.ReportPenerimaan_NomorRekening_EnableDisable == false) &&
					((isNaN($scope.ReportPenerimaan_NomorRekening) == true) ||
						(angular.isUndefined($scope.ReportPenerimaan_NomorRekening) == true) ||
						($scope.ReportPenerimaan_NomorRekening == ""))))
			) {
				if ((isNaN($scope.ReportPenerimaan_TanggalEnd) != true) &&
					(angular.isUndefined($scope.ReportPenerimaan_TanggalEnd) != true)) {
					endDate = $scope.formatDate($scope.ReportPenerimaan_TanggalEnd);
				}
				else {
					endDate = $scope.formatDate(new Date());
				}

				ReportPenerimaanFactory.getCetakReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
					endDate, $scope.ReportPenerimaan_NamaBranch, $scope.ReportPenerimaan_NomorRekening,
					$scope.ReportPenerimaan_MetodePembayaran).then(
						function (res) {
							var file = new Blob([res.data], { type: 'application/pdf' });
							var fileURL = URL.createObjectURL(file);
							//window.open(fileURL);

							console.log("pdf", fileURL);
							//$scope.content = $sce.trustAsResourceUrl(fileURL);
							//pdfFile = fileURL;

							printJS(fileURL);
							//printJS({printable: file,showModal:true,type: 'pdf'})
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
			else {
				alert('Data harus lengkap di isi !');
			}
		};

		$scope.ReportPenerimaan_PDF_Clicked = function () {
			var endDate = "";
			//Penjagaan Jika tanggal lebih dari 30 hari
			var date1 = new Date($scope.ReportPenerimaan_TanggalStart);
			var date2 = new Date($scope.ReportPenerimaan_TanggalEnd);
			var t = date2.getTime() - date1.getTime(); 
			var MesinEDC = $scope.ReportPenerimaan_MesinEDC;
   
			var DaysDiff = t / (1000 * 3600 * 24);
			
			console. log("tanggal 1>>>", date1);
			console. log("tanggal 2>>>", date2);

			if (((isNaN($scope.ReportPenerimaan_TanggalStart) == true) ||
				(angular.isUndefined($scope.ReportPenerimaan_TanggalStart) == true) ||
				($scope.ReportPenerimaan_TanggalStart == null)) || ((isNaN($scope.ReportPenerimaan_TanggalEnd) == true) ||
					(angular.isUndefined($scope.ReportPenerimaan_TanggalEnd) == true) ||
					($scope.ReportPenerimaan_TanggalEnd == null))) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal harus diisi / format tanggal salah.",
					type: "warning"
				});
			}
			else if ((new Date($scope.ReportPenerimaan_TanggalStart) > new Date()) ||
				(new Date($scope.ReportPenerimaan_TanggalEnd) > new Date())) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if (new Date($scope.ReportPenerimaan_TanggalStart) > new Date($scope.ReportPenerimaan_TanggalEnd)) {

				bsNotify.show({
					title: "Warning",
					content: "Tanggal awal tidak boleh lebih dari tanggal akhir.",
					type:"warning"
				});
			}
			else if (DaysDiff > 30 ){
				bsNotify.show({
					title:"Warning",
					content:"Tanggal tidak boleh lebih dari 30 Hari.",
					type: "warning"
				});
			}
			else {

				if (!(
					(isNaN($scope.ReportPenerimaan_TanggalStart) == true) ||
					(angular.isUndefined($scope.ReportPenerimaan_TanggalStart) == true) ||
					($scope.ReportPenerimaan_TanggalStart == null))
					&& (!(((angular.isUndefined($scope.ReportPenerimaan_MetodePembayaran) == true) ||
						($scope.ReportPenerimaan_MetodePembayaran == null)))
					)
					&& ( (!(((angular.isUndefined($scope.ReportPenerimaan_MesinEDC) == true) ||
						($scope.ReportPenerimaan_MesinEDC == null)||($scope.ReportPenerimaan_MesinEDC == "")))
					)
					|| (!(
						($scope.ReportPenerimaan_NomorRekening_EnableDisable == false) &&
						((isNaN($scope.ReportPenerimaan_NomorRekening) == true) ||
							(angular.isUndefined($scope.ReportPenerimaan_NomorRekening) == true) ||
							($scope.ReportPenerimaan_NomorRekening == ""))))
				)) {
					if ((isNaN($scope.ReportPenerimaan_TanggalEnd) != true) &&
						(angular.isUndefined($scope.ReportPenerimaan_TanggalEnd) != true)) {
						endDate = $scope.formatDate($scope.ReportPenerimaan_TanggalEnd);
					}
					else {
						endDate = $scope.formatDate(new Date());
					}

					if ($scope.user.OrgCode.substring(3, 9) == '000000') {
						console.log('masuk', $scope.user.OrgCode);
						if ($scope.FormatDownloadRptPenerimaan_SearchBy == '1') {
							if($scope.ReportPenerimaan_MetodePembayaran == "Credit/Debit Card"){
								ReportPenerimaanFactory.getExcelReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
								endDate, $scope.ReportPenerimaan_NamaBranch, MesinEDC,
								$scope.ReportPenerimaan_MetodePembayaran).then(
									function (res) {
										var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
										var b64Data = res.data;

										var blob = b64toBlob(b64Data, contentType);
										var blobUrl = URL.createObjectURL(blob);
										// window.open(blobUrl);
										//$scope.sm_show2=false;
										saveAs(blob, 'Report_Penerimaan' + '.xlsx');
										// var contentType = 'application/pdf';
										// var blob = new Blob([res.data], { type: contentType });
										// saveAs(blob, 'Report_Penerimaan.pdf');
										// return res;
									},
									function (err) {
										console.log("err=>", err);
									}
								);
							}else{
								ReportPenerimaanFactory.getExcelReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
								endDate, $scope.ReportPenerimaan_NamaBranch, $scope.ReportPenerimaan_NomorRekening,
								$scope.ReportPenerimaan_MetodePembayaran).then(
									function (res) {
										var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
										var b64Data = res.data;

										var blob = b64toBlob(b64Data, contentType);
										var blobUrl = URL.createObjectURL(blob);
										// window.open(blobUrl);
										//$scope.sm_show2=false;
										saveAs(blob, 'Report_Penerimaan' + '.xlsx');
										// var contentType = 'application/pdf';
										// var blob = new Blob([res.data], { type: contentType });
										// saveAs(blob, 'Report_Penerimaan.pdf');
										// return res;
									},
									function (err) {
										console.log("err=>", err);
									}
								);
							}
						} else {
							if($scope.ReportPenerimaan_MetodePembayaran == "Credit/Debit Card"){
								ReportPenerimaanFactory.getCetakReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
								endDate, $scope.ReportPenerimaan_NamaBranch, MesinEDC,
								$scope.ReportPenerimaan_MetodePembayaran).then(
									function (res) {
										// var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
										// var b64Data = res.data;
	
										// var blob = b64toBlob(b64Data, contentType);
										// var blobUrl = URL.createObjectURL(blob);
										// // window.open(blobUrl);
										// //$scope.sm_show2=false;
										// saveAs(blob, 'Report_Penerimaan' + '.xlsx');
										var contentType = 'application/pdf';
										var blob = new Blob([res.data], { type: contentType });
										saveAs(blob, 'Report_Penerimaan.pdf');
										// return res;
									},
									function (err) {
										console.log("err=>", err);
									}
								);
							}else{
								ReportPenerimaanFactory.getCetakReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
								endDate, $scope.ReportPenerimaan_NamaBranch, $scope.ReportPenerimaan_NomorRekening,
								$scope.ReportPenerimaan_MetodePembayaran).then(
									function (res) {
										// var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
										// var b64Data = res.data;

										// var blob = b64toBlob(b64Data, contentType);
										// var blobUrl = URL.createObjectURL(blob);
										// // window.open(blobUrl);
										// //$scope.sm_show2=false;
										// saveAs(blob, 'Report_Penerimaan' + '.xlsx');
										var contentType = 'application/pdf';
										var blob = new Blob([res.data], { type: contentType });
										saveAs(blob, 'Report_Penerimaan.pdf');
										// return res;
									},
									function (err) {
										console.log("err=>", err);
									}
								);
							}
						}
					}
					else {
					
					if($scope.ReportPenerimaan_MetodePembayaran == "Credit/Debit Card"){
						ReportPenerimaanFactory.getCetakReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
							endDate, $scope.ReportPenerimaan_NamaBranch, MesinEDC,
							$scope.ReportPenerimaan_MetodePembayaran).then(
								function (res) {
									// var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
									// var b64Data = res.data;

									// var blob = b64toBlob(b64Data, contentType);
									// var blobUrl = URL.createObjectURL(blob);
									// // window.open(blobUrl);
									// //$scope.sm_show2=false;
									// saveAs(blob, 'Report_Penerimaan' + '.xlsx');
									var contentType = 'application/pdf';
									var blob = new Blob([res.data], { type: contentType });
									saveAs(blob, 'Report_Penerimaan.pdf');
									// return res;
								},
								function (err) {
									console.log("err=>", err);
								}
							);
					}else{
						ReportPenerimaanFactory.getCetakReportPenerimaan($scope.formatDate($scope.ReportPenerimaan_TanggalStart),
						endDate, $scope.ReportPenerimaan_NamaBranch, $scope.ReportPenerimaan_NomorRekening,
						$scope.ReportPenerimaan_MetodePembayaran).then(
							function (res) {
								// var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
								// var b64Data = res.data;

								// var blob = b64toBlob(b64Data, contentType);
								// var blobUrl = URL.createObjectURL(blob);
								// // window.open(blobUrl);
								// //$scope.sm_show2=false;
								// saveAs(blob, 'Report_Penerimaan' + '.xlsx');
								var contentType = 'application/pdf';
								var blob = new Blob([res.data], { type: contentType });
								saveAs(blob, 'Report_Penerimaan.pdf');
								// return res;
							},
							function (err) {
								console.log("err=>", err);
							}
						);
					}
						
					}


				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Data harus diisi lengkap.",
						type: "warning"
					});
				}

			}


		}

	});