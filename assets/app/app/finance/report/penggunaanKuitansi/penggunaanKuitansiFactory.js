angular.module('app')
	.factory('PenggunaanKuitansiFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
                var factory={};
                var debugMode=true;
		
        factory.getDataBranchPenggunaanKuitansi=function(currentUser) {
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+currentUser;
			var res=$http.get(url);  
			return res;			
        };
		
		factory.getDataParamPenggunaanKuitansi=function() {
			var url = '/api/fe/FinanceParam/?Id=PrePrintedReceiptNumStatusId';
			var res=$http.get(url);  
			return res;			
        };
		
		factory.getDataParamJenisPenggunaanKuitansi=function() {
			var url = '/api/fe/FinanceParam/?Id=PreprintedReceiptTypeId';
			var res=$http.get(url);  
			return res;			
        };
		
		
        factory.getExcelReportPenggunaanKuitansi = function(TipeKuitansi, Branch, NomorKuitansiAwal, NomorKuitansiAkhir, Status){
			
			var url = '/api/fe/FinanceRptPreprintedReceiptUsed/Excel?ReceiptType=' + TipeKuitansi 
															+ '&OutletId=' + Branch 
															+ '&ReceiptNoStart=' + NomorKuitansiAwal 
															+ '&ReceiptNoEnd='+ NomorKuitansiAkhir 
															+ '&Status=' + Status 
															;
			var res=$http.get(url);
			return res;
		};

	return factory;
});