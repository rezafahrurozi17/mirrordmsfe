angular.module('app')
.controller('PenggunaanKuitansiController', function ($scope, $http, $filter, CurrentUser, PenggunaanKuitansiFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.MainReportPenggunaanKuitansi_Show = true;
	
	// $scope.SetComboBranch=function() {
	// 	PenggunaanKuitansiFactory.getDataBranchPenggunaanKuitansi().then(
	// 		function(res) {
	// 			$scope.optionsFinanceReportPenggunaanKuitansi_NamaBranch = res.data.Result;	
	// 			$scope.FinanceReportPenggunaanKuitansi_NamaBranch = $scope.user.OrgId;
	// 		},
	// 		function (err) {
	// 			console.log('error -->', err)
	// 		}
	// 	);
	// };
	
	$scope.$on('$viewContentLoaded', function () {
		$scope.SetComboBranch();

	});
	

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.SetComboBranch = function () {
		var ho;
		ho = $scope.user.OrgId;
		PenggunaanKuitansiFactory.getDataBranchPenggunaanKuitansi(ho).then(
			function(res) {
				$scope.optionsFinanceReportPenggunaanKuitansi_NamaBranch = res.data.Result;	
				$scope.FinanceReportPenggunaanKuitansi_NamaBranch = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
		if ($scope.right($scope.user.OrgCode, 6) == '000000'||$scope.user.RoleName == 'ADH') {
			$scope.Main_NamaBranch_EnableDisable = true;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
			//ho = 0;
		}

		
		
		PenggunaanKuitansiFactory.getDataParamPenggunaanKuitansi().then(
			function(res){
				console.log("res data param : ", res);
				$scope.optionsFinanceReportPenggunaanKuitansi_Status = res.data;
			}
		);
		
		PenggunaanKuitansiFactory.getDataParamJenisPenggunaanKuitansi().then(
			function(res){
				console.log("res data param : ", res);
				$scope.optionsFinanceReportPenggunaanKuitansi_TipeKuitansi = res.data;
			}
		);
	};

	$scope.FinanceReportPenggunaanKuitansi_TipeKuitansi_Change = function () {
		if (!($scope.FinanceReportPenggunaanKuitansi_TipeKuitansi == '' || $scope.FinanceReportPenggunaanKuitansi_TipeKuitansi == null ||angular.isUndefined($scope.FinanceReportPenggunaanKuitansi_TipeKuitansi)))
		{
			PenggunaanKuitansiFactory.getDataParamPenggunaanKuitansi().then(
				function(res){
					console.log("res data param : ", res);
					var data = res.data;
					for(var i = 0; i < data.length; i++) {
						if(data[i].name == 'Tersedia') {
							data.splice(i, 1);
							break;
						}
					}
					$scope.optionsFinanceReportPenggunaanKuitansi_Status = data;
				}
			);
		}
		else
		{
			PenggunaanKuitansiFactory.getDataParamPenggunaanKuitansi().then(
				function(res){
					console.log("res data param : ", res);
					$scope.optionsFinanceReportPenggunaanKuitansi_Status = res.data;
				}
			);
		}
	}

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.FinanceReportPenggunaanKuitansi_Excel_Clicked=function() {
		//$scope.FinanceReportPenggunaanKuitansi_TipeKuitansi = 0;
		//$scope.FinanceReportPenggunaanKuitansi_NamaBranch = 31;
		//$scope.FinanceReportPenggunaanKuitansi_NomorAwal=1;
		//$scope.FinanceReportPenggunaanKuitansi_NomorAkhir=100
		//$scope.FinanceReportPenggunaanKuitansi_Status=0;

		if ($scope.FinanceReportPenggunaanKuitansi_NomorAwal == "" || angular.isUndefined($scope.FinanceReportPenggunaanKuitansi_NomorAwal)) {
			bsNotify.show({
				title: "Warning",
				content: "Nomor Kuitansi Preprinted Awal harus diisi!!",
				type: 'warning'
			});
			return false;
		}

		if ($scope.FinanceReportPenggunaanKuitansi_NomorAkhir == "" || angular.isUndefined($scope.FinanceReportPenggunaanKuitansi_NomorAkhir)) {
			bsNotify.show({
				title: "Warning",
				content: "Nomor Kuitansi Preprinted Akhir harus diisi!!",
				type: 'warning'
			});
			return false;
		}

		PenggunaanKuitansiFactory.getExcelReportPenggunaanKuitansi(
			$scope.FinanceReportPenggunaanKuitansi_TipeKuitansi, 
			$scope.FinanceReportPenggunaanKuitansi_NamaBranch,
			$scope.FinanceReportPenggunaanKuitansi_NomorAwal,
			$scope.FinanceReportPenggunaanKuitansi_NomorAkhir,
			$scope.FinanceReportPenggunaanKuitansi_Status
		)
			.then(
				function(res){
					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);
					// window.open(blobUrl);
					//$scope.sm_show2=false;
					saveAs(blob, 'Report_PenggunaanKuitansi' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
			}
		);
	}

});