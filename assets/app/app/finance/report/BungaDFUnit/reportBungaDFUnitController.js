angular.module('app')
.controller('ReportBungaDFUnitController', function ($scope, $http, $filter, CurrentUser, ReportBungaDFUnitFactory, $timeout) {
	$scope.user = CurrentUser.user();
	$scope.MainReportBungaDFUnit_Show = true;
	$scope.reportBungaDFUnit_MetodeBungaDFUnitOptions = [{name : "Bank Transfer", value : "Bank Transfer"}, {name :"Cash", value:"Cash"}];
	$scope.reportBungaDFUnit_MetodeBungaDFUnit = {};
	$scope.ReportBungaDFUnit_PilihNamaVendor = "";
	$scope.MainReportBungaDFUnit_Show = true;
	$scope.right=function (str, chr)
  	{
		return str.slice(str.length-chr,str.length);
 	}

	$scope.SetComboBranch=function() {
		var ho;
		if ($scope.right($scope.user.OrgCode,6) == '000000')
		{
			$scope.Main_NamaBranch_EnableDisable = false;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = true;
			ho = 0;
		}
		ReportBungaDFUnitFactory.getDataBranchRptBungaDFUnit(ho).then(
			function(res) {
				$scope.optionsReportBungaDFUnit_NamaBranch = res.data.Result;	
				$scope.ReportBungaDFUnit_NamaBranch = $scope.user.OutletId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};

	$scope.RefreshGrid=function() {
		ReportBungaDFUnitFactory.getListBungaDFUnit('0').then(
			function(res) {
				$scope.BungaDFUnit_UIGrid.data = res.data.Result;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.BungaDFUnit_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"NomorGL", displayName:"Nomor GL Account", field:"GLAccountNo", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"GLAccountName", displayName:"Nama GL Account", field:"GLAccountName", enableCellEdit: false, enableHiding: false},
				{name:"FrameNo", displayName:"Nomor Rangka", field:"FrameNo", enableCellEdit: false, enableHiding: false},
				{name:"PaymentInstructionDate", displayName:"Tanggal Instruksi Pembayaran", field:"PaymentInstructionDate", enableCellEdit: false,
					 enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'  },
				{name:"BungaDF", displayName:"Nominal Bunga DF", field:"BungaDF", enableCellEdit: false, enableHiding: false}
			],
		onRegisterApi: function(gridApi) {
			$scope.ReportBungaDFUnit_Vendor_UIGrid_gridAPI = gridApi;
		}
	};
	
	$scope.RefreshGrid();

	$scope.ReportBungaDFUnit_PilihNamaVendor_Click=function() {
			ReportBungaDFUnitFactory.getVendorListRptBungaDFUnit( 'reportBungaDFUnit' , '').then(
				function(res) {
					$scope.ReportBungaDFUnit_Vendor_UIGrid.data = res.data.Result;
					angular.element('#Modal_ReportBungaDFUnit_PilihVendor').modal('setting',{closeable:false}).modal('show');
				},
				function(err) {
					console.log("err=>", err);
				} 
			);	
	};

	$scope.Pilih_ReportBungaDFUnit_Vendor = function() {
		if ($scope.ReportBungaDFUnit_Vendor_UIGrid_gridAPI.selection.getSelectedCount() > 0)
		{
			$scope.ReportBungaDFUnit_Vendor_UIGrid_gridAPI.selection.getSelectedRows().forEach(function(row) {	
				$scope.ReportBungaDFUnit_PilihNamaVendor =  row.Name;
				$scope.ReportBungaDFUnit_PilihIdVendor = row.Id;
			});
			
			angular.element('#Modal_ReportBungaDFUnit_PilihVendor').modal('hide');	
		}
		else 
		{
			alert('Belum ada data yang dipilih');
		}			
	}

	$scope.Batal_ReportBungaDFUnit_Vendor=function() {
		$scope.ReportBungaDFUnit_PilihNamaVendor =  "";
		$scope.ReportBungaDFUnit_PilihIdVendor = "";
		angular.element('#Modal_ReportBungaDFUnit_PilihVendor').modal('hide');	
	}

	//$scope.SetComboBranch();

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.ReportBungaDFUnit_Cetak_Clicked = function() {        
		if(  ( (isNaN($scope.reportBungaDFUnit_TanggalStart) != true) &&  
			 (angular.isUndefined($scope.reportBungaDFUnit_TanggalStart) != true) &&
			  ($scope.reportBungaDFUnit_TanggalStart != null)
			 ) &&
			   ( (isNaN($scope.ReportBungaDFUnit_NamaBranch) != true) && 
			  (angular.isUndefined($scope.ReportBungaDFUnit_NamaBranch) != true) )&&
			 (angular.isUndefined($scope.reportBungaDFUnit_MetodeBungaDFUnit) != true) &&  

			// ( ( angular.isUndefined($scope.ReportBungaDFUnit_PilihIdVendor) != true)  &&
			// 	(isNaN($scope.ReportBungaDFUnit_PilihIdVendor) != true) && 
			// 	($scope.ReportBungaDFUnit_PilihIdVendor != "") ) && 
				(  (angular.isUndefined($scope.ReportBungaDFUnit_PilihNamaVendor) != true) &&
					($scope.ReportBungaDFUnit_PilihNamaVendor != "") ) && 
				
				  ( (isNaN($scope.reportBungaDFUnit_TanggalEnd) != true) && 
					 (angular.isUndefined($scope.reportBungaDFUnit_TanggalEnd) != true) &&
					 ($scope.reportBungaDFUnit_TanggalEnd != null)
					 )
			)
		{		 
			var pdfFile="";  
			// var endDate="";

			// if	 ( (isNaN($scope.reportBungaDFUnit_TanggalEnd) != true) && 
			//  (angular.isUndefined($scope.reportBungaDFUnit_TanggalEnd) != true) )
			//  {
			// 	endDate = $scope.formatDate($scope.reportBungaDFUnit_TanggalEnd);
			//  }
            ReportBungaDFUnitFactory.getCetakReportBungaDFUnit($scope.formatDate($scope.reportBungaDFUnit_TanggalStart), 
				 $scope.formatDate($scope.reportBungaDFUnit_TanggalEnd), $scope.ReportBungaDFUnit_NamaBranch, $scope.ReportBungaDFUnit_PilihNamaVendor , 
				$scope.reportBungaDFUnit_MetodeBungaDFUnit ).then(
				function (res) {
                var file = new Blob([res.data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                //window.open(fileURL);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                //pdfFile = fileURL;

				printJS(fileURL);
				//printJS({printable: file,showModal:true,type: 'pdf'})
            },
			function(err){
					console.log("err=>",err);
				}
			);
		}
		else {
			alert('Data parameter tidak lengkap diisi !');
		}
	};

	$scope.ReportBungaDFUnit_Excel_Clicked=function() {
		// if(  ( (isNaN($scope.reportBungaDFUnit_TanggalStart) != true) &&  
		// 	 (angular.isUndefined($scope.reportBungaDFUnit_TanggalStart) != true) &&
		// 	  ($scope.reportBungaDFUnit_TanggalStart != null)
		// 	 ) &&
		// 	   ( (isNaN($scope.ReportBungaDFUnit_NamaBranch) != true) && 
		// 	 (angular.isUndefined($scope.ReportBungaDFUnit_NamaBranch) != true) )&&
		// 	 (angular.isUndefined($scope.reportBungaDFUnit_MetodeBungaDFUnit) != true) &&  

		// 	// ( ( angular.isUndefined($scope.ReportBungaDFUnit_PilihIdVendor) != true)  &&
		// 	// 	(isNaN($scope.ReportBungaDFUnit_PilihIdVendor) != true) && 
		// 	// 	($scope.ReportBungaDFUnit_PilihIdVendor != "") ) && 
		// 		(  (angular.isUndefined($scope.ReportBungaDFUnit_PilihNamaVendor) != true) &&
		// 			($scope.ReportBungaDFUnit_PilihNamaVendor != "") ) && 

		// 		  ( (isNaN($scope.reportBungaDFUnit_TanggalEnd) != true) && 
		// 			 (angular.isUndefined($scope.reportBungaDFUnit_TanggalEnd) != true) &&
		// 			 ($scope.reportBungaDFUnit_TanggalEnd != null)
		// 			 )
		// 	)
		// {
		// 	// var endDate="";
		// 	console.log('isi vendor', $scope.ReportBungaDFUnit_PilihNamaVendor);
		// 	// if	 ( (isNaN($scope.reportBungaDFUnit_TanggalEnd) != true) && 
		// 	//  (angular.isUndefined($scope.reportBungaDFUnit_TanggalEnd) != true) )
		// 	//  {
		// 	// 	endDate = $scope.formatDate($scope.reportBungaDFUnit_TanggalEnd);
		// 	//  }

			ReportBungaDFUnitFactory.getExcelReportBungaDFUnit().then(
				function(res){
					var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
					var b64Data = res.data;

					var blob = b64toBlob(b64Data, contentType);
					var blobUrl = URL.createObjectURL(blob);
					saveAs(blob, 'Report_BungaDFUnit' + '.xlsx');

					return res;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		//  }
		//  else {
		//  	alert('Data parameter tidak lengkap diisi !');
		//  }
	}

});