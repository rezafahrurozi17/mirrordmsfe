angular.module('app')
	.factory('reportRekonFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataBranch=function() {
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
      var res=$http.get(url);  
      return res;			
    };

    factory.getVendorList= function(tipe, name) {
        var FilterData = [{PIType : tipe, VendorName : name, ShowAll : 1}];
        var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData));
        return res;
    };   

	factory.getExcelReportRekon=function(date,NoRek) {
        var url = '/api/fe/FinanceRptRekonByDate/Excel/?AccountNo=' + NoRek+ '&Tanggal=' + date;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};

	factory.getCetakReportAdvancePayment=function(date,status,vendorName) {
        var url = '/api/fe/FinanceRptAdvancePayment/Cetak/?Date=' + date + '&VendorName=' + vendorName + '&Status=' + status;
        var res=$http.get(url , {responseType: 'arraybuffer'});

        return res;
	};

	return factory;
});