angular.module('app')
.controller('reportRekonController', function ($scope, $http, $filter, CurrentUser, reportRekonFactory, $timeout) {
	$scope.user = CurrentUser.user();
	//$scope.optionsReportAdvancePayment_Status = [{name:"Semua", value:"Semua"}, {name:"Open", value:"Open"}, {name:"Close", value:"Close"}];
	$scope.ReportRekon_Tanggal = new Date();
	console.log('a');
	
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	// $scope.Report_Cetak_Clicked = function() {         
			// var pdfFile="";  
			// var endDate="";

            // ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
				// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
			// .then(
				// function (res) {
                // var file = new Blob([res.data], {type: 'application/pdf'});
                // var fileURL = URL.createObjectURL(file);

                // console.log("pdf", fileURL);


				// printJS(fileURL);
            // },
			// function(err){
					// console.log("err=>",err);
				// }
			// );
	// };

	$scope.Report_Excel_Clicked=function() {

		reportRekonFactory.getExcelReportRekon($filter('date')(new Date($scope.ReportRekon_Tanggal), "yyyyMMdd"),
				$scope.ReportRekon_NoRek)
		.then(
			function(res){

				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
				var b64Data = res.data;

				var blob = b64toBlob(b64Data, contentType);
				var blobUrl = URL.createObjectURL(blob);

				saveAs(blob, 'Report_Rekon' + '.xlsx');

				return res;
			},
			function(err){
				console.log("err=>",err);
			}
		);
	}

});