angular.module('app')
.controller('ReportCashMarginController', function ($scope, $http, $filter, CurrentUser, ReportCashMarginFactory, $timeout,bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.ReportCashMargin_Show = true;
	$scope.ReportCashMargin_NamaBranch_EnableDisable = true;
	$scope.dateNow = Date.now();

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.SetComboBranch=function() {
		var ho;
		//RoleName == 'HO'
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.ReportCashMargin_NamaBranch_EnableDisable = false;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.ReportCashMargin_NamaBranch_EnableDisable = true;
			ho = 0;
		}

		ReportCashMarginFactory.getBranchByOutletId(ho)
			.then(
				function (res) {
					$scope.ReportCashMargin_NamaBranchOptions = res.data.Result;	
					$scope.ReportCashMargin_NamaBranch = $scope.user.OrgId;
				}
			);

		// ReportCashMarginFactory.getDataBranchRptCashMargin().then(
		// 	function(res) {
		// 		$scope.ReportCashMargin_NamaBranchOptions = res.data.Result;	
		// 		$scope.ReportCashMargin_NamaBranch = $scope.user.OrgId;
		// 	},
		// 	function (err) {
		// 		console.log('error -->', err)
		// 	}
		// );
	};

	$scope.SetComboBranch();

    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};


	$scope.reportCashMargin_ButtonExcel_Clicked=function() {
			var endDate="";
			var endDECDate = "";
			var startDate = "";
			var startDECDate = "";

			if	 ( ( (isNaN($scope.reportCashMargin_TanggalStart) == true) ||  (angular.isUndefined($scope.reportCashMargin_TanggalStart) == true) 
					|| ($scope.reportCashMargin_TanggalStart === null) 	)
				&& 
				( (isNaN($scope.reportCashMargin_TanggalDECStart) == true) ||  (angular.isUndefined($scope.reportCashMargin_TanggalDECStart) == true) 
				|| ($scope.reportCashMargin_TanggalDECStart === null) )
			  	)  
			 {
				//alert('Tanggal Lunas atau DEC harus salah satu diisi !');
				bsNotify.show({
					title:"Warning",
					content:"Tanggal Lunas atau DEC harus salah satu diisi.",
					type:"warning"
				});
			 }
			 else if 
				( ( (isNaN($scope.reportCashMargin_TanggalStart) == false) &&  (angular.isUndefined($scope.reportCashMargin_TanggalStart) == false)
					&& ($scope.reportCashMargin_TanggalStart !== null) 	)
				&& 
				( (isNaN($scope.reportCashMargin_TanggalDECStart) == false) &&  (angular.isUndefined($scope.reportCashMargin_TanggalDECStart) == false) )
					 && ($scope.reportCashMargin_TanggalDECStart !== null) )
				{
					//alert('Tanggal Lunas atau DEC harus salah satu diisi tidak boleh keduanya !');
					bsNotify.show({
						title:"Warning",
						content:"Tanggal Lunas atau DEC harus salah satu diisi tidak boleh keduanya .",
						type:"warning"
					});
					console.log('sisi DEC', $scope.reportCashMargin_TanggalDECStart);
					console.log('Mauk isnan' , isNaN($scope.reportCashMargin_TanggalDECStart));
					console.log('masuk undefined', angular.isUndefined($scope.reportCashMargin_TanggalDECStart));

					console.log('isi start',$scope.reportCashMargin_TanggalStart );
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalDECStart) == false) 
						&&(new Date($scope.reportCashMargin_TanggalDECStart)> new Date())){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal tidak bisa lebih besar dari hari ini.",
								type:"warning"
							});
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalDECEnd) == false) 
						&&(new Date($scope.reportCashMargin_TanggalDECEnd)> new Date())){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal tidak bisa lebih besar dari hari ini.",
								type:"warning"
							});
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalAkhir) == false) 
						&&(new Date($scope.reportCashMargin_TanggalAkhir)> new Date())){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal tidak bisa lebih besar dari hari ini.",
								type:"warning"
							});
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalStart) == false) 
						&&(new Date($scope.reportCashMargin_TanggalStart)> new Date())){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal tidak bisa lebih besar dari hari ini.",
								type:"warning"
							});
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalStart) == false && angular.isUndefined($scope.reportCashMargin_TanggalAkhir) == false) 
						&&(new Date($scope.reportCashMargin_TanggalStart)> new Date($scope.reportCashMargin_TanggalAkhir)  ) ){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal lunas awal tidak bisa lebih besar dari tanggal lunas akhir.",
								type:"warning"
							});
				}
				else if ( (angular.isUndefined($scope.reportCashMargin_TanggalDECStart) == false && angular.isUndefined($scope.reportCashMargin_TanggalDECAkhir) == false) 
						&&(new Date($scope.reportCashMargin_TanggalDECStart)> new Date($scope.reportCashMargin_TanggalDECAkhir)  ) ){
							bsNotify.show({
								title:"Warning",
								content:"Tanggal DEC awal tidak bisa lebih besar dari tanggal DEC akhir.",
								type:"warning"
							});
				}
			 else {
				if	 ( (isNaN($scope.reportCashMargin_TanggalStart) != true) && 
				(angular.isUndefined($scope.reportCashMargin_TanggalStart) != true) )
				{
					startDate = $scope.formatDate($scope.reportCashMargin_TanggalStart);
				}
				else {
					startDate = "";
				}

				if	 ( (isNaN($scope.reportCashMargin_TanggalDECStart) != true) && 
				(angular.isUndefined($scope.reportCashMargin_TanggalDECStart) != true) )
				{
					startDECDate = $scope.formatDate($scope.reportCashMargin_TanggalDECStart);
				}
				else {
					startDECDate = "";
				}

				if	 ( (isNaN($scope.reportCashMargin_TanggalAkhir) != true) && 
				(angular.isUndefined($scope.reportCashMargin_TanggalAkhir) != true) )
				{
					endDate = $scope.formatDate($scope.reportCashMargin_TanggalAkhir);
					if	 ( (isNaN($scope.reportCashMargin_TanggalStart) == true) && 
					(angular.isUndefined($scope.reportCashMargin_TanggalStart) == true) )
					{
						$scope.reportCashMargin_TanggalAkhir == "";
						endDate = "";

					}
				}
				else {
					endDate = "";
				}

				if	 ( (isNaN($scope.reportCashMargin_TanggalDECEnd) != true) && 
				(angular.isUndefined($scope.reportCashMargin_TanggalDECEnd) != true) )
				{
					endDECDate = $scope.formatDate($scope.reportCashMargin_TanggalDECEnd);
					if	 ( (angular.isUndefined($scope.reportCashMargin_TanggalDECStart) == true) )
					{
						$scope.reportCashMargin_TanggalDECEnd == "";
						endDECDate = "";

					}
				}
				else {
					endDECDate = "";
				}			 

				ReportCashMarginFactory.getExcelReportCashMargin( startDate, 
					endDate , startDECDate , endDECDate, $scope.ReportCashMargin_NamaBranch).then(
					function(res){

						var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
						var b64Data = res.data;

						var blob = b64toBlob(b64Data, contentType);
						var blobUrl = URL.createObjectURL(blob);
						// window.open(blobUrl);
						//$scope.sm_show2=false;
						saveAs(blob, 'Report_CashMargin' + '.xlsx');

						return res;
					},
					function(err){
						console.log("err=>",err);
						if (err != null) 
						{
							alert(err.data.Message);
						}	
						else {
							alert('Gagal membuat laporan !');
						}
					}
				);
			 }
	}

});