angular.module('app')
	.factory('ReportCashMarginFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
        factory.getDataBranchRptCashMargin=function() {
                //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
                var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=0';
                var res=$http.get(url);  
                return res;			
        };

	factory.getExcelReportCashMargin=function(startdate,enddate,startdecdate, enddecdate, branch) {
                //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
                var url = '/api/fe/FFinanceRptCashMargin/Excel?StartDate=' + startdate + '&EndDate=' + enddate + '&startdecdate=' + 
                        startdecdate + '&enddecdate=' + enddecdate + '&outletId=' +  branch;
                var res=$http.get(url);
                return res;
        };
        
        factory.getBranchByOutletId= function(outletId){
                //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
                var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + outletId;
                var res=$http.get(url);  
                return res;	
        };

	return factory;
});