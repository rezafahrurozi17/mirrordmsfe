angular.module('app')
	.factory('ReportProfitLossFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;		

	factory.getExcelReportProfitLoss=function(dateStartOrigin,dateEndOrigin,dateStartDiff, dateEndDiff,OutletId) {
        var url = '/api/fe/FinanceRptProfitLoss/Excel/?datestartorigin=' + dateStartOrigin + '&dateendorigin=' + dateEndOrigin + '&datestartdiff='+dateStartDiff+'&dateenddiff=' + dateEndDiff+'&outletid='+OutletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};
	
	factory.getBranchByOutletId= function(outletId){
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + outletId;
			var res=$http.get(url);  
			return res;	
	};
	
	return factory;
});