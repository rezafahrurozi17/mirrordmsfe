angular.module('app')
	.controller('ReportProfitLossController', function ($scope, $http, $filter, CurrentUser, ReportProfitLossFactory, $timeout, bsNotify) {
		var user = CurrentUser.user();

		var dateFormat = 'dd/MM/yyyy';

		$scope.dateOptions = { format: dateFormat }

		$scope.right = function (str, chr) {
			return str.slice(str.length - chr, str.length);
		}

		$scope.ReportProfitLoss_GetDataBranchByOutletId = function () {
			var ho;
			//RoleName == 'HO'
			if ($scope.right(user.OrgCode, 6) == '000000') {
				$scope.ReportProfitLoss_Branch_isDisabled = true;
				ho = user.OrgId;
			}
			else {
				$scope.ReportProfitLoss_Branch_isDisabled = false;
				ho = 0;
			}

			ReportProfitLossFactory.getBranchByOutletId(ho)
				.then(
					function (res) {
						console.log("res data BranchName : ", res.data.Result[0]);
						$scope.ReportProfitLoss_Branch = user.OutletId;
						$scope.optionsReportProfitLoss_Branch = res.data.Result;
					}
				);
		}

		$scope.ReportProfitLoss_GetDataBranchByOutletId();

		function b64toBlob(b64Data, contentType) {
			contentType = contentType || '';
			sliceSize = 512;

			var byteCharacters = atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}

				var byteArray = new Uint8Array(byteNumbers);

				byteArrays.push(byteArray);
			}

			var blob = new Blob(byteArrays, { type: contentType });
			return blob;
		}

		// $scope.Report_Cetak_Clicked = function() {         
		// var pdfFile="";  
		// var endDate="";

		// ReportAdvancePaymentFactory.getCetakReportAdvancePayment( $filter('date')(new Date($scope.ReportAdvancePayment_Tanggal), "dd-MM-yyyy"),
		// $scope.ReportAdvancePayment_Status, $scope.ReportAdvancePayment_NamaVendor)
		// .then(
		// function (res) {
		// var file = new Blob([res.data], {type: 'application/pdf'});
		// var fileURL = URL.createObjectURL(file);

		// console.log("pdf", fileURL);


		// printJS(fileURL);
		// },
		// function(err){
		// console.log("err=>",err);
		// }
		// );
		// };

		$scope.Report_Excel_Clicked = function () {
			$scope.tanggalPeriode_Changed();
			$scope.tanggalPeriodePembanding_Changed();
			if (!$scope.tanggalPeriodeOK) {
				bsNotify.show({
					title: "Warning",
					content: $scope.errTanggalPeriode,
					type: "warning"
				});
				return false;
			}
			if (!$scope.tanggalPeriodePembandingOK) {
				bsNotify.show({
					title: "Warning",
					content: $scope.errTanggalPeriodePembanding,
					type: "warning"
				});				
				return false;
			}

			ReportProfitLossFactory.getExcelReportProfitLoss($filter('date')(new Date($scope.ReportProfitLoss_TanggalAwal), "yyyyMMdd"),
				$filter('date')(new Date($scope.ReportProfitLoss_TanggalAkhir), "yyyyMMdd"),
				$filter('date')(new Date($scope.ReportProfitLoss_TanggalPembandingAwal), "yyyyMMdd"),
				$filter('date')(new Date($scope.ReportProfitLoss_TanggalPembandingAkhir), "yyyyMMdd"),$scope.ReportProfitLoss_Branch)
				.then(
					function (res) {

						var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
						var b64Data = res.data;

						var blob = b64toBlob(b64Data, contentType);
						var blobUrl = URL.createObjectURL(blob);

						saveAs(blob, 'Report_ProfitLoss' + '.xlsx');

						return res;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.tanggalPeriodeOK = true;
		$scope.tanggalPeriode_Changed = function () {
			var dt = $scope.ReportProfitLoss_TanggalAwal;
			var dtTo = $scope.ReportProfitLoss_TanggalAkhir;

			var now = new Date();

			if (dt > dtTo) {
				$scope.errTanggalPeriode = "Tanggal periode awal tidak boleh lebih besar dari tanggal periode akhir";
				$scope.tanggalPeriodeOK = false;
			}
			else if (dt > now) {
				$scope.errTanggalPeriode = "Tanggal periode awal tidak boleh lebih besar dari tanggal hari ini";
				$scope.tanggalPeriodeOK = false;
			}
			else if (dtTo > now) {
				$scope.errTanggalPeriode = "Tanggal periode akhir tidak boleh lebih besar dari tanggal hari ini";
				$scope.tanggalPeriodeOK = false;
			}
			else {
				$scope.tanggalPeriodeOK = true;
			}
		}

		$scope.tanggalPeriodePembandingOK = true;
		$scope.tanggalPeriodePembanding_Changed = function () {
			var dt = $scope.ReportProfitLoss_TanggalPembandingAwal;
			var dtTo = $scope.ReportProfitLoss_TanggalPembandingAkhir;

			var now = new Date();

			if (dt > dtTo) {
				$scope.errTanggalPeriodePembanding = "Tanggal periode pembanding awal tidak boleh lebih besar dari tanggal periode pembanding akhir";
				$scope.tanggalPeriodePembandingOK = false;
			}
			else if (dt > now) {
				$scope.errTanggalPeriodePembanding = "Tanggal periode pembanding awal tidak boleh lebih besar dari tanggal hari ini";
				$scope.tanggalPeriodePembandingOK = false;
			}
			else if (dtTo > now) {
				$scope.errTanggalPeriodePembanding = "Tanggal periode pembanding akhir tidak boleh lebih besar dari tanggal hari ini";
				$scope.tanggalPeriodePembandingOK = false;
			}
			else {
				$scope.tanggalPeriodePembandingOK = true;
			}
		}

	});