angular.module('app')
.controller('ReportEStampDutyController', function ($scope, $http, $filter, CurrentUser, ReportEStampDutyFactory, $timeout) {
	$scope.user = CurrentUser.user();
	$scope.ReportEStampDuty_SelectBranch = "";
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
	}

	var user = CurrentUser.user();
	
	if(user.OrgCode.substring(3, 9) == '000000')
		// $scope.ReportEStampDuty_HO_Show = true;
		$scope.ReportEStampDuty_Branch_isDisabled = true;
	else
		// $scope.ReportEStampDuty_HO_Show = false;
		$scope.ReportEStampDuty_Branch_isDisabled = false;
	$scope.ReportEStampDuty_getDataBranch = function () 
	{	
		ReportEStampDutyFactory.getDataBranchComboBox()
		.then
		(
			function (res) 
			{
				$scope.optionsMainReportEStampDuty_SelectBranch = res.data;
				$scope.optionsTambahReportEStampDuty_SelectBranch = res.data;
				$scope.optionsReportEStampDuty_SelectBranch = res.data;
				$scope.ReportEStampDuty_SelectBranch = res.data[0].value;
				console.log("user.OutletId", user.OutletId);
				console.log("res data branch",res.data[0].value);
				
				// res.data.some(function(obj, i){
					// if(obj.value == user.OutletId)
						// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
				// });
				
				console.log("$scope.ReportEStampDuty_SelectBranch : ", $scope.ReportEStampDuty_SelectBranch);
				
				res.data.some(function(obj, i){
					if(obj.value == user.OutletId){
						$scope.ReportEStampDuty_NamaBranch = obj.name;
						$scope.ReportEStampDuty_NamaBranch = obj.name;
					}
				});
			},
			function (err) 
			{
				console.log("err=>", err);
			}
		);
	};
	
	$scope.ReportEStampDuty_getDataBranch();

	$scope.ReportEStampDutyTanggalAwalOK = true;
	$scope.ReportEStampDutyTanggalAwalChanged = function () {
		
		console.log($scope.ReportEStampDutyTanggalAwalOK);
		var dt = $scope.ReportEStampDuty_TanggalAwal;
		var now = new Date();
		 if (dt > now) {
			$scope.errReportEStampDutyTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.ReportEStampDutyTanggalAwalOK = false;			
		}
		else {
			$scope.ReportEStampDutyTanggalAwalOK = true;
		}
		console.log('asdasd ' + $scope.ReportEStampDutyTanggalAwalOK);
	}
	$scope.ReportEStampDutyTanggalAkhirOK = true;
	$scope.ReportEStampDutyTanggalAkhirChanged = function () {
		
		console.log($scope.ReportEStampDutyTanggalAkhirOK);
		var dt = $scope.ReportEStampDuty_TanggalAkhir;
		var now = new Date();
		 if (dt > now) {
			$scope.errReportEStampDutyTanggalAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.ReportEStampDutyTanggalAkhirOK = false;			
		}
		else {
			$scope.ReportEStampDutyTanggalAkhirOK = true;
		}
		console.log('asdasd ' + $scope.ReportEStampDutyTanggalAkhirOK);
	}
	
	$scope.Report_Excel_Clicked=function() {

		ReportEStampDutyFactory.getExcelReportEStampDuty($filter('date')(new Date($scope.ReportEStampDuty_TanggalAwal), "yyyyMMdd"),
				$filter('date')(new Date($scope.ReportEStampDuty_TanggalAkhir), "yyyyMMdd"),$scope.ReportEStampDuty_SelectBranch)
		.then(
			function(res){

				var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
				var b64Data = res.data;

				var blob = b64toBlob(b64Data, contentType);
				var blobUrl = URL.createObjectURL(blob);

				saveAs(blob, 'Report_BeaMateraiElektronik' + '.xlsx');

				return res;
			},
			function(err){
				console.log("err=>",err);
			}
		);
	}

});