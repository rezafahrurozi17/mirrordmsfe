angular.module('app')
	.factory('ReportEStampDutyFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user();
	  var factory={};
	  var debugMode=true;


	factory.getExcelReportEStampDuty=function(dateStart,dateEnd,OutletId) {
        var url = '/api/fe/FinanceRptEStampDuty/Excel/?DateStart=' + dateStart + '&dateend=' + dateEnd + '&outletId=' + OutletId;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
	};	

	factory.getDataBranchComboBox = function () {
		console.log("factory.getDataBranchComboBox");
		var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
		var res = $http.get(url);
		return res;
	};

	return factory;
});