angular.module('app')
.controller('PenerimaanCashCITCIBController', function ($scope, $http, $filter, CurrentUser, PenerimaanCashCITCIBFactory, $timeout,bsNotify, ngDialog) {
	$scope.user = CurrentUser.user();
	$scope.MainPembayaranCashCITCIB_Show = true;
	$scope.PembayaranCashCITCIB_TemplateEmail_Disabled = false;
	$scope.UserData={};
	$scope.Edit = false;
	$scope.index = 0;
	$scope.PembayaranCashCITCIB_UserPenerima_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	 
		columnDefs: [
				{name:"Id", displayName:"Id", field:"UserListId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"UserId", displayName:"UserId", field:"EmployeeId", enableCellEdit: true, enableHiding: false, visible:false},
				{name:"Name", displayName:"User", field:"EmployeeName", enableCellEdit: false, enableHiding: false},
				{name:"Role", displayName:"Role", field:"RoleDesc", enableCellEdit: false, enableHiding: false},
				{name:"Email", displayName:"Alamat Email", field:"Email", enableCellEdit: false, enableHiding: false},
				{name:"Action", displayName:"Action",field:"Action", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.EditGrid_PembayaranCashCITCIB_UserPenerima(row)">Edit</a>&nbsp&nbsp&nbsp&nbsp<a ng-click="grid.appScope.KonfirmasiHapus(row)">hapus</a>  ' 
				}
			],
		onRegisterApi: function(gridApi) {
			$scope.PembayaranCashCITCIB_UserPenerima_gridAPI = gridApi;
		}
	};

	$scope.ModalPenerimaanCashCITCIB_TambahUser_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,		

		columnDefs: [
				{name:"UserId", displayName:"UserId", field:"EmployeeId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"EmployeeName", displayName:"Nama", field:"EmployeeName", enableCellEdit: false, enableHiding: false},
				{name:"RoleDesc", displayName:"Role", field:"RoleDesc", enableCellEdit: false, enableHiding: false},
				{name:"Email", displayName:"Email", field:"Email", enableCellEdit: false, enableHiding: false}
			],
			
		onRegisterApi: function(gridApi) {
			$scope.ModalPenerimaanCashCITCIB_TambahUser_gridAPI = gridApi;
		}
	};
	
	$scope.EditGrid_PembayaranCashCITCIB_UserPenerima = function(row)
	{
		$scope.Edit = true;
		$scope.index = $scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.indexOf(row.entity);
		PenerimaanCashCITCIBFactory.GetEmployeeList( $scope.user.OrgId).then(
			function(resList) {
				$scope.ModalPenerimaanCashCITCIB_TambahUser_UIGrid.data = resList.data.Result;
			},
			function(err) {
				console.log("err=>", err);
			} 
		);	
		angular.element('#ModalTambahUserList_CITCIB').modal('show');
	}

	$scope.KonfirmasiHapus = function (row) {
		var index = $scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.indexOf(row.entity);
		ngDialog.openConfirm({
			template:  '\
			             <div align="center" class="ngdialog-buttons">\
			             <p><h4>Konfirmasi</h4></p>\
			             <hr>\
			             <p>Anda yakin akan menghapus row ini?</p>\
			             <hr>\
			             <div class="ngdialog-buttons" align="center">\
			               <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="closeThisDialog(0)">Tidak</button>\
			               <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="DeleteGrid_PembayaranCashCITCIB_UserPenerima('+ index + ')">Ya</button>\
			             </div>\
			             </div>',
			plain: true,
			//controller: 'PenerimaanCashSPKController',
			scope: $scope
		});
	}

	$scope.DeleteGrid_PembayaranCashCITCIB_UserPenerima=function(index) {
		//var index = $scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.indexOf(row.entity);
		$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.splice(index, 1);
		$scope.ngDialog.close();
	};

	$scope.PembayaranCashCITCIB_Tambah_Clicked=function()
	{
		$scope.Edit = false;
		PenerimaanCashCITCIBFactory.GetEmployeeList( $scope.user.OrgId).then(
			function(resList) {
				$scope.ModalPenerimaanCashCITCIB_TambahUser_UIGrid.data = resList.data.Result;
			},
			function(err) {
				console.log("err=>", err);
			} 
		);	
		angular.element('#ModalTambahUserList_CITCIB').modal('show');
	}

	$scope.Batal_PenerimaanCashCITCIB_TambahUser = function()
	{
		angular.element('#ModalTambahUserList_CITCIB').modal('hide');	
	}

	$scope.Pilih_PenerimaanCashCITCIB_TambahUser=function() {
		if ($scope.ModalPenerimaanCashCITCIB_TambahUser_gridAPI.selection.getSelectedCount() > 0)
		{
			$scope.found = false;
			$scope.ModalPenerimaanCashCITCIB_TambahUser_gridAPI.selection.getSelectedRows().forEach(function(row) {
				$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.forEach(function(curRow) {
					if (curRow.EmployeeId == row.EmployeeId)
					{
						if ( ( ($scope.Edit == true) && 
							($scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.indexOf(curRow.entity) != $scope.index)) || 
							 ($scope.Edit == false) )
						{
							alert('data employee sudah ada terdaftar');
							$scope.found = true;	
						}
					}
				})
				if ($scope.found == false) {
					if ($scope.Edit == false) {
						$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data.push( {
							UserListId:0,
							EmployeeId: row.EmployeeId,
							RoleDesc:row.RoleDesc,
							EmployeeName:row.EmployeeName,
							Email:row.Email
						});
					}
					else {		
						$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data[$scope.index].EmployeeId = row.EmployeeId;
						$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data[$scope.index].RoleDesc = row.RoleDesc;
						$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data[$scope.index].EmployeeName = row.EmployeeName;
						$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data[$scope.index].Email = row.Email;
					}
				}
			});
			
			angular.element('#ModalTambahUserList_CITCIB').modal('hide');	
		}
		else 
		{
			alert('Belum ada Employee yang dipilih');
		}

	};

	$scope.RefreshData=function()
	{
		PenerimaanCashCITCIBFactory.getDataMaster($scope.user.OrgId).then(
			function(res) {
				//console.log('data ', JSON.stringify(res));
				if (res.data.Result.length > 0) {
					$scope.PembayaranCashCITCIB_NominalMaks = res.data.Result[0].MaxLimitCITCIB;
					$scope.PembayaranCashCITCIB_JudulEmail =  res.data.Result[0].EmailHeader;
					$scope.PembayaranCashCITCIB_TemplateEmail = res.data.Result[0].EmailBody;
				}
			}, 
			function(err) {
				console.log('err ', err);
			}
		);

		PenerimaanCashCITCIBFactory.getDataUserList($scope.user.OrgId).then(
			function(res) {
				if (res.data.Result.length > 0) {
					$scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data = res.data.Result;
				}
			}, 
			function(err) {
				console.log('err ', err);
			}
		);
	}

	$scope.PembayaranCashCITCIB_Simpan_Clicked = function()
	{
			data = [{MaxLimitCITCIB:$scope.PembayaranCashCITCIB_NominalMaks,
					EmailHeader:$scope.PembayaranCashCITCIB_JudulEmail,
					EmailBody:$scope.PembayaranCashCITCIB_TemplateEmail,
					OutletId:$scope.user.OrgId,
					UserList:  JSON.stringify($scope.PembayaranCashCITCIB_UserPenerima_UIGrid.data)          
				}]	

		PenerimaanCashCITCIBFactory.submitData(data).then(
            function(res){
                //$scope.loading=false;
				//alert('Data disimpan');
				bsNotify.show({
					title:"Berhasil",
					content:"Data berhasil disimpan.",
					type:"success"
				});
                
            },
            function(err){
                console.log("err=>",err);
            }
        );
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.RefreshData();
});