var app = angular.module('app');
app.controller('ttController', function ($scope, $http, $filter, CurrentUser, ParamKuitansiTTUSFactory, $timeout, bsNotify, ngDialog) {

	/*==========================================================================================*/
	/*								Kuitansi TTUS Main Begin									*/
	/*==========================================================================================*/
	$scope.ngDialog = ngDialog;

	ParamKuitansiTTUSFactory.getData('ParamKuitansi')
		.then(
			function (res) {
				console.log("res data", res.data.Result);
				$scope.ParamKuitansiKeteranganList_UIGrid_Paging();
				$scope.ParamKuitansiTTUS_SetFieldsKuitansi(res);
			}
		);

	ParamKuitansiTTUSFactory.getData('ParamTTUS')
		.then(
			function (res) {
				console.log("res data", res.data.Result);
				$scope.ParamTTUSKeteranganList_UIGrid_Paging();
				$scope.ParamKuitansiTTUS_SetFieldsTTUS(res);
			}
		);

	$scope.ParamKuitansiTTUS_SetFieldsKuitansi = function (res) {

		$scope.ParamKuitansi_BanyakCetak = res.data.Result[0].ParameterDetailValue;
		$scope.ParameterDetailIdJumlahCetakKuitansi = res.data.Result[0].ParameterDetailId;
		$scope.ParameterDetailIdisTampilTextKuitansi = res.data.Result[1].ParameterDetailId;
		$scope.ParameterDetailIdisBoldKuitansi = res.data.Result[2].ParameterDetailId;
		$scope.ParameterDetailIdisItalicKuitansi = res.data.Result[3].ParameterDetailId;
		$scope.ParameterDetailIdisUnderlineKuitansi = res.data.Result[4].ParameterDetailId;
		$scope.ParameterDetailIdTemplateTextKuitansi = res.data.Result[5].ParameterDetailId;
		$scope.ParamKuitansi_TemplateText = res.data.Result[5].ParameterDetailValue;

		if (res.data.Result[1].ParameterDetailValue == 1)
			$scope.ParamKuitansi_isTampilkanText = true;
		else
			$scope.ParamKuitansi_isTampilkanText = false;

		if (res.data.Result[2].ParameterDetailValue == 1)
			$scope.ParamKuitansi_isTextTebal = true;
		else
			$scope.ParamKuitansi_isTextTebal = false;

		if (res.data.Result[3].ParameterDetailValue == 1)
			$scope.ParamKuitansi_isTextMiring = true;
		else
			$scope.ParamKuitansi_isTextMiring = false;

		if (res.data.Result[4].ParameterDetailValue == 1)
			$scope.ParamKuitansi_isGarisBawah = true;
		else
			$scope.ParamKuitansi_isGarisBawah = false;
	}

	$scope.ParamKuitansiTTUS_SetFieldsTTUS = function (res) {
		$scope.ParamTTUS_BanyakCetak = res.data.Result[0].ParameterDetailValue;
		$scope.ParameterDetailIdJumlahCetakTTUS = res.data.Result[0].ParameterDetailId;
		$scope.ParameterDetailIdisTampilTextTTUS = res.data.Result[1].ParameterDetailId;
		$scope.ParameterDetailIdisBoldTTUS = res.data.Result[2].ParameterDetailId;
		$scope.ParameterDetailIdisItalicTTUS = res.data.Result[3].ParameterDetailId;
		$scope.ParameterDetailIdisUnderlineTTUS = res.data.Result[4].ParameterDetailId;
		$scope.ParameterDetailIdTemplateTextTTUS = res.data.Result[5].ParameterDetailId;
		$scope.ParamTTUS_TemplateText = res.data.Result[5].ParameterDetailValue;

		if (res.data.Result[1].ParameterDetailValue == 1)
			$scope.ParamTTUS_isTampilkanText = true;
		else
			$scope.ParamTTUS_isTampilkanText = false;

		if (res.data.Result[2].ParameterDetailValue == 1)
			$scope.ParamTTUS_isTextTebal = true;
		else
			$scope.ParamTTUS_isTextTebal = false;

		if (res.data.Result[3].ParameterDetailValue == 1)
			$scope.ParamTTUS_isTextMiring = true;
		else
			$scope.ParamTTUS_isTextMiring = false;

		if (res.data.Result[4].ParameterDetailValue == 1)
			$scope.ParamTTUS_isGarisBawah = true;
		else
			$scope.ParamTTUS_isGarisBawah = false;
	}

	$scope.KonfirmasiHapus = function (row, t) {
		var index = ((t == 1) ? $scope.ParamKuitansiKeteranganList_UIGrid.data.indexOf(row.entity) : $scope.ParamTTUSKeteranganList_UIGrid.data.indexOf(row.entity));
		var id = row.entity.ParameterDetailValue1;
		var txt = "No Urut " + id + " - " + row.entity.ParameterDetailValue2;
		var KuitansiOrTTUS = ((t == 1) ? 'ParamKuitansiTTUS_KuitansiDeleteKeterangan' : 'ParamKuitansiTTUS_TTUSDeleteKeterangan');
		ngDialog.openConfirm({
			template: '\
						<div class="ngdialog-buttons">\
						<p><h4>Konfirmasi</h4></p>\
						<hr>\
						<p>Anda yakin akan menghapus row <b>' + txt + '</b>?</p>\
						<hr>\
						<div class="ngdialog-buttons" align="center">\
						<button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="closeThisDialog(0)">Tidak</button>\
						<button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="' + KuitansiOrTTUS + '('+ index + ')">Ya</button>\
						</div>\
						</div>',
			plain: true,
			// controller: 'ParamKuitansiTTUSController',
			scope: $scope
		});
	}

	$scope.ParamKuitansiTTUS_KuitansiDeleteKeterangan = function (index) {
		// debugger;
		// var isDelete = false;
		// var CurrentKey = 0;
		// angular.forEach($scope.ParamKuitansiTTUS_KuitansiDetailData, function (value, key) {
		// 	debugger;
		// 	if (value.ParameterDetailValue1 == id) {
		// 		$scope.ParamKuitansiTTUS_KuitansiDetailData.splice(key, 1);
		// 		isDelete = true;
		// 		CurrentKey = key;
		// 	}
		// 	if (isDelete && CurrentKey != key) {
		// 		$scope.ParamKuitansiTTUS_KuitansiDetailData[key].ParameterDetailValue1 = parseInt($scope.ParamKuitansiTTUS_KuitansiDetailData[key].ParameterDetailValue1) - 1;
		// 	}

		// });
		$scope.ParamKuitansiTTUS_KuitansiDetailData.splice(index, 1);

		// $scope.ParamKuitansiKeteranganList_UIGrid.data = $scope.ParamKuitansiTTUS_KuitansiDetailData;
		$scope.ngDialog.close();
	}

	$scope.ParamKuitansiTTUS_TTUSDeleteKeterangan = function (index) {
		// debugger;
		// var isDelete = false;
		// var CurrentKey = 0;
		// angular.forEach($scope.ParamKuitansiTTUS_TTUSDetailData, function (value, key) {
		// 	debugger;
		// 	if (value.ParameterDetailValue1 == row.entity.ParameterDetailValue1) {
		// 		$scope.ParamKuitansiTTUS_TTUSDetailData.splice(key, 1);
		// 		isDelete = true;
		// 		CurrentKey = key;
		// 	}
		// 	if (isDelete && CurrentKey != key) {
		// 		$scope.ParamKuitansiTTUS_TTUSDetailData[key].ParameterDetailValue1 = parseInt($scope.ParamKuitansiTTUS_TTUSDetailData[key].ParameterDetailValue1) - 1;
		// 	}

		// });
		$scope.ParamKuitansiTTUS_TTUSDetailData.splice(index, 1);

		// $scope.ParamKuitansiKeteranganList_UIGrid.data = $scope.ParamKuitansiTTUS_TTUSDetailData;
		$scope.ngDialog.close();
	}

	$scope.ParamKuitansiKeterangan_Tambah_Clicked = function () {
		var lastNumber = 0;

		if ($scope.ParamKuitansiTTUS_KuitansiDetailData.length != 0){
			lastNumber = parseInt($scope.ParamKuitansiTTUS_KuitansiDetailData[$scope.ParamKuitansiTTUS_KuitansiDetailData.length - 1].ParameterDetailValue1, 10);
		}

		$scope.ParamKuitansiTTUS_KuitansiDetailData.push({ "ParameterValueId": 0, "ParameterDetailValue1": lastNumber + 1, "ParameterDetailValue2": "" });
		$scope.ParamKuitansiKeteranganList_UIGrid.data = $scope.ParamKuitansiTTUS_KuitansiDetailData;
	}

	$scope.ParamTTUSKeterangan_Simpan_Clicked = function () {
		var lastNumber = parseInt($scope.ParamKuitansiTTUS_TTUSDetailData[$scope.ParamKuitansiTTUS_TTUSDetailData.length - 1].ParameterDetailValue1, 10);

		if ($scope.ParamKuitansiTTUS_TTUSDetailData.length != 0){
			lastNumber = parseInt($scope.ParamKuitansiTTUS_TTUSDetailData[$scope.ParamKuitansiTTUS_TTUSDetailData.length - 1].ParameterDetailValue1, 10);
		}

		$scope.ParamKuitansiTTUS_TTUSDetailData.push({ "ParameterValueId": 0, "ParameterDetailValue1": lastNumber + 1, "ParameterDetailValue2": "" });
		$scope.ParamTTUSKeteranganList_UIGrid.data = $scope.ParamKuitansiTTUS_TTUSDetailData;
	}
	/*==========================================================================================*/
	/*									Kuitansi TTUS Main End									*/
	/*==========================================================================================*/

	/*==========================================================================================*/
	/*									Kuitansi UIGrid Begin									*/
	/*==========================================================================================*/

	$scope.uiGridPageSize = 10;

	$scope.ParamKuitansiKeteranganList_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "ParameterValueId", field: "ParameterValueId", visible: false },
			{ name: "No Urut", field: "ParameterDetailValue1", enableCellEdit: false },
			{
				name: "Keterangan", field: "ParameterDetailValue2",
				editableCellTemplate: '<input type="text" ng-maxlength="50" maxlength="50" ui-grid-editor ng-model="row.entity.ParameterDetailValue2">'
			},
			{ name: "Action", cellTemplate: '<a ng-click="grid.appScope.KonfirmasiHapus(row, 1)">Hapus</a>' },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiParamKuitansiKeteranganList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.ParamKuitansiKeteranganList_UIGrid_Paging();
			});
		}
	};

	$scope.ParamKuitansiKeteranganList_UIGrid_Paging = function () {
		ParamKuitansiTTUSFactory.getDataBJDDetail('ParamKuitansiInformation')
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res kuitansi detail =>", res.data.Result);
						$scope.ParamKuitansiTTUS_KuitansiDetailData = res.data.Result;
						$scope.ParamKuitansiKeteranganList_UIGrid.data = res.data.Result;
						$scope.ParamKuitansiKeteranganList_UIGrid.totalItems = res.data.Total;
						$scope.ParamKuitansiKeteranganList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.ParamKuitansiKeteranganList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
					}
				}

			);
	}

	/*==========================================================================================*/
	/*										Kuitansi UIGrid End									*/
	/*==========================================================================================*/

	/*==========================================================================================*/
	/*										TTUS UIGrid Begin									*/
	/*==========================================================================================*/

	$scope.uiGridPageSize = 10;

	$scope.ParamTTUSKeteranganList_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "ParameterValueId", field: "ParameterValueId", visible: false },
			{ name: "No Urut", field: "ParameterDetailValue1", enableCellEdit: false },
			{
				name: "Keterangan", field: "ParameterDetailValue2",
				editableCellTemplate: '<input type="text" ng-maxlength="50" maxlength="50" ui-grid-editor ng-model="row.entity.ParameterDetailValue2">'
			},
			{ name: "Action", cellTemplate: '<a ng-click="grid.appScope.KonfirmasiHapus(row, 2)">Hapus</a>' },
		],
		onRegisterApi: function (gridApi) {
			$scope.gridApiParamTTUSKeteranganList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.ParamTTUSKeteranganList_UIGrid_Paging();
			});
		}
	};

	$scope.ParamTTUSKeteranganList_UIGrid_Paging = function () {
		ParamKuitansiTTUSFactory.getDataBJDDetail('ParamTTUSInformation')
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res =>", res.data.Result);
						$scope.ParamKuitansiTTUS_TTUSDetailData = res.data.Result;
						$scope.ParamTTUSKeteranganList_UIGrid.data = res.data.Result;
						$scope.ParamTTUSKeteranganList_UIGrid.totalItems = res.data.Total;
						$scope.ParamTTUSKeteranganList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.ParamTTUSKeteranganList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
					}
				}

			);
	}

	/*==========================================================================================*/
	/*										TTUS UIGrid End										*/
	/*==========================================================================================*/

	$scope.ParamKuitansiTTUS_Simpan_Clicked = function () {
		var isTampilkanText = 0, isTextTebal = 0, isTextMiring = 0, isGarisBawah = 0;

		//Untuk Kuitansi
		if ($scope.ParamKuitansi_isTampilkanText)
			isTampilkanText = 1

		if ($scope.ParamKuitansi_isTextTebal)
			isTextTebal = 1

		if ($scope.ParamKuitansi_isTextMiring)
			isTextMiring = 1

		if ($scope.ParamKuitansi_isGarisBawah)
			isGarisBawah = 1

		if ($scope.ParameterDetailIdJumlahCetakKuitansi == undefined)
			$scope.ParameterDetailIdJumlahCetakKuitansi = -11;

		if ($scope.ParameterDetailIdisTampilTextKuitansi == undefined)
			$scope.ParameterDetailIdisTampilTextKuitansi = -12;

		if ($scope.ParameterDetailIdisBoldKuitansi == undefined)
			$scope.ParameterDetailIdisBoldKuitansi = -13;

		if ($scope.ParameterDetailIdisItalicKuitansi == undefined)
			$scope.ParameterDetailIdisItalicKuitansi = -14;

		if ($scope.ParameterDetailIdisUnderlineKuitansi == undefined)
			$scope.ParameterDetailIdisUnderlineKuitansi = -15;

		if ($scope.ParameterDetailIdTemplateTextKuitansi == undefined)
			$scope.ParameterDetailIdTemplateTextKuitansi = -16;

		var inputData = [
			{ "ParameterDetailId": $scope.ParameterDetailIdJumlahCetakKuitansi, "ParameterDetailValue": $scope.ParamKuitansi_BanyakCetak },
			{ "ParameterDetailId": $scope.ParameterDetailIdisTampilTextKuitansi, "ParameterDetailValue": isTampilkanText },
			{ "ParameterDetailId": $scope.ParameterDetailIdisBoldKuitansi, "ParameterDetailValue": isTextTebal },
			{ "ParameterDetailId": $scope.ParameterDetailIdisItalicKuitansi, "ParameterDetailValue": isTextMiring },
			{ "ParameterDetailId": $scope.ParameterDetailIdisUnderlineKuitansi, "ParameterDetailValue": isGarisBawah },
			{ "ParameterDetailId": $scope.ParameterDetailIdTemplateTextKuitansi, "ParameterDetailValue": $scope.ParamKuitansi_TemplateText },
		];


		ParamKuitansiTTUSFactory.create(inputData)
			.then(
				function (res) {
					ParamKuitansiTTUSFactory.createKuitansiDetail($scope.ParamKuitansiKeteranganList_UIGrid.data)
						.then(
							function (res) {
								ParamKuitansiTTUSFactory.getData('ParamKuitansi')
									.then(
										function (res) {
											$scope.ParamKuitansiTTUS_SetFieldsKuitansi(res);
											$scope.ParamKuitansiKeteranganList_UIGrid_Paging();
										}
									);
							}
						);
				},
				function (err) {
					console.log("Selisih Kelebihan Error : ", err);
				}
			);

		//Untuk TTUS
		isTampilkanText = 0;
		isTextTebal = 0;
		isTextMiring = 0;
		isGarisBawah = 0;

		if ($scope.ParamTTUS_isTampilkanText)
			isTampilkanText = 1

		if ($scope.ParamTTUS_isTextTebal)
			isTextTebal = 1

		if ($scope.ParamTTUS_isTextMiring)
			isTextMiring = 1

		if ($scope.ParamTTUS_isGarisBawah)
			isGarisBawah = 1

		if ($scope.ParameterDetailIdJumlahCetakTTUS == undefined)
			$scope.ParameterDetailIdJumlahCetakTTUS = -17;

		if ($scope.ParameterDetailIdisTampilTextTTUS == undefined)
			$scope.ParameterDetailIdisTampilTextTTUS = -18;

		if ($scope.ParameterDetailIdisBoldTTUS == undefined)
			$scope.ParameterDetailIdisBoldTTUS = -19;

		if ($scope.ParameterDetailIdisItalicTTUS == undefined)
			$scope.ParameterDetailIdisItalicTTUS = -20;

		if ($scope.ParameterDetailIdisUnderlineTTUS == undefined)
			$scope.ParameterDetailIdisUnderlineTTUS = -21;

		if ($scope.ParameterDetailIdTemplateTextTTUS == undefined)
			$scope.ParameterDetailIdTemplateTextTTUS = -22;

		var inputData = [
			{ "ParameterDetailId": $scope.ParameterDetailIdJumlahCetakTTUS, "ParameterDetailValue": $scope.ParamTTUS_BanyakCetak },
			{ "ParameterDetailId": $scope.ParameterDetailIdisTampilTextTTUS, "ParameterDetailValue": isTampilkanText },
			{ "ParameterDetailId": $scope.ParameterDetailIdisBoldTTUS, "ParameterDetailValue": isTextTebal },
			{ "ParameterDetailId": $scope.ParameterDetailIdisItalicTTUS, "ParameterDetailValue": isTextMiring },
			{ "ParameterDetailId": $scope.ParameterDetailIdisUnderlineTTUS, "ParameterDetailValue": isGarisBawah },
			{ "ParameterDetailId": $scope.ParameterDetailIdTemplateTextTTUS, "ParameterDetailValue": $scope.ParamTTUS_TemplateText },
		];


		ParamKuitansiTTUSFactory.create(inputData)
			.then(
				function (res) {
					ParamKuitansiTTUSFactory.createTTUSDetail($scope.ParamTTUSKeteranganList_UIGrid.data)
						.then(
							function (res) {
								//alert("Data berhasil disimpan");

								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);

								ParamKuitansiTTUSFactory.getData('ParamTTUS')
									.then(
										function (res) {
											$scope.ParamKuitansiTTUS_SetFieldsTTUS(res);
											$scope.ParamTTUSKeteranganList_UIGrid_Paging();
										}
									);
							}
						);
				},
				function (err) {
					console.log("Selisih Kelebihan Error : ", err);
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				}
			);
	}
});