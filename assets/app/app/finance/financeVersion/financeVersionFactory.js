angular.module('app')
  .factory('FinanceVersionFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
        getVersion: function() {
            var res=$http.get('/api/fe/FinanceVersion');
            console.log('res=>',res);
            return res;
        },
    }

  });