angular.module('app')
	.controller('NomorFakturPajakController', function ($scope, $http, $filter, CurrentUser, NomorFakturPajakFactory, $timeout, bsNotify, ngDialog) {
		$scope.user = CurrentUser.user();
		$scope.MainNomorFakturPajak_Show = true;
		$scope.NomorFakturPajak_TemplateEmail_Disabled = false;
		$scope.UserData = {};
		$scope.Edit = false;
		$scope.ActiveRow = {};
		$scope.index = 0;

		$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
		function checkRbyte(rb, b) {
			var p = rb & Math.pow(2, b);
			return ((p == Math.pow(2, b)));
		};

		$scope.allowApprove = checkRbyte($scope.ByteEnable, 2);
		console.log($scope.allowApprove);
		if ($scope.allowApprove == false) {
			$scope.allowSimpan = false;
		}
		else {
			if ($scope.user.OrgCode.substring(3, 9) == '000000')
				$scope.allowSimpan = true;
			else
				$scope.allowSimpan = false;
		}


		$scope.NomorFakturPajak_UserPenerima_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: true,
			columnDefs: [
				{ name: "Id", displayName: "Id", field: "UserListId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "Email", displayName: "Alamat Email", field: "Email", enableCellEdit: false, enableHiding: true },
				{
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false,
					cellTemplate: '<a ng-click="grid.appScope.EditGrid_NomorFakturPajak_UserPenerima(row)" ng-show="grid.appScope.allowSimpan">Edit</a>&nbsp&nbsp&nbsp&nbsp<a ng-click="grid.appScope.KonfirmasiHapus(row)" ng-show="grid.appScope.allowSimpan">hapus</a>  '
				}
			],
			onRegisterApi: function (gridApi) {
				$scope.NomorFakturPajak_UserPenerima_UIGrid_gridAPI = gridApi;
			}
		};

		$scope.EditGrid_NomorFakturPajak_UserPenerima = function (row) {
			$scope.Edit = true;
			$scope.ActiveRow = row;
			$scope.index = $scope.NomorFakturPajak_UserPenerima_UIGrid.data.indexOf(row.entity);
			//console.log('Data Row', JSON.stringify($scope.ActiveRow));
			$scope.NomorFakturPajak_TambahEmail = row.entity.Email;
			// NomorFakturPajakFactory.GetEmployeeList( $scope.user.OrgId).then(
			// 	function(res) {
			// 		$scope.ModalPenerimaanCashSPK_TambahUser_UIGrid.data = res.data.Result;
			// 	},
			// 	function(err) {
			// 		console.log("err=>", err);
			// 	} 
			// );	
			angular.element('#ModalTambahEmailList').modal('show');
		}

		$scope.KonfirmasiHapus = function (row) {
			//var id = row.entity.ParameterDetailValue1;
			var index = $scope.NomorFakturPajak_UserPenerima_UIGrid.data.indexOf(row.entity);
			ngDialog.openConfirm({
				template: '\
			             <div align="center" class="ngdialog-buttons">\
			             <p><h4>Konfirmasi</h4></p>\
			             <hr>\
			             <p>Anda yakin akan menghapus row ini?</p>\
			             <hr>\
			             <div class="ngdialog-buttons" align="center">\
			               <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="closeThisDialog(0)">Tidak</button>\
			               <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="DeleteGrid_NomorFakturPajak_UserPenerima('+ index + ')">Ya</button>\
			             </div>\
			             </div>',
				plain: true,
				//controller: 'NomorFakturPajakController',
				scope: $scope
			});
		}

		$scope.DeleteGrid_NomorFakturPajak_UserPenerima = function (index) {

			//var index = $scope.NomorFakturPajak_UserPenerima_UIGrid.data.indexOf(row.entity);
			$scope.NomorFakturPajak_UserPenerima_UIGrid.data.splice(index, 1);
			$scope.ngDialog.close();
		};

		$scope.NomorFakturPajak_Tambah_Clicked = function () {
			$scope.Edit = false;
			$scope.NomorFakturPajak_TambahEmail = "";
			if ($scope.NomorFakturPajak_UserPenerima_UIGrid.data.length > 4) {
				alert('Tidak bisa menambah email lagi !');
			}
			else {

				// NomorFakturPajakFactory.GetEmployeeList( $scope.user.OrgId).then(
				// 	function(res) {
				// 		$scope.ModalPenerimaanCashSPK_TambahUser_UIGrid.data = res.data.Result;
				// 	},
				// 	function(err) {
				// 		console.log("err=>", err);
				// 	} 
				// );	
				angular.element('#ModalTambahEmailList').modal('show');
			}
		}

		// $scope.ValidateEmail = function(inputText)  
		// {  
		// 	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		// 	if(inputText.value.match(mailformat))  
		// 	{  
		// 		//document.form1.text1.focus();  
		// 		return true;  
		// 	}  
		// 	else  
		// 	{  
		// 		//alert("You have entered an invalid email address!");  
		// 		return false;  
		// 	}  
		// } 

		$scope.validateEmail = function (emailField) {
			//var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

			if (reg.test(emailField) == false) {
				//alert('Invalid Email Address');
				return false;
			}

			return true;

		}

		$scope.Batal_NomorFakturPajak_TambahUser = function () {
			angular.element('#ModalTambahEmailList').modal('hide');
		}

		$scope.Pilih_NomorFakturPajak_TambahUser = function () {
			//alert('masuk pilh');
			if ($scope.NomorFakturPajak_TambahEmail != "") {
				if ($scope.validateEmail($scope.NomorFakturPajak_TambahEmail) == false) {
					alert('Alamat email tidak valid !');
				}
				else {
					$scope.found = false;
					$scope.NomorFakturPajak_UserPenerima_UIGrid.data.forEach(function (curRow) {
						if (curRow.Email == $scope.NomorFakturPajak_TambahEmail) {
							if ((($scope.Edit == true) &&
								($scope.NomorFakturPajak_UserPenerima_UIGrid.data.indexOf(curRow.entity) != $scope.index)) ||
								($scope.Edit == false)) {
								alert('data email sudah ada terdaftar');
								$scope.found = true;
							}
						}
					})
					if ($scope.found == false) {
						if ($scope.Edit == false) {
							$scope.NomorFakturPajak_UserPenerima_UIGrid.data.push({
								UserListId: 0,
								Email: $scope.NomorFakturPajak_TambahEmail
							});
						}
						else {
							$scope.NomorFakturPajak_UserPenerima_UIGrid.data[$scope.index].Email = $scope.NomorFakturPajak_TambahEmail;
						}
					}
				}
				angular.element('#ModalTambahEmailList').modal('hide');
			}
			else {
				alert('Alamat email masih kosong');
			}

		};

		$scope.RefreshData = function () {
			NomorFakturPajakFactory.getDataMaster($scope.user.OrgId).then(
				function (res) {
					//onsole.log('data ', JSON.stringify(res));
					if (res.data.Result.length > 0) {
						$scope.NomorFakturPajak_NominalMaks = res.data.Result[0].MinimumNumber;
						$scope.NomorFakturPajak_JudulEmail = res.data.Result[0].EmailHeader;
						$scope.NomorFakturPajak_TemplateEmail = res.data.Result[0].EmailBody;
					}
				},
				function (err) {
					console.log('err ', err);
				}
			);

			NomorFakturPajakFactory.getDataEmailList($scope.user.OrgId).then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.NomorFakturPajak_UserPenerima_UIGrid.data = res.data.Result;
					}
				},
				function (err) {
					console.log('err ', err);
				}
			);
		}

		$scope.NomorFakturPajak_Simpan_Clicked = function () {
			if($scope.allowSimpan){
				data = [{
					MinimumNumber: $scope.NomorFakturPajak_NominalMaks,
					EmailHeader: $scope.NomorFakturPajak_JudulEmail,
					EmailBody: $scope.NomorFakturPajak_TemplateEmail,
					OutletId: $scope.user.OrgId,
					EmailList: JSON.stringify($scope.NomorFakturPajak_UserPenerima_UIGrid.data)
				}]
	
				NomorFakturPajakFactory.submitData(data).then(
					function (res) {
						//$scope.loading=false;
						//alert('Data disimpan');
						bsNotify.show({
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: "success"
						});
	
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
			else {
				bsNotify.show({
					title: "Warning",
					content: "Anda tidak berhak.",
					type: "warning"
				});
			}
		}

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.RefreshData();
	});
// .directive('myDirective', function() {
// 	function link(scope, elem, attrs, ngModel) {
// 		ngModel.$parsers.push(function(viewValue) {
// 		  var reg = /^[^.]*$/;
// 		  //var reg = /^[0-9]*([\.][0-9]+)?$/g;
// 		  // if view values matches regexp, update model value
// 		  if (viewValue.match(reg)) {
// 			return viewValue;
// 		  }
// 		  // keep the model value as it is
// 		  var transformedValue = ngModel.$modelValue;
// 		  ngModel.$setViewValue(transformedValue);
// 		  ngModel.$render();
// 		  return transformedValue;
// 		});
// 	}

// 	return {
// 		restrict: 'A',
// 		require: 'ngModel',
// 		link: link
// 	};      
// });