/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('informasiPembayaranAsuransiFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;
		
		factory.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			var finalDate
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
            return finalDate;
		}
		
		factory.getSalesOrderBySPKId = function(start, limit, SPKId, IncomingType)
		{
			var find = "/";
			var regex = new RegExp(find, "g");
			SPKId = SPKId.replace(regex, "~"); 					
			var url = '/api/fe/AlokasiBFDPUnit/GetAlokasiSOBySPKId/start/' + start + '/limit/' + limit + '/spkid/' + SPKId + '|' + IncomingType;
			console.log ("execute GetAlokasiSOBySPKId");
			return $http.get(url);
		}
		
		factory.GetInformasiInsurance = function(start, limit, TranDate, TranDateTo, InsuranceId)
		{
			var url = '/api/fe/Insurance/GetInformasiInsurance?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + TranDateTo + '|' + InsuranceId;
			console.log ("execute GetInformasiInsurance");
			return $http.get(url);
		}
		
		factory.GetInformasiInsuranceFilter = function(start, limit, TranDate, TranDateTo, InsuranceId, searchText, searchType, flag)
		{
			if(flag == 1){
				searchText = this.changeFormatDate(searchText);
			}
			var url = '/api/fe/Insurance/GetInformasiInsurance?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + TranDateTo + '|' + InsuranceId + '|' + searchText + '|' + searchType ;
			console.log ("execute GetInformasiInsurance");
			return $http.get(url);
		}
		factory.getInsuranceComboBox = function () {
			console.log("factory.getInsuranceComboBox");
			  var url = '/api/fe/Insurance/SelectData?start=0&limit=0&FilterData=0';
			  var res=$http.get(url);  
			  return res;	
		}				 
		
		
		
		return factory;
	});