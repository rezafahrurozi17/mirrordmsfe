
var app = angular.module('app');
//20170425, eric, begin
//Nama controller disini harus case sensitive sama dengan route.js
app.controller('informasiPembayaranAsuransiController', function ($scope, $http, CurrentUser, $filter, informasiPembayaranAsuransiFactory, $timeout, uiGridConstants) {
	$scope.optionsLihatInformasi_DaftarInformasi_Search = [{name: "Tgl Pelunasan", value: "TglPelunasan"}, {name: "Nomor WO", value: "NomorWo"}, {name: "Nomor Polisi", value: "NomorPolisi"}, {name: "Nomor Billing", value: "NomorBilling"}, { name: "Nominal", value: "Nominal"}];

	console.log("informasiPembayaranAsuransi");
	$scope.Show_ShowInformasiInsurance = true;
	$scope.uiGridPageSize = 10;
	$scope.currentOutletId = 0;
	$scope.LihatTanggalInformasi = new Date();
	$scope.LihatTanggalInformasiTo = new Date();

	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};


	//----------------------------------
	// Start-Up
	//----------------------------------

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		$scope.getDataInsuranceCB();
		//   $scope.gridData=[];

	});
	
	$scope.getDataInsuranceCB = function () 
	{	
		informasiPembayaranAsuransiFactory.getInsuranceComboBox()
		.then
		(
			function (res) 
			{
				$scope.loading = false;
				console.log(res.data.Result);
				$scope.optionsLihatInformasi_NamaInsurance_Search = res.data.Result;
				console.log($scope.optionsLihatInformasi_NamaInsurance_Search);
			},
			function (err) 
			{
				console.log("err=>", err);
			}
		);
	};
	
	$scope.CariInformasiData = function () {
		// if (($scope.LihatTanggalInformasi != null)
		// && ($scope.LihatTanggalInformasiTo != null))
		// {
			// $scope.GetInformasiInsurance(1, $scope.uiGridPageSize);
			// $scope.ShowInformasiData = true;
		// }
		// else
		// {
			// alert("Tgl Awal/Akhir Blm Diisi");
		// }
		if (angular.isUndefined($scope.LihatTanggalInformasi)==true)
		{
			alert("Tgl Awal Blm Diisi");
		}
		if (angular.isUndefined($scope.LihatTanggalInformasiTo)==true)
		{
			alert("Tgl Akhir Blm Diisi");
		}
		else if (angular.isUndefined($scope.LihatInformasi_NamaInsurance_Search)==true)
		{
			alert("Nama Asuransi Blm Diisi");
		}
		else
		{
			$scope.GetInformasiInsurance(1, $scope.uiGridPageSize);
			$scope.ShowInformasiData = true;
		}		
	};
	
	$scope.LihatInformasi_NamaInsurance_Search_SelectedChange  = function () {
		console.log($scope.LihatInformasi_NamaInsurance_Search);
	}
	
    $scope.GetInformasiInsurance = function(Start, Limit) {
		//console.log($scope.LihatClearing_NamaBranch_Search);
		informasiPembayaranAsuransiFactory.GetInformasiInsurance(Start, Limit, $filter('date')(new Date($scope.LihatTanggalInformasi.toLocaleString()), 'yyyy-MM-dd'),
		$filter('date')(new Date($scope.LihatTanggalInformasiTo.toLocaleString()), 'yyyy-MM-dd'), $scope.LihatInformasi_NamaInsurance_Search)
        .then(
            function(res){
			$scope.LihatInformasi_DaftarInformasi_UIGrid.data=res.data.Result;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.totalItems = res.data.Total;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSize = Limit;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSizes = [Limit];				
                $scope.loading=false;				
            },
            function(err){
                console.log("err=>",err);
            }
        )
    };		
		
	$scope.LihatInformasi_DaftarInformasi_UIGrid = {
		
		// paginationPageSizes: 10,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// multiselect: false,
		// enableFiltering: true,
		// enableSorting: false,
		// enableRowSelection: true,
		// enableSelectAll: false,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		enableFiltering: true,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableColumnResizing: true,
		columnDefs: 
		[
			{ name: 'Tanggal Pelunasan', field: 'TglPelunasan', cellFilter: 'date:\"dd-MM-yyyy\"'  },
			{ name: 'Nama Pelanggan', field: 'NamaPelanggan' },			
			{ name: 'Nomor WO', field: 'NoWO' },		
			{ name: 'Nomor Polisi', field: 'NoPolisi' },					
			{ name: 'Nomor Billing', field: 'NoBilling' },
			{ name: 'Nominal', field: 'Nominal', cellFilter: 'currency:"":0'  }			
		],
		onRegisterApi: function (gridApi) {
			$scope.LihatInformasi_DaftarInformasi_gridAPI= gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			// 	console.log(row);
			// });
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.GetInformasiInsurance(pageNumber, pageSize);
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.GetInformasiInsurance(pageNumber,pageSize);

			// 	console.log("pageNumber ==>", pageNumber);
			// 	console.log("pageSize ==>", pageSize);
			// });
		},
	};
	// OLD filter
	// $scope.LihatInformasi_DaftarInformasi_Filter_Clicked = function() 
	// {
	// 	console.log("LihatInformasi_DaftarInformasi_Filter_Clicked");
	// 	if ( ($scope.LihatInformasi_DaftarInformasi_Search_Text != "") &&
	// 		($scope.LihatInformasi_DaftarInformasi_Search != "") )
	// 	{		
	// 		var nomor;		
	// 		var value = $scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 		$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.clearAllFilters();
	// 		console.log('filter text -->',$scope.LihatInformasi_DaftarInformasi_Search_Text );
	// 		nomor = 0;
	// 		console.log($scope.LihatInformasi_DaftarInformasi_Search);
	// 		console.log("nomor --> ", nomor);
	// 		 if (nomor == 0) 
	// 		 {
	// 			console.log("masuk 0");
	// 			//$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[1].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[2].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[3].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[4].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[5].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[6].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 			while (termObj) 
	// 				{
	// 				var oSearchArray = termObj.split(' ');
	// 				$scope.LihatInformasi_DaftarInformasi_UIGrid.data = $filter('filter')($scope.LihatInformasi_DaftarInformasi_UIGrid.data, oSearchArray[0], undefined);
	// 				oSearchArray.shift();
	// 				termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
	// 				}
	// 			}
	// 		 else
	// 		 {
	// 			console.log("masuk else");			
	// 			$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[nomor].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
	// 		}
	// 	}
	// 	else {
	// 		$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };		

	// New Filter
	$scope.LihatInformasi_DaftarInformasi_Filter_Clicked = function(){
		var tmpflg = 0;
		console.log("cek search type", $scope.LihatInformasi_DaftarInformasi_Search)
		if($scope.LihatInformasi_DaftarInformasi_Search == "Nominal"){
			$scope.LihatInformasi_DaftarInformasi_Search_Text = $scope.LihatInformasi_DaftarInformasi_Search_Text.replace(/\,/g,"").replace(/\./g,"");
		}
		if($scope.LihatInformasi_DaftarInformasi_Search == "TglPelunasan"){
			tmpflg = 1;
		}
		console.log("cek Nominal", $scope.LihatInformasi_DaftarInformasi_Search_Text)
		informasiPembayaranAsuransiFactory.GetInformasiInsuranceFilter(1, $scope.uiGridPageSize, $filter('date')(new Date($scope.LihatTanggalInformasi.toLocaleString()), 'yyyy-MM-dd'),
		$filter('date')(new Date($scope.LihatTanggalInformasiTo.toLocaleString()), 'yyyy-MM-dd'), $scope.LihatInformasi_NamaInsurance_Search, $scope.LihatInformasi_DaftarInformasi_Search_Text, $scope.LihatInformasi_DaftarInformasi_Search, tmpflg)
        .then(
            function(res){
			$scope.LihatInformasi_DaftarInformasi_UIGrid.data=res.data.Result;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.totalItems = res.data.Total;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];				
                $scope.loading=false;				
            },
            function(err){
                console.log("err=>",err);
            }
		);	
	}
	
	// $scope.LihatDetailClearing = function(row) 
	// {
		// $scope.LihatClearing_Top_NomorClearing = row.entity.NoClearing;
		// $scope.LihatClearing_TanggalClearing = row.entity.TanggalClearing;
		
		// clearingFactory.getLihatClearingListAR(1, $scope.uiGridPageSize, row.entity.ClearingId)
		// .then
		// (
			// function(res)
			// {
				// $scope.LihatClearing_DaftarARUntukClearing_UIGrid.data = res.data.Result;
				// $scope.LihatClearing_DaftarARUntukClearing_UIGrid.totalItems = res.data.Total;
				// $scope.LihatClearing_DaftarARUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.LihatClearing_DaftarARUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );		
		// clearingFactory.getLihatClearingListAP(1, $scope.uiGridPageSize, row.entity.ClearingId)
		// .then
		// (
			// function(res)
			// {
				// $scope.LihatClearing_DaftarAPUntukClearing_UIGrid.data = res.data.Result;
				// $scope.LihatClearing_DaftarAPUntukClearing_UIGrid.totalItems = res.data.Total;
				// $scope.LihatClearing_DaftarAPUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.LihatClearing_DaftarAPUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );			
		// clearingFactory.getLihatClearingListGL(1, $scope.uiGridPageSize, row.entity.ClearingId)
		// .then
		// (
			// function(res)
			// {
				// $scope.LihatClearing_DaftarGLUntukClearing_UIGrid.data = res.data.Result;
				// $scope.LihatClearing_DaftarGLUntukClearing_UIGrid.totalItems = res.data.Total;
				// $scope.LihatClearing_DaftarGLUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.LihatClearing_DaftarGLUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );	
		// clearingFactory.getLihatClearingListBiaya(1, $scope.uiGridPageSize, row.entity.ClearingId)
		// .then
		// (
			// function(res)
			// {
				// $scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.data = res.data.Result;
				// $scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.totalItems = res.data.Total;
				// $scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );			
	// };

	// $scope.TambahClearing_DaftarAR_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarAR = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.BillingId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: "BillingId",field:"BillingId",visible:false},
			// { name: 'No Billing', field: 'NoBilling' },
			// { name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"' },
			// { name: 'No Pelanggan', field: 'CustomerId' },
			// { name: 'Nama Pelanggan', field: 'CustomerName' },
			// { name: 'Nominal Ar', field: 'NominalBilling', cellFilter: 'currency:"":0' },
			// { name: 'Debet/Kredit', field: 'TipeDK' },
		// ]
	// };
	
	// $scope.TambahClearing_DaftarARUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// columnDefs: [
						// { name: "BillingId",field:"BillingId",visible:false},
						// { name: 'No Billing', field: 'NoBilling' },
						// { name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"' },
						// { name: 'No Pelanggan', field: 'CustomerId' },
						// { name: 'Nama Pelanggan', field: 'CustomerName' },
						// { name: 'Nominal Ar', field: 'NominalBilling', cellFilter: 'currency:"":0' },
						// { name: 'Debet/Kredit', field: 'TipeDK' },
						// { name: 'Keterangan', field: 'Keterangan', enableCellEdit: true, type: 'string'},
						// { name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarARUntukClearing(row)">Hapus</button>'}
					// ],
		// onRegisterApi: function(gridApi) {
			// $scope.GridApiTambahClearing_DaftarARUntukClearing = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// //$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			// }
			// );
			// gridApi.selection.on.rowSelectionChanged($scope, function(row){			
				// }
			// )
		// }
	// };	

	// $scope.deleteDaftarARUntukClearing = function(row) {
		// var index = $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.indexOf(row.entity);
		// $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.splice(index, 1);
		// $scope.CalculateBalance();
	// };
	
	// $scope.TambahDaftarAR_AddItem = function(){
		// console.log("TambahDaftarAR_AddItem");
		// $scope.gridApi_DaftarAR.selection.getSelectedRows().forEach(function(row) 
		// {
			// var found = 0;
			// $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.forEach(function(obj) {
				// console.log('isi grid -->', obj.NoBilling);
				// if ( row.NoBilling == obj.NoBilling)
					// found = 1;
			// });
			// if (found == 0) 
			// {	
				// $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.push
				// ( 
					// { 
						// BillingId: row.BillingId, 
						// NoBilling: row.NoBilling, 
						// TanggalBilling: row.TanggalBilling, 
						// CustomerId: row.CustomerId, 
						// CustomerName: row.CustomerName,
						// NominalBilling: row.NominalBilling,
						// TipeDK: row.TipeDK
					// }
				// )
			// }
		// }
	// )
	// $scope.CalculateBalance();
	// };	
	
	// $scope.TambahClearing_DaftarGLUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// columnDefs: [
						// { name: "No Gl Account",field:"NoGL"},
						// { name: 'Nama Gl Account', field: 'NamaGL' },
						// { name: 'No Referensi', field: 'NoReferensi' },
						// { name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
						// { name: 'Debet/Kredit', field: 'TipeDK' },
						// { name: 'Keterangan', field: 'Keterangan', enableCellEdit: true, type: 'string'},
						// { name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarGLUntukClearing(row)">Hapus</button>'}
					// ],
		// onRegisterApi: function(gridApi) {
			// $scope.GridApiTambahClearing_DaftarGLClearing = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// //$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			// }
			// );
			// gridApi.selection.on.rowSelectionChanged($scope, function(row){			
				// }
			// )
		// }
	// };		
	
	// $scope.deleteDaftarGLUntukClearing = function(row) {
		// var index = $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.indexOf(row.entity);
		// $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.splice(index, 1);
		// $scope.CalculateBalance();
	// };
	
	// $scope.TambahClearingGL_AddItem = function(){
		// console.log("TambahDaftarAR_AddItem");

		// $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.push
		// ( 
			// { 
				// NoGL: $scope.TambahClearing_NoGL_Search, 
				// NamaGL: $scope.TambahClearing_NamaGL, 
				// NoReferensi: $scope.TambahClearing_NoRef, 
				// Nominal: $scope.TambahClearing_Nominal,
				// TipeDK: $scope.TambahClearing_DK_Search,
				// Keterangan: $scope.TambahClearing_Ket
			// }
		// )
		// $scope.CalculateBalance();
	// };		
	
	// $scope.TambahClearing_DaftarAP_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarAP = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.SPKId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: 'No Instruksi Pembayaran', field: 'PaymentNo' },
			// { name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"' },
			// { name: 'No Branch/Vendor', field: 'VendorId' },
			// { name: 'Nama Branch/Vendor', field: 'VendorName' },
			// { name: 'Nominal Ap', field: 'Nominal', cellFilter: 'currency:"":0' },
			// { name: 'Debet/Kredit', field: 'TipeDK' },
			// { name: 'PaymentId', field: 'PaymentId' },
		// ]
	// };	

	// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// columnDefs: [
						// { name: 'No Instruksi Pembayaran', field: 'PaymentId' },
						// { name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"' },
						// { name: 'No Branch/Vendor', field: 'VendorId' },
						// { name: 'Nama Branch/Vendor', field: 'VendorName' },
						// { name: 'Nominal Ap', field: 'Nominal', cellFilter: 'currency:"":0' },
						// { name: 'Debet/Kredit', field: 'TipeDK' },
						// { name: 'Keterangan', field: 'Keterangan', enableCellEdit: true, type: 'string'},
						// { name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarAPUntukClearing(row)">Hapus</button>'}
					// ],
		// onRegisterApi: function(gridApi) {
			// $scope.GridApiTambahClearing_DaftarAPUntukClearing = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// //$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			// }
			// );
			// gridApi.selection.on.rowSelectionChanged($scope, function(row){			
				// }
			// )
		// }
	// };	

	// $scope.deleteDaftarAPUntukClearing = function(row) {
		// var index = $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.indexOf(row.entity);
		// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.splice(index, 1);
		// $scope.CalculateBalance();
	// };
	
	// $scope.TambahDaftarAP_AddItem = function(){
		// console.log("TambahDaftarAP_AddItem");
		// $scope.gridApi_DaftarAP.selection.getSelectedRows().forEach(function(row) 
		// {
			// var found = 0;
			// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.forEach(function(obj) {
				// console.log('isi grid -->', obj.PaymentId);
				// if ( row.PaymentId == obj.PaymentId)
					// found = 1;
			// });
			// if (found == 0) 
			// {	
				// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.push
				// ( 
					// { 
						// PaymentId: row.PaymentId, 
						// TanggalPayment: row.TanggalPayment, 
						// VendorId: row.VendorId, 
						// VendorName: row.VendorName,
						// Nominal: row.Nominal,
						// TipeDK: row.TipeDK
					// }
				// )
			// }
		// }
	// )
	// $scope.CalculateBalance();
	// };		

	// $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// columnDefs: [
						// { name: 'Tipe Biaya Lain Lain', field: 'TipeBiaya' },
						// { name: 'Debet/Kredit', field: 'TipeDK' },
						// { name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
						// { name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarBiayaUntukClearing(row)">Hapus</button>'}
					// ],
		// onRegisterApi: function(gridApi) {
			// $scope.GridApiTambahClearing_DaftarBiayaClearing = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// //$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			// }
			// );
			// gridApi.selection.on.rowSelectionChanged($scope, function(row){			
				// }
			// )
		// }
	// };		
	
	// $scope.deleteDaftarBiayaUntukClearing = function(row) {
		// var index = $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.indexOf(row.entity);
		// $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.splice(index, 1);
		// $scope.CalculateBalance();
	// };
	
	// $scope.TambahClearingBiaya_AddItem = function(){
		// console.log("TambahClearingBiaya_AddItem");

		// $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.push
		// ( 
			// { 
				// TipeBiaya: $scope.TambahClearing_TipeBiaya_Search, 
				// Nominal: $scope.TambahClearing_BiayaNominal,
				// TipeDK: $scope.TambahClearing_BiayaDK_Search
			// }
		// )
		// $scope.CalculateBalance();
	// };			
	
	// $scope.CalculateBalance = function() {
	// var TotalAR=0;
	// var TotalAP=0;
	// var TotalGLDebet=0;
	// var TotalGLKredit=0;
	// var TotalBiayaDebet=0;
	// var TotalBiayaKredit=0;
	
		// $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.forEach(function(obj) 
		// {
			// TotalAR+=obj.NominalBilling;
		// });	
	
		// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.forEach(function(obj) 
		// {
			// TotalAP+=obj.Nominal;
		// });	

		// $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.forEach(function(obj) 
		// {
			// if (obj.TipeDK == "Debet")
			// {
				// TotalGLDebet+=obj.Nominal;
			// }
			// else
			// {
				// TotalGLKredit+=obj.Nominal;
			// }
		// });	
		
		// $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.forEach(function(obj) 
		// {
			// console.log(obj.TipeDK );
			// console.log(obj.Nominal );
			// if (obj.TipeDK == "Debet")
			// {
				// TotalBiayaDebet+=obj.Nominal;
				// //console.log("TotalBiayaDebet");
				// //console.log(TotalBiayaDebet);
			// }
			// else
			// {
				// TotalBiayaKredit+=obj.Nominal;
				// console.log("TotalBiayaKredit");
				// console.log(TotalBiayaKredit);
			// }
		// });	
		
		// $scope.TambahClearing_Balance = parseInt(TotalAR) - parseInt(TotalAP) - parseInt(TotalBiayaDebet) - parseInt(TotalGLDebet);
		// $scope.TambahClearing_Balance+= parseInt(TotalGLKredit);
		// $scope.TambahClearing_Balance+= parseInt(TotalBiayaKredit);
	// };
	

	
// //UI Grid Lihat
	// $scope.LihatClearing_DaftarARUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarAR = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.SPKId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: "BillingId",field:"BillingId",visible:false},
			// { name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"' },
			// { name: 'No Billing', field: 'NoBilling' },
			// { name: 'No Pelanggan', field: 'CustomerId' },
			// { name: 'Nama Pelanggan', field: 'CustomerName' },
			// { name: 'Nominal Ar', field: 'NominalBilling', cellFilter: 'currency:"":0' },
			// { name: 'Debet/Kredit', field: 'TipeDK' },
			// { name: 'Keterangan', field: 'Description' }
		// ]
	// };
	
	// $scope.LihatClearing_DaftarAPUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarAP = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.SPKId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: 'No Instruksi Pembayaran', field: 'PaymentId' },
			// { name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"' },
			// { name: 'No Branch/Vendor', field: 'VendorId' },
			// { name: 'Nama Branch/Vendor', field: 'VendorName' },
			// { name: 'Nominal Ap', field: 'Nominal', cellFilter: 'currency:"":0' },
			// { name: 'Debet/Kredit', field: 'TipeDK' },
			// { name: 'Keterangan', field: 'Description' }
		// ]
	// };
	
	// $scope.LihatClearing_DaftarGLUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarGL = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.SPKId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: "No Gl Account",field:"NoGL"},
			// { name: 'Nama Gl Account', field: 'NamaGL' },
			// { name: 'No Referensi', field: 'NoReferensi' },
			// { name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
			// { name: 'Debet/Kredit', field: 'TipeDK' },
			// { name: 'Keterangan', field: 'Description' }
		// ]
	// };	
	
	// $scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination : true,
		// enableFiltering: true,
		// enableSelectAll: false,
		// multiSelect: false,
		// onRegisterApi: function (gridApi) {
			// $scope.gridApi_DaftarBiaya = gridApi;
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				// //$scope.currentSPKIdSource = row.entity.SPKId;
			// });
			// gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				// var msg = 'rows changed ' + rows.length;
				// console.log(msg);
			// });
		// },
		// enableRowSelection: true,
		// enableSelectAll: false,
		// columnDefs: 
		// [
			// { name: 'Tipe Biaya Lain Lain', field: 'TipeBiaya' },
			// { name: 'Nominal ', field: 'Nominal' },
			// { name: 'Debet/Kredit', field: 'TipeDK' }
		// ]
	// };		
	
	// $scope.ToTambahClearing = function () 
	// {
		// $scope.Show_ShowLihatClearing = false;
		// $scope.Show_ShowTambahClearing = true;
		// Enable_TambahClearing_DaftarAR();
		// $scope.TambahClearing_TanggalBilling = new Date();
		// $scope.TambahClearing_TanggalBillingTo = new Date();
		// $scope.TambahClearing_TanggalInstruksi = new Date();
		// $scope.TambahClearing_TanggalInstruksiTo = new Date();
		// $scope.TambahClearing_TanggalClearing = new Date();
	// };	
	
	// $scope.TambahClearing_Pengajuan = function () 
	// {
		// var data;
		
		// $scope.CalculateBalance();
		
		// $scope.ARSubmitData = [];
		// $scope.GLSubmitData = [];
		// $scope.APSubmitData = [];
		// $scope.EXSubmitData = [];
		
		// $scope.TambahClearing_ARListData();
		// $scope.ARSubmitData = $scope.CreateARSubmitData;
		// $scope.TambahClearing_GLListData();
		// $scope.GLSubmitData = $scope.CreateGLSubmitData;
		// $scope.TambahClearing_APListData();
		// $scope.APSubmitData = $scope.CreateAPSubmitData;
		// $scope.TambahClearing_EXListData();
		// $scope.EXSubmitData = $scope.CreateEXSubmitData;

		// data = 
		// [{
			// OutletId:$scope.currentOutletId,
			// ClearingDate:$scope.TambahClearing_TanggalClearing,
			// ClearingDetailARXML:"",
			// ClearingDetailGLXML:"",
			// ClearingDetailAPXML:"",
			// ClearingDetailEXXML:"",
			// ARList:$scope.ARSubmitData,
			// GLList:$scope.GLSubmitData,
			// APList:$scope.APSubmitData,
			// EXList:$scope.EXSubmitData
		// }]				
		
		// clearingFactory.submitClearing(data)
		// .then
		// (
			// function (res) 
			// {
				// console.log(res);
				// alert("Simpan Berhasil");
				// $scope.TambahClearing_Batal();
			// },
			// function (err) 
			// {
				// console.log("err=>", err);
				// if (err != null) 
				// {
					// alert(err.data.Message);
				// }	
			// }
		// )
	// }

	// $scope.TambahClearing_ARListData = function()
	// {
		// $scope.CreateARSubmitData = [];	
		// angular.forEach($scope.TambahClearing_DaftarARUntukClearing_UIGrid.data, function(value, key){
		// var obj = 
		// {
			// "BillingId" : value.BillingId, 
			// "Nominal" : value.NominalBilling,
			// "TipeDK" : value.TipeDK,
			// "Description" : value.Keterangan
		// };
		// console.log("Object of Id ==>", obj);
		// console.log("Object of Value ==>", value);
		// $scope.CreateARSubmitData.push(obj);
		// });		
	// }	
	
	// $scope.TambahClearing_GLListData = function()
	// {
		// $scope.CreateGLSubmitData = [];	
		// angular.forEach($scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data, function(value, key){
		// var obj = 
		// {
			// "NomorGL" : value.NoGL, 
			// "NomorRef" : value.NoReferensi, 
			// "Nominal" : value.Nominal,
			// "TipeDK" : value.TipeDK,
			// "Description" : value.Keterangan
		// };
		// console.log("Object of Id ==>", obj);
		// console.log("Object of Value ==>", value);
		// $scope.CreateGLSubmitData.push(obj);
		// });		
	// }	
	
	// $scope.TambahClearing_APListData = function()
	// {
		// $scope.CreateAPSubmitData = [];	
		// angular.forEach($scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data, function(value, key){
		// var obj = 
		// {
			// "PaymentId" : value.PaymentId, 
			// "Nominal" : value.Nominal,
			// "TipeDK" : value.TipeDK,
			// "Description" : value.Keterangan
		// };
		// console.log("Object of Id ==>", obj);
		// console.log("Object of Value ==>", value);
		// $scope.CreateAPSubmitData.push(obj);
		// });		
	// }	
	
	// $scope.TambahClearing_EXListData = function()
	// {
		// $scope.CreateEXSubmitData = [];	
		// angular.forEach($scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data, function(value, key){
		// var obj = 
		// {
			// "TipeBiaya" : value.TipeBiaya, 
			// "Nominal" : value.Nominal,
			// "TipeDK" : value.TipeDK
		// };
		// console.log("Object of Id ==>", obj);
		// console.log("Object of Value ==>", value);
		// $scope.CreateEXSubmitData.push(obj);
		// });		
	// }	
	
	// $scope.TambahClearing_NoRef_Blur = function ()
	// {
		// if ($scope.TambahClearing_NoGL_Search !== "")
		// {
			// clearingFactory.getDataGL($scope.TambahClearing_NoRef, $scope.TambahClearing_NoGL_Search)
				// .then(
					// function(res)
					// {
						// console.log("GL")
						// $scope.TambahClearing_NamaGL = res.data.Result[0].NamaGL;
						// $scope.TambahClearing_Nominal = res.data.Result[0].Nominal;				}
				// );		
		// }
	// }
	
	// $scope.TambahClearing_CariClearingDataAR = function () {
		// clearingFactory.GetListDaftarAR(1, $scope.uiGridPageSize, $filter('date')(new Date($scope.TambahClearing_TanggalBilling.toLocaleString()), 'yyyy-MM-dd'),
		// $filter('date')(new Date($scope.TambahClearing_TanggalBillingTo.toLocaleString()), 'yyyy-MM-dd'))
		// .then
		// (
			// function(res)
			// {
				// $scope.TambahClearing_DaftarAR_UIGrid.data = res.data.Result;
				// $scope.TambahClearing_DaftarAR_UIGrid.totalItems = res.data.Total;
				// $scope.TambahClearing_DaftarAR_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.TambahClearing_DaftarAR_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );		
	// };
	
	// $scope.TambahClearing_CariClearingDataAP = function () {
		// clearingFactory.GetListDaftarAP(1, $scope.uiGridPageSize, $filter('date')(new Date($scope.TambahClearing_TanggalInstruksi.toLocaleString()), 'yyyy-MM-dd'),
		// $filter('date')(new Date($scope.TambahClearing_TanggalInstruksiTo.toLocaleString()), 'yyyy-MM-dd'))
		// .then
		// (
			// function(res)
			// {
				// $scope.TambahClearing_DaftarAP_UIGrid.data = res.data.Result;
				// $scope.TambahClearing_DaftarAP_UIGrid.totalItems = res.data.Total;
				// $scope.TambahClearing_DaftarAP_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.TambahClearing_DaftarAP_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			// }
		// );		
	// };	
	
    // $scope.Pengajuan_LihatClearing_PengajuanReversalModal = function () {
		// console.log("Pengajuan Reversal");
		// var data;
		// data = 
		// [{		
			// "ApprovalTypeName":"Clearing Reversal",
			// "DocumentId":$scope.currentClearingId,
			// "ReversalReason":$scope.LihatClearing_PengajuanReversal_AlasanReversal
		// }]				

		// clearingFactory.submitDataApproval(data)
			// .then
			// (
				// function (res) 
				// {
					// console.log(res);
					// $scope.GetListClearing(1, uiGridPageSize);
				// },
				// function (err) 
				// {
					// console.log("err=>", err);
				// }
			// );	
	// };
	
	// $scope.TambahClearing_Batal = function() 
	// {
		// $scope.Show_ShowLihatClearing = true;
		// $scope.Show_ShowTambahClearing = false;
		// $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data = [];	
		// $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data = [];
		// $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data = [];
		// $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data = [];
		// $scope.TambahClearing_NoGL_Search = ""; 
		// $scope.TambahClearing_NamaGL = "";
		// $scope.TambahClearing_NoRef = ""; 
		// $scope.TambahClearing_Nominal = 0;
		// $scope.TambahClearing_DK_Search = "";
		// $scope.TambahClearing_Ket = "";
		// $scope.TambahClearing_TipeBiaya_Search= ""; 
		// $scope.TambahClearing_BiayaNominal= 0;
		// $scope.TambahClearing_BiayaDK_Search= "";
		
	// }

	// $scope.TambahClearing_TipeBiaya_SelectedChange = function() 
	// {
		// if ($scope.TambahClearing_TipeBiaya_Search == "Bea Materai")
		// {
				// $scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }];
		// }
		// else if ($scope.TambahClearing_TipeBiaya_Search == "Other Income")
		// {
				// $scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Kredit", value: "Kredit" }];
		// }
		// else if ($scope.TambahClearing_TipeBiaya_Search == "Other Expense")
		// {
				// $scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }];
		// }
		// else if ($scope.TambahClearing_TipeBiaya_Search == "PPN Masukan")
		// {
				// $scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		// }
		// else if ($scope.TambahClearing_TipeBiaya_Search == "PPV")
		// {
				// $scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		// }
	// }
		
// //Filter LihatClearing
	// $scope.LihatClearing_DaftarClearing_Filter_Clicked = function() 
	// {
		// console.log("LihatClearing_DaftarClearing_Filter_Clicked");
		// if ( ($scope.LihatClearing_DaftarClearing_Search_Text != "") &&
			// ($scope.LihatClearing_DaftarClearing_Search != "") )
		// {		
			// var nomor;		
			// var value = $scope.LihatClearing_DaftarClearing_Search_Text;
			// $scope.LihatClearing_DaftarClearing_gridAPI.grid.clearAllFilters();
			// console.log('filter text -->',$scope.LihatClearing_DaftarClearing_Search_Text );
			// if ($scope.LihatClearing_DaftarClearing_Search == "No Clearing") {
				// nomor = 1;
			// }				
			// console.log("nomor --> ", nomor);
			// $scope.LihatClearing_DaftarClearing_gridAPI.grid.columns[nomor].filters[0].term=$scope.LihatClearing_DaftarClearing_Search_Text;
		// }
		// else 
		// {
			// $scope.LihatClearing_DaftarClearing_gridAPI.grid.clearAllFilters();
		// }
	// };	

	// $scope.TambahClearing_DaftarAR_Clicked = function() 
	// {
		// console.log("TambahClearing_DaftarAR_Clicked");
		// if ( ($scope.TambahClearing_DaftarAR_Search_Text != "") &&
			// ($scope.TambahClearing_DaftarAR_SearchDropdown != "") )
		// {		
			// var nomor;		
			// var value = $scope.TambahClearing_DaftarAR_Search_Text;
			// $scope.gridApi_DaftarAR.grid.clearAllFilters();
			// console.log('filter text -->',$scope.TambahClearing_DaftarAR_Search_Text );
			// if ($scope.TambahClearing_DaftarAR_SearchDropdown == "Tanggal Billing") {
				// nomor = 3;
			// }				
			// if ($scope.TambahClearing_DaftarAR_SearchDropdown == "No Billing") {
				// nomor = 2;
			// }
			 // if ($scope.TambahClearing_DaftarAR_SearchDropdown == "No Pelanggan") {
				 // nomor = 4;
			 // }
			 // if ($scope.TambahClearing_DaftarAR_SearchDropdown == "Nama Pelanggan") {
				 // nomor = 5;
			 // }			 
			// console.log($scope.TambahClearing_DaftarAR_SearchDropdown);
			// console.log("nomor --> ", nomor);
			// $scope.gridApi_DaftarAR.grid.columns[nomor].filters[0].term=$scope.TambahClearing_DaftarAR_Search_Text;
		// }
		// else 
		// {
			// $scope.gridApi_DaftarAR.grid.clearAllFilters();
		// }
	// };		

	// $scope.TambahClearing_DaftarAP_Clicked = function() 
	// {
		// console.log("TambahClearing_DaftarAP_Clicked");
		// if ( ($scope.TambahClearing_DaftarAP_Search_Text != "") &&
			// ($scope.TambahClearing_DaftarAP_SearchDropdown != "") )
		// {		
			// var nomor;		
			// var value = $scope.TambahClearing_DaftarAP_Search_Text;
			// $scope.gridApi_DaftarAP.grid.clearAllFilters();
			// console.log('filter text -->',$scope.TambahClearing_DaftarAP_Search_Text );
			// if ($scope.TambahClearing_DaftarAP_SearchDropdown == "Tgl Instruksi Pembayaran") {
				// nomor = 2;
			// }				
			// if ($scope.TambahClearing_DaftarAP_SearchDropdown == "No Instruksi Pembayaran") {
				// nomor = 1;
			// }
			 // if ($scope.TambahClearing_DaftarAP_SearchDropdown == "No Vendor/Branch") {
				 // nomor = 3;
			 // }
			 // if ($scope.TambahClearing_DaftarAP_SearchDropdown == "Nama Vendor/Branch") {
				 // nomor = 4;
			 // }			 
			// console.log($scope.TambahClearing_DaftarAP_SearchDropdown);
			// console.log("nomor --> ", nomor);
			// $scope.gridApi_DaftarAP.grid.columns[nomor].filters[0].term=$scope.TambahClearing_DaftarAP_Search_Text;
		// }
		// else 
		// {
			// $scope.gridApi_DaftarAP.grid.clearAllFilters();
		// }
	// };	
	
// /*===============================Bulk Action Section, Begin===============================*/
	// $scope.LihatClearing_DaftarBulkAction_Changed = function(){
			// console.log("Bulk Action");						
			// if ($scope.LihatClearing_DaftarBulkAction_Search == "Setuju"){
				// angular.forEach($scope.LihatClearing_DaftarClearing_gridAPI.selection.getSelectedRows(), function(value, key){
					// $scope.ModalTolakClearing_ClearingId = value.ClearingId;
					// $scope.ModalTolakClearing_TipePengajuan = value.TipePengajuan;
					// console.log(value.StatusPengajuan);
					// if(value.StatusPengajuan == "Diajukan"){
						// $scope.CreateApprovalData("Disetujui" );
						// clearingFactory.updateApprovalData($scope.ApprovalData)
						// .then(
							// function(res){
								// alert("Berhasil Disetujui");
								// $scope.Batal_LihatClearing_TolakModal();
								// $scope.GetListClearing(1, uiGridPageSize);
							// },
							
							// function(err){
								// alert("Persetujuan gagal");
							// }
						// );
					// }
				// });
			// }
			// else if ($scope.LihatClearing_DaftarBulkAction_Search == "Tolak"){
				// console.log("grid api ap list ", $scope.LihatClearing_DaftarClearing_gridAPI);
				// angular.forEach($scope.LihatClearing_DaftarClearing_gridAPI.selection.getSelectedRows(), function(value, key){
						// console.log(value);
						// $scope.ModalTolakClearing_ClearingId = value.ClearingId;
						// $scope.ModalTolakClearing_TipePengajuan = value.TipePengajuan;						
						// $scope.ModalTolakClearing_TanggalClearing = value.TanggalClearing;
						// $scope.ModalTolakClearing_NoClearing = value.NoClearing;
						// $scope.ModalTolakClearing_TanggalPengajuan = value.TglPengajuan;
						// $scope.ModalTolakClearing_AlasanPengajuan = value.AlasanPengajuan;
						// if(value.StatusPengajuan == "Diajukan")
							// angular.element('#ModalTolakClearing').modal();
					// }
				// );
			// }
	// }
	
	// $scope.CreateApprovalData = function(ApprovalStatus){
		// if ($scope.ModalTolakClearing_TipePengajuan == "Reversal")
		// {
			// $scope.ModalTolakClearing_TipePengajuan = "Clearing Reversal";
		// }
		
		// $scope.ApprovalData = 
		// [
			// {
				// "ApprovalTypeName": $scope.ModalTolakClearing_TipePengajuan,
				// "RejectedReason": $scope.ModalTolakClearing_AlasanPenolakan,
				// "DocumentId": $scope.ModalTolakClearing_ClearingId,
				// "ApprovalStatusName": ApprovalStatus
			// }
		// ];
	// }
	
	// $scope.Batal_LihatClearing_TolakModal = function(){
		// angular.element('#ModalTolakClearing').modal('hide');
		// $scope.LihatClearing_DaftarBulkAction_Search = "";
		// $scope.ClearActionFields();
		// $scope.GetListClearing(1, $scope.uiGridPageSize);
	// }
	
	// $scope.Pengajuan_LihatClearing_TolakModal = function(){
		// $scope.CreateApprovalData("Ditolak");
		// clearingFactory.updateApprovalData($scope.ApprovalData)
		// .then(
			// function(res){
				// alert("Berhasil tolak");
				// $scope.Batal_LihatClearing_TolakModal();
				// $scope.GetListClearing(1, uiGridPageSize);
			// },
			
			// function(err){
				// alert("Penolakan gagal");
			// }
		// );
	// }
	
	// $scope.ClearActionFields = function(){
		// $scope.ModalTolakClearing_TanggalClearing = "";
		// $scope.ModalTolakClearing_NoClearing = "";
		// $scope.ModalTolakClearing_TanggalPengajuan = "";
		// $scope.ModalTolakClearing_AlasanPengajuan = "";
	// }
	// /*===============================Bulk Action Section, End===============================*/		
	
		
});
