var app = angular.module('app');
app.controller('AdvancedPaymentController', function ($scope, $http, $filter, uiGridConstants, CurrentUser, AdvancedPaymentFactory, $timeout, bsNotify) {

	$scope.dateNow = Date.now();
	$scope.uiGridPageSize = 10;
	$scope.TambahAdvancePayment_VendorId = 0;
	var user = CurrentUser.user();
	$scope.user = CurrentUser.user();
	$scope.Show_MainAdvancePayment = true;
	$scope.EditMode = false;
	$scope.Loading = false;
	//$scope.optionsTambahAdvancePaymentTipeAdvancePayment = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK-BPKB", value: "STNK-BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "General Transaction", value: "General Transaction" }];
	//$scope.optionsLihatAdvancePaymentTipeAdvancePayment = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK-BPKB", value: "STNK-BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "General Transaction", value: "General Transaction" }];
	$scope.optionsTambahAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Bank Transfer", value: "Bank Transfer" }];
	$scope.optionsLihatAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Bank Transfer", value: "Bank Transfer" }];
	$scope.optionsMainAdvancePayment_BulkAction = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	$scope.optionsTambahAdvancePayment_POSelectKolom = [{ name: "Nomor PO", value: 2 }, { name: "Nominal PO", value: 4 }];
	$scope.optionsTambahAdvancePayment_VendorSelectKolom = [{ name: "Code", value: "Code" }, { name: "Name", value: "Name" }, { name: "Address", value: "Address" }];
	$scope.optionsTambahIAdvance_TipeAdvanceiNotice = [{name:'Notice',value:'Notice'},{name:'Unnotice',value:'Unnotice'}];

	$scope.MainAdvancePayment_TanggalAwal = new Date();
	$scope.MainAdvancePayment_TanggalAwal.setHours(0, 0, 0, 0);

	$scope.MainAdvancePayment_TanggalAkhir = new Date();
	$scope.MainAdvancePayment_TanggalAkhir.setHours(0, 0, 0, 0);

	/*==============================================================================================================*/
	/*										Comma Separator setup, Begin											*/
	/*==============================================================================================================*/
	$scope.advPayment_NominalPembayaran = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran = $scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran;
			return;
		}
	};

	function addSeparatorsNF(nStr, inD, outD, sep) {
		nStr += '';
		var dpos = nStr.indexOf(inD);
		var nStrEnd = '';
		if (dpos != -1) {
			nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
			nStr = nStr.substring(0, dpos);
		}
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(nStr)) {
			nStr = nStr.replace(rgx, '$1' + sep + '$2');
		}
		return nStr + nStrEnd;
	}
	/*==============================================================================================================*/
	/*										Comma Separator setup, end												*/
	/*==============================================================================================================*/
	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	}

	var rByte = JSON.parse($scope.tab.item).ByteEnable;
	var allowBulk = checkRbyte(rByte, 4);
	var allowUnit = checkRbyte(rByte, 5);
	var ho = 0;
	if (allowBulk)
		if ($scope.user.RoleName != 'Admin Finance')
		$scope.MainAdvancePayment_BulkAction_isDisabled = false;
		else
		$scope.MainAdvancePayment_BulkAction_isDisabled = true;
	else
		$scope.MainAdvancePayment_BulkAction_isDisabled = true;

	if (user.OrgCode.substring(3, 9) == '000000') {
		$scope.Main_NamaBranch_EnableDisable = true;
		ho = user.OrgId;
	}

	else
		$scope.Main_NamaBranch_EnableDisable = false;

	$scope.AdvancedPayment_getDataBranch = function () {
		AdvancedPaymentFactory.getDataBranchComboBox(user.OrgId)
			.then
			(
			function (res) {
				$scope.optionsMainAdvancedPayment_SelectBranch = res.data.Result;
				$scope.optionsTambahAdvancedPayment_SelectBranch = res.data.Result;
				console.log("user.OutletId", user.OutletId);
				console.log("res data branch", res.data);

				// res.data.some(function(obj, i){
				// if(obj.value == user.OutletId)
				// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
				// });
				$scope.MainAdvancedPayment_SelectBranch = res.data.Result[0].BranchId;
				$scope.TambahAdvancedPayment_SelectBranch = res.data.Result[0].BranchId;
				console.log("$scope.MainAdvancedPayment_SelectBranch : ", $scope.MainAdvancedPayment_SelectBranch);

				res.data.Result.some(function (obj, i) {
					if (obj.BranchId == user.OutletId)
						$scope.MainAdvancedPayment_NamaBranch = obj.BranchName;
				});
			},
			function (err) {
				console.log("err=>", err);
			}
			);
		console.log('disini');
		console.log(user.OrgCode);
		console.log(user.OrgCode.substring(3, 9) == '000000');
		console.log('till here');
		if (user.OrgCode.substring(3, 9) == '000000') {
			$scope.Main_NamaBranch_EnableDisable = true;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
		}

	};

	$scope.AdvancedPayment_getDataBranch();

	AdvancedPaymentFactory.getDataAPType(allowUnit)
		.then(
			function (res) {
				var tipeAdvancePayment = [];
				console.log("res", res.data);
				angular.forEach(res.data.Result, function (value, key) {
					if (value.name.toLowerCase() != 'unit')
						{
							tipeAdvancePayment.push({ "name": value.name, "value": { "label": value.name, "valueType": value.value } });
						}
				});
				console.log("tipeAdvancePayment", tipeAdvancePayment);
				$scope.optionsTambahAdvancePaymentTipeAdvancePayment = tipeAdvancePayment;
				$scope.optionsLihatAdvancePaymentTipeAdvancePayment = tipeAdvancePayment;
			}
		);

	$scope.TambahAdvancePayment_VendorSelect = [{ isVendor: "OneTimeVendor", name: "One Time Vendor" }, { isVendor: "PilihVendor", name: "Pilih Vendor" }];

	$scope.TambahAdvancedPayment_Clicked = function () {
		$scope.EditMode = true;
		$scope.Loading = false;
		$scope.Show_LihatAdvancePayment = false;
		$scope.Show_MainAdvancePayment = false;
		$scope.Show_TambahAdvancePayment = true;
		$scope.Show_TambahAdvancePayment_Vendor_Detail = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_1 = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_2 = false;
		var tanggalAdvancePayment = new Date();
		//tanggalAdvancePayment.setMinutes(tanggalAdvancePayment.getMinutes() + 420);
		$scope.TambahAdvancedPayment_TanggalAdvancePayment = tanggalAdvancePayment;
	}

	$scope.TambahAdvancedPayment_Batal_Clicked = function () {
		$scope.TambahAdvancePayment_ClearFields();
		$scope.TambahAdvancePayment_TipeAdvancePayment = "";
		$scope.Show_MainAdvancePayment = true;
		$scope.Show_LihatAdvancePayment = false;
		$scope.Show_TambahAdvancePayment = false;
		$scope.Show_TambahAdvancePayment_Vendor_Detail = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_1 = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_2 = false;
	}

	/*
	$scope.TambahAdvancedPayment_Pengajuan_Clicked = function(){
		alert("Data sudah diajukan");
		$scope.TambahAdvancedPayment_Batal_Clicked();
	}*/


	$scope.TambahAdvancePayment_TipeAdvancePayment_Changed = function () {
		$scope.TambahIAdvance_TipeAdvanceiNotice = "";
		$scope.TambahAdvancedPayment_SelectedVendorTypeSelect = "NotVendor";
		$scope.TambahAdvancePayment_ClearFields();
		console.log("isinya : ", $scope.TambahAdvancePayment_TipeAdvancePayment);
		if (!angular.isUndefined($scope.TambahAdvancePayment_TipeAdvancePayment)) {
			if ($scope.TambahAdvancePayment_TipeAdvancePayment.label != 'General Transaction') {
				$scope.Show_TambahAdvancePayment_Vendor_Detail = true;

				$scope.Show_TambahAdvancePayment_DaftarPO = true;
				$scope.Show_TambahAdvancePayment_GeneralTransactionVendor = false;
				if($scope.TambahAdvancePayment_TipeAdvancePayment.label == 'STNK/BPKB'){
					$scope.Show_AdvanceTambahTipeNotice = true;
				}else{
					$scope.Show_AdvanceTambahTipeNotice = false;
				}
			} 
			else {
				$scope.Show_TambahAdvancePayment_Vendor_Detail = true;

				$scope.Show_TambahAdvancePayment_DaftarPO = false;
				$scope.Show_TambahAdvancePayment_GeneralTransactionVendor = true;
			}
		}
		else {
			$scope.Show_TambahAdvancePayment_Vendor_Detail = false;
		}
	}

	/*==============================Radio Buttion Handler, begin====================================*/
	$scope.TambahAdvancePayment_VendorTypeSelection = {
		value: ''
	};

	$scope.CleareField_GT = function(){
		$scope.TambahAdvancePayment_TujuanPembayaran_VendorId = 0;
		$scope.TambahAdvancePayment_NamaVendor = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NomorVendor = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor = "";
		$scope.TambahAdvancedPayment_TujuanPembayaran_Alamat = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_Telepon = "";
	}

	$scope.TambahAdvancePayment_VendorSelection_Changed = function (value) {
		$scope.CleareField_GT();
		if (value == "OneTimeVendor") {
			$scope.optionsTambahAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }];
			$scope.optionsLihatAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }];
			$scope.TambahAdvancePayment_NamaVendor_EnableDisable = true;
			$scope.TambahAdvancePayment_Cari_EnableDisable = true;
			$scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor_EnableDisable = true;
			$scope.TambahAdvancePayment_NPWPVendor_EnableDisable = true;
			$scope.TambahAdvancePayment_Alamat_EnableDisable = true;
			$scope.TambahAdvancePayment_KabupatenKota_EnableDisable = true;
			$scope.TambahAdvancePayment_Telepon_EnableDisable = true;
		}
		else {
			$scope.optionsTambahAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Bank Transfer", value: "Bank Transfer" }];
			$scope.optionsLihatAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Bank Transfer", value: "Bank Transfer" }];
			$scope.TambahAdvancePayment_NamaVendor_EnableDisable = false;
			$scope.TambahAdvancePayment_Cari_EnableDisable = false;
			$scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor_EnableDisable = false;
			$scope.TambahAdvancePayment_NPWPVendor_EnableDisable = false;
			$scope.TambahAdvancePayment_Alamat_EnableDisable = false;
			$scope.TambahAdvancePayment_KabupatenKota_EnableDisable = false;
			$scope.TambahAdvancePayment_Telepon_EnableDisable = false;
		}
	}

	/*==============================Radio Buttion Handler, end====================================*/
	/*===============================Filter Section, Begin===============================*/
	$scope.optionsMainAdvancePayment_SelectKolom = [{ name: "Nomor Advance Payment", value: "Nomor Advance Payment" }, { name: "Tipe Advance Payment", value: "Tipe Advance Payment" }, { name: "Nama Vendor", value: "Nama Vendor" }, { name: "Nominal Pembayaran", value: "Nominal Pembayaran" }];

	$scope.MainAdvancePayment_Filter_Clicked = function () {
		var nomor;
		$scope.GridApiAdvancePaymentList.grid.clearAllFilters();
		//$scope.GridApiAdvancePaymentList.grid.refresh();

		if ($scope.MainAdvancePayment_SelectKolom == 'Nomor Advance Payment')
			nomor = 15;
		else if ($scope.MainAdvancePayment_SelectKolom == 'Tipe Advance Payment')
			nomor = 16;
		else if ($scope.MainAdvancePayment_SelectKolom == 'Nama Vendor')
			nomor = 17;
		else if ($scope.MainAdvancePayment_SelectKolom == 'Nominal Pembayaran')
			nomor = 18;
		else
			nomor = 0;

		console.log("nomor dan filter", nomor, $scope.MainAdvancePayment_TextFilter);
		if (nomor != 0 && $scope.MainAdvancePayment_TextFilter != "") {
			console.log("masuk if");
			$scope.GridApiAdvancePaymentList.grid.columns[nomor].filters[0].term = $scope.MainAdvancePayment_TextFilter;
			//$scope.GridApiAdvancePaymentList.grid.refresh();
		}
	}

	$scope.TambahAdvancePayment_POFilter_Clicked = function () {
		// $scope.GridApiPOList.grid.clearAllFilters();
		// console.log($scope.TambahAdvancePayment_POSelectKolom);
		// if(!angular.isUndefined($scope.TambahAdvancePayment_POSelectKolom)){
		// $scope.GridApiPOList.grid.columns[$scope.TambahAdvancePayment_POSelectKolom].filters[0].term = $scope.TambahAdvancePayment_POTextFilter;
		// }
		// $scope.GridApiPOList.grid.queueGridRefresh();
		console.log("$scope.TambahAdvancePayment_POTextFilter : ", $scope.TambahAdvancePayment_POTextFilter);
		$scope.optionsTambahAdvancePayment_POSelectKolom.some(function (obj, i) {
			if (obj.value == $scope.TambahAdvancePayment_POSelectKolom) {
				if (!angular.isUndefined($scope.TambahAdvancePayment_POTextFilter) && $scope.TambahAdvancePayment_POTextFilter !== '')
					$scope.POList_UIGrid_PagingSTNK(1, $scope.TambahAdvancePayment_POTextFilter, obj.name);
				else
					$scope.POList_UIGrid_PagingSTNK(1, '', '');
			}
		});

	}
	$scope.TambahAdvancePayment_VendorFilter_Clicked = function () {
		if ($scope.TambahAdvancePayment_VendorSelectKolom == null || $scope.TambahAdvancePayment_VendorSelectKolom == '')
		$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();
		else
		$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();

		if ($scope.TambahAdvancePayment_VendorSelectKolom == 'Code')
			nomor = 2;
		else if ($scope.TambahAdvancePayment_VendorSelectKolom == 'Name')
			nomor = 3;
		else if ($scope.TambahAdvancePayment_VendorSelectKolom == 'Address')
			nomor = 4
		else
			nomor = 0

		if (!angular.isUndefined($scope.TambahAdvancePayment_VendorSelectKolom) && nomor != 0) {
			$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahAdvancePayment_VendorFilter;
		}
		$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.grid.queueGridRefresh();		

	}
	/*===============================Filter Section, End===============================*/

	/*===============================Bulk Action Section, Begin===============================*/
	$scope.MainAdvancePayment_BulkAction_Changed = function () {
		if ($scope.MainAdvancePayment_BulkAction == "Setuju") {
			angular.forEach($scope.GridApiAdvancePaymentList.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakAdvancePayment_APId = value.AdvancedPaymentId;
				$scope.ModalTolakAdvancePayment_TipePengajuan = value.APApprovalTypeName;
				if (value.APApprovalStatusName == "Diajukan") {
					$scope.CreateApprovalData("Disetujui");
					AdvancedPaymentFactory.updateApprovalData($scope.ApprovalData)
						.then(
							function (res) {
								//alert("Berhasil Disetujui");
								bsNotify.show({
									title: "Success Message",
									content: "Berhasil Disetujui",
									type: 'success'
								});
								$scope.Batal_LihatAdvancePayment_TolakModal();
							},

							function (err) {
								//alert("Persetujuan gagal");
								bsNotify.show({
									title: "Gagal",
									content: "Persetujuan gagal",
									type: 'danger'
								});
							}
						);
				}
			});
		}
		else if ($scope.MainAdvancePayment_BulkAction == "Tolak") {
			console.log("grid api ap list ", $scope.GridApiAdvancePaymentList);
			var tanggalPengajuan = "";
			var tanggalAP = "";
			angular.forEach($scope.GridApiAdvancePaymentList.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakAdvancePayment_APId = value.AdvancedPaymentId;
				tanggalAP = new Date(value.APDate);
				tanggalAP.setMinutes(tanggalAP.getMinutes() + 420);
				$scope.ModalTolakAdvancePayment_Tanggal = tanggalAP;
				$scope.ModalTolakAdvancePayment_NomorAP = value.AdvancedPaymentNumber;
				$scope.ModalTolakAdvancePayment_TipeAP = value.APPaymentTypeName;
				$scope.ModalTolakAdvancePayment_NamaVendor = value.VendorName;
				$scope.ModalTolakAdvancePayment_NPWPVendor = value.NPWPVendor;
				$scope.ModalTolakAdvancePayment_Nominal = value.Nominal;
				$scope.ModalTolakAdvancePayment_TipePengajuan = value.APApprovalTypeName;
				tanggalPengajuan = new Date(value.CreatedDate);
				tanggalPengajuan.setMinutes(tanggalPengajuan.getMinutes() + 420);
				$scope.ModalTolakAdvancePayment_TanggalPengajuan = tanggalPengajuan;
				$scope.ModalTolakAdvancePayment_AlasanPengajuan = value.ReversalReason;

				if (value.APApprovalStatusName == "Diajukan")
					angular.element('#ModalTolakAdvancePayment').modal('show');
			}
			);
		}
	}

	$scope.CreateApprovalData = function (ApprovalStatus) {
		$scope.ApprovalData = [
			{
				"ApprovalTypeName": $scope.ModalTolakAdvancePayment_TipePengajuan,
				"RejectedReason": $scope.ModalTolakAdvancePayment_AlasanPenolakan,
				"DocumentId": $scope.ModalTolakAdvancePayment_APId,
				"ApprovalStatusName": ApprovalStatus,
				"SourceTable":"AdvancePayment_TT"
			}
		];
	}

	$scope.Batal_LihatAdvancePayment_TolakModal = function () {
		angular.element('#ModalTolakAdvancePayment').modal('hide');
		$scope.MainAdvancePayment_BulkAction = "";
		$scope.ClearActionFields();
		$scope.AdvancePaymentList_UIGrid_Paging(1);
	}

	$scope.Pengajuan_LihatAdvancePayment_TolakModal = function () {
		$scope.CreateApprovalData("Ditolak");
		if ($scope.ModalTolakAdvancePayment_AlasanPenolakan == ''||$scope.ModalTolakAdvancePayment_AlasanPenolakan==null||angular.isUndefined($scope.ModalTolakAdvancePayment_AlasanPenolakan))
			{
				bsNotify.show({
					title: "Warning",
					content: 'Alasan Penolakan wajib diisi!',
					type: 'warning'
				});
			}
			else{
				AdvancedPaymentFactory.updateApprovalData($scope.ApprovalData)
				.then(
					function (res) {
						//alert("Berhasil tolak");
						bsNotify.show({
							title: "Berhasil",
							content: "Berhasil tolak",
							type: 'success'
						});
						$scope.Batal_LihatAdvancePayment_TolakModal();
					},
	
					function (err) {
						bsNotify.show({
							title: "Gagal",
							content: "Penolakan gagal",
							type: 'danger'
						});
						//alert("Penolakan gagal");
					}
				);
			}
		
	}

	$scope.ClearActionFields = function () {
		$scope.ModalTolakAdvancePayment_APId = "";
		$scope.ModalTolakAdvancePayment_Tanggal = "";
		$scope.ModalTolakAdvancePayment_NomorAP = "";
		$scope.ModalTolakAdvancePayment_TipeAP = "";
		$scope.ModalTolakAdvancePayment_NamaVendor = "";
		$scope.ModalTolakAdvancePayment_Nominal = "";
		$scope.ModalTolakAdvancePayment_TipePengajuan = "";
		$scope.ModalTolakAdvancePayment_TanggalPengajuan = "";
		$scope.ModalTolakAdvancePayment_AlasanPengajuan = "";
	}
	/*===============================Bulk Action Section, End===============================*/

	$scope.TambahAdvancedPayment_Cari_Clicked = function () {
		
		if ($scope.TambahAdvancePayment_NamaVendor == "" && $scope.TambahAdvancePayment_VendorTypeSelection.value != "OneTimeVendor") {
			//alert("Nama Vendor harus diisi");
			bsNotify.show({
				title: "Warning",
				content: "Nama Vendor harus diisi",
				type: 'warning'
			});
			return;
		}
		if($scope.TambahAdvancePayment_TipeAdvancePayment.label == 'STNK/BPKB')
		{
			if ($scope.TambahIAdvance_TipeAdvanceiNotice == "" || $scope.TambahIAdvance_TipeAdvanceiNotice == null || $scope.TambahIAdvance_TipeAdvanceiNotice == undefined) {
				//alert("Nama Vendor harus diisi");
				bsNotify.show({
					title: "Warning",
					content: "Tipe Notice atau Unnotice wajib dipilih.",
					type: 'warning'
				});
				return;
			}
		}
		$scope.POList_UIGrid.paginationCurrentPage = 1;
			$scope.POList_UIGrid_PagingSTNK(1);
		// AdvancedPaymentFactory.getVendorByName($scope.TambahAdvancePayment_TujuanPembayaran_IdPelangganVendor, $scope.TambahAdvancePayment_TipeAdvancePayment.valueType, $scope.TambahAdvancedPayment_SelectBranch)
		// 	.then(
			AdvancedPaymentFactory.getVendorInfo($scope.TambahAdvancePayment_TipeAdvancePayment.label,
				$scope.TambahAdvancePayment_NamaVendor , $scope.TambahAdvancePayment_TujuanPembayaran_IdPelangganVendor).then(
				function (res) {
					if (angular.isUndefined(res.data.Result) || res.data.Result.length == 0) {
						//alert("Data vendor tidak ditemukan");
						bsNotify.show({
							title: "Warning",
							content: "Data vendor tidak ditemukan",
							type: 'warning'
						});
					}
					else {
						console.log("Data Vendor ==> ", res);
						angular.forEach(res.data.Result, function(value, key){
							if(value.Name == $scope.TambahAdvancePayment_NamaVendor){
								$scope.TambahAdvancePayment_TujuanPembayaran_VendorId = value.Id;
								$scope.TambahAdvancePayment_TujuanPembayaran_NomorVendor = value.VendorCode;
								$scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor = value.Name;
								$scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor = value.NPWP;
								$scope.TambahAdvancePayment_TujuanPembayaran_Telepon = value.Phone;
								$scope.TambahAdvancedPayment_TujuanPembayaran_Alamat = value.Address;
								$scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota = value.City;
							}
						});
					}
				}
			)
	}

	$scope.TambahAdvancePayment_MetodePembayaran_Changed = function () {

		if (!angular.isUndefined($scope.TambahAdvancePayment_MetodePembayaran)) {
			$scope.Show_TambahAdvancePayment_Pembayaran_1 = true;
			$scope.Show_TambahAdvancePayment_Pembayaran_2 = true;
			if ($scope.TambahAdvancePayment_MetodePembayaran == "Cash")
				$scope.Show_TambahAdvancePayment_RincianPembayaran_NamaPenerima = true;
			else
				$scope.Show_TambahAdvancePayment_RincianPembayaran_NamaPenerima = false;
		}
		else {
			$scope.Show_TambahAdvancePayment_Pembayaran_1 = false;
			$scope.Show_TambahAdvancePayment_Pembayaran_2 = false;
		}

	}

	$scope.LihatAdvancePayment_MetodePembayaran_Changed = function () {

		if (!angular.isUndefined($scope.LihatAdvancePayment_MetodePembayaran)) {
			$scope.Show_LihatAdvancePayment_Pembayaran_1 = true;
			$scope.Show_LihatAdvancePayment_Pembayaran_2 = true;
			if ($scope.LihatAdvancePayment_MetodePembayaran == "Cash")
				$scope.Show_LihatAdvancePayment_RincianPembayaran_NamaPenerima = true;
			else
				$scope.Show_LihatAdvancePayment_RincianPembayaran_NamaPenerima = false;
		}
		else {
			$scope.Show_LihatAdvancePayment_Pembayaran_1 = false;
			$scope.Show_LihatAdvancePayment_Pembayaran_2 = false;
		}

	}

	$scope.TambahAdvancePayment_CheckFields = function () {
		var passed = false;

		if (angular.isUndefined($scope.TambahAdvancePayment_TipeAdvancePayment) || $scope.TambahAdvancePayment_TipeAdvancePayment.label == "") {
			//alert("Tipe Advance Payment harus dipilih !");
			bsNotify.show({
				title: "Warning",
				content: "Tipe Advance Payment harus dipilih !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}


		if ($scope.TambahAdvancePayment_TipeAdvancePayment.label == "General Transaction") {
			if (angular.isUndefined($scope.TambahAdvancePayment_VendorTypeSelection.value)) {
				//alert("Pilih One Time Vendor atau Pilih Vendor")
				bsNotify.show({
					title: "Warning",
					content: "Pilih One Time Vendor atau Pilih Vendor.",
					type: 'warning'
				});
				return (passed);
			}

			if ($scope.TambahAdvancePayment_VendorTypeSelection.value == "OneTimeVendor") {
				if (angular.isUndefined($scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor) || $scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor == "") {
					//alert("Nama vendor harus diisi !");
					bsNotify.show({
						title: "Warning",
						content: "Nama vendor harus diisi !",
						type: 'warning'
					});
					return (passed);
				}
				if (angular.isUndefined($scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor) || $scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor == "") {
					//alert("NPWP vendor harus diisi !");
					bsNotify.show({
						title: "Warning",
						content: "NPWP vendor harus diisi !",
						type: 'warning'
					});
					return (passed);
				}
				if (angular.isUndefined($scope.TambahAdvancedPayment_TujuanPembayaran_Alamat) || $scope.TambahAdvancedPayment_TujuanPembayaran_Alamat == "") {
					//alert("Alamat harus diisi !");
					bsNotify.show({
						title: "Warning",
						content: "Alamat harus diisi !",
						type: 'warning'
					});
					return (passed);
				}
				if (angular.isUndefined($scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota) || $scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota == "") {
					//alert("Kabupaten/Kota harus diisi !");
					bsNotify.show({
						title: "Warning",
						content: "Kabupaten/Kota harus diisi !",
						type: 'warning'
					});
					return (passed);
				}
				if (angular.isUndefined($scope.TambahAdvancePayment_TujuanPembayaran_Telepon) || $scope.TambahAdvancePayment_TujuanPembayaran_Telepon == "") {
					//alert("Telepon harus diisi !");
					bsNotify.show({
						title: "Warning",
						content: "Telepon harus diisi !",
						type: 'warning'
					});
					return (passed);
				}
			}
			else {
				if ($scope.TambahAdvancePayment_NamaVendor == "") {
					//alert("Nama Vendor wajib diisi");

					bsNotify.show({
						title: "Warning",
						content: "Nama Vendor wajib diisi !",
						type: 'warning'
					});
					return (passed);
				}
			}
		}
		if (angular.isUndefined($scope.TambahAdvancePayment_MetodePembayaran) || $scope.TambahAdvancePayment_MetodePembayaran == "") {
			//alert("Metode pembayaran harus dipilih !");
			bsNotify.show({
				title: "Warning",
				content: "Metode pembayaran harus dipilih !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}
		if (angular.isUndefined($scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran) || $scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran <= 0) {
			//alert("Nominal pembayaran harus diisi lebih besar dari 0 !");
			bsNotify.show({
				title: "Warning",
				content: "Nominal pembayaran harus diisi lebih besar dari 0 !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}
		if (angular.isUndefined($scope.TambahAdvancedPayment_RincianPembayaran_Keterangan) || $scope.TambahAdvancedPayment_RincianPembayaran_Keterangan == "") {
			//alert("Keterangan harus diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Keterangan harus diisi !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}

		if (angular.isUndefined($scope.TambahAdvancedPayment_TanggalJatuhTempo)) {
			//alert("Tanggal jatuh tempo harus diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Tanggal jatuh tempo harus diisi !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}
		if (angular.isUndefined($scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima) || $scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima == "") {

			if ($scope.TambahAdvancePayment_MetodePembayaran == "Cash") {
				console.log("masuk metode pembayarna cash, nama penerima kosong");
				//alert("Nama penerima harus diisi !");
				bsNotify.show({
					title: "Warning",
					content: "Nama penerima harus diisi !",
					type: 'warning'
				});
				$scope.Loading = false;
				return (passed);
			}
		}
		if (angular.isUndefined($scope.TambahAdvancePayment_Footer_NamaPimpinan) || $scope.TambahAdvancePayment_Footer_NamaPimpinan == "") {
			//alert("Nama Pimpinan harus diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Nama Pimpinan harus diisi !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}
		if (angular.isUndefined($scope.TambahAdvancePayment_Footer_NamaFinance) || $scope.TambahAdvancePayment_Footer_NamaFinance == "") {
			//alert("Nama Finance harus diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Nama Finance harus diisi !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}
		if ((angular.isUndefined($scope.SelectedPOId) || $scope.SelectedPOId.length == 0) && $scope.TambahAdvancePayment_TipeAdvancePayment.label != "General Transaction") {
			//alert("Nomor PO harus dipilih !");
			bsNotify.show({
				title: "Warning",
				content: "Nomor PO harus dipilih !",
				type: 'warning'
			});
			$scope.Loading = false;
			return (passed);
		}

		passed = true;

		return (passed);
	}

	$scope.TambahAdvancedPayment_Pengajuan_Clicked = function () {
		if (!$scope.TambahAdvancePayment_CheckFields())
			return;

		$scope.CreateSaveData();
		AdvancedPaymentFactory.saveData($scope.DatatoSave)
		.then(
			function (res) {
				//alert("Data berhasil diajukan");
				bsNotify.show({
					title: "Berhasil",
					content: "Data berhasil diajukan.",
					type: 'success'
				});
				$scope.TambahAdvancedPayment_Batal_Clicked();
				$scope.TambahAdvancePayment_ClearFields();
			}
		);
		console.log("Data to Save : ", $scope.DatatoSave);
	}

	$scope.TambahAdvancedPayment_PengajuanDanCetak_Clicked = function () {
		$scope.Loading = true;
		if (!$scope.TambahAdvancePayment_CheckFields())
			return;

		$scope.CreateSaveData();
		if($scope.TambahAdvancePayment_TipeAdvancePayment.label == "STNK/BPKB"){
			AdvancedPaymentFactory.saveDataSTNK($scope.DatatoSave)
			.then(
				function (res) {
					//alert("Data berhasil diajukan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil diajukan.",
						type: 'success'
					});
					console.log("hasil res data save : ", res);
					$scope.Report_Cetak_Clicked(res.data.Result[0].AdvancedPaymentId, res.data.Result[0].AdvancedPaymentType, res.data.Result[0].APPaymentMethod);
					$scope.TambahAdvancedPayment_Batal_Clicked();
					$scope.TambahAdvancePayment_ClearFields();
					$scope.MainAdvancedPayment_Cari_Clicked();
				}
			);
			console.log("Data to Save STNK/BPKB ", $scope.DatatoSave);
		}else if($scope.TambahAdvancePayment_TipeAdvancePayment.label == "Order Pekerjaan Luar"){
			AdvancedPaymentFactory.saveDataOPL($scope.DatatoSave)
			.then(
				function (res) {
					//alert("Data berhasil diajukan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil diajukan.",
						type: 'success'
					});
					console.log("hasil res data save : ", res);
					$scope.Report_Cetak_Clicked(res.data.Result[0].AdvancedPaymentId, res.data.Result[0].AdvancedPaymentType, res.data.Result[0].APPaymentMethod);
					$scope.TambahAdvancedPayment_Batal_Clicked();
					$scope.TambahAdvancePayment_ClearFields();
					$scope.MainAdvancedPayment_Cari_Clicked();
				}
			);
			console.log("Data to Save OPL ", $scope.DatatoSave);
		}else{
			AdvancedPaymentFactory.saveData($scope.DatatoSave)
			.then(
				function (res) {
					//alert("Data berhasil diajukan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil diajukan.",
						type: 'success'
					});
					console.log("hasil res data save : ", res);
					$scope.Report_Cetak_Clicked(res.data.Result[0].AdvancedPaymentId, res.data.Result[0].AdvancedPaymentType, res.data.Result[0].APPaymentMethod);
					$scope.TambahAdvancedPayment_Batal_Clicked();
					$scope.TambahAdvancePayment_ClearFields();
					$scope.MainAdvancedPayment_Cari_Clicked();
				}
			);
			console.log("Data to Save : ", $scope.DatatoSave);
		}
	}

	$scope.LihatAdvancedPayment_PengajuanReversal_Clicked = function () {
		angular.element('#ModalReverseAdvancePayment').modal('show');
		$scope.TambahAdvancedPayment_Batal_Clicked();
	}

	$scope.LihatAdvancedPayment_Batal_Clicked = function () {
		$scope.TambahAdvancedPayment_Batal_Clicked();
	}

	$scope.Batal_LihatAdvancePayment_PengajuanReversalModal = function () {
		angular.element('#ModalReverseAdvancePayment').modal('hide');
	}

	$scope.Pengajuan_LihatAdvancePayment_PengajuanReversalModal = function () {
		AdvancedPaymentFactory.deleteDataAP($scope.LihatAdvancePayment_APId, $scope.LihatAdvancePayment_PengajuanReversal_AlasanReversal, $scope.LihatAdvancedPayment_MainNamaBranchId)
			.then(
				function (res) {
					//alert("Data berhasil diajukan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil diajukan.",
						type: 'success'
					});
					$scope.AdvancePaymentList_UIGrid_Paging(1);
					$scope.Batal_LihatAdvancePayment_PengajuanReversalModal();
				},
				function (err) {
					//alert("Data gagal diajukan");
					bsNotify.show({
						title: "Gagal",
						content: 'Reversal gagal diajukan : ' + err.data.Message,
						type: 'danger'
					});
					angular.element('#ModalReverseAdvancePayment').modal('hide');
				}
			);

	}

	$scope.CreateSaveData = function () {
		var isOneTimeVendor = 0;
		if ($scope.TambahAdvancePayment_VendorTypeSelection.value == "OneTimeVendor")
			isOneTimeVendor = 1;

		if (angular.isUndefined($scope.TambahAdvancePayment_TujuanPembayaran_VendorId))
			$scope.TambahAdvancePayment_TujuanPembayaran_VendorId = 0;

			if($scope.TambahAdvancePayment_TipeAdvancePayment.label == "STNK/BPKB"){
				$scope.DatatoSave = [
					{
						"AdvancePaymentDate": null,
						"AdvancePaymentType": $scope.TambahAdvancePayment_TipeAdvancePayment.label,
						"VendorId": $scope.TambahAdvancePayment_TujuanPembayaran_VendorId,
						"PaymentType": $scope.TambahAdvancePayment_MetodePembayaran,
						"PaidAmount": $scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran,
						"PaidDesc": $scope.TambahAdvancedPayment_RincianPembayaran_Keterangan,
						"MaturityDate": $filter('date')(new Date($scope.TambahAdvancedPayment_TanggalJatuhTempo.toLocaleString()), "yyyy-MM-dd"),
						"ReceiverName": $scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima,
						"LeaderName": $scope.TambahAdvancePayment_Footer_NamaPimpinan,
						"FinanceName": $scope.TambahAdvancePayment_Footer_NamaFinance,
						"POId": $scope.SelectedPOId,
						"VendorName": $scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor,
						"VendorNPWP": $scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor,
						"VendorAddress": $scope.TambahAdvancedPayment_TujuanPembayaran_Alamat,
						"VendorPhone": $scope.TambahAdvancePayment_TujuanPembayaran_Telepon,
						"VendorKabKota": $scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota,
						"isOneTimeVendor": isOneTimeVendor,
						"OutletId": $scope.TambahAdvancedPayment_SelectBranch,
						"Notice": $scope.TambahIAdvance_TipeAdvanceiNotice
					}
				];
		
			}else if($scope.TambahAdvancePayment_TipeAdvancePayment.label == "Order Pekerjaan Luar"){
				$scope.DatatoSave = [
					{
						"AdvancePaymentDate": null,
						"AdvancePaymentType": $scope.TambahAdvancePayment_TipeAdvancePayment.label,
						"VendorId": $scope.TambahAdvancePayment_TujuanPembayaran_VendorId,
						"PaymentType": $scope.TambahAdvancePayment_MetodePembayaran,
						"PaidAmount": $scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran,
						"PaidDesc": $scope.TambahAdvancedPayment_RincianPembayaran_Keterangan,
						"MaturityDate": $filter('date')(new Date($scope.TambahAdvancedPayment_TanggalJatuhTempo.toLocaleString()), "yyyy-MM-dd"),
						"ReceiverName": $scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima,
						"LeaderName": $scope.TambahAdvancePayment_Footer_NamaPimpinan,
						"FinanceName": $scope.TambahAdvancePayment_Footer_NamaFinance,
						"POId": $scope.SelectedPOId,
						"VendorName": $scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor,
						"VendorNPWP": $scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor,
						"VendorAddress": $scope.TambahAdvancedPayment_TujuanPembayaran_Alamat,
						"VendorPhone": $scope.TambahAdvancePayment_TujuanPembayaran_Telepon,
						"VendorKabKota": $scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota,
						"isOneTimeVendor": isOneTimeVendor,
						"OutletId": $scope.TambahAdvancedPayment_SelectBranch,
						"POCode": $scope.POCode
					}
				];
			}
			else{
				$scope.DatatoSave = [
					{
						"AdvancePaymentDate": null,
						"AdvancePaymentType": $scope.TambahAdvancePayment_TipeAdvancePayment.label,
						"VendorId": $scope.TambahAdvancePayment_TujuanPembayaran_VendorId,
						"PaymentType": $scope.TambahAdvancePayment_MetodePembayaran,
						"PaidAmount": $scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran,
						"PaidDesc": $scope.TambahAdvancedPayment_RincianPembayaran_Keterangan,
						"MaturityDate": $filter('date')(new Date($scope.TambahAdvancedPayment_TanggalJatuhTempo.toLocaleString()), "yyyy-MM-dd"),
						"ReceiverName": $scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima,
						"LeaderName": $scope.TambahAdvancePayment_Footer_NamaPimpinan,
						"FinanceName": $scope.TambahAdvancePayment_Footer_NamaFinance,
						"POId": $scope.SelectedPOId,
						"VendorName": $scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor,
						"VendorNPWP": $scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor,
						"VendorAddress": $scope.TambahAdvancedPayment_TujuanPembayaran_Alamat,
						"VendorPhone": $scope.TambahAdvancePayment_TujuanPembayaran_Telepon,
						"VendorKabKota": $scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota,
						"isOneTimeVendor": isOneTimeVendor,
						"OutletId": $scope.TambahAdvancedPayment_SelectBranch
					}
				];
		
			}
	}

	$scope.TambahAdvancePayment_ClearFields = function () {
		$scope.TambahAdvancePayment_NamaVendor = "";
		//$scope.TambahAdvancePayment_TipeAdvancePayment = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_VendorId = 0;
		$scope.TambahAdvancePayment_MetodePembayaran = "";
		$scope.TambahAdvancePayment_RincianPembayaran_NominalPembayaran = "";
		$scope.TambahAdvancedPayment_RincianPembayaran_Keterangan = "";
		$scope.TambahAdvancedPayment_TanggalJatuhTempo = "";
		$scope.TambahAdvancePayment_RincianPembayaran_NamaPenerima = "";
		$scope.TambahAdvancePayment_Footer_NamaPimpinan = "";
		$scope.TambahAdvancePayment_Footer_NamaFinance = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NPWPVendor = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NamaVendor = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_NomorVendor = "";
		$scope.TambahAdvancedPayment_TujuanPembayaran_Alamat = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_KabupatenKota = "";
		$scope.TambahAdvancePayment_TujuanPembayaran_Telepon = "";
		$scope.POList_UIGrid.data = [];
		$scope.POListbyId_UIGrid.data = [];
	}

	$scope.MainAdvancedPayment_Cari_Clicked = function () {
		if ((new Date($scope.MainAdvancePayment_TanggalAwal) > new Date()) ||
			(new Date($scope.MainAdvancePayment_TanggalAkhir) > new Date())) {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal tidak bisa lebih besar dari hari ini.",
				type: "warning"
			});
		}
		else if (new Date($scope.MainAdvancePayment_TanggalAwal) > new Date($scope.MainAdvancePayment_TanggalAkhir)) {

			bsNotify.show({
				title: "Warning",
				content: "Tanggal awal tidak boleh lebih dari tanggal akhir.",
				type: "warning"
			});
		}
		else {
			$scope.AdvancePaymentList_UIGrid_Paging(1);
		}
	}

	$scope.ShowLihatAdvancePaymentById = function (row) {
		$scope.Show_LihatAdvancePayment = true;
		$scope.Show_MainAdvancePayment = false;
		$scope.Show_TambahAdvancePayment = false;
		$scope.Show_TambahAdvancePayment_Vendor_Detail = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_1 = false;
		$scope.Show_TambahAdvancePayment_Pembayaran_2 = false;
		if(row.entity.APPaymentTypeName == 'STNK/BPKB'){
			$scope.Show_LihatAdvancePayment_Notice = true; 
		}else{
			$scope.Show_LihatAdvancePayment_Notice = false; 
		}
		console.log("row.entity.IsUsed", row.entity.IsUsed);
		console.log("row.entity.APApprovalTypeName", row.entity.APApprovalTypeName);
		console.log("row.entity.APApprovalStatusName", row.entity.APApprovalStatusName);
		if (row.entity.EnabledCetak == 1) {
			$scope.LihatAdvancePayment_Cetak_EnabledDisabled = true;
		}
		else {
			$scope.LihatAdvancePayment_Cetak_EnabledDisabled = false;
		}

		if (row.entity.IsUsed == 1)
			$scope.LihatAdvancedPayment_PengajuanReversal_Button_Disabled = false;
		else {
			if (row.entity.APApprovalTypeName == 'Reversal AP' && row.entity.APApprovalStatusName != 'Ditolak')
				$scope.LihatAdvancedPayment_PengajuanReversal_Button_Disabled = true;
			else if(row.entity.APApprovalStatusName == 'Ditolak')
				$scope.LihatAdvancedPayment_PengajuanReversal_Button_Disabled = true;
			else
				$scope.LihatAdvancedPayment_PengajuanReversal_Button_Disabled = false;
		}
		if (row.entity.APPaymentTypeName != 'General Transaction')
			$scope.Show_TambahAdvancePayment_DaftarPO = true;
		else
			$scope.Show_TambahAdvancePayment_DaftarPO = false;

		var vendor = '';
		if (row.entity.VendorId == '' || row.entity.VendorId == 0 || row.entity.VendorId == null)
		{
			vendor = row.entity.VendorName;
		}
		else
		{
			vendor = row.entity.VendorId;
		}
		AdvancedPaymentFactory.getVendorByName(vendor, row.entity.AdvancedPaymentType,row.entity.OutletId)
			.then(
				function (res) {
					console.log("Data Vendor ==> ", res);
					$scope.LihatAdvancePayment_TujuanPembayaran_VendorId = res.data.Result[0].VendorId;
					$scope.LihatAdvancePayment_TujuanPembayaran_NamaVendor = res.data.Result[0].VendorName;
					$scope.LihatAdvancePayment_TujuanPembayaran_NPWPVendor = res.data.Result[0].NPWP;
					$scope.LihatAdvancePayment_TujuanPembayaran_NomorVendor = res.data.Result[0].VendorNo;
					$scope.LihatAdvancedPayment_TujuanPembayaran_Alamat = res.data.Result[0].Address;
					$scope.LihatAdvancePayment_TujuanPembayaran_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatAdvancePayment_TujuanPembayaran_Telepon = res.data.Result[0].Phone;
				}
			)
		$scope.LihatAdvancePayment_POId_Selected = row.entity.POId;
		$scope.LihatAdvancePaymnet_APId_Selected = row.entity.AdvancedPaymentId;
		$scope.POListbyId_UIGrid_Paging(1);
		$scope.LihatAdvancePayment_APId = row.entity.AdvancedPaymentId;
		$scope.LihatAdvancePayment_MetodePembayaran = row.entity.APPaymentMethod;
		$scope.LihatAdvancePayment_MetodePembayaran_NominalPembayaran = addSeparatorsNF(row.entity.Nominal, '.', '.', ',');
		$scope.LihatAdvancedPayment_MetodePembayaran_Keterangan = row.entity.Description;
		var tanggalJatuhTempo = new Date(row.entity.MaturityDate);
		tanggalJatuhTempo.setMinutes(tanggalJatuhTempo.getMinutes() + 420);
		$scope.LihatAdvancedPayment_TanggalJatuhTempo = tanggalJatuhTempo;
		$scope.LihatAdvancePayment_MetodePembayaran_NamaPenerima = row.entity.ReceiverName;
		$scope.LihatAdvancePayment_NomorAdvancePayment = row.entity.AdvancedPaymentNumber;
		$scope.LihatAdvancePayment_NomorAdvancePaymentReversal = row.entity.AdvancedPaymentReverseNumber;
		var lihatAPDate = new Date(row.entity.APDate);
		lihatAPDate.setMinutes(lihatAPDate.getMinutes() + 420);
		$scope.LihatAdvancedPayment_TanggalAdvancePayment = lihatAPDate;
		$scope.LihatAdvancePayment_Footer_NamaPimpinan = row.entity.LeaderName;
		$scope.LihatAdvancePayment_Footer_NamaFinance = row.entity.FinanceName;
		console.log("payment type yang dipilih : ", row.entity.APPaymentTypeName);
		$scope.LihatAdvancePayment_TipeAdvancePayment = row.entity.APPaymentTypeName;
		$scope.LihatAdvancePayment_TipeAdvancePaymentId = row.entity.AdvancedPaymentType;
		$scope.LihatAdvancedPayment_MainNamaBranchId = row.entity.OutletId;
		$scope.LihatAdvancedPayment_MainNamaBranch = row.entity.OutletName;
		$scope.LihatAdvancePayment_TipeAdvancePaymentNotice = row.entity.Notice;
		$scope.LihatAdvancePayment_MetodePembayaran_Changed();

	}
	//==========================UI GRID BEGIN==========================//
	$scope.POList_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO" },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nominal PO", field: "NominalPO", cellFilter: "rupiahN" },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPOList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.POList_UIGrid_PagingSTNK(pageNumber, '', '');
			});
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.SelectedPOId = 0;
				}
				else {
					$scope.SelectedPOId = row.entity.POId;
					$scope.POCode = row.entity.NomorPO;
				}
			});
		}
	};

	//Get PO Baru
	$scope.POList_UIGrid_PagingSTNK = function (pageNumber, filterValue, filterColumn) {
		AdvancedPaymentFactory.getDataPONew(pageNumber, $scope.uiGridPageSize,
			$scope.TambahAdvancePayment_NamaVendor + '|' +  $scope.TambahAdvancePayment_TujuanPembayaran_IdPelangganVendor, $scope.TambahAdvancePayment_TipeAdvancePayment.valueType,
			filterValue, filterColumn, $scope.TambahAdvancedPayment_SelectBranch, $scope.TambahIAdvance_TipeAdvanceiNotice)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res =>", res.data.Result);
						$scope.POList_UIGrid.data = res.data.Result;
						$scope.POList_UIGrid.totalItems = res.data.Total;
						$scope.POList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.POList_UIGrid.paginationPageSizes = [10, 25, 50];
					}
				}

			);
	}

	$scope.POListbyId_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO" },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nominal PO", field: "NominalPO", cellFilter: "rupiahN" },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPOListbyId = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.POListbyId_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.POListbyId_UIGrid_Paging = function (pageNumber) {
		AdvancedPaymentFactory.getDataPObyIdCR(pageNumber, $scope.uiGridPageSize,
			$scope.LihatAdvancePayment_POId_Selected, $scope.LihatAdvancePaymnet_APId_Selected)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res =>", res.data.Result);
						$scope.LihatAdvancePayment_TipeAdvancePaymentNotice = res.data.Result[0].Notice;	
						$scope.POListbyId_UIGrid.data = res.data.Result;
						$scope.POListbyId_UIGrid.totalItems = res.data.Total;
						$scope.POListbyId_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.POListbyId_UIGrid.paginationPageSizes = [10, 25, 50];
					}
				}

			);
	}

	$scope.AdvancePaymentList_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		//useCustomPagination: true,
		//useExternalPagination: true,
		enablePagination:true,
		rowSelection: true,
		enableFiltering: true,
		enableHorizontalScrollbar: 2,
		enableVerticalScrollbar: 2,
		columnDefs: [
			{ name: "APId", field: "AdvancedPaymentId", visible: false },
			{ name: "VendorId", field: "VendorId", visible: false },
			{ name: "NPWPVendor", field: "NPWPVendor", visible: false },
			{ name: "APPaymentMethod", field: "APPaymentMethod", visible: false },
			{ name: "Description", field: "Description", visible: false },
			{ name: "MaturityDate", field: "MaturityDate", visible: false },
			{ name: "ReceiverName", field: "ReceiverName", visible: false },
			{ name: "LeaderName", field: "LeaderName", visible: false },
			{ name: "FinanceName", field: "FinanceName", visible: false },
			{ name: "AdvancedPaymentType", field: "AdvancedPaymentType", visible: false },
			{ name: "ApprovalType", field: "ApprovalType", visible: false },
			{ name: "ApprovalStatus", field: "ApprovalStatus", visible: false },
			{ name: "IsUsed", field: "IsUsed", visible: false },
			{ name: "Tanggal Advance Payment", field: "APDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 90 },
			{
				name: "Nomor Advance Payment", field: "AdvancedPaymentNumber", width: "150",
				cellTemplate: '<a ng-click="grid.appScope.ShowLihatAdvancePaymentById(row)">{{row.entity.AdvancedPaymentNumber}}</a>'
				, cellClass: 'ui-grid-cell-contents-auto'
			},
			{ name: "Tipe Advance Payment", field: "APPaymentTypeName", width: 150,enableHiding:false },
			{ name: "Nama Vendor", field: "VendorName", width: 150 },
			{ name: "Nominal Pembayaran", field: "Nominal", width: 150, cellFilter: "rupiahN" },
			{ name: "Tipe Pengajuan", field: "APApprovalTypeName", width: 150 },
			{ name: "Tanggal Pengajuan", field: "CreatedDate", cellFilter: 'date:\"dd-MM-yyyy\"', width: 90 },
			{ name: "Status Pengajuan", field: "APApprovalStatusName", width: 150 },
			{ name: "Alasan Pengajuan", field: "ReversalReason", width: 150 },
			{ name: "Alasan Penolakan", field: "RejectedReason", width: 150 },
			{ name: "OutletId", field: "OutletId", visible: false },
			{ name: "OutletName", field: "OutletName", visible: false }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiAdvancePaymentList = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	//$scope.AdvancePaymentList_UIGrid_Paging(pageNumber);
			// });
		}
	};

	$scope.AdvancePaymentList_UIGrid_Paging = function (pageNumber) {

		AdvancedPaymentFactory.getDataAP(pageNumber, $scope.uiGridPageSize,
			$filter('date')(new Date($scope.MainAdvancePayment_TanggalAwal), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.MainAdvancePayment_TanggalAkhir), 'yyyy-MM-dd'),
			$scope.MainAdvancedPayment_SelectBranch)
			.then(
				function (res) {
					console.log("res =>", res.data.Result);
					$scope.AdvancePaymentList_UIGrid.data = res.data.Result;
					$scope.AdvancePaymentList_UIGrid.totalItems = res.data.Total;
					$scope.AdvancePaymentList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.AdvancePaymentList_UIGrid.paginationPageSizes = [10, 25, 50];
				}

			);


	}
	//==========================UI GRID END==========================//
	//==========================CETAK BJD BEGIN==========================//
	$scope.LihatAdvancedPayment_CetakBJD_Clicked = function () {
		console.log("tipe payment : ", $scope.LihatAdvancePayment_MetodePembayaran);
		$scope.Report_Cetak_Clicked($scope.LihatAdvancePayment_APId, $scope.LihatAdvancePayment_TipeAdvancePaymentId, $scope.LihatAdvancePayment_MetodePembayaran);
	}

	$scope.Report_Cetak_Clicked = function (APId, APTypeId, APPaymentMethod) {
		var pdfFile = "";
		var endDate = "";
		console.log("masuk cetak dengan apid :", APId);
		console.log("masuk cetak metode :", APPaymentMethod);
		if (APPaymentMethod == 'Bank Transfer') {
			AdvancedPaymentFactory.getCetakReportAdvancePaymentBJD(APId, APTypeId)
				.then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);

						console.log("pdf", fileURL);

						printJS(fileURL);
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}
		else {
			AdvancedPaymentFactory.getCetakReportAdvancePaymentBJDCash(APId, APTypeId)
				.then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);

						console.log("pdf", fileURL);

						printJS(fileURL);
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}
	};
	//==========================CETAK BJD END==========================//

	//============================ Start List Nama Vendor ======================//

	$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_UIGrid = {

		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Id", displayName:"Id", field:"Id", enableCellEdit: false, enableHiding: false, visible:false},
				//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
				{name:"DataName", displayName:"Name", field:"Name", enableCellEdit: false, enableHiding: false}
			],
			
		onRegisterApi: function(gridApi) {
			$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI = gridApi;
		}
	};

	$scope.TambahAdvancePayment_TujuanPembayaran_NamaPelangganVendor_Click=function() {
		if ($scope.EditMode == true)
		{
			$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_UIGrid.columnDefs= [
				{name:"Id", displayName:"Id", field:"Id", enableCellEdit: false, enableHiding: false, visible:false},
				//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
				{name:"DataCode", displayName:"Code", field:"VendorCode", enableCellEdit: false, enableHiding: false},
				{name:"DataName", displayName:"Name", field:"Name", enableCellEdit: false, enableHiding: false},
				{name:"DataAlamat", displayName:"Address", field:"Address", enableCellEdit: false, enableHiding: false}
			];
			AdvancedPaymentFactory.getSOVendorList( $scope.TambahAdvancePayment_TipeAdvancePayment.label , '', $scope.TambahAdvancedPayment_SelectBranch).then(
				function(res) {
					$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_UIGrid.data = res.data.Result;
					angular.element('#ModalTambahAdvancePayment_GetSOVendorCustomer').modal('setting',{closeable:false}).modal('show');
				},
				function(err) {
					console.log("err=>", err);
				} 
			);	
		}	
	};

	$scope.Batal_TambahAdvancePayment_GetSOVendorCustomer=function() {
		angular.element('#ModalTambahAdvancePayment_GetSOVendorCustomer').modal('hide');	
	}

	$scope.Pilih_TambahAdvancePayment_GetSOVendorCustomer=function() {
		if ($scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.selection.getSelectedCount() > 0)
		{
			$scope.ModalTambahAdvancePayment_GetSOVendorCustomer_gridAPI.selection.getSelectedRows().forEach(function(row) {	
				$scope.TambahAdvancePayment_NamaVendor =  row.Name;
				$scope.TambahAdvancePayment_TujuanPembayaran_IdPelangganVendor = row.Id;
			});
			
			angular.element('#ModalTambahAdvancePayment_GetSOVendorCustomer').modal('hide');	
		}
		else 
		{
			//alert('Belum ada data yang dipilih');
			bsNotify.show({
				title: "Warning",
				content: 'Belum ada data yang dipilih !',
				type: 'warning'
			});
			$scope.Loading = false;
		}		
	}

});

app.filter('rupiahN', function () { 
	return function (val) {
		var newVal = val.toString().split('.');
		var rval = '';
		while (/(\d+)(\d{3})/.test(newVal[0])) {
			newVal[0] = newVal[0].replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
		}

		if(!angular.isUndefined(newVal[1])){
			rval = newVal[0] + ',' + newVal[1];
		}
		else
		{rval = newVal[0]}
		return rval;
	};
});
