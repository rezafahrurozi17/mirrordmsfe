angular.module('app')
	.factory('AdvancedPaymentFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getDataBranchComboBox:function (orgId) {
			console.log("factory.getDataBranchComboBox");
			//var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData='+ orgId;
			var res = $http.get(url);
			return res;
		},
		
		getDataPO:function(Start, Limit, VendorName, AdvPaymentType, filterValue, filterColumn, OutletId){
			var url = '/api/fe/FinancePO/GetPOADPbyVendorName?start=' + Start + '&limit='+ Limit + '&vendorname=' + VendorName + '&OutletId=' + OutletId + '&AdvPaymentTypeId=' + AdvPaymentType + '&filterValue=' + filterValue + '&filterColumn=' + filterColumn;
			console.log("url getDataPO : ", url);
			var res=$http.get(url);
			return res;
		},

		getDataPONew:function(Start, Limit, VendorName, AdvPaymentType, filterValue, filterColumn, OutletId, Notice){
			var url = '/api/fe/FinancePO/GetPOADPbyVendorNameCR?start=' + Start + '&limit='+ Limit + '&vendorname=' + VendorName + '&OutletId=' + OutletId + '&AdvPaymentTypeId=' + AdvPaymentType + '&filterValue=' + filterValue + '&filterColumn=' + filterColumn + '&Notice=' + Notice;
			console.log("url getDataPO : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataPObyId:function(Start, Limit, POId){
			var url = '/api/fe/FinancePO/GetPOADPbyPOId?start=' + Start + '&limit='+ Limit + '&poid=' + POId;
			console.log("url getDataPO : ", url);
			var res=$http.get(url);
			return res;
		},

		getDataPObyIdCR:function(Start, Limit, POId, ApId){
			var url = '/api/fe/FinancePO/GetPOADPbyPOIdCR?start=' + Start + '&limit='+ Limit + '&poid=' + POId + '&APId=' +ApId;
			console.log("url getDataPO : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getVendorByName:function(VendorName, vendorTypeId, OutletId){ 
			var url = '/api/fe/FinanceVendor/GetDatabyVendorName?VendorName=' + VendorName + '&outletid='+ OutletId + '&IncomingInvoiceTypeId=' + vendorTypeId;
			console.log("url getDataVendor : ", url);
			var res=$http.get(url);
			return res;
		},
		saveData:function(inputData){
			var url = '/api/fe/advancepayment/SubmitData/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData AdvancedPayment ", param);
			var res=$http.post(url, param);
			return res;
		},
		saveDataOPL:function(inputData){
			var url = '/api/fe/advancepayment/SubmitDataOPL/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData AdvancedPayment ", param);
			var res=$http.post(url, param);
			return res;
		},

		//Save STNK/BPKB
		saveDataSTNK:function(inputData){
			var url = '/api/fe/advancepayment/SubmitDataCR/';
			var param = JSON.stringify(inputData);
			console.log("object input saveData AdvancedPayment ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		updateApprovalData:function(inputData){
			var url = '/api/fe/Financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		getDataAP:function(Start, Limit, dateStart, dateEnd, outletId){
			var url = '/api/fe/advancepayment?start=' + Start + '&limit='+ Limit + '&datestart=' + dateStart + '&dateend=' + dateEnd + '&outletid=' + outletId;
			console.log("url getDataAP : ", url);
			var res=$http.get(url);
			return res;
		},
		
		deleteDataAP:function(APId, reversalReason, OutletId){
			var url = '/api/fe/advancepayment/deletedata?AdvancePaymentId=' + APId + '&ReversalReason=' + reversalReason + '&outletid=' + OutletId;
			console.log("url reverse DataAP : ", url);
			var res=$http.delete(url);
			return res;
		},
		
		getDataAPType:function(allowUnit){
			var TypeId = 0;
			if(allowUnit)
				TypeId = 1;
			var url = '/api/fe/AdvancePayment/GetAdvancePaymentType/?TypeId='+TypeId;
			console.log("url getDataAPType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getCetakReportAdvancePaymentBJD:function(APId, PaymentTypeId) {
			var url = '/api/fe/FinanceRptCetakBJD/Cetak/?PaymentId=' + APId + '&PaymentTypeId='+PaymentTypeId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},
		
		getCetakReportAdvancePaymentBJDCash:function(APId, PaymentTypeId) {
			var url = '/api/fe/FinanceRptCetakBJD/CetakCash/?PaymentId=' + APId + '&PaymentTypeId='+PaymentTypeId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},

		getSOVendorList: function(tipe, name, outletId) {
			var FilterData = [{PIType : tipe, VendorName : name, ShowAll : 1, OutletId: outletId}];
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			console.log("url getSOVendorList", url);
			var res=$http.get(url);
			//console.log('res=>',res);
			return res;
		},

		getVendorInfo:function(tipe, name , id) {
			var FilterData = [{PIType : tipe, VendorName : name, VendorId: id,ShowAll : 0}];
			var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			var res=$http.get(url);
			console.log("getVendorInfo url : ", url);
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		}
	}
});
