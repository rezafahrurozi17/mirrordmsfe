angular.module('app')
.controller('AnggaranBelanjaController', function ($scope, $http, $filter, CurrentUser, AnggaranBelanjaFactory, $timeout,bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.ShowMainAnggaranBelanja = true;
	
	/*==================================================================================================*/
	/* 										Main Anggaran Belanja Begin									*/
	/*==================================================================================================*/
	$scope.MainAnggaranBelanjaUpload_Clicked = function(){
		$scope.ShowMainAnggaranBelanja = false;
		$scope.ShowUploadAnggaranBelanja = true;
		$scope.ShowTambahAnggaranBelanja = false;
	}
	
	$scope.MainAnggaranBelanjaTambah_Clicked = function(){
		$scope.ShowMainAnggaranBelanja = false;
		$scope.ShowUploadAnggaranBelanja = false;
		$scope.ShowTambahAnggaranBelanja = true;
		$scope.AnggaranBelanjaTambahList_UIGrid_Paging();
	}
	
	$scope.MainAnggaranBelanjaBackToMain = function(){
		$scope.ShowMainAnggaranBelanja = true;
		$scope.ShowUploadAnggaranBelanja = false;
		$scope.ShowTambahAnggaranBelanja = false;
		
	}
	/*==================================================================================================*/
	/* 										Main Anggaran Belanja End 									*/
	/*==================================================================================================*/
	
	/*==================================================================================================*/
	/* 									Upload Anggaran Belanja Begin									*/
	/*==================================================================================================*/
	$scope.UploadAnggaranBelanjaKembali_Clicked = function(){
		$scope.ShowMainAnggaranBelanja = true;
		$scope.ShowUploadAnggaranBelanja = false;
		$scope.ShowTambahAnggaranBelanja = false;
	}
	
	$scope.UploadAnggaranBelanja_DownloadExcelTemplate = function(){
		var excelData = [];
		var fileName = "";
		
		excelData = [{"Nama_GL_Biaya":"", "Kode_GL_Biaya":"", "Cost_Center":"", "Periode_Awal":"", "Periode_Akhir":"", "Anggaran_Maksimal":""}];
		fileName = "AnggaranBelanjaTemplate";
		
		XLSXInterface.writeToXLSX(excelData, fileName);
	}
	
	$scope.UploadAnggaranBelanja_Download_Clicked = function(){
		$scope.UploadAnggaranBelanja_DownloadExcelTemplate();
	}
	
	$scope.loadXLS = function(ExcelFile){
		var ExcelData = [];
		
		var myEl = angular.element( document.querySelector( '#uploadAnggaranBelanja' ) ); //ambil elemen dari dokumen yang di-upload 
		
		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				ExcelData = json[0];
				
				if(!$scope.validateColumn(ExcelData)){
					console.log("Kolom file excel tidak sesuai");
				}
				else{
					$scope.ExcelData = json;
				}
            });
	}
	
	$scope.validateColumn = function(inputExcelData){
		if(angular.isUndefined(inputExcelData.Anggaran_Maksimal) || angular.isUndefined(inputExcelData.Cost_Center) 
			|| angular.isUndefined(inputExcelData.Kode_GL_Biaya) || angular.isUndefined(inputExcelData.Nama_GL_Biaya)
			|| angular.isUndefined(inputExcelData.Periode_Awal) || angular.isUndefined(inputExcelData.Periode_Akhir)){
			return(false);
		}
		
		return(true);
	}
	
	$scope.UploadAnggaranBelanja_Upload_Clicked = function(){
		console.log("isi Excel Data :", $scope.ExcelData);
		var UIGridData = [];
		var keepGoing = true;
		angular.forEach($scope.ExcelData, function(value, key){
			if(keepGoing){
				if(value.Periode_Awal.indexOf("/") != -1 || value.Periode_Akhir.indexOf("/") != -1){
					//alert("Periode harus ber-format dd-MM-yyyy. Mohon file di-upload ulang dengan format tanggal yang benar");
					bsNotify.show({
						title: "Warning",
						content: 'Periode harus ber-format dd-MM-yyyy. Mohon file di-upload ulang dengan format tanggal yang benar!',
						type: 'warning'
					});
					$scope.UploadAnggaranBelanja_ClearFields();
					keepGoing = false;
				}
			}
		});
		
		if(!keepGoing)
			return;
		
		angular.forEach($scope.ExcelData, function(value, key){
			UIGridData.push({
				"BudgetId":0,
				"GLId":0,
				"GLBudgetName":value.Nama_GL_Biaya,
				"GLBudgetCode":value.Kode_GL_Biaya,
				"CostCenter":value.Cost_Center,
				"DateStart":value.Periode_Awal,
				"DateEnd":value.Periode_Akhir,
				"Nominal":value.Anggaran_Maksimal,
			});
		});
		console.log("UIGridData : ", UIGridData);
		$scope.AnggaranBelanjaUploadList_UIGrid.data = UIGridData;
		$scope.GridApiAnggaranBelanjaUploadList.grid.refresh();
	}
	
	$scope.UploadAnggaranBelanjaSimpan_Clicked = function(){
		AnggaranBelanjaFactory.submitData($scope.AnggaranBelanjaUploadList_UIGrid.data)
		.then(
			function(res){
				//alert("data anggaran belanja berhasil disimpan");
				if (res.data.code == "200")
				{
					bsNotify.show({
						title:"Berhasil",
						content:"data anggaran belanja berhasil disimpan.",
						type:"success"
					});
					$scope.UploadAnggaranBelanja_ClearFields();
					$scope.MainAnggaranBelanjaBackToMain();	
				}
				else{
					bsNotify.show({
						title:"Error",
						content:res.data.message,
						type:"danger"
					});
				}
				
				
			},
			function(err){
				bsNotify.show({
					title:"Error",
					content:"err.data.ExceptionMessage.",
					type:"danger"
				});
				//alert(err.data.ExceptionMessage);
			}
		);
	}
	
	$scope.UploadAnggaranBelanja_ClearFields = function(){
		angular.element( document.querySelector( '#uploadAnggaranBelanja' ) ).val(null);
		$scope.AnggaranBelanjaUploadList_UIGrid.data = [];
	}
	/*==================================================================================================*/
	/* 									 Upload Anggaran Belanja End									*/
	/*==================================================================================================*/
	
	/*==================================================================================================*/
	/* 									 Tambah Anggaran Belanja Begin									*/
	/*==================================================================================================*/
	
	$scope.TambahAnggaranBelanjaSimpan_Clicked = function(){
		debugger;
		var temp = true;
		var NomAdd = 0;
		angular.forEach($scope.AnggaranBelanjaTambahList_UIGrid.data, function(value, key){
			NomAdd = value.NominalAdd 
			if(NomAdd < 0)
			{
				temp=false;
			}
		});
		
		if(temp==true)
		{
			AnggaranBelanjaFactory.updateData($scope.AnggaranBelanjaTambahList_UIGrid.data)
			.then(
				function(res){
					//alert("data anggaran belanja berhasil disimpan");
					bsNotify.show({
						title:"Berhasil",
						content:"Data anggaran belanja berhasil disimpan.",
						type:"success"
					});
					$scope.MainAnggaranBelanjaBackToMain();
				}
			);
		}
		else
		{
			bsNotify.show({
				title:"Gagal",
				content:"Data anggaran belanja tidak berhasil disimpan.",
				type:"danger"
			});
		}
	}
	
	$scope.TambahAnggaranBelanjaKembali_Clicked = function(){
		$scope.MainAnggaranBelanjaBackToMain();
	}
	/*==================================================================================================*/
	/* 									 Tambah Anggaran Belanja End									*/
	/*==================================================================================================*/
	
	
	/*==================================================================================================*/
	/* 								Upload Anggaran Belanja UI Grid Begin								*/
	/*==================================================================================================*/
	$scope.uiGridPageSize = 100;
	
	$scope.AnggaranBelanjaUploadList_UIGrid = {
		paginationPageSizes: [10,25,50],
		useCustomPagination: true,
		useExternalPagination : true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: [
						{name:"BudgetId",field:"BudgetId",visible:false},
						{name:"GLId",field:"GLId",visible:false},
						{name:"Nama GL Biaya",field:"GLBudgetName",displayName:"Nama GL Biaya"},
						{name:"Kode GL Biaya",field:"GLBudgetCode",displayName:"Kode GL Biaya"},
						{name:"Cost Center", field:"CostCenter"},
						{name:"Periode Awal", field:"DateStart", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Periode Akhir", field:"DateEnd", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Anggaran Maksimal", field:"Nominal", cellFilter:"rupiahC", type:"number"},
					],
		onRegisterApi: function(gridApi) {
			$scope.GridApiAnggaranBelanjaUploadList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.AnggaranBelanjaUploadList_UIGrid_Paging();
			});
		}
	};
	
	$scope.AnggaranBelanjaUploadList_UIGrid_Paging = function(){
		  AnggaranBelanjaFactory.getDataAnggaranBelanja()
		  .then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					console.log("res =>", res.data.Result);
					$scope.AnggaranBelanjaUploadList_UIGrid.data = res.data.Result;
					$scope.AnggaranBelanjaUploadList_UIGrid.totalItems = res.data.Total;
					$scope.AnggaranBelanjaUploadList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.AnggaranBelanjaUploadList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				}
			}
			
		);
	}
	/*==================================================================================================*/
	/* 								 Upload Anggaran Belanja UI Grid End								*/
	/*==================================================================================================*/
	
	/*==================================================================================================*/
	/* 								Tambah Anggaran Belanja UI Grid Begin								*/
	/*==================================================================================================*/
	$scope.AnggaranBelanjaTambahList_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination : true,
		rowSelection : true,
		multiSelect : false,
		columnDefs: [
						{name:"BudgetId",field:"BudgetId",visible:false},
						{name:"GLId",field:"GLId",visible:false},
						{name:"Nama GL Biaya",field:"GLBudgetName", enableCellEdit:false,displayName:"Nama GL Biaya"},
						{name:"Kode GL Biaya",field:"GLBudgetCode", enableCellEdit:false,displayName:"Nama GL Biaya"},
						{name:"Cost Center", field:"CostCenter", enableCellEdit:false},
						{name:"Periode Awal", field:"DateStart", cellFilter:'date:\"dd-MM-yyyy\"', enableCellEdit:false},
						{name:"Periode Akhir", field:"DateEnd", cellFilter:'date:\"dd-MM-yyyy\"', enableCellEdit:false},
						{name:"Anggaran Maksimal", field:"Nominal", enableCellEdit:false,cellFilter:"rupiahC"},
						{name:"Penambahan Anggaran", field:"NominalAdd", cellFilter:"rupiahC", enableCellEdit:true, type:"number",
						 cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							          return 'canEdit';
							        }
						},
					],
		onRegisterApi: function(gridApi) {
			$scope.GridApiAnggaranBelanjaTambahList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.AnggaranBelanjaTambahList_UIGrid_Paging();
			});
		}
	};
	
	$scope.AnggaranBelanjaTambahList_UIGrid_Paging = function(){
		  AnggaranBelanjaFactory.getDataAnggaranBelanja()
		  .then(
			function(res)
			{
				if(!angular.isUndefined(res.data.Result)){
					console.log("res =>", res.data.Result);
					$scope.AnggaranBelanjaTambahList_UIGrid.data = res.data.Result;
					$scope.AnggaranBelanjaTambahList_UIGrid.totalItems = res.data.Total;
					$scope.AnggaranBelanjaTambahList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.AnggaranBelanjaTambahList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				}
			}
			
		);
	}
	/*==================================================================================================*/
	/* 								 Tambah Anggaran Belanja UI Grid End								*/
	/*==================================================================================================*/
	
});

app.filter('rupiahC', function () {
	return function (val) {
		console.log('value '+val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
	});