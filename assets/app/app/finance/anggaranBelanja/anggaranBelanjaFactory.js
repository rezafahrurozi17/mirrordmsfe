angular.module('app')
	.factory('AnggaranBelanjaFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
		
    factory.getDataAnggaranBelanja=function() {
		var url = '/api/fe/FinanceBudget/?start=1&limit=100';
		var res=$http.get(url);  
		return res;			
    }; 
	
	factory.submitData = function(inputData){
		var url = '/api/fe/FinanceBudget/SubmitData';
		//var param = JSON.stringify(inputData);
		var param = angular.toJson(inputData);
		console.log("Budget data : ", url);
		var res=$http.post(url, param);
		return res;
	}; 
	
	factory.updateData = function(inputData){
		var url = '/api/fe/FinanceBudget/UpdateData';
		var param = JSON.stringify(inputData);
		console.log("Budget data : ", url);
		var res=$http.put(url, param);
		return res;
	};
	
	return factory;
});