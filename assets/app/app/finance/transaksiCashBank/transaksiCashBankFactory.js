angular.module('app')
  .factory('TransaksiCashBankFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getDataNomorGL:function(isHO){
            var url = '/api/fe/FinanceCOA/GetDataByPurpose/?strGLType=GL Cash Bank&isHO='+isHO;
            console.log("url List GL CashBank : ", url);
            var res=$http.get(url);
            return res;
	  },
	  
	  getDataTipeTransaksiCashBank: function(){
		  var url = '/api/fe/financeparam/TipeTransaksiCashBank';
		  var res=$http.get(url);
		  return res;
	  },
	  
	  createCashBankData: function(inputData){
			debugger;
		  var url = '/api/fe/financecashbank/submitdata/';
		  var param = JSON.stringify(inputData);
		  console.log("object input cashbankdata", param);
		  var res=$http.post(url, param);
		  return res;
	  },
	  
	  getDataCashBank: function(Start, Limit, StartDate, EndDate, SearchText, SearchType){
		  var url = '/api/fe/FinanceCashBank/?start='+Start+'&limit='+Limit+'&StartDate='+StartDate+'&EndDate='+EndDate+ '|' +SearchText+ '|' +SearchType;
		  console.log("url get data cash bank : ", url);
		  var res=$http.get(url);
		  return res;
	  },
	  
	  /*
	  getDataCashBankDetail: function(id){
		  console.log("masuk getDataCashBankDetail dengan id :", id);
		  var url = 'http://localhost:9001/dmsfinanceapi/api/FinanceCashBank/getsingledata/' + id;
		  var res=$http.get(url);
		  return res;
	  },*/
	  
	  createCashBankReversal: function(inputData){
		  var url = '/api/fe/FinanceApproval/submitdata/';
		  var param = JSON.stringify(inputData);
		  var res=$http.post(url, param);
		  return res;
	  },
	  
	  updateApprovalData:function(inputData){
			var url = '/api/fe/FinanceApproval/ApproveRejectData/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		},
      // create: function(TransaksiCashBank) {
        // return $http.post('/api/fw/Role', [{
                                            // AppId: 1,
                                            // ParentId: 0,
                                            // Name: TransaksiCashBank.Name,
                                            // Description: TransaksiCashBank.Description}]);
      // },
      // update: function(TransaksiCashBank){
        // return $http.put('/api/fw/Role', [{
                                            // Id: TransaksiCashBank.Id,
                                            // //pid: negotiation.pid,
                                            // Name: TransaksiCashBank.Name,
                                            // Description: TransaksiCashBank.Description}]);
      // },
      // delete: function(id) {
        // return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      // },
    }
  });