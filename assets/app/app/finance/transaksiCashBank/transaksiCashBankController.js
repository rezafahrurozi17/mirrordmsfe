angular.module('app')
	.controller('TransaksiCashBankController', function ($scope, $http, $filter, CurrentUser, TransaksiCashBankFactory, $timeout, bsNotify) {
		$scope.ShowLihatTransaksiCashBank = true;
		var dateFormat = 'dd/MM/yyyy';
		$scope.dateOptions = { format: dateFormat }

		var paginationOptions_TransaksiCashBank_UIGrid = {
			pageNumber: 1,
			pageSize: 10,
			sort: null
		}

		//$scope.TambahTransaksiCashBank_TipeTransaksiCashBankOptions= [{ name: "Funding HO ke Cabang", value: "Funding HO ke Cabang" }, { name: "Pooling Cabang ke HO", value: "Pooling Cabang ke HO" }];
		var user = CurrentUser.user();
		$scope.dateNow = Date.now();
		//$scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions= [{ name: "Funding HO ke Cabang", value: "Funding HO ke Cabang" }, { name: "Pooling Cabang ke HO", value: "Pooling Cabang ke HO" }];

		$scope.LihatTransaksiCashBank_TransaksiCashBankSearchOptions = [
			{ name: "Nomor Transaksi Cash & Bank", value: "Nomor Transaksi Cash Bank" },
			{ name: "Tipe Transaksi Cash & Bank", value: "Tipe Transaksi Cash Bank" },
			{ name: "Nominal", value: "Nominal" }];

		// NEW BULK ACTION CEK RIGHT BYTE
		$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
		console.log("RIGHT BYTE >>>>>>>",$scope.ByteEnable);
		$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
		if ($scope.allowApprove == true)
			$scope.optionsLihatTransaksiCashBank_BulkAction = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
		else
			$scope.optionsLihatTransaksiCashBank_BulkAction = [];

		function checkRbyte(rb, b) {
			var p = rb & Math.pow(2, b);
			return ((p == Math.pow(2, b)));
		};

		// OLD BULK ACTION
		// $scope.optionsLihatTransaksiCashBank_BulkAction = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];

		// if (user.RoleName == 'ADH' ||user.RoleName == 'ADH BP' || user.RoleName == 'Admin TAM')
		// 	$scope.LihatTransaksiCashBank_BulkAction_isDisabled = false;
		// else
		// 	$scope.LihatTransaksiCashBank_BulkAction_isDisabled = true;

		$scope.LihatTransaksiCashBank_Cari = function () {
			//$scope.LihatTransaksiCashBank_DaftarTransaksiCashBank=true;
			$scope.TransaksiCashBank_UIGrid.paginationCurrentPage = 1;
			$scope.TransaksiCashBank_UIGrid_Paging(1);
		};

		/*
		$scope.LihatTransaksiCashBank_DaftarItem_SelectedChanged = function () {
			$scope.LihatTransaksiCashBank_Details=true;
		};*/

		TransaksiCashBankFactory.getDataTipeTransaksiCashBank()
			.then(
				function (res) {
					$scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions = res.data;
					//$scope.TipeTransaksiCashBank = res.data;
				}
			);

		$scope.BackToMainTransaksiCashBank = function () {
			$scope.ShowLihatTransaksiCashBank = true;
			$scope.ShowTambahTransaksiCashBank = false;
		};

		$scope.ClearAllField = function(){
			$scope.TambahTransaksiCashBank_TanggalTransaksiCashBank ="";
			$scope.TambahTransaksiCashBank_TipeTransaksiCashBank = "";
			$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO = "";
			$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang = "";
			$scope.TambahTransaksiCashBank_Nominal = "";
			$scope.TambahTransaksiCashBank_Keterangan = "";
			$scope.TambahTransaksiCashBank_KodeGlAccountHo = "";
			$scope.TambahTransaksiCashBank_KodeGlAccountCabang = "";
		}

		$scope.ToTambahTransaksiCashBank = function () {
			$scope.ProsesSubmit = false;
			$scope.ShowLihatTransaksiCashBank = false;
			$scope.ShowTambahTransaksiCashBank = true;
			$scope.ClearAllField();
		};
		$scope.ProsesSubmit = false;
		$scope.TambahTransaksiCashBank_Simpan = function () {
			$scope.TambahTransaksiCashBank_Simpan_Button_Clicked();
		};

		/*
		$scope.LihatTransaksiCashBank_PengajuanReversalModal = function () {
			angular.element('#ModalLihatTransaksiCashBank_PengajuanReversal').modal('show');
		};
		*/
		$scope.Batal_LihatTransaksiCashBank_PengajuanReversalModal = function () {
			angular.element('#ModalLihatTransaksiCashBank_PengajuanReversal').modal('hide');
		};

		$scope.Pengajuan_LihatTransaksiCashBank_PengajuanReversalModal = function () {
			if($scope.LihatTransaksiCashBank_PengajuanReversal_AlasanReversal == ""){
				bsNotify.show({
					title: "Warning",
					content: "Alasan Reversal harus diisi",
					type: "warning"
				});
				return false;
			}
			$scope.Pengajuan_LihatTransaksiCashBank_PengajuanReversalModal_Clicked();
		};

		//======================Devokus Edit, BEGIN=======================//

		$scope.ShowLihatTransaksiCashBankDetailById = function (row) {
			console.log("row.entity", row.entity);
			console.log("LihatTransaksiCashBank_TipeTransaksiCashBankOptions : ", $scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions);
			console.log("TranType : ", row.entity.TranType);
			$scope.LihatTransaksiCashBank_LastApprovalId = row.entity.LastApprovalId;
			$scope.LihatTransaksiCashBank_CashBankId = row.entity.CashBankId;
			$scope.LihatTransaksiCashBank_TanggalTransaksi = row.entity.TranDate;
			$scope.LihatTransaksiCashBank_NoTransaksi = row.entity.CashBankNo;
			$scope.LihatTransaksiCashBank_TipeTransaksi = row.entity.Label;
			$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountHO = row.entity.GLHO;
			$scope.LihatTransaksiCashBank_KodeGlAccountHo = row.entity.GLHOCode;
			$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountCabang = row.entity.GLBranch;
			$scope.LihatTransaksiCashBank_KodeGlAccountCabang = row.entity.GLBranchCode;
			$scope.LihatTransaksiCashBank_Nominal = row.entity.Nominal;
			$scope.LihatTransaksiCashBank_Keterangan = row.entity.Description;
			$scope.LihatTransaksiCashBank_Details = true;
			$scope.ShowLihatTransaksiCashBank = false;
			$scope.LihatTransaksiCashBank_NoTransaksiReversal = row.entity.ReverseNo;
			
			if(row.entity.ApprovalStatus == "Diajukan" || row.entity.ApprovalStatus == "Disetujui" ){
				$scope.LihatTransaksiCashBank_Reversal_Clicked_Disabled = true;
			}else{
				$scope.LihatTransaksiCashBank_Reversal_Clicked_Disabled = false;
			}
		}

		$scope.LihatTransaksiCashBank_Reversal_Clicked = function () {
			angular.element('#ModalLihatTransaksiCashBank_PengajuanReversal').modal('show');
		}

		$scope.LihatTransaksiCashBank_Kembali_Clicked = function () {
			$scope.LihatTransaksiCashBank_Details = false;
			$scope.ShowLihatTransaksiCashBank = true;
			// $scope.LihatTransaksiCashBank_Cari();
		}
		/*===============================Bulk Action Section, Begin===============================*/
		$scope.LihatTransaksiCashBank_BulkAction_Changed = function () {
			if ($scope.LihatTransaksiCashBank_BulkAction == "Setuju") {
				angular.forEach($scope.GridApiTransaksiCashBank.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolakTransaksiCashBank_CashBankId = value.CashBankId;
					$scope.ModalTolakTransaksiCashBank_TipePengajuan = value.ApprovalType;
					if (value.ApprovalStatus == "Diajukan") {
						$scope.CreateApprovalData("Disetujui");
						console.log('Obj Approval -> ' + $scope.ApprovalData);
						TransaksiCashBankFactory.updateApprovalData($scope.ApprovalData)
							.then(
								function (res) {
									bsNotify.show({
										title: "Berhasil",
										content: "Persetujuan Berhasil",
										type: 'success'
									});
									$scope.LihatTransaksiCashBank_Cari();
								},

								function (err) {
									//alert("Persetujuan gagal");
									bsNotify.show({
										title: "Gagal",
										content: "Persetujuan gagal. " + err.data.Message,
										type: "danger"
									});
									return false;
								}
							);
					}
					else {
						//alert("Status data bukan diajukan");
						bsNotify.show({
							title: "Info",
							content: "status data : " + value.CashBankNo + ' bukan diajukan',
							type: "info"
						});
						return false;
					}
				});
				$scope.LihatTransaksiCashBank_BulkAction = "";
			}

			else if ($scope.LihatTransaksiCashBank_BulkAction == "Tolak") {
				console.log("grid api ap list ", $scope.GridApiTransaksiCashBank);
				var tanggalPengajuan = "";
				var tanggalAP = "";
				angular.forEach($scope.GridApiTransaksiCashBank.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolakTransaksiCashBank_CashBankId = value.CashBankId;
					$scope.ModalTolakTransaksiCashBank_CashBankNo = value.CashBankNo;
					$scope.ModalTolakTransaksiCashBank_TanggalTransaksi = $filter('date')(new Date(value.TranDate.setMinutes(value.TranDate.getMinutes() + 420)), "dd/MM/yyyy");
					$scope.ModalTolakTransaksiCashBank_TipeTransaksi = value.Label;
					$scope.ModalTolakTransaksiCashBank_Nominal = value.Nominal;
					$scope.ModalTolakTransaksiCashBank_TipePengajuan = value.ApprovalType;
					$scope.ModalTolakTransaksiCashBank_TanggalPengajuan = $filter('date')(new Date(value.CreateDate.setMinutes(value.CreateDate.getMinutes() + 420)), "dd/MM/yyyy");
					$scope.ModalTolakTransaksiCashBank_AlasanPengajuan = value.Description;

					if (value.ApprovalStatus == "Diajukan")
						angular.element('#ModalTolakTransaksiCashBank').modal('show');
					else
						bsNotify.show({
							title: "Info",
							content: "status data : " + value.CashBankNo + ' bukan diajukan',
							type: "info"
						});
					return false;
				}
				);
				$scope.LihatTransaksiCashBank_BulkAction = "";
			}
		}

		$scope.CreateApprovalData = function (ApprovalStatus) {
			$scope.ApprovalData = [
				{
					"ApprovalTypeName": $scope.ModalTolakTransaksiCashBank_TipePengajuan,
					"RejectedReason": $scope.ModalTolakTransaksiCashBank_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakTransaksiCashBank_CashBankId,
					"ApprovalStatusName": ApprovalStatus,
				}
			];
		}

		$scope.Pengajuan_ModalTolakTransaksiCashBank = function () {
			$scope.CreateApprovalData("Ditolak");
			TransaksiCashBankFactory.updateApprovalData($scope.ApprovalData)
				.then(
					function (res) {
						//alert("Berhasil Ditolak");
						bsNotify.show({
							title: "Berhasil",
							content: "Berhasil ditolak.",
							type: "success"
						});
						$scope.ModalTolakTransaksiCashBank_AlasanPenolakan = '';
						angular.element('#ModalTolakTransaksiCashBank').modal('hide');
						$scope.LihatTransaksiCashBank_Cari();
					},

					function (err) {
						//alert("Penolakan gagal");
						bsNotify.show({
							title: "Gagal",
							content: "Penolakan gagal.",
							type: "danger"
						});
						angular.element('#ModalTolakTransaksiCashBank').modal('hide');
					}
				);
		}

		$scope.Batal_ModalTolakTransaksiCashBank = function () {
			angular.element('#ModalTolakTransaksiCashBank').modal('hide');
		}
		/*$scope.Batal_LihatAdvancePayment_TolakModal = function(){
			angular.element('#ModalTolakAdvancePayment').modal('hide');
			$scope.MainAdvancePayment_BulkAction = "";
			$scope.ClearActionFields();
			$scope.AdvancePaymentList_UIGrid_Paging(1);
		}
		
		$scope.Pengajuan_LihatAdvancePayment_TolakModal = function(){
			$scope.CreateApprovalData("Ditolak");
			AdvancedPaymentFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function(res){
					alert("Berhasil tolak");
					$scope.Batal_LihatAdvancePayment_TolakModal();
				},
				
				function(err){
					alert("Penolakan gagal");
				}
			);
		}
		
		$scope.ClearActionFields = function(){
			$scope.ModalTolakAdvancePayment_APId = "";
			$scope.ModalTolakAdvancePayment_Tanggal = "";
			$scope.ModalTolakAdvancePayment_NomorAP = "";
			$scope.ModalTolakAdvancePayment_TipeAP = "";
			$scope.ModalTolakAdvancePayment_NamaVendor = "";
			$scope.ModalTolakAdvancePayment_Nominal = "";
			$scope.ModalTolakAdvancePayment_TipePengajuan = "";
			$scope.ModalTolakAdvancePayment_TanggalPengajuan = "";
			$scope.ModalTolakAdvancePayment_AlasanPengajuan = "";
		}*/
		/*===============================Bulk Action Section, End===============================*/

		$scope.TransaksiCashBank_Selected_Id = "";

		TransaksiCashBankFactory.getDataTipeTransaksiCashBank()
			.then(
				function (res) {
					console.log("tipe transaksi cash bank res.data : ", res.data);
					$scope.TambahTransaksiCashBank_TipeTransaksiCashBankOptions = res.data;
				}
			);

		$scope.TambahTransaksiCashBank_TipeTransaksiCashBank_Selected_Changed = function () {
			TransaksiCashBankFactory.getDataNomorGL(1)
				.then(
					function (res) {
						var kodeGLAccountHO = [];
						angular.forEach(res.data.Result, function (value, key) {
							kodeGLAccountHO.push({ name: value.GLAccountName, value: { "GLAccountId": value.GLAccountId, "GLAccountName": value.GLAccountCode } });
						});
						console.log("kodeGLAccountHOOption")
						$scope.TambahTransaksiCashBank_kodeGLAccountHOOptions = kodeGLAccountHO;
					}
				);


			TransaksiCashBankFactory.getDataNomorGL(-2)
				.then(
					function (res) {
						var kodeGLAccountCabang = [];
						angular.forEach(res.data.Result, function (value, key) {
							kodeGLAccountCabang.push({ name: value.GLAccountName, value: { "GLAccountId": value.GLAccountId, "GLAccountName": value.GLAccountCode } });
						});
						console.log("kodeGLAccountcabangOption")
						$scope.TambahTransaksiCashBank_kodeGLAccountCabangOptions = kodeGLAccountCabang;
					}
				);
		}

		$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO_Selected_Changed = function () {
			//$scope.TambahTransaksiCashBank_KodeGlAccountHo = $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO;
			console.log("masuk changed");
			console.log("is undefined", angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO));
			if (!angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO)) {
				$scope.TambahTransaksiCashBank_KodeGlAccountHo = $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO.GLAccountName;
			}
		}

		$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang_Selected_Changed = function () {
			//$scope.TambahTransaksiCashBank_KodeGlAccountCabang = $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang;
			if (!angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang)) {
				$scope.TambahTransaksiCashBank_KodeGlAccountCabang = $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang.GLAccountName;
			}
		}

		$scope.TambahTransaksiCashBank_CheckFields = function () {
			var isPassed = false;
			var message;
			if (angular.isUndefined($scope.TambahTransaksiCashBank_TanggalTransaksiCashBank) || $scope.TambahTransaksiCashBank_TanggalTransaksiCashBank == "")
				message = ("Tanggal transaksi cash bank masih kosong");
			else if (angular.isUndefined($scope.TambahTransaksiCashBank_TipeTransaksiCashBank) || $scope.TambahTransaksiCashBank_TanggalTransaksiCashBank == [])
				message = ("Tipe Transaksi Cash Bank Harus diisi");
			else if (angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO) || $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO == [])
				message = ("Kode GL Account HO harus dipilih");
			else if (angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang) || $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang == [])
				message = ("Kode GL Account Cabang harus dipilih");
			else if (angular.isUndefined($scope.TambahTransaksiCashBank_Nominal) || $scope.TambahTransaksiCashBank_Nominal <= 0)
				message = ("Nominal tidak boleh negatif dan harus lebih besar dari 0");
			else
				isPassed = true;
			bsNotify.show({
				title: "Warning",
				content: message,
				type: "warning"
			});
			return (isPassed);
		}

		$scope.TambahTransaksiCashBank_Simpan_Button_Clicked = function () {
			// if (!$scope.TambahTransaksiCashBank_CheckFields())
			// 	return;
			$scope.ProsesSubmit = true;
			if (!angular.isUndefined($scope.TambahTransaksiCashBank_TanggalTransaksiCashBank)
				&& !angular.isUndefined($scope.TambahTransaksiCashBank_TipeTransaksiCashBank)
				&& !angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO)
				&& !angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang)
				&& !angular.isUndefined($scope.TambahTransaksiCashBank_Nominal)
				&& !($scope.TambahTransaksiCashBank_Nominal > 999999999999999)
			) {
				var TambahTransaksiCashBank_SubmitData = [{
					TranDate: $filter('date')(new Date($scope.TambahTransaksiCashBank_TanggalTransaksiCashBank), "yyyy-MM-dd"),
					TranTypeId: $scope.TambahTransaksiCashBank_TipeTransaksiCashBank,
					GLHONumber: $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO.GLAccountId,
					GLBranchNumber: $scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang.GLAccountId,
					Nominal: $scope.TambahTransaksiCashBank_Nominal,
					Description: $scope.TambahTransaksiCashBank_Keterangan,
					CurrencyCode: "IDR"
				}];
				console.log("Data ==>", TambahTransaksiCashBank_SubmitData);
				TransaksiCashBankFactory.createCashBankData(TambahTransaksiCashBank_SubmitData)
					.then(
						function (res) {
							//TambahTransaksiCashBank_Simpan_Clicked();
							$scope.TambahTransaksiCashBank_TanggalTransaksiCashBank = "";
							$scope.TambahTransaksiCashBank_TipeTransaksiCashBank = [{}];
							$scope.TambahTransaksiCashBank_KodeGLAccountHo = "";
							$scope.TambahTransaksiCashBank_KodeGLAccountCabang = "";
							$scope.TambahTransaksiCashBank_Nominal = "";
							$scope.TambahTransaksiCashBank_Keterangan = "";
							$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO = [{}];
							$scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang = [{}];
							//alert('data berhasil disimpan');
							bsNotify.show({
								title: "Berhasil",
								content: "Data berhasil disimpan",
								type: "success"
							});
							$scope.ProsesSubmit = true;
							$scope.ShowTambahTransaksiCashBank = false;
							$scope.ShowLihatTransaksiCashBank = true;
							$scope.TransaksiCashBank_UIGrid_Paging(1);
						},
						function (err) {
							console.log("err=>", err);
							if (err != null) {
								//alert(err.data.Message);
								bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: "danger"
								});
								$scope.ProsesSubmit = false;
							}
						}
					)
			}
			else {
				var Message = "Silakan lengkapi data \n";
				if (angular.isUndefined($scope.TambahTransaksiCashBank_TanggalTransaksiCashBank))
					Message += "Tanggal belum di isi.\n";
				if (angular.isUndefined($scope.TambahTransaksiCashBank_TipeTransaksiCashBank))
					Message += "Tipe Transaksi belum di isi.\n";
				if (angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountHO))
					Message += "Nama GL Account HO belum di isi.\n";
				if (angular.isUndefined($scope.TambahTransaksiCashBank_KodeGLAccount_GLAccountCabang))
					Message += "Nama GL Account Cabang belum di isi.\n";
				if (angular.isUndefined($scope.TambahTransaksiCashBank_Nominal))
					Message += "Nominal belum di isi.\n";
				if ($scope.TambahTransaksiCashBank_Nominal > 999999999999999)
					Message += "Nominal maksimal 999,999,999,999,999\n";
				bsNotify.show({
					title: "Warning",
					content: Message,
					type: "warning"
				});
				$scope.ProsesSubmit = false;
				//alert(Message);
			}
		}

		var uiGridPageSize = 10;

		$scope.TransaksiCashBank_UIGrid = {};
		$scope.LihatTransaksiCashBank_TanggalTransaksiCashBank = new Date();
		$scope.LihatTransaksiCashBank_TanggalTransaksiCashBank.setHours(0, 0, 0, 0);

		$scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir = new Date();
		$scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir.setHours(0, 0, 0, 0);

		$scope.TransaksiCashBank_UIGrid_Paging = function (pageNumber) {

			if ((angular.isUndefined($scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir) == false)
				&& (new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir) > new Date())) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if ((angular.isUndefined($scope.LihatTransaksiCashBank_TanggalTransaksiCashBank) == false)
				&& (new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBank) > new Date())) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
				});
			}
			else if ((angular.isUndefined($scope.LihatTransaksiCashBank_TanggalTransaksiCashBank) == false && angular.isUndefined($scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir) == false)
				&& (new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBank) > new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir))) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal awal tidak bisa lebih besar dari tanggal akhir.",
					type: "warning"
				});
			}
			else {
				var newString  =  "";
			if($scope.LihatTransaksiCashBank_TextSearch != '' && $scope.LihatTransaksiCashBank_TextSearch != undefined){
				newString = $scope.LihatTransaksiCashBank_TextSearch.toString().replace(/\//g, '-').toString().replace(/\./g, '-')
			}else{
				newString = $scope.LihatTransaksiCashBank_TextSearch;
			}
			console.log("replace =>>", newString);
				TransaksiCashBankFactory.getDataCashBank(pageNumber, paginationOptions_TransaksiCashBank_UIGrid.pageSize,
					$filter('date')(new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBank), 'yyyy-MM-dd'),
					$filter('date')(new Date($scope.LihatTransaksiCashBank_TanggalTransaksiCashBankAkhir), 'yyyy-MM-dd'),newString, $scope.LihatTransaksiCashBank_SearchCriteria)
					.then(
						function (res) {
							console.log("Get data cash bank", res.data.Result);
							$scope.TransaksiCashBank_UIGrid.data = [];
							angular.forEach(res.data.Result, function (value, key) {

								var crtDate = new Date(value.CreateDate).getFullYear();
								if (crtDate == 1) {
									value.CreateDate = '';
								}
								
								if (value.ApprovalStatus == "") {
									value.CreateDate = '';
								}

								var obj =
									{
										"CashBankId": value.CashBankId,
										"LastApprovalId": value.LastApprovalId,
										"GLBranch": value.GLBranch,
										"GLHO": value.GLHO,
										"GLHOCode": value.GLHOCode,
										"GLHOName": value.GLHOName,
										"GLBranchCode": value.GLBranchCode,
										"GLBranchName": value.GLBranchName,
										"TranType": value.TranType,
										"TranDate": value.TranDate,
										"CashBankNo": value.CashBankNo,
										"Label": value.Label,
										"Nominal": value.Nominal,
										"ApprovalType": value.ApprovalType,
										"CreateDate": value.CreateDate,
										"ApprovalStatus": value.ApprovalStatus,
										"Description": value.Description,
										"ReversalReason": value.ReversalReason,
										"RejectedReaason": value.RejectedReason,
										"ReverseNo": value.ReverseNo,										
									};
								console.log("Object of Id ==>", obj);
								console.log("Object of Value ==>", value);
								$scope.TransaksiCashBank_UIGrid.data.push(obj);
							});

							// $scope.TransaksiCashBank_UIGrid.data = res.data.Result;
							$scope.TransaksiCashBank_UIGrid.totalItems = res.data.Total;
							$scope.TransaksiCashBank_UIGrid.paginationPageSize = paginationOptions_TransaksiCashBank_UIGrid.pageSize;
							$scope.TransaksiCashBank_UIGrid.paginationPageSizes = [10, 25, 50];
							$scope.TransaksiCashBank_UIGrid.paginationCurrentPage = pageNumber;

						}
					);
			}
		}

		$scope.LihatTransaksiCashBank_GetDataGL = function () {

			TransaksiCashBankFactory.getDataNomorGL(1)
				.then(
					function (res) {
						var kodeGLAccountHO = [];
						angular.forEach(res.data.Result, function (value, key) {
							kodeGLAccountHO.push({ name: value.GLAccountName, value: value.GLAccountId });
						});
						console.log("kodeGLAccountHOOption")
						$scope.LihatTransaksiCashBank_kodeGLAccountHOOptions = kodeGLAccountHO;
					}
				);

			TransaksiCashBankFactory.getDataNomorGL(-2)
				.then(
					function (res) {
						console.log(res.data.Result);
						var kodeGLAccountCabang = [];
						angular.forEach(res.data.Result, function (value, key) {
							kodeGLAccountCabang.push({ name: value.GLAccountName, value: value.GLAccountId });
						});
						console.log("kodeGLAccountcabangOption")
						$scope.LihatTransaksiCashBank_kodeGLAccountCabangOptions = kodeGLAccountCabang;
					}
				);
		}

		$scope.LihatTransaksiCashBank_GetDataGL();

		$scope.TransaksiCashBank_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			enableColumnResizing: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "CashBankId", field: "CashBankId", visible: false },
				{ name: "LastApprovalId", field: "LastApprovalId", visible: false },
				{ name: "GLBranch", field: "GLBranch", visible: false },
				{ name: "GLHO", field: "GLHO", visible: false },
				{ name: "GLHOCode", field: "GLHOCode", visible: false },
				{ name: "GLHOName", field: "GLHOName", visible: false },
				{ name: "GLBranchCode", field: "GLBranchCode", visible: false },
				{ name: "GLBranchName", field: "GLBranchName", visible: false },
				{ name: "TranType", field: "TranType", visible: false },
				{ name: "Tanggal Transaksi Cash & Bank", field: "TranDate", cellFilter: 'date:\"dd/MM/yyyy\"', width: 180 },
				{
					name: "Nomor Transaksi Cash & Bank", field: "CashBankNo", width: 180,
					cellTemplate: '<a ng-click="grid.appScope.ShowLihatTransaksiCashBankDetailById(row)">{{row.entity.CashBankNo}}</a>'
				},
				{ name: "Tipe Transaksi Cash & Bank", field: "Label", visible: 'true', width: 180 },
				{ name: "Nominal", field: "Nominal", visible: 'true', width: 180, cellFilter: 'rupiahC' },
				{ name: "Tipe Pengajuan", field: "ApprovalType", visible: 'true', width: 180 },
				{ name: "Tanggal Pengajuan", field: "CreateDate", visible: 'true', cellFilter: 'date:\"dd/MM/yyyy\"', width: 180 },
				{ name: "Status Pengajuan", field: "ApprovalStatus", visible: 'true', width: 180 },
				{ name: "Description", field: "Description", visible: false },
				{ name: "Alasan Pengajuan", field: "ReversalReason", visible: 'true', width: 180 },
				{ name: "Alasan Penolakan", field: "RejectedReason", visible: 'true', width: 180 },
				{ name: "ReverseNo", field: "ReverseNo", visible: 'false',  width: 180 },
			],
			onRegisterApi: function (gridApi) {
				$scope.GridApiTransaksiCashBank = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					paginationOptions_TransaksiCashBank_UIGrid.pageSize = pageSize;
					paginationOptions_TransaksiCashBank_UIGrid.pageNumber = pageNumber;
					$scope.TransaksiCashBank_UIGrid.data = $scope.TransaksiCashBank_UIGrid_Paging(pageNumber);
					$scope.TransaksiCashBank_UIGrid.pageSize = pageSize;
					$scope.TransaksiCashBank_UIGrid.pageNumber = pageNumber;
					$scope.TransaksiCashBank_UIGrid.paginationCurrentPage = pageNumber;
					$scope.TransaksiCashBank_UIGrid.paginationPageSizes = [10, 25, 50];
				}
				);
				/*
				gridApi.selection.on.rowSelectionChanged($scope,function(row){
					TransaksiCashBankFactory.getDataCashBankDetail(row.entity.CashBankNo)
					.then (
						function(res){
							$scope.TransaksiCashBank_Selected_Id = row.entity.CashBankNo;
							
							for(var TipeTransaksiCashBankItem in $scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions){
								if($scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions[TipeTransaksiCashBankItem].value == res.data[0].TranTypeId)
								{
									$scope.LihatTransaksiCashBank_TipeTransaksiCashBank = $scope.LihatTransaksiCashBank_TipeTransaksiCashBankOptions[TipeTransaksiCashBankItem];
								}
							}
							
							for(var GLAccountHOItem in $scope.LihatTransaksiCashBank_kodeGLAccountHOOptions){
								if($scope.LihatTransaksiCashBank_kodeGLAccountHOOptions[GLAccountHOItem].value == res.data[0].GLHONumber)
								{
									$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountHO = $scope.LihatTransaksiCashBank_kodeGLAccountHOOptions[GLAccountHOItem];
								}
							}
							
							for(var GLAccountCabangItem in $scope.LihatTransaksiCashBank_kodeGLAccountCabangOptions){
								if($scope.LihatTransaksiCashBank_kodeGLAccountCabangOptions[GLAccountCabangItem].value == res.data[0].GLBranchNumber)
								{
									$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountCabang = $scope.LihatTransaksiCashBank_kodeGLAccountCabangOptions[GLAccountCabangItem];
								}
							}
							
							$scope.LihatTransaksiCashBank_KodeGlAccountHo = res.data[0].GLHONumber;
							$scope.LihatTransaksiCashBank_KodeGlAccountCabang = res.data[0].GLBranchNumber;
							$scope.LihatTransaksiCashBank_Nominal = res.data[0].Nominal;
							$scope.LihatTransaksiCashBank_Keterangan = res.data[0].Description;
						}
					);
				}
				);*/
			}
		};

		// $scope.MainGridFilterClick = function () {
		// 	$scope.GridApiTransaksiCashBank.grid.clearAllFilters();
		// 	$scope.GridApiTransaksiCashBank.grid.refresh();
		// 	$scope.GridApiTransaksiCashBank.grid.getColumn($scope.LihatTransaksiCashBank_SearchCriteria).filters[0].term = $scope.LihatTransaksiCashBank_TextSearch;
		// 	$scope.GridApiTransaksiCashBank.grid.refresh();
		// }

		// NEW FILTER =====================
		$scope.MainGridFilterClick = function () {
			$scope.TransaksiCashBank_UIGrid_Paging(1);
		}

		$scope.Pengajuan_LihatTransaksiCashBank_PengajuanReversalModal_Clicked = function () {
			var inputDataReversal = [
				{
					"ApprovalTypeName": "Reversal Cash Bank",
					"DocumentId": $scope.LihatTransaksiCashBank_CashBankId,
					"ReversalReason": $scope.LihatTransaksiCashBank_PengajuanReversal_AlasanReversal,
					"LastApprovalId": $scope.LihatTransaksiCashBank_LastApprovalId,
				}
			];
			console.log("Data reversal : ", inputDataReversal);
			TransaksiCashBankFactory.createCashBankReversal(inputDataReversal)
				.then(
					function (res) {
						//Pengajuan_LihatTransaksiCashBank_PengajuanReversalModal();
						/*$scope.LihatTransaksiCashBank_Keterangan = "";
						$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountCabang = [{}];
						$scope.LihatTransaksiCashBank_KodeGLAccount_GLAccountHO = [{}];
						$scope.LihatTransaksiCashBank_KodeGlAccountHo = "";
						$scope.LihatTransaksiCashBank_KodeGlAccountCabang = "";
						$scope.LihatTransaksiCashBank_Nominal = "";
						$scope.LihatTransaksiCashBank_TipeTransaksiCashBank = [{}];
						$scope.LihatTransaksiCashBank_CashBankId = "";
						$scope.GridApiTransaksiCashBank.selection.clearSelectedRows();*/
						//alert('Reversal telah diajukan');
						bsNotify.show({
							title: "Berhasil",
							content: "Reversal telah diajukan.",
							type: "success"
						});
						$scope.LihatTransaksiCashBank_Details = false;
						$scope.ShowLihatTransaksiCashBank = true;
						$scope.LihatTransaksiCashBank_Cari();
					},
					function (err) {
						console.log("Err", err.data.Message);
						//alert("Error : " + err.data.Message);
						bsNotify.show({
							title: "Error",
							content: "Error : " + err.data.Message,
							type: "danger"
						});
					}

				);
			$scope.LihatTransaksiCashBank_PengajuanReversal_AlasanReversal = '';
			angular.element('#ModalLihatTransaksiCashBank_PengajuanReversal').modal('hide');
		}
		//======================Devokus Edit, END=======================//
	});

app.filter('rupiahC', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
}); 