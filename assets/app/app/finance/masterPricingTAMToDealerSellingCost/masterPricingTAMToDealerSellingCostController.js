var app = angular.module('app');
app.controller('MasterPricingTAMToDealerSellingCostController', function ($scope, $http, $filter, CurrentUser, MasterPricingTAMToDealerSellingCostFactory, $timeout) {
	$scope.optionsKolomFilterSatu_SellingTAMCost = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_SellingTAMCost = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_SellingTAMCost = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_SellingTAMCost = [
			{name:"Dealer Name", value:"DealerName"},
			{name:"Province", value:"Province"}, {name:"Price Area", value:"PriceArea"}, 
			{name:"Delivery Cost", value:"DeliveryCost"},
			{name:"Adm Cost", value:"AdmCost"},
			{name:"Inventory Cost", value:"InventoryCost"},
			{name:"Effective Date From", value:"EffectiveDateFrom"},
			{name:"Effective Date To", value:"EffectiveDateTo"}, {name:"Remarks", value:"Remarks"}
	];
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'SellingCost';
	angular.element('#SellingTAMCostModal').modal('hide');
	$scope.TypePE = 1; //0 = dealer to branch , 1 = tam to dealer
	$scope.cekKolom = '';
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_SellingTAMCost = false;
$scope.cekKolom = '';
	$scope.MasterSellingPriceFU_SellingTAMCost_grid = {
		paginationPageSize : 25,
		paginationPageSizes: [25,50,75],	
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'DealerType', displayName: 'Dealer Type', field: 'DealerType',enableCellEdit: false, enableHiding: false },		
			{ name: 'DealerName', displayName: 'Dealer Name', field: 'DealerName',enableCellEdit: false , enableHiding: false},			
			{ name: 'Province', displayName: 'Province', field: 'Province',enableCellEdit: false, enableHiding: false },
			{ name: 'PriceArea', displayName: 'Price Area', field: 'PriceArea',enableCellEdit: false , enableHiding: false},
			{ name: 'DeliveryCost', displayName: 'Delivery Cost', field: 'DeliveryCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"  },	
			{ name: 'InventoryCost', displayName: 'Inventory Cost', field: 'InventoryCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"  },
			{ name: 'AdmCost', displayName: 'Adm Cost', field: 'AdmCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"  },
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date From', field: 'EffectiveDateFrom',enableCellEdit: false, enableHiding: false },
			{ name: 'EffectiveDateTo', displayName: 'Effective Date To', field: 'EffectiveDateTo',enableCellEdit: false , enableHiding: false},  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks',enableCellEdit: true , enableHiding: false}, 
			{ name: 'SellingCostId',displayName:'Id', field: 'SellingCostId', visible: false , enableHiding: false},		
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_SellingTAMCost' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				
				$scope.MasterSelling_SellingTAMCost_Upload_Clicked();
				myEl.val('');
            });
	}

	$scope.validateColumn = function(inputExcelData){
		// ini harus di rubah


		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.DeliveryCost)) {
			$scope.cekKolom = $scope.cekKolom + ' DeliveryCost \n';
		}
		
		if (angular.isUndefined(inputExcelData.DealerName)) {
			$scope.cekKolom = $scope.cekKolom + ' DealerName \n';
		}
		if 	(angular.isUndefined(inputExcelData.Province)){
			$scope.cekKolom = $scope.cekKolom + ' Province \n';
		}
		if (angular.isUndefined(inputExcelData.PriceArea)) {
			$scope.cekKolom = $scope.cekKolom + ' PriceArea \n';
		}
		
		if (angular.isUndefined(inputExcelData.InventoryCost)) {
			$scope.cekKolom = $scope.cekKolom + ' InventoryCost \n';
		}
		if 	(angular.isUndefined(inputExcelData.AdmCost)){
			$scope.cekKolom = $scope.cekKolom + ' AdmCost \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}

		if ( angular.isUndefined(inputExcelData.DeliveryCost) || 
			angular.isUndefined(inputExcelData.DealerName) ||
			angular.isUndefined(inputExcelData.DealerType) ||
			angular.isUndefined(inputExcelData.Province) ||
			angular.isUndefined(inputExcelData.PriceArea) ||
			angular.isUndefined(inputExcelData.InventoryCost) ||
			angular.isUndefined(inputExcelData.AdmCost) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.ComboBulkAction_SellingTAMCost_Changed = function()
	{
		if ($scope.ComboBulkAction_SellingTAMCost == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_BBN_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_BBN_grid.data.splice(index, 1);
			// });
			// var counter = 0;
			// angular.forEach($scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows(), function (data, index) {
			// 	//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
			// 	counter++;
			// });

			if ($scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.selection.getSelectedCount() > 0) {
				$scope.TotalSelectedData = $scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.selection.getSelectedCount();
				angular.element('#SellingTAMCostModal').modal('show');
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_SellingTAMCost_grid.data.splice($scope.MasterSellingPriceFU_SellingTAMCost_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_SellingTAMCost = "";
		angular.element('#SellingTAMCostModal').modal('hide');
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_SellingTAMCost = "";
		angular.element('#SellingTAMCostModal').modal('hide');
	}		

	$scope.MasterSelling_SellingTAMCost_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingTAMCost";

		if ($scope.MasterSellingPriceFU_SellingTAMCost_grid.data.length == 0){
				excelData.push({ 
						DealerType : "",
						DealerName : "",
						Province: "",
						PriceArea: "",
						DeliveryCost : "",
						InventoryCost : "",
						AdmCost : "",
						EffectiveDateFrom: "",
						EffectiveDateTo: "",
						Remarks: "" });
			fileName = "MasterSellingTAMCostTemplate";		
		}
		else {
		//$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_SellingTAMCost_grid.data.forEach(function(row) {
				excelData.push({ 
						DealerType : row.DealerType,
						DealerName : row.DealerName,
						Province: row.Province,
						PriceArea: row.PriceArea,
						DeliveryCost : row.DeliveryCost,
						InventoryCost : row.InventoryCost,
						AdmCost : row.AdmCost,
						EffectiveDateFrom: row.EffectiveDateFrom,
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks:row.Remarks });
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_SellingTAMCost_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.grid.clearAllFilters();
		MasterPricingTAMToDealerSellingCostFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
//				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_SellingTAMCost_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_SellingTAMCost_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_SellingTAMCost_grid.data);		
		MasterPricingTAMToDealerSellingCostFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_SellingTAMCost_grid.data = [];
		$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_SellingTAMCost = "";
		$scope.TextFilterSatu_SellingTAMCost = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_SellingTAMCost' ) );
		myEl.val('');		
	}

	$scope.MasterSelling_SellingTAMCost_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_SellingTAMCost_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_SellingTAMCost_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_SellingTAMCost_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_SellingTAMCost_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_SellingTAMCost_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_SellingTAMCost_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_SellingTAMCost,
			Text1 : $scope.TextFilterSatu_SellingTAMCost,
			AndOr : $scope.Filter_SellingTAMCost_AndOr,
			Filter2 : $scope.KolomFilterDua_SellingTAMCost,
			Text2 : $scope.TextFilterDua_SellingTAMCost,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingTAMToDealerSellingCostFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_SellingTAMCost_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_SellingTAMCost_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_SellingTAMCost_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_SellingTAMCost;
		$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_SellingTAMCost != "") {
			$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.grid.getColumn($scope.ComboFilterGrid_SellingTAMCost).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_SellingTAMCost_gridAPI.grid.clearAllFilters();

	}

});