angular.module('app')
  .factory('InstruksiPembayaranFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user();
    var factory = {};
    var debugMode = true;

    factory.getDataAnggaranBelanja = function () {
      var url = '/api/fe/FinanceBudget/?start=1&limit=100';
      var res = $http.get(url);
      return res;
    };

    factory.IsAnggaran = function () {
      var url = '/api/fe/financeisbudget/getdata/?OutletId=' + currentUser.OutletId;
      console.log("url excess difference : ", url);
      var res = $http.get(url);
      return res;
    };

    factory.getDataTransactionType = function () {
      var url = '/api/fe/FinanceParam/TipeTransaksiIP';
      var res = $http.get(url);
      return res;
    };

    factory.getDataForRefund = function (start, limit, FilterData) {
      // console.log('isi data getDataForRefund', JSON.stringify(FilterData));
      console.log('isi data getDataForRefund', FilterData);
      var res = $http.get('/api/fe/PaymentInstructionRefund/SelectData?start=' + start + '&limit='+ limit+ '&filterData=' + JSON.stringify(FilterData));
      //var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData/Start/' + start + '/Limit/' + limit + '/filterData=' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };
    
    // Get Filter
    factory.getDataForRefundFilter = function (start, limit, FilterData) {
      // console.log('isi data getDataForRefundFilter', JSON.stringify(FilterData));
      console.log('isi data getDataForRefundFilter', FilterData);
      var res = $http.get('/api/fe/PaymentInstructionRefundNew/SelectData?start=' + start + '&limit='+ limit+ '&filterData=' + JSON.stringify(FilterData));
      //var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData/Start/' + start + '/Limit/' + limit + '/filterData=' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    // ============================ BEGIN | NOTO | 190409 | Refund - Unknown [5] | Service Incoming Unknown Detail ============================
    factory.GetListRincianPembayaranUnknown = function (start, limit, IPParentId) {
      var url = '/api/fe/FinanceIncomingPayments/GetListRincianPembayaran/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
      console.log("execute GetListRincianPembayaran");
      return $http.get(url);
    }
    // ============================ END | NOTO | 190409 | Refund - Unknown [5] | Service Incoming Unknown Detail ============================

    factory.cetakBPHCash = function (pnPaymentId, userId, pnAPTypeId, outletId) {
      var url = '/api/fe/FFinanceRptPrintBPHCash/Cetak?pnPaymentId=' + pnPaymentId + '&userId=' + userId + '&pnAPTypeId=' + pnAPTypeId + '&outletId=' + outletId;
      var res = $http.get(url, { responseType: 'arraybuffer' });

      return res;
    }

    factory.getStatusBungaDF = function (outletId, pitype) {
      var url = '/api/fe/PaymentInstruction/BungaDF?outletId=' + outletId + '&PIType=' + pitype;
      //var url = '/api/fe/PaymentInstruction/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(inputData);
      var res = $http.get(url);

      return res;
    }

    factory.cetakBPHBankTransfer = function (pnPaymentId, userId, pnAPTypeId, outletId) {
      var url = '/api/fe/FFinanceRptPrintBPH/Cetak?pnPaymentId=' + pnPaymentId + '&userId=' + userId + '&pnAPTypeId=' + pnAPTypeId + '&outletId=' + outletId;
      var res = $http.get(url, { responseType: 'arraybuffer' });

      return res;
    }

    factory.getDataForAccruedFreeService = function (start, limit, Id, Code, Type, SearchText, SearchType, outletId) {
      var FilterData = [{ LookupId: Id, Mode: Type, LookUpCode: Code + SearchText + "|" + SearchType + "|" + outletId}];
      var url = '/api/fe/PaymentInstructionAccruedFreeService/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData);
      console.log("url getDtaForAccruedFreeService: ", url);
      var res = $http.get(url);
      //var res=$http.get('/api/fe/PaymentInstructionAccruedFreeService/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    factory.getDataForAccruedFrame = function (start, limit, Id, Code, Type, PIId) {
      var FilterData = [{ LookupId: Id, Mode: Type, LookUpCode: Code, PIId: PIId }];
      var url = '/api/fe/PaymentInstructionFrame/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData);
      console.log("url getDataForAccruedFrame: ", url);
      var res = $http.get(url);
      // var res=$http.get('/api/fe/PaymentInstructionFrame/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    factory.getDataForAccrued = function (start, limit, Id, Code, Type) {
      var FilterData = [{ LookupId: Id, Mode: Type, LookUpCode: Code }];
      //var res=$http.get('/api/fe/PaymentInstructionAccrued/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
      var res = $http.get('/api/fe/PaymentInstructionAccrued/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    factory.getVendorInfo = function (tipe, name, id) {
      var FilterData = [{ PIType: tipe, VendorName: name, VendorId: id, ShowAll: 1 }];
      var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
      var res = $http.get(url);
      console.log("getVendorInfo url : ", url);
      //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    factory.getVendorInfoDetail = function (tipe, name, id, outletId) {
      var FilterData = [{ PIType: tipe, VendorName: name, VendorId: id, ShowAll: 0, OutletId: outletId}];
      var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
      var res = $http.get(url);
      console.log("getVendorInfo url : ", url);
      //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
      //console.log('res=>',res);
      return res;
    };

    factory.getBiayaMaterai = function (amount) {
      //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
      var res = $http.get('/api/fe/ParamBeaMateraiElektronik/GetNominal?nominal=' + amount);
      //console.log('res=>',res);
      return res;
    }

    factory.getSOVendorList = function (tipe, name, Id) {
      var FilterData = [{ PIType: tipe, VendorName: name, ShowAll: 0, OutletId: Id }];
      //var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
      var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
      console.log("url getSOVendorList", url);
      var res = $http.get(url);
      //console.log('res=>',res);
      return res;
    };

    factory.getIncoiveTaxList = function (start, limit, Id, Type) {
      var data = [{ ReffId: Id, TaxType: Type }];
      //var res=$http.get('/api/fe/PaymentInstructionTax/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data));
      var res = $http.get('/api/fe/PaymentInstructionTax/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data));
      return res;
    };

    factory.getDataInvoiceList = function (start, limit, Id, Type, DocId, Action) {
      var Data = [{ VendorId: Id, PIType: Type, PIId: DocId }];
      if((Type == 'Unit' || Type == 'Parts') && Action == 'Lihat'){ //CR4 P2 Finance BUngaDF Start
        var res = $http.get('/api/fe/PaymentInstructionInvoice/LihatData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(Data));
      }else{//CR4 P2 Finance BUngaDF Start
        var res = $http.get('/api/fe/PaymentInstructionInvoice/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(Data));
      }
      //var res=$http.get('/api/fe/PaymentInstructionInvoice/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(Data));
      return res;
    };

    factory.getDataInterbranch = function (start, limit, Id, Type, DocId) {
      var Data = [{ VendorId: Id, PIType: Type, PIId: DocId }];
      var res = $http.get('/api/fe/PaymentInstructionInvoice/InterBranch?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(Data));
      //var res=$http.get('/api/fe/PaymentInstructionInvoice/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(Data));
      return res;
    };

    //utk main approval
    factory.getDataForMainGrid = function (start, limit, FilterData, vSearchText, vSearchType, newOutletId) {
      //var url = '/api/fe/PaymentInstructionApproval/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + FilterData;
      var url = '/api/fe/PaymentInstructionApproval/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + FilterData + '&SearchText=' + vSearchText + '&SearchType=' + vSearchType +'&newOutletId='+ newOutletId;
      var res = $http.get(url);
      return res;
    };

    factory.getDataBranch = function (orgId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
      var res = $http.get(url);
      return res;
    };

    factory.getAdvancePaymentList = function (start, limit, FilterData, TVendor, VIdName) {
      var data = [{ Type: FilterData, VendorType: TVendor, VendorIdName: VIdName }];
      //var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data) );
      var res = $http.get('/api/fe/PaymentInstructionAdvance/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data));
      //console.log('res=>',res);
      return res;
    };

    //new for GT
    factory.getAdvancePaymentListGT = function (start, limit, FilterData, TVendor, VIdName, PIId) {
      var data = [{ Type: FilterData, VendorType: TVendor, VendorIdName: VIdName, PaymentId: PIId }];
      //var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectDataGT/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data) );
      var res = $http.get('/api/fe/PaymentInstructionAdvance/SelectDataGT?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data));
      //console.log('res=>',res);
      return res;
    };

    //new get Advance payment
    factory.getAdvancePaymentListNew = function (start, limit, FilterData, TVendor, VIdName, InvoiceId) {
      var data = [{ Type: FilterData, VendorType: TVendor, VendorIdName: VIdName, InvoiceId: InvoiceId }];
      //var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data) );
      var res = $http.get('/api/fe/PaymentInstructionAdvance/SelectDataInv?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data));
      //console.log('res=>',res);
      return res;
    };

    factory.getMasterAdvancePayment = function (start, limit, FilterData, id) {
      var data = [{ Type: FilterData }];
      if (debugMode) { console.log('Masuk ke getMasterAdvancePayment') };
      if (debugMode) { console.log('Parameter :' + JSON.stringify(data) + ' , Id : ' + id) };
      //var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data) );
      var res = $http.get('/api/fe/PaymentInstructionAdvance/ViewData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data) + '&Id=' + id);
      //console.log('res=>',res);
      return res;
    };

    factory.updateApprovalData = function (inputData) {
      var url = '/api/fe/financeapproval/approverejectdata/';
      var param = JSON.stringify(inputData);
      console.log("object input update approval ", param);
      var res = $http.put(url, param);
      return res;
    };

    factory.CheckValidReversal = function (id, outletid) {
      if (debugMode) { console.log('Parameter Reversal : id' + id + ' outlet : ' + outletid) };
      if (outletid == undefined) {
        outletid = 0;
      }

      var res = $http.get('/api/fe/PaymentInstruction/CheckReversal?id=' + id + '&outletid=' + outletid);
      //var res=$http.get('/api/fe/IncomingInstruction/id/' + id);
      return res;
    }

    factory.getMasterLihat = function (start, limit, id, outlet) {
      var inputData = [{ PIId: id, OutletId: outlet }];
      var url = '/api/fe/PaymentInstruction/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(inputData);
      //var url = '/api/fe/PaymentInstruction/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(inputData);
      var res = $http.get(url);

      return res;
    };

    factory.getMasterLihatCostCenter = function (start, limit, id) {
      var url = '/api/fe/PaymentInstructionCostCenter/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + id;
      //var url = '/api/fe/PaymentInstructionCostCenter/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + id;
      var res = $http.get(url);

      return res;
    };

    // factory.getOtherInfoKomisi = function (start, limit, SoId, KTP) {
    //   var filter = [{ SoId: SoId, KTP: KTP }];
    //   var url = '/api/fe/PaymentInstructionCommision/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
    //   //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
    //   var res = $http.get(url);

    //   return res;
    // };
    // Komisi Sales By SO
    factory.getOtherInfoKomisi = function (start, limit, SoNumber, KTP) {
      var filter = [{ SoNumber: SoNumber, KTP: KTP}];
      var url = '/api/fe/PaymentInstructionCommisionBySO/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
      //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
      var res = $http.get(url);

      return res;
    };

    factory.getOtherInfoKomisiNew = function (start, limit, SoNumber, KTP, Mode, PIId) {
      var filter = [{ SoNumber: SoNumber, KTP: KTP, Mode: Mode, PIId: PIId }];
      var url = '/api/fe/PaymentInstructionCommisionBySO/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
      //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
      var res = $http.get(url);

      return res;
    };

    factory.getReOtherInfoKomisi = function (start, limit, SoId, KTP, AkumulasiDPP) {
      var filter = [{ SoId: SoId, KTP: KTP, AkumulasiDPP: AkumulasiDPP }];
      var url = '/api/fe/PaymentInstructionReCommision/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
      //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
      var res = $http.get(url);

      return res;
    };

    // New refresh 
    // factory.getReOtherInfoKomisiRe = function (start, limit, SoNumber, KTP, AkumulasiDPP) {
    //   var filter = [{ SoNumber: SoNumber, KTP: KTP, AkumulasiDPP: AkumulasiDPP }];
    //   var url = '/api/fe/PaymentInstructionReCommisionRe/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
    //   //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
    //   var res = $http.get(url);

    //   return res;
    // };

    factory.getMasterLihatCharges = function (start, limit, id) {
      var url = '/api/fe/PaymentInstructionCharges/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + id;
      //var url = '/api/fe/PaymentInstructionCharges/SelectData/start/' + start + '/Limit/' + limit + '/FilterData/' + id;
      var res = $http.get(url);

      return res;
    };

    factory.submitDataInvoice = function (data) {
      var url = '/api/fe/PaymentInstructionInvoice/SubmitData/';
      //var url = '/api/fe/PaymentInstructionInvoice';
      //alert('submit ' + JSON.stringify(data));
      if (debugMode) { console.log('Masuk ke submitDataInvoice') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };
      var res = $http.post(url, JSON.stringify(data));

      return res;
    };

    factory.SubmitInterbranch = function (data) {
      var url = '/api/fe/PaymentInstructionInvoice/SubmitInterbranch/';
      //var url = '/api/fe/PaymentInstructionInvoice';
      //alert('submit ' + JSON.stringify(data));
      if (debugMode) { console.log('Masuk ke SubmitInterbranch') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };
      var res = $http.post(url, JSON.stringify(data));

      return res;
    };

    factory.submitDataAccrued = function (data) {
      var url = '/api/fe/PaymentInstruction/SubmitData/';
      // var url = '/api/fe/PaymentInstruction';
      if (debugMode) { console.log('Masuk ke submitDataInvoice') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)); };
      var res = $http.post(url, data);

      return res;
    };

    factory.submitDataAccruedFS = function (data) {
      var url = '/api/fe/PaymentInstructionAccruedFreeService/SubmitData/';
      //var url = '/api/fe/PaymentInstructionAccruedFreeService';
      if (debugMode) { console.log('Masuk ke submitDataAccruedFS') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };
      var res = $http.post(url, JSON.stringify(data));

      return res;
    };

    factory.submitDataRefund = function (data) {
      console.log('submitDataRefund ===>',data)
      var url = '/api/fe/PaymentInstructionRefund/SubmitData/';
      //var url = '/api/fe/PaymentInstructionRefund';
      if (debugMode) { console.log('Masuk ke submitDataInvoice') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };
      var res = $http.post(url, JSON.stringify(data));

      return res;
    };

    factory.submitDataReversal = function (data) {
      var url = '/api/fe/FFinancePaymentInstructionReversal/SubmitData/';
      //var url = '/api/fe/FinanceApprovalSubmission';
      if (debugMode) { console.log('Masuk ke reverse') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };
      var res = $http.post(url, JSON.stringify(data));
      return res;
    };

    // factory.update= function(instruksiPembayaran){
    //   return $http.put('/api/fw/Role', [{
    //                                       Id: instruksiPembayaran.Id,
    //                                       //pid: negotiation.pid,
    //                                       Name: instruksiPembayaran.Name,
    //                                       Description: instruksiPembayaran.Description}]);
    // };

    // factory.delete= function(id) {
    //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
    // };

    factory.getGLList = function (Start, Limit, Filter) {
      //var url = '/api/fe/GeneralLedgers/SelectData/start/' + Start + '/Limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/GeneralLedgers/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
      if (debugMode) { console.log('Masuk ke getdata factory function '); };
      if (debugMode) { console.log('url :' + url); };
      return $http.get(url);
    }
    factory.getCostCenterList = function (Start, Limit, Filter) {
      //var url = '/api/fe/CostCenters/SelectData/start/' + Start + '/Limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/CostCenters/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
      if (debugMode) { console.log('Masuk ke getdata factory function '); };
      if (debugMode) { console.log('url :' + url); };
      return $http.get(url);
    }

    factory.getIncomingInvoiceList = function (Start, Limit, Filter) {
      //var url = '/api/fe/IncomingInvoice/SelectData/Start/' + Start + '/limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/IncomingInvoice/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
      if (debugMode) { console.log('Masuk ke getdata factory function '); };
      if (debugMode) { console.log('url :' + url); };
      return $http.get(url);
    }
    factory.getPajakList = function (Start, Limit, Filter) {
      //var url = '/api/fe/Pajak/SelectData/start/' + Start + '/limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/Pajak/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
      if (debugMode) { console.log('Masuk ke getdata factory function '); };
      if (debugMode) { console.log('url :' + url); };
      return $http.get(url);
    }

    factory.submitDataGeneralTransaction = function (data) {
      console.log('data pas di factoru ===>',data);
      var url = '/api/fe/PaymentInstruction/SubmitData/';
      //var url = '/api/fe/PaymentInstruction';
      if (debugMode) { console.log('Masuk ke submitData') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)); };
      var res = $http.post(url, JSON.stringify(data)); 

      return res;
    };

    // ============================ BEGIN | NOTO | 190410 | Bank & Rekening [1] | Service ============================
    factory.getDataAccountBank = function (AccountId) {
			console.log("getDataAccountBank");
			console.log(AccountId);
			var url = '/api/fe/AccountBank/GetDataRekening/' + AccountId;
			var res = $http.get(url);
			return res;
    }
    
    factory.getDataBankComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataBankComboBox/';
			if (debugMode) { console.log('Masuk ke getdata factory GetDataBankComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}
    
    factory.getDataBankAccountComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataComboBox/';
			var res = $http.get(url);
			return res;
    }

    //CR4 P2 Finance Start
    factory.getDataBankAccountUnitPartComboBox = function (VendorIdBungaDF) {
			var url = '/api/fe/FinanceVendor/GetDataComboBox?VendorIdBungaDF=' +VendorIdBungaDF;
			var res = $http.get(url);
			return res;
    }
    //CR4 P2 Finance End

    factory.getBillingFilter = function (Start, Limit, Filter) {
      var url = '/api/fe/PaymentInstructionAdminCorrection/?start=' + Start + '&limit=' + Limit + '&searchType=Billing&searchQuery=' + Filter;
      var res = $http.get(url);
      return res;
    };

    factory.getIncomingPaymentFilter = function (Start, Limit, Filter) {
      var url = '/api/fe/PaymentInstructionAdminCorrection/?start=' + Start + '&limit=' + Limit + '&searchType=IP&searchQuery=' + Filter;
      var res = $http.get(url);
      return res;
    };

    factory.getIncomingPaymentDetail = function (Start, Limit, Filter, PaymentId) {
      var url = '/api/fe/PaymentInstructionAdminCorrection/?start=' + Start + '&limit=' + Limit + '&searchType=IP&searchQuery=' + Filter + '&paymentId=' + PaymentId;
      var res = $http.get(url);
      return res;
    };

    factory.getDetailIncomingPaymentAndBillingFilter = function (Filter) {
        var url = '/api/fe/PaymentInstructionAdminCorrection/Billing?BillingId=' + Filter.BillingId+'&Module='+Filter.Module;
        var res = $http.get(url);
        return res;     
    };

    factory.getInformasiIncomingPaymentAndBillingFilter = function (ModuleType, Filter,mode) {
        var type = ModuleType == 'Parts' ? 'Part' : ModuleType;
        var tempUrl = type == 'Unit' ? '/api/fe/PaymentInstructionAdminCorrection/'+type+'?IPParentId=' + Filter.IPParentId + '&mode=' + mode :
        '/api/fe/PaymentInstructionAdminCorrection/'+type+'?IPParentId=' + Filter.IPParentId ;
        var url = tempUrl;
        var res = $http.get(url);
        return res; 
    };

    factory.SubmitKoreksiAdministratif = function (data) {
      var url = '/api/fe/PaymentInstructionAdminCorrection/';
      console.log('Submit', data)
      if (debugMode) { console.log('Masuk ke SubmitKoreksiAdministratif') };
      if (debugMode) { console.log('url :' + url); };
      if (debugMode) { console.log('Parameter POST :' + JSON.stringify(data)) };

      if((data[0].IsDraft == 1 && data[0].PaymentId == null) || data[0].IsDraft == 0 && data[0].PaymentId == null){
        var res = $http.post(url, JSON.stringify(data));
      }else{
        var res = $http.put(url, JSON.stringify(data));
      }
      return res;
    };
    // ============================ END | NOTO | 190410 | Bank & Rekening [1] | Service ============================

    return factory;
  });
