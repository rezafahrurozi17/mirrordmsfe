angular.module('app')
  .factory('DaftarIncomingPaymentFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
			  
		var da_json=[
				{ Nomor_Incoming_Payment: "no1",Tanggal_Incoming_Payment: new Date("2/14/2017"),Nominal_Selisih_Kelebihan_Pembayaran: 1000,Selected: false},
				{ Nomor_Incoming_Payment: "no2",Tanggal_Incoming_Payment: new Date("2/14/2017"),Nominal_Selisih_Kelebihan_Pembayaran: 1000,Selected: false},
				{ Nomor_Incoming_Payment: "no3",Tanggal_Incoming_Payment: new Date("2/14/2017"),Nominal_Selisih_Kelebihan_Pembayaran: 1000,Selected: false},
			  ];	  
		res=da_json;
		
        return res;
      },
      create: function(daftarIncomingPayment) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: daftarIncomingPayment.Name,
                                            Description: daftarIncomingPayment.Description}]);
      },
      update: function(daftarIncomingPayment){
        return $http.put('/api/fw/Role', [{
                                            Id: daftarIncomingPayment.Id,
                                            //pid: negotiation.pid,
                                            Name: daftarIncomingPayment.Name,
                                            Description: daftarIncomingPayment.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });