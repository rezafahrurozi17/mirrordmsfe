angular.module('app')
  .factory('DaftarTagihanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
			  
		var da_json=[
				{ Nomor_Invoice_Masuk: "no1",Tanggal_Jatuh_Tempo: new Date("2/14/2017"),Nominal_Billing: 1000,Selected: false},
				{ Nomor_Invoice_Masuk: "no2",Tanggal_Jatuh_Tempo: new Date("2/14/2017"),Nominal_Billing: 1000,Selected: false},
				{ Nomor_Invoice_Masuk: "no3",Tanggal_Jatuh_Tempo: new Date("2/14/2017"),Nominal_Billing: 1000,Selected: false},
			  ];	  
		res=da_json;
		
        return res;
      },
      create: function(daftarTagihan) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: daftarTagihan.Name,
                                            Description: daftarTagihan.Description}]);
      },
      update: function(daftarTagihan){
        return $http.put('/api/fw/Role', [{
                                            Id: daftarTagihan.Id,
                                            //pid: negotiation.pid,
                                            Name: daftarTagihan.Name,
                                            Description: daftarTagihan.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });