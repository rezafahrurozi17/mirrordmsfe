angular.module('app')
  .factory('DaftarWorkOrderFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
			  
		var da_json=[
				{ Nomor_Wo: "no1",Tanggal_Wo: new Date("2/14/2017"),Nominal_Pph_23: 1000,Selected: false},
				{ Nomor_Wo: "no2",Tanggal_Wo: new Date("2/14/2017"),Nominal_Pph_23: 1000,Selected: false},
				{ Nomor_Wo: "no3",Tanggal_Wo: new Date("2/14/2017"),Nominal_Pph_23: 1000,Selected: false},
			  ];	  
		res=da_json;
		
        return res;
      },
      create: function(daftarWorkOrder) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: daftarWorkOrder.Name,
                                            Description: daftarWorkOrder.Description}]);
      },
      update: function(daftarWorkOrder){
        return $http.put('/api/fw/Role', [{
                                            Id: daftarWorkOrder.Id,
                                            //pid: negotiation.pid,
                                            Name: daftarWorkOrder.Name,
                                            Description: daftarWorkOrder.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });