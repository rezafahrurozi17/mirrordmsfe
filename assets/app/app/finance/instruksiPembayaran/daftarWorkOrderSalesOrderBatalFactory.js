angular.module('app')
  .factory('DaftarWorkOrderSalesOrderBatalFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
			  
		var da_json=[
				{ Nomor_Wo_So_Batal: "no1",Tanggal_Wo_So_Batal: new Date("2/14/2017"),Nominal_Down_Payment: 1000,Selected: false},
				{ Nomor_Wo_So_Batal: "no2",Tanggal_Wo_So_Batal: new Date("2/14/2017"),Nominal_Down_Payment: 1000,Selected: false},
				{ Nomor_Wo_So_Batal: "no3",Tanggal_Wo_So_Batal: new Date("2/14/2017"),Nominal_Down_Payment: 1000,Selected: false},
			  ];	  
		res=da_json;
		
        return res;
      },
      create: function(daftarWorkOrderSalesOrderBatal) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: daftarWorkOrderSalesOrderBatal.Name,
                                            Description: daftarWorkOrderSalesOrderBatal.Description}]);
      },
      update: function(daftarWorkOrderSalesOrderBatal){
        return $http.put('/api/fw/Role', [{
                                            Id: daftarWorkOrderSalesOrderBatal.Id,
                                            //pid: negotiation.pid,
                                            Name: daftarWorkOrderSalesOrderBatal.Name,
                                            Description: daftarWorkOrderSalesOrderBatal.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });