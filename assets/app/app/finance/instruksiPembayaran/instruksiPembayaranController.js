angular.module('app')
	.controller('InstruksiPembayaranController', function ($scope, $http, CurrentUser, InstruksiPembayaranFactory, DaftarTagihanFactory, DaftarWorkOrderSalesOrderBatalFactory, DaftarWorkOrderFactory, DaftarIncomingPaymentFactory, $timeout, uiGridConstants, bsNotify) {
		$scope.Show_Main_DaftarList = true;
		$scope.ShowLihatInstruksiPembayaran = false;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = "Pilih Vendor";
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = "2";
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
		$scope.AnggaranBelanja_isEnable = false;
		$scope.dateNow = Date.now();
		$scope.Main_TanggalStart = new Date();
		$scope.Main_TanggalEnd = new Date();
		$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "";
		$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" },
		{ name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" },
		{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" },
		{ name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
		{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
		{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }
			, { name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Interbranch", value: "Interbranch" }];
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendorOptions = [{ name: "One Time Vendor", value: "One Time Vendor" }, { name: "Pilih Vendor", value: "Pilih Vendor" }];
		$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = null;
		$scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record = "";
		$scope.TambahInstruksiPembayaran_TotalDaftarTagihan = 0;
		// $scope.TambahInstruksiPembayaran_VendorSelect = [{ isVendor: "OneTimeVendor", name: "One Time Vendor" }, { isVendor: "PilihVendor", name: "Pilih Vendor" }];
		// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = {
		// 	value: ''
		// };
		//harja, selain part dan unit, begin
		var uiGridPageSize = 25;
		$scope.currentIncomingInvoiceNumber = 0;
		$scope.ActiveRow = [];
		$scope.DocumentIdPrint = -1;
		$scope.MetodeBayarPrint = "";
		$scope.DocumentId = 0;
		$scope.TempDataGrid1 = [];
		$scope.DataTipePajak = [];
		$scope.DataCostCenter = [];
		$scope.EditMode = false;
		$scope.AdvOpsi = 0;
		$scope.TotalNominal = 0;
		$scope.success = true;
		$scope.SelectedGridId = -1;
		$scope.user = CurrentUser.user();
		$scope.cr2 = true;
		$scope.cr2Unit = false;
		$scope.draft = "";
		$scope.typeRefundDisabled = false;
		$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total = 0;
		$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;
		$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		$scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit = null;

		$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
		$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;

		//$scope.TambahInstruksiPembayaran_TipeRefundOptions = [{ name: "Customer Guarantee", value: "Customer Guarantee" }, { name: "PPh 23", value: "PPh 23" }, { name: "PPN WAPU", value: "PPN WAPU" }, { name: "Titipan", value: "Titipan" }];
		$scope.TambahInstruksiPembayaran_TipeRefundOptions = [{ name: "Customer Guarantee", value: "Customer Guarantee" }, { name: "PPh 23", value: "PPh 23" }, { name: "PPN WAPU", value: "PPN WAPU" }, { name: "Titipan", value: "Titipan" }, { name: "Unknown", value: "Unknown" }];
		$scope.TambahInstruksiPembayaran_TipeRefund = null;

		$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [];//[{ name: "Bea Materai", value: "Bea Materai" }] //,{name:"Biaya Bank", value:"Biaya Bank"}];
		$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = null;
		//harja, selain part dan unit, end
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendorOptions = [{ name: "One Time Vendor", value: "One Time Vendor" }, { name: "Pilih Vendor", value: "Pilih Vendor" }];
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = "2";
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "Pilih Vendor";
		$scope.Main_NamaBranch_EnableDisable = false;
		$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
		$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = false;
		$scope.Show_TipeInput =true;

		$scope.TempInformasiBiaya = [];
		$scope.showCariNomorIncomingPayment = false;
		$scope.showCariNomorBilling = false;
		$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
		console.log($scope.ByteEnable);

		function checkRbyte(rb, b) {
			var p = rb & Math.pow(2, b);
			return ((p == Math.pow(2, b)));
		};
		console.log("yang ke 5");
		$scope.givePatternNominalPembayaran = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran;
				return;
			}
		};
		var user = CurrentUser.user();

		if (user.OrgCode.substring(3, 9) == '000000')
			$scope.Main_NamaBranch_EnableDisable = true;
		else
			$scope.Main_NamaBranch_EnableDisable = false;

		$scope.TanggalBChanged = function () {


			var dts = $scope.Main_TanggalStart;
			var dte = $scope.Main_TanggalEnd;
			var now = new Date();

			if (dte < dts) {
				$scope.errTanggalB = "Tanggal awal tidak boleh lebih dari tanggal billing akhir";
				$scope.TanggalMainStartB = false;
			}
			else if (dts > now) {
				$scope.errTanggalB = "Tanggal awal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalMainStartB = false;
			}
			else {
				$scope.TanggalMainStartB = true;

			}

		}

		$scope.TanggalBChangedEnd = function () {


			var dts = $scope.Main_TanggalStart;
			var dte = $scope.Main_TanggalEnd;
			var now = new Date();

			if (dte > now) {
				$scope.errTanggalEndB = "Tanggal akhir tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalMainEndB = false;
			}

			else {
				$scope.TanggalMainEndB = true;
			}

			if (dte < dts) {
				$scope.errTanggalB = "Tanggal awal tidak boleh lebih dari tanggal billing akhir";
				$scope.TanggalMainStartB = false;
			}
			console.log($scope.TanggalMainEndB);

		}

		$scope.Main_Daftar_PaymentInstruction_UIGrid = {
			paginationPageSize: 25,
			paginationPageSizes: [25, 50, 75],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: true,
			canSelectRows: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "ApprovalId", displayName: "Approval Id", field: "ApprovalId", enableCellEdit: false, enableHiding: false, visible: false, width:'10%' },
				{ name: "DocumentDate", displayName: "Tanggal Instruksi Pembayaran", field: "DocumentDate", enableCellEdit: false, enableHiding: false, type: "date", cellFilter: 'date:"dd/MM/yyyy"', width:'10%' , },
				{
					name: "Id", displayName: "Nomor Instruksi Pembayaran", field: "DocumentNumber", enableHiding: false, visible: true, width:'10%',
					cellTemplate: '<a ng-click="grid.appScope.ShowLihatDetailFromMain(row)">{{row.entity.DocumentNumber}}</a>'
				},
				{ name: "TipeApprovalId", displayName: "TipeApprovalId", field: "ApprovalType", enableHiding: false, visible: false , width:'10%' },
				{ name: "TipeInstruksi", displayName: "Tipe Instruksi Pembayaran", field: "DocumentType", enableHiding: false, width:'10%'  },
				{ name: "NomorInvoice", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableHiding: false, width:'10%'  },
				{ name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false, visible: false, width:'10%'  },
				{ name: "VendorName", displayName: "Nama Pelanggan / Vendor", field: "VendorName", enableCellEdit: false, enableHiding: false, width:'10%'  },
				{ name: "Nominal Pembayaran", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2', type: "number", width:'10%'  },
				{ name: "StatusDraft", displayName: "Status Draft", field: "StatusDraft", enableHiding: false, width:'10%'  },
				{ name: "TipeApproval", displayName: "Tipe Pengajuan", field: "ApprovalTypeDesc", enableHiding: false, width:'10%'  },
				{ name: "TanggalPengajuan", displayName: "Tanggal Pengajuan", field: "ApprovalCreateDate", enableCellEdit: false, enableHiding: false, type: "date", cellFilter: 'date:"dd/MM/yyyy"', width:'10%'  },
				{ name: "StatusPengajuan", displayName: "Status Pengajuan", field: "ApprovalStatusDesc", enableCellEdit: false, enableHiding: false, width:'10%'  },
				{ name: "AlasanPengajuan", displayName: "Alasan Pengajuan", field: "ReversalReason", enableCellEdit: false, enableHiding: false, width:'10%'  },
				{ name: "AlasanPenolakan", displayName: "Alasan Penolakan", field: "RejectedReason", enableCellEdit: false, enableHiding: false, width:'10%'  },
				{ name: "VendorId", displayName: "VendorId", field: "VendorId", enableCellEdit: false, enableHiding: false, visible: false, width:'10%'  },
				{ name: "EnabledCetak", displayName: "EnabledCetak", field: "EnabledCetak", enableCellEdit: false, enableHiding: false, visible: false, width:'10%'  }
			],

			onRegisterApi: function (gridApi) {
				$scope.Main_Daftar_PaymentInstruction_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {

					// $scope.GetListIncoming(pageNumber, pageSize);
					// paginationOptions_MainIncoming.pageNumber = pageNumber;
					// paginationOptions_MainIncoming.pageSize = pageSize;
				});
				gridApi.core.on.renderingComplete($scope, function () {
					if ($scope.user.RoleName == 'ADH' || $scope.user.RoleName == 'ADH BP' ) {
						$scope.Main_TextFilter = 'Diajukan';
						$scope.Main_SearchDropdown = 'ApprovalStatusDesc';
						vSearchText = $scope.Main_TextFilter;
						vSearchType = $scope.Main_SearchDropdown;
					}
					else {
						$scope.Main_TextFilter = '';
						$scope.Main_SearchDropdown = '';
						vSearchText = $scope.Main_TextFilter;
						vSearchType = $scope.Main_SearchDropdown;
					}
					$scope.Main_NamaBranch = user.OutletId
					$scope.SetDataMainRequest(1, uiGridPageSize, $scope.formatDate($scope.Main_TanggalStart),
						$scope.formatDate($scope.Main_TanggalEnd),
						$scope.Main_NamaBranch);

				});
			}
		};

		//blok show main
		//$scope.SetComboBranch();

		$scope.optionsMain_SearchDropdown = [
			{ name: "Nomor Instruksi Pembayaran", value: "DocumentNumber" },
			{ name: "Tipe Instruksi Pembayaran", value: "DocumentType" },
			{ name: "Nama Pelanggan/Vendor", value: "VendorName" },
			{ name: "Nominal Pembayaran", value: "Nominal" },
			{ name: "Status Draft", value: "StatusDraft" },
			{ name: "Nomor Invoice Dari Vendor", value: "InvoiceVendorNo" },
			{ name: "Status Pengajuan", value: "ApprovalStatusDesc" }];

		$scope.MainOutgoingPayment_SearchDropdown = null;

		$scope.optionsTambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search = [{ name: "Nomor Invoice Masuk", value: "Nomor Invoice Masuk" },
		{ name: "Tanggal Invoice", value: "Tanggal Invoice" }, { name: "Nama Branch", value: "Nama Branch" }, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }
			, { name: "Nominal Invoice", value: "Nominal Invoice" }];
		$scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search = null;
		$scope.optionsMain_BulkAction_SearchDropdown = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
		$scope.Main_BulkAction_SearchDropdown = null;

		$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
		console.log($scope.allowApprove);
		if ($scope.allowApprove == false) {
			$scope.optionsMain_BulkAction_SearchDropdown = [];
		}

		$scope.givePattern = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = $scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal;
				return;
			}
		};

		function addCommas(nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		/*
		To use addSeparatorsNF, you need to pass it the following arguments: 
		nStr: The number to be formatted, as a string or number. No validation is done, so don't input a formatted number. If inD is something other than a period, then nStr must be passed in as a string. 
		inD: The decimal character for the input, such as '.' for the number 100.2 
		outD: The decimal character for the output, such as ',' for the number 100,2 
		sep: The separator character for the output, such as ',' for the number 1,000.2
		*/
		function addSeparatorsNF(nStr, inD, outD, sep) {
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1) {
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr)) {
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		//eric -- begin
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			multiselect: false,
			useCustomPagination: false,
			canSelectRows: false,
			enableSelectAll: false,
			enableColumnResizing: true,
			showGridFooter: true,
			showColumnFooter: true,
			columnDefs: [
				{
					name: "CostCenterName", displayName: "Cost Center", field: "CostCenterName", enableCellEdit: false, enableHiding: false,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >Sub Total</div>'
				},
				// {
				// 	name: 'Keterangan', field: 'Keterangan', visible: false
				// },
				{
					name: 'Nominal', field: 'Nominal',
					enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP'
				},
				//	{ name: "Keterangan", displayName: "Keterangan", field: "Keterangan", enableCellEdit: false, enableHiding: false },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarGeneralTransactionRow(row)" ng-disabled = "Show_TambahInstruksiPembayaran_EditDraft">Hapus</a>'
				},
				{ name: "CostCenter", displayName: "Cost Center", field: "CostCenter", enableCellEdit: false, enableHiding: false, visible: false }
			],
			// ,gridFooterTemplate:
			// 	'<div class="ui-grid-cell-contents" >Sub Total {{TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total}}/div>',
			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI = gridApi;
			}
		}; 
		$scope.DeleteGridDaftarGeneralTransactionRow = function (row) {
			var index = $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.indexOf(row.entity);
			if ($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != "" || $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != null || $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != undefined){
				var nomAdv = 0;
				console.log("Cek Nominal Advance 1", nomAdv);
			}else{
				var nomAdv = parseFloat($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment);
				console.log("Cek Nominal Advance 2", nomAdv);
			}
			var genSaldo;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.splice(index, 1);
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total -= row.entity.Nominal;
			genSaldo = $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total - nomAdv;

			$scope.HasilSaldoGen = genSaldo; // cb di samain kaya waktu tambah nya 

			$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = addSeparatorsNF(genSaldo, '.', '.', ',');

			//$scope.TambahInstruksiPembayaran_TotalDaftarTagihan-=row.entity.Nominal;

			$scope.CalculateNominalPembayaran();
			// Nominal Pembayaran 
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == 'General Transaction'){
					var CopyNominal = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran.replace(/\./g, "");
					var tmpNom = CopyNominal.split(",");
					console.log("cek Nominal", tmpNom[0]);
					// var tmpNominal = parseInt(tmpNom[0]) - row.entity.Nominal; // ga perlu di kurang nominal nya lg, karena sudah di definisikan ulang hasilsaldogen nya di atas
					var tmpNominal = parseInt(tmpNom[0])
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(tmpNominal.toFixed(2), '.', ',', '.');
					console.log("Hasil hapus", $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);				
			}

			
		};
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction_Selected_Changed = function () {
			if ((angular.isUndefined($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction) == true) ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction == {} ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction == null) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = "";
			}
			else {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction;
			}
		}

		$scope.ProsesSubmit = false;
		$scope.TambahInstruksiPembayaran_PengajuanCetak = function () {
			$scope.typeRefundDisabled = false;
			$scope.ValidateSaveInstruksi(0, 1);
		}

		$scope.LihatInstruksiPembayaran_CetakBPH = function () {
			if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash") {
				InstruksiPembayaranFactory.cetakBPHCash($scope.DocumentId, 1, 1, $scope.user.OrgId).then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);
						console.log("pdf", fileURL);
						printJS(fileURL);
						//printJS({printable: file,showModal:true,type: 'pdf'})
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
			else {
				InstruksiPembayaranFactory.cetakBPHBankTransfer($scope.DocumentId, 1, 1, $scope.user.OrgId).then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);
						console.log("pdf", fileURL);
						printJS(fileURL);
						//printJS({printable: file,showModal:true,type: 'pdf'})
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
		}

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_Blur = function(){
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
			$scope.tmpnonnpwp = 0;			
			$scope.tmpNpwp = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP.replace(/\./g, "").replace(/\-/g,"");
			console.log("cek NPWP REPLACE", $scope.tmpNpwp);
			console.log("cek >>>>>", $scope.tmpNpwp.length);

			if($scope.tmpNpwp.length < 15 || $scope.tmpNpwp.length > 15){
				console.log("Masuk Ke IF");
				$scope.tmpnonnpwp = 1;
			}else{
				console.log("Masuk Ke ELSE");
				if($scope.tmpNpwp == "000000000000000"){
					$scope.tmpnonnpwp = 1;
				}else{
					$scope.tmpnonnpwp = 0;
				}
			}
			$scope.CalculateNominalPembayaran();

		}

		$scope.ToTambahGeneralTransactionPajak = function () {
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == undefined || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == null){
				$scope.tmpnonnpwp = 1;
			}
			if (!(
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction == "" ||
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP == ""
			)
				&& (!(
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction == null ||
					$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP == null
				))
				&& (!(
					($scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak_EnableDisable == false) &&
					(
						(angular.isUndefined($scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak) == true) ||
						($scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak == "")))
				)
				&& (!(
					($scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak_EnableDisable == false) &&
					((isNaN($scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak) == true) ||
						(angular.isUndefined($scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak) == true) ||
						($scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak == null))))
			) {
				var nominal = 0;
				var tarif = 0;
				var nomdpp = parseFloat($scope.TambahInstruksiPembayaran_GeneralTransaction_DPP);

				if (nomdpp != 0) {
					for (i = 0; i < $scope.DataTipePajak.length; i++) {
						if ($scope.DataTipePajak[i].TaxType == $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction) {
							if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP != "00.000.000.0-000.000") && ($scope.tmpnonnpwp == 0)) {
								//alert(' npwp ' + $scope.DataTipePajak[i].NPWPAmount + ' - type ' + $scope.DataTipePajak[i].TaxType);
								tarif = $scope.DataTipePajak[i].NPWPAmount;
							}
							else {
								//alert(' non npwp ' + $scope.DataTipePajak[i].NPWPAmount + ' - type ' + $scope.DataTipePajak[i].TaxType);
								tarif = $scope.DataTipePajak[i].NonNPWPAmount;
							}

							nominal = nomdpp * tarif / 100.00;
							$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.push({
								TaxId: 0,
								TaxType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction,
								TaxFee: tarif,
								DebetKredit: $scope.DataTipePajak[i].DC,
								Nominal: nominal,
								TaxNumber: $scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak,
								TaxDate: $scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak
							});
							$scope.CalculateNominalPembayaran();
						}
					}
				}
				else {//alert('Nomonial DPP tidak boleh 0');
					bsNotify.show({
						title: "Warning",
						content: 'Nomonial DPP tidak boleh 0.',
						type: 'warning'
					});
				}
			}
			else {//alert('isian tidak lengkap');
				bsNotify.show({
					title: "Warning",
					content: 'isian tidak lengkap',
					type: 'warning'
				});
			}
		}

		$scope.givePattern3 = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP = $scope.TambahInstruksiPembayaran_GeneralTransaction_DPP;
				return;
			}
		};


		// 	$scope.HasilSaldoGen = 0;
		$scope.ToTambahGeneralTransaction = function () {
			if (!(
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction == "" ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal == ""
			)
				&& (!(
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction == null ||
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal == null
				))
			) {

				var nominal = parseFloat($scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal);

				var ccName = "";
				var tmpSama = 0;
				// var GLKet = $scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan;
				// console.log("Cek keterangan", GLKet);

				for (i = 0; i < $scope.DataCostCenter.length; i++) {
					if ($scope.DataCostCenter[i].CostCenterCode == $scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction) {
						ccName = $scope.DataCostCenter[i].CostCenterName;
					}
				}

				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.forEach(function (obj) {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction == obj.CostCenter){
						tmpSama = 1;
					}
				});

				console.log("Cek Konsidi TMPSAMA >>>>", tmpSama)

				if(tmpSama == 0){
					console.log("Cek TMPSAMA 0>>>>", tmpSama)
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.push({
						CostCenter: $scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction,
						CostCenterName: ccName,
						Nominal: nominal
						// Keterangan: GLKet
					});
				}else{
					console.log("Cek TMPSAMA 1>>>>", tmpSama)
					bsNotify.show({
						title: "Warning",
						content: 'Sudah Terdapat Cost Center yang sama.',
						type: 'warning'
					});
					return;
				}
				
				console.log("Cek Grid",	$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data);
				debugger;
				var nominalAdvPayment = 0;
				$scope.Totnominal = 0;
				if (!($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != null || $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != "")) { 
					nominalAdvPayment = parseFloat($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment); 
				}

				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.forEach(function(row){
					$scope.Totnominal += row.Nominal; 
				});
				
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = $scope.Totnominal;
				var saldoGen = $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total -
					nominalAdvPayment;
				$scope.HasilSaldoGen = saldoGen;
				$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = addSeparatorsNF(saldoGen, '.', '.', ',');
				//  ($scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI.grid.columns[2].getAggregationValue() 
				//  + $scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal) - $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment;
				$scope.CalculateNominalPembayaran();

			}
			else {
				bsNotify.show({
					title: "Warning",
					content: 'isian tidak lengkap',
					type: 'warning'
				});
			}

		}


		$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarPajak_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			multiselect: false,
			useCustomPagination: false,
			canSelectRows: false,
			enableSelectAll: false,
			columnDefs: [
				{ name: "JenisPajak", displayName: "Jenis Pajak", field: "JenisPajak", visible: true },
				{ name: "TarifPajak", displayName: "Tarif Pajak %", field: "TarifPajak", enableCellEdit: false, enableHiding: false },
				{ name: "DebetKredit", displayName: "Debet/Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
				{ name: "NominalPajak", displayName: "NominalPajak", field: "NominalPajak", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "NomorDokumenPajak", displayName: "Nomor Dokumen Pajak", field: "NomorDokumenPajak", enableCellEdit: false, enableHiding: false },
				{ name: "TanggalDokumenPajak", displayName: "Tanggal Dokumen Pajak", field: "TanggalDokumenPajak", enableCellEdit: false, enableHiding: false },
			],
			onRegisterApi: function (gridApi) {
				//$scope.TambahInstruksiPembayaran_DaftarTagihanDibayar_gridAPI = gridApi;
			}
		};
		$scope.DeleteGridDaftarPajakRow = function (row) {
			var index = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarPajak_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarPajak_UIGrid.data.splice(index, 1);

			//$scope.TambahInstruksiPembayaran_TotalDaftarTagihan-=row.entity.Nominal;
		};

		$scope.ToTambahDaftarPajak = function () {
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarPajak_UIGrid.data.push({
				JenisPajak: 0,
				TarifPajak: 1,
				NominalPajak: 2
			});
		}

		$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				{ name: "RangkaId", displayName: "Id Nomor Rangka", field: "RangkaId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "RangkaNo", displayName: "Nomor Rangka", field: "RangkaNumber", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "PreviousBalance", displayName: "Saldo Accrued Sebelumnya", field: "PreviousBalance", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "BillAmount", displayName: "Nominal Tagihan Free Service", field: "BillAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "TaxAmount", displayName: "Nominal Pajak", field: "TaxAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "SubTotal", displayName: "SubTotal", field: "SubTotal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false },
				{ name: "AccruedBalance", displayName: "Saldo Accrued", field: "AccruedBalance", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{
					name: "Description", displayName: "Keterangan", field: "Description", cellEditableCondition: $scope.IsEditable, enableHiding: false, visible: true,
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}
				}
				//{name: "DescriptionShow", displayName: "Description", field: "Description", enableCellEdit: false, enableHiding: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_gridAPI = gridApi;
			}
		};


		$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid_View = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 10,
			useCustomPagination: true,
			useExternalPagination: true,
			columnDefs: [
				{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
				{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "BranchName", displayName: "Nama Branch", field: "BranchName", enableCellEdit: false, enableHiding: false },
				{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI_View = gridApi;
			}
		};
		
		$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: null,
			useCustomPagination: true,
			useExternalPagination: true,
			// enableColumnResizing: true,
			// enableFiltering: true,
			// enableSelectAll: false,
			// displaySelectionCheckbox: false,
			// enableRowSelection: true,
			// multiselect: false,
			// canSelectRows: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
			columnDefs: [
				{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
				{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "BranchName", displayName: "Nama Branch", field: "BranchName", enableCellEdit: false, enableHiding: false },
				{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI = gridApi;
				
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					$scope.getDataFree(pageNumber, pageSize, $scope.DocumentId, '', $scope.mode);
					$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
				});
				
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					$scope.TotalNominal = 0;
					$scope.tmpSelectRow = false;
					console.log('cek data 1 >>>>>>', $scope.tmpSelectRow)
					if (row.isSelected == true) {
						$scope.tmpSelectRow = true;
						console.log('cek data 1 >>>>>>', $scope.tmpSelectRow)
						var mode = 'Lihat';
						if($scope.Action == "Edit"){
							if ($scope.EditMode == true) {
								mode = 'Lihat';
								$scope.TotalNominal = row.entity.Nominal;
							}
						}else{
							if ($scope.EditMode == true) {
								mode = '';
								$scope.TotalNominal = row.entity.Nominal;
							}else{
								$scope.TotalNominal = row.entity.Nominal;
								$scope.tmpPaymentId = 0;
							}
						}
						// if ($scope.EditMode == true) {
						// 	mode = '';
						// 	$scope.TotalNominal = row.entity.Nominal;
						// }
						InstruksiPembayaranFactory.getDataForAccruedFrame(1, uiGridPageSize, row.entity.InvoiceId, '', mode, $scope.tmpPaymentId).then(
							function (res) {
								$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = res.data.Result;
								// CR5 #38 Finance Start
								InstruksiPembayaranFactory.getIncoiveTaxList(1, uiGridPageSize, row.entity.InvoiceId, '').then(
									function (res) {
										$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
									},
									function (err) {
										console.log('error --> ', err);
									}
								);
								// CR5 #38 Finance End
							},
							function (err) {
								console.log('error --> ', err);
							}
						);
					}else{
						$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
					}
					$scope.CalculateNominalPembayaran();
				});
			}
		};

		$scope.getGLList = function () {
			$scope.currentCustomerId = 0;
			InstruksiPembayaranFactory.getGLList(1, 1000, "GL")
				.then(
					function (res) {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransactionOption = res.data.Result;
					},
					function (err) {
						console.log("err=>", err);
					});
		}

		$scope.getCostCenterList = function () {
			InstruksiPembayaranFactory.getCostCenterList(1, 1000, "COSTCENTER")
				.then(
					function (res) {
						$scope.DataCostCenter = res.data.Result;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransactionOption = res.data.Result;
					},
					function (err) {
						console.log("err=>", err);
						$scope.DataCostCenter = [];
					}
				);
		}
		$scope.getPajakList = function () {
			InstruksiPembayaranFactory.getPajakList(1, 1000, "PAJAK")
				.then(
					function (res) {
						$scope.DataTipePajak = res.data.Result;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption = res.data.Result;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction_Changed = function () {
			var istaxDoc;
			for (i = 0; i < $scope.DataTipePajak.length; i++) {
				if ($scope.DataTipePajak[i].TaxType == $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction) {
					$scope.TambahInstruksiPembayaran_GeneralTransaction_KetObjekPajak = $scope.DataTipePajak[i].TaxDesc;
					istaxDoc = $scope.DataTipePajak[i].isTaxDoc;
				}
			}
			if ($scope.EditMode == true) {
				// if ( $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction == "PPN - 10%" ||
				// 	$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction == "PPh 22 - 0.45%" ||
				// 	$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction == "PPh 22 Sangat Mewah - 5%") {
				if (istaxDoc == 1) {
					$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak_EnableDisable = false;

				}
				else {
					$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak = "";
					$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak = null;
					$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak_EnableDisable = true;
				}
			}

		};

		$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			multiselect: false,
			useCustomPagination: false,
			enableColumnResizing: true,
			canSelectRows: false,
			enableSelectAll: false,
			columnDefs: [
				{ name: "PajakId", displayName: "Id Pajak", field: "TaxId", visible: false },
				{ name: "JenisPajak", displayName: "Jenis Pajak", field: "TaxType", enableCellEdit: false, enableHiding: false },
				{ name: "TarifPajak", displayName: "Tarif Pajak", field: "TaxFee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "DebetKredit", displayName: "Debet / Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal Pajak", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "NoDokPajak", displayName: "Nomor Dokumen Pajak", field: "TaxNumber", enableCellEdit: false, enableHiding: false },
				{ name: "TglDokPajak", displayName: "Tanggal Dokumen Pajak", field: "TaxDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				//{name:"NoReferensi", displayName:"Nomor Referensi", field:"RefferenceNumber", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},
				{
					name: "Action", displayName: "Action", field: "Action", enableHiding: false, visible: true,
					cellTemplate: '<a ng-click="grid.appScope.HapusDaftarPajakGeneralTransaction(row)">Hapus</a>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_gridAPI = gridApi;

			}
		};

		$scope.HapusDaftarPajakGeneralTransaction = function (row) {
			var index = $scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.splice(index, 1);
			$scope.CalculateNominalPembayaran();
		};

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_DaftarTagihan_UIGrid = {

			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			multiselect: false,
			useCustomPagination: false,
			canSelectRows: false,
			enableSelectAll: false,
			columnDefs: [
				{ name: "InvoiceId", displayName: "Nomor Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false },
				{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "VendorName", displayName: "Nama Vendor", field: "VendorName", enableCellEdit: false, enableHiding: false },
				{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
				{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
				{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_DaftarTagihan_gridAPI = gridApi;

				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					if (row.isSelected == false) {
						$scope.currentIncomingInvoiceNumber = 0;
					}
					else {
						$scope.currentIncomingInvoiceNumber = row.entity.InvoiceId;
					}
				});
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});
			}
		};

		//eric -- end

		$scope.SetFormModeView = function (type) {
			if (type == "General Transaction") {

			}
			else if (type == "Refund") {

			}
			else {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
			}
		};

		$scope.EnableUIGridColumn = function () {
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.columnDefs[2].visible = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.columnDefs[7].visible = true;
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = true;
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

			//if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") 
			//	$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = true;
			//if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts") 
			//	$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = false;
			//else 
			//$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = false;			
			//$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
		}

		// disable botton 
		$scope.SimpanDraf_Disabled = true;
		$scope.InformasiAlamat_Disabled = true;
		$scope.KabupatenKota_Disabled = true;
		
		$scope.isialamat = function (item){
			if(item == null || item == '' || item == undefined){
				$scope.InformasiAlamat_Disabled = true;
				console.log("isialamat", item)
			}else{
				$scope.InformasiAlamat_Disabled = false;
				console.log("isialamat", item)
			}
		}
		$scope.isikotakab = function (item){
			if(item == null || item == '' || item == undefined){
				$scope.KabupatenKota_Disabled = true;
				console.log("isialamat", item)
			}else{
				$scope.KabupatenKota_Disabled = false;
				console.log("isialamat", item)
			}
		}

		$scope.TambahInstruksiPembayaran_EditDraft = function () {
			$scope.EditMode = true;
			$scope.typeRefundDisabled = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
			$scope.Show_NomorRekTujuan_Disable = false;
			$scope.Show_NomorRekTujuan_EnableDisable = false;
			//CR4 P2 Finance BungaDF Start
			$scope.Action = 'Edit'
			//CR4 P2 Finance BungaDF End
			
			if (user.OrgCode.substring(3, 9) == '000000'){
				$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = true;
			}else{
				$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = false;
			}
			//NOTO | EDIT DRAFT
			$scope.Show_TambahInstruksiPembayaran_RincianPembayaran_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable = true;
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "STNK/BPKB" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Order Pengurusan Dokumen" ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard") {
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [{ name: "Bea Materai", value: "Bea Materai" }];
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }];
			}else{
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [];
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
			}

			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = true;
				$scope.Show_EditDraft = false;
				$scope.show_draft_EditDraft = true;

				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.columnDefs[2].visible = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

				$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.columnDefs[7].visible = true;
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.columnDefs[0].visible = true;
				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);


				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				var data = $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data;
				$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = true;
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed(1, uiGridPageSize); //DEBUG SINI
				console.log('data TambahInstruksiPembayaran_EditDraft ===>',data);
				angular.forEach(data, function (row) {
					//Cr4 Finance 
					if($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee"){ //customer guarantee cr4 Finance
						console.log('MASUKK GET 4')
						if(row.TypeGuarantee == 1){
							row.temp = 'Titipan'
						}else if(row.TypeGuarantee == 2 && row.InnerType == "SAH_SPK_TT"){
							row.temp = 'Booking Fee dan Down Payment'
						}else if(row.TypeGuarantee == 2 && row.InnerType == "AJob_List_TT"){
							row.temp = 'Down Payment'
						}else if(row.TypeGuarantee == 2 && row.InnerType == "Apart_SalesOrder_TT"){
							row.temp = 'Down Payment'
						}
						else{
							row.temp = null
						}
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
							RefundId: 0,
							RefundType: row.RefundType,
							ReferenceNumber: row.ReferenceNumber,
							Nominal: row.Nominal,
							ReferenceId: row.ReferenceId,
							InnerType: row.InnerType,
							NominalDP: row.NominalDP,
							NominalGuarantee: row.NominalGuarantee,
							TypeGuarantee: row.TypeGuarantee,
							NominalDPPure: row.NominalDPPure,
							NominalGuaranteePure: row.NominalGuaranteePure,
							temp: row.temp
						});
					}else if($scope.TambahInstruksiPembayaran_TipeRefund == "Titipan"){
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
							RefundId: 0,
							RefundType: row.RefundType,
							ReferenceNumber: row.ReferenceNumber,
							Nominal: row.Nominal,
							ReferenceId: row.ReferenceId,
							InnerType: row.InnerType,
							IPId: row.IPId,
							SOCode : row.SOCode
						});
					}else{
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
							RefundId: 0,
							RefundType: row.RefundType,
							ReferenceNumber: row.ReferenceNumber,
							Nominal: row.Nominal,
							ReferenceId: row.ReferenceId,
							InnerType: row.InnerType
						});
					}
				});

				$scope.Show_TambahInstruksiPembayaran_Refund_Tambah = true;
				//$scope.Show_TambahInstruksiPembayaran_EditDraft = true;

				$scope.Show_TambahInstruksiPembayaran_Refund = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.Show_EditDraft = false;
				$scope.show_draft_EditDraft = false;

				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
				$scope.Show_TambahInstruksiPembayaran_Refund_WoSo = true;

				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == "Unknown") {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = true;
					$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = false;
					$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
				$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid = true;
				$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid_View = false;
				
				var exData = $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
				InstruksiPembayaranFactory.getDataForAccruedFreeService(1, uiGridPageSize, $scope.DocumentId, '', 'Edit', $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
					function (resFS) {
						$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = resFS.data.Result;
						$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
						var index = 1;
						$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data.forEach(function (row) {
							if (row.InvoiceId == exData[0].InvoiceId) {
								$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.selection.selectRow(
									$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data[index]);
								$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
							}
							index += 1;
						});
						$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.selection.selectAllRows();
					},
					function (errFS) {
						console.log('Error -->', errFS);
					}
				);
				$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
				// $scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.Show_EditDraft = false;
				$scope.show_draft_EditDraft = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
				if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor != "2") {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "One Time Vendor";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon_EnableDisable = true;
				}
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				$scope.Show_TambahInstruksiPembayaran_EditDraft = true;
				// $scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.Show_EditDraft = true;
				$scope.show_draft_EditDraft = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
				//harja
				InstruksiPembayaranFactory.getDataForAccrued(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, 'Edit').then(
						function (result) {
							$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data = result.data.Result;
							$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data.forEach(function (rowAC) {
								if (rowAC.SubsidyId == $scope.SelectedGridId) {
									rowAC.radidata = true;
									//		$scope.SelectedGridId = $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[0].SubsidyId;
								}
							});
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = result.data.Result[0].VendorCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = result.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = result.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = result.data.Result[0].VendorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = result.data.Result[0].Handphone;
							$scope.success = true;
						},
						function (err) {
							console.log('error -->', err);
							$scope.success = false;
						}
					);
				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.columnDefs[0].visible = true;
				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				// $scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.Show_TambahInstruksiPembayaran_EditDraft = true;
				$scope.Show_EditDraft = true;
				$scope.show_draft_EditDraft = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
					{
						name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
						cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
					},
					{ name: "IncomingInstructionId", displayName: "IdIncomingInstruction", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "BranchReceipent", displayName: "Nama Branch Penerima", field: "BranchReceipent", enableCellEdit: false, enableHiding: false },
					{ name: "IncomingInstructionNumber", displayName: "Nomor Incoming Payment", field: "IncomingInstructionNumber", enableCellEdit: false, enableHiding: false },
					{ name: "IncomingDate", displayName: "Tanggal Incoming Payment", field: "IncomingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
					{ name: "CustomerName", displayName: "Nama Pelanggan", field: "CustomerName", enableCellEdit: false, enableHiding: false, visible: true },
					{ name: "Description", displayName: "Untuk Pembayaran", field: "Description", enableCellEdit: false, enableHiding: false }				//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
				];

				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
				$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Cash", value: "Cash" }];
				InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.user.OutletId,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, $scope.DocumentId).then(
						function (resInv) {
							var a = 0;
							var nominalPembayaran = 0;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInv.data.Result;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
								for (i = 0; i < $scope.TempDataGrid1.length; i++) {
									if (row.InvoiceId == $scope.TempDataGrid1[i].InvoiceId) {
										row.radidata = true;
										//alert('masuk tax');
										nominalPembayaran = nominalPembayaran + row.Nominal;
										// $scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.forEach(function(row){
										// 	nominalPembayaran = nominalPembayaran + row.Nominal;
										// });

									}
								}
								console.log("Cek Nominal 1",addSeparatorsNF(nominalPembayaran, '.', ',', '.'))
								console.log("before");
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nominalPembayaran.toFixed(2), '.', ',', '.');
								console.log("Cek Nominal 2",$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran)
								$scope.TotalNominal = nominalPembayaran;
								a = a + 1;
								console.log("after");
							});
						},
						function (errInv) {
							console.log('error -->', errInv);
						}
					);
				// $scope.Show_EditDraft = true;
				// $scope.show_draft_EditDraft = true;

			}else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif") {
				$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.show_draft_EditDraft = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = true;
				$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
					{ name: "NamaCustomer", displayName: "Nama Customer", field: "NamaCustomer", enableCellEdit: false, enableHiding: false },
					{ name: "Alamat", displayName: "Alamat", field: "Alamat", enableCellEdit: false, enableHiding: false },
					{
						name: "NomorIncomingPayment", field: "NomorIncomingPayment",displayName: "No Incoming Payment",enableCellEdit: false,
						cellTemplate: '<a ng-click="grid.appScope.SelectedIncomingPayment(row)">{{row.entity.NomorIncomingPayment}}</a>'
					},
					{ name: "TanggalIncoming", displayName: "Tanggal Incoming Payment", field: "TanggalIncoming",enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalIncomingPayment", displayName: "Nominal Incoming Payment", field: "NominalIncomingPayment", cellFilter: 'rupiahC',enableCellEdit: false, enableHiding: false, visible: true }			//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
				];
			}
			else {
				var data = $scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data;
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs = [
					{ name: "ChargesId", displayName: "ID", field: "ChargesId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "ChargesType", enableCellEdit: false, enableHiding: false },
					{ name: "DebitKredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "Description", displayName: "Description", field: "Description", enableCellEdit: false, enableHiding: false, visible: false },
					{
						name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
						cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarBiayaLainLain(row)">Hapus</a>'
					}];
				// $scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = true;
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				//$scope.SetColumnUIGridDaftarTagihan();
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") ||
					($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {

					//$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = false;

					if (($scope.cr2 == true) &&
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
						//alert('masuk sini 111 - harjia !');
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: true, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }						//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					if (($scope.cr2 == true) &&
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit")) {
							$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
							$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = []; //,{name:"Biaya Bank", value:"Biaya Bank"}];
						//alert('masuk sini 111 - harjia !');
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: true, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }						//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					else {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: true, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible: false },

							{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false },
							{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false }
							//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					if ($scope.cr2 == true) {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs[3].visible = true;
					}
				}
				else {
					//$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = false;
					if ($scope.cr2 == true) {
						//alert('masuk sini - harjia !');
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }						//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					else {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							//	{name:"DPP", displayName:"DPP",field:"DPP",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
							//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
							//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
							//{name:"BungaDF", displayName:"Bunga DF",field:"BungaDF",enableCellEdit: true, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},	
							{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible: false },
							{
								name: "AdvancePaymentNo", displayName: "Nomor Advance Payment", field: "AdvancePaymentNo", enableCellEdit: false, enableHiding: false, visible: false,
								cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentNo}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
							},
							{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false },
							{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false }
							//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
				}

				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
				console.log("STATUS DRAF >>>>>", $scope.draft);
				console.log("nuse debug", $scope.DocumentId);
				InstruksiPembayaranFactory.getDataInvoiceList(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, $scope.DocumentId, $scope.Action).then(
						function (resInv) {
							var a = 0;
							var nominalPembayaran = 0;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInv.data.Result;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
								for (i = 0; i < $scope.TempDataGrid1.length; i++) {
									if (row.InvoiceId == $scope.TempDataGrid1[i].InvoiceId) {
										if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit"  //CR4 P2 FInance BungaDF Start
										&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Parts"){
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[a].AdvancePaymentId = $scope.TempDataGrid1[i].AdvancePaymentId;
										row.AdvanceNominal = $scope.TempDataGrid1[i].AdvanceNominal;
										row.Saldo = $scope.TempDataGrid1[i].Saldo;
										row.BungaDF = $scope.TempDataGrid1[i].BungaDF;
										}//CR4 P2 FInance BungaDF End
										row.radidata = true;
										row.BungaDF = row.BungaDF.toFixed(2);
										//alert('masuk tax');
										console.log("nuse check row : ", row);
										$scope.PrepareGridTaxList(1, uiGridPageSize, row);
										console.log("nuse edit nominal pembayaran");
										nominalPembayaran = nominalPembayaran + row.Saldo;//+ row.BungaDF;

										// $scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.forEach(function(row){
										// 	nominalPembayaran = nominalPembayaran + row.Nominal;
										// });

									}
								}
								console.log("awal 1",nominalPembayaran);
								console.log("awal 2",nominalPembayaran.toFixed(2));
								console.log("awal 3",addSeparatorsNF(nominalPembayaran.toFixed(2), '.', ',', '.'));
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nominalPembayaran.toFixed(2), '.', ',', '.');
								$scope.TotalNominal = nominalPembayaran;
								a = a + 1;
								console.log("akhir");
								//CR4 P2 Finanace BungaDF Start
								if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit" 
								|| $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts"){
									var id = row.VendorIdBungaDF != 0 ? row.VendorIdBungaDF : row.VendorId;
									var isDF = row.VendorIdBungaDF != 0 ? 1 : 0;
									$scope.getDataBankAccountUnitPart(id,isDF);
									$scope.VendorIdBungaDF = row.VendorIdBungaDF;
								}//CR4 P2 Finanace BungaDF End
							});
							$scope.CalculateNominalPembayaran();
						},
						function (errInv) {
							console.log('error -->', errInv);
						}
					);
				//$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = $scope.TempDataGrid1.data.Result;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
				// $scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
				$scope.Show_EditDraft = false;
				$scope.show_draft_EditDraft = true;
				//Show_TambahInstruksiPembayaran_EditDraft
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
				//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = data;
			}
			$scope.TambahInstruksiPembayaran_EditDraft_EnableDisable = true;
			$scope.enable_draft_EditDraft = false;
			$scope.Show_TambahInstruksiPembayaran_EditDraft_button = false;
		};

		$scope.OtherGridSelected = function (row) {
			console.log('selected', row)
			$scope.TotalNominal = 0;
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (dt) {
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
					if (dt.IncomingInstructionId == $scope.SelectedGridId) {
						dt.radidata = false;
					}
				}
				else {
					if (dt.InvoiceId == $scope.SelectedGridId) {
						dt.radidata = false;
						$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data = [];				
					}
				}
			});
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
				if ($scope.SelectedGridId != row.entity.IncomingInstructionId) {
					$scope.SelectedGridId = row.entity.IncomingInstructionId;
					row.entity.radidata = true;
					$scope.TotalNominal = row.entity.Nominal;
				}
				else {
					$scope.SelectedGridId = -1;
				}
			}
			else {
				if ($scope.SelectedGridId != row.entity.InvoiceId) {
					$scope.SelectedGridId = row.entity.InvoiceId;
					row.entity.radidata = true;
					var BDF = 0;

					if($scope.draft != 'Ya'){
						if (row.entity.BungaDF != null){
							BDF = row.entity.BungaDF;
							$scope.TotalNominal = parseFloat(row.entity.Saldo) + parseFloat(BDF);
						}
					}else{
						$scope.TotalNominal = parseFloat(row.entity.Saldo)
					}
					$scope.PrepareGridTaxList(1, uiGridPageSize, row);
					$scope.getAdvancePaymnet();
				}
				else {
					$scope.SelectedGridId = -1;
					$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = [];
				}
			}
			console.log('OtherGridSelected - TotalNominal ', $scope.TotalNominal);
			$scope.CalculateNominalPembayaran();
			//CR4 P2 Finance BungaDF Start
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit" 
			|| $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts"){
				var id = row.entity.VendorIdBungaDF != 0 ? row.entity.VendorIdBungaDF : row.entity.VendorId;
				var isDF = row.entity.VendorIdBungaDF != 0 ? 1 : 0;
				$scope.getDataBankAccountUnitPart(id,isDF);
				$scope.VendorIdBungaDF = row.entity.VendorIdBungaDF;
			}
			$scope.TenorBungaDF = row.entity.TenorBungaDF;
			$scope.PercentageBungaDF = row.entity.PercentageBungaDF;
			//CR4 P2 Finance BungaDF End
			//alert(' dipilih ' + row.entity.SubsidyType);
		};


		//new get advance paymnet 
		$scope.getAdvancePaymnet = function(){
			InstruksiPembayaranFactory.getAdvancePaymentListNew(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, 2, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, $scope.SelectedGridId)
			.then(
				function(res){
					$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data = res.data.Result;
					console.log("cek List Data advance payment", res.data.Result)
					$scope.CalculateNominalPembayaran();
				},
				function(err){
					console.log("cek error ==>", err);
				}
			);
		}
		$scope.ToTambahDaftarAdvancePayment = function () {
			if ($scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment != "") {
				var found = false;
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.forEach(function (row) {
					if (row.AdvancedPaymentId == $scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment) {
						found = true;
						//break;
					}
				});

				if (found == false) {
					$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.push({
						AdvancedPaymentId: $scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment,
						AdvancedPaymentNumber: $scope.TambahInstruksiPembayaran_DaftarAdvPayment_NoAdvPayment,
						Nominal: parseFloat($scope.TambahInstruksiPembayaran_DaftarAdvPayment_Nominal.replace(/\,/g, '')),
						FrameNo: $scope.TambahInstruksiPembayaran_DaftarAdvPayment_FrameNo
					});
				}
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_FrameNo = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Nominal = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NoAdvPayment = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NamaVendor = "";

				$scope.CalculateNominalPembayaran();
			}
		}
		
		$scope.ShowLihatDetailFromMain = function (row) {
			//angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal('hide');
			//$scope.SetComboBranch();
			var AdvSaldo = 0;
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = false;
			$scope.DocumentId = row.entity.DocumentId;
			$scope.ToTambahInstruksiPembayaran();
			$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = false;
			$scope.EditMode = false;
			$scope.success = true;
			$scope.draft = row.entity.StatusDraft;
			console.log("STATUS DRAF >>>>>", $scope.draft);
			//$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;

			$scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text = ""; 
			$scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
			$scope.Show_TambahInstruksiPembayaran_EditDraft = true;
			$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = true;

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = row.entity.DocumentType;
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Interbranch"){
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed();
			}
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = true;

			$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21_EnableDisable = false;
			if (row.entity.StatusDraft == "Ya") {

				$scope.TambahInstruksiPembayaran_EditDraft_EnableDisable = false;
				$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
				$scope.Show_LihatInstruksiPembayaran_CetakBPH = false;
				$scope.show_draft_EditDraft = true;
				$scope.enable_draft_EditDraft = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable = true;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_button = true;
			}
			else {
				$scope.ShowTambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaran = true;
				$scope.ShowTambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaranReversal = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaran = row.entity.DocumentNumber;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaranReversal = row.entity.DocumentReverseNumber;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable = true;
				$scope.If_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
				$scope.If_TambahInstruksiPembayaran_DaftarAdvancePayment_NoAdvanceTambah = true;
				$scope.TambahInstruksiPembayaran_TanggalJatuhTempo_EnableDisable = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain_Form = false;
				$scope.Show_NomorRekTujuan_EnableDisable = true;

				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs = [
					{ name: "AdvancedPaymentId", displayName: "ID", field: "AdvancedPaymentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "AdvancedPaymentNumber", displayName: "No Advance Payment", field: "AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false },
					//{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{
						name: 'Nominal', field: 'Nominal',
						enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP'
					},
					{ name: "FrameNo", displayName: "No Rangka", field: "FrameNo", enableCellEdit: false, enableHiding: false, visible: false },
				];

				$scope.TambahInstruksiPembayaran_EditDraft_EnableDisable = true;
				$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = true;
				$scope.Show_LihatInstruksiPembayaran_CetakBPH = true;
				if (row.entity.EnabledCetak == 1) {
					$scope.allowPrint = true;
				}
				else {
					$scope.allowPrint = false;
				}
				$scope.show_draft_EditDraft = false;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_button = false;
			}


			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
				$scope.Hide_TambahInstruksiPembayaran_AdvancePayment = true;
			}
			else {
				$scope.Hide_TambahInstruksiPembayaran_AdvancePayment = false;
			}

			//tambah disable all//
			//alert('isi row ' + row.entity.DocumentType);
			var nomPembayaranDB;
			var nominalDB;
			var paymentAmtDB;
			InstruksiPembayaranFactory.getMasterLihat(1, uiGridPageSize, row.entity.DocumentId, 0).then(
				function (res) {
					//alert('masuk - call master');
					$scope.tmpPaymentId = res.data.Result[0].PaymentId;
					$scope.VariabelBaru = res.data.Result[0].RefundType;	
					nomPembayaranDB = res.data.Result[0].NominalPembayaran;
					nominalDB = res.data.Result[0].Nominal;
					paymentAmtDB = res.data.Result[0].PaymentAmount;
					if ($scope.TambahInstruksiPembayaran_EditDraft_EnableDisable == true) {
						var tanggal = new Date(res.data.Result[0].PaymentInstructionDate);
						tanggal.setMinutes(tanggal.getMinutes() + 420);
						$scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran = tanggal;
					}
					$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = res.data.Result[0].NamaPimpinan;
					$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = res.data.Result[0].NamaFinance;
					if(user.OrgCode.substring(3, 9) == '000000'){
						$scope.TambahInstruksiPembayaran_NamaBranch = row.entity.OutletId;
					}else{
						$scope.TambahInstruksiPembayaran_NamaBranch = res.data.Result[0].OutletId;
					}
					console.log("Data Lihat >>>>>>>>", $scope.TambahInstruksiPembayaran_NamaBranch);

					$scope.LihatInstruksiPembayaran_TujuanPembayaran_NoRekening = res.data.Result[0].NoRekening;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = res.data.Result[0].NamaBank;
					//$scope.LihatInstruksiPembayaran_TujuanPembayaran_NoRekening = "Tes Nomor Rekening";
					//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = "Tes Nama Bank";

					console.log("Data : ", res.data.Result[0])
					console.log("No Rek. Tujuan : ", res.data.Result[0].AccountTo)
					if (row.entity.StatusDraft !== "Ya") {
						//	alert('masuk yaa');
						if (res.data.Result[0].AllowReversal == 0) {
							$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
						}
						else {
							$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = true;
						}
					}
					if (row.entity.DocumentType == "General Transaction") {
						if (res.data.Result[0].VendorType == "Pilih Vendor") {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
							$scope.LogicCariBtn_TopSearch();
							$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = true;
						}

						if (res.data.Result[0].VendorType == "One Time Vendor") {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = "1";
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val;
							$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = true;
							//$scope.SetColumnUIGridDaftarTagihan();
							$scope.LogicCariBtn_TopSearch();

						}
						else {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = "2";
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val;
						}
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						// else { $scope.success = true;}
						// if ($scope.success == true ) { 
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak_EnableDisable = false;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak_EnableDisable = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].VendorAddress;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].VendorCity;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].VendorPhone;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = res.data.Result[0].VendorNPWP;
						$scope.currentIncomingInvoiceNumber = res.data.Result[0].IncomingInvoiceNumber;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = res.data.Result[0].GLCode;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction = res.data.Result[0].GLCode;
						//alert('masuk set awal');
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = res.data.Result[0].NominalPPh21;

						if (res.data.Result[0].PaymentMethod != 'cash'){
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
						}
						else {
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						}

						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
						console.log("yang satuu");
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nomPembayaranDB.toFixed(2), '.', ',', '.'); //aaaaa  res.data.Result[0].NominalPembayaran;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

						var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
						tanggal2.setMinutes(tanggal2.getMinutes() + 420);
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;

						$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = res.data.Result[0].Description;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment = res.data.Result[0].AdvancePaymentId;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorAdvPayment = res.data.Result[0].AdvancedPaymentNumber;
						var advnominalGen = res.data.Result[0].AdvanceNominal;
						var saldoGen = res.data.Result[0].Nominal;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = addSeparatorsNF(advnominalGen, '.', '.', ','); // res.data.Result[0].AdvanceNominal;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = addSeparatorsNF(saldoGen, '.', '.', ',');  //es.data.Result[0].Nominal;

						InstruksiPembayaranFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.DocumentId).then(
							function (resChg) {
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							},
							function (errChg) {
								console.log('error -->', errChg);
							}
						);
						InstruksiPembayaranFactory.getMasterLihatCostCenter(1, uiGridPageSize, row.entity.DocumentId).then(
							function (resCC) {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data = resCC.data.Result;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.columnDefs[2].visible = false;
							},
							function (errCC) {
								console.log('error -->', errCC);
							}
						);

						InstruksiPembayaranFactory.getIncoiveTaxList(1, uiGridPageSize, row.entity.DocumentId, row.entity.DocumentType).then(
							function (resTax) {
								$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = resTax.data.Result;
								$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.columnDefs[7].visible = false;
							},
							function (errTax) {
								console.log('error -->', errTax);
							}
						);
						//}
					}
					else if (row.entity.DocumentType == "Accrued Free Service") {
						console.log("yang dua");
						$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid = false;
						$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid_View = true;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nominalDB, '.', '.', ','); // res.data.Result[0].Nominal;
						
						if (res.data.Result[0].PaymentMethod.toLowerCase() != 'cash'){
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
						}
						else {
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
						}

						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
						var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
						tanggal2.setMinutes(tanggal2.getMinutes() + 420);
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;

						InstruksiPembayaranFactory.getDataForAccruedFreeService(1, uiGridPageSize, row.entity.DocumentId, '', 'Lihat', $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
							function (resFS) {
								$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid_View.data = resFS.data.Result;
								// $scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
								$scope.success = true;
								$scope.InvoiceId = resFS.data.Result[0].InvoiceId;
								
								InstruksiPembayaranFactory.getDataForAccruedFrame(1, uiGridPageSize, $scope.InvoiceId, '', 'Lihat', $scope.tmpPaymentId).then(
									function (resFrame) {
										$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
										$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = resFrame.data.Result;
										// CR5 #38 Finance Start
										InstruksiPembayaranFactory.getIncoiveTaxList(1, uiGridPageSize, $scope.InvoiceId, '').then(
											function (res) {
												$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
											},
											function (err) {
												console.log('error --> ', err);
											}
										);
										// CR5 #38 Finance End
									},
									function (errFrame) {
										console.log('error --> ', errFrame);
									}
								);
							},
							function (errFS) {
								console.log('Error -->', errFS);
								$scope.success = false;
							}
						);

						// InstruksiPembayaranFactory.getDataForAccruedFrame(1, uiGridPageSize, $scope.InvoiceId, '', 'Lihat').then(
						// 	function (resFrame) {
						// 		$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
						// 		$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = resFrame.data.Result;
						// 	},
						// 	function (errFrame) {
						// 		console.log('error --> ', errFrame);
						// 	}
						// );

						InstruksiPembayaranFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.DocumentId).then(
							function (resChg) {
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							},
							function (errChg) {
								console.log('error -->', errChg);
							}
						);
					}
					else if (row.entity.DocumentType == "Accrued Subsidi DP/Rate") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].SOId;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorNameTyped;
						$scope.LogicCariBtn_TopSearch();

						if (res.data.Result[0].PaymentMethod != 'cash'){
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
						}
						else {
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						}

						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
						var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
						tanggal2.setMinutes(tanggal2.getMinutes() + 420);
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
						console.log("yang tiga");
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nomPembayaranDB, '.', '.', ','); //res.data.Result[0].NominalPembayaran;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan = res.data.Result[0].Description;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;

						// InstruksiPembayaranFactory.getDataForAccrued(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor , 
						// 		'lihat'	, $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor).then(
						// 	function(result){
						// 		$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data = result.data.Result;
						// 		$scope.success = true;
						// 	},
						// 	function(err) {
						// 		console.log('error -->', err);
						// 		$scope.success = false;
						// 	}
						// );

						InstruksiPembayaranFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.DocumentId).then(
							function (resChg) {
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							},
							function (errChg) {
								console.log('error -->', errChg);
							}
						);
					}
					else if (row.entity.DocumentType == "Accrued Komisi Sales") {
						$scope.TambahInstruksiPembayaran_InputSO = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].SOId;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.VendorKTP = res.data.Result[0].VendorKTP;
						$scope.LogicCariBtn_TopSearch();
						if ($scope.success == true) {
							//TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = "2";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_Selected_Changed();
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
							$scope.TotalNominal = res.data.Result[0].Nominal;
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
							if (res.data.Result[0].VendorType != "Pilih Vendor") {
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "One Time Vendor";
								//	alert('masuk sini ' + res.data.Result[0].VendorNameTyped );
								if (res.data.Result[0].VendorType == "One Time Vendor") {
									$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = 1;
								}
								else {
									$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = 2;
								}
								
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = res.data.Result[0].VendorId
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = res.data.Result[0].VendorAddress;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = res.data.Result[0].VendorCity;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = res.data.Result[0].VendorPhone;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = res.data.Result[0].VendorKTP;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = res.data.Result[0].VendorNameTyped;
							}
							else {
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "Pilih Vendor";
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor = res.data.Result[0].VendorId;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = res.data.Result[0].VendorNameTyped;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = res.data.Result[0].VendorId

								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = res.data.Result[0].VendorAddress;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = res.data.Result[0].VendorCity;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = res.data.Result[0].VendorPhone;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = res.data.Result[0].VendorKTP;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = res.data.Result[0].VendorNameTyped;
							}
							var nomKomisi = res.data.Result[0].Nominal;
							var nomPPH21 = res.data.Result[0].NominalPPh21;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nomPPH21; //res.data.Result[0].NominalPPh21;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nomKomisi; // res.data.Result[0].Nominal;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain = res.data.Result[0].Description;

							if (res.data.Result[0].PaymentMethod != 'cash'){
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
							}
							else {
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							}
	
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
							var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
							tanggal2.setMinutes(tanggal2.getMinutes() + 420);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
							console.log("yang ke 6");
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(nomPembayaranDB.toFixed(2), '.', ',', '.'); //res.data.Result[0].NominalPembayaran;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

							InstruksiPembayaranFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.DocumentId).then(
								function (resChg) {
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								},
								function (errChg) {
									console.log('error -->', errChg);
								}
							);
						}
					}
					else if (row.entity.DocumentType == "Refund") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
						if (row.entity.VendorName == "Unknown") {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "Unknown";
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = 0;
							$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = true;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = false;
							InstruksiPembayaranFactory.getDataBankComboBox()
								.then(
									function (resBank) {
										$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = resBank.data;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = res.data.Result[0].BankToId;
										angular.forEach(resBank.data, function (data) {
											if (data.value == res.data.Result[0].BankToId) {
												$scope.LihatInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = data.name;
											}
										});
									},
									function (errBank) {
										console.log("err=>", errBank);
									}
								);
						}
						else {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
						}
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund = res.data.Result[0].NamaPenerima;
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
						// var filter = [ {PIId: row.entity.DocumentId , 
						// 	RefundType : row.entity.RefundType, VendorId:  row.entity.VendorId }];
						$scope.LogicCariBtn_TopSearch();
						if ($scope.success == true) {
							if ($scope.TambahInstruksiPembayaran_TipeRefund != res.data.Result[0].RefundType) {
								$scope.TambahInstruksiPembayaran_TipeRefund = res.data.Result[0].RefundType;
							}
							else {
								$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed(1, uiGridPageSize);
							}

							InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.user.OutletId,
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, row.entity.DocumentId).then(
									function (resInt) {
										//console.log('data invoice -->' , JSON.stringify(resInv));
										$scope.TempDataGrid1 = resInt.data.Result;
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInt.data.Result;
									},
									function (errInv) {
										console.log('error -->', resInt);
									}
								);
							// cek data bank refund
							InstruksiPembayaranFactory.getDataBankComboBox()
							.then(
								function (resBankRef) {
									$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = resBankRef.data;
									console.log('tes dulu',$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund)
									var BankToidCopy = angular.copy(res.data.Result[0].BankToId)
									BankToidCopy = BankToidCopy.toString();
									setTimeout(function() { $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = BankToidCopy }, 200);
									
									// angular.forEach(resBankRef.data, function (data) {
									// 	resBankRef.data.some(function (data, i){
									// 	if (data.value == res.data.Result[0].BankToId) {
									// 		// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = data.name;
									// 		console.log("Cek Data bank refund>>>>>>", $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund)
									// 	}
									// });
								},
								function (errBank) {
									console.log("err=>", errBank);
								}
							);

							$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
							console.log("yang empat");
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(paymentAmtDB.toFixed(2), '.', ',', '.'); // res.data.Result[0].PaymentAmount;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
							var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
							tanggal2.setMinutes(tanggal2.getMinutes() + 420);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

							if (res.data.Result[0].PaymentMethod == 'Cash') {
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							}
							else {
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
							}

							$scope.TambahInstruksiPembayaran_Refund_Keterangan = res.data.Result[0].Description;


						}
					}
					else if (row.entity.DocumentType == "Interbranch") {
						var paymentAmtDB = res.data.Result[0].PaymentAmount;
						$scope.TotalNominal = res.data.Result[0].Nominal;
						$scope.LogicCariBtn_TopSearch();
						//alert('pas 1 ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran);
						//$scope.SetColumnUIGridDaftarTagihan();
						if ($scope.success == true) {
							InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.user.OutletId,
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, row.entity.DocumentId).then(
									function (resInt) {
										//console.log('data invoice -->' , JSON.stringify(resInv));
										$scope.TempDataGrid1 = resInt.data.Result;
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInt.data.Result;
									},
									function (errInv) {
										console.log('error -->', resInt);
									}
								);
								console.log("yang lima");
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(paymentAmtDB, '.', '.', ','); //res.data.Result[0].PaymentAmount;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
							var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
							tanggal2.setMinutes(tanggal2.getMinutes() + 420);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

							if (res.data.Result[0].PaymentMethod != 'cash') {
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
							}
							else {
								$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							}

						}
					}
					else if (row.entity.DocumentType == "Koreksi Administratif") {
						
						if (res.data.Result[0].PaymentMethod != 'cash'){
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = res.data.Result[0].AccountTo;
						}	
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif = 1;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text = res.data.Result[0].BillingNo;
						IPParentId = res.data.Result[0].Koreksi[0].IPParentId;
						OutletId = res.data.Result[0].Koreksi[0].OutletId;
						$scope.Show_TipeInput = false;
						console.log('GRID',$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data)
						var paymentAmtDB = res.data.Result[0].PaymentAmount;
						$scope.TotalNominal = res.data.Result[0].Nominal;
						InstruksiPembayaranFactory.getDataBankComboBox()
						.then(
							function (resBank) {
								$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = resBank.data;
								angular.forEach(resBank.data, function (data) {
									if (data.value == res.data.Result[0].BankToId) {
										$scope.LihatInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = data.name;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = data.value;
									}
								});
							},
							function (errBank) {
								console.log("err=>", errBank);
							}
						);
						if ($scope.success == true) {
							console.log('garapan 1951');
							// InstruksiPembayaranFactory.getIncomingPaymentFilter(1, 1000, res.data.Result[0].Koreksi[0].NomorIncomingPayment)
							// .then(
							console.log('payment id 1954 -> ', row.entity.DocumentId);
							InstruksiPembayaranFactory.getIncomingPaymentDetail(1, 1000, res.data.Result[0].Koreksi[0].NomorIncomingPayment, row.entity.DocumentId)
							.then(
								function (ress) {
									console.log('garapan 1954');
									console.log(ress.data[0], 'search ip');
									nominalPembayaranKoreksi = ress.data[0].NominalIncomingPayment
									// $scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data = ress.data[0];
									// var entity = ress.data[0];
									// var temp = {};
									// temp = entity;
									var data = [];
									data.push({entity: ress.data[0]});
									// console.log('DATA',angular.copy(entity));
									// console.log('DATA1',angular.copy(temp));
									console.log('DATA2',data);
									$scope.ChooseIncomingOrBilling(data[0]);
									ModuleType = ress.data[0].Module;
									NoBilling = data[0].entity.NomorBilling;
									$scope.DocumentIdPrint = res.data.Result[0].PaymentId;
									console.log('DocumentIdPrint',$scope.DocumentIdPrint)
									console.log($scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data, 'search ip 1');
									InstruksiPembayaranFactory.getInformasiIncomingPaymentAndBillingFilter(ress.data[0].Module, ress.data[0],1)
									.then(
										function (resss) {
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSPK_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoWO_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSO_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSO_Text = null;
											$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalBookingFee_Text = null;
											$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = [];
											tempBiayaDaftarIp = [];
											console.log(resss.data, 'wkwk');
											console.log(resss, 'wkwk1');
											$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
											if(ModuleType == 'Unit'){
												$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = resss.data[0].SO;
												$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSPK_Text = resss.data[0].FormSPKNo;
												$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = resss.data[0].SPKDate == null ? '' : resss.data[0].SPKDate;
												$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalBookingFee_Text = resss.data[0].NominalBookingFee;
											}else if(ModuleType == 'Parts'){
												$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = resss.data[0].Detail;
												// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSO_Text = resss.data[0].SalesOrderNo;
												// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = resss.data[0].SODate;
											}else if(ModuleType == 'Service'){
												for(var x in resss.data[0].Detail){
													if(resss.data[0].Detail[x].WONo == "0" || resss.data[0].Detail[x].WONo == null){
														resss.data[0].Detail[x].WONo = null;
														resss.data[0].Detail[x].JobDate = null;
													}
												}
												
												$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = resss.data[0].Detail;
												
												// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoWO_Text = resss.data[0].WONo;
												// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = resss.data[0].JobDate;
											}
											dataFilter = Object.assign($scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data);
											tempBiayaDaftarIp = resss.data[0].Biaya;
											$scope.SelectedInformasiKoreksi(res);
											$scope.informasiKoreksiAdministratifModeGrid(ModuleType);
										},
											function (err) {
												console.log("err=>", err);
											}
										);
								},
								function (err) {
									console.log("err=>", err);
								}
							);
								console.log("yang lima",paymentAmtDB);
								 
							// nominalPembayaranKoreksi = paymentAmtDB;
							
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(paymentAmtDB.toFixed(2), '.', ',', '.'); //res.data.Result[0].PaymentAmount;
							console.log("yang lima",$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = res.data.Result[0].Description
							var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
							tanggal2.setMinutes(tanggal2.getMinutes() + 420);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						}
					}
					else {  //other unit
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
						$scope.TotalNominal = res.data.Result[0].Nominal;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
						
						if (res.data.Result[0].PaymentMethod != 'cash'){
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = SplitBankTransfer(res.data.Result[0].PaymentMethod, 0);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = SplitBankTransfer(res.data.Result[0].PaymentMethod, 1);
						}
						else {
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						}

						$scope.LogicCariBtn_TopSearch();


						//alert('pas 1 ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran);
						//$scope.SetColumnUIGridDaftarTagihan();
						if ($scope.success == true) {
							InstruksiPembayaranFactory.getDataInvoiceList(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, row.entity.DocumentId, $scope.Action).then(
									function (resInv) {
										//console.log('data invoice -->' , JSON.stringify(resInv));
										$scope.TempDataGrid1 = resInv.data.Result;
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInv.data.Result;
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
											row.BungaDF = row.BungaDF.toFixed(2);
											console.log("bunga DF 1 >>>>>",row.BungaDF);
										});
									},
									function (errInv) {
										console.log('error -->', errInv);
									}
								);

							if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit") {
								InstruksiPembayaranFactory.getMasterAdvancePayment(1, uiGridPageSize,
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
									row.entity.DocumentId).then(
										function (resAdv) {
											// debugger;
											//$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[4].visible = false;
											// if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") {
											// 	$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = true;
											// 	$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
											// }
											// else {
											// 	$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.columnDefs[3].visible = false;
											// }
											$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data = resAdv.data.Result;

											// $scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.forEach(function (row) {
											// 	AdvSaldo += row.Nominal;
											// });
											$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = addSeparatorsNF(paymentAmtDB.toFixed(2), '.', ',', '.');

										},
										function (errAdv) {
											debugger;
											console.log('error -->', errAdv);
										}
									);
							}
							InstruksiPembayaranFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.DocumentId).then(
								function (resChg) {
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								},
								function (errChg) {
									console.log('error -->', errChg);
								}
							);
							console.log("nuse debug masuk other");
							InstruksiPembayaranFactory.getIncoiveTaxList(1, uiGridPageSize, row.entity.DocumentId, 'Unit').then(
								function (res) {
									$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
								},
								function (err) {
									console.log('error --> ', err);
								}
							);
							console.log("nominal pembayaran >>>>>>>>",$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
							console.log("1 >>>>>>", paymentAmtDB);
							console.log("2 >>>>>>>", paymentAmtDB.toFixed(2));
							console.log("3 >>>>>>>>>", addSeparatorsNF(paymentAmtDB.toFixed(2), '.',',','.'));
							// $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF(paymentAmtDB.toFixed(2), '.',',','.'); //aaaaaaac res.data.Result[0].PaymentAmount;
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran =roundUp(paymentAmtDB, 2).replace(/\B(?=(\d{3})+(?!\d))/g, ',');//aaaaaaac res.data.Result[0].PaymentAmount;							
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = res.data.Result[0].SaldoAnggaran;
							var tanggal2 = new Date(res.data.Result[0].PaymentDueDate);
							tanggal2.setMinutes(tanggal2.getMinutes() + 420);

							if (tanggal2.getFullYear() == 1) {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = '';
							}
							else {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = tanggal2;
							}

						}
					}
					$scope.TambahInstruksiPembayaran_TujuanPembayan_TipeInstruksiPembayaran_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening_Selected_Changed();
				},
				function (err) {
					console.log('error -->', err);
					$scope.success = false;
				}
			);

			if ($scope.success == true) {
				$scope.DocumentId = row.entity.DocumentId;
			}

			console.log("UnReversal", row.entity.UnReversal);
			if (row.entity.UnReversal) {
				$scope.bUnreversal = true;
			}
			else {
				$scope.bUnreversal = false;
			}

			// Pembulatan | Begin
			function roundUp(num, precision) {
				precision = Math.pow(10, precision)
				return Math.ceil(num * precision) / precision
			}
			// Pembulatan | End
			  
			

			$timeout(function(){
				// // $('#innerGrid  :input')
				// $("#innerGrid").each(function(){
				// 	$(this).find(':input').attr("disabled","disabled");
				// 	$(this).find(':button').attr("disabled","disabled");
				// });
				console.log("its WORRKKKK");
			},1000);

			//NOTO | EDIT DRAFT
			$scope.Show_TambahInstruksiPembayaran_RincianPembayaran_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance_EnableDisable = false;
			$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = false;
			//CR4 P2 Finance BungaDF Start
			$scope.Action = 'Lihat'
			//CR4 P2 Finance BungaDF End
		};

		$scope.Main_Cari = function () {
			//console.log('isi combo '+$scope.Main_NamaBranch );
			//if  ( ($scope.Main_TanggalStart != null) && ($scope.Main_TanggalEnd != null) )
			
			console.log(">>>>>>>>>>>>>>>", $scope.Main_NamaBranch);
			// $scope.TambahInstruksiPembayaran_NamaBranch = $scope.Main_NamaBranch;
			if ((new Date($scope.Main_TanggalStart) > new Date()) ||
				(new Date($scope.Main_TanggalEnd) > new Date())) {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal tidak bisa lebih besar dari hari ini.",
					type: "warning"
					
				});
			}
			else if (new Date($scope.Main_TanggalStart) > new Date($scope.Main_TanggalEnd)) {

				bsNotify.show({
					title: "Warning",
					content: "Tanggal awal tidak boleh lebih dari tanggal akhir.",
					type: "warning"
				});
			}
			else

			// if(  ( (isNaN($scope.Main_TanggalStart) != true) && 
			// 	 (angular.isUndefined($scope.Main_TanggalStart) != true) ) &&
			// 	  ( (isNaN($scope.Main_TanggalEnd) != true) && 
			// 	 (angular.isUndefined($scope.Main_TanggalEnd) != true) )&&
			// 	  ( (isNaN($scope.Main_NamaBranch) != true) && 
			// 	 (angular.isUndefined($scope.Main_NamaBranch) != true) )  ) 
			{
				if($scope.Main_NamaBranch == "" || $scope.Main_NamaBranch == undefined || $scope.Main_NamaBranch == null ){
					$scope.Main_NamaBranch = user.OutletId;
				}
				$scope.SetDataMainRequest(1, uiGridPageSize, $scope.formatDate($scope.Main_TanggalStart),
					$scope.formatDate($scope.Main_TanggalEnd),
					$scope.Main_NamaBranch);
			}
			$scope.Main_Daftar_PaymentInstruction_gridAPI.grid.clearAllFilters();

		};

		var vSearchText = '';
		var vSearchType = '';
		$scope.SetDataMainRequest = function (pagenumber, uiGridPageSize, startdate, endDate, branch) {

			if($scope.Main_NamaBranch == "" || $scope.Main_NamaBranch == undefined || $scope.Main_NamaBranch == null ){
				$scope.Main_NamaBranch = user.OutletId;
			}

			var parameter = startdate + '|' + endDate + '|' + branch;

			if (!angular.isUndefined($scope.Main_SearchDropdown)) {
				vSearchType = $scope.Main_SearchDropdown;
			}
			else {
				vSearchType = '';
			}

			vSearchText = $scope.Main_TextFilter;


			InstruksiPembayaranFactory.getDataForMainGrid(pagenumber, uiGridPageSize, parameter, vSearchText, vSearchType, $scope.Main_NamaBranch)
				.then(
					function (res) {
						//console.log('data --> ',JSON.stringify(res.data));
						$scope.Main_Daftar_PaymentInstruction_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
						//$scope.Main_Daftar_PaymentInstruction_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
						//$scope.Main_Daftar_PaymentInstruction_UIGrid.paginationPageSize = uiGridPageSize;
						//$scope.Main_Daftar_PaymentInstruction_UIGrid.paginationPageSizes = [uiGridPageSize];
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.right = function (str, chr) {
			return str.slice(str.length - chr, str.length);
		}

		$scope.SetComboBranch = function () {
			var ho;
			if ($scope.right($scope.user.OrgCode, 6) == '000000') {
				$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
				ho = $scope.user.OrgId;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" },
				{ name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" },
				{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" },
				{ name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
				{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
				{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }
					, { name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" }, { name: "Aksesoris Standard", value: "Aksesoris Standard" }];
			}
			else {
				$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
				ho = 0;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Aksesoris", value: "Aksesoris" },
				{ name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" },
				{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" },
				{ name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
				{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
				{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }
					, { name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" }, { name: "Aksesoris Standard", value: "Aksesoris Standard" }];
			}

			InstruksiPembayaranFactory.getDataBranch(ho).then(
				function (resBr) {
					$scope.optionsMain_NamaBranch = resBr.data.Result;
					//console.log(JSON.stringify(res.data));		
					//console.log('cur user ' + JSON.stringify(CurrentUser) );	
					$scope.optionsTambahInstruksiPembayaran_NamaBranch = resBr.data.Result;
					$scope.Main_NamaBranch = $scope.user.OutletId;
					$scope.TambahInstruksiPembayaran_NamaBranch = $scope.user.OutletId;
					//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch = branch;
					//$scope.Main_NamaBranch_EnableDisable = true;
					/*
					if (CurrentUser.role == "Finance" ) 
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					else
						
					*/
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};
		$scope.SetComboBranch();

		$scope.Main_BulkAction_SearchDropdown_Changed = function () {
			//alert('isi ' + JSON.stringify($scope.Main_Daftar_PaymentInstruction_UIGrid.data));

			if ($scope.Main_BulkAction_SearchDropdown == "Setuju") {
				//$scope.Main_Daftar_PaymentInstruction_UIGrid.data.forEach(function(row) {
				//if (row.isSelected==true) {

				angular.forEach($scope.Main_Daftar_PaymentInstruction_gridAPI.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolak_APId = value.DocumentId;
					$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
					if (value.ApprovalStatusDesc == "Diajukan") {
						$scope.CreateApprovalData("Disetujui");
						//alert('masuk sini ' + JSON.stringify($scope.ApprovalData));
						InstruksiPembayaranFactory.updateApprovalData($scope.ApprovalData)
							.then(
								function (res) {
									//alert("Berhasil Disetujui");
									bsNotify.show({
										title: "Success",
										content: 'Berhasil Disetujui.',
										type: 'success'
									});
									$scope.Batal_Lihat_TolakModal();
								},

								function (err) {
									//alert("Persetujuan gagal");
									bsNotify.show({
										title: "Gagal",
										content: 'Persetujuan gagal. ' + err.data.Message,
										type: 'danger'
									});
								}
							);
					}
				});;
			}
			else if ($scope.Main_BulkAction_SearchDropdown == "Tolak") {
				//$scope.Main_Daftar_PaymentInstruction_UIGrid.data.forEach(function(value) {
				//if (value.isSelected==true) {
				angular.forEach($scope.Main_Daftar_PaymentInstruction_gridAPI.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolak_APId = value.DocumentId;
					$scope.ModalTolak_Tanggal = value.DocumentDate;
					$scope.ModalTolak_NomorAP = value.DocumentNumber;
					$scope.ModalTolak_TipeOutgoing = value.DocumentType;
					$scope.ModalTolak_NamaVendor = value.VendorName;
					$scope.ModalTolak_Nominal = value.Nominal;
					$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
					$scope.ModalTolak_TanggalPengajuan = value.ApprovalCreateDate;
					$scope.ModalTolak_AlasanPengajuan = value.ReversalReason;

					if (value.ApprovalStatusDesc == "Diajukan") {
						$scope.ModalTolak_AlasanPenolakan = '';
						angular.element('#ModalTolakApproval').modal('setting', { closeable: false }).modal('show');

					}

				});
				//});
			}
		}

		$scope.CreateApprovalData = function (ApprovalStatus) {
			if (ApprovalStatus == "Disetujui") {
				$scope.ApprovalData = [
					{
						"ApprovalTypeName": $scope.ModalTolak_TipePengajuan + '|PaymentInstruction_TT',
						"RejectedReason": null,
						"DocumentId": $scope.ModalTolak_APId,
						"ApprovalStatusName": ApprovalStatus,
					}
				];
			}
			else {
				$scope.ApprovalData = [
					{
						"ApprovalTypeName": $scope.ModalTolak_TipePengajuan + '|PaymentInstruction_TT',
						"RejectedReason": $scope.ModalTolak_AlasanPenolakan,
						"DocumentId": $scope.ModalTolak_APId,
						"ApprovalStatusName": ApprovalStatus,
					}
				];
			}

		}

		$scope.Batal_Lihat_TolakModal = function () {
			angular.element('#ModalTolakApproval').modal('hide');
			$scope.Main_BulkAction = "";
			$scope.ClearActionFields();
			$scope.SetDataMainRequest(1, uiGridPageSize, $scope.formatDate($scope.Main_TanggalStart),
				$scope.formatDate($scope.Main_TanggalEnd),
				$scope.Main_NamaBranch);
		}

		$scope.Pengajuan_Lihat_TolakModal = function () {
			$scope.CreateApprovalData("Ditolak");
			if ($scope.ModalTolak_AlasanPenolakan == '' || $scope.ModalTolak_AlasanPenolakan == null) {
				bsNotify.show({
					title: "Warning",
					content: 'Alasan Penolakan wajib diisi!',
					type: 'warning'
				});
			}
			else {
				InstruksiPembayaranFactory.updateApprovalData($scope.ApprovalData)
					.then(
						function (res) {
							//alert("Berhasil tolak");
							bsNotify.show({
								title: "Success",
								content: 'Berhasil ditolak.',
								type: 'success'
							});
							$scope.Batal_Lihat_TolakModal();
						},

						function (err) {
							//alert("Penolakan gagal");
							bsNotify.show({
								title: "Gagal",
								content: 'Penolakan gagal.',
								type: 'danger'
							});
						}
					);
			}

		}

		$scope.ClearActionFields = function () {
			$scope.ModalTolak_APId = "";
			$scope.ModalTolak_Tanggal = "";
			$scope.ModalTolak_NomorAP = "";
			$scope.ModalTolak_TipeAP = "";
			$scope.ModalTolak_NamaVendor = "";
			$scope.ModalTolak_Nominal = "";
			$scope.ModalTolak_TipePengajuan = "";
			$scope.ModalTolak_TanggalPengajuan = "";
			$scope.ModalTolak_AlasanPengajuan = "";
		}



		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		//blok show main end

		$scope.CalculateNominalPembayaran = function () {
			var TotalChargesD = 0;
			var TotalChargesK = 0;
			var TotalChargesPjkD = 0;
			var TotalChargesPjkK = 0;
			var AdvSaldo = 0;
			var PPh21 = 0;
			var totalNomninal = 0;
			var TotalCharges = 0;

			
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.forEach(function (row) {
				if (row.DebitCredit == 'D' || row.DebitCredit == 'Debet') {
					TotalChargesD += row.Nominal;
				}
				else {
					TotalChargesK += row.Nominal;
				}
				console.log('TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid - 1', row.Nominal);
			});

			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.forEach(function (row) {
				if (row.DebetKredit == 'D' || row.DebetKredit == 'Debet') {
					TotalChargesPjkD += row.Nominal;
				}
				else {
					TotalChargesPjkK += row.Nominal;
				}
				console.log('TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid - 2', row.Nominal);
			});
			// Pajak sudah ada di
			// $scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.forEach(function (row) {
			// 	if (row.DebetKredit == 'D' || row.DebetKredit == 'Debet') {
			// 		TotalCharges += row.Nominal;
			// 	}
			// 	else {
			// 		TotalCharges -= row.Nominal;
			// 	}
			// 	console.log('TambahInstruksiPembayaran_DaftarPajak_UIGrid - 1', row.Nominal);
			// });

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.forEach(function (row) {
				TotalCharges += row.Nominal;
				console.log('TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid - 3', row.Nominal);
			});

			if (($scope.cr2 == true) && ($scope.TambahInstruksiPembayaran_DaftarAdvancePayment == true)) {
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.forEach(function (row) {
					var Nominal = 0;
					if (row.Nominal != "") {
						Nominal = row.Nominal;
					}
					TotalCharges -= Nominal;
					AdvSaldo += Nominal;
					console.log('TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid - 4', row.Nominal);
				});
			}

			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = AdvSaldo; //addSeparatorsNF(AdvSaldo, '.','.',','); //
			// $scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function(row) {
			// 	if ( row.radidata == true) {
			// 		console.log('isi ', row.Nominal);
			// 		TotalCharges += row.Nominal;
			// 		//isiGrid.push({InvoiceId: row.InvoiceId, BungaDF: row.BungaDF, AdvancePaymentId: row.AdvancePaymentId , AdvanceNominal : row.AdvanceNominal});
			// 	}
			// });
			if ($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != "")
				TotalCharges -= parseFloat($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment.toString().replace(/,/g, ''));
			//nuse tambah
			//var nominalPembayaranInternal = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran;

			if (isNaN($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21) == false) {
				PPh21 = $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21;
				if (isNaN($scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21) == false && PPh21 == 0) {
					PPh21 = $scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21;
				}
			}
			// totalNomninal = $scope.TotalNominal + TotalCharges - PPh21;
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales"){
				totalNomninal = $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi - PPh21 + TotalChargesD - TotalChargesK ;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund"){
				totalNomninal = $scope.TotalNominal;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction"){
				totalNomninal = $scope.HasilSaldoGen + TotalChargesPjkD - TotalChargesPjkK + TotalChargesD - TotalChargesK - $scope.ActiveRow.AdvanceNominal - PPh21;
			}else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service"){
				totalNomninal = $scope.TotalNominal + TotalChargesD - TotalChargesK;
			}else{
				totalNomninal = $scope.TotalNominal + TotalCharges + TotalChargesD - TotalChargesK - PPh21;
			}
			console.log("yang ke 1");
			if(totalNomninal < 0){
				totalNomninal = 0;
			}
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = totalNomninal; // $scope.TotalNominal + TotalCharges - PPh21;

			//nuse edit begin
			// if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
			// ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") )

			// {
			// InstruksiPembayaranFactory.getBiayaMaterai($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).then(
			// function(res) {
			// if (res.data.Total > 0 ) {
			// $scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal=res.data.Result[0].Nominal;
			// }
			// else {
			// $scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
			// }
			// },
			// function(err) {
			// console.log('Error ', err);
			// $scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
			// }
			// );
			// }
			//nuse edit end
			console.log("yang ke 2");
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == 'General Transaction' && $scope.AnggaranBelanja_isEnable) {
				if (parseFloat($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran) > parseFloat($scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1)) {
					$scope.notBudget = true;
				}
				else {
					$scope.notBudget = false;
				}
			}
			$scope.CalculateSaldo();
		}

		$scope.CalculateSaldo = function () {
			var TotalCharges = 0;
			var AdvSaldo = 0;
			var PPh21 = 0;
			var totalNomninal = 0;

			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.forEach(function (row) {
				if (row.DebetKredit == 'D' || row.DebetKredit == 'Debet') {
					TotalCharges += row.Nominal;
				}
				else {
					TotalCharges -= row.Nominal;
				}
				console.log('TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid - 2', row.Nominal);
			});
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.forEach(function (row) {
				TotalCharges += row.Nominal;
				console.log('TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid - 3', row.Nominal);
			});

			if (($scope.cr2 == true) & ($scope.TambahInstruksiPembayaran_DaftarAdvancePayment == true)) {
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.forEach(function (row) {
					var Nominal = 0;
					if (row.Nominal != "") {
						Nominal = row.Nominal;
					}
					TotalCharges -= Nominal;
					AdvSaldo += Nominal;
					console.log('TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid - 4', row.Nominal);
				});
			}

			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = AdvSaldo;
			if ($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment != "")
				TotalCharges -= parseFloat($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment.toString().replace(/,/g, ''));

			if (isNaN($scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21) == false) {
				PPh21 = $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21;
				if (isNaN($scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21) == false && PPh21 == 0) {
					PPh21 = $scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21;
				}
			}
			totalNomninal = $scope.TotalNominal + TotalCharges - PPh21;
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = totalNomninal;
			console.log("yang ke 3");
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == 'General Transaction' && $scope.AnggaranBelanja_isEnable) {
				if (parseFloat($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran) > parseFloat($scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1)) {
					$scope.notBudget = true;
				}
				else {
					$scope.notBudget = false;
				}
			}

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran.toFixed(2), '.', ',', '.');
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = addSeparatorsNF($scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo.toFixed(2), '.', ',', '.');
								
		}

		$scope.IsEditable = function () {
			return $scope.EditMode;
		};

		$scope.clearData = function(){
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = "";
			$scope.TambahInstruksiPembayaran_VendorFilter = "";
			$scope.TambahInstruksiPembayaran_VendorSelectKolom = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = "2";
			//clear komisi sales start	
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = "";
			//clear komisi sales end
		}

		$scope.ToTambahInstruksiPembayaran = function () {
			$scope.ActiveRow.AdvanceNominal = 0;
			$scope.DocumentIdPrint = null;
			$scope.tmpnonnpwp = 0;
			$scope.ProsesSubmit = false;
			$scope.Show_TipeInput =true;
			if (user.OrgCode.substring(3, 9) == '000000'){
				$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = true;
			}else{
				$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = false;
			}
			$scope.typeRefundDisabled = false;

			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "STNK/BPKB" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Order Pengurusan Dokumen" ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard") {
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [{ name: "Bea Materai", value: "Bea Materai" }];
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }];
			}else{
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [];
				$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
			}

			$scope.a = false;
			$scope.b = true;
			$scope.Show_TambahInstruksiPembayaran_EditDraft_CetakBPH = false;
			$scope.Show_TambahInstruksiPembayaran_RincianPembayaran_EnableDisable = true;
			$scope.Show_TambahInstruksiPembayaran_Cari_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran = new Date();
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total = 0;
			$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
			$scope.Show_LihatInstruksiPembayaran_CetakBPH = false;
			$scope.TambahInstruksiPembayaran_Bottom = true;
			$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = false;
			$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
			$scope.TambahInstruksiPembayaran_InputSO = "";
			$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = false;
			$scope.clearData();


			$scope.Show_EditDraft = false;
			$scope.show_draft_EditDraft = false;
			$scope.Show_TambahInstruksiPembayaran_EditDraft_button = false;
			$scope.TambahInstruksiPembayaran_NamaBranch = $scope.user.OrgId;
			$scope.TotalNominal = 0;
			//  $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" },{ name: "Aksesoris", value: "Aksesoris" },{ name: "Purna Jual", value: "Purna Jual" },{ name: "Karoseri", value: "Karoseri" },{ name: "STNK/BPKB", value: "STNK/BPKB" },{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" },{ name: "Swapping", value: "Swapping" },{ name: "Ekspedisi", value: "Ekspedisi" },{ name: "Parts", value: "Parts" },{ name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" },{ name: "Refund", value: "Refund" },{ name: "General Transaction", value: "General Transaction" },{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" } , { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" } 
			//  , { name: "Accrued Free Service", value: "Accrued Free Service" } , { name: "Interbranch", value: "Interbranch" }  ];
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
			if ($scope.EditMode == false) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
			}
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = null;
			// $scope.SetComboBranch();
			//	$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = [{ name: "Nomor Invoice Masuk", value: "Nomor Invoice Masuk" },{ name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" },{ name: "Nominal Billing", value: "Nominal Billing" }];
			//	$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search={};

			//$scope.TambahInstruksiPembayaran_TipeRefundOptions = [{ name: "Customer Guarantee", value: "Customer Guarantee" },{ name: "PPh 23", value: "PPh 23" },{ name: "PPN WAPU", value: "PPN WAPU" }, { name: "Titipan", value: "Titipan" }];
			$scope.TambahInstruksiPembayaran_TipeRefund = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
			$scope.DocumentId = 0;

			$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
			$scope.ShowTambahInstruksiPembayaran = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
			$scope.Show_Main_DaftarList = false;
			$scope.ShowLihatInstruksiPembayaran = false;
			$scope.EditMode = true;

			$scope.ShowTambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaran = false;
			$scope.ShowTambahInstruksiPembayaran_TujuanPembayaran_NomorInstruksiPembayaranReversal = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable = false;
			$scope.If_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
			$scope.If_TambahInstruksiPembayaran_DaftarAdvancePayment_NoAdvanceTambah = false;
			$scope.TambahInstruksiPembayaran_TanggalJatuhTempo_EnableDisable = false;
			$scope.Show_TambahInstruksiPembayaran_BiayaLainLain_Form = false;
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = true;
			//CR4 P2 Finance BungaDF Start
			$scope.Action = 'Tambah'
			//CR4 P2 Finance BungaDF End
		};

		$scope.BackToMainInstruksiPembayaran = function () {
			$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false; //CR2
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = 0;
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment = "";
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NoAdvPayment = "";
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Nominal = "";
			$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NamaVendor = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
			$scope.draft = '';
			$scope.Show_Main_DaftarList = true;
			$scope.DocumentId = 0;
			$scope.EditMode = false;
			$scope.TotalNominal = 0;
			$scope.SelectedGridId = -1;
			$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = "";
			$scope.TambahInstruksiPembayaran_Bottom = false;
			$scope.ShowTambahInstruksiPembayaran = false;
			$scope.ShowLihatInstruksiPembayaran = false;
			$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
			$scope.Show_LihatInstruksiPembayaran_CetakBPH = false;
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = "";
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = "";
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = "";
			$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
			$scope.TambahInstruksiPembayaran_BiayaLainLain_Keterangan = "";
			$scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit = {};
			$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = {};
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = null;

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = "";
			$scope.TambahInstruksiPembayaran_Refund_Keterangan = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
			$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = {};
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan = "";
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = 0;
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = "";
			$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];

			//general transaction
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction = {};
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = "";
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction = {};
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak = "";
			$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak = "";
			$scope.TambahInstruksiPembayaran_GeneralTransaction_KetObjekPajak = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = "";
			$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorAdvPayment = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = "";

			$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
			$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;

			$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = "2";
			// $scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Aksesoris", value: "Aksesoris" },{ name: "Purna Jual", value: "Purna Jual" },{ name: "Karoseri", value: "Karoseri" },{ name: "STNK/BPKB", value: "STNK/BPKB" },{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" },{ name: "Swapping", value: "Swapping" },{ name: "Ekspedisi", value: "Ekspedisi" },{ name: "Parts", value: "Parts" },{ name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" },{ name: "Refund", value: "Refund" },{ name: "General Transaction", value: "General Transaction" }];
			// $scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran=null;
			// $scope.SetComboBranch();
			// $scope.optionsLihatInstruksiPembayaran_DaftarInstruksiPembayaran_Search = [{ name: "Nomor Instruksi Pembayaran", value: "Nomor Instruksi Pembayaran" },{ name: "Nama Pelanggan/Vendor", value: "Nama Pelanggan/Vendor" },{ name: "Nominal Pembayaran", value: "Nominal Pembayaran" }];
			// $scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Search=null;
			//harja, begin
			$scope.TambahInstruksiPembayaran_TotalDaftarTagihan = 0;
			$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
			//harja , end
			$scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record = "";
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
			$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = false;
			$scope.Main_Cari();
			$scope.HideMenuKoreksiAdministratif();
		};

		$scope.DisableTambahInstruksiPembayaran_TujuanPembayaran_TopSearch = function () {

			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
			//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch=false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
			//$scope.Show_TambahInstruksiPembayaran_Tagihan=true;
			$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = false;

			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;

			$scope.TambahInstruksiPembayaran_ButtonTambah = false;
			$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService = false;
			$scope.Show_TambahInstruksiPembayaran_DaftarRangka_AccruedFreeService = false;

		};

		$scope.Disable_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = function () {
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
			//harja, begin
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWPShow_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = false;
			//$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = false;
			//$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
			$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
			$scope.TambahInstruksiPembayaran_DaftarPajak = false;
			//harja, end
		};

		$scope.Disable_Show_TambahInstruksiPembayaran_Refund = function () {
			$scope.Show_TambahInstruksiPembayaran_Refund = false;
			$scope.Show_TambahInstruksiPembayaran_Refund_WoSo = false;
			$scope.Show_TambahInstruksiPembayaran_Refund_Wo = false;
			$scope.Show_TambahInstruksiPembayaran_Refund_Ip = false;
			$scope.Show_TambahInstruksiPembayaran_Refund_Tambah = false;
		};

		$scope.TambahInstruksiPembayaran_TambahTagihan = function () {
			//harja, selain part dan unit, begin
			//var selected = $scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.selection.getSelectedRows();

			console.log('data grid', $scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data);
			$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.selection.getSelectedRows().forEach(function (row) {
				var found = 0;
				$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.forEach(function (obj) {
					console.log('isi grid -->', obj.InvoiceId);
					if (row.InvoiceId == obj.InvoiceId)
						found = 1;
				});
				if (found == 0) {

					if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") ||
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
						$scope.TambahInstruksiPembayaran_TotalDaftarTagihan += row.Saldo;
						$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.push({
							InvoiceId: row.InvoiceId,
							InvoiceReceivedDate: row.InvoiceReceivedDate,
							MaturityDate: row.MaturityDate,
							DPP: row.DPP,
							PPN: row.PPN,
							PPh22: row.PPh22,
							BungaDF: row.BungaDF,
							AdvancePaymentId: row.AdvancePaymentId,
							AdvanceNominal: row.AdvanceNominal,
							Saldo: row.Saldo
						});
					}
					else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund") ||
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "General Transaction") ||
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued")) {
						$scope.TambahInstruksiPembayaran_TotalDaftarTagihan += row.Nominal;
						$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.push({
							InvoiceId: row.InvoiceId,
							InvoiceReceivedDate: row.InvoiceReceivedDate,
							MaturityDate: row.MaturityDate,
							Nominal: row.Nominal,
							AdvancePaymentId: row.AdvancePaymentId,
							AdvanceNominal: row.AdvanceNominal,
							Saldo: row.Saldo
						});
					}
				}

				$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.selection.clearSelectedRows();
				//$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.splice($scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.indexOf(row), 1);

			});

			//harja, selain part dan unit, end

		};
		//harja, selain part dan unit, begin

		$scope.givePattern2 = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal = $scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal;
				return;
			}
		};

		$scope.TambahInstruksiPembayaran_TambahBiayaLainLain = function () {
			var nominal = $scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal.toString().replace(/,/g, '');
			var tmpFound = 0;
			if (!angular.isNumber(nominal) && (nominal != undefined)) {
				console.log("PaidAmount", nominal);
				nominal = parseFloat(nominal);
				console.log("PaidAmount", nominal);
			}
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.forEach(function (obj) {
				if ($scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain == obj.ChargesType){
					tmpFound = 1;
				}
			});

			if(tmpFound == 0){
				if (($scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain != null) &&
					($scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit != null) &&
					(nominal > 0)) {
					console.log(' isi nominal - ', nominal);
					$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = true;
					$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.push({
						ChargesId: 0,
						ChargesType: $scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain,
						DebitCredit: $scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit,
						Nominal: nominal,
						Description: $scope.TambahInstruksiPembayaran_BiayaLainLain_Keterangan
					});
					$scope.CalculateNominalPembayaran();
				}
				else {
					//alert("Isian biaya lain lain tidak lengkap !");
					bsNotify.show({
						title: "Warning",
						content: 'Isian biaya lain lain tidak lengkap .',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
			}
			
		};

		$scope.TambahInstruksiPembayaran_SimpanDraft = function () {
			$scope.typeRefundDisabled = false;
			$scope.ValidateSaveInstruksi(1, 0);
		};

		$scope.TambahInstruksiPembayaran_Pengajuan = function () {
			$scope.ValidateSaveInstruksi(0, 0);
		};
		$scope.tmpSelectRow = false;
		$scope.ValidateSaveInstruksi = function (status, print) {
			var validDueDate;
			$scope.ProsesSubmit = true;
			var ceksaldominus = 0;
			for(var i in $scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data){
				console.log("cek data minus");
				if($scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data[i].AccruedBalance < 0){
					ceksaldominus++
				}
			}
			// $scope.tmpNpwp = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP.replace(/\./g, "").replace(/\-/g,"");
			console.log('TMPNPWP>>>>>>>>>>>>>>>>>>>>>>', $scope.tmpNpwp);
			// if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" && $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "1"){
			// 	if($scope.tmpNpwp.length < 15 || $scope.tmpNpwp.length > 15){
			// 		bsNotify.show({
			// 			title: "Warning",
			// 			content: 'Field NPWP harus terisi minimal 15 karakter',
			// 			type: 'warning'
			// 		});
			// 		$scope.ProsesSubmit = false;
			// 		return false;
			// 	}
			// }
			
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo instanceof Date && !isNaN($scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo.valueOf())) {
				validDueDate = true;
			}
			else {
				validDueDate = false;
				//console.log('isi bukan tanggal :' , formatDate);
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate" && $scope.TotalNominal == 0 ){
				bsNotify.show({
					title: "Warning",
					content: 'Nominal Subsidi tidak boleh 0',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate" && ($scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan == null || $scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan == undefined || $scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan == "") ){
				bsNotify.show({
					title: "Warning",
					content: 'Keterangan Harus diisi!',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service" && $scope.tmpSelectRow == false){
				bsNotify.show({
					title: "Warning",
					content: 'Invoice Rujukan belum di Pilih.',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service" && ceksaldominus != 0 ){
				bsNotify.show({
					title: "Warning",
					content: 'Ada saldo Accrued sebelumnya yang lebih kecil dari Nominal tagihan Free Service',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}

			if($scope.draft != "Ya" ){
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" && $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.length == 0) {
					bsNotify.show({
						title: "Warning",
						content: 'Data Pengajuan tidak sesuai',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}	
			}else{
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" && $scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.length == 0) {
					bsNotify.show({
						title: "Warning",
						content: 'Data Pengajuan tidak sesuai',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}	
			}
						
			if(($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund == undefined)){
				bsNotify.show({
					title: "Warning",
					content: 'Nama Penerima Refund harus diisi',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" && $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "2"){
				console.log('Lanjut Ke Function>>>>>>>>')
				$timeout(function(){
					$scope.Penjagaan_ByName(status, print);
				})
				// $scope.ProsesSubmit = false;
				return false;
			}
			else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" && $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "1"){
				console.log('Lanjut Ke Function 222222222222222222>>>>>>>>')
				$timeout(function(){
					$scope.Penjagaan_ByName(status, print);
				})
				// $scope.ProsesSubmit = false;
				return false;
			}

			// Cr4 P2 finanace koreksi Start
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Bank Transfer" &&
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan == '')){
					bsNotify.show({
						title: "Warning",
						content: 'Nomor Rekening Tujuan harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && ($scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == null || 
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == '')){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Penerima harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Bank Transfer" &&
					($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi == '')){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Bank harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
			}
			// Cr4 P2 finanace koreksi End
			

			if (($scope.TambahInstruksiPembayaran_Bottom_NamaFinance == "" || $scope.TambahInstruksiPembayaran_Bottom_NamaFinance == null)
				&& status == 0) {
				//alert('Nama Finance harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Nama Finance harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else if (($scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan == "" || $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan == null)
				&& status == 0) {
				//alert('Nama Supervisor harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Nama Pimpinan harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else if (validDueDate == false) {
				//alert('Tanggal Jatuh Tempo harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Tanggal Jatuh Tempo harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}else if($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" || $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null || $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == undefined || $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "0" ){
				bsNotify.show({
					title: "Warning",
					content: 'Metode Pembayaran Harus dipilih!',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else {
				$scope.SaveInstruksiPembayaran_Pengajuan(status, print);
			}
			console.log("Refresh Data >>>");
			$scope.Main_Cari();
		}

		//penjagaan By Name
		$scope.Penjagaan_ByName = function(status, print){
			console.log('Masuk Ke Function>>>>>>>>')
			var validDueDate;
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo instanceof Date && !isNaN($scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo.valueOf())) {
				validDueDate = true;
			}
			else {
				validDueDate = false;
			}

			if($scope.draft == "Ya"){
				$scope.tmpNpwp = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP.replace(/\./g, "").replace(/\-/g,"");
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "1"){
				if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor == undefined){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Vendor Harus diisi',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP == undefined){
					bsNotify.show({
						title: "Warning",
						content: 'NPWP Harus diisi',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}else if($scope.tmpNpwp.length < 15 || $scope.tmpNpwp.length > 15){
					bsNotify.show({
						title: "Warning",
						content: 'Field NPWP harus terisi minimal 15 karakter',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat == undefined){
					bsNotify.show({
						title: "Warning",
						content: 'Alamat Harus diisi',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota == undefined){
					bsNotify.show({
						title: "Warning",
						content: 'Kabupaten/Kota Harus diisi',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon == undefined){
					bsNotify.show({
						title: "Warning",
						content: 'Telpon Harus diisi',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
				}
			}

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction == null){
				bsNotify.show({
					title: "Warning",
					content: 'Nama GL Transaction Harus diisi',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == null){
				bsNotify.show({
					title: "Warning",
					content: 'Keterangan Harus diisi',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.length <= 0){
				bsNotify.show({
					title: "Warning",
					content: 'Tidak Ada Data Daftar General Transaction!',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			
			// Cr4 P2 finanace koreksi Start
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Bank Transfer" &&
			($scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan == '')){
				bsNotify.show({
					title: "Warning",
					content: 'Nomor Rekening Tujuan harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && ($scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == null || 
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == '')){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Penerima harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" && $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Bank Transfer" &&
					($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi == null || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi == '')){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Bank harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					return false;
			}
			// Cr4 P2 finanace koreksi End

			if (($scope.TambahInstruksiPembayaran_Bottom_NamaFinance == "" || $scope.TambahInstruksiPembayaran_Bottom_NamaFinance == null)) {
				//alert('Nama Finance harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Nama Finance harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else if (($scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan == "" || $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan == null)) {
				//alert('Nama Supervisor harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Nama Pimpinan harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" || $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null || $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == undefined)){
			//alert('Nama Supervisor harus diisi !');
			bsNotify.show({
				title: "Warning",
				content: 'Metode Pembayaran harus diisi !',
				type: 'warning'
			});
			$scope.ProsesSubmit = false;
			return false;
			}
			else if (validDueDate == false) {
				//alert('Tanggal Jatuh Tempo harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Tanggal Jatuh Tempo harus diisi !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
				return false;
			}
			else {
				$scope.SaveInstruksiPembayaran_Pengajuan(status, print);
			}
		}

		$scope.PrintBPH = function (docId, Metode) {
			if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash") {
				InstruksiPembayaranFactory.cetakBPHCash(docId, 1, 1, $scope.user.OrgId).then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);
						console.log("pdf", fileURL);
						printJS(fileURL);
						//printJS({printable: file,showModal:true,type: 'pdf'})
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
			else {
				InstruksiPembayaranFactory.cetakBPHBankTransfer(docId, 1, 1, $scope.user.OrgId).then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);
						console.log("pdf", fileURL);
						printJS(fileURL);
						//printJS({printable: file,showModal:true,type: 'pdf'})
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
		}

		$scope.SaveInstruksiPembayaran_Pengajuan = function (status, print) {
			var data;
			var replaceString;
			var isiTax = [];
			var taxgrid;
			var tanggal;
			var formatDate;
			var SubmitMsg = "";
			var tglduedate;
			var formatdueDate;
			if (status == 1) {
				SubmitMsg = 'Data berhasil disimpan';
			}
			else {
				SubmitMsg = 'Data berhasil diajukan';
			}
			debugger;
			tglduedate = new Date($scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo);
			tglduedate.setMinutes(tglduedate.getMinutes() + 420);
			formatdueDate = $scope.formatDate(tglduedate);
			console.log('isi tanggal :', formatdueDate);
			// ========================
			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran
			var copyStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
			if($scope.draft != "Ya"){
				console.log("hasil Simpan >>>>>", 	angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran) );
				if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran != 0){
					var newStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).replace(/\./g, '');
					var newFloat = angular.copy(newStr).replace(/\,/g, '.');			
				}else{
					var newStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
					var newFloat = angular.copy(newStr);	
				}	
				// var newStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).replace(/\./g, '');
				// var newFloat = angular.copy(newStr).replace(/\,/g, '.');	
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = angular.copy(newFloat);
				console.log("hasil Simpan 1 >>>>>", 	angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran) );		
			}else{
				if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund"){
					console.log('duitttt',angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran))
					var newNom = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).replace(/\./g, '');
					var newNominal = angular.copy(newNom).replace(/\,/g, '.');
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = angular.copy(newNominal);
					console.log('nominal simpan', $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran)
				}else{
						var newStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).replace(/\./g, '');
						var newFloat = angular.copy(newStr).replace(/\,/g, '.');	
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = angular.copy(newFloat);
						console.log("hasil Simpan Edit draft GT >>>>>", 	$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran );	
				}
				
			}
			// ========================

			// ============================ BEGIN | NOTO | 190410 | Bank & Rekening [3] | Metodebayar + Bank & Rekening ============================
			var metodeBayar = $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran
				// + '|' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank
				+ '|' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening;
			// ============================ END | NOTO | 190410 | Bank & Rekening [3] | Metodebayar + Bank & Rekening ============================

			if (angular.isUndefined($scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan)) {
				NoRekTujuan = "";
			}
			else {
				NoRekTujuan = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan;
			}

			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {

				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction == "" ||
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction == null) && status == 0) {
					//alert('Nama GL harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: "Nama GL harus diisi !",
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
				}
				else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == "" ||
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == null) && status == 0) {
					//alert('Keterangan harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: "Keterangan harus diisi !",
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
				}
				else if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
				}
				else {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "1") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = 0;
					}

					$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.forEach(function (row) {
						if (row.TaxDate instanceof Date && !isNaN(row.TaxDate.valueOf())) {
							tanggal = new Date(row.TaxDate);
							tanggal.setMinutes(tanggal.getMinutes() + 420);
							formatDate = $scope.formatDate(tanggal);
							console.log('isi tanggal :', formatDate);
						}
						else {
							formatDate = "";
							console.log('isi bukan tanggal :', formatDate);
						}

						console.log($scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption)

						var temp_taxid = 0
						for (var i=0; i<$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption.length; i++) {
							if (row.TaxType == $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption[i].TaxType) {
								temp_taxid = $scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption[i].TaxTypeId
							}
						}

						isiTax.push({
							// TaxId: 0,
							TaxId: temp_taxid,
							PajakTipe: row.TaxType.replace("%", ""),
							TaxFee: row.TaxFee,
							DebetKredit: row.DebetKredit,
							Nominal: row.Nominal,
							TaxNumber: row.TaxNumber,
							TaxDate: formatDate
						});
					});

					replaceString = JSON.stringify(isiTax);
					//debugger;
					//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					data = [{
						VendorType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val,
						IncomingInvoiceNumber: $scope.currentIncomingInvoiceNumber,
						GLNo: $scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction,
						CostCenterGrid: JSON.stringify($scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data),
						NominalPPh21: $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21,
						// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
						MetodeBayar: metodeBayar,
						AccountTo: NoRekTujuan,
						SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
						PaymentDueDate: formatdueDate,
						NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
						Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
						TaxGrid: replaceString,
						SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
						FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
						IsDraft: status,
						Description: $scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan,
						PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
						VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
						PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
						PiId: $scope.DocumentId,
						VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor,
						VendorAddress: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat,
						VendorCity: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota,
						VendorPhone: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon,
						VendorNPWP: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP,
						AdvancePaymentId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment,
						ChargesGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data),
						Nominal: $scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo
					}];

					console.log('data yang dikirim ke BE ===>',data);
					//besok do sini , test coy. 7/6/2018
					InstruksiPembayaranFactory.submitDataGeneralTransaction(data).then(
						function (res) {
							debugger;
							//$scope.loading=false;
							//alert(SubmitMsg);
							bsNotify.show({
								title: "Success",
								content: SubmitMsg,
								type: 'success'
							});
							if (print == 1) {
								$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
							}
							$scope.DocumentIdPrint = res.data.Result[0].PiId;
							$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
							console.log(res);
							$scope.BackToMainInstruksiPembayaran();
							$scope.Main_Cari();
						},
						function (err) {
							debugger;
							console.log("err=>", err);
							bsNotify.show({
								title: "Warning",
								content: err.data.Message,
								type: "warning"
							});
							$scope.ProsesSubmit = false;
							$scope.DocumentIdPrint = -1;
							$scope.MetodeBayarPrint = "";
						});
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				// if  ( ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") || 
				// 	 ($scope.TambahInstruksiPembayaran_TipeRefund == "PPh 23") )
				// {
				if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
				}
				else if ($scope.TambahInstruksiPembayaran_Refund_Keterangan == "" || $scope.TambahInstruksiPembayaran_Refund_Keterangan == null) {
					bsNotify.show({
						title: "Warning",
						content: 'Keterangan harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
				}
                else if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash" &&
                          ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" && 
                            ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund == ""
				          || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund == null))) {
   				      bsNotify.show({
   				      	title: "Warning",
   				      	content: 'Nama Penerima (Refund) harus diisi !',
   				      	type: 'warning'
   				      });
								$scope.ProsesSubmit = false;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
                }
                else if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash" &&
                          ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund" && 
                            ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerima == ""
				          || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerima == null))) {
				      bsNotify.show({
				      	title: "Warning",
				      	content: 'Nama Penerima harus diisi !',
				      	type: 'warning'
				      });
							$scope.ProsesSubmit = false;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = copyStr;
                }

				else {
					for(var x in $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data){
						delete $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data[x].temp
					}
					console.log('UANGGG', angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran))
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
					data = [{}];
					if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash") {
						console.log('ini apa ?', $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran);

						if($scope.draft == "Ya"){
							data = [{
								PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								DataGrid: JSON.stringify($scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data),
								NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								Penerima: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund,
								// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
								MetodeBayar: metodeBayar,
								AccountTo: NoRekTujuan,
								SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
								PaymentDueDate: formatdueDate,
								Description: $scope.TambahInstruksiPembayaran_Refund_Keterangan,
								SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
								FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund,
								IsDraft: status,
								PiId: $scope.DocumentId,
								BankToId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund
							}
							];
						}else{
							data = [{
								PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								DataGrid: JSON.stringify($scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data),
								NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								Penerima: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund,
								// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
								MetodeBayar: metodeBayar,
								AccountTo: NoRekTujuan,
								SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
								PaymentDueDate: formatdueDate,
								Description: $scope.TambahInstruksiPembayaran_Refund_Keterangan,
								SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
								FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund,
								IsDraft: status,
								PiId: $scope.DocumentId,
								BankToId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund
							}
							];
						}
					}
					else {

						if($scope.draft == "Ya"){
							data = [{
								PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								DataGrid: JSON.stringify($scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data),
								NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								Penerima: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund,
								// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
								MetodeBayar: metodeBayar,
								AccountTo: NoRekTujuan,
								SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
								PaymentDueDate: formatdueDate,
								Description: $scope.TambahInstruksiPembayaran_Refund_Keterangan,
								SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
								FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund,
								IsDraft: status,
								PiId: $scope.DocumentId,
								BankToId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund
							}
							];
						}else{
							data = [{
								PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								DataGrid: JSON.stringify($scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data),
								NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
								Penerima: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund,
								// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
								MetodeBayar: metodeBayar,
								AccountTo: NoRekTujuan,
								SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
								PaymentDueDate: formatdueDate,
								Description: $scope.TambahInstruksiPembayaran_Refund_Keterangan,
								SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
								FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund,
								IsDraft: status,
								PiId: $scope.DocumentId,
								BankToId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund
							}
							];
						}
						
					}

					//alert('submit ' + JSON.stringify(data));
					InstruksiPembayaranFactory.submitDataRefund(data)
						.then(
							function (res) {
								//$scope.loading=false;
								console.log(res);
								if (print == 1) {
									$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
								}
								$scope.DocumentIdPrint = res.data.Result[0].PiId;
								$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
								//alert(SubmitMsg);
								bsNotify.show({
									title: "Success",
									content: SubmitMsg,
									type: 'success'
								});
								$scope.ProsesSubmit = false;
								$scope.BackToMainInstruksiPembayaran();
							},
							function (err) {
								console.log("err=>", err);
								if (err != null) {
									bsNotify.show({
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									});
								}
								else {
									bsNotify.show({
										title: "Gagal",
										content: 'Data Gagal Disimpan.',
										type: 'danger'
									});
								}
								$scope.DocumentIdPrint = -1;
								$scope.MetodeBayarPrint = "";
								$scope.ProsesSubmit = false;
								return false;
							}
						);
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
				var isiGridFS = [];
				var SubsidiAmount;
				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data.forEach(function (row) {
					if (row.SubsidyId == $scope.SelectedGridId) {
						isiGridFS.push({ SubsidyId: row.SubsidyId, SubsidyAmount: row.SubsidyAmount, SubsidyType: row.SubsidyType });
						SubsidiAmount = row.SubsidyAmount;
					}
				});
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

				if ((isiGridFS.length > 0) || status == 1) {
					data = [{
						PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
						//					VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
						Nominal: SubsidiAmount,
						SubsidiGrid: JSON.stringify(isiGridFS),
						ChargesGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data),
						NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
						Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
						// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
						MetodeBayar: metodeBayar,
						AccountTo: NoRekTujuan,
						SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
						PaymentDueDate: formatdueDate,
						Description: $scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan,
						SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
						FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
						SOId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
						VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
						PiId: $scope.DocumentId,
						BillingNo: $scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling,
						PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
						IsDraft: status
					}
					];
					//alert('msk ' + JSON.stringify(data));
					InstruksiPembayaranFactory.submitDataAccrued(data)
						.then(
							function (res) {
								//$scope.loading=false;
								if (print == 1) {
									$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
								}
								$scope.DocumentIdPrint = res.data.Result[0].PiId;
								$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
								console.log(res);
								//alert(SubmitMsg);
								bsNotify.show({
									title: "Success",
									content: SubmitMsg,
									type: 'success'
								});
								$scope.BackToMainInstruksiPembayaran();
							},
							function (err) {
								console.log("err=>", err);
								if (err != null) {
									//alert(err.data.Message);
									bsNotify.show({
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									});
								}
								else {
									//alert('Data Gagal Disimpan');
									bsNotify.show({
										title: "Gagal",
										content: 'Data Gagal Disimpan.',
										type: 'danger'
									});
								}
								$scope.DocumentIdPrint = -1;
								$scope.MetodeBayarPrint = "";
								$scope.ProsesSubmit = false;
							}
						);
				}
				else {
					//alert('Data Subsidi belum dipilih !');
					bsNotify.show({
						title: "Warning",
						content: 'Data Subsidi belum dipilih.',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {

				if
					($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor == "2" &&
					($scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor == 0 ||
						typeof $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor == "undefined") && status == 0) {
					//alert ('Vendor harus dipilih !');
					bsNotify.show({
						title: "Warning",
						content: 'Vendor harus dipilih !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert ('Tipe Pembayaran harus di pilih !');
					bsNotify.show({
						title: "Warning",
						content: 'Tipe Pembayaran harus di pilih !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor != "Pilih Vendor" && (
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama == "" ||
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat == "" ||
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota == "" ||
					//				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon == "" ||
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP == "") && status == 0) {
					//alert ('keterangan vendor harus di isi semua !');
					bsNotify.show({
						title: "Warning",
						content: 'keterangan vendor harus di isi semua !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					data = [{
						PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
						VendorId: $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor,
						Nominal: $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi,
						//DataGrid:JSON.stringify(TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data),
						ChargesGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data),
						NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
						Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
						// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
						MetodeBayar: metodeBayar,
						AccountTo: NoRekTujuan,
						SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
						PaymentDueDate: formatdueDate,
						Description: $scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain,
						SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
						FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
						VendorType: $scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val,
						SOId: $scope.getSOId,
						PiId: $scope.DocumentId,
						AkumulasiDPP: $scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP,
						NominalPPh21: $scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21,
						PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
						IsDraft: status,
						VendorName: $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama,
						VendorAddress: $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat,
						VendorCity: $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota,
						VendorPhone: $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon,
						VendorKTP: $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP,
						BillingNo: $scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling
					}
					];
					InstruksiPembayaranFactory.submitDataAccrued(data)
						.then(
							function (res) {
								//$scope.loading=false;
								if (print == 1) {
									$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
								}
								$scope.DocumentIdPrint = res.data.Result[0].PiId;
								$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
								console.log(res);
								//alert(SubmitMsg);
								bsNotify.show({
									title: "Success",
									content: SubmitMsg,
									type: 'success'
								});
								$scope.BackToMainInstruksiPembayaran();
							},
							function (err) {
								console.log("err=>", err);
								$scope.DocumentIdPrint = -1;
								$scope.MetodeBayarPrint = "";
								if (err != null) {
									//alert(err.data.Message);
									bsNotify.show({
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									});
									$scope.ProsesSubmit = false;
								}
								else {
									//alert('Data Gagal Disimpan');
									bsNotify.show({
										title: "Gagal",
										content: 'Data Gagal Disimpan.',
										type: 'danger'
									});
									$scope.ProsesSubmit = false;
								}
							}
						);
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
				if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else {
					var isiGridFS = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.selection.getSelectedRows().forEach(function (row) {
						isiGridFS.push({ InvoiceId: row.InvoiceId });
					});
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					if (isiGridFS.length > 0 || status == 1) {
						data = [{
							PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							//	VendorId: $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor,
							//	Nominal:$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total,
							DataGrid: JSON.stringify(isiGridFS),
							ChargesGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data),
							NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
							// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
							MetodeBayar: metodeBayar,
							AccountTo: NoRekTujuan,
							SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
							PaymentDueDate: formatdueDate,
							Description: "", //$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain,
							SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
							FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
							VendorType: $scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val,
							SOId: 0,
							PiId: $scope.DocumentId,
							CostCenterGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data),
							PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
							IsDraft: status
						}
						];

						InstruksiPembayaranFactory.submitDataAccruedFS(data)
							.then(
								function (res) {
									//$scope.loading=false;
									console.log(res);
									if (print == 1) {
										$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
									}
									$scope.DocumentIdPrint = res.data.Result[0].PiId;
									$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
									//alert(SubmitMsg);
									bsNotify.show({
										title: "Success",
										content: SubmitMsg,
										type: 'success'
									});
									$scope.BackToMainInstruksiPembayaran();
								},
								function (err) {
									console.log("err=>", err);
									$scope.DocumentIdPrint = -1;
									$scope.MetodeBayarPrint = "";
									if (err != null) {
										//alert(err.data.Message);
										bsNotify.show({
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										});
										$scope.ProsesSubmit = false;
									}
									else {
										//alert('Data Gagal Disimpan');
										bsNotify.show({
											title: "Gagal",
											content: 'Data Gagal Disimpan',
											type: 'danger'
										});
									}
									$scope.ProsesSubmit = false;
								}
							);
					}
					else { //alert ('Daftar Tagihan belum ada yang dipilih'); 
						bsNotify.show({
							title: "Warning",
							content: 'Daftar Tagihan belum ada yang dipilih.',
							type: 'warning'
						});
						$scope.ProsesSubmit = false;
					}
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
				if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else {
					var isiGrid = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
						//console.log('isi grid -->', obj.PaymentId);
						if (row.radidata == true) {
							isiGrid.push({ IncomingInstructionId: row.IncomingInstructionId, Nominal: row.Nominal });
						}
					});

					// $scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.selection.getSelectedRows().forEach(function(row) {
					// 	isiGrid.push({InvoiceId: row.InvoiceId, BungaDF: row.BungaDF, AdvancePaymentId: row.AdvancePaymentId , AdvanceNominal : row.AdvanceNominal});
					// });
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					if (isiGrid.length > 0 || status == 1) {
						var data;
						debugger;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran;
						data = [{
							PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							DataGrid: JSON.stringify(isiGrid),
							//ChargesGrid:"",
							NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
							// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
							MetodeBayar: metodeBayar,
							AccountTo: NoRekTujuan,
							SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
							PaymentDueDate: formatdueDate,
							SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
							FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
							PiId: $scope.DocumentId,
							PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
							IsDraft: status
						}
						];
						InstruksiPembayaranFactory.SubmitInterbranch(data)
							.then(
								function (res) {
									//$scope.loading=false;
									console.log(res);
									if (print == 1) {
										$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
									}
									$scope.DocumentIdPrint = res.data.Result[0].PiId;
									$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
									//alert(SubmitMsg);
									bsNotify.show({
										title: "Success",
										content: SubmitMsg,
										type: 'success'
									});
									$scope.BackToMainInstruksiPembayaran();
								},
								function (err) {
									console.log("err=>", err);
									if (err != null) {
										// alert(err.data.Message);
										var msg = err.data.Message;
										bsNotify.show({
											title: "Gagal",
											content: msg,
											type: 'danger'
										});
										$scope.ProsesSubmit = false;
									}
									else {
										//alert('Data Gagal Disimpan');
										bsNotify.show({
											title: "Gagal",
											content: 'Data Gagal Disimpan.',
											type: 'danger'
										});
										$scope.ProsesSubmit = false;
									}
								}
							);
					}
					else {
						//alert("Semua data harus diisi !");
						bsNotify.show({
							title: "Warning",
							content: 'Semua data harus diisi !',
							type: 'warning'
						});
						$scope.ProsesSubmit = false;
					}
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif") {
				$scope.CalculateNominalPembayaranKoreksi();
				console.log('SAVE SEBELUM NOMINAL', $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
				var newStr = angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran).replace(/\./g, '');
				var newFloat = angular.copy(newStr).replace(/\,/g, '.');	
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = angular.copy(newFloat);
				console.log('SAVE Sesudah NOMINAL', $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);
				if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}else if(($scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == "" ||
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima == null) && $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == 'Cash'
				&& status == 0){
					bsNotify.show({
						title: "Warning",
						content: 'Nama Penerima harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}else if(($scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == "" ||
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan == null) && status == 0){
					bsNotify.show({
						title: "Warning",
						content: 'Keterangan harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}else {
					var isiGrid = [];
					$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.forEach(function (row) {
						//console.log('isi grid -->', obj.PaymentId);
							row.DK = (row.DK == 'Debet' || row.DK == 'D' ) ? 'D' : 'K';
							isiGrid.push({ ChargesType: row.Type, Nominal: row.Nominal, DebitCredit: row.DK});
						
					});
					$timeout(function(){
						var data;
					
						console.log('NOMINAL KOREKSI JADI =>',$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran)
						$scope.DocumentIdPrint = status == 1 ? $scope.DocumentIdPrint = $scope.DocumentIdPrint : $scope.DocumentIdPrint = $scope.DocumentIdPrint;
						console.log('DocumentIdPrint',$scope.DocumentIdPrint)
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

						data = [{
							OutletId:$scope.user.OrgId,
							IPParentId: IPParentId,
							Module: ModuleType,
							PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							PaymentAmount: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
							MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
							Description: $scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan,
							PaymentDueDate: formatdueDate,
							SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
							FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
							IsDraft: status,
							Biaya:isiGrid,
							BillingNo: NoBilling,
							PaymentId: $scope.DocumentIdPrint,
							AccountTo: NoRekTujuan,
							BankToId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == 'Cash' ?
							'' : $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi
						}
						];
						console.log('DATA SAVE =>', data)
						InstruksiPembayaranFactory.SubmitKoreksiAdministratif(data)
						.then(
							function (res) {
								//$scope.loading=false;
								console.log(res);
								if (print == 1) {
									$scope.PrintBPH(res.data[0].PaymentId, res.data[0].MetodeBayar);
								}
								$scope.DocumentIdPrint = res.data[0].PaymentId;
								$scope.MetodeBayarPrint = res.data[0].MetodeBayar;
								//alert(SubmitMsg);
								bsNotify.show({
									title: "Success",
									content: SubmitMsg,
									type: 'success'
								});
								$scope.BackToMainInstruksiPembayaran();
							},
							function (err) {
								console.log("err=>", err);
								
								if (err != null) {
									var MsgFromBe = err.data.Message.includes("Incoming Payment sudah dikoreksi");
									var MsgFromBe1 = err.data.Message.includes("Status Incoming Payment: Reversal Disetujui");
									var MsgFromBe2 = err.data.Message.includes("Koreksi Incoming Payment sudah diajukan");
									if(MsgFromBe){
										bsNotify.show({
											title: "Gagal",
											content: 'Incoming Payment Sudah Pernah Dikoreksi',
											type: 'danger'
										});
									}else if(MsgFromBe1){
										bsNotify.show({
											title: "Gagal",
											content: 'Status Incoming Payment: Reversal Disetujui',
											type: 'danger'
										});
									}else if(MsgFromBe2){
										bsNotify.show({
											title: "Gagal",
											content: 'Koreksi Incoming Payment sudah diajukan',
											type: 'danger'
										});
									}else{
										bsNotify.show({
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										});
									}
									$scope.ProsesSubmit = false;
								}
								else {
									//alert('Data Gagal Disimpan');
									bsNotify.show({
										title: "Gagal",
										content: 'Data Gagal Disimpan.',
										type: 'danger'
									});
									$scope.ProsesSubmit = false;
								}
							}
						);
					},1000);
				
				}
			}
			else  // unit dan lain-lain
			{
				if (($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "" ||
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == null) && status == 0) {
					//alert('Metode Pembayaran harus diisi !');
					bsNotify.show({
						title: "Warning",
						content: 'Metode Pembayaran harus diisi !',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;					
				}
				else {
					var isiGrid = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
						//console.log('isi grid -->', obj.PaymentId);
						if (row.radidata == true) {
							isiGrid.push({ InvoiceId: row.InvoiceId, BungaDF: row.BungaDF, AdvancePaymentId: row.AdvancePaymentId, AdvanceNominal: row.AdvanceNominal,
								TenorBungaDF: row.TenorBungaDF,PercentageBungaDF: row.PercentageBungaDF});
						}
					});

					// $scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.selection.getSelectedRows().forEach(function(row) {
					// 	isiGrid.push({InvoiceId: row.InvoiceId, BungaDF: row.BungaDF, AdvancePaymentId: row.AdvancePaymentId , AdvanceNominal : row.AdvanceNominal});
					// });
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.saparator($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					if (isiGrid.length > 0 || status == 1) {
						var data;
						//debugger;
						//var a = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran.toString();
						data = [{
							PaymentInstructionType: $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
							VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor,
							Nominal: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							DataGrid: JSON.stringify(isiGrid),
							ChargesGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data),
							NominalBayar: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran,
							Penerima: $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima,
							// MetodeBayar: $scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran,
							MetodeBayar: metodeBayar,
							AccountTo: NoRekTujuan,
							SaldoAnggaran: $scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran,
							PaymentDueDate: formatdueDate,
							SupervisorName: $scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan,
							FinanceName: $scope.TambahInstruksiPembayaran_Bottom_NamaFinance,
							PiId: $scope.DocumentId,
							PaymentInstructionDate: $scope.formatDate($scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran),
							IsDraft: status,
							AdvPaymentGrid: JSON.stringify($scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data),
							//Cr4 p2 Finance bungadf Start
							VendorIdBungaDF: ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit" 
							|| $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts") ? $scope.VendorIdBungaDF : null,//Cr4 p2 Finance bungadf End
						}
						];
						InstruksiPembayaranFactory.submitDataInvoice(data)
							.then(
								function (res) {
									//$scope.loading=false;
									console.log(res);
									if (print == 1) {
										$scope.PrintBPH(res.data.Result[0].PiId, res.data.Result[0].MetodeBayar);
									}
									$scope.DocumentIdPrint = res.data.Result[0].PiId;
									$scope.MetodeBayarPrint = res.data.Result[0].MetodeBayar;
									//alert(SubmitMsg);
									bsNotify.show({
										title: "Success",
										content: SubmitMsg,
										type: 'success'
									});
									$scope.BackToMainInstruksiPembayaran();
								},
								function (err) {
									console.log("err=>", err);
									if (err != null) {
										// alert(err.data.Message);
										var msg = err.data.Message;
										bsNotify.show({
											title: "Gagal",
											content: msg,
											type: 'danger'
										});
										$scope.ProsesSubmit = false;
									}
									else {
										//alert('Data Gagal Disimpan');
										bsNotify.show({
											title: "Gagal",
											content: 'Data Gagal Disimpan',
											type: 'danger'
										});
										$scope.ProsesSubmit = false;
									}
								}
							);
					}
					else {
						//alert("Semua data harus diisi !");
						bsNotify.show({
							title: "Warning",
							content: 'Semua data harus diisi !',
							type: 'warning'
						});
						$scope.ProsesSubmit = false;
					}
				}
			}
			$scope.Main_Daftar_PaymentInstruction_UIGrid = [];
			$scope.Main_Cari(1, uiGridPageSize);
			console.log("data di sini nih >>>>>");
		};

		$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			multiselect: false,
			enableColumnResizing: true,
			useCustomPagination: false,
			canSelectRows: false,
			enableSelectAll: false,
			columnDefs: [
				{ name: "PajakId", displayName: "Id Pajak", field: "TaxId", visible: false },
				{ name: "JenisPajak", displayName: "Jenis Pajak", field: "TaxType", enableCellEdit: false, enableHiding: false },
				{ name: "TarifPajak", displayName: "Tarif Pajak", field: "TaxFee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "DebetKredit", displayName: "Debet / Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal Pajak", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "NoDokPajak", displayName: "Nomor Dokumen Pajak", field: "TaxNumber", enableCellEdit: false, enableHiding: false },
				{ name: "TglDokPajak", displayName: "Tanggal Dokumen Pajak", field: "TaxDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "NoReferensi", displayName: "Nomor Referensi", field: "RefferenceNumber", enableCellEdit: false, enableHiding: false }
			],
			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarPajak_gridAPI = gridApi;
			}
		};

		$scope.DeleteGridDaftarTagihanDibayarRow = function (row) {
			var index = $scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data.splice(index, 1);

			$scope.TambahInstruksiPembayaran_TotalDaftarTagihan -= row.entity.Nominal;
		};

		$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: true,
			useExternalPagination: true,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: true,
			columnDefs: [
				{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InvoiceNumber", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
				{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
				{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
				{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI = gridApi;
//fffffffffffffffffff
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch"){
						console.log("cek next page interbranch")
							$scope.NextPageInterbranch(pageNumber, pageSize);
						}
					});
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});

				/* --- ini remark sementara 
				gridApi.selection.on.rowSelectionChanged($scope,function(row){
					if (row.isSelected==true)
					{
						$scope.TotalNominal = row.entity.Nominal;
						$scope.CalculateNominalPembayaran();
						//alert('masuk sini');
						$scope.PrepareGridTaxList(1, uiGridPageSize,  row);
						//console.log("data diinput --", row.entity.InvoiceId);
					}
				});	 */
			}
		};
		$scope.NextPageInterbranch = function (pageNumber, pageSize){
			InstruksiPembayaranFactory.getDataInterbranch(pageNumber, pageSize, $scope.user.OutletId, 'Interbranch', $scope.DocumentId).then(
				function (resFS) {
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resFS.data.Result;
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = resFS.data.Total;//cccc
				},
				function (errFS) {
					console.log('Error -->', errFS);
				}
			);
		}

		$scope.PrepareGridTaxList = function (start, limit, row) {
			var InvoiceId = 0;

			if (!angular.isUndefined(row.entity)) {
				InvoiceId = row.entity.InvoiceId;
			}
			else if (!angular.isUndefined(row)) {
				InvoiceId = row.InvoiceId;
			}

			InstruksiPembayaranFactory.getIncoiveTaxList(start, limit, InvoiceId, '').then(
				function (res) {
					$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
					$scope.CalculateNominalPembayaran();
				},
				function (err) {
					console.log('error --> ', err);
				}
			);
		};

		// $scope.TambahInstruksiPembayaran_Refund_ButtonFilter = function() {
		// 	if ( ($scope.TambahInstruksiPembayaran_Refund_TextFilter != "") &&
		// 		($scope.TambahInstruksiPembayaran_Refund_SearchDropDown != "") )
		// 	{	
		// 		var nomor;		
		// 		var value = $scope.TambahInstruksiPembayaran_Refund_TextFilter;

		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.clearAllFilters();
		// 		if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor SPK/WO/SO Batal" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Registrasi BP PPh 23" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Registrasi SSP" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Incoming Payment" ) {
		// 			nomor = 1;
		// 		}				
		// 		else if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal SPK/WO/SO Batal" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Registrasi BP PPh 23" ||
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Registrasi SSP" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Incoming Payment") {
		// 			nomor =2;
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nominal Down Payment" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Billing" || 
		// 			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nominal Selisih Kelebihan") {
		// 			nomor =3;
		// 		}	
		// 		else if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Billing") {
		// 			nomor = 4;
		// 		}								
		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.columns[nomor].filters[0].term=value;
		// 	}
		// 	else {
		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.clearAllFilters();
		// 	}		
		// }

		$scope.Main_Filter = function () {
			if (($scope.Main_TextFilter != "") &&
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != "")) {
				var nomor;
				var value = $scope.Main_TextFilter;
				$scope.Main_Daftar_PaymentInstruction_gridAPI.grid.clearAllFilters();
				if ($scope.Main_SearchDropdown == "Nomor Instruksi Pembayaran") {
					nomor = 3;
				}
				if ($scope.Main_SearchDropdown == "Tipe Instruksi Pembayaran") {
					nomor = 5;
				}
				if ($scope.Main_SearchDropdown == "Nama Pelanggan/Vendor") {
					nomor = 8;
				}
				if ($scope.Main_SearchDropdown == "Nominal Pembayaran") {
					nomor = 9;
				}
				if ($scope.Main_SearchDropdown == "Status Draft") {
					nomor = 10;
				}
				if ($scope.Main_SearchDropdown == "Nomor Invoice Dari Vendor") {
					nomor = 6;
				}
				$scope.Main_Daftar_PaymentInstruction_gridAPI.grid.columns[nomor].filters[0].term = value;
			}
			else {
				$scope.Main_Daftar_PaymentInstruction_gridAPI.grid.clearAllFilters();
				//alert("inputan filter belum diisi !");
			}
		};

		// OLD Filter
		// $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Filter_Click = function () {
		// 	if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text != "") &&
		// 		($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != "")) {
		// 		var nomor;
		// 		var value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text;
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.clearAllFilters();
		// 		console.log('filter text -->', $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text);

		// 		if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nama Branch Penerima") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Tanggal Incoming Payment") {
		// 				nomor = 3;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nomor Incoming Payment") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nominal Pembayaran") {
		// 				nomor = 4;
		// 				//console.log('filter text -->',parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text) );
		// 				//$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.columns[3].filters[0].term=parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text);
		// 			}
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") {
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nomor Invoice Masuk") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Tanggal Jatuh Tempo") {
		// 				nomor = 5;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nominal Invoice") {
		// 				nomor = 6;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nomor Invoice Dari Vendor") {
		// 				nomor = 4;
		// 				//console.log('filter text -->',parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text) );
		// 				//$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.columns[3].filters[0].term=parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text);
		// 			}
		// 		}
		// 		else {
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nomor Invoice Masuk") {
		// 				nomor = 3;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Tanggal Jatuh Tempo") {
		// 				nomor = 6;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "DPP") {
		// 				nomor = 8;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nominal Invoice") {
		// 				nomor = 7;
		// 				//console.log('filter text -->',parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text) );
		// 				//$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.columns[3].filters[0].term=parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text);
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search == "Nomor Invoice Dari Vendor") {
		// 				nomor = 5;
		// 				//console.log('filter text -->',parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text) );
		// 				//$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.columns[3].filters[0].term=parseInt($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text);
		// 			}
		// 		}
		// 		console.log("nomor --> ", nomor);
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text;
		// 	}
		// 	else {
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI.grid.clearAllFilters();
		// 		//alert("inputan filter belum diisi !");
		// 	}
		// };

		// NEW FILTER
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Filter_Click = function () {
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif"){
				var tempData = Object.assign(dataFilterincoming)
				var tmpSearch = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search ;
				var tmpText = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text;	
				$scope.filterKoreksi(tmpSearch,tmpText,tempData,1);
			}
			else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Interbranch"){
					if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text != "") &&
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != null && $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != undefined)) {
						value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text;
						console.log("Input Text Filter ==>>", $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text );
						console.log("ComboBox Filter ==>>", $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search );
						
						InstruksiPembayaranFactory.getDataInvoiceList(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran +'|'+ $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text +'|'+ 
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search, $scope.DocumentId, $scope.Action)
						.then(
							function (res) {
								console.log("res =>", res.data.Result);
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = res.data.Result.length;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSize = uiGridPageSize;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSizes = [10,25,50];
							}   
						);

					}
					else {
					
						InstruksiPembayaranFactory.getDataInvoiceList(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran +'|'+ $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text +'|'+ 
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search, $scope.DocumentId, $scope.Action)
						.then(
							function (res) {
								console.log("res =>", res.data.Result);
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = res.data.Result.length;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSize = uiGridPageSize;
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSizes = [10,25,50];
							}   
						);

						//alert("inputan filter belum diisi !");
					}
			}else{
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text != "") &&
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != null && $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != undefined)) {
				value = $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text;
				console.log("Input inter Text Filter ==>>", $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text );
				console.log("ComboBox inter Filter ==>>", $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search );
				
				InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_NamaBranch,
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran +'|'+ $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text +'|'+ 
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search, $scope.DocumentId)
				.then(
					function (res) {
						console.log("res =>", res.data.Result);
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = res.data.Total;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSize = uiGridPageSize;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSizes = [10,25,50];
					}   
				);

			}
			else {
			
				InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_NamaBranch,
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran +'|'+ $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text +'|'+ 
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search, $scope.DocumentId)
				.then(
					function (res) {
						console.log("res =>", res.data.Result);
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = res.data.Result.length;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSize = uiGridPageSize;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.paginationPageSizes = [10,25,50];
					}   
				);

				//alert("inputan filter belum diisi !");
			}
			}
			
		}

		$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			showColumnFooter: true,
			showGridFooter: true,
			columnDefs: [
				{ name: "AdvancedPaymentId", displayName: "ID", field: "AdvancedPaymentId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "AdvancedPaymentNumber", displayName: "No Advance Payment", field: "AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false },
				//{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{
					name: 'Nominal', field: 'Nominal',
					enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP'
				},
				{ name: "FrameNo", displayName: "No Rangka", field: "FrameNo", enableCellEdit: false, enableHiding: false, visible: false },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false, visible: false,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarAdvancePayment(row)">Hapus</a>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_gridAPI = gridApi;
			}
		}

		$scope.DeleteGridDaftarAdvancePayment = function (row) {
			var index = $scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data.splice(index, 1);
			$scope.CalculateNominalPembayaran();
		};

		$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				{ name: "ChargesId", displayName: "ID", field: "ChargesId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "ChargesType", enableCellEdit: false, enableHiding: false },
				{ name: "DebitKredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "Description", displayName: "Description", field: "Description", enableCellEdit: false, enableHiding: false, visible: false },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarBiayaLainLain(row)">Hapus</a>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI = gridApi;
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});
			}
		};

		$scope.DeleteGridDaftarBiayaLainLain = function (row) {
			var index = $scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.splice(index, 1);
			$scope.CalculateNominalPembayaran();
		};

		$scope.ModalTambahInstruksiPembayaran_GetVendorKomisi_UIGrid = {
			// 		paginationPageSizes: [25,50,100],
			// 		paginationPageSize: 25,
			// 		useCustomPagination: false,
			// 		useExternalPagination : false,
			// 		// enableFiltering: true,
			// 		// enableSelectAll: false,
			// 		// displaySelectionCheckbox: false,
			// 		// enableFullRowSelection : true,
			// 		multiselect:false,
			// //		canSelectRows: true,

			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			enableFiltering: true,
			enableColumnResizing: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: true,
			columnDefs: [
				{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "DataCode", displayName: "Code", field: "VendorCode", enableCellEdit: false, visible: false },
				//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
				{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false },
				{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false },
				{ name: "Phone", displayName: "Phone", field: "Phone", enableCellEdit: false, enableHiding: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalTambahInstruksiPembayaran_GetVendorKomisi_gridAPI = gridApi;
			}
		};

		$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid = {
			// paginationPageSizes: [25,50,100],
			// paginationPageSize: 25,
			// useCustomPagination: false,
			// useExternalPagination : false,
			// displaySelectionCheckbox: false,
			// canSelectRows: true,
			// enableFiltering: true,
			// enableSelectAll: false,
			// multiselect:false,
			// enableFullRowSelection : true,

			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			canSelectRows: true,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: true,
			columnDefs: [
				{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
				//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
				{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false },
				{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI = gridApi;
			}
		};

		// get data free service
		$scope.getDataFree = function(start, limit, Id, Code, Type){
			InstruksiPembayaranFactory.getDataForAccruedFreeService(start, limit, Id, Code, Type, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
				function (resFS) {
					$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = resFS.data.Result;
					$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.totalItems = resFS.data.Total;
					// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSize = limit;
					// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSizes = [50, 100, 200];
					// $scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
				},
				function (errFS) {
					console.log('Error -->', errFS);
				}
			);
		}

		$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid = {
			// paginationPageSizes: [25,50,100],
			// paginationPageSize: 25,
			// useCustomPagination: false,
			// useExternalPagination : false,
			// enableFiltering: true,
			// enableSelectAll: false,
			// displaySelectionCheckbox: false,
			// enableFullRowSelection : true,
			// multiselect:false,
			// canSelectRows: true,
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			canSelectRows: true,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: true,


			columnDefs: [
				{ name: "AdvancePaymentId", displayName: "Id Advance Payment", field: "AdvancedPaymentId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "AdvancedPaymentNumber", displayName: "Nomor Advanced Payment", field: "AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "AdvancedPaymentType", displayName: "Advanced Payment Type", field: "AdvancedPaymentType", enableCellEdit: false, enableHiding: false },
				{ name: "MetodeBayar", displayName: "Metode Bayar", field: "MetodeBayar", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Maturity Date", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "VendorName", displayName: "Vendor Name", field: "VendorName", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "FrameNo", displayName: "FrameNo", field: "FrameNo", enableCellEdit: false, enableHiding: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_gridAPI = gridApi;
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});
			}
		};

		$scope.TambahInstruksiPembayaran_Refund_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: null,
			useCustomPagination: true,
			useExternalPagination: true,
			enableFiltering: true,
			enableSelectAll: false,
			enableColumnResizing: true,
			displaySelectionCheckbox: false,
			enableFullRowSelection: false,
			multiselect: true,
			canSelectRows: false,
			columnDefs: [
				{ name: "WOSOSPKNumber", displayName: "Nomor SPK/App/WO/SO Batal", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "WOSPKDate", displayName: "Tanggal SPK/WO/SO Batal", field: "DocumentDate", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "WOSOSPKId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false },
				{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_Refund_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {		
					$scope.GetAllData(pageNumber, pageSize);
					console.log("pageNumber", pageNumber)
					console.log("pagesize", pageSize)
				});		
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});
			}	
		};

		// hapus Lihat tabel 
		$scope.DeleteGrid_DaftarRefund_Lihat =function(row){
			$scope.TotalNominal = 0;
			var index = $scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.indexOf(row.entity);
			$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.splice(index, 1);

			angular.forEach($scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data, function (value) {
				$scope.TotalNominal += value.Nominal
			});

			// $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total -= row.entity.Nominal;			
			// $scope.TotalNominal = $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total;

			$scope.CalculateNominalPembayaran();
		}

		$scope.DeleteGrid_DaftarRefund = function (row) {
			$scope.TotalNominal = 0;
			var index = $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.splice(index, 1);

			angular.forEach($scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data, function (value) {
				$scope.TotalNominal += value.Nominal
			});

			// $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total -= row.entity.Nominal;			
			// $scope.TotalNominal = $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total;

			$scope.CalculateNominalPembayaran();
		};

		$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			enableFullRowSelection: true,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				{
					name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
					cellTemplate: '<div><input name="AcrcruedSubsidiSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.SubsidiGridSelected(row)"></div>'
				},
				{ name: "SubsidyId", displayName: "SubsidyId", field: "SubsidyId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "Subsidi", displayName: "Subsidi", field: "SubsidyType", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal Subsidi", field: "SubsidyAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],
			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI = gridApi;
			}
		};

		$scope.SubsidiGridSelected = function (row) {
			$scope.TotalNominal = row.entity.SubsidyAmount;
			$scope.CalculateNominalPembayaran();
			$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data.forEach(function (dt) {
				if (dt.SubsidyId == $scope.SelectedGridId) {
					dt.radidata = false;
				}
			});
			$scope.SelectedGridId = row.entity.SubsidyId;
			row.entity.radidata = true;
			//alert(' dipilih ' + row.entity.SubsidyType);
		};

		$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			showGridFooter: true,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			enableFullRowSelection: false,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				//debug harja
				{ name: "RefundId", displayName: "RefundId", field: "RefundId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "RefundType", displayName: "Tipe Refund", field: "RefundType", enableCellEdit: false, enableHiding: false },
				{
					name: "ReferenceNumber", displayName: "Nomor Referensi", field: "ReferenceNumber", enableCellEdit: false, enableHiding: false,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
				},
				{
					name: 'Nominal', field: 'Nominal',
					enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP'
				},
				{ name: "ReferenceId", displayName: "ReferenceId", field: "ReferenceId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "NominalDP", displayName: "NominalDP", field: "NominalDP", enableCellEdit: false, enableHiding: false, visible: false, visible: false},
				{ name: "NominalGuarantee", displayName: "NominalGuarantee", field: "NominalGuarantee", enableCellEdit: false, enableHiding: false, visible: false },
				{ 
					//customer guarantee cr4 Finance
					name: "temp", displayName: "Keterangan", field: "temp", enableCellEdit: false, enableHiding: false
						// cellTemplate: '<div ng-show="row.entity.TypeGuarantee == 1">Titipan</div><div ng-show="row.entity.TypeGuarantee == 2">Booking Fee dan Down Payment</div>'
				},
				{ name: "NominalDPPure", displayName: "NominalDPPure", field: "NominalDPPure", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "NominalGuaranteePure", displayName: "NominalGuaranteePure", field: "NominalGuaranteePure", enableCellEdit: false, enableHiding: false, visible: false },
				{
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false, visible: true,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarRefund(row)">hapus</a>'
				}
				// debug end				
				// {name:"RefundId", displayName:"RefundId", field:"RefundId", enableCellEdit: false, enableHiding: false, visible:false},
				// {name:"RefundType", displayName:"Tipe Refund", field:"RefundType", enableCellEdit: false, enableHiding: false},
				// {name:"ReferenceNumber", displayName:"Nomor Referensi", field:"ReferenceNumber", enableCellEdit: false, enableHiding: false , 
				// 	footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>' 
				// },
				// {name:"Nominal", displayName:"Nominal", field:"Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP',
				// 	aggregationType: uiGridConstants.aggregationTypes.sum	,
				// 	footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | rupiahCIP }}</div>' 
				// },
				// {name:"ReferenceId", displayName:"ReferenceId", field:"ReferenceId", enableCellEdit: false, enableHiding: false, visible:true},
				// {name:"InnerType", displayName:"InnerType", field:"InnerType", enableCellEdit: false, enableHiding: false, visible:true},
				// {name:"Action", displayName:"Action",field:"Action", enableCellEdit:false,  visible:true,
				// 	cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarRefund(row)">hapus</a>' 
				// }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					// $scope.NextPageLihat (pageNumber, pageSize);
				});
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
				//console.log("masuk setting grid upload dan tambah");
				//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
				//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
				//}
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//});
			}
		};

		// grid lihat pph23 Refund aaaaaaaaaaaaaaaa
		$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: true,
			useExternalPagination: true,
			enableFiltering: true,
			enableSelectAll: false,
			showGridFooter: true,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			enableFullRowSelection: false,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				//debug harja
				{ name: "RefundId", displayName: "RefundId", field: "RefundId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "RefundType", displayName: "Tipe Refund", field: "RefundType", enableCellEdit: false, enableHiding: false },
				{
					name: "ReferenceNumber", displayName: "Nomor Referensi", field: "ReferenceNumber", enableCellEdit: false, enableHiding: false,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
				},
				{
					name: 'Nominal', field: 'Nominal',
					enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP'
				},
				{ name: "ReferenceId", displayName: "ReferenceId", field: "ReferenceId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "NominalDP", displayName: "NominalDP", field: "NominalDP", enableCellEdit: false, enableHiding: false, visible: false},
				{ name: "NominalGuarantee", displayName: "NominalGuarantee", field: "NominalGuarantee", enableCellEdit: false, enableHiding: false, visible: false },
				{ 
					//customer guarantee cr4 Finance
					name: "temp", displayName: "Keterangan", field: "temp", enableCellEdit: false, enableHiding: false
						// cellTemplate: '<div ng-show="row.entity.TypeGuarantee == 1">Titipan</div><div ng-show="row.entity.TypeGuarantee == 2">Booking Fee dan Down Payment</div>'
				},
				{ name: "NominalDPPure", displayName: "NominalDPPure", field: "NominalDPPure", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "NominalGuaranteePure", displayName: "NominalGuaranteePure", field: "NominalGuaranteePure", enableCellEdit: false, enableHiding: false, visible: false },
				{
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false, visible: true,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarRefund_Lihat(row)">hapus</a>'
				}
				// debug end				
				// {name:"RefundId", displayName:"RefundId", field:"RefundId", enableCellEdit: false, enableHiding: false, visible:false},
				// {name:"RefundType", displayName:"Tipe Refund", field:"RefundType", enableCellEdit: false, enableHiding: false},
				// {name:"ReferenceNumber", displayName:"Nomor Referensi", field:"ReferenceNumber", enableCellEdit: false, enableHiding: false , 
				// 	footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>' 
				// },
				// {name:"Nominal", displayName:"Nominal", field:"Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP',
				// 	aggregationType: uiGridConstants.aggregationTypes.sum	,
				// 	footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | rupiahCIP }}</div>' 
				// },
				// {name:"ReferenceId", displayName:"ReferenceId", field:"ReferenceId", enableCellEdit: false, enableHiding: false, visible:true},
				// {name:"InnerType", displayName:"InnerType", field:"InnerType", enableCellEdit: false, enableHiding: false, visible:true},
				// {name:"Action", displayName:"Action",field:"Action", enableCellEdit:false,  visible:true,
				// 	cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarRefund(row)">hapus</a>' 
				// }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					$scope.NextPageLihat (pageNumber, pageSize);
				});
			}
		};

		$scope.ClearRefund = function(){
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = "";
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_Refund_Keterangan ="";
			$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = "";
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = "";
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPenerimaRefund = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = "";
		}

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Click = function () {
			$scope.ClearRefund();
			if ($scope.EditMode == true) {
				angular.element('#ModalTambahInstruksiPembayaran_GetSOVendorCustomer').modal('setting', { closeable: false }).modal('show');

				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data Pelanggan";
					$scope.optionsTambahInstruksiPembayaran_VendorSelectKolom = [{ name: "Name", value: "Name" }, { name: "Alamat", value: "Alamat" }];
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.columnDefs = [
						{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "DataCode", displayName: "Code", field: "VendorCode", enableCellEdit: false, visible: false },
						//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
						{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false },
						{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false },
						{ name: "Phone", displayName: "Phone", field: "Phone", enableCellEdit: false, enableHiding: false }
					];
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data SO";
					$scope.optionsTambahInstruksiPembayaran_VendorSelectKolom = [{ name: "Code", value: "Code" }];
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.columnDefs = [
						{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
						//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
						{ name: "DataName", displayName: "Code", field: "Name", enableCellEdit: false, enableHiding: false }
					];
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data SO";
					$scope.optionsTambahInstruksiPembayaran_VendorSelectKolom = [{ name: "Code", value: "Code" }];
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.columnDefs = [
						{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
						//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
						{ name: "DataNamesubs", displayName: "Code", field: "NameSPK", enableCellEdit: false, enableHiding: false }
					];
				}
				else {
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data Vendor";
					$scope.optionsTambahInstruksiPembayaran_VendorSelectKolom = [{ name: "Code", value: "Code" }, { name: "Name", value: "Name" }, { name: "Address", value: "Address" }];
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.columnDefs = [
						{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
						//{name:"AdvancedPaymentNumber", displayName:"Nomor Advanced Payment", field:"AdvancedPaymentNumber", enableCellEdit: false, enableHiding: false},
						{ name: "DataCode", displayName: "Code", field: "VendorCode", enableCellEdit: false, enableHiding: false },
						{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false },
						{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false }
					];
				}

			}
		};

		$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Click = function () {
			if ($scope.EditMode == true && $scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor == "2") {
				InstruksiPembayaranFactory.getSOVendorList('mediator', '').then(
					function (res) {
						$scope.ModalTambahInstruksiPembayaran_GetVendorKomisi_UIGrid.data = res.data.Result;
						angular.element('#ModalTambahInstruksiPembayaran_GetVendorKomisi').modal('show');
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}
		}

		$scope.Batal_TambahInstruksiPembayaran_GetSOVendorCustomer = function () {
			//$('.modal').modal('hide');
			angular.element('#ModalTambahInstruksiPembayaran_GetSOVendorCustomer').modal('hide');
		}

		$scope.Pilih_TambahInstruksiPembayaran_GetSOVendorCustomer = function () {
			if ($scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.selection.getSelectedCount() > 0) {
				$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.selection.getSelectedRows().forEach(function (row) {
					if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate"){
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = row.NameSPK;
					}else{
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = row.Name;
					}
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = row.Id;
				});
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == "Unknown") {
					// $scope.Show_TambahInstruksiPembayaran_EditDraft_cari = true;
					$scope.LogicCariBtn_TopSearch();
					$scope.TambahInstruksiPembayaran_TipeRefund = "Unknown";
					$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed(1, uiGridPageSize);
				}
				// else {
				// 	$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = false;
				// }
				$('.modal').modal('hide');
				angular.element('#ModalTambahInstruksiPembayaran_GetSOVendorCustomer').modal('hide');

			}
			else {
				//alert('Belum ada data yang dipilih');
				bsNotify.show({
					title: "Warning",
					content: 'Belum ada data yang dipilih !',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
			}
		}

		$scope.Pilih_TambahInstruksiPembayaran_GetVendorKomisi = function () {
			if ($scope.ModalTambahInstruksiPembayaran_GetVendorKomisi_gridAPI.selection.getSelectedCount() > 0) {
				$scope.ModalTambahInstruksiPembayaran_GetVendorKomisi_gridAPI.selection.getSelectedRows().forEach(function (row) {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = row.Name;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor = row.Id;
				});
				
				angular.element('#ModalTambahInstruksiPembayaran_GetVendorKomisi').modal('hide');
			}
			else {
				//alert('Belum ada data yang dipilih');
				bsNotify.show({
					title: "Warning",
					content: 'Belum ada data yang dipilih.',
					type: 'warning'
				});
			}
		}

		$scope.Batal_TambahInstruksiPembayaran_GetVendorKomisi = function () {
			angular.element('#ModalTambahInstruksiPembayaran_GetVendorKomisi').modal('hide');
		}

		$scope.TambahInstruksiPembayaran_SearchAdvPayment = function () {
			if ($scope.EditMode == true) {
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == 1) {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor == '' || $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor == null) {
						bsNotify.show({
							title: "Warning",
							content: 'Masukan nama vendor!',
							type: 'warning'
						});
						$scope.ProsesSubmit = false;
					}
					else {
						InstruksiPembayaranFactory.getAdvancePaymentListGT(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, 1, $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor,$scope.DocumentId).then(
							function (res) {
								$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid.data = res.data.Result;
							},
							function (err) {
								console.log("err=>", err);
							}
						);
						angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('show');
					}




				}
				else {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == '' || $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == 0) {
						bsNotify.show({
							title: "Warning",
							content: 'Pilih nama vendor!',
							type: 'warning'
						});
						$scope.ProsesSubmit = false;
					}
					else {
						InstruksiPembayaranFactory.getAdvancePaymentListGT(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, 2, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,$scope.DocumentId).then(
							function (res) {
								$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid.data = res.data.Result;
							},
							function (err) {
								console.log("err=>", err);
							}
						);
						angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('show');
					}
				}


			}
		};

		$scope.TambahInstruksiPembayaran_SearchAdvPayment_CR2 = function () {
			if ($scope.EditMode == true) {
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == '' || $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == 0) {
					bsNotify.show({
						title: "Warning",
						content: 'Pilih nama vendor!',
						type: 'warning'
					});
					$scope.ProsesSubmit = false;
				}
				else {
					InstruksiPembayaranFactory.getAdvancePaymentListGT(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, 2, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,$scope.DocumentId).then(
						function (res) {
							$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid.data = res.data.Result;
						},
						function (err) {
							console.log("err=>", err);
						}
					);
					angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('show');
				}


			}
		};

		// $scope.TambahInstruksiPembayaran_SearchAdvPayment_CR2 = function () {
		// 	if ($scope.EditMode == true) {
		// 		InstruksiPembayaranFactory.getAdvancePaymentList(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran).then(
		// 			function (res) {
		// 				$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid.data = res.data.Result;
		// 			},
		// 			function (err) {
		// 				console.log("err=>", err);
		// 			}
		// 		);
		// 		angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('show');
		// 	}
		// };


		$scope.ShowModalAdvancePayment = function (row) {
			//$scope.ActiveRow  = angular.copy(row.entity);
			//$scope.ActiveRow = row;
			$scope.index = $scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.indexOf(row.entity);
			InstruksiPembayaranFactory.getAdvancePaymentListGT(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,$scope.DocumentId).then(
				function (res) {
					$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_UIGrid.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('show');
		};

		$scope.Batal_TambahInstruksiPembayaran_GetAdvancePaymentModal = function () {
			angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('hide');
		};
		$scope.ActiveRow.AdvanceNominal = 0;
		$scope.Pilih_TambahInstruksiPembayaran_GetAdvancePaymentModal = function () {
			if ($scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_gridAPI.selection.getSelectedCount() > 0) {
				$scope.ModalTambahInstruksiPembayaran_GetAdvancePayment_gridAPI.selection.getSelectedRows().forEach(function (row) {
					$scope.ActiveRow.AdvancePaymentId = row.AdvancedPaymentId;
					$scope.ActiveRow.AdvancedPaymentNo = row.AdvancedPaymentNumber;
					$scope.ActiveRow.AdvanceNominal = row.Nominal;
					$scope.ActiveRow.Saldo = $scope.ActiveRow.Nominal - row.Nominal;
					console.log(' selected adv id-->', row.AdvancedPaymentId);
					console.log(' selected nominal -->', row.Nominal);
					console.log('$scope.ActiveRow --> ', $scope.ActiveRow);
					var nominal = row.Nominal;
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment = row.AdvancedPaymentId;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorAdvPayment = row.AdvancedPaymentNumber;
						$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = nominal;
						var saldoGen = $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI.grid.columns[2].getAggregationValue() - row.Nominal;
						//alert('isinya ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI.grid.columns[2].getAggregationValue());
						$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = saldoGen;
						//$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = $scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total - row.Nominal;
						$scope.CalculateNominalPembayaran();
					}
					else {
						if (($scope.cr2 == false) || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit")) {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].AdvancePaymentId = row.AdvancedPaymentId;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].AdvancePaymentNo = row.AdvancedPaymentNumber;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].AdvanceNominal = row.Nominal;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].Saldo = ($scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].Nominal +
								$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].BungaDF) - row.Nominal;
							if ($scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].radidata == true) {
								$scope.TotalNominal = $scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index].Saldo;
							}
						}
						else {
							$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NoAdvPayment = row.AdvancedPaymentNumber;
							$scope.TambahInstruksiPembayaran_DaftarAdvPayment_FrameNo = row.FrameNo;
							$scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment = row.AdvancedPaymentId;
							$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Nominal = addSeparatorsNF($scope.ActiveRow.AdvanceNominal, '.', '.', ',');
							$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NamaVendor = row.VendorName;
						}
						$scope.CalculateNominalPembayaran();
					}
					//console.log('grid. index  --> ' ,$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data[$scope.index]);
				});

				angular.element('#ModalTambahInstruksiPembayaran_GetAdvancePayment').modal('hide');
			}
			else {
				//alert('Belum ada Advance payment yang dipilih');
				bsNotify.show({
					title: "Warning",
					content: 'Belum ada Advance payment yang dipilih.',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
			}

		};

		$scope.SetColumnUIGridDaftarTagihan = function () {
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
					{ name: "InvoiceId", displayName: "Nomor Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false },
					{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
					{ name: "VendorName", displayName: "Nama Vendor", field: "VendorName", enableCellEdit: false, enableHiding: false },
					{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
					{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
					{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
					{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }];
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {

			}
			else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund") ||
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Subsidi DP/Rate") ||
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Komisi Sales")) {
				// $scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
				// 	{name:"InvoiceId",displayName:"Id Invoice Masuk", field:"InvoiceId", enableCellEdit: false, enableHiding: false, visible : false},
				// 	{name:"InvoiceNo",displayName:"Nomor Invoice Masuk", field:"IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false},
				// 	{name:"InvoiceReceivedDate",displayName:"Tanggal Invoice Masuk", field:"InvoiceReceivedDate", enableCellEdit: false, enableHiding: false},
				// 	{name:"MaturityDate",displayName:"Tanggal Jatuh Tempo", field:"MaturityDate",enableCellEdit: false, enableHiding: false},
				// 	{name:"Nominal", displayName:"Nominal 1",field:"Nominal",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
				// 	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false,
				// 		cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentId}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
				// 	},
				// 	{name:"AdvanceNominal", displayName:"Nominal Advance Payment", field:"AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},
				// 	{name:"Saldo", displayName:"Saldo", field:"Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}
				// ];
				//alert('masuk ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran );
				if ($scope.EditMode == true) {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "IncomingInstructionId", displayName: "IdIncomingInstruction", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "BranchReceipent", displayName: "Nama Branch Penerima", field: "BranchReceipent", enableCellEdit: false, enableHiding: false },
							{ name: "IncomingInstructionNumber", displayName: "Nomor Incoming Payment", field: "IncomingInstructionNumber", enableCellEdit: false, enableHiding: false },
							{ name: "IncomingDate", displayName: "Tanggal Incoming Payment", field: "IncomingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "CustomerName", displayName: "Nama Pelanggan", field: "CustomerName", enableCellEdit: false, enableHiding: false, visible: true },
							{ name: "Description", displayName: "Untuk Pembayaran", field: "Description", enableCellEdit: false, enableHiding: false }				//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") ||
						($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
						//alert('masuk ke sini !!');
						var allowEditBungaDF = false;
						if (($scope.cr2 == true) && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
								{
									name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
									cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
								},
								{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
								{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
								{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
								//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
								{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }						//	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
							];
						}
						else {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
								{
									name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
									cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
								},
								{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
								{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
								{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
								//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
								{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible: false },
								{
									name: "AdvancePaymentNo", displayName: "Nomor Advance Payment", field: "AdvancePaymentNo", enableCellEdit: false, enableHiding: false, visible: false,
									cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentNo}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
								},
								{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false }
								//,{name:"Saldo", displayName:"Saldo", field:"Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}//,
								//	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
							];
						}
						InstruksiPembayaranFactory.getStatusBungaDF($scope.user.OutletId, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran).then(
							function (resBungaDF) {
								//alert('dapat status !');
								console.log('isi ', JSON.stringify(resBungaDF.data));
								if (resBungaDF.data.length > 0) {
									if (resBungaDF.data[0].Status == 1) {
										if //(($scope.cr2 == true) && 
											($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts") {
											$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
												{
													name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
													cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
												},
												{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
												{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
												{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
												{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
												{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
												{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
												{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
												//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
												//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
												{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }										//	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
											];
										}
										else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") {
											$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
												{
													name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
													cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
												},
												{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
												{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
												{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
												{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
												{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
												{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
												{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
												//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
												//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
												{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
												{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible: false },
												{
													name: "AdvancePaymentNo", displayName: "Nomor Advance Payment", field: "AdvancePaymentNo", enableCellEdit: false, enableHiding: false, visible: false,
													cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentNo}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
												},
												{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false }
												//,{name:"Saldo", displayName:"Saldo", field:"Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}//,
												//	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
											];
										}
									}
								}
							},
							function (err) {
								console.log("err=>", err);
							}
						);
						if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts") {
							$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = $scope.cr2;
						}
					}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard"){
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
						//if ($scope.cr2 == true) { 
						$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false,visible: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }								//{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
						];
					}
					else {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
						//if ($scope.cr2 == true) { 
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{
								name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
								cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
							},
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }								//{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
						];
						// }
						// else {
						// 	$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						// 		{name:"radidata", displayName:"", field:"radidata", enableCellEdit: false, enableHiding: false, visible:true, type: "boolean",width: 25,	
						// 			cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'			
						// 		},	
						// 		{name:"InvoiceId",displayName:"Id Invoice Masuk", field:"InvoiceId", enableCellEdit: false, enableHiding: false, visible:false},
						// 		{name:"InvoiceNo",displayName:"Nomor Invoice Masuk", field:"IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false},
						// 		{name:"InvoiceReceivedDate",displayName:"Tanggal Invoice Masuk", field:"InvoiceReceivedDate", enableCellEdit: false, enableHiding: false},
						// 		{name:"InvoiceVendorNo",displayName:"Nomor Invoice Dari Vendor", field:"InvoiceVendorNo", enableCellEdit: false, enableHiding: false},
						// 		{name:"MaturityDate",displayName:"Tanggal Jatuh Tempo", field:"MaturityDate",enableCellEdit: false, enableHiding: false},
						// 		{name:"Nominal", displayName:"Nominal Invoice",field:"Nominal",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 	//	{name:"DPP", displayName:"DPP",field:"DPP",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 		//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 		//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
						// 		//{name:"BungaDF", displayName:"Bunga DF",field:"BungaDF",enableCellEdit: true, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},		
						// 		{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible:false},	
						// 		{name:"AdvancePaymentNo", displayName:"Nomor Advance Payment", field:"AdvancePaymentNo", enableCellEdit: false, enableHiding: false,
						// 			cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentNo}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
						// 		},
						// 		{name:"AdvanceNominal", displayName:"Nominal Advance Payment", field:"AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},
						// 		{name:"Saldo", displayName:"Saldo", field:"Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}//,
						// 		//{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
						// 	];							
						// }
						//alert(' masuk 11 - tipe ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran );
						if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund") &&
							($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Subsidi DP/Rate") &&
							($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Komisi Sales") &&
							($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Aksesoris Standard") ) {
							//alert(' masuk 11 - tipe ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran );
							$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = $scope.cr2;
						}
					}
				}
				else {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{ name: "IncomingInstructionId", displayName: "IdIncomingInstruction", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "BranchReceipent", displayName: "Nama Branch Penerima", field: "BranchReceipent", enableCellEdit: false, enableHiding: false },
							{ name: "IncomingInstructionNumber", displayName: "Nomor Incoming Payment", field: "IncomingInstructionNumber", enableCellEdit: false, enableHiding: false },
							{ name: "IncomingDate", displayName: "Tanggal Incoming Payment", field: "IncomingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "CustomerName", displayName: "Nama Pelanggan", field: "CustomerName", enableCellEdit: false, enableHiding: false, visible: true },
							{ name: "Description", displayName: "Untuk Pembayaran", field: "Description", enableCellEdit: false, enableHiding: false }				//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
						];
					}
					else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
						if (($scope.cr2 == true) && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")) {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
								{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
								{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
								{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
								//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
								{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }];
						}
						else {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
								{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
								{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
								{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
								{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
								//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
								{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
								{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "AdvancePaymentNo", displayName: "Nomor Advance Payment", field: "AdvancePaymentNo", enableCellEdit: false, enableHiding: false, visible: false },
								{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false },
								{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false }
							];
						}
						if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts") {
							$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = $scope.cr2;
						}
					}
					else {
						//if ($scope.cr2 == true) { 
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
							{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
							{ name: "InvoiceVendorNo", displayName: "Nomor Invoice Dari Vendor", field: "InvoiceVendorNo", enableCellEdit: false, enableHiding: false },
							{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
							{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
							{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' }];
						// }
						// else {
						// 		$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						// 		{name:"InvoiceId",displayName:"Id Invoice Masuk", field:"InvoiceId", enableCellEdit: false, enableHiding: false, visible:false},
						// 		{name:"InvoiceNo",displayName:"Nomor Invoice Masuk", field:"IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false},
						// 		{name:"InvoiceReceivedDate",displayName:"Tanggal Invoice Masuk", field:"InvoiceReceivedDate", enableCellEdit: false, enableHiding: false},
						// 		{name:"InvoiceVendorNo",displayName:"Nomor Invoice Dari Vendor", field:"InvoiceVendorNo", enableCellEdit: false, enableHiding: false},
						// 		{name:"MaturityDate",displayName:"Tanggal Jatuh Tempo", field:"MaturityDate",enableCellEdit: false, enableHiding: false},
						// 		{name:"Nominal", displayName:"Nominal Invoice",field:"Nominal",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 		//{name:"DPP", displayName:"DPP",field:"DPP",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 		//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},
						// 		//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},				
						// 		//{name:"BungaDF", displayName:"Bunga DF",field:"BungaDF",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'rupiahCIP'},		
						// 		{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false , visible:false},				
						// 		{name:"AdvancePaymentNo", displayName:"Nomor Advance Payment", field:"AdvancePaymentNo", enableCellEdit: false, enableHiding: false	},
						// 		{name:"AdvanceNominal", displayName:"Nominal Advance Payment", field:"AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},
						// 		{name:"Saldo", displayName:"Saldo", field:"Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}
						// 	];						
						// }
						//alert(' masuk 22 - tipe ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran );
						if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard"){
							$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
						}else{
							$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = $scope.cr2;
						}
					}
				}
			}
		}

		$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran_Selected_Changed = function () {
			if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash") {
				$scope.Show_NomerRekening_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = "";
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = false;
				//Cr4 P2 Koreksi Start
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaKoreksi_Show = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaKoreksi_Show = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
				$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
				//Cr4 P2 Koreksi End
				
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = true;
				}
				else {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = true;
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
				}
			}
			else if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
					$scope.ShowLabelRefund = true;
					$scope.ShowLabelKoreksi = false;
				}
				else {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = false;
				}
				
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;//vvvvvvv
				//cr4 p2 koreksi start
				// $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = 
				// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif" ? true : false;
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif") {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true ;
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = true ;
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaKoreksi_Show = true ;
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;
					$scope.ShowLabelRefund = false;
					$scope.ShowLabelKoreksi = true;
				}
				//cr4 p2 koreksi End
				if ($scope.EditMode == true) {
					$scope.Show_NomorRekTujuan_Disable = false;
					$scope.Show_NomorRekTujuan_EnableDisable = false;

					// if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" && $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == "Unknown") {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = true;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = true;
						InstruksiPembayaranFactory.getDataBankComboBox()
							.then(
								function (res) {
									$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = res.data;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankRefund;
								},
								function (err) {
									console.log("err=>", err);
								}
							);
					}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif"){
						InstruksiPembayaranFactory.getDataBankComboBox()
						.then(
							function (res) {
								$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = res.data;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi;
							},
							function (err) {
								console.log("err=>", err);
							}
						);
					}else {
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
					}
				}
				else {
					$scope.Show_NomorRekTujuan_Disable = true;
					$scope.Show_NomorRekTujuan_EnableDisable = true;
					if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund"){
						$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = false;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = true;
					}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif"){
						$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = true;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
					}else{
						$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankRefund_Show = true;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
						$scope.LihatInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
					}
					// $scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = true;
				}
				if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif"){
					$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaRefund_Show = false;
				}

				$scope.Show_NomerRekening_EnableDisable = false;

				//CR4 P2 FInance BungaDF Start
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit" 
					&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Parts"){
						$scope.getDataBankAccount();
				}//CR4 P2 FInance BungaDF End
			}
			else {
				$scope.Show_NomorRekTujuan_Disable = true;
				$scope.Show_NomerRekening_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show == false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekTujuan = "";
				//CR4 P2 Fianace Koreksi Start'
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerimaKoreksi_Show = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaBankKoreksi_Show = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = false;
				//CR4 P2 Fianace Koreksi End'
			}
		};

		// ============================ BEGIN | NOTO | 190410 | Bank & Rekening [2] | Controller binding ============================
		$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBank = [];
		$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening = [];
		$scope.getDataBankAccount = function () {
			// InstruksiPembayaranFactory.getDataBankComboBox()
			// 	.then(
			// 		function (res) {
			// 			$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NamaBank = res.data;
			// 		},
			// 		function (err) {
			// 			console.log("err=>", err);
			// 		}
			// 	);
			$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening = [];
			InstruksiPembayaranFactory.getDataBankAccountComboBox()
			.then(
				function (res) {
					$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
		}
		//CR4 P2 Finance BungaDF Start
		$scope.dataBank;
		$scope.getDataBankAccountUnitPart = function(id,isDf){
			$scope.dataBank = [];
			$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening = [];
			InstruksiPembayaranFactory.getDataBankAccountUnitPartComboBox(id)
			.then(
				function (res) {
					$scope.dataBank = res.data;
					for(var x in $scope.dataBank){
						var data = {
							"name": $scope.dataBank[x].AccountNumber +'-'+ $scope.dataBank[x].BankName,
							"value": $scope.dataBank[x].VendorBankId
						};
						$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening.push(data);
						console.log('push', $scope.dataBank)
						console.log('push', $scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening)
						if(isDf == 1 && $scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening.length > 0){
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = $scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening[$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening.length - 1].value;
						}
						if($scope.draft == 'Ya' && ($scope.LihatInstruksiPembayaran_TujuanPembayaran_NoRekening != null || $scope.LihatInstruksiPembayaran_TujuanPembayaran_NoRekening != undefined)){
							if($scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening[x].name == $scope.LihatInstruksiPembayaran_TujuanPembayaran_NoRekening){
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening = $scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_NoRekening[x].value;
							}
						}
					}
				},
				function (err) {
					console.log("err=>", err);
				}
			);
		}
		//CR4 P2 Finance BungaDF End
		//CR4 P2 Finance BungaDF Start
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening_Selected_Changed = function () {
			if ($scope.EditMode == true && $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit" 
				&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Parts") {
				InstruksiPembayaranFactory.getDataAccountBank($scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening)
					.then(
						function (res) {
							console.log("No")
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = res.data.Result[0].AccountBankName;
						}
					);
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening == undefined || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening == "") {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = "";
				}
			}else if($scope.EditMode == true && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit" 
			|| $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")){
				for(var x in $scope.dataBank){
					if($scope.dataBank[x].VendorBankId == $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoRekening){
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBank = $scope.dataBank[x].BankName;
						break;
					}
				}
			}
		}//CR4 P2 Finance BungaDF End

		function SplitBankTransfer(p, i) {
			p = p.toString().split('|')[i];
			return p
		}
		// ============================ END | NOTO | 190410 | Bank & Rekening [2] | Controller binding ============================

		$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_Changed = function () {
			var mode = "";
			if ($scope.EditMode == true) {
				mode = 'Edit';
				// console.log("Cek Nomor SO", $scope.TambahInstruksiPembayaran_TujuanPembayaran_InputSO);
				if($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor == "2"){
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = true;
				}
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = "";
				InstruksiPembayaranFactory.getOtherInfoKomisiNew(1, uiGridPageSize,
					$scope.TambahInstruksiPembayaran_InputSO, $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP, mode, $scope.DocumentId).then(
						function (res) {
							console.log("ccccccccccccc")

							// infomasi pelanggan
							if(res.data.Result.length>0){
								$scope.getSOId = res.data.Result[0].Id;
							}else{
								$scope.clearData();
							}
							// $scope.getSOId = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data.Result[0].CustomerName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data.Result[0].CustomerCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data.Result[0].HP;
							// informasi lain-lain
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = res.data.Result[0].AkumulasiDPP;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = res.data.Result[0].PPH21;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = res.data.Result[0].Commision;

							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = res.data.Result[0].MediatorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = res.data.Result[0].MediatorPhoneNo;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = res.data.Result[0].MediatorKTPNo;

							// var nominalkomisi = res.data.Result[0].Commision;
							// var nominalpph21 = res.data.Result[0].PPH21;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							// var nomAkumulasiDPP = res.data.Result[0].AkumulasiDPP;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = nomAkumulasiDPP;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nominalkomisi//addSeparatorsNF(nominalkomisi, '.', '.', ','); //res.data.Result[0].Commision;
							// if (!($scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP == '' || $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP == null))
							// 	$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nominalpph21//addSeparatorsNF(nominalpph21, '.', '.', ','); // res.data.Result[0].PPH21;

							// $scope.TotalNominal = $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi;
							// $scope.CalculateNominalPembayaran();
							$scope.recalculate();
						},
						function (err) {
							console.log('Error ', err);
						}
					);


			}else{
				mode = 'Lihat';
				InstruksiPembayaranFactory.getOtherInfoKomisiNew(1, uiGridPageSize,
					$scope.TambahInstruksiPembayaran_InputSO, $scope.VendorKTP, mode, $scope.DocumentId).then(
						function (res) {
							console.log("ccccccccccccc")

							// infomasi pelanggan
							if(res.data.Result.length>0){
								$scope.getSOId = res.data.Result[0].Id;
							}else{
								$scope.clearData();
							}
							// $scope.getSOId = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data.Result[0].CustomerName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data.Result[0].CustomerCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data.Result[0].HP;
							// informasi lain-lain
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = res.data.Result[0].AkumulasiDPP;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = res.data.Result[0].PPH21;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = res.data.Result[0].Commision;

							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = res.data.Result[0].MediatorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = res.data.Result[0].MediatorPhoneNo;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = res.data.Result[0].MediatorKTPNo;

							// var nominalkomisi = res.data.Result[0].Commision;
							// var nominalpph21 = res.data.Result[0].PPH21;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							// var nomAkumulasiDPP = res.data.Result[0].AkumulasiDPP;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = nomAkumulasiDPP;
							// $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nominalkomisi//addSeparatorsNF(nominalkomisi, '.', '.', ','); //res.data.Result[0].Commision;
							// if (!($scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP == '' || $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP == null))
							// 	$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nominalpph21//addSeparatorsNF(nominalpph21, '.', '.', ','); // res.data.Result[0].PPH21;

							// $scope.TotalNominal = $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi;
							// $scope.CalculateNominalPembayaran();
							$scope.recalculate();
						},
						function (err) {
							console.log('Error ', err);
						}
					);
			}
		};

		$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Cari_Changed = function () {
			if ($scope.EditMode == true) {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = "";
				InstruksiPembayaranFactory.getOtherInfoKomisi(1, uiGridPageSize,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, $scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP).then(
						function (res) {
							//var nominalkomisi = res.data.Result[0].Commision;
							var nomAkumulasiDPP = res.data.Result[0].AkumulasiDPP;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = nomAkumulasiDPP;
							var nominalpph21 = res.data.Result[0].PPH21;
							//$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;
							//$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nominalkomisi//addSeparatorsNF(nominalkomisi, '.', '.', ','); //res.data.Result[0].Commision;

							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nominalpph21//addSeparatorsNF(nominalpph21, '.', '.', ','); // res.data.Result[0].PPH21;

							//$scope.TotalNominal = $scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi;
							$scope.CalculateNominalPembayaran();
						},
						function (err) {
							console.log('Error ', err);
						}
					);


			}
		};

		//harja, selain part dan unit, end
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_Selected_Changed = function () {
			
			if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor != "2") {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "One Time Vendor";
				if ($scope.EditMode == true) {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK;
				}
			}
			if ($scope.EditMode == true) {
				if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor == "2") {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "Pilih Vendor";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = true;
					
					$scope.SimpanDraf_Disabled = true;
					$scope.InformasiAlamat_Disabled = false;
					$scope.KabupatenKota_Disabled = false;
				}
				else {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_val = "One Time Vendor";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = false;

					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = false;

					$scope.SimpanDraf_Disabled = false;
					$scope.InformasiAlamat_Disabled = true;
					$scope.KabupatenKota_Disabled = true;

					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_Changed();
				}
			}
			else {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = true;
			}
		}

		$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari = function () {
			if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor != "" && $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor != null && $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor != undefined) {
				$scope.SimpanDraf_Disabled = false;
				InstruksiPembayaranFactory.getVendorInfo('mediator',
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor, $scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor).then(
						function (res) {
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = res.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = res.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = res.data.Result[0].Name;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI		
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = res.data.Result[0].KTP;

							InstruksiPembayaranFactory.getReOtherInfoKomisi(1, uiGridPageSize, $scope.getSOId,
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP, $scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP).then(
									function (resCom) {
										var nomComision = resCom.data.Result[0].Commision;
										var nomPPH21 = resCom.data.Result[0].PPH21;
										var nomAkumulasiDPP = resCom.data.Result[0].AkumulasiDPP;
										$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nomComision //addSeparatorsNF(nomComision, '.', '.', ','); // resCom.data.Result[0].Commision;
										$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = resCom.data.Result[0].BillingNo;
										$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nomPPH21//addSeparatorsNF(nomPPH21, '.', '.', ','); //resCom.data.Result[0].PPH21;

										var nomAkumulasiDPP = resCom.data.Result[0].AkumulasiDPP;
										$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = nomAkumulasiDPP;
										$scope.CalculateNominalPembayaran();
									},
									function (errCom) {
										console.log('Error ', errCom);
									}
								);
						},
						function (err) {
							console.log('Error ', err);
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor = "";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nomor = "";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = "";
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = "";		//Data hasil dari WebAPI		
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = "";
						}
					);
			}
			else {
				//alert('Nama Vendor belum dipilih');
				$scope.SimpanDraf_Disabled = true;
				bsNotify.show({
					title: "Warning",
					content: 'Nama Vendor belum dipilih.',
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
			}
		}

		$scope.recalculate = function () {
			InstruksiPembayaranFactory.getReOtherInfoKomisi(1, uiGridPageSize, $scope.getSOId,
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP, $scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP).then(
					function (resCom) {
						var nomComision = resCom.data.Result[0].Commision;
						var nomPPH21 = resCom.data.Result[0].PPH21;
						var nomAkumulasiDPP = resCom.data.Result[0].AkumulasiDPP;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = nomComision //addSeparatorsNF(nomComision, '.', '.', ','); // resCom.data.Result[0].Commision;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = resCom.data.Result[0].BillingNo;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = nomPPH21//addSeparatorsNF(nomPPH21, '.', '.', ','); //resCom.data.Result[0].PPH21;

						var nomAkumulasiDPP = resCom.data.Result[0].AkumulasiDPP;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = nomAkumulasiDPP;
						$scope.CalculateNominalPembayaran();
					},
					function (errCom) {
						console.log('Error ', errCom);
					}
				);
		}

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_Change = function (value) {
			$scope.tmpnonnpwp = 0;
			if (value == "1") {
				$scope.a = true;
				$scope.b = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = "1";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = 'One Time Vendor';
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaVendor_EnableDisable = false;
				$scope.Show_TambahInstruksiPembayaran_Cari_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
				$scope.LogicCariBtn_TopSearch();
				//alert('masuk rubah tipe');
				//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;	
				if ($scope.EditMode == true) {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = "";
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = "";		//Data hasil dari WebAPI
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = "";
					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Cash", value: "Cash" }];
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = "Cash";
				}
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat_EnableDisable = $scope.EditMode;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota_EnableDisable = $scope.EditMode;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor_EnableDisable = $scope.EditMode;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon_EnableDisable = $scope.EditMode;		//Data hasil dari WebAPI
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_EnableDisable = $scope.EditMode;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = true;
				//}
			}
			else {
				$scope.a = false;
				$scope.b = true;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = "2";
				if ($scope.EditMode == true) {
					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
					$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;
				}
				$scope.Show_TambahInstruksiPembayaran_Tagihan = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_EditDraft_cari = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaVendor_EnableDisable = true;
				$scope.Show_TambahInstruksiPembayaran_Cari_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon_EnableDisable = false;		//Data hasil dari WebAPI
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_EnableDisable = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
			}
		};

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch_Click = function () {
			console.log("xxxxx", $scope.TambahInstruksiPembayaran_InputSO);
			console.log("xxxxx", $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran);
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund" && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == "" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == null ||
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == undefined)){
				bsNotify.show(
					{
						title: "Warning",
						content: "Nama Pelanggan Tidak Boleh Kosong!",
						type: 'warning'
					}
				);	
				return false;	
			}
			$scope.LogicCariBtn_TopSearch();
		}

		$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21_Changed = function () {
			if ($scope.EditMode != false) {
				$scope.CalculateNominalPembayaran();
			}
		}

		$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21_EnableDisable = true;
		$scope.LogicCariBtn_TopSearch = function () {
			$scope.TambahInstruksiPembayaran_DaftarPajak = false;
			$scope.TambahInstruksiPembayaran_ButtonTambah = false;
			var lanjut = false;
			$scope.success = true;



			if ($scope.EditMode == true) {
				//enable kolom action
				//alert('masuk sini!!');

				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Saldo = 0;
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_IdAdvPayment = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NoAdvPayment = "";
				$scope.TambahInstruksiPembayaran_DaftarAdvPayment_Nominal = "";
				//$scope.TambahInstruksiPembayaran_DaftarAdvPayment_NamaVendor = "";
				//$scope.Show_Main_DaftarList = true;
				$scope.DocumentId = 0;
				//$scope.EditMode = false;
				$scope.TotalNominal = 0;
				$scope.SelectedGridId = -1;
				$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
				// $scope.ShowTambahInstruksiPembayaran=false;
				// $scope.ShowLihatInstruksiPembayaran=false;
				// $scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
				$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = "";
				$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = "";
				$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = "";
				$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
				$scope.TambahInstruksiPembayaran_BiayaLainLain_Keterangan = "";
				$scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit = null;
				$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = null;
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = 0;
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = 0;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = null;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = {};
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_AkumulasiDPP = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = "";
				$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = 0;
				$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];

				//general transaction
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransaction = {};
				$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_DaftarAdvancedPayment_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal = 0;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = "";
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransaction = {};
				$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak = "";
				$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak = "";
				$scope.TambahInstruksiPembayaran_GeneralTransaction_KetObjekPajak = "";
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = "";
				// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = "";
				// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = "";
				// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = "";
				// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction = null;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = "";
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = "";
				$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorAdvPayment = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment = "";

				$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
				$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;

				$scope.TambahInstruksiPembayaran_TotalDaftarTagihan = 0;
				$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
				$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
				//harja , end
				$scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record = "";
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "STNK/BPKB" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Order Pengurusan Dokumen" ||
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard") {
					$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [{ name: "Bea Materai", value: "Bea Materai" }];
					$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }];
				}else{
					$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [];
					$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
				}
				
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") {
					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
					$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = []; //,{name:"Biaya Bank", value:"Biaya Bank"}];
				}

				$scope.EnableUIGridColumn();
			}

			//alert('isi ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor);
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				//alert(' masuk ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor + ' - ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor );	
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value == "2") &&
					(($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "") ||
						(typeof $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "undefined"))) {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = 'Pilih Vendor'
					if ($scope.draft == 'Ya') { lanjut = true; }
					else {
						lanjut = false;
					}
				}
				else {
					lanjut = true;
				}
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
				lanjut = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				lanjut = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
				lanjut = true;
			}
			else {
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "") ||
					(typeof $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "undefined")) {
					//alert(' dianggap kosong ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor );
					//lanjut = false; 
					if ($scope.draft == 'Ya') { lanjut = true; }
					else {
						lanjut = false;
					}
				}
				else {
					lanjut = true;
				}
			}

			if (lanjut == false) {
				bsNotify.show({
					title: "Warning",
					content: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label + ' masih kosong !',
					type: 'warning'
				});
				//alert( $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label + ' masih kosong !');
				$scope.success = false;
			}
			else {
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
					var mode;
					var id;
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data[0].Id;		
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data[0].Address;	
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data[0].City;	
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data[0].Name;	
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiP$elangganTop_Telepon = res.data[0].Phone;		//Data hasil dari WebAPI
					if ($scope.EditMode == true) {
						mode = 'Edit';
						id = $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor;
					}
					else {
						mode = 'Lihat';
						id = $scope.DocumentId;
					}

					InstruksiPembayaranFactory.getDataForAccrued(1, uiGridPageSize, id,
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, mode).then(
							function (result) {
								//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = result.data.Result[0].
								//$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = result.data.Result[0].Id;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = result.data.Result[0].BillingNumber;
								$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data = result.data.Result;
								if (mode == 'Lihat' && result.data.Result.length > 0) {
									$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[0].radidata = true;
									// $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].radioShow = $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].SubsidyId;
									// alert( ' isi data --' + $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].SubsidyType);
									$scope.SelectedGridId = $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[0].SubsidyId;
									$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.columnDefs[0].visible = false;
									$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								}

								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = result.data.Result[0].VendorCode;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = result.data.Result[0].Address;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = result.data.Result[0].City;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = result.data.Result[0].VendorName;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = result.data.Result[0].Handphone;
								$scope.success = true;
							},
							function (err) {
								console.log('error -->', err);
								$scope.success = false;
							}
						);
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
					$scope.success = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
					$scope.success = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
					$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_Changed();
					// InstruksiPembayaranFactory.getVendorInfoDetail($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
					// 	$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor).then(
					// 		function (res) {
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].Id;
					// 			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data.Result[0].Id;
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data.Result[0].Address;
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data.Result[0].City;
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data.Result[0].Name;
					// 			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI

					// 			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
					// 				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
					// 				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = res.data.Result[0].NameSPK;
					// 				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = res.data.Result[0].PhoneSPK;
					// 				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = res.data.Result[0].KTPSPK;
					// 			}
					// 			$scope.success = true;
					// 			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_Changed();
					// 			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data[0].Id;		
					// 			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data[0].Address;	
					// 			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data[0].City;	
					// 			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data[0].Name;	
					// 			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data[0].Phone;		//Data hasil dari WebAPI
					// 			//}
					// 		},
					// 		function (err) {
					// 			console.log("err=>", err);
					// 			$scope.success = false;
					// 		}
					// 	);
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor == "Unknown" || $scope.VariabelBaru == "Unknown") {

						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = 0;
						if ($scope.EditMode == true || $scope.draft == "Ya") {
							$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
						}
						else {
							$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
						}
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
						$scope.ShowLabelRefund = true;
						$scope.ShowLabelKoreksi = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = "-";
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = "-";
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = "-";
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = "Unknown";
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = "-";

						if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
							$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = "-";
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = "-";
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = "-";
						}
						$scope.success = true;
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
						$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed(1, uiGridPageSize)
					}
					else {
						if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor != null ){
							var tmpReplacePem = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.replace(/\&/g, "%26");
						}else{
							var tmpReplacePem = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor;
						}
						InstruksiPembayaranFactory.getVendorInfoDetail($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							tmpReplacePem, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, $scope.TambahInstruksiPembayaran_NamaBranch).then(
								function (res) {
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].Id;
									if ($scope.EditMode == true || $scope.draft == "Ya") {
										$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
									}
									else {
										$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
									}
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data.Result[0].VendorCode;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data.Result[0].Address;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data.Result[0].City;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data.Result[0].Name;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI

									if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
										$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = res.data.Result[0].NameSPK;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = res.data.Result[0].PhoneSPK;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = res.data.Result[0].KTPSPK;
									}
									$scope.success = true;
									$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data[0].Id;		
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data[0].Address;	
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data[0].City;	
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data[0].Name;	
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data[0].Phone;		//Data hasil dari WebAPI
									//}
								},
								function (err) {
									console.log("err=>", err);
									$scope.success = false;
								}
							);
					}

				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value != "1") {
						var tmpReplacePem = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.replace(/\&/g, "%26");
						InstruksiPembayaranFactory.getVendorInfoDetail($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
							tmpReplacePem, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, $scope.TambahInstruksiPembayaran_NamaBranch).then(
								function (res) {
									if (res.data.Total > 0) {
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_val = 'Pilih Vendor';
										$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = res.data.Result[0].VendorCode;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].Address;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].City;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].Name;
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = res.data.Result[0].NPWP;
									}
									$scope.success = true;
								},
								function (err) {
									console.log('Error -->', err);
									$scope.success = false;
								}
							)
					}
					else { $scope.success = true; }

					if ($scope.success == true) {
						$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = true;
						$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
						$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					}

				}
				else  //( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
				// 		 ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
				{

					// if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts" || $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Order Pekerjaan Luar" ){
					if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Swapping"){
						$scope.tmpReplace = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.replace(/\&/g, "%26");
					}else{
						$scope.tmpReplace = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor;
					}
						console.log("Cek TMPSEPLACE 11111 >>>>>>>>>>>>>>", $scope.tmpReplace)
					// }else{
					// 	$scope.tmpReplace = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor;
					// 	console.log("Cek TMPSEPLACE 22222 >>>>>>>>>>>>>>", $scope.tmpReplace)
					// }
					InstruksiPembayaranFactory.getVendorInfoDetail($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
						$scope.tmpReplace, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, $scope.TambahInstruksiPembayaran_NamaBranch).then(
							function (res) {
								$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = res.data.Result[0].VendorCode;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].Address;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].City;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].Name;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI

								InstruksiPembayaranFactory.getDataInvoiceList(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor,
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, $scope.DocumentId, $scope.Action).then(
										function (res) {
											$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
											$scope.success = true;

											$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
												row.Saldo = row.Nominal;
												row.BungaDF = row.BungaDF.toFixed(2);
												console.log("row >>>>>", row);
												
											});
											console.log("cari Insruksi pembayaran >>>>>>>>>>>");
										},
										function (err) {
											console.log("err=>", err);
											$scope.success = false;
										}
									);
							},
							function (err) {
								console.log('Error -->', err);
								$scope.success = false;
							}
						)
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
					}

				}
				$scope.SetColumnUIGridDaftarTagihan();
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "General Transaction") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedSubsidi = false;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					$scope.TambahInstruksiPembayaran_DaftarPajak = false;
					$scope.Show_TambahInstruksiPembayaran_Refund = false;
					$scope.TambahInstruksiPembayaran_ButtonTambah = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedKomisi_VendorSPK = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = false;
					// $scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
				}
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.Show_TambahInstruksiPembayaran_Refund_DaftarRefund = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
					$scope.Show_TambahInstruksiPembayaran_Refund = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;

					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
					if($scope.EditMode == true && $scope.draft == "Ya"){
						$scope.ShowTambahRefund = false;
						$scope.ShowLihatRefund = true;
					}else 
					if($scope.EditMode == true){
						$scope.ShowTambahRefund = true;
						$scope.ShowLihatRefund = false;
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = true;
					}else{
						$scope.ShowTambahRefund = false;
						$scope.ShowLihatRefund = true;	
						$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = false;
					}
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = true;
					//$scope.TambahInstruksiPembayaran_DaftarPajak = true;
					$scope.show_TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = true;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					$scope.show_TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
					//alert('masuk ke sini subsidi');
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedSubsidi = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedKomisi_VendorSPK = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				}
				else //if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Parts")  )
				{
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.TambahInstruksiPembayaran_DaftarPajak = true;
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit") {
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					}
					else {
						$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = $scope.cr2Unit;
					}
				}
			}

			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
			}
			else {
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
			}
			console.log("akhir>>>>>>>>>");
		};


		$scope.Show_TambahInstruksiPembayaran_InputSO = false;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed = function () {
			$scope.HideMenuKoreksiAdministratif();
			$scope.tmpnonnpwp = 0;		
			$scope.Show_TambahInstruksiPembayaran_InputSO = false;
			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InputSO ="";
			$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.data = [];
			$scope.DisableTambahInstruksiPembayaran_TujuanPembayaran_TopSearch();
			$scope.Disable_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop();
			$scope.Disable_Show_TambahInstruksiPembayaran_Refund();
			// NEW from UT
			$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
			$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
			$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
			$scope.TambahIInstruksi_TipeInstruksiNotice = "";
			$scope.Show_TambahInstruksiPembayaran_Cari_EnableDisable = true;
			//end
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
			//Hide Advance Payment
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction"){
				$scope.Show_ListInstruksiPembayaranAdvancePayment = true;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif"){
				$scope.Show_ListInstruksiPembayaranAdvancePayment = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
				$scope.ShowLabelRefund = false;
				$scope.ShowLabelKoreksi = true;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund"){
				$scope.Show_ListInstruksiPembayaranAdvancePayment = false;
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
				$scope.ShowLabelRefund = true;
				$scope.ShowLabelKoreksi = false;
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service"){
				$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid = true;
				$scope.ShowTambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid_View = false;
				$scope.Show_ListInstruksiPembayaranAdvancePayment = false;		
			}
			else{
				$scope.Show_ListInstruksiPembayaranAdvancePayment = false;
			}
			
			if ($scope.EditMode == true) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
			}

			if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != null) &&
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "undefined")) //.length>0)
			{

				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
					// $scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Cash", value: "Cash" }];
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
					//20170512, eric, begin
					$scope.getGLList();
					$scope.getCostCenterList();
					$scope.getPajakList();
					$scope.GetIsAnggaran();
					//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails=true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Vendor";

					//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedKomisi_VendorSPK = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = true;
					$scope.show_TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
					//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch=true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = {};
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = '';
					$timeout(function(){
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor.Value = "2";
						// aaaa
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat_EnableDisable = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota_EnableDisable = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor_EnableDisable = false;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon_EnableDisable = false;		//Data hasil dari WebAPI
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_EnableDisable = false;
						$scope.Show_TambahInstruksiPembayaran_Cari_EnableDisable = true;
						
					},100);
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = "2";
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedSubsidi = false;
				}
				else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate")){ //||
					// ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales")) {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nomor SO";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				}
				else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales"){
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
					$scope.TambahInstruksiPembayaran_InputSO_EnableDisable = false;
					// $scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NomorSO_TopSearch = true;
					// $scope.Show_InputSO = true;
					$scope.Show_TambahInstruksiPembayaran_InputSO = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nomor SO";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Interbranch") {
					$scope.LogicCariBtn_TopSearch();
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
					$scope.HeaderGridInstruksiPembayaran_DaftarTagihan = 'Daftar Incoming Payment Interbranch'
					$scope.TambahInstruksiPembayaran_ButtonTambah = false;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
					$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search =
						[{ name: "Nama Branch Penerima", value: "BranchRecipient" },
						{ name: "Nomor Incoming Payment", value: "IncomingInstructionNumber" },
						{ name: "Tanggal Incoming Payment", value: "IncomingDate" },
						{ name: "Nominal Pembayaran", value: "Nominal" }];
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = {};
					//$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
					var Mode = '';
					if ($scope.EditMode == true) {
						Mode = "Edit";
					}
					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Cash", value: "Cash" }];
					//alert('come in');
					InstruksiPembayaranFactory.getDataInterbranch(1, uiGridPageSize, $scope.user.OutletId, 'Interbranch', $scope.DocumentId).then(
						function (resFS) {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resFS.data.Result;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = resFS.data.Total;//cccc
						},
						function (errFS) {
							console.log('Error -->', errFS);
						}
					);
				}
				else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;

					$scope.TambahInstruksiPembayaran_ButtonTambah = false;
					$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService = true;
					$scope.Show_TambahInstruksiPembayaran_DaftarRangka_AccruedFreeService = true;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaranGeneralTransaction = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					//$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					var Mode = '';
					if ($scope.EditMode == true) {
						Mode = "New";
						$scope.mode = Mode;
					}

					if($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text == null || $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text == "" || $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == null || $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "" ){
						$scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text = undefined;
						$scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search = undefined;
					}
					//alert('come in');
					InstruksiPembayaranFactory.getDataForAccruedFreeService(1, uiGridPageSize, $scope.DocumentId, '', Mode, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
						function (resFS) {
							$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = resFS.data.Result;
							$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.totalItems = resFS.data.Total;
							console.log("cek data >>>>>", resFS.data.Total)
							// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSize = uiGridPageSize;
							// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSizes = [25, 50, 100];
							$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
							// $scope.getDataFree(1, resFS.data.Total, $scope.DocumentId, '', Mode);						
						},
						function (errFS) {
							console.log('Error -->', errFS);
						}
					);
					// CR5 #38 Finance Start
					$scope.TambahInstruksiPembayaran_DaftarPajak = true;
					// CR5 #38 Finance End
				}else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Koreksi Administratif") {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
					$scope.TambahInstruksiPembayaran_ButtonTambah = false;
					$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService = false;
					$scope.Show_TambahInstruksiPembayaran_DaftarRangka_AccruedFreeService = false;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaBankKoreksi = '';
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.NumberType = false;

					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [];
					$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" },{ name: "Cash", value: "Cash" }];
					$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search =
						[{ name: "Nama Customer", value: "NamaCustomer" },
						{ name: "Alamat", value: "Alamat" },
						{ name: "No Incoming Payment", value: "NomorIncomingPayment" },
						{ name: "Nominal Incoming Payment", value: "NominalIncomingPayment" }];
					var Mode = '';
					if ($scope.EditMode == true) {
						Mode = "New";
						$scope.mode = Mode;
					}
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						// {
						// 	name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
						// 	cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.SelectedIncomingPayment(row)"></div>'
						// },
						// { name: "IncomingInstructionId", displayName: "IdIncomingInstruction", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "NamaCustomer", displayName: "Nama Customer", field: "NamaCustomer", enableCellEdit: false, enableHiding: false },
						{ name: "Alamat", displayName: "Alamat", field: "Alamat", enableCellEdit: false, enableHiding: false },
						{
							name: "NomorIncomingPayment", field: "NomorIncomingPayment",displayName: "No Incoming Payment",enableCellEdit: false,
							cellTemplate: '<a ng-click="grid.appScope.SelectedIncomingPayment(row)">{{row.entity.NomorIncomingPayment}}</a>'
						},
						// { name: "IncomingInstructionNumber", displayName: "No Incoming Payment", field: "IncomingInstructionNumber", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
						{ name: "TanggalIncoming", displayName: "Tanggal Incoming Payment", field: "TanggalIncoming",enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
						{ name: "NominalIncomingPayment", displayName: "Nominal Incoming Payment", field: "NominalIncomingPayment", cellFilter: 'rupiahC',enableCellEdit: false, enableHiding: false, visible: true }			//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
					];

					InstruksiPembayaranFactory.getDataForAccruedFreeService(1, uiGridPageSize, $scope.DocumentId, '', Mode, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
						function (resFS) {
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resFS.data.Result;
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.totalItems = resFS.data.Total;
							console.log("cek data >>>>>", resFS.data.Total)
							// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSize = uiGridPageSize;
							// $scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.paginationPageSizes = [25, 50, 100];
							$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
							// $scope.getDataFree(1, resFS.data.Total, $scope.DocumentId, '', Mode);						
						},
						function (errFS) {
							console.log('Error -->', errFS);
						}
					);
				}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Aksesoris Standard"){
					$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
					$scope.HeaderGridInstruksiPembayaran_DaftarTagihan = 'Daftar Tagihan'
					//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = [{
						name: "Nomor Invoice Masuk",
						value: "Nomor Invoice Masuk"
					}, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }, { name: "Nominal Invoice", value: "Nominal Invoice" }
						, { name: "Nomor Invoice Dari Vendor", value: "Nomor Invoice Dari Vendor" }];
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = {};
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Vendor";
				}
				else // if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran=="Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran=="Parts") )
				{
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
					$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = true;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
					$scope.HeaderGridInstruksiPembayaran_DaftarTagihan = 'Daftar Tagihan'
					//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = [{
						name: "Nomor Invoice Masuk",
						value: "Nomor Invoice Masuk"
					}, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }, { name: "Nominal Invoice", value: "Nominal Invoice" }
						, { name: "Nomor Invoice Dari Vendor", value: "Nomor Invoice Dari Vendor" }];
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = {};
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Unit") {
						//alert(' masuk 33 - tipe ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran );
						$scope.TambahInstruksiPembayaran_DaftarAdvancePayment = $scope.cr2;
					}
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Vendor";
				}

				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_Unit_Show = false;
				}
				else {
					$scope.TambahInstruksiPembayaran_RincianPembayaran_Unit_Show = false;
				}
			}
		};

		$scope.GetIsAnggaran = function () {
			InstruksiPembayaranFactory.IsAnggaran()
				.then(
					function (res) {
						if (res.data.Result.length != 0) {	
							if (res.data.Result[0].isBudget == 1) {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = 0;
								$scope.AnggaranBelanja_isEnable = true;
								InstruksiPembayaranFactory.getDataAnggaranBelanja()
									.then(
										function (resB) {
											angular.forEach(resB.data.Result, function (value, key) {
												$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 += value.Nominal
											})
										}
									);
							}
							else {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran1 = 0;
								$scope.AnggaranBelanja_isEnable = false;
							}
						}


					}
				);
		}

		// ================================ OLD FILTER FREE SERVICE START ==================================
		// $scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_AccruedFreeServiceFilter_Click = function () {
		// 	if (($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text != "") &&
		// 		($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search != "")) {
		// 		var nomor;
		// 		if ($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "Nomor Invoice Masuk") {
		// 			nomor = 2;
		// 		}
		// 		if ($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "Tanggal Invoice") {
		// 			nomor = 3;
		// 		}
		// 		if ($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "Nama Branch") {
		// 			nomor = 4;
		// 		}
		// 		if ($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "Tanggal Jatuh Tempo") {
		// 			nomor = 6;
		// 		}
		// 		if ($scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search == "Nominal Invoice") {
		// 			nomor = 7;
		// 		}
		// 		//alert(' masukkk ' + nomor );
		// 		// {name:"InvoiceId",displayName:"Id Invoice Masuk", field:"InvoiceId", enableCellEdit: false, enableHiding: false, visible : false},
		// 		// {name:"InvoiceNo",displayName:"Nomor Invoice Masuk", field:"IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false},
		// 		// {name:"InvoiceReceivedDate",displayName:"Tanggal Invoice Masuk", field:"InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"'},
		// 		// {name:"BranchName",displayName:"Nama Branch", field:"BranchName",enableCellEdit: false, enableHiding: false},
		// 		// {name:"NPWP", displayName:"NPWP", field:"NPWP", enableCellEdit: false, enableHiding: false},
		// 		// {name:"MaturityDate",displayName:"Tanggal Jatuh Tempo", field:"MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"'},
		// 		// {name:"Nominal", displayName:"Nominal Invoice", field:"Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'}
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.grid.clearAllFilters();
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text;

		// 	}
		// 	else {
		// 		$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI.grid.clearAllFilters();
		// 	}
		// };
		// ================================ OLD FILTER FREE SERVICE END ==================================

		// ================================ NEW FILTER FREE SERVICE START ==================================
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_AccruedFreeServiceFilter_Click = function () {
			var Mode = '';
			if ($scope.EditMode == true) {
				Mode = "New";
				$scope.mode = Mode;
			}
			InstruksiPembayaranFactory.getDataForAccruedFreeService(1, uiGridPageSize,  $scope.DocumentId, '', Mode, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Text, $scope.TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService_Search, $scope.TambahInstruksiPembayaran_NamaBranch).then(
				function (res) {
					$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = res.data.Result;
					$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.totalItems = res.data.Total;
					$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
				}
			);
		}
		// ================================ OLD FILTER FREE SERVICE END ==================================

		// OLD FIlter 
		// $scope.TambahInstruksiPembayaran_Refund_ButtonFilter = function () {
		// 	if (($scope.TambahInstruksiPembayaran_Refund_TextFilter != "") &&
		// 		($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown != "")) {
		// 		var nomor;
		// 		if ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") {
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor SPK/WO/SO Batal") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal SPK/WO/SO Batal") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Nominal Down Payment") {
		// 				nomor = 3;
		// 			}
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPh 23") {
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Registrasi BP PPh 23") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Registrasi BP PPh 23") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Nomor Billing") {
		// 				nomor = 3;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Tanggal Billing") {
		// 				nomor = 4;
		// 			}
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPN WAPU") {
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Registrasi SSP") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Registrasi SSP") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Nomor Billing") {
		// 				nomor = 3;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Tanggal Billing") {
		// 				nomor = 4;
		// 			}
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "Titipan") {
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Incoming Payment") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Incoming Payment") {
		// 				nomor = 2;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDow == "Nomor Selisih Kelebihan") {
		// 				nomor = 3;
		// 			}
		// 		}
		// 		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "Unknown") {
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Nomor Referensi") {
		// 				nomor = 1;
		// 			}
		// 			if ($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "Tanggal Refund") {
		// 				nomor = 2;
		// 			}
		// 			// ============================ BEGIN | NOTO | 190408 | Refund - Unknown [2] | Grid Filter ============================
		// 		}
		// 		//console.log('masuk ke nomor -->' , nomor);
		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.clearAllFilters();
		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInstruksiPembayaran_Refund_TextFilter;
		// 	}
		// 	else {
		// 		$scope.TambahInstruksiPembayaran_Refund_gridAPI.grid.clearAllFilters();
		// 	}
		// };

		// NEW Filter
		$scope.TambahInstruksiPembayaran_Refund_ButtonFilter = function(){
			$scope.getFilterData(1, uiGridPageSize);
		}

		$scope.getFilterData = function(Start, Limit){
			var NewText = $scope.TambahInstruksiPembayaran_Refund_TextFilter;
			if($scope.TambahInstruksiPembayaran_Refund_SearchDropDown == "TanggalRefund"){
				NewText = $scope.TambahInstruksiPembayaran_Refund_TextFilter.replace(/\//g, "-");
			}
			if($scope.TambahInstruksiPembayaran_TipeRefund == 'Customer Guarantee'){
				filter = [{
					VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
					VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
					RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: 0, SearchText: NewText, SearchType: $scope.TambahInstruksiPembayaran_Refund_SearchDropDown,PIdTampilEdit: $scope.DocumentId
				}];
			}else{
				filter = [{
					VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
					VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
					RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: $scope.DocumentId, SearchText: NewText, SearchType: $scope.TambahInstruksiPembayaran_Refund_SearchDropDown
				}];
			}

			InstruksiPembayaranFactory.getDataForRefundFilter(Start, Limit, filter).then(
				function (res) {
					if(res.data.Result.length > 0){ //customer guarantee cr4 Finance Start
						for(var x in res.data.Result){
							if(res.data.Result[x].TypeGuarantee == 1){
								res.data.Result[x].temp = 'Titipan'
							}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "SAH_SPK_TT"){
								res.data.Result[x].temp = 'Booking Fee dan Down Payment'
							}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "AJob_List_TT"){
								res.data.Result[x].temp = 'Down Payment'
							}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "Apart_SalesOrder_TT"){
								res.data.Result[x].temp = 'Down Payment'
							}else{
								res.data.Result[x].temp = null
							}
						}
					} //customer guarantee cr4 Finance End

					$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					console.log('data dari getDataForRefundFilter => dimasukin ke grid ===>',$scope.TambahInstruksiPembayaran_Refund_UIGrid.data);
					for(var i in res.data.Result){
						res.data.Result[i].DocumentDate = res.data.Result[i].DocumentDate.split(" ")[0];
						res.data.Result[i].xDocumentDate = $scope.changeFormatDate(res.data.Result[i].DocumentDate);
					}
					// $scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.totalItems = res.data.Total;
					console.log("cek data >>>>>>>>", res.data.Result);
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}

		$scope.SetRefundGridColumn = function () {
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [];
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];

			if ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") {
				$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor SPK/WO/SO Batal", value: "Nomor SPK/WO/SO Batal" },
				{ name: "Tanggal SPK/WO/SO Batal", value: "Tanggal SPK/WO/SO Batal" }, { name: "Nominal Down Payment", value: "Nominal Down Payment" }];
				$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
				$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Work Order / Sales Order Batal";
				$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
					{ name: "WOSOSPKNumber", displayName: "Nomor SPK/WO/SO Batal", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
					{ name: "WOSPKDate", displayName: "Tanggal SPK/WO/SO Batal", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "WOSOSPKId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "NominalDP", displayName: "NominalDP", field: "NominalDP", enableCellEdit: false, enableHiding: false, visible: false, visible: false },
					{ name: "NominalGuarantee", displayName: "NominalGuarantee", field: "NominalGuarantee", enableCellEdit: false, enableHiding: false, visible: false },
					{ 
						//customer guarantee cr4 Finance
						name: "temp", displayName: "Keterangan", field: "temp", enableCellEdit: false, enableHiding: false
						// cellTemplate: '<div ng-show="row.entity.TypeGuarantee == 1">Titipan</div><div ng-show="row.entity.TypeGuarantee == 2">Booking Fee dan Down Payment</div>'
					},
					{ name: "NominalDPPure", displayName: "NominalDPPure", field: "NominalDPPure", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "NominalGuaranteePure", displayName: "NominalGuaranteePure", field: "NominalGuaranteePure", enableCellEdit: false, enableHiding: false, visible: false },
				]
			}
			else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPh 23") {
				$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Registrasi BP PPh 23", value: "Nomor Registrasi BP PPh 23" },
				{ name: "Tanggal Registrasi BP PPh 23", value: "Tanggal Registrasi BP PPh 23" }, { name: "Nomor Billing", value: "Nomor Billing" },
				{ name: "Tanggal Billing", value: "Tanggal Billing" }];
				$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
				$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Bukti Potong PPh 23";
				$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
					{ name: "DocumentNumber", displayName: "Nomor Registrasi BP PPh 23", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
					{ name: "DocumentDate", displayName: "Tanggal Registrasi BP PPh 23", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "BillingNo", displayName: "Nomor Billing", field: "BillingNo", enableCellEdit: false, enableHiding: false },
					{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "BuktiPotongNo", displayName: "Nomor Bukti Potong", field: "BuktiPotongNumber", enableCellEdit: false, enableHiding: false },
					{ name: "BuktiPotongDate", displayName: "Tanggal Bukti Potong", field: "BuktiPotongDate", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "PPh 23", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
				]
			}
			else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPN WAPU") {
				$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Registrasi SSP", value: "Nomor Registrasi SSP" },
				{ name: "Tanggal Registrasi SSP", value: "Tanggal Registrasi SSP" }, { name: "Nomor Billing", value: "Nomor Billing" },
				{ name: "Tanggal Billing", value: "Tanggal Billing" }];
				$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
				$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Surat Setoran Pajak WAPU";
				$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
					// {name:"DocumentNumber", displayName:"Nomor Registrasi SSP", field:"DocumentNumber", enableCellEdit: false, enableHiding: false},
					// {name:"DocumentDate", displayName:"Tanggal Registrasi SSP", field:"DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},
					// {name:"BillingNo", displayName:"Nomor Billing", field:"BillingNo", enableCellEdit: false, enableHiding: false},
					// {name:"BillingDate", displayName:"Tanggal Billing", field:"BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},
					// {name:"PPN", displayName:"PPN", field:"Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},							
					// {name:"NTPN", displayName:"NTPN", field:"NTPN", enableCellEdit: false, enableHiding: false},
					// {name:"DocumentId", displayName:"Id", field:"DocumentId", enableCellEdit: false, enableHiding: false, visible:false},
					// {name:"DocumentType", displayName:"DocumentType", field:"DocumentType", enableCellEdit: false, enableHiding: false, visible:false},
					// {name:"InnerType", displayName:"InnerType", field:"InnerType", enableCellEdit: false, enableHiding: false, visible:false},
					// {name:"BuktiPotongDate", displayName:"Tanggal Bayar", field:"PaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},

					{ name: "DocumentNumber", displayName: "Nomor Registrasi SSP", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
					{ name: "DocumentDate", displayName: "Tanggal Registrasi SSP", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "BillingNo", displayName: "Nomor Billing", field: "BillingNo", enableCellEdit: false, enableHiding: false },
					{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "PPN", displayName: "PPN", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "NTPN", displayName: "NTPN", field: "NTPN", enableCellEdit: false, enableHiding: false },
					{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "BuktiPotongDate", displayName: "Tanggal Bayar", field: "PaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				]
			}
			//Tambahan CR4
			else if ($scope.TambahInstruksiPembayaran_TipeRefund == "Titipan") {
				$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" },
				{ name: "Tanggal Incoming Payment", value: "Tanggal Incoming Payment" }, { name: "Nominal Selisih Kelebihan", value: "Nominal Selisih Kelebihan" }];
				$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
				$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Incoming Payment";
				$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
					{ name: "DocumentNumber", displayName: "Nomor Incoming Payment", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
					{ name: "IPId", displayName: "IP ID", field: "IPId", visible:false }, //Tambahan CR4
					{ name: "SOCode", displayName: "Nomor SO / Nomor WO", field: "SOCode"}, //tambahan CR4
					{ name: "DocumentDate", displayName: "Tanggal Incoming Payment", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "Nominal", displayName: "Nominal Selisih Kelebihan Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
				]
			}
			else if ($scope.TambahInstruksiPembayaran_TipeRefund == "Unknown") {
				// ============================ BEGIN | NOTO | 190408 | Refund - Unknown [1] | Onchange DDL Refund Type ============================
				$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown =
					[{ name: "Tanggal Refund", value: "TanggalRefund" },
					{ name: "Nomor Referensi", value: "NomorReferensi" }];
				$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
				$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Unknown Refund";

				$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
					{
						name: "NomorReferensi", displayName: "Nomor Referensi", field: "DocumentNumber", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"',
						cellTemplate: '<a ng-click="grid.appScope.LihatUnknownDetail(row)">{{row.entity.DocumentNumber}}</a>'
					},
					{ name: "DocumentDate", displayName: "Tanggal Refund", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: "Nominal", displayName: "Nominal Refund", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
					{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
					{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
				];
				// ============================ END | NOTO | 190408 | Refund - Unknown [1] | Onchange DDL Refund Type ============================
			}
		};


		//Next Page Lihat 
		$scope.NextPageLihat = function (start, limit){
			var filter;
			filter = [{
				VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
				VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
				RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId: $scope.DocumentId
			}];
			
			InstruksiPembayaranFactory.getDataForRefund(start, limit, filter).then(
				function (res) {
					$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = res.data.Result;
					// $scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.totalItems = res.data.Total;
					$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.paginationPageSize = limit;
					$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.paginationPageSizes = [25, 50, 100];
					console.log("cek data >>>>>>>>", res.data.Result);
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}
		$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed = function () {
			$scope.SetRefundGridColumn();
			if ($scope.TambahInstruksiPembayaran_TipeRefund == "Unknown") {
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = true;
				$scope.ShowLabelRefund = true;
				$scope.ShowLabelKoreksi = false;
			}
			else {
				$scope.TambahInstruksiPembayaran_RincianPembayaran_NoRekTujuan_Show = false;
			}

			if ($scope.EditMode == false) {
				$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = false;
				//alert('masuk sini');
			}
			else {
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[11].visible = true;
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				$scope.Show_TambahInstruksiPembayaran_Refund_Tambah = true;
				//$scope.SetRefundGridColumn();
				$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
				$scope.Show_TambahInstruksiPembayaran_Refund_WoSo = true;
			}

			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund" && ($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == 0 || $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "")) {
				// if ($scope.EditMode != false) {
				// 	bsNotify.show({
				// 		title: "Warning",
				// 		content: 'Nama Pelanggan belum dipilih !',
				// 		type: 'warning'
				// 	});
				// 	//alert('Nama Pelanggan belum dipilih !');
				// }
			}
			else {
				var filter;

				if ($scope.EditMode == false) {
					if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor != undefined && $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor!= null && $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor != ""){
						if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.includes('&')){
							console.log("masuk if & sini")
							var tmpReplacePem = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.replace(/\&/g,"%26")
							filter = [{
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: tmpReplacePem,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId: $scope.DocumentId
							}];
						}else{
							console.log("masuk else & sini")
							filter = [{
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId: $scope.DocumentId
							}];
						}
					}else{
						filter = [{
							VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
							VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
							RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId: $scope.DocumentId
						}];
					}
					InstruksiPembayaranFactory.getDataForRefund(1, uiGridPageSize, filter).then(
						function (res) {
							console.log('MASUKK GETT 2')
						if(res.data.Result.length > 0){ //customer guarantee cr4 Finance Start
							for(var x in res.data.Result){
								if(res.data.Result[x].TypeGuarantee == 1){
									res.data.Result[x].temp = 'Titipan'
								}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "SAH_SPK_TT"){
									res.data.Result[x].temp = 'Booking Fee dan Down Payment'
								}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "AJob_List_TT"){
									res.data.Result[x].temp = 'Down Payment'
								}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "Apart_SalesOrder_TT"){
									res.data.Result[x].temp = 'Down Payment'
								}else{
									res.data.Result[x].temp = null
								}
							}
						}//customer guarantee cr4 Finance End
							$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = res.data.Result;
							for(var i in res.data.Result){
								if(res.data.Result[i].DocumentDate != null){
									res.data.Result[i].DocumentDate = res.data.Result[i].DocumentDate.split(" ")[0];
									res.data.Result[i].xDocumentDate = $scope.changeFormatDate(res.data.Result[i].DocumentDate);			
								}
							}
							// $scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
							$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.totalItems = res.data.Total;
							$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.paginationPageSize = uiGridPageSize;
							$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.paginationPageSizes = [25, 50, 100];
							console.log("cek data >>>>>>>>", res.data.Result);
						},
						function (err) {
							console.log('Error -->', err);
						}
					);
				}
				else {
					console.log('TIPE REFUND =>',$scope.TambahInstruksiPembayaran_TipeRefund)
					if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor != undefined && $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor!= null && $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor != ""){
						if($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.includes('&')){
							console.log("masuk if & sini")
							var tmpReplacePem = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor.replace(/\&/g,"%26")
							if($scope.TambahInstruksiPembayaran_TipeRefund == 'Customer Guarantee'){
								filter = [{
									VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
									VendorName: tmpReplacePem,
									RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: 0, PIdTampilEdit: $scope.DocumentId
								}];
							}else{
								filter = [{
									VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
									VendorName: tmpReplacePem,
									RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: $scope.DocumentId
								}];
							}
						
						}else{
							console.log("masuk else & sini")
							if($scope.TambahInstruksiPembayaran_TipeRefund == 'Customer Guarantee'){
								filter = [{
									VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
									VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
									RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: 0, PIdTampilEdit: $scope.DocumentId
								}];
							}else{
								filter = [{
									VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
									VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
									RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: $scope.DocumentId
								}];
							}
							
						}
					}else{
						if($scope.TambahInstruksiPembayaran_TipeRefund == 'Customer Guarantee'){
							filter = [{
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: 0, PIdTampilEdit: $scope.DocumentId
							}];
						}else{
							filter = [{
								VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
								RefundType: $scope.TambahInstruksiPembayaran_TipeRefund + '|' + $scope.TambahInstruksiPembayaran_NamaBranch, PIId: $scope.DocumentId
							}];
						}
					
					}
					InstruksiPembayaranFactory.getDataForRefund(1, uiGridPageSize, filter).then(
						function (res) {
							console.log('MASUKK GETT 3')
							if(res.data.Result.length > 0){ //customer guarantee cr4 Finance Start
								for(var x in res.data.Result){
									if(res.data.Result[x].TypeGuarantee == 1){
										res.data.Result[x].temp = 'Titipan'
									}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "SAH_SPK_TT"){
										res.data.Result[x].temp = 'Booking Fee dan Down Payment'
									}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "AJob_List_TT"){
										res.data.Result[x].temp = 'Down Payment'
									}else if(res.data.Result[x].TypeGuarantee == 2 && res.data.Result[x].InnerType == "Apart_SalesOrder_TT"){
										res.data.Result[x].temp = 'Down Payment'
									}else{
										res.data.Result[x].temp = null
									}
								}
							} //customer guarantee cr4 Finance End
							$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
							for(var i in res.data.Result){
								res.data.Result[i].DocumentDate = res.data.Result[i].DocumentDate.split(" ")[0];
								res.data.Result[i].xDocumentDate = $scope.changeFormatDate(res.data.Result[i].DocumentDate);
							}
							$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
							$scope.TambahInstruksiPembayaran_Refund_UIGrid.totalItems = res.data.Total;
							$scope.TambahInstruksiPembayaran_Refund_UIGrid.paginationPageSize = uiGridPageSize;
							$scope.TambahInstruksiPembayaran_Refund_UIGrid.paginationPageSizes = [25, 50, 100];
							console.log("cek data >>>>>>>>", res.data.Result);
						},
						function (err) {
							console.log('Error -->', err);
						}
					);
				}
			}
		};

		$scope.GetAllData = function(start, limit){
			filter = [{
				VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
				VendorName: $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor,
				RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId: 0
			}];
			InstruksiPembayaranFactory.getDataForRefund(start, limit, filter).then(
				function (res) {
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					for(var i in res.data.Result){
						res.data.Result[i].DocumentDate = res.data.Result[i].DocumentDate.split(" ")[0];
						res.data.Result[i].xDocumentDate = $scope.changeFormatDate(res.data.Result[i].DocumentDate);
					}
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.totalItems = res.data.Total;
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.paginationPageSize = Limit;
					$scope.TambahInstruksiPembayaran_Refund_UIGrid.paginationPageSizes = [25, 50, 100];
					console.log("cek data >>>>>>>>", res.data.Result);
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}

		$scope.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			console.log("changeFormatDate item", item);
			tmpAppointmentDate = new Date(tmpAppointmentDate);
			console.log("tmpAppointmentDate",tmpAppointmentDate);
			var finalDate
			if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
				var yyyy = tmpAppointmentDate.getFullYear().toString();
				var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
				var dd = tmpAppointmentDate.getDate().toString();
				finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
			} else {
				finalDate = '';
			}
			console.log("changeFormatDate finalDate", finalDate);
			return finalDate;
		};

		$scope.TambahInstruksiPembayaran_Refund_Tambah = function () {
			if ($scope.TambahInstruksiPembayaran_Refund_gridAPI.selection.getSelectedCount() > 0) {
				$scope.TambahInstruksiPembayaran_Refund_gridAPI.selection.getSelectedRows().forEach(function (row) {
					var found = 0;
					$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.forEach(function (obj) {
						//console.log('isi grid -->', obj.PaymentId);
						if ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") {
							if ((row.DocumentId == obj.ReferenceId) && (row.InnerType == obj.InnerType) && (row.TypeGuarantee == obj.TypeGuarantee)) {
								found = 1;
							}
						}else{
							if ((row.DocumentId == obj.ReferenceId) && (row.InnerType == obj.InnerType)) {
								found = 1;
							}
						}
					});

					if (found == 0) {
						if ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") {
							$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += row.Nominal;
							if($scope.draft == "Ya"){
								$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId,
									InnerType: row.InnerType,
									NominalDP: row.NominalDP,
									NominalGuarantee: row.NominalGuarantee,
									TypeGuarantee: row.TypeGuarantee,
									NominalDPPure: row.NominalDPPure,
									NominalGuaranteePure: row.NominalGuaranteePure,
									temp: row.temp //customer guarantee cr4 Finance Start
								});
							}else{
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId,
									InnerType: row.InnerType,
									NominalDP: row.NominalDP,
									NominalGuarantee: row.NominalGuarantee,
									TypeGuarantee: row.TypeGuarantee,
									NominalDPPure: row.NominalDPPure,
									NominalGuaranteePure: row.NominalGuaranteePure,
									temp: row.temp //customer guarantee cr4 Finance Start
								});
							}
						}

						if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPh 23") {
							$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += row.Nominal;
							if($scope.draft == "Ya"){
								$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.BillingNo,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});
							}else{
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.BillingNo,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});
							}
						}

						if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPN WAPU") {
							$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += row.Nominal;
							if($scope.draft == "Ya"){
								$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.BillingNo,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});
							}else{
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.BillingNo,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});
							}

						}
						if ($scope.TambahInstruksiPembayaran_TipeRefund == "Titipan") {
							$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += row.Nominal;
							if($scope.draft == "Ya"){
								$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType,
									IPId: row.IPId, // CR4
									SOCode : row.SOCode , //CR4
								});
							}else{
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType,
									IPId: row.IPId, // CR4
									SOCode : row.SOCode , //CR4
								});
							}
							
						}
						if ($scope.TambahInstruksiPembayaran_TipeRefund == "Unknown") {
							// ============================ BEGIN | NOTO | 190408 | Refund - Unknown [3] | Grid Tambah ============================
							if($scope.draft == "Ya"){
								$scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});
	
							}else{
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data.push({
									RefundId: 0,
									RefundType: row.DocumentType,
									ReferenceNumber: row.DocumentNumber,
									Nominal: row.Nominal,
									ReferenceId: row.DocumentId, InnerType: row.InnerType
								});	
							}
							
						
							// ============================ END | NOTO | 190408 | Refund - Unknown [3] | Grid Tambah ============================
						}
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total = 0;
						if($scope.draft == "Ya"){
							angular.forEach($scope.LihatInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data, function (value) {
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += value.Nominal
							});
						}else{
							angular.forEach($scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data, function (value) {
								$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total += value.Nominal
							});
						}
						$scope.TotalNominal = $scope.TambahInstruksiPembayaran_Refund_DaftarRefund_ToRepeat_Total;
						$scope.CalculateNominalPembayaran();
					}
				});
				$scope.TambahInstruksiPembayaran_Refund_gridAPI.selection.clearSelectedRows();
			}else{
				console.log('ini ga ngapa"in jancok')
			}
		};

		// ============================ BEGIN | NOTO | 190409 | Refund - Unknown [4] | Grid Unknown Detail ============================
		$scope.TambahInstruksiPembayaran_UnknownDetail_UIGrid =
			{
				paginationPageSizes: null,
				useCustomPagination: true,
				useExternalPagination: true,
				multiselect: false,
				enableFiltering: true,
				enableSorting: false,
				enableRowSelection: false,
				enableSelectAll: false,
				columnDefs:
					[
						{ name: 'Metode Pembayaran', field: 'PaymentMethod', visible: true },
						{ name: 'Tanggal Pembayaran', field: 'PaymentDate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
						{ name: 'Nama Bank Pengirim/Credit/Debit Card', field: 'SenderBankId' },
						{ name: 'Nama Pengirim', field: 'SenderName' },
						{ name: 'Nomor Cheque/Credit/Debit Card', field: 'NoCheque' },
						{ name: 'Nomor Rekening Penerima', field: 'ReceiverAccNumber' },
						{ name: 'Nama Bank Penerima', field: 'BankPenerima' },
						{ name: 'Nama Pemegang Rekening', field: 'Penerima' },
						{ name: 'Nama Mesin EDC', displayName: "Nama Mesin EDC", field: 'NamaEDC' },
						{ name: 'Keterangan', field: 'Keterangan' },
						{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahCIP' }
					]
			};
		// ============================ END | NOTO | 190409 | Refund - Unknown [4] | Grid Unknown Detail ============================

		$scope.LihatUnknownDetail = function (row) {
			InstruksiPembayaranFactory.GetListRincianPembayaranUnknown(1, uiGridPageSize, row.entity.DocumentId)
				.then
				(
					function (res) {
						$scope.TambahInstruksiPembayaran_UnknownDetail_UIGrid.data = res.data.Result;
					},
					function (err) {
						console.log("err=>", err);
					}
				);

			angular.element('#ModalBankUnknown').modal('show');
		}

		$scope.ModalBankUnknown_Batal = function (row) {
			angular.element('#ModalBankUnknown').modal('hide');
		}

		$scope.LihatInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch_Click = function () {
			$scope.ShowLihatInstruksiPembayaran_TujuanPembayaran_DaftarInstruksiPembayaran = true;
		};

		$scope.Disable_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = function () {
			$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
			$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
			$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
			$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
		};

		$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed = function () {
			$scope.Disable_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop();
			$scope.Show_LihatInstruksiPembayaran_Tagihan = false;
			$scope.Show_LihatInstruksiPembayaran_Refund = false;
			$scope.Disable_Show_TambahInstruksiPembayaran_Refund();

			if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != null) {
				if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
					$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
					$scope.Show_LihatInstruksiPembayaran_Refund = true;
				}
				else if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
					$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = true;

				}
				else {
					$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
					$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
					$scope.Show_LihatInstruksiPembayaran_Tagihan = true;

				}
			}
		};

		$scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record_SelectedChanged = function () {

			if ($scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record != "") {
				$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = $scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record;
				$scope.ShowLihatInstruksiPembayaran_TujuanPembayaran_DaftarInstruksiPembayaranDetails = true;
			}
		};

		$scope.LihatInstruksiPembayaran_PengajuanReversal = function () {
			InstruksiPembayaranFactory.CheckValidReversal($scope.DocumentId, $scope.user.OutletId).then(
				function (res) {
					if (res.data.Result[0].Status == "Recon") {
						//alert('Data Instruksi Pembayaran sudah ada Outgoing Payment');
						bsNotify.show({
							title: "Warning",
							content: 'Data Instruksi Pembayaran sudah ada Outgoing Payment.',
							type: 'warning'
						});
					}
					else {
						angular.element('#ModalLihatInstruksiPembayaran_PengajuanReversal').modal('show');
					}
				},
				function (err) {
					bsNotify.show({
						title: "Gagal",
						content: 'Reversal gagal diajukan dengan pesan : ' + err.data.ExceptionMessage,
						type: 'danger'
					});
					//alert('Reversal gagal diajukan dengan pesan : ' + err.data.ExceptionMessage);
					console.log('error --> ', err);
				}
			);
		};

		$scope.Batal_LihatInstruksiPembayaran_PengajuanReversalModal = function () {
			angular.element('#ModalLihatInstruksiPembayaran_PengajuanReversal').modal('hide');
		};

		$scope.Jalankan_LihatInstruksiPembayaran_PengajuanReversalModal = function () {
			if ($scope.LihatInstruksiPembayaran_PengajuanReversal_AlasanReversal == "") {
				//alert(' alasan reverse harus diisi');
				bsNotify.show({
					title: "Warning",
					content: 'alasan reverse harus diisi.',
					type: 'warning'
				});
			}
			else {
				var reverse = [
					{
						"DocumentId": $scope.DocumentId,
						"SourceTable": "PaymentInstruction_TT",
						"ReversalReason": $scope.LihatInstruksiPembayaran_PengajuanReversal_AlasanReversal
					}
				]

				InstruksiPembayaranFactory.submitDataReversal(reverse).then(
					function (res) {
						//alert('Reversal berhasil diajukan');
						bsNotify.show({
							title: "Success",
							content: 'Reversal berhasil diajukan.',
							type: 'success'
						});
						angular.element('#ModalLihatInstruksiPembayaran_PengajuanReversal').modal('hide');
						$scope.BackToMainInstruksiPembayaran();
						$scope.Main_Cari();
					},
					function (err) {
						//alert('Reversal gagal diajukan dengan pesan : ' + err.data.ExceptionMessage);
						bsNotify.show({
							title: "Gagal",
							content: 'Reversal gagal diajukan dengan pesan : ' + err.data.Message,
							type: 'danger'
						});
						console.log('error --> ', err);
					}
				);
			}
		};


		$scope.LihatInstruksiPembayaran_PengajuanReversal_Click = function () {
			angular.element('#ModalLihatInstruksiPembayaran_PengajuanReversal').modal('show');
		};

		$scope.Batal_LihatInstruksiPembayaran_PengajuanReversalModal = function () {
			angular.element('#ModalLihatInstruksiPembayaran_PengajuanReversal').modal('hide');
		};

		//------------------------------------NUSE EDIT SECTION BEGIN-------------------------------------------//
		$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain_Changed = function () {
			if ($scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain == "Bea Materai") {
				// if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
				// 	($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") )

				// {
				var tmpNom = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran.split(",")[0];
				var tmpNom2 = tmpNom.toString().replace(/\./g,"");

				console.log("cek TMPNOM", tmpNom)
				console.log("cek TMPNOM22", tmpNom2)
				var nominal;
				InstruksiPembayaranFactory.getBiayaMaterai(tmpNom2).then(
					function (res) {
						if (res.data.Total > 0) {
							nominal = res.data.Result[0].Nominal;
							$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = addSeparatorsNF(nominal, '.', '.', ',');
						}
						else {
							$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
						}
					},
					function (err) {
						console.log('Error ', err);
						$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
					}
				);
				//}
			}
		}
		//------------------------------------NUSE EDIT SECTION END-------------------------------------------//
		$scope.optionsTambahInstruksiPembayaran_VendorSelectKolom = [{ name: "Code", value: "Code" }, { name: "Name", value: "Name" }, { name: "Address", value: "Address" }];
		$scope.TambahInstruksiPembayaran_VendorFilter_Clicked = function () {
		var replaceVendorFilter = $scope.TambahInstruksiPembayaran_VendorFilter.replace(/\&/g,"%26");
		console.log("Replace Vendor", replaceVendorFilter)
			InstruksiPembayaranFactory.getSOVendorList($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, replaceVendorFilter, $scope.TambahInstruksiPembayaran_NamaBranch).then(
				function (res) {
					if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
						res.data.Result.push({
							Id: 0,
							VendorCode: '-',
							Name: 'Unknown',
							Address: '-',
							Phone: '-'
						});
						$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data Pelanggan";
					}
					else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales" ||
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
						$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data SO";
					}
					else {
						$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_Header = "Pilih Data Vendor";
					}
					$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_UIGrid.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			if ($scope.TambahInstruksiPembayaran_VendorSelectKolom == null || $scope.TambahInstruksiPembayaran_VendorSelectKolom == '')
				$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();
			else
				$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();

			if ($scope.TambahInstruksiPembayaran_VendorSelectKolom == 'Code')
				nomor = 2;
			else if ($scope.TambahInstruksiPembayaran_VendorSelectKolom == 'Name')
				nomor = 3;
			else if ($scope.TambahInstruksiPembayaran_VendorSelectKolom == 'Address')
				nomor = 4
			else
				nomor = 0

			if (!angular.isUndefined($scope.TambahInstruksiPembayaran_VendorSelectKolom) && nomor != 0) {
				$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInstruksiPembayaran_VendorFilter;
			}
			$scope.ModalTambahInstruksiPembayaran_GetSOVendorCustomer_gridAPI.grid.queueGridRefresh();

		}
		
		$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: true,
			displaySelectionCheckbox: true,
			enableColumnResizing: true,
			multiselect: true,
			canSelectRows: true,
			columnDefs: [
				// {
				// 	name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
				// 	cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.SelectedInformasiBiaya(row)"></div>'
				// },
				{ name: "Type", displayName: "Tipe Biaya Lain Lain", field: "Type", enableCellEdit: false, enableHiding: false },
				{ name: "DK", displayName: "Debet / Kredit", field: "DK", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahInstruksiPembayaran_InformasiBiaya_gridAPI = gridApi;
			}
		}

		$scope.TambahInstruksiPembayaran_InformasiBiaya_Click = function(){
			// $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data = [];
			var datainsert = false;
			if ($scope.TambahInstruksiPembayaran_InformasiBiaya_gridAPI.selection.getSelectedCount() > 0) {
				var temp = angular.copy($scope.TambahInstruksiPembayaran_InformasiBiaya_gridAPI.selection.getSelectedRows())
				for (var i in temp) {
					console.log('SELECT', temp[i])
					console.log('MASUKKKKKKK11',$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.length)
					datainsert = true;
					if($scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.length != 0){
						for(x = 0; x < $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.length; x++ ){
							if(!($scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[x].Type == temp[i].Type 
								&& $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[x].DK == temp[i].DK
								&& $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[x].Nominal == temp[i].Nominal)){
									console.log('MASUKKKKKKK',x)
									if(datainsert == true){
										datainsert = true;
									}
							}else{
								datainsert = false;
							}
						}
						if (datainsert == true){
							$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.push({
																$$hashKey: temp[i].$$hashKey,
																Type: temp[i].Type,
																DK: temp[i].DK,
																Nominal: temp[i].Nominal
															})
						}
					}else{
						$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.push({
										$$hashKey: temp[i].$$hashKey,
										Type: temp[i].Type,
										DK: temp[i].DK,
										Nominal: temp[i].Nominal
									})
					}
				};
				$scope.TambahInstruksiPembayaran_InformasiBiaya_gridAPI.selection.clearSelectedRows();
			}
			$scope.CalculateNominalPembayaranKoreksi();
		}
		$scope.CalculateNominalPembayaranKoreksi = function(){
			if($scope.TambahInstruksiPembayaran_Bottom_NamaFinance_EnableDisable){
				 
				console.log('ARRAY', $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.length)
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = angular.copy(nominalPembayaranKoreksi);
				console.log('NOMINAL =>', nominalPembayaranKoreksi);
				console.log('NOMINAL 3 =>', $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran);

					if($scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.length != 0){
						var temp = angular.copy($scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data);
						console.log('TEMP 1', temp);
						for(var a in $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data){
							var index;
							temp.some(function(x, i) {
								if (x.DK == $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[a].DK
									&& x.Nominal == $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[a].Nominal
									&& x.Type == $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data[a].Type) return (index = i);
							});
							console.log('TEMP 2', index);
							temp.splice(index, 1);
						}
						
						if(temp.length != 0){
							for(var y in temp){
								if(temp[y].Type != 'Bea Materai'){
									if(temp[y].DK == 'Debet'){
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran -=  
											temp[y].Nominal;
											console.log('MASUK 2')
									}else{
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran += 
											temp[y].Nominal;
											console.log('MASUK 3')
									}
								}else if(temp[y].Type == 'Bea Materai'){
									if(temp[y].DK == 'Debet'){
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran -=  
											temp[y].Nominal;
											console.log('MASUK 4')
									}
								}
							}
						}
					}else{
						if($scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data.length > 0){
							for(var i in $scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data){
								if($scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data[i].Type != 'Bea Materai'){
									if($scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data[i].DK == 'Debet'){
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran -=  
										$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data[i].Nominal;
									}else{
										$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran +=  
										$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data[i].Nominal;
									}
								}
							}
						}
					}

				console.log('Sebelum', angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran));
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = addSeparatorsNF($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran.toFixed(2), '.', ',', '.');	
				console.log('sesudah', angular.copy($scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran));
			}
		}

		$scope.ClearGridKoreksi = function(){
			console.log('HAPUSSS')
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSPK_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoWO_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSO_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSO_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalBookingFee_Text = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text = null;
			$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = null;
			$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_JatuhTempo = null;
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = null;
			$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = null;
			$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = null;
		}

		$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				{ name: "Type", displayName: "Tipe Biaya Lain Lain", field: "Type", enableCellEdit: false, enableHiding: false },
				{ name: "DK", displayName: "Debet / Kredit", field: "DK", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<a ng-hide="grid.appScope.Show_TambahInstruksiPembayaran_EditDraft_button == true || row.entity.Type.toLowerCase().includes(\'titipan\') || row.entity.Type.toLowerCase().includes(\'selisih\') || (row.entity.Type.toLowerCase().includes(\'bea materai\') && row.entity.DK == \'Kredit\') " ng-click="grid.appScope.DeleteGridInfoBiaya(row)">Hapus</a>'
				},
			],
		}

		$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				
				{ name: "NomorIncomingPayment", displayName: "Nomor Incoming Payment", field: "NomorIncomingPayment", enableCellEdit: false, enableHiding: false },
				{ name: "NominalIncomingPayment", displayName: "Nominal Incoming Payment", field: "NominalIncomingPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "NamaCustomer", displayName: "Nama Customer", field: "NamaCustomer", enableCellEdit: false, enableHiding: false },
				{ name: "OutletId", displayName: "Outlet ID", field: "OutletId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "IPParentId", displayName: "IPParentId", field: "IPParentId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "StatusCode", displayName: "StatusCode", field: "StatusCode", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "Module", displayName: "Jenis", field: "Module", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "BillingId", displayName: "Billing ID", field: "BillingId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "NomorBilling", displayName: "Nomor Billing", field: "NomorBilling", enableCellEdit: false, enableHiding: false },
				{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
				{ name: "LastModifiedDate", displayName: "LastModifiedDate", field: "LastModifiedDate", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "LastModifiedUserId", displayName: "LastModifiedUserId", field: "LastModifiedUserId", enableCellEdit: false, enableHiding: false, visible: false },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<a ng-click="grid.appScope.ChooseIncomingOrBilling(row)">Pilih</a>'
				}
			],
		}

		var ModuleType;
		var dataFilterincoming;
		$scope.ChooseIncomingOrBilling = function (row){
			// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
			console.log('INI ROW',row)
			ModuleType = row.entity.Module;
			$scope.showCariNomorIncomingPayment = false;
			$scope.showCariNomorBilling = false;

			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 1){ //jika incoming payment
				var TempBillingNo = row.entity.NomorBilling;

				// var TypeIncoming = TempBillingNo.slice(TempBillingNo.lastIndexOf('|') + 1);
				// var BillingNomor = TempBillingNo.slice(0, TempBillingNo.lastIndexOf('|') + 0);
				// var ProformaInvoiceCategoryId = TempBillingNo.slice(0, TempBillingNo.lastIndexOf('|') + 2);
				// var [BillingNomor, TypeIncoming, ProformaInvoiceCategoryId] = TempBillingNo.split('|');
				var temp = TempBillingNo.split('|');
				var TypeIncoming = temp[1];
				var BillingNomor = temp[0];
				var ProformaInvoiceCategoryId = temp[2]
				console.log('SPLICE =>', TypeIncoming,BillingNomor,ProformaInvoiceCategoryId)
	
				console.log('Hasil Pecah nobilling =>', TempBillingNo , ' | ',TypeIncoming,' | ',BillingNomor);
				NoBilling = BillingNomor == '0' ? null : BillingNomor;
				console.log('NoBilling =>',NoBilling)
				if(BillingNomor != '0' && (TypeIncoming == '901' || TypeIncoming == '902' || TypeIncoming == '909' || TypeIncoming == '911' || (TypeIncoming == '904' && ProformaInvoiceCategoryId == '1'))){
					bsNotify.show({
						title: "Warning",
						content: "Incoming ini sudah ada billing dengan nomor " + BillingNomor,
						type: "warning"
					});
					return false;
				}
			}
			
			console.log('tempData ==', ModuleType);
			if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 2){
				InstruksiPembayaranFactory.getDetailIncomingPaymentAndBillingFilter(row.entity)
				.then(
					function (res) {
						$scope.ClearGridKoreksi();
						console.log('Dataa',res.data);
						$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data;
						dataFilterincoming = $scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data;
						$scope.ShowKoreksi();
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			}else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 1) {
				$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [row.entity];
				$scope.ShowKoreksi();
			}
		}

		$scope.ShowKoreksi = function(){
			if(ModuleType == 'Unit'
			&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 1){
				// True
				$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
				$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
				$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
				$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = true;
				$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
				// False
				$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = false;
				$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = false;
				$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = false;
			}else if(ModuleType == 'Unit'
					&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 2){
					// True
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
					// False
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = false;
			}else if(ModuleType == 'Service'
			&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 1){
					// True
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
					// False
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = false;
			}else if(ModuleType == 'Service'
			&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 2){
					// True
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = true;
					// False
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = false;
			}else if(ModuleType == 'Parts'
			&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 1){
					// True
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
					// False
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = false;
			}else if(ModuleType == 'Parts'
			&& $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif == 2){
					// True
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = true;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = true;
					$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = true;
					// False
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = false;
					$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = false;
			}
			$scope.filterOptionKoreksi();
		}

		$scope.filterOptionKoreksi = function(){
			if(ModuleType == 'Service'){
				$scope.optionsTambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Search =
					[{ name: "Nomor Work Order", value: "WONo" },
					{ name: "Nominal Down Payment", value: "NominalDownPayment" },
					{ name: "Customer Guarantee", value: "CustomerGuarantee" },
					{ name: "No Billing", value: "BillingCode" },
					{ name: "Nominal Billing", value: "NominalBilling" },
					{ name: "Account Receivable", value: "AccountReceivable" }];
			}else if(ModuleType == 'Parts'){
				$scope.optionsTambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Search =
					[{ name: "Nomor Sales Order", value: "SONo" },
					{ name: "Nominal Down Payment", value: "NominalDownPayment" },
					{ name: "Customer Guarantee", value: "CustomerGuarantee" },
					{ name: "No Billing", value: "BillingCode" },
					{ name: "Nominal Billing", value: "NominalBilling" },
					{ name: "Account Receivable", value: "AccountReceivable" }];
			}
		}
		$scope.DeleteGridInfoBiaya = function(row){
			if(row.entity.Type.toLowerCase().includes('titipan') || row.entity.Type.toLowerCase().includes('selisih') || (row.entity.Type.toLowerCase().includes('bea materai') && row.entity.DK == 'Kredit')){
				bsNotify.show({
					title: "Warning",
					content: 'Biaya bertipe Bae Materai kredit, titipan  dan selisih tidak bisa dihapus',
					type: 'warning'
				});
			}else{
				var index = $scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.indexOf(row.entity);
				$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.splice(index, 1);
				$scope.CalculateNominalPembayaranKoreksi();
			}

		}

		$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Filter_Click = function(){
			var tempData = Object.assign(dataFilter)
			var tmpSearch = $scope.TambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Search ;
			var tmpText = $scope.TambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Text;	
			$scope.filterKoreksi(tmpSearch,tmpText,tempData,2);
		}

		$scope.filterKoreksi = function(tmpSearch,tmpText,tempData,position){
			var temp = [];	
			console.log('sdsd', tmpSearch)
			if(tmpSearch && tmpText){
				console.log('masuk 1')
				for (var i in tempData) {
					if(tempData[i][tmpSearch] != null){
						var cnvrt = tempData[i][tmpSearch].toString();
						if(cnvrt.toLowerCase().includes(tmpText.toLowerCase())){
							temp.push(tempData[i]);
							console.log('DATA',temp)
						}
					}
				}
				if(position == 1){
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = temp;
				}else{
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = temp;
				}
			
			}else if(tmpText && (tmpSearch == undefined || tmpSearch == '' || tmpSearch == null)){
				console.log('masuk )2')
				var filter = position == 2 ? $scope.optionsTambahInstruksiPembayaran_InformasiKoreksiAdministrasi_Search
							: $scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search;

				for(var i in tempData){
					for(var x in filter) { 
						var valFilter = filter[x].value;
						if(tempData[i][valFilter] != null){
							var cnvrt = tempData[i][valFilter].toString();
							if (cnvrt.toLowerCase().includes(tmpText.toLowerCase())) {
								temp.push(tempData[i]);
								console.log('DATA1',temp)
								break;
							}
						}
					}
				}
				if(position == 1){
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = temp;
				}else{
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = temp;
				}
			}else{
				console.log('masuk 3')
				if(position == 1){
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = dataFilterincoming;
				}else{
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
					$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = dataFilter;
				}
			}
		}

		$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid = {
			paginationPageSizes: [25, 50, 100],
			paginationPageSize: 25,
			useCustomPagination: false,
			useExternalPagination: false,
			enableFiltering: true,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			columnDefs: [
				{ name: "IncomingInstructionId", displayName: "Id", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false,visible: false },
				{ name: "SOCode", displayName: "No So", field: "SOCode", enableCellEdit: false, enableHiding: false },
				{ name: "SODate", displayName: "Tanggal So", field: "SODate", enableCellEdit: false, enableHiding: false, cellFilter: 'tanggalCIP' },
				{ name: "NominalDownPayment", displayName: "Nominal Down Payment", field: "NominalDownPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi' },
				{ name: "CustomerGuarantee", displayName: "Customer Guarantee", field: "CustomerGuarantee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi'},
				// { name: "IncomingDate", displayName: "No Billing", field: "IncomingDate", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'rupiahCIP' },
				{ name: "BillingCode", displayName: "No Billing", field: "BillingCode", enableCellEdit: false, enableHiding: false, enableFiltering: true},
				{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' },
				{ name: "AccountReceivable", displayName: "Account Receivable", field: "AccountReceivable", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' }		//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
			],
		}

		$scope.SelectedInformasiKoreksi = function(res){
			 
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Text = null;
			$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = [];
			$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data = [];
			
			if(res != null){
				for(var i in res.data.Result[0].Biaya){
					res.data.Result[0].Biaya[i].DebitCredit = res.data.Result[0].Biaya[i].DebitCredit == 'D' ? res.data.Result[0].Biaya[i].DebitCredit = 'Debet' 
															: res.data.Result[0].Biaya[i].DebitCredit = 'Kredit';
					$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.push({
						Type: res.data.Result[0].Biaya[i].ChargesType,
						DK: res.data.Result[0].Biaya[i].DebitCredit,
						Nominal: res.data.Result[0].Biaya[i].Nominal
					})
				}
			}
	
			console.log('MASUK AUTO 4',tempBiayaDaftarIp)
			console.log('STATUS', $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable)
			if(tempBiayaDaftarIp.length > 0){
				for(var x in tempBiayaDaftarIp){
					console.log('MASUK AUTO ')
					 tempBiayaDaftarIp[x].DK == 'K' ? tempBiayaDaftarIp[x].DK ='Kredit' : tempBiayaDaftarIp[x].DK ='Debet';
					if (tempBiayaDaftarIp[x].Type.toLowerCase().includes('titipan') || tempBiayaDaftarIp[x].Type.toLowerCase().includes('selisih') 
					|| (tempBiayaDaftarIp[x].Type.toLowerCase().includes('bea materai') && tempBiayaDaftarIp[x].DK == 'Kredit')){
						console.log('MASUK AUTO 1',$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data)
						if(!$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranDDL_EnableDisable){
							console.log('MASUK AUTO 2')
							$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data.push({
								Type: tempBiayaDaftarIp[x].Type,
								DK: tempBiayaDaftarIp[x].DK,
								Nominal: tempBiayaDaftarIp[x].Nominal
							})
						}
						
					}
				}
			//	console.log('JALAN ini', row)
				$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = [];
				$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = tempBiayaDaftarIp;
				$scope.CalculateNominalPembayaranKoreksi();
			}
		}

		$scope.HideMenuKoreksiAdministratif = function (){
			$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart = false;
			$scope.Show_TambahInstruksiPembayaran_Koreksi_UnitServicePart_DaftarIncomingPayment = false;
			$scope.Show_TambahInstruksiPembayaran_UnitServicePart_IndormasiKoreksiAdministratif = false;
			$scope.Show_TambahInstruksiPembayaran_Koreksi_Unit = false;
			$scope.Show_TambahInstruksiPembayaran_UnitServicePart_InformasiBiaya = false;
			$scope.Show_TambahInstruksiPembayaran_UnitServicePart_NomorBilling = false;
			$scope.Show_TambahInstruksiPembayaran_Koreksi_Service = false;
			$scope.Show_TambahInstruksiPembayaran_Koreksi_Part = false;

			$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [];
			$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
			
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif = undefined;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text = undefined;
		
		}

		$scope.DeleteGridInformasiBiaya = function (row) {
			var index = $scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.indexOf(row.entity);
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.splice(index, 1);
		};

		var tempBiayaDaftarIp;
		var nominalPembayaranKoreksi;
		var dataFilter;
		var IPParentId;
		var OutletId;
		var NoBilling;
		$scope.SelectedIncomingPayment = function(row){
			$scope.informasiKoreksiAdministratifModeGrid(ModuleType);
			nominalPembayaranKoreksi = 0;
			IPParentId = row.entity.IPParentId;
			OutletId = row.entity.OutletId;
			nominalPembayaranKoreksi = angular.copy(row.entity.NominalIncomingPayment)
			console.log('NOMINAL 1 =>', row.entity.NominalIncomingPayment);
			console.log('NOMINAL 2 =>', nominalPembayaranKoreksi);
			console.log(row.entity)
			console.log($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif)
			console.log(ModuleType)
			console.log('MODEEEE =>',$scope.EditMode)
			if($scope.EditMode){
			InstruksiPembayaranFactory.getInformasiIncomingPaymentAndBillingFilter(ModuleType, row.entity,0)
						.then(
							function (res) {
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSPK_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoWO_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSO_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSO_Text = null;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalBookingFee_Text = null;
								$scope.TambahInstruksiPembayaran_InformasiBiaya_UIGrid.data = [];
								$scope.TambahInstruksiPembayaran_InformasiBiaya_Action_UIGrid.data = [];
								tempBiayaDaftarIp = [];
								console.log(res.data, 'search ip');
								console.log(res, 'search ip 2');
								 //$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data = res.data;
								$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = [];
								if(ModuleType == 'Unit'){
									$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = res.data[0].SO;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSPK_Text = res.data[0].FormSPKNo;
									var tanggal = new Date(res.data[0].SPKDate == null ? '' : res.data[0].SPKDate);
									tanggal.setMinutes(tanggal.getMinutes() + 420);
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = tanggal;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalBookingFee_Text = res.data[0].NominalBookingFee;
								}else if(ModuleType == 'Parts'){
									$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = res.data[0].Detail;
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_NoSO_Text = res.data[0].SalesOrderNo;
									// // $scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = res.data[0].SODate;
									// var tanggal = new Date(res.data[0].SODate);
									// tanggal.setMinutes(tanggal.getMinutes() + 420);
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TglSPK_Text = tanggal;

								}else{
									for(var x in res.data[0].Detail){
										if(res.data[0].Detail[x].WONo == "0" || res.data[0].Detail[x].WONo == null){
											res.data[0].Detail[x].WONo = null;
											res.data[0].Detail[x].JobDate = null;
										}
									}
									$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data = res.data[0].Detail;
									
								//	$scope.TambahInstruksiPembayaran_TujuanPembayaran_NoWO_Text = res.data[0].WONo;
									// $scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = res.data[0].JobDate;
									//var tanggal = new Date(res.data[0].JobDate);
									//tanggal.setMinutes(tanggal.getMinutes() + 420);
								//	$scope.TambahInstruksiPembayaran_TujuanPembayaran_TglWO_Text = tanggal;

								}
								dataFilter = Object.assign($scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.data);
								tempBiayaDaftarIp = res.data[0].Biaya;
								$scope.SelectedInformasiKoreksi(null);
								$scope.CalculateNominalPembayaranKoreksi();
							},
							function (err) {
								console.log("err=>", err);
								bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								});
							}
						);
			}
		}

		$scope.informasiKoreksiAdministratifModeGrid = function(ModuleType){
			$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.columnDefs = [];
			console.log('TYPEEEE',ModuleType)
			if(ModuleType == 'Unit'){
				$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.columnDefs = [
					{ name: "IncomingInstructionId", displayName: "Id", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false,visible: false },
					{ name: "SOCode", displayName: "No So", field: "SOCode", enableCellEdit: false, enableHiding: false },
					{ name: "SODate", displayName: "Tanggal So", field: "SODate", enableCellEdit: false, enableHiding: false, cellFilter: 'tanggalCIP' },
					{ name: "NominalDownPayment", displayName: "Nominal Down Payment", field: "NominalDownPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "CustomerGuarantee", displayName: "Customer Guarantee", field: "CustomerGuarantee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi'},
					{ name: "BillingCode", displayName: "No Billing", field: "BillingCode", enableCellEdit: false, enableHiding: false, enableFiltering: true},
					{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "AccountReceivable", displayName: "Account Receivable", field: "AccountReceivable", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' }		//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
				]
			}else if(ModuleType == 'Parts'){
				$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.columnDefs = [
					{ name: "IncomingInstructionId", displayName: "Id", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false,visible: false },
					{ name: "SalesOrderNo", displayName: "Nomor Sales Order", field: "SalesOrderNo", enableCellEdit: false, enableHiding: false },
					{ name: "SODate", displayName: "Tanggal Sales Order", field: "SODate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalDownPayment", displayName: "Nominal Down Payment", field: "NominalDownPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "CustomerGuarantee", displayName: "Customer Guarantee", field: "CustomerGuarantee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi'},
					{ name: "BillingCode", displayName: "No Billing", field: "BillingCode", enableCellEdit: false, enableHiding: false, enableFiltering: true},
					{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "AccountReceivable", displayName: "Account Receivable", field: "AccountReceivable", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' }		//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
				]
			}else if(ModuleType == 'Service'){
				$scope.TambahInstruksiPembayaran_InformasiKoreksiAdministratif_UIGrid.columnDefs = [
					{ name: "IncomingInstructionId", displayName: "Id", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false,visible: false },
					{ name: "WONo", displayName: "Nomor Work Order/Appointment", field: "WONo", enableCellEdit: false, enableHiding: false },
					{ name: "JobDate", displayName: "Tanggal Work Order/Appointment", field: "JobDate", enableCellEdit: false, enableHiding: false,cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalDownPayment", displayName: "Nominal Down Payment", field: "NominalDownPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "CustomerGuarantee", displayName: "Customer Guarantee", field: "CustomerGuarantee", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIPKoreksi'},
					{ name: "BillingCode", displayName: "No Billing", field: "BillingCode", enableCellEdit: false, enableHiding: false, enableFiltering: true},
					{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'rupiahCIPKoreksi' },
					{ name: "AccountReceivable", displayName: "Account Receivable", field: "AccountReceivable", enableCellEdit: false, enableHiding: false, visible: true , cellFilter: 'rupiahCIPKoreksi'}		//,{name:"AdvancePaymentIdOri", displayName:"Nomor Advance Payment Ori", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false	}
				]
			}

		}

		// KOREKSI ADMINISTRATIF
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_Koreksi_Search_Click = function (data) {
			if(data.length >= 5){
				$scope.ClearGridKoreksi();
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text = data;
				$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data = [];
				console.log('CARI', $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text,
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif);
	
				/* -------------------INCOMING PAYMENT ---------------------*/
				if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif === 1){
					$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.columnDefs = [];
					$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.columnDefs = [
							{ name: "NomorIncomingPayment", displayName: "Nomor Incoming Payment", field: "NomorIncomingPayment", enableCellEdit: false, enableHiding: false },
							{ name: "NominalIncomingPayment", displayName: "Nominal Incoming Payment", field: "NominalIncomingPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP' },
							{ name: "NamaCustomer", displayName: "Nama Customer", field: "NamaCustomer", enableCellEdit: false, enableHiding: false },
							{ name: "OutletId", displayName: "Outlet ID", field: "OutletId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "IPParentId", displayName: "IPParentId", field: "IPParentId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "StatusCode", displayName: "StatusCode", field: "StatusCode", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "Module", displayName: "Jenis", field: "Module", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "BillingId", displayName: "Billing ID", field: "BillingId", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "NomorBilling", displayName: "Nomor Billing", field: "NomorBilling", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false  },
							{ name: "LastModifiedDate", displayName: "LastModifiedDate", field: "LastModifiedDate", enableCellEdit: false, enableHiding: false, visible: false },
							{ name: "LastModifiedUserId", displayName: "LastModifiedUserId", field: "LastModifiedUserId", enableCellEdit: false, enableHiding: false, visible: false },
							{
								name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
								cellTemplate: '<a ng-click="grid.appScope.ChooseIncomingOrBilling(row)">Pilih</a>'
							}
						];
						if(!angular.isUndefined($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text)){
							InstruksiPembayaranFactory.getIncomingPaymentFilter(1, 1000, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text)
							.then(
								function (res) {
									console.log('garapan 8269');
									console.log(res.data, 'search ip');
									$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data = res.data;
									//  var tmpNobilling = res.data[0].NomorBilling.slice(0, res.data[0].NomorBilling.lastIndexOf('|') + 0);
									// var [BillingNomor, TypeIncoming, ProformaInvoiceCategoryId] = res.data[0].NomorBilling.split('|');
									var temp = res.data[0].NomorBilling.split('|');
									var BillingNomor = temp[0];
									NoBilling = BillingNomor == '0' ? null : BillingNomor;
									console.log('NoBilling =>',NoBilling)
									$scope.showCariNomorIncomingPayment = true;
									$scope.showCariNomorBilling = false;
								},
								function (err) {
									console.log("err=>", err.data.Message);
									bsNotify.show({
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									});
								}
							);
						}
				}
				/* ------------------- BILLING ---------------------*/
				else if($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif === 2) 
				{
					$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.columnDefs = [];
					$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.columnDefs = [
						{ name: "NomorIncomingPayment", displayName: "Nomor Incoming Payment", field: "NomorIncomingPayment", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "NominalIncomingPayment", displayName: "Nominal Incoming Payment", field: "NominalIncomingPayment", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP', visible: false },
						{ name: "OutletId", displayName: "Outlet ID", field: "OutletId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "IPParentId", displayName: "IPParentId", field: "IPParentId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "StatusCode", displayName: "StatusCode", field: "StatusCode", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "Module", displayName: "Jenis", field: "Module", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "BillingId", displayName: "Billing ID", field: "BillingId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "NomorBilling", displayName: "Nomor Billing", field: "NomorBilling", enableCellEdit: false, enableHiding: false},
						{ name: "NominalBilling", displayName: "Nominal Billing", field: "NominalBilling", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahCIP'},
						{ name: "NamaCustomer", displayName: "Nama Customer", field: "NamaCustomer", enableCellEdit: false, enableHiding: false },
						{ name: "LastModifiedDate", displayName: "LastModifiedDate", field: "LastModifiedDate", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "LastModifiedUserId", displayName: "LastModifiedUserId", field: "LastModifiedUserId", enableCellEdit: false, enableHiding: false, visible: false },				
						{
							name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
							cellTemplate: '<a ng-click="grid.appScope.ChooseIncomingOrBilling(row)">Pilih</a>'
						}
					];
					if(!angular.isUndefined($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text)){
						InstruksiPembayaranFactory.getBillingFilter(1, 1000, $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif_Text)
						.then(
							function (res) {
								console.log(res.data, 'search billing');
								$scope.TambahCariNomorIncomingPaymentOrBilling_UIGrid.data = res.data;
								NoBilling = res.data[0].NomorBilling
								$scope.showCariNomorBilling = true;
								$scope.showCariNomorIncomingPayment = false;
							},
							function (err) {
								console.log("err=>", err);
								bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								});
							}
						);
					}
				}	
			}
		}

		$scope.BackCariNomorIncomingPaymentOrBilling = function(){
			$scope.showCariNomorIncomingPayment = false;
			$scope.showCariNomorBilling = false;
		}

		$scope.changeNomortipe = function(selected){
			selected = parseInt(selected);
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_KoreksiAdministratif = selected;

			console.log(selected)
			if(selected == 1){
				$scope.NumberType = true
				$scope.nomorIncoming = true;
				$scope.nomorBilling = false;
			}else{
				$scope.NumberType = true
				$scope.nomorBilling = true;
				$scope.nomorIncoming = false;
	
			}
		}

		//Function Saparator Start
		$scope.saparator = function(nominal){
			var cekDesimal = "";
			var nominalCopy = angular.copy(nominal);
			if (nominalCopy.substr(nominalCopy.length-3,1) == "."){
				SisaFloat = nominalCopy.substr(nominalCopy.length-3);
				nominalCopy = nominalCopy.substr(0,nominalCopy.length-3);
				cekDesimal = "."
			}else if (nominalCopy.substr(nominalCopy.length-2,1) == "."){
				SisaFloat = nominalCopy.substr(nominalCopy.length-2);
				nominalCopy = nominalCopy.substr(0,nominalCopy.length-2);
				cekDesimal = "."
			}else if (nominalCopy.substr(nominalCopy.length-3,1) == ","){
				SisaFloat = nominalCopy.substr(nominalCopy.length-3);
				nominalCopy = nominalCopy.substr(0,nominalCopy.length-3);
				cekDesimal = ","
			}else if (nominalCopy.substr(nominalCopy.length-2,1) == ","){
				SisaFloat = nominalCopy.substr(nominalCopy.length-2);
				nominalCopy = nominalCopy.substr(0,nominalCopy.length-2);
				cekDesimal = ","
			}

			if(cekDesimal == "."){
				var val = nominalCopy.split(",").join("");
		
				val = val.replace(/[a-zA-Z\s]/g, '');
				val = String(val).split("").reverse().join("")
					.replace(/(\d{3}\B)/g, "$1,")
					.split("").reverse().join("");

				if (val == ""){
					val = "0.00";
				}

				if (SisaFloat == "undefined" || !SisaFloat){
					nominalCopy = val.split(",").join("") +".00";
				}
				else{
					nominalCopy = val.split(",").join("") + SisaFloat;
				}
			}else{
				var val = nominalCopy.split(".").join("");
		
				val = val.replace(/[a-zA-Z\s]/g, '');
				val = String(val).split("").reverse().join("")
					.replace(/(\d{3}\B)/g, "$1.")
					.split("").reverse().join("");

				if (val == ""){
					val = "0.00";
				}

				if (SisaFloat == "undefined" || !SisaFloat){
					nominalCopy = val.split(".").join("") +".00";
				}
				else{
					nominalCopy = val.split(".").join("") + SisaFloat.replace(",",".");
				}
			}
			return nominalCopy ;

		}
		//Function Saparator End

	});

// app.filter('rupiahCIP', function () {
// 	return function (val) {
// 		if (angular.isDefined(val)) {
// 			var valS = val.toString().split('.');
// 			val = valS[0];
// 			while (/(\d+)(\d{3})/.test(val.toString())) {
// 				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
// 			}
// 			if (valS.length > 1 )
// 			{
// 				val = val + ','+valS[1].substring(0, 2);
// 			}

// 		}
// 		return val;
// 	};
// });

app.filter('tanggalCIP', function () {
	return function (val) {
		console.log('Tanggal SO',val)
		var d;
		if(val != null){
			if (new Date(val) == 'Invalid Date') {
				val = val.replace(/(\d+[/])(\d+[/])/, '$2$1');
			}
			d = new Date(val),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;

			return [day, month, year].join('-');

		}else{
			return null
		}
		
	};
});

app.filter('rupiahCIPKoreksi', function () {
	return function (val) {
		if(val == null){
			val = null;
		}else{
			if (angular.isDefined(val)) {
				while (/(\d+)(\d{3})/.test(val.toString())) {
					val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
				}
			}
		}
		return val;
	};
});

app.filter('rupiahCIP', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});
