angular.module('app')
	.factory('RegisterChequeFactory', function ($http, CurrentUser) {
	return{
		getAllBankData:function(){
			var url = '/api/fe/FinanceBank/GetAllData';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
        
		getDataStatus:function(){
			var url = '/api/fe/financeparam/ChequeStatus';
			console.log("url status cek : ", url);
			var res=$http.get(url);
			return res;
        },
        
		getDataCL:function(start, limit, filter){
            if (filter != '')
            { 
                var url = '/api/fe/FinanceCheque/?start=' + start + '&limit='+ limit + '&filterData' + filter ;
            }
			else {
                var url = '/api/fe/FinanceCheque/?start=' + start + '&limit='+ limit ;
            }
			console.log("url get Data Cheque : ", url);
			var res=$http.get(url);
			return res;
        },
        getDataCLByDate:function(start, limit, filter, startdate, enddate){
            if (filter != '')
            { 
                var url = '/api/fe/FinanceCheque/GetDataByDate/?start=' + start + '&limit='+ limit  + '&filterData' + filter+ '&startdate='+ startdate + '&enddate='+ enddate ;
            }
			else {
                var url = '/api/fe/FinanceCheque/GetDataByDate/?start=' + start + '&limit='+ limit  + '&filterData' + filter+ '&startdate='+ startdate + '&enddate='+ enddate ;
            }
			console.log("url get Data Cheque : ", url);
			var res=$http.get(url);
			return res;
        },
        
		create : function(inputData){
			var url = '/api/fe/FinanceCheque/';
			var param = [];
			param.push(inputData);
			console.log("object input FinanceCheque ", param);
			var res=$http.post(url, param);
			return res;
		},
        update : function(inputData){
			var url = '/api/fe/FinanceCheque/';
			var param = [];
			param.push(inputData);
			console.log("object put FinanceCheque ", param);
			var res=$http.put(url, param);
			return res;
        },
        delete : function(inputData){
			var url = '/api/fe/FinanceCheque/';
			var param = [];
			param.push(inputData);
			
			console.log("object delete FinanceCheque ", param);
			 
			var res=$http.delete(url, {data:param, headers:{'content-type' : 'application/json'}});
			//var res=$http.delete(url, param);
			return res;
		},
        getDataPandC : function(name){
			if (name !='' && name != undefined )
				var url = '/api/fe/Customers/GetProspectAndCustomerByName?Name='+name; 
			else 
				var url = '/api/fe/Customers/GetProspectAndCustomer';
			var res=$http.get(url);
			console.log("object get Cust ", res);
			return res;
		}
 
	}
});