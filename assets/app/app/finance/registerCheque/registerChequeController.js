var app = angular.module('app');
app.controller('RegisterChequeController', function ($scope, $http, $filter, CurrentUser, RegisterChequeFactory, $timeout, bsNotify) {

    console.log("curent ueser ", CurrentUser);
    $scope.SimpanCounter = 0;
    $scope.RegisterCheque_Data = {};

    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEnd = {
        minDate: new Date(),
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    RegisterChequeFactory.getAllBankData()
        .then(
            function (res) {

                console.log("banklist", res);
                var bankList = [];
                angular.forEach(res.data.Result, function (value, key) {
                    bankList.push({ "name": value.BankName, "value": value.BankId });
                });
                $scope.optionsBankName = bankList;
            }
        );
    RegisterChequeFactory.getDataStatus()
        .then(
            function (res) {
                console.log("Status", res);
                var statusList = [];
                angular.forEach(res.data, function (value, key) {
                    if (value.name != 'Cair') {
                        var iValue = parseInt(value.value, 10);
                        statusList.push({ "name": value.name, "value": iValue });
                    }
                });
                $scope.optionsStatus = statusList;
            }
        );
    $scope.getData = function () {
        RegisterChequeFactory.getDataCL(1, 100, '')
            .then(
                function (res) {
                    $scope.grid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );
    }

    $scope.RegisterCheque_Cari_Clicked = function () {
        $scope.getDataByDate();
    };
    $scope.getDataByDate = function () {
        RegisterChequeFactory.getDataCLByDate(1, 100, '',
            $filter('date')(new Date($scope.StartDate), 'yyyy-MM-dd'),
            $filter('date')(new Date($scope.EndDate), 'yyyy-MM-dd'))
            .error(
                function (res) {
                    console.log(res);
                }
            )
            .then(
                function (res) {
                    $scope.grid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );
    }
    $scope.BankName_Changed = function () {
        // if(angular.isUndefined($scope.BankId)){
        // 	$scope.BankId = $scope.RegisterCheque_Data.BankId;
        // }
        console.log("Selected Bank Id", $scope.RegisterCheque_Data.BankId);
    }

    $scope.givePattern = function (item, event) {
        console.log("item", item);
        if (event.which > 37 && event.which < 40) {
            event.preventDefault();
            return false;
        } else {
            $scope.RegisterCheque_Data.Amount = $scope.RegisterCheque_Data.Amount.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return;
        }
    };


    $scope.grid = {
        paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,
		paginationPageSizes: [10, 25, 50],

        columnDefs: [
            { name: 'ChequeId', field: 'ChequeId', visible: false },
            { name: 'ChequeNo', displayName: 'Nomor Cheque', field: 'ChequeNo', width:'10%' },

            { name: 'Name', displayName: 'Nama Pelanggan', field: 'Name', enableFiltering: true, width:'10%' },
            { name: 'DueDate', displayName: 'Tanggal Jatuh Tempo Pencairan', cellFilter: 'date:"yyyy-MM-dd"', field: 'DueDate', width:'10%' },
            { name: 'BankName', displayName: 'Nama Bank', field: 'BankName', width:'10%' },
            { name: 'Amount', displayName: 'Nominal', field: 'Amount', cellFilter: 'currency:"":2', width:'10%' },
            { name: 'StatusText', displayName: 'Status', field: 'StatusText', width:'10%' },
            { name: 'ReferenceNo', displayName: 'Nomor Referensi', field: 'ReferenceNo', width:'10%' },


            { name: 'CustomerId', field: 'CustomerId', visible: false },
            { name: 'ProspectId', field: 'ProspectId', visible: false },
            { name: 'BankId', field: 'BankId', visible: false },
            { name: 'RegisterDate', field: 'RegisterDate', visible: false },
            { name: 'Description', field: 'Description', visible: false },
            { name: 'Status', displayName: 'Status', field: 'Status', visible: false },
            {
                name: 'Action', enableFiltering: false,
                //cellTemplate:'<a ng-click="grid.appScope.LihatRegisterCheque_Edit(row)">Edit</a>&nbsp;&nbsp;&nbsp;<a ng-click="grid.appScope.RegisterCheque_Hapus(row)">Hapus</a>'}
                cellTemplate: '<a ng-click="grid.appScope.LihatRegisterCheque_Edit(row)">Update Status</a>'
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid1Api = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
            // 	$scope.MainGrid_Paging(pageNumber);
            // });
        }
    };

    // $scope.MainGrid_Paging = function(pageNumber){
    // 	  BeaMateraiElektronikFactory.getDataByDate(
    //             pageNumber, $scope.uiGridPageSize, 
    //             $filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAwal), 'yyyy-MM-dd'),
    //             $filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAkhir), 'yyyy-MM-dd'))
    // 	  .then(
    // 		function(res)
    // 		{
    // 			console.log("res =>", res.data.Result);
    // 			$scope.grid.data = res.data.Result;
    // 			$scope.grid.totalItems = res.data.Total;
    // 			$scope.grid.paginationPageSize = $scope.uiGridPageSize;
    // 			$scope.grid.paginationPageSizes = [$scope.uiGridPageSize];
    // 		}

    // 	);
    // } 
    $scope.Show_MainRegisterCheque = true;
    $scope.Show_TambahRegisterCheque = false;
    $scope.StartDate = new Date();
    $scope.EndDate = new Date();
    $scope.getDataByDate();
    $scope.RegisterCheque_Tambah_Button_EnabledDisabled = false;
    $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = false;

    $scope.RegisterCheque_Data =
        {
            ChequeId: 0,
            ProspectId: 0,
            CustomerId: 0,
            PelangganCode: "",
            ChequeNo: "",
            DueDate: new Date(),
            BankId: 0,
            Amount: 0,
            Description: "",
            Status: 4003,
            StatusCode: 0,
            Name: "",
            Address: "",
            Phone: "",
            City: "",
        };
    // $scope.ChequeId = 0;
    // $scope.ProspectId = 0;
    // $scope.CustomerId = 0; 
    // $scope.ChequeNo = "123";
    // $scope.DueDate = new Date();
    // $scope.BankId = 0;
    // $scope.Amount = 0;
    // $scope.Description = "desc";
    // $scope.Status = 0;
    // $scope.StatusCode = 0;

    // TAMBAH DATA BEGIN
    $scope.RegisterCheque_ShowTambah_Clicked = function () {
        $scope.SimpanCounter = 0;
        $scope.Show_MainRegisterCheque = false;
        $scope.Show_TambahRegisterCheque = true;
        $scope.Show_RegisterCheque_Tambah_Button = true;
        $scope.Show_RegisterCheque_Ubah_Button = false;
        $scope.RegisterCheque_Tambah_Button_EnabledDisabled = true;
        $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = false;
        $scope.RegisterCheque_Data =
            {
                ChequeId: 0,
                ProspectId: 0,
                CustomerId: 0,
                PelangganCode: "",
                ChequeNo: "",
                DueDate: new Date(),
                BankId: 0,
                Amount: 0,
                Description: "",
                Status: 4003,
                StatusCode: 1,
                Name: "",
                Address: "",
                Phone: "",
                City: "",
                RegisterDate: new Date()
            };
    }
    $scope.RegisterCheque_Tambah_Clicked = function () {
        console.log("button", $scope.RegisterCheque_Tambah_Button_EnabledDisabled);
        $scope.SimpanCounter = $scope.SimpanCounter + 1;
        console.log("Counter", $scope.SimpanCounter);
        if ($scope.SimpanCounter == 1) {
            if ($scope.RegisterCheque_Data.CustomerId == 0 &&
                $scope.RegisterCheque_Data.ProspectId == 0
            ) {
                // alert("Harap memilih Data Customer/Prospect untuk melanjutkan");
                bsNotify.show(
                    {
                        title: "Warning",
                        content: "Harap memilih Data Customer/Prospect untuk melanjutkan",
                        type: 'warning'
                    }
                );
            }
            else {
                if ($scope.RegisterCheque_Data.ChequeNo == "" ||
                    $scope.RegisterCheque_Data.BankId == 0
                ) {
                    // alert("Harap lengkapi semua data");
                    bsNotify.show(
                        {
                            title: "Warning",
                            content: "Harap lengkapi semua data",
                            type: 'warning'
                        }
                    );

                }
                else {
                    var amount = $scope.RegisterCheque_Data.Amount;
                    $scope.RegisterCheque_Tambah_Button_EnabledDisabled = false;
                    console.log("button2", $scope.RegisterCheque_Tambah_Button_EnabledDisabled);
                    console.log("1", amount);
                    console.log("1", angular.isNumber(amount));
                    if (!angular.isNumber(amount) && (amount != undefined)) {
                        console.log("2", amount);
                        amount = amount.replace(/,/g, "");
                        console.log("2", amount);
                    }
                    console.log("3", amount);
                    $scope.RegisterCheque_Data.Amount = amount;
                    RegisterChequeFactory.create($scope.RegisterCheque_Data)
                        .then(
                            function (res) {
                                // alert("Data berhasil disimpan");
                                bsNotify.show(
                                    {
                                        title: "Berhasil",
                                        content: "Berhasil menambah data Register Cheque",
                                        type: 'success'
                                    }
                                );
                                $scope.Show_TambahRegisterCheque = false;
                                $scope.Show_MainRegisterCheque = true;

                                $scope.getDataByDate();
                                $scope.RegisterCheque_Data =
                                    {
                                        ChequeId: 0,
                                        ProspectId: 0,
                                        CustomerId: 0,
                                        PelangganCode: "",
                                        ChequeNo: "",
                                        DueDate: new Date(),
                                        BankId: 0,
                                        Amount: 0,
                                        Description: "",
                                        Status: 4003,
                                        StatusCode: 1,
                                        Name: "",
                                        Address: "",
                                        Phone: "",
                                        City: "",
                                        RegisterDate: new Date()
                                    };
                                $scope.RegisterCheque_Tambah_Button_EnabledDisabled = true;
                            }
                        );
                    $scope.RegisterCheque_Tambah_Button_EnabledDisabled = true;
                }
            }
        }
    }

    $scope.RegisterCheque_BatalTambah_Clicked = function () {
        $scope.Show_MainRegisterCheque = true;
        $scope.Show_TambahRegisterCheque = false;
        $scope.RegisterCheque_Tambah_Button_EnabledDisabled = false;
        $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = false;
    }


    //TAMBAH DATA END

    ///HAPUS DATA BEGIN
    $scope.RegisterCheque_Hapus = function (row) {
        $scope.ChequeId = row.entity.ChequeId;
        $scope.ChequeNo = row.entity.ChequeNo;
        angular.element('#ModalHapusRegisterCheque').modal('show');
    }

    $scope.Batal_ModalHapusRegisterCheque = function () {
        $scope.ChequeId = 0;
        angular.element('#ModalHapusRegisterCheque').modal('hide');
    }
    $scope.Hapus_ModalHapusRegisterCheque = function () {

        RegisterChequeFactory.delete($scope.ChequeId)
            .then(
                function (res) {
                    // alert("Data berhasil dihapus");
                    bsNotify.show(
                        {
                            title: "Berhasil",
                            content: "Berhasil hapus data Register Cheque",
                            type: 'success'
                        }
                    );
                    $scope.getDataByDate();
                    $scope.Batal_ModalHapusRegisterCheque();
                }
            );
    }
    ///HAPUS DATA END
    ///EDIT DATA BEGIN
    $scope.LihatRegisterCheque_Edit = function (row) {
        $scope.RegisterCheque_Data = row.entity;

        $scope.Show_MainRegisterCheque = false;
        $scope.Show_TambahRegisterCheque = true;
        $scope.Show_RegisterCheque_Tambah_Button = false;
        $scope.Show_RegisterCheque_Ubah_Button = true;
        $scope.RegisterCheque_Tambah_Button_EnabledDisabled = false;
        $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = true;

        console.log("CurrentDataToEdit", $scope.RegisterCheque_Data);
    }

    $scope.RegisterCheque_Ubah_Clicked = function () {

        $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = false;
        RegisterChequeFactory.update($scope.RegisterCheque_Data)
            .then(
                function (res) {
                    // alert("Data berhasil disimpan");
                    bsNotify.show(
                        {
                            title: "Berhasil",
                            content: "Berhasil mengubah data Register Cheque",
                            type: 'success'
                        }
                    );
                    $scope.Show_TambahRegisterCheque = false;
                    $scope.Show_MainRegisterCheque = true;
                    $scope.getDataByDate();
                    $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = true;
                }
            );
        $scope.RegisterCheque_Ubah_Clicked_EnabledDisabled = true;
    }
    ///EDIT DATA END

    ///SEARCH PROSPECT 
    $scope.ShowModalSearchProspect = function () {
        RegisterChequeFactory.getDataPandC($scope.RegisterCheque_Data.Name)
            .then(
                function (res) {
                    debugger;
                    $scope.prospectGrid.data = res.data.Result;
                    if (res.data.Result.length > 0)
                        console.log("sample", res.data.Result[0]);
                }
            );

        angular.element('#ModalSearchProspectID').modal('setting', { closeable: false }).modal('show');
    }
    $scope.ModalSearchProspect_Batal = function () {
        angular.element('#ModalSearchProspectID').modal('hide');
    }
    $scope.SearchProspectPilihRow = function (row) {
        //RegisterCheque_Data.Name
        console.log("ProspectCustomer Selected", row.entity)

        $scope.RegisterCheque_Data.CustomerId = row.entity.CustomerId;
        $scope.RegisterCheque_Data.ProspectId = row.entity.ProspectId;
        $scope.RegisterCheque_Data.PelangganCode = row.entity.PelangganCode;
        $scope.RegisterCheque_Data.City = row.entity.City;
        $scope.RegisterCheque_Data.Address = row.entity.Address;
        $scope.RegisterCheque_Data.Phone = row.entity.Phone;
        $scope.RegisterCheque_Data.Name = row.entity.Name;
        console.log("CurrentDataToEditWithCust", $scope.RegisterCheque_Data);

        angular.element('#ModalSearchProspectID').modal('hide');
    }
    $scope.prospectGrid = {
        enableSorting: true,
        enableRowSelection: false,
        multiSelect: false,
        enableSelectAll: false,
        enableFiltering: true,
        paginationPageSizes: [10, 25, 50],
        columnDefs: [
            { name: 'CustomerId', field: 'CustomerId', visible: false },
            { name: 'ProspectId', field: 'ProspectId', visible: false },
            { name: 'Name', displayName: 'Nama Pelanggan', field: 'Name', enableFiltering: true },
            { name: 'Address', displayName: 'Alamat', field: 'Address', enableFiltering: true },
            { name: 'City', displayName: 'Kabupaten/Kota', field: 'City', enableFiltering: true },
            { name: 'Phone', displayName: 'Phone', field: 'Phone', enableFiltering: true },
            {
                name: 'Action', enableFiltering: false,
                cellTemplate: '<a ng-click="grid.appScope.SearchProspectPilihRow(row)">Pilih</a>'
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        }
    };

    $scope.MainGridFilterOptions = [{ name: "Nomor Cheque", value: "ChequeNo" },
    { name: "Nama Pelanggan", value: "Name" },
    { name: "Tanggal Jatuh Tempo", value: "DueDate" },
    { name: "Nama Bank", value: "BankName" },
    { name: "Status", value: "StatusText" }];

    $scope.MainGridFilterClick = function () {
        $scope.grid1Api.grid.clearAllFilters();
        $scope.grid1Api.grid.refresh();

        console.log("nomor dan filter", $scope.CurrentOption);
        $scope.grid1Api.grid.getColumn($scope.CurrentOption).filters[0].term = $scope.MainGrid_TextFilter;
        $scope.grid1Api.grid.refresh();
    }





    ///NOT CONVERTED YET 

    /*
    
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mChequeList = null; //Model 
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        console.log("Test");
        RegisterChequeFactory.getDataCL(1,100,'').then(
            function(res){
                gridData = [];
                console.log("getdata=>",res.data.Result);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    } 

    function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function(rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function(rows) {
			console.log("onSelectRows=>", rows);
		}
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		//showTreeExpandNoChildren: true,
		// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
		// paginationPageSize: 15, 
		columnDefs: [ 
            { name:'ChequeId', field:'ChequeId', visible:false } ,
            { name:'ChequeNo', displayName:'Nomor Cheque', field:'ChequeNo',  } ,
            
            { name:'CustomerId', field:'CustomerId', visible:false},
            { name:'ProspectId', field:'ProspectId', visible:false},
            { name:'Name', displayName:'Nama Pelanggan', field:'Name' }, 
            { name:'DueDate', displayName:'Tanggal Jatuh Tempo Pencairan', field:'DueDate'}, 
            { name:'BankName', displayName:'Nama Bank', field:'BankName'}, 
            { name:'Amount', displayName:'Nominal', field:'Amount' },
            { name:'StatusText', displayName:'Status', field:'StatusText' },
            { name:'ReferenceNo', displayName:'Nomor Referensi', field:'ReferenceNo'},  
            
            
            { name:'BankId', field:'BankId', visible:false},
            { name:'RegisterDate', field:'RegisterDate', visible:false}, 
            { name:'Description', field:'Description', visible:false}, 
            { name:'Status', displayName:'Status', field:'Status', visible:false}, 
		],
    }; */
});


