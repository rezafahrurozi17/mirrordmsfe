angular.module('app')
	.factory('PEDealerToBranchFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getPricingType=function() {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PricingEngine/PricingType?outletId=0';
      var res=$http.get(url);  
      return res;			
    };

    factory.getDataVariable=function(component, type, outlet) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PricingEngine/ComponentVariable?outletId=' + outlet + '&typeId=' + type + '&componentType=' + component;
      var res=$http.get(url);  
      return res;			
    };

    factory.getDataFormulaList=function(type, outlet) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PricingEngine/FormulaList?outletId=' + outlet + '&typeId=' + type;
      var res=$http.get(url);  
      return res;			
    };

    factory.getDataDisplayList=function(type, outlet, view) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PricingEngine/DisplayList?outletId=' + outlet + '&typeId=' + type + '&view=' + view;
      var res=$http.get(url);  
      return res;			
    };

        factory.Submit=function(data)
        {
                var url = '/api/fe/PricingEngine/SubmitData/';
                if (debugMode){console.log('Masuk ke reverse')};
                if (debugMode){console.log('url :'+url);};
                if (debugMode){console.log('Parameter POST :'+ JSON.stringify(data)) };
                var res=$http.post(url, JSON.stringify(data));

                return res;
        };

        return factory;
});