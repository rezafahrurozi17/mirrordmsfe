angular.module('app')
.controller('PEDealerToBranchController', function ($scope, $http, $filter, CurrentUser, PEDealerToBranchFactory, $timeout) {
	$scope.user = CurrentUser.user();
	$scope.MainPEDealerToBranch_Show = true;
	$scope.index = 0;
	$scope.seq = 0;

// Create Base64 Object
// private property
var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

// public method for encoding
function encode (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;
    input = _utf8_encode(input);
    while (i < input.length) {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);
        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;
        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }
        output = output +
        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
    }
    return output;
};

// public method for decoding
function decode (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = this._keyStr.indexOf(input.charAt(i++));
        enc2 = this._keyStr.indexOf(input.charAt(i++));
        enc3 = this._keyStr.indexOf(input.charAt(i++));
        enc4 = this._keyStr.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }
    }
    output = _utf8_decode(output);
    return output;
};

// private method for UTF-8 encoding
function _utf8_encode (str) {
	var rgx = /\r\n/g;
    str = str.replace(rgx,"\n");
    var utftext = "";
    for (var n = 0; n < str.length; n++) {
        var c = str.charCodeAt(n);
        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }
    }
    return utftext;
};

// private method for UTF-8 decoding
function _utf8_decode (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;
    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    return string;
};

	angular.element('#Modal_PEDealerToBranch').modal('hide');	
	$scope.InitializeData=function() {
		PEDealerToBranchFactory.getPricingType().then(
			function(res) {
				$scope.optionsMainPEDealerToBranchPricingType = res.data.Result;	
				$scope.seq = 0;
				//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};

	$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Sequence", displayName:"Sequence", field:"Sequence", enableCellEdit: false, enableHiding: false, visible:true},
				{name:"DisplayId", displayName:"DisplayId", field:"DisplayId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"DisplayOne", displayName:"", field:"DisplayOne", enableCellEdit: false, enableHiding: false},
				{name:"DisplayTwo", displayName:"", field:"DisplayTwo", enableCellEdit: false, enableHiding: false},
				{name:"Update", displayName:"",field:"Update", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_UpdateDisplayMobile(row)">Update</a>' 
				},
				{name:"Hapus", displayName:"",field:"Delete", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_DeleteDisplayMobile(row)">Delete</a>' 
				}				
			],
		onRegisterApi: function(gridApi) {
			$scope.PEDealerToBranch_DaftarDisplayMobile_gridAPI = gridApi;
		}
	};	

	$scope.PEDealerToBranch_DaftarFormula_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"SequenceProcessing", displayName:"Sequence", field:"SequenceProcessing", enableCellEdit: false, enableHiding: false, visible:true},
				{name:"ComponentId", displayName:"ComponentId", field:"ComponentId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"ComponentCode", displayName:"Price Komponen", field:"ComponentCode", enableCellEdit: false, enableHiding: false},
				{name:"Formula", displayName:"Formula", field:"Formula", enableCellEdit: false, enableHiding: false},
				{name:"Update", displayName:"",field:"Update", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_UpdateFormula(row)">Update</a>' 
				},
				{name:"Hapus", displayName:"",field:"Delete", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_DeleteFormula(row)">Delete</a>' 
				}				
			],
		onRegisterApi: function(gridApi) {
			$scope.PEDealerToBranch_DaftarFormula_gridAPI = gridApi;
		}
	};	


	$scope.PEDealerToBranch_DaftarVariable_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Nomor", displayName:"No", field:"Nomor", enableCellEdit: false, enableHiding: false, visible:true},
				{name:"TypeComponentId", displayName:"TypeComponentId", field:"TypeComponentId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"ComponentCode", displayName:"Data Variable", field:"ComponentCode", enableCellEdit: false, enableHiding: false}
			],
		onRegisterApi: function(gridApi) {
			$scope.PEDealerToBranch_DaftarVariable_gridAPI = gridApi;
		}
	};
	
	$scope.PEDealerToBranch_DaftarInput_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Nomor", displayName:"No", field:"Nomor", enableCellEdit: false, enableHiding: false, visible:true},
				{name:"TypeComponentId", displayName:"TypeComponentId", field:"TypeComponentId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"ComponentCode", displayName:"Data Variable", field:"ComponentCode", enableCellEdit: false, enableHiding: false}
			],
		onRegisterApi: function(gridApi) {
			$scope.PEDealerToBranch_DaftarInput_gridAPI = gridApi;
		}
	};

	$scope.MainPEDealerToBranchPricingType_Changed=function()
	{

		//console.log(' $scope.MainPEDealerToBranchPricingType  == ' ,  $scope.MainPEDealerToBranchPricingType );

		if ( (isNaN($scope.MainPEDealerToBranchPricingType) != true) &&  
			 (angular.isUndefined($scope.MainPEDealerToBranchPricingType) != true) &&
			  ($scope.MainPEDealerToBranchPricingType != null)
			 )	{
			PEDealerToBranchFactory.getDataFormulaList($scope.MainPEDealerToBranchPricingType,$scope.user.OrgId ).then(
				function(res) {
					$scope.PEDealerToBranch_DaftarFormula_UIGrid.data = res.data.Result;	
					//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);

			PEDealerToBranchFactory.getDataDisplayList($scope.MainPEDealerToBranchPricingType, $scope.user.OrgId , 'mobile' ).then(
				function(res) {
					$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data = res.data.Result;	
					//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);	

			PEDealerToBranchFactory.getDataDisplayList($scope.MainPEDealerToBranchPricingType,$scope.user.OrgId , 'desktop' ).then(
				function(res) {
					$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data = res.data.Result;	
					//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);			

			PEDealerToBranchFactory.getDataVariable(0, $scope.MainPEDealerToBranchPricingType, $scope.user.OrgId ).then(
				function(res) {
					$scope.PEDealerToBranch_DaftarVariable_UIGrid.data = res.data.Result;	
					//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);	
			PEDealerToBranchFactory.getDataVariable(1, $scope.MainPEDealerToBranchPricingType, $scope.user.OrgId ).then(
				function(res) {
					$scope.PEDealerToBranch_DaftarInput_UIGrid.data = res.data.Result;	
					//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
				},
				function (err) {
					console.log('error -->', err)
				}
			);			
		}
	}

	$scope.InitializeData();

	$scope.PEDealerToBranch_DisplayMobile_Clicked=function()
	{
		$scope.PEDealerToBranch_DaftarDisplayMobile_Show = true;
		$scope.PEDealerToBranch_DaftarDisplayDesktop_Show = false;
	}

	$scope.PEDealerToBranch_DisplayDesktop_Clicked=function()
	{
		$scope.PEDealerToBranch_DaftarDisplayMobile_Show = false;
		$scope.PEDealerToBranch_DaftarDisplayDesktop_Show = true;		
	}

	$scope.PEDealerToBranch_DisplayDesktop_Clicked();

	$scope.PEDealerToBranch_Simpan_Clicked = function()
	{
		var dataSimpan = {};
		dataSimpan = [{PricingType: $scope.MainPEDealerToBranchPricingType,
			OutletId : $scope.user.OrgId,
			FormulaGrid: window.btoa(JSON.stringify($scope.PEDealerToBranch_DaftarFormula_UIGrid.data)),
			DisplayDesktopGrid : window.btoa(JSON.stringify($scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data)),
			DisplayMobileGrid : window.btoa(JSON.stringify($scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data))
		}];

		// Encode the String
		var encodedString = window.btoa(dataSimpan);
		console.log(dataSimpan); // 

		PEDealerToBranchFactory.Submit(dataSimpan).then(
			function(res){
				//$scope.loading=false;
				$scope.MainPEDealerToBranchPricingType_Changed();
				console.log(res);                
			},
			function(err){
				console.log("err=>",err);
			}			
		);
	}

	$scope.PEDealerToBranch_Batal_Clicked=function()
	{
		$scope.MainPEDealerToBranchPricingType = "";	
		$scope.MainPEDealerToBranchPricingType_Changed();
	}
	
	$scope.initDataModalDisplay=function()
	{
		PEDealerToBranchFactory.getDataVariable(2,$scope.MainPEDealerToBranchPricingType, $scope.user.OrgId ).then(
			function(res) {
				$scope.optionsMainPEDealerToBranch_TambahDisplay_Variable1 = res.data.Result;	
				$scope.optionsMainPEDealerToBranch_TambahDisplay_Variable2 = res.data.Result;	
				$scope.optionsMainPEDealerToBranch_TambahDisplay_Variable3 = res.data.Result;	
				//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);	
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Clicked1=function()
	{
		if ($scope.MainPEDealerToBranch_TambahDisplay_Variable1 != '') {
			$scope.Modal_PEDealerToBranch_NamaDisplayVariable1 = $scope.MainPEDealerToBranch_TambahDisplay_Variable1;
		}
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Clicked2=function() {
		if ($scope.MainPEDealerToBranch_TambahDisplay_Variable2 != '') {
			$scope.Modal_PEDealerToBranch_NamaDisplayVariable2 = $scope.MainPEDealerToBranch_TambahDisplay_Variable2;
		}		
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Clicked3=function() {
		if ($scope.MainPEDealerToBranch_TambahDisplay_Variable3 != '') {
			$scope.Modal_PEDealerToBranch_NamaDisplayVariable3 = $scope.MainPEDealerToBranch_TambahDisplay_Variable3;
		}		
	}

	$scope.DealerToBranch_UpdateDisplayMobile = function(row){
		$scope.Modal_PEDealerToBranch_TambahDisplay_Button = "Simpan";
		$scope.Modal_PEDealerToBranch_TambahDisplay_Header = "Update Display Mobile";
		$scope.ShowDisplayKolomThree = false;
		$scope.initDataModalDisplay();
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable1 = row.entity.DisplayOne;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable2 = row.entity.DisplayTwo;
		$scope.index = $scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data.indexOf(row.entity);
		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = true;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');	
	}

	$scope.DealerToBranch_DeleteDisplayMobile=function(row){
		$scope.ShowDisplayKolomThreeHapus = false;
		$scope.index = $scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data.indexOf(row.entity);
		$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus1 = row.entity.DisplayOne;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus2 = row.entity.DisplayTwo;
		//$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus3 = row.entity.DisplayThree;
		$scope.Modal_PEDealerToBranch_DeleteDisplay = true;
		$scope.Modal_PEDealerToBranch_TambahDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');					
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Tambah_Clicked=function()
	{
		if ($scope.Modal_PEDealerToBranch_TambahDisplay_Button == "Tambah")
		{
			if ($scope.ShowDisplayKolomThree == false) {
				$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data.push({
					DisplayOne : $scope.Modal_PEDealerToBranch_NamaDisplayVariable1 ,
					DisplayTwo : $scope.Modal_PEDealerToBranch_NamaDisplayVariable2 })
			}
			else {
				$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data.push({
					DisplayOne : $scope.Modal_PEDealerToBranch_NamaDisplayVariable1 ,
					DisplayTwo : $scope.Modal_PEDealerToBranch_NamaDisplayVariable2,
					DisplayThree : $scope.Modal_PEDealerToBranch_NamaDisplayVariable3
				})			
			}
		}
		else {
			if ($scope.ShowDisplayKolomThree == false) {
				$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data[$scope.index].DisplayOne = 
					$scope.Modal_PEDealerToBranch_NamaDisplayVariable1;
				$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data[$scope.index].DisplayTwo = 
					$scope.Modal_PEDealerToBranch_NamaDisplayVariable2;
					
			}
			else {	
				$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data[$scope.index].DisplayOne = 
					$scope.Modal_PEDealerToBranch_NamaDisplayVariable1;
				$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data[$scope.index].DisplayTwo = 
					$scope.Modal_PEDealerToBranch_NamaDisplayVariable2;				
				$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data[$scope.index].DisplayThree = 
					$scope.Modal_PEDealerToBranch_NamaDisplayVariable3;							
			}
		}
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');	
	}

	$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid = {
		paginationPageSizes: [25,50,100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,	
		columnDefs: [
				{name:"Sequence", displayName:"Sequence", field:"Sequence", enableCellEdit: false, enableHiding: false, visible:true},
				{name:"DisplayId", displayName:"DisplayId", field:"DisplayId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"DisplayOne", displayName:"", field:"DisplayOne", enableCellEdit: false, enableHiding: false},
				{name:"DisplayTwo", displayName:"", field:"DisplayTwo", enableCellEdit: false, enableHiding: false},
				{name:"DisplayThree", displayName:"", field:"DisplayThree", enableCellEdit: false, enableHiding: false},
				{name:"Update", displayName:"",field:"Update", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_UpdateDisplayDesktop(row)">Update</a>' 
				},
				{name:"Hapus", displayName:"",field:"Delete", enableCellEdit:false, 
					cellTemplate: '<a ng-click="grid.appScope.DealerToBranch_DeleteDisplayDesktop(row)">Delete</a>' 
				}				
			],
		onRegisterApi: function(gridApi) {
			$scope.PEDealerToBranch_DaftarDisplayDesktop_gridAPI = gridApi;
		}
	};	

	$scope.PEDealerToBranch_TambahDisplay_Clicked=function()
	{
		$scope.Modal_PEDealerToBranch_TambahDisplay_Button = "Tambah";
		if ($scope.PEDealerToBranch_DaftarDisplayDesktop_Show == true) {
			$scope.Modal_PEDealerToBranch_TambahDisplay_Header = "Tambah Display Desktop";
		}
		else {
		$scope.Modal_PEDealerToBranch_TambahDisplay_Header = "Tambah Display Mobile";
		}
		$scope.ShowDisplayKolomThree = $scope.PEDealerToBranch_DaftarDisplayDesktop_Show;

		$scope.Modal_PEDealerToBranch_NamaDisplayVariable1 = "";
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable2 = "";
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable3 = "";
		$scope.initDataModalDisplay();
		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = true;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');			
	}

	$scope.DealerToBranch_UpdateDisplayDesktop = function(row){
		$scope.Modal_PEDealerToBranch_TambahDisplay_Button = "Simpan";
		$scope.Modal_PEDealerToBranch_TambahDisplay_Header = "Update Display Desktop";
		$scope.ShowDisplayKolomThree = true;
		$scope.initDataModalDisplay();
		$scope.index = $scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data.indexOf(row.entity);
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable1 = row.entity.DisplayOne;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable2 = row.entity.DisplayTwo;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariable3 = row.entity.DisplayThree;
		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = true;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');		
	}

	$scope.DealerToBranch_DeleteDisplayDesktop=function(row){
		$scope.ShowDisplayKolomThreeHapus = true;
		$scope.index = $scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data.indexOf(row.entity);
		$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus1 = row.entity.DisplayOne;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus2 = row.entity.DisplayTwo;
		$scope.Modal_PEDealerToBranch_NamaDisplayVariableHapus3 = row.entity.DisplayThree;
		
		$scope.Modal_PEDealerToBranch_DeleteDisplay = true;
		$scope.Modal_PEDealerToBranch_TambahDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');		
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Batal_Clicked=function()
	{
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');	
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_HapusBatal_Clicked = function()
	{
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');	
	}

	$scope.TambahDisplay_Modal_PEDealerToBranch_Hapus_Clicked = function()
	{
		if ($scope.ShowDisplayKolomThreeHapus == false) {
			$scope.PEDealerToBranch_DaftarDisplayMobile_UIGrid.data.splice($scope.index, 1);
		}
		else {
			$scope.PEDealerToBranch_DaftarDisplayDesktop_UIGrid.data.splice($scope.index, 1)
		}
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');			
	}

	$scope.DealerToBranch_DeleteFormula=function(row) {
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen_Disabled = true;
		$scope.index = $scope.PEDealerToBranch_DaftarFormula_UIGrid.data.indexOf(row.entity);
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen_Delete = row.entity.ComponentCode;
		$scope.Modal_PEDealerToBranch_PricingFormula_Delete = row.entity.Formula;

		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahFormula = false;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = true;		
		angular.element('#Modal_PEDealerToBranch').modal('show');			
	}

	$scope.PEDealerToBranch_TambahFormula_Clicked=function()
	{
		PEDealerToBranchFactory.getDataVariable(2, $scope.MainPEDealerToBranchPricingType, $scope.user.OrgId ).then(
			function(res) {
				$scope.optionsMainPEDealerToBranch_TambahFormula_Variable = res.data.Result;	
				//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);	
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen_Disabled = false;
		$scope.Modal_PEDealerToBranch_PricingFormula = "";
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen = "";
		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahFormula = true;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');	
	}

	$scope.DealerToBranch_UpdateFormula=function(row) {
		PEDealerToBranchFactory.getDataVariable(2, $scope.MainPEDealerToBranchPricingType, $scope.user.OrgId ).then(
			function(res) {
				$scope.optionsMainPEDealerToBranch_TambahFormula_Variable = res.data.Result;	
				//$scope.MainPEDealerToBranchPricingType = $scope.user.OrgId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);	
		console.log(' row.entity ', row.entity);
		//$scope.index = row.entity;
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen_Disabled = false;
		$scope.index = $scope.PEDealerToBranch_DaftarFormula_UIGrid.data.indexOf(row.entity);
		//console.log(' isi index ' , $scope.index);
		$scope.Modal_PEDealerToBranch_NamaPricingKomponen = row.entity.ComponentCode;
		$scope.Modal_PEDealerToBranch_PricingFormula = row.entity.Formula;

		$scope.Modal_PEDealerToBranch_DeleteDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahDisplay = false;
		$scope.Modal_PEDealerToBranch_TambahFormula = true;	
		$scope.Modal_PEDealerToBranch_DeleteFormula = false;		
		angular.element('#Modal_PEDealerToBranch').modal('show');	

	};

	$scope.Tambah_Modal_PEDealerToBranch_Clicked=function()
	{
		if ($scope.MainPEDealerToBranch_TambahFormula_Variable != '')
		{
			$scope.Modal_PEDealerToBranch_PricingFormula = $scope.Modal_PEDealerToBranch_PricingFormula + $scope.MainPEDealerToBranch_TambahFormula_Variable;
		}
	}

	$scope.Delete_Modal_PEDealerToBranch_Hapus_Clicked=function()
	{
		$scope.PEDealerToBranch_DaftarFormula_UIGrid.data.splice($scope.index, 1);	
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');		
	}

	$scope.Delete_Modal_PEDealerToBranch_Batal_Clicked=function()
	{
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');	
	}

	$scope.Tambah_Modal_PEDealerToBranch_Batal_Clicked=function()
	{
		$scope.index = -1;
		angular.element('#Modal_PEDealerToBranch').modal('hide');	
	}

	$scope.Tambah_Modal_PEDealerToBranch_Simpan_Clicked = function(){
		if ( ($scope.Modal_PEDealerToBranch_NamaPricingKomponen != '') && (
			$scope.Modal_PEDealerToBranch_PricingFormula != '') )
		{
			if ($scope.index != -1) {
				//$scope.PEDealerToBranch_DaftarFormula_UIGrid.data[$scope.index].ComponentId = $scope.;
				$scope.PEDealerToBranch_DaftarFormula_UIGrid.data[$scope.index].ComponentCode = $scope.Modal_PEDealerToBranch_NamaPricingKomponen;
				$scope.PEDealerToBranch_DaftarFormula_UIGrid.data[$scope.index].Formula = $scope.Modal_PEDealerToBranch_PricingFormula;
			}
			else {
				$scope.PEDealerToBranch_DaftarFormula_UIGrid.data.push({ ComponentId: 0,
				ComponentCode:  $scope.Modal_PEDealerToBranch_NamaPricingKomponen,
				Formula:  $scope.Modal_PEDealerToBranch_PricingFormula});
			}
			$scope.index = -1;
			angular.element('#Modal_PEDealerToBranch').modal('hide');	
		}
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

});