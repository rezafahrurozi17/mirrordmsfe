var app = angular.module('app');
app.controller('MaterialCostController', function ($scope, $http, $filter, CurrentUser, MaterialCostFactory, $timeout, uiGridConstants, bsNotify) {
	var user = CurrentUser.user();
	$scope.MaterialCost_ExcelData = [];
	$scope.file = $scope.uploadMaterialCost;
	$scope.fileEdit = $scope.uploadLihatMaterialCost;
	$scope.MainMaterialCost_Show = true;
	$scope.MaterialCost_FileName_Current = '';
	$scope.MaterialCost_FileId_Current = 0;
	$scope.MaterialCost_EditStatusCurrent = 'Upload';
	$scope.optionsMainMaterialCostFilterKolom = [{ name: "Semua Kolom", value: "" }, { name: "Tipe Material", value: "TipeMaterial" }, { name: "Tanggal Berlaku", value: "TanggalBerlaku" }, { name: "No. Dokumen Upload", value: "NoDokumenUpload" }, { name: "Tanggal Upload", value: "TanggalUpload" }, { name: "Status", value: "Status" }];
	$scope.MatCostDeleteRow = false;
	$scope.MatCostData_DataGrid = [];
	$scope.MatCostDataUnit_DataGrid = [];
	$scope.MainMaterialCostisFilterClicked = false;
	$scope.LihatMaterialCost_DraftDataColumn = [];
	$scope.LihatMaterialCost_DraftDataColumnLihat = [];
	$scope.MinDate = new Date();
	//$scope.MatCostData_UIGrid_lihat.data = [];
	$scope.MainMaterialCost_UpdateMaterialCostBaru_Clicked = function () {
		$scope.MaterialCost_EditStatusCurrent = 'Upload';
		$scope.AddMaterialCost_Simpan_Button_disabled = true;
		$scope.MainMaterialCost_Show = false;
		$scope.AddMaterialCost_Show = true;
		$scope.ShowAddMaterialCost_UnitBahan = false;
		$scope.ShowAddMaterialCost_Unit = false;
		$scope.ShowAddMaterialCost_ValidLabel = false;
		$scope.AddMaterialCost_ClearFields();
	}
	angular.element('#MatCostDataUnitModal').hide('');
	angular.element('#MatCostDataUnitModalNew').hide('');
	angular.element('#MatCostDataUnitDraftModal').hide('');

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log('eaeaea ' + $scope.ByteEnable);
	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};
	$scope.HideButton = checkRbyte($scope.ByteEnable, 2);
	if($scope.HideButton == true){
		$scope.MainMaterialCost_UpdateMaterialCostBaru_Clicked_Show = true;
		$scope.hide_ubah = true;
		
	}else{
		$scope.MainMaterialCost_UpdateMaterialCostBaru_Clicked_Show = false;
		$scope.hide_ubah = false;
	}
	var dateFormat = 'dd/MM/yyyy';
	var uiGridPageNumberMatCost = 1;
	var uiGridPageSizeMatCost = 10;
	$scope.uipage = 10;

	$scope.dateOptions = { format: dateFormat }

	MaterialCostFactory.getMaterialType()
		.then(
			function (res) {
				var materialType = [];
				angular.forEach(res.data, function (value, key) {
					materialType.push({ name: value.name, value: { label: value.name, valueType: value.value } });
				});

				$scope.optionsAddMaterialCostTipeMaterial = materialType;
				$scope.optionsMainMaterialCostTipeMaterial = materialType;
			}
		);

	MaterialCostFactory.getMaterialStatus()
		.then(
			function (res) {
				var materialStatusType = [];
				angular.forEach(res.data, function (value, key) {
					if (value.name != "Uploaded")
						materialStatusType.push({ name: value.name, value: { label: value.name, valueType: value.value } });
				});

				$scope.optionsMainMaterialCostStatus = materialStatusType;
			}
		);
		
	MaterialCostFactory.getDataFile(1, $scope.uipage,
		angular.isUndefined($scope.MainMaterialCost_TipeMaterial) ? "" : $scope.MainMaterialCost_TipeMaterial.valueType,
		angular.isUndefined($scope.MainMaterialCost_Status) ? "" : $scope.MainMaterialCost_Status.valueType,
		angular.isUndefined($scope.MainMaterialCost_NoDokumenUpload) ? "" :$scope.MainMaterialCost_NoDokumenUpload,
		angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalBerlakuAwal), "yyyyMMdd"),
		angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalBerlakuAkhir), "yyyyMMdd"),
		angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalUploadAwal), "yyyyMMdd"),
		angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalUploadAkhir), "yyyyMMdd")
	)
	.then(
		function (res) {

			console.log("res =>", res.data.Result);
			$scope.MatCostFile_UIGrid.data = res.data.Result;
			if($scope.HideButton == true){
				$scope.hide_ubah = false;
			}else{
				$scope.hide_ubah = true;
			}
			$scope.MatCostFile_UIGrid.totalItems = res.data.Total;
			$scope.MatCostFile_UIGrid.paginationPageSize = pageSize;
			$scope.MatCostFile_UIGrid.paginationPageSizes = [10, 100, 500];
			
		}
	);
	//==========================================================================//
	// 						Main Material Cost Begin							//
	//==========================================================================//
	$scope.MainMaterialCost_Filter_Clicked = function () {
		if (!angular.isUndefined($scope.MainMaterialCost_TipeMaterial)
			&& (angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) || angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir)
				|| angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) || angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir))) {
			msg = ("Jika tipe material diisi, tanggal berlaku dan tanggal upload wajib diisi");
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}
		else if (!angular.isUndefined($scope.MainMaterialCost_Status)
			&& (angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) || angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir)
				|| angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) || angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir))) {
			msg = ("Jika status diisi, tanggal berlaku dan tanggal upload wajib diisi");
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}
		else if ((angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) && !angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir))
			|| (!angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) && angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir))) {
			msg = ("Tanggal berlaku awal dan akhir tidak boleh kosong salah satunya");
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}
		else if ((angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) && !angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir))
			|| (!angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) && angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir))) {
			msg = ("Tanggal upload awal dan akhir tidak boleh kosong salah satunya");
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}

		$scope.MatCostFile_UIGrid_Paging(1,$scope.uiGridPageSize);
	}

	$scope.MainMaterialCost_HapusFilter_Clicked = function () {
		$scope.MainMaterialCost_TanggalUploadAwal = undefined;
		$scope.MainMaterialCost_TanggalUploadAkhir = undefined;
		$scope.MainMaterialCost_TanggalBerlakuAwal = undefined;
		$scope.MainMaterialCost_TanggalBerlakuAkhir = undefined;
		$scope.MainMaterialCost_NoDokumenUpload = "";
		$scope.MainMaterialCost_Status = "";
		$scope.MainMaterialCost_TipeMaterial = "";

	}

	$scope.LihatMaterialCost = function (row) {
		$scope.cekubah = 0;
		$scope.LihatMaterialCost_DraftData_Show_Bulk = false;
		if(row.entity.MaterialTypeName != "Finish Unit"){
			$scope.ShowLihatMaterialCost_UnitBahan_Show = true;
			$scope.ShowLihatMaterialCost_Unit_Show = false;
		}else{
			$scope.ShowLihatMaterialCost_UnitBahan_Show = false;
			$scope.ShowLihatMaterialCost_Unit_Show = true;
		}
		$scope.MainMaterialCost_Show = false;
		$scope.LihatMaterialCost_Show = true;
		$scope.LihatMaterialCost_DraftData_Show = false;
		$scope.LihatMaterialCost_DraftData_Show_Lihat = true;
		$scope.LihatMaterialCost_UploadData_Show = false;

		$scope.LihatMaterialCost_EditMode_Show = false;
		$scope.LihatMaterialCost_TanggalBerlaku_IsDisable = true;

		$scope.LihatMaterialCost_DraftDataColumnLihat =
			[
				{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
				{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
				{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
				{ name: "No Material", field: "MaterialNo", enableFiltering: true },
				{ name: "Nama Material", field: "MaterialName", enableCellEdit: false },
				{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
				{
					name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", cellClass: function (grid, row, rowRenderIndex, colRenderIndex) {
						row.entity.DiffAmount = row.entity.StandardCost - row.entity.PrevCost;

					}, enableCellEdit: false
				},
				{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
				{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
			];
		$scope.MatCostDraftDataLihat_UIGrid.columnDefs = $scope.LihatMaterialCost_DraftDataColumnLihat;
		$scope.GridApiMatCostDraftDataLihat.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

		$scope.LihatMaterialCost_TipeMaterial = { name: row.entity.MaterialTypeName, value: { label: row.entity.MaterialTypeName, valueType: row.entity.MaterialTypeId } };
		$scope.LihatMaterialCost_TipeMaterialId = row.entity.MaterialTypeId;
		$scope.LihatMaterialCost_FileId = row.entity.MatCostFileId;
		$scope.LihatMaterialCost_NoDokumen = row.entity.UploadDocNo;
		$scope.LihatMaterialCost_TanggalBerlaku = row.entity.MatureDate.setMinutes(row.entity.MatureDate.getMinutes() + 420);
		$scope.LihatMaterialCost_TanggalUpload = row.entity.UploadDate.setMinutes(row.entity.UploadDate.getMinutes() + 420);
		$scope.LihatMaterialCost_StatusDokumen = row.entity.StatusCode;
		$scope.MatCostDraftData_UIGridLihat_Paging(1, $scope.uiGridPageSize);
		$scope.MaterialCost_EditStatusCurrent = 'Update';
		console.log("column def : ", $scope.MatCostData_UIGrid_lihat.columnDefs);
		$scope.MatCostData_UIGrid_lihat.columnDefs[11].visible = false;

	}

	//==========================================================================//
	// 						Main Material Cost End								//
	//==========================================================================//

	//==========================================================================//
	// 					Main Material Cost Edit Row Begin						//
	//==========================================================================//
	$scope.cekubah = 0;
	$scope.UbahMaterialCost = function (row) {
		$scope.cekubah = 1;
		$scope.MainMaterialCost_Show = false;
		$scope.LihatMaterialCost_Show = true;
		$scope.LihatMaterialCost_DraftData_Show = true;
		$scope.LihatMaterialCost_DraftData_Show_Lihat = false;

		console.log("nuse debug materialtypename : ", row.entity.MaterialTypeName);
		if (row.entity.MaterialTypeName != "Finish Unit") {
			$scope.ShowLihatMaterialCost_UnitBahan_Show = true;
			$scope.ShowLihatMaterialCost_Unit_Show = false;
			console.log("bukan finish unit");
		}
		else {
			$scope.ShowLihatMaterialCost_UnitBahan_Show = false;
			$scope.ShowLihatMaterialCost_Unit_Show = true;
			console.log("finish unit");
		}

		if (row.entity.StatusCode == "Draft") {

			$scope.LihatMaterialCost_UploadData_Show = false;

			$scope.LihatMaterialCost_EditMode_Show = true;
			$scope.LihatMaterialCost_TanggalBerlaku_IsDisable = false;
			$scope.LihatMaterialCost_DraftDataColumn =
				[
					{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
					{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
					{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
					{ name: "No Material", field: "MaterialNo", enableCellEdit: false },
					{ name: "Nama Material", field: "MaterialName", enableCellEdit: false },
					{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
					{
						name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", cellClass: function (grid, row, rowRenderIndex, colRenderIndex) {
							row.entity.DiffAmount = row.entity.StandardCost - row.entity.PrevCost;
							return 'canEdit';
						}
					},
					{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
					{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
					{
						name: "Action", enableCellEdit: false,
						cellTemplate: '<a ng-click="grid.appScope.HapusMaterialCostDraftData(row)">Hapus</a>'
					},
				];
		}
		else {
			msg = ("Status sudah Completed. Data sudah tidak bisa diubah, hanya bisa dilihat.");
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			$scope.LihatMaterialCost_UploadData_Show = false;

			$scope.LihatMaterialCost_EditMode_Show = false;
			$scope.LihatMaterialCost_TanggalBerlaku_IsDisable = true;
			$scope.LihatMaterialCost_DraftDataColumn =
				[
					{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
					{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
					{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
					{ name: "No Material", field: "MaterialNo", enableCellEdit: false },
					{ name: "Nama Material", field: "MaterialName", enableCellEdit: false },
					{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
					{
						name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", cellClass: function (grid, row, rowRenderIndex, colRenderIndex) {
							row.entity.DiffAmount = row.entity.StandardCost - row.entity.PrevCost;
						}
					},
					{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
					{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
				];
		}

		$scope.MatCostDraftData_UIGrid.columnDefs = $scope.LihatMaterialCost_DraftDataColumn;
		$scope.GridApiMatCostData.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

		$scope.LihatMaterialCost_TipeMaterial = { name: row.entity.MaterialTypeName, value: { label: row.entity.MaterialTypeName, valueType: row.entity.MaterialTypeId } };
		$scope.LihatMaterialCost_TipeMaterialId = row.entity.MaterialTypeId;
		$scope.LihatMaterialCost_FileId = row.entity.MatCostFileId;
		$scope.LihatMaterialCost_NoDokumen = row.entity.UploadDocNo;
		$scope.LihatMaterialCost_TanggalBerlaku = row.entity.MatureDate;//row.entity.MatureDate.setMinutes(row.entity.MatureDate.getMinutes() + 420);
		$scope.LihatMaterialCost_TanggalUpload = row.entity.UploadDate.setMinutes(row.entity.UploadDate.getMinutes() + 420);
		$scope.LihatMaterialCost_StatusDokumen = row.entity.StatusCode;
		$scope.GridApiMatCostDraftData.grid.clearAllFilters()
		$scope.MatCostDraftData_UIGrid_Paging(1, $scope.uiGridPageSize);
		$scope.MaterialCost_EditStatusCurrent = 'Update';
	}
	//==========================================================================//
	// 					 Main Material Cost Edit Row End						//
	//==========================================================================//

	//==========================================================================//
	// 							Excel Upload Begin								//
	//==========================================================================//
	$scope.loadXLS = function (ExcelFile) {
		var ExcelData = [];
		if ($scope.MaterialCost_EditStatusCurrent == 'Update') {
			var myEl = angular.element(document.querySelector('#uploadLihatMaterialCost')); //ambil elemen dari dokumen yang di-upload 
		}
		else {
			var myEl = angular.element(document.querySelector('#uploadMaterialCost')); //ambil elemen dari dokumen yang di-upload 
		}

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			ExcelData = json[0];
			console.log("ExcelFile : ", ExcelFile);
			console.log("ExcelData : ", ExcelData);
			console.log("myEl : ", myEl);
			console.log("json : ", json);
			$scope.MaterialCost_ExcelData = json;
			$scope.MaterialCost_FileName_Current = myEl[0].files[0].name;
			// if(!$scope.validateColumn(ExcelData)){
			// alert("Kolom file excel tidak sesuai");
			// return;
			// }
			// else{
			// $scope.createInputDataCheck(json, myEl[0].files[0].name);
			// $scope.MaterialCost_FileName_Current = myEl[0].files[0].name;
			// }
		});
	}
	$scope.MaterialCost_ExcelData = [];
	$scope.MaterialCost_FileName_Current = '';
	$scope.createInputDataCheck = function (inputMaterialList, inputExcelName) {
		var materialType = '';
		var fileId = 0;
		var matureDate = '';
		if ($scope.MaterialCost_EditStatusCurrent == 'Update') {
			fileId = $scope.LihatMaterialCost_FileId;
			materialType = $scope.LihatMaterialCost_TipeMaterialId;
			matureDate = $filter('date')(new Date($scope.LihatMaterialCost_TanggalBerlaku), "yyyy-MM-dd");
		}
		else {
			materialType = $scope.AddMaterialCost_TipeMaterial.valueType;
			matureDate = $filter('date')(new Date($scope.AddMaterialCost_TanggalBerlaku), "yyyy-MM-dd");
		}

		$scope.AddMaterialCost_inputDataCheck = [
			{
				"MatCostFileId": fileId,
				"MatCostFileName": inputExcelName,
				"MaterialTypeId": materialType,
				"MatureDate": matureDate,
				"ListMaterialCostData": inputMaterialList,
				"OutletId": user.OutletId
			}
		];
	}

	$scope.validateColumn = function (inputExcelData) {
		//console.log("inputExcelData : ", inputExcelData);
		//console.log("No_Material : ", inputExcelData.No_Material);
		//console.log("Nama_Material : ", inputExcelData.Nama_Material);
		//console.log("Standard_Cost : ", inputExcelData.Standard_Cost);
		if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
			if (angular.isUndefined(inputExcelData.No_Material) || angular.isUndefined(inputExcelData.Nama_Material) || angular.isUndefined(inputExcelData.Standard_Cost)) {
				return (false);
			}
		}
		else {
			if (angular.isUndefined(inputExcelData.Model_Type) || angular.isUndefined(inputExcelData.Standard_Cost)) {
				return (false);
			}
		}

		return (true);
	}

	//==========================================================================//
	// 							Excel Upload End								//
	//==========================================================================//
	//==========================================================================//
	// 							Excel Download Begin							//
	//==========================================================================//
	$scope.MaterialCost_DownloadExcelTemplate = function () {
		var excelData = [];
		var fileName = "";
		if ($scope.ModalMaterialCost_TipeMaterial == "Parts dan Bahan") {
			excelData = [{ "No_Material": "", "Nama_Material": "", "Standard_Cost": "" }];
			fileName = "Parts_dan_Bahan_Template";
		}
		else {
			excelData = [{ "Model_Type": "", "Standard_Cost": "" }];
			fileName = "Finish_Unit_Template";
		}
		XLSXInterface.writeToXLSX(excelData, fileName);
	}
	//==========================================================================//
	// 							Excel Download End								//
	//==========================================================================//
	//==========================================================================//
	// 								Modal Begin									//
	//==========================================================================//

	// $scope.AddMaterialCost_DateChanged = function() {
	// 	var now = new Date()
	// 	if ($scope.AddMaterialCost_TanggalBerlaku <= now){
	// 		bsNotify.show({
	// 			title: "Warning",
	// 			content: "Tanggal berlaku harus lebih dari hari ini",
	// 			type: 'warning'
	// 		});
	// 	}
	// }

	$scope.MainMaterialCost_DownloadTemplate_Clicked = function () {
		$scope.optionsModalMaterialCostTipeMaterial = [{ name: "Parts dan Bahan", value: "Parts dan Bahan" }, { name: "Finish Unit", value: "Finish Unit" }];
		angular.element('#MatCostDataUnitDraftModal').remove();
		angular.element('#ModalLihatMaterialCostUploadExcel').remove();
		angular.element('#MatCostDataUnitModal').remove();
		angular.element('#ModalMaterialCost').modal('show');
		$('#ModalMaterialCost').modal('show');
	}

	$scope.ModalMaterialCost_Kembali_Clicked = function () {
		$scope.ModalMaterialCost_TipeMaterial = "";
		$('#ModalMaterialCost').modal('hide');
	}

	// OLD
	// $scope.ModalMaterialCost_OK_Clicked = function () {
	// 	$scope.MaterialCost_DownloadExcelTemplate();
	// 	$scope.ModalMaterialCost_Kembali_Clicked();
	// }

	// New irfan
	$scope.getDataTmp = function(){
		if($scope.ModalMaterialCost_TipeMaterial == "Parts dan Bahan"){
			MaterialCostFactory.downloadExcel(3701)
			.then(
				function(res){
					console.log("res data >>", res);
					var fileName = "Parts_dan_Bahan_Template";
					for(var i in res.data.Result){
						delete res.data.Result[i].LastModifiedDate;
						delete res.data.Result[i].LastModifiedUserId;
						// delete res.data.Result[i].Standard_Cost;
						if(res.data.Result[i].Standard_Cost == null){
							res.data.Result[i].Standard_Cost = "";
						}
					}
					XLSXInterface.writeToXLSX(res.data.Result, fileName);
					return res;
				},
				function(err){
					console.log("err=>",err);
					alert(err.data.ExceptionMessage);
				}
			);
		}else if($scope.ModalMaterialCost_TipeMaterial == "Finish Unit"){
			MaterialCostFactory.downloadExcel(3703)
			.then(
				function(res){
					console.log("res data >>", res);
					var fileName = "Finish_Unit_Template";

					for(var i in res.data.Result){
						delete res.data.Result[i].LastModifiedDate;
						delete res.data.Result[i].LastModifiedUserId;
						// delete res.data.Result[i].Standard_Cost;
						if(res.data.Result[i].Standard_Cost == null){
							res.data.Result[i].Standard_Cost = "";
						}
					}
					XLSXInterface.writeToXLSX(res.data.Result, fileName);
					return res;

					return res;
				},
				function(err){
					console.log("err=>",err);
					alert(err.data.ExceptionMessage);
				}
			);
		}
	} 
	$scope.ModalMaterialCost_OK_Clicked = function () {
		$scope.getDataTmp();
		$('#ModalMaterialCost').modal('hide');
		// $scope.MaterialCost_DownloadExcelTemplate();
	}
	// End
	//==========================================================================//
	// 								Modal End									//
	//==========================================================================//

	//==========================================================================//
	// 							Delete Data Begin								//
	//==========================================================================//
	$scope.HapusMaterialCostData = function (row) {
		MaterialCostFactory.deleteData(row.entity.MatCostFileId, row.entity.MatCostLogId, row.entity.MatCostTempId)
			.then(
				function (res) {
					msg = ('Data berhasil dihapus');
					bsNotify.show({
						title: "Berhasil",
						content: msg,
						type: "success"
					});
					$scope.MatCostDeleteRow = true;
					if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit")
						$scope.MatCostData_UIGrid_Paging(1);
					else
						$scope.MatCostDataUnit_UIGrid_Paging(1);

					$scope.AddMaterialCost_totalErrData = $scope.AddMaterialCost_totalErrData - 1;
				}
			);
	}

	//==========================================================================//
	// 							 Delete Data End								//
	//==========================================================================//

	//==========================================================================//
	// 								Add Material Begin							//
	//==========================================================================//
	//$scope.AddMaterialCost_TanggalBerlaku = new Date
	$scope.AddMaterialCost_TipeMaterial_Changed = function () {
		$scope.AddMaterialCost_Filter_NoMaterial = "";
		$scope.AddMaterialCost_Filter_NamaMaterial = "";
		$scope.AddMaterialCost_Filter_ModelTipe = "";
		$scope.GridApiMatCostDataUnit.grid.clearAllFilters();

		if (!angular.isUndefined($scope.AddMaterialCost_TipeMaterial)) {

			//$scope.MatCostData_UIGrid.data = [];

			if ($scope.AddMaterialCost_TipeMaterial.label == "Finish Unit") {
				$scope.ShowAddMaterialCost_UnitBahan = false;
				$scope.ShowAddMaterialCost_Unit = true;
			}
			else {
				$scope.ShowAddMaterialCost_UnitBahan = true;
				$scope.ShowAddMaterialCost_Unit = false;
			}
		}
	}

	$scope.AddMaterialCost_Upload_Clicked = function () {
		var Result = false;
		var now = new Date()
		
		if (angular.isUndefined($scope.AddMaterialCost_TipeMaterial) || $scope.AddMaterialCost_TipeMaterial == "") {
			msg = ("Tipe material wajib dipilih dahulu !")
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}
		console.log("Tanggal Berlaku", $scope.AddMaterialCost_TanggalBerlaku)///aaaaa
		if (angular.isUndefined($scope.AddMaterialCost_TanggalBerlaku)  || $scope.AddMaterialCost_TanggalBerlaku == "" || $scope.AddMaterialCost_TanggalBerlaku == undefined || $scope.AddMaterialCost_TanggalBerlaku == "Invalid Date") {
			msg = ("Tanggal berlaku wajib diisi !")
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}

		if ($scope.AddMaterialCost_TanggalBerlaku <= now){
			bsNotify.show({
				title: "Warning",
				content: "Tanggal berlaku harus lebih dari hari ini",
				type: 'warning'
			});
			return false;
		}

		if ($scope.MaterialCost_EditStatusCurrent == 'Update') {
			$scope.file = $scope.uploadLihatMaterialCost;
		}
		else {
			$scope.file = $scope.uploadMaterialCost;
		}
		XLSXInterface.loadToJson($scope.file, function (json) {
			ExcelData = json[0];
			$scope.MaterialCost_ExcelData = json;
			$scope.MaterialCost_FileName_Current = $scope.file.name;
			// if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
			// 	if ($scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData[0].No_Material == "") {
			// 		msg = ("Tidak ada data pada excel yang di-upload");
			// 		bsNotify.show({
			// 			title: "Warning",
			// 			content: msg,
			// 			type: "warning"
			// 		});
			// 		return;
			// 	}
			// }
			// else {
			// 	if ($scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData[0].Model_Type == "") {
			// 		msg = ("Tidak ada data pada excel yang di-upload");
			// 		bsNotify.show({
			// 			title: "Warning",
			// 			content: msg,
			// 			type: "warning"
			// 		});
			// 		return;
			// 	}
			// }

			
			$scope.ShowAddMaterialCost_ValidLabel = false;
			$scope.createInputDataCheck($scope.MaterialCost_ExcelData, $scope.MaterialCost_FileName_Current);
	
			console.log("List material input data check 0 : ", $scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData);
			console.log("List material : ", $scope.AddMaterialCost_inputDataCheck);
	
			
	
	
			MaterialCostFactory.checkMatCostData($scope.AddMaterialCost_inputDataCheck)
				.then(
					function (res) {
						$scope.MaterialCost_FileId_Current = res.data.Result[0].MatCostFileId;
						console.log("$scope.MaterialCost_FileId_Current", $scope.MaterialCost_FileId_Current);
						if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
							console.log("Masuk MatCostData");///nnnnnn
							$scope.MatCostData_UIGrid_Paging(1);
						}
						else {
							console.log("Masuk MatCostData untuk unit");
							$scope.MatCostDataUnit_UIGrid_Paging(1);
						}
						// console.log("isi matcostdata : ", $scope.MatCostData_UIGrid.data);
						// $scope.MatCostCurrentFileId = $scope.MatCostData_UIGrid.data[0].MatCostFileId;
	
						// .then(
						// function(res)
						// {
						// console.log("res =>", res.data.Result[0].TotalErrData);
						// $scope.AddMaterialCost_totalErrData = $scope.MatCostData_UIGrid.data[0].TotalErrData;
						// if($scope.AddMaterialCost_totalErrData > 0){
						// var errMessage  = 'Proses upload gagal. Terdapat ' + $scope.AddMaterialCost_totalErrData + ' data yang tidak valid';
						// $scope.ShowAddMaterialCost_ValidLabel = true;
						// alert(errMessage);
						// }
						// }
	
						// );
	
						// alert("berhasil cek");
						// console.log("seudah jalanin gettempdatadetail : ", $scope.MatCostData_UIGrid.data);
					},
					function (err) {
						var msg = '';
						console.log('Err', err);
						console.log('Err', err.data.ExceptionMessage);
						if(err.data.ExceptionMessage == null || err.data.ExceptionMessage == "" ||err.data.ExceptionMessage == undefined ){
							msg = (err.data.Message);
						}else{
							msg = (err.data.ExceptionMessage);
						}
						bsNotify.show({
							title: "Error",
							content: msg,
							type: "danger"
						});
					}
				);

			if (!$scope.validateColumn($scope.MaterialCost_ExcelData[0])) {
				msg = ("Kolom file excel tidak sesuai");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		});

		// MaterialCostFactory.getDataDetail(1, $scope.uiGridPageSize, $scope.LihatMaterialCost_FileId)
		// .then(
		// function(res)
		// {
		// $scope.MatCostDraftData_UIGrid.data = res.data.Result;
		// }
		// );

		// $scope.MatCostData_UIGrid_Paging(1);

		//$scope.MatCostData_UIGrid_Paging(1);
		// console.log("data material cost : ", $scope.AddMaterialCost_inputDataCheck)
		// console.log("seudah jalanin checkMatCostData : ", $scope.MatCostData_UIGrid.data);
		
	}

	$scope.grid_correction = function (gridType) {
		var Result = false;
		var editedData = [];
		if (gridType == "partsbahan") {
			angular.forEach($scope.MatCostData_UIGrid.data, function (row, index) {
				editedData.push({ "No_Material": row.MaterialNo, "Nama_Material": row.MaterialName, "Standard_Cost": row.StandardCost });
			});
		}
		else {
			angular.forEach($scope.MatCostDataUnit_UIGrid.data, function (row, index) {
				editedData.push({ "Model_Type": row.ModelType, "Standard_Cost": row.StandardCost });
			});
		}

		$scope.MaterialCost_ExcelData = editedData;
		$scope.MaterialCost_FileName_Current = "";
		if (angular.isUndefined($scope.AddMaterialCost_TipeMaterial) || $scope.AddMaterialCost_TipeMaterial == "") {
			msg = ("Tipe material wajib dipilih dahulu !")
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}

		if (angular.isUndefined($scope.AddMaterialCost_TanggalBerlaku) || $scope.AddMaterialCost_TanggalBerlaku == "") {
			msg = ("Tanggal berlaku wajib diisi !")
			bsNotify.show({
				title: "Warning",
				content: msg,
				type: "warning"
			});
			return;
		}



		$scope.ShowAddMaterialCost_ValidLabel = false;
		$scope.createInputDataCheck($scope.MaterialCost_ExcelData, $scope.MaterialCost_FileName_Current);

		console.log("List material input data check 0 : ", $scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData);
		console.log("List material : ", $scope.AddMaterialCost_inputDataCheck);

		if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
			if ($scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData[0].No_Material == "") {
				msg = ("Tidak ada data pada excel yang di-upload");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		}
		else {
			if ($scope.AddMaterialCost_inputDataCheck[0].ListMaterialCostData[0].Model_Type == "") {
				msg = ("Tidak ada data pada excel yang di-upload");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		}


		MaterialCostFactory.checkMatCostData($scope.AddMaterialCost_inputDataCheck)
			.then(
				function (res) {
					$scope.MaterialCost_FileId_Current = res.data.Result[0].MatCostFileId;
					console.log("$scope.MaterialCost_FileId_Current", $scope.MaterialCost_FileId_Current);
					if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
						console.log("Masuk MatCostData");
						$scope.MatCostData_UIGrid_Paging(1);
					}
					else {
						console.log("Masuk MatCostData untuk unit");
						$scope.MatCostDataUnit_UIGrid_Paging(1);
					}
				},
				function (err) {
					msg = (err.data.ExceptionMessage);
					bsNotify.show({
						title: "Error",
						content: msg,
						type: "danger"
					});
				}
			);
		
	}

	// OLD FILTER
	// $scope.AddMaterialCost_Filter_Clicked = function () {
	// 	$scope.GridApiMatCostData.grid.clearAllFilters();
	// 	$scope.GridApiMatCostDataUnit.grid.clearAllFilters();

	// 	if (!angular.isUndefined($scope.AddMaterialCost_Filter_NoMaterial)) {
	// 		console.log("isi text : ", $scope.AddMaterialCost_Filter_NoMaterial);

	// 		$scope.GridApiMatCostData.grid.getColumn("No Material").filters[0].term = $scope.AddMaterialCost_Filter_NoMaterial;
	// 		console.log("kolom : ", $scope.GridApiMatCostData.grid.getColumn("No Material").filters);
	// 	}
	// 	console.log("isi nama material :", $scope.AddMaterialCost_Filter_NamaMaterial);
	// 	if (!angular.isUndefined($scope.AddMaterialCost_Filter_NamaMaterial)) {
	// 		console.log("masuk ke if lho");
	// 		$scope.GridApiMatCostData.grid.getColumn("Nama Material").filters[0].term = $scope.AddMaterialCost_Filter_NamaMaterial;
	// 	}

	// 	if (!angular.isUndefined($scope.AddMaterialCost_Filter_ModelTipe))
	// 		$scope.GridApiMatCostDataUnit.grid.getColumn("Model Tipe").filters[0].term = $scope.AddMaterialCost_Filter_ModelTipe;

	// 	$scope.GridApiMatCostDataUnit.core.notifyDataChange(uiGridConstants.dataChange.ALL);
	// 	$scope.GridApiMatCostDataUnit.core.refresh();
	// }

	// NEW FILTER
	$scope.getData = function(start, limit){
		if((($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != null) && 
			(($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != undefined) ){
				MaterialCostFactory.getTempDataDetail(start, limit, $scope.MaterialCost_FileId_Current + '|' + $scope.AddMaterialCost_Filter_NoMaterial + '|' + $scope.AddMaterialCost_Filter_NamaMaterial + '|' + $scope.AddMaterialCost_Filter_ModelTipe )
				.then(
					function(res){
						$scope.MatCostData_UIGrid.data = res.data.Result;
						$scope.MatCostData_UIGrid.totalItems = res.data.Total;
						$scope.MatCostData_UIGrid.paginationPageSize = limit;
						$scope.MatCostData_UIGrid.paginationPageSizes = [10, 100, 500];
					},
					function(err){
						console.log("err=>", err);

					}
				);
		}
		// $scope.GridApiMatCostData.grid.clearAllFilters();
	}

	$scope.getDataUnit = function(start, limit){
		if((($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != null) && 
			(($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != undefined) ){
				MaterialCostFactory.getTempDataDetail(start, limit, $scope.MaterialCost_FileId_Current + '|' + $scope.AddMaterialCost_Filter_NoMaterial + '|' + $scope.AddMaterialCost_Filter_NamaMaterial + '|' + $scope.AddMaterialCost_Filter_ModelTipe )
				.then(
					function(res){
						$scope.MatCostDataUnit_UIGrid.data = res.data.Result;
						$scope.MatCostDataUnit_UIGrid.totalItems = res.data.Total;
						$scope.MatCostDataUnit_UIGrid.paginationPageSize = limit;
						$scope.MatCostDataUnit_UIGrid.paginationPageSizes = [10, 25, 50];
					},
					function(err){
						console.log("err=>", err);

					}
				);
		}
		// $scope.GridApiMatCostDataUnit.grid.clearAllFilters();
	}

	$scope.AddMaterialCost_Filter_Clicked = function () {
		if((($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != null) && 
		(($scope.AddMaterialCost_Filter_NoMaterial || $scope.AddMaterialCost_Filter_NamaMaterial || $scope.AddMaterialCost_Filter_ModelTipe) != undefined) ){
			if($scope.AddMaterialCost_TipeMaterial.label == "Parts" || $scope.AddMaterialCost_TipeMaterial.label == "Bahan"){
				console.log('masuk parts bahan')
				MaterialCostFactory.getTempDataDetail(1, $scope.uiGridPageSize, $scope.MaterialCost_FileId_Current + '|' + $scope.AddMaterialCost_Filter_NoMaterial + '|' + $scope.AddMaterialCost_Filter_NamaMaterial + '|' + $scope.AddMaterialCost_Filter_ModelTipe )
				.then(
					function(res){
							$scope.MatCostData_UIGrid.data = res.data.Result;
							$scope.MatCostData_UIGrid.totalItems = res.data.Total;
							$scope.MatCostData_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.MatCostData_UIGrid.paginationPageSizes = [10, 100, 500];
					},
					function(err){
						console.log("err=>", err);

					}
				);
			} else if ($scope.AddMaterialCost_TipeMaterial.label == "Finish Unit") {
				console.log('finish unit');
				MaterialCostFactory.getTempDataDetail(1, $scope.uiGridPageSize, $scope.MaterialCost_FileId_Current + '|' + $scope.AddMaterialCost_Filter_NoMaterial + '|' + $scope.AddMaterialCost_Filter_NamaMaterial + '|' + $scope.AddMaterialCost_Filter_ModelTipe )
				.then(
					function(res){
							$scope.MatCostDataUnit_UIGrid.data = res.data.Result;
							$scope.MatCostDataUnit_UIGrid.totalItems = res.data.Total;
							$scope.MatCostDataUnit_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.MatCostDataUnit_UIGrid.paginationPageSizes = [10, 100, 500];
					},
					function(err){
						console.log("err=>", err);

					}
				);
			}
		}
		
		$scope.GridApiMatCostData.grid.clearAllFilters();
		$scope.GridApiMatCostDataUnit.grid.clearAllFilters();
	}
	$scope.AddMaterialCost_HapusFilter_Clicked = function () {
		$scope.AddMaterialCost_Filter_NoMaterial = "";
		$scope.AddMaterialCost_Filter_NamaMaterial = "";
		$scope.AddMaterialCost_Filter_ModelTipe = "";

		$scope.GridApiMatCostData.grid.clearAllFilters();
		$scope.GridApiMatCostDataUnit.grid.clearAllFilters();
	}
	$scope.AddMaterialCost1_ClearFields = function () {
		angular.element(document.querySelector('#uploadMaterialCost')).val(null);		
		$scope.AddMaterialCost_TipeMaterial = undefined;
		//$scope.AddMaterialCost_TanggalBerlaku = '';
		
	}
	//==========================================================================//
	// 								Add Material End							//
	//==========================================================================//
	//==========================================================================//
	// 						Add Material Save Data Begin						//
	//==========================================================================//
	$scope.AddMaterialCost_Simpan_Clicked = function () {
		//$scope.MatCostData_UIGrid_Paging(1);


		var now = new Date()
		if ($scope.AddMaterialCost_TanggalBerlaku <= now){
			bsNotify.show({
				title: "Warning",
				content: "Tanggal berlaku harus lebih dari hari ini",
				type: 'warning'
			});
			return false;
		}

		var FileId = 0;
		console.log("Tipe material : ", $scope.AddMaterialCost_TipeMaterial.valueType);
		if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit") {
			// if ($scope.MatCostData_DataGrid.length == 0) {
				if ($scope.MatCostData_UIGrid.length == 0) {
				msg = ("Tidak ada data Parts atau Bahan yang akan disimpan.");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
			FileId = $scope.MatCostData_UIGrid.data[0].MatCostFileId;
		}
		else {
			// if ($scope.MatCostDataUnit_DataGrid.length == 0) {
				if ($scope.MatCostDataUnit_UIGrid.length == 0) {////vvvvv
				msg = ("Tidak ada data Unit yang akan disimpan.");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
			FileId = $scope.MatCostDataUnit_UIGrid.data[0].MatCostFileId;
		}


		var inputData = [
			{
				"MatCostFileId": $scope.MaterialCost_FileId_Current, //FileId,
				"MatCostTypeId": $scope.AddMaterialCost_TipeMaterial.valueType,
				"MatureDate" : $filter('date')(new Date($scope.AddMaterialCost_TanggalBerlaku), "yyyy-MM-dd") // new Date($scope.AddMaterialCost_TanggalBerlaku)
			}
		];

		console.log("simpan input data : ", inputData);

		MaterialCostFactory.submitMatCostData(inputData)
			.then(
				function (res) {
					console.log("res.data.Result", res.data.Result);
					if (res.data.Result.length == 0) {
						msg = ("Data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: msg,
							type: "success"
						});
						$scope.AddMaterialCost_Back();
						$scope.MatCostFile_UIGrid_Paging(1,$scope.uiGridPageSize);
					}
					else {
						msg = (res.data.Result[0].ErrorMessage);
						bsNotify.show({
							title: "Error",
							content: msg,
							type: "danger"
						});
					}

				}
			);

	}

	$scope.AddMaterialCost_Back = function () {
		$scope.MainMaterialCost_Show = true;
		$scope.AddMaterialCost_Show = false;
		$scope.AddMaterialCost_ClearFields();
	}

	$scope.AddMaterialCost_Kembali_Clicked = function () {
		//MaterialCostFactory.clearUncommitedData($scope.MaterialCost_FileName_Current)
		MaterialCostFactory.clearUncommitedData($scope.MaterialCost_FileId_Current)
			.then(
				function (res) {
					$scope.AddMaterialCost_Back();
				}
			);


	}

	$scope.AddMaterialCost_ClearFields = function () {
		angular.element(document.querySelector('#uploadMaterialCost')).val(null);
		$scope.MaterialCost_FileName_Current = "";
		$scope.MaterialCost_FileId_Current = 0;
		$scope.MatCostData_UIGrid.data = [];
		$scope.MatCostDataUnit_UIGrid.data = [];
		$scope.MatCostData_DataUIGrid = [];
		$scope.MatCostDataUnit_DataUIGrid = [];
		$scope.AddMaterialCost_TipeMaterial = undefined;
		$scope.AddMaterialCost_TanggalBerlaku = '';
		$scope.ShowAddMaterialCost_ValidLabel = false;	
	}
	//==========================================================================//
	// 							Add Material Save Data End						//
	//==========================================================================//

	//==========================================================================//
	// 						Lihat Material Save Data Begin						//
	//==========================================================================//
	// OLD FILTER
	// $scope.LihatMaterialCost_Filter_Clicked = function () {
	$scope.FilterFE = function () {
		$scope.GridApiMatCostDraftData.grid.clearAllFilters();
		$scope.GridApiMatCostData.grid.clearAllFilters();

		if($scope.LihatMaterialCost_TipeMaterial.name != "Finish Unit"){

			if (!angular.isUndefined($scope.LihatMaterialCost_Filter_NoMaterial)) {
				console.log("isi text : ", $scope.LihatMaterialCost_Filter_NoMaterial);

				$scope.GridApiMatCostDraftData.grid.getColumn("No Material").filters[0].term = $scope.LihatMaterialCost_Filter_NoMaterial;
				console.log("kolom : ", $scope.GridApiMatCostDraftData.grid.getColumn("No Material").filters);
			}
			console.log("isi nama material :", $scope.LihatMaterialCost_Filter_NamaMaterial);
			if (!angular.isUndefined($scope.LihatMaterialCost_Filter_NamaMaterial)) {
				console.log("masuk ke if lho");
				$scope.GridApiMatCostDraftData.grid.getColumn("Nama Material").filters[0].term = $scope.LihatMaterialCost_Filter_NamaMaterial;
			}
		}else{
			if (!angular.isUndefined($scope.LihatMaterialCost_Filter_ModelTipe)) {
				console.log('1',$scope.LihatMaterialCost_Filter_ModelTipe);
				console.log($scope.LihatMaterialCost_Filter_ModelTipe);
				$scope.GridApiMatCostDraftData.grid.getColumn("No Material").filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[0].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[1].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[2].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[3].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[4].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;
				// $scope.GridApiMatCostDraftData.grid.columns[5].filters[0].term = $scope.LihatMaterialCost_Filter_ModelTipe;

			}
		}
		$scope.GridApiMatCostDraftData.core.notifyDataChange(uiGridConstants.dataChange.ALL);
		$scope.GridApiMatCostDraftData.core.refresh();
	}

	// NEW FILTER
	$scope.LihatMaterialCost_Filter_Clicked = function(){
		if($scope.cekubah == 1){
			$scope.FilterFE();
		}else{
			$scope.FilterBE();
		}
	}
	$scope.FilterBE = function (){
		if($scope.LihatMaterialCost_Filter_NoMaterial == undefined){
			$scope.LihatMaterialCost_Filter_NoMaterial = "";
		}else if($scope.LihatMaterialCost_Filter_NamaMaterial == undefined){
			$scope.LihatMaterialCost_Filter_NamaMaterial ="";
		}else if($scope.LihatMaterialCost_Filter_ModelTipe == undefined){
			$scope.LihatMaterialCost_Filter_ModelTipe = "";
		}

		if((($scope.LihatMaterialCost_Filter_NoMaterial || $scope.LihatMaterialCost_Filter_NamaMaterial || $scope.LihatMaterialCost_Filter_ModelTipe) != null) && 
		(($scope.LihatMaterialCost_Filter_NoMaterial || $scope.LihatMaterialCost_Filter_NamaMaterial || $scope.LihatMaterialCost_Filter_ModelTipe) != undefined) ){
			if($scope.LihatMaterialCost_TipeMaterial.name == "Parts" || $scope.LihatMaterialCost_TipeMaterial.name == "Bahan"){
				MaterialCostFactory.getDataDetailFilter(1, $scope.uiGridPageSize, $scope.LihatMaterialCost_FileId, $scope.LihatMaterialCost_Filter_ModelTipe +'|'+ $scope.LihatMaterialCost_Filter_NoMaterial +'|'+ $scope.LihatMaterialCost_Filter_NamaMaterial )
				.then(
					function (res) {
						console.log("Filter Parts & bahan", res.data.Result);
						$scope.MatCostDraftDataLihat_UIGrid.data = res.data.Result;
						$scope.MatCostDraftData_DataResult = res.data.Result;
						$scope.MatCostDraftDataLihat_UIGrid.totalItems = res.data.Total;
						$scope.MatCostDraftDataLihat_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.MatCostDraftDataLihat_UIGrid.paginationPageSizes = [10, 100, 500];
					}
				);
			}else{
				MaterialCostFactory.getDataDetailFilter(1, $scope.uiGridPageSize, $scope.LihatMaterialCost_FileId, $scope.LihatMaterialCost_Filter_ModelTipe +'|'+ $scope.LihatMaterialCost_Filter_NoMaterial +'|'+ $scope.LihatMaterialCost_Filter_NamaMaterial )
				.then(
					function (res) {
						console.log("Filter Parts & bahan", res.data.Result);
						$scope.MatCostDraftDataLihat_UIGrid.data = res.data.Result;
						$scope.MatCostDraftData_DataResult = res.data.Result;
						$scope.MatCostDraftDataLihat_UIGrid.totalItems = res.data.Total;
						$scope.MatCostDraftDataLihat_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.MatCostDraftDataLihat_UIGrid.paginationPageSizes = [10, 100, 500];
					}
				);
			}
		}
	}

	$scope.LihatMaterialCost_HapusFilter_Clicked = function () {
		$scope.LihatMaterialCost_Filter_NoMaterial = "";
		$scope.LihatMaterialCost_Filter_NamaMaterial = "";
		$scope.LihatMaterialCost_Filter_ModelTipe = "";

		// $scope.MatCostDraftData_UIGrid.grid.clearAllFilters();
		// $scope.MatCostData_UIGrid_lihat.grid.clearAllFilters();
	}

	$scope.LihatMaterialCost_TipeMaterial_Changed = function () {

		if (!angular.isUndefined($scope.LihatMaterialCost_TipeMaterial)) {
			if ($scope.LihatMaterialCost_TipeMaterial.label == "Finish Unit") {
				$scope.ShowLihatMaterialCost_UnitBahan = false;
				$scope.ShowLihatMaterialCost_Unit = true;
			}
			else {
				$scope.ShowLihatMaterialCost_UnitBahan = true;
				$scope.ShowLihatMaterialCost_Unit = false;
			}
		}
	}

	$scope.LihatMaterialCost_Simpan_Clicked = function () {
		var now = new Date()
		console.log("isi filename : ", $scope.MaterialCost_FileName_Current);
		if ($scope.MaterialCost_FileName_Current != "") {
			var inputData = [
				{
					"UpdateType": "Updated",
					"MatCostFileId": $scope.MatCostData_UIGrid_lihat.data[0].MatCostFileId,
					"MatCostTypeId": $scope.LihatMaterialCost_TipeMaterial.value.valueType,
					"MatureDate": $filter('date')(new Date($scope.LihatMaterialCost_TanggalBerlaku), "yyyy-MM-dd"),
				}
			];

			console.log("simpan input data : ", inputData);
			if ($scope.LihatMaterialCost_TanggalBerlaku <= now){
				bsNotify.show({
					title: "Warning",
					content: "Tanggal berlaku harus lebih dari hari ini",
					type: 'warning'
				});
				return false;
			}

			MaterialCostFactory.submitMatCostData(inputData)
				.then(
					function (res) {
						msg = ("Data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: msg,
							type: "success"
						});
						$scope.LihatMaterialCost_Kembali_Clicked();
						$scope.MatCostFile_UIGrid_Paging(1,$scope.uiGridPageSize);
					}
				);
		}
		else {
			var inputData = [
				{
					"MatCostFileId": $scope.LihatMaterialCost_FileId,
					"ListMaterialCostDataView": $scope.MatCostDraftData_UIGrid.data,
					"MatureDate": $filter('date')(new Date($scope.LihatMaterialCost_TanggalBerlaku), "yyyy-MM-dd"),
				}
			];

			console.log('data yang diupdate : ', inputData);
			if ($scope.LihatMaterialCost_TanggalBerlaku <= now){
				bsNotify.show({
					title: "Warning",
					content: "Tanggal berlaku harus lebih dari hari ini",
					type: 'warning'
				});
				return false;
			}
			MaterialCostFactory.updateMatCostData(inputData)
				.then(
					function (res) {
						msg = ("Data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: msg,
							type: "success"
						});
						// if ($scope.MainMaterialCostisFilterClicked)
						// 	$scope.MatCostFile_UIGrid_Paging(1);
						$scope.LihatMaterialCost_Kembali_Clicked();
						$scope.MatCostFile_UIGrid_Paging(1,$scope.uiGridPageSize);
					}
				);
		}
	}

	$scope.LihatMaterialCost_Kembali_Clicked = function () {
		$scope.MainMaterialCost_Show = true;
		$scope.LihatMaterialCost_Show = false;
		$scope.LihatMaterialCost_ClearFields();
		$scope.LihatMaterialCost_HapusFilter_Clicked();
	}

	$scope.LihatMaterialCost_ClearFields = function () {
		angular.element(document.querySelector('#uploadLihatMaterialCost')).val(null);
		$scope.MaterialCost_FileName_Current = "";
	}
	//==========================================================================//
	// 						 Lihat Material Save Data End						//
	//==========================================================================//

	//==========================================================================//
	// 						 	Lihat Material Begin							//
	//==========================================================================//
	$scope.LihatMaterialCost_Upload_Clicked = function () {
		angular.element('#MatCostDataUnitDraftModal').remove();
		angular.element('#ModalMaterialCost').remove();
		angular.element('#MatCostDataUnitModal').remove();
		// angular.element('#ModalLihatMaterialCostUploadExcel').modal('show');
		$('#ModalLihatMaterialCostUploadExcel').modal('show');
		$('#ModalLihatMaterialCostUploadExcelNew').modal('show');
	}

	$scope.ModalLihatMaterialCostUploadExcel_OK_Clicked = function () {
		$scope.fileEdit = $scope.uploadLihatMaterialCost;
		XLSXInterface.loadToJson($scope.fileEdit, function (json) {
			ExcelData = json[0];
			$scope.MaterialCost_ExcelData = json;
			$scope.MaterialCost_FileName_Current = $scope.fileEdit.name;

			if (!$scope.validateColumn($scope.MaterialCost_ExcelData[0])) {
				msg = ("Kolom file excel tidak sesuai");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		});
		$scope.createInputDataCheck($scope.MaterialCost_ExcelData, $scope.MaterialCost_FileName_Current);
		MaterialCostFactory.checkMatCostData($scope.AddMaterialCost_inputDataCheck)
			.then(
				function (res) {
					msg = ("Data berhasil dicek");
					bsNotify.show({
						title: "Berhasil",
						content: msg,
						type: "success"
					});
					$scope.MaterialCost_FileId_Current = res.data.Result[0].MatCostFileId;
					$scope.LihatMaterialCost_FileId = res.data.Result[0].MatCostFileId;
					if ($scope.LihatMaterialCost_TipeMaterial.name != "Finish Unit")
						$scope.MatCostData_UIGrid_lihat.columnDefs = [
							{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
							{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
							{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
							
							{ name: "No Material", field: "MaterialNo", enableFiltering: true },
							{ name: "Nama Material", field: "MaterialName", enableFiltering: true },
							{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
							{ name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", enableCellEdit: false },
							{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
							{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
							{
								name: "Action", enableCellEdit: false,
								cellTemplate: '<a ng-click="grid.appScope.HapusEditMaterialCostData(row)">Hapus</a>'
							},

						];
					else
						$scope.MatCostData_UIGrid_lihat.columnDefs = [
							{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
							{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
							{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
							
							{ name: "Model Tipe", field: "ModelType", enableFiltering: true },
							{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
							{ name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", enableCellEdit: false },
							{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
							{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
							{
								name: "Action", enableCellEdit: false,
								cellTemplate: '<a ng-click="grid.appScope.HapusEditMaterialCostData(row)">Hapus</a>'
							},
						];


					$scope.MatCostData_UIGrid_Lihat_Paging(1, $scope.uiGridPageSize);
					// if ($scope.LihatMaterialCost_TipeMaterial.label != "Finish Unit")
					// 	$scope.MatCostData_UIGrid_Paging(1);
					// else
					// 	$scope.MatCostDataUnit_UIGrid_Paging(1);
					//MatCostData_UIGrid
					//console.log("INi setelah upload isi si matcost data uigrid", $scope.MatCostData_UIGrid_lihat.data);
					$scope.LihatMaterialCost_DraftData_Show = false;
					$scope.LihatMaterialCost_UploadData_Show = true;
					$scope.ModalLihatMaterialCostUploadExcel_Kembali_Clicked();
				}
			);
		console.log("data material cost : ", $scope.AddMaterialCost_inputDataCheck)
	}

	$scope.ModalLihatMaterialCostUploadExcel_Kembali_Clicked = function () {
		angular.element('#ModalLihatMaterialCostUploadExcelNew').modal('hide');
	}

	$scope.HapusMaterialCostDraftData = function (row) {
		var index = $scope.MatCostDraftData_UIGrid.data.indexOf(row.entity);
		$scope.MatCostDraftData_UIGrid.data.splice(index, 1);
	}
	$scope.HapusEditMaterialCostData = function(row){
		var index = $scope.MatCostData_UIGrid_lihat.data.indexOf(row.entity);
		$scope.MatCostData_UIGrid_lihat.data.splice(index, 1);
	}
	//==========================================================================//
	// 						 	  Lihat Material End							//
	//==========================================================================//

	//==========================================================================//
	// 						UIGrid Material Cost File Begin						//
	//==========================================================================//
	$scope.uiGridPageSize = 10;

	$scope.MatCostFile_UIGrid = {
		paginationPageSizes: null,
		paginationPageSizes: [10, 100, 500],
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		multiSelect: false,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
			{ name: "MaterialTypeId", field: "MaterialTypeId", visible: false },
			{ name: "Tipe Material", field: "MaterialTypeName" },
			{ name: "Tanggal Berlaku", field: "MatureDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nomor Dokumen Upload", field: "UploadDocNo" },
			{ name: "Tanggal Upload", field: "UploadDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Status", field: "StatusCode" },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.LihatMaterialCost(row)">Lihat</a>&nbsp;&nbsp;<a ng-hide ="grid.appScope.hide_ubah" ng-click="grid.appScope.UbahMaterialCost(row)">Ubah</a>'
			},
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostFile = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.MatCostFile_UIGrid_Paging(pageNumber, pageSize);
			});
		}
	};

	$scope.MatCostFile_UIGrid_Paging = function (pageNumber, pageSize) {
		$scope.MatCostFile_UIGrid.data = [];
		console.log("TanggalBerlakuAwal : ", $scope.TanggalBerlakuAwal);
		$scope.MainMaterialCostisFilterClicked = true;
		var tmptglBerAwal = angular.copy($scope.MainMaterialCost_TanggalBerlakuAwal);
		var tmptglBerAakhir = angular.copy($scope.MainMaterialCost_TanggalBerlakuAkhir);
		var tmptglUplAwal = angular.copy($scope.MainMaterialCost_TanggalUploadAwal);
		var tmptglUplAkhir = angular.copy($scope.MainMaterialCost_TanggalUploadAkhir);

		if(tmptglBerAwal == null || tmptglBerAwal == "Invalid Date" || tmptglBerAwal == undefined ){
			tmptglBerAwal = "";
		}else{
			tmptglBerAwal = angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAwal) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalBerlakuAwal), "yyyyMMdd");
		}

		if(tmptglBerAakhir == null || tmptglBerAakhir == "Invalid Date" || tmptglBerAakhir == undefined ){
			tmptglBerAakhir = "" ;
		}else{
			tmptglBerAakhir = angular.isUndefined($scope.MainMaterialCost_TanggalBerlakuAkhir) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalBerlakuAkhir), "yyyyMMdd");
		} 
		
		if(tmptglUplAwal == null || tmptglUplAwal == "Invalid Date" || tmptglUplAwal == undefined ){
			tmptglUplAwal = "";
		}else{
			tmptglUplAwal = angular.isUndefined($scope.MainMaterialCost_TanggalUploadAwal) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalUploadAwal), "yyyyMMdd")
		}

		if(tmptglUplAkhir == null || tmptglUplAkhir == "Invalid Date" || tmptglUplAkhir == undefined ){
			tmptglUplAkhir = "";
		}else{
			tmptglUplAkhir = angular.isUndefined($scope.MainMaterialCost_TanggalUploadAkhir) ? "" : $filter('date')(new Date($scope.MainMaterialCost_TanggalUploadAkhir), "yyyyMMdd")
		}

		MaterialCostFactory.getDataFile(pageNumber, pageSize,
			angular.isUndefined($scope.MainMaterialCost_TipeMaterial) ? "" : $scope.MainMaterialCost_TipeMaterial.valueType,
			angular.isUndefined($scope.MainMaterialCost_Status) ? "" : $scope.MainMaterialCost_Status.valueType,
			angular.isUndefined($scope.MainMaterialCost_NoDokumenUpload) ? "" :$scope.MainMaterialCost_NoDokumenUpload,
			tmptglBerAwal, tmptglBerAakhir, tmptglUplAwal, tmptglUplAkhir
		)
			.then(
				function (res) {

					console.log("res =>", res.data.Result);
					$scope.MatCostFile_UIGrid.data = res.data.Result;
					$scope.MatCostFile_UIGrid.totalItems = res.data.Total;
					$scope.MatCostFile_UIGrid.paginationPageSize = pageSize;
					$scope.MatCostFile_UIGrid.paginationPageSizes = [10, 100, 500];
				}

			);
			if($scope.HideButton == true){
				$scope.hide_ubah = false;
			}else{
				$scope.hide_ubah = true;
			}
	}
	//==========================================================================//
	// 						UIGrid Material Cost File End						//
	//==========================================================================//
	//==========================================================================//
	// 						UIGrid Material Cost Data Begin						//
	//==========================================================================//
	$scope.MatCostData_UIGrid = {//ccccc
		paginationPageSizes: null,
		paginationPageSizes: [10, 100, 500],
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		enableFiltering: true,
		//useExternalFiltering: true,
		enableSelectAll: true,
		multiSelect: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
			{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
			{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
			{ name: "TotalUploadData", field: "TotalUploadData", visible: false },
			{ name: "TotalErrData", field: "TotalErrData", visible: false },
			{ name: "No Material", field: "MaterialNo", enableFiltering: true, enableCellEdit: false },
			{ name: "Nama Material", field: "MaterialName", enableFiltering: true, enableCellEdit: false },
			{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
			{ name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", enableCellEdit: false },
			{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
			{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
			// {name:"Action", enableCellEdit:false, 
			// 		cellTemplate:'<a ng-click="grid.appScope.HapusMaterialCostData(row)">Hapus</a>'},
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostData = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	if (pageNumber > 1)
			// 		$scope.MatCostData_UIGrid_Paging(pageNumber);
			// });
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			// 	console.log("$scope.MainBankGridAPI.selection.getSelectedCount()", $scope.GridApiMatCostData.selection.getSelectedCount());
			// 	console.log("row", row.isSelected);
			// 	$scope.MatCostData_selectedRows = $scope.GridApiMatCostData.selection.getSelectedRows();

			// });
			// gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
			// 	$scope.grid_correction("partsbahan");
			// });
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					
			// 	// $scope.getData(pageNumber, pageSize);
			// 	// uiGridPageSizeMatCost = pageSize;
			// 	// uiGridPageNumberMatCost = pageNumber;
			// 	console.log("pageNumber ==>", pageNumber);
			// 	console.log("pageSize ==>", pageSize);
			// });
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("$scope.MainBankGridAPI.selection.getSelectedCount()", $scope.GridApiMatCostData.selection.getSelectedCount());
					console.log("row", row.isSelected);
					$scope.MatCostData_selectedRows = $scope.GridApiMatCostData.selection.getSelectedRows();
	
			});
		}
	};
	$timeout(function(){
		$scope.GridApiMatCostData.pagination.on.paginationChanged($scope, function (pageNumber, pageSize){
			$timeout(function(){
				console.log("ini dia>>>>>>>")
				$scope.getData(pageNumber, pageSize);
			},1000);
		});
	},1000);

	$scope.searchError = function (myArray) {
		for (var i = 0; i < myArray.length; i++) {
			if (myArray[i].LogName != null) {
				return true;
			}
		}
	}
	$scope.MatCostData_UIGrid_Paging = function (pageNumber) {

		MaterialCostFactory.getTempDataDetail(pageNumber, $scope.uiGridPageSize, $scope.MaterialCost_FileId_Current)
			.then(
				function (res) {
					//if (res.data.Result.length > 0) {
						// MaterialCostFactory.getFileDatabyFileName($scope.MaterialCost_FileName_Current)
						// 	.then(
						// 		function (res) {
						// 			$scope.MatCostData_DataGrid = res.data.Result;
						// 			if (!$scope.MatCostDeleteRow)
						// 				msg = ("Proses Upload Berhasil. " + $scope.MatCostData_DataGrid[0].DataUploaded + " data Material telah di-upload");

						// 			bsNotify.show({
						// 				title: "Berhasil",
						// 				content: msg,
						// 				type: "success"
						// 			});									
						// 			return;
						// 		}
						// 	);
						if (!angular.isUndefined(res.data.Result[0])) {
							$scope.AddMaterialCost_totalErrData = res.data.Result[0].TotalErrData;
							// $scope.MatCostData_DataGrid.data = res.data.Result;
							
							$scope.MatCostData_UIGrid.data = res.data.Result;
							$scope.MatCostData_UIGrid.totalItems = res.data.Total;
							$scope.MatCostData_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.MatCostData_UIGrid.paginationPageSizes = [10, 100, 500];

							if (!$scope.searchError(res.data.Result)) {
								$scope.AddMaterialCost_Simpan_Button_disabled = false;
							}
						}
						else
						{
							$scope.AddMaterialCost_totalErrData = [];
							$scope.MatCostData_DataGrid =[];
							$scope.AddMaterialCost_Simpan_Button_disabled = false;
							$scope.MatCostData_UIGrid.data =[];
							if (!$scope.searchError(res.data.Result)) {
								$scope.AddMaterialCost_Simpan_Button_disabled = false;
							}
						}
	
						console.log("res MatCostData_DataGrid nuse debug =>", $scope.MatCostData_DataGrid);
	
						if ($scope.AddMaterialCost_totalErrData > 0) {
							console.log("res MatCostData_UIGrid=>", res.data.Result);
							$scope.MatCostData_DataUIGrid = res.data.Result;
							$scope.MatCostData_UIGrid.data = res.data.Result;
							$scope.MatCostData_UIGrid.totalItems = res.data.Total;
							$scope.MatCostData_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.MatCostData_UIGrid.paginationPageSizes = [10, 100, 500];
						}
						else {
							if (!$scope.MatCostDeleteRow)
								msg = ("Proses Upload Berhasil. " + res.data.Result[0].TotalUploadData + " data Material telah di-Upload");
							else
								msg = "Hapus data berhasil.";
							bsNotify.show({
								title: "Berhasil",
								content: msg,
								type: "success"
							});	
							$scope.MatCostDeleteRow = false;				
						}
	
						if (!$scope.MatCostDeleteRow) {
	
							if ($scope.AddMaterialCost_totalErrData > 0) {
								var errMessage = 'Proses upload gagal. Terdapat ' + $scope.AddMaterialCost_totalErrData + ' data yang tidak valid';
								$scope.ShowAddMaterialCost_ValidLabel = true;
	
								//alert(errMessage);
								bsNotify.show({
									title: "Error",
									content: errMessage,
									type: "danger"
								});
								
							}
	
							$scope.MatCostDeleteRow = false;
						}

					//}
				
				}

			);
	}

	$scope.MatCostData_UIGrid_Lihat_Paging = function (pageNumber, pageSize) {

		MaterialCostFactory.getTempDataDetail(pageNumber, pageSize, $scope.LihatMaterialCost_FileId)
			.then(
				function (res) {
					$scope.MatCostData_UIGrid_lihat.data = [];
					if (res.data.Result.length == 0) {
						console.log("proses data berhasil upload semua tanpa error");
						MaterialCostFactory.getFileDatabyFileName($scope.MaterialCost_FileName_Current)
							.then(
								function (res) {
									$scope.MatCostData_DataGrid = res.data.Result;
									if (!$scope.MatCostDeleteRow)
										msg = ("Proses Upload Berhasil. " + $scope.MatCostData_DataGrid[0].DataUploaded + " data Material telah di-upload");

									bsNotify.show({
										title: "Berhasil",
										content: msg,
										type: "success"
									});
									
									return;
								}
							);
					}

					if (!angular.isUndefined(res.data.Result[0])) {
						$scope.AddMaterialCost_totalErrData = res.data.Result[0].TotalErrData;
						$scope.MatCostData_DataGrid = res.data.Result;
						
						$scope.MatCostData_UIGrid_lihat.data = res.data.Result;
						$scope.MatCostData_UIGrid_lihat.totalItems = res.data.Total;
						$scope.MatCostData_UIGrid_lihat.paginationPageSize = pageSize;
						$scope.MatCostData_UIGrid_lihat.paginationPageSizes = [10, 100, 500];
						if (!$scope.searchError(res.data.Result)) {
							$scope.AddMaterialCost_Simpan_Button_disabled = false;
						}
					}

					console.log("res MatCostData_DataGrid nuse debug =>", $scope.MatCostData_DataGrid);

					if ($scope.AddMaterialCost_totalErrData > 0) {
						console.log("res MatCostData_UIGrid=>", res.data.Result);
						$scope.MatCostData_DataUIGrid = res.data.Result;
						$scope.MatCostData_UIGrid_Lihat.data = res.data.Result;
						$scope.MatCostData_UIGrid_Lihat.totalItems = res.data.Total;
						$scope.MatCostData_UIGrid_Lihat.paginationPageSize = $scope.uiGridPageSize;
						$scope.MatCostData_UIGrid_Lihat.paginationPageSizes = [10, 100, 500];
					}
					else {
						if (!$scope.MatCostDeleteRow)
							msg = ("Proses Upload Berhasil. " + res.data.Result[0].TotalUploadData + " data Material telah di-Upload");

						bsNotify.show({
							title: "Berhasil",
							content: msg,
							type: "success"
						});
						
					}

					if (!$scope.MatCostDeleteRow) {

						if ($scope.AddMaterialCost_totalErrData > 0) {
							var errMessage = 'Proses upload gagal. Terdapat ' + $scope.AddMaterialCost_totalErrData + ' data yang tidak valid';
							$scope.ShowAddMaterialCost_ValidLabel = true;

							//alert(errMessage);
							bsNotify.show({
								title: "Error",
								content: errMessage,
								type: "danger"
							});
							
						}

						$scope.MatCostDeleteRow = false;
					}
				}

			);
	}
	//==========================================================================//
	// 						UIGrid Material Cost Data End						//
	//==========================================================================//

	//==========================================================================//
	// 					UIGrid Material Cost Data Unit Begin					//
	//==========================================================================//
	
	$scope.MatCostData_UIGrid_lihat = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		enableFiltering: true,
		multiSelect: false,
		enableColumnResizing: true,
		
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostDataLihat = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// if (pageNumber > 1)
					$scope.MatCostData_UIGrid_Lihat_Paging(pageNumber, pageSize);
			});

			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.MatCostDataLihat_selectedRows = $scope.GridApiMatCostDataLihat.selection.getSelectedRows();
			});
		
		}
	};
	$scope.MatCostDataUnit_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination: true,
		// rowSelection: true,
		// enableFiltering: true,
		// multiSelect: true,
		// enableColumnResizing: true,
		paginationPageSizes: [250, 500, 1000],
		useCustomPagination: true,
		useExternalPagination: true,

		showGridFooter: true,
		showColumnFooter: true,
		enableFiltering: true,

		enableSorting: true,
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: "MatCostFileId", field: "MatCostFileId", visible: false },
			{ name: "MatCostTempId", field: "MatCostTempId", visible: false },
			{ name: "MatCostLogId", field: "MatCostLogId", visible: false },
			{ name: "TotalUploadData", field: "TotalUploadData", visible: false },
			{ name: "TotalErrData", field: "TotalErrData", visible: false },
			{ name: "Model Tipe", field: "ModelType", enableFiltering: true, enableCellEdit: false },
			{ name: "Standar Cost Sebelum", cellFilter: 'rupiahC', field: "PrevCost", enableCellEdit: false },
			{ name: "Standar Cost Baru", cellFilter: 'rupiahC', field: "StandardCost", enableCellEdit: false },
			{ name: "Perbedaan", cellFilter: 'rupiahC', field: "DiffAmount", enableCellEdit: false },
			{ name: "Pesan Error", field: "LogName", enableCellEdit: false },
			// {
			// 	name: "Action", enableCellEdit: false,
			// 	cellTemplate: '<a ng-click="grid.appScope.HapusMaterialCostData(row)">Hapus</a>'
			// },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostDataUnit = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	if (pageNumber > 1)
			// 		$scope.MatCostDataUnit_UIGrid_Paging(pageNumber);
			// });
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.MatCostDataUnit_selectedRows = $scope.GridApiMatCostDataUnit.selection.getSelectedRows();
			});
			// gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
			// 	$scope.grid_correction("finishunit");
			// });
			//xxxxx
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					
				// $scope.getData(pageNumber, pageSize);
				// uiGridPageSizeMatCost = pageSize;
				// uiGridPageNumberMatCost = pageNumber;
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});

			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			// 	$scope.MatCostDataUnit_selectedRows = $scope.GridApiMatCostDataUnit.selection.getSelectedRows();
			// });
			// gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
			// 	$scope.grid_correction("finishunit");
			// });
		}
	};
	$timeout(function(){
		$scope.GridApiMatCostDataUnit.pagination.on.paginationChanged($scope, function (pageNumber, pageSize){
			$timeout(function(){
				console.log("ini data>>>>>>>")
				$scope.getDataUnit(pageNumber, pageSize);
			},1000);
		});
	},1000);

	$scope.MatCostDataUnit_UIGrid_Paging = function (pageNumber) {

		console.log("file id current :", $scope.MaterialCost_FileId_Current);
		MaterialCostFactory.getTempDataDetail(pageNumber, $scope.uiGridPageSize, $scope.MaterialCost_FileId_Current)
			.then(
				function (res) {
					console.log("res matcostdataunit berhasil semua : ", res.data.Result.length);

					// if (res.data.Result.length == 0) {
					// 	console.log("proses data berhasil upload semua tanpa error");
					// 	$scope.MatCostDataUnit_UIGrid.data = [];
					// 	//MaterialCostFactory.getFileDatabyFileName($scope.MaterialCost_FileName_Current)
					// 	MaterialCostFactory.getFileDatabyFileId($scope.MaterialCost_FileId_Current)
					// 		.then(
					// 			function (res) {
					// 				$scope.MatCostDataUnit_DataGrid = res.data.Result;
					// 				if (!$scope.MatCostDeleteRow)
					// 					msg = ("Proses Upload Berhasil. " + $scope.MatCostDataUnit_DataGrid[0].DataUploaded + " data Material telah di-upload");
					// 				bsNotify.show({
					// 					title: "Berhasil",
					// 					content: msg,
					// 					type: "success"
					// 				});
									
					// 				return;
					// 			}
					// 		);
					// }

					if (!angular.isUndefined(res.data.Result[0])) {
						$scope.AddMaterialCost_totalErrData = res.data.Result[0].TotalErrData;
						// $scope.MatCostDataUnit_DataGrid = res.data.Result;
						$scope.MatCostDataUnit_UIGrid.data = res.data.Result;

						$scope.MatCostDataUnit_UIGrid.totalItems = res.data.Total;
						$scope.MatCostDataUnit_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.MatCostDataUnit_UIGrid.paginationPageSizes = [10, 25, 50];

						if (!$scope.searchError(res.data.Result)) {
							$scope.AddMaterialCost_Simpan_Button_disabled = false;
						}
					}
					else
					{
						$scope.AddMaterialCost_totalErrData = [];
						$scope.MatCostDataUnit_DataGrid = [];
						$scope.MatCostDataUnit_UIGrid.data = [];
						$scope.AddMaterialCost_Simpan_Button_disabled = false;
						if (!$scope.searchError(res.data.Result)) {
							$scope.AddMaterialCost_Simpan_Button_disabled = false;
						}
					}

					console.log("Mat cost data unit : ", $scope.MatCostDataUnit_DataGrid);

					if ($scope.AddMaterialCost_totalErrData > 0) {
						console.log("res =>", res.data.Result);
						$scope.MatCostDataUnit_UIGrid.data = res.data.Result;
						$scope.MatCostDataUnit_UIGrid.totalItems = res.data.Total;
						$scope.MatCostDataUnit_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						$scope.MatCostDataUnit_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
					}
					else {
						if (!$scope.MatCostDeleteRow)
							msg = ("Proses Upload Berhasil. " + res.data.Result[0].TotalUploadData + " data Material telah di-Upload");
						else
							msg = "Data berhasil di hapus"
						bsNotify.show({
							title: "Berhasil",
							content: msg,
							type: "success"
						});
						
					}

					if (!$scope.MatCostDeleteRow) {
						console.log("Total Unit data Error : ", $scope.AddMaterialCost_totalErrData);
						if ($scope.AddMaterialCost_totalErrData > 0) {
							var errMessage = 'Proses upload gagal. Terdapat ' + $scope.AddMaterialCost_totalErrData + ' data yang tidak valid';
							$scope.ShowAddMaterialCost_ValidLabel = true;

							msg = (errMessage);
							bsNotify.show({
								title: "Error",
								content: msg,
								type: "danger"
							});
							
						}

						$scope.MatCostDeleteRow = false;
					}
				}

			);
	}
	//==========================================================================//
	// 						UIGrid Material Cost Data Unit End					//
	//==========================================================================//

	//==========================================================================//
	// 					UIGrid Material Cost Draft Data Begin					//
	//==========================================================================//
	$scope.MatCostDraftData_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: false,
		useExternalPagination: false,
		rowSelection: true,
		enableFiltering: true,
		//useExternalFiltering: true,
		enableColumnResizing: true,
		multiSelect: true,
		// columnDefs: [
		// {name:"MatCostFileId",field:"MatCostFileId",visible:false},
		// {name:"MatCostTempId",field:"MatCostTempId",visible:false},
		// {name:"MatCostLogId",field:"MatCostLogId",visible:false},
		// {name:"No Material",field:"MaterialNo", enableCellEdit:false},
		// {name:"Nama Material", field:"MaterialName", enableCellEdit:false},
		// {name:"Standar Cost Sebelum", field:"PrevCost", enableCellEdit:false},
		// {name:"Standar Cost Baru", field:"StandardCost", cellClass:function(grid, row, rowRenderIndex, colRenderIndex){
		// row.entity.DiffAmount = row.entity.StandardCost - row.entity.PrevCost;
		// }},
		// {name:"Perbedaan", field:"DiffAmount", enableCellEdit:false},
		// {name:"Pesan Error", field:"LogName", enableCellEdit:false},
		// {name:"Action", 
		// cellTemplate:'<a ng-click="grid.appScope.HapusMaterialCostDraftData(row)">Hapus</a>'},
		// ],
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostDraftData = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// if($scope.cekubah == 0){
				// 	$scope.MatCostDraftData_UIGrid_Paging(pageNumber, pageSize);
				// }
			});
			//$scope.selectedRows = $scope.GridApiMatCostDraftData.selection.getSelectedRows();
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.MatCostDraftData_selectedRows = $scope.GridApiMatCostDraftData.selection.getSelectedRows();
			});
			gridApi.edit.on.beginCellEdit($scope, function (rowEntity, colDef) {
				$scope.copyStandarCost = angular.copy(rowEntity.StandardCost);
				$scope.lastCellEdited = rowEntity.MatCostTempId;
			});
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				if(newValue <= 0){
					var newValue = $scope.copyStandarCost;
					rowEntity[colDefs.name] = newValue;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Nominal Standar Cost Baru Tidak Boleh 0.",
								type: 'warning'
							}
						); 
					rowEntity.StandardCost = $scope.copyStandarCost;
					rowEntity.DiffAmount = $scope.copyStandarCost;
				}
			});
		}
	};

	// Lihat Start
	$scope.MatCostDraftDataLihat_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		enableFiltering: true,
		//useExternalFiltering: true,
		enableColumnResizing: true,
		multiSelect: true,
		onRegisterApi: function (gridApi) {
			$scope.GridApiMatCostDraftDataLihat = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// if($scope.cekubah == 0){
					$scope.MatCostDraftData_UIGridLihat_Paging(pageNumber, pageSize);
				// }
			});
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.MatCostDraftData_selectedRows = $scope.GridApiMatCostDraftData.selection.getSelectedRows();
			});
			gridApi.edit.on.beginCellEdit($scope, function (rowEntity, colDef) {
				$scope.lastCellEdited = rowEntity.MatCostTempId;
			});
		}
	};

	$scope.MatCostDraftData_UIGridLihat_Paging = function (pageNumber,pageSize) {
		MaterialCostFactory.getDataDetailLihat(pageNumber, pageSize, $scope.LihatMaterialCost_FileId, $scope.LihatMaterialCost_Filter_ModelTipe +'|'+ $scope.LihatMaterialCost_Filter_NoMaterial +'|'+ $scope.LihatMaterialCost_Filter_NamaMaterial)
			.then(
				function (res) {
				// 	console.log("res MatCostDraftData_UIGrid=>", res.data.Result);

					$scope.MatCostDraftDataLihat_UIGrid.data = res.data.Result;
				// 	$scope.MatCostDraftData_DataResult = res.data.Result;
					$scope.MatCostDraftDataLihat_UIGrid.totalItems = res.data.Total;
					$scope.MatCostDraftDataLihat_UIGrid.paginationPageSize = pageSize;
					$scope.MatCostDraftDataLihat_UIGrid.paginationPageSizes = [10, 100, 500];
				}

			);
	}
	// Lihat End

	$scope.MatCostDraftData_UIGrid_Paging = function (pageNumber,pageSize) {
		MaterialCostFactory.getDataDetail(pageNumber, pageSize, $scope.LihatMaterialCost_FileId)
			.then(
				function (res) {
				// 	console.log("res MatCostDraftData_UIGrid=>", res.data.Result);

					$scope.MatCostDraftData_UIGrid.data = res.data.Result;
				// 	$scope.MatCostDraftData_DataResult = res.data.Result;
					$scope.MatCostDraftData_UIGrid.totalItems = res.data.Total;
				$scope.MatCostDraftData_UIGrid.paginationPageSize = pageSize;
					$scope.MatCostDraftData_UIGrid.paginationPageSizes = [10, 100, 500];
				}

			);
	}
	//==========================================================================//
	// 					  UIGrid Material Cost Draft Data End					//
	//==========================================================================//
	$scope.MatCostDataUnitDraft_actDel = function () {
		$scope.MatCostDataUnit_TotalSelectedData = 0;
		if ($scope.GridApiMatCostDraftData.selection.getSelectedCount() > 0) {
			$scope.MatCostDataUnitDraft_TotalSelectedData = $scope.GridApiMatCostDraftData.selection.getSelectedCount();
		}
		else if ($scope.GridApiMatCostDataUnit.selection.getSelectedCount() > 0) {
			$scope.MatCostDataUnitDraft_TotalSelectedData = $scope.GridApiMatCostDataUnit.selection.getSelectedCount();
		}
		//$('#MatCostDataUnitModal').modal('show');
		angular.element('#ModalMaterialCost').remove();
		angular.element('#ModalLihatMaterialCostUploadExcel').remove();
		angular.element('#MatCostDataUnitModal').remove();
		$('#MatCostDataUnitDraftModal').modal('show');
	}

	$scope.MatCostDataUnitDraftCancel_Button_Clicked = function () {
		$('#MatCostDataUnitModal').modal('hide');
	}

	$scope.MatCostDataUnitDraftOK_Button_Clicked = function () {
		debugger;
		var selectedRowEntities = $scope.GridApiMatCostDraftData.selection.getSelectedRows();

		angular.forEach(selectedRowEntities, function (rowEntity, key) {
			var rowIndexToDelete = $scope.MatCostDraftData_UIGrid.data.indexOf(rowEntity);
			$scope.MatCostDraftData_UIGrid.data.splice(rowIndexToDelete, 1);

		});
	}


	$scope.MatCostDataUnit_actDel = function () {
		$scope.MatCostDataUnit_TotalSelectedData = 0;
		if ($scope.GridApiMatCostDraftData.selection.getSelectedCount() > 0) {
			$scope.MatCostDataUnit_TotalSelectedData = $scope.GridApiMatCostDraftData.selection.getSelectedCount();
		}
		else if ($scope.GridApiMatCostDataUnit.selection.getSelectedCount() > 0) {
			$scope.MatCostDataUnit_TotalSelectedData = $scope.GridApiMatCostDataUnit.selection.getSelectedCount();
		}
		else if ($scope.GridApiMatCostData.selection.getSelectedCount() > 0) {
			$scope.MatCostDataUnit_TotalSelectedData = $scope.GridApiMatCostData.selection.getSelectedCount();
		}
		angular.element('#ModalMaterialCost').remove();
		angular.element('#ModalLihatMaterialCostUploadExcel').remove();
		//angular.element('#MatCostDataUnitModal').remove();
		angular.element('#MatCostDataUnitDraftModal').remove();
		//$('#MatCostDataUnitDraftModal').modal('show');
		$('#MatCostDataUnitModalNew').modal('show');
	}
	$scope.MatCostDataUnitCancel_Button_Clicked = function () {
		$('#MatCostDataUnitDraftModal').modal('hide');
	} 

	$scope.MatCostDataUnitOK_Button_Clicked = function () {
		var deleteData = [];
		debugger;
		if ($scope.GridApiMatCostDraftData.selection.getSelectedCount() > 0) {
			angular.forEach($scope.GridApiMatCostDraftData.selection.getSelectedRows(), function (row, index) {
				deleteData.push({ "MatCostFileId": row.MatCostFileId, "MatCostLogId": row.MatCostLogId, "MatCostTempId": row.MatCostTempId });
			});
		}
		else if ($scope.GridApiMatCostDataUnit.selection.getSelectedCount() > 0) {
			angular.forEach($scope.GridApiMatCostDataUnit.selection.getSelectedRows(), function (row, index) {
				deleteData.push({ "MatCostFileId": row.MatCostFileId, "MatCostLogId": row.MatCostLogId, "MatCostTempId": row.MatCostTempId });
			});
		}
		else if ($scope.GridApiMatCostData.selection.getSelectedCount() > 0) {
			angular.forEach($scope.GridApiMatCostData.selection.getSelectedRows(), function (row, index) {
				deleteData.push({ "MatCostFileId": row.MatCostFileId, "MatCostLogId": row.MatCostLogId, "MatCostTempId": row.MatCostTempId });
			});
		}

		MaterialCostFactory.deleteMultiple(deleteData).
			then(
				function (res) {
					//$scope.MainBank_GetList();
					//$scope.MatCostDataUnit_UIGrid_Paging(1);
					$scope.GridApiMatCostDataUnit.selection.clearSelectedRows();
					$scope.MatCostDeleteRow = true;
					if ($scope.AddMaterialCost_TipeMaterial.label != "Finish Unit")
						$scope.MatCostData_UIGrid_Paging(1);
					else
						$scope.MatCostDataUnit_UIGrid_Paging(1);
				}
			);
	}
}).directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function () {
				scope.$apply(function () {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);

app.filter('rupiahC', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
});

