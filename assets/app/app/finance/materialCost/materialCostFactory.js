angular.module('app')
	.factory('MaterialCostFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getDataFile: function(start, limit, tipeMaterial, status, noDokumen, tanggalBerlakuAwal, tanggalBerlakuAkhir, tanggalUpdateAwal, tanggalUpdateAkhir){
			var url = '/api/fe/FinanceMaterialCost/?Start=' 
							+ start 
							+ '&Limit=' + limit 
							+ '&materialtypeid=' + tipeMaterial 
							+ '&statusid=' + status
							+ '&maturitydatestart=' + tanggalBerlakuAwal
							+ '&maturitydateend=' + tanggalBerlakuAkhir
							+ '&uploaddatestart=' + tanggalUpdateAwal
							+ '&uploaddateend=' + tanggalBerlakuAkhir
							+ '&NoDoc=' + noDokumen
							+ '&OutletId=' + user.OutletId;
			console.log("material cost file : ", url);
			var res=$http.get(url);
			return res;
		},
		
		checkMatCostData: function(inputData){
			var url = '/api/fe/FinanceMaterialCost/SubmitDetailTempData';
			var param = JSON.stringify(inputData);
			console.log("material cost data : ", url);
			var res=$http.post(url, param);
			return res;
		},
		
		submitMatCostData: function(inputData){
			var url = '/api/fe/FinanceMaterialCost/SubmitData';
			var param = JSON.stringify(inputData);
			console.log("material cost data : ", url);
			var res=$http.post(url, param);
			return res;
		},
		
		updateMatCostData: function(inputData){
			var url = '/api/fe/FinanceMaterialCost/UpdateDetailData';
			var param = JSON.stringify(inputData);
			console.log("material cost update data : ", url);
			var res=$http.put(url, param);
			return res;
			
		},
		getMaterialType : function(){
			var url = '/api/fe/FinanceParam/MaterialCostTypeId';
			console.log("material cost Type Id : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getFileDatabyFileName : function(fileName){
			var url = '/api/fe/FinanceMaterialCost/GetFilebyFileName/?FileName='+fileName;
			console.log("material cost Type Id : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getFileDatabyFileId : function(fileId){
			var url = '/api/fe/FinanceMaterialCost/GetFilebyFileId/?FileId='+fileId;
			console.log("Get File by File Id: ", url);
			var res=$http.get(url);
			return res;
		},
		
		
		getMaterialStatus : function(){
			var url = '/api/fe/FinanceParam/MaterialCostStatusId';
			console.log("material Status Type Id : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getTempDataDetail : function(start, limit, fileId){
			//var url = '/api/fe/FinanceMaterialCost/GetDetailDataTemp/?start=' + start + '&limit=' + limit + '&filename=' + fileName;
			var url = '/api/fe/FinanceMaterialCost/GetDetailDataTemp/?start=' + start + '&limit=' + limit + '&fileid=' + fileId;
			console.log("material cost Detail Data Temp : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataDetail : function(start, limit, fileId){
			var url = '/api/fe/FinanceMaterialCost/GetDetailData/?start=' + start + '&limit=100000' + '&fileid=' + fileId;
			console.log("material cost Detail Data : ", url);
			var res=$http.get(url);
			return res;
		},

		getDataDetailLihat : function(start, limit, fileId, filter){
			var url = '/api/fe/FinanceMaterialCost/GetDetailData/?start=' + start + '&limit=' + limit + '&fileid=' + fileId+ '&Filter=' +filter;
			console.log("material cost Detail Data : ", url);
			var res=$http.get(url);
			return res;
		},

		getDataDetailFilter : function(start, limit, fileId, filter){
			var url = '/api/fe/FinanceMaterialCost/GetDetailData/?start=' + start + '&limit=' + limit + '&fileid=' + fileId+ '&Filter=' +filter;
			console.log("material cost Detail Data : ", url);
			var res=$http.get(url);
			return res;
		},
		
		deleteData: function(MatCostFileId, MatCostLogId, MatCostTempId) {
			var url = '/api/fe/financematerialcost/deletedata/?MatCostFileId='+MatCostFileId + '&matcostlogid=' + MatCostLogId + '&matcosttempid=' + MatCostTempId;
			var res=$http.delete(url);
			return res;
		},

		deleteMultiple: function(inputData) {
			var url = '/api/fe/FinanceMaterialCost/DeleteData/';
			var param = JSON.stringify(inputData);
			var res=$http.post(url, param);
			return res;
		},
		
		clearUncommitedData: function(FileId) {
			var url = '/api/fe/financematerialcost/CleanData/?FileId='+FileId;
			var res=$http.delete(url);
			return res;
		},
		// New Irfan
		downloadExcel: function(MaterialCostType){
			var url = '/api/fe/FinanceMaterialCost/DownloadTemplate?MaterialType='+MaterialCostType;
			console.log("Download Excel ", url);
			var res=$http.get(url);
			return res;
		},
	}
});
