var app = angular.module('app');
app.controller('MasterPricingDealerToBranchAreaPricingController', function ($scope, $http, $filter, CurrentUser, bsNotify, bsAlert, MasterPricingDealerToBranchAreaPricingFactory, $timeout) {



	$scope.optionsKolomFilterSatu_AreaPricing = [
		// {name: "Dealer Name", value:"DealerName"}, 
		// {name: "Province", value:"Province"} ,	 
		{ name: "Price Area", value: "PricingClusterCode" },
		{ name: "Model", value: "Model" },
		{ name: "Nama Model / Grade", value: "NamaModel" },
		{ name: "Katashiki", value: "Katashiki" },
		{ name: "Suffix", value: "Suffix" }
	];
	$scope.optionsKolomFilterDua_AreaPricing = [
		// {name: "Dealer Name", value:"DealerName"}, 
		// {name: "Province", value:"Province"} ,	 
		{ name: "Price Area", value: "PricingClusterCode" },
		{ name: "Model", value: "Model" },
		{ name: "Nama Model / Grade", value: "NamaModel" },
		{ name: "Katashiki", value: "Katashiki" },
		{ name: "Suffix", value: "Suffix" }
	];

	$scope.optionsComboBulkAction_AreaPricing = [{ name: "Delete", value: "Delete" }];

	$scope.optionsComboFilterGrid_AreaPricing = [
		// {name:"Dealer Type", value:"DealerType"}, 
		// {name:"Dealer Name", value:"DealerName"},
		// {name:"Province", value:"Province"}, 
		{ name: "Price Area", value: "PricingClusterCode" },
		{ name: "BranchCode", value: "BranchCode" },
		{ name: "Branch", value: "Branch" },
		{ name: "Effective Date From", value: "EffectiveDateFrom" },
		{ name: "Effective Date To", value: "EffectiveDateTo" },
		{ name: "Remarks", value: "Remarks" }
	];

	$scope.btnUpload = "hide";
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'AreaPricing';
	$scope.cekKolom = '';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_AreaPricing = true;
	angular.element('#AreaPricingModal').modal('hide');

	$scope.filterModel = [];


	$scope.MasterSellingPriceFU_AreaPricing_grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			// { name: 'DealerType', displayName: 'Dealer Type', field: 'DealerType',enableCellEdit: false, enableHiding: false },
			// { name: 'DealerName', displayName: 'Dealer Name', field: 'DealerName',enableCellEdit: false, enableHiding: false },			
			// { name: 'Province', displayName: 'Province', field: 'Province',enableCellEdit: false , enableHiding: false},
			{ width: '15%', enableHiding: false, enableCellEdit: false, name: 'Pricing Cluster Code', displayName: 'Pricing Cluster Code', field: 'PricingClusterCode' },
			{ width: '09%', enableHiding: false, enableCellEdit: false, name: 'Branch Code', displayName: 'Branch Code', field: 'BranchCode' },
			{ width: '23%', enableHiding: false, enableCellEdit: false, name: 'Branch Name', displayName: 'Branch Name', field: 'Branch' },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Effective DateF rom', displayName: 'Effective Date From', field: 'EffectiveDateFrom', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Effective Date To', displayName: 'Effective Date To', field: 'EffectiveDateTo', cellFilter: 'date:\"dd-MM-yyyy\"' }, 
			{ width: '21%', enableHiding: false, enableCellEdit: false, name: 'Remarks', displayName: 'Remarks', field: 'Remarks' },
			// { name: 'AreaPricingId', displayName: 'Id', field: 'AreaPricingId', visible: false, enableHiding: false },
		],

		onRegisterApi: function (gridApi) {
			$scope.MasterSellingPriceFU_AreaPricing_gridAPI = gridApi;
		}
	};


	$scope.FilterGridMasterAreaPricing = function () {
		var inputfilter = $scope.textFilterMasterAreaPricing;
		console.log('$scope.textFilterMasterAreaPricing ===>', $scope.textFilterMasterAreaPricing);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_AreaPricing_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterAreaPricing + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_AreaPricing_grid.data = $scope.MasterSellingPriceFU_AreaPricing_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data if ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data)

		} else {
			$scope.MasterSellingPriceFU_AreaPricing_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data else ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data)
		}
	};

	$scope.fixDate = function (date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() +
				('0' + (date.getMonth() + 1)).slice(-2) + 
				('0' + date.getDate()).slice(-2)
			return fix;
		} else {
			return null;
		}
	};


	$scope.MasterSelling_AreaPricing_Generate_Clicked = function () {

		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined || $scope.filterModel.TanggalFilterStart == null) {
			$scope.filterModel.TanggalFilterStart = "";
		} else {
			$scope.filterModel.TanggalFilterStart = $scope.fixDate($scope.filterModel.TanggalFilterStart)
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined || $scope.filterModel.TanggalFilterEnd == null) {
			$scope.filterModel.TanggalFilterEnd = "";
		} else {
			$scope.filterModel.TanggalFilterEnd = $scope.fixDate($scope.filterModel.TanggalFilterEnd)
		}

		if ($scope.filterModel.PricingClusterCode == null || $scope.filterModel.PricingClusterCode == undefined) {
			$scope.filterModel.PricingClusterCode ="";
		}

		var UrlParameter = "&price_area=" + $scope.filterModel.PricingClusterCode +
			"&date_from=" + $scope.filterModel.TanggalFilterStart +
			"&date_to=" + $scope.filterModel.TanggalFilterEnd;


		// var filter = [{
		// 	PEClassification: $scope.JenisPE,
		// 	Filter1: 'price_area',
		// 	Text1: $scope.filterModel.PricingClusterCode,
		// 	// AndOr: $scope.Filter_AreaPricing_AndOr,
		// 	// Filter2: $scope.KolomFilterDua_AreaPricing,
		// 	// Text2: $scope.TextFilterDua_AreaPricing,
		// 	StartDate: tglStart,
		// 	EndDate: tglEnd,
		// 	PEType: $scope.TypePE
		// }];

		// var filter = [{
		// 	PEClassification: $scope.JenisPE,
		// 	price_area: $scope.filterModel.PricingClusterCode, 
		// 	date_from: $scope.filterModel.TanggalFilterStart,
		// 	date_to: $scope.filterModel.TanggalFilterEnd,
		// 	PEType: $scope.TypePE
		// }];

		
// MasterPricingDealerToBranchAreaPricingFactory.getData(1, 10000, JSON.stringify(filter))

		MasterPricingDealerToBranchAreaPricingFactory.getData(1, 10000, UrlParameter)
	.then(
		function (res) {
			$scope.MasterSellingPriceFU_AreaPricing_grid.data = res.data.Result;            //Data hasil dari WebAPI
			$scope.MasterSellingPriceFU_AreaPricing_grid.totalItems = res.data.Total;
			$scope.MasterSellingPriceFU_AreaPricing_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_AreaPricing_grid.data);

		},
		function (err) {
			console.log('error -->', err)
		}
	);
	}


$scope.formatDate = function (date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('');
};

$scope.loadXLS = function (ExcelFile) {
	$scope.MasterSelling_ExcelData = [];
	var myEl = angular.element(document.querySelector('#uploadSellingFile_AreaPricing')); //ambil elemen dari dokumen yang di-upload 
	console.log("myEl : ", myEl);

	XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
		console.log('data asli yang di upload ===>', json.EffectiveDateFrom, json.EffectiveDateTo);
		for (i = 0; i < json.length; i++) {
			var tglUpload = json[i].EffectiveDateFrom //format 20191231
			var thn = tglUpload.substring(0, 4);
			var bln = tglUpload.substring(4, 6);
			var tgl = tglUpload.substring(6, 8);
			var tgl_jadi = tgl + "-" + bln + "-" + thn;
			json[i].EffectiveDateFrom = tgl_jadi;

			var tglUploadTo = json[i].EffectiveDateTo //format 20191231
			var thnTo = tglUploadTo.substring(0, 4);
			var blnTo = tglUploadTo.substring(4, 6);
			var tglTo = tglUploadTo.substring(6, 8);

			var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
			json[i].EffectiveDateTo = tgl_jadiTo;
		}

		$scope.btnUpload = "hide";
		console.log("myEl : ", myEl);
		console.log("json : ", json);
		console.log('data setelah dirubah ===>', json.EffectiveDateFrom, json.EffectiveDateTo);

		$scope.MasterSelling_ExcelData = json;
		$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

		$scope.MasterSelling_AreaPricing_Upload_Clicked();
		myEl.val('');
	});
}

$scope.validateColumn = function (inputExcelData) {
	// ini harus di rubah
	$scope.cekKolom = '';
	// if (angular.isUndefined(inputExcelData.DealerType)) {
	// 	$scope.cekKolom = $scope.cekKolom + ' DealerType \n';
	// }

	// if (angular.isUndefined(inputExcelData.DealerName)) {
	// 	$scope.cekKolom = $scope.cekKolom + ' DealerName \n';
	// }
	// if 	(angular.isUndefined(inputExcelData.Province)){
	// 	$scope.cekKolom = $scope.cekKolom + ' Province \n';
	// }
	if (angular.isUndefined(inputExcelData.PricingClusterCode)) {
		$scope.cekKolom = $scope.cekKolom + ' PricingClusterCode\n';
	}
	if (angular.isUndefined(inputExcelData.BranchCode)) {
		$scope.cekKolom = $scope.cekKolom + ' BranchCode\n';
	}
	// if (angular.isUndefined(inputExcelData.Branch)) {
	// 	$scope.cekKolom = $scope.cekKolom + ' Branch \n';
	// }
	if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
		$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
	}
	if (angular.isUndefined(inputExcelData.EffectiveDateTo)) {
		$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
	}
	if (angular.isUndefined(inputExcelData.Remarks)) {
		$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
	}

	if (
		// angular.isUndefined(inputExcelData.DealerType) || 
		// angular.isUndefined(inputExcelData.DealerName) ||
		// angular.isUndefined(inputExcelData.Province) ||
		angular.isUndefined(inputExcelData.PricingClusterCode) ||
		angular.isUndefined(inputExcelData.BranchCode) ||
		// angular.isUndefined(inputExcelData.Branch) ||
		angular.isUndefined(inputExcelData.EffectiveDateFrom) ||
		angular.isUndefined(inputExcelData.EffectiveDateTo) ||
		angular.isUndefined(inputExcelData.Remarks)) {
		return (false);
	}

	return (true);
}

$scope.ComboBulkAction_AreaPricing_Changed = function () {
	if ($scope.ComboBulkAction_AreaPricing == "Delete") {
		//var index;
		// $scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {
		// 	index = $scope.MasterSellingPriceFU_BBN_grid.data.indexOf(row.entity);
		// 	$scope.MasterSellingPriceFU_BBN_grid.data.splice(index, 1);
		// });
		var counter = 0;
		angular.forEach($scope.MasterSellingPriceFU_AreaPricing_gridAPI.selection.getSelectedRows(), function (data, index) {
			//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
			counter++;
		});

		if (counter > 0) {
			//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
			$scope.TotalSelectedData = counter;
				// angular.element('#AreaPricingModal').modal('show');
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
					function() {
						$scope.DeleteRow();
					},
					function() {
		
					}
				);
	
		}
	}
	}
	$scope.DeleteRow = function(){
		angular.forEach($scope.MasterSellingPriceFU_AreaPricing_gridAPI.selection.getSelectedRows(), function (data, index) {
			console.log("Cek Selected", data);
			$scope.MasterSellingPriceFU_AreaPricing_grid.data.splice($scope.MasterSellingPriceFU_AreaPricing_grid.data.lastIndexOf(data), 1);
		});
		$scope.ComboBulkAction_AreaPricing = "";
}

$scope.OK_Button_Clicked = function () {
	angular.forEach($scope.MasterSellingPriceFU_AreaPricing_gridAPI.selection.getSelectedRows(), function (data, index) {
		$scope.MasterSellingPriceFU_AreaPricing_grid.data.splice($scope.MasterSellingPriceFU_AreaPricing_grid.data.lastIndexOf(data), 1);
	});

	$scope.ComboBulkAction_AreaPricing = "";
	angular.element('#AreaPricingModal').modal('hide');
}
$scope.DeleteCancel_Button_Clicked = function () {
	$scope.ComboBulkAction_AreaPricing = "";
	angular.element('#AreaPricingModal').modal('hide');
}

$scope.MasterSelling_AreaPricing_Download_Clicked = function () {
	var excelData = [];
	var fileName = "";
	fileName = "MasterSellingAreaPricing";

	if ($scope.MasterSellingPriceFU_AreaPricing_grid.data.length == 0) {
		excelData.push({
			// DealerType : "",
			// DealerName : "",
			// Province: "",
			PricingClusterCode: "",
			BranchCode: "",
			Branch: "",
			EffectiveDateFrom: "",
			EffectiveDateTo: "",
			Remarks: ""
		});
		fileName = "MasterSellingAreaPricingTemplate";
	}
	else {

		for (var i = 0; i < $scope.MasterSellingPriceFU_AreaPricing_grid.data.length; i++) {

			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].PricingClusterCode == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].PricingClusterCode = " ";
			}
			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].BranchCode == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].BranchCode = " ";
			}
			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].Branch == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].Branch = " ";
			}
			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom = " ";
			}
			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo = " ";
			}
			if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].Remarks == null) {
				$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].Remarks = " ";
			}
		}


		$scope.MasterSellingPriceFU_AreaPricing_grid.data.forEach(function (row) {
			excelData.push({
				// DealerType : row.DealerType,
				// DealerName : row.DealerName,
				// Province: row.Province,
				PricingClusterCode: row.PricingClusterCode,
				BranchCode: row.BranchCode,
				Branch: row.Branch,
				EffectiveDateFrom: row.EffectiveDateFrom,
				EffectiveDateTo: row.EffectiveDateTo,
				Remarks: row.Remarks
			});
		});
	}
	console.log('isi nya ', JSON.stringify(excelData));
	console.log(' total row ', excelData[0].length);
	console.log(' isi row 0 ', excelData[0]);
	XLSXInterface.writeToXLSX(excelData, fileName);
}

$scope.MasterSelling_AreaPricing_Upload_Clicked = function () {
	if ($scope.MasterSelling_ExcelData.length == 0) {
		alert("file excel kosong !");
		return;
	}

	if (!$scope.validateColumn($scope.MasterSelling_ExcelData[0])) {
		alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
		return;
	}

	console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);

	for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
		var setan = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom.split('-');
		var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
		$scope.MasterSelling_ExcelData[i].EffectiveDateFrom = to_submit;
		console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

		var setan2 = $scope.MasterSelling_ExcelData[i].EffectiveDateTo.split('-');
		var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
		$scope.MasterSelling_ExcelData[i].EffectiveDateTo = to_submit2;
		console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);
	}

	var Grid;
	Grid = JSON.stringify($scope.MasterSelling_ExcelData);
	$scope.MasterSellingPriceFU_AreaPricing_gridAPI.grid.clearAllFilters();



	MasterPricingDealerToBranchAreaPricingFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
		function (res) {
			console.log('isi data', JSON.stringify(res));
			$scope.MasterSellingPriceFU_AreaPricing_grid.data = res.data.Result;			//Data hasil dari WebAPI

			var dataValid = undefined;
			for (i = 0; i < $scope.MasterSellingPriceFU_AreaPricing_grid.data.length; i++) {
				if ($scope.MasterSellingPriceFU_AreaPricing_grid.data[i].Remarks != "") {
					dataValid = false;
				}
			}

			if (dataValid == false) {
				bsNotify.show(
					{
						type: 'warning',
						title: "Data tidak valid!",
						content: "Cek detail kesalahan di kolom Remarks pada tabel",
					}
				);
			}

			$scope.MasterSellingPriceFU_AreaPricing_grid.totalItems = res.data.Total;
		}
	);
}

$scope.MasterSelling_Simpan_Clicked = function () {

	for (i = 0; i < $scope.MasterSellingPriceFU_AreaPricing_grid.data.length; i++) {
		var setan = $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom.split('-');
		var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
		$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom = to_submit;
		console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom);

		var setan2 = $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo.split('-');
		var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
		$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo = to_submit2;
		console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo);
	}

	var Grid;
	Grid = JSON.stringify($scope.MasterSellingPriceFU_AreaPricing_grid.data);

	console.log('ini data grid yg mau disimpan ====>', Grid);
	MasterPricingDealerToBranchAreaPricingFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
		function (res) {
			bsNotify.show({
				title: "Customer Data",
				content: "Data berhasil di simpan",
				type: 'success'
			});
			$scope.MasterSelling_AreaPricing_Generate_Clicked();
		},
		function (err) {
			bsNotify.show(
				{
					type: 'danger',
					title: "Data gagal disimpan!",
					content: err.data.Message.split('-')[1],
				}
			);
		}
	);

	//                        12/31/2019   ===> 0/1/2
	for (i = 0; i < $scope.MasterSellingPriceFU_AreaPricing_grid.data.length; i++) {
		var setan = $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom.split('/');
		var to_submit = "" + setan[1] + "-" + setan[0] + "-" + setan[2];
		$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom = to_submit;
		console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom);

		var setan2 = $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo.split('/');
		var to_submit2 = "" + setan2[1] + "-" + setan2[0] + "-" + setan2[2];
		$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo = to_submit2;
		console.log('$scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AreaPricing_grid.data[i].EffectiveDateTo);
	}
}

$scope.MasterSelling_Batal_Clicked = function () {
	$scope.MasterSellingPriceFU_AreaPricing_grid.data = [];
	$scope.MasterSellingPriceFU_AreaPricing_gridAPI.grid.clearAllFilters();
	$scope.TextFilterGrid = "";
	$scope.TextFilterDua_AreaPricing = "";
	$scope.TextFilterSatu_AreaPricing = "";
	var myEl = angular.element(document.querySelector('#uploadSellingFile_AreaPricing'));
	myEl.val('');
}


$scope.MasterSelling_AreaPricing_Cari_Clicked = function () {
	var value = $scope.TextFilterGrid_AreaPricing;
	$scope.MasterSellingPriceFU_AreaPricing_gridAPI.grid.clearAllFilters();
	if ($scope.ComboFilterGrid_AreaPricing != "") {
		$scope.MasterSellingPriceFU_AreaPricing_gridAPI.grid.getColumn($scope.ComboFilterGrid_AreaPricing).filters[0].term = value;
	}
	// else {
	// 	$scope.MasterSellingPriceFU_AreaPricing_gridAPI.grid.clearAllFilters();

}

});