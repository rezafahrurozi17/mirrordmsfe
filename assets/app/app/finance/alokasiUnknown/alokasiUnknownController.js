//import { EFAULT } from "constants";

/*
Edit by : 
	20170414, eric, edit for Alokasi Unknown swapping & ekspedisi
*/

var app = angular.module('app');
//20170425, eric, begin
//app.controller('AlokasiUnknownController', function ($scope, $http, CurrentUser, AlokasiUnknownFactory, $timeout) {
app.controller('AlokasiUnknownController', function ($scope, $http, CurrentUser, $filter, AlokasiUnknownFactory, $timeout, uiGridConstants, bsNotify) {
	//Dynamic Object model.
	$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy = {};
	$scope.parent = $scope;
	//20170425, eric, end
	$scope.PreprintedSettingData = [];
	$scope.optionsTambahAlokasiUnknown_DaftarSpk_Detail_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SPK", value: "Nomor SPK" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }];
	$scope.optionsTambahAlokasiUnknown_DaftarSpk_Brief_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SPK", value: "Nomor SPK" }];
	$scope.optionsTambahAlokasiUnknown_DaftarNoRangka_Search = [{ name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor SPK", value: "Nomor SPK" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nama di Billing", value: "Nama di Billing" }];
	$scope.optionsTambahAlokasiUnknown_DaftarWorkOrder_Search = [{ name: "Nomor WO", value: "Nomor WO" }, { name: "Tanggal WO", value: "Tanggal WO" }, { name: "Nomor Billing", value: "Nomor Billing" }, { name: "Nomor Polisi", value: "Nomor Polisi" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }];
	$scope.optionsTambahAlokasiUnknown_DaftarArCreditDebit_Search = [{ name: "Tanggal AR Card", value: "Tanggal AR Card" }, { name: "Tipe Kartu", value: "Tipe Kartu" }, { name: "Nominal", value: "Nominal" }];
	$scope.optionsLihatAlokasiUnknown_DaftarArCreditDebit_Search = [{ name: "Tanggal AR Card", value: "Tanggal AR Card" }, { name: "Tipe Kartu", value: "Tipe Kartu" }, { name: "Nominal", value: "Nominal" }];
	$scope.optionsTambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }];
	$scope.optionsTambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor Billing", value: "Nomor Billing" }];
	$scope.optionsTambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor Down Payment", value: "Nomor Down Payment" }];
	$scope.optionsLihatAlokasiUnknown_DaftarAlokasiUnknown_Search = [{ name: "Nomor Alokasi Unknown", value: "Nomor Alokasi Unknown" }, { name: "Tipe Alokasi Unknown", value: "Tipe Alokasi Unknown" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }, { name: "Nominal", value: "Nominal" }, { name: "Status Pengajuan", value: "Status Pengajuan" }];
	$scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }];
	$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }];
	//$scope.optionsLihatAlokasiUnknown_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	$scope.optionsTambahAlokasiUnknown_DaftarARRefundLeasing_Search = [{ name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor SPK", value: "Nomor SPK" }];
	$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKreditOptions = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
	$scope.optionsTambahAlokasiUnknown_DaftarInterbranch_Search = [{ name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Alokasi Unknown", value: "Nomor Alokasi Unknown" }, { name: "Tanggal Alokasi Unknown", value: "Tanggal Alokasi Unknown" }, { name: "Nominal", value: "Nominal" }];
	$scope.optionsTambahAlokasiUnknown_DaftarUnknown_Search = [{ name: "Tanggal Pembayaran", value: "Tanggal Pembayaran" }];
	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log($scope.ByteEnable);

	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	if ($scope.allowApprove == true)
		$scope.optionsLihatAlokasiUnknown_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	else
		$scope.optionsLihatAlokasiUnknown_DaftarBulkAction_Search = [];

	//20170517, nuse, begin
	$scope.Show_ShowLihatAlokasiUnknown = true;
	$scope.ShowAlokasiUnknownDataDetail = false;
	$scope.ShowAlokasiUnknownData = true;
	$scope.ShowAlokasiUnknownHeader = true;
	$scope.ShowAlokasiUnknownHeaderTambah = true;


	//20170517, nuse, end
	//tambah getdata untuk factory AlokasiUnknownFactory
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//20170430, NUSE EDIT, begin
	$scope.uiGridPageSize = 10;
	//20170430, NUSE EDIT, end
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		HideKuitansi();
		//   $scope.gridData=[];

	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mAlokasiUnknown = null; //Model
	//console.log('Result si Model', mfinanceAlokasiUnknownunitdp.ExampleData);
	$scope.xRole = { selected: [] };
	$scope.currentSPKId = 0;
	$scope.Cetak = "N";
	$scope.CetakTTUS = "N";
	$scope.CetakIPParentId = 0;
	$scope.currentNoSO = "";
	$scope.currentSODate = "";
	$scope.currentOutletId = 0;
	$scope.currentNominalBilling = 0;
	$scope.currentSisaPembayaran = 0;
	$scope.currentPaidNominal = 0;
	$scope.currentIPParentId = 0;
	//20170414, eric, begin
	$scope.currentCustomerId = 0;
	$scope.Pembayaran = 0;
	$scope.EnabledCetak = 1;
	$scope.TujuanPembayaran_CashDelivery = "";
	$scope.TambahAlokasiUnknown_NamaBranch_SearchDropdown = "";
	$scope.Preprinted = 0;
	//20170414, eric, end

	//interbranch info
	$scope.currentInterBranchId = 0;
	$scope.currentNamaBranchPenerima = "";
	$scope.currentNomorAlokasiUnknown = 0;
	$scope.currentNominalPembayaran = 0;
	$scope.currentNamaPelanggan = "";
	$scope.currentUntukPembayaran = "";
	$scope.currentTanggalBank = new Date();
	//hilman,begin
	$scope.selNoWO = "";
	$scope.selTglWo = "";
	$scope.selNoBilling = "";
	$scope.selNoPolisi = "";
	$scope.selNamaPelanggan = "";
	$scope.selTotalTerbayar = "";
	$scope.selSisaPembayaran = "";
	$scope.selBiayaMaterai = "";
	$scope.TotalBayarSum = 0;
	//hilman,end
	$scope.currentTglAR = "";
	$scope.currentCardType = "";
	$scope.currentNominal = 0;
	//Alvin, 20170520, begin
	$scope.currentUnknownId = 0;
	$scope.currentBankStatemntUnknownId = 0;
	$scope.currentApprovalTypeName = "";
	//Alvin, 20170520, end
	$scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown = new Date();
	$scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo = new Date();

	$scope.Customer_Data =
		{
			ProspectId: 0,
			CustomerId: 0,
			Name: "",
			Address: "",
			Phone: "",
			City: "",
		};

	var FormStateValue = 0;

	var FormState = {
		isDefault: 0,
		isSaved: 1,
		isUpdated: 2,
		isDeleted: 3
	}

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	var uiGridPageSize = 10;

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	//hilman,begin
	$scope.getDataServiceCustomerAndPayment = function (PageNum, SearchQry, SearchType) {
		AlokasiUnknownFactory.getDataServiceCustomerAndPayment(PageNum, uiGridPageSize, SearchQry, SearchType)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].NamaPelanggan;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].ToyotaId;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalWO = res.data.Result[0].TglWo;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = res.data.Result[0].TglAppointment;
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
						//console.log($scope.currentCustomerId);
						//console.log(res.data.Result[0].CustomerId);
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabupatenKota;
						//$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data=res.data.Result;
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.push({ Keterangan: "Down Payment", NoWO: res.data.Result[0].NoWO, NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP });
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.push({ Keterangan: "Own Risk", NoWO: res.data.Result[0].NoWO, NominalDP: res.data.Result[0].NominalOwnRisk, PaidDP: res.data.Result[0].TotalTerbayar, SisaDP: res.data.Result[0].SisaPembayaran });
						$scope.assignmentData = res.data.Result;
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
					}
					else {
						console.log("masuk3");
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = "";
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = "";
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = "";
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Handphone = "";
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Alamat = "";
						$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = "";
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
						//$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TambahAlokasiUnknown_AddItem = function () {
		console.log("TambahServiceFullPayment_AddItem");
		var found = 0;
		$scope.selectedRows = $scope.gridApi_InformasiPembayaran_UIGrid2.selection.getSelectedRows();
		console.log("selected=>",$scope.selectedRows);
		
		if ($scope.onSelectRows) {
			$scope.onSelectRows($scope.selectedRows);
			console.log("=============================>", $scope.gridApi_InformasiPembayaran_UIGrid2,$scope.selectedRows);
		}

		$scope.gridApi_InformasiPembayaran_UIGrid2.selection.getSelectedRows().forEach(function (row) {
			$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.NoWO);
				if (row.NoWO == obj.NoWO)
					found = 1;
			});
		});

			if (found == 0) {
				// $scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.push({ NoWO: $scope.selNoWO, WoNo: $scope.selWoNo, TglWo: $scope.selTglWo, NoBilling: $scope.selNoBilling, NoPolisi: $scope.selNoPolisi, NamaPelanggan: $scope.selNamaPelanggan, PaidNominal: $scope.selTotalTerbayar, SisaNominal: $scope.selSisaNominal, BiayaMaterai: $scope.selBiayaMaterai });
				for(var i in $scope.selectedRows){
					$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.push({
						NoWO: $scope.selectedRows[i].NoWO,
						WoNo: $scope.selectedRows[i].WoNo,
						TglWo: $scope.selectedRows[i].TglWo,
						NoBilling: $scope.selectedRows[i].NoBilling,
						NoPolisi: $scope.selectedRows[i].NoPolisi,
						NamaPelanggan: $scope.selectedRows[i].NamaPelanggan,
						PaidNominal: $scope.selectedRows[i].PaidNominal,
						SisaNominal: $scope.selectedRows[i].SisaNominal,
						BiayaMaterai: $scope.selectedRows[i].BiayaMaterai,
						IsPrintedKwitansi: $scope.selectedRows[i].IsPrintedKwitansi
					});
					$scope.TotalBayarSum = $scope.TotalBayarSum + $scope.selSisaPembayaran;
					$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran_Selected_Changed();
				}
			}
		
	};

	$scope.deleteRow = function (row) {
		var index = $scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TotalBayarSum = $scope.TotalBayarSum - row.entity.TotalTerbayar;
		$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.splice(index, 1);
		$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran_Selected_Changed();
	};

	$scope.getDataPaymentByCustName = function (PageNum, SearchQry, SearchType) {
		AlokasiUnknownFactory.getDataServiceCustomerAndPayment(PageNum, uiGridPageSize, SearchQry, SearchType)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						console.log("res data ==>> ", res.data.Result);
						for(var i in res.data.Result){
							res.data.Result[i].TglWo = $scope.changeFormatDate(res.data.Result[i].TglWo);
						}
						$scope.TambahAlokasiUnknown_DaftarWorkOrder_UIGrid.data = res.data.Result;
					}
					else {
						$scope.TambahAlokasiUnknown_DaftarWorkOrder_UIGrid.data = [];
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	//hilman, end

	//20170425, eric, begin
	$scope.getSalesOrdersBySONumber1 = function () {
		$scope.currentCustomerId = 0;
		AlokasiUnknownFactory.getSalesOrdersBySONumber($scope.TambahAlokasiUnknown_SearchBy_NomorSo, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;

						AlokasiUnknownFactory.getCustomersByCustomerId(res.data.Result[0].CustomerId)
							.then(
								function (res) {
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabKota;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].CustomerName;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].CustomerId;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = res.data.Result[0].TglAppointment;
									$scope.currentCustomerId = res.data.Result[0].CustomerId;
								},
								function (err) {
									console.log("err=>", err);
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Alamat = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Handphone = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = "";

								}
							);
					}
					$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}


	$scope.getSalesOrdersByCustomerName1 = function (Start, Limit) {
		$scope.currentCustomerId = 0;
		AlokasiUnknownFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerName1");
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.data = res.data.Result;
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersDibayarByCustomerName1 = function (Start, Limit) {
		//$scope.currentCustomerId = 0;

		AlokasiUnknownFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					for (i = 0; i < res.data.Result.length; i++) {
						res.data.Result[i]["LinkHapus"] = "<a>hapus</a>";
					};
					console.log("getSalesOrdersDibayarByCustomerName1");
					console.log(res);
					$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.totalItems = res.data.Total;
					$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}


	//20170425, eric, end


	//20170414, eric, begin
	$scope.getSalesOrdersBySONumber = function () {
		$scope.currentCustomerId = 0;
		AlokasiUnknownFactory.getSalesOrdersBySONumber($scope.TambahAlokasiUnknown_SearchBy_NomorSo, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;

						AlokasiUnknownFactory.getCustomersByCustomerId(res.data.Result[0].CustomerId)
							.then(
								function (res) {
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
									$scope.currentCustomerId = res.data.Result[0].CustomerId;
								},
								function (err) {
									console.log("err=>", err);
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Alamat = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaPt = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Telepon = "";

								}
							);
					}
					$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersByCustomerNameExt = function (Start, Limit) {
		$scope.currentCustomerId = 0;
		AlokasiUnknownFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerNameExt");
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_SoSoSetColumns();
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersByCustomerName = function (Start, Limit) {
		$scope.currentCustomerId = 0;
		AlokasiUnknownFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahAlokasiUnknown_SearchBy_NamaPelangganPt, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerName");
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.data = res.data.Result;
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.submitDataFinanceAlokasiUnknowns = function (data) {
		console.log(data);
		var SimpanOK = 1;
		// angular.forEach(data[0].SOList, function (value, key) {
		// 	if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown != "Service - Down Payment") {
		// 		if (value.PaidAmount == 0 || value.PaidAmount == "") {
		// 			bsNotify.show({
		// 				title: "Warning",
		// 				content: "Rincian pembayaran belum dipilih",
		// 				type: 'warning'
		// 			});
		// 			SimpanOK = 0;
		// 		}
		// 	}
		// });

		// if (SimpanOK == 1) {
			//handle kirim customerid ""
			if (data[0].CustomerId == ""){
				data[0].CustomerId = 0;
			}
			AlokasiUnknownFactory.submitDataFinanceIncomingPayments(data)
				.then
				(
				function (res) {
					console.log("Dalama ==>>", res.data.Result[0].IPParentId);
					$scope.CetakIPParentId = res.data.Result[0].IPParentId;
					bsNotify.show({
						title: "Berhasil",
						content: "Pengajuan berhasil",
						type: 'success'
					});
					$scope.LoadingSimpan = false;
					if ($scope.Cetak == "Y") {
						$scope.CetakKuitansi();
					}
					else if ($scope.CetakTTUS == "Y") {
						$scope.CetakKuitansiTTUS();
					}
					$scope.TambahAlokasiUnknown_Batal();
					$scope.GetListUnknown(1, uiGridPageSize);
					FormState = 1;
					
					$scope.getDataPreprintedCB();

					
					$scope.Show_ShowLihatAlokasiUnknown = true;
					$scope.Show_ShowTambahAlokasiUnknown = false;
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) {
						bsNotify.show({
							title: "Notifikasi",
							content: err.data.Message,
							type: 'danger'
						});
						$scope.LoadingSimpan = false;	
						return false;
					}
				}
				)
		// }
		// else {
		// 	return false;
		// }
	};

	$scope.getDataInterBranch = function (Start, Limit) {
		console.log(Limit);
		AlokasiUnknownFactory.getInterBranch(Start, Limit, "")
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	//20170414, eric, end


	$scope.AlokasigetDataBySalesName = function () {
		AlokasiUnknownFactory.getDataBySalesName($scope.TambahAlokasiUnknown_SearchBy_NamaSalesman, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					$scope.TambahAlokasiUnknown_DaftarSpk_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
					$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
					$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
					//$scope.loading=false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataByNomorSPK = function (NomorSPK) {
		console.log("getDataByNomorSPK");
		AlokasiUnknownFactory.getDataByNomorSPK(NomorSPK, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = res.data.Result;

						console.log(res.data.Result.length);
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.totalItems = res.data.Result.length;
						console.log($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.totalItems);
						$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
						$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
						$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;

						//CR4
						$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data;

						//$scope.getDataProspectByCustomerId(res.data.Result[0].CustomerId);
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
					}
					$scope.loading = false;
				},
				function (err) {
					$scope.currentSPKId = 0;
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataAlokasiUnknownDetail = function () {
		AlokasiUnknownFactory.getDataAlokasiUnknownDetail()
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}

	// $scope.getDataBookingFeeByNomorSPK = function(NomorSPK) {
	//     AlokasiUnknownFactory.getDataByNomorSPK(NomorSPK)
	//     .then(
	//         function(res){
	//         	gridData = [];
	// $scope.grid.data = res.data.Result;
	//             $scope.loading=false;
	//         },
	//         function(err){
	//             console.log("err=>",err);
	//         }
	//     );

	// }
	$scope.getDataProspectByCustomerId = function (CustomerId) {
		AlokasiUnknownFactory.getProspectByCustomerId(CustomerId)
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	$scope.submitData = function (data) {
		AlokasiUnknownFactory.submitData(data)
			.then(
				function (res) {
					//$scope.loading=false;
					console.log(res);

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataBankAccountCashDelivery = function () {
		AlokasiUnknownFactory.getDataBankAccountComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahAlokasiUnknown_TujuanPembayaran_CashDeliveryOptions = res.data;
					console.log($scope.TujuanPembayaran_CashDelivery);
					$scope.TambahAlokasiUnknown_CashDelivery_NamaBank = $scope.TujuanPembayaran_CashDelivery;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataBankAccount = function (CustomerId) {
		AlokasiUnknownFactory.getDataBankAccountComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerimaOptions = res.data;
					$scope.TambahAlokasiUnknown_RincianPembayaran_NoRekeningPenerima_BankTransfer = $scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerimaOptions[0];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataBank = function () {
		AlokasiUnknownFactory.getDataBankComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions = res.data;
					$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions[0];
					$scope.TambahAlokasiUnknown_CreditDebitCard_NamaBankOptions = res.data;
					$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBank_CreditDebitCard = $scope.TambahAlokasiUnknown_CreditDebitCard_NamaBankOptions[0];
					$scope.optionsTambahAlokasiUnknown_SearchBy_NamaBank = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	//Alvin, begin
	$scope.getDataBranchCB = function () {
		AlokasiUnknownFactory.getDataBranchGroupComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsTambahAlokasiUnknown_NamaBranchGroup_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataPreprintedCB = function () {
		AlokasiUnknownFactory.getDataPreprintedComboBox()
			.then(
				function (res) {
					console.log("data combobox =>>", res);
					$scope.loading = false;
					$scope.TambahAlokasiUnknown_NomorKuitansiOption = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getNominalMaterai = function (PaymentAmount) {
		AlokasiUnknownFactory.getMaterai(PaymentAmount)
			.then(
				function (res) {
					//$scope.loading = false;
					console.log("getNominalMaterai2");
					console.log(res);
					console.log(res.data.Result[0]);
					console.log(res.data.Result[0].value);
					$scope.TambahAlokasiUnknown_BiayaBiaya_Nominal = res.data.Result[0].value;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataCharges = function () {
		AlokasiUnknownFactory.getDataCharges()
			.then(
				function (res) {
					$scope.loading = false;
					//$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataPreprinted = function () {
		console.log("getDataPreprinted2");
		console.log($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown);
		AlokasiUnknownFactory.getDataPreprinted($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					$scope.loading = false;
					$scope.Preprinted = res.data.Result[0].isPreprintedReceipt;
					console.log("Preprinted = ", $scope.Preprinted);
					//$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = res.data;
					$scope.getDataPreprintedCB();

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataVendor = function () {
		console.log("sini");
		AlokasiUnknownFactory.getDataVendorComboBox()
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahAlokasiUnknown_Top_NamaLeasingOption = res.data;
				//$scope.LihatAlokasiUnknown_Top_NamaLeasingOption = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}

	$scope.getDataLeasing = function () {
		console.log("sini");
		AlokasiUnknownFactory.getLeasingComboBox()
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahAlokasiUnknown_Top_NamaLeasingOption = res.data;
				//$scope.LihatAlokasiUnknown_Top_NamaLeasingOption = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}
	//Alvin, end

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function (rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function (rows) {
		console.log("onSelectRows=>", rows);
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------

	//20170425, eric, begin
	$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,

		showGridFooter: true,
		showColumnFooter: true,
		enableFiltering: true,

		enableSorting: false,
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
			{ name: 'Biaya Materai', field: 'Materai', visible: false },
			{ name: 'Total Terbayar', field: 'PaidNominal', type: 'number', enableCellEdit: true, cellFilter: 'rupiahC' },
			{ name: 'Saldo', field: 'SisaPembayaran', aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', enableCellEdit: true, cellFilter: 'rupiahC' },
			{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRowSalesOrderDibayar(row)">Delete</button>' },
			//{ name: 'Action', field:'LinkHapus'},
			//{ name: 'Hyperlink', cellTemplate:'<a ng-click="grid.appScope.deleteSalesOrderDibayar(row.entity)">hapus</a>'}
		],
		onRegisterApi: function (gridApi) {
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			// 	if (row.isSelected == false) {
			// 		$scope.currentCustomerId = 0;
			// 		$scope.currentSPKId = 0;
			// 	}
			// 	else {
			// 		$scope.currentCustomerId = row.entity.CustomerId;
			// 		$scope.currentSPKId = row.entity.SPKId;
			// 	}
			// });
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersDibayarByCustomerName1(pageNumber, pageSize);
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},

	};

	$scope.deleteRowSalesOrderDibayar = function (row) {
		var index = $scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.splice(index, 1);
		//$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran_Selected_Changed();
	};


	//20170425, eric, end	
	// 20170414, eric, begin

	//grid ini digunakan untuk Search SO by SO Number
	$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid = {
		enableSorting: false,
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: false },
			{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
			//{ name: 'Biaya Materai', field: 'Materai', visible:false },
			//{ name: 'Total Yang Harus Dibayar', field: 'TotalNominal' },
			{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
			{ name: 'Saldo', field: 'SisaPembayaran', cellFilter: 'rupiahC' },

		]
	};

	//grid ini digunakan untuk Search SO by Customer Name
	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentCustomerId = 0;
					$scope.currentSPKId = 0;
				}
				else {
					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSONumber = row.entity.SONumber;
					console.log($scope.currentCustomerId);
					AlokasiUnknownFactory.getSalesOrdersBySONumber($scope.currentSONumber, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
						.then(
							function (res) {
								console.log("masuk1");
								console.log($scope.currentCustomerId);
								$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
								console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.totalItems);
								$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.totalItems = res.data.Total;
								console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.totalItems);
							},
							function (err) {
								console.log("err=>", err);
							}
						);
					console.log($scope.currentCustomerId);
					AlokasiUnknownFactory.getCustomersByCustomerId($scope.currentCustomerId)
						.then(
							function (res) {
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
								$scope.currentCustomerId = res.data.Result[0].CustomerId;
							},
							function (err) {
								console.log("err=>", err);
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = "";
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Alamat = "";
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = "";
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = "";
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaPt = "";
								$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Telepon = "";

							}
						);
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersByCustomerName(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'NomorSO', field: 'SONumber' },
			{ name: 'Model', field: 'Model' },
			{ name: 'Tipe', field: 'Tipe' },
			{ name: 'Warna', field: 'Warna' },
		]
	};
	// 20170414, eric, end

	$scope.AlokasiUnknownSort = {
		Date: function(a, b) {
			a = new Date(a);
			b = new Date(b);
			if (a < b) {
				return -1;
			}
			else if (a > b) {
				return 1;
			}
			else {
				return 0;
			}
		},

		Nominal: function(a, b) {
			a = parseFloat(a);
			b = parseFloat(b);
			if (a < b) {
				return -1;
			}
			else if (a > b) {
				return 1;
			}
			else {
				return 0;
			}
		}
	};

	//20170511, Alvin, begin, grid Lihat
	$scope.tmpFlag = false;
	$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid = {

		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,
		paginationPageSizes: [10, 25, 50],
		//paginationPageSize: 10,

		enableSorting: true,
		onRegisterApi: function (gridApi) {
			$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIPParentId = 0;
				}
				else {
					$scope.currentUnknownId = row.entity.UnknownId;
					$scope.currentIPParentId = row.entity.IPParentId;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$timeout(function(){
					if($scope.tmpFlag == false){
						$scope.GetListUnknownCari(pageNumber, pageSize);
					}
				}, 100);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
			gridApi.core.on.renderingComplete($scope, function () {
				// $scope.SetMainGridADH();

				$scope.GetDataPreprintedSetting();

				console.log('Form State => ' + FormStateValue);
				if (FormStateValue == FormState.isDefault) {
					$scope.SetMainGridADH();
				}
				else {
					$scope.GetListIncoming(1, uiGridPageSize);
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal Alokasi Unknown', field: 'TanggalUnknown', cellFilter: 'date:\"dd/MM/yyyy\"', width:'10%', 'sortingAlgorithm': $scope.AlokasiUnknownSort.Date },
				{ name: 'UnknownId', field: 'UnknownId', visible: false , width:'10%' },
				{ name: 'Nomor Incoming', field: 'IPParentId', visible: false, width:'10%'  },
				{ name: 'Nomor Alokasi Unknown', field: 'UnknownNo', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailIncoming(row)">{{row.entity.UnknownNo}}</a>' , width:'10%' },
				{ name: 'Nomor Alokasi Unknown Rev', field: 'UnknownRev', visible: false , width:'10%' },
				{ name: 'Tipe Alokasi Unknown', field: 'UnknownPaymentType', visible: true , width:'10%' },
				{ name: 'Nama Pelanggan', field: 'CustomerName' , width:'10%' },
				{ name: 'Metode Bayar', field: 'PaymentMethod', visible: false, width:'10%'  },
				{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' , width:'10%', 'sortingAlgorithm': $scope.AlokasiUnknownSort.Nominal },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan' , width:'10%' },
				{ name: 'Tanggal Pengajuan', field: 'TglPengajuan', cellFilter: 'date:\"dd/MM/yyyy\"', width:'10%', 'sortingAlgorithm': $scope.AlokasiUnknownSort.Date },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan' , width:'10%' },
				{ name: 'Alasan Pengajuan', field: 'AlasanPengajuan', width:'10%'  },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan', width:'10%'  },
				{ name: 'History Lost', field: 'HistoryLost', visible: false }
			]
	};


	$scope.LihatAlokasiUnknown_RincianPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Metode Pembayaran', field: 'PaymentMethod', visible: true, width:'10%'  },
					{ name: 'Tanggal Pembayaran', field: 'PaymentDate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"', width:'10%'  },
					{ name: 'Nama Bank Pengirim/Credit/Debit Card', field: 'SenderBankId', width:'10%'  },
					{ name: 'Nama Pengirim', field: 'SenderName', width:'10%'  },
					{ name: 'Nomor Cheque/Credit/Debit Card', field: 'NoCheque', width:'10%'  },
					{ name: 'Nomor Rekening Penerima', field: 'ReceiverAccNumber', width:'10%'  },
					{ name: 'Nama Bank Penerima', field: 'BankPenerima', width:'10%'  },
					{ name: 'Nama Penerima', field: 'Penerima', visible: false , width:'10%' },
					{ name: 'Nama Pemegang Rekening', field: 'NamaPemegangRek', width:'10%'  },
					{ name: 'Keterangan', field: 'Keterangan', width:'10%'  },
					{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC', width:'10%'  }
				]
		};

	$scope.LihatAlokasiUnknown_DaftarAR_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			onRegisterApi: function (gridApi) { $scope.LihatAlokasiUnknown_DaftarAR_gridAPI = gridApi; },
			columnDefs:
				[
					{ name: 'BankStatementCardId', field: 'BankStatementCardId', visible: false },
					{ name: 'Tanggal AR Card', displayName: 'Tanggal AR Card', field: 'ARDate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: 'Tipe Kartu', field: 'CardType', visible: true },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_Biaya_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tipe Biaya', field: 'OtherChargesType', visible: true },
					{ name: 'Debit / Kredit', field: 'DebitCreditType' },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false},
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true  },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"'},
					{ name: 'Nomor Billing', field: 'NoBilling', visible: true  },
					{ name: 'Nomor Polisi', field: 'NoPolisi', visible: true  },
					{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC'  },
					{ name: 'Pembayaran', field: 'PaidNominal', cellFilter: 'rupiahC'  },
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false  },
					{ name: 'Total Yang Harus Dibayar', field: 'Nominal Billing', cellFilter: 'rupiahC', visible: false  },
					{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC'  }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_WoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false, width:'10%'  },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true, width:'10%'  },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' , width:'10%' },
					{ name: 'Nominal Down Payment', field: 'NominalDP', visible: true, cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'Own Risk', field: 'OwnRisk', visible: false , width:'10%' },
					{ name: 'Biaya Materai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false, width:'10%'  },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' , width:'10%' }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_WoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false, width:'10%'  },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true, width:'10%'  },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"', width:'10%'  },
					{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'rupiahC', visible: false, width:'10%'  },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC', width:'10%'  },
					{ name: 'Sisa Pembayaran', field: 'SisaNominal', cellFilter: 'rupiahC', width:'10%'  }

				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_Appointment_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Appointment', field: 'NoAppointment', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_Materai_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', field: 'SOId', visible: false },
					{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SO', field: 'SOId', visible: false },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaNominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tgl SO', displayName: 'Tanggal SO', field: 'SODate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: 'SOId', field: 'SOId', visible: false },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_DaftarSalesOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SO', field: 'SOId', visible: false },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: 'Tanggal SO', field: 'SODate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Terbayar', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_DaftarSalesOrder_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: 'Nominal Billing', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
					{ name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatAlokasiUnknown_InformasiPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SPK', field: 'SPKId', visible: false },
					{ name: 'Tanggal SPK', displayName: 'Tanggal SPK', field: 'TanggalSPK', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', cellFilter: 'currency:"":0.00' }
				]
		};

	$scope.LihatAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Rangka', field: 'NoRangka', visible: true },
					{ name: 'Nomor SPK', field: 'NoSPK', visible: true },
					{ name: 'Nomor SO', field: 'NoSO', visible: true },
					{ name: 'Nominal', field: 'Nominal' },
					{ name: 'Biaya Materai', field: 'Materai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'Total' }
				]
		};

	//20170511, Alvin, end, grid Lihat

	//20170520, alvin, begin
	$scope.getListUnusedUnknown = function (Start, Limit) {
		var newString  =  "";
			if($scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text != '' && $scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text != undefined &&
				$scope.TambahAlokasiUnknown_DaftarUnknown_Search != undefined){
				newString = $scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-')
			}else{
				newString ="undefined";
			}
			console.log("replace =>>", newString);

		AlokasiUnknownFactory.GetListUnusedUnknown(Start, Limit, newString)
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.data = res.data.Result;
				$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.totalItems = res.data.Total;
				$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.paginationPageSize = Limit;
				$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.paginationPageSizes = [10, 25, 50];
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}

	$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid =
		{
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			enableFiltering: false,
			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Metode Pembayaran', field: 'MetodeBayar' },
					{ name: 'Id', field: 'BankStatementUnknownId', visible: false },
					{ name: 'Tanggal Pembayaran', field: 'PaymentDate', visible: true },
					{ name: 'Nama Bank Pengirim/Credit/Debit Card', field: 'SenderBankId' },
					{ name: 'Nama Pengirim', field: 'SenderName' },
					{ name: 'Nomor Cheque/Credit/Debit Card', field: 'NoCheque' },
					{ name: 'Nomor Rekening Penerima', field: 'AccountBankNo' },
					{ name: 'Nama Bank Penerima', field: 'AccountBankName' },
					{ name: 'Nama Pemegang Rekening', field: 'AccountName' },
					{ name: 'Keterangan', field: 'Description' },
					{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' }
				],
			onRegisterApi: function (gridApi) {
				$scope.TambahAlokasiUnknown_DaftarUnknown_gridAPI = gridApi;
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					if (row.isSelected == false) {
						$scope.currentBankStatemntUnknownId = 0;
					}
					else {
						$scope.currentBankStatemntUnknownId = row.entity.BankStatementUnknownId;
						$scope.Pembayaran = row.entity.Nominal;
					}
				});
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					$scope.getListUnusedUnknown(pageNumber, pageSize);
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);
				});
			}
		};

		// CR4 Norman
		// $scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.onRegisterApi = function (gridApi) {
        //     //set gridApi on scope
        //     $scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid = gridApi;
        //     gridApi.selection.on.rowSelectionChanged($scope, function (row) {
        //         $scope.selectedRow_DaftarUnknown_UIGrid = gridApi.selection.getSelectedRows();
                
        //     });
        //     gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
        //         $scope.selectedRow_DaftarUnknown_UIGrid = gridApi.selection.getSelectedRows();
        //     });
        // };
		
		// End CR4 Norman

	//20170520, alvin, end	

	//20170511, Alvin, begin, grid UnknownCards
	$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahAlokasiUnknown_DaftarArCreditDebit_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentTglAR = $scope.TambahAlokasiUnknown_TanggalAlokasiUnknown;
					$scope.currentCardType = "";
					$scope.currentNominal = 0;
				}
				else {
					$scope.currentTglAR = row.entity.PaymentDate;
					console.log($scope.currentTglAR);
					$scope.currentCardType = row.entity.CardType;
					$scope.currentNominal = row.entity.Nominal;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.GetListUnknownCards(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tgl AR Cards', displayName: 'Tanggal AR Cards', field: 'PaymentDate', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Tipe Kartu', field: 'CardType' },
				{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
			]
	};
	//20170511, Alvin, end, grid UnknownCards

	// 20170414, marthin, start
	//grid ini digunakan untuk Search SO by Customer Name
	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_GridApi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentCustomerId = 1;
					$scope.currentSONumber = 0;
					DisableAll_TambahAlokasiUnknown_InformasiPelanggan();
				}
				else {
					//Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();

					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSONumber = row.entity.SONumber;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSOId = row.entity.SOId;

					AlokasiUnknownFactory.getCustomersByCustomerId($scope.currentCustomerId)
						.then
						(
						function (res) {
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].CustomerCode;
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
							$scope.currentCustomerId = res.data.Result[0].CustomerId;
						},
						function (err) {
							console.log("err=>", err);
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = "";
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_KabupatenKota = "";
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NamaPelangganBranch = "";
							$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_HandphoneTelepon = "";
						}
						);

					AlokasiUnknownFactory.getSalesOrdersBySONumber($scope.currentSONumber, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
						.then(
							function (res) {
								$scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data = res.data.Result;
								console.log("TotalItems ===>", $scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.totalItems);
								$scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.totalItems = res.data.Total;

							},
							function (err) {
								console.log("err=>", err);
							}

						);

					Enable_TambahAlokasiUnknown_InformasiPembayaran_Materai();
				}
			});
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// $scope.getSalesOrdersByCustomerName(pageNumber,pageSize);

			// console.log("pageNumber ==>", pageNumber);
			// console.log("pageSize ==>", pageSize);
			// });
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', displayName: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'SOId', displayName: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
		]
	};

	$scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid = {
		enableSorting: false,
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi; },
		enableRowSelection: false,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'SOId', field: 'SOId', visible: false },
				{ name: 'Biaya Materai', field: 'Materai', visible: false },
				{ name: 'Total yg harus dibayar', field: 'TotalNominal', visible: false },
				{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
				{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' },
			]
	};

	$scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid = {
		enableSorting: false,
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi; },
		enableRowSelection: false,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
				//{ name: 'Biaya Materai', field: 'Materai', cellFilter: 'rupiahC'},
				//{ name: 'Total yg harus dibayar', field: 'TotalNominal', cellFilter: 'rupiahC' },
				{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
				{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
			]
	};


	$scope.getSalesOrderBySONumberParts = function () {
		AlokasiUnknownFactory.getSalesOrdersBySONumber($scope.TambahAlokasiUnknown_SearchBy_NomorSo, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
						$scope.currentSPKId = res.data.Result[0].SPKId;
						$scope.currentSOId = res.data.Result[0].SOId;
						AlokasiUnknownFactory.getCustomersByCustomerId($scope.currentCustomerId)
							.then(
								function (res) {
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].CustomerCode;
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
									$scope.currentCustomerId = res.data.Result[0].CustomerId;
								},
								function (err) {
									console.log("err=>", err);
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_KabupatenKota = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_NamaPelangganBranch = "";
									$scope.TambahAlokasiUnknown_InformasiPelangan_PelangganBranch_HandphoneTelepon = "";

								}

							);
					}
					if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
						$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					} else {
						$scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data = res.data.Result;
					}

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}


	// $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_button_SearchBy = function() {
	// console.log("SoSo");
	// AlokasiUnknownFactory.getSalesOrdersByCategory($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_SearchDropdown, $scope.inputTambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search)
	// .then(
	// function(res) {
	// $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.data = res.data.Result;
	// $scope.TambahAlokasiUnknown_SoSoSetColumns();
	// console.log("TotalItems ===>",$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
	// $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems = res.data.Total;				
	// $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSize = 10;
	// $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSizes = [10];
	// }, function(err) {
	// console.log("err=>", err);
	// }

	// );
	// }	
	// 20170414, marthin, end

	//20170425, eric, begin
	//grid ini digunakan untuk Search SO by Customer Name
	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentCustomerId = 0;
					$scope.currentSPKId = 0;
					$scope.currentNoSO = "";
					$scope.currentSONumber = "";
					$scope.currentNominalBilling = 0;
					$scope.currentSODate = "";
					$scope.currentPaidNominal = 0;
					$scope.currentSisaPembayaran = 0;
				}
				else {
					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSONumber = row.entity.SONumber;
					$scope.currentNoSO = row.entity.SOId;
					$scope.currentNominalBilling = row.entity.TotalNominal;
					$scope.currentPaidNominal = row.entity.PaidNominal;
					$scope.currentSisaPembayaran = row.entity.SisaPembayaran;
					$scope.currentSODate = row.entity.SODate;
					console.log($scope.currentNoSO);
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersByCustomerName1(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', displayName: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'SOId', displayName: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
			{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
			{ name: 'Saldo', field: 'SisaPembayaran', cellFilter: 'rupiahC' },
		]
	};
	//20170425, eric, end

	//20170506, Alvin,begin	
	$scope.TambahPartsFullPayment_AddItem = function () {
		console.log("TambahPartsFullPayment_AddItem");
		var found = 0;
		console.log('Cek Selected Jumlah==>', $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.selection.getSelectedRows());
		$scope.selectedRows = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.selection.getSelectedRows();
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.selection.getSelectedRows().forEach(function (row) {
			$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.SOId);
				if (row.SOId == obj.SOId)
					found = 1;
			});
		});
		// Tidak Bisa Tambah SO yg sama
		if (found == 0) {
			// $scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.push({ SOId: $scope.currentNoSO, SONumber: $scope.currentSONumber, NominalBilling: $scope.currentNominalBilling, SODate: $scope.currentSODate, TotalNominal: $scope.currentNominalBilling, PaidNominal: $scope.currentPaidNominal, SisaPembayaran: $scope.currentSisaPembayaran })
			$timeout(function(){
				for(var i in $scope.selectedRows){
					$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.push({ 
						SOId: $scope.selectedRows[i].SOId, 
						SONumber: $scope.selectedRows[i].SONumber, 
						NominalBilling: $scope.selectedRows[i].NominalBilling, 
						SODate: $scope.selectedRows[i].SODate, 
						TotalNominal: $scope.selectedRows[i].TotalNominal, 
						PaidNominal: $scope.selectedRows[i].PaidNominal, 
						SisaPembayaran: $scope.selectedRows[i].SisaPembayaran 
					});
				}
			},100);
		}
	};
	//20170506, Alvin, end



	$scope.TambahAlokasiUnknown_DaftarSpk_UIGrid = {
		enableSorting: false,
		enableFiltering: true,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarSpk = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				//$scope.TambahAlokasiUnknown_DaftarSpk_UIGrid.data=res.data.Result;
				if (row.isSelected == false) {
					$scope.currentSPKId = 0;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = "";
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = "";
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = "";
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = "";
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = "";
					$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
					$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
				}
				else {
					$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
					$scope.getDataByNomorSPK(row.entity.NomorSPK);
					$scope.currentSPKId = row.entity.NomorSPK;
					$scope.pilihNoSPK = row.entity;
					//$scope.getDataProspectByCustomerId(row.entity.CustomerId);
				}
			});

			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		multiselect: false,
		columnDefs: [
			//{ name:'CustomerId',    field:'CustomerId', visible:false},
			{ name: 'Nomor SPK', displayName: "Nomor SPK", field: 'NomorSPK' },
			{ name: 'Nama Pelanggan', field: 'CustomerName' },
			{ name: 'Handphone', field: 'Handphone' },
			{ name: 'Model', field: 'Model' },
			{ name: 'Tipe', field: 'Tipe' },
			{ name: 'Warna', field: 'Warna' },
			//{ name:'NamaSalesman',    field:'NamaSalesman'},
			//{ name:'NominalBookingFee',    field:'NominalBookingFee'},
			//{ name:'SPKId',    field:'SPKId', visible:false}
			//{ name:'SalesId',    field:'SalesId'},
			//{ name:'SalesName',    field:'SalesName'},
		]
	};

	$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid = {
		enableSorting: false,
		onRegisterApi: function (gridApi) { 
			$scope.gridApi_InformasiPembayaran = gridApi; 
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment"){
					console.log("Test masuk Sini Unit DP")
					console.log("test", rowEntity);
					console.log("colDef ", colDefs);
					if(newValue > rowEntity.SisaDP){
						var newValue = 0;   
						rowEntity[colDefs.name] = newValue;
							bsNotify.show(
								{
									title: "Peringatan",
									content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
									type: 'warning'
								}
							); 
					}
				}
			});
		},
		enableRowSelection: false,
		enableSelectAll: false,
		showGridFooter: true,
		showColumnFooter: true,
		//Alvin, 20170506 begin		
		columnDefs: [
			//hilman, begin
			//{ name:'Tanggal Spk',    field:'Tanggal',cellFilter: 'date:\'yyyy-MM-dd\''},
			{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'TglSPK', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
			{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', visible: false },
			{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
			{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'' },
			{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
			{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
			{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
			{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
			{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
			{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
			//hilman, end
		]
		//Alvin, 20170506 end					
	};
	//hilman,begin

	$scope.TambahAlokasiUnknown_DaftarWorkOrder_UIGrid = {
		enableColumnResizing: true,
		enableSorting: true,
		enableFiltering: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_InformasiPembayaran_UIGrid2 = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.selNoWO = row.entity.NoWO;
					$scope.selWoNo = row.entity.WoNo;
					$scope.selTglWo = row.entity.TglWo;
					$scope.currentCustomerId = row.entity.CustomerId
					$scope.selNoBilling = row.entity.NoBilling;
					$scope.selNoPolisi = row.entity.NoPolisi;
					$scope.selNamaPelanggan = row.entity.NamaPelanggan;
					$scope.selTotalTerbayar = row.entity.PaidNominal;
					$scope.selSisaPembayaran = row.entity.SisaPembayaran;
					$scope.selSisaNominal = row.entity.SisaNominal;
					$scope.selBiayaMaterai = row.entity.BiayaMaterai;
				}
				else {
					$scope.selNoWO = "";
					$scope.selWoNo = "";
					$scope.selTglWo = "";
					$scope.currentCustomerId = "";
					$scope.selNoBilling = "";
					$scope.selNoPolisi = "";
					$scope.selNamaPelanggan = "";
					$scope.selTotalTerbayar = "";
					$scope.selSisaPembayaran = "";
					$scope.selSisaNominal = "";
					$scope.selBiayaMaterai = "";

				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'WOId', displayName: 'WOId', field: 'NoWO', visible: false },
				{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoNo' },
				{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'' },
				{ name: 'Nomor Billing', field: 'NoBilling' },
				{ name: 'Nomor Polisi', field: 'NoPolisi' },
				{ name: 'Nama Pelanggan', field: 'NamaPelanggan' },
				{ name: 'Nominal Terbayar (termasuk OR)', displayName: 'Nominal Terbayar (termasuk OR)', field: 'PaidNominal', cellFilter: 'rupiahC' },
				{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC' },
				{ name: 'Biaya Materai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
			]
	};

	$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid = {
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			 $scope.gridApi_InformasiPembayaran = gridApi; 

			 gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				// var test = Math.floor(newValue);
				var test = newValue.toFixed();
				var Newint = parseInt(test);
				console.log("cek decimal", Newint)
				console.log("cek decimal2", test)
				console.log("test", rowEntity);
				console.log("colDef ", colDefs);
				rowEntity.Pembayaran = Newint;
				if(Newint > rowEntity.SisaNominal){
					var Newint = 0;   
					rowEntity[colDefs.name] = Newint;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
								type: 'warning'
							}
						);  
				}else if(Newint <= 0){
					var Newint = 0;
						rowEntity[colDef.name] = Newint;   
						bsNotify.show(
								{
										title: "Peringatan",
										content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
										type: 'warning'
								}
						);      
				}
			});
		},
		enableRowSelection: false,
		enableSelectAll: false,
		showColumnFooter: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: 'WOId', displayName: 'WOId', field: 'NoWO', visible: false },
			{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoNo', enableCellEdit: false },
			{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false },
			{ name: 'Nomor Billing', field: 'NoBilling', enableCellEdit: false },
			{ name: 'Nomor Polisi', field: 'NoPolisi', enableCellEdit: false },
			{ name: 'Nama Pelanggan', field: 'NamaPelanggan', enableCellEdit: false },
			{ name: 'Nominal Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC', enableCellEdit: false },
			{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'currency:"":0', enableCellEdit: false },
			{ name: 'Biaya Materai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
			{
				name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				},
				enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0',
				// footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.PembayaranAll | number}}</div>',
				
			},
			//{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRow(row)">Hapus</button>'},
			{ name: 'Action', cellTemplate: '<a ng-click="grid.appScope.deleteRow(row)">Hapus</a>' },
		]
	};
	//hilman,end

	$scope.TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_Interbranch_UIGrid = {
		enableSorting: false,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_Interbranch = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					// //20170414, eric, begin
					console.log(row.entity.InterbranchId);
					$scope.currentInterBranchId = row.entity.InterbranchId;
					$scope.currentNamaBranchPenerima = "";
					$scope.currentNomorAlokasiUnknown = "";
					//$scope.currentTanggalBank = row.entity.TanggalBank;
					$scope.currentTanggalBank = row.entity.Date;
					$scope.currentNominalPembayaran = row.entity.Nominal;
					//$scope.currentNamaPelanggan = row.entity.NamaPelanggan;
					$scope.currentNamaPelanggan = row.entity.CustomerName;
					//$scope.currentUntukPembayaran = row.entity.KeteranganInterBranch;
					$scope.currentUntukPembayaran = row.entity.PaymentFor;
					$scope.currentDescription = row.entity.Description;
					$scope.currentPaymentDate = row.entity.Date;
					$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_Interbranch = row.entity.Nominal - $scope.CurrentNominal;
					// //20170414, eric, ebd
				}
				else {
					$scope.currentInterBranchId = 0;
					$scope.currentNamaBranchPenerima = "";
					$scope.currentNomorAlokasiUnknown = 0;
					$scope.currentNominalPembayaran = 0;
					$scope.currentNamaPelanggan = "";
					$scope.currentUntukPembayaran = "";
					$scope.currentTanggalBank = new Date();

				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
				//$scope.getDataInterbranch(pageNumber,uiGridPageSize);			
				AlokasiUnknownFactory.getInterBranch(pageNumber, uiGridPageSize, "")
					.then(
						function (res) {
							if (res.data.Result.length > 0) {
								$scope.TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
							}
							$scope.loading = false;
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		multiselect: false,
		columnDefs: [
			//20170414, eric, begin
			// { name: 'Nama Branch Penerima', field: 'KeteranganInterBranch' },
			// { name: 'Nomor Alokasi Unknown', field: 'Nominal' },
			// { name: 'Tanggal Alokasi Unknown', field: 'TanggalBank', cellFilter: 'date:\'yyyy-MM-dd\'' },
			// { name: 'Nominal Pembayaran', field: 'Nominal' },
			// { name: 'Nama Pelanggan', field: 'NamaPelanggan' },
			// { name: 'Untuk Pembayaran', field: 'UnknownDescription' },
			// { name: 'InterBranchId', field: 'InterBranchId', visible: false }

			{ name: 'Nama Branch Penerima', field: 'Name' },
			{ name: 'Nomor Alokasi Unknown', field: 'IncomingNo' },
			{ name: 'Tanggal Alokasi Unknown', field: 'Date', cellFilter: 'date:\'yyyy-MM-dd\'' },
			{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
			{ name: 'Nama Pelanggan', field: 'CustomerName' },
			{ name: 'Untuk Pembayaran', field: 'PaymentFor' },
			{ name: 'InterBranchId', field: 'InterbranchId', visible: false }
			//20170414, eric, end
		]
	};

	//Alvin, 20170527, begin
	//Proforma Invoice
	$scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangka_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.currentProformaInvoiceId = row.entity.ProformaInvoiceId;
					$scope.currentNoRangka = row.entity.NoRangka;
					$scope.currentNoSPK = row.entity.NoSPK;
					$scope.currentNoSO = row.entity.NoSO;
				}
				else {
					$scope.currentProformaInvoiceId = "";
					$scope.currentNoRangka = "";
					$scope.currentNoSPK = "";
					$scope.currentNoSO = "";
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'ProformaInvoiceId', field: 'ProformaInvoiceId', visible: false },
				{ name: 'ProformaInvoiceCode', field: 'ProformaInvoiceCode', visible: false },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nama di Billing', field: 'NamaBilling' },
				{ name: 'Nominal PI', field: 'NominalPI', cellFilter: 'rupiahC' },
				{ name: 'Nominal AR', field: 'NominalAR', cellFilter: 'rupiahC' }
			]
	};

	$scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		showColumnFooter: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
				}
				else {
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'ProformaInvoiceId', field: 'ProformaInvoiceId', visible: false },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nominal', field: 'Nominal', enableCellEdit: true, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahC' },
				{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteNoRangkaRow(row)">Delete</button>' }
			]
	};

	$scope.TambahAlokasiUnknown_DaftarNoRangka_AddItem = function () {
		console.log("TambahAlokasiUnknown_DaftarNoRangka_AddItem");
		$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangka_UIGrid.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.ProformaInvoiceId);
				console.log('isi grid2 -->', row.ProformaInvoiceId);
				if (row.ProformaInvoiceId == obj.ProformaInvoiceId)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data.push({ ProformaInvoiceId: $scope.currentProformaInvoiceId, NoRangka: $scope.currentNoRangka, NoSPK: $scope.currentNoSPK, NoSO: $scope.currentNoSO });
			}
		}
		)
	};

	$scope.deleteNoRangkaRow = function (row) {
		var index = $scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data.splice(index, 1);
	};


	//Refund Leasing
	$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.currentRefundLeasingId = row.entity.RefundLeasingId;
					$scope.currentNoRangka = row.entity.NoRangka;
					$scope.currentNoSPK = row.entity.NoSPK;
					$scope.currentNoSO = row.entity.NoSO;
				}
				else {
					$scope.currentRefundLeasingId = "";
					$scope.currentNoRangka = "";
					$scope.currentNoSPK = "";
					$scope.currentNoSO = "";
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'RefundLeasingId', field: 'RefundLeasingId', visible: false },
				{ name: 'Vendor Name', field: 'VendorName' },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nominal Refund', field: 'NominalRefund', cellFilter: 'rupiahC' }
			]
	};

	$scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
				}
				else {
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'RefundLeasingId', field: 'RefundLeasingId' },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nominal Refund', field: 'NominalRefund', enableCellEdit: true, type: 'number', cellFilter: 'rupiahC' },
				{ name: 'Delete', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteNoRefundRow(row)">Delete</button>' }
			]
	};

	$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_AddItem = function () {
		console.log("TambahAlokasiUnknown_DaftarARRefundLeasing_AddItem");
		$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.RefundLeasingId);
				console.log('isi grid2 -->', row.RefundLeasingId);
				if (row.RefundLeasingId == obj.RefundLeasingId)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid.data.push({ RefundLeasingId: $scope.currentRefundLeasingId, NoRangka: $scope.currentNoRangka, NoSPK: $scope.currentNoSPK, NoSO: $scope.currentNoSO });
			}
		}
		)
	};

	$scope.deleteNoRefundRow = function (row) {
		var index = $scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid.data.splice(index, 1);
	};

	//VIEW ALOKASI UNKNOWN DP
	$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid =
		{
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			showGridFooter: true,
			showColumnFooter: true,	
			columnDefs: [
				{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'TglSPK', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
				{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', visible: false },
				{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
				{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
				{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
				{ name: 'Keterangan', displayName: "", field: 'Keterangan', visible: true },
				{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
				{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
				{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
				{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
				{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
			]
		};


	$scope.LihatDetailIncoming = function (row) {
		//var index = $scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data.indexOf(row.entity);
		//console.log(row.entity.IPParentId);
		//console.log(row.IPParentId);
		$scope.ShowAlokasiUnknownDataDetail = true;
		$scope.ShowAlokasiUnknownData = false;
		$scope.ShowAlokasiUnknownHeader = false;
		$scope.ShowAlokasiUnknownHeaderTambah = false;
		$scope.Show_Show_LihatAlokasiUnknown_RincianPembayaran = true;
		$scope.Show_Show_LihatAlokasiUnknown_BiayaBiaya = true;
		$scope.Show_Show_LihatAlokasiUnknown_FootKuitansi = true;

		$scope.Show_LihatAlokasiUnknown_CetakKuitansi = true;
		$scope.Show_LihatAlokasiUnknown_SimpanCetakKuitansi = false;
		$scope.LihatAlokasiUnknown_NomorKuitansi_Pilih = "Lihat";

		$scope.currentIPParentId = row.entity.IPParentId;
		$scope.currentUnknownId = row.entity.UnknownId;
		$scope.currentCustomerId = row.entity.CustomerId;
		$scope.LihatAlokasiUnknown_TanggalAlokasiUnknown = row.entity.TanggalUnknown;
		$scope.LihatAlokasiUnknown_Top_NomorAlokasiUnknown = row.entity.UnknownNo;
		$scope.LihatAlokasiUnknown_Top_NomorAlokasiUnknownReversal = row.entity.UnknownRev;

		console.log(row.entity.UnknownPaymentType);
		$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = row.entity.UnknownPaymentType;
		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar_ValueChanged();
		$scope.LihatAlokasiUnknown_RincianPembayaran_TipeAlokasiUnknown_Selected_Changed();

		$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data = [];


		$scope.LihatAlokasiUnknown_PengajuanReversalEnable = true;

		if (row.entity.StatusPengajuan.toLowerCase() != "disetujui" && row.entity.TipePengajuan.toLowerCase() == "alokasiunknown") {
			$scope.LihatAlokasiUnknown_PengajuanReversalEnable = false;
		}
		else if ((row.entity.StatusPengajuan.toLowerCase() == "disetujui" || row.entity.StatusPengajuan.toLowerCase() == "diajukan") && row.entity.TipePengajuan.toLowerCase() == "reversal") {
			$scope.LihatAlokasiUnknown_PengajuanReversalEnable = false;
		}
		else if ((row.entity.StatusPengajuan.toLowerCase() == "ditolak") && row.entity.TipePengajuan.toLowerCase() == "reversal") {
			$scope.LihatAlokasiUnknown_PengajuanReversalEnable = false;
		}
		else {
			$scope.LihatAlokasiUnknown_PengajuanReversalEnable = true;
		}

		//AlokasiUnknownFactory.getCustomersByCustomerId($scope.currentCustomerId)
		AlokasiUnknownFactory.getCustomerDataByIPParentId($scope.currentIPParentId)
			.then
			(
			function (res) {
				//Prospect
				console.log(res);
				console.log("getCustomerDataByIPParentId");
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
				//Branch
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
				//PelangganBranch
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].ProspectCode;
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
				//Toyota								
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].ProspectCode;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].CustomerName;
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
				//Cetak Button
				$scope.EnabledCetak = res.data.Result[0].EnabledCetak;
				console.log("Test enable cetak: ", $scope.EnabledCetak);
				if ($scope.EnabledCetak == "1") {
					if (row.entity.StatusPengajuan.toLowerCase() != "disetujui" && row.entity.TipePengajuan.toLowerCase() == "alokasiunknown") {
						$scope.LihatAlokasiUnknown_Cetak_EnabledDisabled = false;
						$scope.LihatAlokasiUnknown_SimpanCetak_EnabledDisabled = false;
					}
					else {
						console.log("History Lost:", row.entity.HistoryLost);
						if (row.entity.HistoryLost == 1) {
							$scope.HistoryLost = 1
							// Enable edit Kuitansi Preprint.
							$scope.getDataPreprintedCB();
	
							$scope.LihatAlokasiUnknown_NomorKuitansi_EnabledDisabled = true;
							$scope.Show_LihatAlokasiUnknown_CetakKuitansi = false;
							$scope.Show_LihatAlokasiUnknown_SimpanCetakKuitansi = true;
	
							$scope.LihatAlokasiUnknown_NomorKuitansi_Pilih = "Edit";
	
							$scope.LihatAlokasiUnknown_Cetak_EnabledDisabled = false;
							$scope.LihatAlokasiUnknown_SimpanCetak_EnabledDisabled = true;
						}
						else if (row.entity.HistoryLost == 0) {
							$scope.HistoryLost = 0
							$scope.LihatAlokasiUnknown_NomorKuitansi_EnabledDisabled = false;
	
							$scope.LihatAlokasiUnknown_Cetak_EnabledDisabled = true;
							$scope.LihatAlokasiUnknown_SimpanCetak_EnabledDisabled = false;
						}
					}
				}
				else {
					$scope.LihatAlokasiUnknown_NomorKuitansi_EnabledDisabled = false;

					$scope.LihatAlokasiUnknown_Cetak_EnabledDisabled = false;
					$scope.LihatAlokasiUnknown_SimpanCetak_EnabledDisabled = false;
				}
			},
			function (err) {
				console.log("err=>", err);
				//Prospect
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_Alamat = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Prospect_Handphone = "";
				//Branch
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_PelangganBranch_Alamat = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_NamaPt = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Branch_Telepon = "";
				//Toyota								
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_Alamat = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = "";
				$scope.LihatAlokasiUnknown_InformasiPelangan_Toyota_Handphone = "";
			}
			);

		AlokasiUnknownFactory.GetListRincianPembayaran(1, uiGridPageSize, $scope.currentIPParentId)
			.then
			(
			function (res) {
				console.log("1");
				$scope.LihatAlokasiUnknown_RincianPembayaran_UIGrid.data = res.data.Result;
				console.log("Rincian Pembayaran");
				console.log(res.data.Result);
				if(row.entity.HistoryLost != 1){
					$scope.LihatAlokasiUnknown_NomorKuitansi = res.data.Result[0].ReceiptNo;
				}else{
					$scope.LihatAlokasiUnknown_NomorKuitansi = undefined;
				}
				$scope.LihatAlokasiUnknown_KetKuitansi = res.data.Result[0].ReceiptDesc;
				console.log("2");
			},
			function (err) {
				console.log("err=>", err);
			}
			);
		console.log("Cek preprint >>>>>>>>>>>>", $scope.LihatAlokasiUnknown_NomorKuitansi)
		AlokasiUnknownFactory.GetListRincianBiaya(1, uiGridPageSize, $scope.currentIPParentId)
			.then
			(
			function (res) {
				console.log("3");
				$scope.LihatAlokasiUnknown_Biaya_UIGrid.data = res.data.Result;
				console.log("4");
			},
			function (err) {
				console.log("err=>", err);
			}
			);

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSPK(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Booking Fee");
					console.log(res.data.Result[0].TanggalSPK);
					$scope.LihatAlokasiUnknown_InformasiPembayaran_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSpk = res.data.Result[0].NoSPK;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Down Payment");
					$scope.LihatAlokasiUnknown_DaftarSalesOrder_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSpk = res.data.Result[0].NoSPK;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {

					// { name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					// { name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
					// { name: 'Nominal Billing', field: 'NominalDP', cellFilter: 'rupiahC' },
					// { name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					// { name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					// { name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
					// { name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'rupiahC' }

					// $scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Down Payment", NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP, PaidNominal: res.data.Result[0].PaidNominal });
					// $scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk" });

					// angular.forEach($scope.PreprintedSettingData, function (value, key) {
					// 	if (!isPreprintedReceipt) {
					// 		if (value.BusinessType.indexOf('Unit') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Unit') == 0) {
					// 			ShowKuitansi_Lihat();
					// 			isPreprintedReceipt = true;
					// });

					console.log("Unit - Full Payment");
					var arrRes = [];

					angular.forEach(res.data.Result, function (value, key) {
						arrRes.push({
							SONumber: value.SONumber,
							SODate: value.SODate,
							NominalDP: value.NominalBilling,
							MateraiDP: value.MateraiDP,
							PaidDP: value.PaidDP + value.PaymentAmount,
							SisaDP: value.SisaNominal,
							PaymentAmount: value.PaymentAmount
						})
					});

					$scope.LihatAlokasiUnknown_DaftarSalesOrder_UIGrid.data = arrRes;
					$scope.LihatAlokasiUnknown_Top_NomorSPK = res.data.Result[0].NoSPK;
					$('input[name=NomorSpk]').val(res.data.Result[0].NoSPK);
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping")
		//or ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi")
		{
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Swapping");
					$scope.LihatAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen"
			|| $scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Purnal Jual");

					$scope.LihatAlokasiUnknown_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		};

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_InformasiPembayaran_WoBill_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorWo = res.data.Result[0].WoCode;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					//OLD
					// $scope.LihatAlokasiUnknown_InformasiPembayaran_Appointment_UIGrid.data = res.data.Result;
					// $scope.LihatAlokasiUnknown_InformasiPembayaran_WoDown_UIGrid.data = res.data.Result;
					// $scope.LihatAlokasiUnknown_Top_NomorWo = res.data.Result[0].WoCode;
					
					//NEW IRFAN
					$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data = [];
					console.log("DaTA >>>>>",$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data);

					if (res.data.Result.length > 1) {
						console.log("!!!!!!!!!!!!!!!!!!!",res.data.Result.length)
						$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Down Payment", NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP, PaidNominal: res.data.Result[0].PaidNominal });
						$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk", NominalDP: res.data.Result[1].NominalDP, PaidDP: res.data.Result[1].PaidDP, SisaDP: res.data.Result[1].SisaDP, PaidNominal: res.data.Result[1].PaidNominal });
					}
					else {
						console.log("???????????????????",res.data.Result.length)
						$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Down Payment", NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP, PaidNominal: res.data.Result[0].PaidNominal });
						$scope.LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk", NominalDP: 0, PaidDP: 0, SisaDP: 0 });
					}
					
					$scope.LihatAlokasiUnknown_InformasiPembayaran_Appointment_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_InformasiPembayaran_WoDown_UIGrid.data = res.data.Result;

					console.log("WOCODE >>>", res.data.Result[0].WoCode)
					if (res.data.Result[0].WoCode == 0) {
						console.log("WOCODE IF>>>", res.data.Result[0].WoCode)
						$scope.LihatIncomingPayment_Top_NomorAppointment = res.data.Result[0].NoAppointment;
						Disable_LihatAlokasiUnknown_Top_NomorWo();
						Enable_LihatAlokasiUnknown_Top_NomorAppointment();
					}
					else {
						console.log("WOCODE else>>>", res.data.Result[0].WoCode)
						$scope.LihatAlokasiUnknown_Top_NomorWo = res.data.Result[0].WoCode;
						Enable_LihatAlokasiUnknown_Top_NomorWo();
						// Disable_LihatAlokasiUnknown_Top_NomorAppointment();
					}
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					$scope.Show_LihatAlokasiUnknown_DaftarSalesOrderDibayar = false;
					// $scope.LihatAlokasiUnknown_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);

		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					$scope.getDataVendor();
					console.log("5");
					$scope.LihatAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
					$scope.NamaVendor = res.data.Result[0].VendorName
					console.log(res.data.Result[0].VendorName);
					console.log($scope.NamaVendor);
					$scope.LihatAlokasiUnknown_Top_NamaLeasing = $scope.NamaVendor;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
			AlokasiUnknownFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					$scope.getDataVendor();
					console.log("5");
					$scope.LihatAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
					$scope.NamaVendor = res.data.Result[0].VendorName
					console.log(res.data.Result[0].VendorName);
					console.log($scope.NamaVendor);
					$scope.LihatAlokasiUnknown_Top_NamaLeasing = $scope.NamaVendor;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card") {
			$scope.LihatAlokasiUnknown_getDataEDC();
			AlokasiUnknownFactory.GetLihatIncomingPaymentAR(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatAlokasiUnknown_DaftarAR_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_Biaya = res.data.Result[0].BiayaBank;
					$scope.LihatAlokasiUnknown_Top_NamaBank = res.data.Result[0].BankId;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		var sTipe = $scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown;
		var isPreprintedReceipt = false;
		angular.forEach($scope.PreprintedSettingData, function (value, key) {
			if (!isPreprintedReceipt) {
				if (value.BusinessType.indexOf('Unit') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Unit') == 0) {
					ShowKuitansi_Lihat();
					isPreprintedReceipt = true;
				}
				else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
					ShowKuitansi_Lihat();
					isPreprintedReceipt = true;
				}
				else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
					ShowKuitansi_Lihat();
					isPreprintedReceipt = true;
				}
				else {
					HideKuitansi_Lihat();
				}
			}
		});

		if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
			$scope.LihatAlokasiUnknown_KetKuitansi_ShowHide = true;
		}
		else {
			$scope.LihatAlokasiUnknown_KetKuitansi_ShowHide = false;
		}
	};
	//Alvin, 20170527, end
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.refresh = true;	
	// $scope.cek = function(){
	// 	$scope.refresh = true;
	// 	$timeout(function() {
	// 		$scope.refresh = false;
	// 	}, 0);
		
	// }
	$scope.ToTambahAlokasiUnknown = function () {
		//20170517, nuse, begin
		$scope.TambahAlokasiUnknown_NomorKuitansi_New = "";
		$scope.LoadingSimpan = false;
		$scope.Mandatory = false;
		$scope.Show_ShowLihatAlokasiUnknown = false;
		$scope.Show_ShowTambahAlokasiUnknown = true;
		$scope.TambahAlokasiUnknown_TanggalAlokasiUnknown = new Date();
		//20170517, nuse, end
		//DisableAllMetodePembayaranDetails();
		//DisableAll_TambahAlokasiUnknown_InformasiPelanggan();
		//scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = "";


		$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }, { name: "Interbranch", value: "Interbranch" }];
		$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran = {};

		$scope.optionsDebitCreditCard = [{ name: "Credit Card", value: "1" }, { name: "Debit Card", value: "2" }];
		$scope.TambahAlokasiUnknown_RincianPembayaran_TipeKartu_CreditDebitCard = {};

		//20170509, nuse, begin
		//$scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Proforma Invoice)", value: "Unit - Payment (Proforma Invoice)" }, { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" },  { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }];
		//$scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" },  { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }];
		//$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" },  { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }];
		//20170509, nuse, end
		$scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = {};
		
		//Tambahan OI OE
		$scope.getOE();
		$scope.getOI();


	};

	$scope.ToLihatAlokasiUnknown = function () {
		$scope.ShowAlokasiUnknownData = false;

		$scope.LihatDaftarAlokasiUnknown_SearchCategoryOptions = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Alokasi Unknown", value: "Nomor Alokasi Unknown" }, { name: "Tanggal Alokasi Unknown", value: "Tanggal Alokasi Unknown" }, { name: "Nominal", value: "Nominal" }];
		$scope.LihatDaftarAlokasiUnknown_SearchCategory = $scope.LihatDaftarAlokasiUnknown_SearchCategoryOptions[0];

		$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }];
		//$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = $scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknownOptions[0];
		$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = {};
		DisableAll_LihatAlokasiUnknown_InformasiPelanggan();
		DisableAll_LihatAlokasiUnknown_InformasiPembayaran();
		Disable_LihatAlokasiUnknown_Interbranch_InformasiBranch();
		Disable_LihatAlokasiUnknown_DaftarNoRangkaDibayar();
		Disable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		Disable_LihatAlokasiUnknown_DaftarArCreditDebit();
		Disable_LihatAlokasiUnknown_RincianPembayaran();
		Disable_LihatAlokasiUnknown_FootKuitansi();
		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar = "";
		Disable_LihatAlokasiUnknown_DaftarSalesOrder();
		Disable_LihatAlokasiUnknown_DaftarWorkOrderDibayar();
		Disable_LihatAlokasiUnknown_Top_NomorSpk();
		Disable_LihatAlokasiUnknown_Top_NamaLeasing();
		Disable_LihatAlokasiUnknown_Top_NomorSo();
		Disable_LihatAlokasiUnknown_Top_NomorWo();
		Disable_LihatAlokasiUnknown_Top_NamaAsuransi();
		Disable_LihatAlokasiUnknown_Top_NamaBank();
		Disable_LihatAlokasiUnknown_Top_NomorAppointment();

	};

	$scope.CariAlokasiUnknownData = function () {
		if (($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown != null) &&
			($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo != null)) {
			$scope.ShowAlokasiUnknownData = true;
			$scope.GetListUnknownCari(1, uiGridPageSize);
		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal awal / akhir belum diisi",
				type: 'warning'
			});
		}
	};

	//Alvin, 20170527, begin
	$scope.GetListProformaInvoice = function (Start, Limit) {
		AlokasiUnknownFactory.GetListProformaInvoice(Start, Limit, $filter('date')(new Date($scope.TambahAlokasiUnknown_TanggalAlokasiUnknown), 'yyyy-MM-dd'), $scope.TambahAlokasiUnknown_SearchBy_NomorProforma)
			.then(
				function (res) {
					$scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid.totalItems = res.data.Total;
					$scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.GetListRefundLeasing = function (Start, Limit) {
		AlokasiUnknownFactory.GetListRefundLeasing(Start, Limit, $filter('date')(new Date($scope.TambahAlokasiUnknown_TanggalAlokasiUnknown), 'yyyy-MM-dd'), $scope.TambahAlokasiUnknown_Top_NamaLeasing)
			.then(
				function (res) {
					$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.totalItems = res.data.Total;
					$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};
	//Alvin, 20170527, end

	//Alvin, 20170511, begin Lihat
	$scope.GetListUnknown = function (Start, Limit) {

		AlokasiUnknownFactory.GetListUnknown(Start, Limit, $filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo), 'yyyy-MM-dd'), $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text, $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search)
			.then(
				function (res) {
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.totalItems = res.data.Total;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSize = Limit;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.GetListUnknownCari = function (Start, Limit) {

		AlokasiUnknownFactory.GetListUnknown(Start, Limit, $filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo), 'yyyy-MM-dd'))
			.then(
				function (res) {
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.totalItems = res.data.Total;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSize = Limit;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	function filterBy(item) {
		if (item.StatusPengajuan.toLowerCase() == 'diajukan') {
			return true;
		}
	}

	$scope.SetMainGridADH = function () {

		if ($scope.user.RoleName == 'ADH') {
			$scope.GetListUnknownADH(1, uiGridPageSize);
		}
	};

	$scope.GetListUnknownADH = function (Start, Limit) {

		AlokasiUnknownFactory.GetListUnknown(Start, Limit, $filter('date')(new Date('1970-01-01'), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo), 'yyyy-MM-dd'),
			"diajukan", "Status Pengajuan")
			.then(
				function (res) {
					//var filterData = res.data.Result.filter(filterBy);

					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.totalItems = res.data.Total;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSize = Limit;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;

					if (res.data.Result.length == 0) {
						$scope.GetListUnknown(1, uiGridPageSize);
					}
					else {
						var d = new Date(res.data.Result[0].StartDate);
						if (!angular.isUndefined(res.data.Result[0].StartDate)) {
							var StartDate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
							$scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown = new Date(StartDate);
						}
						$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text = 'diajukan';
						$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search = 'Status Pengajuan';
					}

				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.Pengajuan_LihatAlokasiUnknown_PengajuanReversalModal = function () {
		if (angular.isUndefined($scope.LihatAlokasiUnknown_PengajuanReversal_AlasanReversal) || $scope.LihatAlokasiUnknown_PengajuanReversal_AlasanReversal == "") {
			bsNotify.show({
				title: "Warning",
				content: "Alasan Reversal harus diisi",
				type: 'warning'
			});
			return false;
		}
		var data;
		data =
			[{
				"ApprovalTypeName": "AlokasiUnknown Reversal",
				"DocumentId": $scope.currentUnknownId,
				"ReversalReason": $scope.LihatAlokasiUnknown_PengajuanReversal_AlasanReversal
			}]

		AlokasiUnknownFactory.submitDataApprovalIncomingPayments(data)
			.then
			(
			function (res) {
				console.log(res);
				bsNotify.show({
					title: "Berhasil",
					content: "Reversal berhasil diajukan",
					type: 'success'
				});
				$scope.Batal_LihatAlokasiUnknown_PengajuanReversalModal();
				$scope.CariAlokasiUnknownData();
				$scope.LihatAlokasiUnknown_Batal();

			},
			function (err) {
				console.log("err=>", err);
				var errMsg = '';
				if (err.data.Message.split('#').length > 1) {
					errMsg = err.data.Message.split('#')[1];
				}

				bsNotify.show({
					title: "Notifikasi",
					content: err.data.Message,
					type: 'danger'
				});
			}
			);
	};
	//Alvin, 20170511, end Lihat

	//Alvin, 20170511, begin UnknownCards	
	$scope.GetListUnknownCards = function (Start, Limit) {
		AlokasiUnknownFactory.GetListUnknownCards(Start, Limit, $scope.TambahAlokasiUnknown_SearchBy_NamaBank)
			.then(
				function (res) {
					$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid.totalItems = res.data.Total;
					$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid.paginationPageSize = Limit;
					$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};
	//Alvin, 20170511, end UnknownCards

	//20170425, eric, begin
	$scope.deleteSalesOrderDibayar = function (row) {

		var param = new Array(1);
		param[0] = row.SOId;

		AlokasiUnknownFactory.deleteSalesOrdersBySOId(param)
			.then(
				function (res) {
					console.log("delete result=>", res);
					$scope.getSalesOrdersDibayarByCustomerName1(1, uiGridPageSize);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};
	//20170425, eric, end	

	$scope.TambahAlokasiUnknown_SimpanCetak = function () {
		console.log("TambahAlokasiUnknown_SimpanCetak");
		$scope.Cetak = "Y";
		$scope.TambahAlokasiUnknown_Simpan();
		//$scope.CetakIPParentId = 0;
	};

	$scope.TambahAlokasiUnknown_SimpanCetakTTUS = function () {
		console.log("TambahAlokasiUnknown_SimpanCetakTTUS");
		$scope.CetakTTUS = "Y";
		$scope.TambahAlokasiUnknown_Simpan();
		//$scope.CetakIPParentId = 0;
		//$scope.CetakSPKNo = "0";
		//$scope.CetakKuitansi();
	};

	$scope.TambahAlokasiUnknown_CheckFields = function () {
		var passed = false;

		console.log("Check Fields");

		if (($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment")) {
			console.log("1");
			console.log($scope.TambahAlokasiUnknown_KetKuitansi);
			if (angular.isUndefined($scope.TambahAlokasiUnknown_KetKuitansi) || $scope.TambahAlokasiUnknown_KetKuitansi == "") {
				console.log("1b");
				bsNotify.show({
					title: "Warning",
					content: "Keterangan kuitansi harus diisi",
					type: 'warning'
				});
				return (passed);
			}
		}

		// if ($scope.Preprinted == true) {
		// 	console.log("2");
		// 	console.log($scope.TambahAlokasiUnknown_NomorKuitansi);
		// 	if (angular.isUndefined($scope.TambahAlokasiUnknown_NomorKuitansi) || $scope.TambahAlokasiUnknown_NomorKuitansi == "") {
		// 		console.log("2b");
		// 		bsNotify.show({
		// 			title: "Warning",
		// 			content: "No kuitansi harus diisi",
		// 			type: 'warning'
		// 		});
		// 		return (passed);
		// 	}
		// }

		// console.log("3");

		passed = true;

		return (passed);
	}


	//20170414, eric, begin
	function TambahAlokasiUnknown_Simpan_General() {
		//------------------------------------------------
		//INIT DATA
		//------------------------------------------------
		var data;
		$scope.SOSubmitData = [];
		$scope.SOSubmitBiaya = [];
		var paymentType = 0;
		var metodeBayar = 0;
		var userId = 1;
		var CardType = "";
		var today = new Date();
		//var paramDetail = JSON.stringify($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data);

		if (!$scope.TambahAlokasiUnknown_CheckFields())
			return;

		//set metode bayar
		if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Bank Transfer") { metodeBayar = 1; }
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Cash") { metodeBayar = 2; }
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") { metodeBayar = 3; }
		else { metodeBayar = 4; }

		//set CardType
		if ($scope.TambahAlokasiUnknown_RincianPembayaran_TipeKartu_CreditDebitCard == "1") { $scope.CardType = "Credit Card"; }
		else { $scope.CardType = "Debit Card"; }

		console.log("Tipe Kartu");
		console.log($scope.TambahAlokasiUnknown_RincianPembayaran_TipeKartu_CreditDebitCard);
		console.log($scope.CardType);

		if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			$scope.Pembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer;
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Cash") {
			$scope.Pembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_Cash;
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			$scope.Pembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_CreditDebitCard;
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Interbranch") {
			$scope.Pembayaran = $scope.currentNominalPembayaran;
		}

		//set tipe payment
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			//paymentType = 1;
			$scope.TambahAlokasiUnknown_BookingFeeData();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_BookingFeeSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;

		}
		//20170430, NUSE EDIT, begin
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			$scope.TambahAlokasiUnknown_FullPaymentSOData();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_SOSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}

		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			$scope.TambahAlokasiUnknown_FullPaymentSOData();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_SOSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}
		//20170430, NUSE EDIT, end
		//20170505, Alvin, begin
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			console.log("Service - Down Payment")
			$scope.TambahAlokasiUnknown_Service_ByWO();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_ServiceSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			console.log("Service - Full Payment")
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.TambahAlokasiUnknown_Service_ByWO();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_ServiceSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.TambahAlokasiUnknown_Service_ByName();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_ServiceSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			console.log("Parts - Down Payment")
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.TambahAlokasiUnknown_SoDown();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				console.log("Parts - Down Payment - 3");
				$scope.TambahAlokasiUnknown_SalesOrder2();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			console.log("Parts - Full Payment")
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.TambahAlokasiUnknown_SoBillParts();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.TambahAlokasiUnknown_SalesOrder();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			console.log("Unit - Swapping")
			$scope.TambahAlokasiUnknown_SoBill();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			console.log("Unit - Purna Jual")
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.TambahAlokasiUnknown_SoBillOPDPJ();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.TambahAlokasiUnknown_SalesOrderOPDPJ();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			console.log("Unit - Order Pengurusan Dokumen")
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.TambahAlokasiUnknown_SoBillOPDPJ();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.TambahAlokasiUnknown_SalesOrderOPDPJ();
				$scope.SOSubmitData = $scope.TambahAlokasiUnknown_PartsSOSubmitData;
				$scope.TambahAlokasiUnknown_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
			}
		}

		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
			$scope.TambahAlokasiUnknown_NoRangka();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_NoRangkaSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
			$scope.TambahAlokasiUnknown_RefundLeasing();
			$scope.SOSubmitData = $scope.TambahAlokasiUnknown_RefundLeasingSubmitData;
			$scope.TambahAlokasiUnknown_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahAlokasiUnknown_BiayaData;
		}


		console.log($scope.currentTglAR);
		if ($scope.currentTglAR == "") {
			$scope.currentTglAR = $scope.TambahAlokasiUnknown_TanggalAlokasiUnknown;
		}



		data = [{
			OutletId: $scope.currentOutletId,
			SPKId: $scope.currentSPKId,
			TranDate: $scope.TambahAlokasiUnknown_TanggalAlokasiUnknown,
			IncomingPaymentType: $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown,
			CustomerId: $scope.currentCustomerId,
			Currency: "IDR",
			PaymentTotal: 0,
			ReceiptNo: $scope.TambahAlokasiUnknown_NomorKuitansi + "|" + $scope.TambahAlokasiUnknown_KetKuitansi,
			PrintCounter: 0,
			OwnRisk: $scope.TambahAlokasiUnknown_OwnRisk,
			UserId: userId,

			IncomingPaymentDetailXML: "",
			IncomingPaymentChargesXML: "",

			PaymentMethod: "",
			Difference: 0,
			PaymentDate: $scope.TambahAlokasiUnknown_TanggalAlokasiUnknown,
			PaymentDesc: "",
			ReceiverAccNumber: "",
			SenderBankId: "",
			SenderName: "",
			ChequeId: 0,
			CardProvider: "",
			CardNo: "",
			CardType: "",
			InterBranchId: 0,
			InterBranchDesc: "",
			GridARDate: $scope.TambahAlokasiUnknown_TanggalAlokasiUnknown,
			GridCardType: "",
			GridCardNominal: 0,
			BankCharges: 0,
			BankStatementUnknownId: $scope.currentBankStatemntUnknownId,
			InterbranchNamaPelanggan: $scope.TambahAlokasiUnknown_InformasiPelangan_Lain_NamaPelanggan,
			InterbranchUntukPembayaran: $scope.TambahAlokasiUnknown_InformasiPelangan_Lain_UntukPembayaran,
			UnknownTranType: "RequestApproval",
			SOList: $scope.SOSubmitData,
			BiayaList: $scope.SOSubmitBiaya,
			ProformaInvoiceId: ""
		}]

		//------------------------------------------------
		//SUBMIT
		//------------------------------------------------
		//alvin, 20170423, begin
		//$scope.submitData(data);

		console.log("Cek Submit");
		console.log($scope.SOSubmitData);

		angular.forEach($scope.SOSubmitData.data, function (value, key) {
			console.log(value);
			console.log(key);
		});

		var found = 0;

		$scope.SOSubmitData.forEach(function (obj) {
			console.log('isi grid SOID -->', obj.SOId);
			console.log('isi grid PAIDAMOUNT -->', obj.PaidAmount);
			var PaidAmount = obj.PaidAmount;
			console.log("1 paid ", PaidAmount);
			console.log("1", angular.isNumber(PaidAmount));

			if (!angular.isNumber(PaidAmount) && (obj.PaidAmount != undefined)) {
				console.log("PaidAmount", PaidAmount);
				PaidAmount = PaidAmount.toString().replace(/,/g, "");
				console.log("PaidAmount", PaidAmount);
			}
			if ((PaidAmount > 0) && (obj.PaidAmount != undefined))
				found = 1;
		});

		
		if ($scope.TambahAlokasiUnknown_TanggalAlokasiUnknown != null) {
			console.log("TanggalAlokasiUnknown sudah diisi");

			$scope.submitDataFinanceAlokasiUnknowns(data);

		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal incoming belum diisi",
				type: 'warning'
			});
		}
		//alvin, 20170423, end

	};

	//20170414, eric, end

	$scope.LihatAlokasiUnknown_Batal = function () {
		$scope.ShowAlokasiUnknownDataDetail = false;
		$scope.ShowAlokasiUnknownData = true;
		$scope.ShowAlokasiUnknownHeader = true;
		$scope.ShowAlokasiUnknownHeaderTambah = true;

		DisableAll_LihatAlokasiUnknown_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatAlokasiUnknown_InformasiPembayaran();//ini semua info pembayaran di hide

		$scope.Show_Show_LihatAlokasiUnknown_RincianPembayaran = false;
		$scope.Show_Show_LihatAlokasiUnknown_BiayaBiaya = false;
		$scope.Show_Show_LihatAlokasiUnknown_FootKuitansi = false;

	}



	$scope.TambahAlokasiUnknown_Batal = function () {
		console.log("Clean Up");
		HideTopMetodePembayaranDetails();//ini paymentdetail top nya (dropdown)
		DisableAllMetodePembayaranDetails();//ini hide all hasil dropdown metode bayar
		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CreditDebitCard();
		DisableAll_TambahAlokasiUnknown_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_TambahAlokasiUnknown_InformasiPembayaran();//ini semua info pembayaran di hide
		DisableAll_TambahAlokasiUnknown_DaftarSpk();//ini semua ilangin daftar spk
		DisableAll_TambahAlokasiUnknown_SalesOrder();//ini ilangin semua sales order
		DisableAll_TambahAlokasiUnknown_DaftarSalesOrderRadio();
		Disable_TambahAlokasiUnknown_KumpulanNomorRangka();
		Disable_TambahAlokasiUnknown_KumpulanWorkOrder();
		Disable_TambahAlokasiUnknown_DaftarArCreditDebit();
		Disable_TambahAlokasiUnknown_KumpulanARRefundLeasing();
		Disable_TambahAlokasiUnknown_SaveAndPrintButton();
		Disable_TambahAlokasiUnknown_BiayaBiaya();
		$scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSpk_Brief_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill = "";
		$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.columnDefs = [];
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [];
		$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs = [];
		$scope.TambahAlokasiUnknown_DaftarUnknown_UIGrid.data = [];
		$scope.Show_ShowLihatAlokasiUnknown = true;
		$scope.Show_ShowTambahAlokasiUnknown = false;
		$scope.TambahAlokasiUnknown_ClearAllData();
		$scope.Cetak = "N";
		$scope.CetakTTUS = "N";
		$scope.CariAlokasiUnknownData();
	}

	$scope.TambahAlokasiUnknown_Test = function () {
		$scope.TambahAlokasiUnknown_Batal();
		FormStateValue = 1;
	}

	//Looping BIaya Lain-lain start
	$scope.LoopingTambahAlokasiUnknown_Biaya_UIGrid = function (){
		$scope.BiayaKredit = 0;
		$scope.BiayaDebet = 0;
		if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
			for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
				if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						$scope.BiayaKredit += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
				}
				else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
					($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
						$scope.BiayaDebet += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
				}
			}
		}
	}
	//Looping BIaya Lain-lain End
	$scope.LoadingSimpan = false;
	$scope.TambahAlokasiUnknown_Simpan = function () {
		$scope.LoadingSimpan = true;
		//20170414, eric, begin		
		if($scope.TambahAlokasiUnknown_DaftarUnknown_gridAPI.selection.getSelectedCount() <= 0){
			bsNotify.show(
				{
					title: "Notifikasi",
					content: "List Unknown harus Dipilih!",
					type: 'danger'
				}
			);
			$scope.LoadingSimpan = false;
			return false;
		};
		
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			//penjagaan BF
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			var tmpPendapatan = 0;

			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
				$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.NominalBookingFee);
				});
			}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.NominalBookingFee);
				});
			}
			
			$scope.LoopingTambahAlokasiUnknown_Biaya_UIGrid();
			TotBiayaD = $scope.BiayaDebet;
			TotBiayaK = $scope.BiayaKredit;
			//*****************dijadikan satu function start ***************************/
			// if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
			// 	for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			// 		if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
			// 				TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 		}
			// 		else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
			// 				TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
			// 		}
			// 	}
			// }
			//*****************dijadikan satu function end ***************************/
			
			for (var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
				if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
					if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
						tmpPendapatan = 1;
					}
				}
				
			}

			if(tmpPendapatan == 1){
				if (TotalPembayaran != $scope.Pembayaran - TotBiayaD + TotBiayaK){
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					SimpanOK = 0;
				}
			}else{
				SimpanOK = 1;
			}
			
			if (SimpanOK == 1){
				TambahAlokasiUnknown_Simpan_General();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;

			if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0){
				for(var x in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[x].BiayaId == 'Titipan'){
						for(var y in $scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data){
								if($scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data[y].TotalDPMustBePay != $scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data[y].Pembayaran){
									bsNotify.show({
										title: "Warning",
										content: 'Tidak dapat melakukan titipan, jika pembayaran Down Payment belum terpenuhi !',
										type: 'warning'
									});
									SimpanOK = 0;
									$scope.LoadingSimpan = false;
									return false;
								}
						}
					}
				}
			}
			
			if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
					}
					else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
							TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
					}
				}
			}

			//console.log(obj);
			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				console.log("0", obj.Pembayaran);
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran)) && obj.Pembayaran <= obj.TotalDPMustBePay) {
					console.log("1", obj.Pembayaran);
					//console.log(obj.TotalDPMustBePay);
					TotalPembayaran += obj.Pembayaran;

					//alert("Pembayaran DP TIdak Sesuai");
				}
				else if ( obj.Pembayaran > obj.TotalDPMustBePay){
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> Lebih Besar Dari Saldo!",
							type: 'danger'
						}
					);
					SimpanOK = 0;
					$scope.LoadingSimpan = false;
					return false;
				}
				else {
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> wajib diisi!",
							type: 'danger'
						}
					);
					SimpanOK = 0;
					$scope.LoadingSimpan = false;
					return false;
				}
			});

			console.log("2", TotalPembayaran);
			console.log("3", $scope.Pembayaran);
			if (SimpanOK == 1){
				if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran ) {
					SimpanOK = 1;
				}
				else {
					SimpanOK = 0;
				}
	
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
				};
			}
			else{
				return false;
			}
			
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			
			$scope.LoopingTambahAlokasiUnknown_Biaya_UIGrid();
			TotBiayaD = $scope.BiayaDebet;
			TotBiayaK = $scope.BiayaKredit;
			//*****************dijadikan satu function start ***************************/
			// if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
			// 	for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			// 		if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
			// 				TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 		}
			// 		else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
			// 				TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
			// 		}
			// 	}
			// }
			//*****************dijadikan satu function start ***************************/

			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				console.log("Keluar3", obj.Pembayaran);

				// if ((obj.Pembayaran != 0) && (obj.Pembayaran != "") && (!angular.isUndefined(obj.Pembayaran))) {
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran)) && obj.Pembayaran <= obj.TotalPaymentMustBePay) {
					//if (obj.Pembayaran > obj.TotalPaymentMustBePay) {
						// SimpanOK = 0;
					//}
					SimpanOK = 1;
					TotalPembayaran += obj.Pembayaran;
				}
				else if ( obj.Pembayaran > obj.TotalPaymentMustBePay){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> Lebih Besar Dari Saldo!",
							type: 'danger'
						}
					);
					SimpanOK = 0;
					$scope.LoadingSimpan = false;
					return false;
				}
				else {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> wajib diisi!",
							type: 'danger'
						}
					);
					SimpanOK = 0;
					$scope.LoadingSimpan = false;
					return false;
				}
			});

			if (SimpanOK == 1) {
				if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran) {
					SimpanOK = 1;
				}
				else {
					SimpanOK = 0;
				}

				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return false;
				};
			}
			else{
				return false;
			}
			//TambahIncomingPayment_Simpan_General();
			//20170430, NUSE EDIT, end
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			var tmpPendapatan = 0;

			if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
					}
					else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
							TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
					}
				}
				for (var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
						if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
							tmpPendapatan = 1;
						}
					}
				}
			}

			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
				$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.SisaPembayaran);
				});
			}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.SisaPembayaran);
				});
			}

			if (SimpanOK == 1) {
				if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
					if(tmpPendapatan == 1){
						if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran) {
							SimpanOK = 1;
						}
						else {
							SimpanOK = 0;
						}
					}else{
						if(TotalPembayaran - TotBiayaD + TotBiayaK >= $scope.Pembayaran){
							SimpanOK = 1;
						}else{
							SimpanOK = 0;
						}
					}
				}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
					if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
				}
					
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return false;
				};
			}
			else{
				return false;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			var tmpPendapatan = 0;

			if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
					}
					else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
							TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
					}
				}
				for (var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
						if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
							tmpPendapatan = 1;
						}
					}
				}
			}

			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
				$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.SisaPembayaran);
				});
			}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += parseInt(obj.SisaPembayaran);
				});
			}

			if (SimpanOK == 1) {
				if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
					if(tmpPendapatan == 1){
						if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran) {
							SimpanOK = 1;
						}
						else {
							SimpanOK = 0;
						}
					}else{
						if(TotalPembayaran - TotBiayaD + TotBiayaK >= $scope.Pembayaran){
							SimpanOK = 1;
						}else{
							SimpanOK = 0;
						}
					}
				}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
					if (TotalPembayaran - TotBiayaD + TotBiayaK == $scope.Pembayaran) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
				}

				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return false;
				};
			}
			else{
				return false;
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;

			if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0){
				for(var x in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[x].BiayaId == 'Titipan'){
						for(var y in $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data){
								if($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data[y].SisaDP != $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data[y].Pembayaran){
									bsNotify.show({
										title: "Warning",
										content: 'Tidak dapat melakukan titipan, jika pembayaran Down Payment belum terpenuhi !',
										type: 'warning'
									});
									SimpanOK = 0;
									$scope.LoadingSimpan = false;
									return false;
								}
						}
					}
				}
			}
			if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
					}
					else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
							TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
					}
				}
			}
			//console.log(obj);
			$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
				console.log("0", obj.Pembayaran);
				if ((obj.Keterangan == "Own Risk" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0)) && (obj.Keterangan == "Down Payment" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0))) {
					console.log("1", obj.Pembayaran);
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran pada Daftar Informasi Pembayaran wajib diisi!",
								type: 'danger'
							}
						);
						$scope.LoadingSimpan = false;
					
				}
				else {
					if (obj.Pembayaran > obj.SisaDP) {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran harus lebih kecil dari Saldo!",
							type: 'danger'
						}
					);
					$scope.LoadingSimpan = false;
					}else{
						TotalPembayaran += obj.Pembayaran;
					}
				}
			});
			if (SimpanOK == 1) {
				//Re-calculate Biaya Lain-lain
				
				if ($scope.Pembayaran == TotalPembayaran - TotBiayaD + TotBiayaK) {
					SimpanOK = 1;
				}
				else {
					SimpanOK = 0;
				}
			}

			if (SimpanOK == 1) {
				TambahAlokasiUnknown_Simpan_General();
			}
			else {
				bsNotify.show({
					title: "Warning",
					content: "Pembayaran tidak sesuai",
					type: 'warning'
				});
				$scope.LoadingSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			// var SimpanOK = 1;
			// var TotalPembayaran = 0;
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			//console.log(obj);

			if($scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.length <=0){
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Tidak Ada Daftar WO yang dibayar",
						type: "warning"
					}
				)
				$scope.LoadingSimpan = false;
				return false;
			}

			$scope.LoopingTambahAlokasiUnknown_Biaya_UIGrid();
			TotBiayaD = $scope.BiayaDebet;
			TotBiayaK = $scope.BiayaKredit;
			//*****************dijadikan satu function start ***************************/
			// if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
			// 	for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			// 		if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
			// 				TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 		}
			// 		else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
			// 				TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
			// 		}
			// 	}
			// }
			//*****************dijadikan satu function start ***************************/

			$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
				console.log("0", obj.Pembayaran);
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran))) {
					console.log("1", obj.Pembayaran);
					console.log("Sisa Nominal", obj.SisaNominal);
					if (obj.Pembayaran > obj.SisaNominal) {
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran harus lebih kecil dari Saldo!",
								type: 'danger'
							}
						);
						$scope.LoadingSimpan = false;
					}
					else {
						console.log("total Pembayaran>>>>>>>", TotalPembayaran);
						TotalPembayaran += obj.Pembayaran;
					}
				}
				else {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran wajib diisi!",
							type: 'danger'
						}
					);
					$scope.LoadingSimpan = false;
					SimpanOK = 0;
				}
			});

			if (SimpanOK == 1) {
				//Re-calculate Biaya Lain-lain

				if ($scope.Pembayaran == TotalPembayaran - TotBiayaD + TotBiayaK) {
					SimpanOK = 1;
				}
				else {
					SimpanOK = 0;
				}

				var totalBiaya = 0;
				angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
					if ((value.BiayaId == "Bea Materai" && value.DebitCredit == 'Kredit') || 
						(value.BiayaId == "Biaya Bank" && value.DebitCredit == 'Kredit')) {
						totalBiaya = totalBiaya + parseInt(value.Nominal.toString().replace(/\./g, ""));
					}else if((value.BiayaId == "Biaya Bank" && value.DebitCredit == 'Debet')){
						totalBiaya = totalBiaya - parseInt(value.Nominal.toString().replace(/\./g, ""));
					}
				});
				// console.log("hasil Replace >>>>>>",totalBiaya);
				// console.log("Total Pembayaran >>>>>>", TotalPembayaran);
				// TotalPembayaran = parseInt(TotalPembayaran) + totalBiaya;

				// if ($scope.Pembayaran == TotalPembayaran) {
				// 	console.log("masuk kesini gak ya ?????????")
				// 	SimpanOK = 1;
				// }
				// else {
					
				// 	console.log("atau masuk kesini ya ????????? >>>>>>>>>>")
				// 	SimpanOK = 0;
				// }
			}

			if (SimpanOK == 1) {
				TambahAlokasiUnknown_Simpan_General();
				
				console.log("Langsung kesini ya >>>>>>>>>>> ?????????")
			}
			else {
				bsNotify.show({
					title: "Warning",
					content: "Pembayaran Full Payment tidak sesuai",
					type: 'warning'
				});
				$scope.LoadingSimpan = false;
			};
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			//Tidak ada penjagaan
			// TambahAlokasiUnknown_Simpan_General();
			var SimpanOK = 1;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotalPembayaran = 0;
			var tmpPendapatan = 0;

			console.log('$scope.selectedRow_DaftarUnknown_UIGrid',$scope.selectedRow_DaftarUnknown_UIGrid);
			// $scope.Pembayaran = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data[0].NominalDP;
			// TotalPembayaran = $scope.selectedRow_DaftarUnknown_UIGrid[0].Nominal;

			if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
					if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
					}
					else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
						($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
							TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
					}
				}
			}
			var validatTitipan = 0;
			for (var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			//	if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Titipan"){
					if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
						tmpPendapatan = 1;
					}else if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Titipan"){ //validasi titipan cr4 p2 finance
						validatTitipan = 1;
					}
			//	}
			}
			
			//console.log(obj);
			// TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				if($scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data.length == 0){
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Tidak Ada Daftar SO yang dibayarkan!",
							type: 'danger'
						}
					);
					$scope.LoadingSimpan = false;
					return false;
				}

				$scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data.forEach(function (obj) {
					console.log("0", obj.SisaDP);
					if((obj.SisaDP != 0) && (obj.SisaDP != "") && (!angular.isUndefined(obj.SisaDP))){
						TotalPembayaran += obj.SisaDP; 
					}
					// if ((obj.Keterangan == "Own Risk" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0)) && (obj.Keterangan == "Down Payment" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0))) {
					// 	console.log("1", obj.Pembayaran);
					// 		SimpanOK = 0;
					// 		bsNotify.show(
					// 			{
					// 				title: "Gagal",
					// 				content: "Field Pembayaran pada Daftar Informasi Pembayaran wajib diisi!",
					// 				type: 'danger'
					// 			}
					// 		);
						
					// }
					// else {
					// 	if (obj.Pembayaran > obj.SisaDP) {
					// 	SimpanOK = 0;
					// 	bsNotify.show(
					// 		{
					// 			title: "Gagal",
					// 			content: "Field Pembayaran harus lebih kecil dari Saldo!",
					// 			type: 'danger'
					// 		}
					// 	);
					// 	}else{
					// 		TotalPembayaran += obj.Pembayaran;
					// 	}
					// }
				});
	
				if (tmpPendapatan == 1 || validatTitipan == 1) {  // penambahan validasi titipan
					//Re-calculate Biaya Lain-lain
					
					if ($scope.Pembayaran == TotalPembayaran - TotBiayaD + TotBiayaK) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
					
				}else {
					SimpanOK = 1;
				}
				console.log($scope.Pembayaran,'-',TotalPembayaran,'-',TotBiayaD,'-',TotBiayaK)
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					if(validatTitipan == 1){ //validasi titipan
						bsNotify.show({
							title: "Warning",
							content: 'Tidak dapat melakukan titipan !',
							type: 'warning'
						});
						return false;
					}else{
						bsNotify.show({
							title: "Warning",
							content: "Pembayaran tidak sesuai",
							type: 'warning'
						});
						$scope.LoadingSimpan = false;
						return false;
					}
				};


			}else{
				$scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data.forEach(function (obj) {
					console.log("0", obj.SisaDP);
					if((obj.SisaDP != 0) && (obj.SisaDP != "") && (!angular.isUndefined(obj.SisaDP))){
						TotalPembayaran += obj.SisaDP; 
					}
				});
				if (tmpPendapatan == 1) {
					//Re-calculate Biaya Lain-lain
					
					if ($scope.Pembayaran == TotalPembayaran - TotBiayaD + TotBiayaK) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
	
				}else {
					SimpanOK = 1;
				}
	
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return false;
				};

			}
			

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;	
			var TotBiayaD = 0;
			var TotBiayaK = 0;

			// for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			// 	if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 		($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
			// 			TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 			console.log("data k", TotBiayaK)
			// 	}else if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
			// 			TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 			console.log("data D", TotBiayaD)
			// 	}
			// }

			$scope.LoopingTambahAlokasiUnknown_Biaya_UIGrid();
			TotBiayaD = $scope.BiayaDebet;
			TotBiayaK = $scope.BiayaKredit;
			//*****************dijadikan satu function start ***************************/
			// if ($scope.TambahAlokasiUnknown_Biaya_UIGrid.data.length > 0) {
			// 	for(var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			// 		if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
			// 				TotBiayaK += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal);
			// 		}
			// 		else if(($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
			// 			($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
			// 				TotBiayaD += parseInt($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].Nominal); 
			// 				// $scope.Pembayaran += TotBiayaD; 
			// 		}
			// 	}
			// }
			//*****************dijadikan satu function start ***************************/
			
			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2"){
				$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += obj.SisaPembayaran;
				});
	
				if (SimpanOK == 1) {
					//Re-calculate Biaya Lain-lain
					var totalBiaya = 0;
					angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
						if (value.DebitCredit == 'Kredit') {
							totalBiaya = totalBiaya + parseInt(value.Nominal);
						}
					});
					// TotalPembayaran = parseInt(TotalPembayaran) + totalBiaya;
	
					if ($scope.Pembayaran <= TotalPembayaran + TotBiayaK) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
				}
	
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran Full Payment tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return;
				};
			}else if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += obj.SisaPembayaran;
				});
	
				if (SimpanOK == 1) {
					//Re-calculate Biaya Lain-lain
					var totalBiaya = 0;
					angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
						if (value.DebitCredit == 'Kredit') {
							totalBiaya = totalBiaya + parseInt(value.Nominal);
						}
					});
					// TotalPembayaran = parseInt(TotalPembayaran) + totalBiaya;
	
					if ($scope.Pembayaran == TotalPembayaran + TotBiayaK + TotBiayaD) {
						SimpanOK = 1;
					}
					else {
						SimpanOK = 0;
					}
				}
	
				if (SimpanOK == 1) {
					TambahAlokasiUnknown_Simpan_General();
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Pembayaran Full Payment tidak sesuai",
						type: 'warning'
					});
					$scope.LoadingSimpan = false;
					return;
				};
			}
		}
		//Alvin, 20170423, begin
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Unknown") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Interbranch - Penerima") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Cash Delivery") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
			TambahAlokasiUnknown_Simpan_General();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
			TambahAlokasiUnknown_Simpan_General();
		}
		//Alvin, 20170423, end
		//20170414, eric, end
		//20170517, nuse, begin
		//$scope.Show_ShowLihatAlokasiUnknown = true;
		//$scope.Show_ShowTambahAlokasiUnknown = false;
		//20170517, nuse, end

	}

	$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransferBlur = function () {
		var TotalBookingFee = 0;
		var TotalDP = 0;
		var PembayaranDP = 0;

		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
				TotalBookingFee += obj.NominalBookingFee;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalBookingFee;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalDP;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalDP;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
				if (obj.SisaPembayaran != null) {
					PembayaranDP = obj.SisaPembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalDP;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.TotalNominal != null) {
					PembayaranDP = obj.TotalNominal;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalDP;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.TotalNominal != null) {
					PembayaranDP = obj.TotalNominal;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer - TotalDP;
		}
	}

	$scope.TambahAlokasiUnknown_NoRekeningPenerima_Selected_Changed = function () {
		console.log($scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerima);
		if ($scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerima !== "") {
			AlokasiUnknownFactory.getDataAccountBank($scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerima)
				.then(
					function (res) {
						console.log("No")
						$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBankPenerima_BankTransfer = res.data.Result[0].AccountBankName;
						$scope.TambahAlokasiUnknown_RincianPembayaran_NamaPemegangRekening_BankTransfer = res.data.Result[0].AccountName;
					}
				);
		}
	}

	$scope.TambahAlokasiUnknown_TujuanPembayaran_CashDelivery_Selected_Changed = function () {
		console.log("TambahAlokasiUnknown_TujuanPembayaran_CashDelivery_Selected_Changed");
		console.log($scope.TujuanPembayaran_CashDelivery);
		if ($scope.TujuanPembayaran_CashDelivery !== "") {
			console.log($scope.TujuanPembayaran_CashDelivery);
			AlokasiUnknownFactory.getDataAccountBank($scope.TujuanPembayaran_CashDelivery)
				.then
				(
				function (res) {
					console.log("No")
					$scope.TambahAlokasiUnknown_CashDelivery_NamaBank = res.data.Result[0].AccountBankName;
					$scope.TambahAlokasiUnknown_CashDelivery_NamaRekening = res.data.Result[0].AccountName;
				}
				);
		}
	}

	$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran_Selected_Changed = function () {
		DisableAllMetodePembayaranDetails();
		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CreditDebitCard();

		if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			//$scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			$scope.getDataBank();
			//$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions[0];

			//$scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerimaOptions = [{ name: "rek1", value: "rek1" }, { name: "rek2", value: "rek2" }, { name: "rek3", value: "rek3" }];
			$scope.getDataBankAccount();
			//$scope.TambahAlokasiUnknown_RincianPembayaran_NoRekeningPenerima_BankTransfer = $scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerimaOptions[0];

			ShowMetodePembayaranDetails_BankTransfer();

			if (($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment")
			) {
				console.log("a");
				ShowMetodePembayaranDetails_Selisih_BankTransfer();
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4b = false;
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4 = true;
				$scope.KetReqShow = true;
				$scope.KetNotReqShow = false;
			}
			else if (($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card")
				|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing")
			) {
				HideMetodePembayaranDetails_Selisih_BankTransfer();
				$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_BankTransfer = new date();
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4b = true;
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4 = false;
				$scope.KetReqShow = false;
				$scope.KetNotReqShow = true;

			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Unknown") {
				console.log("b");
				HideMetodePembayaranDetails_Selisih_BankTransfer();
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4b = false;
				$scope.Show_TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_BankTransfer4 = true;
				$scope.KetReqShow = false;
				$scope.KetNotReqShow = true;
			}

			//hilman,begin
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TotalBayarSum;
			}
			//hilman,end
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Cash") {
			ShowMetodePembayaranDetails_Cash();
			$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_Cash = $filter('date')(new Date(), 'yyyy-MM-dd');

			//hilman,begin
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_Cash = $scope.TotalBayarSum;
			}
			//hilman,end
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			//20170414, eric, begin
			//$scope.TambahAlokasiUnknown_CreditDebitCard_TipeKartuOptions = [{ name: "Credit Card", value: "1" }, { name: "Debit Card", value: "2" }];
			//20170414, eric, end
			//$scope.TambahAlokasiUnknown_RincianPembayaran_TipeKartu_CreditDebitCard = $scope.TambahAlokasiUnknown_CreditDebitCard_TipeKartuOptions[0];

			//$scope.TambahAlokasiUnknown_CreditDebitCard_NamaBankOptions = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			//$scope.getDataBank();
			$scope.TambahAlokasiUnknown_getDataEDC();
			ShowMetodePembayaranDetails_CreditDebitCard();
			$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_CreditDebitCard = $filter('date')(new Date(), 'yyyy-MM-dd');
			//hilman,begin
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
				$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.TotalBayarSum;
			}
			//hilman,end
		}
		else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Interbranch") {
			$scope.TambahAlokasiUnknown_Interbranch_SearchCategoryOptions = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Alokasi Unknown", value: "Nomor Alokasi Unknown" }, { name: "Tanggal Alokasi Unknown", value: "Tanggal Alokasi Unknown" }, { name: "Nominal", value: "Nominal" }];
			$scope.TambahAlokasiUnknown_Interbranch_SearchCategory = $scope.TambahAlokasiUnknown_Interbranch_SearchCategoryOptions[0];

			//20170414, eric, begin
			//$scope.getDataAlokasiUnknownDetail();
			console.log("Interbranch");
			$scope.getDataInterBranch(1, uiGridPageSize);
			//20170414, eric, end
			ShowMetodePembayaranDetails_Interbranch();


		}
	};

	$scope.Ready_Rincian_Pembayaran_Selected_None = function () {
		ShowTopMetodePembayaranDetails();//refresh tu dropdown metode pembayaran biar balik ke default
		$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }, { name: "Interbranch", value: "Interbranch" }];
		$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran = {};
	};

	$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy_ValueChanged = function () {
		//alert($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb);
		console.log("TambahAlokasiUnknown_TujuanPembayaran_SearchBy_ValueChanged");
		DisableAll_SearchByRadioResult();//disable semua hasil search by radio
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSpk();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaSalesman();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSpk();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSpk();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		//20170527, Alvin, begin		
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
		}
		//20170527, Alvin, end		
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelangganPt();
			}
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi") {
		// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
		// Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
		// }
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
		// Enable_TambahAlokasiUnknown_SearchBy_NamaPelangganPt();
		// }
		// }
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorAppointment();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorWo();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorWo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPembayar();
			}
		}
		//else if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown=="Service - Full Payment Asuransi")
		//{	
		//}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_SearchBy_NomorSo();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan();
			}
		}
		//else if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown=="Bank Statement - Unknown")
		//{	
		//}
		//else if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown=="Bank Statement - Card")
		//{		
		//}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Interbranch - Penerima") {
			console.log("getDataBranchCB");
			$scope.getDataBranchCB();
		}
		Enable_SearchByCariBtn();//soal kalo radio diklik pasti nongol
	};

	$scope.LihatAlokasiUnknown_getDataEDC = function () {
		console.log("GetDataBank");
		var dataBank = [];
		AlokasiUnknownFactory.getDataEDC()
			.then(
				function (res) {
					$scope.loading = false;
					angular.forEach(res.data.Result, function (value, key) {
						dataBank.push({ name: value.EDCName, value: value.BankId });
					});
					$scope.optionLihatAlokasiUnknown_Top_NamaBank = dataBank;
					console.log($scope.optionLihatAlokasiUnknown_Top_NamaBank);
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	}

	$scope.TambahAlokasiUnknown_getDataEDC = function () {
		console.log("GetDataBank");
		var dataBank = [];
		AlokasiUnknownFactory.getDataEDC()
			.then(
				function (res) {
					$scope.loading = false;
					angular.forEach(res.data.Result, function (value, key) {
						dataBank.push({ name: value.EDCName, value: value.BankId });
					});

					$scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions = dataBank;
					$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahAlokasiUnknown_BankTransfer_BankPengirimOptions[0];
					$scope.TambahAlokasiUnknown_CreditDebitCard_NamaBankOptions = dataBank;
					$scope.TambahAlokasiUnknown_RincianPembayaran_NamaBank_CreditDebitCard = $scope.TambahAlokasiUnknown_CreditDebitCard_NamaBankOptions[0];
					$scope.optionsTambahAlokasiUnknown_SearchBy_NamaBank = dataBank;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	}


	//Alvin, begin
	$scope.TambahAlokasiUnknown_Interbranch_InformasiBranch_Selected_Changed = function () {
		if ($scope.TambahAlokasiUnknown_NamaBranch_SearchDropdown !== "") {
			AlokasiUnknownFactory.getDataBranch($scope.TambahAlokasiUnknown_NamaBranch_SearchDropdown)
				.then(
					function (res) {
						console.log("GetDataBranch")
						$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_Alamat = res.data.Result[0].Alamat;
						$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_KodeBranch = res.data.Result[0].OutletId;
						$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_KabupatenKota = res.data.Result[0].Kota;
						$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_Telepon = res.data.Result[0].Telepon;
					}
				);
		}
	}
	//Alvin, end
	
	$scope.TmpVendor = "";
	$scope.TambahAlokasiUnknown_SearchBy = function () {
		$scope.refresh = true;
		$timeout(function() {
			$scope.refresh = false;
		}, 1000);
		DisableAll_TambahAlokasiUnknown_InformasiPelanggan();
		DisableAll_TambahAlokasiUnknown_InformasiPembayaran();
		DisableAll_TambahAlokasiUnknown_DaftarSpk();
		DisableAll_TambahAlokasiUnknown_SalesOrder();
		DisableAll_TambahAlokasiUnknown_DaftarSalesOrderRadio();
		Disable_TambahAlokasiUnknown_KumpulanNomorRangka();
		Disable_TambahAlokasiUnknown_KumpulanWorkOrder();
		Disable_TambahAlokasiUnknown_DaftarArCreditDebit();
		$scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSpk_Brief_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill = "";
		//20170505, nuse, begin

		Enable_TambahAlokasiUnknown_SaveAndPrintButton();

		if (($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Unknown")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card")) {
			console.log("a");
			HideKuitansi();
			Disable_TambahAlokasiUnknown_BiayaBiaya();
		}
		else {
			console.log("b");
			//HideKuitansi();
			Enable_TambahAlokasiUnknown_BiayaBiaya();
		}

		$scope.getDataCharges();
		// $scope.getDataPreprinted();

		//20170505, nuse, end
		//20170520, alvin, begin
		console.log("getListUnusedUnknown", $scope.uiGridPageSize)
		$scope.getListUnusedUnknown(1, 100);
		//20170520, alvin, end

		console.log("TambahAlokasiUnknown_SearchBy");
		console.log($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown);

		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			$scope.ShowProspect = true;
			$scope.ShowNoPelanggan = false;

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPembayaran_Spk();
				Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
				$scope.getDataByNomorSPK($scope.TambahAlokasiUnknown_SearchBy_NomorSpk);

			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSpk_Detail();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_Spk();
				Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
				$scope.ShowModalSearchSales();
				//tampilkan grid
				//$scope.getDataBySalesName();
			}

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			$scope.ShowProspect = true;
			$scope.ShowNoPelanggan = false;

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
				Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem();
				//20170430, NUSE EDIT, begin
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahAlokasiUnknown_UnitFullPaymentSetData();
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_Paging(1);
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_SetColumn();
				//20170430, NUSE EDIT, end
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSpk_Brief();
				$scope.ShowModalSearchProspect();
				//20170430, NUSE EDIT, begin
				//$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_Paging(1);
				//20170430, NUSE EDIT, end
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			$scope.ShowProspect = false;
			$scope.ShowNoPelanggan = true;
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
				Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem();
				//20170430, NUSE EDIT, begin
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahAlokasiUnknown_UnitFullPaymentSetData();
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_Paging(1);
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_SetColumn();
				//20170430, NUSE EDIT, end
			}
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSpk_Brief();
				$scope.ShowModalSearchProspect();
				//20170430, NUSE EDIT, begin
				//$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_Paging(1);
				//20170430, NUSE EDIT, end
			}
		}
		//20170527, alvin, begin			
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {

			Enable_TambahAlokasiUnknown_KumpulanNomorRangka();
			$scope.GetListProformaInvoice(1, uiGridPageSize);
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {

			Enable_TambahAlokasiUnknown_KumpulanARRefundLeasing();
			$scope.GetListRefundLeasing(1, uiGridPageSize);
		}
		//20170527, alvin, end		
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
				//20170414, eric, begin
				$scope.getSalesOrdersBySONumber()
				//20170414, eric, end
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo();
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName(1, uiGridPageSize);
				Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
				//20170414, eric, end
			}
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi") {

		// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
		// Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
		// Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
		// //20170414, eric, begin
		// $scope.getSalesOrdersBySONumber()
		// //20170414, eric, end
		// }
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
		// Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo();
		// //20170414, eric, begin
		// $scope.getSalesOrdersByCustomerName(1,10);
		// Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
		// Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();				
		// //20170414, eric, end
		// }
		// }
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Toyota();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
				//20170425, eric, begin
				$scope.getSalesOrdersBySONumber1()
				//20170425, eric, end
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill();
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				//$scope.getSalesOrdersDibayarByCustomerName1(1,10);
				//20170414, eric, end
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Toyota();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
				//20170425, eric, begin
				$scope.getSalesOrdersBySONumber1()
				//20170425, eric, end
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill();
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				//$scope.getSalesOrdersDibayarByCustomerName1(1,10);
				//20170414, eric, end
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			Enable_TambahAlokasiUnknown_InformasiPelangan_Toyota();
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPembayaran_Appointment();
				$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
				console.log("22");
				$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalWO = false;
				$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = true;

				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahAlokasiUnknown_SearchBy_NomorAppointment, "Appointment^Service - Down Payment");
				//hilman,end
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_InformasiPembayaran_WoDown();
				$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns();
				$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalWO = true;
				$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = false;

				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahAlokasiUnknown_SearchBy_NomorWo, "WO^Service - Down Payment");
				//hilman,end
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPelangan_Toyota();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_WoBill();
				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahAlokasiUnknown_SearchBy_NomorWo, "WO^Service - Full Payment");
				//hilman,end

			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_KumpulanWorkOrder();
				//hilman,begin
				var oldVendor = angular.copy($scope.TambahAlokasi_OtherTypeNamaPembayaran);
				if($scope.TmpVendor == ""){
					$scope.TmpVendor = oldVendor;
				}
				if($scope.TmpVendor != oldVendor){
					$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data = [];
					$scope.TmpVendor = oldVendor;
				}
				$scope.getDataPaymentByCustName(1, $scope.TambahAlokasi_OtherTypeNamaPembayaran, "NamaPembayar^Service - Full Payment");
				//hilman,end
			}
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment Asuransi") {
		// Enable_TambahAlokasiUnknown_KumpulanWorkOrder();
		// }
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {

			//Marthin, 20170430, begin
			// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
			// Enable_TambahAlokasiUnknown_InformasiPembayaran_SoDown();
			// Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
			// }
			// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
			// Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo();

			// }
			//console.log("DPP");
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoDown();
				Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
				$scope.getSalesOrderBySONumberParts();
			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo();
				Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_Materai();
				$scope.getSalesOrdersByCustomerNameExt(1, uiGridPageSize);
			}
			//Marthin, 20170430, end
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			//Marthin, 20170430, begin		
			// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
			// Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
			// Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
			// }
			// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
			// Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill();
			// }

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.getSalesOrderBySONumberParts();
				Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
				Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();

			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				//$scope.getSalesOrdersByCustomerNameExt(1,10);
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill();
			}
			//Marthin, 20170430, end			
		}
		//else if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown=="Bank Statement - Unknown")
		//{	
		//}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card") {
			console.log("Bank Statement - Card");
			Enable_TambahAlokasiUnknown_DaftarArCreditDebit();
			$scope.GetListUnknownCards(1, uiGridPageSize);
		}
		//else if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown=="Interbranch - Penerima")
		//{	
		//}	
	};

	$scope.Show_TambahAlokasiUnknown_DaftarSpk_Detail_ValueChanged = function () {
		//$scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio;
		//alert($scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio);
		if ($scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio != "") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
				if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
					//Enable_TambahAlokasiUnknown_DaftarSpk_Detail();
					Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
					Enable_TambahAlokasiUnknown_InformasiPembayaran_Spk();//ini kosong
				}

			}
		}

	};

	$scope.Show_TambahAlokasiUnknown_DaftarSpk_Brief_ValueChanged = function () {
		if ($scope.TambahAlokasiUnknown_DaftarSpk_Brief_Radio != "") {

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
				if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
					Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
					Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem();
				}

			}
			else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
				if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
					Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
					Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem();
				}

			}
		}

	};

	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_ValueChanged = function () {
		if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo != "") {

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
				if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
					Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
					Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
				}

			}
			// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi") {
			// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
			// Enable_TambahAlokasiUnknown_InformasiPelangan_Branch();
			// Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill();
			// }

			// }

		}

	};

	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_ValueChanged = function () {
		if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo != "") {

			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
				if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
					Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch();
					Enable_TambahAlokasiUnknown_InformasiPembayaran_Materai();
				}
			}
		}
	};

	$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar_ValueChanged = function () {
		console.log("LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar_ValueChanged");
		//console.log(LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar);
		//if ($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar != "") {
		//	$scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown = $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar;
		Enable_LihatAlokasiUnknown_FootKuitansi();
		Enable_LihatAlokasiUnknown_RincianPembayaran();
		//alert(nanti jangan lupa program biar value ni tabel nongol);
		//}
	};

	$scope.AssigmentUnknownControl = false;

	//Tambahan OI OE
	$scope.getOI = function() {
        AlokasiUnknownFactory.getDataOI().then(
            function(res){
				$scope.valueOI = res.data[0].MaxAmount;
				console.log('$scope.valueOI',$scope.valueOI);
                // $scope.loading=false;
                // return res.data;
			}
			// ,
            // function(err){
            //     bsNotify.show(
			// 		{
			// 			title: "gagal",
			// 			content: "Data tidak ditemukan.",
			// 			type: 'danger'
			// 		}
			// 	);
            // }
        );
	}
	
	$scope.getOE = function() {
        AlokasiUnknownFactory.getDataOE().then(
            function(res){
				$scope.valueOE = res.data[0].MaxAmount;
				console.log('$scope.valueOE',$scope.valueOE);
                // $scope.loading=false;
                // return res.data;
			}
			// ,
            // function(err){
            //     bsNotify.show(
			// 		{
			// 			title: "gagal",
			// 			content: "Data tidak ditemukan.",
			// 			type: 'danger'
			// 		}
			// 	);
            // }
        );
	}
	$scope.controlNominal = false;
	$scope.cekNominal = function (dataNominal) {
		$scope.controlNominal = false;
		if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' && dataNominal > $scope.valueOI){
			$scope.controlNominal = true;
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Nominal melebihi maksimal master OI sebesar Rp " +$scope.valueOI,
							type: 'warning'
						}
					);
		}else if($scope.selectTipeBiaya == 'Biaya Lain-lain' && dataNominal > $scope.valueOE) {
			$scope.controlNominal = true;
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OE sebesar Rp " +$scope.valueOE,
					type: 'warning'
				}
			);

		}else{
			$scope.controlNominal = false;
		}
	}

	
	//End Tambahan OI OE

	$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya_Selected_Changed = function (selected) {
		$scope.selectTipeBiaya = angular.copy(selected);
		if (selected == 'Biaya Lain-lain' || selected == 'Pendapatan Lain-lain' || selected == 'Titipan') {
			$scope.AssigmentUnknownControl = true;
		}else{
			$scope.AssigmentUnknownControl = false;
		}
		$scope.TambahAlokasiUnknown_SetupBiaya();

		if ($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "PPN Keluaran") {
			Enable_TambahAlokasiUnknown_Biaya_NoFaktur();
		}
		else if ($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Bea Materai") {
			if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				$scope.currentNominalPembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer
			}
			else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Cash") {
				$scope.currentNominalPembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_Cash
			}
			else if ($scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
				$scope.currentNominalPembayaran = $scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_CreditDebitCard
			}
			console.log("getNominalMaterai1");
			$scope.getNominalMaterai($scope.Pembayaran);
			Disable_TambahAlokasiUnknown_Biaya_NoFaktur();
		}
		else {
			Disable_TambahAlokasiUnknown_Biaya_NoFaktur();
		}
		//gonta ganti assignment sesuai yg dipilih
		if ($scope.selectedTypeIncoming == "Unit - Booking Fee"){
			// if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy != 2){
			
			// 	$scope.assignmentData = [];
			// // $scope.assignmentData.push($scope.pilihNoSPK);
			// $scope.assignmentData = $scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data;
			// console.log('$scope.assignmentData',$scope.assignmentData);
			// }else
			// TambahAlokasiUnknown_InformasiPembayaran_UIGrid
			$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data;
			console.log('$scope.assignmentData',$scope.assignmentData);			
			if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NomorSPK;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SPKId;
			}
		}
			
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}else
		if ($scope.selectedTypeIncoming == "Unit - Down Payment" || $scope.selectedTypeIncoming == "Unit - Full Payment"){
			$scope.assignmentData = $scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Unit - Order Pengurusan Dokumen"){
			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.assignmentData = $scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data;
			}
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Unit - Purna Jual"){
			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3"){
				$scope.assignmentData = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data;
			}
			// $scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Parts - Down Payment"){
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "2") {
				$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data;
			}
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Parts - Full Payment"){
			if($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == 2){
				$scope.assignmentData = $scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data;
			}
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Service - Down Payment"){
			// $scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].WoNo;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].NoWO;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Service - Full Payment"){
			 $scope.assignmentData = $scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NoBilling;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].NoWO;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}
		else
		if ($scope.selectedTypeIncoming == "Unit - Payment (Kuitansi Penagihan)"){
			 $scope.assignmentData = $scope.TambahAlokasiUnknown_DaftarNoRangka_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NoSO;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SoId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}
		
		// TambahIncomingPayment_DaftarNoRangka_UIGrid
		//end gonta ganti assignment sesuai yg dipilih
	}

	$scope.cekNominal = function (dataNominal) {
		// $scope.controlNominal = false;
		
		if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' && dataNominal > $scope.valueOI){
			$scope.controlNominal = true;
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Nominal melebihi maksimal master OI sebesar Rp " +$scope.valueOI,
							type: 'warning'
						}
					);
		}else if($scope.selectTipeBiaya == 'Biaya Lain-lain' && dataNominal > $scope.valueOE) {
			$scope.controlNominal = true;
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OE sebesar Rp " +$scope.valueOE,
					type: 'warning'
				}
			);

		}else{
			$scope.controlNominal = false;
		}
	}

	$scope.LihatAlokasiUnknown_RincianPembayaran_TipeAlokasiUnknown_Selected_Changed = function () {
		

		DisableAll_LihatAlokasiUnknown_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatAlokasiUnknown_InformasiPembayaran();//ini semua info pebayaran di hide
		Disable_LihatAlokasiUnknown_Interbranch_InformasiBranch();
		Disable_LihatAlokasiUnknown_DaftarNoRangkaDibayar();
		Disable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		Disable_LihatAlokasiUnknown_DaftarArCreditDebit();
		//Disable_LihatAlokasiUnknown_RincianPembayaran();
		//Disable_LihatAlokasiUnknown_FootKuitansi();
		//$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_SelectedDaftar="";
		Disable_LihatAlokasiUnknown_DaftarSalesOrder();
		Disable_LihatAlokasiUnknown_DaftarWorkOrderDibayar();
		Disable_LihatAlokasiUnknown_Top_NomorSpk();
		Disable_LihatAlokasiUnknown_Top_NamaLeasing();
		Disable_LihatAlokasiUnknown_Top_NomorSo();
		Disable_LihatAlokasiUnknown_Top_NomorWo();
		Disable_LihatAlokasiUnknown_Top_NamaAsuransi();
		Disable_LihatAlokasiUnknown_Top_NamaBank();

		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {

			Enable_LihatAlokasiUnknown_Top_NomorSpk();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Prospect();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_Spk();

		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			Enable_LihatAlokasiUnknown_Top_NomorSpk();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Prospect();
			Enable_LihatAlokasiUnknown_DaftarSalesOrder();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			Enable_LihatAlokasiUnknown_Top_NomorSpk();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Prospect();
			Enable_LihatAlokasiUnknown_DaftarSalesOrder();
		}
		//20170527, Alvin, begin		
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
			Enable_LihatAlokasiUnknown_Top_NamaLeasing();
			Enable_LihatAlokasiUnknown_DaftarNoRangkaDibayar();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
			Enable_LihatAlokasiUnknown_Top_NamaLeasing();
			//Enable_LihatAlokasiUnknown_KumpulanARRefundLeasing();
			Enable_LihatAlokasiUnknown_DaftarNoRangkaDibayar();
		}
		//20170527, Alvin, end		
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			console.log('111b');
			Enable_LihatAlokasiUnknown_Top_NomorSo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Branch();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill();
		}
		// else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi") {
		// Enable_LihatAlokasiUnknown_Top_NomorSo();
		// Enable_LihatAlokasiUnknown_InformasiPelangan_Branch();
		// Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill();
		// }
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			Enable_LihatAlokasiUnknown_Top_NomorSo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Toyota();
			// Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill();
			Enable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			Enable_LihatAlokasiUnknown_Top_NomorSo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Toyota();
			// Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill();
			Enable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {

			Enable_LihatAlokasiUnknown_InformasiPelangan_Toyota();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_Appointment();
			//Alvin, 20170514, begin
			Enable_LihatAlokasiUnknown_InformasiPembayaran_WoDown();
			//Alvin, 20170514, end
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			Enable_LihatAlokasiUnknown_Top_NomorWo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_Toyota();
			//Alvin, 20170514, begin
			Enable_LihatAlokasiUnknown_InformasiPembayaran_WoBill();
			//Alvin, 20170514, end
			Enable_LihatAlokasiUnknown_DaftarWorkOrderDibayar();
		}
		// else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment Asuransi") {

		// //Enable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		// Enable_LihatAlokasiUnknown_Top_NamaAsuransi();
		// Enable_LihatAlokasiUnknown_DaftarWorkOrderDibayar();
		// }
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			Enable_LihatAlokasiUnknown_Top_NomorSo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_PelangganBranch();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_SoDown();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_Materai();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			Enable_LihatAlokasiUnknown_Top_NomorSo();
			Enable_LihatAlokasiUnknown_InformasiPelangan_PelangganBranch();
			Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill();
			Enable_LihatAlokasiUnknown_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Unknown") {

			//cari apa isinya
			//emang kosong
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card") {
			Enable_LihatAlokasiUnknown_Top_NamaBank();
			Enable_LihatAlokasiUnknown_DaftarArCreditDebit();
		}
		else if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Interbranch - Penerima") {
			Enable_LihatAlokasiUnknown_Interbranch_InformasiBranch();
		}

		$scope.TambahAlokasiUnknown_SetupBiaya();
	};

	//========================================20170430, NUSE EDIT, BEGIN========================================
	$scope.TambahAlokasiUnknown_UnitFullPaymentSetData = function () {
		AlokasiUnknownFactory.getCustomerDataBySPKId($scope.TambahAlokasiUnknown_SearchBy_NomorSpk)
			.then(
				function (res) {
					console.log("Customer by SPK Id ==>", res.data.Result);
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Phone;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Address;
					$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.currentCustomerId = res.data.Result[0].CustomerId;
				}
			);
	}

	$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		showColumnFooter: true,
		/*columnDefs: [
						{name:"SOId",field:"SOId",visible:false},
						{name:"Nomor SO",field:"SONumber"},
						{name:"Tanggal SO",field:"SODate"},
						{name:"Nominal Billing", field:"TotalNominal"},
						{name:"Biaya Materai",field:"Materai"},
						{name:"Total yang Harus Dibayar", field:"TotalAllMustBePay"},
						{name:"Total terbayar", field:"PaidNominal"},
						{name:"Sisa pembayaran", field:"TotalPaymentMustBePay"},
						{name:'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
								enableCellEdit: true, type: 'number'},
					],*/
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSO = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = $scope.TambahAlokasiUnknown_UnitFullPaymentSO_Paging(pageNumber);
			});
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				if($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment"){
					console.log("Test masuk Sini Unit DP")
					console.log("test", rowEntity);
					console.log("colDef ", colDefs);
					if(newValue > rowEntity.TotalDPMustBePay){
						var newValue = 0;   
						rowEntity[colDefs.name] = newValue;
							bsNotify.show(
								{
									title: "Peringatan",
									content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
									type: 'warning'
								}
							); 
					}
				}
			});
		}
	};

	$scope.TambahAlokasiUnknown_UnitFullPaymentSO_SetColumn = function () {
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {
			console.log($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown);
			console.log("1");
			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.columnDefs = [
				{ name: "SOId", field: "SOId", visible: false },
				{ name: "Nomor SO", displayName: "Nomor SO", field: "SONumber" },
				{ name: "Tanggal SO", displayName: "Tanggal SO", field: "SODate", cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: "Nominal Billing", field: "TotalNominal", cellFilter: 'rupiahC' },
				//{name:"Biaya Materai",field:"Materai", cellFilter: 'rupiahC'},
				{ name: "Total yang Harus Dibayar", field: "TotalAllMustBePay", cellFilter: 'rupiahC', visible: false },
				{ name: "Total terbayar", field: "PaidNominal", cellFilter: 'rupiahC' },
				{ name: "Saldo", field: "TotalPaymentMustBePay", cellFilter: 'rupiahC' },
				{
					name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
					enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0', cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}
				},
			];
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			console.log($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown);
			console.log("2");
			$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.columnDefs = [
				{ name: "SOId", field: "SOId", visible: false },
				{ name: "Nomor SO", displayName: "Nomor SO", field: "SONumber" },
				{ name: "Tanggal SO", displayName: "Tanggal SO", field: "SODate", cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: "Nominal Down Payment", field: "NominalDP", cellFilter: 'rupiahC' },
				//{name:"Biaya Materai",field:"Materai", cellFilter: 'rupiahC'},
				{ name: "Total yang Harus Dibayar", field: "TotalAllDPMustPay", cellFilter: 'rupiahC', visible: false },
				{ name: "Total terbayar", field: "PaidDP", cellFilter: 'rupiahC' },
				{ name: "Saldo", field: "TotalDPMustBePay", cellFilter: 'rupiahC' },
				{
					name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
					enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0', cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}
				},
			];
		}
	}

	$scope.TambahAlokasiUnknown_UnitFullPaymentSO_Paging = function (pageNumber) {
		AlokasiUnknownFactory.getSalesOrderBySPKId(pageNumber, $scope.uiGridPageSize, $scope.TambahAlokasiUnknown_SearchBy_NomorSpk, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					console.log("Get data Sales Order", res.data.result);
					$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.totalItems = res.data.Total;
					console.log($scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.totalItems);
					$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];

				}
			);
	}


	$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "SPKId", field: "SPKId", visible: false },
			{ name: "Nomor SPK", displayName: "Nomor SPK", field: "NomorSPK" },
			{ name: "Model", field: "Model" },
			{ name: "Tipe", field: "Tipe" },
			{ name: "Warna", field: "Warna" },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSPK = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.data = $scope.TambahAlokasiUnknown_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.TambahAlokasiUnknown_SearchBy_NomorSpk = row.entity.NomorSPK;
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahAlokasiUnknown_UnitFullPaymentSetData();
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_Paging(1);
				$scope.TambahAlokasiUnknown_UnitFullPaymentSO_SetColumn();
				Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect();
				Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem();
			}
			)
		}
	};

	$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_Paging = function (pageNumber) {
		console.log("masuk paging spk");
		AlokasiUnknownFactory.getSPKByCustomerName(pageNumber, $scope.uiGridPageSize,
			$scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan, $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown)
			.then(
				function (res) {
					$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.data = res.data.Result;
					$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.totalItems = res.data.Total;
					$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				}
			);
	}

	$scope.TambahAlokasiUnknown_FullPaymentSOData = function () {
		$scope.TambahAlokasiUnknown_SOSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data, function (value, key) {
			var obj;
			$scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment" ? 
			obj = { "SOId": value.SOId, "PaidAmount": value.Pembayaran, "SODownPayment": value.NominalDP } : obj = { "SOId": value.SOId, "PaidAmount": value.Pembayaran } ;
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_SOSubmitData.push(obj);
		}
		);
	}
	//=========================================20170430, NUSE EDIT, END=========================================


	//Alvin, 20170505, begin
	$scope.TambahAlokasiUnknown_ServiceFullPaymentWOData = function () {
		$scope.TambahAlokasiUnknown_ServiceWOSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.NoWO, "PaidAmount": value.SisaPembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_ServiceWOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_BookingFeeData = function () {
		$scope.TambahAlokasiUnknown_BookingFeeSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data, function (value, key) {
			//var obj = {"SOId" : value.SPKId, "PaidAmount" : value.NominalBookingFee};
			var obj = { "SOId": value.SPKId, "PaidAmount":  $scope.Pembayaran};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_BookingFeeSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_Biaya2 = function () {
		$scope.TambahAlokasiUnknown_BiayaData = [];
		angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
			var obj =
			{
				"BiayaId": value.BiayaId,
				"DK": value.DebitCredit,
				"PaidAmount": value.Nominal.toString().replace('.', ''),
				"Keterangan": value.Keterangan,
				"SOCode": value.Assignment,
				"NoFaktur": value.NoFaktur
			};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_BiayaData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_SalesOrder = function () {
		console.log("TambahAlokasiUnknown_SalesOrder");
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data, function (value, key) {
			var obj = {"SOId" : value.SOId, "PaidAmount" : value.SisaPembayaran};
			// var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}
	
	$scope.TambahAlokasiUnknown_SalesOrderOPDPJ = function () {
		console.log("TambahAlokasiUnknown_SalesOrder");
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data, function (value, key) {
			var obj = {"SOId" : value.SOId, "PaidAmount" : value.SisaPembayaran};
			// var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_SalesOrder2 = function () {
		console.log("TambahAlokasiUnknown_SalesOrder2");
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		var totalBiayaK = 0;
		var totalBiayaD = 0;
		angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
			if (value.DebitCredit == 'Kredit') {
				totalBiayaK = totalBiayaK + parseInt(value.Nominal);
			}else if (value.DebitCredit == 'Debet') {
				totalBiayaD = totalBiayaD + parseInt(value.Nominal);
			}
		});

		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran - totalBiayaK + totalBiayaD };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_SoDown = function () {
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		var totalBiayaK = 0;
		var totalBiayaD = 0;
		angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
			if (value.DebitCredit == 'Kredit') {
				totalBiayaK = totalBiayaK + parseInt(value.Nominal);
			}else if (value.DebitCredit == 'Debet') {
				totalBiayaD = totalBiayaD + parseInt(value.Nominal);
			}
		});

		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran - totalBiayaK + totalBiayaD };
			// var obj = { "SOId": value.SOId, "PaidAmount": value.SisaDP };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_SoBill = function () {
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		// var totalBiayaK = 0;
		// var totalBiayaD = 0;
		// angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
		// 	if (value.DebitCredit == 'Kredit') {
		// 		totalBiayaK = totalBiayaK + parseInt(value.Nominal);
		// 	}else if (value.DebitCredit == 'Debet') {
		// 		totalBiayaD = totalBiayaD + parseInt(value.Nominal);
		// 	}
		// });

		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran };// - totalBiayaK + totalBiayaD };
			//var obj = {"SOId" : value.SOId, "PaidAmount" : value.SisaPembayaran};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}

	//parts
	$scope.TambahAlokasiUnknown_SoBillParts = function () {
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		var totalBiayaK = 0;
		var totalBiayaD = 0;
		angular.forEach($scope.TambahAlokasiUnknown_Biaya_UIGrid.data, function (value, key) {
			if (value.DebitCredit == 'Kredit') {
				totalBiayaK = totalBiayaK + parseInt(value.Nominal);
			}else if (value.DebitCredit == 'Debet') {
				totalBiayaD = totalBiayaD + parseInt(value.Nominal);
			}
		});

		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran - totalBiayaK + totalBiayaD };
			//var obj = {"SOId" : value.SOId, "PaidAmount" : value.SisaPembayaran};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}
	
	$scope.TambahAlokasiUnknown_SoBillOPDPJ = function () {
		$scope.TambahAlokasiUnknown_PartsSOSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran };
			//var obj = {"SOId" : value.SOId, "PaidAmount" : value.SisaPembayaran};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_PartsSOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_Service_ByName = function () {
		$scope.TambahAlokasiUnknown_ServiceSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			//var obj = {"SOId" : value.NoWO, "PaidAmount" : value.SisaPembayaran};
			var obj = { "SOId": value.NoBilling, "PaidAmount": value.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_ServiceSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_Service_ByWO = function () {
		$scope.TambahAlokasiUnknown_ServiceSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.NoWO, "PaidAmount": value.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_ServiceSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_SoSoSetColumns = function () {
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			console.log("a");
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				//{ name: 'SPKId', field: 'SPKId', visible: false },
				//{ name: 'SOId', field: 'SOId', visible: false },		
				{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
				//{ name: 'Total Terbayar', field: 'PaidNominal' },	
			];
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			console.log("b");
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				{ name: 'SPKId', field: 'SPKId', visible: false },
				{ name: 'SOId', field: 'SOId', visible: false },
				{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
			];
		}
	}

	$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid_SetColumns = function () {
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			console.log("a1");
			$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'Tanggal', cellFilter: 'date:\'yyyy-MM-dd\'', visible: true },
					{ name: 'SPKId', field: 'SPKId', visible: false },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', cellFilter: 'rupiahC', visible: true },
					{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
					{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC', visible: false }
				];
			$scope.CurrentNominal = 0;
			console.log($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.totalItems);
			for (var k = 0; k < $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.totalItems; k++) {
				console.log($scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data[k].NominalBookingFee);
				$scope.CurrentNominal += $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data[k].NominalBookingFee;
			}
			console.log($scope.CurrentNominal);
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {
			console.log("b");
			$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'Tanggal', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', visible: false },
					{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'', visible: false },
					{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
					{ name: 'Keterangan', displayName: "", field: 'Keterangan', visible: true },
					{ name: 'Nominal', field: 'NominalDP', cellFilter: 'rupiahC', visible: true },
					{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: true },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC', visible: true },
					{
						name: 'Pembayaran', field: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum, enableCellEdit: true, type: 'number',
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}, cellFilter: 'rupiahC',
					}
				];
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {
			console.log("c");
			$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'TglWo', cellFilter: 'date:\'yyyy-MM-dd\'', visible: true },
					{ name: 'Nominal Billing', field: 'NominalDP', cellFilter: 'rupiahC', visible: true },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: true },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC', visible: true }
				];
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi"){
		// console.log("c");
		// $scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs =
		// [
		// { name:'Tanggal SO',    field:'TglWo',cellFilter: 'date:\'yyyy-MM-dd\'', visible: true },
		// { name:'Nominal Billing',    field:'NominalDP', cellFilter: 'rupiahC', visible: true},
		// { name:'Total Terbayar',    field:'PaidDP', cellFilter: 'rupiahC', visible: true},
		// { name:'Sisa Pembayaran',    field:'SisaDP', cellFilter: 'rupiahC', visible: true}
		// ];
		// }			
	};


	$scope.TambahAlokasiUnknown_Biaya_UIGrid =
		{
			enableSorting: false,
			onRegisterApi: function (gridApi) { },
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tipe Biaya', field: 'BiayaId' },
					{ name: 'Debet/Kredit', type: 'text', field: 'DebitCredit' },
					{ name: 'Assignment', type: 'text', field: 'Assignment' },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' },
					{ name: 'Keterangan', field: 'Keterangan', visible: false },
					{ name: 'NoFaktur', field: 'NoFaktur', visible: false },
					{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRowBiaya(row)">Delete</button>' },
				]
		};


	$scope.TambahAlokasiUnknown_BiayaBiaya_Tambah = function () {
		
		if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' && $scope.TambahAlokasiUnknown_BiayaBiaya_Nominal > $scope.valueOI){
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OI sebesar Rp " +$scope.valueOI,
					type: 'warning'
				}
			);
			return;

		}else if($scope.selectTipeBiaya == 'Biaya Lain-lain' && $scope.TambahAlokasiUnknown_BiayaBiaya_Nominal > $scope.valueOE) {
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OE sebesar Rp " +$scope.valueOE,
					type: 'warning'
				}
			);
			return;

		}


		for (var i in $scope.TambahAlokasiUnknown_Biaya_UIGrid.data){
			if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
				if($scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahAlokasiUnknown_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
					bsNotify.show({
						title: "Warning",
						content: "Sudah Ada Pendapatan Lain-lain atau Biaya Lain-lain!",
						type: 'warning'
					});
					return;
				}
			}
			
		}
		if (!$scope.TambahAlokasiUnknow_CheckBiaya())
			return;
		console.log($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya);
		console.log($scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit);
		var DebitCredit = "Debet";
		
		var found = 0;
		$scope.TambahAlokasiUnknown_Biaya_UIGrid.data.forEach(function (obj) {
			console.log('isi grid -->', obj.BiayaId);
			console.log('isi biaya -->', $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya);
			if ($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == obj.BiayaId)
				found = 1;
		});

		if (found == 0) {

			if (($scope.TambahAlokasiUnknown_BiayaBiaya_Assignment == null || $scope.TambahAlokasiUnknown_BiayaBiaya_Assignment == 'undefined' || typeof $scope.TambahAlokasiUnknown_BiayaBiaya_Assignment == 'undefined') && $scope.AssigmentUnknownControl == true ){
				bsNotify.show({
					title: "Warning",
					content: "Assignment harus diisi!",
					type: 'warning'
				});
			}else{
				var biaya = $scope.TambahAlokasiUnknown_BiayaBiaya_Nominal.toString().replace(/,/g, "");
				if($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Bea Materai" || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Biaya Bank"){
					$scope.TambahAlokasiUnknown_BiayaBiaya_Assignment = ''
				};
				$scope.TambahAlokasiUnknown_Biaya_UIGrid.data.push(
					{
						BiayaId: $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya,
						DebitCredit: $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit,
						Nominal: biaya,
						Keterangan: $scope.TambahAlokasiUnknown_BiayaBiaya_Keterangan,
						Assignment: $scope.TambahAlokasiUnknown_BiayaBiaya_Assignment,
						NoFaktur: $scope.TambahAlokasiUnknown_BiayaBiaya_NoFaktur
					})
			}
		}
		// $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya = "";
		// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "";
		// $scope.TambahAlokasiUnknown_BiayaBiaya_Keterangan = "";
		// $scope.TambahAlokasiUnknown_BiayaBiaya_Nominal = "";
	};

	$scope.TambahAlokasiUnknow_CheckBiaya = function () {
		var passed = false;

		if ($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "Titipan" && ($scope.TambahAlokasiUnknown_BiayaBiaya_Assignment == '' || $scope.TambahAlokasiUnknown_BiayaBiaya_Assignment == undefined)) {
			console.log("1b");
			// alert("Nominal Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Titipan Assignment Harus diisi",
				type: 'warning'
			});
			return (passed);
		}

		if (angular.isUndefined($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya) || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "") {
			console.log("1b");
			// alert("Tipe Biaya Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Field Tipe biaya lain-lain harus dipilih",
				type: 'warning'
			});
			return (passed);
		}
		if (angular.isUndefined($scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya) || $scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "" ||
		$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya == "0" || $scope.TambahAlokasiUnknown_BiayaBiaya_Nominal == "0") {
			console.log("1b");
			// alert("Nominal Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Field Nominal harus diisi",
				type: 'warning'
			});
			return (passed);
		}

		passed = true;

		return (passed);
	}

	$scope.deleteRowBiaya = function (row) {
		var index = $scope.TambahAlokasiUnknown_Biaya_UIGrid.data.indexOf(row.entity);
		$scope.TambahAlokasiUnknown_Biaya_UIGrid.data.splice(index, 1);
	};

	$scope.TambahAlokasiUnknown_SetupBiaya = function () {
		console.log("Setup Biaya");
		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]; 
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {

			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {

			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {

			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" } ];
			// if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
			// 	$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			// }else{
			// 	$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			// }
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain'|| $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			}else if($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			}
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]; //perbaikan
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";	 
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;
			}else{
				$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;
			}
			// $scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
		}
	}


	//Filter LihatAlokasiUnknown
	// $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Filter_Clicked = function () {
	// 	console.log("LihatAlokasiUnknown_DaftarAlokasiUnknown_Filter_Clicked");
	// 	if (($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text != "") &&
	// 		($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search != "")) {
	// 		var nomor;
	// 		var value = $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text;
	// 		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text);
	// 		if ($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search == "Nomor Alokasi Unknown") {
	// 			nomor = 4;
	// 		}
	// 		if ($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search == "Tipe Alokasi Unknown") {
	// 			nomor = 5;
	// 		}
	// 		if ($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search == "Nama Pelanggan") {
	// 			nomor = 6;
	// 		}
	// 		if ($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search == "Nominal") {
	// 			nomor = 8;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text;
	// 	}
	// 	else {
	// 		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	/// NEW FILTER LIHAT ==================
	$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Filter_Clicked = function () {
		
		if (($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text != "") &&
			($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search != null && $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search != undefined)) {
			var nomor;
			var value = $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text;
			
			AlokasiUnknownFactory.GetListUnknown(1, uiGridPageSize, $filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknown), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatAlokasiUnknown_TujuanPembayaran_TanggalAlokasiUnknownTo), 'yyyy-MM-dd'), $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text, $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search)
			.then(
				function (res) {
					console.log("data alokasi",res.data.Result);
					$scope.tmpFlag = true;
					$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.clearAllFilters();
					$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.getColumn($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search).filters[0].term = value;
						$timeout(function(){
							console.log("data 1", $scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.data);
							console.log("data 2", $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.core.getVisibleRows().length);
							
							$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.totalItems = $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.core.getVisibleRows().length;	
							$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.LihatAlokasiUnknown_DaftarIncoming_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						},100);
				},
				function (err) {
					console.log("err=>", err);
				}
			)
			
		}else{
			$scope.tmpFlag = false;
			$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.grid.clearAllFilters();
			$scope.GetListUnknown(1, uiGridPageSize);

		}
	}

	/// =============================

	//Filter Full Payment Input Name
	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchText != "") &&
			($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchText;
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchText);
			if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchDropdown == "Nomor SO") {
				nomor = 4;
			}
			if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchDropdown == "Nomor Billing") {
				nomor = 6;
			}
			console.log("nomor --> ", nomor);
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchText;
		}
		else {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	//Filter Service Full Payment Input Name
	// $scope.TambahAlokasiUnknown_DaftarWorkOrder_Filter_Clicked = function () {
	// 	console.log("TambahAlokasiUnknown_DaftarWorkOrder_Filter_Clicked");
	// 	if (($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText != "") &&
	// 		($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText;
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText);
	// 		if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Nomor WO") {
	// 			nomor = 1;
	// 		}
	// 		if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Tanggal WO") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Nomor Billing") {
	// 			nomor = 3;
	// 		}
	// 		if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Nomor Polisi") {
	// 			nomor = 4;
	// 		}
	// 		if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Nama Pelanggan") {
	// 			nomor = 5;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText;
	// 	}
	// 	else {
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	/// filter baru ====================================
	$scope.TambahAlokasiUnknown_DaftarWorkOrder_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarWorkOrder_Filter_Clicked");
			if (($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText != "") &&
				($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown != "")) {
				var nomor;
				value = $scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText;
				$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
				$scope.gridApi_InformasiPembayaran_UIGrid2.grid.getColumn($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown).filters[0].term = value;
				console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText);
				// if ($scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchDropdown == "Tanggal Pembayaran") {
				// 	nomor = 3;
				// }
				// console.log("nomor --> ", nomor);
				// $scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.value;
			}
			else {
				$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
				//alert("inputan filter belum diisi !");
			}
	}
	$scope.changeFormatDate = function(item) {
		var tmpAppointmentDate = item;
		console.log("changeFormatDate item", item);
		tmpAppointmentDate = new Date(tmpAppointmentDate);
		var finalDate
		if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
			var yyyy = tmpAppointmentDate.getFullYear().toString();
			var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = tmpAppointmentDate.getDate().toString();
			finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
		} else {
			finalDate = '';
		}
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	};
	// =================================================

	//Filter Proforma Invoice
	$scope.TambahAlokasiUnknown_DaftarNoRangka_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarNoRangka_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarNoRangka_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarNoRangka_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarNoRangka_Text;
			$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangka_UIGrid.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarNoRangka_Text);
			if ($scope.TambahAlokasiUnknown_DaftarNoRangka_SearchDropdown == "Nomor Rangka") {
				nomor = 3;
			}
			if ($scope.TambahAlokasiUnknown_DaftarNoRangka_SearchDropdown == "Nomor SPK") {
				nomor = 4;
			}
			if ($scope.TambahAlokasiUnknown_DaftarNoRangka_SearchDropdown == "Nomor SO") {
				nomor = 5;
			}
			if ($scope.TambahAlokasiUnknown_DaftarNoRangka_SearchDropdown == "Nama di Billing") {
				nomor = 6;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangka_UIGrid.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarNoRangka_Text;
		}
		else {
			$scope.gridApi_TambahAlokasiUnknown_DaftarNoRangka_UIGrid.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarSpk_Brief_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarSpk_Brief_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarSpk_Brief_Search_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarSpk_Brief_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarSpk_Brief_Search_Text;
			$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSPK.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarSpk_Brief_Search_Text);
			if ($scope.TambahAlokasiUnknown_DaftarSpk_Brief_SearchDropdown == "Nomor SPK") {
				nomor = 1;
			}
			if ($scope.TambahAlokasiUnknown_DaftarSpk_Brief_SearchDropdown == "Nama Pelanggan") {
				nomor = 2;
			}
			if ($scope.TambahAlokasiUnknown_DaftarSpk_Brief_SearchDropdown == "Semua Kolom") {
				nomor = 0;
			}

			console.log("nomor --> ", nomor);
			if (nomor == 0) {
				console.log("masuk 0");
				$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSPK.grid.columns.filters[0].term = $scope.TambahAlokasiUnknown_DaftarSpk_Brief_Search_Text;
			}
			else {
				console.log("masuk else");
				$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSPK.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarSpk_Brief_Search_Text;
			}
		}
		else {
			$scope.GridApiTambahAlokasiUnknown_UnitFullPaymentSPK.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarSpk_Detail_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarSpk_Detail_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarSpk_Detail_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text;
			$scope.gridApi_DaftarSpk.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text);
			if ($scope.TambahAlokasiUnknown_DaftarSpk_Detail_SearchDropdown == "Nomor SPK") {
				nomor = 1;
			}
			else if ($scope.TambahAlokasiUnknown_DaftarSpk_Detail_SearchDropdown == "Nama Pelanggan") {
				nomor = 2;
			}
			// if ($scope.TambahAlokasiUnknown_DaftarSpk_Brief_SearchDropdown == "Semua Kolom") {
			else {
				nomor = 0;
			}
			console.log($scope.TambahAlokasiUnknown_DaftarSpk_Detail_SearchDropdown);
			console.log("nomor --> ", nomor);
			if (nomor == 0) {
				console.log("masuk 0");
				$scope.gridApi_DaftarSpk.grid.columns.filters[1].term = $scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text;
				$scope.gridApi_DaftarSpk.grid.columns.filters[2].term = $scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text;
			}
			else {
				console.log("masuk else");
				$scope.gridApi_DaftarSpk.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarSpk_Detail_Search_Text;
			}
		}
		else {
			$scope.gridApi_DaftarSpk.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Text;
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Text);
			if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_SearchDropdown == "Nomor SO") {
				nomor = 3;
			}
			// if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_SearchDropdown == "Metode Bayar") {
			// nomor = 4;
			// }
			console.log("nomor --> ", nomor);
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_Text;
		}
		else {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_button_SearchBy = function () {
		console.log("TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_button_SearchBy");
		if (($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search_Text;
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_GridApi.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search_Text);
			if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_SearchDropdown == "Nomor SO") {
				nomor = 4;
			}
			// if ($scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_SearchDropdown == "Metode Bayar") {
			// nomor = 4;
			// }
			console.log("nomor --> ", nomor);
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_GridApi.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_Search_Text;
		}
		else {
			$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_GridApi.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarArCreditDebit_Click = function () {
		console.log("TambahAlokasiUnknown_DaftarArCreditDebit_Click");
		if (($scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchText != "") &&
			($scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchText;
			$scope.TambahAlokasiUnknown_DaftarArCreditDebit_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchText);
			if ($scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Tanggal AR Card") {
				nomor = 1;
			}
			if ($scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Tipe Kartu") {
				nomor = 2;
			}
			if ($scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Nominal") {
				nomor = 3;
			}
			console.log("nomor --> ", nomor);
			$scope.TambahAlokasiUnknown_DaftarArCreditDebit_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarArCreditDebit_SearchText;
		}
		else {
			$scope.TambahAlokasiUnknown_DaftarArCreditDebit_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.LihatAlokasiUnknown_DaftarArCreditDebit_Click = function () {
		console.log("LihatAlokasiUnknown_DaftarArCreditDebit_Click");
		if (($scope.LihatAlokasiUnknown_DaftarArCreditDebit_Search_Text != "") &&
			($scope.LihatAlokasiUnknown_DaftarArCreditDebit_SearchDropdown != "")) {
			var nomor;
			var value = $scope.LihatAlokasiUnknown_DaftarArCreditDebit_Search_Text;
			$scope.LihatAlokasiUnknown_DaftarAR_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.LihatAlokasiUnknown_DaftarArCreditDebit_Search_Text);
			if ($scope.LihatAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Tanggal AR Card") {
				nomor = 1;
			}
			if ($scope.LihatAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Tipe Kartu") {
				nomor = 2;
			}
			if ($scope.LihatAlokasiUnknown_DaftarArCreditDebit_SearchDropdown == "Nominal") {
				nomor = 3;
			}
			console.log("nomor --> ", nomor);
			$scope.LihatAlokasiUnknown_DaftarAR_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatAlokasiUnknown_DaftarArCreditDebit_Search_Text;
		}
		else {
			$scope.LihatAlokasiUnknown_DaftarAR_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarARRefundLeasing_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarARRefundLeasing_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarARRefundLeasing_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarARRefundLeasing_Text;
			$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarARRefundLeasing_Text);
			if ($scope.TambahAlokasiUnknown_DaftarARRefundLeasing_SearchDropdown == "Nomor SO") {
				nomor = 6;
			}
			if ($scope.TambahAlokasiUnknown_DaftarARRefundLeasing_SearchDropdown == "Nomor SPK") {
				nomor = 5;
			}
			if ($scope.TambahAlokasiUnknown_DaftarARRefundLeasing_SearchDropdown == "Nomor Rangka") {
				nomor = 4;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarARRefundLeasing_Text;
		}
		else {
			$scope.gridApi_TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahAlokasiUnknown_DaftarInterbranch_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarInterbranch_Filter_Clicked");
		if (($scope.TambahAlokasiUnknown_DaftarInterbranch_Search_Text != "") &&
			($scope.TambahAlokasiUnknown_DaftarInterbranch_Search != "")) {
			var nomor;
			var value = $scope.TambahAlokasiUnknown_DaftarInterbranch_Search_Text;
			$scope.gridApi_Interbranch.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarInterbranch_Search_Text);
			if ($scope.TambahAlokasiUnknown_DaftarInterbranch_Search == "Nama Branch Penerima") {
				nomor = 1;
			}
			if ($scope.TambahAlokasiUnknown_DaftarInterbranch_Search == "Nomor Alokasi Unknown") {
				nomor = 2;
			}
			if ($scope.TambahAlokasiUnknown_DaftarInterbranch_Search == "Tanggal Alokasi Unknown") {
				nomor = 3;
			}
			if ($scope.TambahAlokasiUnknown_DaftarInterbranch_Search == "Nominal") {
				nomor = 4;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_Interbranch.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarInterbranch_Search_Text;
		}
		else {
			$scope.gridApi_Interbranch.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};


	// $scope.TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked = function () {
	// 	console.log("TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked");
	// 	if (($scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text != "") &&
	// 		($scope.TambahAlokasiUnknown_DaftarUnknown_Search != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text;
	// 		$scope.TambahAlokasiUnknown_DaftarUnknown_gridAPI.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text);
	// 		if ($scope.TambahAlokasiUnknown_DaftarUnknown_Search == "Tanggal Pembayaran") {
	// 			nomor = 3;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.TambahAlokasiUnknown_DaftarUnknown_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiUnknown_DaftarUnknown_Search_Text;
	// 	}
	// 	else {
	// 		$scope.gridApi_Interbranch.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	// New FILTER =================================
	$scope.TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked = function () {
		$scope.getListUnusedUnknown(1, 100);
	}
	// ============================================
	$scope.TambahAlokasiUnknown_NoRangka = function () {
		$scope.TambahAlokasiUnknown_NoRangkaSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.ProformaInvoiceId, "PaidAmount": value.Nominal };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_NoRangkaSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_RefundLeasing = function () {
		$scope.TambahAlokasiUnknown_RefundLeasingSubmitData = [];
		angular.forEach($scope.TambahAlokasiUnknown_DaftarARRefundLeasingDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.RefundLeasingId, "PaidAmount": value.NominalRefund };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiUnknown_RefundLeasingSubmitData.push(obj);
		}
		);
	}

	$scope.TambahAlokasiUnknown_ClearAllData = function () {
		//Informasi
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_NamaPelanggan = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_ToyotaId = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_TanggalAppointment = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Handphone = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_Alamat = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Toyota_KabupatenKota = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KodeBranch = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Alamat = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaBranch = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_KabupatenKota = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_NamaPt = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Branch_Telepon = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_ProspectId = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Alamat = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_NamaPelanggan = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_KabupatenKota = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Prospect_Handphone = "";
		//Grid
		$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarWorkOrder_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarsalesOrderDibayar_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarSpk_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_UIGrid.data = [];
		//Pembayaran
		//$scope.TambahAlokasiUnknown_TanggalAlokasiUnknown= "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_BankTransfer = 0;
		$scope.TambahAlokasiUnknown_RincianPembayaran_Selisih_BankTransfer = 0;
		$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_BankTransfer = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_Keterangan_BankTransfer = "";
		$scope.TambahAlokasiUnknown_BankTransfer_RekeningPenerima = "";
		$scope.TambahAlokasiUnknown_BankTransfer_BankPengirim = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_NamaPengirim_BankTransfer = "";
		$scope.TambahAlokasiUnknown_OwnRisk = false;
		$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_Cash = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_Keterangan_Cash = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_Cash = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_Cash = 0;
		$scope.TambahAlokasiUnknown_RincianPembayaran_NominalPembayaran_CreditDebitCard = 0;
		$scope.TambahAlokasiUnknown_RincianPembayaran_TanggalPembayaran_CreditDebitCard = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_Keterangan_CreditDebitCard = "";
		$scope.TambahAlokasiUnknown_CreditDebitCard_NamaBank = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_NomorKartu_CreditDebitCard = "";
		//Filter
		$scope.TambahAlokasiUnknown_DaftarWorkOrder_SearchText = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill_SearchText = "";
		$scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_Search_Text = "";
		//SearchBy
		$scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan = "";
		$scope.TambahAlokasiUnknown_SearchBy_NomorSo = "";
		$scope.TambahAlokasiUnknown_SearchBy_NamaSalesman = "";
		$scope.TambahAlokasiUnknown_SearchBy_NomorSpk = "";
		$scope.TambahAlokasiUnknown_SearchBy_NamaLeasing = "";
		$scope.TambahAlokasiUnknown_SearchBy_NamaPelangganPt = "";
		$scope.TambahAlokasiUnknown_SearchBy_NomorAppointment = "";
		$scope.TambahAlokasiUnknown_SearchBy_NomorWo = "";
		$scope.TambahAlokasiUnknown_SearchBy_NamaAsuransi = "";
		$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran = "";
		$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiaya = "";
		$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "";
		$scope.TambahAlokasiUnknown_BiayaBiaya_Nominal = 0;
		$scope.TambahAlokasiUnknown_Biaya = 0;
		$scope.TambahAlokasiUnknown_Biaya_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarARRefundLeasing_UIGrid.data = [];
		$scope.TambahAlokasiUnknown_DaftarArCreditDebit_UIGrid.data = [];
		$scope.ModalTolakAlokasiUnknown_AlasanPenolakan = "";
		$scope.TambahAlokasiUnknown_SearchBy_NamaBank = "";
		$scope.Cetak = "N";
		$scope.currentApprovalTypeName = "";
		$scope.CetakTTUS = "N";
		$scope.TambahAlokasiUnknown_NamaBranch_SearchDropdown = "";
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_Alamat = "";
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_KodeBranch = "";
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_KabupatenKota = "";
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch_Telepon = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Lain_NamaPelanggan = "";
		$scope.TambahAlokasiUnknown_InformasiPelangan_Lain_UntukPembayaran = "";
		$scope.TambahAlokasiUnknown_NomorKuitansi = "";
		$scope.TambahAlokasiUnknown_KetKuitansi = "";
		$scope.LihatAlokasiUnknown_PengajuanReversal_AlasanReversal = "";
		$scope.ModalTolakAlokasiUnknown_AlasanPenolakan = "";
		$scope.LihatAlokasiUnknown_RincianPembayaran_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_Top_NomorSpk = "";
		$scope.LihatAlokasiUnknown_DaftarSalesOrder_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_Top_NomorSo = "";
		$scope.LihatAlokasiUnknown_DaftarSalesOrderDibayar_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_DaftarWorkOrderDibayar_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_WoBill_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_Top_NomorWo = "";
		$scope.LihatAlokasiUnknown_InformasiPembayaran_Appointment_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_WoDown_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_Materai_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_InformasiPembayaran_SoDown_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_DaftarNoRangkaDibayar_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_Top_NamaLeasing = "";
		$scope.LihatAlokasiUnknown_DaftarAR_UIGrid.data = [];
		$scope.LihatAlokasiUnknown_Biaya = 0;
		$scope.LihatAlokasiUnknown_Top_NamaBank = "";


	}
	//Alvin, 20170505, end

	/*===============================Bulk Action Section, Begin===============================*/
	$scope.LihatAlokasiUnknown_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");

		console.log("Cek Rights");
		$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
		console.log($scope.allowApprove);
		//$scope.allowApprove = true;
		if ($scope.LihatAlokasiUnknown_DaftarBulkAction_Search == "Setuju") {
			angular.forEach($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakAlokasiUnknown_IPId = value.IPParentId;
				$scope.ModalTolakAlokasiUnknown_TipePengajuan = value.AlokasiUnknownType;
				$scope.ModalTolakAlokasiUnknown_UnknownId = value.UnknownId;
				$scope.currentApprovalTypeName = value.TipePengajuan;
				if ($scope.allowApprove == false) {
					bsNotify.show({
						title: "Warning",
						content: "Anda Tidak Berhak",
						type: 'warning'
					});
				}
				else {
					if (value.StatusPengajuan == "Diajukan" && value.TipePengajuan == "AlokasiUnknown" && value.StatusCancelBilling == "YA"){
						bsNotify.show({
							title: "Warning",
							content: "Billing Sudah Di Cancel",
							type: 'warning'
						});
						return;
					}
					if (value.StatusPengajuan == "Diajukan") {
						$scope.CreateApprovalData("Disetujui");
						AlokasiUnknownFactory.updateApprovalData($scope.ApprovalData)
							.then(
								function (res) {
									bsNotify.show({
										title: "Berhasil",
										content: "Berhasil Disetujui",
										type: 'success'
									});
									$scope.Batal_LihatAlokasiUnknown_TolakModal();
								},

								function (err) {
									bsNotify.show({
										title: "Notifikasi",
										content: "Persetujuan gagal",
										type: 'danger'
									});
								}
							);
					}
				}
			});
		}
		else if ($scope.LihatAlokasiUnknown_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI);
			angular.forEach($scope.LihatAlokasiUnknown_DaftarAlokasiUnknown_gridAPI.selection.getSelectedRows(), function (value, key) {
				console.log(value);
				$scope.currentApprovalTypeName = value.TipePengajuan;
				$scope.ModalTolakAlokasiUnknown_UnknownId = value.UnknownId;
				$scope.ModalTolakAlokasiUnknown_NomorIP = value.UnknownNo
				$scope.ModalTolakAlokasiUnknown_Tanggal = $filter('date')(new Date(value.TanggalUnknown), 'dd/MM/yyyy');
				$scope.ModalTolakAlokasiUnknown_IPParentId = value.IPParentId;
				$scope.ModalTolakAlokasiUnknown_TipeIP = value.UnknownPaymentType;
				$scope.ModalTolakAlokasiUnknown_NamaPelanggan = value.CustomerName;
				$scope.ModalTolakAlokasiUnknown_MetodePembayaran = value.PaymentMethod;
				$scope.ModalTolakAlokasiUnknown_Nominal = value.Nominal;
				$scope.ModalTolakAlokasiUnknown_TipePengajuan = value.TipePengajuan;
				$scope.ModalTolakAlokasiUnknown_TanggalPengajuan = $filter('date')(new Date(value.TglPengajuan), 'dd/MM/yyyy');
				$scope.ModalTolakAlokasiUnknown_AlasanPengajuan = value.AlasanPengajuan;

				if ($scope.allowApprove == false) {
					bsNotify.show({
						title: "Warning",
						content: "Anda Tidak Berhak",
						type: 'warning'
					});
				}
				else {
					if (value.StatusPengajuan == "Diajukan")
						angular.element('#ModalTolakAlokasiUnknown').modal('show');
				}
			}
			);
		}

	}

	$scope.CreateApprovalData = function (ApprovalStatus) {
		console.log($scope.currentApprovalTypeName);
		if ($scope.currentApprovalTypeName == "Reversal") {
			$scope.currentApprovalTypeName = "AlokasiUnknown Reversal";
			console.log($scope.currentApprovalTypeName);
		}
		$scope.ApprovalData = [
			{
				"ApprovalTypeName": $scope.currentApprovalTypeName,
				"RejectedReason": $scope.ModalTolakAlokasiUnknown_AlasanPenolakan,
				"DocumentId": $scope.ModalTolakAlokasiUnknown_UnknownId,
				"ApprovalStatusName": ApprovalStatus,
			}
		];
	}

	$scope.Batal_LihatAlokasiUnknown_TolakModal = function () {
		angular.element('#ModalTolakAlokasiUnknown').modal('hide');
		$scope.LihatAlokasiUnknown_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListUnknown(1, uiGridPageSize);
	}

	$scope.Pengajuan_LihatAlokasiUnknown_TolakModal = function () {
		if (angular.isUndefined($scope.ModalTolakAlokasiUnknown_AlasanPenolakan) || $scope.ModalTolakAlokasiUnknown_AlasanPenolakan == "") {
			bsNotify.show({
				title: "Warning",
				content: "Alasan Penolakan harus diisi",
				type: 'warning'
			});
			return false;
		}
		$scope.CreateApprovalData("Ditolak");
		AlokasiUnknownFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function (res) {
					bsNotify.show({
						title: "Berhasil",
						content: "Berhasil tolak",
						type: 'success'
					});
					$scope.Batal_LihatAlokasiUnknown_TolakModal();
				},

				function (err) {
					bsNotify.show({
						title: "Notifikasi",
						content: "Penolakan gagal",
						type: 'danger'
					});
				}
			);
	};

	$scope.ClearActionFields = function () {
		$scope.ModalTolakAlokasiUnknown_UnknownId = "";
		$scope.ModalTolakAlokasiUnknown_Tanggal = "";
		$scope.ModalTolakAlokasiUnknown_IPParentId = "";
		$scope.ModalTolakAlokasiUnknown_TipeIP = "";
		$scope.ModalTolakAlokasiUnknown_NamaPelanggan = "";
		$scope.ModalTolakAlokasiUnknown_MetodeBayar = "";
		$scope.ModalTolakAlokasiUnknown_Nominal = "";
		$scope.ModalTolakAlokasiUnknown_TipePengajuan = "";
		$scope.ModalTolakAlokasiUnknown_TanggalPengajuan = "";
		$scope.ModalTolakAlokasiUnknown_AlasanPengajuan = "";
		$scope.ModalTolakAlokasiUnknown_AlasanPenolakan = "";
	};
	/*===============================Bulk Action Section, End===============================*/
	/*=================================Cetak Kuitansi Begin===================================*/

	$scope.LihatAlokasiUnknown_Cetak = function () {
		$scope.CetakIPParentId = $scope.currentIPParentId;
		if ($scope.LihatAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Interbranch - Penerima") {
			$scope.CetakKuitansiTTUS();
		}
		else {
			$scope.CetakKuitansi();
		}
		$scope.LihatAlokasiUnknown_Batal();
		$scope.GetListUnknown(1, uiGridPageSize);
	}

	$scope.LihatAlokasiUnknown_SimpanCetak = function () {
		
		var data = [];
		if($scope.LihatAlokasiUnknown_NomorKuitansi == null || $scope.LihatAlokasiUnknown_NomorKuitansi == undefined || $scope.LihatAlokasiUnknown_NomorKuitansi == ""){
			bsNotify.show({
				title: "warning",
				content: "Kuitansi Preprint Harus di Pilih",
				type: 'warning'
			});
			return;
		}
		data.push({"IPParentId": $scope.currentIPParentId, "PreprintedNumberId": $scope.LihatAlokasiUnknown_NomorKuitansi })
		AlokasiUnknownFactory.KuitansiSave(data)
			.then(
				function (res) {
					console.log("Berhasil menyimpan");
					$scope.LihatAlokasiUnknown_Cetak();
				}
			);
	}

	$scope.CetakKuitansi = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak Kuitansi");
		console.log($scope.CetakIPParentId);

		AlokasiUnknownFactory.getCetakKuitansi($scope.CetakIPParentId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);


					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.CetakKuitansiTTUS = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak TTUS");
		console.log($scope.CetakIPParentId);

		AlokasiUnknownFactory.getCetakTTUS($scope.CetakIPParentId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);


					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};
	/*==================================Cetak Kuitansi End====================================*/


	$scope.LihatAlokasiUnknown_PengajuanReversalModal = function () {
		angular.element('#ModalLihatAlokasiUnknown_PengajuanReversal').modal('show');
	}

	$scope.Batal_LihatAlokasiUnknown_PengajuanReversalModal = function () {
		angular.element('#ModalLihatAlokasiUnknown_PengajuanReversal').modal('hide');
	}

	///SEARCH PROSPECT 
	$scope.ShowModalSearchProspect = function () {
		AlokasiUnknownFactory.getDataPandC($scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan)
			.then(
				function (res) {

					$scope.prospectGrid.data = res.data.Result;
					if (res.data.Result.length > 0)
						console.log("sample", res.data.Result[0]);
				}
			);

		angular.element('#ModalSearchProspectID').modal('setting', { closeable: false }).modal('show');
	}
	$scope.ModalSearchProspect_Batal = function () {
		angular.element('#ModalSearchProspectID').modal('hide');
	}
	$scope.SearchProspectPilihRow = function (row) {
		//RegisterCheque_Data.Name
		console.log("ProspectCustomer Selected", row.entity)

		$scope.Customer_Data.CustomerId = row.entity.CustomerId;
		$scope.Customer_Data.ProspectId = row.entity.ProspectId;
		$scope.Customer_Data.City = row.entity.City;
		$scope.Customer_Data.Address = row.entity.Address;
		$scope.Customer_Data.Phone = row.entity.Phone;
		$scope.Customer_Data.Name = row.entity.Name;
		$scope.TambahAlokasiUnknown_SearchBy_NamaPelanggan = row.entity.Name;
		console.log("CurrentDataToEditWithCust", $scope.Customer_Data);

		if (($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment")
			|| ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment")) {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.TambahAlokasiUnknown_UnitFullPaymentSPK_Paging(1);
			}
		}

		angular.element('#ModalSearchProspectID').modal('hide');
	}
	$scope.prospectGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10, 20],
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'ProspectId', field: 'ProspectId', visible: false },
			{ name: 'Name', displayName: 'Nama Pelanggan', field: 'Name', enableFiltering: true },
			{ name: 'Address', displayName: 'Alamat', field: 'Address', enableFiltering: true },
			{ name: 'City', displayName: 'Kabupaten/Kota', field: 'City', enableFiltering: true },
			{ name: 'Phone', displayName: 'Phone', field: 'Phone', enableFiltering: true },
			{
				name: 'Action', enableFiltering: false,
				cellTemplate: '<a ng-click="grid.appScope.SearchProspectPilihRow(row)">Pilih</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.grid2Api = gridApi;
		}
	};


	///SEARCH SALES 
	$scope.ShowModalSearchSales = function () {
		AlokasiUnknownFactory.getDataSales($scope.TambahAlokasiUnknown_SearchBy_NamaSalesman)
			.then(
				function (res) {

					$scope.salesGrid.data = res.data.Result;
					if (res.data.Result.length > 0)
						console.log("sample", res.data.Result[0]);
				}
			);

		angular.element('#ModalSearchSales').modal('setting', { closeable: false }).modal('show');
	}
	$scope.ModalSearchSales_Batal = function () {
		angular.element('#ModalSearchSales').modal('hide');
	}
	$scope.SearchSalesPilihRow = function (row) {
		//RegisterCheque_Data.Name
		console.log("Sales Selected", row.entity)

		$scope.TambahAlokasiUnknown_SearchBy_NamaSalesman = row.entity.EmployeeName;
		//console.log("CurrentDataToEditWithSales" , $scope.Customer_Data);

		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			if ($scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb == "3") {
				$scope.AlokasigetDataBySalesName();
			}
		}

		angular.element('#ModalSearchSales').modal('hide');
	}
	$scope.salesGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10, 20],
		columnDefs: [
			{ name: 'EmployeeId', field: 'EmployeeId', visible: true },
			{ name: 'EmployeeName', displayName: 'Employee Name', field: 'EmployeeName', enableFiltering: true },
			{
				name: 'Action', enableFiltering: false,
				cellTemplate: '<a ng-click="grid.appScope.SearchSalesPilihRow(row)">Pilih</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.grid2Api = gridApi;
		}
	};

	$scope.GetDataPreprintedSetting = function () {
		AlokasiUnknownFactory.getDataPreprintedSetting()
			.then(
				function (res) {
					$scope.PreprintedSettingData = res.data.Result;
				}
			);
	}

	//------------------------------ bstypeahead Start ---------------------------------------
	$scope.getNoKuitansi = function(key) {
		console.log("key", key);

		if (key == '' || key == null || key == undefined){
			var ress = AlokasiUnknownFactory.getDataPreprintedComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					// $scope.TambahIncomingPayment_NomorKuitansiOption = res.data;
					return res.data
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			
		console.log("ress", ress);
		return ress;
		} else {
			var ress = AlokasiUnknownFactory.getDataPreprintedComboBoxFilter(key)
			.then(
				function (res) {
					$scope.loading = false;
					// $scope.TambahIncomingPayment_NomorKuitansiOption = res.data;
					return res.data
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			
		console.log("ress", ress);
		return ress;
		}
		
	};

	$scope.onSelectNoKuitansi = function($item, $model, $label) {
		console.log("onSelectNoKuitansi=>", $item);
		console.log("onSelectNoKuitansi=>", $model);
		console.log("onSelectNoKuitansi=>", $label);

		$scope.TambahAlokasiUnknown_NomorKuitansi = $item.value
	};

	$scope.onNoResult = function() {
		console.log('hasil tidak ditemukan')
		$scope.TambahAlokasiUnknown_NomorKuitansi = ''
		console.log($scope.TambahAlokasiUnknown_NomorKuitansi)

	}

	$scope.onGotResult = function() {
		console.log("onGotResult=> data ditemukan");
	};
	//------------------------------ bstypeahead End -----------------------------------------


	//Alokasi Type
	$scope.TambahAlokasiUnknown_RincianPembayaran_TipeAlokasiUnknown_Selected_Changed = function (selectedTypeIncoming) {
		$scope.selectedTypeIncoming = selectedTypeIncoming;
		console.log('$scope.selectedTypeIncoming',$scope.selectedTypeIncoming)
		//HideTopMetodePembayaranDetails();//ini paymentdetail top nya (dropdown)
		DisableAllMetodePembayaranDetails();//ini hide all hasil dropdown metode bayar
		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CreditDebitCard();
		DisableAll_TambahAlokasiUnknown_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_TambahAlokasiUnknown_InformasiPembayaran();//ini semua info pembayaran di hide
		DisableAll_TambahAlokasiUnknown_DaftarSpk();//ini semua ilangin daftar spk
		DisableAll_TambahAlokasiUnknown_SalesOrder();//ini ilangin semua sales order
		DisableAll_TambahAlokasiUnknown_DaftarSalesOrderRadio();
		Disable_TambahAlokasiUnknown_KumpulanNomorRangka();
		Disable_TambahAlokasiUnknown_KumpulanWorkOrder();
		Disable_TambahAlokasiUnknown_DaftarArCreditDebit();
		Disable_TambahAlokasiUnknown_KumpulanARRefundLeasing();
		$scope.TambahAlokasiUnknown_NomorKuitansi_New = "";
		$scope.TambahAlokasiUnknown_DaftarSpk_Detail_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSpk_Brief_Radio = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill = "";
		$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.data = []
		$scope.TambahAlokasiUnknown_UnitFullPaymentSO_UIGrid.columnDefs = [];
		$scope.TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [];
		$scope.TambahAlokasiUnknown_InformasiPembayaran_UIGrid.columnDefs = [];
		$scope.ModalTambahAlokasi_CustomerName.columnDefs = [];
		Enable_TambahAlokasiUnknown_SaveAndPrintButton();
		Enable_TambahAlokasiUnknown_BiayaBiaya();
		ShowTopMetodePembayaranDetails();
		$scope.Cetak = "N";
		$scope.CetakTTUS = "N";
		ShowKuitansi();
		HideTTUS();

		//Clear All Data
		$scope.TambahAlokasiUnknown_ClearAllData();

		//$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb="";
		DisableAll_TambahAlokasiUnknown_SearchBy();//hide semua Pilihan Search by
		$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb = "";//ini buat reset semua pilihan searchby
		DisableAll_SearchByRadioResult();//disable semua hasil search by radio

		if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Booking Fee") {
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkSales();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = false;

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Down Payment") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Full Payment") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Swapping") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelangganPt();
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Ekspedisi") {

		// $scope.Ready_Rincian_Pembayaran_Selected_None();
		// Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelangganPt();
		// }
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Order Pengurusan Dokumen") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Purna Jual") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Down Payment") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchByAppointmentWo();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit_EnableDisable = true;

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment") {

			//hilman,begin
			$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy.rb = "3";
			$scope.TambahAlokasiUnknown_TujuanPembayaran_SearchBy_ValueChanged();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			//hilman,end
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			//Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchByWoPelanggan();
		}
		// else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Service - Full Payment Asuransi") {

		// $scope.Ready_Rincian_Pembayaran_Selected_None();
		// Enable_TambahAlokasiUnknown_SearchBy_NamaAsuransi();
		// Enable_SearchByCariBtn();
		// }
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Down Payment") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Parts - Full Payment") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan();
		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Unknown") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
			//$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran={};
			Disable_TambahAlokasiUnknown_BiayaBiaya();
			HideKuitansi();

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Bank Statement - Card") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
			//$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran={};
			//$scope.optionsTambahAlokasiUnknown_SearchBy_NamaBank = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			//$scope.getDataBank();
			$scope.TambahAlokasiUnknown_getDataEDC();
			Enable_TambahAlokasiUnknown_DaftarArCreditDebit();
			Enable_TambahAlokasiUnknown_SearchBy_NamaBank();
			Enable_SearchByCariBtn();
			Disable_TambahAlokasiUnknown_BiayaBiaya();
			HideKuitansi();

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Interbranch - Penerima") {

			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.optionsMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }];
			//$scope.TambahAlokasiUnknown_RincianPembayaran_MetodePembayaran={};
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahAlokasiUnknown_Interbranch_InformasiBranch();
			Enable_TambahAlokasiUnknown_InformasiPelangan_Lain();
			//nongolin info lain
			//Alvin, begin
			console.log("getDataBranchCB");
			$scope.getDataBranchCB();
			ShowTTUS();
			//Alvin, end
		}
		//20170509, nuse, begin
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Cash Delivery") {
			Enable_TambahAlokasiUnknown_TujuanPembayaran_CashDelivery();
			$scope.optionsMetodePembayaran = [{ name: "Cash", value: "Cash" }];
			$scope.getDataBankAccountCashDelivery();
			ShowTopMetodePembayaranDetails();
			//Enable_TambahAlokasiUnknown_SaveAndPrintButton();
			Disable_TambahAlokasiUnknown_BiayaBiaya();
			HideKuitansi();

		}
		//20170509, nuse, end		
		//20170527, alvin, begin		
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Unit - Payment (Kuitansi Penagihan)") {
			//$scope.getDataVendor();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			Enable_TambahAlokasiUnknown_SearchBy_NomorProforma();
			Enable_SearchByCariBtn();

		}
		else if ($scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown == "Refund Leasing") {
			//$scope.getDataVendor();
			$scope.getDataLeasing();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			Enable_TambahAlokasiUnknown_SearchBy_NamaLeasing();
			$scope.TambahAlokasiUnknown_BiayaBiaya_TipeBiayaOptions = [{ name: "PPN Keluaran", value: "PPN Keluaran" }];
			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
			$scope.TambahAlokasiUnknown_BiayaBiaya_DebetKredit = "Kredit";
			Enable_SearchByCariBtn();
			Disable_TambahAlokasiUnknown_BiayaBiaya();
			HideKuitansi();

		}
		//20170527, alvin, end

		var sTipe = $scope.TambahAlokasiUnknown_TujuanPembayaran_TipeAlokasiUnknown;
		var isPreprintedReceipt = false;
		angular.forEach($scope.PreprintedSettingData, function (value, key) {
			if (!isPreprintedReceipt) {
				if (value.BusinessType.indexOf('Unit') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Unit') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else {
					HideKuitansi();
				}
			}
		});

		if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
			$scope.TambahAlokasiUnknown_Ket_Kuitansi = true;
		}
		else {
			$scope.TambahAlokasiUnknown_Ket_Kuitansi = false;
		}
	};

	// START PILIH PEMBAYAR
	// $scope.Mandatory = true;
	$scope.optionsTambahAlokasiNamaPembayar_SelectKolom = [{ name: "NoWO", value: "NoWO" }, { name: "Nama Pembayar", value: "NamaPembayar" }, { name: "No.Polisi", value: "Nopol" }, { name: "Address", value: "Address" }]
	$scope.ModalTambahAlokasi_CustomerName = {
		paginationPageSizes: [10, 50, 100],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination: true,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false }
		],

		onRegisterApi: function (gridApi) {
			$scope.ModalTambahAlokasi_CustomerName_gridAPI = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getDataPembayar(pageNumber, pageSize);
			}
			);
		}
	}

	$scope.TambahAlokasi_OtherTypeNamaPembayaran_Click = function(){
		// $scope.Mandatory = true;
		angular.element('#ModalTambahAlokasi_CustomerName').modal('setting', { closeable: false }).modal('show');
		$scope.ModalTambahAlokasi_CustomerName.columnDefs = [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataNoWO", displayName: "No. WO", field: "NoWO", enableCellEdit: false, enableHiding: false },
			{ name: "DataName", displayName: "Nama Pembayar", field: "NamaPembayar", enableCellEdit: false, enableHiding: false },
			{ name: "DataNopol", displayName: "Nomor Polisi", field: "Nopol", enableCellEdit: false, enableHiding: false },
			{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false }
		];

	}

	$scope.Batal_TambahAlokasiNamaPembayar = function (){
		// $scope.Mandatory = true;
		angular.element('#ModalTambahAlokasi_CustomerName').modal('hide');
		console.log("Cek Batal masuk gak yah >>>>>>>>>>>>")
		// if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
		// 	$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		// }
		$scope.ClearFilter();
	}

	$scope.Pilih_TambahAlokasiNamaPembayar = function (){
		console.log("Cek PIlih masuk gak yah >>>>>>>>>>>>")
		$scope.Mandatory = false;
		console.log("Cek Mandaroty Pilih>>>>>>", $scope.Mandatory);
		if ($scope.ModalTambahAlokasi_CustomerName_gridAPI.selection.getSelectedCount() > 0) {
			$scope.ModalTambahAlokasi_CustomerName_gridAPI.selection.getSelectedRows().forEach(function (row) {
				$scope.TambahAlokasi_OtherTypeNamaPembayaran = row.NamaPembayar;
				// $scope.TambahInvoiceMasuk_OtherTypeNamaVendor_TujuanPembayaran_IdPelangganVendor = row.Id;
			});

			angular.element('#ModalTambahAlokasi_CustomerName').modal('hide');
		}
		else {
			//alert('Belum ada data yang dipilih');
			bsNotify.show({
				title: "Warning",
				content: 'Belum ada data yang dipilih !',
				type: 'warning'
			});
		}
		$scope.ClearFilter();
	}

	$scope.TambahAlokasiNamaPembayar_Filter_Clicked = function(){
		$scope.getDataPembayar(1, $scope.uiGridPageSize);
	}

	$scope.getDataPembayar = function(pageNumber, pageSize){
		AlokasiUnknownFactory.getFilterData(pageNumber, pageSize, $scope.TambahAlokasiNamaPembayar_TextFilter, $scope.TambahAlokasiNamaPembayar_SelectKolom)
		.then(
			function(res){
				$scope.ModalTambahAlokasi_CustomerName.data = res.data.Result;
				$scope.ModalTambahAlokasi_CustomerName.totalItems = res.data.Total;
				$scope.ModalTambahAlokasi_CustomerName.paginationPageSize = pageSize;
				$scope.ModalTambahAlokasi_CustomerName.paginationPageSizes = [10, 50, 100];
			},
			function(err){
				console.log("Err===>",err);
			}
		);
		if ($scope.TambahAlokasiNamaPembayar_SelectKolom == null || $scope.TambahAlokasiNamaPembayar_SelectKolom == '')
				$scope.ModalTambahAlokasi_CustomerName_gridAPI.grid.clearAllFilters();
			else
				$scope.ModalTambahAlokasi_CustomerName_gridAPI.grid.clearAllFilters();
	
			if ($scope.TambahAlokasiNamaPembayar_SelectKolom == 'NoWO')
				nomor = 2;
			else if ($scope.TambahAlokasiNamaPembayar_SelectKolom == 'NamaPembayar')
				nomor = 3;
			else if ($scope.TambahAlokasiNamaPembayar_SelectKolom == 'NoPol')
				nomor = 4
			else if ($scope.TambahAlokasiNamaPembayar_SelectKolom == 'Address')
				nomor =  5
			else
			nomor = 0
	
			if (!angular.isUndefined($scope.TambahAlokasiNamaPembayar_SelectKolom) && nomor != 0) {
				$scope.ModalTambahAlokasi_CustomerName_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiNamaPembayar_TextFilter;
			}
			$scope.ModalTambahAlokasi_CustomerName_gridAPI.grid.queueGridRefresh();
	}

	$scope.ClearFilter = function(){
		$scope.TambahAlokasiNamaPembayar_TextFilter = "";
		$scope.TambahAlokasiNamaPembayar_SelectKolom = "";
	}
	// END PILIH PEMBAYAR

	//function 

	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkSales() {
		//$('#Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkSales').show();
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkSales = true;
	}

	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkPelanggan() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkPelanggan = true;
	}

	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelangganPt() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelangganPt = true;
	}
	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan = true;
	}
	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchByAppointmentWo() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchByAppointmentWo = true;

	}
	function Enable_TambahAlokasiUnknown_TujuanPembayaran_SearchByWoPelanggan() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchByWoPelanggan = true;
	}

	function DisableAll_TambahAlokasiUnknown_SearchBy() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkSales = false;
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySpkPelanggan = false;
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelangganPt = false;
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchBySoPelanggan = false;
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchByAppointmentWo = false;
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_SearchByWoPelanggan = false;
		//20170505, nuse, begin
		$scope.Show_TambahAlokasiUnknown_BiayaBiaya = false;
		//20170505, nuse, end
		//20170509, nuse, begin
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_CashDelivery = false;
		//20170509, nuse, end		
	}

	function DisableAll_TambahAlokasiUnknown_InformasiPelanggan() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Lain = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Prospect = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Branch = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch = false;
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch = false;
	}

	function Enable_TambahAlokasiUnknown_InformasiPelangan_Prospect() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Prospect = true;

	}

	function Enable_TambahAlokasiUnknown_InformasiPelangan_Branch() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Branch = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPelangan_Toyota() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Toyota = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_PelangganBranch = true;
	}

	function Enable_TambahAlokasiUnknown_BiayaBiaya() {
		$scope.Show_TambahAlokasiUnknown_BiayaBiaya = true;
	}

	function Disable_TambahAlokasiUnknown_BiayaBiaya() {
		$scope.Show_TambahAlokasiUnknown_BiayaBiaya = false;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_Spk() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Spk = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_SoBill() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_SoBill = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_Appointment() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Appointment = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_WoDown() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_WoDown = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_WoBill() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_WoBill = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_SoDown() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_SoDown = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPembayaran_Materai() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Materai = true;
	}

	function DisableAll_TambahAlokasiUnknown_InformasiPembayaran() {
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Spk = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_SoBill = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Appointment = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_WoDown = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_WoBill = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_SoDown = false;
		$scope.Show_TambahAlokasiUnknown_InformasiPembayaran_Materai = false;
	}

	function HideTopMetodePembayaranDetails() {
		$scope.Show_TambahAlokasiUnknown_RincianPembayaran = false;
	}

	function ShowTopMetodePembayaranDetails() {
		$scope.Show_TambahAlokasiUnknown_RincianPembayaran = true;
	}

	function Enable_LihatAlokasiUnknown_Top_NomorSpk() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorSpk = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NomorSpk() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorSpk = false;
	}

	function Enable_LihatAlokasiUnknown_Top_NamaLeasing() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaLeasing = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NamaLeasing() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaLeasing = false;
	}

	function Enable_LihatAlokasiUnknown_Top_NomorSo() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorSo = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NomorSo() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorSo = false;
	}

	function Enable_LihatAlokasiUnknown_Top_NomorWo() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorWo = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NomorWo() {
		$scope.Show_LihatAlokasiUnknown_Top_NomorWo = false;
	}

	function Enable_LihatAlokasiUnknown_Top_NamaAsuransi() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaAsuransi = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NamaAsuransi() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaAsuransi = false;
	}
	function Enable_LihatAlokasiUnknown_Top_NamaBank() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaBank = true;
	}

	function Disable_LihatAlokasiUnknown_Top_NamaBank() {
		$scope.Show_LihatAlokasiUnknown_Top_NamaBank = false;
	}

	function Enable_LihatAlokasiUnknown_Interbranch_InformasiBranch() {
		$scope.Show_LihatAlokasiUnknown_Interbranch_InformasiBranch = true;
	}

	function Disable_LihatAlokasiUnknown_Interbranch_InformasiBranch() {
		$scope.Show_LihatAlokasiUnknown_Interbranch_InformasiBranch = false;
	}

	function Enable_LihatAlokasiUnknown_Interbranch_InformasiBranch() {
		$('#Show_LihatAlokasiUnknown_InformasiPelangan').show();
		$scope.Show_LihatAlokasiUnknown_Interbranch_InformasiBranch = true;
	}

	function DisableAll_LihatAlokasiUnknown_InformasiPelanggan() {

		$('#Show_LihatAlokasiUnknown_InformasiPelangan').hide();
		$('#Show_LihatAlokasiUnknown_InformasiPelangan_Lain').hide();
		$('#Show_LihatAlokasiUnknown_InformasiPelangan_Prospect').hide();
		$('#Show_LihatAlokasiUnknown_InformasiPelangan_Branch').hide();
		$('#Show_LihatAlokasiUnknown_InformasiPelangan_Toyota').hide();
		$('#Show_LihatAlokasiUnknown_InformasiPelangan_PelangganBranch').hide();
		$scope.Show_LihatAlokasiUnknown_Interbranch_InformasiBranch = false;

	}

	function DisableAll_SearchByRadioResult() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorSpk = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaSalesman = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPelanggan = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPembayar = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaLeasing = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorProforma = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorSo = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPelangganPt = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPelangganPt = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorAppointment = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorWo = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaAsuransi = false;
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaBank = false;
		$scope.TambahAlokasiUnknown_SearchBy_CariBtn = false;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NomorSpk() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorSpk = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaSalesman() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaSalesman = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaPelanggan() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPelanggan = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaPembayar() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPembayar = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaLeasing() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaLeasing = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NomorProforma() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorProforma = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NomorSo() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorSo = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaPelangganPt() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaPelangganPt = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NomorAppointment() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorAppointment = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NomorWo() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NomorWo = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaAsuransi() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaAsuransi = true;
	}

	function Enable_TambahAlokasiUnknown_SearchBy_NamaBank() {
		$scope.Show_TambahAlokasiUnknown_SearchBy_NamaBank = true;
	}

	function Enable_SearchByCariBtn() {
		$scope.TambahAlokasiUnknown_SearchBy_CariBtn = true;
		// $scope.cekdah = true;
	}

	function DisableAll_TambahAlokasiUnknown_DaftarSpk() {
		$scope.Show_TambahAlokasiUnknown_DaftarSpk = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSpk_Detail = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSpk_Brief = false;
	}

	function Enable_TambahAlokasiUnknown_DaftarSpk_Detail() {
		$scope.Show_TambahAlokasiUnknown_DaftarSpk = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSpk_Detail = true;
	}

	function Enable_TambahAlokasiUnknown_DaftarSpk_Brief() {
		$scope.Show_TambahAlokasiUnknown_DaftarSpk = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSpk_Brief = true;
	}

	function DisableAll_TambahAlokasiUnknown_DaftarSalesOrderRadio() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderDibayar = false;

	}

	function Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = true;
	}

	function Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSo = true;
	}

	function Enable_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoMo = false;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderRadio_SoSoBill = true;
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrderDibayar = true;
	}

	function DisableAll_LihatAlokasiUnknown_InformasiPembayaran() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Spk = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_SoBill = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Appointment = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_WoDown = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_WoBill = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_SoDown = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Materai = false;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_Spk() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Spk = true;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_SoBill() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_SoBill = true;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_Appointment() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Appointment = false;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_WoDown() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_WoDown = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_dariTambah_UIGrid = true;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_WoBill() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = false;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_WoBill = false;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_SoDown() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_SoDown = true;
	}

	function Enable_LihatAlokasiUnknown_InformasiPembayaran_Materai() {
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran = true;
		$scope.Show_LihatAlokasiUnknown_InformasiPembayaran_Materai = true;
	}

	function Enable_LihatAlokasiUnknown_DaftarSalesOrderDibayar() {
		$scope.Show_LihatAlokasiUnknown_DaftarSalesOrderDibayar = true;
	}

	function Disable_LihatAlokasiUnknown_DaftarSalesOrderDibayar() {
		$scope.Show_LihatAlokasiUnknown_DaftarSalesOrderDibayar = false;
	}

	function Enable_LihatAlokasiUnknown_DaftarNoRangkaDibayar() {
		$scope.Show_LihatAlokasiUnknown_DaftarNoRangkaDibayar = true;
	}

	function Disable_LihatAlokasiUnknown_DaftarNoRangkaDibayar() {
		$scope.Show_LihatAlokasiUnknown_DaftarNoRangkaDibayar = false;
	}

	function Enable_LihatAlokasiUnknown_DaftarArCreditDebit() {
		$scope.Show_LihatAlokasiUnknown_DaftarArCreditDebit = true;
	}

	function Disable_LihatAlokasiUnknown_DaftarArCreditDebit() {
		$scope.Show_LihatAlokasiUnknown_DaftarArCreditDebit = false;
	}
	function Enable_LihatAlokasiUnknown_DaftarSalesOrder() {
		$scope.Show_LihatAlokasiUnknown_DaftarSalesOrder = true;
	}

	function Disable_LihatAlokasiUnknown_DaftarSalesOrder() {
		$scope.Show_LihatAlokasiUnknown_DaftarSalesOrder = false;
	}

	function Enable_TambahAlokasiUnknown_TujuanPembayaran_CashDelivery() {
		$scope.Show_TambahAlokasiUnknown_TujuanPembayaran_CashDelivery = true;
	}
	function Enable_TambahAlokasiUnknown_Interbranch_InformasiBranch() {
		$scope.Show_TambahAlokasiUnknown_Interbranch_InformasiBranch = true;
	}

	function Enable_TambahAlokasiUnknown_InformasiPelangan_Lain() {
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan = true;
		$scope.Show_TambahAlokasiUnknown_InformasiPelangan_Lain = true;
	}

	function DisableAll_TambahAlokasiUnknown_SalesOrder() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrder = false;
	}

	function Enable_TambahAlokasiUnknown_DaftarSalesOrder_SoSoDpPem() {
		$scope.Show_TambahAlokasiUnknown_DaftarSalesOrder = true;
	}

	function Disable_TambahAlokasiUnknown_KumpulanNomorRangka() {
		$scope.Show_TambahAlokasiUnknown_DaftarNoRangka = false;
		$scope.Show_TambahAlokasiUnknown_DaftarNoRangkaDibayar = false;
	}

	function Enable_TambahAlokasiUnknown_KumpulanNomorRangka() {
		$scope.Show_TambahAlokasiUnknown_DaftarNoRangka = true;
		$scope.Show_TambahAlokasiUnknown_DaftarNoRangkaDibayar = true;
	}
	function Disable_TambahAlokasiUnknown_KumpulanARRefundLeasing() {
		$scope.Show_TambahAlokasiUnknown_DaftarARRefundLeasing = false;
		$scope.Show_TambahAlokasiUnknown_DaftarARRefundLeasingDibayar = false;
	}

	function Enable_TambahAlokasiUnknown_KumpulanARRefundLeasing() {
		$scope.Show_TambahAlokasiUnknown_DaftarARRefundLeasing = true;
		$scope.Show_TambahAlokasiUnknown_DaftarARRefundLeasingDibayar = true;
	}

	function Disable_TambahAlokasiUnknown_KumpulanWorkOrder() {
		$scope.Show_TambahAlokasiUnknown_DaftarWorkOrder = false;
		$scope.Show_TambahAlokasiUnknown_DaftarWorkOrderDibayar = false;
	}

	function Enable_TambahAlokasiUnknown_KumpulanWorkOrder() {
		$scope.Show_TambahAlokasiUnknown_DaftarWorkOrder = true;
		$scope.Show_TambahAlokasiUnknown_DaftarWorkOrderDibayar = true;
	}

	function Enable_TambahAlokasiUnknown_DaftarArCreditDebit() {
		$scope.Show_TambahAlokasiUnknown_DaftarArCreditDebit = true;
	}

	function Disable_TambahAlokasiUnknown_DaftarArCreditDebit() {
		$scope.Show_TambahAlokasiUnknown_DaftarArCreditDebit = false;
	}

	function Enable_TambahAlokasiUnknown_SaveAndPrintButton() {
		$scope.Show_TambahAlokasiUnknown_SaveAndPrintButton = true;
	}

	function Disable_TambahAlokasiUnknown_SaveAndPrintButton() {
		$scope.Show_TambahAlokasiUnknown_SaveAndPrintButton = false;
	}

	function HideKuitansi() {
		$scope.TambahAlokasiUnknown_NoKuitansi = false;
		$scope.TambahAlokasiUnknown_Ket_Kuitansi = false;
		$('#ShowSimpanCetakKuitansi').hide();
		$('#ShowSimpanCetakTTUS').hide();
	}

	function ShowKuitansi() {
		$scope.TambahAlokasiUnknown_NoKuitansi = true;
		$scope.TambahAlokasiUnknown_Ket_Kuitansi = true;
		$('#ShowSimpanCetakKuitansi').hide();
		$('#ShowSimpanCetakTTUS').hide();
	}

	function Enable_TambahAlokasiUnknown_Biaya_NoFaktur() {
		$scope.Show_TambahAlokasiUnknown_Biaya_NoFaktur = true;
		$scope.Show_TambahAlokasiUnknown_Biaya_NoFaktur2 = false;
	}

	function Disable_TambahAlokasiUnknown_Biaya_NoFaktur() {
		$scope.Show_TambahAlokasiUnknown_Biaya_NoFaktur = false;
		$scope.Show_TambahAlokasiUnknown_Biaya_NoFaktur2 = true;
	}

	function ShowKuitansi_Lihat() {
		$scope.LihatAlokasiUnknown_NoKuitansi_ShowHide = true;
		$scope.LihatAlokasiUnknown_KetKuitansi_ShowHide = true;
	}

	function HideKuitansi_Lihat() {
		$scope.LihatAlokasiUnknown_NoKuitansi_ShowHide = false;
		$scope.LihatAlokasiUnknown_KetKuitansi_ShowHide = false;
	}
});

app.filter('rupiahC', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});