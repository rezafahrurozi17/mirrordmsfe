/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/
//test
var app = angular.module('app');
//20170425, eric, begin
//app.controller('IncomingPaymentController', function ($scope, $http, CurrentUser, IncomingPaymentFactory, $timeout) {
app.controller('IncomingPaymentController', function ($scope, $http, CurrentUser, $filter, IncomingPaymentFactory, $timeout, uiGridConstants, bsNotify) {
	//20170425, eric, end
	$scope.optionsTambahIncomingPayment_DaftarSpk_Detail_Search = [{ name: "Nomor SPK", value: "Nomor SPK" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }];
	$scope.optionsTambahIncomingPayment_DaftarSpk_Brief_Search = [{ name: "Nomor SPK", value: "Nomor SPK" }];
	$scope.optionsTambahIncomingPayment_DaftarNoRangka_Search = [{ name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor SPK", value: "Nomor SPK" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nama di Billing", value: "Nama di Billing" }];
	$scope.optionsTambahIncomingPayment_DaftarWorkOrder_Search = [{ name: "Nomor WO", value: "Nomor WO" }, { name: "Nomor Billing", value: "Nomor Billing" }, { name: "Nomor Polisi", value: "Nomor Polisi" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }];
	$scope.optionsTambahIncomingPayment_DaftarArCreditDebit_Search = [{ name: "Tanggal AR Card", value: "Tanggal AR Card" }, { name: "Tipe Kartu", value: "Tipe Kartu" }, { name: "Nominal", value: "Nominal" }];
	$scope.optionsLihatIncomingPayment_DaftarArCreditDebit_Search = [{ name: "Tanggal AR Card", value: "Tanggal AR Card" }, { name: "Tipe Kartu", value: "Tipe Kartu" }, { name: "Nominal", value: "Nominal" }];
	$scope.optionsTambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }];
	$scope.optionsTambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor Billing", value: "Nomor Billing" }];
	$scope.optionsTambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor Down Payment", value: "Nomor Down Payment" }];
	$scope.optionsLihatIncomingPayment_DaftarIncomingPayment_Search = [{ name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" }, { name: "Tipe Incoming Payment", value: "Tipe Incoming" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }, { name: "Metode Pembayaran", value: "Metode Pembayaran" }, { name: "Nominal", value: "Nominal" }, { name: "Status Pengajuan", value: "Status Pengajuan" }];
	//$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Proforma Invoice)", value: "Unit - Payment (Proforma Invoice)" }, { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, {name:"Cash Delivery", value: "Cash Delivery"}];
	$scope.optionsTambahIncomingPayment_DaftarARRefundLeasing_Search = [{ name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor SO", value: "Nomor SO" }, { name: "Nomor SPK", value: "Nomor SPK" }];
	$scope.TambahIncomingPayment_BiayaBiaya_DebetKreditOptions = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
	$scope.optionsTambahIncomingPayment_DaftarInterbranch_Search = [{ name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" }, { name: "Tanggal Incoming Payment", value: "Tanggal Incoming Payment" }, { name: "Nominal", value: "Nominal" }];
	$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Kuitansi Penagihan)", value: "Unit - Payment (Kuitansi Penagihan)" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing / Insurance", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, { name: "Cash Delivery", value: "Cash Delivery" }];
	$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Kuitansi Penagihan)", value: "Unit - Payment (Kuitansi Penagihan)" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing / Insurance", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, { name: "Cash Delivery", value: "Cash Delivery" }];

	$scope.AssigmentControl = false;
	$scope.selectTipeBiaya = null;
	$scope.controlNominal = false;

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log('eaeaea ' + $scope.ByteEnable);
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	if ($scope.allowApprove == true)
		$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	else
		$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [];

	$scope.parent = $scope;

	//20170517, nuse, begin
	$scope.Show_ShowLihatIncomingPayment = true;
	$scope.Show_ShowTambahIncomingPayment = false;
	$scope.ShowIncomingPaymentDataDetail = false;
	$scope.ShowIncomingPaymentData = true;
	$scope.ShowIncomingPaymentHeader = true;
	$scope.ShowIncomingPaymentHeaderTambah = true;
	$scope.LihatIncomingPayment_DaftarIncoming_UIGrid_Disabled = false;

	$scope.TipeLama = "";
	$scope.TipeBaru = "";
	$scope.Preprinted = 0;

	var FormStateValue = 0;
	$scope.TotalPembayaranAll = 0;

	var FormState = {
		isDefault: 0,
		isSaved: 1,
		isUpdated: 2,
		isDeleted: 3
	}

	var paginationOptions_MainIncoming = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	$scope.CekNamPemb1 = "";
	var NominalPembayaran = 0;
	//20170517, nuse, end
	//tambah getdata untuk factory incomingPaymentFactory
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//20170430, NUSE EDIT, begin
	$scope.uiGridPageSize = 10;
	//20170430, NUSE EDIT, end
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = true;
		//$scope.SetMainGridADH();
		//   $scope.gridData=[];

	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mincomingpayment = null; //Model
	//console.log('Result si Model', mfinanceincomingpaymentunitdp.ExampleData);
	$scope.xRole = { selected: [] };
	$scope.currentSPKId = 0;
	$scope.Cetak = "N";
	$scope.CetakTTUS = "N";
	$scope.CetakIPParentId = 0;
	$scope.currentNoSO = "";
	$scope.currentSODate = "";
	$scope.currentOutletId = 0;
	$scope.currentNominalBilling = 0;
	$scope.currentSisaPembayaran = 0;
	$scope.currentPaidNominal = 0;
	$scope.currentIPParentId = 0;
	//20170414, eric, begin
	$scope.currentCustomerId = 0;
	$scope.Pembayaran = 0;
	$scope.EnabledCetak = 1;
	$scope.TujuanPembayaran_CashDelivery = "";
	$scope.TambahIncomingPayment_NamaBranch_SearchDropdown = "";
	//20170414, eric, end
	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};
	//interbranch info
	$scope.currentInterBranchId = 0;
	$scope.currentNamaBranchPenerima = "";
	$scope.currentNomorIncomingPayment = 0;
	$scope.currentNominalPembayaran = 0;
	$scope.currentNamaPelanggan = "";
	$scope.currentUntukPembayaran = "";
	$scope.currentTanggalBank = new Date();
	$scope.currentProformaInvoiceId = null;
	//hilman,begin
	$scope.selNoWO = "";
	$scope.selTglWo = "";
	$scope.selNoBilling = "";
	$scope.selNoPolisi = "";
	$scope.selNamaPelanggan = "";
	$scope.selTotalTerbayar = "";
	$scope.selSisaPembayaran = "";
	$scope.selBiayaMaterai = "";
	$scope.TotalBayarSum = 0;
	//hilman,end
	$scope.currentTglAR = "";
	$scope.currentCardType = "";
	$scope.currentNominal = 0;
	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = new Date();
	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo = new Date();
	var dateNow = new Date();
    $scope.maxDate = new Date(dateNow.getFullYear(),dateNow.getMonth(), dateNow.getDate());

	$scope.Customer_Data =
		{
			ProspectId: 0,
			CustomerId: 0,
			Name: "",
			Address: "",
			Phone: "",
			City: "",
		};

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	var uiGridPageSize = 10;

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};


	$scope.givePatternBiayaBank = function (item, event) {
		console.log("item", item);
		console.log("event", event.which);
		console.log("event", event);
		if(item.includes('.')){
			var tmpSplit = item.split('.');
			if(tmpSplit[1].length > 2){
				tmpSplit[1] = tmpSplit[1].substr(0,2);
				$scope.TambahIncomingPayment_Biaya = tmpSplit[0] + '.' + tmpSplit[1];
				return false;
			}
		
		}
		if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105)){
			event.preventDefault();
			console.log("disini", event.which);
			console.log("$scope.TambahInvoiceMasuk_Nominal", $scope.TambahIncomingPayment_Biaya);
			$scope.TambahIncomingPayment_Biaya = $scope.TambahIncomingPayment_Biaya.replace(/[^0-9.]+/g,'');
			$scope.TambahIncomingPayment_Biaya = $scope.TambahIncomingPayment_Biaya.replace(event.key,"");
			return false;
		} else {
			$scope.TambahIncomingPayment_Biaya = $scope.TambahIncomingPayment_Biaya;
			return;
		}
	};

	$scope.givePattern = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
			return;
		}
	};

	$scope.givePattern2 = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
			return;
		}
	};

	$scope.givePattern3 = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
			return;
		}
	};

	$scope.givePattern4 = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahIncomingPayment_BiayaBiaya_Nominal = $scope.TambahIncomingPayment_BiayaBiaya_Nominal.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
			return;
		}
	};


	//hilman,begin
	$scope.getDataServiceCustomerAndPayment = function (PageNum, SearchQry, SearchType) {
		IncomingPaymentFactory.getDataServiceCustomerAndPayment(PageNum, uiGridPageSize, SearchQry, SearchType)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].NamaPelanggan;
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].ToyotaId;
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalWO = new Date(res.data.Result[0].TglWo);
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = new Date(res.data.Result[0].TglAppointment);
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
						//console.log($scope.currentCustomerId);
						//console.log(res.data.Result[0].CustomerId);
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabupatenKota;
						//$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data=res.data.Result;
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.push({ Keterangan: "Down Payment", NoWO: res.data.Result[0].NoWO, NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP });
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.push({ Keterangan: "Own Risk", NoWO: res.data.Result[0].NoWO, NominalDP: res.data.Result[0].NominalOwnRisk, PaidDP: res.data.Result[0].TotalTerbayar, SisaDP: res.data.Result[0].SisaPembayaran });
						$scope.assignmentData = res.data.Result;
					}
					else {
						console.log("masuk3");
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = "";
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_ToyotaId = "";
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = "";
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Handphone = "";
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Alamat = "";
						$scope.TambahIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = "";
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
						//$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TambahIncomingPayment_AddItem = function () {
		console.log("TambahServiceFullPayment_AddItem");
		$scope.cekPem1 = $scope.TambahIncomingPayment_SearchBy_NamaPembayar;
		var found = 0;
		$scope.selectedRows = $scope.gridApi_InformasiPembayaran_UIGrid2.selection.getSelectedRows();
		console.log("selected=>",$scope.selectedRows);
		
		if ($scope.onSelectRows) {
			$scope.onSelectRows($scope.selectedRows);
			console.log("=============================>", $scope.gridApi_InformasiPembayaran_UIGrid2,$scope.selectedRows);
		}
		
		$scope.gridApi_InformasiPembayaran_UIGrid2.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.NoWO);
				if (row.NoBilling == obj.NoBilling)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.push({
					NoWO: row.NoWO,
					WoNo: row.WoNo,
					TglWo: row.TglWo,
					NoBilling: row.NoBilling,
					NoPolisi: row.NoPolisi,
					NamaPelanggan: row.NamaPelanggan,
					PaidNominal: row.PaidNominal,
					SisaNominal: row.SisaNominal,
					BiayaMaterai: row.BiayaMaterai,
					IsPrintedKwitansi: row.IsPrintedKwitansi
				});
				
				$scope.TotalBayarSum = $scope.TotalBayarSum + $scope.selSisaPembayaran;
				$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed();
				
			}
			
		});
			
		// 	// if (row.IsPrintedKwitansi) {
		// 	// 	HideKuitansi();
		// 	// }
		// 	// else {
		// 	// 	//ShowKuitansi();
		// 	// 	var sTipe = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		// 	// 	var isPreprintedReceipt = false;
		// 	// 	angular.forEach($scope.PreprintedSettingData, function (value, key) {
		// 	// 		if (!isPreprintedReceipt) {
		// 	// 			if (value.BusinessType.indexOf('Unit') == 0
		// 	// 				&& value.isPreprintedReceipt
		// 	// 				&& sTipe.indexOf('Unit') == 0
		// 	// 			) {
		// 	// 				if (sTipe.indexOf('Proforma') >= 0) {
		// 	// 					HideKuitansi();
		// 	// 				}
		// 	// 				else {
		// 	// 					ShowKuitansi();
		// 	// 					isPreprintedReceipt = true;
		// 	// 					$scope.getDataPreprintedCB();
		// 	// 				}
		// 	// 			}
		// 	// 			else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
		// 	// 				ShowKuitansi();
		// 	// 				isPreprintedReceipt = true;
		// 	// 				$scope.getDataPreprintedCB();
		// 	// 			}
		// 	// 			else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
		// 	// 				ShowKuitansi();
		// 	// 				isPreprintedReceipt = true;
		// 	// 				$scope.getDataPreprintedCB();
		// 	// 			}
		// 	// 			else {
		// 	// 				HideKuitansi();
		// 	// 			}
		// 	// 		}
		// 	// 	});

		// 	// 	if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
		// 	// 		if (sTipe.indexOf('Proforma') == -1) {
		// 	// 			$scope.TambahIncomingPayment_KetKuitansi = true;
		// 	// 		}
		// 	// 	}
		// 	// 	else {
		// 	// 		$scope.TambahIncomingPayment_KetKuitansi = false;
		// 	// 	}
		// 	// }

		// });
		// if (found == 0) {
		// 	// $scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.push({ NoWO: $scope.selNoWO, WoNo: $scope.selWoNo, TglWo: $scope.selTglWo, NoBilling: $scope.selNoBilling, NoPolisi: $scope.selNoPolisi, NamaPelanggan: $scope.selNamaPelanggan, PaidNominal: $scope.selTotalTerbayar, SisaNominal: $scope.selSisaNominal, BiayaMaterai: $scope.selBiayaMaterai, IsPrintedKwitansi: $scope.selPreprinted ? "Ya" : "Tidak" });
		// 	$timeout(function(){
		// 		for(var i in $scope.selectedRows){
		// 			$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.push({
		// 				NoWO: $scope.selectedRows[i].NoWO,
		// 				WoNo: $scope.selectedRows[i].WoNo,
		// 				TglWo: $scope.selectedRows[i].TglWo,
		// 				NoBilling: $scope.selectedRows[i].NoBilling,
		// 				NoPolisi: $scope.selectedRows[i].NoPolisi,
		// 				NamaPelanggan: $scope.selectedRows[i].NamaPelanggan,
		// 				PaidNominal: $scope.selectedRows[i].PaidNominal,
		// 				SisaNominal: $scope.selectedRows[i].SisaNominal,
		// 				BiayaMaterai: $scope.selectedRows[i].BiayaMaterai,
		// 				IsPrintedKwitansi: $scope.selectedRows[i].IsPrintedKwitansi
		// 			})
		// 		}
		// 		// $scope.cekPrintedKuitansii();
		// 	},100)
			
		// 	$scope.TotalBayarSum = $scope.TotalBayarSum + $scope.selSisaPembayaran;
		// 	$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed();
			
		// }

		// $scope.IsPrintedKwitansi_Check();
	};

	// Cek Kuitansi 
	$scope.cekPrintedKuitansii = function(){
		var countKuitansi = 0;
		var jmlData = 0;
		angular.forEach($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			// jmlData++;
			if(value.IsPrintedKwitansi == true){
				// countKuitansi++;
				$scope.ShowSimpanCetakKuitansi = false;
			}
		});
		// if(jmlData == countKuitansi){
		// 	$scope.ShowSimpanCetakKuitansi = false;
		// }
	}

	$scope.IsPrintedKwitansi_Check = function () {
		var IsPrintedKwitansi = 0;
		angular.forEach($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			if (value.IsPrintedKwitansi == "Ya" && IsPrintedKwitansi) {
				IsPrintedKwitansi = 1;
			}
		});
		if (IsPrintedKwitansi) {
			HideKuitansi();
		}
		else {
			var sTipe = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
			var isPreprintedReceipt = false;
			angular.forEach($scope.PreprintedSettingData, function (value, key) {
				if (!isPreprintedReceipt) {
					if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
						ShowKuitansi();
						isPreprintedReceipt = true;
						$scope.getDataPreprintedCB();
					}
					else {
						ShowSimpanCetakKuitansi();
					}
				}
			});

			$scope.TambahIncomingPayment_KetKuitansi = true;
		}
	}

	$scope.deleteRow = function (row) {
		var index = $scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TotalBayarSum = $scope.TotalBayarSum - row.entity.TotalTerbayar;
		$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.splice(index, 1);
		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed();
	
		// $scope.IsPrintedKwitansi_Check();
		// $scope.cekPrintedKuitansii();
	};

	$scope.getDataPaymentByCustName = function (Start, Limit, SearchQry, SearchType) {
		IncomingPaymentFactory.getDataServiceCustomerAndPayment(Start, Limit, SearchQry, SearchType)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.data = res.data.Result;
						$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.totalItems = res.data.Total;
						$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.paginationPageSize = Limit;
						$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.paginationPageSizes = [10, 25, 50];
					}
					else {
						$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.data = [];
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	//hilman, end

	//20170425, eric, begin
	$scope.getSalesOrdersBySONumber1 = function () {

		$scope.currentCustomerId = 0;
		IncomingPaymentFactory.getSalesOrdersBySONumber($scope.TambahIncomingPayment_SearchBy_NomorSo, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;

						console.log("1a");
						IncomingPaymentFactory.getCustomersByCustomerId(res.data.Result[0].CustomerId)
							.then(
								function (res) {
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabKota;
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].CustomerName;
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].CustomerId;
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = res.data.Result[0].TglAppointment;
									$scope.currentCustomerId = res.data.Result[0].CustomerId;
								},
								function (err) {
									console.log("err=>", err);
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Alamat = "";
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = "";
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = "";
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Handphone = "";
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_ToyotaId = "";
									$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = "";

								}
							);
					}
					$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}


	$scope.getSalesOrdersByCustomerName1 = function (Start, Limit) {

		$scope.currentCustomerId = 0;
		IncomingPaymentFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahIncomingPayment_SearchBy_NamaPelanggan, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerName1");
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.data = res.data.Result;
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersDibayarByCustomerName1 = function (Start, Limit) {
		//
		//$scope.currentCustomerId = 0;

		IncomingPaymentFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahIncomingPayment_SearchBy_NamaPelanggan, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					for (i = 0; i < res.data.Result.length; i++) {
						res.data.Result[i]["LinkHapus"] = "<a>hapus</a>";
					};
					console.log("getSalesOrdersDibayarByCustomerName1");
					console.log(res);
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.totalItems = res.data.Total;
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}


	//20170425, eric, end


	//20170414, eric, begin
	$scope.getSalesOrdersBySONumber = function () {
		$scope.currentCustomerId = 0;
		IncomingPaymentFactory.getSalesOrdersBySONumber($scope.TambahIncomingPayment_SearchBy_NomorSo, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;

						console.log("1b");
						IncomingPaymentFactory.getCustomersByCustomerId(res.data.Result[0].CustomerId)
							.then
							(
							function (res) {
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
								$scope.currentCustomerId = res.data.Result[0].CustomerId;
							},
							function (err) {
								console.log("err=>", err);
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KodeBranch = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Alamat = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaBranch = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KabupatenKota = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaPt = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Telepon = "";

							}
							);
					}
					$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersByCustomerNameExt = function (Start, Limit) {
		$scope.currentCustomerId = 0;
		IncomingPaymentFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahIncomingPayment_SearchBy_NamaPelanggan, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerNameExt");
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_SoSoSetColumns();
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getSalesOrdersByCustomerName = function (Start, Limit) {
		$scope.currentCustomerId = 0;
		IncomingPaymentFactory.getSalesOrdersByCustomerName(Start, Limit, $scope.TambahIncomingPayment_SearchBy_NamaPelangganPt, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					console.log("getSalesOrdersByCustomerName");
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.data = res.data.Result;
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems = res.data.Total;
					console.log("TotalItems ===>", $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.totalItems);
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.submitDataFinanceIncomingPayments = function (data) {
		console.log(data);

		//handle kirim customerid ""
		if (data[0].CustomerId == ""){
			data[0].CustomerId = 0;
		}
		
		//begin balikin filter tanggal AR Cards dr tipe Bank Statement - Card
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment  == "Bank Statement - Card"){
			data[0].GridARDate = IncomingPaymentFactory.changeFormatDate(data[0].GridARDate);
		}

		IncomingPaymentFactory.submitDataFinanceIncomingPayments(data)
			.then
			(
			function (res) {
				console.log(res.data.Result[0].IPParentId);
				$scope.CetakIPParentId = res.data.Result[0].IPParentId;
				bsNotify.show({
					title: "Berhasil",
					content: "Simpan Berhasil",
					type: 'success'
				});
				
				$scope.ProsesSimpan = false;
				
				if ($scope.Cetak == "Y") {
					$scope.CetakKuitansi();
				}
				else if ($scope.CetakTTUS == "Y") {
					$scope.CetakKuitansiTTUS();
				}

				// BEGIN | NOTO | FORM STATE
				$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = new Date();
				$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo = new Date();
				$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text = "";
				$scope.LihatIncomingPayment_DaftarIncomingPayment_Search = "";

				$scope.TambahIncomingPayment_Batal();
				$scope.GetListIncoming(1, uiGridPageSize);
				FormStateValue = 1;

				$scope.getDataPreprintedCB();

				$scope.Show_ShowLihatIncomingPayment = true;
				$scope.Show_ShowTambahIncomingPayment = false;

				// END | NOTO | FORM STATE
			},
			function (err) {
				console.log("err=>", err);
				if (err != null) {
					bsNotify.show({
						title: "Notifikasi",
						content: err.data.Message,
						type: 'danger'
					});
					
					$scope.ProsesSimpan = false;
					return false;
				}
			}
			)
	};

	$scope.getDataInterBranch = function (Start, Limit) {
		console.log(Limit);
		IncomingPaymentFactory.getInterBranch(Start, Limit, "")
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	//20170414, eric, end
	$scope.getDataBySalesName = function () {
		IncomingPaymentFactory.getDataBySalesName($scope.TambahIncomingPayment_SearchBy_NamaSalesman, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					$scope.TambahIncomingPayment_DaftarSpk_UIGrid.data = res.data.Result;
					// $scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					// $scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
					// $scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					// $scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					// $scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
					$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
					$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
					//$scope.loading=false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataByNomorSPK = function (NomorSPK) {
		console.log("getDataByNomorSPK");
		IncomingPaymentFactory.getDataByNomorSPK(NomorSPK, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentSPKId = res.data.Result[0].SPKId;
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = res.data.Result;

						console.log(res.data.Result.length);
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.totalItems = res.data.Result.length;
						console.log($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.totalItems);
						$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
						$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
						$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
						$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
						$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
						$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;

						//$scope.getDataProspectByCustomerId(res.data.Result[0].CustomerId);
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
						//CR4
						$scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data;
						console.log('$scope.assignmentData',$scope.assignmentData);

					}
					$scope.loading = false;
					
				},
				function (err) {
					$scope.currentSPKId = 0;
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataIncomingPaymentDetail = function () {
		IncomingPaymentFactory.getDataIncomingPaymentDetail()
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
					}
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}

	// $scope.getDataBookingFeeByNomorSPK = function(NomorSPK) {
	//     IncomingPaymentFactory.getDataByNomorSPK(NomorSPK)
	//     .then(
	//         function(res){
	//         	gridData = [];
	// $scope.grid.data = res.data.Result;
	//             $scope.loading=false;
	//         },
	//         function(err){
	//             console.log("err=>",err);
	//         }
	//     );

	// }
	$scope.getDataProspectByCustomerId = function (CustomerId) {
		IncomingPaymentFactory.getProspectByCustomerId(CustomerId)
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	$scope.submitData = function (data) {
		IncomingPaymentFactory.submitData(data)
			.then(
				function (res) {
					//$scope.loading=false;
					console.log(res);

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataBankAccountCashDelivery = function () {
		IncomingPaymentFactory.getDataBankAccountComboBoxNew()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_TujuanPembayaran_CashDeliveryOptions = res.data;
					console.log($scope.TujuanPembayaran_CashDelivery);
					$scope.TambahIncomingPayment_CashDelivery_NamaBank = $scope.TujuanPembayaran_CashDelivery;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataBankAccount = function (CustomerId) {
		IncomingPaymentFactory.getDataBankAccountComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_BankTransfer_RekeningPenerimaOptions = res.data;
					$scope.TambahIncomingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = $scope.TambahIncomingPayment_BankTransfer_RekeningPenerimaOptions[0];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataCheque = function (CustomerId) {
		IncomingPaymentFactory.getDataChequeComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_BankTransfer_NoChequeOptions = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TanggalPembayaranOK = true;
	// $scope.selectGrid = false;
	$scope.TanggalPembayaranChanged = function () {
		IncomingPaymentFactory.getDataPeriodeReversal()
			.then(
				function (res) {
					$scope.loading = false;
					angular.forEach(res.data.Result, function (value, key) {
						if (value.ReversalName == "Backdate Pembukuan") {
							var backdate = new Date().getTime() - (value.ReversalPeriod * 24 * 60 * 60 * 1000)
							$scope.backdatePeriod = value.ReversalPeriod;
							$scope.backdate = backdate;
						}
					});

				},
				function (err) {
					console.log("err=>", err);
				}
			);
		console.log($scope.TanggalPembayaranOK);
		var dt = $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer;
		var now = new Date();
		if (dt < $scope.backdate) {
			$scope.errTanggalPembayaran = "Tanggal Pembayaran tidak boleh lebih awal " + $scope.backdatePeriod + " hari dari tanggal hari ini";
			$scope.TanggalPembayaranOK = false;
		}
		else if (dt > now) {
			$scope.errTanggalPembayaran = "Tanggal Pembayaran tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalPembayaranOK = false;
		}
		else {
			$scope.TanggalPembayaranOK = true;
		}
		console.log('asdasd ' + $scope.TanggalPembayaranOK);
	}

	$scope.getDataBank = function () {
		IncomingPaymentFactory.getDataBankComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions = res.data;
					$scope.TambahIncomingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions[0];
					$scope.TambahIncomingPayment_CreditDebitCard_NamaBankOptions = res.data;
					$scope.TambahIncomingPayment_RincianPembayaran_NamaBank_CreditDebitCard = $scope.TambahIncomingPayment_CreditDebitCard_NamaBankOptions[0];
					$scope.optionsTambahIncomingPayment_SearchBy_NamaBank = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	//Alvin, begin
	$scope.getDataBranchCB = function () {
		IncomingPaymentFactory.getDataBranchGroupComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsTambahIncomingPayment_NamaBranchGroup_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataPreprintedCB = function () {
		IncomingPaymentFactory.getDataPreprintedComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.TambahIncomingPayment_NomorKuitansiOption = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getNominalMaterai = function (PaymentAmount) {
		IncomingPaymentFactory.getMaterai(PaymentAmount)
			.then(
				function (res) {
					//$scope.loading = false;
					console.log("getNominalMaterai2");
					console.log(res);
					console.log(res.data.Result[0]);
					console.log(res.data.Result[0].value);
					$scope.TambahIncomingPayment_BiayaBiaya_Nominal = res.data.Result[0].value;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataCharges = function () {
		IncomingPaymentFactory.getDataCharges()
			.then(
				function (res) {
					$scope.loading = false;
					//$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	//begin | Noto | 180514 | 00186 UT
	$scope.TambahIncomingPayment_NoKuitansi = true;
	//end | Noto | 180514 | 00186 UT
	$scope.getDataPreprinted = function () {
		console.log("getDataPreprinted2");
		console.log($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment);
		IncomingPaymentFactory.getDataPreprinted($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					$scope.loading = false;
					$scope.Preprinted = res.data.Result[0].isPreprintedReceipt;
					console.log("Preprinted = ", $scope.Preprinted);
					if ($scope.Preprinted == true) {
						ShowKuitansi();
						$scope.getDataPreprintedCB();
					}
					else { HideKuitansi(); }

					//$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataVendor = function () {
		IncomingPaymentFactory.getDataVendorComboBox()
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahIncomingPayment_Top_NamaLeasingOption = res.data;
				//$scope.LihatIncomingPayment_Top_NamaLeasingOption = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}

	$scope.getDataLeasing = function () {
		IncomingPaymentFactory.getLeasingComboBox()
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahIncomingPayment_Top_NamaLeasingOption = res.data;
				// $scope.LihatIncomingPayment_Top_NamaLeasingOption = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}
	//Alvin, end

	//begin | Noto | 180514 | 00197 UT
	$scope.getDataInsurance = function () {
		IncomingPaymentFactory.getInsuranceComboBox()
			.then
			(
			function (res) {
				$scope.loading = false;
				$scope.TambahIncomingPayment_Top_NamaLeasingOption = res.data;
				//$scope.LihatIncomingPayment_Top_NamaLeasingOption = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}
	//end | Noto | 180514 | 00197 UT

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function (rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function (rows) {
		console.log("onSelectRows=>", rows);
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------

	//20170425, eric, begin
	$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,

		showGridFooter: true,
		showColumnFooter: true,
		enableFiltering: true,

		enableSorting: false,
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC' },
			{ name: 'Biaya Materai', field: 'Materai', visible: false },
			{ name: 'Total Terbayar', field: 'PaidNominal', type: 'number', cellFilter: 'rupiahC' },
			// { name: 'Saldo', field: 'SisaPembayaran', type: 'number', cellFilter: 'rupiahC' }, //UAT | 104 | NOTO | 20180830
			{
				name: 'Saldo', field: 'SisaPembayaran',
				enableCellEdit: false, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahCIP',
				},
			// {
			// 	name: 'Saldo', field: 'SisaPembayaran', aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number',
			// 	cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
			// 		return 'canEdit';
			// 	}, enableCellEdit: true, cellFilter: 'rupiahC'
			// },
			{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRowSalesOrderDibayar(row)">Hapus</button>' },
			//{ name: 'Action', field:'LinkHapus'},
			//{ name: 'Hyperlink', cellTemplate:'<a ng-click="grid.appScope.deleteSalesOrderDibayar(row.entity)">hapus</a>'}
		],
		onRegisterApi: function (gridApi) { 
			$scope.gridApi_DaftarSalesOrderDibayar_UIGrid = gridApi; /////zzzzzzzzz
			// gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			// 	if (row.isSelected == false) {
			// 		$scope.currentCustomerId = 0;
			// 		$scope.currentSPKId = 0;
			// 	}
			// 	else {
			// 		$scope.currentCustomerId = row.entity.CustomerId;
			// 		$scope.currentSPKId = row.entity.SPKId;
			// 	}
			// });
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersDibayarByCustomerName1(pageNumber, pageSize);
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},

	};
	$scope.hapusRow = 0;
	$scope.deleteRowSalesOrderDibayar = function (row) {
		var index = $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.indexOf(row.entity);
		$scope.hapusRow = row.entity.SisaPembayaran;
		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.splice(index, 1);
		// $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed();

		// Kalkulasi hapus | Irfan | 26/01/21
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" || $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual"){
			$scope.LoopingBiaya();
			var TotalPembayaran = $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue();
			console.log('cek agregation value', TotalPembayaran)
			$scope.PemOPD = TotalPembayaran;
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) - $scope.hapusRow + parseInt($scope.tmpKredit) - parseInt($scope.tmpDebit);
			var tmpSelisih =
			parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalPembayaran -  parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		} 
	};


	//20170425, eric, end	
	// 20170414, eric, begin

	//grid ini digunakan untuk Search SO by SO Number
	$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: false,
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: false },
			{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
			//{ name: 'Biaya Materai', field: 'Materai', visible:false },
			//{ name: 'Total Yang Harus Dibayar', field: 'TotalNominal' },
			{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
			{ name: 'Saldo', field: 'SisaPembayaran', cellFilter: 'rupiahC' },

		]
	};

	//grid ini digunakan untuk Search SO by Customer Name
	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentCustomerId = 0;
					$scope.currentSPKId = 0;
				}
				else {
					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSONumber = row.entity.SONumber;
					console.log($scope.currentCustomerId);
					IncomingPaymentFactory.getSalesOrdersBySONumber($scope.currentSONumber, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
						.then(
							function (res) {
								console.log("masuk1");
								console.log($scope.currentCustomerId);
								$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
								console.log("TotalItems ===>", $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.totalItems);
								$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.totalItems = res.data.Total;
								console.log("TotalItems ===>", $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.totalItems);
							},
							function (err) {
								console.log("err=>", err);
							}
						);
					console.log($scope.currentCustomerId);
					console.log("1c");
					IncomingPaymentFactory.getCustomersByCustomerId($scope.currentCustomerId)
						.then(
							function (res) {
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
								$scope.currentCustomerId = res.data.Result[0].CustomerId;
							},
							function (err) {
								console.log("err=>", err);
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KodeBranch = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Alamat = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaBranch = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_KabupatenKota = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaPt = "";
								$scope.TambahIncomingPayment_InformasiPelangan_Branch_Telepon = "";

							}
						);
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersByCustomerName(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'NomorSO', field: 'SONumber' },
			{ name: 'Model', field: 'Model' },
			{ name: 'Tipe', field: 'Tipe' },
			{ name: 'Warna', field: 'Warna' },
		]
	};
	// 20170414, eric, end

	//20170511, Alvin, begin, grid Lihat
	$scope.LihatIncomingPayment_DaftarIncoming_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,
		paginationPageSizes: [10, 25, 50],

		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIPParentId = 0;
				}
				else {
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {

				$scope.GetListIncoming(pageNumber, pageSize);
				paginationOptions_MainIncoming.pageNumber = pageNumber;
				paginationOptions_MainIncoming.pageSize = pageSize;
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
			gridApi.core.on.renderingComplete($scope, function () {
				console.log('Form State => ' + FormStateValue);

				$scope.GetDataPreprintedSetting();

				if (FormStateValue == FormState.isDefault) {
					$scope.SetMainGridADH();
				}
				else {
					$scope.GetListIncoming(1, uiGridPageSize);
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal Incoming Payment', field: 'TanggalIncoming', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'IPParentId', field: 'IPParentId', visible: false },
				{ name: 'Nomor Incoming Payment', field: 'IncomingNo', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailIncoming(row)">{{row.entity.IncomingNo}}</a>' },
				{ name: 'Nomor Incoming Payment Rev', field: 'IncomingNoRev', visible: false },
				{ name: 'Tipe Incoming Payment', field: 'IncomingPaymentType', visible: true },
				{ name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Metode Pembayaran', field: 'PaymentMethod' },
				{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan' },
				{ name: 'Tanggal Pengajuan', field: 'TglPengajuan' },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan' },
				{ name: 'Alasan Pengajuan', field: 'AlasanPengajuan' },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan' },
				{ name: 'History Lost', field: 'HistoryLost', visible: false }
			]
	};

	$scope.SetMainGridADH = function () {
		console.log($scope.user.OrgCode);
		if ($scope.user.RoleName == 'ADH' || $scope.user.RoleName == 'ADH BP' ) {
			$scope.GetListIncomingADH(1, uiGridPageSize);
		}
	};

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.LihatIncomingPayment_RincianPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			enableGridMenu: true,
			columnDefs:
				[
					{ name: 'Metode Pembayaran', field: 'PaymentMethod', visible: true },
					{ name: 'Tanggal Pembayaran', field: 'PaymentDate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nama Bank Pengirim/Credit/Debit Card', field: 'SenderBankId' },
					{ name: 'Nama Pengirim', field: 'SenderName' },
					{ name: 'Nomor Cheque/Credit/Debit Card', field: 'NoCheque' },
					{ name: 'Nomor Rekening Penerima', field: 'ReceiverAccNumber' },
					{ name: 'Nama Bank Penerima', field: 'BankPenerima' },
					{ name: 'Nama Pemegang Rekening', field: 'Penerima' },
					{ name: 'Nama Mesin EDC', displayName: "Nama Mesin EDC", field: 'NamaEDC' },
					{ name: 'Keterangan', field: 'Keterangan' },
					{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_DaftarAR_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			onRegisterApi: function (gridApi) { $scope.LihatIncomingPayment_DaftarAR_gridAPI = gridApi; },
			columnDefs:
				[
					{ name: 'BankStatementCardId', field: 'BankStatementCardId', visible: false },
					{ name: 'Tanggal AR Card', displayName: 'Tanggal AR Card', field: 'ARDate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Tipe Kartu', field: 'CardType', visible: true },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_Biaya_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tipe Biaya Lain-Lain', field: 'OtherChargesType', visible: true },
					{ name: 'Debet / Kredit', field: 'DebitCreditType' },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_DaftarWorkOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			showColumnFooter: true,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nomor Billing', field: 'NoBilling', visible: true },
					{ name: 'Nomor Polisi', field: 'NoPolisi', visible: true },
					// { name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC' },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'Pembayaran', field: 'PaidNominal', 
						aggregationType: uiGridConstants.aggregationTypes.sum, enableCellEdit: true, type: 'number',
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
						}, cellFilter: 'rupiahC'
					},
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
					{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Keterangan', displayName: "", field: 'Keterangan', visible: true },
					{ name: 'Nominal', field: 'NominalDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: false },
					{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: false },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: false },
					{ name: 'Pembayaran', field: 'PaidNominal', visible: true, cellFilter: 'rupiahC', enableCellEdit: false },
					// {
					// 	name: 'Pembayaran', field: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
					// 	cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					// 		return 'canEdit';
					// 	}, enableCellEdit: true, type: 'number', cellFilter: 'rupiahC',
					// }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_Appointment_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Appointment', field: 'NoAppointment', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_WoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Down Payment', field: 'NominalDP', visible: true, cellFilter: 'rupiahC' },
					{ name: 'Own Risk', field: 'OwnRisk', visible: false },
					{ name: 'Biaya Materai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_WoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'currency:"":2, visible:false' },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaNominal', cellFilter: 'rupiahC' }

				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_Materai_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', field: 'SOId', visible: false },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', displayname: "No SO", field: 'SOId', visible: false },
					{ name: 'Tanggal SO', displayname: "Tanggal SO", field: 'SODate', visible: true, type: 'date', cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'No SO', displayname: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
					{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_SoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'No SO', displayName: "Nomor SO", field: 'SOId', visible: false },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', displayName: "No SO", field: 'SOId', visible: false },
					{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
					{ name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SPK', field: 'SPKId', visible: false },
					{ name: 'Tgl SPK', displayName: "Tanggal SPK", field: 'TanggalSPK', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', cellFilter: 'rupiahC' }
				]
		};

	$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Rangka', field: 'NoRangka', visible: true },
					{ name: 'No SPK', displayName: 'Nomor SPK', field: 'NoSPK', visible: true },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'NoSO', visible: true },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' },
					{ name: 'Biaya Materai', field: 'Materai', visible: false },
					{ name: 'Total Terbayar', field: 'Total', cellFilter: 'rupiahC' }
				]
		};

	//20170511, Alvin, end, grid Lihat


	//20170511, Alvin, begin, grid UnknownCards
	$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahIncomingPayment_DaftarArCreditDebit_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentTglAR = $scope.TambahIncomingPayment_TanggalIncomingPayment;
					$scope.currentCardType = "";
					$scope.currentNominal = 0;
					$scope.selectGrid = true;
				}
				else {
					$scope.currentTglAR = row.entity.PaymentDate;
					console.log($scope.currentTglAR);
					$scope.currentCardType = row.entity.CardType;
					$scope.currentNominal = row.entity.Nominal;
					$scope.selectGrid = false;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.GetListUnknownCards(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tgl AR Cards', displayName: 'Tanggal AR Cards', field: 'PaymentDate', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Tipe Kartu', field: 'CardType' },
				{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' }
			]
	};
	//20170511, Alvin, end, grid UnknownCards

	// 20170414, marthin, start
	//grid ini digunakan untuk Search SO by Customer Name
	var tmpSelection = [];
	$scope.Selected = function(){
		
	}

	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		//useCustomPagination: true,
		//useExternalPagination: true,
		enableFiltering: true,
		enableSorting: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_GridApi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (tmpSelection.length == 0){
					tmpSelection.push(row.entity)
				} else {
					tmpSelection = [];
					var cekArray = 0;
					for (var i=0; i<tmpSelection.length; i++){
						if (tmpSelection[i].SOId == row.entity.SOId){
							// hapus array nya, index nya i
							tmpSelection.splice(i,1);

							cekArray++;
						}
					}

					if (cekArray == 0){
						tmpSelection.push(row.entity)
					}
				}
				var sisaDP = 0;
				var paidDP = 0;
				for (var i=0; i<tmpSelection.length; i++){
					// total yang mau di jumlah
					cekSOId = tmpSelection[i].SOId;
					sisaDP+=parseInt(tmpSelection[i].SisaDP);
					paidDP+=parseInt(tmpSelection[i].PaidDP);

					
				}
				console.log("cek Sisa DP", sisaDP);
				console.log("cek Paid DP", paidDP);
				console.log("Cek SOID >>>>>>>>",cekSOId);
				console.log("Cek Selction >>>>>>>>",tmpSelection);

				var obj = [{
					'SOId' : cekSOId,
					'Materai' : 0,
					'TotalNominal' : 0,
					'PaidDP' : paidDP,
					'SisaDP' :  sisaDP
				}]
				console.log("cek OBJ", obj)
				$scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = obj;
				

				if (row.isSelected == false) {
					$scope.currentCustomerId = 1;
					$scope.currentSONumber = 0;
					//DisableAll_TambahIncomingPayment_InformasiPelanggan();
				}
				else {
					// Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
					// Enable_TambahIncomingPayment_InformasiPembayaran_Materai();

					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSONumber = row.entity.SONumber;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSOId = row.entity.SOId;
					$scope.currentSaldo = row.entity.SisaDP;

					console.log("1d");
					IncomingPaymentFactory.getCustomersByCustomerId($scope.currentCustomerId)
						.then
						(
						function (res) {
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].CustomerCode;
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
							$scope.currentCustomerId = res.data.Result[0].CustomerId;
						},
						function (err) {
							console.log("err=>", err);
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = "";
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = "";
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = "";
							$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = "";
						}
						);

					// IncomingPaymentFactory.getSalesOrdersBySONumber($scope.currentSONumber, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
					// 	.then(
					// 		function (res) {
					// 			$scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = res.data.Result;
					// 			console.log("TotalItems ===>", $scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.totalItems);
					// 			$scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.totalItems = res.data.Total;

					// 		},
					// 		function (err) {
					// 			console.log("err=>", err);
					// 		}

					// 	);

				}
				if(tmpSelection.length == 0){
					$scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = [];
				}
			});
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// $scope.getSalesOrdersByCustomerName(pageNumber,pageSize);

			// console.log("pageNumber ==>", pageNumber);
			// console.log("pageSize ==>", pageSize);
			// });
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'currency' },
		]
	};

	$scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: false,
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi; },
		enableRowSelection: false,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'SOId', field: 'SOId', visible: false },
				{ name: 'Biaya Materai', field: 'Materai', visible: false },
				{ name: 'Total yg harus dibayar', field: 'TotalNominal', visible: false },
				{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
				{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
			]
	};

	$scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: false,
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi; },
		enableRowSelection: false,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
				//{ name: 'Biaya Materai', field: 'Materai', cellFilter: 'rupiahC'},
				//{ name: 'Total yg harus dibayar', field: 'TotalNominal', cellFilter: 'rupiahC' },
				{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
				{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
			]
	};


	$scope.getSalesOrderBySONumberParts = function () {
		IncomingPaymentFactory.getSalesOrdersBySONumber($scope.TambahIncomingPayment_SearchBy_NomorSo, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					if (res.data.Result.length > 0) {
						$scope.currentCustomerId = res.data.Result[0].CustomerId;
						$scope.currentSPKId = res.data.Result[0].SPKId;
						$scope.currentSOId = res.data.Result[0].SOId;

						console.log("1e");
						IncomingPaymentFactory.getCustomersByCustomerId($scope.currentCustomerId)
							.then(
								function (res) {
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].CustomerCode;
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
									$scope.currentCustomerId = res.data.Result[0].CustomerId;
								},
								function (err) {
									console.log("err=>", err);
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = "";
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = "";
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = "";
									$scope.TambahIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = "";

								}

							);
					}
					if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
						$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					} else {
						$scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data = res.data.Result;
					}

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}


	// $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_button_SearchBy = function() {
	// console.log("SoSo");
	// IncomingPaymentFactory.getSalesOrdersByCategory($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_SearchDropdown, $scope.inputTambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search)
	// .then(
	// function(res) {
	// $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.data = res.data.Result;
	// $scope.TambahIncomingPayment_SoSoSetColumns();
	// console.log("TotalItems ===>",$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems);
	// $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.totalItems = res.data.Total;				
	// $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSize = 10;
	// $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.paginationPageSizes = [10];
	// }, function(err) {
	// console.log("err=>", err);
	// }

	// );
	// }	
	// 20170414, marthin, end

	//20170425, eric, begin
	//grid ini digunakan untuk Search SO by Customer Name
	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_GridApi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentCustomerId = 0;
					$scope.currentSPKId = 0;
					$scope.currentNoSO = "";
					$scope.currentSONumber = "";
					$scope.currentNominalBilling = 0;
					$scope.currentSODate = "";
					$scope.currentPaidNominal = 0;
					$scope.currentSisaPembayaran = 0;
				}
				else {
					$scope.currentCustomerId = row.entity.CustomerId;
					$scope.currentSPKId = row.entity.SPKId;
					$scope.currentSONumber = row.entity.SONumber;
					$scope.currentNoSO = row.entity.SOId;
					$scope.currentNominalBilling = row.entity.TotalNominal;
					$scope.currentPaidNominal = row.entity.PaidNominal;
					$scope.currentSisaPembayaran = row.entity.SisaPembayaran;
					$scope.currentSODate = row.entity.SODate;
					console.log($scope.currentNoSO);
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getSalesOrdersByCustomerName1(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'SPKId', displayName: 'SPKId', field: 'SPKId', visible: false },
			{ name: 'SOId', displayName: 'SOId', field: 'SOId', visible: false },
			{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
			{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
			{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
			{ name: 'Saldo', field: 'SisaPembayaran', cellFilter: 'rupiahC' },
		]
	};
	//20170425, eric, end

	//20170506, Alvin,begin	
	$scope.TambahPartsFullPayment_AddItem = function () {
		console.log("TambahPartsFullPayment_AddItem");
		var TotalPemSO = 0;
		$scope.namPem1 = $scope.TambahIncomingPayment_SearchBy_NamaPelanggan;
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_GridApi.selection.getSelectedRows().forEach(function (row) {
			console.log(row.SOId);
			var found = 0;
			$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.SOId);
				if (row.SOId == obj.SOId)
					found = 1;
			});
			if (found == 0) {
				//$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.push( { SOId: $scope.currentNoSO, SONumber: $scope.currentSONumber, NominalBilling: $scope.currentNominalBilling, SODate: $scope.currentSODate, TotalNominal:$scope.currentNominalVilling, PaidNominal:$scope.currentPaidNominal , SisaPembayaran:$scope.currentSisaPembayaran})
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.push({ SOId: row.SOId, SONumber: row.SONumber, NominalBilling: row.TotalNominal, SODate: row.SODate, TotalNominal: row.TotalNominal, PaidNominal: row.PaidNominal, SisaPembayaran: row.SisaPembayaran })
				console.log("INI DIA >>>>>>>",$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid);
				console.log("SISAH NOMINAL >>>>>", row.SisaPembayaran);
				console.log("TOTAL NOMINAL >>>>>", $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue());
			}
		}
		)
		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
			if(obj.SisaPembayaran != null){
				TotalPemSO += obj.SisaPembayaran;
			}else{
				TotalPemSO = 0;
			}
			$scope.PemOPD = TotalPemSO;
		})

		if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" || $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" ) && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			$scope.LoopingBiaya();
			// var TotalPembayaran = $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue();
			var TotalPembayaran = $scope.PemOPD
			console.log('cek agregation value', TotalPembayaran)
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.tmpKredit) - parseInt($scope.tmpDebit);
			var tmpSelisih =
			parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalPembayaran -  parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
	};
	//20170506, Alvin, end



	$scope.TambahIncomingPayment_DaftarSpk_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: false,
		enableFiltering: true,
		enableRowSelection: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarSpk = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				//$scope.TambahIncomingPayment_DaftarSpk_UIGrid.data=res.data.Result;
				if (row.isSelected == false) {
					$scope.currentSPKId = 0;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = "";
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = "";
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = "";
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = "";
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = "";
					$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
					$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
				}
				else {
					$scope.getDataByNomorSPK(row.entity.NomorSPK);
					$scope.currentSPKId = row.entity.NomorSPK;
					$scope.pilihNoSPK = row.entity;
					//$scope.getDataProspectByCustomerId(row.entity.CustomerId);
				}
			});

			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		columnDefs: [
			//{ name:'CustomerId',    field:'CustomerId', visible:false},
			{ name: 'Nomor SPK', displayName: "Nomor SPK", field: 'NomorSPK' },
			{ name: 'Nama Pelanggan', field: 'CustomerName' },
			{ name: 'Handphone', field: 'Handphone' },
			{ name: 'Model', field: 'Model' },
			{ name: 'Tipe', field: 'Tipe' },
			{ name: 'Warna', field: 'Warna' },
			//{ name:'NamaSalesman',    field:'NamaSalesman'},
			//{ name:'NominalBookingFee',    field:'NominalBookingFee'},
			//{ name:'SPKId',    field:'SPKId', visible:false}
			//{ name:'SalesId',    field:'SalesId'},
			//{ name:'SalesName',    field:'SalesName'},
		]
	};

	$scope.controlTitipanDP = true;
	$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: false,//>>>>>>>>>>>>>>>>>>>>>
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi;
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				console.log("test", rowEntity);
				console.log("colDef ", colDefs);
				if(newValue > rowEntity.SisaDP){
					var newValue = 0;   
					rowEntity[colDefs.name] = newValue;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
								type: 'warning'
							}
						);  
				}
				console.log('newValue',newValue);
				console.log('rowEntity',rowEntity);
				if(newValue == 'undefined' || typeof newValue == 'undefined'){
					$scope.controlTitipanDP = true;
					$scope.TambahIncomingPayment_SetupBiaya();
				}else
				if(newValue == rowEntity.SisaDP){
					$scope.controlTitipanDP = false;
					$scope.TambahIncomingPayment_SetupBiaya();
				}else{
					$scope.controlTitipanDP = true;
					$scope.TambahIncomingPayment_SetupBiaya();
				}
			});
		},
		enableRowSelection: false,
		enableSelectAll: false,
		showGridFooter: true,
		showColumnFooter: true,
		//Alvin, 20170506 begin		
		columnDefs: [
			//hilman, begin
			//{ name:'Tanggal Spk',    field:'Tanggal',cellFilter: 'date:\'dd-MM-yyyy\''},
			{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'TglSPK', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
			{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', visible: false, cellFilter: 'rupiahC' },
			{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
			{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'' },
			{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
			{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
			{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
			{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
			{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
			{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC' }
			//hilman, end
		]
		//Alvin, 20170506 end					
	};
	//hilman,begin

	$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_InformasiPembayaran_UIGrid2 = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.selNoWO = row.entity.NoWO;
					$scope.selWoNo = row.entity.WoNo;
					$scope.selTglWo = row.entity.TglWo;
					$scope.currentCustomerId = row.entity.CustomerId
					$scope.selNoBilling = row.entity.NoBilling;
					$scope.selNoPolisi = row.entity.NoPolisi;
					$scope.selNamaPelanggan = row.entity.NamaPelanggan;
					$scope.selTotalTerbayar = row.entity.PaidNominal;
					$scope.selSisaPembayaran = row.entity.SisaPembayaran;
					$scope.selSisaNominal = row.entity.SisaNominal;
					$scope.selBiayaMaterai = row.entity.BiayaMaterai;
					$scope.selPreprinted = row.entity.IsPrintedKwitansi;
				}
				else {
					$scope.selNoWO = "";
					$scope.selWoNo = "";
					$scope.selTglWo = "";
					$scope.currentCustomerId = "";
					$scope.selNoBilling = "";
					$scope.selNoPolisi = "";
					$scope.selNamaPelanggan = "";
					$scope.selTotalTerbayar = "";
					$scope.selSisaPembayaran = "";
					$scope.selSisaNominal = "";
					$scope.selBiayaMaterai = "";

				}
			});

			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getDataPaymentByCustName(pageNumber, pageSize, newPem, "NamaPembayar^Service - Full Payment");
			});
		},
		enableRowSelection: true,
		enableSelectAll: true,
		columnDefs:
			[
				//{ name:'NoWO',  displayName: 'Nomor WO',  field:'NoWO', visible:false},
				{ name: 'WOId', displayName: 'WOId', field: 'NoWO', visible: false },
				{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoNo', width: "16%" },
				{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'', width: "10%" },
				{ name: 'Nomor Billing', field: 'NoBilling', width: "16%" },
				{ name: 'Nomor Polisi', field: 'NoPolisi', width: "10%" },
				{ name: 'Nama Pelanggan', field: 'NamaPelanggan', width: "20%" },
				{ name: 'Nominal Terbayar', displayName: 'Nominal Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
				{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC' },
				{ name: 'Biaya Materai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false },
				{ name: 'Kuitansi Penagihan', field: 'IsPrintedKwitansi', visible: true, cellTemplate: '<div>{{row.entity.IsPrintedKwitansi?"Ya":"Tidak"}}</div>' }
			]
	};

	$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid = {
		enableSorting: false,
		onRegisterApi: function (gridApi) { $scope.gridApi_InformasiPembayaran = gridApi; 
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				var test = newValue.toFixed();
				var Newint = parseInt(test);
				console.log("cek decimal", Newint)
				console.log("cek decimal2", test)

				console.log("test", rowEntity);
				console.log("colDef ", colDefs);
				rowEntity.Pembayaran = Newint;
				if(Newint > rowEntity.SisaNominal){
					var Newint = 0;   
					rowEntity[colDefs.name] = Newint;
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
								type: 'warning'
							}
						);  
				}else if(Newint <= 0){
					var Newint = 0;
						rowEntity[colDefs.name] = Newint;   
						bsNotify.show(
								{
										title: "Peringatan",
										content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
										type: 'warning'
								}
						);      
				}
				
				$scope.TotalPembayaranAll = 0;
				$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (row) {
						console.log("isi row",row);
						var JmlKredit = 0;
						var JmlDebet = 0;
						if ( row.Pembayaran != undefined ){
							$scope.TotalPembayaranAll += row.Pembayaran ;
						}
						console.log("TotalPembayaranAll",$scope.TotalPembayaranAll);
						
						for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
							console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
							
							if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								JmlKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							}
							else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								JmlDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								// TotPerhitungan;
							}
						}
							var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - parseInt($scope.TotalPembayaranAll) - parseInt(JmlKredit) + parseInt(JmlDebet);
							console.log("tmpSelisih",tmpSelisih);
							if(tmpSelisih < 0){
								tmpSelisih = 0;
							}
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
					}
				);

			});
		},
		enableRowSelection: false,
		enableSelectAll: false,
		showColumnFooter: true,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: 'WOId', displayName: 'WOId', field: 'NoWO', visible: false },
			{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoNo', enableCellEdit: false },
			{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'', enableCellEdit: false },
			{ name: 'Nomor Billing', field: 'NoBilling', enableCellEdit: false },
			{ name: 'Nomor Polisi', field: 'NoPolisi', enableCellEdit: false },
			{ name: 'Nama Pelanggan', field: 'NamaPelanggan', enableCellEdit: false },
			{ name: 'Nominal Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC', enableCellEdit: false },
			{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'currency:"":0', enableCellEdit: false ,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
			},
			{ name: 'Biaya Materai', field: 'BiayaMaterai', cellFilter: 'rupiahC', visible: false }, ,
			{
				name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				},
				enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0',//>>>>>
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalPembayaranAll | number}}</div>',
			},
			{ name: 'Preprinted Kuitansi', field: 'IsPrintedKwitansi', visible: true, cellTemplate: '<div>{{row.entity.IsPrintedKwitansi?"Ya":"Tidak"}}</div>' },
			//{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRow(row)">Hapus</button>'},
			{ name: 'Action', cellTemplate: '<a ng-click="grid.appScope.deleteRow(row)">Hapus</a>' },
		]
	};
	//hilman,end

	$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Interbranch_UIGrid = {
		enableSorting: false,
		enableFiltering: true,
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_Interbranch = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					// //20170414, eric, begin
					console.log(row.entity.InterbranchId);
					$scope.currentInterBranchId = row.entity.InterbranchId;
					$scope.currentNamaBranchPenerima = "";
					$scope.currentNomorIncomingPayment = "";
					//$scope.currentTanggalBank = row.entity.TanggalBank;
					$scope.currentTanggalBank = row.entity.Date;
					$scope.currentNominalPembayaran = row.entity.Nominal;
					//$scope.currentNamaPelanggan = row.entity.NamaPelanggan;
					$scope.currentNamaPelanggan = row.entity.CustomerName;
					//$scope.currentUntukPembayaran = row.entity.KeteranganInterBranch;
					$scope.currentUntukPembayaran = row.entity.PaymentFor;
					$scope.currentDescription = row.entity.Description;
					$scope.currentPaymentDate = row.entity.Date;
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_Interbranch = row.entity.Nominal - $scope.CurrentNominal;
					// //20170414, eric, ebd
				}
				else {
					$scope.currentInterBranchId = 0;
					$scope.currentNamaBranchPenerima = "";
					$scope.currentNomorIncomingPayment = 0;
					$scope.currentNominalPembayaran = 0;
					$scope.currentNamaPelanggan = "";
					$scope.currentUntukPembayaran = "";
					$scope.currentTanggalBank = new Date();

				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
				//$scope.getDataInterbranch(pageNumber,uiGridPageSize);			
				IncomingPaymentFactory.getInterBranch(pageNumber, uiGridPageSize, "")
					.then(
						function (res) {
							if (res.data.Result.length > 0) {
								$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = res.data.Result;
							}
							$scope.loading = false;
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		multiselect: false,
		columnDefs: [
			//20170414, eric, begin
			// { name: 'Nama Branch Penerima', field: 'KeteranganInterBranch' },
			// { name: 'Nomor Incoming Payment', field: 'Nominal' },
			// { name: 'Tanggal Incoming Payment', field: 'TanggalBank', cellFilter: 'date:\'dd-MM-yyyy\'' },
			// { name: 'Nominal Pembayaran', field: 'Nominal' },
			// { name: 'Nama Pelanggan', field: 'NamaPelanggan' },
			// { name: 'Untuk Pembayaran', field: 'UnknownDescription' },
			// { name: 'InterBranchId', field: 'InterBranchId', visible: false }

			{ name: 'Nama Branch Penerima', field: 'Name' },
			{ name: 'Nomor Incoming Payment', field: 'IncomingNo' },
			{ name: 'Tanggal Incoming Payment', field: 'Date', cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
			{ name: 'Nama Pelanggan', field: 'CustomerName' },
			{ name: 'Untuk Pembayaran', field: 'PaymentFor' },
			{ name: 'InterBranchId', field: 'InterbranchId', visible: false }
			//20170414, eric, end
		]
	};

	//Alvin, 20170527, begin
	//Proforma Invoice
	$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahIncomingPayment_DaftarNoRangka_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.currentProformaInvoiceId = row.entity.ProformaInvoiceId;
					$scope.currentNoRangka = row.entity.NoRangka;
					$scope.currentNoSPK = row.entity.NoSPK;
					$scope.currentNoSO = row.entity.NoSO;
					$scope.currentCustomerId = row.entity.CustomerId
				}
				else {
					$scope.currentProformaInvoiceId = "";
					$scope.currentNoRangka = "";
					$scope.currentNoSPK = "";
					$scope.currentNoSO = "";
					$scope.currentCustomerId = "";
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'ProformaInvoiceId', field: 'ProformaInvoiceId', visible: false },
				{ name: 'ProformaInvoiceCode', field: 'ProformaInvoiceCode', visible: false },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', displayName: "Nomor SPK", field: 'NoSPK' },
				{ name: 'Nomor SO', displayName: "Nomor SO", field: 'NoSO' },
				{ name: 'SOId', field: 'SoId', visible: false },
				{ name: 'Nama di Billing', field: 'NamaBilling' },
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				{ name: 'Nominal PI', displayName: "Nominal Kuitansi Penagihan", field: 'NominalPI', cellFilter: 'rupiahC' },
				{ name: 'Saldo', displayName: "Saldo", field: 'NominalAR', cellFilter: 'rupiahC' },
				{ name: 'SisahAR', displayName: "Sisah AR", field: 'SisaAR', cellFilter: 'rupiahC', visible: false }
			]
	};

	$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid = {
		enableSorting: true,
		enableFiltering: true,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		showColumnFooter: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid = gridApi;
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				console.log("test", rowEntity);
				console.log("colDef ", colDefs);
				// $scope.SisaAR = rowEntity.SisaAR;
				$scope.newValKui = newValue;
				// if(newValue > rowEntity.SisaAR){
				// 	var newValue = 0;   
				// 	rowEntity[colDefs.name] = newValue;
				// 	bsNotify.show(
				// 		{
				// 			title: "Peringatan",
				// 			content: "Nominal Pembayaran tidak boleh lebih.",
				// 			type: 'warning'
				// 		}
				// 	);  
				// }else if(newValue <= 0){ 
				// 	var newValue = 0;   
				// 	rowEntity[colDefs.name] = newValue;
				// 	bsNotify.show(
				// 		{
				// 			title: "Peringatan",
				// 			content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
				// 			type: 'warning'
				// 		}
				// 	);      
				// }

				// $scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function(row){
				// 	var JmlKredit = 0;
				// 	var JmlDebet = 0;
				// 	if ( row.Nominal != undefined ){
				// 		$scope.TotalPembayaranAll = row.Nominal ;
				// 	}
				// 	console.log("TotalPembayaranAll",$scope.TotalPembayaranAll);

				// 	for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
				// 		console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						
				// 		if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				// 		   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				// 			JmlKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				// 		}
				// 		else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
				// 			JmlDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				// 		}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
				// 			// TotPerhitungan;
				// 		}
				// 	}
				// 		var tmpSelisih = parseInt($scope.TotalPembayaranAll) - parseInt(row.SisaAR) - parseInt(JmlKredit) + parseInt(JmlDebet);
				// 		console.log("tmpSelisih",tmpSelisih);
				// 		if(tmpSelisih < 0){
				// 			tmpSelisih = 0;
				// 		}
				// 		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
				// });

				if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}

			});
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
				}
				else {
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'ProformaInvoiceId', field: 'ProformaInvoiceId', visible: false },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Nomor SPK', displayName: "Nomor SPK", field: 'NoSPK' },
				{ name: 'Nomor SO', displayName: "Nomor SO", field: 'NoSO' },
				{ name: 'SOId', field: 'SoId', visible: false },
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				{ name: 'Saldo', displayName: 'Sisah AR', field: 'SisaAR', visible: false },
				{
					name: 'Nominal', field: 'Nominal',
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}, enableCellEdit: true, aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'rupiahC'
				},
				{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteNoRangkaRow(row)">Hapus</button>' }
			]
	};


	$scope.TambahIncomingPayment_DaftarNoRangka_AddItem = function () {
		console.log("TambahIncomingPayment_DaftarNoRangka_AddItem");
		$scope.gridApi_TambahIncomingPayment_DaftarNoRangka_UIGrid.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			// $scope.SisaAR += row.SisaAR;
			$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.ProformaInvoiceId);
				console.log('isi grid2 -->', row.ProformaInvoiceId);
				if (row.NoRangka == obj.NoRangka)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.push({ ProformaInvoiceId: row.ProformaInvoiceId, NoRangka: row.NoRangka, NoSPK: row.NoSPK, NoSO: row.NoSO, SoId: row.SoId, Nominal: row.NominalPI, CustomerId: row.CustomerId, SisaAR: row.SisaAR });
			}
			// if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
			// 	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
			// }
		}
		)
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
		}
	};

	$scope.deleteNoRangkaRow = function (row) {
		var index = $scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.splice(index, 1);
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
		}
	};


	//Refund Leasing
	$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableFiltering: true,
		useCustomPagination: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasing_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
					$scope.currentRefundLeasingId = row.entity.RefundLeasingId;
					$scope.currentNoRangka = row.entity.NoRangka;
					$scope.currentNoSPK = row.entity.NoSPK;
					$scope.currentNoSO = row.entity.NoSO;
				}
				else {
					$scope.currentRefundLeasingId = "";
					$scope.currentNoRangka = "";
					$scope.currentNoSPK = "";
					$scope.currentNoSO = "";
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'RefundLeasingId', field: 'RefundLeasingId', visible: false },
				{ name: 'Vendor Name', field: 'VendorName' },
				{ name: 'No Rangka', field: 'NoRangka' },
				{ name: 'No SPK', displayName: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'No SO', displayName: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nominal Refund', field: 'NominalRefund', cellFilter: 'rupiahC' }
			]
	};

	$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableFiltering: true,
		useCustomPagination: true,
		showColumnFooter: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

				if (row.isSelected == true) {
				}
				else {
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'RefundLeasingId', field: 'RefundLeasingId', visible: false },
				{ name: 'No Rangka', field: 'NoRangka' },
				{ name: 'No SPK', displayName: 'Nomor SPK', field: 'NoSPK' },
				{ name: 'No SO', displayName: 'Nomor SO', field: 'NoSO' },
				{ name: 'Nominal Refund', field: 'NominalRefund', aggregationType: uiGridConstants.aggregationTypes.sum, 
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					},
					type: 'number', cellFilter: 'rupiahC' },
				{ name: 'Delete', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteNoRefundRow(row)">Hapus</button>' }
			]
	};

	$scope.TambahIncomingPayment_DaftarARRefundLeasing_AddItem = function () {
		console.log("TambahIncomingPayment_DaftarARRefundLeasing_AddItem");
		$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.RefundLeasingId);
				console.log('isi grid2 -->', row.RefundLeasingId);
				if (row.RefundLeasingId == obj.RefundLeasingId)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data.push({ RefundLeasingId: row.RefundLeasingId, NoRangka: row.NoRangka, NoSPK: row.NoSPK, NoSO: row.NoSO, NominalRefund: row.NominalRefund });
			}
		}
		)
	};

	$scope.deleteNoRefundRow = function (row) {
		var index = $scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data.indexOf(row.entity);
		$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data.splice(index, 1);
	};

	$scope.LihatDetailIncoming = function (row) {
		//var index = $scope.LihatIncomingPayment_DaftarIncoming_UIGrid.data.indexOf(row.entity);
		//console.log(row.entity.IPParentId);
		//console.log(row.IPParentId);
		console.log('selected',row);
		$scope.ShowIncomingPaymentDataDetail = true;
		$scope.ShowIncomingPaymentData = false;
		$scope.ShowIncomingPaymentHeader = false;
		$scope.ShowIncomingPaymentHeaderTambah = false;
		$scope.Show_Show_LihatIncomingPayment_RincianPembayaran = true;
		$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = true;
		$scope.Show_Show_LihatIncomingPayment_FootKuitansi = true;
		$scope.Show_LihatIncomingPayment_OPNumber = false;

		$scope.Show_LihatIncomingPayment_CetakKuitansi = true;
		$scope.Show_LihatIncomingPayment_SimpanCetakKuitansi = false;
		$scope.LihatIncomingPayment_NomorKuitansi_Pilih = "Lihat";

		$scope.currentIPParentId = row.entity.IPParentId;
		$scope.currentCustomerId = row.entity.CustomerId;
		$scope.LihatIncomingPayment_TanggalIncomingPayment = row.entity.TanggalIncoming;
		$scope.LihatIncomingPayment_Top_NomorIncomingPayment = row.entity.IncomingNo;
		$scope.LihatIncomingPayment_Top_NomorIncomingPaymentReversal = row.entity.IncomingNoRev;
		console.log(row.entity.IncomingPaymentType);
		$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = row.entity.IncomingPaymentType;
		$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged();

		if($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima"){
			$scope.Show_DetailIncomingPayment_InformasiPelangan = true;
			$scope.Show_DetailIncomingPayment_InformasiPelangan_Lain = true;
			$scope.Show_DetailIncomingPayment_InformasiPelangan_Lain_NamaPelanggan = row.entity.CustomerName;
			$scope.Show_DetailIncomingPayment_InformasiPelangan_Lain_UntukPembayaran = row.entity.InterbranchUntukPembayaran;
		}else{
			$scope.Show_DetailIncomingPayment_InformasiPelangan = false;
			$scope.Show_DetailIncomingPayment_InformasiPelangan_Lain = false;
			// $scope.Show_DetailIncomingPayment_InformasiPelangan_Lain_NamaPelanggan = row.entity.CustomerName;
			// $scope.Show_DetailIncomingPayment_InformasiPelangan_Lain_UntukPembayaran = row.entity.InterbranchUntukPembayaran;
		}

		console.log("test masuk: ");
		//IncomingPaymentFactory.getCustomersByCustomerId($scope.currentCustomerId)
		IncomingPaymentFactory.getCustomerDataByIPParentId($scope.currentIPParentId)
			.then
			(
			function (res) {
				//Prospect
				console.log("test res: ",res);
				console.log("getCustomerDataByIPParentId");
				$scope.LihatIncomingPayment_RincianPembayaran_TipeIncomingPayment_Selected_Changed();
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
				//Branch
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
				//PelangganBranch
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].ProspectCode;
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].NamaBranch;
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
				//Toyota								
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].ProspectCode;
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabKota;
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].CustomerName;
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
				//Cetak Button
				$scope.EnabledCetak = res.data.Result[0].EnabledCetak;
				$scope.EnabledReverse = res.data.Result[0].EnabledReverse;
				console.log("Test enable cetak: ",$scope.EnabledCetak);
				console.log($scope.EnabledReverse);

				if ($scope.EnabledCetak == "1") {
					console.log("History Lost from selected row:", row.entity.HistoryLost);
					if (row.entity.HistoryLost == 1) {
						$scope.HistoryLost = 1
						// Enable edit Kuitansi Preprint.
						$scope.getDataPreprintedCB();

						$scope.LihatIncomingPayment_NomorKuitansi_EnabledDisabled = true;
						$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
						$scope.Show_LihatIncomingPayment_SimpanCetakKuitansi = true;

						$scope.LihatIncomingPayment_NomorKuitansi_Pilih = "Edit";

						$scope.LihatIncomingPayment_Cetak_EnabledDisabled = false;
						$scope.LihatIncomingPayment_SimpanCetak_EnabledDisabled = true;
					}
					else if (row.entity.HistoryLost == 0) {
						$scope.HistoryLost = 0
						$scope.LihatIncomingPayment_NomorKuitansi_EnabledDisabled = false;
						$scope.Show_LihatIncomingPayment_CetakKuitansi = true;
						//Backdate
						if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Diajukan"){
							$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
							$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
						}else if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Ditolak"){
							$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
							$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
						}
						$scope.showHideKuitansi();

						$scope.LihatIncomingPayment_Cetak_EnabledDisabled = true;
						$scope.LihatIncomingPayment_SimpanCetak_EnabledDisabled = false;
					}
					
				}
				else {
					$scope.LihatIncomingPayment_Cetak_EnabledDisabled = false;
					$scope.LihatIncomingPayment_SimpanCetak_EnabledDisabled = false;
				}
				if ($scope.EnabledReverse == "1") {
					$scope.LihatIncomingPayment_Reverse_EnabledDisabled = true;
				}
				else {
					$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
				}

				//Backdate
				if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Diajukan"){
					$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
					$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
				}else if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Ditolak"){
					$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
					$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
				}
				$scope.showHideKuitansi();

				//NOTO Get data interbranch
				$scope.Show_LihatIncomingPayment_Interbranch_InformasiBranch_NamaBranch = res.data.Result[0].NamaBranch;
				$scope.Show_LihatIncomingPayment_Interbranch_InformasiBranch_KodeBranch = res.data.Result[0].KodeBranch;
				$scope.Show_LihatIncomingPayment_Interbranch_InformasiBranch_Telepon = res.data.Result[0].Phone;
				$scope.Show_LihatIncomingPayment_Interbranch_InformasiBranch_Alamat = res.data.Result[0].Alamat;
				$scope.Show_LihatIncomingPayment_Interbranch_InformasiBranch_KabupatenKota = res.data.Result[0].KabKota;
				//NOTO
			},
			function (err) {
				console.log("err=>", err);
				//Prospect
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_ProspectId = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Alamat = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Handphone = "";
				//Branch
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaBranch = "";
				$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_KabupatenKota = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaPt = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Branch_Telepon = "";
				//Toyota								
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_ToyotaId = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Alamat = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = "";
				$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Handphone = "";
			}
			);

		IncomingPaymentFactory.GetListRincianPembayaran(1, uiGridPageSize, $scope.currentIPParentId)
			.then
			(
			function (res) {
				// console.log("1");
				$scope.LihatIncomingPayment_RincianPembayaran_UIGrid.data = res.data.Result;
				if(row.entity.HistoryLost != 1){
					$scope.LihatIncomingPayment_NomorKuitansi = res.data.Result[0].ReceiptNo;
				}else{
					$scope.LihatIncomingPayment_NomorKuitansi = undefined;
				}
				
				$scope.LihatIncomingPayment_KetKuitansi = res.data.Result[0].ReceiptDesc;
				console.log("2");

				if (res.data.Result[0].OPId > 0) {
					$scope.Show_LihatIncomingPayment_OPNumber = true;
					$scope.LihatIncomingPayment_OPNumber = res.data.Result[0].OPNumber;
				}
			},
			function (err) {
				console.log("err=>", err);
			}
			);

		IncomingPaymentFactory.GetListRincianBiaya(1, uiGridPageSize, $scope.currentIPParentId)
			.then
			(
			function (res) {
				console.log("3");
				$scope.LihatIncomingPayment_Biaya_UIGrid.data = res.data.Result;
				console.log("4");
			},
			function (err) {
				console.log("err=>", err);
			}
			);

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSPK(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Booking Fee");
					console.log(res.data.Result[0].TanggalSPK);
					$scope.LihatIncomingPayment_InformasiPembayaran_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSpk = res.data.Result[0].NoSPK;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Down Payment");

					$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.columnDefs =
						[
							{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
							{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
							{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
							{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC', visible: false },
							{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC' },
							{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC' },
							{ name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'rupiahC' }
						];

					$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.data = res.data.Result;
					console.log(res.data.Result);
					$scope.LihatIncomingPayment_Top_NomorSpk = res.data.Result[0].NoSPK;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Full Payment");

					$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.columnDefs =
						[
							{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
							{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
							{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'rupiahC' },
							{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'rupiahC', visible: false },
							{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'rupiahC', visible: false },
							{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'rupiahC' },
							{ name: 'Saldo', field: 'SisaNominal', cellFilter: 'rupiahC' },
							{ name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'rupiahC' }
						];

					$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.data = res.data.Result;
					console.log(res.data.Result);
					$scope.LihatIncomingPayment_Top_NomorSpk = res.data.Result[0].NoSPK;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping")
		//or ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi")
		{
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Swapping");
					$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Order Pengurusan Dokumen");
					$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		};

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("Unit - Purna Jual");
					$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		};

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					console.log($scope.currentIPParentId);
					$scope.LihatIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_InformasiPembayaran_WoBill_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorWo = res.data.Result[0].WoCode;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					console.log($scope.currentIPParentId);
					console.log("6",res.data.Result[0].NoAppointment)
					$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data = [];

					if (res.data.Result.length > 1) {
						$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Down Payment", NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP, PaidNominal: res.data.Result[0].PaidNominal });
						$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk", NominalDP: res.data.Result[1].NominalDP, PaidDP: res.data.Result[1].PaidDP, SisaDP: res.data.Result[1].SisaDP, PaidNominal: res.data.Result[1].PaidNominal });
					}
					else {
						$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Down Payment", NominalDP: res.data.Result[0].NominalDP, PaidDP: res.data.Result[0].PaidDP, SisaDP: res.data.Result[0].SisaDP, PaidNominal: res.data.Result[0].PaidNominal });
						$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk", NominalDP: 0, PaidDP: 0, SisaDP: 0 });
					}
					// $scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data.push({ Keterangan: "Own Risk" });


					// $scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_InformasiPembayaran_Appointment_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_InformasiPembayaran_WoDown_UIGrid.data = res.data.Result;

					if (res.data.Result[0].WoCode == 0) {
						console.log("IF",res.data.Result[0].WoCode)
						console.log("IF",$scope.LihatIncomingPayment_Top_NomorAppointment)
						$scope.LihatIncomingPayment_Top_NomorAppointment = res.data.Result[0].NoAppointment;
						Disable_LihatIncomingPayment_Top_NomorWo();
						Enable_LihatIncomingPayment_Top_NomorAppointment();
					}
					else {
						console.log("ELSE",res.data.Result[0].WoCode)
						console.log("IF",$scope.LihatIncomingPayment_Top_NomorAppointment)
						$scope.LihatIncomingPayment_Top_NomorWo = res.data.Result[0].WoCode;
						Enable_LihatIncomingPayment_Top_NomorWo();
						Disable_LihatIncomingPayment_Top_NomorAppointment();
					}
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
					$scope.Show_LihatIncomingPayment_DaftarSalesOrderDibayar = false;
					// $scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					Disable_LihatIncomingPayment_DaftarSalesOrderDibayar();
				},
				function (err) {
					console.log("err=>", err);
				}
				);

		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			IncomingPaymentFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					Disable_LihatIncomingPayment_InformasiPembayaran_Materai();
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			IncomingPaymentFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					$scope.getDataVendor();
					console.log("5");
					$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
					$scope.NamaVendor = res.data.Result[0].VendorName
					console.log(res.data.Result[0].VendorName);
					console.log($scope.NamaVendor);
					$scope.LihatIncomingPayment_Top_NamaLeasing = $scope.NamaVendor;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
			$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			IncomingPaymentFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					$scope.getDataVendor();
					console.log("5");
					$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
					$scope.NamaVendor = res.data.Result[0].VendorName
					console.log(res.data.Result[0].VendorName);
					console.log($scope.NamaVendor);
					Disable_LihatIncomingPayment_Top_NamaLeasing();
					Enable_LihatIncomingPayment_Top_NoProforma();
					$scope.LihatIncomingPayment_Top_NoProforma = res.data.Result[0].NoKuitansi
					//$scope.LihatIncomingPayment_Top_NamaLeasing = $scope.NamaVendor;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			$scope.LihatIncomingPayment_DaftarIncoming_UIGrid_Disabled = true;
			$scope.LihatIncomingPayment_getDataEDC();
			IncomingPaymentFactory.GetLihatIncomingPaymentAR(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("5");
					$scope.LihatIncomingPayment_DaftarAR_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_Biaya = res.data.Result[0].BiayaBank;
					$scope.LihatIncomingPayment_Top_NamaBank = res.data.Result[0].EDCId;
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		}

		//NOTO
		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Cash Delivery") {
			$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = false;
			$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
		}

		var sTipe = $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		var isPreprintedReceipt = false;
		angular.forEach($scope.PreprintedSettingData, function (value, key) {
			if (!isPreprintedReceipt) {

				/*
					if (value.BusinessType.indexOf('Unit') == 0
						&& value.isPreprintedReceipt
						&& sTipe.indexOf('Unit') == 0
					) {
						if (sTipe.indexOf('Proforma') >= 0) {
							HideKuitansi();
						}
						else {
							ShowKuitansi();
							isPreprintedReceipt = true;
							$scope.getDataPreprintedCB();
						}
					}
				
				*/

				if (value.BusinessType.indexOf('Unit') == 0
					&& value.isPreprintedReceipt
					&& sTipe.indexOf('Unit') == 0) {
					// ShowKuitansi_Lihat();
					// isPreprintedReceipt = true;

					if (sTipe.indexOf('Proforma') >= 0) {
						HideKuitansi_Lihat();
					}
					else {
						ShowKuitansi_Lihat();
						isPreprintedReceipt = true;
					}
				}
				else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
					ShowKuitansi_Lihat();
					isPreprintedReceipt = true;
				}
				else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
					ShowKuitansi_Lihat();
					isPreprintedReceipt = true;
				}
				else {
					HideKuitansi_Lihat();
				}
			}
		});

		if (sTipe.indexOf('Unit') == 0 && !sTipe.indexOf('Proforma') >= 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
			$scope.LihatIncomingPayment_KetKuitansi_ShowHide = true;
		}
		else {
			$scope.LihatIncomingPayment_KetKuitansi_ShowHide = false;
		}
		//NOTO

		//Backdate
		if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Diajukan"){
			$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
			$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
		}else if(row.entity.TipePengajuan == "Backdate" && row.entity.StatusPengajuan == "Ditolak"){
			$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
			$scope.LihatIncomingPayment_Reverse_EnabledDisabled = false;
		}
		$scope.showHideKuitansi();
	};
	//Alvin, 20170527, end
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Tambahan OI OE
	$scope.getOI = function() {
        IncomingPaymentFactory.getDataOI().then(
            function(res){
				$scope.valueOI = res.data[0].MaxAmount;
				console.log('$scope.valueOI',$scope.valueOI);
                // $scope.loading=false;
                // return res.data;
			}
			// ,
            // function(err){
            //     bsNotify.show(
			// 		{
			// 			title: "gagal",
			// 			content: "Data tidak ditemukan.",
			// 			type: 'danger'
			// 		}
			// 	);
            // }
        );
	}
	
	$scope.getOE = function() {
        IncomingPaymentFactory.getDataOE().then(
            function(res){
				$scope.valueOE = res.data[0].MaxAmount;
				console.log('$scope.valueOE',$scope.valueOE);
                // $scope.loading=false;
                // return res.data;
			}
			// ,
            // function(err){
            //     bsNotify.show(
			// 		{
			// 			title: "gagal",
			// 			content: "Data tidak ditemukan.",
			// 			type: 'danger'
			// 		}
			// 	);
            // }
        );
	}
	// $scope.controlNominal = false;
	$scope.controlAssignment = function(){
		if($scope.selectTipeBiaya == null || $scope.selectTipeBiaya == 'undefined' || typeof $scope.selectTipeBiaya == 'undefined'){
			$scope.controlNominal = true;
		}else{
			$scope.controlNominal = false;
		}

	}
	$scope.cekNominal = function (dataNominal) {
		// $scope.controlNominal = false;
		
		if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' && dataNominal > $scope.valueOI){
			$scope.controlNominal = true;
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Nominal melebihi maksimal master OI sebesar Rp " +$scope.valueOI,
							type: 'warning'
						}
					);
		}else if($scope.selectTipeBiaya == 'Biaya Lain-lain' && dataNominal > $scope.valueOE) {
			$scope.controlNominal = true;
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OE sebesar Rp " +$scope.valueOE,
					type: 'warning'
				}
			);

		}else{
			$scope.controlNominal = false;
		}
	}

	
	//End Tambahan OI OE
	
	$scope.refresh = true;
	$scope.ToTambahIncomingPayment = function () {
		//20170517, nuse, begin
		$scope.refresh = true;
		$scope.Mandatory = false;
		$timeout(function() {
			$scope.refresh = false;
		}, 1000);
		$scope.Show_ShowIncomingPaymentMain = false;
		$scope.Show_ShowLihatIncomingPayment = false;
		$scope.Show_ShowTambahIncomingPayment = true;
		$scope.TambahIncomingPayment_TanggalIncomingPayment = new Date();
		$scope.TambahIncomingPayment_BiayaforParseFloat = 0;
		//20170517, nuse, end
		//DisableAllMetodePembayaranDetails();
		//DisableAll_TambahIncomingPayment_InformasiPelanggan();


		$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" },
		{ name: "Cash Collection", value: "Cash" },
		{ name: "Cash Operation", value: "Cash Operation" },
		{ name: "Credit/Debit Card", value: "Credit/Debit Card" }];

		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = "";
		$scope.TambahIncoming_OtherTypeNamaPembayaran = "";

		$scope.optionsDebitCreditCard = [{ name: "Credit Card", value: "1" }, { name: "Debit Card", value: "2" }];
		$scope.TambahIncomingPayment_RincianPembayaran_TipeKartu_CreditDebitCard = {};

		//20170509, nuse, begin
		//$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Proforma Invoice)", value: "Unit - Payment (Proforma Invoice)" }, { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" },  { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }];
		//$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Proforma Invoice)", value: "Unit - Payment (Proforma Invoice)" }, { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" },  { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, {name:"Cash Delivery", value: "Cash Delivery"}];
		//$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Proforma Invoice)", value: "Unit - Payment (Proforma Invoice)" }, { name: "Unit - Swapping", value: "Unit - Swapping" },  { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" },  { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, {name:"Cash Delivery", value: "Cash Delivery"}];
		//20170509, nuse, end
		$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment = "";

		//begin | Noto | 180514 | 00186 UT
		ShowTopMetodePembayaranDetails();
		Enable_TambahIncomingPayment_BiayaBiaya();
		ShowKuitansi();
		//end | Noto | 180514 | 00186 UT

		//Tambahan OI OE
		$scope.getOE();
		$scope.getOI();
		$scope.ShowPengajuan = false;
	};

	$scope.ToLihatIncomingPayment = function () {
		$scope.ShowIncomingPaymentData = false;

		$scope.LihatDaftarIncomingPayment_SearchCategoryOptions = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" }, { name: "Tanggal Incoming Payment", value: "Tanggal Incoming Payment" }, { name: "Nominal", value: "Nominal" }];
		$scope.LihatDaftarIncomingPayment_SearchCategory = $scope.LihatDaftarIncomingPayment_SearchCategoryOptions[0];

		$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Kuitansi Pembayaran)", value: "Unit - Payment (Kuitansi Penagihan)" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing / Insurance", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }];
		//$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions[0];
		$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = {};
		DisableAll_LihatIncomingPayment_InformasiPelanggan();
		DisableAll_LihatIncomingPayment_InformasiPembayaran();
		Disable_LihatIncomingPayment_Interbranch_InformasiBranch();
		Disable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		Disable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		Disable_LihatIncomingPayment_DaftarArCreditDebit();
		Disable_LihatIncomingPayment_RincianPembayaran();
		Disable_LihatIncomingPayment_FootKuitansi();
		$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar = "";
		Disable_LihatIncomingPayment_DaftarSalesOrder();
		Disable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		Disable_LihatIncomingPayment_Top_NomorSpk();
		Disable_LihatIncomingPayment_Top_NamaLeasing();
		Disable_LihatIncomingPayment_Top_NoProforma();
		Disable_LihatIncomingPayment_Top_NomorSo();
		Disable_LihatIncomingPayment_Top_NomorWo();
		Disable_LihatIncomingPayment_Top_NamaAsuransi();
		Disable_LihatIncomingPayment_Top_NamaBank();

	};

	$scope.CariIncomingPaymentData = function () {
		if (($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment != null) &&
			($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo != null)) {
			$scope.ShowIncomingPaymentData = true;
			$scope.GetListIncoming(1, uiGridPageSize);
		}
		else {
			alert("Tgl Awal/Akhir Blm Diisi");
		}
	};

	//Alvin, 20170527, begin
	$scope.GetListProformaInvoiceNew = function (Start, Limit) {
		IncomingPaymentFactory.GetListProformaInvoiceNew(Start, Limit, $filter('date')(new Date($scope.TambahIncomingPayment_TanggalIncomingPayment), 'yyyy-MM-dd'), $scope.TambahIncomingPayment_SearchBy_NomorProforma)
			.then(
				function (res) {
					$scope.currentProformaInvoiceId = res.data.Result[0].ProformaInvoiceId;
					$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.totalItems = res.data.Total;
					$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.GetListRefundLeasing = function (Start, Limit) {
		$scope.RefundType = $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == 2 ? 'Leasing|' + $scope.TambahIncomingPayment_Top_NamaLeasing : 'Insurance|' + $scope.TambahIncomingPayment_Top_NamaLeasing;
		IncomingPaymentFactory.GetListRefundLeasing(Start, Limit, $filter('date')(new Date($scope.TambahIncomingPayment_TanggalIncomingPayment), 'yyyy-MM-dd'), $scope.RefundType)
			.then(
				function (res) {
					$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.totalItems = res.data.Total;
					$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};
	//Alvin, 20170527, end


	$scope.GetListIncomingADH = function (Start, Limit) {

		IncomingPaymentFactory.GetListIncomingNew(Start, Limit, $filter('date')(new Date('2019-01-01'), 'yyyy-MM-dd'),
			$filter('date')(new Date(), 'yyyy-MM-dd'),
			"diajukan", "Status Pengajuan")
			.then(
				function (res) {

					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.totalItems = res.data.Total;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.paginationPageSize = Limit;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.paginationPageSizes = [10, 25, 50];

					// if (res.data.Result.length == 0) {
					// 	$scope.GetListIncoming(1, uiGridPageSize);
					// }
					// else {
						var StartD = new Date();
						var EndD = "";

						$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text = "diajukan";
						$scope.LihatIncomingPayment_DaftarIncomingPayment_Search = "Status Pengajuan";

						if (!angular.isUndefined(res.data.Result[0].StartDate)) {
							StartD = new Date(res.data.Result[0].StartDate);
						}

						// if (!angular.isUndefined(res.data.Result[0].EndDate)) {
						// 	EndD = new Date(res.data.Result[0].EndDate);
						// }
						$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = StartD;
						$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo = new Date();
						$scope.loading = false;
					// }
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};
	//Alvin, 20170511, begin Lihat

	$scope.GetListIncoming = function (Start, Limit) {

		IncomingPaymentFactory.GetListIncomingNew(Start, Limit, $filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'),
			$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text, $scope.LihatIncomingPayment_DaftarIncomingPayment_Search)
			.then(
				function (res) {
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.totalItems = res.data.Total;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.paginationPageSize = Limit;
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatIncomingPayment_DaftarIncoming_UIGrid.paginationCurrentPage = Start;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.Batal_LihatIncomingPayment_PengajuanReversalModal = function (){
		angular.element('#ModalLihatIncomingPayment_PengajuanReversal ').modal('hide');
	}
	$scope.Pengajuan_LihatIncomingPayment_PengajuanReversalModal = function () {
		//if ($scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal == "") {
		if (angular.isUndefined($scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal) || $scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal == "") {
			bsNotify.show({
				title: "Warning",
				content: "Alasan Reversal harus diisi",
				type: 'warning'
			});
			return false;
		}
		var data;
		data =
			[{
				"ApprovalTypeName": "IncomingPayment Reversal",
				"DocumentId": $scope.currentIPParentId,
				"ReversalReason": $scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal
			}]

		IncomingPaymentFactory.submitDataApprovalIncomingPayments(data)
			.then
			(
			function (res) {
				console.log(res);
				$scope.LihatIncomingPayment_Batal();
				FormStateValue = 2;
				$scope.GetListIncoming(1, uiGridPageSize);
				bsNotify.show({
					title: "Berhasil",
					content: "Reversal berhasil diajukan",
					type: 'success'
				});
			angular.element('#ModalLihatIncomingPayment_PengajuanReversal ').modal('hide');
			},
			function (err) {
				console.log("err=>", err);
				if (err != null) {
					var errMsg = '';
					if(err.data.Message.includes("#")){
						if (err.data.Message.split('#').length > 1) {
							errMsg = err.data.Message.split('#')[1];
						}
					}else{
						errMsg = err.data.Message;
					}
					// if (err.data.Message.split('#').length > 1) {
					// 	errMsg = err.data.Message.split('#')[1];
					// }

					bsNotify.show({
						title: "Warning",
						content: errMsg,
						type: 'warning'
					});
					angular.element('#ModalLihatIncomingPayment_PengajuanReversal ').modal('hide');
					return false;
				}
			}
			);

	};
	//Alvin, 20170511, end Lihat

	//Alvin, 20170511, begin UnknownCards	//.......................
	$scope.GetListUnknownCards = function (Start, Limit) {
		var newString  =  "";
		if($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText != '' && $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText != undefined){
			newString = $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText.toString().replace(/\//g, '-').toString().replace(/\./g, '-')
		}else{
			newString = $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText;
		}
		console.log("replace =>>", newString);
		
		var tmpFlag = 0;
		if( $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown == 'Tanggal AR Card'){
			tmpFlag=1;
		}else{
			tmpFlag=0;
		}

		IncomingPaymentFactory.GetListUnknownCards(Start, Limit, $scope.TambahIncomingPayment_SearchBy_NamaBank, newString, $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown, tmpFlag )
			.then(
				function (res) {
					$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid.data = res.data.Result;
					for(var i in res.data.Result){
						res.data.Result[i].PaymentDate = $scope.changeFormatDate(res.data.Result[i].PaymentDate);
					}
					$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid.totalItems = res.data.Total;
					$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid.paginationPageSize = Limit;
					$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid.paginationPageSizes = [Limit];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.changeFormatDate = function(item) {
		var tmpAppointmentDate = item;
		console.log("changeFormatDate item", item);
		tmpAppointmentDate = new Date(tmpAppointmentDate);
		var finalDate
		if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
			var yyyy = tmpAppointmentDate.getFullYear().toString();
			var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = tmpAppointmentDate.getDate().toString();
			finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
		} else {
			finalDate = '';
		}
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	};
	//Alvin, 20170511, end UnknownCards

	//20170425, eric, begin
	$scope.deleteSalesOrderDibayar = function (row) {

		var param = new Array(1);
		param[0] = row.SOId;

		IncomingPaymentFactory.deleteSalesOrdersBySOId(param)
			.then(
				function (res) {
					console.log("delete result=>", res);
					$scope.getSalesOrdersDibayarByCustomerName1(1, uiGridPageSize);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};
	//20170425, eric, end	

	$scope.TambahIncomingPayment_SimpanCetak = function () {
		console.log("TambahIncomingPayment_SimpanCetak");
		$scope.Cetak = "Y";
		$scope.TambahIncomingPayment_Simpan();
		//$scope.CetakIPParentId = 0;
	};

	$scope.TambahIncomingPayment_SimpanCetakTTUS = function () {
		console.log("TambahIncomingPayment_SimpanCetakTTUS");
		$scope.CetakTTUS = "Y";
		$scope.TambahIncomingPayment_Simpan();
		//$scope.CetakIPParentId = 0;
		//$scope.CetakSPKNo = "0";
		//$scope.CetakKuitansi();
	};

	$scope.TambahIncomingPayment_CheckBiaya = function () {
		var passed = false;

		if ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Titipan" && ($scope.TambahIncomingPayment_BiayaBiaya_Assignment == '' || $scope.TambahIncomingPayment_BiayaBiaya_Assignment == undefined)) {
			console.log("1b");
			// alert("Nominal Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Titipan Assignment Harus diisi",
				type: 'warning'
			});
			return (passed);
		}

		if (angular.isUndefined($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya) || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "") {
			console.log("1b");
			// alert("Tipe Biaya Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Field Tipe biaya lain-lain harus dipilih",
				type: 'warning'
			});
			return (passed);
		}
		if (angular.isUndefined($scope.TambahIncomingPayment_BiayaBiaya_Nominal) || $scope.TambahIncomingPayment_BiayaBiaya_Nominal == "" || $scope.TambahIncomingPayment_BiayaBiaya_Nominal == "0") {
			console.log("1b");
			// alert("Nominal Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Field Nominal harus diisi",
				type: 'warning'
			});
			return (passed);
		}

		if (angular.isUndefined($scope.TambahIncomingPayment_BiayaBiaya_DebetKredit) || $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit == "" ) {
			console.log("1b");
			// alert("Nominal Hrs Diisi !");
			bsNotify.show({
				title: "Warning",
				content: "Field Debit/Kredit harus diisi",
				type: 'warning'
			});
			return (passed);
		}

		passed = true;

		return (passed);
	}


	$scope.TambahIncomingPayment_CheckFields = function () {
		var passed = false;

		console.log("Check Fields");

		if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment")) {
			// console.log("1");
			console.log($scope.TambahIncomingPayment_KeteranganKuitansi);
			// if (angular.isUndefined($scope.TambahIncomingPayment_KeteranganKuitansi) || $scope.TambahIncomingPayment_KeteranganKuitansi == "") {
			// 	console.log("1b");
			// 	alert("Ket Kuitansi Hrs Diisi !");
			// 	return (passed);
			// }

			// if ($scope.Preprinted == true) {
			// 	console.log("2");
			// 	console.log($scope.TambahIncomingPayment_NomorKuitansi);
			// 	if (angular.isUndefined($scope.TambahIncomingPayment_NomorKuitansi) || $scope.TambahIncomingPayment_NomorKuitansi == "") {
			// 		console.log("2b");
			// 		alert("No Kuitansi Hrs Diisi !");
			// 		return (passed);
			// 	}
			// }
		}



		console.log("3");

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {

		}

		passed = true;

		return (passed);
	}

	$scope.LihatIncomingPayment_Batal = function () {
		$scope.ShowIncomingPaymentDataDetail = false;
		$scope.ShowIncomingPaymentData = true;
		$scope.ShowIncomingPaymentHeader = true;
		$scope.ShowIncomingPaymentHeaderTambah = true;

		DisableAll_LihatIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatIncomingPayment_InformasiPembayaran();//ini semua info pembayaran di hide

		$scope.Show_Show_LihatIncomingPayment_RincianPembayaran = false;
		$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = false;
		$scope.Show_Show_LihatIncomingPayment_FootKuitansi = false;
		
		$scope.LihatIncomingPayment_NomorKuitansi_EnabledDisabled = false;
		$scope.Show_LihatIncomingPayment_SimpanCetakKuitansi = false;

		$scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal = "";
		$scope.ModalTolakIncomingPayment_AlasanPenolakan = "";

	}


	$scope.TambahIncomingPayment_Batal = function () {
		console.log("Clean Up");

		//begin | Noto | 180514 | 00186 UT
		//HideTopMetodePembayaranDetails();//ini paymentdetail top nya (dropdown)
		//Disable_TambahIncomingPayment_BiayaBiaya();
		//end | Noto | 180514 | 00186 UT

		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CashOperation();
		HideMetodePembayaranDetails_CreditDebitCard();
		DisableAll_TambahIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_TambahIncomingPayment_InformasiPembayaran();//ini semua info pembayaran di hide
		DisableAll_TambahIncomingPayment_DaftarSpk();//ini semua ilangin daftar spk
		DisableAll_TambahIncomingPayment_SalesOrder();//ini ilangin semua sales order
		DisableAll_TambahIncomingPayment_DaftarSalesOrderRadio();
		Disable_TambahIncomingPayment_KumpulanNomorRangka();
		Disable_TambahIncomingPayment_KumpulanWorkOrder();
		Disable_TambahIncomingPayment_DaftarArCreditDebit();
		Disable_TambahIncomingPayment_KumpulanARRefundLeasing();
		Disable_TambahIncomingPayment_SaveAndPrintButton();
		$scope.TambahIncomingPayment_DaftarSpk_Detail_Radio = "";
		$scope.TambahIncomingPayment_DaftarSpk_Brief_Radio = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill = "";
		$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = []
		$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.columnDefs = [];
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [];
		$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs = [];
		$scope.Show_ShowLihatIncomingPayment = true;
		$scope.Show_ShowTambahIncomingPayment = false;
		$scope.TambahIncomingPayment_ClearAllData();
		$scope.Cetak = "N";
		$scope.CetakTTUS = "N";

	}

	$scope.TambahIncomingPayment_Simpan_Test = function () {
		// 	// BEGIN | NOTO | FORM STATE
		// 	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = new Date();
		// 	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo = new Date();
		// 	$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text = "";
		// 	$scope.LihatIncomingPayment_DaftarIncomingPayment_Search = "";

		// 	$scope.TambahIncomingPayment_Batal();
		// 	FormStateValue = 1;

		// 	$scope.getDataPreprintedCB();

		if ($scope.currentOPId == '' || $scope.currentOPId == 0 || angular.isUndefined($scope.currentOPId)) {
			bsNotify.show({
				title: "Warning",
				content: "Outgoing Payment belum dipilih",
				type: 'warning'
			});
		}

		$scope.TambahIncomingPayment_BookingFeeData();
		$scope.SOSubmitData = $scope.TambahIncomingPayment_BookingFeeSubmitData;
		$scope.TambahIncomingPayment_Biaya2();
		$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;

		if ($scope.currentTglAR == "") {
			$scope.currentTglAR = $scope.TambahIncomingPayment_TanggalIncomingPayment;
		}

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
			//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [3] | Create Data Object
			var data = [{
				OutletId: $scope.currentOutletId,
				SPKId: $scope.currentSPKId,
				TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
				IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
				CustomerId: $scope.currentCustomerId,
				Currency: "IDR",
				PaymentTotal: $scope.currentOPNominal,
				ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
				PrintCounter: 0,
				OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
				UserId: 1,

				IncomingPaymentDetailXML: "",
				IncomingPaymentChargesXML: "",

				PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
				Difference: 0,
				PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation,
				PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CashOperation,
				ReceiverAccNumber: "",
				SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
				SenderName: "",
				ChequeId: 0,
				CardProvider: $scope.TambahIncomingPayment_CreditDebitCard_NamaBank,
				CardNo: $scope.TambahIncomingPayment_RincianPembayaran_NomorKartu_CreditDebitCard,
				CardType: $scope.CardType,
				InterBranchId: 0,
				InterBranchDesc: "",
				GridARDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation,
				GridCardType: "",
				GridCardNominal: 0,
				BankCharges: 0,
				BankStatementUnknownId: 0,
				InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
				InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
				//20170430, NUSE EDIT, begin
				SOList: $scope.SOSubmitData,
				BiayaList: $scope.SOSubmitBiaya,
				ProformaInvoiceId: $scope.currentProformaInvoiceId,
				OPId: $scope.currentOPId
			}]
			console.log(data);
		}

	}

	$scope.ProsesSimpan = false;
	//BEGIN | NOTO | 190228 | VALIDASI NOMINAL
	$scope.CashCollection_and_CreditDebit_Validation = function (NominalIncoming, IsBiayaKredit, IsEqual) {
		
		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash")
			// $scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, "");
			return true;
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation")
			return true;
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card")
			$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, "");
			// return true;
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" || $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation")
			return true;

		//Re-calculate Biaya Lain-lain
		if (IsBiayaKredit) {
			var totalBiaya = 0;
			angular.forEach($scope.TambahIncomingPayment_Biaya_UIGrid.data, function (value, key) {
				if (value.DebitCredit == 'Kredit') {
					totalBiaya = totalBiaya + parseInt(value.Nominal);
				}
			});
			$scope.Pembayaran = parseInt($scope.Pembayaran) - totalBiaya;
		}

		var ret = false;

		if (IsEqual) {
			if (NominalIncoming == $scope.Pembayaran){
				ret = true;
			}else if($scope.Pembayaran <= NominalIncoming && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){ // khusus tipe pembayaran credit/debit card
				ret = true;
			}
		}
		else {
			if ($scope.Pembayaran <= NominalIncoming)
				ret = true;
		}

		return ret;
	}

	var submitGeneralOK = true;
	$scope.paidAmount;
	$scope.TambahIncomingPayment_Simpan = function () {
		
		$scope.ProsesSimpan = true;

		// START Notifikasi Validasi jika Tipe Incoming Payment "Down Payment" dan Tipe biaya lain-lain "Titipan" Jika Saldo != Pembayaran
		var tipe = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		var validateSimpan = false;
		if(tipe == 'Unit - Down Payment' || tipe == 'Service - Down Payment'){
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for(var x in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[x].BiayaId == 'Titipan'){
						if(tipe == 'Unit - Down Payment'){
							console.log('Masuk Unit - Down Payment')
							for(var y in $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data){
								if($scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data[y].TotalDPMustBePay != $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data[y].Pembayaran){
									validateSimpan = true;
								}
							}
						}else if(tipe == 'Service - Down Payment'){
							console.log('Masuk Service - Down Payment')
							for(var y in $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data){
								if($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data[y].SisaDP != $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data[y].Pembayaran){
									validateSimpan = true;
								}
							}
						}
					}
				}
			}
		}
		
		var NominalPembayaranTemp = 0;
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer1){
			console.log('1',parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer))
			NominalPembayaranTemp = parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer)
		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash1){
			console.log('2',parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash))
			NominalPembayaranTemp = parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash)
		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard1){
			console.log('3',parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard))
			NominalPembayaranTemp = parseFloat($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard)
		}else{
			console.log('4',parseFloat( $scope.currentOPNominal))
			NominalPembayaranTemp = $scope.currentOPNominal;
		}
		
		if(tipe == "Parts - Down Payment"){
			var foundTitipan = false;
			var NominalTemp = 0;
			console.log('Nominal =>',$scope.TambahIncomingPayment_BiayaBiaya_Nominal)
			for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if(!($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == 'Bea Materai' && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit'){
						NominalTemp += parseFloat($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal)
						console.log('Nominal 1 =>',NominalTemp)
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet'){
						NominalTemp -= parseFloat($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal)
						console.log('Nominal 2 =>',NominalTemp)
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == 'Titipan'){
						foundTitipan = true;
					}
				}
			}
			if(NominalPembayaranTemp < NominalTemp){
				bsNotify.show({
					title: "Warning",
					content: 'Nominal Pembayaran Kurang dari Nominal Biaya lain-lain!',
					type: 'warning'
				});
				$scope.ProsesSimpan = false;
				return false;
			}else{
				// if(NominalTemp < 0){
				// 	NominalPembayaranTemp -= NominalTemp
				// }else{
				// 	NominalPembayaranTemp += NominalTemp
				// }
				NominalPembayaranTemp -= NominalTemp
			}

			if(NominalPembayaranTemp < 0){
				bsNotify.show({
					title: "Warning",
					content: 'Nominal Pembayaran di rincian pembayaran Kurang !',
					type: 'warning'
				});
				$scope.ProsesSimpan = false;
				return false;
			}
			console.log('foundTitipan',foundTitipan)
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				console.log('Nominal 3 =>',NominalPembayaranTemp, '-',parseFloat($scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data[0].SisaDP))
				if(foundTitipan == true){
					if(NominalPembayaranTemp != parseFloat($scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data[0].SisaDP)){
						validateSimpan = true;
					}
				}
				if(NominalPembayaranTemp > parseFloat($scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data[0].SisaDP)){
					bsNotify.show({
						title: "Warning",
						content: 'Nominal Pembayaran di rincian pembayaran tidak boleh lebih besar dari saldo !',
						type: 'warning'
					});
					$scope.ProsesSimpan = false;
					return false;
				}
			}else{
				console.log('Nominal 3 =>',NominalPembayaranTemp, '-',$scope.currentSaldo)
				if(foundTitipan == true){
					if(NominalPembayaranTemp != $scope.currentSaldo){
						validateSimpan = true;
					}
				}
				if(NominalPembayaranTemp > $scope.currentSaldo){
					bsNotify.show({
						title: "Warning",
						content: 'Nominal Pembayaran di rincian pembayaran tidak boleh lebih besar dari saldo !',
						type: 'warning'
					});
					$scope.ProsesSimpan = false;
					return false;
				}
			}
			$scope.paidAmount = NominalPembayaranTemp.toString();
			console.log('paidamount',$scope.paidAmount);
			
		}
		

		if(validateSimpan == true){
			bsNotify.show({
				title: "Warning",
				content: 'Tidak dapat melakukan titipan, jika pembayaran Down Payment belum terpenuhi !',
				type: 'warning'
			});
			$scope.ProsesSimpan = false;
			return false;
		}
		// END Notifikasi Validasi jika Tipe Incoming Payment "Down Payment" dan Tipe biaya lain-lain "Titipan" Jika Saldo != Pembayaran

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			var tmpPendapatan = 0;
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var TotBooking = 0;

			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
				if(obj.NominalBookingFee != undefined){
					TotBooking += parseInt(obj.NominalBookingFee);
				}
			});

			for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
						tmpPendapatan = 1;
					}
				}
			}

			for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					$scope.tmpBiayaK =TotBiayaK;
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					TotBiayaD -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
				}
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					$scope.tmpBiayaK =TotBiayaK;
					console.log('1');
					// break;
				}
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
					// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					$scope.tmpBiayaK =TotBiayaK;
					console.log('2');
					// break;
				}
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					$scope.tmpBiayaD =TotBiayaD;
					console.log('3');
					// break;
				}
			}
			
			//Penjagaan jika ada OI/OE
			// if(tmpPendapatan == 1){
			// 	if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			// 		if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != TotBooking + TotBiayaK - TotBiayaD){
			// 			SimpanOK = 0;
			// 			bsNotify.show(
			// 				{
			// 					title: "Notifikasi",
			// 					content: "Nominal Pembayaran Tidak Sesuai!",
			// 					type: 'danger'
			// 				}
			// 			);
			// 			$scope.ProsesSimpan = false;
			// 			return false;
			// 		}
			// 	}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
			// 		if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != TotBooking + TotBiayaK - TotBiayaD ){
			// 			SimpanOK = 0;
			// 			bsNotify.show(
			// 				{
			// 					title: "Notifikasi",
			// 					content: "Nominal Pembayaran Tidak Sesuai!",
			// 					type: 'danger'
			// 				}
			// 			);
			// 			$scope.ProsesSimpan = false;
			// 			return false;
			// 		}
			// 	}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
			// 		if($scope.currentOPNominal != TotBooking + TotBiayaK - TotBiayaD ){
			// 			SimpanOK = 0;
			// 			bsNotify.show(
			// 				{
			// 					title: "Notifikasi",
			// 					content: "Nominal Pembayaran Tidak Sesuai!",
			// 					type: 'danger'
			// 				}
			// 			);
			// 			$scope.ProsesSimpan = false;
			// 			return false;
			// 		}
			// 	}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
			// 		if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != TotBooking + TotBiayaK - TotBiayaD ){
			// 			SimpanOK = 0;
			// 			bsNotify.show(
			// 				{
			// 					title: "Notifikasi",
			// 					content: "Nominal Pembayaran Tidak Sesuai!",
			// 					type: 'danger'
			// 				}
			// 			);
			// 			$scope.ProsesSimpan = false;
			// 			return false;
			// 		}
			// 	}
			// }else{
			// 	SimpanOK == 1;
			// }

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Booking Fee Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var HitungPem = 0;
			var BiayaD = 0;
			var BiayaK = 0;
			var cekkosong = 0;
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran))) {
					TotalPembayaran += obj.Pembayaran;

						if(obj.Pembayaran != undefined){
							HitungPem += obj.Pembayaran;
						}
					console.log("Pembayaran Di grid all", HitungPem);
					console.log("Nominal Pembayaran Unit DP BT", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
					console.log("Nominal Pembayaran Unit DP CC", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
					console.log("Nominal Pembayaran Unit DP CD", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
					
				}
				else {
					cekkosong++;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> wajib diisi!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			});

			console.log('cek data kosong', cekkosong)
			if(cekkosong > 0){
				$scope.ProsesSimpan = false;
				return false;
			}else{
				if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								
								// break;
							}
		
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
							
						}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}
						else{
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}
					}
				}
				
				// console.log('$scope.TambahIncomingPayment_Biaya_UIGrid',$scope.TambahIncomingPayment_Biaya_UIGrid.data[i]);
				console.log("TOT Unit DP Biaya Debit", BiayaD);
				console.log("TOT Unit DP Biaya Kredit", BiayaK);
				
				// Penjagaan Unit DP
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungPem - BiayaD + BiayaK){
						console.log('siniiii 1');
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					console.log('nominal ',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
					console.log('HitungPem ',HitungPem);
					console.log('Biaya K  ',BiayaK);
					console.log('Biaya D  ',BiayaD);
					// console.log("TOT Unit DP Biaya Kredit", BiayaKdddddddd);
					if( $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungPem + BiayaK - BiayaD){
						console.log('siniiii 2');
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != HitungPem + BiayaK - BiayaD){
						console.log('siniiii 3');
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					console.log('cd',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
					console.log('HitungPem',HitungPem);
					console.log('BiayaD',BiayaD);
					console.log('BiayaK',BiayaK);
					if( $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungPem - BiayaD + BiayaK){
						console.log('siniiii 4');
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}else{
						SimpanOK = 1;	
					}
				}

				// if (SimpanOK == 1) {
				// 	console.log('SimpanOK1',SimpanOK);
				// 	SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, false, true);
				// }
			}
			if (SimpanOK == 1) {
				console.log('SimpanOK2',SimpanOK);
				TambahIncomingPayment_Simpan_General();
			}
			else {
				console.log('SimpanOK3',SimpanOK);
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Unit - Booking Fee Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var HitungTot = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			console.log('TambahIncomingPayment_Biaya_UIGrid.data',$scope.TambahIncomingPayment_Biaya_UIGrid.data);
			if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							 console.log('1');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('2');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('3');
							// break;
						}
						
						
					}
					// if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		 console.log('1');
					// 		// break;
					// 	}
					// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		console.log('2');
					// 		// break;
					// 	}
					// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					// 		TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		console.log('3');
					// 		// break;
					// 	}
					// }
					else{
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							 console.log('1');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('2');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('3');
							// break;
						}	
					}
					
				}
			}
			
			
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran))) {
					if (obj.Pembayaran > obj.TotalPaymentMustBePay) {
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Lebih Besar Dari Saldo!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}

					// $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (row){
						if(obj.Pembayaran != undefined){
							HitungTot += obj.Pembayaran;
						}
						console.log("Data Di grid all", HitungTot);
					// });

					// Perhitungan Unit FP
					TotalHitungBT = HitungTot + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer ;
					TotalHitungCC = HitungTot;
					TotalHitungCO = HitungTot;
					TotalHitungCD = HitungTot;
					
					console.log("Total Hitung BT", TotalHitungBT);

					console.log("Nominal Pembayaran BT", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
					console.log("Nominal Pembayaran CC", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
					console.log("Nominal Pembayaran CD", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
					console.log("Selisih Kelebihan", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
					console.log("TOT Biaya Debit", TotBiayaD);
					console.log("TOT Biaya Kredit", TotBiayaK);

				}
				else {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor SO : <b>" + obj.SONumber + "</b> wajib diisi!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			});

			// Penjagaan Unit FP
			if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != TotalHitungBT - TotBiayaD + TotBiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
				console.log('ini casshhh',TotalHitungBT);
				console.log('ini TotalHitungCC',TotalHitungCC);
				console.log('ini TotBiayaK',TotBiayaK);
				console.log('ini TotBiayaD',TotBiayaD);
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != TotalHitungCC  - TotBiayaD + TotBiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
				if($scope.currentOPNominal != TotalHitungCO - TotBiayaD + TotBiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
				console.log('masuk sini kah???/');
				console.log('CT',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
				console.log('TotalHitungBT',TotalHitungBT);
				console.log('TotBiayaD',TotBiayaD);
				console.log('TotBiayaK',TotBiayaK);
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != TotalHitungBT - TotBiayaD + TotBiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}

			if (SimpanOK == 1) {

				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
					if ((obj.Pembayaran != 0) && (obj.Pembayaran != "") && (!angular.isUndefined(obj.Pembayaran))) {
						TotalPembayaran += obj.Pembayaran;
					}
				});

				// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, false, true);
			}

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Unit - Full Payment Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			
			for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('2');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('3');
						// break;
					}
				}
			}

			$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
				if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
					TotalPembayaran += obj.SisaPembayaran;
				}
				// Perhitungan Swapping
				HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
				HitungCC = TotalPembayaran;
				HitungCO = TotalPembayaran;
				HitungCD = TotalPembayaran;
			});
			// Penjagaan Unit Swapping	
			if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
				console.log('TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
				console.log('HitungCC',HitungCC);
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash + TotBiayaD - TotBiayaK > HitungCC ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
				if($scope.currentOPNominal > HitungCO){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > HitungCD ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}
			// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, false, true);

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Swapping Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var tmpPendapatan = 0;
			if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
				if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								 console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}else{
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								 console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}
					}

					for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						// if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
								tmpPendapatan = 1;
							}
						// }
						
					}
				}
			}
			
			

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				console.log("masuk 1>>>>>>>");
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += obj.SisaPembayaran;
					$scope.SaldoOPD = TotalPembayaran;
				});

				HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
				HitungCC = TotalPembayaran;
				HitungCO = TotalPembayaran;
				HitungCD = TotalPembayaran;
				
				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, false, false);
				// Penjagaan Unit OPD
				if(tmpPendapatan == 1){
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						console.log('cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
						console.log('CC',HitungCC);
						console.log('biayaK',TotBiayaK);
						console.log('biayaD',TotBiayaD);
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}else{
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						console.log('cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
						console.log('CC',HitungCC);
						console.log('biayaK',TotBiayaK);
						console.log('biayaD',TotBiayaD);
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > HitungCC + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal > HitungCO ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > HitungCD + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}
				
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				console.log("masuk 2>>>>>>>");
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					TotalPembayaran += obj.SisaPembayaran;
					$scope.SaldoOPD = TotalPembayaran;
				});
				
				HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
				HitungCC = TotalPembayaran;
				HitungCO = TotalPembayaran;
				HitungCD = TotalPembayaran;

				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, false, true);
				// Penjagaan Unit OPD
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}
				else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}

			}

			
			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Order Pengurusan Dokumen Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var tmpPendapatan = 0;
			
			if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet")){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
						}
					}
					else{
						if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
						}
					}
				}

				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
							tmpPendapatan = 1;
						}
					}
					
				}
			}
			

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
						TotalPembayaran += obj.SisaPembayaran;
						$scope.SaldoPJ = TotalPembayaran;
					}
					HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
					HitungCC = TotalPembayaran;
					HitungCO = TotalPembayaran;
					HitungCD = TotalPembayaran;
				});
				// Penjagaan Unit Purna Jual 
				if(tmpPendapatan == 1){
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
					else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}else{
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > HitungCC + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal > HitungCO + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > HitungCD + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
					// else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					// 	if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
					// 		SimpanOK = 0;
					// 		bsNotify.show(
					// 			{
					// 				title: "Gagal",
					// 				content: "Nominal Pembayaran Tidak Sesuai!",
					// 				type: 'danger'
					// 			}
					// 		);
					// 		$scope.ProsesSimpan = false;
					// 		return false;
					// 	}
					// }
				}
				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran + TotBiayaK - TotBiayaD, false, false);
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
						TotalPembayaran += obj.SisaPembayaran;
						$scope.SaldoPJ = TotalPembayaran;
					}
					
					HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
					HitungCC = TotalPembayaran;
					HitungCO = TotalPembayaran;
					HitungCD = TotalPembayaran;
				});
				// Penjagaan Unit Purna Jual 
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT - TotBiayaD + TotBiayaK ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC - TotBiayaD + TotBiayaK ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != HitungCO - TotBiayaD + TotBiayaK ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}

				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran- TotBiayaD + TotBiayaK , false, true);
			}


			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Purna Jual Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
		// 	var SimpanOK = 1;
		// 	var TotalPembayaran = 0;
		// 	var TotBiayaD = 0;
		// 	var TotBiayaK = 0;
			
		// 	if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
		// 		for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
		// 				if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
		// 					($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
		// 					($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
		// 						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 				}
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
		// 					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					 console.log('1');
		// 					// break;
		// 				}
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
		// 					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					console.log('2');
		// 					// break;
		// 				}
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
		// 					TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					console.log('3');
		// 					// break;
		// 				}
		// 			}else{
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
		// 					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					 console.log('1');
		// 					// break;
		// 				}
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
		// 					TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					console.log('2');
		// 					// break;
		// 				}
		// 				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
		// 					TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 					console.log('3');
		// 					// break;
		// 				}
		// 			}
		// 		// 	else{
		// 		// 		if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
		// 		// 			($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
		// 		// 				TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
		// 		// 		}
		// 		// 		else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
		// 		// 				TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
		// 		// 		}
		// 		// 	}
		// 		}
		// 	}
			

		// 	if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
		// 		$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
		// 			if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
		// 				TotalPembayaran += obj.SisaPembayaran;
		// 			}
		// 			HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
		// 			HitungCC = TotalPembayaran;
		// 			HitungCO = TotalPembayaran;
		// 			HitungCD = TotalPembayaran;
		// 		});
		// 		// Penjagaan Unit Purna Jual
		// 		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
		// 			console.log('BT  ',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
		// 			console.log('HitungBT',HitungBT);
		// 			console.log('TotBiayaK',TotBiayaK);
		// 			console.log('TotBiayaD',TotBiayaD);
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT - TotBiayaK + TotBiayaD){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else 
		// 		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
		// 			if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}
		// 		SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran + TotBiayaK - TotBiayaD, false, false);
		// 	}
		// 	else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
		// 		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
		// 			if ((obj.PaidNominal != 0) && (obj.PaidNominal != "") && (!angular.isUndefined(obj.PaidNominal))) {
		// 				TotalPembayaran += obj.PaidNominal;
		// 			}
					
		// 			HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
		// 			HitungCC = TotalPembayaran;
		// 			HitungCO = TotalPembayaran;
		// 			HitungCD = TotalPembayaran;
		// 		});
		// 		// Penjagaan Unit Purna Jual 
		// 		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT - TotBiayaD + TotBiayaK ){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC - TotBiayaD + TotBiayaK ){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
		// 			if($scope.currentOPNominal != HitungCO - TotBiayaD + TotBiayaK ){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
		// 			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD ){
		// 				SimpanOK = 0;
		// 				bsNotify.show(
		// 					{
		// 						title: "Gagal",
		// 						content: "Nominal Pembayaran Tidak Sesuai!",
		// 						type: 'danger'
		// 					}
		// 				);
		// 				$scope.ProsesSimpan = false;
		// 				return false;
		// 			}
		// 		}

		// 		SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran- TotBiayaD + TotBiayaK , false, true);
		// 	}


		// 	if (SimpanOK == 1) {
		// 		TambahIncomingPayment_Simpan_General();
		// 	}
		// 	else {
		// 		bsNotify.show(
		// 			{
		// 				title: "Gagal",
		// 				content: "Pembayaran Purna Jual Tidak Sesuai",
		// 				type: 'danger'
		// 			}
		// 		);
		// 		$scope.ProsesSimpan = false;
		// 		return false;
		// 	};
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var BiayaK = 0;
			var BiayaD = 0;
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.forEach(function (obj) {

				if ((obj.Keterangan == "Own Risk" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0)) && (obj.Keterangan == "Down Payment" && (angular.isUndefined(obj.Pembayaran) || obj.Pembayaran == 0))) {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran pada Daftar Informasi Pembayaran wajib diisi!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
				}else if(obj.Pembayaran > obj.SisaDP){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Boleh Lebih dari Saldo!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
				}
				// else {
					// var posisi = 0;
					// 	for (var i=0; i<$scope.gridApi_InformasiPembayaran.grid.columns.length; i++){
					// 		if ($scope.gridApi_InformasiPembayaran.grid.columns[i].field == 'Pembayaran'){
					// 			posisi = i;
					// 		}
					// 	}

					// if ((($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer > $scope.gridApi_InformasiPembayaran.grid.columns[posisi].getAggregationValue()) || (obj.Keterangan == "Down Payment" && (obj.Pembayaran > obj.SisaDP)) || (obj.Keterangan == "Own Risk" && (obj.Pembayaran > obj.SisaDP)))
					// 	|| (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > $scope.gridApi_InformasiPembayaran.grid.columns[posisi].getAggregationValue()) || (obj.Keterangan == "Down Payment" && (obj.Pembayaran > obj.SisaDP)) || (obj.Keterangan == "Own Risk" && (obj.Pembayaran > obj.SisaDP)) )
					// 	|| (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > $scope.gridApi_InformasiPembayaran.grid.columns[posisi].getAggregationValue()) || (obj.Keterangan == "Down Payment" && (obj.Pembayaran > obj.SisaDP)) || (obj.Keterangan == "Own Risk" && (obj.Pembayaran > obj.SisaDP))) ) {
					// 	console.log("masuk kesini >>>>>")
					// 	console.log("TOTAL PEMBAYARAN >>>>>>>>>", obj.Pembayaran);
					// 	// $scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue();
					// 	// console.log("getAggregat>>>>>",$scope.gridApi_InformasiPembayaran.grid.columnDefs["Pembayaran"].getAggregationValue() );
					// 	SimpanOK = 0;
					// 	bsNotify.show(
					// 		{
					// 			title: "Gagal",
					// 			content: "Field Pembayaran harus lebih kecil dari Total Pembayaran!",
					// 			type: 'danger'
					// 		}
					// 	);
					// 	$scope.ProsesSimpan = false;
						
					// }
					// else {
					// 	console.log("TOTAL PEMBAYARAN 2 >>>>>>>>>", obj.Pembayaran);
					// 	console.log("getAggregat>>>>>",$scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() );// untuk mengambil total pembayaran pada ui grid
					// 	TotalPembayaran += obj.Pembayaran;
						
					// }
					
				// }

			});

			//CR4 
			if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0) {
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('kredit');
						// break;
					}

					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							 
							// break;
						}
	
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							 console.log('1');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('2');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('3');
							// break;
						}
						
					}else {
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							 console.log('1');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('2');
							// break;
						}
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
							BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							console.log('3');
							// break;
						}
					}
				}
			}
			// new irr
			
			console.log("tipe Bank", $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran)
			console.log("Total Pembayaran", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer)
			console.log("TOT GRID", $scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue());
			console.log("Biaya Kredit",BiayaK)
			console.log("Biaya Debit",BiayaD)
			console.log("cash",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash)
			
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length == 0){
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD )){
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else 
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					console.log('msukkk siniiii gaaaa');
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD)){
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK)){
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}
				else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					console.log("TOT GRID2", $scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue());
					console.log("Biaya Kredit2",BiayaK)
					console.log("Biaya Debit2",BiayaD)
					console.log("CDC",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard)
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD)){
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}
			}else{
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						//	BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("Biaya Kredit", BiayaK);
					}
					
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
							BiayaD -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("Biaya DEBIT", BiayaD);
					}
					
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					
					}
					
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD )){
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD)){
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD )){
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
					else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						console.log('cdc',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != ($scope.gridApi_InformasiPembayaran.grid.columns[11].getAggregationValue() + BiayaK - BiayaD)){
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}
			}
			

			if (SimpanOK == 1) {
				$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
					console.log("0", obj.Pembayaran);
					if ((obj.Pembayaran != 0) && (obj.Pembayaran != "") && (!angular.isUndefined(obj.Pembayaran))) {
						console.log("1", obj.Pembayaran);
						TotalPembayaran += obj.Pembayaran;
					}
				});

				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);
			}

			// console.log(SimpanOK)
			// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Service - Down Payment Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var BiayaD = 0;
			var BiayaK = 0;

			if($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.length <= 0){
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Tidak Ada Daftar WO yang dibayar",
						type: "warning"
					}
				)
				$scope.ProsesSimpan = false;
				return false;
			}

			for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
				
				// berdasarkan Tipe BANK TRANSFER
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId  == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("TOTAL HITUNG KREDIT", BiayaK);
					}
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("TOTAL HITUNG DEBIT", BiayaD);
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// TotPerhitungan;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						 console.log('1');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('2');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('3');
						// break;
					}
					// TotPerhitunganBT = TotPerhitunganBT - BiayaD + BiayaK;
				// CASH COLLECTION
				}else{
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("TOTAL HITUNG KREDIT", BiayaK);
			
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						 console.log('1');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('2');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log('3');
						// break;
					}
				}	
			}

			angular.forEach($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data, function (obj,row,key) {
				if (obj.Pembayaran == 0 || (obj.Pembayaran != "" && !angular.isUndefined(obj.Pembayaran))) {
					console.log("1", obj.Pembayaran);
					console.log("Sisa Nominal", obj.SisaNominal);

					//Perhitungan 
						var TotPerhitungan = 0;
						TotPerhitunganBT = $scope.TotalPembayaranAll + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
						TotPerhitunganCC = $scope.TotalPembayaranAll;
						TotPerhitunganCO = $scope.TotalPembayaranAll;
						TotPerhitunganCD = $scope.TotalPembayaranAll;
						// Selisih = - $scope.gridApi_InformasiPembayaran.grid.columns[10].getAggregationValue()
						console.log("penjumlahan",TotPerhitunganBT);
						console.log("TOTAL CC",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
						console.log("TOTAL CC",TotPerhitunganCC);

						// for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						// 	console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
							
						// 	// berdasarkan Tipe BANK TRANSFER
						// 	if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						// 		if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId  == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						// 			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						// 				TotPerhitunganBT += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log("TOTAL HITUNG KREDIT", TotPerhitunganBT);
						// 		}
						// 		else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// 			TotPerhitunganBT -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log("TOTAL HITUNG DEBIT", TotPerhitunganBT);
						// 		}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// 		// TotPerhitungan;
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						// 			BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			 console.log('1');
						// 			// break;
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						// 			BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						// 			// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log('2');
						// 			// break;
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						// 			BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log('3');
						// 			// break;
						// 		}
						// 		// TotPerhitunganBT = TotPerhitunganBT - BiayaD + BiayaK;
						// 	// CASH COLLECTION
						// 	}else{
						// 		if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						// 			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						// 			TotPerhitunganCC += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log("TOTAL HITUNG KREDIT", TotPerhitunganCC);
						
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						// 			BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			 console.log('1');
						// 			// break;
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						// 			BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						// 			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log('2');
						// 			// break;
						// 		}
						// 		if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						// 			BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 			console.log('3');
						// 			// break;
						// 		}
						// 		// TotPerhitunganBT = TotPerhitunganBT + BiayaD - BiayaK;
						// 	// CASH OPERATIO
						// 	// }else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						// 	// 	if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						// 	// 		($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						// 	// 		TotPerhitunganCO += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 	// 		console.log("TOTAL HITUNG KREDIT", TotPerhitunganCO);
						
						// 	// 	}
						// 	// //Credit/Debit
						// 	// }else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						// 	// 	if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						// 	// 		($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						// 	// 		TotPerhitunganCD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 	// 		console.log("TOTAL HITUNG KREDIT", TotPerhitunganCD);
					
						// 	// 	}
						// 	}	
								
						// 	// if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						// 	//    ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						// 	// 	TotPerhitungan += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 	//   	console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
						
						// 	// }
						// 	// else if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// 	//   	TotPerhitungan -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						// 	//   	console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
						// 	// }else if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// 	// 	// TotPerhitungan;
						// 	// }
						// }

							
					//End<>

					// if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer > TotPerhitungan) {
					// 	SimpanOK = 0;
					// }
						// var pos = 0;
						// for (var i=0; i<$scope.gridApi_InformasiPembayaran.grid.columns.length; i++){
						// 	if ($scope.gridApi_InformasiPembayaran.grid.columns[i].field == 'Pembayaran'){
						// 		pos = i;
						// 	}
						// }
						// console.log("TEST>>>>>>", $scope.gridApi_InformasiPembayaran.grid.columns[pos].getAggregationValue())
					// if (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer > $scope.gridApi_InformasiPembayaran.grid.columns[pos].getAggregationValue()) || ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > $scope.gridApi_InformasiPembayaran.grid.columns[pos].getAggregationValue())
					// 	|| $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > $scope.gridApi_InformasiPembayaran.grid.columns[pos].getAggregationValue()) {
					// 	SimpanOK = 0;
					// }
				}
				else {
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran untuk Nomor Billing : <b>" + obj.NoBilling + "</b> wajib diisi!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			})
			//Penjagaan 
			if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				console.log('cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				console.log('BT',TotPerhitunganBT);
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != TotPerhitunganBT + BiayaK - BiayaD ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				  }
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
				console.log('cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
				console.log('CC',TotPerhitunganCC);
				console.log('BiayaD',BiayaD);
				console.log('BiayaK',BiayaK);
			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != TotPerhitunganCC - BiayaD + BiayaK){
				SimpanOK = 0;
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Field Pembayaran Tidak Sesuai!",
						type: 'danger'
					}
				);
				console.log("Debit Card",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash)
				console.log("Credit Card",TotPerhitunganCC)
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
				if($scope.currentOPNominal != TotPerhitunganCC - BiayaD + BiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
			if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != TotPerhitunganCC - BiayaD + BiayaK){
				SimpanOK = 0;
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Field Pembayaran Tidak Sesuai!",
						type: 'danger'
					}
				);
				console.log("Debit Card",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard)
				console.log("Credit Card",TotPerhitunganCD)
				}
			};

			if (SimpanOK == 1) {
				$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
					console.log("0", obj.Pembayaran);
					if ((obj.Pembayaran != 0) && (obj.Pembayaran != "") && (!angular.isUndefined(obj.Pembayaran))) {
						console.log("1", obj.Pembayaran);
						TotalPembayaran += obj.Pembayaran;
					}
				});

				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);
			}

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Service - Full Payment Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var tmpPendapatan = 0;

			$scope.LoopingBiaya();
			for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
						tmpPendapatan = 1;
					}
				}
			}

			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data.forEach(function(obj){
					if((obj.SisaDP != 0) && (obj.SisaDP != "") && (!angular.isUndefined(obj.SisaDP))){
						TotalPembayaran += obj.SisaDP; 
					}
				});
			}else{
				$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.data.forEach(function(obj){
					if((obj.SisaDP != 0) && (obj.SisaDP != "") && (!angular.isUndefined(obj.SisaDP))){
						TotalPembayaran += obj.SisaDP
					}
				});
			}

			//Penjagaan Parts DP
			if(tmpPendapatan == 1){
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != TotalPembayaran + $scope.tmpKredit - $scope.tmpDebit ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					  }
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != TotalPembayaran + $scope.tmpKredit - $scope.tmpDebit){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != TotalPembayaran + $scope.tmpKredit - $scope.tmpDebit){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != TotalPembayaran + $scope.tmpKredit - $scope.tmpDebit){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}
			}else{
				SimpanOK = 1
			}

			if(SimpanOK == 1){
				TambahIncomingPayment_Simpan_General();
			}else{
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Parts - Down Payment Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			var SimpanOK = 1;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var tmpPendapatan = 0;

			
			for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaK =TotBiayaK;
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotBiayaD -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaK =TotBiayaK;
						console.log('1');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaK =TotBiayaK;
						console.log('2');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaD =TotBiayaD;
						console.log('3');
						// break;
					}
				}else{
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							$scope.tmpBiayaK =TotBiayaK;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaK =TotBiayaK;
						console.log('1');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaK =TotBiayaK;
						console.log('2');
						// break;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						$scope.tmpBiayaD =TotBiayaD;
						console.log('3');
						// break;
					}
				}
				// if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				// 	($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// }else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				// 		TotBiayaD -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
				// 	}
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		 console.log('1');
				// 		// break;
				// 	}
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
				// 		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		console.log('2');
				// 		// break;
				// 	}
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				// 		TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		console.log('3');
				// 		// break;
				// 	}
				// 	// TotPerhitunganBT = TotPerhitunganBT - TotBiayaD + TotBiayaK;
				// }else{
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		 console.log('1');
				// 		// break;
				// 	}
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				// 		TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
				// 		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		console.log('2');
				// 		// break;
				// 	}
				// 	if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				// 		TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
				// 		console.log('3');
				// 		// break;
				// 	}
				// }
			}

			for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
						tmpPendapatan = 1;
					}
				}
				
			}

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				// START BARU DI TAMBAHKAN IRFAN
				angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data,function (obj) {
					if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
						TotalPembayaran += obj.SisaPembayaran;
						$scope.NomPem = TotalPembayaran;
						// if(($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer > obj.SisaPembayaran) 
						// 	|| ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > obj.SisaPembayaran)
						// 	|| ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > obj.SisaPembayaran) ){
						// 	SimpanOK = 0;
						// }

						// perhitungan Parts FP
						HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
						HitungCC = TotalPembayaran;
						HitungCO = TotalPembayaran;
						HitungCD = TotalPembayaran;
						console.log("Nilai Kredit", TotBiayaK);
						console.log("Nilai Debit", TotBiayaD);
					}else{
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Harus Lebih atau sama dengan Saldo",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				});

				// Penjagaan Parts FP
				if(tmpPendapatan == 1){
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						console.log('cash',$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
						console.log('HitungCC',HitungCC);
						console.log('TotBiayaK',TotBiayaK);
						console.log('TotBiayaD',TotBiayaD);
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}else{
					if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer > HitungBT + TotBiayaK - TotBiayaD){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash > HitungCC + TotBiayaK ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
						if($scope.currentOPNominal > HitungCO + TotBiayaK ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
						if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard > HitungCD + TotBiayaK ){
							SimpanOK = 0;
							bsNotify.show(
								{
									title: "Notifikasi",
									content: "Nominal Pembayaran Tidak Sesuai!",
									type: 'danger'
								}
							);
							$scope.ProsesSimpan = false;
							return false;
						}
					}
				}
				
			
				if (SimpanOK == 1) {
					$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
						console.log("0", obj.SisaPembayaran);
						if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
							console.log("1", obj.SisaPembayaran);
							TotalPembayaran += obj.SisaPembayaran;
						}
					});
	
					// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);
				}
	
				if (SimpanOK == 1) {
					TambahIncomingPayment_Simpan_General();
				}
				else {
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Pembayaran Parts - Full Payment Tidak Sesuai",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				};
				// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, false);
			}

			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				angular.forEach($scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data,function (obj) {
					
					console.log("Parts SIsa Pembayaran>>>>>",obj.SisaPembayaran);
					// var posisi1 = 0;
					// 	for (var i=0; i<$scope.gridApi_InformasiPembayaran.grid.columns.length; i++){
					// 		if ($scope.gridApi_InformasiPembayaran.grid.columns[i].field == 'Saldo'){
					// 			posisi1 = i;
					// 		}
					// 	}
					if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
						TotalPembayaran += obj.SisaPembayaran;
						
						// if(($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer >  $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[posisi1].getAggregationValue()) 
						// 	|| ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash >  $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[posisi1].getAggregationValue())
						// 	|| ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard >  $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[posisi1].getAggregationValue()) ){
						// 	SimpanOK = 0;
						// }

						// perhitungan Parts FP
						HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
						HitungCC = TotalPembayaran;
						HitungCO = TotalPembayaran;
						HitungCD = TotalPembayaran;
						
					}else{
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Pembayaran Harus Lebih kecil atau sama dengan Saldo",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				});

				// Penjagaan Parts FP
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT + TotBiayaK - TotBiayaD){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC + TotBiayaK - TotBiayaD){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != HitungCO + TotBiayaK - TotBiayaD ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD + TotBiayaK - TotBiayaD ){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}
				// SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);
				if (SimpanOK == 1) {
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
						console.log("0", obj.SisaPembayaran);
						if ((obj.SisaPembayaran != 0) && (obj.SisaPembayaran != "") && (!angular.isUndefined(obj.SisaPembayaran))) {
							console.log("1", obj.SisaPembayaran);
							TotalPembayaran += obj.SisaPembayaran;
						}
					});
	
					SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(TotalPembayaran, true, true);
				}
	
				if (SimpanOK == 1) {
					TambahIncomingPayment_Simpan_General();
				}
				else {
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Pembayaran Parts - Full Payment Tidak Sesuai",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				};
			}
			// END TAMBAHAN IRFAN

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown") {
			TambahIncomingPayment_Simpan_General();
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			var SimpanOK = 1;
			var pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.toString().replace(/,/g, "");

			if ($scope.TambahIncomingPayment_BiayaforParseFloat == "" || $scope.TambahIncomingPayment_BiayaforParseFloat == undefined || $scope.TambahIncomingPayment_BiayaforParseFloat == null ) {
				$scope.TambahIncomingPayment_BiayaforParseFloat = 0;
			}
			var ARBank = parseFloat($scope.currentNominal) - parseFloat($scope.TambahIncomingPayment_BiayaforParseFloat);
			if (parseFloat(pembayaran) != parseFloat(ARBank.toFixed(2))) {
				SimpanOK = 0;
			}

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				$scope.ProsesSimpan = false;
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Bank Statement - Card Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			TambahIncomingPayment_Simpan_General();
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Cash Delivery") {
			TambahIncomingPayment_Simpan_General();
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			var SimpanOK = 1;
			var totalRangkaDibayar = 0;
			var TotalPembayaran = 0;
			var TotBiayaD = 0;
			var TotBiayaK = 0;
			var countSama = 0;

			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran != "Bank Transfer"){
				if($scope.newValKui > $scope.SisaAR){
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
							type: 'warning'
						}
					);  
					$scope.ProsesSimpan = false;
					return;
				}else if($scope.newValKui <= 0){ 
					bsNotify.show(
						{
							title: "Peringatan",
							content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
							type: 'warning'
						}
					);    
					$scope.ProsesSimpan = false;  
					return;
				}
			}
			// $scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data.forEach(function (PIHeader) {
				$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (PIDetail) {
					if ((PIDetail.Nominal != 0) && (PIDetail.Nominal != "") && (!angular.isUndefined(PIDetail.Nominal))) {
						TotalPembayaran += PIDetail.Nominal;
					}
					$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data.forEach(function (PIHeader) {
						if (PIHeader.NoRangka == PIDetail.NoRangka) {
							if (PIDetail.Nominal > PIHeader.NominalPI) {
								SimpanOK = 0;
								countSama++
								$scope.NoRangka = PIDetail.NoRangka;
							}
						}
					});
				});
				
				if(countSama > 0){
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Field Nominal untuk Nomor Rangka : <b>" + $scope.NoRangka + "</b> tidak boleh lebih besar dari Nominal Kuitansi Penagihan!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return;
				}
			// });

			console.log("Cek Total Pembayaran Kuitansi Penagihan", TotalPembayaran);

					// perhitungan unit kuitansi penagihan
					// HitungBT = TotalPembayaran + $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
					HitungBT = TotalPembayaran;
					HitungCC = TotalPembayaran;
					HitungCO = TotalPembayaran;
					HitungCD = TotalPembayaran;

					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								 console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}else{
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								 console.log('1');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								TotBiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('2');
								// break;
							}
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								TotBiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log('3');
								// break;
							}
						}
					}
			// 	});
			// });

			// Penjagaan Parts FP
			if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT - TotBiayaD + TotBiayaK){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash != HitungCC - TotBiayaD + TotBiayaK ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
				if($scope.currentOPNominal != HitungCO - TotBiayaD + TotBiayaK ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
				if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard != HitungCD - TotBiayaD + TotBiayaK ){
					SimpanOK = 0;
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Nominal Pembayaran Tidak Sesuai!",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				}
			}

			if (SimpanOK == true) {
				$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (obj) {
					if (!obj.Nominal) {
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Field Nominal untuk Nomor Rangka : <b>" + obj.NoRangka + "</b> wajib diisi!",
								type: 'danger'
							}
						);
					}
					else {
						totalRangkaDibayar += obj.Nominal
					}
				});

				SimpanOK = $scope.CashCollection_and_CreditDebit_Validation(totalRangkaDibayar, true, true);
			}

			if (SimpanOK == 1) {
				TambahIncomingPayment_Simpan_General();
			}
			else {
				bsNotify.show(
					{
						title: "Notifikasi",
						content: "Pembayaran Unit - Payment (Kuitansi Penagihan) Tidak Sesuai",
						type: 'danger'
					}
				);
				$scope.ProsesSimpan = false;
				return false;
			};
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			var SimpanOK = 1;
			var TotPem = 0;
			var NomPPN = 0;

			for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId  == "PPN Keluaran" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					NomPPN += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					console.log("Cek nilai Nom PPN", NomPPN);
				}
			}

			angular.forEach($scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data, function(obj){
					if (obj.NominalRefund != 0 && (obj.NominalRefund != "" && !angular.isUndefined(obj.NominalRefund))) {
						TotPem += obj.NominalRefund;

						HitungBT = TotPem;
						HitungCO = TotPem;
					}
				});
				
				console.log("cek nilai BT", HitungBT);
				
				//penjagaan Refaund Leasing
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer != HitungBT - NomPPN){
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation"){
					if($scope.currentOPNominal != HitungCO - NomPPN){
						console.log("Cek nIlai Outgoing", $scope.currentOPNominal);
						SimpanOK = 0;
						bsNotify.show(
							{
								title: "Notifikasi",
								content: "Nominal Pembayaran Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.ProsesSimpan = false;
						return false;
					}
				}

				if (SimpanOK == 1) {
					TambahIncomingPayment_Simpan_General();
				}
				else {
					bsNotify.show(
						{
							title: "Notifikasi",
							content: "Pembayaran Refund Lising Tidak Sesuai",
							type: 'danger'
						}
					);
					$scope.ProsesSimpan = false;
					return false;
				};
				// TambahIncomingPayment_Simpan_General();
			
		}
	}

	function TambahIncomingPayment_Simpan_General() {
		//------------------------------------------------
		//INIT DATA
		//------------------------------------------------
		var data;
		$scope.SOSubmitData = [];
		$scope.SOSubmitBiaya = [];
		var paymentType = 0;
		var metodeBayar = 0;
		var userId = 1;
		var CardType = "";
		var today = new Date();
		//var paramDetail = JSON.stringify($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data);

		//set metode bayar
		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { metodeBayar = 1; }
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") { metodeBayar = 2; }
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") { metodeBayar = 3; }
		else { metodeBayar = 4; }

		//set CardType
		if ($scope.TambahIncomingPayment_RincianPembayaran_TipeKartu_CreditDebitCard == "1") { $scope.CardType = "Credit Card"; }
		else { $scope.CardType = "Debit Card"; }

		console.log("Tipe Kartu");
		console.log($scope.TambahIncomingPayment_RincianPembayaran_TipeKartu_CreditDebitCard);
		console.log($scope.CardType);

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"){
				$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.toString().replace(/,/g, "");
			}else{
				$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "");
			}
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
			$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, "");
		} else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
			//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [2] | Binding Variabel dari Grid Outgoing ?? Gak perlu, langsung ambil $scope.currentOPNominal
			$scope.Pembayaran = $scope.currentOPNominal;
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, "");
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Interbranch") {
			$scope.Pembayaran = $scope.currentNominalPembayaran;
		}

		//set tipe payment
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			//paymentType = 1;
			$scope.TambahIncomingPayment_BookingFeeData();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_BookingFeeSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;

		}
		//20170430, NUSE EDIT, begin
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			$scope.TambahIncomingPayment_FullPaymentSOData();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_SOSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}

		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			$scope.TambahIncomingPayment_FullPaymentSOData();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_SOSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}
		//20170430, NUSE EDIT, end
		//20170505, Alvin, begin
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			console.log("Service - Down Payment")
			$scope.TambahIncomingPayment_Service_ByWO();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_ServiceSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			console.log("Service - Full Payment")

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_Service_ByWO();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_ServiceSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_Service_ByName();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_ServiceSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			console.log("Parts - Down Payment",$scope.TambahIncomingPayment_PartsSOSubmitData)
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_SoDown();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
				console.log('masuk 2',$scope.TambahIncomingPayment_PartsSOSubmitData)
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_SalesOrder2();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
				console.log('masuk 3')
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			console.log("Parts - Full Payment")
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_SoBillPFP();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_SalesOrder();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			console.log("Unit - Swapping")
			$scope.TambahIncomingPayment_SoBill();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {
		// console.log("Unit - Ekspedisi")
		// $scope.TambahIncomingPayment_SoBill();	
		// $scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
		// $scope.TambahIncomingPayment_Biaya2();
		// $scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		// }		
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			console.log("Unit - Purna Jual")
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_SoBillPJOPD();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_SalesOrder();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			console.log("Unit - Order Pengurusan Dokumen")
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_SoBillPJOPD();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_SalesOrder();
				$scope.SOSubmitData = $scope.TambahIncomingPayment_PartsSOSubmitData;
				$scope.TambahIncomingPayment_Biaya2();
				$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
			}
		}

		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			$scope.TambahIncomingPayment_NoRangka();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_NoRangkaSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			$scope.TambahIncomingPayment_RefundLeasing();
			$scope.SOSubmitData = $scope.TambahIncomingPayment_RefundLeasingSubmitData;
			$scope.TambahIncomingPayment_Biaya2();
			$scope.SOSubmitBiaya = $scope.TambahIncomingPayment_BiayaData;
		}


		console.log($scope.currentTglAR);
		if ($scope.currentTglAR == "") {
			$scope.currentTglAR = $scope.TambahIncomingPayment_TanggalIncomingPayment;
		}

		//------------------------------------------------
		//MAIN DATA
		//------------------------------------------------

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			//Alvin, 20170423, begin

			//if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card")
			//	|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing")) {
			//	console.log("currentTglAR -> ", $scope.currentTglAR);
			//	$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer = new Date();
			//}

			console.log("currentTglAR2 -> ", $scope.currentTglAR);
			
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee" ||
				$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment" ||
				$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment" ||
				$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment"){
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
			}
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"){
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,

					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",

					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer,
					PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_BankTransfer,
					ReceiverAccNumber: $scope.TambahIncomingPayment_BankTransfer_RekeningPenerima,
					SenderBankId: $scope.TambahIncomingPayment_BankTransfer_BankPengirim,
					SenderName: $scope.TambahIncomingPayment_RincianPembayaran_NamaPengirim_BankTransfer,
					ChequeId: $scope.TambahIncomingPayment_BankTransfer_NoCheque,
					CardProvider: $scope.TambahIncomingPayment_SearchBy_NamaBank,
					CardNo: "",
					CardType: "",
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: $scope.currentTglAR,
					GridCardType: $scope.currentCardType,
					GridCardNominal: $scope.currentNominal,
					BankCharges: $scope.TambahIncomingPayment_Biaya,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end


					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
				//Alvin, 20170423, end
				console.log("currentTglAR3 -> ", $scope.currentTglAR);
				console.log(data);$scope.Pembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.toString().replace(/,/g, "");
			}else{
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,

					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",

					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer,
					PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_BankTransfer,
					ReceiverAccNumber: $scope.TambahIncomingPayment_BankTransfer_RekeningPenerima,
					SenderBankId: $scope.TambahIncomingPayment_BankTransfer_BankPengirim,
					SenderName: $scope.TambahIncomingPayment_RincianPembayaran_NamaPengirim_BankTransfer,
					ChequeId: $scope.TambahIncomingPayment_BankTransfer_NoCheque,
					CardProvider: $scope.TambahIncomingPayment_SearchBy_NamaBank,
					CardNo: "",
					CardType: "",
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: $scope.currentTglAR,
					GridCardType: $scope.currentCardType,
					GridCardNominal: $scope.currentNominal,
					BankCharges: $scope.TambahIncomingPayment_Biaya,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end


					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
				//Alvin, 20170423, end
				console.log("currentTglAR3 -> ", $scope.currentTglAR);
				console.log(data);
			}
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
			//Alvin, 20170423, begin

			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima"){
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,
	
					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",
	
					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: 0,
					PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash,
					ReceiverAccNumber: $scope.TujuanPembayaran_CashDelivery, //For Cash Delivery
					SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
					SenderName: "",
					ChequeId: 0,
					CardProvider: "",
					CardNo: "",
					CardType: "",
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash,
					GridCardType: "",
					GridCardNominal: 0,
					BankCharges: 0,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end
	
	
					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
			}else{
				var tmpCash = $filter('date')(new Date($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash),'yyyy-MM-dd');
				var tmpTrand = $filter('date')(new Date($scope.TambahIncomingPayment_TanggalIncomingPayment),'yyyy-MM-dd');
				
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: tmpTrand,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,
	
					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",
	
					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: 0,
					PaymentDate: tmpCash,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash,
					ReceiverAccNumber: $scope.TujuanPembayaran_CashDelivery, //For Cash Delivery
					SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
					SenderName: "",
					ChequeId: 0,
					CardProvider: "",
					CardNo: "",
					CardType: "",
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: tmpCash,
					GridCardType: "",
					GridCardNominal: 0,
					BankCharges: 0,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end
					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
			}
			
			//Alvin, 20170423, end
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
			//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [3] | Create Data Object
			data = [{
				OutletId: $scope.currentOutletId,
				SPKId: $scope.currentSPKId,
				TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
				IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
				CustomerId: $scope.currentCustomerId,
				Currency: "IDR",
				PaymentTotal: $scope.currentOPNominal,
				ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
				PrintCounter: 0,
				OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
				UserId: userId,

				IncomingPaymentDetailXML: "",
				IncomingPaymentChargesXML: "",

				PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
				Difference: 0,
				PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation,
				PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CashOperation,
				ReceiverAccNumber: $scope.TujuanPembayaran_CashDelivery, //For Cash Delivery
				SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
				SenderName: "",
				ChequeId: 0,
				CardProvider: "",
				CardNo: "",
				CardType: "",
				InterBranchId: 0,
				InterBranchDesc: "",
				GridARDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation,
				GridCardType: "",
				GridCardNominal: 0,
				BankCharges: 0,
				BankStatementUnknownId: 0,
				InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
				InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
				SOList: $scope.SOSubmitData,
				BiayaList: $scope.SOSubmitBiaya,
				ProformaInvoiceId: $scope.currentProformaInvoiceId,
				OPId: $scope.currentOPId
			}]
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			//Alvin, 20170423, begin								
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima"){
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CreditDebitCard,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,
	
					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",
	
					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: 0,
					PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CreditDebitCard,
					ReceiverAccNumber: "",
					SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
					SenderName: "",
					ChequeId: 0,
					CardProvider: $scope.TambahIncomingPayment_CreditDebitCard_NamaBank,
					CardNo: $scope.TambahIncomingPayment_RincianPembayaran_NomorKartu_CreditDebitCard,
					CardType: $scope.CardType,
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard,
					GridCardType: "",
					GridCardNominal: 0,
					BankCharges: 0,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end
	
	
					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
			}else{
				data = [{
					OutletId: $scope.currentOutletId,
					SPKId: $scope.currentSPKId,
					TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
					IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
					CustomerId: $scope.currentCustomerId,
					Currency: "IDR",
					PaymentTotal: $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, ""),
					ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
					PrintCounter: 0,
					OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
					UserId: userId,
	
					IncomingPaymentDetailXML: "",
					IncomingPaymentChargesXML: "",
	
					PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
					Difference: 0,
					PaymentDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard,
					PaymentDesc: $scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CreditDebitCard,
					ReceiverAccNumber: "",
					SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
					SenderName: "",
					ChequeId: 0,
					CardProvider: $scope.TambahIncomingPayment_CreditDebitCard_NamaBank,
					CardNo: $scope.TambahIncomingPayment_RincianPembayaran_NomorKartu_CreditDebitCard,
					CardType: $scope.CardType,
					InterBranchId: 0,
					InterBranchDesc: "",
					GridARDate: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard,
					GridCardType: "",
					GridCardNominal: 0,
					BankCharges: 0,
					BankStatementUnknownId: 0,
					InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
					InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
					//20170430, NUSE EDIT, begin
					SOList: $scope.SOSubmitData,
					BiayaList: $scope.SOSubmitBiaya,
					//20170430, NUSE EDIT, end
	
	
					ProformaInvoiceId: $scope.currentProformaInvoiceId
				}]
			}
			
			//Alvin, 20170423, end				
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Interbranch") {
			//Alvin, 20170423, begin			

			data = [{
				OutletId: $scope.currentOutletId,
				SPKId: $scope.currentSPKId,
				TranDate: $scope.TambahIncomingPayment_TanggalIncomingPayment,
				IncomingPaymentType: $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment,
				CustomerId: $scope.currentCustomerId,
				Currency: "IDR",
				PaymentTotal: $scope.currentNominalPembayaran,
				ReceiptNo: $scope.TambahIncomingPayment_NomorKuitansi + "|" + $scope.TambahIncomingPayment_KeteranganKuitansi,
				PrintCounter: 0,
				OwnRisk: $scope.TambahIncomingPayment_OwnRisk,
				UserId: userId,

				IncomingPaymentDetailXML: "",
				IncomingPaymentChargesXML: "",

				PaymentMethod: $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran,
				Difference: 0,
				PaymentDate: $scope.currentTanggalBank,
				PaymentDesc: "",
				ReceiverAccNumber: "",
				SenderBankId: $scope.TambahIncomingPayment_NamaBranch_SearchDropdown,
				SenderName: "",
				ChequeId: 0,
				CardProvider: "",
				CardNo: "",
				CardType: "",
				InterBranchId: $scope.currentInterBranchId,
				InterBranchDesc: $scope.currentUntukPembayaran,
				GridARDate: $scope.currentTanggalBank,
				GridCardType: "",
				GridCardNominal: 0,
				BankCharges: 0,
				BankStatementUnknownId: 0,
				InterbranchNamaPelanggan: $scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan,
				InterbranchUntukPembayaran: $scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran,
				//20170430, NUSE EDIT, begin
				SOList: $scope.SOSubmitData,
				BiayaList: $scope.SOSubmitBiaya,
				//20170430, NUSE EDIT, end		


				ProformaInvoiceId: $scope.currentProformaInvoiceId
			}]
			//Alvin, 20170423, end					

		}

		//------------------------------------------------
		//SUBMIT
		//------------------------------------------------
		//alvin, 20170423, begin
		//$scope.submitData(data);

		if (!$scope.TambahIncomingPayment_CheckFields())
			return;

		if ((angular.isUndefined($scope.SOSubmitData)) || ($scope.SOSubmitData == "")) {
			console.log($scope.SOSubmitData);
			if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Unknown")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Card")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Cash Delivery")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Interbranch - Penerima")) {
				bsNotify.show({
					title: "Warning",
					content: "Data pembayaran belum lengkap",
					type: 'warning'
				});
				submitGeneralOK = false;
			}
		}

		console.log("Cek Submit");
		console.log($scope.SOSubmitData);

		angular.forEach($scope.SOSubmitData.data, function (value, key) {
			console.log(value);
			console.log(key);
		});

		var found = 0;

		$scope.SOSubmitData.forEach(function (obj) {
			console.log('isi grid -->', obj.SOId);
			console.log('isi grid -->', obj.PaidAmount);
			var PaidAmount = obj.PaidAmount;
			console.log("1", PaidAmount);
			console.log("1", angular.isNumber(PaidAmount));

			if (!angular.isNumber(PaidAmount) && (obj.PaidAmount != undefined)) {
				console.log("PaidAmount", PaidAmount);
				PaidAmount = PaidAmount.toString().replace(/,/g, "");
				console.log("PaidAmount", PaidAmount);
			}
			if ((PaidAmount > 0) && (obj.PaidAmount != undefined))
				found = 1;
		});
		
		if (found == 0) {
			if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Unknown")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Card")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Cash Delivery")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Interbranch - Penerima")) {
				bsNotify.show({
					title: "Warning",
					content: "Detail pembayaran belum lengkap",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
		}else{
			submitGeneralOK = true;
		}


		if (angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran)
			|| $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "") {
			bsNotify.show({
				title: "Warning",
				content: "Metode pembayaran harus dipilih",
				type: 'warning'
			});
			submitGeneralOK = false;
			$scope.ProsesSimpan = false;
		}

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"){
				if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard)) || ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer == "")) {
					bsNotify.show({
						title: "Warning",
						content: "Nominal Pembayaran belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
					$scope.ProsesSimpan = false;
				}
				if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard == 0) {
					bsNotify.show({
						title: "Warning",
						content: "Nominal Pembayaran harus lebih dari 0",
						type: 'warning'
					});
					submitGeneralOK = false;
					$scope.ProsesSimpan = false;
				}
			}else{
				if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer)) || ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer == "")) {
					bsNotify.show({
						title: "Warning",
						content: "Nominal Pembayaran belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
					$scope.ProsesSimpan = false;
				}
				if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer == 0) {
					bsNotify.show({
						title: "Warning",
						content: "Nominal Pembayaran harus lebih dari 0",
						type: 'warning'
					});
					submitGeneralOK = false;
					$scope.ProsesSimpan = false;
				}
			}
			if ((angular.isUndefined($scope.TambahIncomingPayment_BankTransfer_RekeningPenerima)) || ($scope.TambahIncomingPayment_BankTransfer_RekeningPenerima == "")) {
				bsNotify.show({
					title: "Warning",
					content: "Rekening penerima belum diisi",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}

			if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Unknown")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Bank Statement - Card")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Cash Delivery")
				&& ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Interbranch - Penerima")) {
				if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_Keterangan_BankTransfer)) || ($scope.TambahIncomingPayment_RincianPembayaran_Keterangan_BankTransfer == "")) {
					bsNotify.show({
						title: "Warning",
						content: "Keterangan belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
					$scope.ProsesSimpan = false;
				}
			}
		}
		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
			if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash)) || ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash == "")) {
				bsNotify.show({
					title: "Warning",
					content: "Nominal Pembayaran belum diisi",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
			if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash == 0) {
				bsNotify.show({
					title: "Warning",
					content: "Nominal Pembayaran harus lebih dari 0",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
			if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash)) || ($scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash == "")) {
				bsNotify.show({
					title: "Warning",
					content: "Keterangan belum diisi",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
		}

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
			//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [4] | Validasi Pilih Outgoing
			if ($scope.currentOPId == '' || $scope.currentOPId == 0 || $scope.currentOPId == 'undefined') {
				bsNotify.show({
					title: "Warning",
					content: "Outgoing Payment belum dipilih",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
		}
		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			if ((angular.isUndefined($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard))
				|| ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard == "")) {
				bsNotify.show({
					title: "Warning",
					content: "Nominal Pembayaran belum diisi",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
			if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard == 0) {
				bsNotify.show({
					title: "Warning",
					content: "Nominal Pembayaran harus lebih dari 0",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
			if ((angular.isUndefined($scope.TambahIncomingPayment_CreditDebitCard_NamaBank))
				|| ($scope.TambahIncomingPayment_CreditDebitCard_NamaBank == "")) {
				bsNotify.show({
					title: "Warning",
					content: "Nama bank pembayaran belum diisi",
					type: 'warning'
				});
				submitGeneralOK = false;
				$scope.ProsesSimpan = false;
			}
		}

		if (!submitGeneralOK) {
			return false;
		}

		if ($scope.TambahIncomingPayment_TanggalIncomingPayment != null) {
			console.log("TanggalIncomingPayment sudah diisi");
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				console.log("Bank Transfer");
				//console.log(TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer);
				if (($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer != null)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer != undefined)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer != "Invalid Date")
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer != "")) {
					console.log("Bank Transfer1");
					console.log(data);
					$scope.submitDataFinanceIncomingPayments(data);
				}
				// else if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Bank Statement - Card")
				// || ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Refund Leasing"))
				// {
				// $scope.submitDataFinanceIncomingPayments(data);
				// }
				else {
					bsNotify.show({
						title: "Warning",
						content: "Tanggal pembayaran bank transfer belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
				}
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
				if (($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash != null)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash != undefined)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash != "Invalid Date")
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash != "")) {
					
					//Penambahan backdate reason
					// data.forEach(object => {
					// 	object.BackdateReason = $scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate;
					// });

					data[0].BackdateReason = $scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate;
					$scope.submitDataFinanceIncomingPayments(data);
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Tanggal pembayaran cash belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
				}
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
				//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [5] | Submit Data
				$scope.submitDataFinanceIncomingPayments(data);
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
				if (($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard != null)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard != undefined)
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard != "Invalid Date")
					&& ($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard != "")) {

					//Penambahan backdate reason
					data[0].BackdateReason = $scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate;

					$scope.submitDataFinanceIncomingPayments(data);
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Tanggal pembayaran credit/debit belum diisi",
						type: 'warning'
					});
					submitGeneralOK = false;
				}
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Interbranch") {
				$scope.submitDataFinanceIncomingPayments(data);
			}
		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal Incoming belum diisi",
				type: 'warning'
			});
			submitGeneralOK = false;
		}

		if (!submitGeneralOK) {
			return false;
		}
		//alvin, 20170423, end
	};

	$scope.TambahIncomingPayment_Biaya_Blur = function () {
		var SisaFloat = "";
			if ($scope.TambahIncomingPayment_Biaya.substr($scope.TambahIncomingPayment_Biaya.length-3,1) == "."){
				SisaFloat = $scope.TambahIncomingPayment_Biaya.substr($scope.TambahIncomingPayment_Biaya.length-3);
				$scope.TambahIncomingPayment_Biaya = $scope.TambahIncomingPayment_Biaya.substr(0,$scope.TambahIncomingPayment_Biaya.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahIncomingPayment_Biaya);
			}
			else if ($scope.TambahIncomingPayment_Biaya.substr($scope.TambahIncomingPayment_Biaya.length-2,1) == "."){
				SisaFloat = $scope.TambahIncomingPayment_Biaya.substr($scope.TambahIncomingPayment_Biaya.length-2);
				$scope.TambahIncomingPayment_Biaya = $scope.TambahIncomingPayment_Biaya.substr(0,$scope.TambahIncomingPayment_Biaya.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahIncomingPayment_Biaya);
			}
			var val = $scope.TambahIncomingPayment_Biaya.split(",").join("");
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1,")
				.split("").reverse().join("");
	
			var str_array = val.split(',');
	
			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}
	
				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}

			if (!NominalValid) {
				$scope.TambahIncomingPayment_Biaya_Blur(val);
			}
			else {
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahIncomingPayment_Biaya = val+".00";
					$scope.TambahIncomingPayment_BiayaforParseFloat = val.split(",").join("")+".00";
				}
				else{
					$scope.TambahIncomingPayment_Biaya = val+SisaFloat;
					$scope.TambahIncomingPayment_BiayaforParseFloat = val.split(",").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahIncomingPayment_BiayaforParseFloat);
				}
			}
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			var ARBank = parseFloat($scope.currentNominal) - parseFloat($scope.TambahIncomingPayment_BiayaforParseFloat);
			console.log('cek nominla', parseFloat($scope.TambahIncomingPayment_BiayaforParseFloat))
			if(ARBank < 0){
				ARBank = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard = ARBank.toFixed(2);
			$scope.formatCurrency()
			$scope.ProsesSimpan = false;
		}
	}

	$scope.formatCurrency = function(){
		var SisaFloat = "";
			if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-3,1) == "."){
				SisaFloat = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-3);
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr(0,$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard);
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-2,1) == "."){
				SisaFloat = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-2);
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.substr(0,$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard);
			}
			var val = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard.split(",").join("");
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1,")
				.split("").reverse().join("");
	
			var str_array = val.split(',');
	
			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}
	
				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}

			if (!NominalValid) {
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_Change_StatementCard(val);
			}
			else {
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard = val+".00";
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCardforParseFloat = val.split(",").join("")+".00";
				}
				else{
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard = val+SisaFloat;
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCardforParseFloat = val.split(",").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard);
				}
			}
	}

	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur_StatementCard = function () {
		var TotalBookingFee = 0;
		var TotalDP = 0;
		var PembayaranDP = 0;
		$scope.TotalDPUnitFP = 0;
		$scope.KuitansiPem = 0;
		$scope.PemOPD = 0;
		$scope.PemSwap = 0;
		$scope.SisaAR = 0;
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
				TotalBookingFee += obj.NominalBookingFee;
			});
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalBookingFee;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				$scope.TotalDPUnitFP += PembayaranDP;
				
			});
			var BiayaBankD = 0; 
			var BiayaBankK = 0;
			var BiayaD = 0;
			var BiayaK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - $scope.TotalDPUnitFP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
			
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			console.log("blur");
			console.log("isi nominal >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == 2){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					console.log("masuk sini >>>>", obj.SisaPembayaran);
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
						console.log(obj.SisaPembayaran);
						console.log("isi nominal 2 >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
						console.log("if calculate >>>>>", obj.SisaPembayaran);
					}
					else {
						PembayaranDP = 0;
						console.log("else calculate >>>>>");
					}
					TotalDP += PembayaranDP;
					$scope.PemSwap = TotalDP;
					console.log("Total DP >>>>",PembayaranDP);
					console.log("Sisah Pembayaran >>>>>>", obj.SisaPembayaran);
				});	
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemSwap = TotalDP;
				});
			}
			var BiayaBankD = 0; 
			var BiayaBankK = 0;
			var BiayaD = 0;
			var BiayaK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			console.log("blur");
			console.log("isi nominal >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					console.log("masuk sini >>>>", obj.SisaPembayaran);
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
						console.log(obj.SisaPembayaran);
						console.log("isi nominal 2 >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
						console.log("if calculate >>>>>", obj.SisaPembayaran);
					}
					else {
						PembayaranDP = 0;
						console.log("else calculate >>>>>");
					}
					TotalDP += PembayaranDP;
					$scope.PemOPD = TotalDP;
					console.log("Total DP >>>>",PembayaranDP);
					console.log("Sisah Pembayaran >>>>>>", obj.SisaPembayaran);
				});	
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemOPD = TotalDP;
				});
			}
			var BiayaBankD = 0; 
			var BiayaBankK = 0;
			var BiayaD = 0;
			var BiayaK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemPJ = TotalDP;
				});
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemPJ = TotalDP;
				});
			}
			var BiayaBankD = 0; 
			var BiayaBankK = 0; 
			var BiayaD = 0;
			var BiayaK = 0;
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =	
			parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.Nominal != null) {
					PembayaranDP = obj.Nominal;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
				$scope.KuitansiPem = TotalDP;
				$scope.SisaAR += obj.SisaAR;
			});

			var tmpTotal = 0;
			var tmpTotalD = 0;
			var tmpTotalK = 0;
			var BiayaK = 0;
			var BiayaD = 0;
			tmpTotal = TotalDP; 

			for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					tmpTotalK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				}
				else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					tmpTotalD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// TotPerhitungan;
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}
			}

			var tmpSelisih = tmpTotal - parseInt($scope.SisaAR) - parseInt(tmpTotalK) + parseInt(tmpTotalD) + BiayaD- BiayaK;
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});

			// ===========================
			var tmpTotal = 0;
			var tmpTotalD = 0;
			var tmpTotalK = 0;
			var BiayaK = 0;
			var BiayaD = 0;
			tmpTotal = TotalDP;
			// tmpTotal = $scope.gridApi_InformasiPembayaran.grid.columns[10].getAggregationValue();
			for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
				console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
				
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					tmpTotalK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				  	// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
			
				}
				else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					tmpTotalD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				  	// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
				}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// TotPerhitungan;
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}
			}
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpTotal - parseInt(tmpTotalK) + parseInt(tmpTotalD) + BiayaD- BiayaK;
				console.log("tmpSelisih",tmpSelisih);
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
				
			// ===========================
			// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
			// 	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			// START TAMBAHAN IRFAN
			var TotKredit = 0;
			var TotDebet = 0;
			var TotTip = 0;
			var TotOI = 0;
			var TotOE = 0
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					// if (obj.SisaPembayaran != null) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.TotalDPX = TotalDP;
				});
				console.log("Total Saldo 2 >>>>>>", PembayaranDP);
				
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
					}
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						TotDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// TotPerhitungan;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotTip += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					}
				}
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP - parseInt(TotKredit) + parseInt(TotDebet) - parseInt(TotOI) + parseInt(TotOE);
					
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;

			}else if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					// if (obj.SisaPembayaran != null) {
					if ($scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue() != null) {
						PembayaranDP = $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue();
					}
					else {
						PembayaranDP = 0;
					}
					console.log("TOTAL DP>>>>>>",TotalDP);
					
				});
				TotalDP = PembayaranDP;
				$scope.TotalDPXX = TotalDP;
				console.log("Total Saldo 1 >>>>>>", $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue());
				console.log("selisih>>>>>>>", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
					}
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						TotDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// TotPerhitungan;
					}
				}
				
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP - parseInt(TotKredit) + parseInt(TotDebet);
				console.log("tmpSelisih",tmpSelisih);
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
			}
			// END TAMBAHAN IRFAN
		}
		$scope.formatCurrency();
		if ($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0) {
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
		}
	}	

	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur = function () {
		var TotalBookingFee = 0;
		var TotalDP = 0;
		var PembayaranDP = 0;
		$scope.TotalDPUnitFP = 0;
		$scope.KuitansiPem = 0;
		$scope.PemOPD = 0;
		$scope.PemSwap = 0;
		$scope.SisaAR = 0;
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.forEach(function (obj) {
				TotalBookingFee += obj.NominalBookingFee;
			});
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalBookingFee;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				$scope.TotalDPUnitFP += PembayaranDP;
				
			});
			var BiayaBankD = 0; 
			var BiayaBankK = 0;
			var BiayaD = 0;
			var BiayaK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - $scope.TotalDPUnitFP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
			
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
				if (obj.SisaPembayaran != null) {
					PembayaranDP = obj.SisaPembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
				$scope.PemSwap = TotalDP
			});
			
			var BiayaBankD = 0; 
			var BiayaBankK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}
				}
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD);
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			console.log("blur");
			console.log("isi nominal >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					console.log("masuk sini >>>>", obj.SisaPembayaran);
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
						console.log(obj.SisaPembayaran);
						console.log("isi nominal 2 >>>>", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
						console.log("if calculate >>>>>", obj.SisaPembayaran);
					}
					else {
						PembayaranDP = 0;
						console.log("else calculate >>>>>");
					}
					TotalDP += PembayaranDP;
					$scope.PemOPD = TotalDP;
					console.log("Total DP >>>>",PembayaranDP);
					console.log("Sisah Pembayaran >>>>>>", obj.SisaPembayaran);
				});	
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemOPD = TotalDP;
				});
			}
			var BiayaBankD = 0; 
			var BiayaBankK = 0;
			var BiayaD = 0;
			var BiayaK = 0; 
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =
				parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemPJ = TotalDP;
				});
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.PemPJ = TotalDP;
				});
			}
			var BiayaBankD = 0; 
			var BiayaBankK = 0; 
			var BiayaD = 0;
			var BiayaK = 0;
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						BiayaBankK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				 	}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
					}
				}
			}
			var tmpSelisih =	
			parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalDP - parseInt(BiayaBankK) + parseInt(BiayaBankD) + BiayaD - BiayaK;
			if(tmpSelisih < 0){
				tmpSelisih = 0;
			}
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.Nominal != null) {
					PembayaranDP = obj.Nominal;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
				$scope.KuitansiPem = TotalDP;
				$scope.SisaAR += obj.SisaAR;
			});

			var tmpTotal = 0;
			var tmpTotalD = 0;
			var tmpTotalK = 0;
			var BiayaK = 0;
			var BiayaD = 0;
			tmpTotal = TotalDP; 

			for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					tmpTotalK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				}
				else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					tmpTotalD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// TotPerhitungan;
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}
			}

			var tmpSelisih = tmpTotal - parseInt($scope.SisaAR) - parseInt(tmpTotalK) + parseInt(tmpTotalD) + BiayaD- BiayaK;
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data.forEach(function (obj) {
				if (obj.Pembayaran != null) {
					PembayaranDP = obj.Pembayaran;
				}
				else {
					PembayaranDP = 0;
				}
				TotalDP += PembayaranDP;
			});

			// ===========================
			var tmpTotal = 0;
			var tmpTotalD = 0;
			var tmpTotalK = 0;
			var BiayaK = 0;
			var BiayaD = 0;
			tmpTotal = TotalDP;
			// tmpTotal = $scope.gridApi_InformasiPembayaran.grid.columns[10].getAggregationValue();
			for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
				console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
				
				if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
				   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					tmpTotalK = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				  	// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
			
				}
				else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					tmpTotalD = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
				  	// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
				}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// TotPerhitungan;
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);		
				}
			}
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpTotal - parseInt(tmpTotalK) + parseInt(tmpTotalD) + BiayaD- BiayaK;
				console.log("tmpSelisih",tmpSelisih);
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
				
			// ===========================
			// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
			// 	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			// START TAMBAHAN IRFAN
			var TotKredit = 0;
			var TotDebet = 0;
			var TotTip = 0;
			var TotOI = 0;
			var TotOE = 0
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
					// if (obj.SisaPembayaran != null) {
					if (obj.SisaPembayaran != null) {
						PembayaranDP = obj.SisaPembayaran;
					}
					else {
						PembayaranDP = 0;
					}
					TotalDP += PembayaranDP;
					$scope.TotalDPX = TotalDP;
				});
				console.log("Total Saldo 2 >>>>>>", PembayaranDP);
				
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
					}
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						TotDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// TotPerhitungan;
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotTip += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						TotOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
					}
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TotOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					}
				}
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP - parseInt(TotKredit) + parseInt(TotDebet) - parseInt(TotOI) + parseInt(TotOE);
					
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;

			}else if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
					// if (obj.SisaPembayaran != null) {
					if ($scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue() != null) {
						PembayaranDP = $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue();
					}
					else {
						PembayaranDP = 0;
					}
					console.log("TOTAL DP>>>>>>",TotalDP);
					
				});
				TotalDP = PembayaranDP;
				$scope.TotalDPXX = TotalDP;
				console.log("Total Saldo 1 >>>>>>", $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue());
				console.log("selisih>>>>>>>", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
						TotKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
							// console.log("TOTAL HITUNG KREDIT", TotPerhitungan);
					}
					else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						TotDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						// console.log("TOTAL HITUNG DEBIT", TotPerhitungan);
					}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
						// TotPerhitungan;
					}
				}
				
				var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP - parseInt(TotKredit) + parseInt(TotDebet);
				console.log("tmpSelisih",tmpSelisih);
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
			}
			// END TAMBAHAN IRFAN
		}

		if ($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0) {
			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
		}
	}
	$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_Change = function(item){
		NominalPembayaran = item;
	}

	$scope.DefaultRincianPembayaran = 0;
	$scope.TambahIncomingPayment_Calculate_RincianLain = function (isDeleteRowBiaya) {
		if (isDeleteRowBiaya == '' || isDeleteRowBiaya == null) {
			isDeleteRowBiaya = false;
		}
		else {
			isDeleteRowBiaya = true;
		}

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Service') >= 0 || $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Parts') >= 0) {
			var totalBiaya = 0;
			var saldoUnitFP = 0;
			var TotalDP = 0;
			var tmpKelebihan = 0;
			var tmpKBiaya = 0;
			console.log("Cek biaya>>>>>",$scope.tmpObj)
			for (var i in $scope.tmpObj){
				if(($scope.tmpObj[i].BiayaId == "Bea Materai" || $scope.tmpObj[i].BiayaId == "Biaya Bank") && $scope.tmpObj[i].DebitCredit == 'Kredit'){
					totalBiaya = totalBiaya + parseInt($scope.tmpObj[i].Nominal);
				}else if($scope.tmpObj[i].BiayaId == "Biaya Bank" && $scope.tmpObj[i].DebitCredit == 'Debet'){
					for (var j in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[j].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[j].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[j].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[j].DebitCredit == 'Kredit') ){
								// tmpTotal += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								tmpKBiaya += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[j].Nominal);
								console.log("TOTAL HITUNG KREDIT", tmpKBiaya);
						
						}
					}
					if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment"){
						if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
							$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
								if (obj.SisaPembayaran != null) {
									PembayaranDP = obj.SisaPembayaran;
								}
								else {
									PembayaranDP = 0;
								}
								TotalDP += PembayaranDP;
							});
							tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - parseInt(TotalDP)- tmpKBiaya + parseInt($scope.tmpObj[i].Nominal); 
							if(tmpKelebihan < 0){
								tmpKelebihan = 0;
							}
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;	
						}else{
							$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
								if (obj.SisaPembayaran != null) {
									PembayaranDP = obj.SisaPembayaran;
								}
								else {
									PembayaranDP = 0;
								}
								TotalDP += PembayaranDP;
							});
							tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - parseInt(TotalDP)- tmpKBiaya + parseInt($scope.tmpObj[i].Nominal); 
							if(tmpKelebihan < 0){
								tmpKelebihan = 0;
							}
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;
						}
					}else{
						tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.TotalPembayaranAll - tmpKBiaya + parseInt($scope.tmpObj[i].Nominal); 
						console.log("selisih saldo ini dia yahhh", tmpKelebihan);
						if(tmpKelebihan < 0){
							tmpKelebihan = 0;
						}
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;
					}
				}
				
			}
			// angular.forEach($scope.TambahIncomingPayment_Biaya_UIGrid.data, function (value, key) {
			// 	if ((value.BiayaId == "Bea Materai" || value.BiayaId == "Biaya Bank") && value.DebitCredit == 'Kredit') {
			// 		totalBiaya = totalBiaya + parseInt(value.Nominal);
				
			// 		// console.log("TOTAL BIAYA",totalBiaya);
			// 		// if(isDeleteRowBiaya){
			// 		// 	if ($scope.indexDelete == key){
			// 		// 		totalBiaya = totalBiaya - parseInt(value.Nominal);
			// 		// 	}
			// 		// }else{
			// 		// 	totalBiaya = totalBiaya + parseInt(value.Nominal);
			// 		// } 
			// 	}
			// 	else if(value.BiayaId == "Biaya Bank" && value.DebitCredit == 'Debet'){
			// 		// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer += parseInt(value.Nominal); 
			// 		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment"){
			// 			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
			// 				$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
			// 					// if (obj.SisaPembayaran != null) {
			// 					if (obj.SisaPembayaran != null) {
			// 						PembayaranDP = obj.SisaPembayaran;
			// 					}
			// 					else {
			// 						PembayaranDP = 0;
			// 					}
			// 					TotalDP += PembayaranDP;
			// 				});
			// 				// for(var i in $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data){
			// 				// 	 saldoUnitFP += $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data[i].SisaPembayaran;
			// 				// }
			// 				tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - parseInt(TotalDP)- totalBiaya + parseInt(value.Nominal); 
			// 				if(tmpKelebihan < 0){
			// 					tmpKelebihan = 0;
			// 				}
			// 				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;	
			// 			}else{
			// 				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function (obj) {
			// 					// if (obj.SisaPembayaran != null) {
			// 					if (obj.SisaPembayaran != null) {
			// 						PembayaranDP = obj.SisaPembayaran;
			// 					}
			// 					else {
			// 						PembayaranDP = 0;
			// 					}
			// 					TotalDP += PembayaranDP;
			// 				});
			// 				// for(var i in $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data){
			// 				// 	 saldoUnitFP += $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data[i].SisaPembayaran;
			// 				// }
			// 				tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - parseInt(TotalDP)- totalBiaya + parseInt(value.Nominal); 
			// 				if(tmpKelebihan < 0){
			// 					tmpKelebihan = 0;
			// 				}
			// 				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;
			// 			}
			// 		}else{
			// 			tmpKelebihan = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.TotalPembayaranAll - totalBiaya + parseInt(value.Nominal); 
			// 			console.log("selisih saldo ini dia yahhh", tmpKelebihan);
			// 			if(tmpKelebihan < 0){
			// 				tmpKelebihan = 0;
			// 			}
			// 			$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpKelebihan;
			// 		}
			// 	}
			// });

			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				if (isDeleteRowBiaya) {
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.DefaultRincianPembayaran + totalBiaya;
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
						
					// var tmpTotal = 0;
					// tmpTotal = TotalDP;
					if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment"){
						if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
							tmpTotal = $scope.TotalDPX;
						}else if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
							tmpTotal = $scope.TotalDPXX;
						}
					}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
						tmpTotal = $scope.KuitansiPem;
					}else{
						tmpTotal = $scope.TotalPembayaranAll;
					}
					
					var tmpGridKreditDebit = 0
					var tmpGridD = 0;
					var flagK = 0;
					var tmpPL = 0;
					var tmpBL = 0;
					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						if($scope.indexDelete == i){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit'){
								flagK = 1;
							}else{
								flagK = 0;
							}
						}
							if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								// tmpTotal += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								tmpGridKreditDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG KREDIT", tmpGridKreditDebit);
						
							}
							else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								tmpGridD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG DEBIT", tmpGridKreditDebit);
							}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								// TotPerhitungan;
							}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								tmpPL += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
								// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
								tmpBL += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
								// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							}
						// }	
					}
					var copyNomBT = angular.copy($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
					var tmpSelisih = 0;
					if (flagK == 0){
						// yg di hapus debet
						
						if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment"){
							if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
								if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Biaya Lain-lain"){
									tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit + tmpGridD;
								}else{
									tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit - tmpGridD;
								}
								// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = NominalPembayaran + tmpGridKreditDebit;
							}else if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
								if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Biaya Lain-lain"){
									tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit + tmpGridD;
								}else{
									tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit - tmpGridD;
								}
								// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = NominalPembayaran + tmpGridKreditDebit;
							}
						}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)"){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Biaya Lain-lain"){
								tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit + tmpGridD;
							}else{
								tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit - tmpGridD;
							}
							// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = NominalPembayaran + tmpGridKreditDebit;
						}else{
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Biaya Lain-lain"){
								tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit + tmpGridD;
							}else{
								tmpSelisih = copyNomBT - tmpTotal - tmpGridKreditDebit - tmpGridD;
							}
							// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = NominalPembayaran + tmpGridKreditDebit;
						}
					} else {
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Pendapatan Lain-lain"){
							tmpSelisih = copyNomBT - tmpTotal  - tmpGridKreditDebit + tmpGridD;
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "");				
						}else{
							tmpSelisih = copyNomBT - tmpTotal  - tmpGridKreditDebit + tmpGridD - tmpPL;
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpGridKreditDebit;					
						}
					}
					// var tmpSelisih = copyNomBT.toString().replace(/,/g, "") - tmpTotal - tmpGridKreditDebit + tmpGridD;
					if(tmpSelisih < 0){
						tmpSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
						
						console.log("TOTAL BIAYA BT IF",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				}	
				else {
					if ($scope.DefaultRincianPembayaran == 0) {
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
						console.log("TOTAL BIAAYA BT ELSE", $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
					}else if($scope.DefaultRincianPembayaran > 0){
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
					}
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.DefaultRincianPembayaran + totalBiaya;
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}

			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
				if (isDeleteRowBiaya) {
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = $scope.DefaultRincianPembayaran + totalBiaya;
					// var tmpGridKreditDebit = 0
					// for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					// 	console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						
					// 	if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
					// 	($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
					// 		tmpTotal += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		tmpGridKreditDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		console.log("TOTAL HITUNG KREDIT", tmpGridKreditDebit);
					
					// 	}
					// 	else if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// 		tmpTotal -= parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 		console.log("TOTAL HITUNG DEBIT", tmpGridKreditDebit);
					// 	}else if(($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
					// 		// TotPerhitungan;
					// 	}
					// }
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = NominalPembayaran + tmpGridKreditDebit;

					
					var tmpGridKreditDebit = 0
					var tmpGridD = 0;
					var flagK = 0;
					var tmpPL = 0;
					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						if($scope.indexDelete == i){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit'){
								flagK = 1;
							}else{
								flagK = 0;
							}
						}
							if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								// tmpTotal += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								tmpGridKreditDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG KREDIT", tmpGridKreditDebit);
						
							}
							else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								tmpGridD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG DEBIT", tmpGridKreditDebit);
							}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								// TotPerhitungan;
							}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								tmpPL += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
								// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							}
						// }	
					}
					var copyNomBT = angular.copy($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
					var tmpSelisih = 0;
					if (flagK == 0){

					}else{
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Pendapatan Lain-lain"){
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, "");				
						}else{
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, "") - tmpGridKreditDebit;					
						}
					}

				}
				else {
					if ($scope.DefaultRincianPembayaran == 0) {
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash;
						// $scope.DefaultRincianPembayaran = NominalPembayaran;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
					}else if($scope.DefaultRincianPembayaran > 0){
						// var tmpCopyCash = $scope.DefaultRincianPembayaran;
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash;
						// $scope.DefaultRincianPembayaran = NominalPembayaran;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
					}
				}
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
				//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [10] | Calculate Biaya Lain2 Kredit

			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
				if (isDeleteRowBiaya) {
					// $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.DefaultRincianPembayaran + totalBiaya;
					var tmpGridKreditDebit = 0
					var tmpGridD = 0;
					var flagK = 0;
					var tmpPL = 0;
					for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
						console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						if($scope.indexDelete == i){
							if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit'){
								flagK = 1;
							}else{
								flagK = 0;
							}
						}
							if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
							($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
								// tmpTotal += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								tmpGridKreditDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG KREDIT", tmpGridKreditDebit);
						
							}
							else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								tmpGridD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
								console.log("TOTAL HITUNG DEBIT", tmpGridKreditDebit);
							}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
								// TotPerhitungan;
							}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
								tmpPL += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
								// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer - parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
							}
						// }	
					}
					var copyNomBT = angular.copy($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);
					var tmpSelisih = 0;
					if (flagK == 0){

					}else{
						if($scope.TambahIncomingPayment_Biaya_UIGrid.data[$scope.indexDelete].BiayaId == "Pendapatan Lain-lain"){
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, "");				
						}else{
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, "") - tmpGridKreditDebit;					
						}
					}
				}
				else {
					if ($scope.DefaultRincianPembayaran == 0) {
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
					}else if($scope.DefaultRincianPembayaran > 0){
						$scope.DefaultRincianPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard;
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = parseInt($scope.DefaultRincianPembayaran) + totalBiaya;
					}
				}
			}
			console.log('Calculate RincianLain : ' + totalBiaya);
		}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit - Full Payment') >= 0)){
			console.log("hapus masuk ke sini >>>>>>>>>")
			if(isDeleteRowBiaya == true){
				// if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				// 	tmpTotalDPUnitFP = $scope.TotalDPUnitFP;
				// 	var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpTotalDPUnitFP;
				// 	if(tmpSelisih < 0){
				// 		tmpSelisih = 0;
				// 	}
				// 	$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
						
				// 	console.log("TOTAL BIAYA BT Unit FP",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
				// }
				var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.deletedRowBiayaId == "Biaya Lain-lain" || ($scope.deletedRowBiayaId == "Biaya Bank" && $scope.deletedRowDebitCredit == "Debet" )){
						tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
						$scope.TotalBiayaBankOE = $scope.TotalBiayaBankOE - $scope.deletedRowNominal;
					}
					else if($scope.deletedRowBiayaId == "Pendapatan Lain-lain"){
						if ( $scope.deletedRowNominal <= (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemUFP) + $scope.TotalBiayaBankOE)){
							tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}else{
							tmpSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemUFP) + $scope.TotalBiayaBankOE ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}
							
					}
				}
				var PembayaranDP = 0;
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function(obj){
					if(obj.Pembayaran != null){
						PembayaranDP += obj.Pembayaran;
					}else{
						PembayaranDP = 0;
					}
					$scope.PemUFP = PembayaranDP;
				});
				var TotalPembayaran = $scope.PemUFP
				console.log('cek agregation value', TotalPembayaran)
				$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
			}
		}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit - Payment (Kuitansi Penagihan)') >= 0)){
			if(isDeleteRowBiaya == true){
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					tmpTotalDPUnitFP = $scope.KuitansiPem;
					var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpTotalDPUnitFP;
					if(tmpSelisih < 0){
						tmpSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
						
					console.log("TOTAL BIAYA BT Unit FP",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
				}
			}
		}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit - Order Pengurusan Dokumen') >= 0)){
			if(isDeleteRowBiaya == true){
				// if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
				// 	tmpTotalOPD = $scope.PemOPD;
				// 	var tmpSelisih = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - tmpTotalOPD;
				// 	if(tmpSelisih < 0){
				// 		tmpSelisih = 0;
				// 	}
				// 	$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
						
				// 	console.log("TOTAL BIAYA BT Unit OPD",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
				
				// }

				//new 
				var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.deletedRowBiayaId == "Biaya Lain-lain" || ($scope.deletedRowBiayaId == "Biaya Bank" && $scope.deletedRowDebitCredit == "Debet" )){
						tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
						$scope.TotalBiayaBankOE = $scope.TotalBiayaBankOE - $scope.deletedRowNominal;
					}
					else if($scope.deletedRowBiayaId == "Pendapatan Lain-lain"){
						if ( $scope.deletedRowNominal <= (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemOPD) + $scope.TotalBiayaBankOE)){
							tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}else{
							tmpSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemOPD) + $scope.TotalBiayaBankOE ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}
							
					}
				}
				var TotalPemSO = 0;
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
					if(obj.SisaPembayaran != null){
						TotalPemSO += obj.SisaPembayaran;
					}else{
						TotalPemSO = 0;
					}
					$scope.PemOPD = TotalPemSO;
				})
				var TotalPembayaran = $scope.PemOPD
				console.log('cek agregation value', TotalPembayaran)
				$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
	
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
					
				console.log("TOTAL BIAYA BT Unit OPD",$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			}
		}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit - Swapping') >= 0)){
			if(isDeleteRowBiaya == true){
				var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.deletedRowBiayaId == "Biaya Lain-lain" || ($scope.deletedRowBiayaId == "Biaya Bank" && $scope.deletedRowDebitCredit == "Debet" )){
						tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
						$scope.TotalBiayaBankOE = $scope.TotalBiayaBankOE - $scope.deletedRowNominal;
					}
					else if($scope.deletedRowBiayaId == "Pendapatan Lain-lain"){
						if ( $scope.deletedRowNominal <= (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemSwap) + $scope.TotalBiayaBankOE)){
							tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}else{
							tmpSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemSwap) + $scope.TotalBiayaBankOE ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}
							
					}
				}
				var TotalPemSO = 0;
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
					if(obj.SisaPembayaran != null){
						TotalPemSO += obj.SisaPembayaran;
					}else{
						TotalPemSO = 0;
					}
					$scope.PemSwap = TotalPemSO;
				})
				var TotalPembayaran = $scope.PemSwap
				console.log('cek agregation value', TotalPembayaran)
				$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
	
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
					
				console.log("TOTAL BIAYA BT Unit OPD",$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			}
		}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit - Purna Jual') >= 0)){
			//begin ubah kalkulasi selisih tipe unit dari OI OE
			if(isDeleteRowBiaya == true){
				var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
				if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					if($scope.deletedRowBiayaId == "Biaya Lain-lain" || ($scope.deletedRowBiayaId == "Biaya Bank" && $scope.deletedRowDebitCredit == "Debet" )){
						tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
						$scope.TotalBiayaBankOE = $scope.TotalBiayaBankOE - $scope.deletedRowNominal;
					}
					else if($scope.deletedRowBiayaId == "Pendapatan Lain-lain"){
						if ( $scope.deletedRowNominal <= (($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemPJ) + $scope.TotalBiayaBankOE)){
							tmpSelisih = tmpSelisih - $scope.deletedRowNominal ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}else{
							tmpSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemPJ) + $scope.TotalBiayaBankOE ;
							$scope.TotalBiayaOI = $scope.TotalBiayaOI - $scope.deletedRowNominal;
						}
							
					}
				}
				var TotalPemSO = 0;
				$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
					if(obj.SisaPembayaran != null){
						TotalPemSO += obj.SisaPembayaran;
					}else{
						TotalPemSO = 0;
					}
					$scope.PemOPD = TotalPemSO;
				})
				var TotalPembayaran = $scope.PemOPD
				console.log('cek agregation value', TotalPembayaran)
				$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
	
				if(tmpSelisih < 0){
					tmpSelisih = 0;
				}
				$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
					
				console.log("TOTAL BIAYA BT Unit Purna Jual",$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			
			}
		}
	}

	$scope.TambahIncomingPayment_NoRekeningPenerima_Selected_Changed = function () {
		console.log($scope.TambahIncomingPayment_BankTransfer_RekeningPenerima);
		
		if ($scope.TambahIncomingPayment_BankTransfer_RekeningPenerima !== "") {
			IncomingPaymentFactory.getDataAccountBank($scope.TambahIncomingPayment_BankTransfer_RekeningPenerima)
				.then(
					function (res) {
						console.log("No")
						$scope.TambahIncomingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = res.data.Result[0].AccountBankName;
						$scope.TambahIncomingPayment_RincianPembayaran_NamaPemegangRekening_BankTransfer = res.data.Result[0].AccountName;
					}
				);
		}
	}

	$scope.TambahIncomingPayment_TujuanPembayaran_CashDelivery_Selected_Changed = function () {
		console.log("TambahIncomingPayment_TujuanPembayaran_CashDelivery_Selected_Changed");
		console.log($scope.TujuanPembayaran_CashDelivery);
		if ($scope.TujuanPembayaran_CashDelivery !== "") {
			console.log($scope.TujuanPembayaran_CashDelivery);
			IncomingPaymentFactory.getDataAccountBank($scope.TujuanPembayaran_CashDelivery)
				.then
				(
				function (res) {
					console.log("No")
					$scope.TambahIncomingPayment_CashDelivery_NamaBank = res.data.Result[0].AccountBankName;
					$scope.TambahIncomingPayment_CashDelivery_NamaRekening = res.data.Result[0].AccountName;
				}
				);
		}
	}

	$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed = function () {
		// DisableAllMetodePembayaranDetails();
		// console.log("isi data field grid", $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid);
		// console.log("data di grid biaaya lain", $scope.TambahIncomingPayment_Biaya_UIGrid);
		// console.log("data Di grid Daftar SO UDP",$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid);
		$scope.ShowPengajuan = false;
		$scope.GetDataPreprintedSetting();
		$scope.clearCharges();
		console.log("Cek Data kuitansi", $scope.PreprintedSettingData);

		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran != "Bank Transfer"){
			if($scope.newValKui > $scope.SisaAR){
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
						type: 'warning'
					}
				);  
				$scope.ProsesSimpan = false;
			}else if($scope.newValKui <= 0){ 
				bsNotify.show(
					{
						title: "Peringatan",
						content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
						type: 'warning'
					}
				);      
				$scope.ProsesSimpan = false;
			}
		}

		var sTipe = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		var isPreprintedReceipt = false;
		angular.forEach($scope.PreprintedSettingData, function (value, key) {
			if (!isPreprintedReceipt) {
				if (value.BusinessType.indexOf('Unit') == 0
					&& value.isPreprintedReceipt
					&& sTipe.indexOf('Unit') == 0
				) {
					if (sTipe.indexOf('Proforma') >= 0) {
						HideKuitansi();
					}
					else {
						ShowKuitansi();
						isPreprintedReceipt = true;
						$scope.getDataPreprintedCB();
					}
				}
				else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else {
					HideKuitansi();
					// $scope.TambahIncomingPayment_KetKuitansi = true;
				}
			}

			if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
				if (sTipe.indexOf('Proforma') == -1) {
					$scope.TambahIncomingPayment_KetKuitansi = true;
				}
			}
			else {
				$scope.TambahIncomingPayment_KetKuitansi = false;
			}
	
			if (sTipe == "Interbranch - Penerima") {
				ShowTTUS();
			}
			else if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
				if (sTipe.indexOf('Proforma') == -1) {
					ShowSimpanCetakKuitansi();
				}
			}
		$scope.TambahIncomingPayment_SetupBiaya();
		
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment"){
			// $scope.cekPrintedKuitansii();
		}
	
		});

		console.log("Nominal Pembayaran BT",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
		console.log("Nominal Pembayaran CC",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash);
		console.log("Nominal Pembayaran CD",$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard);

		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CashOperation();
		HideMetodePembayaranDetails_CreditDebitCard();
		$scope.DefaultRincianPembayaran = 0;

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "" || angular.isUndefined($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)) {
			return false;
		}

		if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
			//$scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			$scope.getDataBank();
			//$scope.TambahIncomingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions[0];

			//$scope.TambahIncomingPayment_BankTransfer_RekeningPenerimaOptions = [{ name: "rek1", value: "rek1" }, { name: "rek2", value: "rek2" }, { name: "rek3", value: "rek3" }];
			$scope.getDataBankAccount();
			$scope.getDataCheque();

			//$scope.TambahIncomingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = $scope.TambahIncomingPayment_BankTransfer_RekeningPenerimaOptions[0];

			ShowMetodePembayaranDetails_BankTransfer();

			//convert to global date
			$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer = $filter('date')(new Date(), 'yyyy-MM-dd');

			if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment")
			) {
				console.log("a");
				if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" || $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual")
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
					$scope.LoopingBiaya();
					var TotalPembayaran = $scope.gridApi_DaftarSalesOrderDibayar_UIGrid.grid.columns[6].getAggregationValue();
					console.log('cek agregation value', TotalPembayaran)
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.tmpKredit) - parseInt($scope.tmpDebit);
					var tmpSelisih =
					parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - TotalPembayaran -  parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
					if(tmpSelisih < 0){
						tmpSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
				} 
				ShowMetodePembayaranDetails_Selisih_BankTransfer();
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4b = false;
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4 = true;
				$scope.KetReqShow = true;
				$scope.KetNotReqShow = false;
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
				console.log("Card");
				HideMetodePembayaranDetails_Selisih_BankTransfer();
				HideMetodePembayaranDetails_Tanggal_BankTransfer();
				//$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer = $filter('date')(new Date(), 'yyyy-MM-dd');
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4 = false;
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4b = true;
				$scope.KetReqShow = false;
				$scope.KetNotReqShow = true;
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [];
			}
			else if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown")
				|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card")) {
				console.log("b");
				HideMetodePembayaranDetails_Selisih_BankTransfer();
				//HideMetodePembayaranDetails_Tanggal_BankTransfer();
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4b = false;
				$scope.Show_TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4 = true;
				$scope.KetReqShow = false;
				$scope.KetNotReqShow = true;
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [];

				if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
					if ($scope.TambahIncomingPayment_Biaya == "") {
						$scope.TambahIncomingPayment_Biaya = 0;
					}
					var ARBank = parseFloat($scope.currentNominal) - parseFloat($scope.TambahIncomingPayment_Biaya);
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = ARBank;
				}
			}

			//hilman,begin
			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = $scope.TotalBayarSum;
			}
			//hilman,end
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
			ShowMetodePembayaranDetails_Cash();
			$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash = new Date();

			//hilman,begin
			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = $scope.TotalBayarSum;
			}
			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }];
			//hilman,end
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
			//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [11] | Show Function & Jenis Biaya Lain2
			ShowMetodePembayaranDetails_CashOperation();
			$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation = $filter('date')(new Date(), 'yyyy-MM-dd');
			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" },, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }];
			$scope.getCashOperationOutgoing(1, $scope.uiGridPageSize);
		}
		else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
			//20170414, eric, begin
			//$scope.TambahIncomingPayment_CreditDebitCard_TipeKartuOptions = [{ name: "Credit Card", value: "1" }, { name: "Debit Card", value: "2" }];
			//20170414, eric, end
			//$scope.TambahIncomingPayment_RincianPembayaran_TipeKartu_CreditDebitCard = $scope.TambahIncomingPayment_CreditDebitCard_TipeKartuOptions[0];

			//$scope.TambahIncomingPayment_CreditDebitCard_NamaBankOptions = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			//$scope.getDataBank();
			$scope.TambahIncomingPayment_getDataEDC();
			ShowMetodePembayaranDetails_CreditDebitCard();
			$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard = new Date();
			//hilman,begin
			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
				$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = $scope.TotalBayarSum;
			}
			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }];
			//hilman,end
		}

		if ((($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment"))
			&& $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
		) {
			$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5 = true;
			$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5b = false;
		}
		else {
			$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5 = false;
			$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5b = true;
		}

		$scope.TambahIncomingPayment_SetupBiaya();
		$scope.CheckPengajuan();
	};

	$scope.Ready_Rincian_Pembayaran_Selected_None = function () {
		ShowTopMetodePembayaranDetails();//refresh tu dropdown metode pembayaran biar balik ke default
		$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash Collection", value: "Cash" }, { name: "Cash Operation", value: "Cash Operation" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }];
		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = {};
	};

	$scope.TambahIncomingPayment_TujuanPembayaran_SearchBy_ValueChanged = function () {
		//alert($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy);
		console.log("TambahIncomingPayment_TujuanPembayaran_SearchBy_ValueChanged");
		DisableAll_SearchByRadioResult();//disable semua hasil search by radio

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSpk();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaSalesman();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSpk();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSpk();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			//begin | Noto | 180514 | 00197 UT
			Enable_TambahIncomingPayment_SearchBy_NamaLeasing();
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.TambahIncomingPayment_Top_NamaLeasing = '';
				$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.data = [];
				$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data = [];
				$scope.getDataLeasing();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_Top_NamaLeasing = '';
				$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.data = [];
				$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data = [];
				$scope.getDataInsurance();
			}
			//end | Noto | 180514 | 00197 UT
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelangganPt();
			}
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {
		// if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
		// Enable_TambahIncomingPayment_SearchBy_NomorSo();
		// }
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
		// Enable_TambahIncomingPayment_SearchBy_NamaPelangganPt();
		// }
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorAppointment();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NomorWo();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorWo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPembayar();
			}
		}
		//else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Service - Full Payment Asuransi")
		//{	
		//}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_SearchBy_NomorSo();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_SearchBy_NamaPelanggan();
			}
		}
		//else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Bank Statement - Unknown")
		//{	
		//}
		//else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Bank Statement - Card")
		//{		
		//}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			console.log("getDataBranchCB");
			$scope.getDataBranchCB();
		}
		Enable_SearchByCariBtn();//soal kalo radio diklik pasti nongol
		$scope.DefaultRincianPembayaran = 0;
	};

	$scope.LihatIncomingPayment_getDataEDC = function () {
		console.log("GetDataBank");
		var dataBank = [];
		IncomingPaymentFactory.getDataEDC()
			.then(
				function (res) {
					$scope.loading = false;
					angular.forEach(res.data.Result, function (value, key) {
						dataBank.push({ name: value.EDCName, value: value.EDCId });
					});
					$scope.optionLihatIncomingPayment_Top_NamaBank = dataBank;
					console.log($scope.optionLihatIncomingPayment_Top_NamaBank);
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	}

	$scope.TambahIncomingPayment_getDataEDC = function () {
		console.log("GetDataBank");
		var dataBank = [];
		IncomingPaymentFactory.getDataEDC()
			.then(
				function (res) {
					$scope.loading = false;
					angular.forEach(res.data.Result, function (value, key) {
						dataBank.push({ name: value.EDCName, value: value.EDCId });
					});

					$scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions = dataBank;
					$scope.TambahIncomingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = $scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions[0];
					$scope.TambahIncomingPayment_CreditDebitCard_NamaBankOptions = dataBank;
					$scope.TambahIncomingPayment_RincianPembayaran_NamaBank_CreditDebitCard = $scope.TambahIncomingPayment_CreditDebitCard_NamaBankOptions[0];
					$scope.optionsTambahIncomingPayment_SearchBy_NamaBank = dataBank;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	}






	// garapan 17juni

	$scope.TambahIncomingPayment_SetupBiaya = function () {
		console.log("Setup Biaya");
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }, { name: "Titipan", value: "Titipan" }] } //, { name: "Titipan", value: "Titipan" }
			else if ($scope.controlTitipanDP == false){
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }] // ,{ name: "Titipan", value: "Titipan" }	
			} 
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" } ,{ name: "Titipan", value: "Titipan" }] }; // ,{ name: "Titipan", value: "Titipan" }
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] } //,{ name: "Titipan", value: "Titipan" }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }; //,{ name: "Titipan", value: "Titipan" }
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { 
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }] //,{ name: "Titipan", value: "Titipan" }
				if ($scope.controlTitipanDP == false){
					$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]	//,{ name: "Titipan", value: "Titipan" }
				}else{
					$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]	//,{ name: "Titipan", value: "Titipan" }
				} 

			}
			else if ($scope.controlTitipanDP == false){
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]	//,{ name: "Titipan", value: "Titipan" }
			}
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }] //,{ name: "Titipan", value: "Titipan" }
		};
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;	
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = true;	
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;	
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = true;	
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }, { name: "Titipan", value: "Titipan" }] } //, { name: "Titipan", value: "Titipan" }
			// else if ($scope.controlTitipanDP == false){
			// 	$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" },{ name: "Titipan", value: "Titipan" }]	
			// }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }, { name: "Titipan", value: "Titipan" }] }; //,{ name: "Titipan", value: "Titipan" }
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;	
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = true;	
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
			}else if ($scope.selectTipeBiaya == 'Biaya Lain-lain'){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;	
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = true;	
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown") {
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			Disable_TambahIncomingPayment_BiayaBiaya();
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			Disable_TambahIncomingPayment_BiayaBiaya();
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {

			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Cash Delivery") {
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			Disable_TambahIncomingPayment_BiayaBiaya();

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }] };
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "PPN Keluaran", value: "PPN Keluaran" }, { name: "Biaya Lain-lain", value: "Biaya Lain-lain" }, { name: "Pendapatan Lain-lain", value: "Pendapatan Lain-lain" }];
			if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' || $scope.selectTipeBiaya == 'Titipan' ){
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			}else{
				$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";	
			}
			// $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
	}

	$scope.TambahIncomingPayment_RincianPembayaran_TipeIncomingPayment_Selected_Changed = function (selectedTypeIncoming) {
		//begin | Noto | 180514 | 00186 UT
		//HideTopMetodePembayaranDetails();//ini paymentdetail top nya (dropdown)
		//end | Noto | 180514 | 00186 UT

		// if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment"){
		// 	$scope.Mandatory = true;
		// }else{
		// 	$scope.Mandatory = false;
		// }
		// console.log("Cek Mandatory woooyyyyyyyy>>>>>>>>>>>>>>>>",$scope.Mandatory)
		$scope.GetDataPreprintedSetting();
		$scope.selectedTypeIncoming = selectedTypeIncoming;
		console.log('$scope.selectedTypeIncoming',$scope.selectedTypeIncoming)
		
		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CashOperation();
		HideMetodePembayaranDetails_CreditDebitCard();
		DisableAll_TambahIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_TambahIncomingPayment_InformasiPembayaran();//ini semua info pembayaran di hide
		DisableAll_TambahIncomingPayment_DaftarSpk();//ini semua ilangin daftar spk
		DisableAll_TambahIncomingPayment_SalesOrder();//ini ilangin semua sales order
		DisableAll_TambahIncomingPayment_DaftarSalesOrderRadio();
		Disable_TambahIncomingPayment_KumpulanNomorRangka();
		Disable_TambahIncomingPayment_KumpulanWorkOrder();
		Disable_TambahIncomingPayment_DaftarArCreditDebit();
		Disable_TambahIncomingPayment_KumpulanARRefundLeasing();
		$scope.TambahIncomingPayment_DaftarSpk_Detail_Radio = "";
		$scope.TambahIncomingPayment_DaftarSpk_Brief_Radio = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill = "";
		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = null;
		$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = []
		$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.columnDefs = [];
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [];
		$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs = [];
		$scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data = [];
		$scope.ModalTambahIncoming_CustomerName.columnDefs = [];
		Enable_TambahIncomingPayment_SaveAndPrintButton();
		$scope.TambahIncomingPayment_ClearAllData();
		Enable_TambahIncomingPayment_BiayaBiaya();
		$scope.Cetak = "N";
		$scope.CetakTTUS = "N";
		ShowKuitansi();
		HideTTUS();
		$scope.DefaultRincianPembayaran = 0;
		$scope.currentProformaInvoiceId = null;// aaaaa

		//begin | Noto | 180514 | 00197 UT
		DisableAll_TambahIncomingPayment_SearchBy();
		//end | Noto | 180514 | 00197 UT

		$scope.ShowNominalPembayaran ();

		//Clear All Data
		$scope.TipeBaru = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		if ($scope.TipeLama == "")
			// $scope.TambahIncomingPayment_ClearAllData();
			$scope.$destroy;
		else if (($scope.TipeLama == "Unit - Down Payment") && ($scope.TipeBaru != "Unit - Full Payment"))
			// $scope.TambahIncomingPayment_ClearAllData();
			$scope.$destroy;
		else if (($scope.TipeLama == "Unit - Full Payment") && ($scope.TipeBaru != "Unit - Down Payment"))
			// $scope.TambahIncomingPayment_ClearAllData();
			$scope.$destroy;
		else
			// $scope.TambahIncomingPayment_ClearAllData();
			$scope.$destroy;


		$scope.TipeLama = $scope.TipeBaru;

		$scope.TambahIncomingPayment_TujuanPembayaran_SearchBy = "";//ini buat reset semua pilihan searchby
		DisableAll_SearchByRadioResult();//disable semua hasil search by radio

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "" || angular.isUndefined($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)) {
			$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Enabled = false;
			$scope.TambahIncomingPayment_ClearAllData();
			return false;
		}
		else {
			$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran_Enabled = true;
		}

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			$scope.selectGrid = false;
			$scope.Ready_Rincian_Pembayaran_Selected_None();

			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySpkSales();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = false;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {

			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySpkPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {

			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySpkPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelangganPt();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {

		// $scope.Ready_Rincian_Pembayaran_Selected_None();
		// Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelangganPt();
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchByAppointmentWo();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit_EnableDisable = true;

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_SearchBy_NamaPembayar();
			Enable_SearchByCariBtn();
			$scope.TambahIncomingPayment_TujuanPembayaran_SearchBy = "3";
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }];
			}
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			//hilman,end
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			//Enable_TambahIncomingPayment_TujuanPembayaran_SearchByWoPelanggan();
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment Asuransi") {

		// $scope.Ready_Rincian_Pembayaran_Selected_None();
		// Enable_TambahIncomingPayment_SearchBy_NamaAsuransi();
		// Enable_SearchByCariBtn();
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown") {
			$scope.selectGrid = false;
			HideKuitansi();
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			Disable_TambahIncomingPayment_BiayaBiaya();
			$scope.Ready_Rincian_Pembayaran_Selected_None();

			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
			//$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran={};
			//begin | Noto | 180515 | 00195 UT
			$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = "";
			//$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran.selectedTab = 'Bank Transfer';
			$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran.trigger('change');
			//end | Noto | 180515 | 00195 UT

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			$scope.selectGrid = true;
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }];
			//$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran={};
			//$scope.optionsTambahIncomingPayment_SearchBy_NamaBank = [{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
			//$scope.getDataBank();
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			//begin | Noto | 180515 | 00195 UT
			$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = "Bank Transfer";
			//end | Noto | 180515 | 00195 UT
			$scope.TambahIncomingPayment_getDataEDC();
			Enable_TambahIncomingPayment_DaftarArCreditDebit();
			Enable_TambahIncomingPayment_SearchBy_NamaBank();
			Enable_SearchByCariBtn();
			Disable_TambahIncomingPayment_BiayaBiaya();
			HideKuitansi();

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			$scope.selectGrid = false;
			Disable_TambahIncomingPayment_BiayaBiaya();
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			$scope.optionsMetodePembayaran = [{ name: "Cash Collection", value: "Cash" }, { name: "Credit/Debit Card", value: "Credit/Debit Card" }];
			//$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran={};
			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" },{ name: "Titipan", value: "Titipan" }];//,{ name: "Titipan", value: "Titipan" }
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Debet";
			Enable_TambahIncomingPayment_Interbranch_InformasiBranch();
			Enable_TambahIncomingPayment_InformasiPelangan_Lain();
			//nongolin info lain
			//Alvin, begin
			console.log("getDataBranchCB");
			$scope.getDataBranchCB();
			//Alvin, end
		}
		//20170509, nuse, begin
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Cash Delivery") {
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_CashDelivery();
			$scope.optionsMetodePembayaran = [{ name: "Cash Operation", value: "Cash" }];
			{ $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [] };
			$scope.getDataBankAccountCashDelivery();
			ShowTopMetodePembayaranDetails();
			//Enable_TambahIncomingPayment_SaveAndPrintButton();
			Disable_TambahIncomingPayment_BiayaBiaya();
			HideKuitansi();

		}
		//20170509, nuse, end		
		//20170527, alvin, begin		
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			//$scope.getDataVendor();
			$scope.selectGrid = false;
			$scope.Ready_Rincian_Pembayaran_Selected_None();
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Biaya Bank", value: "Biaya Bank" }] }
			else { $scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "Bea Materai", value: "Bea Materai" }] };
			Enable_TambahIncomingPayment_SearchBy_NomorProforma();
			Enable_SearchByCariBtn();
			// HideKuitansi();

		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			//$scope.getDataVendor();

			//begin | Noto | 180514 | 00197 UT
			$scope.selectGrid = false;
			Enable_TambahIncomingPayment_TujuanPembayaran_SearchByLeasingInsurance();
			//$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByLeasingInsurance = true;
			//end | Noto | 180514 | 00197 UT

			$scope.Ready_Rincian_Pembayaran_Selected_None();

			$scope.TambahIncomingPayment_BiayaBiaya_TipeBiayaOptions = [{ name: "PPN Keluaran", value: "PPN Keluaran" }];
			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash Operation", value: "Cash Operation" }];
			$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "Kredit";
			$scope.ShowSimpanCetakKuitansi = false;

			Disable_TambahIncomingPayment_BiayaBiaya();
			HideKuitansi();
			HideButtonSimpanCetakKuitansi();
		}
		//20170527, alvin, end
		//begin | Noto | Cek Preprinted Setting
		var sTipe = $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment;
		var isPreprintedReceipt = false;
		angular.forEach($scope.PreprintedSettingData, function (value, key) {
			if (!isPreprintedReceipt) {
				if (value.BusinessType.indexOf('Unit') == 0
					&& value.isPreprintedReceipt
					&& sTipe.indexOf('Unit') == 0
				) {
					if (sTipe.indexOf('Proforma') >= 0) {
						HideKuitansi();
					}
					else {
						ShowKuitansi();
						isPreprintedReceipt = true;
						$scope.getDataPreprintedCB();
					}
				}
				else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Service') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && sTipe.indexOf('Parts') == 0) {
					ShowKuitansi();
					isPreprintedReceipt = true;
					$scope.getDataPreprintedCB();
				}
				else {
					HideKuitansi();
				}
			}
		});

		if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
			if (sTipe.indexOf('Proforma') == -1) {
				$scope.TambahIncomingPayment_KetKuitansi = true;
			}
		}
		else {
			$scope.TambahIncomingPayment_KetKuitansi = false;
		}

		if (sTipe == "Interbranch - Penerima") {
			ShowTTUS();
		}
		else if (sTipe.indexOf('Unit') == 0 || sTipe.indexOf('Service') == 0 || sTipe.indexOf('Parts') == 0) {
			if (sTipe.indexOf('Proforma') == -1) {
				ShowSimpanCetakKuitansi();
			}
		}

		// 	var isPreprintedReceipt = false;
		// angular.forEach($scope.PreprintedSettingData, function (value, key) {
		// 	if (value.BusinessType.indexOf('Unit') == 0 && value.isPreprintedReceipt && $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Unit') == 0) {
		// 		isPreprintedReceipt = true;
		// 	}
		// 	else if (value.BusinessType.indexOf('Service') == 0 && value.isPreprintedReceipt && $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Service') == 0) {
		// 		isPreprintedReceipt = true;
		// 	}
		// 	else if (value.BusinessType.indexOf('Parts') == 0 && value.isPreprintedReceipt && $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Parts') == 0) {
		// 		isPreprintedReceipt = true;
		// 	}
		// });

		// if(isPreprintedReceipt){
		// 	ShowKuitansi();
		// }
		// else {
		// 	HideKuitansi();
		// }

		//end | Noto | Cek Preprinted Setting

		HideMetodePembayaranDetails_BankTransfer();

		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = "";
		console.log('9438 otw setupBiaya')
		$scope.TambahIncomingPayment_SetupBiaya();
	};

	//Alvin, begin
	$scope.TambahIncomingPayment_Interbranch_InformasiBranch_Selected_Changed = function () {
		console.log($scope.TambahIncomingPayment_NamaBranch_SearchDropdown);
		if ($scope.TambahIncomingPayment_NamaBranch_SearchDropdown !== "") {
			IncomingPaymentFactory.getDataBranch($scope.TambahIncomingPayment_NamaBranch_SearchDropdown)
				.then(
					function (res) {
						console.log("GetDataBranch")
						$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_Alamat = res.data.Result[0].Alamat;
						$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_KodeBranch = res.data.Result[0].OutletCode;
						$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_KabupatenKota = res.data.Result[0].Kota;
						$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_Telepon = res.data.Result[0].Telepon;
					}
				);
		}
	}
	//Alvin, end

	$scope.ShowNominalPembayaran = function(){
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"){
			$scope.ShowtipeStatementCard = true;
			$scope.ShowAlltipe = false;
		}else{
			$scope.ShowtipeStatementCard = false;
			$scope.ShowAlltipe = true;
		}
	}

	$scope.assignmentData = [];
	$scope.tmpnama1 = "";
	$scope.TambahIncomingPayment_SearchBy = function () {
		$scope.refresh = true;
		$timeout(function() {
			$scope.refresh = false;
		}, 1000);
		DisableAll_TambahIncomingPayment_InformasiPelanggan();
		DisableAll_TambahIncomingPayment_InformasiPembayaran();
		DisableAll_TambahIncomingPayment_DaftarSpk();
		DisableAll_TambahIncomingPayment_SalesOrder();
		DisableAll_TambahIncomingPayment_DaftarSalesOrderRadio();
		Disable_TambahIncomingPayment_KumpulanNomorRangka();
		Disable_TambahIncomingPayment_KumpulanWorkOrder();
		Disable_TambahIncomingPayment_DaftarArCreditDebit();
		$scope.TambahIncomingPayment_DaftarSpk_Detail_Radio = "";
		$scope.TambahIncomingPayment_DaftarSpk_Brief_Radio = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo = ""
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill = "";
		$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data = [];//aaa 
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan ="";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat ="";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = "";
		$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
		$scope.assignmentData = [];
		$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data = [];
		$scope.controlTitipanDP = true;
		$scope.TambahIncomingPayment_SetupBiaya();
		//20170505, nuse, begin
		
		Enable_TambahIncomingPayment_SaveAndPrintButton();

		$scope.ShowNominalPembayaran ();
		// if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"){
		// 	$scope.ShowtipeStatementCard = true;
		// 	$scope.ShowAlltipe = false;
		// }else{
		// 	$scope.ShowtipeStatementCard = false;
		// 	$scope.ShowAlltipe = true;
		// }

		if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card")) {
			Disable_TambahIncomingPayment_BiayaBiaya();
		}
		else {
			Enable_TambahIncomingPayment_BiayaBiaya();
		}

		$scope.getDataCharges();
		// $scope.getDataPreprinted();
		//20170505, nuse, end
		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment" ){
			var oldPem =$scope.TambahIncoming_OtherTypeNamaPembayaran;
			var newPem = oldPem.replace("&","dan").replace("+","%2B");
			// var cekPem1 = "";
			// var cekpem2 = newPem;

			if($scope.tmpnama1 == ""){
				$scope.tmpnama1 = newPem;
			}
			if($scope.tmpnama1 != newPem){
				// var cekpem2 = newPem;
				// if(cekPem1 != cekpem2){
					$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data = [];
					$scope.tmpnama1 = newPem;
				// }
			}
			console.log("new pembayar >>>>", newPem);
		}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment"){
			var namPem2 = $scope.TambahIncomingPayment_SearchBy_NamaPelanggan;

			if($scope.namPem1 != namPem2){
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data = [];
			}
		}
			

		console.log("TambahIncomingPayment_SearchBy");
		console.log($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment);

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			$scope.ShowProspect = true;
			$scope.ShowNoPelanggan = false;

			//begin | Noto | 180514 | 00186 UT
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
			//end | Noto | 180514 | 00186 UT

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPembayaran_Spk();
				Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
				$scope.getDataByNomorSPK($scope.TambahIncomingPayment_SearchBy_NomorSpk);

			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSpk_Detail();
				Enable_TambahIncomingPayment_InformasiPembayaran_Spk();
				Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
				$scope.ShowModalSearchSales();
				//tampilkan grid
				//$scope.getDataBySalesName();
			}
			
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			$scope.ShowProspect = true;
			$scope.ShowNoPelanggan = false;

			//begin | Noto | 180514 | 00186 UT
			Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
			Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem();
			//end | Noto | 180514 | 00186 UT
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				//20170430, NUSE EDIT, begin
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahIncomingPayment_UnitFullPaymentSetData();
				$scope.TambahIncomingPayment_UnitFullPaymentSO_Paging(1);
				$scope.TambahIncomingPayment_UnitFullPaymentSO_SetColumn();
				//20170430, NUSE EDIT, end


			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSpk_Brief();
				$scope.ShowModalSearchProspect();
				//20170430, NUSE EDIT, begin
				//$scope.TambahIncomingPayment_UnitFullPaymentSPK_Paging(1);
				//20170430, NUSE EDIT, end
			}
			// $scope.assignmentData = $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data;
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			$scope.ShowProspect = false;
			$scope.ShowNoPelanggan = true;

			//begin | Noto | 180514 | 00190 UT
			Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
			Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem();
			//end | Noto | 180514 | 00190 UT
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				//20170430, NUSE EDIT, begin
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahIncomingPayment_UnitFullPaymentSetData();
				$scope.TambahIncomingPayment_UnitFullPaymentSO_Paging(1);
				$scope.TambahIncomingPayment_UnitFullPaymentSO_SetColumn();
				//20170430, NUSE EDIT, end
			}
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSpk_Brief();
				$scope.ShowModalSearchProspect();
				//20170430, NUSE EDIT, begin
				//$scope.TambahIncomingPayment_UnitFullPaymentSPK_Paging(1);
				//20170430, NUSE EDIT, end
			}
			$scope.assignmentData = $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data;
		}
		//20170527, alvin, begin			
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			// HideKuitansi();
			Enable_TambahIncomingPayment_KumpulanNomorRangka();
			$scope.GetListProformaInvoiceNew(1, uiGridPageSize);
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {

			Enable_TambahIncomingPayment_KumpulanARRefundLeasing();
			$scope.GetListRefundLeasing(1, uiGridPageSize);

			$scope.ShowSimpanCetakTTUS = false;

			HideKuitansi();
			HideButtonSimpanCetakKuitansi();
		}
		//20170527, alvin, end		
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPelangan_Branch();
				Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
				//20170414, eric, begin
				$scope.getSalesOrdersBySONumber()
				//20170414, eric, end
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo();
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName(1, uiGridPageSize);
				Enable_TambahIncomingPayment_InformasiPelangan_Branch();
				Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
				//20170414, eric, end
			}
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {

		// if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
		// Enable_TambahIncomingPayment_InformasiPelangan_Branch();
		// Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
		// //20170414, eric, begin
		// $scope.getSalesOrdersBySONumber()
		// //20170414, eric, end
		// }
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
		// Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo();
		// //20170414, eric, begin
		// $scope.getSalesOrdersByCustomerName(1,10);
		// Enable_TambahIncomingPayment_InformasiPelangan_Branch();
		// Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();				
		// //20170414, eric, end
		// }
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			$scope.ShowProspect = false;
			$scope.ShowNoPelanggan = true;
			console.log("c");

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPelangan_Toyota();
				Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
				//20170425, eric, begin
				$scope.getSalesOrdersBySONumber1()
				//20170425, eric, end
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill();
				$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = false;
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				//$scope.getSalesOrdersDibayarByCustomerName1(1,10);
				//20170414, eric, end
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPelangan_Toyota();
				Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
				//20170425, eric, begin
				$scope.getSalesOrdersBySONumber1()
				//20170425, eric, end
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill();
				$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = false;
				//20170414, eric, begin
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				//$scope.getSalesOrdersDibayarByCustomerName1(1,10);
				//20170414, eric, end
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			Enable_TambahIncomingPayment_InformasiPelangan_Toyota();
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPembayaran_Appointment();
				$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
				console.log("22");
				$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota_TanggalWO = false;
				$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = true;

				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahIncomingPayment_SearchBy_NomorAppointment, "Appointment^Service - Down Payment");
				//hilman,end
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_InformasiPembayaran_WoDown();
				$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns();
				$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota_TanggalWO = true;
				$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = false;

				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahIncomingPayment_SearchBy_NomorWo, "WO^Service - Down Payment");
				//hilman,end
			}
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPelangan_Toyota();
				Enable_TambahIncomingPayment_InformasiPembayaran_WoBill();
				//hilman,begin
				$scope.getDataServiceCustomerAndPayment(1, $scope.TambahIncomingPayment_SearchBy_NomorWo, "WO^Service - Full Payment");
				//hilman,end

			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_KumpulanWorkOrder();
				//hilman,begin
				$scope.getDataPaymentByCustName(1, 10, newPem, "NamaPembayar^Service - Full Payment");
				//hilman,end
			}
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment Asuransi") {
		// Enable_TambahIncomingPayment_KumpulanWorkOrder();
		// }
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {

			//Marthin, 20170430, begin
			// if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
			// Enable_TambahIncomingPayment_InformasiPembayaran_SoDown();
			// Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
			// }
			// else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
			// Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSo();

			// }
			//console.log("DPP");
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				Enable_TambahIncomingPayment_InformasiPembayaran_SoDown();
				Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
				$scope.getSalesOrderBySONumberParts();
			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
				Enable_TambahIncomingPayment_InformasiPembayaran_Materai();

				$scope.getSalesOrdersByCustomerNameExt(1, uiGridPageSize);
				Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSo();
			}
			//Marthin, 20170430, end
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			//Marthin, 20170430, begin		
			// if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
			// Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
			// Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
			// }
			// else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
			// Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill();
			// }

			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2") {
				$scope.getSalesOrderBySONumberParts();
				Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
				Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();

			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				//$scope.getSalesOrdersByCustomerNameExt(1,10);
				$scope.getSalesOrdersByCustomerName1(1, uiGridPageSize);
				Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill();
			}
			//Marthin, 20170430, end			
		}
		//else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Bank Statement - Unknown")
		//{	
		//}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			console.log("Bank Statement - Card");
			Enable_TambahIncomingPayment_DaftarArCreditDebit();
			$scope.GetListUnknownCards(1, uiGridPageSize);
		}
		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
		//else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment=="Interbranch - Penerima")
		//{	
		//}	
	};

	$scope.Show_TambahIncomingPayment_DaftarSpk_Detail_ValueChanged = function () {
		//$scope.TambahIncomingPayment_DaftarSpk_Detail_Radio;
		//alert($scope.TambahIncomingPayment_DaftarSpk_Detail_Radio);
		if ($scope.TambahIncomingPayment_DaftarSpk_Detail_Radio != "") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
				if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
					//Enable_TambahIncomingPayment_DaftarSpk_Detail();
					Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
					Enable_TambahIncomingPayment_InformasiPembayaran_Spk();//ini kosong
				}

			}
		}

	};

	$scope.Show_TambahIncomingPayment_DaftarSpk_Brief_ValueChanged = function () {
		if ($scope.TambahIncomingPayment_DaftarSpk_Brief_Radio != "") {

			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
				if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
					Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
					Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem();
				}

			}
			else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
				if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
					Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
					Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem();
				}

			}
		}

	};

	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_ValueChanged = function () {
		if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo != "") {

			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
				if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
					Enable_TambahIncomingPayment_InformasiPelangan_Branch();
					Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
				}

			}
			// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {
			// if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
			// Enable_TambahIncomingPayment_InformasiPelangan_Branch();
			// Enable_TambahIncomingPayment_InformasiPembayaran_SoBill();
			// }

			// }

		}

	};

	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_ValueChanged = function () {
		if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo != "") {

			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
				if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
					Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch();
					Enable_TambahIncomingPayment_InformasiPembayaran_Materai();
				}
			}
		}
	};

	$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged = function () {
		console.log("LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged");
		//console.log(LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar);
		//if ($scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar != "") {
		//	$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = $scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar;
		Enable_LihatIncomingPayment_FootKuitansi();
		Enable_LihatIncomingPayment_RincianPembayaran();
		//alert(nanti jangan lupa program biar value ni tabel nongol);
		//}
	};

	$scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya_Selected_Changed = function (selected) {
		console.log('selectttttt',selected)
		$scope.selectTipeBiaya = angular.copy(selected);

		if (selected == 'Biaya Lain-lain'){
			$scope.AssigmentControl = true;
		}else if(selected == 'Pendapatan Lain-lain' || selected == 'Titipan'){
			$scope.AssigmentControl = true;
			
			// $scope.parent.TambahIncomingPayment_BiayaBiaya_DebetKredit.name = "Kredit";	

			// console.log('combo debet',$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit);
			// console.log('combo debet',$scope.TambahIncomingPayment_BiayaBiaya_DebetKreditxxxx);
		}else {
			$scope.AssigmentControl = false;
			$scope.TambahIncomingPayment_BiayaBiaya_Assignment = "";
		}

		$scope.TambahIncomingPayment_SetupBiaya();

		if ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "PPN Keluaran") {
			Enable_TambahIncomingPayment_Biaya_NoFaktur();
		}
		else if ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Bea Materai") {
			if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				$scope.currentNominalPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "");
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
				$scope.currentNominalPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash.toString().replace(/,/g, "");
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash Operation") {
				//BEGIN | NOTO | 190124 | Nominal Pembayaran - Cash Operation [12] | Get Biaya Materai
				$scope.currentNominalPembayaran = $scope.currentOPNominal;
			}
			else if ($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card") {
				$scope.currentNominalPembayaran = $scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard.toString().replace(/,/g, "");
			}
			console.log("getNominalMaterai1");
			$scope.getNominalMaterai($scope.currentNominalPembayaran);
			Disable_TambahIncomingPayment_Biaya_NoFaktur();
		}
		else {
			Disable_TambahIncomingPayment_Biaya_NoFaktur();
		}

		//gonta ganti assignment sesuai yg dipilih
		if ($scope.selectedTypeIncoming == "Unit - Booking Fee"){
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy != 2){
			
				$scope.assignmentData = [];
			$scope.assignmentData.push($scope.pilihNoSPK);
			console.log('$scope.assignmentData',$scope.assignmentData);
			}else			
			if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NomorSPK;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SPKId;
			}
		}
			
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}else
		if ($scope.selectedTypeIncoming == "Unit - Down Payment" || $scope.selectedTypeIncoming == "Unit - Full Payment"){
			$scope.assignmentData = $scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Unit - Order Pengurusan Dokumen"){
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.assignmentData = $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data;
			}
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Unit - Purna Jual"){
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
				$scope.assignmentData = $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data;
			}
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Parts - Down Payment"){
			if($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
				var tmpSelect = $scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data;
			}else{
				var tmpSelect = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.data;
			}
			$scope.assignmentData = tmpSelect;
		console.log('$scope.assignmentData',$scope.assignmentData);
		console.log('MASUK PART DP')

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}
		else
		if ($scope.selectedTypeIncoming == "Parts - Full Payment"){
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy != 2){
				$scope.assignmentData = $scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data;
			}else{
				$scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data;
			}
			
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Service - Down Payment"){
			// $scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].WoNo;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].NoWO;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if ($scope.selectedTypeIncoming == "Service - Full Payment"){
			 $scope.assignmentData = $scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NoBilling;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].NoWO;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}
		else
		if ($scope.selectedTypeIncoming == "Unit - Payment (Kuitansi Penagihan)"){
			 $scope.assignmentData = $scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data;
		console.log('$scope.assignmentData',$scope.assignmentData);

		if ($scope.assignmentData.length > 0){
			for (var i in $scope.assignmentData){
				console.log('$scope.masuk sini ga???');
				$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].NoSO;
				$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SoId;
			}
			console.log('$scope.assignmentData2',$scope.assignmentData);
		}
		}else
		if($scope.selectedTypeIncoming == "Unit - Swapping"){
			console.log('DATA =>',$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data)
			$scope.assignmentData = $scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data;
			console.log('$scope.assignmentData',$scope.assignmentData);
	
			if ($scope.assignmentData.length > 0){
				for (var i in $scope.assignmentData){
					console.log('$scope.masuk sini ga???');
					$scope.assignmentData[i].AssignmentNo = $scope.assignmentData[i].SONumber;
					$scope.assignmentData[i].AssignmentId = $scope.assignmentData[i].SOId;
				}
				console.log('$scope.assignmentData2',$scope.assignmentData);
			}
		}

		
		// TambahIncomingPayment_DaftarNoRangka_UIGrid
		//end gonta ganti assignment sesuai yg dipilih
		
	}

	$scope.Assignment_Selected_Changed = function(selected){
		console.log('seleccttttttt',selected)
	}

	$scope.LihatIncomingPayment_RincianPembayaran_TipeIncomingPayment_Selected_Changed = function (selectedTypeIncoming) {
		
		DisableAll_LihatIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatIncomingPayment_InformasiPembayaran();//ini semua info pebayaran di hide
		Disable_LihatIncomingPayment_Interbranch_InformasiBranch();
		Disable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		Disable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		Disable_LihatIncomingPayment_DaftarArCreditDebit();
		//Disable_LihatIncomingPayment_RincianPembayaran();
		//Disable_LihatIncomingPayment_FootKuitansi();
		//$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar="";
		Disable_LihatIncomingPayment_DaftarSalesOrder();
		Disable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		Disable_LihatIncomingPayment_Top_NomorSpk();
		Disable_LihatIncomingPayment_Top_NamaLeasing();
		Disable_LihatIncomingPayment_Top_NoProforma();
		Disable_LihatIncomingPayment_Top_NomorSo();
		Disable_LihatIncomingPayment_Top_NomorWo();
		Disable_LihatIncomingPayment_Top_NamaAsuransi();
		Disable_LihatIncomingPayment_Top_NamaBank();

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {

			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_InformasiPembayaran_Spk();

		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_DaftarSalesOrder();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_DaftarSalesOrder();
		}
		//20170527, Alvin, begin		
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			Enable_LihatIncomingPayment_Top_NamaLeasing();
			Enable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			Enable_LihatIncomingPayment_Top_NamaLeasing();
			//Enable_LihatIncomingPayment_KumpulanARRefundLeasing();
			Enable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		}
		//20170527, Alvin, end		
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_Branch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
		}
		// else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi") {
		// Enable_LihatIncomingPayment_Top_NomorSo();
		// Enable_LihatIncomingPayment_InformasiPelangan_Branch();
		// Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
		// }
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			//Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			//Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			//Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			//Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			Enable_LihatIncomingPayment_Top_NomorWo();
			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			Enable_LihatIncomingPayment_InformasiPembayaran_Appointment();
			//Alvin, 20170514, begin
			Enable_LihatIncomingPayment_InformasiPembayaran_WoDown();
			//Alvin, 20170514, end
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorWo();
			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			//Alvin, 20170514, begin -> noto komen
			// Enable_LihatIncomingPayment_InformasiPembayaran_WoBill();
			//Alvin, 20170514, end
			Enable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		}
		// else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment Asuransi") {

		// //Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		// Enable_LihatIncomingPayment_Top_NamaAsuransi();
		// Enable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		// }
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_PelangganBranch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoDown();
			Enable_LihatIncomingPayment_InformasiPembayaran_Materai();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_PelangganBranch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown") {

			//cari apa isinya
			//emang kosong
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			Enable_LihatIncomingPayment_Top_NamaBank();
			Enable_LihatIncomingPayment_DaftarArCreditDebit();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			Enable_LihatIncomingPayment_Interbranch_InformasiBranch();
		}
	};

	//========================================20170430, NUSE EDIT, BEGIN========================================
	$scope.TambahIncomingPayment_UnitFullPaymentSetData = function () {
		IncomingPaymentFactory.getCustomerDataBySPKId($scope.TambahIncomingPayment_SearchBy_NomorSpk)
			.then(
				function (res) {
					console.log("Customer by SPK Id ==>", res.data.Result);
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Phone;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Address;
					$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.currentCustomerId = res.data.Result[0].CustomerId;
				}
			);
	}

	$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		enableFiltering: true,
		showColumnFooter: true,
		/*columnDefs: [
						{name:"SOId",field:"SOId",visible:false},
						{name:"Nomor SO",field:"SONumber"},
						{name:"Tanggal SO",field:"SODate"},
						{name:"Nominal Billing", field:"TotalNominal"},
						{name:"Biaya Materai",field:"Materai"},
						{name:"Total yang Harus Dibayar", field:"TotalAllMustBePay"},
						{name:"Total terbayar", field:"PaidNominal"},
						{name:"Sisa pembayaran", field:"TotalPaymentMustBePay"},
						{name:'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
								enableCellEdit: true, type: 'number'},
					],*/
		onRegisterApi: function (gridApi) {//ccccccccccccccc
			$scope.GridApiTambahIncomingPayment_UnitFullPaymentSO = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = $scope.TambahIncomingPayment_UnitFullPaymentSO_Paging(pageNumber, pageSize);
			});
			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDefs, newValue){
				if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment"){
					// console.log("Test masuk Sini Unit FP")
					console.log("test", rowEntity);
					console.log("colDef ", colDefs);
					if(newValue > rowEntity.TotalPaymentMustBePay){
						var newValue = 0;   
						rowEntity[colDefs.name] = newValue;
							bsNotify.show(
								{
									title: "Peringatan",
									content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
									type: 'warning'
								}
							); 
					}
				}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment"){
					console.log("Test masuk Sini Unit DP")
					console.log("test", rowEntity);
					console.log("colDef ", colDefs);
					if(newValue > rowEntity.TotalDPMustBePay){
						var newValue = 0;   
						rowEntity[colDefs.name] = newValue;
							bsNotify.show(
								{
									title: "Peringatan",
									content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
									type: 'warning'
								}
							); 
					}if(newValue == 'undefined' || typeof newValue == 'undefined'){
						$scope.controlTitipanDP = true;
						$scope.TambahIncomingPayment_SetupBiaya();
					}else
					if(newValue == rowEntity.TotalDPMustBePay){
						$scope.controlTitipanDP = false;
						$scope.TambahIncomingPayment_SetupBiaya();
					}else{
						$scope.controlTitipanDP = true;
						$scope.TambahIncomingPayment_SetupBiaya();
					}
				}
				

				$scope.TotGridUnitFullPayment = 0;
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (row){
					var JmlKredit = 0;
					var JmlDebet = 0;
					if ( row.Pembayaran != undefined ){
						$scope.TotGridUnitFullPayment += row.Pembayaran ;
					}
					console.log("TotGridUnitFullPayment",$scope.TotGridUnitFullPayment);
					
					for(var i=0; i<$scope.TambahIncomingPayment_Biaya_UIGrid.data.length; i++){
						console.log("ui grid",$scope.TambahIncomingPayment_Biaya_UIGrid.data);
						
						if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
						   ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
							JmlKredit = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						}
						else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
							JmlDebet = $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal;
						}else if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Debet')){
							// TotPerhitungan;
						}
					}
						var tmpSelisih = parseInt($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "")) - parseInt($scope.TotGridUnitFullPayment) - parseInt(JmlKredit) + parseInt(JmlDebet);
						console.log("tmpSelisih",tmpSelisih);
						if(tmpSelisih < 0){
							tmpSelisih = 0;
						}
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = tmpSelisih;
				});
			});

		}
	};

	

	$scope.TambahIncomingPayment_UnitFullPaymentSO_SetColumn = function () {
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			console.log($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment);
			console.log("1");

			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.columnDefs = [
				{ name: "SOId", field: "SOId", visible: false },
				{ name: "Nomor SO", displayName: "Nomor SO", field: "SONumber", enableCellEdit: false },
				{ name: "Tanggal SO", displayName: "Tanggal SO", field: "SODate", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
				{ name: "Nominal Billing", field: "TotalNominal", cellFilter: 'rupiahC', enableCellEdit: false },
				//{name:"Biaya Materai",field:"Materai", cellFilter: 'rupiahC'},
				{ name: "Total yang Harus Dibayar", field: "TotalAllMustBePay", cellFilter: 'rupiahC', visible: false },
				{ name: "Total terbayar", field: "PaidNominal", cellFilter: 'rupiahC', enableCellEdit: false },
				{ name: "Saldo", field: "TotalPaymentMustBePay", cellFilter: 'currency:"":0', enableCellEdit: false },
				{
					name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					},
					enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0'
				},
			];
		
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			console.log($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment);
			console.log("2");


			$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.columnDefs = [
				{ name: "SOId", field: "SOId", visible: false },
				{ name: "Nomor SO", displayName: "Nomor SO", field: "SONumber", enableCellEdit: false },
				{ name: "Tanggal SO", displayName: "Tanggal SO", field: "SODate", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
				{ name: "Nominal Down Payment", field: "NominalDP", cellFilter: 'rupiahC', enableCellEdit: false },
				//{name:"Biaya Materai",field:"Materai", cellFilter: 'rupiahC'},
				{ name: "Total yang Harus Dibayar", field: "TotalAllDPMustPay", cellFilter: 'rupiahC', visible: false },
				{ name: "Total terbayar", field: "PaidDP", cellFilter: 'rupiahC', enableCellEdit: false },
				{ name: "Saldo", field: "TotalDPMustBePay", cellFilter: 'currency:"":0', enableCellEdit: false },
				{

					name: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					},
					enableCellEdit: true, type: 'number', cellFilter: 'currency:"":0'
				},
			];
			
			
		}
		
	}

	$scope.TambahIncomingPayment_UnitFullPaymentSO_Paging = function (pageNumber, pageSize) {
		if (pageSize == "" || pageSize == 0 || angular.isUndefined(pageSize)) {
			pageSize = 10;
		}
		IncomingPaymentFactory.getSalesOrderBySPKId(pageNumber, pageSize, $scope.TambahIncomingPayment_SearchBy_NomorSpk, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					console.log("Get data Sales Order", res.data.result);
					$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.totalItems = res.data.Total;
					console.log($scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.totalItems);
					$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.paginationPageSize = pageSize;
					$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.paginationPageSizes = [10, 25, 50];

				}
			);
	}


	$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "SPKId", field: "SPKId", visible: false },
			{ name: "Nomor SPK", displayName: "Nomor SPK", field: "NomorSPK" },
			{ name: "Model", field: "Model" },
			{ name: "Tipe", field: "Tipe" },
			{ name: "Warna", field: "Warna" },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahIncomingPayment_UnitFullPaymentSPK = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.data = $scope.TambahIncomingPayment_UnitFullPaymentSPK_Paging(pageNumber, pageSize);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.TambahIncomingPayment_SearchBy_NomorSpk = row.entity.NomorSPK;
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data = []
				$scope.TambahIncomingPayment_UnitFullPaymentSetData();
				$scope.TambahIncomingPayment_UnitFullPaymentSO_Paging(1);
				$scope.TambahIncomingPayment_UnitFullPaymentSO_SetColumn();
				Enable_TambahIncomingPayment_InformasiPelangan_Prospect();
				Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem();
			}
			)
		}
	};

	$scope.TambahIncomingPayment_UnitFullPaymentSPK_Paging = function (pageNumber, pageSize) {
		if (pageSize == "" || pageSize == 0 || angular.isUndefined(pageSize)) {
			pageSize = 10;
		}
		IncomingPaymentFactory.getSPKByCustomerName(pageNumber, pageSize,
			$scope.TambahIncomingPayment_SearchBy_NamaPelanggan, $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.data = res.data.Result;
						$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.totalItems = res.data.Total;
						$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.paginationPageSize = pageSize;
						$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.paginationCurrentPage = pageNumber;
					}
				}
			);
	}

	$scope.TambahIncomingPayment_FullPaymentSOData = function () {
		$scope.TambahIncomingPayment_SOSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data, function (value, key) {
			var obj;
			$scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment" ? 
			obj = { "SOId": value.SOId, "PaidAmount": value.Pembayaran, "SODownPayment": value.NominalDP } : obj = { "SOId": value.SOId, "PaidAmount": value.Pembayaran } ;
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_SOSubmitData.push(obj);
		}
		);
	}
	//=========================================20170430, NUSE EDIT, END=========================================


	//Alvin, 20170505, begin
	$scope.TambahIncomingPayment_ServiceFullPaymentWOData = function () {
		$scope.TambahIncomingPayment_ServiceWOSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.NoWO, "PaidAmount": value.SisaPembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_ServiceWOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_BookingFeeData = function () {
		var BiayaBankD = 0;
		var BiayaK = 0;
		var BiayaD = 0;

		$scope.TambahIncomingPayment_BookingFeeSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data, function (value, key) {
			//var obj = {"SOId" : value.SPKId, "PaidAmount" : value.NominalBookingFee};
			if ($scope.TambahIncomingPayment_Biaya_UIGrid.data.length > 0){
				for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaBankD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					}
				}
	
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					 console.log('1');
					// break;
				}
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					BiayaK += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					console.log('2');
					// break;
				}
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					BiayaD += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					console.log('3');
					// break;
				}
				var NomPaidAmount = parseInt($scope.Pembayaran) + parseInt(BiayaBankD) - parseInt(BiayaK) + parseInt(BiayaD) ;
			var obj = { "SOId": value.SPKId, "PaidAmount": NomPaidAmount.toString()};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_BookingFeeSubmitData.push(obj);
			}else{
			var NomPaidAmount = parseInt($scope.Pembayaran) + parseInt(BiayaBankD) ;
			var obj = { "SOId": value.SPKId, "PaidAmount": NomPaidAmount.toString()};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_BookingFeeSubmitData.push(obj);
			}

			
			// var NomPaidAmount = parseInt($scope.Pembayaran) + parseInt(BiayaBankD) + parseInt(BiayaK) - parseInt(BiayaD) ;
			// var obj = { "SOId": value.SPKId, "PaidAmount": NomPaidAmount.toString()};
			// console.log("Object of SO ==>", obj);
			// console.log("Object of Value ==>", value);
			// $scope.TambahIncomingPayment_BookingFeeSubmitData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_Biaya2 = function () {
		$scope.TambahIncomingPayment_BiayaData = [];
		angular.forEach($scope.TambahIncomingPayment_Biaya_UIGrid.data, function (value, key) {
			var obj =
			{
				"BiayaId": value.BiayaId,
				"DK": value.DebitCredit,
				"PaidAmount": value.Nominal,
				"Keterangan": value.Keterangan,
				"SOCode": value.Assignment,
				"NoFaktur": value.NoFaktur
			};
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_BiayaData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_SalesOrder = function () {
		var tmpKredit = 0;
		var tmpDebit = 0;
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			var tmpSelisihKel = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
		}else{
			var tmpSelisihKel = 0;
		}
		$scope.TambahIncomingPayment_PartsSOSubmitData = [];
		
		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
		}

		// var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt(tmpSelisihKel) - parseInt(tmpKredit) + parseInt(tmpDebit);
		angular.forEach($scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": value.SisaPembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
		}
		);
	}


	$scope.TambahIncomingPayment_SalesOrder2 = function () {
		console.log("TambahAlokasiUnknown_SalesOrder2");
		$scope.TambahIncomingPayment_PartsSOSubmitData = [];
		
		$scope.LoopingBiaya();
		var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
		
		angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_Materai_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": tmpPembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
		}
		);	
	}

	// Looping Biaya lain-lain
	$scope.LoopingBiaya = function(){
		$scope.tmpKredit = 0;
		$scope.tmpDebit = 0;
		
		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				$scope.tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				$scope.tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
		}
	}

	// Looping Biaya lain-lain
	$scope.LoopingBiaya = function(){
		$scope.tmpKredit = 0;
		$scope.tmpDebit = 0;
		
		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				$scope.tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				$scope.tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				$scope.tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
		}
	}


	$scope.TambahIncomingPayment_SoDown = function () {
		$scope.TambahIncomingPayment_PartsSOSubmitData = [];
		$scope.Pembayaran = $scope.paidAmount;
		angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": $scope.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_SoBill = function () {
		$scope.TambahIncomingPayment_PartsSOSubmitData = [];
		$scope.LoopingBiaya();
		var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
		if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer > 0 && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			tmpPembayaran = tmpPembayaran - $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
		}
		angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": tmpPembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
		}
		);
		console.log('SELISIH =>', $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer)
		console.log("AR =>", tmpPembayaran)
	}

	$scope.TambahIncomingPayment_SoBillPJOPD = function () {
		var tmpCekBayar = 0;
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && ($scope.Pembayaran > $scope.SaldoOPD)){
				tmpCekBayar = 1;
			}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && ($scope.Pembayaran > $scope.SaldoPJ)){
				tmpCekBayar = 1;
			}
		}
		$scope.LoopingBiaya();
		var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt($scope.tmpKredit) + parseInt($scope.tmpDebit);
		
		//based on selisih
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && ($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer > 0)){
				tmpPembayaran = $scope.SaldoOPD;
			}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && ($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer > 0)){
				tmpPembayaran = $scope.SaldoPJ;
			}
		}

		if(tmpCekBayar == 1){
			$scope.TambahIncomingPayment_PartsSOSubmitData = [];
			angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
				var obj = { "SOId": value.SOId, "PaidAmount": value.SisaPembayaran };
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
			}
			);
		}else{
			$scope.TambahIncomingPayment_PartsSOSubmitData = [];
			angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
				var obj = { "SOId": value.SOId, "PaidAmount": tmpPembayaran };
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
			}
			);
		}
	}

	$scope.TambahIncomingPayment_SoBillPFP = function () {
		var cekPembayaran = 0
		var tmpKredit = 0;
		var tmpDebit = 0;
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			var tmpSelisihKel = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer;
		}else{
			var tmpSelisihKel = 0;
		}
		$scope.TambahIncomingPayment_PartsSOSubmitData = [];
		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
					cekPembayaran = 1;
				}
			}
			
		}

		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Bea Materai" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ||
			($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == 'Kredit') ){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}else if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal); 
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Titipan" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
				tmpKredit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);									
			}
			if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
				tmpDebit += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
			}
		}
		if(cekPembayaran == 0){
			// if(($scope.tmpBiayaK == null || $scope.tmpBiayaK == undefined) && ($scope.tmpBiayaD == null || $scope.tmpBiayaD == undefined)){
			// 	$scope.tmpBiayaK = 0;
			// 	$scope.tmpBiayaD = 0;
			// }
			var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt(tmpSelisihKel) - parseInt(tmpKredit) + parseInt(tmpDebit);
			angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
				var obj = { "SOId": value.SOId, "PaidAmount": tmpPembayaran.toString() };
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
			}
			);
		}else{
			// if(($scope.tmpBiayaK == null || $scope.tmpBiayaK == undefined) && ($scope.tmpBiayaD == null || $scope.tmpBiayaD == undefined)){
			// 	$scope.tmpBiayaK = 0;
			// 	$scope.tmpBiayaD = 0;
			// }
			var tmpPembayaran = parseInt($scope.Pembayaran) - parseInt(tmpSelisihKel) - parseInt(tmpKredit) + parseInt(tmpDebit);
			angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data, function (value, key) {
				var obj = { "SOId": value.SOId, "PaidAmount": tmpPembayaran };
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_PartsSOSubmitData.push(obj);
			}
			);
		}
	}
	

	$scope.TambahIncomingPayment_Service_ByName = function () {
		$scope.TambahIncomingPayment_ServiceSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data, function (value, key) {
			//var obj = {"SOId" : value.NoWO, "PaidAmount" : value.SisaPembayaran};
			// var obj = { "SOId": value.NoWO, "PaidAmount": $scope.Pembayaran };
			var obj = { "SOId": value.NoBilling, "PaidAmount": value.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_ServiceSubmitData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_Service_ByWO = function () {
		$scope.TambahIncomingPayment_ServiceSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.NoWO, "PaidAmount": value.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_ServiceSubmitData.push(obj);
		}
		);
	}

	$scope.TambahIncomingPayment_SoSoSetColumns = function () {
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			console.log("a");
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				//{ name: 'SPKId', field: 'SPKId', visible: false },
				//{ name: 'SOId', field: 'SOId', visible: false },		
				{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC' },
				//{ name: 'Total Terbayar', field: 'PaidNominal' },	
			];
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			console.log("b");
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.columnDefs = [
				{ name: 'CustomerId', field: 'CustomerId', visible: false },
				{ name: 'SPKId', field: 'SPKId', visible: false },
				{ name: 'SOId', field: 'SOId', visible: false },
				{ name: 'Nomor SO', displayName: 'Nomor SO', field: 'SONumber' },
				{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'SODate', type: 'date', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC' },
			];
		}
	}

	$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid_SetColumns = function () {
		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			console.log("a1");
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'Tanggal', cellFilter: 'date:\'dd-MM-yyyy\'', visible: true, enableCellEdit: false },
					{ name: 'SPKId', field: 'SPKId', visible: false },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', cellFilter: 'rupiahC', visible: true, enableCellEdit: false },
					{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
					{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: false },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'rupiahC', visible: false }
				];
			$scope.CurrentNominal = 0;
			console.log($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.totalItems);
			if ($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data.length > 0) {
				for (var k = 0; k < $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.totalItems; k++) {
					console.log($scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data[k].NominalBookingFee);
					$scope.CurrentNominal = $scope.CurrentNominal + $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data[k].NominalBookingFee;
				}
			}
			console.log($scope.CurrentNominal);

			
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
			console.log("b");
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SPK', displayName: "Tanggal SPK", field: 'Tanggal', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', visible: false, cellFilter: 'rupiahC' },
					{ name: 'Tanggal Appointment', field: 'TglAppointment', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'', visible: false },
					{ name: 'No WO', displayName: "Nomor WO", field: 'NoWO', visible: false },
					{ name: 'Keterangan', displayName: "", field: 'Keterangan', visible: true },
					{ name: 'Nominal', field: 'NominalDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: false },
					{ name: 'Biaya Materai', field: 'BiayaMaterai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalYgHarusDibayar', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: true },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC', visible: true, enableCellEdit: true },
					{
						name: 'Pembayaran', field: 'Pembayaran', aggregationType: uiGridConstants.aggregationTypes.sum,
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}, enableCellEdit: true, type: 'number', cellFilter: 'rupiahC',
					}
				];
				console.log("des kolom >>>>>>>>",$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid);
		}
		else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			console.log("c");
			$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs =
				[
					{ name: 'Tanggal SO', displayName: 'Tanggal SO', field: 'TglWo', cellFilter: 'date:\'dd-MM-yyyy\'', visible: true },
					{ name: 'Nominal Billing', field: 'TotalNominal', cellFilter: 'rupiahC', visible: true },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'rupiahC', visible: true },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'rupiahC', visible: true }
				];
		}
		// else if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi"){
		// console.log("c");
		// $scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.columnDefs =
		// [
		// { name:'Tanggal SO',    field:'TglWo',cellFilter: 'date:\'dd-MM-yyyy\'', visible: true },
		// { name:'Nominal Billing',    field:'NominalDP', cellFilter: 'rupiahC', visible: true},
		// { name:'Total Terbayar',    field:'PaidDP', cellFilter: 'rupiahC', visible: true},
		// { name:'Sisa Pembayaran',    field:'SisaDP', cellFilter: 'rupiahC', visible: true}
		// ];
		// }			
	};


	$scope.TambahIncomingPayment_Biaya_UIGrid =
		{
			enableSorting: false,
			//onRegisterApi: function(gridApi){},
			onRegisterApi: function (gridApi) { $scope.gridApi_BiayaUIGrid = gridApi; },
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tipe Biaya Lain-Lain', field: 'BiayaId' },
					{ name: 'Debet/Kredit', type: 'text', field: 'DebitCredit' },
					{ name: 'Assignment', type: 'text', field: 'Assignment' },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'rupiahC' },
					{ name: 'Keterangan', field: 'Keterangan', visible: false },
					{ name: 'NoFaktur', field: 'NoFaktur', visible: false },
					{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRowBiaya(row)">Hapus</button>' },
				]
		};


	$scope.TambahIncomingPayment_BiayaBiaya_Tambah = function () {
		$scope.TotalBiayaOI = 0;
		$scope.TotalBiayaBankOE = 0;
		var TambahSelisih = 0;
		var TotalPemSO = 0;
		var PembayaranDP = 0;

		if($scope.selectTipeBiaya == 'Pendapatan Lain-lain' && $scope.TambahIncomingPayment_BiayaBiaya_Nominal > $scope.valueOI){
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OI sebesar Rp " +$scope.valueOI,
					type: 'warning'
				}
			);	
			return;

		}else if($scope.selectTipeBiaya == 'Biaya Lain-lain' && $scope.TambahIncomingPayment_BiayaBiaya_Nominal > $scope.valueOE) {
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Nominal melebihi maksimal master OE sebesar Rp " +$scope.valueOE,
					type: 'warning'
				}
			);
			return;

		}

		for (var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
			if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == "Biaya Lain-lain"){
				if($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain"){
					bsNotify.show({
						title: "Warning",
						content: "Sudah Ada Pendapatan Lain-lain atau Biaya Lain-lain!",
						type: 'warning'
					});
					return;
				}
			}
			
		}
		

		if (!$scope.TambahIncomingPayment_CheckBiaya())
			return;

		console.log($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya);
		console.log($scope.TambahIncomingPayment_BiayaBiaya_DebetKredit);
		var DebitCredit = "Debet";

		$scope.tmpObj = [];
		var found = 0;
		$scope.TambahIncomingPayment_Biaya_UIGrid.data.forEach(function (obj) {
			console.log('isi grid -->', obj.BiayaId);
			console.log('isi biaya -->', $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya);
			// if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya =="Titipan" && $scope.TambahIncomingPayment_BiayaBiaya_Assignment !=obj.Assignment){
			// 	found = 0;
			// }else
			if ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == obj.BiayaId){
				found = 1;
			}
				
		});

		if (found == 0) {
			var biaya = $scope.TambahIncomingPayment_BiayaBiaya_Nominal.toString().replace(/,/g, "");
			var HitungTotal = 0;
			console.log('$scope.TambahIncomingPayment_BiayaBiaya_Assignment',$scope.TambahIncomingPayment_BiayaBiaya_Assignment);
			if (($scope.TambahIncomingPayment_BiayaBiaya_Assignment == null || $scope.TambahIncomingPayment_BiayaBiaya_Assignment == 'undefined' || typeof $scope.TambahIncomingPayment_BiayaBiaya_Assignment == 'undefined') && $scope.AssigmentControl == true ){
				bsNotify.show({
					title: "Warning",
					content: "Assignment harus diisi!",
					type: 'warning'
				});
			}else{
							
				$scope.tmpObj = [{
						BiayaId: $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya,
						DebitCredit: $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit,
						Nominal: biaya,
						Keterangan: $scope.TambahIncomingPayment_BiayaBiaya_Keterangan,
						Assignment: $scope.TambahIncomingPayment_BiayaBiaya_Assignment,
						NoFaktur: $scope.TambahIncomingPayment_BiayaBiaya_NoFaktur
				}]
				console.log("kuyyyyy", $scope.tmpObj)
			
				$scope.TambahIncomingPayment_Biaya_UIGrid.data.push(
					{
						BiayaId: $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya,
						DebitCredit: $scope.TambahIncomingPayment_BiayaBiaya_DebetKredit,
						Nominal: biaya,
						Keterangan: $scope.TambahIncomingPayment_BiayaBiaya_Keterangan,
						Assignment: $scope.TambahIncomingPayment_BiayaBiaya_Assignment,
						NoFaktur: $scope.TambahIncomingPayment_BiayaBiaya_NoFaktur
					})

				if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Service') >= 0 || $scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment.indexOf('Parts') >= 0){
					$scope.TambahIncomingPayment_Calculate_RincianLain();
				}
				
				$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (row){
					if(row.Pembayaran != undefined){
						HitungTotal += row.Pembayaran;
					}
				});
	
				for(var i in $scope.TambahIncomingPayment_Biaya_UIGrid.data){
					if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") && 
					(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain")){
						//pisah OI OE
						if ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain"){
							$scope.TotalBiayaOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else{
							$scope.TotalBiayaBankOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}

					}
					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
						TambahSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.KuitansiPem) + parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("tambah selisih", TambahSelisih);
						if(TambahSelisih < 0){
							TambahSelisih = 0;
						}
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					}
					// else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					// $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet"){
					// 	TambahSelisih = ($scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemOPD) + parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 	console.log("tambah selisih", TambahSelisih);
					// 	if(TambahSelisih < 0){
					// 		TambahSelisih = 0;
					// 	}
					// 	$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					// 	$scope.TambahIncomingPayment_Calculate_RincianLain();
					// }else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					// $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					// 	$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 	console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
					// 	if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
					// 		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
					// 	}
					// 	// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					// 	$scope.TambahIncomingPayment_Calculate_RincianLain();
					// }
					
					//new 
					else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") && 
					(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain")){
						//pisah OI OE
						if ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain"){
							$scope.TotalBiayaOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else{
							$scope.TotalBiayaBankOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}

					}

					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain")){
						//pisah OI OE
						if ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain"){
							$scope.TotalBiayaOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else{
							$scope.TotalBiayaBankOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
					}else if(($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") && 
					(($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Bank" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Debet") ||
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Biaya Lain-lain" || $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain")){
						//pisah OI OE
						if ($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain"){
							$scope.TotalBiayaOI += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						else{
							$scope.TotalBiayaBankOE += parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						}
						
					}
					// else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					// $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
					// 	$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
					// 	console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
					// 	if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
					// 		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
					// 	}
					// }
					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
						if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
						}
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					}
					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
						if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
						}
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					}
					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
						if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
						}
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					}
					else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer" && 
					$scope.TambahIncomingPayment_Biaya_UIGrid.data[i].BiayaId == "Pendapatan Lain-lain" && $scope.TambahIncomingPayment_Biaya_UIGrid.data[i].DebitCredit == "Kredit"){
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer -  parseInt($scope.TambahIncomingPayment_Biaya_UIGrid.data[i].Nominal);
						console.log("tambah selisih", $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer);
						if($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer < 0){
							$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
						}
						// $scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					}
				}

				if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
					$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function(obj){
						if(obj.Pembayaran != null){
							PembayaranDP += obj.Pembayaran;
						}else{
							PembayaranDP = 0;
						}
						$scope.PemUFP = PembayaranDP;
					});
					var TotalPembayaran = $scope.PemUFP;//tandain
					console.log('cek agregation value', TotalPembayaran);
					$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
		
					TambahSelisih = ($scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - TotalPembayaran) + $scope.TotalBiayaBankOE - $scope.TotalBiayaOI;
					console.log("tambah selisih", TambahSelisih);
					if(TambahSelisih < 0){
						TambahSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}
				else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
						if(obj.SisaPembayaran != null){
							TotalPemSO += obj.SisaPembayaran;
						}else{
							TotalPemSO = 0;
						}
						$scope.PemOPD = TotalPemSO;
					});
					var TotalPembayaran = $scope.PemOPD;//tandain
					console.log('cek agregation value', TotalPembayaran);
					$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
		
					TambahSelisih = ($scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - TotalPembayaran) + $scope.TotalBiayaBankOE - $scope.TotalBiayaOI;
					console.log("tambah selisih", TambahSelisih);
					if(TambahSelisih < 0){
						TambahSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}
				else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}
				else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}
				else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
						if(obj.SisaPembayaran != null){
							TotalPemSO += obj.SisaPembayaran;
						}else{
							TotalPemSO = 0;
						}
						$scope.PemPJ = TotalPemSO;
					})
					var TotalPembayaran = $scope.PemPJ
					console.log('cek agregation value', TotalPembayaran)
					$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
		
					TambahSelisih = ($scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - $scope.PemPJ) + $scope.TotalBiayaBankOE - $scope.TotalBiayaOI;
					console.log("tambah selisih", TambahSelisih);
					if(TambahSelisih < 0){
						TambahSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "2"){
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}else if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"
				&& $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3"){
					$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data.forEach(function(obj){
						if(obj.SisaPembayaran != null){
							TotalPemSO += obj.SisaPembayaran;
						}else{
							TotalPemSO = 0;
						}
						$scope.PemSwap = TotalPemSO;
					});
					var TotalPembayaran = $scope.PemSwap;//tandain
					console.log('cek agregation value', TotalPembayaran);
					$scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = parseInt(TotalPembayaran) + parseInt($scope.TotalBiayaOI) - parseInt($scope.TotalBiayaBankOE);
		
					TambahSelisih = ($scope.Var_TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer - TotalPembayaran) + $scope.TotalBiayaBankOE - $scope.TotalBiayaOI;
					console.log("tambah selisih", TambahSelisih);
					if(TambahSelisih < 0){
						TambahSelisih = 0;
					}
					$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = TambahSelisih;
					$scope.TambahIncomingPayment_Calculate_RincianLain();
					$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
				}
				
			}
			
		}

	};

	$scope.deleteRowBiaya = function (row) {
		var index = $scope.TambahIncomingPayment_Biaya_UIGrid.data.indexOf(row.entity);
		console.log("cek row.entity deleted biaya",row.entity);
		$scope.deletedRowBiayaId = row.entity.BiayaId;
		$scope.deletedRowNominal = parseInt(row.entity.Nominal);
		$scope.deletedRowDebitCredit = row.entity.DebitCredit;
		$scope.indexDelete = index;
		$scope.TambahIncomingPayment_Calculate_RincianLain(true);
		$scope.TambahIncomingPayment_Biaya_UIGrid.data.splice(index, 1);
		$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransferBlur();
	};

	//Filter LihatIncomingPayment
	$scope.LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked = function () {
		console.log("LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked");
		//if ( ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != "") &&
		//	($scope.LihatIncomingPayment_DaftarIncomingPayment_Search != "") )
		//{		
		//	var nomor;		
		//	var value = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
		//	$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
		//	console.log('filter text -->',$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text );
		//	if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Nomor Incoming Payment") {
		//		nomor = 3;
		//	}				
		//	if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Tipe Incoming") {
		//		nomor = 4;
		//	}				
		//	if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Nama Pelanggan") {
		//		nomor = 5;
		//	}				
		//	if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Metode Pembayaran") {
		//		nomor = 6;
		//	}
		//	if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Nominal") {
		//		nomor = 7;
		//	}				
		//	console.log("nomor --> ", nomor);
		//	$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.columns[nomor].filters[0].term=$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
		//}
		//else {
		//	$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
		//	//alert("inputan filter belum diisi !");
		//}

		$scope.GetListIncoming(1, uiGridPageSize);
	};

	//Filter Full Payment Input Name
	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchText != "") &&
			($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchText;
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchText);
			if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchDropdown == "Nomor SO") {
				nomor = 4;
			}
			if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchDropdown == "Nomor Billing") {
				nomor = 6;
			}
			console.log("nomor --> ", nomor);
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchText;
		}
		else {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_GridApi.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	//Filter Service Full Payment Input Name
	// $scope.TambahIncomingPayment_DaftarWorkOrder_Filter_Clicked = function () {
	// 	console.log("TambahIncomingPayment_DaftarWorkOrder_Filter_Clicked");
	// 	if (($scope.TambahIncomingPayment_DaftarWorkOrder_SearchText != "") &&
	// 		($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahIncomingPayment_DaftarWorkOrder_SearchText;
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahIncomingPayment_DaftarWorkOrder_SearchText);
	// 		if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Nomor WO") {
	// 			nomor = 1;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Tanggal WO") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Nomor Billing") {
	// 			nomor = 3;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Nomor Polisi") {
	// 			nomor = 4;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Nama Pelanggan") {
	// 			nomor = 5;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarWorkOrder_SearchText;
	// 	}
	// 	else {
	// 		$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	/// filter baru ======================================================
	$scope.TambahIncomingPayment_DaftarWorkOrder_Filter_Clicked = function () {
		console.log("TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarWorkOrder_SearchText != "") &&
			($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown != "")) {
			var nomor;
			value = $scope.TambahIncomingPayment_DaftarWorkOrder_SearchText;
			$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
			$scope.gridApi_InformasiPembayaran_UIGrid2.grid.getColumn($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown).filters[0].term = value;
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarWorkOrder_SearchText);
			// if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Tanggal Pembayaran") {
			// 	nomor = 3;
			// }
			// console.log("nomor --> ", nomor);
			// $scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.value;
		}
		else {
			$scope.gridApi_InformasiPembayaran_UIGrid2.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};
/// ==================================================================	

	// START PILIH PEMBAYAR
	// $scope.Mandatory = true;
	// console.log("Cek Mandaroty awal >>>>>>", $scope.Mandatory);
	$scope.optionsTambahIncomingNamaPembayar_SelectKolom = [{ name: "NoWO", value: "NoWO" }, { name: "Nama Pembayar", value: "NamaPembayar" }, { name: "No.Polisi", value: "NoPol" }, { name: "Address", value: "Address" }]
	$scope.ModalTambahIncoming_CustomerName = {
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination: true,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false }
		],

		onRegisterApi: function (gridApi) {
			$scope.ModalTambahIncoming_CustomerName_gridAPI = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getDataPembayar(pageNumber, pageSize);
			}
			);
		}
	}

	$scope.TambahIncoming_OtherTypeNamaPembayaran_Click = function(){
		// $scope.Mandatory = true;
		// console.log("Cek Mandaroty >>>>>>", $scope.Mandatory);
		$scope.refresh = true;
		$timeout(function() {
			$scope.refresh = false;
		}, 1000);
		angular.element('#ModalTambahIncoming_CustomerName').modal('setting', { closeable: false }).modal('show');
		$scope.ModalTambahIncoming_CustomerName .columnDefs = [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataNoWO", displayName: "No. WO", field: "NoWO", enableCellEdit: false, enableHiding: false },
			{ name: "DataName", displayName: "Nama Pembayar", field: "NamaPembayar", enableCellEdit: false, enableHiding: false },
			{ name: "DataNopol", displayName: "Nomor Polisi", field: "Nopol", enableCellEdit: false, enableHiding: false },
			{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false }
		];

	}

	$scope.Batal_TambahIncomingNamaPembayar = function (){
		// $scope.Mandatory = true;
		// console.log("Cek Mandaroty Klik Batal >>>>>>", $scope.Mandatory);
		angular.element('#ModalTambahIncoming_CustomerName ').modal('hide');
		console.log("Cek Batal masuk gak yah >>>>>>>>>>>>")
		// if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
		// 	$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		// }
		$scope.ClearFilter();
	}

	$scope.Pilih_TambahIncomingNamaPembayar = function (){
		console.log("Cek PIlih masuk gak yah >>>>>>>>>>>>")
		$scope.Mandatory = false;
		console.log("Cek Mandaroty Pilih>>>>>>", $scope.Mandatory);
		if ($scope.ModalTambahIncoming_CustomerName_gridAPI.selection.getSelectedCount() > 0) {
			$scope.ModalTambahIncoming_CustomerName_gridAPI.selection.getSelectedRows().forEach(function (row) {
				$scope.TambahIncoming_OtherTypeNamaPembayaran = row.NamaPembayar;
				// $scope.TambahInvoiceMasuk_OtherTypeNamaVendor_TujuanPembayaran_IdPelangganVendor = row.Id;
			});

			angular.element('#ModalTambahIncoming_CustomerName').modal('hide');
		}
		else {
			//alert('Belum ada data yang dipilih');
			bsNotify.show({
				title: "Warning",
				content: 'Belum ada data yang dipilih !',
				type: 'warning'
			});
		}
		$scope.ClearFilter();
	}

	// filter
	$scope.TambahIncomingNamaPembayar_Filter_Clicked = function(){
		$scope.getDataPembayar(1, $scope.uiGridPageSize);
	}

	$scope.getDataPembayar = function(pageNumber, pageSize){
		if (pageSize == "" || pageSize == 0 || angular.isUndefined(pageSize)) {
			pageSize = 10;
		}
		IncomingPaymentFactory.getFilterData(pageNumber, pageSize, $scope.TambahIncomingNamaPembayar_TextFilter, $scope.TambahIncomingNamaPembayar_SelectKolom)
		.then(
			function(res){
				$scope.ModalTambahIncoming_CustomerName.data = res.data.Result;
				$scope.ModalTambahIncoming_CustomerName.totalItems = res.data.Total;
				$scope.ModalTambahIncoming_CustomerName.paginationPageSize = pageSize;
				$scope.ModalTambahIncoming_CustomerName.paginationPageSizes = [10, 25, 50];
			},
			function(err){
				console.log("Err===>",err);
			}
		);
		if ($scope.TambahIncomingNamaPembayar_SelectKolom == null || $scope.TambahIncomingNamaPembayar_SelectKolom == '')
				$scope.ModalTambahIncoming_CustomerName_gridAPI.grid.clearAllFilters();
			else
				$scope.ModalTambahIncoming_CustomerName_gridAPI.grid.clearAllFilters();
	
			if ($scope.TambahIncomingNamaPembayar_SelectKolom == 'NoWO')
				nomor = 2;
			else if ($scope.TambahIncomingNamaPembayar_SelectKolom == 'NamaPembayar')
				nomor = 3;
			else if ($scope.TambahIncomingNamaPembayar_SelectKolom == 'NoPol')
				nomor = 4
			else if ($scope.TambahIncomingNamaPembayar_SelectKolom == 'Address')
				nomor =  5
			else
			nomor = 0
	
			if (!angular.isUndefined($scope.TambahIncomingNamaPembayar_SelectKolom) && nomor != 0) {
				$scope.ModalTambahIncoming_CustomerName_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahIncomingNamaPembayar_TextFilter;
			}
			$scope.ModalTambahIncoming_CustomerName_gridAPI.grid.queueGridRefresh();
	}

	$scope.ClearFilter = function(){
		$scope.TambahIncomingNamaPembayar_TextFilter = "";
		$scope.TambahIncomingNamaPembayar_SelectKolom = "";
	}

	$scope.clearCharges = function(){
		$scope.TambahIncomingPayment_Biaya_UIGrid.data = [];
		$scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya = "";
		$scope.TambahIncomingPayment_BiayaBiaya_Nominal = "";
	}
	// END PILIH PEMBAYAR

	//Filter Proforma Invoice
	$scope.TambahIncomingPayment_DaftarNoRangka_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarNoRangka_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarNoRangka_Text != "") &&
			($scope.TambahIncomingPayment_DaftarNoRangka_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarNoRangka_Text;
			$scope.gridApi_TambahIncomingPayment_DaftarNoRangka_UIGrid.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarNoRangka_Text);
			if ($scope.TambahIncomingPayment_DaftarNoRangka_SearchDropdown == "Nomor Rangka") {
				nomor = 3;
			}
			if ($scope.TambahIncomingPayment_DaftarNoRangka_SearchDropdown == "Nomor SPK") {
				nomor = 4;
			}
			if ($scope.TambahIncomingPayment_DaftarNoRangka_SearchDropdown == "Nomor SO") {
				nomor = 5;
			}
			if ($scope.TambahIncomingPayment_DaftarNoRangka_SearchDropdown == "Nama di Billing") {
				nomor = 6;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_TambahIncomingPayment_DaftarNoRangka_UIGrid.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarNoRangka_Text;
		}
		else {
			$scope.gridApi_TambahIncomingPayment_DaftarNoRangka_UIGrid.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarSpk_Brief_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarSpk_Brief_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarSpk_Brief_Search_Text != "") &&
			($scope.TambahIncomingPayment_DaftarSpk_Brief_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarSpk_Brief_Search_Text;
			$scope.GridApiTambahIncomingPayment_UnitFullPaymentSPK.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarSpk_Brief_Search_Text);
			if ($scope.TambahIncomingPayment_DaftarSpk_Brief_SearchDropdown == "Nomor SPK") {
				nomor = 1;
			}
			if ($scope.TambahIncomingPayment_DaftarSpk_Brief_SearchDropdown == "Nama Pelanggan") {
				nomor = 2;
			}
			if ($scope.TambahIncomingPayment_DaftarSpk_Brief_SearchDropdown == "Semua Kolom") {
				nomor = 0;
			}

			console.log("nomor --> ", nomor);
			if (nomor == 0) {
				console.log("masuk 0");
				$scope.GridApiTambahIncomingPayment_UnitFullPaymentSPK.grid.columns.filters[0].term = $scope.TambahIncomingPayment_DaftarSpk_Brief_Search_Text;
			}
			else {
				console.log("masuk else");
				$scope.GridApiTambahIncomingPayment_UnitFullPaymentSPK.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarSpk_Brief_Search_Text;
			}
		}
		else {
			$scope.GridApiTambahIncomingPayment_UnitFullPaymentSPK.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarSpk_Detail_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarSpk_Detail_Filter_Clicked");

		if (($scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text != "") &&
			($scope.TambahIncomingPayment_DaftarSpk_Detail_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text;
			$scope.gridApi_DaftarSpk.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text);
			if ($scope.TambahIncomingPayment_DaftarSpk_Detail_SearchDropdown == "Nomor SPK") {
				nomor = 1;
			}
			else if ($scope.TambahIncomingPayment_DaftarSpk_Detail_SearchDropdown == "Nama Pelanggan") {
				nomor = 2;
			}
			// if ($scope.TambahIncomingPayment_DaftarSpk_Brief_SearchDropdown == "Semua Kolom") {
			else {
				nomor = 0;
			}
			console.log($scope.TambahIncomingPayment_DaftarSpk_Detail_SearchDropdown);
			console.log("nomor --> ", nomor);
			if (nomor == 0) {
				console.log("masuk 0");
				$scope.gridApi_DaftarSpk.grid.columns.filters[1].term = $scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text;
				$scope.gridApi_DaftarSpk.grid.columns.filters[2].term = $scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text;
			}
			else {
				console.log("masuk else");
				$scope.gridApi_DaftarSpk.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarSpk_Detail_Search_Text;
			}
		}
		else {
			$scope.gridApi_DaftarSpk.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Text != "") &&
			($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Text;
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Text);
			if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_SearchDropdown == "Nomor SO") {
				nomor = 3;
			}
			// if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_SearchDropdown == "Metode Bayar") {
			// nomor = 4;
			// }
			console.log("nomor --> ", nomor);
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_Text;
		}
		else {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_button_SearchBy = function () {
		console.log("TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_button_SearchBy");
		if (($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search_Text != "") &&
			($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search_Text;
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_GridApi.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search_Text);
			if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_SearchDropdown == "Nomor SO") {
				nomor = 4;
			}
			// if ($scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_SearchDropdown == "Metode Bayar") {
			// nomor = 4;
			// }
			console.log("nomor --> ", nomor);
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_GridApi.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_Search_Text;
		}
		else {
			$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_GridApi.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	// $scope.TambahIncomingPayment_DaftarArCreditDebit_Click = function () {
	// 	console.log("TambahIncomingPayment_DaftarArCreditDebit_Click");
	// 	if (($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText != "") &&
	// 		($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText;
	// 		$scope.TambahIncomingPayment_DaftarArCreditDebit_gridAPI.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText);
	// 		if ($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Tanggal AR Card") {
	// 			nomor = 1;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Tipe Kartu") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Nominal") {
	// 			nomor = 3;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.TambahIncomingPayment_DaftarArCreditDebit_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarArCreditDebit_SearchText;
	// 	}
	// 	else {
	// 		$scope.TambahIncomingPayment_DaftarArCreditDebit_gridAPI.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	// NEW FILTER =========================================
	$scope.TambahIncomingPayment_DaftarArCreditDebit_Click = function () {
		$scope.GetListUnknownCards(1, uiGridPageSize);
	}
	// ====================================================

	$scope.LihatIncomingPayment_DaftarArCreditDebit_Click = function () {
		console.log("LihatIncomingPayment_DaftarArCreditDebit_Click");
		if (($scope.LihatIncomingPayment_DaftarArCreditDebit_Search_Text != "") &&
			($scope.LihatIncomingPayment_DaftarArCreditDebit_SearchDropdown != "")) {
			var nomor;
			var value = $scope.LihatIncomingPayment_DaftarArCreditDebit_Search_Text;
			$scope.LihatIncomingPayment_DaftarAR_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.LihatIncomingPayment_DaftarArCreditDebit_Search_Text);
			if ($scope.LihatIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Tanggal AR Card") {
				nomor = 1;
			}
			if ($scope.LihatIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Tipe Kartu") {
				nomor = 2;
			}
			if ($scope.LihatIncomingPayment_DaftarArCreditDebit_SearchDropdown == "Nominal") {
				nomor = 3;
			}
			console.log("nomor --> ", nomor);
			$scope.LihatIncomingPayment_DaftarAR_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatIncomingPayment_DaftarArCreditDebit_Search_Text;
		}
		else {
			$scope.LihatIncomingPayment_DaftarAR_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarARRefundLeasing_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarARRefundLeasing_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarARRefundLeasing_Text != "") &&
			($scope.TambahIncomingPayment_DaftarARRefundLeasing_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarARRefundLeasing_Text;
			$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarARRefundLeasing_Text);
			if ($scope.TambahIncomingPayment_DaftarARRefundLeasing_SearchDropdown == "Nomor SO") {
				nomor = 5;
			}
			if ($scope.TambahIncomingPayment_DaftarARRefundLeasing_SearchDropdown == "Nomor SPK") {
				nomor = 4;
			}
			if ($scope.TambahIncomingPayment_DaftarARRefundLeasing_SearchDropdown == "Nomor Rangka") {
				nomor = 3;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarARRefundLeasing_Text;
		}
		else {
			$scope.gridApi_TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_DaftarInterbranch_Filter_Clicked = function () {
		console.log("TambahIncomingPayment_DaftarInterbranch_Filter_Clicked");
		if (($scope.TambahIncomingPayment_DaftarInterbranch_Search_Text != "") &&
			($scope.TambahIncomingPayment_DaftarInterbranch_Search != "")) {
			var nomor;
			var value = $scope.TambahIncomingPayment_DaftarInterbranch_Search_Text;
			$scope.gridApi_Interbranch.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahIncomingPayment_DaftarInterbranch_Search_Text);
			if ($scope.TambahIncomingPayment_DaftarInterbranch_Search == "Nama Branch Penerima") {
				nomor = 1;
			}
			if ($scope.TambahIncomingPayment_DaftarInterbranch_Search == "Nomor Incoming Payment") {
				nomor = 2;
			}
			if ($scope.TambahIncomingPayment_DaftarInterbranch_Search == "Tanggal Incoming Payment") {
				nomor = 3;
			}
			if ($scope.TambahIncomingPayment_DaftarInterbranch_Search == "Nominal") {
				nomor = 4;
			}
			console.log("nomor --> ", nomor);
			$scope.gridApi_Interbranch.grid.columns[nomor].filters[0].term = $scope.TambahIncomingPayment_DaftarInterbranch_Search_Text;
		}
		else {
			$scope.gridApi_Interbranch.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};

	$scope.TambahIncomingPayment_NoRangka = function () {
		$scope.TambahIncomingPayment_NoRangkaSubmitData = [];

		if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)" && $scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
			angular.forEach($scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data, function (value, key) {
				var tmpTotPembayaran = value.Nominal - parseInt($scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer)
				var obj = { "SOId": value.SoId, "PaidAmount": tmpTotPembayaran };
				console.log("Object of SO ==>", obj);	
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_NoRangkaSubmitData.push(obj);
			}
			);
		}else{
			angular.forEach($scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data, function (value, key) {
				var obj = { "SOId": value.SoId, "PaidAmount": value.Nominal };
				console.log("Object of SO ==>", obj);	
				console.log("Object of Value ==>", value);
				$scope.TambahIncomingPayment_NoRangkaSubmitData.push(obj);
			}
			);
		}
		
	}

	$scope.TambahIncomingPayment_RefundLeasing = function () {
		$scope.TambahIncomingPayment_RefundLeasingSubmitData = [];
		angular.forEach($scope.TambahIncomingPayment_DaftarARRefundLeasingDibayar_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.RefundLeasingId, "PaidAmount": value.NominalRefund };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahIncomingPayment_RefundLeasingSubmitData.push(obj);
		}
		);


		var obj = { "SOId": $scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == 2 ? 'Leasing|' + $scope.TambahIncomingPayment_Top_NamaLeasing : 'Insurance|' + $scope.TambahIncomingPayment_Top_NamaLeasing, "PaidAmount": 0 };
		$scope.TambahIncomingPayment_RefundLeasingSubmitData.push(obj);
	}

	$scope.TambahIncomingPayment_ClearAllData = function () {
		//Informasi
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_ToyotaId = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_TanggalAppointment = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Handphone = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_Alamat = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_KodeBranch = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_Alamat = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaBranch = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_KabupatenKota = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_NamaPt = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Branch_Telepon = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_ProspectId = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Alamat = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Prospect_Handphone = "";
		//Grid
		$scope.TambahIncomingPayment_InformasiPembayaran_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarWorkOrder_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data = [];
		$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarsalesOrderDibayar_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSo_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoMo_UIGrid.data = [];
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Interbranch_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarSpk_UIGrid.data = [];
		$scope.TambahIncomingPayment_UnitFullPaymentSPK_UIGrid.data = [];
		//Pembayaran
		//$scope.TambahIncomingPayment_TanggalIncomingPayment= "";
		$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = 0;
		$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_StatementCard = 0;
		$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer = 0;
		$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_BankTransfer = "";
		$scope.TambahIncomingPayment_RincianPembayaran_Keterangan_BankTransfer = "";
		$scope.TambahIncomingPayment_BankTransfer_RekeningPenerima = "";
		$scope.TambahIncomingPayment_BankTransfer_BankPengirim = "";
		$scope.TambahIncomingPayment_RincianPembayaran_NamaPengirim_BankTransfer = "";
		$scope.TambahIncomingPayment_OwnRisk = false;
		$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash = "";
		$scope.TambahIncomingPayment_RincianPembayaran_Keterangan_Cash = "";
		$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash = "";
		$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_Cash = 0;
		$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_CreditDebitCard = 0;
		$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard = "";
		$scope.TambahIncomingPayment_RincianPembayaran_Keterangan_CreditDebitCard = "";
		$scope.TambahIncomingPayment_CreditDebitCard_NamaBank = "";
		$scope.TambahIncomingPayment_RincianPembayaran_NomorKartu_CreditDebitCard = "";
		$scope.TambahIncomingPayment_SearchBy_NomorProforma = "";
		//Filter
		$scope.TambahIncomingPayment_DaftarWorkOrder_SearchText = "";
		$scope.TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill_SearchText = "";
		$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text = "";
		//SearchBy
		$scope.TambahIncomingPayment_SearchBy_NamaPelanggan = "";
		$scope.TambahIncomingPayment_SearchBy_NomorSo = "";
		$scope.TambahIncomingPayment_SearchBy_NamaSalesman = "";
		$scope.TambahIncomingPayment_SearchBy_NomorSpk = "";
		$scope.TambahIncomingPayment_SearchBy_NamaLeasing = "";
		$scope.TambahIncomingPayment_SearchBy_NamaPelangganPt = "";
		$scope.TambahIncomingPayment_SearchBy_NomorAppointment = "";
		$scope.TambahIncomingPayment_SearchBy_NomorWo = "";
		$scope.TambahIncomingPayment_SearchBy_NamaAsuransi = "";
		$scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran = "";
		$scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya = "";
		$scope.TambahIncomingPayment_BiayaBiaya_DebetKredit = "";
		$scope.TambahIncomingPayment_BiayaBiaya_Nominal = 0;
		$scope.TambahIncomingPayment_Biaya = 0;
		$scope.TambahIncomingPayment_Biaya_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarARRefundLeasing_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarArCreditDebit_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = [];
		$scope.TambahIncomingPayment_DaftarNoRangka_UIGrid.data = [];
		$scope.ModalTolakIncomingPayment_AlasanPenolakan = "";
		$scope.TambahIncomingPayment_SearchBy_NamaBank = "";
		$scope.Cetak = "N";
		$scope.CetakTTUS = "N";
		$scope.TambahIncomingPayment_NamaBranch_SearchDropdown = "";
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_Alamat = "";
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_KodeBranch = "";
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_KabupatenKota = "";
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_Telepon = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Lain_NamaPelanggan = "";
		$scope.TambahIncomingPayment_InformasiPelangan_Lain_UntukPembayaran = "";
		$scope.TambahIncomingPayment_BiayaBiaya_Nominal = 0;
		$scope.TambahIncomingPayment_BiayaBiaya_Keterangan = "";
		$scope.TambahIncomingPayment_BiayaBiaya_NoFaktur = "";
		$scope.TambahIncomingPayment_NomorKuitansi = "";
		$scope.TambahIncomingPayment_NomorKuitansi_text = "";

		$scope.TambahIncomingPayment_KeteranganKuitansi = "";
		$scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal = "";
		$scope.ModalTolakIncomingPayment_AlasanPenolakan = "";
		$scope.LihatIncomingPayment_RincianPembayaran_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_UIGrid.data = [];
		$scope.LihatIncomingPayment_Top_NomorSpk = "";
		$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.LihatIncomingPayment_Top_NomorSo = "";
		$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = [];
		$scope.LihatIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_WoBill_UIGrid.data = [];
		$scope.LihatIncomingPayment_Top_NomorWo = "";
		$scope.LihatIncomingPayment_InformasiPembayaran_dariTambah_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_Appointment_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_WoDown_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = [];
		$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = [];
		$scope.LihatIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data = [];
		$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = [];
		$scope.LihatIncomingPayment_Top_NamaLeasing = "";
		$scope.LihatIncomingPayment_DaftarAR_UIGrid.data = [];
		$scope.LihatIncomingPayment_Biaya = 0;
		$scope.LihatIncomingPayment_Top_NamaBank = "";
		$scope.currentCustomerId = 0;
		$scope.TotalPembayaranAll = 0;		$scope.currentTglAR =  "";
		$scope.currentTglAR =  "";
		$scope.currentCardType = "";
		$scope.currentNominal = 0; 
		$scope.TambahIncomingPayment_BiayaBiaya_Assignment = "";
		$scope.TujuanPembayaran_CashDelivery = "";
		$scope.TambahIncomingPayment_CashDelivery_NamaBank = "";
		$scope.TambahIncomingPayment_CashDelivery_NamaRekening = "";	

	}
	//Alvin, 20170505, end

	/*===============================Bulk Action Section, Begin===============================*/
	var tipePengajuan;
	$scope.LihatIncomingPayment_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");

		console.log("Cek Rights");
		$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
		console.log($scope.allowApprove);

		if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Setuju") {
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakIncomingPayment_IPId = value.IPParentId;
				$scope.ModalTolakIncomingPayment_TipePengajuan = value.IncomingPaymentType;
				if ($scope.allowApprove == false) {
					// alert("Anda Tidak Berhak");
					bsNotify.show({
						title: "Warning",
						content: "Anda Tidak Berhak",
						type: 'warning'
					});
				}
				else {
					if (value.StatusPengajuan == "Diajukan") {
						tipePengajuan = value.TipePengajuan;

						$scope.CreateApprovalData("Disetujui", tipePengajuan);
						IncomingPaymentFactory.updateApprovalData($scope.ApprovalData)
							.then(
								function (res) {
									// alert("Berhasil Disetujui");
									bsNotify.show({
										title: "Berhasil",
										content: "Berhasil Disetujui",
										type: 'success'
									});
									$scope.Batal_LihatIncomingPayment_TolakModal();
								},

								function (err) {
									// alert("Persetujuan gagal");
									bsNotify.show(
										{

											title: "Notifikasi",
											content: "Persetujuan gagal. " + err.data.Message,
											type: 'danger'
										}
									);
								}
							);
					}
				}
			});
		}
		else if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI);
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
				console.log(value);
				$scope.ModalTolakIncomingPayment_IPId = value.IPParentId;
				//$scope.ModalTolakIncomingPayment_Tanggal = value.TanggalIncoming;
				$scope.ModalTolakIncomingPayment_Tanggal = $filter('date')(new Date(value.TanggalIncoming), 'dd/MM/yyyy');
				$scope.ModalTolakIncomingPayment_NomorIP = value.IncomingNo;
				$scope.ModalTolakIncomingPayment_TipeIP = value.IncomingPaymentType;
				$scope.ModalTolakIncomingPayment_NamaPelanggan = value.CustomerName;
				$scope.ModalTolakIncomingPayment_MetodePembayaran = value.PaymentMethod;
				$scope.ModalTolakIncomingPayment_Nominal = value.Nominal;
				$scope.ModalTolakIncomingPayment_TipePengajuan = value.TipePengajuan;
				$scope.ModalTolakIncomingPayment_TanggalPengajuan = value.TglPengajuan;
				$scope.ModalTolakIncomingPayment_AlasanPengajuan = value.AlasanPengajuan;

				tipePengajuan = value.TipePengajuan;

				if ($scope.allowApprove == false) {
					bsNotify.show({
						title: "Warning",
						content: "Anda Tidak Berhak",
						type: 'warning'
					});
				}
				else {
					if (value.StatusPengajuan == "Diajukan")
						angular.element('#ModalTolakIncomingPayment').modal('show');
				}
			}
			);
		}

	}

	$scope.CreateApprovalData = function (ApprovalStatus,tipePengajuan) {

		if(tipePengajuan == "Backdate"){
			$scope.ApprovalData = [
				{
					"ApprovalTypeName": "Backdate",
					"RejectedReason": $scope.ModalTolakIncomingPayment_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakIncomingPayment_IPId,
					"ApprovalStatusName": ApprovalStatus,
				}
			];
		}else{
			$scope.ApprovalData = [
				{
					"ApprovalTypeName": "IncomingPayment Reversal",
					"RejectedReason": $scope.ModalTolakIncomingPayment_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakIncomingPayment_IPId,
					"ApprovalStatusName": ApprovalStatus,
				}
			];
		}
	}

	$scope.Batal_LihatIncomingPayment_TolakModal = function () {
		angular.element('#ModalTolakIncomingPayment').modal('hide');
		$scope.LihatIncomingPayment_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListIncoming(1, uiGridPageSize);
	}

	$scope.Pengajuan_LihatIncomingPayment_TolakModal = function () {
		// if ($scope.ModalTolakIncomingPayment_AlasanPenolakan == "") {
		if (angular.isUndefined($scope.ModalTolakIncomingPayment_AlasanPenolakan) || $scope.ModalTolakIncomingPayment_AlasanPenolakan == "") {
			bsNotify.show({
				title: "Warning",
				content: "Alasan Penolakan harus diisi",
				type: 'warning'
			});
			return false;
		}
		$scope.CreateApprovalData("Ditolak",tipePengajuan);
		IncomingPaymentFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function (res) {
					bsNotify.show({
						title: "Berhasil",
						content: "Berhasil tolak",
						type: 'success'
					});
					$scope.Batal_LihatIncomingPayment_TolakModal();
				},

				function (err) {
					bsNotify.show({
						title: "Notifikasi",
						content: "Penolakan gagal",
						type: 'danger'
					});
				}
			);
	};

	$scope.ClearActionFields = function () {
		$scope.ModalTolakIncomingPayment_IPId = "";
		$scope.ModalTolakIncomingPayment_Tanggal = "";
		$scope.ModalTolakIncomingPayment_NomorIP = "";
		$scope.ModalTolakIncomingPayment_TipeIP = "";
		$scope.ModalTolakIncomingPayment_NamaPelanggan = "";
		$scope.ModalTolakIncomingPayment_MetodeBayar = "";
		$scope.ModalTolakIncomingPayment_Nominal = "";
		$scope.ModalTolakIncomingPayment_TipePengajuan = "";
		$scope.ModalTolakIncomingPayment_TanggalPengajuan = "";
		$scope.ModalTolakIncomingPayment_AlasanPengajuan = "";
	};
	/*===============================Bulk Action Section, End===============================*/
	/*=================================Cetak Kuitansi Begin===================================*/

	$scope.LihatIncomingPayment_Cetak = function () {
		$scope.CetakIPParentId = $scope.currentIPParentId;
		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			$scope.CetakKuitansiTTUS();
		}
		else {
			$scope.CetakKuitansi();
		}
		$scope.LihatIncomingPayment_Batal();
		$scope.GetListIncoming(1, uiGridPageSize);
	}

	$scope.LihatIncomingPayment_SimpanCetak = function () {
		
		var data = [];
		if($scope.LihatIncomingPayment_NomorKuitansi == null || $scope.LihatIncomingPayment_NomorKuitansi == undefined || $scope.LihatIncomingPayment_NomorKuitansi == ""){
			bsNotify.show({
				title: "warning",
				content: "Kuitansi Preprint Harus di Pilih",
				type: 'warning'
			});
			return;
		}
		data.push({"IPParentId": $scope.currentIPParentId, "PreprintedNumberId": $scope.LihatIncomingPayment_NomorKuitansi })
		IncomingPaymentFactory.KuitansiSave(data)
			.then(
				function (res) {
					console.log("Berhasil menyimpan");
					$scope.LihatIncomingPayment_Cetak();
				}
			);
	}

	$scope.CetakKuitansi = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak Kuitansi");
		console.log($scope.CetakIPParentId);

		IncomingPaymentFactory.getCetakKuitansi($scope.CetakIPParentId)
			.then(
				function (res) {
					// debugger;
					// var ea = $scope.user;
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);


					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.CetakKuitansiTTUS = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak TTUS");
		console.log($scope.CetakIPParentId);

		IncomingPaymentFactory.getCetakTTUS($scope.CetakIPParentId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);


					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};
	/*==================================Cetak Kuitansi End====================================*/


	$scope.LihatIncomingPayment_PengajuanReversalModal = function () {
		var numLength = angular.element('.ui.modal.ModalLihatIncomingPayment_PengajuanReversal').length;
                    setTimeout(function () {
                        angular.element('.ui.modal.ModalLihatIncomingPayment_PengajuanReversal').modal('refresh');
                    }, 0);

                    if (numLength > 1) {
                        angular.element('.ui.modal.ModalLihatIncomingPayment_PengajuanReversal').not(':first').remove(); // ini destroy sisain yg pertama aja (first)
                    }
		angular.element('.ui.modal.ModalLihatIncomingPayment_PengajuanReversal').modal('show');
	}


	///SEARCH PROSPECT 
	$scope.ShowModalSearchProspect = function () {

		IncomingPaymentFactory.getDataPandC($scope.TambahIncomingPayment_SearchBy_NamaPelanggan)
			.then(
				function (res) {

					$scope.prospectGrid.data = res.data.Result;
					if (res.data.Result.length > 0)
						console.log("sample", res.data.Result[0]);
				}
			);

		angular.element('#ModalSearchProspectID_Incoming').modal('setting', { closeable: false }).modal('show');
	}
	$scope.ModalSearchProspect_Batal = function () {
		angular.element('#ModalSearchProspectID_Incoming').modal('hide');
	}
	$scope.SearchProspectPilihRow = function (row) {
		//RegisterCheque_Data.Name
		console.log("ProspectCustomer Selected", row.entity)
		$scope.Customer_Data.CustomerId = row.entity.CustomerId;
		$scope.Customer_Data.ProspectId = row.entity.ProspectId;
		$scope.Customer_Data.City = row.entity.City;
		$scope.Customer_Data.Address = row.entity.Address;
		$scope.Customer_Data.Phone = row.entity.Phone;
		$scope.Customer_Data.Name = row.entity.Name;
		$scope.TambahIncomingPayment_SearchBy_NamaPelanggan = row.entity.Name;
		console.log("CurrentDataToEditWithCust", $scope.Customer_Data);

		if (($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment")
			|| ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment")) {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.TambahIncomingPayment_UnitFullPaymentSPK_Paging(1);
			}
		}

		angular.element('#ModalSearchProspectID_Incoming').modal('hide');
	}
	$scope.prospectGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10, 20],
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'ProspectId', field: 'ProspectId', visible: false },
			{ name: 'Name', displayName: 'Nama Pelanggan', field: 'Name', enableFiltering: true },
			{ name: 'Address', displayName: 'Alamat', field: 'Address', enableFiltering: true },
			{ name: 'City', displayName: 'Kabupaten/Kota', field: 'City', enableFiltering: true },
			{ name: 'Phone', displayName: 'Phone', field: 'Phone', enableFiltering: true },
			{
				name: 'Action', enableFiltering: false,
				cellTemplate: '<a ng-click="grid.appScope.SearchProspectPilihRow(row)">Pilih</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.grid2Api = gridApi;
		}
	};


	///SEARCH SALES 
	$scope.ShowModalSearchSales = function () {
		IncomingPaymentFactory.getDataSales($scope.TambahIncomingPayment_SearchBy_NamaSalesman)
			.then(
				function (res) {

					$scope.salesGrid.data = res.data.Result;
					if (res.data.Result.length > 0)
						console.log("sample", res.data.Result[0]);
				}
			);

		angular.element('#ModalSearchSales').modal('setting', { closeable: false }).modal('show');
	}
	$scope.ModalSearchSales_Batal = function () {
		angular.element('#ModalSearchSales').modal('hide');
	}
	$scope.SearchSalesPilihRow = function (row) {
		//RegisterCheque_Data.Name
		console.log("Sales Selected", row.entity)

		$scope.TambahIncomingPayment_SearchBy_NamaSalesman = row.entity.EmployeeName;
		//console.log("CurrentDataToEditWithSales" , $scope.Customer_Data);

		if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
			if ($scope.TambahIncomingPayment_TujuanPembayaran_SearchBy == "3") {
				$scope.getDataBySalesName();
			}
		}

		angular.element('#ModalSearchSales').modal('hide');
	}
	$scope.salesGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10, 20],
		columnDefs: [
			{ name: 'EmployeeId', field: 'EmployeeId', visible: true },
			{ name: 'EmployeeName', displayName: 'Employee Name', field: 'EmployeeName', enableFiltering: true },
			{
				name: 'Action', enableFiltering: false,
				cellTemplate: '<a ng-click="grid.appScope.SearchSalesPilihRow(row)">Pilih</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.grid2Api = gridApi;
		}
	};

	$scope.TambahIncomingPayment_BankTransfer_NoCheque_Selected_Changed = function (X1) {
		if (!angular.isUndefined($scope.TambahIncomingPayment_BankTransfer_NoCheque)) {
			IncomingPaymentFactory.getDataChequeById($scope.TambahIncomingPayment_BankTransfer_NoCheque)
				.then(
					function (res) {
						console.log("Cheque by id =>> ", res);
						$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = res.data[0].nominal;

						$('.btn-save').removeAttr('disabled');
						var PembayaranDP = 0;
						var TotalDP = 0;
						if($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment"){
							$scope.TambahIncomingPayment_UnitFullPaymentSO_UIGrid.data.forEach(function (obj) {
								if (obj.Pembayaran != null) {
									PembayaranDP = obj.Pembayaran;
								}
								else {
									PembayaranDP = 0;
								}
									TotalDP += PembayaranDP;
							});
						}else{
							$scope.TambahIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data.forEach(function (obj) {
								if (obj.SisaPembayaran != null) {
									PembayaranDP = obj.SisaPembayaran;
								}
								else {
									PembayaranDP = 0;
								}
								TotalDP += PembayaranDP;
							});
						}
						
						$scope.TambahIncomingPayment_RincianPembayaran_Selisih_BankTransfer =
							$scope.TambahIncomingPayment_RincianPembayaran_NominalPembayaran_BankTransfer.toString().replace(/,/g, "") - TotalDP;

					}
				);
		}
	}

	$scope.GetDataPreprintedSetting = function () {
		IncomingPaymentFactory.getDataPreprintedSetting()
			.then(
				function (res) {
					$scope.PreprintedSettingData = res.data.Result;
				},
				function(err){
					console.log("Error",err)
				}
			);
	}

	// ============================ BEGIN | NOTO | 190124 | Cash Operation - Outgoing Payment Grid ============================
	$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableSelectAll: false,
		enableColumnResizing: true,
		multiSelect: false,
		enableFiltering: true,
		columnDefs: [
			{ name: "OPId", displayName: "Outgoing Payment Id", field: "OPId", visible: false },
			{ name: "OutgoingPaymentNumber", displayName: "Nomor Outgoing Payment", field: "OutgoingPaymentNumber", enableHiding: false, visible: true, enableCellEdit: false },
			{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
			{ name: "Description", displayName: "Keterangan", field: "Description", enableCellEdit: false, visible: true },
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_gridAPI = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.getCashOperationOutgoing(pageNumber, pageSize);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("selection ROW", row)
				if (row.isSelected == true) {
					$scope.currentOPId = row.entity.OPId;
					$scope.currentOPNominal = row.entity.Nominal;
					console.log('OPId -> ' + $scope.currentOPId, 'OP Nominal -> ' + $scope.currentOPNominal);
				}
				else {
					$scope.currentOPId = "";
					$scope.currentOPNominal = "";
				}
			});

		}
	};

	$scope.getCashOperationOutgoing = function (start, limit) {
		// var parameter = [{ Tanggal: $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation }];
		$scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation = $filter('date')(new Date(), 'yyyy-MM-dd');
		var parameter = $scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CashOperation + '|0';
		IncomingPaymentFactory.getCashOperationOutgoing(start, limit, parameter)
			.then(
				function (res) {
					$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_UIGrid.data = res.data.Result;
					$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_UIGrid.totalItems = res.data.Total;
					$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_UIGrid.paginationPageSize = limit;
					$scope.TambahIncomingPayment_RincianPembayaran_OutgoingPayment_UIGrid.paginationPageSizes = [10, 25, 50];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	//ppenambahan irfan//

	// $scope.TambahIncomingPayment_DaftarWorkOrderDibayar_UIGrid.onRegisterApi = function(gridApi){
	// 	$scope.gridApi = gridApi;

		
	// }
	// ============================ END   | NOTO | 190124 | Cash Operation - Outgoing Payment Grid ============================

	// ============================ BEGIN | NOTO | 180516 | TUNING PERFORMANCE | NG-IF ============================

	//begin | NOTO | 180516 | TAMBAH | Onchange Tipe Incoming -> Pilih Radio button
	function DisableAll_TambahIncomingPayment_SearchBy() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySpkSales = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySpkPelanggan = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelangganPt = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByAppointmentWo = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByWoPelanggan = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_CashDelivery = false;
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByLeasingInsurance = false;

	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySpkSales() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySpkSales = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySpkPelanggan() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySpkPelanggan = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelangganPt() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelangganPt = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchBySoPelanggan = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchByAppointmentWo() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByAppointmentWo = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchByWoPelanggan() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByWoPelanggan = true;
	}

	function Enable_TambahIncomingPayment_TujuanPembayaran_SearchByLeasingInsurance() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_SearchByLeasingInsurance = true;
	}
	//end | NOTO | 180516 | TAMBAH | Onchange Tipe Incoming -> Pilih Radio button

	//begin | NOTO | 180516 | TAMBAH | Onchange Radio button -> Search By
	function DisableAll_SearchByRadioResult() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorSpk = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaSalesman = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPelanggan = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NomorProforma = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaLeasing = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPembayar = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NomorSo = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPelangganPt = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NomorAppointment = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NomorWo = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaAsuransi = false;
		$scope.Show_TambahIncomingPayment_SearchBy_NamaBank = false;
		$scope.TambahIncomingPayment_SearchBy_CariBtn = false;
	}

	function Enable_TambahIncomingPayment_SearchBy_NomorSpk() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorSpk = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaSalesman() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaSalesman = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaPelanggan() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPelanggan = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NomorProforma() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorProforma = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaLeasing() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaLeasing = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaPembayar() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPembayar = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NomorSo() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorSo = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaPelangganPt() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaPelangganPt = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NomorAppointment() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorAppointment = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NomorWo() {
		$scope.Show_TambahIncomingPayment_SearchBy_NomorWo = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaAsuransi() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaAsuransi = true;
	}

	function Enable_TambahIncomingPayment_SearchBy_NamaBank() {
		$scope.Show_TambahIncomingPayment_SearchBy_NamaBank = true;
	}

	function Enable_SearchByCariBtn() {
		$scope.TambahIncomingPayment_SearchBy_CariBtn = true;
	}

	//end | NOTO | 180516 | TAMBAH | Onchange Radio button -> Search By

	//begin | NOTO | 180517 | TAMBAH | Onclick button Cari -> Show/Hide Grid
	function Enable_TambahIncomingPayment_TujuanPembayaran_CashDelivery() {
		$scope.Show_TambahIncomingPayment_TujuanPembayaran_CashDelivery = true;
	}

	function DisableAll_TambahIncomingPayment_DaftarSpk() {
		$scope.Show_TambahIncomingPayment_DaftarSpk = false;
		$scope.Show_TambahIncomingPayment_DaftarSpk_Detail = false;
		$scope.Show_TambahIncomingPayment_DaftarSpk_Brief = false;
	}

	function Enable_TambahIncomingPayment_DaftarSpk_Detail() {
		$scope.Show_TambahIncomingPayment_DaftarSpk = true;
		$scope.Show_TambahIncomingPayment_DaftarSpk_Detail = true;
	}

	function Enable_TambahIncomingPayment_DaftarSpk_Brief() {
		$scope.Show_TambahIncomingPayment_DaftarSpk = true;
		$scope.Show_TambahIncomingPayment_DaftarSpk_Brief = true;
	}

	function DisableAll_TambahIncomingPayment_InformasiPelanggan() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = false;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Lain = false;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Prospect = false;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Branch = false;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota = false;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_PelangganBranch = false;
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch = false;
	}

	function Enable_TambahIncomingPayment_Interbranch_InformasiBranch() {
		$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch = true;
	}

	function Enable_TambahIncomingPayment_InformasiPelangan_Lain() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = true;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Lain = true;
	}

	function Enable_TambahIncomingPayment_InformasiPelangan_Prospect() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = true;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Prospect = true;
	}

	function Enable_TambahIncomingPayment_InformasiPelangan_Branch() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = true;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Branch = true;
	}

	function Enable_TambahIncomingPayment_InformasiPelangan_Toyota() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = true;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_Toyota = true;
	}

	function Enable_TambahIncomingPayment_InformasiPelangan_PelangganBranch() {
		$scope.Show_TambahIncomingPayment_InformasiPelangan = true;
		$scope.Show_TambahIncomingPayment_InformasiPelangan_PelangganBranch = true;
	}

	function DisableAll_TambahIncomingPayment_SalesOrder() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrder = false;
	}

	function Enable_TambahIncomingPayment_DaftarSalesOrder_SoSoDpPem() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrder = true;
	}

	function DisableAll_TambahIncomingPayment_DaftarSalesOrderRadio() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio = false;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = false;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoSo = false;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill = false;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderDibayar = false;
	}

	function Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio = true;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = true;
	}

	function Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSo() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio = true;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoSo = true;
	}

	function Enable_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill() {
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio = true;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoMo = false;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderRadio_SoSoBill = true;
		$scope.Show_TambahIncomingPayment_DaftarSalesOrderDibayar = true;
	}

	function DisableAll_TambahIncomingPayment_InformasiPembayaran() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Spk = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_SoBill = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Appointment = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_WoDown = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_WoBill = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_SoDown = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Materai = false;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_Spk() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Spk = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_SoBill() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_SoBill = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_Appointment() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Appointment = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_WoDown() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_WoDown = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_WoBill() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_WoBill = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_SoDown() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = true;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_SoDown = true;
	}

	function Enable_TambahIncomingPayment_InformasiPembayaran_Materai() {
		$scope.Show_TambahIncomingPayment_InformasiPembayaran = false;
		$scope.Show_TambahIncomingPayment_InformasiPembayaran_Materai = true;
	}

	function Disable_TambahIncomingPayment_KumpulanNomorRangka() {
		$scope.Show_TambahIncomingPayment_DaftarNoRangka = false;
		$scope.Show_TambahIncomingPayment_DaftarNoRangkaDibayar = false;
	}

	function Enable_TambahIncomingPayment_KumpulanNomorRangka() {
		$scope.Show_TambahIncomingPayment_DaftarNoRangka = true;
		$scope.Show_TambahIncomingPayment_DaftarNoRangkaDibayar = true;
	}

	function Disable_TambahIncomingPayment_KumpulanARRefundLeasing() {
		$scope.Show_TambahIncomingPayment_DaftarARRefundLeasing = false;
		$scope.Show_TambahIncomingPayment_DaftarARRefundLeasingDibayar = false;
	}

	function Enable_TambahIncomingPayment_KumpulanARRefundLeasing() {
		$scope.Show_TambahIncomingPayment_DaftarARRefundLeasing = true;
		$scope.Show_TambahIncomingPayment_DaftarARRefundLeasingDibayar = true;
	}

	function Disable_TambahIncomingPayment_KumpulanWorkOrder() {
		$scope.Show_TambahIncomingPayment_DaftarWorkOrder = false;
		$scope.Show_TambahIncomingPayment_DaftarWorkOrderDibayar = false;
	}

	function Enable_TambahIncomingPayment_KumpulanWorkOrder() {
		$scope.Show_TambahIncomingPayment_DaftarWorkOrder = true;
		$scope.Show_TambahIncomingPayment_DaftarWorkOrderDibayar = true;
	}

	function Disable_TambahIncomingPayment_DaftarArCreditDebit() {
		$scope.Show_TambahIncomingPayment_DaftarArCreditDebit = false;
	}

	function Enable_TambahIncomingPayment_DaftarArCreditDebit() {
		$scope.Show_TambahIncomingPayment_DaftarArCreditDebit = true;
	}
	//end | NOTO | 180517 | TAMBAH | Onclick button Cari -> Show/Hide Grid

	//begin | NOTO | 180524 | TAMBAH | Rencana Pembayaran
	function ShowMetodePembayaranDetails_BankTransfer() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer1 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer2 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer3 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer6 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer7 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer8 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer9 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer10 = true;
	}

	function HideMetodePembayaranDetails_BankTransfer() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer1 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer2 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer3 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer4b = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer5b = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer6 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer7 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer8 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer9 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_BankTransfer10 = false;
	}

	function ShowMetodePembayaranDetails_Cash() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash1 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash2 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash3 = true;
	}

	function HideMetodePembayaranDetails_Cash() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash1 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash2 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_Cash3 = false;
	}

	function ShowMetodePembayaranDetails_CashOperation() {
		$scope.Show_TambahIncomingPayment_RincianPembayaran_CashOperation = true;
		HideMetodePembayaranDetails_BankTransfer();
		HideMetodePembayaranDetails_Cash();
		HideMetodePembayaranDetails_CreditDebitCard();
	}

	function HideMetodePembayaranDetails_CashOperation() {
		$scope.Show_TambahIncomingPayment_RincianPembayaran_CashOperation = false;
	}

	function ShowMetodePembayaranDetails_CreditDebitCard() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard1 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard2 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard3 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard4 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard5 = true;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard6 = true;
	}

	function HideMetodePembayaranDetails_CreditDebitCard() {
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard1 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard2 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard3 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard4 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard5 = false;
		$scope.TambahIncomingPayment_RincianPembayaran_MetodeSelected_CreditDebitCard6 = false;
	}

	//end | NOTO | 180524 | TAMBAH | Rencana Pembayaran


	function Enable_TambahIncomingPayment_BiayaBiaya() {
		$scope.Show_TambahIncomingPayment_BiayaBiaya = true;
	}

	function Disable_TambahIncomingPayment_BiayaBiaya() {
		$scope.Show_TambahIncomingPayment_BiayaBiaya = false;
	}

	function Enable_TambahIncomingPayment_Biaya_NoFaktur() {
		$scope.Show_TambahIncomingPayment_Biaya_NoFaktur = true;
		$scope.Show_TambahIncomingPayment_Biaya_NoFaktur2 = false;
	}

	function Disable_TambahIncomingPayment_Biaya_NoFaktur() {
		$scope.Show_TambahIncomingPayment_Biaya_NoFaktur = false;
		$scope.Show_TambahIncomingPayment_Biaya_NoFaktur2 = true;
	}

	function HideKuitansi() {
		$scope.TambahIncomingPayment_NoKuitansi = false;
		$scope.TambahIncomingPayment_KetKuitansi = false;
		$scope.ShowSimpanCetakKuitansi = false;
		$scope.ShowSimpanCetakTTUS = false;
	}

	function ShowKuitansi() {
		console.log('show kuitansi');
		$scope.TambahIncomingPayment_NoKuitansi = true;
		$scope.TambahIncomingPayment_KetKuitansi = true;
		$scope.ShowSimpanCetakKuitansi = true;
		$scope.ShowSimpanCetakTTUS = false;
	}

	function HideButtonSimpanCetakKuitansi() {
		$scope.ShowSimpanCetakKuitansi = false;
	}

	function HideSimpanCetakKuitansi() {
		$scope.ShowSimpanCetakKuitansi = false;
		$scope.ShowSimpanCetakTTUS = true;
	}

	function ShowSimpanCetakKuitansi() {
		$scope.ShowSimpanCetakKuitansi = true;
		$scope.ShowSimpanCetakTTUS = false;
	}

	function HideTTUS() {
		$scope.ShowSimpanCetakKuitansi = true;
		$scope.ShowSimpanCetakTTUS = false;
	}

	function ShowTTUS() {
		$scope.ShowSimpanCetakKuitansi = false;
		$scope.ShowSimpanCetakTTUS = true;
	}

	//begin | NOTO | LIHAT | Preprinted Kuitansi

	function ShowKuitansi_Lihat() {
		$scope.LihatIncomingPayment_NoKuitansi_ShowHide = true;
		$scope.LihatIncomingPayment_KetKuitansi_ShowHide = true;
		$scope.Show_LihatIncomingPayment_CetakKuitansi = true;
		// $scope.ShowSimpanCetakKuitansi = true;
		// $scope.ShowSimpanCetakTTUS = false;
	}

	function HideKuitansi_Lihat() {
		$scope.LihatIncomingPayment_NoKuitansi_ShowHide = false;
		$scope.LihatIncomingPayment_KetKuitansi_ShowHide = false;
		$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
		// $scope.ShowSimpanCetakKuitansi = false;
		// $scope.ShowSimpanCetakTTUS = false;
	}

	//begin | NOTO | LIHAT | Preprinted Kuitansi

	// ============================ END | NOTO | 180516 | TUNING PERFORMANCE | NG-IF ============================




	// ------------------------------------- bstypeahead ------------------------------------------------------- start
	$scope.getNoKuitansi = function(key) {
		console.log("key", key);

		if (key == '' || key == null || key == undefined){
			var ress = IncomingPaymentFactory.getDataPreprintedComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					// $scope.TambahIncomingPayment_NomorKuitansiOption = res.data;

					return res.data
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			
		console.log("ress", ress);
		return ress;
		} else {
			var ress = IncomingPaymentFactory.getDataPreprintedComboBoxFilter(key)
			.then(
				function (res) {
					$scope.loading = false;
					// $scope.TambahIncomingPayment_NomorKuitansiOption = res.data;

					return res.data
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			
		console.log("ress", ress);
		return ress;
		}
		
	};

	$scope.onSelectNoKuitansi = function($item, $model, $label) {
		console.log("onSelectNoKuitansi=>", $item);
		console.log("onSelectNoKuitansi=>", $model);
		console.log("onSelectNoKuitansi=>", $label);

		$scope.TambahIncomingPayment_NomorKuitansi = $item.value
	};

	$scope.onNoResult = function() {
		console.log('hasil tidak ditemukan')
		$scope.TambahIncomingPayment_NomorKuitansi = ''
		console.log($scope.TambahIncomingPayment_NomorKuitansi)

	}

	$scope.onGotResult = function() {
		console.log("onGotResult=> data ditemukan");
	};

	$scope.ubahNoKuitansi = function(data){
		console.log('ubur ubur',data)
		// $scope.TambahIncomingPayment_NomorKuitansi = ''
	
	}

	$scope.CheckPengajuan = function() {
		var dateDock = angular.copy(new Date($scope.TambahIncomingPayment_TanggalIncomingPayment));
		var dateCard = angular.copy(new Date($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_CreditDebitCard));
		var dateCash = angular.copy(new Date($scope.TambahIncomingPayment_RincianPembayaran_TanggalPembayaran_Cash));
		
		dateDock.setHours(0,0,0,0);
		dateCard.setHours(0,0,0,0);
		dateCash.setHours(0,0,0,0);

		console.log('tgl dok',dateDock)
		console.log('tgl pembayaran deb', dateCard)
		console.log('tgl pembayaran cash', dateCash)
		if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Credit/Debit Card"){
			if(dateCard < dateDock){
				$scope.ShowPengajuan = true;
				$scope.ShowSimpanCetakKuitansi = false;
			}else{
				$scope.ShowPengajuan = false;
				$scope.ShowSimpanCetakKuitansi = true;
			}
		}else if($scope.TambahIncomingPayment_RincianPembayaran_MetodePembayaran == "Cash"){
			if ($scope.TambahIncomingPayment_TujuanPembayaran_TipeIncomingPayment != "Cash Delivery"){
				if(dateCash < dateDock){
					$scope.ShowPengajuan = true;
					$scope.ShowSimpanCetakKuitansi = false;
				}else{
					$scope.ShowPengajuan = false;
					$scope.ShowSimpanCetakKuitansi = true;
				}
			}else{
				$scope.ShowSimpanCetakKuitansi = false;
			}
		}
	}

	// ------------------------------------- bstypeahead ------------------------------------------------------- end



// ------------------------------------- Pengajuan Back Date ------------------------------------------------------- Start

	$scope.TambahIncomingPayment_Pengajuan = function (){
		angular.element('#ModalIncomingPayment_PengajuanBackDate ').modal('show');
		
	}

	$scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate = ""; //init alasan back date
	$scope.Pengajuan_IncomingPayment_PengajuanBackDateModal = function(){
		angular.element('#ModalIncomingPayment_PengajuanBackDate ').modal('hide');
		if($scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate == "" || $scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate == null ||
		$scope.ModalIncomingPayment_PengajuanBackDate_AlasanBackDate == undefined ){
			bsNotify.show(
				{
					title: "Peringatan",
					content: "Alasan Pengajuan Back Date harus di isi.",
					type: 'warning'
				}
			);  
		}else{
			$scope.TambahIncomingPayment_Simpan();
		}

	}

	$scope.Batal_IncomingPayment_PengajuanBackDateModal = function (){
		angular.element('#ModalIncomingPayment_PengajuanBackDate ').modal('hide');
	}

	$scope.showHideKuitansi = function(){
		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima"
			|| $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Cash Delivery") {
			$scope.Show_LihatIncomingPayment_CetakKuitansi = false;
		}
	}
// ------------------------------------- Pengajuan Back Date ------------------------------------------------------- End

});

app.filter('rupiahC', function () {
	return function (val) {
		console.log("Val", val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});
app.filter('rupiahCT', function () {
	return function (val) {
		console.log("Val", val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});
