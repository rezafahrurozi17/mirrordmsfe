/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('IncomingPaymentFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

		
		factory.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			var finalDate
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
            return finalDate;
		}
		//20170425, eric, begin
		factory.deleteSalesOrdersBySOId = function (SOId) {
			var url = '/api/fe/SalesOrders';
			if (debugMode) { console.log('Masuk ke delete factory function dengan Nomor SalesOrderId :' + SOId); };
			if (debugMode) { console.log('url :' + url); };
			var headersData = { "content-type": "application/json" };
			config = { data: SOId, headers: headersData };
			var res = $http.delete(url, config);
			return res;
		}
		//20170425, eric, end

		//20170429, marthin, begin
		factory.getSalesOrdersByCategory = function (Category, textSearch, IncomingType) {
			var url = '/api/fe/SalesOrders/GetData?start=1&limit=5&filterData=' + Category + textSearch + '|' + IncomingType;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor Sales Order :' + Category + textSearch); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
		//20170429, marthin, end

		//20170430, nuse edit, begin
		factory.getCustomerDataBySPKId = function (SPKId) {
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//SPKId = SPKId.replace(regex, "~"); 			
			//var url = '/api/fe/Customers/GetBySPKId/' + SPKId;
			var url = '/api/fe/Customers/GetBySPKId?SPKId=' + SPKId;
			console.log("execute getCustomerDataBySPKId");
			return $http.get(url);
		}

		factory.getCustomerDataByIPParentId = function (IPParentId) {
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//SPKId = SPKId.replace(regex, "~"); 			
			//var url = '/api/fe/Customers/GetBySPKId/' + SPKId;
			var url = '/api/fe/Customers/GetByIPParentId?IPParentId=' + IPParentId;
			console.log("execute GetByIPParentId");
			return $http.get(url);
		}

		factory.getSalesOrderBySPKId = function (start, limit, SPKId, IncomingType) {
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//SPKId = SPKId.replace(regex, "~"); 					
			//var url = '/api/fe/SalesOrders/getdatabyspkid/start/' + start + '/limit/' + limit + '/spkid/' + SPKId + '|' + IncomingType;
			var url = '/api/fe/SalesOrders/getdatabyspkid?start=' + start + '&limit=' + limit + '&spkid=' + SPKId + '|' + IncomingType;
			console.log("execute getSalesOrderBySPKId");
			return $http.get(url);
		}

		factory.getSPKByCustomerName = function (start, limit, CustomerName, IncomingType) {
			//var url = '/api/fe/SPK/getbycustomername/start/' + start + '/limit/' + limit + '/customername/' + CustomerName + '|' + IncomingType;
			var url = '/api/fe/SPK/GetByCustomerName?start=' + start + '&limit=' + limit + '&customername=' + CustomerName + '|' + IncomingType;
			console.log("execute getSPKByCustomerName");
			return $http.get(url);
		}
		//20170430, nuse edit, end

		//20170414, eric, begin
		factory.getSalesOrdersBySONumber = function (NomorSO, IncomingType) {
			var url = '/api/fe/SalesOrders/GetData?start=1&limit=5&filterData=SN' + NomorSO + '|' + IncomingType;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor Sales Order :' + NomorSO); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);

		}
		// get data pembayaran
		factory.getFilterData = function (start, limit, Kolom, combo) {
			var url = '/api/fe/ServiceFullPaymentGetDetail?start='+start+'&limit='+limit+'&filterData='+Kolom + '|' + combo;
			console.log("execute ServiceFullPaymentGetDetail");
			return $http.get(url);

		}
		factory.getSalesOrdersByCustomerName = function (Start, Limit, NomorSO, IncomingType) {
			var url = '/api/fe/SalesOrders/GetData?start=' + Start + '&limit=' + Limit + '&filterData=CN' + NomorSO + '|' + IncomingType;
			console.log(url);
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor Sales Order :' + NomorSO); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
		factory.getCustomersByCustomerId = function (CustomerId) {
			var url = '/api/fe/Customers/GetData?start=1&limit=1&filterData=' + CustomerId;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor Customer Id :' + CustomerId); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}

		factory.submitDataFinanceIncomingPayments = function (inputData) {
			var url = '/api/fe/FinanceIncomingPayments/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.getInterBranch = function (Start, Limit, Filter) {
			var url = '/api/fe/InterBranch?start=' + Start + '&limit=' + Limit + '&filterData=CN' + Filter;
			if (debugMode) { console.log('Masuk ke getdata factory getInterBranch'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		//20170414, eric, end
		factory.getDataByNomorSPK = function (NomorSPK, IncomingType) {
			//NomorSPK=NomorSPK.toString().replace("/","~");
			//NomorSPK=NomorSPK.toString().replace("/","~");
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//NomorSPK = NomorSPK.replace(regex, "~"); 			
			//var url = '/api/fe/SPK/GetByNomorSPK/' + NomorSPK + '|' + IncomingType;
			var url = '/api/fe/SPK/GetByNomorSPK?Id=' + NomorSPK + '|' + IncomingType;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor SPK :' + NomorSPK); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.getDataBySalesName = function (SalesName, IncomingType) {
			//var url = '/api/fe/SPK/GetBySalesName/' + SalesName +'|' + IncomingType;
			var url = '/api/fe/SPK/GetBySalesName?Id=' + SalesName + '|' + IncomingType;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Sales Name :' + SalesName); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.getDataIncomingPaymentDetail = function () {
			var url = '/api/fe/IncomingPayment/GetData';
			if (debugMode) { console.log('Masuk ke getdata factory getDataIncomingPaymentDetail'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		//   factory.getDataBookingFeeByNomorSPK= function() {
		// console.log('Masuk ke getdata factory function dengan Nomor SPK :');
		//     	var res=$http.get('/api/fe/SPK/GetBookingFee/SPK001');
		// return res;
		//   }

		factory.getProspectByCustomerId = function (CustomerId) {
			var url = '/api/fe/Prospect/GetByCustomerId/' + CustomerId;
			if (debugMode) { console.log('Masuk ke getdata factory getProspectByCustomerId'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.submitData = function (inputData) {
			var url = '/api/fe/IncomingPayment/SubmitData/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		}


		//to supply combo box data
		factory.getDataBankAccountComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataComboBox/';
			if (debugMode) { console.log('Masuk ke getdata factory getDataBankAccountComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.getDataBankAccountComboBoxNew = function () {
			var url = '/api/fe/AccountBank/GetDataComboBoxNew/';
			if (debugMode) { console.log('Masuk ke getdata factory getDataBankAccountComboBoxNew'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.getDataPeriodeReversal = function () {
			var url = '/api/fe/financereversalperiod/getdata';
			//console.log("url periode reversal : ", url);
			var res=$http.get(url);
			return res;
		}

		factory.getDataChequeComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataChequeComboBox/';
			if (debugMode) { console.log('Masuk ke getdata factory GetDataChequeComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.getDataChequeById = function (id) {
			var url = '/api/fe/AccountBank/GetDataChequeComboBox/?GetChequeId=' + id;
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		//to supply combo box data
		factory.getDataBankComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataBankComboBox/';
			if (debugMode) { console.log('Masuk ke getdata factory GetDataBankComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}
		//Alvin, begin
		factory.getDataBranchComboBox = function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		}
		factory.getDataBranchGroupComboBox = function () {
			console.log("factory.getDataBranchGroupComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchGroupComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.getDataPreprintedComboBox = function () {
			console.log("factory.GetDataPreprintedComboBox");
			var url = '/api/fe/AccountBank/GetDataPreprintedComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.getDataPreprintedComboBoxFilter = function (key) {
			console.log("factory.GetDataPreprintedComboBoxFilter");
			var url = '/api/fe/AccountBank/GetDataPreprintedComboBox/?filter=' + key;
			var res = $http.get(url);
			return res;
		}

		factory.getDataBranch = function (BranchId) {
			var url = '/api/fe/AccountBank/GetDataBranch/' + BranchId;
			var res = $http.get(url);
			return res;
		}

		factory.getMaterai = function (PaymentAmount) {
			var url = '/api/fe/AccountBank/GetMaterai?PaymentNominal=' + PaymentAmount;
			var res = $http.get(url);
			return res;
		}

		factory.getDataAccountBank = function (AccountId) {
			console.log("getDataAccountBank");
			console.log(AccountId);
			var url = '/api/fe/AccountBank/GetDataRekening/' + AccountId;
			var res = $http.get(url);
			return res;
		}

		factory.getDataCharges = function () {
			var url = '/api/fe/financeparam/Charges';
			var res = $http.get(url);
			return res;
		}

		factory.getDataPreprinted = function (TipeIncoming) {
			var url = '/api/fe/AccountBank/GetPreprinted/' + TipeIncoming;
			console.log(url);
			var res = $http.get(url);
			return res;
		}

		factory.getDataPreprintedSetting = function(){
			var url = '/api/fe/financepreprintedreceipt/getdata';
			console.log("url Preprinted Kuitansi : ", url);
			var res=$http.get(url);
			return res;
		},

		factory.getDataVendorComboBox = function () {
			console.log("factory.getDataVendorComboBox");
			var url = '/api/fe/AccountBank/GetDataVendorComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.getInsuranceComboBox = function () {
			console.log("factory.getInsuranceComboBox");
			var url = '/api/fe/AccountBank/GetDataInsuranceComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.KuitansiSave = function (inputData) {
			var url = '/api/fe/FinanceIncomingPayments/KuitansiSave/';
			var param = JSON.stringify(inputData);
			console.log("factory.KuitansiSave");
			var res = $http.post(url, param);
			return res;
		}

		factory.GetListIncoming = function (start, limit, TranDate, TranDateTo, SearchText, SearchType) {
			var url = '/api/fe/FinanceIncomingPayments/GetListIncoming/?start=' + start + '&limit=' + limit + '&TranDate=' + TranDate + '|' + TranDateTo + '|' + SearchText + '|' + SearchType;
			console.log("execute GetListIncoming");
			console.log(url);
			return $http.get(url);
		}

		// NEW cari & Filter
		factory.GetListIncomingNew = function (start, limit, TranDate, TranDateTo, SearchText, SearchType) {
			var url = '/api/fe/FinanceIncomingPayments/GetListIncomingNew/?start=' + start + '&limit=' + limit + '&TranDate=' + TranDate + '|' + TranDateTo + '|' + SearchText + '|' + SearchType;
			console.log("execute GetListIncoming");
			console.log(url);
			return $http.get(url);
		}

		factory.GetListRincianPembayaran = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetListRincianPembayaran/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetListRincianPembayaran");
			return $http.get(url);
		}

		factory.GetListRincianBiaya = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetListRincianBiaya/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetListRincianBiaya");
			return $http.get(url);
		}

		factory.GetListUnknownCards = function (start, limit, BankId, SearchText, SearchType, flag) {
			if(flag == 1){
				SearchText = this.changeFormatDate(SearchText)
			}	
			var url = '/api/fe/FinanceIncomingPayments/GetListUnknownCards/start/' + start + '/limit/' + limit + '/BankId/' + BankId + '|' + SearchText + '|' +SearchType;
			console.log("execute GetListUnknownCards");
			return $http.get(url);
		}

		factory.GetLihatIncomingPaymentWO = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentWO/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetLihatIncomingPaymentWO");
			return $http.get(url);
		}

		factory.GetLihatIncomingPaymentSO = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentSO/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetLihatIncomingPaymentSO");
			return $http.get(url);
		}

		factory.GetLihatIncomingPaymentVendor = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentVendor/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetLihatIncomingPaymentVendor");
			return $http.get(url);
		}

		factory.GetLihatIncomingPaymentAR = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentAR/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetLihatIncomingPaymentAR");
			return $http.get(url);
		}

		factory.GetLihatIncomingPaymentSPK = function (start, limit, IPParentId) {
			var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentSPK/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute GetLihatIncomingPaymentSPK");
			return $http.get(url);
		}

		factory.GetListRefundLeasing = function (start, limit, TranDate, VendorId) {
			var url = '/api/fe/RefundLeasing/GetListRefundLeasing?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + VendorId;
			console.log("execute GetListRefundLeasing");
			return $http.get(url);
		}

		factory.GetListProformaInvoice = function (start, limit, TranDate, VendorId) {
			var url = '/api/fe/ProformaInvoice/GetListProformaInvoice?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + VendorId;
			console.log("execute GetListProformaInvoice");
			console.log(url);
			return $http.get(url);
		}

		//NEW Kuitansi
		factory.GetListProformaInvoiceNew = function (start, limit, TranDate, VendorId) {
			var url = '/api/fe/ProformaInvoice/GetListProformaInvoiceNew?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + VendorId;
			console.log("execute GetListProformaInvoice");
			console.log(url);
			return $http.get(url);
		}

		//Alokasi Unknown
		factory.GetListUnusedUnknown = function (start, limit, TranDate) {
			var url = '/api/fe/FinanceIncomingPayments/GetListUnusedUnknown/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate;
			console.log("execute GetListUnusedUnknown");
			return $http.get(url);
		}

		factory.GetListUsedUnknown = function (start, limit, TranDate) {
			var url = '/api/fe/FinanceIncomingPayments/GetListUsedUnknown/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate;
			console.log("execute GetListUsedUnknown");
			return $http.get(url);
		}


		factory.submitDataApprovalIncomingPayments = function (inputData) {
			var url = '/api/fe/FinanceApproval/SubmitData/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.updateApprovalData = function (inputData) {
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res = $http.put(url, param);
			return res;
		},
			//Alvin, end	

			factory.getLeasingComboBox = function () {
				console.log("factory.getLeasingComboBox");
				var url = '/api/fe/AccountBank/GetDataLeasingComboBox/';
				var res = $http.get(url);
				return res;
			},

			factory.getCetakKuitansi = function (IPParentId) {
				var url = '/api/fe/FinanceRptKuitansi/Cetak/?IPParentId=' + IPParentId;
				console.log(url);
				var res = $http.get(url, { responseType: 'arraybuffer' });

				return res;
			},

			factory.getCetakTTUS = function (IPParentId) {
				var url = '/api/fe/FinanceRptTTUS/Cetak/?IPParentId=' + IPParentId;
				console.log(url);
				var res = $http.get(url, { responseType: 'arraybuffer' });

				return res;
			},

			//hilman,begin
			factory.getDataServiceCustomerAndPayment = function (Start, Limit, SearchQry, SearchType) {
				var url = '/api/fe/ServiceCustomerAndPayment/?start=' + Start + '&limit=' + Limit + '&filterData=' + SearchQry + '|' + SearchType;
				console.log("masuk getDataServiceCustomerAndPayment");
				var res = $http.get(url);
				return res;
			}
		//hilman,end

		factory.getDataEDC = function () {
			var url = '/api/fe/FinanceEDC/getdata/';
			console.log("masuk get data finance EDC");
			var res = $http.get(url);
			return res;
		}

		factory.getDataPandC = function (name) {
			if (name != '' && name != undefined)
				var url = '/api/fe/Customers/GetProspectAndCustomerByName?Name=' + name;
			else
				var url = '/api/fe/Customers/GetProspectAndCustomer';
			var res = $http.get(url);
			console.log("object get Cust ", res);
			return res;
		}

		factory.getDataSales = function (name) {
			if (name != '' && name != undefined)
				var url = '/api/fe/Customers/GetSalesByName?Name=' + name;
			else
				var url = '/api/fe/Customers/GetSales';
			var res = $http.get(url);
			console.log("object get Sales ", res);
			return res;
		}

		factory.getCashOperationOutgoing = function (start, limit, filterData) {
			var url = '/api/fe/FinanceIncomingPayments/GetListOutgoingPaymentCash/start/'+start+'/limit/'+limit+'/FilterData/' + filterData;		
			console.log("Url outgoing ", url);	
			var res=$http.get(url);  
			return res;	
		}
		//Tambahan OI OE
		factory.getDataOI = function (name) {
			var url = '/api/fe/FinanceOIOE/GetValueOI';
			var res = $http.get(url);
			console.log("object get OI ", res);
			return res;
		}

		factory.getDataOE = function (name) {
			var url = '/api/fe/FinanceOIOE/GetValueOE';
			var res = $http.get(url);
			console.log("object get OE ", res);
			return res;
		}
		
		return factory;
	});