angular.module('app')
  .factory('RekeningBankFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[{ name: "rek1", value: "rek1" }, { name: "rek2", value: "rek2" }, { name: "rek3", value: "rek3" }];
		res=da_json;
		
        return res;
      },
      create: function(rekeningBank) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: rekeningBank.Name,
                                            Description: rekeningBank.Description}]);
      },
      update: function(rekeningBank){
        return $http.put('/api/fw/Role', [{
                                            Id: rekeningBank.Id,
                                            //pid: negotiation.pid,
                                            Name: rekeningBank.Name,
                                            Description: rekeningBank.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });