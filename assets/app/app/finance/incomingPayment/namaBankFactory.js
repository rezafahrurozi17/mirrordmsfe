angular.module('app')
  .factory('NamaBankFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[{ name: "BCA", value: "BCA" }, { name: "BII", value: "BII" }, { name: "Mandiri", value: "Mandiri" }];
		res=da_json;
		
        return res;
      },
      create: function(namaBank) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: namaBank.Name,
                                            Description: namaBank.Description}]);
      },
      update: function(namaBank){
        return $http.put('/api/fw/Role', [{
                                            Id: namaBank.Id,
                                            //pid: negotiation.pid,
                                            Name: namaBank.Name,
                                            Description: namaBank.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });