
var app = angular.module('app');
//20170425, eric, begin
//Nama controller disini harus case sensitive sama dengan route.js
app.controller('alokasiBFDPUnitController', function ($scope, $http, CurrentUser, $filter, AlokasiBFDPUnitFactory, $timeout, uiGridConstants, bsNotify) {
	$scope.optionsTambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search = [{ name: "Nomor SPK", value: "Nomor Spk" }, { name: "Status SPK", value: "Status SPK" }, { name: "Model", value: "Model" }, { name: "Tipe", value: "Tipe" }, { name: "Nominal Titipan", value: "Nominal Titipan" }];
	$scope.optionsTambahAlokasiBFDPUnit_DaftarSpk_Target_Search = [{ name: "Nomor Spk", value: "Nomor Spk" }, { name: "Model", value: "Model" }, { name: "Tipe", value: "Tipe" }];
	$scope.optionsLihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search = [{ name: "No Alokasi BF/DP", value: "No Alokasi BF/DP" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }, { name: "Nominal Titipan", value: "Nominal" }, { name: "Status Pengajuan", value: "Status Pengajuan" }];
	$scope.optionsLihatAlokasiBFDPUnit_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];

	$scope.Show_ShowLihatAlokasiBFDPUnit = true;
	$scope.ShowLihatHeader = true;
	$scope.ShowLihatFooter = false;
	$scope.ShowAlokasiBFDPUnitDataDetail = false;
	//Disable_LihatAlokasiBFDPUnit_DaftarSpk();
	$scope.uiGridPageSize = 10;
	$scope.currentOutletId = 0;
	$scope.currentSPKSourceNominal = 0;

	$scope.dateOptions = {
		startingDay: 1,
		format: "dd/MM/yyyy",
		disableWeekend: 1
	};

	var paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log($scope.ByteEnable);
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	if ($scope.allowApprove == true)
		$scope.optionsLihatAlokasiBFDPUnit_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	else
		$scope.optionsLihatAlokasiBFDPUnit_DaftarBulkAction_Search = [];


	$scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnit = new Date();
	$scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnitTo = new Date();

	//----------------------------------
	// Start-Up
	//----------------------------------

	$scope.user = CurrentUser.user();

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		//   $scope.gridData=[];
		// $scope.SetMainGridADH();

	});

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.CariAlokasiBFDPUnitData = function () {
		if (($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnit != null)
			&& ($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnitTo != null)) {
			$scope.GetListAlokasi();
			console.log('test1');

			// $scope.ShowAlokasiBFDPUnitData = true;
			//$scope.ShowAlokasiBFDPUnitDataDetail = true;
		}
		else {
			alert("Tgl Awal/Akhir Blm Diisi");
		}
	};

	$scope.GetListAlokasi = function () {
		var Start = paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageNumber;
		var Limit = paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageSize;
		AlokasiBFDPUnitFactory.GetListAlokasiBFDPUnit(Start, Limit, $filter('date')(new Date($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnit), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnitTo), 'yyyy-MM-dd'),
			$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text, $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search)
			.then(
				function (res) {
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.totalItems = res.data.Total;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationPageSize = Limit;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationCurrentPage = Start;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.SetMainGridADH = function () {
		console.log($scope.user.OrgCode);
		if ($scope.user.RoleName == 'ADH') {
			$scope.GetListAlokasiADH();
			console.log('testADH1');
		}
	};

	$scope.GetListAlokasiADH = function () {
		var Start = paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageNumber;
		var Limit = paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageSize;
		AlokasiBFDPUnitFactory.GetListAlokasiBFDPUnit(Start, Limit, "1970-01-01",
			$filter('date')(new Date($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnitTo), 'yyyy-MM-dd'),
			"diajukan", "Status Pengajuan")
			.then(
				function (res) {
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.data = res.data.Result;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.totalItems = res.data.Total;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationPageSize = Limit;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.paginationCurrentPage = Start;

					// var d = new Date(res.data.Result[0].StartDate);
					// var StartDate = new Date(d.getFullYear(), d.getMonth() + 1, d.getDate());

					var StartD = new Date();
					if (!angular.isUndefined(res.data.Result[0].StartDate)) {
						StartD = new Date(res.data.Result[0].StartDate);
					}

					$scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnit = StartD;
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search = "Status Pengajuan";
					$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text = "diajukan";
					// $scope.ShowAlokasiBFDPUnitData = true;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,

		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIPParentId = 0;
					$scope.currentIPParentId = 0;
				}
				else {
					$scope.currentUnknownId = row.entity.UnknownId;
					$scope.currentIPParentId = row.entity.IPParentId;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageSize = pageSize;
				paginationOptions_LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.pageNumber = pageNumber;
				$scope.GetListAlokasi();


				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
			gridApi.core.on.renderingComplete($scope, function () {
				$scope.SetMainGridADH();
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal Alokasi BF/DP', displayName: "Tanggal Alokasi BF/DP", field: 'TanggalAlokasi', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nomor Alokasi BF/DP', displayName: "Nomor Alokasi BF/DP", field: 'NoAlokasi', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailAlokasi(row)">{{row.entity.NoAlokasi}}</a>' },
				{ name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Nomor SPK Sumber Alokasi', displayName: "Nomor SPK Sumber Alokasi", field: 'SPKSource' },
				{ name: 'Nomor SPK Target Alokasi', displayName: "Nomor SPK Target Alokasi", field: 'SPKTarget' },
				{ name: 'Nominal Titipan', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan' },
				{ name: 'Tanggal Pengajuan', field: 'TglPengajuan', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan' },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan' },
				{ name: 'AlokasiId', field: 'AlokasiId', visible: false }

			]
	};

	$scope.LihatDetailAlokasi = function (row) {
		Enable_LihatAlokasiBFDPUnit_DaftarSpk();
		//$scope.ShowAlokasiBFDPUnitData = true;
		$scope.ShowLihatHeader = false;
		$scope.ShowLihatFooter = true;
		$scope.ShowAlokasiBFDPUnitDataDetail = true;

		$scope.LihatAlokasiBFDPUnit_TanggalAlokasiBFDPUnit = row.entity.TanggalAlokasi;
		$scope.LihatAlokasiBFDPUnit_Top_NomorAlokasi = row.entity.NoAlokasi;
		$scope.LihatAlokasiBFDPUnit_NamaPelanggan = row.entity.CustomerName;
		$scope.currentNoSPKSource = row.entity.SPKSource;
		$scope.currentNoSPKSource = $scope.currentNoSPKSource.replace(/\./g,"-");
		$scope.currentNoSPKTarget = row.entity.SPKTarget;
		$scope.currentNoSPKTarget = $scope.currentNoSPKTarget.replace(/\./g,"-");
		console.log("cek replace>>>",$scope.currentNoSPKTarget)
		AlokasiBFDPUnitFactory.getDataByNomorSPK($scope.currentNoSPKSource, "Alokasi")
			.then
			(
			function (res) {
				$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = res.data.Result;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.totalItems = res.data.Total;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
		AlokasiBFDPUnitFactory.getDataByNomorSPK($scope.currentNoSPKTarget, "Alokasi")
			.then
			(
			function (res) {
				$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = res.data.Result;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.totalItems = res.data.Total;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
		AlokasiBFDPUnitFactory.getSalesOrderBySPKId(1, $scope.uiGridPageSize, $scope.LihatAlokasiBFDPUnit_Top_NomorAlokasi, "LihatAlokasi")
			.then
			(
			function (res) {
				console.log("Get data Sales Order", res.data.result);
				$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = res.data.Result;
				$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.totalItems = res.data.Total;
				$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);

	};

	$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid = {
		paginationPageSizes: [10, 50, 100],
		paginationPageSize: 10,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarSpk = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("Cek Selection", row)
				if(row.isSelected == false){
					$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = [];
					$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = [];
				}else{
					$scope.currentSPKIdSource = row.entity.SPKId;
					$scope.StatusSPK = row.entity.SpkStatusId;
					$scope.currentSPKSourceNominal = row.entity.Titipan;
	
					
					AlokasiBFDPUnitFactory.getSPKTarget(1, $scope.uiGridPageSize, $scope.currentSPKIdSource, $scope.TambahAlokasiBFDPUnit_SearchBy_NamaPelanggan)
						.then
						(
						function (res) {
							$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = res.data.Result;
							$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.totalItems = res.data.Total;
							$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSize = $scope.uiGridPageSize;
							$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						}
						);
				}
	
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: "SPKId", field: "SPKId", visible: false },
				{ name: 'NomorSPK', displayName: 'Nomor SPK', field: 'NomorSPK' },
				{ name: 'StatusSPK', displayName: 'Status SPK', field: 'SpkStatusId' },
				{ name: 'Model', field: 'Model' },
				{ name: 'Tipe', field: 'Tipe' },
				{ name: 'Nominal Titipan', field: 'Titipan', cellFilter: 'currency:"":2' },
			]
	};

	$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: "SPKId", field: "SPKId", visible: false },
			{ name: "Nomor Spk", displayName: "Nomor SPK", field: "NomorSPK" },
			{ name: "Model", field: "Model" },
			{ name: "Tipe", field: "Tipe" }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = $scope.TambahAlokasiBFDPUnit_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.SPKTargetSelected = false;
					$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = [];
				}
				else {
					$scope.SPKTargetSelected = true;
					$scope.currentNoSPKTarget = row.entity.NomorSPK;
					$scope.currentNoSPKTarget = $scope.currentNoSPKTarget.replace(/\./g,"-");
					console.log("cek replace>>>",$scope.currentNoSPKTarget)
					$scope.currentSPKIdTarget = row.entity.SPKId;
					AlokasiBFDPUnitFactory.getSalesOrderBySPKId(1, $scope.uiGridPageSize, $scope.currentNoSPKTarget, "Alokasi")
						.then(
							function (res) {
								console.log("Get data Sales Order", res.data.result);
								$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = res.data.Result;
								$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.totalItems = res.data.Total;
								$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.paginationPageSize = $scope.uiGridPageSize;
								$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
							}
						);
				}
			}
			)
		}
	};

	$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "SOId", field: "SOId", visible: false },
			{ name: "Nomor SO", displayName: "Nomor SO", field: "SONumber" },
			{ name: "Tgl SO", displayName: "Tanggal SO", field: "SODate", cellFilter: 'date:\'yyyy-MM-dd\'' },
			{
				name: 'Alokasi Down Payment', field: "Pembayaran", aggregationType: uiGridConstants.aggregationTypes.sum,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: 'number', cellFilter: 'currency:"":2'
			},
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = $scope.TambahAlokasiBFDPUnit_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdTarget = row.entity.SPKId;
			}
			)
		}
	};

	//UI Grid Lihat
	$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarSpk = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: "SPKId", field: "SPKId", visible: false },
				{ name: 'NomorSPK', displayName: 'Nomor SPK', field: 'NomorSPK' },
				{ name: 'StatusSPK', displayName: 'Status SPK', field: 'SpkStatusId' },
				{ name: 'Model', field: 'Model' },
				{ name: 'Tipe', field: 'Tipe' },
				{ name: 'Nominal', field: 'TotalAlokasi', cellFilter: 'currency:"":2' },
			]
	};

	$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: "SPKId", field: "SPKId", visible: false },
			{ name: "Nomor Spk", displayName: 'Nomor SPK', field: "NomorSPK" },
			{ name: "Model", field: "Model" },
			{ name: "Tipe", field: "Tipe" }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiLihatAlokasiBFDPUnit_DaftarSPKTarget = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = $scope.TambahAlokasiBFDPUnit_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {

			}
			)
		}
	};

	$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "SOId", field: "SOId", visible: false },
			{ name: "Nomor SO", displayName: 'Nomor SO', field: "SONumber" },
			{ name: "Tgl SO", displayName: 'Tanggal SO', field: "SODate", cellFilter: 'date:\'yyyy-MM-dd\'' },
			{ name: 'Alokasi Down Payment', field: "AllocatedAmount", aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'currency:"":2' },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiLihatAlokasiBFDPUnit_DaftarSPKTarget = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = $scope.TambahAlokasiBFDPUnit_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdTarget = row.entity.SPKId;
			}
			)
		}
	};


	$scope.ToTambahAlokasiBFDPUnit = function () {
		$scope.ClearAllData();
		$scope.Show_ShowLihatAlokasiBFDPUnit = false;
		$scope.Show_ShowTambahAlokasiBFDPUnit = true;
		$scope.ShowTambahFooter = true;
		$scope.TambahAlokasiBFDPUnit_SearchBy_NamaPelanggan = "";
		$scope.ShowDaftar_SPK = false;
		
	};

	$scope.TambahAlokasiBFDPUnit_Batal = function () {
		$scope.Show_ShowLihatAlokasiBFDPUnit = true;
		$scope.Show_ShowTambahAlokasiBFDPUnit = false;
		$scope.ShowTambahFooter = false;
		$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = [];
		$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = [];
		$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = [];

		$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_UIGrid.data = [];
		$scope.LihatAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = [];
		$scope.LihatAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = [];
		$scope.LihatAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = [];

		$scope.LihatAlokasiBFDPUnit_TanggalAlokasiBFDPUnit = "";
		$scope.LihatAlokasiBFDPUnit_Top_NomorAlokasi = "";
		$scope.LihatAlokasiBFDPUnit_NamaPelanggan = "";

	}

	$scope.LihatAlokasiBFDPUnit_Batal = function () {
		$scope.Show_ShowLihatAlokasiBFDPUnit = true;
		$scope.Show_ShowTambahAlokasiBFDPUnit = false;
		$scope.ShowLihatHeader = true;
		$scope.ShowLihatFooter = false;
		$scope.ShowAlokasiBFDPUnitDataDetail = false;
	}

	// ClearAll Data
	$scope.ClearAllData = function(){
		//clear grid
		$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = [];
		$scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = [];
		$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data = [];
	}

	$scope.TambahAlokasiBFDPUnit_SearchBy = function () {
		$scope.ClearAllData();
		$scope.ShowDaftar_SPK = true;
		Enable_TambahAlokasiBFDPUnit_DaftarSpk();
		AlokasiBFDPUnitFactory.getSPKByCustomerName(1, $scope.uiGridPageSize,
			$scope.TambahAlokasiBFDPUnit_SearchBy_NamaPelanggan, "Alokasi")
			.then
			(
			function (res) {
				var dataSPK = [];

				angular.forEach(res.data.Result, function (value, key) {
					var ttpn = parseFloat(value.PaidDP.replace('.0000', '')) + parseFloat(value.PaidBF.replace('.0000', ''));
					dataSPK.push({ SPKId: value.SPKId, NomorSPK: value.NomorSPK, SpkStatusId: value.SpkStatusId, Model: value.Model, Tipe: value.Tipe, Titipan: ttpn });
				});

				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = dataSPK;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.totalItems = res.data.Total;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSizes = [10, 50, 100];

				// $scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.data = res.data.Result;
				// $scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.totalItems = res.data.Total;
				// $scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.TambahAlokasiBFDPUnit_DaftarSpkTarget_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
			}
			);
	};


	$scope.ProsesSubmit = false;
	$scope.TambahAlokasiBFDPUnit_Simpan = function () {
		var data;
		var userId = 1;
		var TotalAlokasi = 0;
		var TotalSO = 0;
		var amountAlokasi = 0;
		$scope.ProsesSubmit = true;

		if (!$scope.SPKTargetSelected) {
			bsNotify.show(
				{
					title: "Warning",
					content: "SPK Target harus dipilih",
					type: 'warning'
				}
			);
			$scope.ProsesSubmit = false;
			return false;
		}

		// Start Penjagaan lengkap
		if($scope.StatusSPK == "Lengkap"){
			
			if($scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data.length == 0){
				bsNotify.show(
					{
						title: "Warning",
						content: "SPK Target harus dipilih",
						type: 'warning'
					}
				);
				$scope.ProsesSubmit = false;
				return false;
			}

			$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data.forEach(function (obj){
				if(obj.Pembayaran != null){
					amountAlokasi += obj.Pembayaran;
				}
			});
			console.log("Cek OBJ", amountAlokasi)

			if(amountAlokasi != $scope.currentSPKSourceNominal){
				console.log("Nominal amount", $scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data[0].Titipan)
				bsNotify.show(
					{
						title: "Warning",
						content: "Nominal Pembayaran Tidak Sesuai!",
						type: 'warning'
					}
				);
				$scope.ProsesSubmit = false;
				return false;
			}
		}
		// End Penjagaan Lengkap

		$scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data.forEach(function (obj) {
			console.log(obj.Pembayaran);
			if (obj.Pembayaran != null) {
				TotalAlokasi += parseInt(obj.Pembayaran)
				TotalSO = TotalSO + 1;
			};
		});

		console.log(TotalAlokasi);
		console.log($scope.currentSPKSourceNominal);

		$scope.SOSubmitData = [];
		$scope.TambahAlokasiBFDPUnit_CreateData();
		$scope.SOSubmitData = $scope.TambahAlokasiBFDPUnit_SubmitData;

		console.log(TotalSO);

		if (TotalSO == 0) {
			console.log("TambahAlokasiBFDPUnit_Simpan BF");

			data =
				[{
					OutletId: $scope.currentOutletId,
					SPKSourceId: $scope.currentSPKIdSource,
					SPKTargetId: $scope.currentSPKIdTarget,
					UserId: userId
				}]

			AlokasiBFDPUnitFactory.submitDataAlokasiBFDPUnit(data)
				.then
				(
				function (res) {
					console.log(res);
					// alert("Pengajuan Berhasil");

					bsNotify.show(
						{
							title: "Berhasil",
							content: "Pengajuan berhasil",
							type: 'success'
						}
					);
					$scope.ProsesSubmit = false;
					$scope.TambahAlokasiBFDPUnit_Batal();
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) {
						// alert(err.data.Message);
						bsNotify.show({
							title: "Notifikasi",
							content: err.data.Message,
							type: 'danger'
						});
						$scope.ProsesSubmit = false;
					}
				}
				);
		}
		else {
			if (TotalAlokasi == $scope.currentSPKSourceNominal) {
				console.log("TambahAlokasiBFDPUnit_Simpan SO");

				data =
					[{
						OutletId: $scope.currentOutletId,
						SPKSourceId: $scope.currentSPKIdSource,
						SPKTargetId: $scope.currentSPKIdTarget,
						UserId: userId,
						SOList: $scope.SOSubmitData
					}]

				AlokasiBFDPUnitFactory.submitDataAlokasiBFDPUnit(data)
					.then
					(
					function (res) {
						console.log(res);
						// alert("Pengajuan Berhasil");

						bsNotify.show(
							{
								title: "Berhasil",
								content: "Pengajuan berhasil",
								type: 'success'
							}
						);
						$scope.ProsesSubmit = false;

						$scope.TambahAlokasiBFDPUnit_Batal();
					},
					function (err) {
						console.log("err=>", err);
						if (err != null) {
							// alert(err.data.Message);
							bsNotify.show({
								title: "Notifikasi",
								content: err.data.Message,
								type: 'danger'
							});
							$scope.ProsesSubmit = false;
						}
					}
					);
			}
			else {
				// alert("Total Alokasi Tidak Sama Dengan Nominal SPK");
				bsNotify.show({
					title: "Warning",
					content: "Total Alokasi Tidak Sama Dengan Nominal SPK",
					type: 'warning'
				});
				$scope.ProsesSubmit = false;
			}
		}
	}

	$scope.TambahAlokasiBFDPUnit_CreateData = function () {
		$scope.TambahAlokasiBFDPUnit_SubmitData = [];
		angular.forEach($scope.TambahAlokasiBFDPUnit_DaftarSalesOrder_UIGrid.data, function (value, key) {
			var obj = { "SOId": value.SOId, "PaidAmount": value.Pembayaran };
			console.log("Object of SO ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.TambahAlokasiBFDPUnit_SubmitData.push(obj);
		}
		);
	}

	//Filter LihatAlokasiBFDPUnit
	$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Filter_Clicked = function () {
		if (($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnit != null)
			&& ($scope.LihatAlokasiBFDPUnit_TujuanPembayaran_TanggalAlokasiBFDPUnitTo != null)) {
			$scope.GetListAlokasi();
			console.log('test1');
		}

		// console.log("LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Filter_Clicked");
		// if (($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text != "") &&
		// 	($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search != "")) {
		// 	// var nomor;
		// 	// var value = $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text;
		// 	// $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI.grid.clearAllFilters();
		// 	// console.log('filter text -->', $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text);
		// 	// if ($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search == "No Alokasi BF/DP") {
		// 	// 	nomor = 2;
		// 	// }
		// 	// if ($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search == "Nama Pelanggan") {
		// 	// 	nomor = 3;
		// 	// }
		// 	// if ($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search == "Nominal") {
		// 	// 	nomor = 6;
		// 	// }
		// 	// console.log("nomor --> ", nomor);
		// 	// $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_Search_Text;
		// 	$scope.GetListAlokasi();
		// }
		// else {
		// 	$scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI.grid.clearAllFilters();
		// 	//alert("inputan filter belum diisi !");
		// }
	};

	//OLD Folder
	// $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Filter_Clicked = function () {
	// 	console.log("TambahAlokasiBFDPUnit_DaftarSpk_Detail_Filter_Clicked");
	// 	if (($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text != "") &&
	// 		($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text;
	// 		$scope.gridApi_DaftarSpk.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text);
	// 		if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown == "Nomor Spk") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown == "Status SPK") {
	// 			nomor = 3;
	// 		}
	// 		if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown == "Model") {
	// 			nomor = 4;
	// 		}
	// 		if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown == "Tipe") {
	// 			nomor = 5;
	// 		}
	// 		if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown == "Nominal Titipan") {
	// 			nomor = 6;
	// 		}
	// 		console.log($scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown);
	// 		console.log("nomor --> ", nomor);
	// 		if (nomor == 0) {
	// 			console.log("masuk 0");
	// 			$scope.gridApi_DaftarSpk.grid.columns.filters[1].term = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text;
	// 			$scope.gridApi_DaftarSpk.grid.columns.filters[2].term = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text;
	// 		}
	// 		else {
	// 			console.log("masuk else");
	// 			$scope.gridApi_DaftarSpk.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text;
	// 		}
	// 	}
	// 	else {
	// 		$scope.gridApi_DaftarSpk.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	$scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Filter_Clicked = function(){
		AlokasiBFDPUnitFactory.getListSPK(1, $scope.uiGridPageSize,
			$scope.TambahAlokasiBFDPUnit_SearchBy_NamaPelanggan, "Alokasi", $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_Search_Text, $scope.TambahAlokasiBFDPUnit_DaftarSpk_Sumber_SearchDropdown )
		.then (
			function(res){
				var dataSPK = [];
				angular.forEach(res.data.Result, function (value, key) {
					var ttpn = parseFloat(value.PaidDP.replace('.0000', '')) + parseFloat(value.PaidBF.replace('.0000', ''));
					dataSPK.push({ SPKId: value.SPKId, NomorSPK: value.NomorSPK, SpkStatusId: value.SpkStatusId, Model: value.Model, Tipe: value.Tipe, Titipan: ttpn });
				});
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.data = dataSPK;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.totalItems = res.data.Total;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.TambahAlokasiBFDPUnit_DaftarSpkSumber_UIGrid.paginationPageSizes = [10, 50, 100];
			}
		)
	}

	$scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Filter_Clicked = function () {
		console.log("TambahAlokasiBFDPUnit_DaftarSpk_Target_Filter_Clicked");
		if (($scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Search_Text != "") &&
			($scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Search_Text;
			$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Search_Text);
			if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_SearchDropdown == "Nomor Spk") {
				nomor = 2;
			}
			if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_SearchDropdown == "Model") {
				nomor = 3;
			}
			if ($scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_SearchDropdown == "Tipe") {
				nomor = 4;
			}

			console.log("nomor --> ", nomor);
			if (nomor == 0) {
				console.log("masuk 0");
				$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget.grid.columns.filters[0].term = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Search_Text;
			}
			else {
				console.log("masuk else");
				$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget.grid.columns[nomor].filters[0].term = $scope.TambahAlokasiBFDPUnit_DaftarSpk_Target_Search_Text;
			}
		}
		else {
			$scope.GridApiTambahAlokasiBFDPUnit_DaftarSPKTarget.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};


	/*===============================Bulk Action Section, Begin===============================*/
	$scope.LihatAlokasiBFDPUnit_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");
		if ($scope.LihatAlokasiBFDPUnit_DaftarBulkAction_Search == "Setuju") {
			angular.forEach($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakAlokasiBFDPUnit_AlokasiId = value.AlokasiId;
				console.log(value.StatusPengajuan);
				if (value.StatusPengajuan == "Diajukan") {
					$scope.CreateApprovalData("Disetujui");
					AlokasiBFDPUnitFactory.updateApprovalData($scope.ApprovalData)
						.then(
							function (res) {
								// alert("Berhasil Disetujui");
								bsNotify.show({
									title: "Berhasil",
									content: "Berhasil Disetujui",
									type: 'success'
								});
								$scope.Batal_LihatAlokasiBFDPUnit_TolakModal();
							},

							function (err) {
								if(err != null){
									var errMsg = '';
									if (err.data.Message.split('#').length > 1) {
										errMsg = err.data.Message.split('#')[1];
									}else{
										errMsg = err.data.Message;
									}
									bsNotify.show({
										title: "Notifikasi",
										content: errMsg,
										type: 'danger'
									});
								}
							}
						);
				}
			});
		}
		else if ($scope.LihatAlokasiBFDPUnit_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI);
			angular.forEach($scope.LihatAlokasiBFDPUnit_DaftarAlokasiBFDPUnit_gridAPI.selection.getSelectedRows(), function (value, key) {
				console.log(value);
				$scope.ModalTolakAlokasiBFDPUnit_TanggalAlokasi = $filter('date')(new Date(value.TanggalAlokasi), 'dd/MM/yyyy');
				$scope.ModalTolakAlokasiBFDPUnit_AlokasiId = value.AlokasiId;
				$scope.ModalTolakAlokasiBFDPUnit_NamaPelanggan = value.CustomerName;
				$scope.ModalTolakAlokasiBFDPUnit_SPKSumber = value.SPKSource;
				$scope.ModalTolakAlokasiBFDPUnit_SPKTarget = value.SPKTarget;
				$scope.ModalTolakAlokasiBFDPUnit_Nominal = value.Nominal;
				$scope.ModalTolakAlokasiBFDPUnit_TanggalPengajuan = $filter('date')(new Date(value.TglPengajuan), 'dd/MM/yyyy');
				if (value.StatusPengajuan == "Diajukan")
					angular.element('#ModalTolakAlokasiBFDPUnit').modal('show');
			}
			);
		}
	}

	$scope.CreateApprovalData = function (ApprovalStatus) {
		$scope.ApprovalData =
			[
				{
					"ApprovalTypeName": "Alokasi BF/DP",
					"RejectedReason": $scope.ModalTolakAlokasiBFDPUnit_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakAlokasiBFDPUnit_AlokasiId,
					"ApprovalStatusName": ApprovalStatus
				}
			];
	}

	$scope.Batal_LihatAlokasiBFDPUnit_TolakModal = function () {
		angular.element('#ModalTolakAlokasiBFDPUnit').modal('hide');
		$scope.LihatAlokasiBFDPUnit_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListAlokasi();
		console.log('test3');

	}

	$scope.Pengajuan_LihatAlokasiBFDPUnit_TolakModal = function () {
		$scope.CreateApprovalData("Ditolak");
		AlokasiBFDPUnitFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function (res) {
					// alert("Berhasil tolak");
					bsNotify.show({
						title: "Berhasil",
						content: "Berhasil tolak",
						type: 'success'
					});
					$scope.ModalTolakAlokasiBFDPUnit_AlasanPenolakan = '';
					$scope.Batal_LihatAlokasiBFDPUnit_TolakModal();
				},

				function (err) {
					// alert("Penolakan gagal");
					bsNotify.show({
						title: "Notifikasi",
						content: "Penolakan gagal",
						type: 'danger'
					});
				}
			);
	}

	$scope.ClearActionFields = function () {
		$scope.ModalTolakAlokasiBFDPUnit_TanggalAlokasi = "";
		$scope.ModalTolakAlokasiBFDPUnit_AlokasiId = "";
		$scope.ModalTolakAlokasiBFDPUnit_NamaPelanggan = "";
		$scope.ModalTolakAlokasiBFDPUnit_SPKSumber = "";
		$scope.ModalTolakAlokasiBFDPUnit_SPKTarget = "";
		$scope.ModalTolakAlokasiBFDPUnit_Nominal = "";
		$scope.ModalTolakAlokasiBFDPUnit_TanggalPengajuan = "";
	}
	/*===============================Bulk Action Section, End===============================*/

});
