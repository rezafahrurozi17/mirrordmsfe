/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('AlokasiBFDPUnitFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;
		
		factory.getSalesOrderBySPKId = function(start, limit, SPKId, IncomingType)
		{
			var find = "/";
			var regex = new RegExp(find, "g");
			SPKId = SPKId.replace(regex, "~"); 					
			var url = '/api/fe/AlokasiBFDPUnit/GetAlokasiSOBySPKId/start/' + start + '/limit/' + limit + '/spkid/' + SPKId + '|' + IncomingType;
			console.log ("execute GetAlokasiSOBySPKId");
			return $http.get(url);
		}
		
		factory.getSPKByCustomerName = function(start, limit, CustomerName, IncomingType)
		{
			//var url = '/api/fe/SPK/getbycustomername/start/' + start + '/limit/' + limit + '/customername/' + CustomerName + '|' + IncomingType;
			var url = '/api/fe/SPK/GetByCustomerName?start=' + start + '&limit=' + limit + '&customername=' + CustomerName + '|' + IncomingType;
			
			console.log ("execute getSPKByCustomerName");
			return $http.get(url);
		}

		factory.getListSPK = function(start, limit, CustomerName, IncomingType, SearchText, SearchBox)
		{
			//var url = '/api/fe/SPK/getbycustomername/start/' + start + '/limit/' + limit + '/customername/' + CustomerName + '|' + IncomingType;
			var url = '/api/fe/SPK/GetByCustomerName?start=' + start + '&limit=' + limit + '&customername=' + CustomerName + '|' + IncomingType + '|' + SearchText + '|' +SearchBox;
			
			console.log ("execute getSPKByCustomerName");
			return $http.get(url);
		}

		factory.getSPKTarget = function(start, limit, SPKId, CustomerName)
		{
			//var url = '/api/fe/SPK/getbycustomername/start/' + start + '/limit/' + limit + '/customername/' + CustomerName + '|' + IncomingType;
			var url = '/api/fe/SPK/GetSPKTarget?start=' + start + '&limit=' + limit + '&SPKId=' + SPKId + '&CustomerName=' + CustomerName;
			
			console.log ("execute getSPKTarget");
			return $http.get(url);
		}
		
		factory.submitDataAlokasiBFDPUnit = function (inputData) 
		{
			var url = '/api/fe/AlokasiBFDPUnit/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitDataAlokasiBFDPUnit') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.getDataByNomorSPK = function (NomorSPK, IncomingType) 
		{
			//NomorSPK=NomorSPK.toString().replace("/","~");
			//NomorSPK=NomorSPK.toString().replace("/","~");
			console.log(NomorSPK);
			var find = "/";
			var regex = new RegExp(find, "g");
			NomorSPK = NomorSPK.replace(regex, "~"); 			
			var url = '/api/fe/AlokasiBFDPUnit/GetAlokasiByNomorSPK/' + NomorSPK + '|' + IncomingType;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor SPK :' + NomorSPK); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}

		factory.GetListAlokasiBFDPUnit = function(start, limit, TranDate, TranDateTo, SearchText, SearchType)
		{
			// var url = '/api/fe/AlokasiBFDPUnit/GetListAlokasiBFDPUnit/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo;
			var url = '/api/fe/AlokasiBFDPUnit/GetListAlokasiBFDPUnit?start=' + start + '&limit=' + limit + '&TranDate=' + TranDate + '|' + TranDateTo + '|' + SearchText + '|' + SearchType;
			console.log ("execute GetListAlokasiBFDPUnit : " + url) ;
			return $http.get(url);
		}
	
		factory.updateApprovalData = function(inputData)
		{
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		}
		
		return factory;
	});