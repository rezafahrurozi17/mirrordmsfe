angular.module('app')
	.factory('PembuatanFakturPajakFactory', function ($http, CurrentUser) {
	var user=CurrentUser.user();
	return{
		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},
		
		getCustomerData: function(customerName, outletId){
			var url = '/api/fe/FinanceCustomer/GetDataByCustName/?customername=' + customerName + '&outletId=' + outletId;
			console.log("url Customer Data : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getBillingData: function(start, limit, customerId){
			var url = '/api/fe/FinanceBilling/?start=' + start + '&limit=' + limit + '&customerId=' + customerId;
			console.log("url Billing Data : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getBillingDataTaxed: function(start, limit, customerId, dateStart, dateEnd, outletId,customername){
			var url = '/api/fe/FinanceJoinedTax/GetDataBillingTaxed/?CustomerId=' + customerId + '&DateStart=' + dateStart + '&DateEnd=' + dateEnd + '&Start= ' + start + '&limit=' + limit + '&OutletId=' + outletId+'&CustomerName='+customername;
			console.log("url Billing Data : ", url);
			var res=$http.get(url);
			return res;
		},
		// Filter Data 
		getBillingFilter: function(start, limit, customerId, dateStart, dateEnd, outletId, customername){
			var url = '/api/fe/FinanceJoinedTax/GetDataBillingTaxed/?CustomerId=' + customerId + '&DateStart=' + dateStart + '&DateEnd=' + dateEnd + '&Start= ' + start + '&limit=' + limit + '&OutletId=' + outletId+'&CustomerName='+customername;
			console.log("url Billing Data : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getCSV:function(customerId, inputData){
			var param = JSON.stringify(inputData);
			var parameters = {BillingList : param, CustomerId:customerId, OutletId:user.OutletId};
			var url = '/api/fe/FinanceManualTaxInvoice/Cetak/';
			console.log("getCSV : ", url);
			var res=$http.get(url, 
							{params:parameters, 
							responseType: 'arraybuffer', 
							headers:{'Content-Type' : 'application/json', 'Accept' : 'application/json'}
							});
			return res;
		},
		
		createCSV:function(inputData){
			var url = '/api/fe/FinanceJoinedTax/SubmitData/';			
			//var url = '/api/fe/FinanceManualTaxInvoice/Cetak/';
			var param = JSON.stringify(inputData);
			console.log("object input invoice masuk ", param);
			var res=$http.post(url, param);
			return res;
		},

		getCSV: function (customerId, billingList, TaxDocDate, OutletId) {
			//var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
			var url = '/api/fe/FinanceManualTaxInvoice/Cetak/'
			console.log("getCSV : ", url);
			// var billingList = [{
			// 	"billingid":BillingId,
			// 	"billingcode":BillingCode
			// }];
			var inputData = [{
				"CustomerId": customerId,
				"BillingList": billingList
				// "billingid":BillingId,
				// "billingcode":BillingCode,
				// "taxdocDate":TaxDocDate,
				// "outletid":OutletId

			}];
			var res = $http.post(url, inputData, { responseType: 'arraybuffer' });
			return res;
		}
	}
});