var app = angular.module('app');
app.controller('PembuatanFakturPajakController', function ($scope,bsNotify, $http, $filter, uiGridConstants, CurrentUser, PembuatanFakturPajakFactory, $timeout) {


/*==================================================================================*/
/*								Main Function Begin									*/
/*==================================================================================*/
$scope.BuatFakturPajak_CustomerId = 0;
$scope.dateOptions = {
	format: "dd/MM/yyyy"
};
$scope.PembuatanFakturPajak_TanggalBillingAwal= new Date();
$scope.PembuatanFakturPajak_TanggalBillingAkhir= new Date();
var user = CurrentUser.user();
$scope.optionsPGA_BGTFilterColumnList =[{ name: "Nomor Billing", value: "Nomor Billing" }, { name: "Tanggal Billing", value: "Tanggal Billing" }, { name: "Tipe Bisnis", value: "Tipe Bisnis" }, { name: "Nomor Faktur Pajak", value: "Nomor Faktur Pajak" }, { name: "Tanggal Faktur Pajak", value: "Tanggal Faktur Pajak" }];

function checkRbyte(rb, b) {
	var p = rb & Math.pow(2, b);
	return ((p == Math.pow(2, b)));
};

$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
$scope.FakturPajak = checkRbyte($scope.ByteEnable, 1);

if(user.OrgCode.substring(3, 9) == '000000')
	$scope.PembuatanFakturPajak_HO_Show = false;
else
	$scope.PembuatanFakturPajak_HO_Show = false;

$scope.PembuatanFakturPajak_getDataBranch = function () 
{	
	PembuatanFakturPajakFactory.getDataBranchComboBox()
	.then
	(
		function (res) 
		{
			$scope.optionsPembuatanFakturPajak_SelectBranch = res.data;
			
			$scope.PembuatanFakturPajak_SelectBranch = user.OutletId;
			res.data.some(function(obj, i){
				if(obj.value == user.OutletId)
					$scope.PembuatanFakturPajak_NamaBranch = obj.name;
			});
		},
		function (err) 
		{
			console.log("err=>", err);
		}
	);
};

// $scope.PGA_BGT_Filter_Button_Clicked = function () {
// 	$scope.BillingList_UIGrid_Paging(1);
// }

// NEW Filter FE
$scope.PGA_BGT_Filter_Button_Clicked = function(){
	// $scope.BillingList_Filter();
	console.log("TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked");
	if (($scope.PGA_BGTTeksFilter != "") &&
		($scope.PGA_BGTFilterColumnList != "")) {
		var nomor;
		value = $scope.PGA_BGTTeksFilter;
		$scope.GridApiBillingList.grid.clearAllFilters();
		$scope.GridApiBillingList.grid.getColumn($scope.PGA_BGTFilterColumnList).filters[0].term = value;
		console.log('filter text -->', $scope.PGA_BGTTeksFilter);
		// if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Tanggal Pembayaran") {
		// 	nomor = 3;
		// }
		// console.log("nomor --> ", nomor);
		// $scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.value;
	}
	else {
		$scope.GridApiBillingList.grid.clearAllFilters();
		//alert("inputan filter belum diisi !");
	}
}
// // New Filter BE
// $scope.BillingList_Filter = function(pageNumber){
// 	var DateStart = '';
// 	var DateEnd = '';
	
// 	if($scope.PembuatanFakturPajak_TanggalBillingAwal != undefined)
// 		DateStart = $filter('date')(new Date($scope.PembuatanFakturPajak_TanggalBillingAwal), 'yyyyMMdd');
// 		// $scope.tglBlnAwal = DateStart.getMonth();
// 	if($scope.PembuatanFakturPajak_TanggalBillingAkhir != undefined)
// 		DateEnd = $filter('date')(new Date($scope.PembuatanFakturPajak_TanggalBillingAkhir), 'yyyyMMdd');
// 		// $scope.tglBlnAkhir = DateEnd.getMonth();

// 	PembuatanFakturPajakFactory.getBillingFilter(pageNumber, $scope.uiGridPageSize, $scope.BuatFakturPajak_CustomerId, 
// 												DateStart, DateEnd, $scope.PembuatanFakturPajak_SelectBranch,$scope.BuatFakturPajak_NamaPelanggan + '|' + $scope.PGA_BGTTeksFilter + '|' + $scope.PGA_BGTFilterColumnList)
// 	.then(
// 		function(res)
// 		{
// 			// if(!angular.isUndefined(res.data.Result)){
// 				console.log("res =>", res.data.Result);
// 				$scope.BillingList_UIGrid.data = res.data.Result;
// 				$scope.BillingList_UIGrid.totalItems = res.data.Total;
// 				// $scope.BillingList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
// 				// $scope.BillingList_UIGrid.paginationPageSizes = [10,25,50];
// 			// }
// 		}
// 	);
// }

$scope.PembuatanFakturPajak_getDataBranch();

$scope.BuatFakturPajak_Cari_Clicked = function(){
	PembuatanFakturPajakFactory.getCustomerData($scope.BuatFakturPajak_NamaPelanggan,$scope.PembuatanFakturPajak_SelectBranch)
	.then(
		function(res){
			console.log("res", res.data.Result);
			$scope.BuatFakturPajak_CustomerId = res.data.Result[0].CustomerId;
			$scope.BuatFakturPajak_NomorPelanggan = res.data.Result[0].CustomerCode;
			$scope.BuatFakturPajak_NPWP = res.data.Result[0].NPWP;
			$scope.BuatFakturPajak_Alamat = res.data.Result[0].Address;
			$scope.BuatFakturPajak_KabKota = res.data.Result[0].KabKota;
			$scope.BuatFakturPajak_Telepon = res.data.Result[0].Phone;
			
			$scope.BillingList_UIGrid_Paging(1);
		}
	);
}
$scope.errorMsg = false;
$scope.PembuatanFakturPajak_TanggalBillingAwalOK = true;
	$scope.PembuatanFakturPajak_TanggalBillingAwalChanged = function () {
		
		console.log($scope.PembuatanFakturPajak_TanggalBillingAwalOK);
		var dt = $scope.PembuatanFakturPajak_TanggalBillingAwal;
		var dte = $scope.PembuatanFakturPajak_TanggalBillingAkhir;
		$scope.tglBlnAwal = dt.getMonth();
		$scope.tglBlnAkhir = dte.getMonth();
		$scope.tglBlnAwal1 = parseInt($scope.tglBlnAwal);
		$scope.tglBlnAkhir1 = parseInt($scope.tglBlnAkhir);
		console.log("tgal bulan awl", $scope.tglBlnAwal)
		console.log("tgal bulan awl", $scope.tglBlnAkhir)
		var now = new Date();
		if($scope.tglBlnAwal1 != $scope.tglBlnAkhir1){ // penjagaan tanggal hanya untuk bulan itu saja
			$scope.errorMsg = true;
			// bsNotify.show({
			// 	title:"warning",
			// 	content:"Bulan Tidak sama cuy!!!!!.",
			// 	type:"warning"
			// });
		} else {
			$scope.errorMsg = false;
		}
		 if (dt > now) {
			$scope.errPembuatanFakturPajak_TanggalBillingAwal = "Tanggal Awal harus lebih kecil dari tanggal akhir";
			$scope.PembuatanFakturPajak_TanggalBillingAwalOK = false;			
		}
		else {
			$scope.PembuatanFakturPajak_TanggalBillingAwalOK = true;
		}
		console.log('asdasd ' + $scope.PembuatanFakturPajak_TanggalBillingAwalOK);
	}
	$scope.PembuatanFakturPajak_TanggalBillingAkhirOK = true;
	$scope.PembuatanFakturPajak_TanggalBillingAkhirChanged = function () {
		var dt = $scope.PembuatanFakturPajak_TanggalBillingAwal;
		var dte = $scope.PembuatanFakturPajak_TanggalBillingAkhir;
		$scope.tglBlnAwal = dt.getMonth();
		$scope.tglBlnAkhir = dte.getMonth();
		$scope.tglBlnAwal1 = parseInt($scope.tglBlnAwal);
		$scope.tglBlnAkhir1 = parseInt($scope.tglBlnAkhir);
		console.log("tgal bulan awl", $scope.tglBlnAwal)
		console.log("tgal bulan awl", $scope.tglBlnAkhir)
		var now = new Date();
		if($scope.tglBlnAwal1 != $scope.tglBlnAkhir1){
			$scope.errorMsg = true;
			// $scope.errPembuatanFakturPajak_TanggalBillingAwal = "Data Billing Gabungan hanya untuk bulan yang sama";
		}else{
			$scope.errorMsg = false;
		}

		var now = new Date();
		 if (dte < now) {
			$scope.errPembuatanFakturPajak_TanggalBillingAwal = "Tanggal Akhir harus lebih besar dari tanggal Awal";
			$scope.PembuatanFakturPajak_TanggalBillingAkhirOK = false;			
		}
		else {
			$scope.PembuatanFakturPajak_TanggalBillingAkhirOK = true;
		}
		console.log('asdasd ' + $scope.PembuatanFakturPajak_TanggalBillingAkhirOK);
	}

/*==================================================================================*/
/*								Main Function End									*/
/*==================================================================================*/


/*==================================================================================*/
/*							Billing List UI Grid Begin								*/
/*==================================================================================*/
$scope.uiGridPageSize = 10000;
$scope.PGA_BGTFilterColumnList = [{ name: "BillingId", value: 1 }, { name: "Nomor Billing", value: 2 },
{ name: "Tanggal Billing", value: 3 },{ name: "Tipe Bisnis", value: 4 },{ name: "DPP", value: 5 },
{ name: "PPN", value: 6 },{ name: "PPh23", value: 7 },{ name: "Nomor Faktur Pajak", value: 8 },
{ name: "Tanggal Faktur Pajak", value: 9 }];

$scope.BillingList_UIGrid = {
	paginationPageSizes: [10,25,50],
	// useCustomPagination: true,
	// useExternalPagination : true,
	// rowSelection : true,
	multiSelect : true,
	enableSorting: true,
	enableFiltering: true,
	enableRowSelection: true,
	enableSelectAll: false,
	columnDefs: [
					{name:"BillingId",field:"BillingId",visible:false},
					{name:"Nomor Billing",field:"BillingCode"},
					{name:"Tanggal Billing",field:"BillingDate", cellFilter:'date:\"dd/MM/yyyy\"'},
					{name:"Tipe Bisnis", field:"BusinessType"},
					{name:"DPP", field:"DPP", width:150,cellFilter:'rupiahC'},
					{name:"PPN", field:"PPN", width:150,cellFilter:'rupiahC'},
					{name:"PPh23", field:"PPH23", width:150,cellFilter:'rupiahC'},
					{name:"Nomor Faktur Pajak Invoice", field:"TaxInvoiceNo", visible:false},
					{name:"Nomor Faktur Pajak", field:"TaxInvoiceNoJoin"},
					{name:"Tanggal Faktur Pajak", field:"TaxInvoiceDate", cellFilter:'date:\"dd/MM/yyyy\"'},
				],
	onRegisterApi: function(gridApi) {
		$scope.GridApiBillingList = gridApi;
		gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// $scope.BillingList_UIGrid_Paging(pageNumber);
		});
	}
};

$scope.BillingList_UIGrid_Paging = function(pageNumber){
	var DateStart = '';
	var DateEnd = '';
	
	if($scope.PembuatanFakturPajak_TanggalBillingAwal != undefined)
		DateStart = $filter('date')(new Date($scope.PembuatanFakturPajak_TanggalBillingAwal), 'yyyyMMdd');
		// $scope.tglBlnAwal = DateStart.getMonth();
	if($scope.PembuatanFakturPajak_TanggalBillingAkhir != undefined)
		DateEnd = $filter('date')(new Date($scope.PembuatanFakturPajak_TanggalBillingAkhir), 'yyyyMMdd');
		// $scope.tglBlnAkhir = DateEnd.getMonth();

	PembuatanFakturPajakFactory.getBillingDataTaxed(pageNumber, $scope.uiGridPageSize, 
														$scope.BuatFakturPajak_CustomerId, 
														DateStart, 
														DateEnd, 
														$scope.PembuatanFakturPajak_SelectBranch,
														$scope.BuatFakturPajak_NamaPelanggan
													)
	.then(
		function(res)
		{
			// if(!angular.isUndefined(res.data.Result)){
				console.log("res =>", res.data.Result);
				$scope.BillingList_UIGrid.data = res.data.Result;
				$scope.BillingList_UIGrid.totalItems = res.data.Total;
				$scope.LoadingBuat = false;
				// $scope.BillingList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// $scope.BillingList_UIGrid.paginationPageSizes = [10,25,50];
			// }
		}
	);
}
/*==================================================================================*/
/*							Billing List UI Grid End								*/
/*==================================================================================*/
/*==================================================================================*/
/*							 CSV List UI Grid Begin									*/
/*==================================================================================*/
	function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob; 
	}
	
	$scope.LoadingBuat = false;
	$scope.BuatFakturPajak_BuatCSV_Clicked=function() {
		
		$scope.LoadingBuat = true;
		var selectedRow = $scope.GridApiBillingList.selection.getSelectedRows();
		$scope.xselectedRow = selectedRow;
		var countservices = 0;
		var countparts = 0;
		var countunit = 0;
		var tmpFlagsama = 0;
		var tmpFlagbeda = 0;
		console.log("selected Row : ", selectedRow);
		
		if (selectedRow.length < 1)
		{
			bsNotify.show({
				title:"warning",
				content:"Tidak ada Data yang dipilih",
				type:"warning"
			});
			$scope.LoadingBuat = false;
			return;
		}
		// cek jika udah pernah di gabung 
		for(var j=0; j<selectedRow.length; j++){
			if( selectedRow[j].TaxInvoiceNoJoin != null &&  selectedRow[j].TaxInvoiceNoJoin != undefined){
				
				$scope.cek = selectedRow[j].TaxInvoiceNoJoin;

				for (var l=0; l<selectedRow.length; l++){
					if( selectedRow[j].TaxInvoiceNoJoin != null &&  selectedRow[j].TaxInvoiceNoJoin != undefined){
					
						$scope.cek2 = selectedRow[l].TaxInvoiceNoJoin;
						if($scope.cek == $scope.cek2 && j != l){
							tmpFlagsama = 1;
						}else if ($scope.cek != $scope.cek2 && j != l){
							tmpFlagbeda = 1;		
						}
						
					}
					
				}
			}
			
			
		}
		if(tmpFlagsama == 1 && tmpFlagbeda == 0){
			bsNotify.show({
						title:"danger",
						content:"Data Sudah Pernah di gabungkan",
						type:"danger"
					});
			$scope.LoadingBuat = false;
					return;
		} else {
			for(var i=0; i<selectedRow.length; i++){
				if(selectedRow[i].BusinessType == "Services"){
					countservices++;
					console.log("test cek kesini")
				}else if(selectedRow[i].BusinessType == "Unit"){
					countunit++;
				}else if(selectedRow[i].BusinessType == "Parts"){
					countparts++;
				}
			}

			if(countservices == selectedRow.length){
				console.log("Cek jumlah Count Service",countservices)
				$scope.BuatCSV(selectedRow);

			}else if(countparts == selectedRow.length){
				console.log("Cek jumlah Count Parts",countservices)
				$scope.BuatCSV(selectedRow);
			}else if(countunit == selectedRow.length){
				console.log("Cek jumlah Count Unit",countservices)
				$scope.BuatCSV(selectedRow);
			}else{
				bsNotify.show({
					title:"warning",
					content:"Penggabungan hanya untuk tipe bisnis yang sama.",
					type:"warning"
				});
				$scope.LoadingBuat = false;
				return;
			}
		}



	}

	$scope.BuatCSV = function (selectedRow){
		var ErrorMsg  = "";
		var billingList = [];
		var inputData = [{
			"CustomerId" : $scope.BuatFakturPajak_CustomerId,
			"BillingList" : selectedRow
		}]

		console.log("data selected row", selectedRow)
		angular.forEach(selectedRow, function (value, key) {
			billingList.push({
				"billingid": value.BillingId,
				"billingcode": value.BillingCode,
				"isTaxSubstitute": "Tidak"
			})
			
		});
		console.log("bilinglist", billingList)
				
		//PembuatanFakturPajakFactory.getCSV($scope.BuatFakturPajak_CustomerId, selectedRow)
		PembuatanFakturPajakFactory.createCSV(inputData)
		.then(
			function(res){
				// Nomor Faktur kosong 
				console.log("cek data upload", res.data)
				ErrorMsg = res.data;
				if(ErrorMsg == "Sisa Nomor Faktur Pajak 0"){
					bsNotify.show({
						title: "Warning",
						content: "Sisa Nomor Faktur Pajak 0",
						type: "warning"
					});
					return;
				}

				if (res)
				{
					$scope.DownloadCSV(0, billingList, '', 0);
					bsNotify.show({
						title:"success",
						content:"Data faktur pajak berhasil dibuat",
						type:"success"
					});
				}
				else{
					//alert("Data tidak ditemukan!");
					bsNotify.show({
						title:"warning",
						content:"Data faktur pajak gagak dibuat!",
						type:"warning"
					});
					$scope.LoadingBuat = false;
					return;
				}
				// var contentType = 'text/csv';
				// var b64Data = res.data;

				// var blob = b64toBlob(b64Data, contentType);
				// var blobUrl = URL.createObjectURL(blob);

				// saveAs(blob, 'ContohCSV' + '.csv');

				// return res;
				//var filename = 'testcsv.csv';
				//var contentType = 'text/csv;charset=utf-8';
				//var blob = new Blob([res.data], { type: contentType });
				//saveAs(blob, selectedRow[0].BillingCode + '.csv');
				
			},
			function(err){
				console.log("err=>",err);
			}
		);
	}

	// Download CSV
	$scope.DownloadCSV = function (CustomerId, billingList, TaxDocDate, OutletId) {

		PembuatanFakturPajakFactory.getCSV(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {

					// var contentType = 'text/csv';
					// var b64Data = res.data;

					// var blob = b64toBlob(b64Data, contentType);
					// var blobUrl = URL.createObjectURL(blob);

					// saveAs(blob, 'ContohCSV' + '.csv');

					// return res;
					var filename = 'testcsv.csv';
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'Pembuatan Faktur Pajak (Manual)' + '.csv');
					$scope.BuatFakturPajak_Cari_Clicked();
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

});

app.filter('rupiahC', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			var valS = val.toString().split('.');
			val = valS[0];
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			if (valS.length > 1 )
			{
				val = val + ','+valS[1];
			}
			
		}
		return val;
	};
});