angular.module('app')
	.factory('BiayaMateraiFactory', function ($http, CurrentUser) {
	return{
		getData: function(SearchText, SearchType){
			//var url = '/api/fe/FinanceStampDuty/start/1/limit/100';
			var url = '/api/fe/FinanceStampDuty/GetData?start=1&limit=100&filterData=' + SearchText + '|' + SearchType;
			console.log("url Get Biaya Materai : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(biayaMateraiData){
			var url = '/api/fe/FinanceStampDuty/create/';
			var ArrayBiayaMateraiData = [biayaMateraiData];
			var param = JSON.stringify(ArrayBiayaMateraiData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(biayaMateraiData){
			var url = '/api/fe/FinanceStampDuty/Update/';
			var ArrayBiayaMateraiData = [biayaMateraiData];
			var param = JSON.stringify(ArrayBiayaMateraiData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/fe/FinanceStampDuty/deletedata/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});