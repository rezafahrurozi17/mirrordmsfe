var app = angular.module('app');
app.controller('BiayaMateraiController', function ($scope, $http, $filter, CurrentUser, BiayaMateraiFactory, $timeout, bsNotify) {

	// //----------------------------------
	// // Start-Up
	// //----------------------------------
	// $scope.$on('$viewContentLoaded', function() {
	// //$scope.loading = true;
	// $scope.gridData = [];
	// });
	// $scope.actionCancelCaption="Kembali";
	// //----------------------------------
	// // Initialization
	// //----------------------------------

	$scope.MainBiayaMaterai_Show = true;
	$scope.TambahBiayaMaterai_Show = false;
	$scope.testNominal = [];
	$scope.tesInput = [];

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOptions = { format: dateFormat };
	$scope.optionsLihatBiayaMaterai_Search = [{ name: 'Nominal', value: 'nominal' }, { name: 'Minimal Nilai Transaksi', value: 'min' }, { name: 'Maksimal Nilai Transaksi', value: 'max' }, { name: 'Tanggal Mulai Berlaku', value: 'validate' }];
	$scope.optionsLihatBiayaMaterai_Search_M = [{ name: "Hapus", value: "Hapus" }];

	$scope.MainBiayaMaterai_Tambah_Clicked = function () {
		$scope.MainBiayaMaterai_Show = false;
		$scope.TambahBiayaMaterai_Show = true;
		CurrentStatus = 'New';
	}

	$scope.TambahBiayaMaterai_Kembali_Clicked = function () {
		$scope.optionsLihatBiayaMaterai_Search_M = [{ name: "Hapus", value: "Hapus" }];
		$scope.LihatBiayaMaterai_Search_M = "";
		$scope.MainBiayaMaterai_Show = true;
		$scope.TambahBiayaMaterai_Show = false;
		$scope.Nominal = "";
		$scope.TrxMinNominal = "";
		$scope.TrxMaxNominal = "";
		$scope.TambahBeaMaterai_ValidDate = "";
	}

	$scope.TambahBiayaMaterai_Simpan_Clicked = function () {
		console.log("test save", $scope.TrxMinNominal);
		if (CurrentStatus == 'New') {
			var BankData = {
				"StampDutyId": 0,
				"Nominal": $scope.Nominal,
				"TrxMinNominal": $scope.TrxMinNominal,
				"TrxMaxNominal": $scope.TrxMaxNominal,
				"ValidDate": $scope.TambahBeaMaterai_ValidDate, //$filter('date')(new Date($scope.TambahBeaMaterai_ValidDate.toLocaleString()), 'yyyyMMdd')
				"LastModifiedDate": $scope.LastModifiedDate
			};

			BiayaMateraiFactory.create(BankData)
				.then(
					function (res) {
						if(res.data.code == "200"){
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Berhasil menambah data biaya materai",
									type: 'success'
								}
							);
							$scope.TambahBiayaMaterai_ClearFields();
							$scope.MainBiayaMaterai_GetList();
							$scope.TambahBiayaMaterai_Kembali_Clicked();
						}
						else
						{
							bsNotify.show(
								{
									title: "Error",
									content: res.data.message,
									type: 'error'
								}
							);
							$scope.TambahBiayaMaterai_ClearFields();
							$scope.MainBiayaMaterai_GetList();
							$scope.TambahBiayaMaterai_Kembali_Clicked();
						}
						console.log('return =>>>> ' + res);
					}
				);
		}
		else if (CurrentStatus == 'Update') {
			var BankData = {
				"StampDutyId": $scope.StampDutyId,
				"Nominal": $scope.Nominal,
				"TrxMinNominal": $scope.TrxMinNominal,
				"TrxMaxNominal": $scope.TrxMaxNominal,
				"ValidDate": $scope.TambahBeaMaterai_ValidDate,//$filter('date')(new Date($scope.TambahBeaMaterai_ValidDate.toLocaleString()), 'yyyyMMdd')
				"LastModifiedDate": $filter('date')($scope.LastModifiedDate,'yyyy-MM-dd HH:mm:ss')
			};
			BiayaMateraiFactory.update(BankData)
				.then(
					function (res) {
						if(res.data.code == "200"){
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Berhasil mengubah data biaya materai",
									type: 'success'
								}
							);
							$scope.TambahBiayaMaterai_ClearFields();
							$scope.MainBiayaMaterai_GetList();
							$scope.TambahBiayaMaterai_Kembali_Clicked();
						}
						else
						{
							bsNotify.show(
								{
									title: "Error",
									content: res.data.message,
									type: 'error'
								}
							);
							$scope.TambahBiayaMaterai_ClearFields();
							$scope.MainBiayaMaterai_GetList();
							$scope.TambahBiayaMaterai_Kembali_Clicked();
						}
						console.log('return =>>>> ' + res);
					}
				);

		}
	}

	$scope.TambahBiayaMaterai_ClearFields = function () {
		$scope.StampDutyId = 0;
		$scope.Nominal = '';
		$scope.TrxMinNominal = '';
		$scope.TrxMaxNominal = ''
		CurrentStatus = '';
		$scope.TambahBeaMaterai_ValidDate = undefined;
	}

	$scope.MainBiayaMaterai_EditData = function (row) {
		$scope.MainBiayaMaterai_Show = false;
		$scope.TambahBiayaMaterai_Show = true;
		CurrentStatus = 'Update'

		$scope.StampDutyId = row.entity.StampDutyId;
		$scope.Nominal = row.entity.Nominal;
		$scope.TrxMinNominal = row.entity.TrxMinNominal;
		$scope.TrxMaxNominal = row.entity.TrxMaxNominal;
		$scope.TambahBeaMaterai_ValidDate = row.entity.ValidDate;
		$scope.LastModifiedDate = row.entity.LastModifiedDate;
	}

	$scope.actDel = function () {
		// function actDel() {
		if ($scope.LihatBiayaMaterai_Search_M == "Hapus") {
			$scope.BeaMaterai_TotalSelectedData = $scope.MainBiayaMateraiGridAPI.selection.getSelectedCount();
			angular.element('#BiayaMateraiModal').modal('show');
		}
	}

	$scope.BiayaMateraiOK_Button_Clicked = function () {
		var deleteData = [];
		angular.forEach($scope.MainBiayaMateraiGridAPI.selection.getSelectedRows(), function (row, index) {
			deleteData.push(row.StampDutyId);
		});
		BiayaMateraiFactory.delete(deleteData).
			then(
				function (res) {
					$scope.MainBiayaMaterai_GetList();
					$scope.MainBiayaMateraiGridAPI.selection.clearSelectedRows();
					$scope.LihatBiayaMaterai_Search_M = "";
					
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Berhasil menghapus biaya materai",
							type: 'success'
						}
					);
				}
			);
	}

	$scope.actRefresh = function () {
		$scope.MainBiayaMaterai_GetList();
	}

	$scope.BiayaMateraiCancel_Button_Clicked = function () {
		$scope.LihatBiayaMaterai_Search_M = "";
		angular.element('#BiayaMateraiModal').modal('hide');
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------
	// var actionClick= '<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-if="grid.appScope.allowEdit &amp;&amp; !grid.appScope.hideEditButton &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)" ng-click="grid.appScope.gridClickEditHandler(row.entity)" tabindex="0">'
	// +'<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>'
	// +'</a>';

	$scope.MainBiayaMaterai_UIGrid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		// allowApprove: false,
		// allowReject: false,
		// allowReview: false,
		// allowPrint: false,
		//showTreeExpandNoChildren: true,
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,

		columnDefs: [
			{ name: 'id', field: 'StampDutyId', visible: false },
			{ name: 'Nominal', field: 'Nominal', cellFilter: "rupiahC" },
			{ name: 'Minimal Nilai Transaksi', field: 'TrxMinNominal', cellFilter: "rupiahC" },
			{ name: 'Maksimal Nilai Transaksi', field: 'TrxMaxNominal', cellFilter: "rupiahC" },
			{ name: 'Tanggal Mulai Berlaku', field: 'ValidDate', cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'Last Modified', field: 'LastModifiedDate', cellFilter: 'date:\"dd/MM/yyyy HH:mm:ss\"', visible: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.MainBiayaMaterai_EditData(row)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'
			}
			// {
			// name: 'action',
			// allowCellFocus: false,
			// enableColumnMenu: false,
			// enableSorting: false,
			// width: '10%',
			// enableColumnResizing: true,
			// cellTemplate: actionClick
			// }
		],
		onRegisterApi: function (gridApi) {
			$scope.MainBiayaMateraiGridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.selectedRows = $scope.MainBiayaMateraiGridAPI.selection.getSelectedRows();
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
				$scope.selectedRows = $scope.MainBiayaMateraiGridAPI.selection.getSelectedRows();
			});
		}
	};

	$scope.MainBiayaMaterai_GetList = function () {
		BiayaMateraiFactory.getData($scope.LihatBiayaMaterai_Search_Text, $scope.LihatBiayaMaterai_Search)
			.then(
				function (res) {
					$scope.MainBiayaMaterai_UIGrid.data = res.data.Result;
				}
			);
	}

	$scope.LihatBiayaMaterai_Filter_Clicked = function () {
		$scope.MainBiayaMaterai_GetList();
	}

	$scope.MainBiayaMaterai_GetList();
});

app.filter('rupiahC', function () {
	return function (val) {
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
		}
		return val;
	};
});