angular.module('app')
	.factory('ParameterGLAccountFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
		var mappingGroupId = 3850;
		var cashBankGroupId = 3810;
		var taxGroupId = 3830;
	return{
		getDataPGA_MappingGroup : function(){
			var url = '/api/fe/FinanceParameterGLAccount/GetParamInternal?groupId='+mappingGroupId;
			console.log("url Mapping Group : ", url);
			var response=$http.get(url);
			return response;
		},
		
		getDataPGA_PerMappingGroup : function(mappingValueId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDataPerMappingGroup?valueId='+mappingValueId;
			console.log("url Data Per Mapping Group : ", url);
			var response=$http.get(url);
			return response;
		},
		
		getDataPGA_GLAccount : function(){
			var url = '/api/fe/FinanceParameterGLAccount/GetGLAccount';
			console.log("url GetGLAccount : ", url);
			var res=$http.get(url);
			return res;
		},
		
		savePGA_MappingGLAccount:function(InputData){
			var url = '/api/fe/FinanceParameterGLAccount/PostSaveMappingGLAccount';
			var param = JSON.stringify(InputData);
			console.log("object input saveMappingGLAccount", param);
			var res=$http.post(url, param);
			return res;
		},
		
		getDataPGA_CashBankType : function(){
			var url = '/api/fe/FinanceParameterGLAccount/GetParamInternal?groupId='+cashBankGroupId;
			console.log("url Cash Bank Type : ", url);
			var response=$http.get(url);
			return response;
		},
		
		getDataPGA_TaxType : function(){
			var url = '/api/fe/FinanceParameterGLAccount/GetParamInternal?groupId='+taxGroupId;
			console.log("url Tax Type : ", url);
			var response=$http.get(url);
			return response;
		},
		
		getDataPGA_CashBankGLAccount : function(Start, Limit, FilterColumn, FilterTeks){
			var url;
			if (FilterTeks == undefined || FilterTeks.trim().length == 0)
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataCashBankGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn;
			}
			else
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataCashBankGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn+'&filterTeks='+FilterTeks;
			}
			
			console.log("url Cash Bank GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		deletePGA_CashBankGLAccount : function(GLAccountId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDeleteCashBankGLAccount?glAccountId='+GLAccountId;
			console.log("url Delete Cash Bank GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		savePGA_CashBankGLAccount : function(CashBankGLAccountData){
			var url = '/api/fe/FinanceParameterGLAccount/PostSaveCashBankGLAccount';
			var param = JSON.stringify(CashBankGLAccountData);
			var response=$http.post(url, param);
			return response;
		},
		
		getDataPGA_BGTGLAccount : function(Start, Limit, FilterColumn, FilterTeks){
			var url;
			if (FilterTeks == undefined || FilterTeks.trim().length == 0)
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataBGTGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn;
			}
			else
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataBGTGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn+'&filterTeks='+FilterTeks;
			}
			
			console.log("url General Transaction GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		deletePGA_BGTGLAccount : function(GLAccountId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDeleteBGTGLAccount?glAccountId='+GLAccountId;
			console.log("url Delete General Transaction GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		savePGA_BGTGLAccount : function(BGTGLAccountData){
			var url = '/api/fe/FinanceParameterGLAccount/PostSaveBGTGLAccount';
			var param = JSON.stringify(BGTGLAccountData);
			var response=$http.post(url, param);
			return response;
		},
		
		getDataPGA_TaxGLAccount : function(Start, Limit, FilterColumn, FilterTeks){
			var url;
			if (FilterTeks == undefined || FilterTeks.trim().length == 0)
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataTaxGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn;
			}
			else
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataTaxGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn+'&filterTeks='+FilterTeks;
			}
			
			console.log("url Tax GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		deletePGA_TaxGLAccount : function(GLAccountId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDeleteTaxGLAccount?glAccountId='+GLAccountId;
			console.log("url Delete Tax GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		savePGA_TaxGLAccount : function(TaxGLAccountData){
			var url = '/api/fe/FinanceParameterGLAccount/PostSaveTaxGLAccount';
			var param = JSON.stringify(TaxGLAccountData);
			var response=$http.post(url, param);
			return response;
		},
		
		getDataPGA_ClearingGLAccount : function(Start, Limit, FilterColumn, FilterTeks){
			var url;
			if (FilterTeks == undefined || FilterTeks.trim().length == 0)
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataClearingGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn;
			}
			else
			{
				url = '/api/fe/FinanceParameterGLAccount/GetDataClearingGLAccount?start='+Start+'&limit='+Limit+'&filterColumn='+FilterColumn+'&filterTeks='+FilterTeks;
			}
			
			console.log("url Clearing GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		deletePGA_ClearingGLAccount : function(GLAccountId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDeleteClearingGLAccount?glAccountId='+GLAccountId;
			console.log("url Delete Clearing GL Account : ", url);
			var response=$http.get(url);
			return response;
		},
		
		savePGA_ClearingGLAccount : function(ClearingGLAccountData){
			var url = '/api/fe/FinanceParameterGLAccount/PostSaveClearingGLAccount';
			var param = JSON.stringify(ClearingGLAccountData);
			var response=$http.post(url, param);
			return response;
		}
	}
});