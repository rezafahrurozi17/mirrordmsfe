var app = angular.module('app');
app.controller('ParameterGLAccountController', function ($scope, $http, $filter, bsAlert,CurrentUser, ParameterGLAccountFactory, $timeout, bsNotify) {
	var user = CurrentUser.user();
	//dynamic page custom
	var paginationOptions_CashBank = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};
	var paginationOptions_BGT = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};
	var paginationOptions_Pajak = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};
	var paginationOptions_Clearing = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	//general
	$scope.uiGridPageSize = 10;
	$scope.ParameterGLAccountBody_Show = true;
	$scope.ParameterGLAccountTab_Show = true;
	$scope.PGA_GLAccountList = [];
	$scope.optionsPGA_GLAccount = [];
	$scope.optionsPGA_GLAccount_Cash = [];
	$scope.optionsPGA_GLAccount_GT = [];
	$scope.optionsPGA_GLAccount_Tax = [];
	$scope.optionsPGA_GLAccount_Clearing = [];
	$scope.uiGridPageSize = 10;

	//mapping tab variables
	$scope.optionsPGA_MappingGroup = [];
	$scope.dataPerMappingGroup = [];

	//cash & bank variables
	$scope.PGA_CashBankTypeList = [];
	$scope.optionsPGA_CashBankType = [];

	//tax variables
	$scope.PGA_TaxTypeList = [];
	$scope.optionsPGA_TaxType = [];

	$scope.PGA_CashBankFilterColumnList = [{ name: "Nomor GL Account", value: 1 }, { name: "Nama GL Account", value: 2 }, { name: "Tipe Cash & Bank", value: 3 }];
	$scope.PGA_BGTFilterColumnList = [{ name: "Nomor GL Account", value: 1 }, { name: "Nama GL Account", value: 2 }];
	$scope.PGA_PajakTypeList = [];
	$scope.PGA_PajakFilterColumnList = [{ name: "Nomor GL Account", value: 1 }, { name: "Nama GL Account", value: 2 }, { name: "Tipe GL Pajak", value: 3 }];
	$scope.PGA_ClearingFilterColumnList = [{ name: "Nomor GL Account", value: 1 }, { name: "Nama GL Account", value: 2 }];

	//Declare Function
	$scope.PGA_GetMappingGroup = function () {
		ParameterGLAccountFactory.getDataPGA_MappingGroup()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.optionsPGA_MappingGroup.push({ name: value.Label, value: value.ValueId });
					});
				}
			);
	}

	$scope.PGA_UpdateMappingGrid = function () {
		if ($scope.PGA_MappingGroup != undefined) {
			ParameterGLAccountFactory.getDataPGA_PerMappingGroup($scope.PGA_MappingGroup)
				.then(
					function (res) {
						var temp = [];
						angular.forEach(res.data.Result, function (value, key) {
							temp.push({
								"GroupingId": value.GroupingId, "Parameter": value.COAOverrideName, "Description": value.COAOverrideDesc,
								"GLAccountID": value.GLAccountId, "GLAccountNo": value.GLAccountCode, "GLAccountName": value.GLAccountName
							});
						});
						$scope.PGA_MappingGLAccount_UIGrid.data = temp;
					}
				);
		}
		else {
			$scope.PGA_MappingGLAccount_UIGrid.data = [];
		}
	}

	$scope.PGA_GetDataGLAccount = function () {
		ParameterGLAccountFactory.getDataPGA_GLAccount()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.optionsPGA_GLAccount.push({ name: value.GLAccountName, value: value.GLAccountId });
						$scope.optionsPGA_GLAccount_Cash.push({ name: value.GLAccountCode, value: value.GLAccountId });
						$scope.optionsPGA_GLAccount_GT.push({ name: value.GLAccountCode, value: value.GLAccountId });
						$scope.optionsPGA_GLAccount_Tax.push({ name: value.GLAccountCode, value: value.GLAccountId });
						$scope.optionsPGA_GLAccount_Clearing.push({ name: value.GLAccountCode, value: value.GLAccountId });
						$scope.PGA_GLAccountList[value.GLAccountId] = { GLAccountCode: value.GLAccountCode, GLAccountName: value.GLAccountName, GLDesc: value.GLDesc };
					});
				}
			);
	}

	$scope.PGA_GetCashBankTypes = function () {
		ParameterGLAccountFactory.getDataPGA_CashBankType()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.optionsPGA_CashBankType.push({ name: value.Label, value: value.ValueId });
						$scope.PGA_CashBankTypeList[value.ValueId] = value.Label;
					});
				}
			);
	}

	$scope.PGA_CB_ShowGLAccountData = function () {
		if ($scope.PGA_CBGLAccountID != undefined) {
			$scope.PGA_CBGLAccountName = $scope.PGA_GLAccountList[$scope.PGA_CBGLAccountID].GLAccountName;
			$scope.PGA_CBInformation = $scope.PGA_GLAccountList[$scope.PGA_CBGLAccountID].GLDesc;
		}
		else {
			$scope.PGA_CBGLAccountName = "";
			$scope.PGA_CBInformation = "";
		}
	}

	$scope.PGA_BGT_ShowGLAccountData = function () {
		if ($scope.PGA_BGTGLAccountID != undefined) {
			$scope.PGA_BGTGLAccountName = $scope.PGA_GLAccountList[$scope.PGA_BGTGLAccountID].GLAccountName;
			$scope.PGA_BGTInformation = $scope.PGA_GLAccountList[$scope.PGA_BGTGLAccountID].GLDesc;
		}
		else {
			$scope.PGA_BGTGLAccountName = "";
			$scope.PGA_BGTInformation = "";
		}
	}

	$scope.PGA_Tax_ShowGLAccountData = function () {
		if ($scope.PGA_TaxGLAccountID != undefined) {
			$scope.PGA_TaxGLAccountName = $scope.PGA_GLAccountList[$scope.PGA_TaxGLAccountID].GLAccountName;
			$scope.PGA_TaxInformation = $scope.PGA_GLAccountList[$scope.PGA_TaxGLAccountID].GLDesc;
		}
		else {
			$scope.PGA_TaxGLAccountName = "";
			$scope.PGA_TaxInformation = "";
		}
	}

	$scope.PGA_Clearing_ShowGLAccountData = function () {
		if ($scope.PGA_ClearingGLAccountID != undefined) {
			$scope.PGA_ClearingGLAccountName = $scope.PGA_GLAccountList[$scope.PGA_ClearingGLAccountID].GLAccountName;
			$scope.PGA_ClearingInformation = $scope.PGA_GLAccountList[$scope.PGA_ClearingGLAccountID].GLDesc;
		}
		else {
			$scope.PGA_ClearingGLAccountName = "";
			$scope.PGA_ClearingInformation = "";
		}
	}

	$scope.PGA_GetTaxTypes = function () {
		ParameterGLAccountFactory.getDataPGA_TaxType()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.optionsPGA_TaxType.push({ name: value.Label, value: value.ValueId });
						$scope.PGA_TaxTypeList[value.ValueId] = value.Label;
					});
				}
			);
	}

	$scope.PGA_Mapping_Simpan_Button_Clicked = function () {
		var inputData = [];
		var eachData = {};

		angular.forEach($scope.PGA_MappingGLAccount_UIGrid.data, function (value, key) {
			if (value.GLAccountID != undefined) {
				eachData = {
					"OutletID": 0,
					"GroupingID": value.GroupingId,
					"GLAccountID": value.GLAccountID
				}

				inputData.push(eachData);
			}
		});

		console.log("data input sbelum masuk factory :", inputData);
		ParameterGLAccountFactory.savePGA_MappingGLAccount(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
				},
				function (err) {
					console.log("err : ", err);
				}
			);
	}

	$scope.PGA_CashBank_Tambah_Button_Clicked = function () {
		var isHO = ($scope.PGA_CashBank_CBGLAccountHO == undefined) ? false : $scope.PGA_CashBank_CBGLAccountHO;
		var row_data = {
			"GLAccountId": $scope.PGA_CBGLAccountID,
			"GLAccountNo": $scope.PGA_GLAccountList[$scope.PGA_CBGLAccountID].GLAccountCode,
			"GLAccountName": $scope.PGA_CBGLAccountName,
			"CashBankTypeId": $scope.PGA_CashBankTypes,
			"CashBankType": $scope.PGA_CashBankTypeList[$scope.PGA_CashBankTypes],
			"GLAccountHO": isHO
		}

		$scope.PGA_CashBankGLAccount_UIGrid.data.push(row_data);
	}

	$scope.PGA_CashBank_Hapus_Button_Clicked = function (row) {
		var index = $scope.PGA_CashBankGLAccount_UIGrid.data.indexOf(row.entity);
		var tempGLAccountId = $scope.PGA_CashBankGLAccount_UIGrid.data[index].GLAccountId;
		ParameterGLAccountFactory.deletePGA_CashBankGLAccount(tempGLAccountId)
			.then(
				function (res) {
					$scope.PGA_CashBankGLAccount_UIGrid.data.splice(index, 1);
				}
			);

	}

	$scope.PGA_CashBank_Filter_Button_Clicked = function () {
		$scope.PGA_CashBankGLAccount_UIGrid_Paging(1);
	}

	$scope.PGA_CashBank_Simpan_Button_Clicked = function () {
		var inputData = [];
		var eachData = {};
		debugger;
		angular.forEach($scope.PGA_CashBankGLAccount_UIGrid.data, function (value, key) {
			eachData = {
				"OutletID": 0,
				"GLAccountId": value.GLAccountId,
				"CashBankTypeId": value.CashBankTypeId,
				"isHO": value.GLAccountHO
			};

			inputData.push(eachData);
		});

		console.log("data input sebelum masuk factory :", inputData);
		ParameterGLAccountFactory.savePGA_CashBankGLAccount(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
				},
				function (err) {
					console.log("err : ", err);
				}
			);
	}

	// OLD
	// $scope.PGA_BGT_Tambah_Button_Clicked = function () {
	// 	var row_data = {
	// 		"GLAccountId": $scope.PGA_BGTGLAccountID,
	// 		"GLAccountNo": $scope.PGA_GLAccountList[$scope.PGA_BGTGLAccountID].GLAccountCode,
	// 		"GLAccountName": $scope.PGA_BGTGLAccountName
	// 	}

	// 	$scope.PGA_BGTGLAccount_UIGrid.data.push(row_data);
	// }
	// $scope.PGA_BGT_Hapus_Button_Clicked = function (row) {
	// 	var index = $scope.PGA_BGTGLAccount_UIGrid.data.indexOf(row.entity);
	// 	var tempGLAccountId = $scope.PGA_BGTGLAccount_UIGrid.data[index].GLAccountId;
	// 	ParameterGLAccountFactory.deletePGA_BGTGLAccount(tempGLAccountId)
	// 		.then(
	// 			function (res) {
	// 				$scope.PGA_BGTGLAccount_UIGrid.data.splice(index, 1);
	// 			}
	// 		);
	// };

	// NEW
	// Klik Tambah
	$scope.PGA_BGT_Tambah_Button_Clicked = function () {
		bsAlert.alert({
			title: "Apakah anda yakin untuk menyimpan data?",
			text: "",
			type: "warning",
			showCancelButton: true
		},
		function() {
			$scope.PGA_BGT_Simpan_Button_Clicked();
		},
		function() {

		}
	)
	}
	$scope.PGA_BGT_Hapus = function(row){
		var index = $scope.PGA_BGTGLAccount_UIGrid.data.indexOf(row.entity);
		var tempGLAccountId = $scope.PGA_BGTGLAccount_UIGrid.data[index].GLAccountId;
		ParameterGLAccountFactory.deletePGA_BGTGLAccount(tempGLAccountId)
			.then(
				function (res) {
					$scope.PGA_BGTGLAccount_UIGrid.data.splice(index, 1);
				}
			);
	}
	// Klik Hapus
	$scope.PGA_BGT_Hapus_Button_Clicked = function (row) {
		bsAlert.alert({
				title: "Apakah anda yakin untuk menghapus data?",
				text: "",
				type: "warning",
				showCancelButton: true
			},
			function() {
				$scope.PGA_BGT_Hapus(row);
			},
			function() {

			}
		)
	}


	$scope.PGA_BGT_Filter_Button_Clicked = function () {
		$scope.PGA_BGTGLAccount_UIGrid_Paging(1);
	}

	$scope.PGA_BGT_Simpan_Button_Clicked = function () {
		var inputData = [];
		var eachData = {};
		var row_data = {
			"GLAccountId": $scope.PGA_BGTGLAccountID,
			"GLAccountNo": $scope.PGA_GLAccountList[$scope.PGA_BGTGLAccountID].GLAccountCode,
			"GLAccountName": $scope.PGA_BGTGLAccountName
		}

		$scope.PGA_BGTGLAccount_UIGrid.data.push(row_data);

		angular.forEach($scope.PGA_BGTGLAccount_UIGrid.data, function (value, key) {
			eachData = {
				"OutletID": 0,
				"GLAccountId": value.GLAccountId
			};

			inputData.push(eachData);
		});

		console.log("data input sebelum masuk factory :", inputData);
		ParameterGLAccountFactory.savePGA_BGTGLAccount(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
				},
				function (err) {
					console.log("err : ", err);
				}
			);
	}

	$scope.PGA_Pajak_Tambah_Button_Clicked = function () {
		var row_data = {
			"GLAccountId": $scope.PGA_TaxGLAccountID,
			"GLAccountNo": $scope.PGA_GLAccountList[$scope.PGA_TaxGLAccountID].GLAccountCode,
			"GLAccountName": $scope.PGA_TaxGLAccountName,
			"TaxTypeId": $scope.PGA_PajakTypes,
			"GLPajakType": $scope.PGA_TaxTypeList[$scope.PGA_PajakTypes]
		}

		$scope.PGA_PajakGLAccount_UIGrid.data.push(row_data);
	}

	$scope.PGA_Pajak_Hapus_Button_Clicked = function (row) {
		var index = $scope.PGA_PajakGLAccount_UIGrid.data.indexOf(row.entity);
		var tempGLAccountId = $scope.PGA_PajakGLAccount_UIGrid.data[index].GLAccountId;
		ParameterGLAccountFactory.deletePGA_TaxGLAccount(tempGLAccountId)
			.then(
				function (res) {
					$scope.PGA_PajakGLAccount_UIGrid.data.splice(index, 1);
				}
			);
	};

	$scope.PGA_Pajak_Filter_Button_Clicked = function () {
		$scope.PGA_PajakGLAccount_UIGrid_Paging(1);
	}

	$scope.PGA_Pajak_Simpan_Button_Clicked = function () {
		var inputData = [];
		var eachData = {};

		angular.forEach($scope.PGA_PajakGLAccount_UIGrid.data, function (value, key) {
			eachData = {
				"OutletID": 0,
				"GLAccountId": value.GLAccountId,
				"TaxTypeId": value.TaxTypeId
			};

			inputData.push(eachData);
		});

		console.log("data input sebelum masuk factory :", inputData);
		ParameterGLAccountFactory.savePGA_TaxGLAccount(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
				},
				function (err) {
					console.log("err : ", err);
				}
			);
	}

	$scope.PGA_Clearing_Tambah_Button_Clicked = function () {
		var row_data = {
			"GLAccountId": $scope.PGA_ClearingGLAccountID,
			"GLAccountNo": $scope.PGA_GLAccountList[$scope.PGA_ClearingGLAccountID].GLAccountCode,
			"GLAccountName": $scope.PGA_ClearingGLAccountName
		}

		$scope.PGA_ClearingGLAccount_UIGrid.data.push(row_data);
	}

	$scope.PGA_Clearing_Hapus_Button_Clicked = function (row) {
		var index = $scope.PGA_ClearingGLAccount_UIGrid.data.indexOf(row.entity);
		var tempGLAccountId = $scope.PGA_ClearingGLAccount_UIGrid.data[index].GLAccountId;
		ParameterGLAccountFactory.deletePGA_ClearingGLAccount(tempGLAccountId)
			.then(
				function (res) {
					$scope.PGA_ClearingGLAccount_UIGrid.data.splice(index, 1);
				}
			);
	};

	$scope.PGA_Clearing_Filter_Button_Clicked = function () {
		$scope.PGA_ClearingGLAccount_UIGrid_Paging(1);
	}

	$scope.PGA_Clearing_Simpan_Button_Clicked = function () {
		var inputData = [];
		var eachData = {};

		angular.forEach($scope.PGA_ClearingGLAccount_UIGrid.data, function (value, key) {
			eachData = {
				"OutletID": 0,
				"GLAccountId": value.GLAccountId
			};

			inputData.push(eachData);
		});

		console.log("data input sebelum masuk factory :", inputData);
		ParameterGLAccountFactory.savePGA_ClearingGLAccount(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
				},
				function (err) {
					console.log("err : ", err);
				}
			);
	}

	//Initiate Function Call
	$scope.PGA_GetMappingGroup();
	$scope.PGA_GetDataGLAccount();
	$scope.PGA_GetCashBankTypes();
	$scope.PGA_GetTaxTypes();

	//MAPPING GL ACCOUNT LIST (UI GRID)
	$scope.PGA_MappingGLAccount_UIGrid = {
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination: false,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: "GroupingId", field: "GroupingId", visible: false },
			{ name: "Parameter", field: "Parameter", enableCellEdit: false },
			{ name: "Deskripsi", field: "Description", enableCellEdit: false },
			{ name: "GL Account ID", field: "GLAccountID", visible: false },
			{
				name: "Nomor GL Account", field: "GLAccountNo", enableCellEdit: false},
			{
				name: "Nama GL Account", field: "GLAccountName", enableCellEdit: false,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownOptionsArray: $scope.optionsPGA_GLAccount, editDropdownIdLabel: 'name', editDropdownValueLabel: 'name'
			}
		],
		onRegisterApi: function (gridApi) {
			gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
				console.log("CEK DROPDOWN")
				if (rowEntity.GLAccountName == undefined) {
					rowEntity.GLAccountID = 0;
					rowEntity.GLAccountNo = "";
					rowEntity.GLAccountName = "";
				}
				else {
					for (var key in $scope.PGA_GLAccountList) {
						if ($scope.PGA_GLAccountList[key].GLAccountName == rowEntity.GLAccountName) {
							rowEntity.GLAccountID = parseInt(key);
							rowEntity.GLAccountNo = $scope.PGA_GLAccountList[key].GLAccountCode;
							break;
						}
					}

				}
			});
		}
	};

	//CASH & BANK LIST (UI GRID)
	$scope.PGA_CashBankGLAccount_UIGrid = {
		paginationPageSize: 10,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: "GL Account Id", field: "GLAccountId", visible: false },
			{ name: "Nomor GL Account", field: "GLAccountNo", enableCellEdit: false },
			{ name: "Nama GL Account", field: "GLAccountName", enableCellEdit: false },
			{ name: "Tipe Cash & Bank Id", field: "CashBankTypeId", visible: false },
			{ name: "Tipe Cash & Bank", field: "CashBankType", enableCellEdit: false },
			{
				name: "GL Account HO", field: "GLAccountHO", enableCellEdit: false, type: "boolean",
				cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.GLAccountHO">'
			},
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.PGA_CashBank_Hapus_Button_Clicked(row)">Hapus</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiCashBankGLAccountList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_CashBank.pageNumber = pageNumber;
				paginationOptions_CashBank.pageSize = pageSize;
				$scope.PGA_CashBankGLAccount_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.PGA_CashBankGLAccount_UIGrid_Paging = function (pageNumber) {
		var filterColumn = ($scope.PGA_CashBankFilterColumn == undefined) ? 0 : $scope.PGA_CashBankFilterColumn;
		var filterTeks = ($scope.PGA_CBTeksFilter == undefined) ? "" : $scope.PGA_CBTeksFilter;
		ParameterGLAccountFactory.getDataPGA_CashBankGLAccount(pageNumber, paginationOptions_CashBank.pageSize, filterColumn, filterTeks)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						var grid_data = [];
						angular.forEach(res.data.Result, function (value, key) {
							var isHO = (value.isHO == 1) ? true : false;
							grid_data.push({
								"GLAccountId": value.GLAccountId,
								"GLAccountNo": value.GLAccountCode,
								"GLAccountName": value.GLAccountName,
								"CashBankType": $scope.PGA_CashBankTypeList[value.GLPurposeIdCashBank],
								"GLAccountHO": isHO
							});
						});

						$scope.PGA_CashBankGLAccount_UIGrid.data = grid_data;
						$scope.PGA_CashBankGLAccount_UIGrid.totalItems = res.data.Total;
						//$scope.PGA_CashBankGLAccount_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						//$scope.PGA_CashBankGLAccount_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						$scope.PGA_CashBankGLAccount_UIGrid.paginationPageSize = paginationOptions_CashBank.pageSize;
						$scope.PGA_CashBankGLAccount_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.PGA_CashBankGLAccount_UIGrid.paginationCurrentPage = pageNumber;
					}
				}
			);
	}

	$scope.PGA_CashBankGLAccount_UIGrid_Paging(1);

	//BIAYA GENERAL TRANSACTION LIST (UI GRID)
	$scope.PGA_BGTGLAccount_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: "GL Account Id", field: "GLAccountId", visible: false },
			{ name: "Nomor GL Account", field: "GLAccountNo", enableCellEdit: false },
			{ name: "Nama GL Account", field: "GLAccountName", enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.PGA_BGT_Hapus_Button_Clicked(row)">Hapus</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiBGTGLAccountList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_BGT.pageSize = pageSize;
				paginationOptions_BGT.pageNumber = pageNumber;
				$scope.PGA_BGTGLAccount_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.PGA_BGTGLAccount_UIGrid_Paging = function (pageNumber) {
		var filterColumn = ($scope.PGA_BGTFilterColumn == undefined) ? 0 : $scope.PGA_BGTFilterColumn;
		var filterTeks = ($scope.PGA_BGTTeksFilter == undefined) ? "" : $scope.PGA_BGTTeksFilter;
		ParameterGLAccountFactory.getDataPGA_BGTGLAccount(pageNumber, paginationOptions_BGT.pageSize, filterColumn, filterTeks)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						var grid_data = [];
						angular.forEach(res.data.Result, function (value, key) {
							grid_data.push({
								"GLAccountId": value.GLAccountId,
								"GLAccountNo": value.GLAccountCode,
								"GLAccountName": value.GLAccountName
							});
						});

						$scope.PGA_BGTGLAccount_UIGrid.data = grid_data;
						$scope.PGA_BGTGLAccount_UIGrid.totalItems = res.data.Total;
						//$scope.PGA_BGTGLAccount_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						//$scope.PGA_BGTGLAccount_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						$scope.PGA_BGTGLAccount_UIGrid.paginationPageSize = paginationOptions_BGT.pageSize;
						$scope.PGA_BGTGLAccount_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.PGA_BGTGLAccount_UIGrid.paginationCurrentPage = pageNumber;
					}
				}
			);
	}

	$scope.PGA_BGTGLAccount_UIGrid_Paging(1);

	//PAJAK LIST (UI GRID)
	$scope.PGA_PajakGLAccount_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: "GL Account Id", field: "GLAccountId", visible: false },
			{ name: "Nomor GL Account", field: "GLAccountNo", enableCellEdit: false },
			{ name: "Nama GL Account", field: "GLAccountName", enableCellEdit: false },
			{ name: "Tax Type Id", field: "TaxTypeId", visible: false },
			{ name: "Tipe GL Pajak", field: "GLPajakType", enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.PGA_Pajak_Hapus_Button_Clicked(row)">Hapus</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPajakGLAccountList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_Pajak.pageNumber = pageNumber;
				paginationOptions_Pajak.pageSize = pageSize;
				$scope.PGA_PajakGLAccount_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.PGA_PajakGLAccount_UIGrid_Paging = function (pageNumber) {
		var filterColumn = ($scope.PGA_PajakFilterColumn == undefined) ? 0 : $scope.PGA_PajakFilterColumn;
		var filterTeks = ($scope.PGA_PajakTeksFilter == undefined) ? "" : $scope.PGA_PajakTeksFilter;
		ParameterGLAccountFactory.getDataPGA_TaxGLAccount(pageNumber, paginationOptions_Pajak.pageSize, filterColumn, filterTeks)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						var grid_data = [];
						angular.forEach(res.data.Result, function (value, key) {
							grid_data.push({
								"GLAccountId": value.GLAccountId,
								"GLAccountNo": value.GLAccountCode,
								"GLAccountName": value.GLAccountName,
								"TaxTypeId": value.GLPurposeIdTax,
								"GLPajakType": $scope.PGA_TaxTypeList[value.GLPurposeIdTax]
							});
						});

						$scope.PGA_PajakGLAccount_UIGrid.data = grid_data;
						$scope.PGA_PajakGLAccount_UIGrid.totalItems = res.data.Total;
						//$scope.PGA_PajakGLAccount_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						//$scope.PGA_PajakGLAccount_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						$scope.PGA_PajakGLAccount_UIGrid.paginationPageSize = paginationOptions_Pajak.pageSize;
						$scope.PGA_PajakGLAccount_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.PGA_PajakGLAccount_UIGrid.paginationCurrentPage = pageNumber;
					}
				}
			);
	}

	$scope.PGA_PajakGLAccount_UIGrid_Paging(1);

	//CLEARING LIST (UI GRID)
	$scope.PGA_ClearingGLAccount_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: "GL Account Id", field: "GLAccountId", visible: false },
			{ name: "Nomor GL Account", field: "GLAccountNo", enableCellEdit: false },
			{ name: "Nama GL Account", field: "GLAccountName", enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.PGA_Clearing_Hapus_Button_Clicked(row)">Hapus</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiClearingGLAccountList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_Clearing.pageNumber = pageNumber;
				paginationOptions_Clearing.pageSize = pageSize;
				$scope.PGA_ClearingGLAccount_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.PGA_ClearingGLAccount_UIGrid_Paging = function (pageNumber) {
		var filterColumn = ($scope.PGA_ClearingFilterColumn == undefined) ? 0 : $scope.PGA_ClearingFilterColumn;
		var filterTeks = ($scope.PGA_ClearingTeksFilter == undefined) ? "" : $scope.PGA_ClearingTeksFilter;
		ParameterGLAccountFactory.getDataPGA_ClearingGLAccount(pageNumber, paginationOptions_Clearing.pageSize, filterColumn, filterTeks)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						var grid_data = [];
						angular.forEach(res.data.Result, function (value, key) {
							grid_data.push({
								"GLAccountId": value.GLAccountId,
								"GLAccountNo": value.GLAccountCode,
								"GLAccountName": value.GLAccountName
							});
						});

						$scope.PGA_ClearingGLAccount_UIGrid.data = grid_data;
						$scope.PGA_ClearingGLAccount_UIGrid.totalItems = res.data.Total;
						//$scope.PGA_ClearingGLAccount_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						//$scope.PGA_ClearingGLAccount_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
						$scope.PGA_ClearingGLAccount_UIGrid.paginationPageSize = paginationOptions_Clearing.pageSize;
						$scope.PGA_ClearingGLAccount_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.PGA_ClearingGLAccount_UIGrid.paginationCurrentPage = pageNumber;
					}
				}
			);
	}

	$scope.PGA_ClearingGLAccount_UIGrid_Paging(1);
});