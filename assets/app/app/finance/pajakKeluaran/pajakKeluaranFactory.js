/*
Edit by : 
	20170618, eric, initialize
*/
angular.module('app')
	.factory('PajakKeluaranFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user();
		var factory = {};
		var debugMode = true;

		factory.getDataBranchComboBox = function (orgId) {
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
			var res = $http.get(url);
			return res;
		}

		factory.getDataPajakKeluaran = function (Start, Limit, dateStart, dateEnd, nomorBilling, nomorFakturPajak, tanggalFakturAwal, tanggalFakturAkhir, tipeBisnis,
			KodeTransaksi, Nama, NPWP, TanggalBuatAwal, TanggalBuatAkhir, StatusDownload, OutletId, FGStatus, BelumApprove, SiapApprove,
			DalamProses, ApprovalSukses, Reject, Batal, Cancel) {
			var url = '/api/fe/FinanceVatOut/?start=' + Start + '&limit=' + Limit + '&datestart=' + dateStart + '&dateend=' + dateEnd
				+ '&outletid=' + OutletId
				+ '&BillingNo=' + nomorBilling
				+ '&TaxReceiptNo=' + nomorFakturPajak
				+ '&DateStartReceipt=' + tanggalFakturAwal
				+ '&DateEndReceipt=' + tanggalFakturAkhir
				+ '&BusinessType=' + tipeBisnis
				+ '&TranCode=' + KodeTransaksi
				+ '&Name=' + Nama
				+ '&NPWP=' + NPWP
				+ '&DateStartCreate=' + TanggalBuatAwal
				+ '&DateEndCreate=' + TanggalBuatAkhir
				+ '&DownloadStatus=' + StatusDownload
				+ '&FGStatus=' + FGStatus
				+ '&BelumApprove=' + BelumApprove
				+ '&SiapApprove=' + SiapApprove
				+ '&DalamProses=' + DalamProses
				+ '&ApprovalSukses=' + ApprovalSukses
				+ '&Reject=' + Reject
				+ '&Batal=' + Batal
				+ '&CancelBilling=' + Cancel;
			console.log('get data pajak keluaran : ', url);
			return $http.get(url);
		}

		factory.getDataBusinessType = function () {
			var url = '/api/fe/FinanceVatOut/GetBusinessType';
			console.log('get data pajak business type : ', url);
			return $http.get(url);
		}

		factory.getDownloadFile = function (url) {

			var urlSplit = url.split("/");
			console.log(url);
			console.log(urlSplit[urlSplit.length - 1]);
			var anchor = angular.element('<a/>');
			anchor.attr({
				// href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
				href: url,
				target: '_blank'
				, download: urlSplit[urlSplit.length - 1]
			})[0].click();
			//$http.get(url).success(function(data, status, headers, config) {});
		}

		factory.updateData = function (inputData) {
			var url = '/api/fe/FinanceVatOut/submitUpdateData';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke Update Data') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		}

		factory.getDataPajakDetailOF = function (start, limit, TaxId, OutletId) {
			var url = '/api/fe/FinanceVatOut/GetVatOutOFModel/?start=' + start + '&limit=' + limit + '&TaxId=' + TaxId + '&OutletId=' + OutletId;
			console.log('get data pajak detail OF: ', url);
			return $http.get(url);
		}

		factory.getDataPajakDetailFK = function (TaxId, OutletId) {
			var url = '/api/fe/FinanceVatOut/GetVatOutFKModel/?TaxId=' + TaxId + '&OutletId=' + OutletId;
			console.log('get data pajak detail FK: ', url);
			return $http.get(url);
		}

		factory.saveDataFKOF = function (inputData) {
			var url = '/api/fe/FinanceVatOut/SubmitFKData';
			var param = JSON.stringify(inputData);
			console.log("input data data FK OF : ", inputData);
			var res = $http.post(url, param);

			return res;
		}

		factory.updateCSVStatus = function (inputData) {
			var url = '/api/fe/FinanceVatOut/submitdata';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke updateCSVStatus') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		}

		factory.getCSV = function (customerId, billingList, TaxDocDate, OutletId) {
			//var url = '/api/fe/FinanceManualTaxInvoice/Cetak/?CustomerId='+ customerId + '&billingid=' + BillingId + '&billingcode='+BillingCode + '&taxdocDate=' + TaxDocDate + '&outletid=' + OutletId;
			var url = '/api/fe/FinanceManualTaxInvoice/Cetak/'
			console.log("getCSV : ", url);
			// var billingList = [{
			// 	"billingid":BillingId,
			// 	"billingcode":BillingCode
			// }];
			var inputData = [{
				"CustomerId": customerId,
				"BillingList": billingList
				// "billingid":BillingId,
				// "billingcode":BillingCode,
				// "taxdocDate":TaxDocDate,
				// "outletid":OutletId

			}];
			var res = $http.post(url, inputData, { responseType: 'arraybuffer' });
			return res;
		}



		factory.uploadDJPStatus = function (inputData) {
			var url = '/api/fe/FinancePajakMasukan/UploadStatusDJP/';
			var param = JSON.stringify(inputData);
			return $http.post(url, param);
		}

		return factory;
	});