var app = angular.module('app');
app.controller('PajakKeluaranController', function ($scope, $http, $filter, uiGridConstants, CurrentUser, PajakKeluaranFactory, $timeout, bsNotify, ngDialog) {
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.FinancePajakKeluaranEdit_TaxDataDetailId = 0;
	$scope.maxDate = new Date();
	$scope.FinancePajakKeluaranFP_OF_Data = [];
	$scope.PajakKeluaranFP_TaxId = 0;
	$scope.uiGridPageSize = 10;
	$scope.optionsPajakKeluaran_SelectKolom =
		[{ name: "Nomor Billing", value: "NomorBilling" },
		{ name: "Nomor Faktur Pajak", value: "NomorFakturPajak" },
		{ name: "Tanggal Faktur Pajak", value: "TanggalFakturPajak" },
		{ name: "Nama Pelanggan", value: "NamaPelanggan" },
		{ name: "NPWP", value: "NPWP" }];
	$scope.PajakKeluaran_SelectKolom = "NomorBilling";
	
	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log("Right Byte",$scope.ByteEnable);
	$scope.RightByte = checkRbyte($scope.ByteEnable, 2);

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};


	$scope.optionsFinancePajakKeluaran_StatusDownload = [{ name: "Sudah", value: 2 }, { name: "Belum", value: 1 }];
	$scope.optionsFinancePajakKeluaran_KodeJenisTransaksi = [{ name: "01", value: "01" }, { name: "02", value: "02" }, { name: "03", value: "03" }, { name: "04", value: "04" }, { name: "05", value: "05" }, { name: "06", value: "06" }, { name: "07", value: "07" }, { name: "08", value: "08" }, { name: "09", value: "09" }]
	$scope.optionsFinancePajakKeluaran_TipeBisnis = [{ name: "Unit", value: 1 }, { name: "Service", value: 2 }, { name: "Part", value: 3 }];
	$scope.optionsFinancePajakKeluaran_FakturPajakPengganti = [{ name: "Ya", value: 2 }, { name: "Tidak", value: 1 }]
	var user = CurrentUser.user();
	var dataAll = [];
	$scope.filter = "20160101|20190101";

	if (user.OrgCode.substring(3, 9) == '000000'){
		$scope.PajakKeluaranMain_HO_Show = false;
	}
	else{
		$scope.PajakKeluaranMain_HO_Show = true;
	}
		
	$scope.PajakKeluaran_getDataBranch = function () {
		PajakKeluaranFactory.getDataBranchComboBox(user.OrgId)
			.then
			(
			function (res) {
				$scope.optionsPajakKeluaranMain_SelectBranch = res.data.Result;

				$scope.PajakKeluaranMain_SelectBranch = user.OrgId;

				res.data.Result.some(function (obj, i) {
					if (obj.BranchId == user.OutletId)
						$scope.PajakKeluaranMain_NamaBranch = obj.BranchName;
				});
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	};

	$scope.PajakKeluaran_getDataBranch();

	//----------------------------------
	// Start-Up
	//----------------------------------

	var reminder = 0;
	if (!reminder) {
		//$('#PajakMasukan_ReminderDJP').modal('show');
		ngDialog.openConfirm({
			template: '\
						 <div class="ngdialog-buttons">\
						 <p><h4><b>Reminder!!</b></h4></p>\
						 <hr>\
						 <h5 class="text-center">Apakah Anda sudah mengupdate status DJP Anda?</h5>\
						 <hr>\
						 </div>',
			plain: true,
			//controller: 'ParamKuitansiTTUSController',
			scope: $scope
		});
		reminder = 1;
	}

	$scope.TanggalBChanged = function () {


		var dts = $scope.PajakKeluaran_TanggalAwal;
		var dte = $scope.PajakKeluaran_TanggalAkhir;
		var now = new Date();

		if (dte < dts) {
			$scope.errTanggalB = "Tanggal billing awal tidak boleh lebih dari tanggal billing akhir";
			$scope.TanggalMainStartB = false;
		}
		else if (dts > now) {
			$scope.errTanggalB = "Tanggal billing awal tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalMainStartB = false;
		}
		else {
			$scope.TanggalMainStartB = true;

		}

	}

	$scope.TanggalBChangedEnd = function () {


		var dts = $scope.PajakKeluaran_TanggalAwal;
		var dte = $scope.PajakKeluaran_TanggalAkhir;
		var now = new Date();

		if (dte > now) {
			$scope.errTanggalEndB = "Tanggal billing akhir tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalMainEndB = false;
		}

		else {
			$scope.TanggalMainEndB = true;
		}

		if (dte < dts) {
			$scope.errTanggalB = "Tanggal billing awal tidak boleh lebih dari tanggal billing akhir";
			$scope.TanggalMainStartB = false;
		}
		console.log($scope.TanggalMainEndB);

	}

	$scope.TanggalFChanged = function () {


		var dts = $scope.FinancePajakKeluaran_TanggalFakturAwal;
		var dte = $scope.FinancePajakKeluaran_TanggalFakturAkhir;
		var now = new Date();

		if (dte < dts) {
			$scope.errTanggalF = "Tanggal faktur pajak awal tidak boleh lebih dari tanggal faktur pajak akhir";
			$scope.TanggalMainStartF = false;
		}

		else {
			$scope.TanggalMainStartF = true;

		}

		if (dts > now) {
			$scope.errTanggalF = "Tanggal faktur pajak awal tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalMainStartF = false;
		}

	}

	$scope.TanggalFChangedEnd = function () {


		var dts = $scope.FinancePajakKeluaran_TanggalFakturAwal;
		var dte = $scope.FinancePajakKeluaran_TanggalFakturAkhir;
		var now = new Date();

		if (dte > now) {
			$scope.errTanggalEndF = "Tanggal faktur pajak akhir tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalMainEndF = false;
		}
		else if (dte < dts) {
			$scope.errTanggalF = "Tanggal faktur pajak awal tidak boleh lebih dari tanggal faktur pajak akhir";
			$scope.TanggalMainStartF = false;
		}
		else {
			$scope.TanggalMainEndF = true;
		}
		console.log($scope.TanggalMainEndF);

	}

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		//   $scope.gridData=[];


	});

	// $scope.PajakKeluaran_GetDataBusinessType = function () {
	// 	PajakKeluaranFactory.getDataBusinessType()
	// 		.then(
	// 			function (res) {
	// 				console.log("res", res);
	// 				$scope.optionsFinancePajakKeluaran_TipeBisnis = res.data.Result;
	// 			}
	// 		);
	// }

	$scope.PajakKeluaranMain_Show = true;

	$scope.FinancePajakKeluaran_EditData = function (row) {
		$scope.FinancePajakKeluaranEdit_NomorBilling = row.entity.BillingCode;
		$scope.FinancePajakKeluaranEdit_TanggalBilling = row.entity.BillingDate;
		$scope.FinancePajakKeluaranEdit_NomorFaktur = row.entity.TaxInvoiceNo;
		$scope.FinancePajakKeluaranEdit_TanggalFaktur = row.entity.UsedDate;
		$scope.FinancePajakKeluaranEdit_TipeBisnis = row.entity.BusinessTypeName;
		$scope.FinancePajakKeluaranEdit_JenisTransaksi = row.entity.TranCodeName;
		$scope.FinancePajakKeluaranEdit_Nama = row.entity.CustomerName;
		$scope.FinancePajakKeluaranEdit_Alamat = row.entity.Address;
		$scope.FinancePajakKeluaranEdit_NPWP = row.entity.Npwp;
		$scope.FinancePajakKeluaranEdit_DPP = row.entity.DPP;
		$scope.FinancePajakKeluaranEdit_PPN = row.entity.PPN;
		$scope.FinancePajakKeluaranEdit_StatusDownload = row.entity.DownloadCSVStatus;
		$scope.FinancePajakKeluaranEdit_TaxId = row.entity.TaxId;
		$scope.FinancePajakKeluaranEdit_SelectBranch = row.entity.OutletId;
		$scope.FinancePajakKeluaranEdit_NamaBranch = row.entity.OutletName;
		$scope.FinancePajakKeluaranEdit_TaxDataDetailId = row.entity.TaxDataDetailId;
		$scope.PajakKeluaranEdit_Show = true;
		$scope.PajakKeluaranMain_Show = false;
	}

	$scope.PajakKeluaranEdit_Kembali_Clicked = function () {
		$scope.PajakKeluaranEdit_Show = false;
		$scope.PajakKeluaranMain_Show = true;
	}

	$scope.PajakKeluaranEdit_Simpan_Clicked = function () {
		var dataInput = [{
			"CustomerName": $scope.FinancePajakKeluaranEdit_Nama,
			"TranCodeName": $scope.FinancePajakKeluaranEdit_JenisTransaksi,
			"Address": $scope.FinancePajakKeluaranEdit_Alamat,
			"Npwp": $scope.FinancePajakKeluaranEdit_NPWP,
			"TaxId": $scope.FinancePajakKeluaranEdit_TaxId,
			"OutletId": $scope.FinancePajakKeluaranEdit_SelectBranch,
			"TaxDataDetailId": $scope.FinancePajakKeluaranEdit_TaxDataDetailId
		}];

		PajakKeluaranFactory.updateData(dataInput)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});


					$scope.PajakKeluaranEdit_Kembali_Clicked();
					$scope.PajakKeluaran_Cari_Clicked();
					$scope.PajakKeluaran_ListData_Grid_UIGrid_Paging(1, uiGridPageSize);
				}
			);
	}

	$scope.FinancePajakKeluaran_FPPengganti = function (row) {
		$scope.PajakKeluaranFP_TaxId = row.entity.TaxId;
		$scope.FinancePajakKeluaranFP_NomorBilling = row.entity.BillingCode;
		$scope.FinancePajakKeluaranFP_TanggalBilling = row.entity.BillingDate;
		$scope.FinancePajakKeluaranFP_TipeBisnis = row.entity.BusinessTypeName;
		$scope.FinancePajakKeluaranFP_SelectBranch = row.entity.OutletId;
		$scope.FinancePajakKeluaranFP_NamaBranch = row.entity.OutletName;
		$scope.FinancePajakKeluaranFP_FGPengganti = row.entity.FGPengganti;
		PajakKeluaranFactory.getDataPajakDetailFK($scope.PajakKeluaranFP_TaxId, $scope.FinancePajakKeluaranFP_SelectBranch)
			.then(
				function (res) {
					console.log("res PajakKeluaran FK : ", res);
					$scope.FinancePajakKeluaranFP_KDJenisTransaksi = res.data.Result[0].TaxTrxCode,
						$scope.FinancePajakKeluaranFP_NomorFaktur = res.data.Result[0].TaxInvoiceNo,
						$scope.FinancePajakKeluaranFP_MasaPajak = res.data.Result[0].TaxPeriod,
						$scope.FinancePajakKeluaranFP_TahunPajak = res.data.Result[0].TaxYear,
						$scope.FinancePajakKeluaranFP_TanggalFaktur = res.data.Result[0].TaxInvoiceDate,
						$scope.FinancePajakKeluaranFP_Nama = res.data.Result[0].CustomerName,
						$scope.FinancePajakKeluaranFP_NPWP = res.data.Result[0].NPWP,
						$scope.FinancePajakKeluaranFP_AlamatLengkap = res.data.Result[0].Address,
						$scope.FinancePajakKeluaranFP_JumlahDPP = res.data.Result[0].TotalDPP,
						$scope.FinancePajakKeluaranFP_JumlahPPN = res.data.Result[0].TotalPPN,
						$scope.FinancePajakKeluaranFP_JumlahPPNBM = res.data.Result[0].TotalPPNBM,
						$scope.FinancePajakKeluaranFP_IDKeteranganTambahan = res.data.Result[0].AddedDesc,
						$scope.FinancePajakKeluaranFP_FGUangMuka = res.data.Result[0].DownPayment,
						$scope.FinancePajakKeluaranFP_UangMukaDPP = res.data.Result[0].DPPDownPayment,
						$scope.FinancePajakKeluaranFP_UangMukaPPN = res.data.Result[0].PPNDownPayment,
						$scope.FinancePajakKeluaranFP_UangMukaPPNBM = res.data.Result[0].PPNBMDownPayment,
						$scope.FinancePajakKeluaranFP_Referensi = res.data.Result[0].Reference
				}
			);

		$scope.FinancePajakKeluaranFP_OF_UIGrid_Paging(1);
		$scope.PajakKeluaranFP_Show = true;
		$scope.PajakKeluaranMain_Show = false;
	}

	$scope.FinancePajakKeluaranFP_Kembali_Clicked = function () {
		$scope.PajakKeluaranFP_TaxId = 0;
		$scope.PajakKeluaranFP_Show = false;
		$scope.PajakKeluaranMain_Show = true;
	}

	$scope.FinancePajakKeluaranFP_Simpan_Clicked = function () {
		var inputData = [{
			"TaxId": $scope.PajakKeluaranFP_TaxId,
			"OutletId": $scope.MainPajakKeluaran_SelectBranch,
			"TaxTrxCode": $scope.FinancePajakKeluaranFP_KDJenisTransaksi,
			"TaxInvoiceNo": $scope.FinancePajakKeluaranFP_NomorFaktur,
			"TaxPeriod": $scope.FinancePajakKeluaranFP_MasaPajak,
			"TaxYear": $scope.FinancePajakKeluaranFP_TahunPajak,
			"TaxInvoiceDate": $filter('date')(new Date($scope.FinancePajakKeluaranFP_TanggalFaktur),'yyyy/MM/dd'),
			"CustomerName": $scope.FinancePajakKeluaranFP_Nama,
			"NPWP": $scope.FinancePajakKeluaranFP_NPWP,
			"Address": $scope.FinancePajakKeluaranFP_AlamatLengkap,
			"TotalDPP": $scope.FinancePajakKeluaranFP_JumlahDPP,
			"TotalPPN": $scope.FinancePajakKeluaranFP_JumlahPPN,
			"TotalPPNBM": $scope.FinancePajakKeluaranFP_JumlahPPNBM,
			"AddedDesc": $scope.FinancePajakKeluaranFP_IDKeteranganTambahan,
			"DownPayment": $scope.FinancePajakKeluaranFP_FGUangMuka,
			"DPPDownPayment": $scope.FinancePajakKeluaranFP_UangMukaDPP,
			"PPNDownPayment": $scope.FinancePajakKeluaranFP_UangMukaPPN,
			"PPNBMDownPayment": $scope.FinancePajakKeluaranFP_UangMukaPPNBM,
			"Reference": $scope.FinancePajakKeluaranFP_Referensi,
			"OFList": $scope.FinancePajakKeluaranFP_OF_Data,
			"OutletId": $scope.FinancePajakKeluaranFP_SelectBranch,
			"FGPengganti": $scope.FinancePajakKeluaranFP_FGPengganti
		}];

		PajakKeluaranFactory.saveDataFKOF(inputData)
			.then(
				function (res) {
					//alert("data berhasil disimpan");
					bsNotify.show({
						title: "Success",
						content: "Data berhasil disimpan.",
						type: "success"
					});

					$scope.FinancePajakKeluaranFP_ClearFields();
					$scope.FinancePajakKeluaranFP_Kembali_Clicked();
					$scope.PajakKeluaran_Cari_Clicked();
				}
			);
	}

	$scope.FinancePajakKeluaranFP_ClearFields = function () {
		$scope.PajakKeluaranFP_TaxId = 0;
		$scope.PajakKeluaran_getDataBranch();
		$scope.FinancePajakKeluaranFP_KDJenisTransaksi = "";
		$scope.FinancePajakKeluaranFP_NomorFaktur = "";
		$scope.FinancePajakKeluaranFP_MasaPajak = "";
		$scope.FinancePajakKeluaranFP_TahunPajak = "";
		$scope.FinancePajakKeluaranFP_TanggalFaktur = "";
		$scope.FinancePajakKeluaranFP_Nama = "";
		$scope.FinancePajakKeluaranFP_NPWP = "";
		$scope.FinancePajakKeluaranFP_AlamatLengkap = "";
		$scope.FinancePajakKeluaranFP_JumlahDPP = "";
		$scope.FinancePajakKeluaranFP_JumlahPPN = "";
		$scope.FinancePajakKeluaranFP_JumlahPPNBM = "";
		$scope.FinancePajakKeluaranFP_IDKeteranganTambahan = "";
		$scope.FinancePajakKeluaranFP_FGUangMuka = "";
		$scope.FinancePajakKeluaranFP_UangMukaDPP = "";
		$scope.FinancePajakKeluaranFP_UangMukaPPN = "";
		$scope.FinancePajakKeluaranFP_UangMukaPPNBM = "";
		$scope.FinancePajakKeluaranFP_Referensi = "";
		$scope.FinancePajakKeluaranFP_OF_Data = [];
	}

	$scope.FinancePajakKeluaranFP_OF_Tambah_Clicked = function () {
		var dataInput = {
			"TaxId": $scope.PajakKeluaranFP_TaxId,
			"ObjectCode": $scope.FinancePajakKeluaranFP_OF_KodeObjek,
			"ObjectName": $scope.FinancePajakKeluaranFP_OF_NamaObjek,
			"UnitPrice": $scope.FinancePajakKeluaranFP_OF_HargaSatuan,
			"Amount": $scope.FinancePajakKeluaranFP_OF_JumlahBarang,
			"TotalPrice": $scope.FinancePajakKeluaranFP_OF_HargaTotal,
			"Discount": $scope.FinancePajakKeluaranFP_OF_Diskon,
			"TotalDPP": $scope.FinancePajakKeluaranFP_OF_DPP,
			"TotalPPN": $scope.FinancePajakKeluaranFP_OF_PPN,
			"PPNBMTariff": $scope.FinancePajakKeluaranFP_OF_TarifPPNBM,
			"TotalPPNBM": $scope.FinancePajakKeluaranFP_OF_PPNBM,
			"BillingNo": $scope.FinancePajakKeluaranFP_OF_NoBilling
		};

		$scope.FinancePajakKeluaranFP_OF_ClearFields();
		$scope.FinancePajakKeluaranFP_OF_Data.push(dataInput);
		$scope.FinancePajakKeluaranFP_OF_UIGrid.data = $scope.FinancePajakKeluaranFP_OF_Data;
	}

	$scope.FinancePajakKeluaranFP_OF_ClearFields = function () {
		$scope.FinancePajakKeluaranFP_OF_KodeObjek = "";
		$scope.FinancePajakKeluaranFP_OF_NamaObjek = "";
		$scope.FinancePajakKeluaranFP_OF_HargaSatuan = "";
		$scope.FinancePajakKeluaranFP_OF_JumlahBarang = "";
		$scope.FinancePajakKeluaranFP_OF_HargaTotal = "";
		$scope.FinancePajakKeluaranFP_OF_Diskon = "";
		$scope.FinancePajakKeluaranFP_OF_DPP = "";
		$scope.FinancePajakKeluaranFP_OF_PPN = "";
		$scope.FinancePajakKeluaranFP_OF_TarifPPNBM = "";
		$scope.FinancePajakKeluaranFP_OF_PPNBM = "";
		$scope.FinancePajakKeluaranFP_OF_NoBilling = "";
	}

	//$scope.PajakKeluaran_GetDataBusinessType();
	//----------------------------------
	// Grid Setup
	//----------------------------------

	var paginationOptions_PajakKeluaran = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}


	$scope.PajakKeluaran_ListData_Grid_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: false,
		useExternalPagination: false,
		enableSorting: true,
		enableRowSelection: true,
		enableSelectAll: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'Nomor Billing', field: 'BillingCode', width: 100 },
			{ name: 'Tanggal Billing', field: 'BillingDate', width: 100 },
			{ name: 'NomorFakturPajak', field: 'TaxInvoiceNo', width: 100, visible: false },
			{ name: 'Nomor Faktur Pajak', field: 'TaxInvoiceNoJoin', width: 100 },
			{ name: 'Tanggal Faktur Pajak', field: 'UsedDate', width: 100 },
			{ name: 'Tipe Bisnis', field: 'BusinessTypeName', width: 100 },
			{ name: 'Kode Jenis Transaksi', field: 'TranCodeName', width: 100 },
			{ name: 'Nama ', field: 'CustomerName', width: 100 },
			{ name: 'Alamat', field: 'Address', width: 100 },
			{ name: 'NPWP', field: 'Npwp', width: 100 },
			{ name: 'DPP', field: 'DPP', width: 100, cellFilter: 'rupiahPK' },
			{ name: 'PPN', field: 'PPN', width: 100, cellFilter: 'rupiahPK' },
			{ name: 'Faktur Pajak Pengganti', field: 'isTaxSubstitute', width: 100 },
			{ name: 'Tanggal Pembuatan Faktur Pajak', field: 'ProcessDate', width: 100 },
			{ name: 'Status Download CSV', field: 'DownloadCSVStatus', width: 100 },


			{ name: "Approval Status", field: "ApprovalStatus", width: 100 },
			{ name: "Tanggal Approval", field: "TanggalApproval", enableCellEdit: false, width: 100 },
			{ name: "Tanggal Rekam", field: "TanggalRekam", enableCellEdit: false, width: 100 },
			{ name: "Perekam", field: "Perekam", enableCellEdit: false, width: 100 },

			{
				name: 'Action', width: 200, pinnedRight:true,
				cellTemplate: '<a ng-click="grid.appScope.FinancePajakKeluaran_EditData(row)">&nbsp;Edit Data</a>&nbsp&nbsp;&nbsp;<a ng-click="grid.appScope.FinancePajakKeluaran_FPPengganti(row)">Buat FP Pengganti</a>'
			},
			{ name: 'CSVFilePath', field: 'FilePath', visible: false },
			{ name: 'TaxId', field: 'TaxId', visible: false },
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'BusinessTypeId', field: 'BusinessTypeId', visible: false },
			{ name: 'TranCodeId', field: 'TranCodeId', visible: false },
			{ name: 'OutletId', field: 'OutletId', visible: false },
			{ name: 'OutletName', field: 'OutletName', visible: false },
			{ name: 'DownloadCSVStatusId', field: 'DownloadCSVStatusId', visible: false },
			{ name: 'TaxDataDetailId', field: 'TaxDataDetailId', visible: false },
			{ name: 'FGPengganti', field: 'FGPengganti', visible: false },
		],
		onRegisterApi: function (gridApi) {
			$scope.gridPajakKeluaran = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.PajakKeluaran_ListData_Grid_UIGrid_Paging();
			// 	paginationOptions_PajakKeluaran.pageNumber = pageNumber;
			// 	paginationOptions_PajakKeluaran.pageSize = pageSize;
			// });

			// 	gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.PajakKeluaran_ListData_Grid_UIGrid_Paging(pageNumber, pageSize);
			// 	paginationOptions_PajakKeluaran.pageNumber = pageNumber;
			// 	paginationOptions_PajakKeluaran.pageSize = pageSize;
			// 	console.log("pageNumber ==>", pageNumber);
			// 	console.log("pageSize ==>", pageSize);
			// });

			//gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );

		}
	};

	if($scope.RightByte == true){
		$scope.PajakKeluaran_ListData_Grid_UIGrid.columnDefs[19].visible = true;
	}else{
		$scope.PajakKeluaran_ListData_Grid_UIGrid.columnDefs[19].visible = false;
	}

	// $scope.ColDif = function () {
	// 	//Kolom Action
	// 	if (user.RoleName == "Admin Finance" || user.RoleName == "Admin Service GR" || user.RoleName == "Admin Service BP" || user.RoleName == "Admin Unit" || user.RoleName == "ADH") {
	// 		$scope.PajakKeluaran_ListData_Grid_UIGrid.columnDefs[19].visible = false;
	// 		//$scope.gridPajakKeluaran.grid.refresh();
	// 	}
	// 	else {
	// 		$scope.PajakKeluaran_ListData_Grid_UIGrid.columnDefs[19].visible = true;
	// 		//$scope.gridPajakKeluaran.grid.refresh();
	// 	}
	// }

	// $scope.ColDif();

	$scope.PajakKeluaran_ListData_Grid_UIGrid_Paging = function (Start, Limit) {
		$scope.getDataPajakKeluaran(Start, Limit,
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_NomorBilling,
			$scope.FinancePajakKeluaran_NomorFakturPajak,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_TipeBisnis,
			$scope.FinancePajakKeluaran_KodeJenisTransaksi,
			$scope.FinancePajakKeluaran_Nama,
			$scope.FinancePajakKeluaran_NPWP,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAkhir), 'yyyyMMdd'),

			$scope.FinancePajakKeluaran_StatusDownload,
			$scope.FinancePajakKeluaran_FakturPajakPengganti
		);
	}

	$scope.singleFilter = function (renderableRows) {
		var matcher = new RegExp($scope.PajakKeluaran_TextFilter);
		renderableRows.forEach(function (row) {
			var match = false;

			[$scope.PajakKeluaran_SelectKolom].forEach(function (field) {
				if (row.entity[field].match(matcher)) {
					match = true;
				}
			});
			if (!match) {
				row.visible = false;
			}
		});
		return renderableRows;
	};

	$scope.PajakKeluaran_Filter_Clicked = function () {
		$scope.gridPajakKeluaran.grid.refresh();
	};

	var paginationOptions_FinancePajakKeluaranFP = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	$scope.FinancePajakKeluaranFP_OF_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "TaxId", field: "TaxId", visible: false },
			{ name: "Kode Objek", field: "ObjectCode" },
			{ name: "Nama", field: "ObjectName" },
			{ name: "Harga Satuan", field: "UnitPrice", cellFilter: "rupiahPK" },
			{ name: "Jumlah Barang", field: "Amount", cellFilter: "rupiahPK" },
			{ name: "Harga Total", field: "TotalPrice", cellFilter: "rupiahPK" },
			{ name: "Diskon", field: "Discount", cellFilter: "rupiahPK" },
			{ name: "DPP", field: "TotalDPP", cellFilter: "rupiahPK" },
			{ name: "PPN", field: "TotalPPN", cellFilter: "rupiahPK" },
			{ name: "Tarif PPNBM", field: "PPNBMTariff", cellFilter: "rupiahPK" },
			{ name: "PPNBM", field: "TotalPPNBM", cellFilter: "rupiahPK" },
			{ name: "No Billing", field: "BillingNo" },
			{
				name: 'Action',
				cellTemplate: '<a ng-click="grid.appScope.PajakKeluaranFP_OF_Delete_Clicked(row)">Hapus</a>'
			}
		],
		data: $scope.FinancePajakKeluaranFP_OF_Data,
		onRegisterApi: function (gridApi) {
			$scope.GridApiFinancePajakKeluaranFPOFList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.FinancePajakKeluaranFP_OF_UIGrid_Paging(pageNumber);
				paginationOptions_FinancePajakKeluaranFP.pageNumber = pageNumber;
				paginationOptions_FinancePajakKeluaranFP.pageSize = pageSize;
			});
		}
	};
	$scope.PajakKeluaranFP_OF_Delete_Clicked = function (row) {
		var index = $scope.FinancePajakKeluaranFP_OF_UIGrid.data.indexOf(row.entity);
		$scope.FinancePajakKeluaranFP_OF_UIGrid.data.splice(index, 1);
		// var index = 0;
		// angular.forEach($scope.gridDetail_Data, function (value, key) {
		// 	if (value.TaxId == row.entity.TaxId) {
		// 		$scope.FinancePajakKeluaranFP_OF_Data.splice(index, 1);
		// 		$scope.FinancePajakKeluaranFP_OF_UIGrid.data = $scope.FinancePajakKeluaranFP_OF_Data;
		// 	}
		// 	index++;
		// });
	}
	$scope.FinancePajakKeluaranFP_OF_UIGrid_Paging = function (pageNumber) {
		PajakKeluaranFactory.getDataPajakDetailOF(paginationOptions_FinancePajakKeluaranFP.pageNumber, paginationOptions_FinancePajakKeluaranFP.pageSize, $scope.PajakKeluaranFP_TaxId, $scope.FinancePajakKeluaranFP_SelectBranch)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res =>", res.data.Result);
						$scope.FinancePajakKeluaranFP_OF_Data = res.data.Result;
						$scope.FinancePajakKeluaranFP_OF_UIGrid.data = $scope.FinancePajakKeluaranFP_OF_Data;
						$scope.FinancePajakKeluaranFP_OF_UIGrid.totalItems = res.data.Total;
						$scope.FinancePajakKeluaranFP_OF_UIGrid.paginationPageSize = paginationOptions_FinancePajakKeluaranFP.pageSize;
						$scope.FinancePajakKeluaranFP_OF_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.FinancePajakKeluaranFP_OF_UIGrid.paginationCurrentPage = paginationOptions_FinancePajakKeluaranFP.pageNumber;
					}
				}

			);
	}

	//----------------------------------
	// Process
	//----------------------------------
	//get data from factory
	$scope.getDataPajakKeluaran = function (Start, Limit, dateStart, dateEnd, nomorBilling, nomorFakturPajak, tanggalFakturAwal, tanggalFakturAkhir, tipeBisnis,
		KodeTransaksi, Nama, NPWP, TanggalBuatAwal, TanggalBuatAkhir, StatusDownload, OutletId, FGStatus, BelumApprove, SiapApprove,DalamProses, ApprovalSukses, Reject, Batal, Cancel) {
		console.log("ouletid :", $scope.PajakKeluaranMain_SelectBranch);
		PajakKeluaranFactory.getDataPajakKeluaran(Start, Limit, dateStart, dateEnd, nomorBilling, nomorFakturPajak, tanggalFakturAwal, tanggalFakturAkhir, tipeBisnis,
			KodeTransaksi, Nama, NPWP, TanggalBuatAwal, TanggalBuatAkhir, StatusDownload, OutletId, FGStatus, BelumApprove, SiapApprove,
			DalamProses, ApprovalSukses, Reject, Batal, Cancel)
			.then(
				function (res) {
					console.log(res);
					$scope.PajakKeluaran_ListData_Grid_UIGrid.data = res.data.Result;
					$scope.PajakKeluaran_ListData_Grid_UIGrid.totalItems = res.data.Total;
					$scope.PajakKeluaran_ListData_Grid_UIGrid.paginationPageSize = paginationOptions_PajakKeluaran.pageSize;
					$scope.PajakKeluaran_ListData_Grid_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.PajakKeluaran_ListData_Grid_UIGrid.paginationCurrentPage = paginationOptions_PajakKeluaran.pageNumber;
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}

	$scope.PajakKeluaran_Download_Clicked = function () {
		var billingList = [];

		angular.forEach($scope.gridPajakKeluaran.selection.getSelectedRows(), function (value, key) {

			var TaxIdArray = [];
			TaxIdJson = {
				TaxId: value.TaxId
			};
			TaxIdArray.push(TaxIdJson);
			console.log(TaxIdArray);
			billingList.push({
				"billingid": value.TaxDataDetailId,
				"billingcode": value.BillingCode,
				"isTaxSubstitute": value.isTaxSubstitute 
			})

			// PajakKeluaranFactory.updateCSVStatus(TaxIdArray)
			// .then(
			// function (res) {
			// console.log("res=>",res);
			// PajakKeluaranFactory.getDownloadFile(value.FilePath);
			// $scope.getDataPajakKeluaran(1,10,$filter('date')(new Date($scope.PajakKeluaran_TanggalAwal), 'yyyyMMdd'), $filter('date')(new Date($scope.PajakKeluaran_TanggalAkhir), 'yyyyMMdd'));
			// $scope.gridPajakKeluaran.grid.refresh();
			// },
			// function (err) {
			// console.log("err=>", err);
			// }
			// );

		});
		$scope.BuatFakturPajak_BuatCSV(0, billingList, '', 0);
	}

	$scope.BuatFakturPajak_BuatCSV = function (CustomerId, billingList, TaxDocDate, OutletId) {

		PajakKeluaranFactory.getCSV(CustomerId, billingList, TaxDocDate, OutletId)
			.then(
				function (res) {

					// var contentType = 'text/csv';
					// var b64Data = res.data;

					// var blob = b64toBlob(b64Data, contentType);
					// var blobUrl = URL.createObjectURL(blob);

					// saveAs(blob, 'ContohCSV' + '.csv');

					// return res;
					var filename = 'testcsv.csv';
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'Pajak Keluaran' + '.csv');
					$scope.PajakKeluaran_Cari_Clicked();
					//$scope.PajakKeluaran_Cari_Clicked();
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	function getYYYYMMDD(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');

	}

	$scope.PajakKeluaran_Cari_Clicked = function () {
		if ((typeof $scope.PajakKeluaran_TanggalAwal != 'undefined') && (typeof $scope.PajakKeluaran_TanggalAkhir != 'undefined') && ( getYYYYMMDD($scope.PajakKeluaran_TanggalAwal) != '19700101') && (getYYYYMMDD($scope.PajakKeluaran_TanggalAkhir) != '19700101')) {
			$scope.filter = getYYYYMMDD($scope.PajakKeluaran_TanggalAwal) + "|" + getYYYYMMDD($scope.PajakKeluaran_TanggalAkhir);
			console.log("Data tanggal : ", $scope.filter);
			$scope.getDataPajakKeluaran(1, 10,
				$filter('date')(new Date($scope.PajakKeluaran_TanggalAwal), 'yyyyMMdd'),
				$filter('date')(new Date($scope.PajakKeluaran_TanggalAkhir), 'yyyyMMdd'),
				$scope.FinancePajakKeluaran_NomorBilling,
				$scope.FinancePajakKeluaran_NomorFakturPajak,
				$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAwal), 'yyyyMMdd'),
				$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAkhir), 'yyyyMMdd'),
				$scope.FinancePajakKeluaran_TipeBisnis,
				$scope.FinancePajakKeluaran_KodeJenisTransaksi,
				$scope.FinancePajakKeluaran_Nama,
				$scope.FinancePajakKeluaran_NPWP,
				$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAwal), 'yyyyMMdd'),
				$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAkhir), 'yyyyMMdd'),
				$scope.FinancePajakKeluaran_StatusDownload,
				$scope.PajakKeluaranMain_SelectBranch,
				$scope.FinancePajakKeluaran_FakturPajakPengganti,
				$scope.FinancePajakKeluaran_belumapprove ? 1 : 0,
				$scope.FinancePajakKeluaran_siapapprove ? 1 : 0,
				$scope.FinancePajakKeluaran_dalamproses ? 1 : 0,
				$scope.FinancePajakKeluaran_approvesukses ? 1 : 0,
				$scope.FinancePajakKeluaran_reject ? 1 : 0,
				$scope.FinancePajakKeluaran_batal ? 1 : 0,
				$scope.FinancePajakKeluaran_cancel ? 1 : 0
			);
			console.log($scope.FinancePajakKeluaran_reject);
		}
		else {
			//alert("data tanggal tidak lengkap");
			bsNotify.show({
				title: "Warning",
				content: "Data Tanggal Billing Tidak Lengkap.",
				type: "warning"
			});
		};

	}

	$scope.getAllData = function () {
		PajakKeluaranFactory.getDataPajakKeluaran(1, $scope.PajakKeluaran_ListData_Grid_UIGrid.data[0].TotalData,
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_NomorBilling,
			$scope.FinancePajakKeluaran_NomorFakturPajak,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_TipeBisnis,
			$scope.FinancePajakKeluaran_KodeJenisTransaksi,
			$scope.FinancePajakKeluaran_Nama,
			$scope.FinancePajakKeluaran_NPWP,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_StatusDownload, $scope.PajakKeluaranMain_SelectBranch, $scope.FinancePajakKeluaran_FakturPajakPengganti)
			.then(
				function (res) {
					console.log(res);
					dataAll = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.PajakKeluaran_DownloadDaftar_Clicked = function () {
		var excelData = [];
		var fileName = "";
		var eachData = {};
		PajakKeluaranFactory.getDataPajakKeluaran(1, $scope.PajakKeluaran_ListData_Grid_UIGrid.data[0].TotalData,
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.PajakKeluaran_TanggalAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_NomorBilling,
			$scope.FinancePajakKeluaran_NomorFakturPajak,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalFakturAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_TipeBisnis,
			$scope.FinancePajakKeluaran_KodeJenisTransaksi,
			$scope.FinancePajakKeluaran_Nama,
			$scope.FinancePajakKeluaran_NPWP,
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAwal), 'yyyyMMdd'),
			$filter('date')(new Date($scope.FinancePajakKeluaran_TanggalBuatFakturAkhir), 'yyyyMMdd'),
			$scope.FinancePajakKeluaran_StatusDownload, $scope.PajakKeluaranMain_SelectBranch, $scope.FinancePajakKeluaran_FakturPajakPengganti)
			.then(
				function (res) {
					console.log(res);
					dataAll = res.data.Result;
					var TaxIdArray = [];
					if (dataAll.length > 0) {
						angular.forEach($scope.gridPajakKeluaran.selection.getSelectedRows(), function (value, key) {
							TaxIdArray.push(value.TaxDataDetailId);
						});
						//$scope.getAllData();
						angular.forEach(dataAll, function (value, key) {
							console.log("value : ", value);

							// eachData = {"Nomor Billing":value.BillingCode, "Tanggal Billing":value.BillingDate, "Nomor Faktur Pajak":value.TaxInvoiceNo, 
							// 		"Tanggal Faktur Pajak":value.UsedDate, "Tipe Bisnis":value.BusinesTypeName, "Kode Jenis Transaksi":value.TranCodeName, "Nama":value.CustomerName, 
							// 		"Alamat":value.Address, "NPWP":value.Npwp, "DPP":value.DPP, "PPN":value.PPN, "Faktur Pajak Pengganti":value.isTaxSubstitute, 
							// 		"Tanggal Pembuatan Faktur Pajak":value.ProcessDate, "Status Download CSV":value.DownloadCSVStatus};
							if (TaxIdArray.includes(value.TaxDataDetailId)) {
								eachData = {
									"Nomor Billing": value.BillingCode || '', "Tanggal Billing": value.BillingDate || '', "Nomor Faktur Pajak": value.TaxInvoiceNoJoin || '',
									"Tanggal Faktur Pajak": value.UsedDate || '', "Tipe Bisnis": value.BusinessTypeName || '', "Kode Jenis Transaksi": value.TranCodeName || '', "Nama": value.CustomerName || '',
									"Alamat": value.Address || '', "NPWP": value.Npwp || '', "DPP": value.DPP, "PPN": value.PPN, "Faktur Pajak Pengganti": value.isTaxSubstitute || '',
									"Tanggal Pembuatan Faktur Pajak": value.ProcessDate || '', "Status Download CSV": value.DownloadCSVStatus || '',
									"Approval Status": value.ApprovalStatus || '',"Tanggal Approval": value.TanggalApproval || '',
									"Tanggal Rekam": value.TanggalRekam || '',"Perekam": value.Perekam || ''
								};
								excelData.push(eachData);
							}


						});
						console.log("excel Data :", excelData);
						fileName = "DaftarPajakKeluaran";

						XLSXInterface.writeToXLSX(excelData, fileName);
					}
					else {
						eachData = {
							"Nomor Billing": "-", "Tanggal Billing": "-", "Nomor Faktur Pajak": "-",
							"Tanggal Faktur Pajak": "-", "Tipe Bisnis": "-", "Kode Jenis Transaksi": "-", "Nama": "-",
							"Alamat": "-", "NPWP": "-", "DPP": "-", "PPN": "-", "Faktur Pajak Pengganti": "-",
							"Tanggal Pembuatan Faktur Pajak": "-", "Status Download CSV": "-"
						};
						excelData.push(eachData);
						console.log("excel Data :", excelData);
						fileName = "DaftarPajakKeluaran";

						XLSXInterface.writeToXLSX(excelData, fileName);
					}
				},
				function (err) {
					console.log("err=>", err);
					bsNotify.show({
						title: "Error",
						content: err,
						type: "danger"
					});

				}
			);




	}

	// ============================ BEGIN | NOTO | 190415 | Pajak Keluaran | Upload Status DJP ============================
	$scope.PajakKeluaran_Upload_Clicked = function () {
		$scope.PajakKeluaranMain_Show = false;
		$scope.PajakKeluaranUpload_Show = true;
		$scope.PajakKeluaran_UploadData_UIGrid.data = [];
		$scope.PajakKeluaran_ExcelData = [];
	}

	$scope.PajakKeluaran_Kembali_Clicked = function () {
		$scope.PajakKeluaranMain_Show = true;
		$scope.PajakKeluaranUpload_Show = false;
	}
	// ============================ END | NOTO | 190415 | Pajak Keluaran | Upload Status DJP ============================

	// ============================ BEGIN | NOTO | 190415 | Pajak Keluaran | Download Template ============================
	$scope.PajakKeluaran_DownloadTemplate_Clicked = function () {
		var excelData = [{ "NOMOR_FAKTUR_PAJAK": "", "APPROVAL_STATUS": "", "TANGGAL_APPROVAL": "", "TANGGAL_REKAM": "", "PEREKAM": "" }];
		var fileName = "PajakKeluaran_StatusDJP_Template";
		XLSXInterface.writeToXLSX(excelData, fileName);
	}
	// ============================ END | NOTO | 190415 | Pajak Keluaran | Download Template ============================

	// ============================ BEGIN | NOTO | 190415 | Pajak Keluaran | Validasi & Submit ============================
	$scope.PajakKeluaran_UploadData_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "Nomor Faktur Pajak", field: "NOMOR_FAKTUR_PAJAK", enableFiltering: true },
			{ name: "Approval Status", field: "APPROVAL_STATUS", enableFiltering: true },
			{ name: "Tanggal Approval", field: "TANGGAL_APPROVAL", enableCellEdit: false },
			{ name: "Tanggal Rekam", field: "TANGGAL_REKAM", enableCellEdit: false },
			{ name: "Perekam", field: "PEREKAM", enableCellEdit: false },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPajakKeluaranDJP = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.PajakKeluaran_UploadData_UIGrid.paginationCurrentPage = pageNumber;
				$scope.PajakKeluaran_UploadData_UIGrid.paginationPageSize = pageSize;
				$scope.PajakKeluaran_UploadData_UIGrid.paginationPageSizes = [10, 25, 50];
			});
		}
	};

	$scope.PajakKeluaran_Upload_File_Clicked = function () {
		var myEl = angular.element(document.querySelector('#uploadPajakKeluaranDJP')); //ambil elemen dari dokumen yang di-upload 

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			$scope.PajakKeluaran_ExcelData = json;
			$scope.PajakKeluaran_FileName_Current = myEl[0].files[0].name;
			$scope.PajakKeluaran_UploadData_UIGrid.data = $scope.PajakKeluaran_ExcelData;
			$scope.PajakKeluaran_UploadData_UIGrid.totalItems = $scope.PajakKeluaran_ExcelData.length;

			if (!$scope.validateColumn($scope.PajakKeluaran_ExcelData[0])) {
				msg = ("Kolom file excel tidak sesuai");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		});
	}

	$scope.validateColumn = function (inputExcelData) {
		if (angular.isUndefined(inputExcelData.NOMOR_FAKTUR_PAJAK)
			|| angular.isUndefined(inputExcelData.APPROVAL_STATUS)
			|| angular.isUndefined(inputExcelData.TANGGAL_APPROVAL)
			|| angular.isUndefined(inputExcelData.TANGGAL_REKAM)
			|| angular.isUndefined(inputExcelData.PEREKAM)) {
			return false;
		}
		return true;
	}

	$scope.PajakKeluaran_Upload_Simpan_Clicked = function () {
		var data = [{ "TipePajak": "PajakKeluaran", "DJPList": $scope.PajakKeluaran_ExcelData }]
		PajakKeluaranFactory.uploadDJPStatus(data).then
			(
			function (res) {
				bsNotify.show({
					title: "Berhasil",
					content: "Berhasil simpan",
					type: 'success'
				});
				$scope.PajakKeluaran_Cari_Clicked();
				$scope.PajakKeluaran_Kembali_Clicked();
			},
			function (err) {
				if (err != null) {
					bsNotify.show({
						title: "Gagal simpan",
						content: err.data.Message,
						type: 'warning'
					});
				}
			}
			);
	}
	// ============================ END | NOTO | 190415 | Pajak Keluaran | Validasi & Submit ============================

});

app.filter('rupiahPK', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});