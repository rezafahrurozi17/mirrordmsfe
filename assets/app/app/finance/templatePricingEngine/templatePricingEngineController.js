///PAGE CONTROLLER
 app.controller('TemplatePricingEngineController', function($scope, $timeout, $http) {
    $scope.jsData = {

        "HeaderInputs": {
            "$AssemblyType$": "cbu",
            "$CC$": "2000",
            "$OutletId$": "1",
            "$VehicleTypeColorId$": "1",
            "$VehicleYear$": "2017",
            "{AccTotal}": "2"
        },
    };
    $scope.PricingId = "1";
    $scope.OutletId = "1";
    $scope.DisplayId = "1";
    $scope.listAccesories =[{Label:"TipeA", Jumlah:"10",HargaSatuan:"1000", VAT:"0", TOTAL:"0" },
        {Label:"TipeB", Jumlah:"122",HargaSatuan:"2000", VAT:"0", TOTAL:"0"},
        {Label:"TipeA", Jumlah:"3",HargaSatuan:"3000", VAT:"0", TOTAL:"0"}];
    $scope.urlPricing = '/api/fe/PricingEngine?PricingId='+$scope.PricingId+'&OutletId='+$scope.OutletId+'&DisplayId='+$scope.DisplayId+''
    $scope.GetDataFromFactory = function(){
        console.log("Data dari directive diakses lewat controller");
        console.log($scope.jsData);
        
        $scope.jsData.HeaderInputs['{AccTotal}'] = 0;
        angular.forEach( $scope.listAccesories, function(acc, accIdx){
        
            var param = {
                "Title": "Harja",
                "HeaderInputs": {
                    "{InputHargaAcc}": acc.HargaSatuan
                }
            };

            $http.post('/api/fe/PricingEngine?PricingId=71&OutletId=1&DisplayId=1',param)
            .then(  function(res){
                console.log("res=>",res); 
                   $scope.listAccesories[accIdx].VAT = res.data.Codes["[VATAcc]"];
                   $scope.listAccesories[accIdx].TOTAL = res.data.Codes["[SubTotalAcc]"];
                   
                   $scope.jsData.HeaderInputs['{AccTotal}'] = parseInt($scope.jsData.HeaderInputs['{AccTotal}'] )  + parseInt(res.data.Codes["[SubTotalAcc]"]); 
                });
            
        }); 
            
        console.log($scope.listAccesories);

    };
}); 

 