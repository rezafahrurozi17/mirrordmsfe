angular.module('app')
  .factory('TemplatePricingEngineFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(JSDATA) {
        //var res=$http.get('/api/fe/fe/prizingengine/?tipe=1&outletid=2&variable=[{"VariableName":"OutletId", "VariableValue":"1"}, {"VariableName":"VehicleYear", "VariableValue":"2017"}, {"VariableName":"VehicleTypeColorId", "VariableValue":"1"}, {"VariableName":"AssemblyType", "VariableValue":"CBU"}, ]');
        var res=$http.post('/api/fe/fe/PricingEngine?PricingId=1&OutletId=1&DisplayId=1', 
        JSDATA
          );
          
        return res;
      },
      create: function(TemplatePricingEngineFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: TemplatePricingEngineFactory.SalesProgramName}]);
      },
      update: function(TemplatePricingEngineFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: TemplatePricingEngineFactory.SalesProgramId,
                                            SalesProgramName: TemplatePricingEngineFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd