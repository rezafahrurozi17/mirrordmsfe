/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('clearingFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;
		
		factory.getSalesOrderBySPKId = function(start, limit, SPKId, IncomingType)
		{
			var find = "/";
			var regex = new RegExp(find, "g");
			SPKId = SPKId.replace(regex, "~"); 					
			var url = '/api/fe/AlokasiBFDPUnit/GetAlokasiSOBySPKId/start/' + start + '/limit/' + limit + '/spkid/' + SPKId + '|' + IncomingType;
			console.log ("execute GetAlokasiSOBySPKId");
			return $http.get(url);
		}
		
		factory.getSPKByCustomerName = function(start, limit, CustomerName, IncomingType)
		{
			var url = '/api/fe/SPK/GetByCustomerName?start=' + start + '&limit=' + limit + '&customername=' + CustomerName + '|' + IncomingType;
			//var url = '/api/fe/SPK/getbycustomername/start/' + start + '/limit/' + limit + '/customername/' + CustomerName + '|' + IncomingType;
			console.log ("execute getSPKByCustomerName");
			return $http.get(url);
		}

		factory.submitClearing = function (inputData) 
		{
			var url = '/api/fe/Clearing/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitClearing') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.getDataGL = function (NoRef, TaxType) 
		{
			var url = '/api/fe/Clearing/getDataGL?FilterData=' + NoRef + '|' + TaxType;
			console.log(url);
			var res = $http.get(url);
			return res;
		}

		factory.getGLName = function (NoGL) 
		{
			var url = '/api/fe/Clearing/getGLName?FilterData=' + NoGL;
			console.log(url);
			var res = $http.get(url);
			return res;
		}
		factory.GetListClearing = function(start, limit, TranDate, TranDateTo, Branch)
		{
			var url = '/api/fe/Clearing/GetListClearing/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo + '|' + Branch;
			console.log ("execute GetListClearing");
			return $http.get(url);
		},
		factory.GetListClearingFilter = function(start, limit, TranDate, TranDateTo, Branch)
		{
			var url = '/api/fe/Clearing/GetListClearing/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo + '|' + Branch;
			console.log ("execute GetListClearing");
			return $http.get(url);
		}
	
		factory.updateApprovalData = function(inputData)
		{
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		}

		factory.getNoClearing =  function(){
			var url = '/api/fe/Clearing/GetNoClearing/';
			console.log(url);
			var res=$http.get(url);
			return res;
		},			
		factory.getListGLClearing = function () {
			console.log("factory.getListGLClearing");
			var url = '/api/fe/Clearing/getListGLClearing/';
			var res = $http.get(url);
			return res;
		}

		factory.getLihatClearingListAR = function(start, limit, FilterData)
		{
			var url = '/api/fe/Clearing/GetListDaftarARById/start/' + start + '/limit/' + limit + '/FilterData/' + FilterData;
			console.log ("execute getLihatClearingListAR");
			return $http.get(url);
		}

		factory.getLihatClearingListAP = function(start, limit, FilterData)
		{
			var url = '/api/fe/Clearing/GetListDaftarAPById/start/' + start + '/limit/' + limit + '/FilterData/' + FilterData;
			console.log ("execute getLihatClearingListAP");
			return $http.get(url);
		}

		factory.getLihatClearingListGL = function(start, limit, FilterData)
		{
			var url = '/api/fe/Clearing/GetListDaftarGLById/start/' + start + '/limit/' + limit + '/FilterData/' + FilterData;
			console.log ("execute getLihatClearingListGL");
			return $http.get(url);
		}

		factory.getLihatClearingListBiaya = function(start, limit, FilterData)
		{
			var url = '/api/fe/Clearing/GetListDaftarBiayaById/start/' + start + '/limit/' + limit + '/FilterData/' + FilterData;
			console.log ("execute getLihatClearingListBiaya");
			return $http.get(url);
		}
		
		factory.GetListDaftarAR = function(start, limit, TranDate, TranDateTo, Status)
		{
			var url = '/api/fe/Clearing/GetListDaftarAR/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo + '|' + Status;
			console.log ("execute GetListDaftarAR");
			return $http.get(url);
		}
		// Filter AR
		factory.GetListDaftarARFilter = function(start, limit, TranDate, TranDateTo, Status)
		{
			var url = '/api/fe/Clearing/GetListDaftarAR/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo + '|' + Status;
			console.log ("execute GetListDaftarAR");
			return $http.get(url);
		}		
		// Filter AP
		factory.GetListDaftarAPFilter = function(start, limit, TranDate, TranDateTo)
		{
			var url = '/api/fe/Clearing/GetListDaftarAP/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo;
			console.log ("execute GetListDaftarAP");
			return $http.get(url);
		}

		factory.GetListDaftarAP = function(start, limit, TranDate, TranDateTo)
		{
			var url = '/api/fe/Clearing/GetListDaftarAP/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate + '|' + TranDateTo;
			console.log ("execute GetListDaftarAP");
			return $http.get(url);
		}
		 factory.getDataNomorGL = function(GLTypeId) 
		 {
			var url = '/api/fe/financegl/' + GLTypeId;
			console.log ("execute getDataNomorGL");
			return $http.get(url);
		 }		
		 
		factory.getDataBranchComboBox = function (outletId) {
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + outletId;
			var res=$http.get(url);  
			return res;	
		}				 
		
		factory.submitDataApproval = function (inputData) {
			var url = '/api/fe/FinanceApproval/SubmitData/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}		
		
		factory.updateApprovalData = function(inputData){
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		}		

		factory.getDataBranch=function() {
			//var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData=0';
		var res=$http.get(url);  
		return res;			
		};			
		
		factory.getDataForRefund=function(start, limit, FilterData) {
			console.log('isi data ',JSON.stringify(FilterData) );
			var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData));
			//var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData/Start/' + start + '/Limit/' + limit + '/filterData=' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.getDataForRefundNew=function(FilterData) {
			console.log('isi data ',JSON.stringify(FilterData) );
			var res=$http.get('/api/fe/Clearing/GetListClearingRdp/?taxtype=' + FilterData);
			return res;
		};
		
		return factory;
	});