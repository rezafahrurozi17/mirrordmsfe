
var app = angular.module('app');
//20170425, eric, begin
//Nama controller disini harus case sensitive sama dengan route.js
app.controller('clearingController', function ($scope, $http, CurrentUser, $filter, clearingFactory, $timeout, uiGridConstants, bsNotify) {
	$scope.optionsLihatClearing_DaftarClearing_Search = [{ name: "No Clearing", value: "No Clearing" }];
	$scope.optionsLihatClearing_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	$scope.optionsLihatClearing_DK_Search = [{ name: "Kredit", value: "Kredit" },{ name: "Debet", value: "Debet" }];
	//$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
	$scope.optionsLihatClearing_TipeBiaya_Search = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Other Income", value: "Other Income" }, { name: "Other Expense", value: "Other Expense" }, { name: "PPN Masukan", value: "PPN Masukan" }, { name: "PPV", value: "PPV" }];
	// $scope.optionsLihatClearing_NoGL_Search = [{ name: "PPh 22 Terbayar WAPU", value: "PPh 22 Terbayar WAPU" }, { name: "PPh 23 Terbayar", value: "PPh 23 Terbayar" }, { name: "PPN Keluaran WAPU", value: "PPN Keluaran WAPU" }];
	$scope.optionsTambahClearing_DaftarAR_Search = [{ name: "Tanggal Billing", value: "Tanggal Billing" }, { name: "Nomor Billing", value: "Nomor Billing" }, { name: "No Pelanggan", value: "No Pelanggan" }, { name: "Nama Pelanggan", value: "Nama Pelanggan" }];
	$scope.optionsTambahClearing_DaftarAP_Search = [{ name: "Tgl Instruksi Pembayaran", value: "Tgl Instruksi Pembayaran" }, { name: "No Instruksi Pembayaran", value: "No Instruksi Pembayaran" }, { name: "No Branch/Vendor", value: "No Branch Vendor" }, { name: "Nama Branch/Vendor", value: "Nama Branch Vendor" }];
	$scope.optionsTambahStatusBatal_Search = [{ name: "Ya", value: "Ya" }, { name: "Tidak", value: "Tidak" }];
	// $scope.optionsTambahClearing_TipeRef_Search = [{ name: "PPh 22 Terbayar WAPU", value: "PPh 22 Terbayar WAPU" }, { name: "PPh 23 Terbayar", value: "PPh 23 Terbayar" }, { name: "PPN Keluaran WAPU", value: "PPN Keluaran WAPU" }, { name: "Down Payment Unit", value: "Down Payment Unit" }, { name: "Down Payment Service", value: "Down Payment Service" }, { name: "Down Payment Parts", value: "Down Payment Parts" }, { name: "Internal", value: "Internal"}];
	$scope.optionsTambahClearing_TipeRef_Search = [{ name: "PPh 22 Terbayar WAPU", value: "PPh 22 Terbayar WAPU" }, { name: "PPh 23 Terbayar", value: "PPh 23 Terbayar" }, { name: "PPN Keluaran WAPU", value: "PPN Keluaran WAPU" }, { name: "Down Payment Unit", value: "Down Payment Unit" }, { name: "Down Payment Service", value: "Down Payment Service" }, { name: "Down Payment Parts", value: "Down Payment Parts" }];

	console.log("Clearing");
	$scope.user = CurrentUser.user();
	$scope.Show_ShowLihatClearing = true;
	$scope.ShowClearingDetail = false;
	$scope.ShowClearingData = true;
	$scope.ShowNoRegPajak = false;
	$scope.ShowNoSO = false;
	$scope.Show_LihatClearingHeader = true;
	$scope.uiGridPageSize = 10;
	$scope.currentOutletId = 0;
	$scope.LihatClearing_TujuanPembayaran_TanggalClearing = new Date();
	$scope.LihatClearing_TujuanPembayaran_TanggalClearingTo = new Date();

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOptions = { format: dateFormat }

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log($scope.ByteEnable);
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	console.log($scope.allowApprove);
	if ($scope.allowApprove == true)
		$scope.optionsLihatClearing_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	else
		$scope.optionsLihatClearing_DaftarBulkAction_Search = [];


	//----------------------------------
	// Start-Up
	//----------------------------------

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		$scope.getDataBranchCB();
		//$scope.gridData=[];

	});

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.givePattern = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahClearing_BiayaNominal = $scope.TambahClearing_BiayaNominal.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			return;
		}
	};

	$scope.givePattern2 = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahClearing_NominalSO = $scope.TambahClearing_NominalSO.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			return;
		}
	};

	$scope.givePattern3 = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahClearing_NominalInternal = $scope.TambahClearing_NominalInternal.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			return;
		}
	};

	function addSeparatorsNF(nStr, inD, outD, sep) {
		nStr += '';
		var dpos = nStr.indexOf(inD);
		var nStrEnd = '';
		if (dpos != -1) {
			nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
			nStr = nStr.substring(0, dpos);
		}
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(nStr)) {
			nStr = nStr.replace(rgx, '$1' + sep + '$2');
		}
		return nStr + nStrEnd;
	}

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.getDataBranchCB = function () {
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.LihatClearing_NamaBranch_Search_EnabledDisabled = true;
			$scope.TambahClearing_NamaBranch_Search_EnabledDisabled = true;
		}
		else {
			$scope.LihatClearing_NamaBranch_Search_EnabledDisabled = false;
			$scope.TambahClearing_NamaBranch_Search_EnabledDisabled = false;
		}

		clearingFactory.getDataBranchComboBox($scope.user.OrgId)
			.then(
				function (res) {
					$scope.optionsLihatClearing_NamaBranch_Search = res.data.Result;
					$scope.LihatClearing_NamaBranch_Search = $scope.user.OutletId;

					$scope.optionsTambahClearing_NamaBranch_Search = res.data.Result;
					$scope.TambahClearing_NamaBranch_Search = $scope.user.OutletId;
				}
			);
	};
	$scope.getListGLClearing = function () {
		console.log('getListGLClearing');
		clearingFactory.getListGLClearing()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsLihatClearing_NoGL_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TambahClearing_NoGL_Selected_Changed = function () {

		console.log($scope.TambahClearing_NoGL_Search);
		if (angular.isUndefined($scope.TambahClearing_NoGL_Search)) {
			console.log("Not defined");
		}
		else {
			clearingFactory.getGLName($scope.TambahClearing_NoGL_Search)
				.then(
					function (res) {
						$scope.loading = false;
						$scope.TambahClearing_NamaGL = res.data.Result[0].NamaGL;
					},
					function (err) {
						console.log("err=>", err);
					});
		}
	}


	$scope.TambahClearing_TipeRefL_Selected_Changed = function () {
		var filter;
		$scope.optionsLihatClearing_DK_Search = [{ name: "Kredit", value: "Kredit" },{ name: "Debet", value: "Debet" }];
		$scope.TambahClearing_DK_Search = "Debet";
		if ($scope.TambahClearing_TipeRef_Search == "PPh 22 Terbayar WAPU") {
			$scope.ShowNoRegPajak = true;
			$scope.ShowNoSO = false;
			RefundType =  'PPh 22';
			// filter = [{
				//VendorId: $scope.CustIdAR,
				//VendorName: '',
				//RefundType: 'PPh 22'
				//PIId: 0
			// }];
			clearingFactory.getDataForRefundNew(RefundType).then(
				function (res) {
					$scope.TambahClearing_NoRefOption = res.data.Result;
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}
		else if ($scope.TambahClearing_TipeRef_Search == "PPh 23 Terbayar") {
			$scope.ShowNoRegPajak = true;
			$scope.ShowNoSO = false;
			$scope.ShowInternal = false;
			RefundType = 'PPh 23';

			// filter = [{
				//VendorId: $scope.CustIdAR,
				//VendorName: '',
				//RefundType: 'PPh 23',
				//PIId: 0
			// }];
			clearingFactory.getDataForRefundNew(RefundType).then(
				function (res) {
					$scope.TambahClearing_NoRefOption = res.data.Result;
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}
		else if ($scope.TambahClearing_TipeRef_Search == "PPN Keluaran WAPU") {
			$scope.ShowNoRegPajak = true;
			$scope.ShowNoSO = false;
			$scope.ShowInternal = false;
			RefundType = 'PPN WAPU';

			// filter = [{
				// VendorId: $scope.CustIdAR,
				// VendorName: '',
				//RefundType: 'PPN WAPU',
				//PIId: 0
			// }];
			clearingFactory.getDataForRefundNew(RefundType).then(
				function (res) {
					$scope.TambahClearing_NoRefOption = res.data.Result;
				},
				function (err) {
					console.log('Error -->', err);
				}
			);
		}
		else if (($scope.TambahClearing_TipeRef_Search == "Down Payment Unit")
			|| ($scope.TambahClearing_TipeRef_Search == "Down Payment Service")
			|| ($scope.TambahClearing_TipeRef_Search == "Down Payment Parts")) {
			$scope.ShowNoRegPajak = false;
			$scope.ShowNoSO = true;
			$scope.ShowInternal = false;
		}
		else if ($scope.TambahClearing_TipeRef_Search == "Internal"){
			$scope.ShowNoRegPajak = false;
			$scope.ShowNoSO = false;
			$scope.ShowInternal = true;
			$scope.optionsLihatClearing_DK_Search = [{ name: "Kredit", value: "Kredit" },{ name: "Debet", value: "Debet" }];
			$scope.TambahClearing_DK_Search = "Kredit";
		}
	}


	$scope.LihatClearingt_CheckFields = function () {
		var passed = false;

		// if (angular.isUndefined($scope.LihatClearing_TujuanPembayaran_TanggalClearing) || isNaN($scope.LihatClearing_TujuanPembayaran_TanggalClearing)) {
		// 	alert("Tanggal  harus diisi !");
		// 	return (passed);
		// }

		// if (angular.isUndefined($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo) || isNaN($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo)) {
		// 	alert("Tanggal  harus diisi !");
		// 	return (passed);
		// }

		// if (angular.isUndefined($scope.LihatClearing_NamaBranch_Search) || $scope.LihatClearing_NamaBranch_Search == "") {
		// 	alert("Nama Branch harus diisi !");
		// 	return (passed);
		// }

		if(angular.isUndefined($scope.LihatClearing_TujuanPembayaran_TanggalClearing) || isNaN($scope.LihatClearing_TujuanPembayaran_TanggalClearing)){
			bsNotify.show({
				title: "Warning",
				content: "Tanggal Awal harus diisi !",
				type: 'warning'
			});
			return (passed);
		}else if(angular.isUndefined($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo) || isNaN($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo)){
			bsNotify.show({
				title: "Warning",
				content: "Tanggal Akhir harus diisi !",
				type: 'warning'
			});
			return (passed);
		}else if(angular.isUndefined(angular.isUndefined($scope.LihatClearing_NamaBranch_Search) || $scope.LihatClearing_NamaBranch_Search == "")){
			bsNotify.show({
				title: "Warning",
				content: "Nama Branch harus diisi !",
				type: 'warning'
			});
			return (passed);
		}

		passed = true;

		return (passed);
	}

	$scope.CariClearingData = function () {
		//if (($scope.LihatClearing_TujuanPembayaran_TanggalClearing != null)
		//&& ($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo != null))
		//{
		//	$scope.GetListClearing(1, $scope.uiGridPageSize);
		//	$scope.ShowClearingData = true;
		//	Enable_LihatClearing();
		//}
		//else
		//{
		//	alert("Tgl Awal/Akhir Blm Diisi");
		//}

		if (!$scope.LihatClearingt_CheckFields())
			return;

		$scope.GetListClearing(1, $scope.uiGridPageSize);
		$scope.ShowClearingData = true;
		Enable_LihatClearing();

	};

	$scope.LihatClearing_NamaBranch_Search_SelectedChange = function () {
		console.log("Nama Branch ", $scope.LihatClearing_NamaBranch_Search);
	}

	$scope.GetListClearing = function (Start, Limit) {
		//console.log($scope.LihatClearing_NamaBranch_Search);
		clearingFactory.GetListClearing(Start, Limit, $filter('date')(new Date($scope.LihatClearing_TujuanPembayaran_TanggalClearing), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo), 'yyyy-MM-dd'), $scope.LihatClearing_NamaBranch_Search)
			.then(
				function (res) {
					$scope.LihatClearing_DaftarClearing_UIGrid.data = res.data.Result;
					$scope.LihatClearing_DaftarClearing_UIGrid.totalItems = res.data.Total;
					$scope.LihatClearing_DaftarClearing_UIGrid.paginationPageSize = Limit;
					$scope.LihatClearing_DaftarClearing_UIGrid.paginationPageSizes = [Limit];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};
	$scope.GetListClearingFilter = function (Start, Limit) {
		//console.log($scope.LihatClearing_NamaBranch_Search);
		var newTextFilter = $scope.LihatClearing_DaftarClearing_Search_Text.replace(/\//g,"").replace(/\-/g,"");
		console.log("newFilter", newTextFilter)
		clearingFactory.GetListClearingFilter(Start, Limit, $filter('date')(new Date($scope.LihatClearing_TujuanPembayaran_TanggalClearing), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatClearing_TujuanPembayaran_TanggalClearingTo), 'yyyy-MM-dd'), $scope.LihatClearing_NamaBranch_Search + '|' + newTextFilter + '|' + $scope.LihatClearing_DaftarClearing_Search)
			.then(
				function (res) {
					$scope.LihatClearing_DaftarClearing_UIGrid.data = res.data.Result;
					$scope.LihatClearing_DaftarClearing_UIGrid.totalItems = res.data.Total;
					$scope.LihatClearing_DaftarClearing_UIGrid.paginationPageSize = Limit;
					$scope.LihatClearing_DaftarClearing_UIGrid.paginationPageSizes = [Limit];
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.LihatClearing_DaftarClearing_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,

		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.LihatClearing_DaftarClearing_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentClearingId = 0;
				}
				else {
					$scope.currentClearingId = row.entity.ClearingId;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.GetListClearing(pageNumber, pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tgl Clearing ', field: 'TanggalClearing', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'ClearingId', field: 'ClearingId', visible: false },
				{ name: 'No Clearing ', field: 'NoClearing', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailClearing(row)">{{row.entity.NoClearing}}</a>' },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan' },
				{ name: 'Tgl Pengajuan', field: 'TglPengajuan', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan' },
				{ name: 'Alasan Pengajuan', field: 'AlasanPengajuan' },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan' }
			]
	};

	$scope.LihatDetailClearing = function (row) {
		$scope.LihatClearing_Top_NomorClearing = row.entity.NoClearing;
		$scope.LihatClearing_TanggalClearing = row.entity.TanggalClearing;
		$scope.currentClearingId = row.entity.ClearingId;
		$scope.ShowClearingDetail = true;
		$scope.Show_LihatClearingHeader = false;
		$scope.ShowClearingData = false;

		//UNFIX || Butuh return OrgId dari function getList dari row
		clearingFactory.getDataBranchComboBox($scope.user.OrgId)
			.then(
				function (res) {
					$scope.optionsLihatPengajuan_Clearing_NamaBranch_Search = res.data.Result;
					$scope.LihatPengajuan_Clearing_NamaBranch_Search = $scope.user.OutletId;
				}
			);

		clearingFactory.getLihatClearingListAR(1, $scope.uiGridPageSize, row.entity.ClearingId)
			.then
			(
			function (res) {
				$scope.LihatClearing_DaftarARUntukClearing_UIGrid.data = res.data.Result;
				$scope.LihatClearing_DaftarARUntukClearing_UIGrid.totalItems = res.data.Total;
				$scope.LihatClearing_DaftarARUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatClearing_DaftarARUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				//button revers
				$scope.EnabledReverse = res.data.Result[0].Status;
				if($scope.EnabledReverse == "true"){
					$scope.LihatClearing_PengajuanReversalModal_EnabledDisabled = true;
				}else{
					$scope.LihatClearing_PengajuanReversalModal_EnabledDisabled = false;
				}
			}
			);
		clearingFactory.getLihatClearingListAP(1, $scope.uiGridPageSize, row.entity.ClearingId)
			.then
			(
			function (res) {
				$scope.LihatClearing_DaftarAPUntukClearing_UIGrid.data = res.data.Result;
				$scope.LihatClearing_DaftarAPUntukClearing_UIGrid.totalItems = res.data.Total;
				$scope.LihatClearing_DaftarAPUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatClearing_DaftarAPUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				//button revers
				$scope.EnabledReverse = res.data.Result[0].Status;
				if($scope.EnabledReverse == "true"){
					$scope.LihatClearing_PengajuanReversalModal_EnabledDisabled = true;
				}else{
					$scope.LihatClearing_PengajuanReversalModal_EnabledDisabled = false;
				}
			}
			);
		clearingFactory.getLihatClearingListGL(1, $scope.uiGridPageSize, row.entity.ClearingId)
			.then
			(
			function (res) {
				$scope.LihatClearing_DaftarGLUntukClearing_UIGrid.data = res.data.Result;
				$scope.LihatClearing_DaftarGLUntukClearing_UIGrid.totalItems = res.data.Total;
				$scope.LihatClearing_DaftarGLUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatClearing_DaftarGLUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
		clearingFactory.getLihatClearingListBiaya(1, $scope.uiGridPageSize, row.entity.ClearingId)
			.then
			(
			function (res) {
				$scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.data = res.data.Result;
				$scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.totalItems = res.data.Total;
				$scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
	};

	$scope.TambahClearing_DaftarAR_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarAR = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.BillingId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
				$scope.GetClearing_CariClearingDataAR(pageNumber, pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: "BillingId", field: "BillingId", visible: false },
				{ name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'Nomor Billing', field: 'NoBilling' },

				{ name: 'Nomor SO', displayName: "Nomor SO", field: 'SoCode' },
				{ name: 'No Pelanggan', field: 'CustomerId', visible: false },
				{ name: 'No Pelanggan', field: 'CustomerCode' },
				{ name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Status Batal', field: 'Status' },
				{ name: 'Nominal AR', displayName: 'Nominal AR', field: 'NominalBilling', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' },
			]
	};

	$scope.TambahClearing_DaftarARUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: "BillingId", field: "BillingId", visible: false },
			{ name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: 'No Billing', field: 'NoBilling', enableCellEdit: false },
			{
				name: 'Nomor SO', displayName: "Nomor SO", field: 'SoCode', enableCellEdit: false
			},
			{ name: 'No Pelanggan', field: 'CustomerId', visible: false },
			{ name: 'Nama Pelanggan', field: 'CustomerName', enableCellEdit: false },
			{ name: 'Status Batal', field: 'Status', enableCellEdit: false },
			{ name: 'Nominal AR', displayName: 'Nominal AR', field: 'NominalBilling', cellFilter: 'currency:"":0', enableCellEdit: false },
			{ name: 'Debet/Kredit', field: 'TipeDK', enableCellEdit: false },
			{
				name: 'Keterangan', field: 'Keterangan',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: 'string',
				editableCellTemplate: '<input type="text" ui-grid-editor maxlength="50" ng-model="row.entity.Keterangan">'
			},
			{ name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarARUntukClearing(row)">Hapus</button>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahClearing_DaftarARUntukClearing = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			}
			)
		}
	};

	$scope.deleteDaftarARUntukClearing = function (row) {
		var index = $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.indexOf(row.entity);
		$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.splice(index, 1);
		$scope.CalculateBalance();
	};

	$scope.TambahDaftarAR_AddItem = function () {
		console.log("TambahDaftarAR_AddItem");
		$scope.gridApi_DaftarAR.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.NoBilling);
				if (row.NoBilling == obj.NoBilling)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.push
					(
					{
						BillingId: row.BillingId,
						NoBilling: row.NoBilling,
						SoCode: row.SoCode,
						TanggalBilling: row.TanggalBilling,
						CustomerId: row.CustomerId,
						CustomerName: row.CustomerName,
						NominalBilling: row.NominalBilling,
						Status: row.Status,
						TipeDK: row.TipeDK
					}
					);
					$scope.CustIdAR = row.CustomerId;
					$scope.TambahClearing_NoGL_Search_EnableDisable = true;
			}
		}
		)
		$scope.TambahClearing_NoGL_Search_EnableDisable = true;
		$scope.CalculateBalance();
		
	};

	$scope.TambahClearing_DaftarGLUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: "No Gl Account Clearing", field: "NoGL" },
			{ name: 'Nama Gl Account Clearing', field: 'NamaGL' },
			{ name: 'Tipe Referensi', field: 'TipeReferensi' },
			{ name: 'Nomor Referensi', field: 'NoReferensi' },
			{ name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
			{ name: 'Debet/Kredit', field: 'TipeDK' },
			{
				name: 'Keterangan', field: 'Keterangan',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: 'string',
				editableCellTemplate: '<input type="text" ui-grid-editor maxlength="50" ng-model="row.entity.Keterangan">'
			},
			{ name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarGLUntukClearing(row)">Hapus</button>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahClearing_DaftarGLClearing = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			}
			)
		}
	};

	$scope.deleteDaftarGLUntukClearing = function (row) {
		var index = $scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.indexOf(row.entity);
		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.splice(index, 1);
		$scope.CalculateBalance();
	};

	$scope.TambahClearingGL_AddItem = function () {
		console.log("TambahDaftarAR_AddItem");
		
		if($scope.TambahClearing_NoGL_Search == "" || $scope.TambahClearing_NoGL_Search == undefined || $scope.TambahClearing_NoGL_Search == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Nomor GL Account harus diisi.",
					type: 'warning'
				}
			);
			return;  
		}else if($scope.TambahClearing_TipeRef_Search ==  "" || $scope.TambahClearing_TipeRef_Search == undefined || $scope.TambahClearing_TipeRef_Search == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Tipe Referensi harus diisi.",
					type: 'warning'
				}
			);
			return;  
		}else if($scope.TambahClearing_DK_Search ==  "" || $scope.TambahClearing_DK_Search == undefined || $scope.TambahClearing_DK_Search == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Debet/Kredit harus di pilih.",
					type: 'warning'
				}
			);
			return;  
		}

		var GLNominal = 0;
		if (($scope.TambahClearing_TipeRef_Search == "PPh 22 Terbayar WAPU")
			|| ($scope.TambahClearing_TipeRef_Search == "PPh 23 Terbayar")
			|| ($scope.TambahClearing_TipeRef_Search == "PPN Keluaran WAPU")) {
				if($scope.TambahClearing_NoRef == undefined || $scope.TambahClearing_NoRef == ""){
					bsNotify.show(
						{
							title : "Warning",
							content : "Norek Dokumen Pajak harus diisi",
							type : "warning"
						}
					);
					return;
				}
			$scope.NoReferensi = $scope.TambahClearing_NoRef;
			$scope.Nominal = $scope.TambahClearing_Nominal;
		}
		else if (($scope.TambahClearing_TipeRef_Search == "Down Payment Unit")
			|| ($scope.TambahClearing_TipeRef_Search == "Down Payment Service")
			|| ($scope.TambahClearing_TipeRef_Search == "Down Payment Parts")) {
				if($scope.TambahClearing_NoSO == undefined || $scope.TambahClearing_NoSO == ""){
					bsNotify.show(
						{
							title : "Warning",
							content : "Nomor SO harus diisi",
							type : "warning"
						}
					);
					return;
				}
			$scope.NoReferensi = $scope.TambahClearing_NoSO;
			$scope.Nominal = $scope.TambahClearing_NominalSO;
		}
		else if ($scope.TambahClearing_TipeRef_Search == "Internal")
		{
			$scope.Nominal = $scope.TambahClearing_NominalInternal;
		}
		GLNominal = $scope.Nominal;
		console.log("GLNominal", GLNominal);

		if (!angular.isNumber(GLNominal) && (GLNominal != undefined)) {
			console.log("GLNominal", GLNominal);
			GLNominal = GLNominal.replace(/,/g, "");
			console.log("GLNominal", GLNominal);
		}

		var GLId = $scope.TambahClearing_NoGL_Search;
		var NoGL = $filter('filter')($scope.optionsLihatClearing_NoGL_Search, { value: GLId })[0].name.split(' - ')[0];

		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.push
			(
			{
				NoGL: NoGL,
				NamaGL: $scope.TambahClearing_NamaGL,
				TipeReferensi: $scope.TambahClearing_TipeRef_Search,
				NoReferensi: $scope.NoReferensi,
				Nominal: GLNominal,
				TipeDK: $scope.TambahClearing_DK_Search,
				Keterangan: $scope.TambahClearing_Ket
			}
			)
		$scope.CalculateBalance();
	};

	$scope.TambahClearing_DaftarAP_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarAP = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// debugger;
				$scope.GetClearing_CariClearingDataAP(pageNumber, pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'No Instruksi Pembayaran', field: 'PaymentNo' },
				{ name: 'Vendor Code', field: 'VendorId' },
				{ name: 'Nama Branch/Vendor', field: 'VendorName' },
				{ name: 'Nominal AP', displayName: 'Nominal AP', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' },
				{ name: 'PaymentId', field: 'PaymentId', visible:false},
			]
	};

	$scope.TambahClearing_DaftarAPUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: 'Payment Id', field: 'PaymentId',visible:false },
			{ name: 'No Instruksi Pembayaran', field: 'PaymentNo', enableCellEdit: false},
			{ name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: 'No Branch/Vendor', field: 'VendorId', enableCellEdit: false },
			{ name: 'Nama Branch/Vendor', field: 'VendorName', enableCellEdit: false },
			{ name: 'Nominal AP', displayName: 'Nominal AP', field: 'Nominal', cellFilter: 'currency:"":0', enableCellEdit: false },
			{ name: 'Debet/Kredit', field: 'TipeDK', enableCellEdit: false },
			{
				name: 'Keterangan', field: 'Keterangan',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: 'string',
				editableCellTemplate: '<input type="text" ui-grid-editor maxlength="50" ng-model="row.entity.Keterangan">'
			},
			{ name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarAPUntukClearing(row)">Hapus</button>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahClearing_DaftarAPUntukClearing = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
				$scope.GetClearing_CariClearingDataAP(pageNumber, pageSize);
			});
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			}
			)
		}
	};

	$scope.deleteDaftarAPUntukClearing = function (row) {
		var index = $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.indexOf(row.entity);
		$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.splice(index, 1);
		$scope.CalculateBalance();
	};

	$scope.TambahDaftarAP_AddItem = function () {
		console.log("TambahDaftarAP_AddItem");
		$scope.gridApi_DaftarAP.selection.getSelectedRows().forEach(function (row) {
			var found = 0;
			$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.PaymentId);
				if (row.PaymentId == obj.PaymentId)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.push(
					{
						PaymentId: row.PaymentId,
						PaymentNo: row.PaymentNo,
						TanggalPayment: row.TanggalPayment,
						VendorId: row.VendorId,
						VendorName: row.VendorName,
						Nominal: row.Nominal,
						TipeDK: row.TipeDK
					}
				)
			}
		}
		)
		$scope.CalculateBalance();
	};

	$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		columnDefs: [
			{ name: 'Tipe Biaya Lain Lain', field: 'TipeBiaya' },
			{ name: 'Debet/Kredit', field: 'TipeDK' },
			{ name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
			{ name: 'Hapus', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarBiayaUntukClearing(row)">Hapus</button>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahClearing_DaftarBiayaClearing = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahClearing_DaftarSpkTarget_UIGrid.data = $scope.TambahClearing_UnitFullPaymentSPK_Paging(pageNumber);
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			}
			)
		}
	};

	$scope.deleteDaftarBiayaUntukClearing = function (row) {
		var index = $scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.indexOf(row.entity);
		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.splice(index, 1);
		$scope.CalculateBalance();
	};

	$scope.TambahClearingBiaya_AddItem = function () {
		console.log("TambahClearingBiaya_AddItem");
		if($scope.TambahClearing_TipeBiaya_Search == undefined || $scope.TambahClearing_TipeBiaya_Search == ""){
			bsNotify.show({
				title: "Warning",
				content: "Field Tipe biaya lain-lain harus dipilih",
				type: 'warning'
			});
			return
		}else if($scope.TambahClearing_BiayaDK_Search == undefined || $scope.TambahClearing_BiayaDK_Search == ""){
			bsNotify.show({
				title: "Warning",
				content: "Field Debet/Kredit harus dipilih",
				type: 'warning'
			});
			return
		}else if($scope.TambahClearing_BiayaNominal == undefined || $scope.TambahClearing_BiayaNominal == ""){
			bsNotify.show({
				title: "Warning",
				content: "Field Nominal Harus diisi",
				type: 'warning'
			});
			return
		}
		var NominalBiaya = 0;
		NominalBiaya = $scope.TambahClearing_BiayaNominal;
		console.log("NominalBiaya", NominalBiaya);

		if (!angular.isNumber(NominalBiaya) && (NominalBiaya != undefined)) {
			console.log("NominalBiaya", NominalBiaya);
			NominalBiaya = NominalBiaya.replace(/,/g, "");
			console.log("NominalBiaya", NominalBiaya);
		}

		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.push
			(
			{
				TipeBiaya: $scope.TambahClearing_TipeBiaya_Search,
				Nominal: NominalBiaya,
				TipeDK: $scope.TambahClearing_BiayaDK_Search
			}
			)
		$scope.CalculateBalance();
	};

	$scope.CalculateBalance = function () {
		var TotalAR = 0;
		var TotalAP = 0;
		var TotalGLDebet = 0;
		var TotalGLKredit = 0;
		var TotalBiayaDebet = 0;
		var TotalBiayaKredit = 0;

		$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.forEach(function (obj) {
			TotalAR += parseInt(obj.NominalBilling);
		});

		$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.forEach(function (obj) {
			TotalAP += parseInt(obj.Nominal);
		});

		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.forEach(function (obj) {
			if (obj.TipeDK == "Debet") {
				TotalGLDebet += parseInt(obj.Nominal);
			}
			else {
				TotalGLKredit += parseInt(obj.Nominal);
			}
		});

		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data.forEach(function (obj) {
			console.log(obj.TipeDK);
			console.log(obj.Nominal);
			if (obj.TipeDK == "Debet") {
				TotalBiayaDebet += parseInt(obj.Nominal);
				//console.log("TotalBiayaDebet");
				//console.log(TotalBiayaDebet);
			}
			else {
				TotalBiayaKredit += parseInt(obj.Nominal);
				console.log("TotalBiayaKredit");
				console.log(TotalBiayaKredit);
			}
		});

		var Balance = 0;
		console.log(TotalAR);
		console.log(TotalAP);
		console.log(TotalBiayaDebet);
		console.log(TotalGLDebet);
		Balance = parseInt(TotalAR) - parseInt(TotalAP) - parseInt(TotalBiayaDebet) - parseInt(TotalGLDebet);
		Balance += parseInt(TotalGLKredit);
		Balance += parseInt(TotalBiayaKredit);
		$scope.TambahClearing_Balance = addSeparatorsNF(Balance, '.', '.', ',');
	};



	//UI Grid Lihat
	$scope.LihatClearing_DaftarARUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarAR = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: "BillingId", field: "BillingId", visible: false },
				{ name: 'Tanggal Billing', field: 'TanggalBilling', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'No Billing', field: 'NoBilling' },
				{ name: 'No Pelanggan', field: 'CustomerId', visible: false },
				{ name: 'No Pelanggan', field: 'CustomerCode' },
				{ name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Nominal Ar', field: 'NominalBilling', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' },
				{ name: 'Keterangan', field: 'Description' }
			]
	};

	$scope.LihatClearing_DaftarAPUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarAP = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Payment Id', field: 'PaymentId',visible:false },
				{ name: 'No Instruksi Pembayaran', field: 'PaymentNo' },
				{ name: 'Tanggal Instruksi Pembayaran', field: 'TanggalPayment', cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: 'No Branch/Vendor', field: 'VendorId' },
				{ name: 'Nama Branch/Vendor', field: 'VendorName' },
				{ name: 'Nominal Ap', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' },
				{ name: 'Keterangan', field: 'Description' }
			]
	};

	$scope.LihatClearing_DaftarGLUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarGL = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: "No Gl Account", field: "NoGL" },
				{ name: 'Nama Gl Account', field: 'NamaGL' },
				{ name: 'Tipe Referensi', field: 'TipeReferensi' },
				{ name: 'No Referensi', field: 'NoReferensi' },
				{ name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' },
				{ name: 'Keterangan', field: 'Description' }
			]
	};

	$scope.LihatClearing_DaftarBiayaUntukClearing_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarBiaya = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				{ name: 'Tipe Biaya Lain Lain', field: 'TipeBiaya' },
				{ name: 'Nominal ', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: 'Debet/Kredit', field: 'TipeDK' }
			]
	};

	$scope.ToTambahClearing = function () {
		$scope.TambahClearing_ResetData();
		$scope.getListGLClearing();
		// $scope.getNoClearing();
		$scope.Show_ShowLihatClearing = false;
		$scope.Show_ShowTambahClearing = true;
		$scope.TambahClearing_NoGL_Search_EnableDisable = true;
		$scope.ShowClearingDetail = false;
		$scope.ShowClearingData = false;
		$scope.Show_LihatClearingHeader = false;

		Enable_TambahClearing_DaftarAR();
		$scope.TambahClearing_TanggalBilling = new Date();
		$scope.TambahClearing_TanggalBillingTo = new Date();
		$scope.TambahClearing_TanggalInstruksi = new Date();
		$scope.TambahClearing_TanggalInstruksiTo = new Date();
		$scope.TambahClearing_TanggalClearing = new Date();
	};

	// $scope.getNoClearing = function () {
	// 	console.log("getNoClearing");
	// 	clearingFactory.getNoClearing()
	// 		.then(
	// 			function (res) {
	// 				$scope.loading = false;
	// 				$scope.TambahClearing_Top_NomorClearing = res.data.Result[0].NoClearing;
	// 				console.log("NoClearing = ", $scope.NoClearing);
	// 			},
	// 			function (err) {
	// 				console.log("err=>", err);
	// 			}
	// 		);
	// }

	$scope.LoadingSimpan = false;
	$scope.TambahClearing_Pengajuan = function () {
		var data;
		$scope.LoadingSimpan = true;

		$scope.CalculateBalance();

		$scope.ARSubmitData = [];
		$scope.GLSubmitData = [];
		$scope.APSubmitData = [];
		$scope.EXSubmitData = [];

		$scope.TambahClearing_ARListData();
		$scope.ARSubmitData = $scope.CreateARSubmitData;
		$scope.TambahClearing_GLListData();
		$scope.GLSubmitData = $scope.CreateGLSubmitData;
		$scope.TambahClearing_APListData();
		$scope.APSubmitData = $scope.CreateAPSubmitData;
		$scope.TambahClearing_EXListData();
		$scope.EXSubmitData = $scope.CreateEXSubmitData;

		data =
			[{
				OutletId: $scope.currentOutletId,
				ClearingDate: $scope.TambahClearing_TanggalClearing,
				NoClearing: $scope.TambahClearing_Top_NomorClearing,
				ClearingDetailARXML: "",
				ClearingDetailGLXML: "",
				ClearingDetailAPXML: "",
				ClearingDetailEXXML: "",
				ARList: $scope.ARSubmitData,
				GLList: $scope.GLSubmitData,
				APList: $scope.APSubmitData,
				EXList: $scope.EXSubmitData
			}]
	if(($scope.TambahClearing_DaftarAR_UIGrid.data.length != 0 && $scope.TambahClearing_DaftarARUntukClearing_UIGrid.data.length != 0) ||
		($scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data.length != 0) || ($scope.TambahClearing_DaftarAP_UIGrid.data.length != 0 && $scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data.length != 0) ){
		clearingFactory.submitClearing(data)
		.then
		(
		function (res) {
			console.log(res);
			//alert("Pengajuan Berhasil");
			bsNotify.show(
				{
					title: "Berhasil",
					content: "Pengajuan Berhasil.",
					type: 'success'
				}
			);
			$scope.LoadingSimpan = false;
			$scope.CariClearingData();//ssssss
			$scope.TambahClearing_Batal();
		},
		function (err) {
			console.log("err=>", err);
			// if (err != null) {
			// 	alert(err.data.Message);
			// }
			
			bsNotify.show({
				title: "Gagal",
				content: err.data.Message,
				type: 'danger'
			});
			
			// bsNotify.show(
			// 	{
			// 		title: "Gagal",
			// 		content: "Pengajuan Gagal.",
			// 		type: 'danger'
			// 	}
			// );
			$scope.LoadingSimpan = false;
			return false;
		}
		)
	}else{
		bsNotify.show(
			{
				title: "Gagal",
				content: "Pengajuan Gagal.",
				type: 'danger'
			}
		);
		$scope.LoadingSimpan = false;
		return;
	}
		// clearingFactory.submitClearing(data)
		// 	.then
		// 	(
		// 	function (res) {
		// 		console.log(res);
		// 		//alert("Pengajuan Berhasil");
		// 		bsNotify.show(
		// 			{
		// 				title: "Berhasil",
		// 				content: "Pengajuan Berhasil.",
		// 				type: 'success'
		// 			}
		// 		);
		// 		$scope.CariClearingData();//ssssss
		// 		$scope.TambahClearing_Batal();
		// 	},
		// 	function (err) {
		// 		console.log("err=>", err);
		// 		if (err != null) {
		// 			alert(err.data.Message);
		// 		}
		// 		bsNotify.show(
		// 			{
		// 				title: "Gagal",
		// 				content: "Pengajuan Gagal.",
		// 				type: 'danger'
		// 			}
		// 		);
		// 	}
		// 	)
	}

	$scope.TambahClearing_ARListData = function () {
		$scope.CreateARSubmitData = [];
		angular.forEach($scope.TambahClearing_DaftarARUntukClearing_UIGrid.data, function (value, key) {
			var obj =
				{
					"BillingId": value.BillingId,
					"Nominal": value.NominalBilling,
					"TipeDK": value.TipeDK,
					"Description": value.Keterangan,
					"BillingNo": value.NoBilling
				};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateARSubmitData.push(obj);
		});
	}

	$scope.TambahClearing_GLListData = function () {
		$scope.CreateGLSubmitData = [];
		angular.forEach($scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data, function (value, key) {
			var obj =
				{
					"NomorGL": value.NoGL,
					"NomorRef": value.NoReferensi,
					"TipeRef": value.TipeReferensi,
					"Nominal": value.Nominal,
					"TipeDK": value.TipeDK,
					"Description": value.Keterangan
				};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateGLSubmitData.push(obj);
		});
	}

	$scope.TambahClearing_APListData = function () {
		$scope.CreateAPSubmitData = [];
		angular.forEach($scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data, function (value, key) {
			var obj =
				{
					"PaymentId": value.PaymentId,
					"Nominal": value.Nominal,
					"TipeDK": value.TipeDK,
					"Description": value.Keterangan
				};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateAPSubmitData.push(obj);
		});
	}

	$scope.TambahClearing_EXListData = function () {
		$scope.CreateEXSubmitData = [];
		angular.forEach($scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data, function (value, key) {
			var obj =
				{
					"TipeBiaya": value.TipeBiaya,
					"Nominal": value.Nominal,
					"TipeDK": value.TipeDK
				};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateEXSubmitData.push(obj);
		});
	}

	$scope.TambahClearing_NoRef_Blur = function () {
		if ($scope.TambahClearing_NoGL_Search !== "") {

			clearingFactory.getDataGL($scope.TambahClearing_NoRef, $scope.TambahClearing_TipeRef_Search)
				.then(
					function (res) {
						console.log("GL")
						$scope.TambahClearing_NamaGL = res.data.Result[0].NamaGL;

						$scope.TambahClearing_Nominal = addSeparatorsNF(res.data.Result[0].Nominal, '.', '.', ',');
					}
				);
		}
	}

	$scope.TambahClearing_CheckFields = function () {
		var passed = false;

		console.log("Check Fields");

		// if (angular.isUndefined($scope.TambahClearing_TanggalBilling) || $scope.TambahClearing_TanggalBilling == "") {
		// 	alert("Tgl Awal Hrs Diisi !");
		// 	return (passed);
		// }
		// if (angular.isUndefined($scope.TambahClearing_TanggalBillingTo) || $scope.TambahClearing_TanggalBillingTo == "") {
		// 	alert("Tgl Akhir Hrs Diisi !");
		// 	return (passed);
		// }
		// if (angular.isUndefined($scope.TambahStatusBatal_SearchDropdown) || $scope.TambahStatusBatal_SearchDropdown == "") {
		// 	alert("Status Hrs Diisi !");
		// 	return (passed);
		// }

		if(angular.isUndefined($scope.TambahClearing_TanggalBilling) || $scope.TambahClearing_TanggalBilling == ""){
			bsNotify.show({
				title: "Warning",
				content: "Tgl Awal Hrs Diisi !",
				type: 'warning'
			});
			return (passed);
		}else if (angular.isUndefined($scope.TambahClearing_TanggalBillingTo) || $scope.TambahClearing_TanggalBillingTo == ""){
			bsNotify.show({
				title: "Warning",
				content: "Tgl Akhir Hrs Diisi !",
				type: 'warning'
			});
			return (passed);
		}else if (angular.isUndefined($scope.TambahStatusBatal_SearchDropdown) || $scope.TambahStatusBatal_SearchDropdown == ""){
			bsNotify.show({
				title: "Warning",
				content: "Status Hrs Diisi !",
				type: 'warning'
			});
			return (passed);
		}

		passed = true;

		return (passed);
	}

	$scope.GetClearing_CariClearingDataAR = function (start, page, tmpCari) {
		if(tmpCari == 666){
			if (!$scope.TambahClearing_CheckFields())
			return;
		}

		clearingFactory.GetListDaftarAR(start, page, $filter('date')(new Date($scope.TambahClearing_TanggalBilling), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.TambahClearing_TanggalBillingTo), 'yyyy-MM-dd'), $scope.TambahStatusBatal_SearchDropdown)
			.then
			(
			function (res) {
				$scope.TambahClearing_DaftarAR_UIGrid.data = res.data.Result;
				$scope.TambahClearing_DaftarAR_UIGrid.totalItems = res.data.Total;
				$scope.TambahClearing_DaftarAR_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.TambahClearing_DaftarAR_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
	};

	$scope.TambahClearing_CariClearingDataAR = function (tmpCari) {
		
		$scope.GetClearing_CariClearingDataAR(1, $scope.uiGridPageSize, tmpCari);
	};

	$scope.GetClearing_CariClearingDataAP = function (start, page) {
		clearingFactory.GetListDaftarAP(start, page, $filter('date')(new Date($scope.TambahClearing_TanggalInstruksi), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.TambahClearing_TanggalInstruksiTo), 'yyyy-MM-dd'))
			.then
			(
			function (res) {
				$scope.TambahClearing_DaftarAP_UIGrid.data = res.data.Result;
				$scope.TambahClearing_DaftarAP_UIGrid.totalItems = res.data.Total;
				$scope.TambahClearing_DaftarAP_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.TambahClearing_DaftarAP_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}
			);
	};


	$scope.TambahClearing_CariClearingDataAP = function () {
		$scope.GetClearing_CariClearingDataAP(1, $scope.uiGridPageSize);
	};

	// $scope.LihatClearing_PengajuanReversalModal = function(){
	// 	console.log("Pengajuan Clearing 2");
	// 	angular.element('#ModalLihatClearing_PengajuanReversal').modal('show');
	// }	

	$scope.Batal_LihatClearing_PengajuanReversalModal = function(){
		angular.element('#ModalLihatClearing_PengajuanReversal').modal('hide');
	}
	$scope.Pengajuan_LihatClearing_PengajuanReversalModal = function () {
		console.log("Pengajuan Reversal");
		var data;
		data =
			[{
				"ApprovalTypeName": "Clearing Reversal",
				"DocumentId": $scope.currentClearingId,
				"ReversalReason": $scope.LihatClearing_PengajuanReversal_AlasanReversal
			}]

		clearingFactory.submitDataApproval(data)
			.then
			(
			function (res) {
				console.log(res);
				
				$scope.GetListClearing(1, $scope.uiGridPageSize);
				$scope.Batal_LihatClearing_PengajuanReversalModal();
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Pengajuan Berhasil.",
						type: 'success'
					}
				);
				$scope.TambahClearing_Batal();



			},
			function (err) {
				console.log("err=>", err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Pengajuan Gagal.",
						type: 'danger'
					}
				);
			}
			);
	};

	$scope.LihatClearing_Batal = function () {
		$scope.Show_ShowLihatClearing = true;
		$scope.Show_ShowTambahClearing = false;
		$scope.ShowClearingDetail = false;
		$scope.Show_LihatClearingHeader = true;
		$scope.ShowClearingData = true;
	}

	$scope.TambahClearing_Batal = function () {
		$scope.Show_ShowLihatClearing = true;
		$scope.Show_ShowTambahClearing = false;
		$scope.ShowClearingDetail = false;
		$scope.Show_LihatClearingHeader = true;
		$scope.ShowClearingData = true;
		$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_NoGL_Search = "";
		$scope.TambahClearing_NamaGL = "";
		$scope.TambahClearing_NoRef = "";
		$scope.TambahClearing_Nominal = 0;
		$scope.TambahClearing_NominaInternal = 0;
		$scope.TambahClearing_DK_Search = "";
		$scope.TambahClearing_Ket = "";
		$scope.TambahClearing_TipeBiaya_Search = "";
		$scope.TambahClearing_BiayaNominal = 0;
		$scope.TambahClearing_BiayaDK_Search = "";

	}

	$scope.TambahClearing_TipeBiaya_SelectedChange = function () {
		if ($scope.TambahClearing_TipeBiaya_Search == "Bea Materai") {
			$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }];
		}
		else if ($scope.TambahClearing_TipeBiaya_Search == "Other Income") {
			$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Kredit", value: "Kredit" }];
		}
		else if ($scope.TambahClearing_TipeBiaya_Search == "Other Expense") {
			$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }];
		}
		else if ($scope.TambahClearing_TipeBiaya_Search == "PPN Masukan") {
			$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		}
		else if ($scope.TambahClearing_TipeBiaya_Search == "PPV") {
			$scope.optionsLihatClearing_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		}
	}

	//Filter LihatClearing OLD
	// $scope.LihatClearing_DaftarClearing_Filter_Clicked = function () {
	// 	console.log("LihatClearing_DaftarClearing_Filter_Clicked");
	// 	if (($scope.LihatClearing_DaftarClearing_Search_Text != "") &&
	// 		($scope.LihatClearing_DaftarClearing_Search != "")) {
	// 		var nomor;
	// 		var value = $scope.LihatClearing_DaftarClearing_Search_Text;
	// 		$scope.LihatClearing_DaftarClearing_gridAPI.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.LihatClearing_DaftarClearing_Search_Text);
	// 		if ($scope.LihatClearing_DaftarClearing_Search == "No Clearing") {
	// 			nomor = 1;
	// 		}
	// 		console.log("nomor --> ", nomor);
	// 		$scope.LihatClearing_DaftarClearing_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatClearing_DaftarClearing_Search_Text;
	// 	}
	// 	else {
	// 		$scope.LihatClearing_DaftarClearing_gridAPI.grid.clearAllFilters();
	// 	}
	// };

	// NEW Filter 
	$scope.LihatClearing_DaftarClearing_Filter_Clicked = function () {
		$scope.GetListClearingFilter(1, $scope.uiGridPageSize);
	}

	// $scope.TambahClearing_DaftarAR_Clicked = function () {
	// 	console.log("TambahClearing_DaftarAR_Clicked");
	// 	if (($scope.TambahClearing_DaftarAR_Search_Text != "") &&
	// 		($scope.TambahClearing_DaftarAR_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahClearing_DaftarAR_Search_Text;
	// 		$scope.gridApi_DaftarAR.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahClearing_DaftarAR_Search_Text);
	// 		if ($scope.TambahClearing_DaftarAR_SearchDropdown == "Tanggal Billing") {
	// 			nomor = 3;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAR_SearchDropdown == "No Billing") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAR_SearchDropdown == "No Pelanggan") {
	// 			nomor = 4;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAR_SearchDropdown == "Nama Pelanggan") {
	// 			nomor = 5;
	// 		}
	// 		console.log($scope.TambahClearing_DaftarAR_SearchDropdown);
	// 		console.log("nomor --> ", nomor);
	// 		$scope.gridApi_DaftarAR.grid.columns[nomor].filters[0].term = $scope.TambahClearing_DaftarAR_Search_Text;
	// 	}
	// 	else {
	// 		$scope.gridApi_DaftarAR.grid.clearAllFilters();
	// 	}
	// };

	// new filter FE
	$scope.TambahClearing_DaftarAR_Clicked = function(){
		$scope.FitlerAR(1, $scope.uiGridPageSize);
		// console.log("TambahAlokasiUnknown_DaftarUnknown_Filter_Clicked");
		// if (($scope.TambahClearing_DaftarAR_Search_Text != "") &&
		// 	($scope.TambahClearing_DaftarAR_SearchDropdown != "")) {
		// 	var nomor;
		// 	value = $scope.TambahClearing_DaftarAR_Search_Text;
		// 	$scope.gridApi_DaftarAR.grid.clearAllFilters();
		// 	$scope.gridApi_DaftarAR.grid.getColumn($scope.TambahClearing_DaftarAR_SearchDropdown).filters[0].term = value;
		// 	console.log('filter text -->', $scope.TambahClearing_DaftarAR_Search_Text);
		// 	// if ($scope.TambahIncomingPayment_DaftarWorkOrder_SearchDropdown == "Tanggal Pembayaran") {
		// 	// 	nomor = 3;
		// 	// }
		// 	// console.log("nomor --> ", nomor);
		// 	// $scope.gridApi_InformasiPembayaran_UIGrid2.grid.columns[nomor].filters[0].term = $scope.value;
		// }
		// else {
		// 	$scope.gridApi_DaftarAR.grid.clearAllFilters();
		// 	//alert("inputan filter belum diisi !");
		// }
	}

	$scope.FitlerAR = function(start, page){
		var newText = $scope.TambahClearing_DaftarAR_Search_Text.replace(/\./g,"").replace(/\//g,"").replace(/\-/g,"");
		console.log("new Text", newText);
		clearingFactory.GetListDaftarARFilter(start, page, $filter('date')(new Date($scope.TambahClearing_TanggalBilling), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.TambahClearing_TanggalBillingTo), 'yyyy-MM-dd'), $scope.TambahStatusBatal_SearchDropdown + '|' + newText + '|' + $scope.TambahClearing_DaftarAR_SearchDropdown)
			.then(
				function (res) {
					$scope.TambahClearing_DaftarAR_UIGrid.data = res.data.Result;
					$scope.TambahClearing_DaftarAR_UIGrid.totalItems = res.data.Total;
					$scope.TambahClearing_DaftarAR_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.TambahClearing_DaftarAR_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				}
			);
	}

	$scope.FitlerAP = function(start, page){
		var newTextAP = $scope.TambahClearing_DaftarAP_Search_Text.replace(/\./g,"").replace(/\//g,"").replace(/\-/g,"");
		console.log("new Text AP", newTextAP);
		clearingFactory.GetListDaftarAPFilter(start, page, $filter('date')(new Date($scope.TambahClearing_TanggalInstruksi), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.TambahClearing_TanggalInstruksiTo), 'yyyy-MM-dd') + '|' + newTextAP + '|' + $scope.TambahClearing_DaftarAP_SearchDropdown)
			.then(
				function (res) {
					$scope.TambahClearing_DaftarAP_UIGrid.data = res.data.Result;
					$scope.TambahClearing_DaftarAP_UIGrid.totalItems = res.data.Total;
					$scope.TambahClearing_DaftarAP_UIGrid.paginationPageSize = $scope.uiGridPageSize;
					$scope.TambahClearing_DaftarAP_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
				}
			);
	}
	// OLD FIlter AP
	// $scope.TambahClearing_DaftarAP_Clicked = function () {
	// 	console.log("TambahClearing_DaftarAP_Clicked");
	// 	if (($scope.TambahClearing_DaftarAP_Search_Text != "") &&
	// 		($scope.TambahClearing_DaftarAP_SearchDropdown != "")) {
	// 		var nomor;
	// 		var value = $scope.TambahClearing_DaftarAP_Search_Text;
	// 		$scope.gridApi_DaftarAP.grid.clearAllFilters();
	// 		console.log('filter text -->', $scope.TambahClearing_DaftarAP_Search_Text);
	// 		if ($scope.TambahClearing_DaftarAP_SearchDropdown == "Tgl Instruksi Pembayaran") {
	// 			nomor = 2;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAP_SearchDropdown == "No Instruksi Pembayaran") {
	// 			nomor = 1;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAP_SearchDropdown == "No Vendor/Branch") {
	// 			nomor = 3;
	// 		}
	// 		if ($scope.TambahClearing_DaftarAP_SearchDropdown == "Nama Vendor/Branch") {
	// 			nomor = 4;
	// 		}
	// 		console.log($scope.TambahClearing_DaftarAP_SearchDropdown);
	// 		console.log("nomor --> ", nomor);
	// 		$scope.gridApi_DaftarAP.grid.columns[nomor].filters[0].term = $scope.TambahClearing_DaftarAP_Search_Text;
	// 	}
	// 	else {
	// 		$scope.gridApi_DaftarAP.grid.clearAllFilters();
	// 	}
	// };

	// New Filter AP
	$scope.TambahClearing_DaftarAP_Clicked = function () {
		$scope.FitlerAP(1, $scope.uiGridPageSize);
	}

	//created by bayu
	$scope.TambahClearing_ResetData = function () {
		$scope.TambahClearing_Top_NomorClearing = "";
		$scope.TambahStatusBatal_SearchDropdown = undefined;
		$scope.TambahClearing_DaftarAR_Search_Text = "";
		$scope.TambahClearing_DaftarAR_SearchDropdown = undefined;
		$scope.TambahClearing_DaftarAR_UIGrid.data = [];
		$scope.TambahClearing_DaftarAR_UIGrid.totalItems = 0;
		$scope.TambahClearing_DaftarARUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarARUntukClearing_UIGrid.totalItems = 0;
		$scope.TambahClearing_NoGL_Search = undefined;
		$scope.TambahClearing_NamaGL = "";
		$scope.TambahClearing_DK_Search = undefined;
		$scope.TambahClearing_TipeRef_Search = undefined;
		$scope.TambahClearing_Ket = "";
		$scope.TambahClearing_NoRef = "";
		$scope.TambahClearing_Nominal = "";
		$scope.TambahClearing_NoSO = "";
		$scope.TambahClearing_NominalSO = "";
		$scope.TambahClearing_NominalInternal = "";
		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarGLUntukClearing_UIGrid.totalItems = 0;
		$scope.TambahClearing_DaftarAP_Search_Text = "";
		$scope.TambahClearing_DaftarAP_SearchDropdown = undefined;
		$scope.TambahClearing_DaftarAP_UIGrid.data = [];
		$scope.TambahClearing_DaftarAP_UIGrid.totalItems = 0;
		$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarAPUntukClearing_UIGrid.totalItems = 0;
		$scope.TambahClearing_TipeBiaya_Search = undefined;
		$scope.TambahClearing_BiayaDK_Search = undefined;
		$scope.TambahClearing_BiayaNominal = "";
		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.data = [];
		$scope.TambahClearing_DaftarBiayaUntukClearing_UIGrid.totalItems = 0;
	}
	/*===============================Bulk Action Section, Begin===============================*/
	$scope.LihatClearing_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");
		if ($scope.LihatClearing_DaftarBulkAction_Search == "Setuju") {
			angular.forEach($scope.LihatClearing_DaftarClearing_gridAPI.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakClearing_ClearingId = value.ClearingId;
				$scope.ModalTolakClearing_TipePengajuan = value.TipePengajuan;
				console.log(value.StatusPengajuan);
				if (value.StatusPengajuan == "Diajukan") {
					$scope.CreateApprovalData("Disetujui");
					clearingFactory.updateApprovalData($scope.ApprovalData)
						.then(
							function (res) {
								//alert("Berhasil Disetujui");
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Berhasil Disetujui.",
										type: 'success'
									}
								);
								$scope.Batal_LihatClearing_TolakModal();
								$scope.GetListClearing(1, $scope.uiGridPageSize);
							},

							function (err) {
								//alert("Persetujuan gagal");
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
						);
				}
			});
		}
		else if ($scope.LihatClearing_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatClearing_DaftarClearing_gridAPI);
			angular.forEach($scope.LihatClearing_DaftarClearing_gridAPI.selection.getSelectedRows(), function (value, key) {
				console.log(value);
				$scope.ModalTolakClearing_ClearingId = value.ClearingId;
				$scope.ModalTolakClearing_TipePengajuan = value.TipePengajuan;
				$scope.ModalTolakClearing_TanggalClearing = $filter('date')(new Date(value.TanggalClearing), 'dd/MM/yyyy');
				$scope.ModalTolakClearing_NoClearing = value.NoClearing;
				$scope.ModalTolakClearing_TanggalPengajuan = $filter('date')(new Date(value.TglPengajuan), 'dd/MM/yyyy');
				$scope.ModalTolakClearing_AlasanPengajuan = value.AlasanPengajuan;
				if (value.StatusPengajuan == "Diajukan")
					angular.element('#ModalTolakClearing').modal('show');
			}
			);
		}
	}

	$scope.CreateApprovalData = function (ApprovalStatus) {
		if ($scope.ModalTolakClearing_TipePengajuan == "Reversal") {
			$scope.ModalTolakClearing_TipePengajuan = "Clearing Reversal";
		}

		$scope.ApprovalData =
			[
				{
					"ApprovalTypeName": $scope.ModalTolakClearing_TipePengajuan,
					"RejectedReason": $scope.ModalTolakClearing_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakClearing_ClearingId,
					"ApprovalStatusName": ApprovalStatus
				}
			];
	}

	$scope.Batal_LihatClearing_TolakModal = function () {
		angular.element('#ModalTolakClearing').modal('hide');
		$scope.LihatClearing_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListClearing(1, $scope.uiGridPageSize);
	}

	$scope.Pengajuan_LihatClearing_TolakModal = function () {
		$scope.CreateApprovalData("Ditolak");
		clearingFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function (res) {
					//alert("Berhasil tolak");
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Berhasil tolak.",
							type: 'success'
						}
					);
					$scope.Batal_LihatClearing_TolakModal();
					$scope.GetListClearing(1, $scope.uiGridPageSize);
				},

				function (err) {
					//alert("Penolakan gagal");
					bsNotify.show(
						{
							title: "Gagal",
							content: "Penolakan gagal.",
							type: 'danger'
						}
					);
				}
			);
	}

	$scope.ClearActionFields = function () {
		$scope.ModalTolakClearing_TanggalClearing = "";
		$scope.ModalTolakClearing_NoClearing = "";
		$scope.ModalTolakClearing_TanggalPengajuan = "";
		$scope.ModalTolakClearing_AlasanPengajuan = "";
		$scope.ModalTolakClearing_AlasanPenolakan = "";
	}
	/*===============================Bulk Action Section, End===============================*/


});
