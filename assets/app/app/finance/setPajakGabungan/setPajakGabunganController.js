var app = angular.module('app');
app.controller('SetPajakGabunganController', function ($scope, $http, uiGridConstants, CurrentUser, SetPajakGabunganFactory, $timeout, bsNotify) {
	var user = CurrentUser.user();

	if (user.OrgCode.substring(3, 8) == '000000')
		$scope.SetPajakGabungan_HO_Show = false;
	else
		$scope.SetPajakGabungan_HO_Show = false;

	$scope.SetPajakGabungan_getDataBranch = function () {
		SetPajakGabunganFactory.getDataBranchComboBox()
			.then
			(
			function (res) {
				$scope.optionsSetPajakGabungan_SelectBranch = res.data;
				$scope.SetPajakGabungan_SelectBranch = user.OutletId;

				res.data.some(function (obj, i) {
					if (obj.value == user.OutletId)
						$scope.SetPajakGabungan_NamaBranch = obj.name;
				});
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	};

	$scope.SetPajakGabungan_getDataBranch();

	$scope.SetPajakGabungan_Cari_Clicked = function () {
		SetPajakGabunganFactory.getCustomerData($scope.SetPajakGabungan_NamaPelanggan, $scope.SetPajakGabungan_SelectBranch)
			.then(
				function (res) {

					$scope.PajakGabunganPelangganGrid.data = res.data.Result;
					if (res.data.Result.length > 0)
						console.log("sample", res.data.Result[0]);
				}
			);

		angular.element('#ModalSearchSetPajakGabunganPelanggan').modal('setting', { closeable: false }).modal('show');
	}
	$scope.ModalSearchSetPajakGabunganPelanggan_Batal = function () {
		angular.element('#ModalSearchSetPajakGabunganPelanggan').modal('hide');
	}

	$scope.SearchSetPajakGabunganPelangganPilihRow = function (row) {
		//RegisterCheque_Data.Name
		$scope.SetPajakGabungan_NamaPelanggan = row.entity.CustomerName;
		$scope.SetPajakGabungan_CustomerId = row.entity.CustomerId;
		$scope.SetPajakGabungan_NomorPelanggan = row.entity.CustomerCode;
		$scope.SetPajakGabungan_NPWP = row.entity.NPWP;
		$scope.SetPajakGabungan_Alamat = row.entity.Address;
		$scope.SetPajakGabungan_KabKota = row.entity.KabKota;
		$scope.SetPajakGabungan_Telepon = row.entity.Phone;
		$scope.SetPajakGabungan_Tergabung = row.entity.isJoinedTax;

		angular.element('#ModalSearchSetPajakGabunganPelanggan').modal('hide');
	}
	$scope.PajakGabunganPelangganGrid = {
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: false,
		enableSelectAll: false,
		enableFiltering: true,
		paginationPageSizes: [10, 20],
		columnDefs: [
			{ name: 'CustomerId', field: 'CustomerId', visible: false },
			{ name: 'CustomerCode', displayName: 'Kode Pelanggan', field: 'CustomerCode', enableFiltering: true },
			{ name: 'CustomerName', displayName: 'Nama Pelanggan', field: 'CustomerName', enableFiltering: true },
			{ name: 'NPWP', displayName: 'NPWP', field: 'NPWP', enableFiltering: true },
			{ name: 'Address', displayName: 'Alamat', field: 'Address', enableFiltering: true },
			{ name: 'KabKota', displayName: 'Kabupaten / Kota', field: 'KabKota', enableFiltering: true },
			{ name: 'Phone', displayName: 'Telepon', field: 'Phone', enableFiltering: true },
			{ name: 'isJoinedTax', field: 'isJoinedTax', visible: false },
			{
				name: 'Action', enableFiltering: false,
				cellTemplate: '<a ng-click="grid.appScope.SearchSetPajakGabunganPelangganPilihRow(row)">Pilih</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.grid2Api = gridApi;
		}
	};

	// $scope.SetPajakGabungan_Cari_Clicked = function(){
	// SetPajakGabunganFactory.getCustomerData($scope.SetPajakGabungan_NamaPelanggan)
	// .then(
	// function(res){
	// if(res.data.Result.length > 0){
	// $scope.SetPajakGabungan_CustomerId = res.data.Result[0].CustomerId;
	// $scope.SetPajakGabungan_NomorPelanggan = res.data.Result[0].CustomerCode;
	// $scope.SetPajakGabungan_NPWP = res.data.Result[0].NPWP;
	// $scope.SetPajakGabungan_Alamat = res.data.Result[0].Address;
	// $scope.SetPajakGabungan_KabKota = res.data.Result[0].KabKota;
	// $scope.SetPajakGabungan_Telepon = res.data.Result[0].Phone;
	// $scope.SetPajakGabungan_Tergabung = res.data.Result[0].isJoinedTax;
	// }
	// else{
	// alert("Data tidak ditemukan");
	// }
	// },
	// function(err){
	// console.log("Err GetCustomerData : ", err);
	// }
	// );
	// }

	$scope.LoadingSimpan = false;
	$scope.SetPajakGabungan_Simpan_Clicked = function () {
		$scope.LoadingSimpan = true;
		var inputData = [];
		if (!isNaN($scope.SetPajakGabungan_CustomerId) && $scope.SetPajakGabungan_CustomerId != "") {
			inputData.push({ "CustomerId": $scope.SetPajakGabungan_CustomerId, "isJoinedTax": $scope.SetPajakGabungan_Tergabung, "OutletId": $scope.SetPajakGabungan_SelectBranch });
			SetPajakGabunganFactory.createCustomerData(inputData)
				.then(
					function (res) {
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Data pelanggan telah di-update untuk Faktur Pajak.",
								type: 'success'
							}
						);
						$scope.LoadingSimpan = false;
						$scope.SetPajakGabungan_CustomerId = "";
						$scope.SetPajakGabungan_NomorPelanggan = "";
						$scope.SetPajakGabungan_NPWP = "";
						$scope.SetPajakGabungan_Alamat = "";
						$scope.SetPajakGabungan_KabKota = "";
						$scope.SetPajakGabungan_Telepon = "";
						$scope.SetPajakGabungan_Tergabung = "";
						$scope.SetPajakGabungan_NamaPelanggan = "";
					},
					function (err) {
						bsNotify.show(
							{
								title: "Gagal",
								content: "Gagal simpan data.",
								type: 'danger'
							}
						);
						$scope.LoadingSimpan = false;
						console.log("Error CreateCustomerData : ", err);
					}
				);
		}
		else {
			bsNotify.show(
				{
					title: "Gagal",
					content: "Tidak ada data yang disimpan.",
					type: 'danger'
				}
			);
			$scope.LoadingSimpan = false;
		}
	}
});