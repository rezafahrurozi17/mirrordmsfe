angular.module('app')
	.factory('SetPajakGabunganFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},
		
		getCustomerData: function(customerName,  OutletId){
			var url = '/api/fe/FinanceCustomer/GetDataByJoinedTax/?CustomerName=' + customerName + '&outletid=' + OutletId;
			console.log("url Customer Data : ", url);
			var res=$http.get(url);
			return res;
		},
		
		createCustomerData : function(inputData){
			var url = '/api/fe/FinanceCustomer/submitdatajoinedtax/';
			var param = JSON.stringify(inputData);
			console.log("object input customer joined tax ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});