angular.module('app')
	.factory('ParamKuitansiTTUSFactory', function ($http, CurrentUser) {
	return{
		getData: function(BJDBPHType){
			var url = '/api/fe/FinanceBJDBPH/getdata/?bjdbphtype=' + BJDBPHType;
			console.log("url FInance BJD BPH : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataNew: function(BJDBPHType){
			var url = '/api/fe/FinanceBJDBPH/getdatanew/?bjdbphtype=' + BJDBPHType;
			console.log("url FInance BJD BPH : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataBJDDetail: function(limit,BJDBPHType){
			var url = '/api/fe/FinanceBJDBPH/GetBJDDataDetail/?start=1&limit='+limit+'&bjdbphtype=' + BJDBPHType;
			console.log("url FInance BJD BPH : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/FinanceBJDBPH/create/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bjd ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		createKuitansiDetail : function(inputData){
			var url = '/api/fe/FinanceParamKuitansiTTUS/CreateDetailDataKuitansi/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bjd ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		createTTUSDetail : function(inputData){
			var url = '/api/fe/FinanceParamKuitansiTTUS/CreateDetailDataTTUS/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bph ", param);
			var res=$http.post(url, param);
			return res;
		},
	}
});