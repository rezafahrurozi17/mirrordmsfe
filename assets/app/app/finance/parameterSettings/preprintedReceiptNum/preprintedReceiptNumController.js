var app = angular.module('app');
app.controller('PreprintedReceiptNumController', function ($scope, $http, $filter, CurrentUser, PreprintedReceiptNumFactory, $timeout, bsNotify, $q) {
	var user = CurrentUser.user();
	var outletID = user.OutletId;

	//general
	var paginationOptions = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log('eaeaea ' + $scope.ByteEnable);
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	if ($scope.allowApprove == true)
		$scope.optionsPreprintedReceiptNum_DaftarBulkAction_Search = [
			{ name: "Tersedia", value: "Tersedia" },
			{ name: "Tidak Tersedia", value: "Tidak Tersedia" },
			{ name: "Hilang", value: "Hilang" },
			{ name: "Rusak", value: "Rusak" },
			{ name: "Hapus", value: "Hapus" },
			{ name: "Koreksi", value: "Koreksi" },
			{ name: "Setuju", value: "Setuju" },
			{ name: "Tolak", value: "Tolak" }];
	else
		$scope.optionsPreprintedReceiptNum_DaftarBulkAction_Search = [
			{ name: "Tidak Tersedia", value: "Tidak Tersedia" },
			{ name: "Hilang", value: "Hilang" },
			{ name: "Rusak", value: "Rusak" },
			{ name: "Hapus", value: "Hapus" },
			{ name: "Koreksi", value: "Koreksi" }];

	$scope.uiGridPageSize = 25;
	$scope.uiGridTotalData = 0;
	$scope.Main_PreprintedReceiptNum_Show = true;
	$scope.rowIndexToBeRemoved = undefined;
	$scope.insertStatus = 0;
	$scope.PRN_StatusId == undefined;
	$scope.currentIPParentId = 0;

	$scope.PRN_Main_Add_Button_Clicked = function () {
		$scope.Main_PreprintedReceiptNum_Show = false;
		$scope.Add_PreprintedReceiptNum_Show = true;
		$scope.PRN_StartNumberPRN_EndNumber = false;

	}

	$scope.PRN_Main_Delete_Button_Clicked = function (row) {
		console.log("delete row:", row);
		var index = $scope.PRN_Main_UIGrid.data.indexOf(row.entity);
		var status = $scope.PRN_Main_UIGrid.data[index].Status.toLowerCase();
		$scope.rowIndexToBeRemoved = index;

		if (status == "tersedia" || status == "tidak tersedia" || status == "hilang") {
			$scope.DeletedPreprintedNumber = row.entity.PreprintedNumber;
			angular.element('#PRN_DeleteNumberQuestion_Modal').modal('show');
		}
		else {
			//alert("Hanya nomor dengan status \"Tersedia\" yang dapat dihapus");
			bsNotify.show(
				{
					title: "Warning",
					content: "Hanya nomor dengan status \"Tersedia\", \"Tidak Tersedia\", atau \"Hilang\" yang dapat dihapus",
					type: 'warning'
				}
			);
		}
	}

	$scope.PRN_Main_No_Delete_Button_Clicked = function () {
		$scope.rowIndexToBeRemoved = undefined;
		angular.element('#PRN_DeleteNumberQuestion_Modal').modal('hide');
	}

	$scope.PRN_Main_Yes_Delete_Button_Clicked = function () {
		PreprintedReceiptNumFactory.deleteData_PreprintedReceiptNum($scope.DeletedPreprintedNumber)
			.then(
				function (res) {
					//$scope.PRN_Main_UIGrid.data.splice($scope.rowIndexToBeRemoved, 1);
					//$scope.uiGridTotalData -= 1;
					//$scope.PRN_Main_UIGrid.totalItems = $scope.uiGridTotalData;
					$scope.PRN_Main_UIGrid_Paging(1);
					angular.element('#PRN_DeleteNumberQuestion_Modal').modal('hide');
					bsNotify.show(
						{
							title: "Berhasil",
							content: "Berhasil menghapus data Nomer Kuitansi Preprinted : " + $scope.DeletedPreprintedNumber,
							type: 'success'
						}
					);
					$scope.DeletedPreprintedNumber = '';
				},
				function (err) {
					angular.element('#PRN_DeleteNumberQuestion_Modal').modal('hide');
				}
			);

		$scope.rowIndexToBeRemoved = undefined;
	}

	$scope.PRN_Add_Back_Button_Clicked = function () {
		$scope.Main_PreprintedReceiptNum_Show = true;
		$scope.Add_PreprintedReceiptNum_Show = false;

		//reset value
		$scope.PRN_StartCode = "";
		$scope.PRN_StartNumber = "";
		$scope.PRN_EndNumber = "";
	}

	$scope.LoadingSimpan = false;
	$scope.PRN_Add_Save_Button_Clicked = function () {
		$scope.LoadingSimpan = true;

		var inputData = [{
			"OutletID": outletID,
			"StartCode": $scope.PRN_StartCode,
			"StartNumber": parseInt($scope.PRN_StartNumber, 10),
			"EndNumber": parseInt($scope.PRN_EndNumber, 10)
		}];

		if ($scope.PRN_StartNumber.length == 1) {
			$scope.New_PRN_StartNumber = '000000' + $scope.PRN_StartNumber;
		}
		else if ($scope.PRN_StartNumber.length == 2) {
			$scope.New_PRN_StartNumber = '00000' + $scope.PRN_StartNumber;
		}
		else if ($scope.PRN_StartNumber.length == 3) {
			$scope.New_PRN_StartNumber = '0000' + $scope.PRN_StartNumber;
		}
		else if ($scope.PRN_StartNumber.length == 4) {
			$scope.New_PRN_StartNumber = '000' + $scope.PRN_StartNumber;
		}
		else if ($scope.PRN_StartNumber.length == 5) {
			$scope.New_PRN_StartNumber = '00' + $scope.PRN_StartNumber;
		}
		else if ($scope.PRN_StartNumber.length == 6) {
			$scope.New_PRN_StartNumber = '0' + $scope.PRN_StartNumber;
		}
		else {
			$scope.New_PRN_StartNumber = $scope.PRN_StartNumber;
		}
		/* New blablabla */

		if ($scope.PRN_EndNumber.length == 1) {
			$scope.New_PRN_EndNumber = '000000' + $scope.PRN_EndNumber;
		}
		else if ($scope.PRN_EndNumber.length == 2) {
			$scope.New_PRN_EndNumber = '00000' + $scope.PRN_EndNumber;
		}
		else if ($scope.PRN_EndNumber.length == 3) {
			$scope.New_PRN_EndNumber = '0000' + $scope.PRN_EndNumber;
		}
		else if ($scope.PRN_EndNumber.length == 4) {
			$scope.New_PRN_EndNumber = '000' + $scope.PRN_EndNumber;
		}
		else if ($scope.PRN_EndNumber.length == 5) {
			$scope.New_PRN_EndNumber = '00' + $scope.PRN_EndNumber;
		}
		else if ($scope.PRN_EndNumber.length == 6) {
			$scope.New_PRN_EndNumber = '0' + $scope.PRN_EndNumber;
		}
		else {
			$scope.New_PRN_EndNumber = $scope.PRN_EndNumber;
		}


		if ($scope.New_PRN_StartNumber > $scope.New_PRN_EndNumber) {
			console.log('Benar', $scope.PRN_StartNumber, ' >', $scope.PRN_EndNumber);
			bsNotify.show(
				{
					title: "Warning",
					content: "Nomor awal harus bernilai lebih kecil atau sama dengan nomor akhir!",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
		}
		else {
			PreprintedReceiptNumFactory.saveData_PreprintedReceiptNum(inputData)
				.then(
					function (res) {
						angular.forEach(res.data.Result, function (value, key) {
							$scope.insertStatus = value.Status;
						});

						if ($scope.insertStatus == 1) {
							//success insert new data
							$scope.Main_PreprintedReceiptNum_Show = true;
							$scope.Add_PreprintedReceiptNum_Show = false;
							$scope.Edit_PreprintedReceiptNum_Show = false;

							//reset value
							$scope.PRN_StartCode = "";
							$scope.PRN_StartNumber = "";
							$scope.PRN_EndNumber = "";

							$scope.PRN_Main_UIGrid_Paging(1);
							//alert("Data berhasil disimpan");
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Berhasil menambah data Nomer Kuitansi Preprinted",
									type: 'success'
								}
							);
							$scope.LoadingSimpan = false;

						}
						else {
							//failed insert new data
							//alert("Kombinasi nomor sudah ada. Cari kombinasi lain!");
							bsNotify.show(
								{
									title: "Warning",
									content: "Kombinasi nomor sudah ada. Cari kombinasi lain",
									type: 'warning'
								}
							);
							$scope.LoadingSimpan = false;
						}

					}
				);
		}

	}

	$scope.PRN_Main_Edit_Button_Clicked = function (row) {
		var index = $scope.PRN_Main_UIGrid.data.indexOf(row.entity);
		var status = $scope.PRN_Main_UIGrid.data[index].Status.toLowerCase();
		var PNumber = $scope.PRN_Main_UIGrid.data[index].PreprintedNumber;
		var PNumberId = $scope.PRN_Main_UIGrid.data[index].PreprintedNumberId;
		console.log("PreprintedNumberId:", PNumberId);
		$scope.PRN_Status = status;
		$scope.SelectedRow = row.entity;
		$scope.rowIndexToBeRemoved = index;

		if (status == "tersedia" || status == "tidak tersedia" || status == "hilang" || status == "terpakai") {
			$scope.PreprintedReceiptNum_EditStatus_Search = $scope.PRN_Main_UIGrid.data[index].Status;
			$scope.PreprintedNumber = PNumber;
			$scope.PreprintedNumberId = PNumberId;
			// $scope.PreprintedStatus = "";
			$scope.Main_PreprintedReceiptNum_Show = false;
			$scope.Edit_PreprintedReceiptNum_Show = true;
		}
		else {
			bsNotify.show(
				{
					title: "Warning",
					content: "Hanya nomor dengan status \"Tersedia\", \"Tidak Tersedia\", \"Hilang\" atau \"Terpakai\" yang dapat diubah",
					type: 'warning'
				}
			);
			$scope.rowIndexToBeRemoved = undefined;
		}
	}

	$scope.PRN_Edit_Back_Button_Clicked = function () {
		$scope.Main_PreprintedReceiptNum_Show = true;
		$scope.Edit_PreprintedReceiptNum_Show = false;
		$scope.rowIndexToBeRemoved = undefined;

		//reset value
		// $scope.PreprintedNumber = "";
		// $scope.PreprintedStatus = "";
	}

	$scope.PRN_Edit_Save_Button_Clicked = function () {
		$scope.LoadingSimpan = true;
		// var index = $scope.PRN_Main_UIGrid.data.indexOf(row.entity);
		// var index = $scope.rowIndexToBeRemoved

		var status = $scope.PRN_Status.toLowerCase();
		var approvalstatus = $scope.SelectedRow.ApprovalStatus;

		if ($scope.PRN_StatusId == undefined || $scope.PreprintedReceiptNum_EditStatus_Search == undefined) {

			bsNotify.show(
				{
					title: "Warning",
					content: "Mohon pilih Status",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
		}
		else if (approvalstatus == 'Diajukan') {
			bsNotify.show(
				{
					title: "Warning",
					content: "Approval Status Kuitansi Preprinted masih Diajukan",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
			$scope.Main_PreprintedReceiptNum_Show = true;
			$scope.Edit_PreprintedReceiptNum_Show = false;

			$scope.rowIndexToBeRemoved = undefined;
		}
		else if ($scope.PreprintedReceiptNum_EditStatus_Search.toLowerCase() == status) {
			$scope.Main_PreprintedReceiptNum_Show = true;
			$scope.Edit_PreprintedReceiptNum_Show = false;
			//reset value
			// $scope.PRN_StartCode = "";
			// $scope.PRN_StartNumber = "";
			// $scope.PRN_EndNumber = "";

			$scope.PRN_Main_UIGrid_Paging(1);
			bsNotify.show(
				{
					title: "Berhasil",
					content: "Status Kuitansi Preprinted tidak berubah",
					type: 'success'
				}
			);
			$scope.LoadingSimpan = false;
			$scope.rowIndexToBeRemoved = undefined;
		}
		else if (status == "terpakai" && $scope.PreprintedReceiptNum_EditStatus_Search.toLowerCase() != "koreksi") {
			bsNotify.show(
				{
					title: "Warning",
					content: "Status nota terpakai hanya boleh diubah menjadi koreksi",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
		}
		else {
			// // Update number.
			// var inputData = [{
			// 	// "OutletID": outletID,
			// 	"NumberRelation": null,
			// 	"PreprintedNumberId": $scope.SelectedRow.PreprintedNumberId,
			// 	"ReceiptTypeId": 0,
			// 	// "PICId": 'NULL',
			// 	"StatusId": $scope.PRN_StatusId
			// }];

			var inputData = [{
				"NumberRelation": null,
				"PreprintedNumberId": $scope.SelectedRow.PreprintedNumberId,
				"ReceiptTypeId": 0,
				"RequestStatus": $scope.PRN_StatusId,
				"ApprovalStatus": 0,
				"RejectedReason": ""
			}];

			PreprintedReceiptNumFactory.updateData_PreprintedReceiptNum(inputData)
				.then(
					function (res) {
						angular.forEach(res.data.Result, function (value, key) {
							$scope.insertStatus = value.Status;
						});
						console.log("PreprintedNumberId:", $scope.SelectedRow.PreprintedNumberId);

						$scope.Main_PreprintedReceiptNum_Show = true;
						$scope.Edit_PreprintedReceiptNum_Show = false;

						$scope.PRN_Main_UIGrid_Paging(1);

						bsNotify.show(
							{
								title: "Berhasil",
								content: "Berhasil mengubah status Nomor Kuitansi Preprinted",
								type: 'success'
							}
						);
						$scope.LoadingSimpan = false;
						$scope.PRN_StatusId = undefined;
					},
					function (err) {
						if(err != null){
							bsNotify.show({
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							});
							$scope.LoadingSimpan = false;
						}
					}
				);
			$scope.rowIndexToBeRemoved = undefined;
		}
	}

	//PREPRINTED RECEIPT NUMBER(UI GRID)
	$scope.PRN_Main_UIGrid = {
		// paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		rowSelection: true,
		multiSelect: true,
		columnDefs: [
			{ name: "Nomor Kuitansi Preprinted Id", field: "PreprintedNumberId", visible: false },
			// { name: "Number Relation", field: "NumberRelation", visible: false },
			{ name: "Nomor Kuitansi Preprinted", field: "PreprintedNumber", enableCellEdit: false },
			{ name: "Status", field: "Status", enableCellEdit: false },
			{ name: "Request Status", field: "RequestStatus", enableCellEdit: false },
			{ name: "Approval Status", field: "ApprovalStatus", enableCellEdit: false },
			{ name: "Alasan Penolakan", field: "RejectedReason", enableCellEdit: false },
			{
				name: "Action",
				// cellTemplate: '<a ng-click="grid.appScope.PRN_Main_Delete_Button_Clicked(row)"> Hapus</a>'
				cellTemplate: '<a> </a> <a ng-click="grid.appScope.PRN_Main_Edit_Button_Clicked(row)">Edit</a> <a ng-click="grid.appScope.PRN_Main_Delete_Button_Clicked(row)">Hapus</a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.PRN_Main_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIPParentId = 0;
				}
				else {
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions.pageNumber = pageNumber;
				paginationOptions.pageSize = pageSize;
				$scope.PRN_Main_UIGrid_Paging(pageNumber);
			});
		}
	};

	$scope.PRN_Main_UIGrid_Paging = function (pageNumber) {
		PreprintedReceiptNumFactory.getTotalData_PreprintedReceiptNum($scope.KuintansiPreprinted_SelectFilter, $scope.FilterStatusPreprinted_SelectKolom,
			$scope.FilterApprovalPreprinted_SelectKolom)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						angular.forEach(res.data.Result, function (value, key) {
							$scope.uiGridTotalData = value.TotalData;
						});
					}

					PreprintedReceiptNumFactory.getData_FilterPreprintedReceiptNum(pageNumber, paginationOptions.pageSize, $scope.KuintansiPreprinted_SelectFilter, $scope.FilterStatusPreprinted_SelectKolom,
						$scope.FilterApprovalPreprinted_SelectKolom)
						.then(
							function (res) {
								if (!angular.isUndefined(res.data.Result)) {
									$scope.PRN_Main_UIGrid.data = res.data.Result;
									$scope.PRN_Main_UIGrid.totalItems = ($scope.uiGridTotalData < res.data.Total) ? res.data.Total : $scope.uiGridTotalData;
									$scope.PRN_Main_UIGrid.paginationPageSize = paginationOptions.pageSize;
									// $scope.PRN_Main_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
									$scope.PRN_Main_UIGrid.paginationPageSizes = [10, 25, 50];
									$scope.PRN_Main_UIGrid.paginationCurrentPage = pageNumber;
								}
							}
						);
				}
			);
	}

	$scope.PRN_Main_UIGrid_Paging(1);

	$scope.PreprintedReceiptNum_DaftarBulkAction_Changed = function () {
		var lanjut = true;
		rows = $scope.PRN_Main_gridAPI.selection.getSelectedRows();
		if (rows.length == 0 && $scope.PreprintedReceiptNum_DaftarBulkAction_Search != undefined) {
			bsNotify.show(
				{
					title: "Warning",
					content: "Mohon pilih Nomor Kuitansi Preprinted terlebih dahulu.",
					type: 'warning'
				}
			);
			$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
		}
		else {
			if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tersedia"
				|| $scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tidak Tersedia"
				|| $scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Hilang"
				|| $scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Rusak") {

				angular.forEach(rows, function (value) {
					if (value.Status != "Tersedia" && value.Status != "Tidak Tersedia") {
						lanjut = false;
					}

					if (value.ApprovalStatus == "Diajukan") {
						lanjut = false;
					}
				});

				if (lanjut) {
					$scope.ModalPRNEditStatus = $scope.PreprintedReceiptNum_DaftarBulkAction_Search;
					angular.element('#PRN_BulkActionEditSatus_Modal').modal('show');
				}
				else {
					bsNotify.show(
						{
							title: "Warning",
							content: "Perubahan status hanya untuk Tersedia dan Tidak Tersedia dan approval status tidak Diajukan",
							type: 'warning'
						}
					);
					$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
				}
			}
			if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Koreksi") 
			{
				console.log('bulk koreksi');
				angular.forEach(rows, function (value) {
					if (value.Status != "Tersedia" && value.Status != "Tidak Tersedia") {
						console.log('masuk if');
						// pengecualian koreksi, untuk receipt status terpakai 
						if(value.Status == "Terpakai") { 
							lanjut = true;
						} else {
							lanjut = false;
						}
					}

					if (value.ApprovalStatus == "Diajukan") {
						lanjut = false;
					}
				});

				if (lanjut) {
					$scope.ModalPRNEditStatus = $scope.PreprintedReceiptNum_DaftarBulkAction_Search;
					angular.element('#PRN_BulkActionEditSatus_Modal').modal('show');
				}
				else {
					bsNotify.show(
						{
							title: "Warning",
							// content: "Perubahan status hanya untuk Tersedia dan Tidak Tersedia dan approval status tidak Diajukan",
							content: "Perubahan status hanya untuk status Terpakai",
							type: 'warning'
						}
					);
					$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
				}
			}
			else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Hapus") {
				angular.forEach(rows, function (value) {
					if (value.Status != "Tersedia" && value.Status != "Tidak Tersedia") {
						lanjut = false;
					}

					if (value.ApprovalStatus == "Diajukan") {
						lanjut = false;
					}
				});

				if (lanjut) {
					$scope.ModalPRNEditStatus = $scope.PreprintedReceiptNum_DaftarBulkAction_Search;
					angular.element('#PRN_BulkActionHapus_Modal').modal('show');
				}
				else {
					bsNotify.show(
						{
							title: "Warning",
							content: "Perubahan status hanya untuk Tersedia dan Tidak Tersedia dan approval status tidak Diajukan",
							type: 'warning'
						}
					);
					$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
				}
			}
			else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Setuju") {
				angular.forEach(rows, function (value) {
					if (value.ApprovalStatus != "Diajukan") {
						lanjut = false;
					}
				});

				if (lanjut) {
					angular.element('#PRN_BulkActionSetuju_Modal').modal('show');
				}
				else {
					bsNotify.show(
						{
							title: "Warning",
							content: "Persetujuan hanya untuk status approval Diajukan",
							type: 'warning'
						}
					);
					$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
				}
			}
			else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tolak") {
				angular.forEach(rows, function (value) {
					if (value.ApprovalStatus != "Diajukan") {
						lanjut = false;
					}
				});

				if (lanjut) {
					angular.element('#PRN_BulkActionTolak_Modal').modal('show');
				}
				else {
					bsNotify.show(
						{
							title: "Warning",
							content: "Persetujuan hanya untuk status Diajukan",
							type: 'warning'
						}
					);
					$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
				}
			}
		}

	}

	$scope.Batal_ModalPRN_Setuju = function () {
		angular.element('#PRN_BulkActionSetuju_Modal').modal('hide');
	}

	$scope.Setuju_ModalPRN_Setuju = function () {
		angular.forEach($scope.PRN_Main_gridAPI.selection.getSelectedRows(), function (value) {
			var inputData = [{
				"NumberRelation": null,
				"PreprintedNumberId": value.PreprintedNumberId,
				"ReceiptTypeId": 0,
				"RequestStatus": 0,
				"ApprovalStatus": 1,
				"RejectedReason": ""
			}];

			PreprintedReceiptNumFactory.updateData_PreprintedReceiptNum(inputData)
				.then(
					function (res) {
						console.log("PreprintedNumberId:", value.PreprintedNumberId);
						$scope.PRN_Main_UIGrid_Paging(1);
						$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Berhasil disetujui",
								type: 'success'
							}
						);
						angular.element('#PRN_BulkActionSetuju_Modal').modal('hide');
					}
				);
		});
	}

	$scope.Batal_ModalPRN_Tolak = function () {
		angular.element('#PRN_BulkActionTolak_Modal').modal('hide');
	}

	$scope.Tolak_ModalPRN_Tolak = function () {
		if (angular.isUndefined($scope.Tolak_ModalPRN_AlasanPenolakan) || $scope.Tolak_ModalPRN_AlasanPenolakan == '') {
			bsNotify.show(
				{
					title: "Warning",
					content: "Alasan Penolakan harus diisi",
					type: 'warning'
				}
			);
			return false;
		}

		angular.forEach($scope.PRN_Main_gridAPI.selection.getSelectedRows(), function (value) {
			var inputData = [{
				"NumberRelation": null,
				"PreprintedNumberId": value.PreprintedNumberId,
				"ReceiptTypeId": 0,
				"RequestStatus": 0,
				"ApprovalStatus": 2,
				"RejectedReason": $scope.Tolak_ModalPRN_AlasanPenolakan
			}];

			PreprintedReceiptNumFactory.updateData_PreprintedReceiptNum(inputData)
				.then(
					function (res) {
						console.log("PreprintedNumberId:", value.PreprintedNumberId);
						$scope.PRN_Main_UIGrid_Paging(1);
						$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Berhasil ditolak",
								type: 'success'
							}
						);
						angular.element('#PRN_BulkActionTolak_Modal').modal('hide');
					},

					function (err) {
						// alert("Persetujuan gagal");
						bsNotify.show({
							title: "Gagal",
							content: "Gagal tolak",
							type: 'danger'
						});
					}
				);
		});
	}

	$scope.optionsPreprintedReceiptNum_EditStatus_Search = [{ name: "Tersedia", value: "Tersedia" }, 
		{ name: "Tidak Tersedia", value: "Tidak Tersedia" }, 
		{ name: "Hilang", value: "Hilang" }, 
		{ name: "Rusak", value: "Rusak" },
		{ name: "Koreksi", value: "Koreksi" }
	];

	// Edit Menu
	$scope.PreprintedReceiptNum_EditStatus_Changed = function () {
		console.log("Change Status");
		console.log("before 686 ->", $scope.PRN_StatusId);
		if ($scope.PreprintedReceiptNum_EditStatus_Search == "Tersedia") {
			$scope.PRN_StatusId = 3751; // Tersedia
		}
		else if ($scope.PreprintedReceiptNum_EditStatus_Search == "Tidak Tersedia") {
			$scope.PRN_StatusId = 3754; // Tidak Tersedia
		}
		else if ($scope.PreprintedReceiptNum_EditStatus_Search == "Hilang") {
			$scope.PRN_StatusId = 3756; // Hilang
		}
		else if ($scope.PreprintedReceiptNum_EditStatus_Search == "Rusak") {
			$scope.PRN_StatusId = 3755; // Rusak
		}
		else if ($scope.PreprintedReceiptNum_EditStatus_Search == "Koreksi") {
			$scope.PRN_StatusId = 3758; // Koreksi
		}
		console.log("after 702 ->", $scope.PRN_StatusId);
	}

	// Bulk Action Modal
	$scope.Batal_ModalPRN_EditStatus = function () {
		$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
		angular.element('#PRN_BulkActionEditSatus_Modal').modal('hide');
	};

	$scope.Edit_ModalPRN_EditStatus = function () {
		console.log("status bulk action 712 ->", $scope.PreprintedReceiptNum_DaftarBulkAction_Search);
		if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tersedia") {
			$scope.PRN_StatusId = 3751; // Tersedia
		}
		else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tidak Tersedia") {
			$scope.PRN_StatusId = 3754; // Tidak Tersedia
		}
		else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Hilang") {
			$scope.PRN_StatusId = 3756; // Hilang
		}
		else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Rusak") {
			$scope.PRN_StatusId = 3755; // Rusak
		}
		else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Hapus") {
			$scope.PRN_StatusId = 3757; // Hapus
		}
		else if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Koreksi") {
			$scope.PRN_StatusId = 3758; // Koreksi
		}
		console.log("PRN_StatusId 731 ->", $scope.PRN_StatusId);


		var loopPromises = [];
		angular.forEach($scope.PRN_Main_gridAPI.selection.getSelectedRows(), function (value) {
			// $scope.SelectedRow.PreprintedNumberId = value.PreprintedNumberId;

			var deferred = $q.defer();
			loopPromises.push(deferred.promise);
			//sample of a long-running operation inside loop...
			setTimeout(function () {
				deferred.resolve();

				var inputData = [{
					"NumberRelation": null,
					"PreprintedNumberId": value.PreprintedNumberId,
					"ReceiptTypeId": 0,
					"RequestStatus": $scope.PRN_StatusId,
					"ApprovalStatus": 0,
					"RejectedReason": ""
				}];

				PreprintedReceiptNumFactory.updateData_PreprintedReceiptNum(inputData)
					.then(
						function (res) {
							console.log("PreprintedNumberId:", value.PreprintedNumberId);
							var msg = "";

							if ($scope.PreprintedReceiptNum_DaftarBulkAction_Search == "Tersedia")
								msg = " berhasil ubah status";
							else
								msg = " berhasil diajukan";

							$scope.PRN_Main_UIGrid_Paging(1);
							bsNotify.show({
								title: "Berhasil",
								content: value.PreprintedNumber + msg,
								type: 'success'
							});
						},

						function (err) {
							// alert("Persetujuan gagal");
							bsNotify.show({
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							});
						}
					);

				angular.element('#PRN_BulkActionEditSatus_Modal').modal('hide');
			}, 2000);
		});

		$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
	};

	$scope.Batal_ModalPRN_Hapus = function () {
		$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
		angular.element('#PRN_BulkActionHapus_Modal').modal('hide');
	}

	$scope.Hapus_ModalPRN_Hapus = function () {
		var loopPromises = [];
		angular.forEach($scope.PRN_Main_gridAPI.selection.getSelectedRows(), function (value) {

			var deferred = $q.defer();
			loopPromises.push(deferred.promise);
			//sample of a long-running operation inside loop...
			setTimeout(function () {
				deferred.resolve();

				var inputData = [{
					"NumberRelation": null,
					"PreprintedNumberId": value.PreprintedNumberId,
					"ReceiptTypeId": 0,
					"RequestStatus": 3757,
					"ApprovalStatus": 0,
					"RejectedReason": ""
				}];

				PreprintedReceiptNumFactory.updateData_PreprintedReceiptNum(inputData)
					.then(
						function (res) {
							$scope.PRN_Main_UIGrid_Paging(1);
							bsNotify.show({
								title: "Berhasil",
								content: value.PreprintedNumber + " pengajuan hapus",
								type: 'success'
							});
						},

						function (err) {
							// alert("Persetujuan gagal");
							bsNotify.show({
								title: "Gagal",
								content: value.PreprintedNumber + " gagal hapus",
								type: 'danger'
							});
						}
					);
			}, 2000);
		});
		$scope.PreprintedReceiptNum_DaftarBulkAction_Search = undefined;
	}

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	/*======================Filter | Irfan | Begin | 20190624========================*/
	$scope.optionsFilterStatusPreprinted_SelectKolom = [{ name: "Tersedia", value: "Tersedia" }, { name: "Tidak Tersedia", value: "Tidak Tersedia" },
	{ name: "Hilang", value: "Hilang" }, { name: "Rusak", value: "Rusak" }, { name: "Terpakai", value: "Terpakai" }, { name: "Koreksi", value: "Koreksi" }];
	$scope.optionsFilterApprovalPreprinted_SelectKolom = [{ name: "Diajukan", value: "Diajukan" }, { name: "Disetujui", value: "Disetujui" }];

	$scope.KuintansiPreprinted_SelectFilter_Clicked = function () {
		$scope.getData_FilterPreprintedReceiptNum(1, 10,
			$scope.KuintansiPreprinted_SelectFilter,
			$scope.FilterStatusPreprinted_SelectKolom,
			$scope.FilterApprovalPreprinted_SelectKolom
		);

	}

	// var paginationOptions_Printed = {
	// 	pageNumber: 1,
	// 	pageSize: 10,
	// 	sort: null
	// }

	$scope.getData_FilterPreprintedReceiptNum = function (pageNumber) {
		console.log("ouletid :", $scope.PajakKeluaranMain_SelectBranch);
		PreprintedReceiptNumFactory.getTotalData_PreprintedReceiptNum($scope.KuintansiPreprinted_SelectFilter, $scope.FilterStatusPreprinted_SelectKolom,
			$scope.FilterApprovalPreprinted_SelectKolom)
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						angular.forEach(res.data.Result, function (value, key) {
							$scope.uiGridTotalData = value.TotalData;
						});
					}
					PreprintedReceiptNumFactory.getData_FilterPreprintedReceiptNum(pageNumber, paginationOptions.pageSize, $scope.KuintansiPreprinted_SelectFilter, $scope.FilterStatusPreprinted_SelectKolom,
						$scope.FilterApprovalPreprinted_SelectKolom)
						.then(
							function (res) {
								console.log(res);
								$scope.PRN_Main_UIGrid.data = res.data.Result;
								$scope.PRN_Main_UIGrid.totalItems = ($scope.uiGridTotalData < res.data.Total) ? res.data.Total : $scope.uiGridTotalData;
								$scope.PRN_Main_UIGrid.paginationPageSize = paginationOptions.pageSize;
								$scope.PRN_Main_UIGrid.paginationPageSizes = [10, 25, 50];
								$scope.PRN_Main_UIGrid.paginationCurrentPage = pageNumber;
							},
							function (err) {
								console.log("err=>", err);
							}
						);
				}
			);
	}
	// 	var gridapi = $scope.PRN_Main_gridAPI;
	// 	if ($scope.FilterKuintansiPreprinted_SelectKolom == null || $scope.FilterKuintansiPreprinted_SelectKolom == '')
	// 		gridapi.grid.clearAllFilters();
	// 	else
	// 		gridapi.grid.clearAllFilters();

	// 	if ($scope.optionsFilterApprovalPreprinted_SelectKolom == "Diajukan") {
	// 		nomor = 2;
	// 	}
	// 	else if ($scope.optionsFilterApprovalPreprinted_SelectKolom == "Disetujui") {
	// 		nomor = 3;
	// 	}

	// 	else {
	// 		nomor = 0;
	// 	}

	// 	console.log("nomor dan filter", nomor, $scope.KuintansiPreprinted_SelectFilter);

	// 	if (!angular.isUndefined($scope.KuintansiPreprinted_SelectFilter) && nomor != 0) {
	// 		console.log("masuk if");
	// 		gridapi.grid.columns[nomor].filters[0].term = $scope.KuintansiPreprinted_SelectFilter;
	// 	}
	// 	gridapi.grid.queueGridRefresh();
	// }

	/*======================Filter | Irfan | End | 20190624========================*/
});
