angular.module('app')
	.factory('PreprintedReceiptNumFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
		var outletID = user.OutletId;
		return {

			getTotalData_PreprintedReceiptNum: function (Filter, Status, Approval) {
				var url = '/api/fe/FinancePreprintedReceiptNum/GetTotalData?outletId=' + outletID + '&filter=' + Filter + '&status=' + Status + '&approval=' + Approval;
				console.log("url GetTotalData : ", url);
				var res = $http.get(url);
				return res;
			},

			getData_FilterPreprintedReceiptNum: function (Start, Limit, Filter, Status, Approval) {
				var url = '/api/fe/FinancePreprintedReceiptNum/GetFilterData?outletId=' + outletID + '&start=' + Start + '&limit=' + Limit
					+ '&filter=' + Filter + '&status=' + Status + '&approval=' + Approval;
				console.log("url GetData : ", url);
				var res = $http.get(url);
				return res;
			},

			getData_PreprintedReceiptNum: function (Start, Limit) {
				var url = '/api/fe/FinancePreprintedReceiptNum/GetData?outletId=' + outletID + '&start=' + Start + '&limit=' + Limit;
				console.log("url GetData : ", url);
				var res = $http.get(url);
				return res;
			},

			saveData_PreprintedReceiptNum: function (InputData) {
				var url = '/api/fe/FinancePreprintedReceiptNum/PostSaveData';
				var param = JSON.stringify(InputData);
				console.log("object input Save Data", param);
				var res = $http.post(url, param);
				return res;
			},

			updateData_PreprintedReceiptNum: function (InputData) {
				var url = '/api/fe/FinancePreprintedReceiptNum/PostUpdateData';
				var param = JSON.stringify(InputData);
				console.log("object input Update Data", param);
				var res = $http.post(url, param);
				return res;
			},

			deleteData_PreprintedReceiptNum: function (PreprintedReceiptNum) {
				var url = '/api/fe/FinancePreprintedReceiptNum/GetDeleteData?outletId=' + outletID + '&preprintedNumber=' + PreprintedReceiptNum;
				console.log("url Delete Data : ", url);
				var response = $http.get(url);
				return response;
			},

			cancelData_PreprintedReceiptNum: function (PreprintedReceiptNum) {
				var url = '/api/fe/FinancePreprintedReceiptNum/GetCancelData?outletId=' + outletID + '&preprintedNumber=' + PreprintedReceiptNum;
				console.log("url Cancel Data : ", url);
				var response = $http.get(url);
				return response;
			}
		}
	});