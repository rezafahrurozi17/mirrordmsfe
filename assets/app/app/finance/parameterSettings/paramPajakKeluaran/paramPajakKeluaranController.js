var app = angular.module('app');
app.controller('ParamPajakKeluaranController', function ($scope, $http, $filter, CurrentUser, ParamPajakKeluaranFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.allowApprove = checkRbyte($scope.ByteEnable, 2);
	console.log($scope.allowApprove);
	if ($scope.allowApprove == false) {
		$scope.allowSimpan = false;
	}
	else {
		if ($scope.user.OrgCode.substring(3, 9) == '000000')
			$scope.allowSimpan = true;
		else
			$scope.allowSimpan = false;
	}

	$scope.getListData = function () {
		ParamPajakKeluaranFactory.getData()
			.then(
				function (res) {
					// $scope.PajakKeluaran_PPNParamId = res.data.Result[0].ParameterDetailId;
					// $scope.PajakKeluaran_PPN = res.data.Result[0].TaxOutPercentage;

					// $scope.PajakKeluaran_PPh22ParamId = res.data.Result[1].ParameterDetailId;
					// $scope.PajakKeluaran_PPh22 = res.data.Result[1].TaxOutPercentage;

					// $scope.PajakKeluaran_PPh23ParamId = res.data.Result[2].ParameterDetailId;
					// $scope.PajakKeluaran_PPh23 = res.data.Result[2].TaxOutPercentage;

					// $scope.PajakKeluaran_PPh22_WAPU_ParamId = res.data.Result[3].ParameterDetailId;
					// $scope.PajakKeluaran_PPh22_WAPU = res.data.Result[3].TaxOutPercentage;

					//begin | NOTO | 180608 | Bug list R14 | A93
					angular.forEach(res.data.Result, function (value, key) {
						if (res.data.Result[key].TaxOutType == "PPn") {
							$scope.PajakKeluaran_PPNParamId = res.data.Result[key].ParameterDetailId;
							$scope.PajakKeluaran_PPN = res.data.Result[key].TaxOutPercentage.toString().replace('.', ',');
						}
						else if (res.data.Result[key].TaxOutType == "PPh22" || res.data.Result[key].TaxOutType == "PPh22 Barang Sangat Mewah") {
							$scope.PajakKeluaran_PPh22ParamId = res.data.Result[key].ParameterDetailId;
							$scope.PajakKeluaran_PPh22 = res.data.Result[key].TaxOutPercentage.toString().replace('.', ',');
						}
						else if (res.data.Result[key].TaxOutType == "PPh23") {
							$scope.PajakKeluaran_PPh23ParamId = res.data.Result[key].ParameterDetailId;
							$scope.PajakKeluaran_PPh23 = res.data.Result[key].TaxOutPercentage.toString().replace('.', ',');
						}
						else if (res.data.Result[key].TaxOutType == "Prepaid PPh22 Wapu") {
							$scope.PajakKeluaran_PPh22_WAPU_ParamId = res.data.Result[key].ParameterDetailId;
							$scope.PajakKeluaran_PPh22_WAPU = res.data.Result[key].TaxOutPercentage.toString().replace('.', ',');
						}
					});
					//end | NOTO | 180608 | Bug list R14 | A93
				}
			);
	}

	$scope.getListData();

	$scope.PajakKeluaran_Simpan_Clicked = function () {
		if ($scope.allowSimpan) {
			if ($scope.PajakKeluaran_PPNParamId == undefined)
				$scope.PajakKeluaran_PPNParamId = -1;

			if ($scope.PajakKeluaran_PPh22ParamId == undefined)
				$scope.PajakKeluaran_PPh22ParamId = -2;

			if ($scope.PajakKeluaran_PPh23ParamId == undefined)
				$scope.PajakKeluaran_PPh23ParamId = -3;

			if ($scope.PajakKeluaran_PPh22_WAPU_ParamId == undefined)
				$scope.PajakKeluaran_PPh22_WAPU_ParamId = -4;

			if (angular.isDefined($scope.PajakKeluaran_PPN))
				$scope.PajakKeluaran_PPN = $scope.PajakKeluaran_PPN.toString().replace(',', '.');

			if (angular.isDefined($scope.PajakKeluaran_PPh22))
				$scope.PajakKeluaran_PPh22 = $scope.PajakKeluaran_PPh22.toString().replace(',', '.');

			if (angular.isDefined($scope.PajakKeluaran_PPh23))
				$scope.PajakKeluaran_PPh23 = $scope.PajakKeluaran_PPh23.toString().replace(',', '.');

			if (angular.isDefined($scope.PajakKeluaran_PPh22_WAPU))
				$scope.PajakKeluaran_PPh22_WAPU = $scope.PajakKeluaran_PPh22_WAPU.toString().replace(',', '.');

			var createData = [
				{ "ParameterDetailId": $scope.PajakKeluaran_PPNParamId, "TaxOutPercentage": $scope.PajakKeluaran_PPN },
				{ "ParameterDetailId": $scope.PajakKeluaran_PPh22ParamId, "TaxOutPercentage": $scope.PajakKeluaran_PPh22 },
				{ "ParameterDetailId": $scope.PajakKeluaran_PPh23ParamId, "TaxOutPercentage": $scope.PajakKeluaran_PPh23 },
				{ "ParameterDetailId": $scope.PajakKeluaran_PPh22_WAPU_ParamId, "TaxOutPercentage": $scope.PajakKeluaran_PPh22_WAPU }
			];

			ParamPajakKeluaranFactory.create(createData)
				.then(
					function (res) {
						//alert("Data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: "success"
						});
						$scope.getListData();
					},
					function (err) {
						console.log("Pajak Keluaran Error : ", err);
					}
				)
		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Anda tidak berhak",
				type: "warning"
			});
		}
	}

}).directive('validPercent', function () {
	return {
		restrict: 'A',
		link: function (scope, elm, attrs, ctrl) {
			elm.on('keydown', function (event) {
				var $input = $(this);
				var value = $input.val();
				value = value.replace(/[^0-9\.]/g, '')
				var findsDot = new RegExp(/\./g)
				var containsDot = value.match(findsDot)
				if (containsDot != null && ([46, 110, 190].indexOf(event.which) > -1)) {
					event.preventDefault();
					return false;
				}
				$input.val(value);
				if (event.which == 64 || event.which == 16) {
					// numbers  
					return false;
				} if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {
					// backspace, enter, escape, arrows  
					return true;
				} else if (event.which >= 48 && event.which <= 57) {
					// numbers  
					return true;
				} else if (event.which >= 96 && event.which <= 105) {
					// numpad number  
					return true;
				} else if ([46, 110, 190].indexOf(event.which) > -1) {
					// dot and numpad dot  
					return true;
				} else {
					event.preventDefault();
					return false;
				}
			});
		}
	}
});  