angular.module('app')
	.factory('ParamPajakKeluaranFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/financeParamTaxOut/getdata';
			console.log("url Pajak Keluaran : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/financeParamTaxOut/create/';
			var param = JSON.stringify(inputData);
			console.log("object input pajak keluaran ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});