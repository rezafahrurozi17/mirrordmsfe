angular.module('app')
	.factory('ParamBungaDFFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/financeDFInterest/getdata';
			console.log("url refund cg : ", url);
			var res=$http.get(url);
			return res;
		},
	
		//CR4 Phase2
		getDataBankComboBox : function () {
			var url = '/api/fe/AccountBank/GetDataBankComboBox/';
			var res = $http.get(url);
			return res;
		},

		getDataNomorRekeningComboBox : function(bankId) {
			var url = '/api/fe/VendorBankBungaDFAccount/SelectData/?BankId=' + bankId;
			var res=$http.get(url);  
			return res;			
		},

		getDataDetail : function(vendorBankId) {
			var url = '/api/fe/BankBungaDF/SelectData/?VendorBankId=' + vendorBankId;
			var res=$http.get(url);  
			return res;			
		},

		create : function(inputData){
			var url = '/api/fe/BankBungaDF/';
			var param = JSON.stringify(inputData);
			console.log("object input refund cg ", param);
			var res=$http.post(url, param);
			return res;
		},

		getLoadData : function () {
			var url = '/api/fe/BankBungaDF/SelectData/';
			var res = $http.get(url);
			return res;
		},

		getVendoreBankType : function () {
			var url = '/api/fe/AccountBank/GetDataVendorBankType';
			var res = $http.get(url);
			return res;
		},

		//End CR4 Phase2
	}
});