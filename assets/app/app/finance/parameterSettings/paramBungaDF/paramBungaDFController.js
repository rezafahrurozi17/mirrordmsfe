var app = angular.module('app');
app.controller('ParamBungaDFController', function ($scope, $http, $filter, CurrentUser, ParamBungaDFFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.parent = $scope;
	$scope.DataBungaDF = [];
	$scope.paramBungaUnitTemp = {};
	$scope.paramBungaPartTemp = {};
	$scope.TempUnit = [];
	$scope.TempParts = [];
	$scope.isDataUnit = false;
	$scope.isEditUnit = false;
	$scope.isEditPart = false;
	$scope.isDataPart = false;
	$scope.validateTemp = true;
	$scope.ParamBunga_IsPembayaranUnit = false;
	$scope.ParamBunga_IsPembayaranParts = false;
	console.log('$scope.validateTemp',$scope.validateTemp)

	$scope.optionsLihatBungaDF_Search = [{ name: 'Dealer Financing Provider', value: 'ProviderName' },{ name: 'Bunga', value: 'Bunga' }, { name: 'Tenor', value: 'Tenor' },];

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 2);
	console.log($scope.allowApprove);
	if ($scope.allowApprove == false) {
		$scope.allowSimpan = false;
	}
	else {
		if ($scope.user.OrgCode.substring(3, 9) == '000000')
			$scope.allowSimpan = true;
		else
			$scope.allowSimpan = false;
	}

	$scope.getMainData = function () {
		$scope.ParamBunga_IsPembayaranUnit = false;
		$scope.ParamBunga_IsPembayaranParts = false;
		
		// ParamBungaDFFactory.getData()
		// 	.then(
		// 		function (res) {
		// 			angular.forEach(res.data.Result, function (value, key) { 
		// 				if (!angular.isUndefined(value.ParamDFInterestValue)) {
		// 					value.ParamDFInterestValue = value.ParamDFInterestValue.toString().replace('.', ',');
		// 				}
		// 			});
		// 			angular.forEach(res.data.Result, function (value, key) {
		// 				if (value.ParamDFInterestType == 'Pembayaran Unit') {
		// 					$scope.ParamBunga_IsPembayaranUnitParamId = value.ParameterDetailId;
		// 					if (value.ParamDFInterestValue == 0) {
		// 						$scope.ParamBunga_IsPembayaranUnit = false;
		// 					}
		// 					else {
		// 						$scope.ParamBunga_IsPembayaranUnit = true;
		// 					}
		// 				}

		// 				if (value.ParamDFInterestType == 'Pembayaran Parts') {
		// 					$scope.ParamBunga_IsPembayaranPartsParamId = value.ParameterDetailId;
		// 					if (value.ParamDFInterestValue == 0) {
		// 						$scope.ParamBunga_IsPembayaranParts = false;
		// 					}
		// 					else {
		// 						$scope.ParamBunga_IsPembayaranParts = true;
		// 					}
		// 				}

		// 				if (value.ParamDFInterestType == 'Bunga Parts') {
		// 					$scope.ParamBunga_BungaPartsParamId = value.ParameterDetailId;
		// 					$scope.ParamBunga_PembayaranParts = value.ParamDFInterestValue;
		// 				}

		// 				if (value.ParamDFInterestType == 'Tenor Parts') {
		// 					$scope.ParamBunga_TenorPartsParamId = value.ParameterDetailId;
		// 					$scope.ParamBunga_TenorParts = value.ParamDFInterestValue;
		// 				}

		// 				if (value.ParamDFInterestType == 'Bunga Unit') {
		// 					$scope.ParamBunga_BungaUnitParamId = value.ParameterDetailId;
		// 					$scope.ParamBunga_PembayaranUnit = value.ParamDFInterestValue;
		// 				}

		// 				if (value.ParamDFInterestType == 'Tenor Unit') {
		// 					$scope.ParamBunga_TenorUnitParamId = value.ParameterDetailId;
		// 					$scope.ParamBunga_TenorUnit = value.ParamDFInterestValue;
		// 				}
		// 			});
		// 		}
		// 	);
			//CR4 Phase 2
	$scope.ParamBunga_NamaBank = null;
	$scope.ParamBunga_NorekBank = null;
	$scope.ParamBunga_DealerFinancingProvicerUnit = null;
	$scope.ParamBunga_DealerFinancingProvicerPart = null;

	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};
	
		// ParamBungaDFFactory.getDataBankComboBox()
		// 	.then(
		// 		function (res) {
		// 			$scope.loading = false;
		// 			$scope.TambahIncomingPayment_BankTransfer_BankPengirimOptions = res.data;
					
		// 		},
		// 		function (err) {
		// 			console.log("err=>", err);
		// 		}
		// 	);
	ParamBungaDFFactory.getLoadData()
		.then(
			function (res) {
				console.log('DATAAA =>',res)
				$scope.loading = false;
				$scope.validateTemp = true;
				$scope.DataBungaDF = res.data;
				$scope.paramBungaUnit.data= res.data.UnitBungaDF; 
				$scope.paramBungaPart.data= res.data.PartsBungaDF;
				$scope.TempUnit = Object.assign($scope.paramBungaUnit.data);
				$scope.isEditUnit = $scope.TempUnit.length == 0 ? true : false;
				$scope.TempParts = Object.assign($scope.paramBungaPart.data);
				$scope.isEditPart = $scope.TempParts.length == 0 ? true : false;
				$scope.ParamBunga_IsPembayaranUnit = $scope.DataBungaDF.PembayaranUnit;
				$scope.ParamBunga_IsPembayaranParts = $scope.DataBungaDF.PembayaranParts;
				console.log('true',$scope.ParamBunga_IsPembayaranUnit,$scope.ParamBunga_IsPembayaranParts)
				
			},
			function (err) {
				console.log("err=>", err);
			}
		);
		ParamBungaDFFactory.getVendoreBankType()
		.then(
			function (res) {
				console.log('DATAAA =>',res)
				$scope.loading = false;
				$scope.DataParamBunga_DealerFinancingProvicerUnit = res.data;
				$scope.DataParamBunga_DealerFinancingProvicerPart = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
		);
	//End CR4 Phase 2
	}

	$scope.getMainData();

	$scope.ParamBungaDF_Simpan_Clicked = function () {
		$scope.NowDate = new Date();
		if (!$scope.allowSimpan) {
			bsNotify.show({
				title: "Warning",
				content: "Anda tidak berhak",
				type: "warning"
			});
			return false;
		}
		
		var isPembayaranUnit = 0;
		var isPembayaranParts = 0;
		var createData = [];
		createData = $scope.DataBungaDF;
		// createData.UnitBungaDF = $scope.paramBungaUnitTemp;
		// createData.PartsBungaDF = $scope.paramBungaPartTemp.PartBungaDF;
		createData.LastModifiedDate = $scope.NowDate;
		createData.LastModifiedUserId = $scope.user.OrgId;

		console.log("save1 ======>",$scope.paramBungaUnitTemp);
		if ($scope.ParamBunga_IsPembayaranUnit) {
			createData.UnitBungaDF = undefined;
			createData.PembayaranUnit = true
			createData.UnitBungaDF = _.isEmpty($scope.paramBungaUnitTemp) ? [] : [$scope.paramBungaUnitTemp];
		}
		else {
			createData.UnitBungaDF = undefined;
			createData.UnitBungaDF = _.isEmpty($scope.paramBungaUnitTemp) ? [] : [$scope.paramBungaUnitTemp];
			createData.PembayaranUnit = false;
		}

		if ($scope.ParamBunga_IsPembayaranParts) {
			createData.PartsBungaDF = undefined;
			createData.PembayaranParts = true; 
			createData.PartsBungaDF = _.isEmpty($scope.paramBungaPartTemp) ? [] : [$scope.paramBungaPartTemp];
		}
		else {
			createData.PartsBungaDF = undefined;
			createData.PartsBungaDF = _.isEmpty($scope.paramBungaPartTemp) ? [] : [$scope.paramBungaPartTemp];;
			createData.PembayaranParts = false;
		}
		console.log("sav2 ==================>", createData);
		ParamBungaDFFactory.create(createData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show(
						{
							title: "Sukses",
							content: "Data berhasil disimpan",
							type: 'success'
						}
					);
					$scope.ClearDataPartTemp();
					$scope.ClearDataUnitTemp();
					$scope.getMainData();
				},
				function (err) {
					console.log("Bunga DF Error : ", err);
					bsNotify.show(
						{
							title: "Gagal",
							content: err.data.Message,
							type: 'danger'
						}
					);
				}
			)
			console.log('TEMP =>',$scope.paramBungaPartTemp)
			console.log('TEMP =>',$scope.paramBungaUnitTemp)
	}

	//CR4 Phase 2
	// Unit
	$scope.paramBungaUnit = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
			{ name:'Dealer Financing Provider',field:'ProviderName', },
            { name:'Bunga',field:'Bunga', },
			{ name:'Tenor', field:'Tenor' },
			{ name:'Tanggal Awal', field:'TanggalAwal',cellFilter: 'date:\"dd/MM/yyyy\"'  },
			{ name:'Tanggal Akhir', field:'TanggalAkhir',cellFilter: 'date:\"dd/MM/yyyy\"'  }
        ]
	};

	$scope.MainBungaDFUnit_GetList = function () {
		
		var tempData = Object.assign($scope.TempUnit)
		var tmpSearch = $scope.LihatBungaDFUnit_Search;
		var tmpText = $scope.LihatBungaDFUnit_Search_Text
		$scope.paramBungaUnit.data = [];
		if($scope.LihatBungaDFUnit_Search && $scope.LihatBungaDFUnit_Search_Text){
			var temp = [];
			for (var i = 0; i < tempData.length; i++) {
				if($scope.LihatBungaDFUnit_Search == 'Bunga'){tmpSearch = tempData[i].Bunga}else if($scope.LihatBungaDFUnit_Search == 'Tenor'){tmpSearch = tempData[i].Tenor}
				else if($scope.LihatBungaDFUnit_Search == 'ProviderName'){tmpSearch = tempData[i].ProviderName}
				if (tmpSearch == tmpText) {
					temp.push(tempData[i]);
				}
			}
			$scope.paramBungaUnit.data = temp;
		}else{
			$scope.GetListDetail();
		}
	}

	$scope.LihatBungaDFUnit_Filter_Clicked = function () {
		$scope.MainBungaDFUnit_GetList();
	}
	
	$scope.ClearDataUnit = function(){
		$scope.ParamBunga_PembayaranUnit = 0;
		$scope.ParamBunga_TenorUnit = 0;
		$scope.ParamBunga_TanggalBerlakuUnit = undefined;
		$scope.ParamBunga_DealerFinancingProvicerUnit = null;		
	}

	$scope.ClearDataUnitTemp = function(){
		$scope.paramBungaUnitTemp = {};
		$scope.paramBungaUnit.data = [];
		$scope.ClearDataUnit();
		$scope.GetDisableUnitTambah();
	}

	$scope.GetDisableUnitTambah = function(){
		$scope.isDataUnit = !angular.isUndefined($scope.paramBungaUnitTemp.Bunga);
	}

	$scope.ClearDataPart = function(){
		$scope.ParamBunga_PembayaranParts = 0;
		$scope.ParamBunga_TenorParts = 0;
		$scope.ParamBunga_TanggalBerlakuParts = undefined;
		$scope.ParamBunga_DealerFinancingProvicerPart = null;
		
	}

	$scope.ClearDataPartTemp = function(){
		$scope.paramBungaPartTemp = {};
		$scope.paramBungaPart.data = [];
		$scope.ClearDataPart();
		$scope.GetDisablePartTambah();
	}

	$scope.GetDisablePartTambah = function(){
		$scope.isDataPart = !angular.isUndefined($scope.paramBungaPartTemp.Bunga);
	}

	$scope.ParamBungaDFUnit_Edit_Clicked = function () {
		var validasi = 0;
		$scope.paramBungaUnit.data = Object.assign($scope.TempUnit);
		var tempData = $scope.paramBungaUnit.data;
		var maxDate = new Date("9999-12-31T00:00:00");
		$scope.NowDate = new Date();
		$scope.NowDate.setHours(0,0,0);
		$scope.ParamBunga_TanggalBerlakuUnit.setHours(0,0,1);
	//	var i = $scope.TempUnit.length -1; //temp

		var latest = tempData.reduce(function (r, a) {
			return r.TanggalAkhir > a.TanggalAkhir ? r : a;
			});
			console.log('last',latest)

		var date = new Date($scope.ParamBunga_TanggalBerlakuUnit)
			date.setDate(date.getDate() - 1)
		var i = $scope.paramBungaUnit.data.length - 2

		// if(($scope.ParamBunga_TanggalBerlakuUnit < $scope.NowDate)){
		// 	validasi = 1;
		// 	console.log('masuk if 1')	
		// 	$scope.ValidateDate();
		// }else if(latest.VendorId != null && ($scope.ParamBunga_TanggalBerlakuUnit < $scope.NowDate)){ 
		// 	validasi = 1;
		// 	console.log('masuk if 2')	
		// 	$scope.ValidateDate();
		// }else if(latest.VendorId != null && ((latest.TanggalAwal.getDate() < $scope.NowDate.getDate()) && ($scope.ParamBunga_TanggalBerlakuUnit > $scope.NowDate))){
		// 	validasi = 1;
		// 	console.log('masuk if 3')
		// 	$scope.ValidateDate();
		// }else if($scope.paramBungaUnit.data.length >= 2){
		// 	if($scope.paramBungaUnit.data[i].TanggalAwal > date){
		// 		validasi = 1;
		// 		$scope.ValidateDate();
		// 	}
		// }
		$scope.ParamBunga_TanggalBerlakuUnit.setHours(0,0,0);
		latest.TanggalAwal.setHours(0,0,0);
		console.log($scope.ParamBunga_TanggalBerlakuUnit,latest.TanggalAwal)
		if($scope.ParamBunga_TanggalBerlakuUnit < latest.TanggalAwal){
			validasi = 1;
			$scope.ValidateDate();
		}

		if(validasi == 0){
			$scope.validateTemp = false;
			var index;
			tempData.some(function(x, i) {
				if (x.Bunga == latest.Bunga
					&& x.Tenor == latest.Tenor
					&& x.TanggalAwal == latest.TanggalAwal) return (index = i);
			});
				console.log('index >', index)
			if($scope.paramBungaUnit.data.length >= 2){
				// var date = new Date($scope.ParamBunga_TanggalBerlakuUnit)
				// date.setDate(date.getDate() - 1)
				$scope.paramBungaUnit.data[index-1].TanggalAkhir = date
			}
				var ProviderName = $scope.DealerProviderByValue($scope.ParamBunga_DealerFinancingProvicerUnit);
				$scope.paramBungaUnit.data[index].ProviderName = ProviderName;
				$scope.paramBungaUnit.data[index].VendorId = $scope.ParamBunga_DealerFinancingProvicerUnit;
				$scope.paramBungaUnit.data[index].Bunga = $scope.ParamBunga_PembayaranUnit;
				$scope.paramBungaUnit.data[index].Tenor = $scope.ParamBunga_TenorUnit;
				$scope.paramBungaUnit.data[index].TanggalAwal = $scope.ParamBunga_TanggalBerlakuUnit;
				$scope.paramBungaUnit.data[index].TanggalAkhir = maxDate;
				$scope.GetDisableUnitTambah();
				$scope.paramBungaUnitTemp= $scope.paramBungaUnit.data[index];
				$scope.paramBungaUnitTemp.LastModifiedDate = $scope.NowDate;
				$scope.paramBungaUnitTemp.LastModifiedUserId= $scope.user.OrgId;
				console.log('DATAA =>', $scope.paramBungaUnitTemp)
				$scope.GetDisableUnitTambah();
				$scope.ClearDataUnit();

		}
			
	}
	$scope.DealerProviderByValue = function(value){
		for(x in $scope.DataParamBunga_DealerFinancingProvicerUnit){
			if($scope.DataParamBunga_DealerFinancingProvicerUnit[x].value == value){
				return $scope.DataParamBunga_DealerFinancingProvicerUnit[x].name;
			}
		}
	}

	$scope.ParamBungaDFUnit_Tambah_Clicked = function () {
		$scope.paramBungaUnit.data = Object.assign($scope.TempUnit);
		$scope.NowDate = new Date();
		$scope.NowDate.setHours(0,0,0);
		var maxDate = new Date("9999-12-31T00:00:00");
		var tempData = $scope.paramBungaUnit.data;
		
		//cari dealer financing by provicer
		var ProviderName = $scope.DealerProviderByValue($scope.ParamBunga_DealerFinancingProvicerUnit);
		if($scope.paramBungaUnit.data.length == 0){
			$scope.ParamBunga_TanggalBerlakuUnit.setHours(0,0,1);
			console.log('DATE',$scope.ParamBunga_TanggalBerlakuUnit,$scope.NowDate)
			// if($scope.ParamBunga_TanggalBerlakuUnit < $scope.NowDate){
			// 	$scope.ValidateDate();
			// }else{
				$scope.validateTemp = false;
				var BankData = {
					"VendorId": $scope.ParamBunga_DealerFinancingProvicerUnit,
					"ProviderName": ProviderName,
					"Bunga": $scope.ParamBunga_PembayaranUnit,
					"Tenor": $scope.ParamBunga_TenorUnit,
					"TanggalAwal": $scope.ParamBunga_TanggalBerlakuUnit,
					"TanggalAkhir": maxDate,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
				$scope.paramBungaUnit.data.push(BankData)
				
				$scope.paramBungaUnitTemp= BankData;
				console.log('DATAA =>', $scope.paramBungaUnitTemp)
				$scope.GetDisableUnitTambah();
				$scope.ClearDataUnit();
			// }
			
		} else{
			var latest = tempData.reduce(function (r, a) {
				return r.TanggalAkhir > a.TanggalAkhir ? r : a;
				});
		//	var i = $scope.paramBungaUnit.data[$scope.paramBungaUnit.data.length -1]

			var date = new Date($scope.ParamBunga_TanggalBerlakuUnit)
				date.setDate(date.getDate() - 1)
			var i = $scope.paramBungaUnit.data.length -1
			// if($scope.ParamBunga_TanggalBerlakuUnit <= latest.TanggalAkhir && $scope.ParamBunga_TanggalBerlakuUnit.getDate() < $scope.NowDate.getDate()){
			// 	$scope.ValidateDate();
			// }else if($scope.paramBungaUnit.data[i].TanggalAwal > date){
			// 	$scope.ValidateDate();
			$scope.ParamBunga_TanggalBerlakuUnit.setHours(0,0,0);
			latest.TanggalAwal.setHours(0,0,0);
			console.log($scope.ParamBunga_TanggalBerlakuUnit,latest.TanggalAwal)
			if($scope.ParamBunga_TanggalBerlakuUnit <= latest.TanggalAwal){
				$scope.ValidateDate();
			}else{
				
				console.log('index last', i, $scope.paramBungaUnit.data);
				var BankData = {
					"VendorId": $scope.paramBungaUnit.data[i].VendorId,
					"ProviderName": $scope.paramBungaUnit.data[i].ProviderName,
					"Bunga": $scope.paramBungaUnit.data[i].Bunga,
					"Tenor": $scope.paramBungaUnit.data[i].Tenor,
					"TanggalAwal": $scope.paramBungaUnit.data[i].TanggalAwal,
					"TanggalAkhir": date,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
			//	$scope.paramBungaUnitTemp.push(BankData);
				$scope.paramBungaUnit.data[i] = (BankData)
				console.log('index last', i);

				$scope.validateTemp = false;
				BankData = {
					"VendorId": $scope.ParamBunga_DealerFinancingProvicerUnit,
					"ProviderName": ProviderName,
					"Bunga": $scope.ParamBunga_PembayaranUnit,
					"Tenor": $scope.ParamBunga_TenorUnit,
					"TanggalAwal": $scope.ParamBunga_TanggalBerlakuUnit,
					"TanggalAkhir": maxDate,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
				$scope.paramBungaUnit.data.push(BankData)
				$scope.paramBungaUnitTemp = BankData;
				console.log('DATAA =>', $scope.paramBungaUnitTemp)
				$scope.GetDisableUnitTambah();
				$scope.ClearDataUnit();
			}	
		}
		$scope.isEditUnit = false;
	}

	$scope.ValidateDate = function(){
		bsNotify.show({
			title: "Warning",
			content: "Tanggal tidak boleh lebih kecil dari tanggal awal data terakhir",
			type: "warning"
		});
		return false;
	}

	// Part
	$scope.paramBungaPart = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
			{ name:'Dealer Financing Provider',field:'ProviderName', },
            { name:'Bunga',field:'Bunga', },
			{ name:'Tenor', field:'Tenor' },
			{ name:'Tanggal Awal', field:'TanggalAwal',cellFilter: 'date:\"dd/MM/yyyy\"'  },
			{ name:'Tanggal Akhir', field:'TanggalAkhir',cellFilter: 'date:\"dd/MM/yyyy\"'  }
        ]
	};
	
	$scope.MainBungaDFPart_GetList = function () {

		var tempData = Object.assign($scope.TempParts)
		var tmpSearch = $scope.LihatBungaDFPart_Search;
		var tmpText = $scope.LihatBungaDFPart_Search_Text
		$scope.paramBungaPart.data = [];
		if($scope.LihatBungaDFPart_Search && $scope.LihatBungaDFPart_Search_Text){
			var temp = [];
			for (var i = 0; i < tempData.length; i++) {
				if($scope.LihatBungaDFPart_Search == 'Bunga'){tmpSearch = tempData[i].Bunga}else if($scope.LihatBungaDFPart_Search == 'Tenor'){tmpSearch = tempData[i].Tenor}
				else if($scope.LihatBungaDFPart_Search == 'ProviderName'){tmpSearch = tempData[i].ProviderName}
				if (tmpSearch == tmpText) {
					temp.push(tempData[i]);
				}
			}
			$scope.paramBungaPart.data = temp;
		}else{
			$scope.GetListDetail();
		}
	}

	$scope.LihatBungaDFPart_Filter_Clicked = function () {
		$scope.MainBungaDFPart_GetList();
	}

	$scope.ParamBungaDFPart_Edit_Clicked = function () {
		var validasi = 0;
		$scope.paramBungaPart.data = Object.assign($scope.TempParts);
		var tempData = $scope.paramBungaPart.data;
		var maxDate = new Date("9999-12-31T00:00:00");
		$scope.NowDate = new Date();
		$scope.NowDate.setHours(0,0,0);
		$scope.ParamBunga_TanggalBerlakuParts.setHours(0,0,0)
	//	var i = $scope.TempParts.length -1; //temp

		var latest = tempData.reduce(function (r, a) {
			return r.TanggalAkhir > a.TanggalAkhir ? r : a;
			});

		var date = new Date($scope.ParamBunga_TanggalBerlakuParts)
			date.setDate(date.getDate() - 1)
		var i = $scope.paramBungaPart.data.length -2

		// console.log($scope.paramBungaPart.data[i].TanggalAwal, date)
		// console.log($scope.paramBungaPart.data)
		// if(($scope.ParamBunga_TanggalBerlakuParts < $scope.NowDate)){
		// 	validasi = 1;
		// 	console.log('masuk if 1')	
		// 	$scope.ValidateDate();
		// }else if(latest.VendorId != null && ($scope.ParamBunga_TanggalBerlakuParts < $scope.NowDate)){ 
		// 	validasi = 1;
		// 	console.log('masuk if 2')	
		// 	$scope.ValidateDate();
		// }else if(latest.VendorId != null && ((latest.TanggalAwal.getDate() < $scope.NowDate.getDate()) && ($scope.ParamBunga_TanggalBerlakuParts > $scope.NowDate))){
		// 	validasi = 1;
		// 	console.log('masuk if 3')
		// 	$scope.ValidateDate();
		// }else if($scope.paramBungaPart.data.length >= 2){
		// 	if($scope.paramBungaPart.data[i].TanggalAwal > date){
		// 		validasi = 1;
		// 		$scope.ValidateDate();
		// 	}
		// }
		$scope.ParamBunga_TanggalBerlakuParts.setHours(0,0,0)
		latest.TanggalAwal.setHours(0,0,0);
		console.log($scope.ParamBunga_TanggalBerlakuParts,latest.TanggalAwal);
		if($scope.ParamBunga_TanggalBerlakuParts < latest.TanggalAwal){
			validasi = 1;
			$scope.ValidateDate();
		}

		if(validasi == 0){
			$scope.validateTemp = false;
			var index;
			tempData.some(function(x, i) {
				if (x.Bunga == latest.Bunga
					&& x.Tenor == latest.Tenor
					&& x.TanggalAwal == latest.TanggalAwal) return (index = i);
			});
				console.log('index >', index)
			if($scope.paramBungaPart.data.length >= 2){
				// var date = new Date($scope.ParamBunga_TanggalBerlakuParts)
				// date.setDate(date.getDate() - 1)
				 $scope.paramBungaPart.data[index-1].TanggalAkhir = date
			}
				var ProviderName = $scope.DealerProviderByValue($scope.ParamBunga_DealerFinancingProvicerPart);
				$scope.paramBungaPart.data[index].ProviderName = ProviderName;
				$scope.paramBungaPart.data[index].VendorId = $scope.ParamBunga_DealerFinancingProvicerPart;
				$scope.paramBungaPart.data[index].Bunga = $scope.ParamBunga_PembayaranParts;
				$scope.paramBungaPart.data[index].Tenor = $scope.ParamBunga_TenorParts;
				$scope.paramBungaPart.data[index].TanggalAwal = $scope.ParamBunga_TanggalBerlakuParts;
				$scope.paramBungaPart.data[index].TanggalAkhir = maxDate;
				$scope.GetDisablePartTambah();
				$scope.paramBungaPartTemp = $scope.paramBungaPart.data[index];
				$scope.paramBungaPartTemp.LastModifiedDate = $scope.NowDate;
				$scope.paramBungaPartTemp.LastModifiedUserId= $scope.user.OrgId;
				console.log('DATAA =>', $scope.paramBungaPartTemp)
				$scope.GetDisablePartTambah();
				$scope.ClearDataPart();
		}
			
	}

	
	$scope.ParamBungaDFPart_Tambah_Clicked = function () {
		$scope.paramBungaPart.data = Object.assign($scope.TempParts);
		$scope.NowDate = new Date();
		$scope.NowDate.setHours(0,0,0);
		var maxDate = new Date("9999-12-31T00:00:00");
		var tempData = $scope.paramBungaPart.data;
		var ProviderName = $scope.DealerProviderByValue($scope.ParamBunga_DealerFinancingProvicerPart);
		if($scope.paramBungaPart.data.length == 0){
			$scope.ParamBunga_TanggalBerlakuParts.setHours(0,0,1);
			// if($scope.ParamBunga_TanggalBerlakuParts < $scope.NowDate){
			// 	$scope.ValidateDate();
			// }else{
				$scope.validateTemp = false;
				var BankData = {
					"VendorId": $scope.ParamBunga_DealerFinancingProvicerPart,
					"ProviderName": ProviderName,
					"Bunga": $scope.ParamBunga_PembayaranParts,
					"Tenor": $scope.ParamBunga_TenorParts,
					"TanggalAwal": 	$scope.ParamBunga_TanggalBerlakuParts,
					"TanggalAkhir": maxDate,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
				$scope.paramBungaPart.data.push(BankData)
				
				$scope.paramBungaPartTemp= BankData;
				console.log('DATAA =>', $scope.paramBungaPartTemp)
				$scope.GetDisablePartTambah();
				$scope.ClearDataPart();
			// }
			
		} else{
			var latest = tempData.reduce(function (r, a) {
				return r.TanggalAkhir > a.TanggalAkhir ? r : a;
				});
			// var i = $scope.paramBungaPart.data[$scope.paramBungaPart.data.length -1]

			var date = new Date($scope.ParamBunga_TanggalBerlakuParts)
				date.setDate(date.getDate() - 1)
			var i = $scope.paramBungaPart.data.length -1
			// if($scope.ParamBunga_TanggalBerlakuParts <= latest.TanggalAkhir && $scope.ParamBunga_TanggalBerlakuParts.getDate() < $scope.NowDate.getDate()){
			// 	$scope.ValidateDate();
			// }else if($scope.paramBungaPart.data[i].TanggalAwal > date){
			// 	$scope.ValidateDate();
			$scope.ParamBunga_TanggalBerlakuParts.setHours(0,0,0);
			latest.TanggalAwal.setHours(0,0,0);
			console.log($scope.ParamBunga_TanggalBerlakuParts,latest.TanggalAwal)
			if($scope.ParamBunga_TanggalBerlakuParts <= latest.TanggalAwal){
				$scope.ValidateDate();
			}else{
				console.log('index last', i, $scope.paramBungaPart.data);
				var BankData = {
					"VendorId": $scope.paramBungaPart.data[i].VendorId,
					"ProviderName": $scope.paramBungaPart.data[i].ProviderName,
					"Bunga": $scope.paramBungaPart.data[i].Bunga,
					"Tenor": $scope.paramBungaPart.data[i].Tenor,
					"TanggalAwal": $scope.paramBungaPart.data[i].TanggalAwal,
					"TanggalAkhir": date,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
			//	$scope.paramBungaPartsTemp.push(BankData);
				$scope.paramBungaPart.data[i] = (BankData)
				console.log('index last', i);

				$scope.validateTemp = false;
				BankData = {
					"VendorId": $scope.ParamBunga_DealerFinancingProvicerPart,
					"ProviderName": ProviderName,
					"Bunga": $scope.ParamBunga_PembayaranParts,
					"Tenor": $scope.ParamBunga_TenorParts,
					"TanggalAwal": $scope.ParamBunga_TanggalBerlakuParts,
					"TanggalAkhir": maxDate,
					"LastModifiedDate": $scope.NowDate,
					"LastModifiedUserId": $scope.user.OrgId,
				};
				$scope.paramBungaPart.data.push(BankData)
				$scope.paramBungaPartTemp = BankData;
				console.log('DATAA =>', $scope.paramBungaPartTemp)
				$scope.GetDisablePartTambah();
				$scope.ClearDataPart();
			}	
		}

		$scope.isEditPart = false;
	}

	// $scope.GetListNamaRekening = function () {
	// 	$scope.ClearDataUnitTemp();
	// 	$scope.ClearDataPartTemp();
	// 	$scope.ParamBunga_NorekBank = undefined;
	// 	$scope.TambahIncomingPayment_BankTransfer_NorekBankPengirimOptions = undefined;
	// 	if( $scope.ParamBunga_NamaBank !== undefined){
	// 		ParamBungaDFFactory.getDataNomorRekeningComboBox($scope.ParamBunga_NamaBank)
	// 		.then(
	// 			function (res) {
	// 				$scope.validateTemp = true;
	// 				$scope.TambahIncomingPayment_BankTransfer_NorekBankPengirimOptions = res.data.Result;
	// 			},
	// 				function (err) {
	// 					console.log("err=>", err);
	// 				}
	// 		);
	// 	}
	// }

	$scope.GetListDetail = function () {
		console.log('GET DATA')
		$scope.ClearDataUnitTemp();
		$scope.ClearDataPartTemp();

		ParamBungaDFFactory.getLoadData()
		.then(
			function (res) {
				console.log('DATAAA =>',res)
				$scope.loading = false;
				$scope.validateTemp = true;
				$scope.DataBungaDF = res.data;
				$scope.paramBungaUnit.data= res.data.UnitBungaDF; 
				$scope.paramBungaPart.data= res.data.PartsBungaDF;
				$scope.TempUnit = Object.assign($scope.paramBungaUnit.data);
				$scope.isEditUnit = $scope.TempUnit.length == 0 ? true : false;
				$scope.TempParts = Object.assign($scope.paramBungaPart.data);
				$scope.isEditPart = $scope.TempParts.length == 0 ? true : false;
				$scope.ParamBunga_IsPembayaranUnit = $scope.DataBungaDF.PembayaranUnit;
				$scope.ParamBunga_IsPembayaranParts = $scope.DataBungaDF.PembayaranParts;
				console.log('true',$scope.ParamBunga_IsPembayaranUnit,$scope.ParamBunga_IsPembayaranParts)
			},
			function (err) {
				console.log("err=>", err);
			}
		);
	}

	$scope.validateNumber = function (value){
		console.log('BLUR jalan',value)
		if(value == 1){
			$scope.ParamBunga_PembayaranParts = $scope.ParamBunga_PembayaranParts.replace(/[^0-9.]/g, '')
		}else if(value == 2){
			$scope.ParamBunga_PembayaranUnit = $scope.ParamBunga_PembayaranUnit.replace(/[^0-9.]/g, '')
		}
	}
	//End CR4 Phase 2
});