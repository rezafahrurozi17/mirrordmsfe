var app = angular.module('app');
app.controller('ParamBJDBPHController', function ($scope, $http, $filter, CurrentUser, ParamBJDBPHFactory, $timeout, bsNotify, ngDialog) {

	/*==========================================================================================*/
	/*										BJD Main Begin										*/
	/*==========================================================================================*/
	var uiGridPageSize = 10;
	ParamBJDBPHFactory.getData('ParamBJD')
		.then(
			function (res) {
				console.log("res data BJD", res.data.Result);
				$scope.ParamBJDKeteranganList_UIGrid_Paging(uiGridPageSize);
				$scope.ParamBJDBPH_SetFields(res);

			}
		);

	ParamBJDBPHFactory.getData('ParamBPH')
		.then(
			function (res) {
				console.log("res data BPH", res.data.Result);
				$scope.ParamBPHKeteranganList_UIGrid_Paging(uiGridPageSize);
				$scope.ParamBJDBPH_SetFieldsBPH(res);

			}
		);

	$scope.ParamBJDBPH_SetFieldsBPH = function (res) {
		$scope.ParamBPH_BanyakCetak = res.data.Result[0].ParameterDetailValue;
		$scope.ParameterDetailIdJumlahCetakBPH = res.data.Result[0].ParameterDetailId;
		$scope.ParameterDetailIdisTampilTextBPH = res.data.Result[1].ParameterDetailId;
		$scope.ParameterDetailIdisBoldBPH = res.data.Result[2].ParameterDetailId;
		$scope.ParameterDetailIdisItalicBPH = res.data.Result[3].ParameterDetailId;
		$scope.ParameterDetailIdisUnderlineBPH = res.data.Result[4].ParameterDetailId;

		if (res.data.Result[1].ParameterDetailValue == 1)
			$scope.ParamBPH_isTampilkanText = true;
		else
			$scope.ParamBPH_isTampilkanText = false;

		if (res.data.Result[2].ParameterDetailValue == 1)
			$scope.ParamBPH_isTextTebal = true;
		else
			$scope.ParamBPH_isTextTebal = false;

		if (res.data.Result[3].ParameterDetailValue == 1)
			$scope.ParamBPH_isTextMiring = true;
		else
			$scope.ParamBPH_isTextMiring = false;

		if (res.data.Result[4].ParameterDetailValue == 1)
			$scope.ParamBPH_isGarisBawah = true;
		else
			$scope.ParamBPH_isGarisBawah = false;
	}

	$scope.ParamBJDBPH_SetFields = function (res) {
		$scope.ParamBJD_BanyakCetak = res.data.Result[0].ParameterDetailValue;
		$scope.ParameterDetailIdJumlahCetak = res.data.Result[0].ParameterDetailId;
		$scope.ParameterDetailIdisTampilText = res.data.Result[1].ParameterDetailId;
		$scope.ParameterDetailIdisBold = res.data.Result[2].ParameterDetailId;
		$scope.ParameterDetailIdisItalic = res.data.Result[3].ParameterDetailId;
		$scope.ParameterDetailIdisUnderline = res.data.Result[4].ParameterDetailId;

		if (res.data.Result[1].ParameterDetailValue == 1)
			$scope.ParamBJD_isTampilkanText = true;
		else
			$scope.ParamBJD_isTampilkanText = false;

		if (res.data.Result[2].ParameterDetailValue == 1)
			$scope.ParamBJD_isTextTebal = true;
		else
			$scope.ParamBJD_isTextTebal = false;

		if (res.data.Result[3].ParameterDetailValue == 1)
			$scope.ParamBJD_isTextMiring = true;
		else
			$scope.ParamBJD_isTextMiring = false;

		if (res.data.Result[4].ParameterDetailValue == 1)
			$scope.ParamBJD_isGarisBawah = true;
		else
			$scope.ParamBJD_isGarisBawah = false;
	}

	$scope.KonfirmasiHapus = function (row, t) {
		var index = ((t == 1) ? $scope.ParamBJDKeteranganList_UIGrid.data.indexOf(row.entity) : $scope.ParamBPHKeteranganList_UIGrid.data.indexOf(row.entity));
		var id = row.entity.ParameterDetailValue1;
		var txt = "No Urut " + id + " - " + row.entity.ParameterDetailValue2;
		var BJDOrBPH = ((t == 1) ? 'ParamBJDBPH_BJDDeleteKeterangan' : 'ParamBJDBPH_BPHDeleteKeterangan');
		ngDialog.openConfirm({
			template: '\
						 <div class="ngdialog-buttons">\
						 <p><h4>Konfirmasi</h4></p>\
						 <hr>\
						 <p>Anda yakin akan menghapus row <b>' + txt + '</b>?</p>\
						 <hr>\
						 <div class="ngdialog-buttons" align="center">\
						   <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="closeThisDialog(0)">Tidak</button>\
						   <button type="button" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="' + BJDOrBPH + '('+ index + ')">Ya</button>\
						 </div>\
						 </div>',
			plain: true,
			//controller: 'ParamKuitansiTTUSController',
			scope: $scope
		});
	}

	$scope.ParamBJDBPH_BJDDeleteKeterangan = function (index) {
		// var isDelete = false;
		// var CurrentKey = 0;
		// angular.forEach($scope.ParamBJD_BJDDetailData, function (value, key) {
		// 	if (value.ParameterDetailValue1 == row.entity.ParameterDetailValue1) {
		// 		$scope.ParamBJD_BJDDetailData.splice(key, 1);
		// 		isDelete = true;
		// 		CurrentKey = key;
		// 	}
		// 	if (isDelete && CurrentKey != key) {
		// 		$scope.ParamBJD_BJDDetailData[key].ParameterDetailValue1 = parseInt($scope.ParamBJD_BJDDetailData[key].ParameterDetailValue1) - 1;
		// 	}

		// });

		// $scope.ParamBJDKeteranganList_UIGrid.data = $scope.ParamBJD_BJDDetailData;
		$scope.ParamBJD_BJDDetailData.splice(index, 1);
		$scope.ngDialog.close();
	}

	$scope.ParamBJDBPH_BPHDeleteKeterangan = function (index) {
		// var isDelete = false;
		// var CurrentKey = 0;
		// angular.forEach($scope.ParamBJD_BPHDetailData, function (value, key) {
		// 	if (value.ParameterDetailValue1 == row.entity.ParameterDetailValue1) {
		// 		$scope.ParamBJD_BPHDetailData.splice(key, 1);
		// 		isDelete = true;
		// 		CurrentKey = key;
		// 	}
		// 	if (isDelete && CurrentKey != key) {
		// 		$scope.ParamBJD_BPHDetailData[key].ParameterDetailValue1 = parseInt($scope.ParamBJD_BPHDetailData[key].ParameterDetailValue1) - 1;
		// 	}

		// });

		// $scope.ParamBPHKeteranganList_UIGrid.data = $scope.ParamBJD_BPHDetailData;
		$scope.ParamBJD_BPHDetailData.splice(index, 1);
		$scope.ngDialog.close();
	}

	$scope.ParamBJDKeterangan_Tambah_Clicked = function () {
		var lastNumber = 0;

		if ($scope.ParamBJD_BJDDetailData.length != 0){
		lastNumber = parseInt($scope.ParamBJD_BJDDetailData[$scope.ParamBJD_BJDDetailData.length - 1].ParameterDetailValue1, 10);
		}

		$scope.ParamBJD_BJDDetailData.push({ "ParameterValueId": 0, "ParameterDetailValue1": lastNumber + 1, "ParameterDetailValue2": "" });
		$scope.ParamBJDKeteranganList_UIGrid.data = $scope.ParamBJD_BJDDetailData;
	}

	$scope.ParamBPHKeterangan_Simpan_Clicked = function () {
		var lastNumber = 0;

		if ($scope.ParamBJD_BPHDetailData.length != 0){
			lastNumber = parseInt($scope.ParamBJD_BPHDetailData[$scope.ParamBJD_BPHDetailData.length - 1].ParameterDetailValue1, 10);
		}

		$scope.ParamBJD_BPHDetailData.push({ "ParameterValueId": 0, "ParameterDetailValue1": lastNumber + 1, "ParameterDetailValue2": "" });
		$scope.ParamBPHKeteranganList_UIGrid.data = $scope.ParamBJD_BPHDetailData;
	}
	/*==========================================================================================*/
	/*										BJD Main End										*/
	/*==========================================================================================*/

	/*==========================================================================================*/
	/*										BJD UIGrid Begin									*/
	/*==========================================================================================*/

	$scope.uiGridPageSize = 10;

	$scope.ParamBJDKeteranganList_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination: true,
		// rowSelection: true,
		// multiSelect: false,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination : false,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		multiselect: false,
		canSelectRows: true,
		enableFiltering: true,
		columnDefs: [
			{ name: "ParameterValueId", field: "ParameterValueId", visible: false },
			{ name: "No Urut", field: "ParameterDetailValue1", enableCellEdit: false },
			{ 
				name: "Keterangan", field: "ParameterDetailValue2",
				editableCellTemplate: '<input type="text" ng-maxlength="50" maxlength="50" ui-grid-editor ng-model="row.entity.ParameterDetailValue2">' 
			},
			{ name: "Action", cellTemplate: '<a ng-click="grid.appScope.KonfirmasiHapus(row,1)">Hapus</a>' },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiParamBJDKeteranganList = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.ParamBJDKeteranganList_UIGrid_Paging(uiGridPageSize);
			// });
		}
	};

	$scope.ParamBJDKeteranganList_UIGrid_Paging = function (Limit) {
		ParamBJDBPHFactory.getDataBJDDetail(Limit,'ParamBJDInformation')
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res ParamBJDInformation paging =>", res.data.Result);
						$scope.ParamBJD_BJDDetailData = res.data.Result;
						$scope.ParamBJDKeteranganList_UIGrid.data = res.data.Result;
						// $scope.ParamBJDKeteranganList_UIGrid.totalItems = res.data.Total;
						// $scope.ParamBJDKeteranganList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						// $scope.ParamBJDKeteranganList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
					}
				}

			);
	}

	/*==========================================================================================*/
	/*										BJD UIGrid End									*/
	/*==========================================================================================*/

	/*==========================================================================================*/
	/*										BPH UIGrid Begin									*/
	/*==========================================================================================*/

	$scope.uiGridPageSize = 10;

	$scope.ParamBPHKeteranganList_UIGrid = {
		// paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination: true,
		// rowSelection: true,
		// multiSelect: false,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination : false,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		multiselect: false,
		canSelectRows: true,
		enableFiltering: true,
		columnDefs: [
			{ name: "ParameterValueId", field: "ParameterValueId", visible: false },
			{ name: "No Urut", field: "ParameterDetailValue1", enableCellEdit: false },
			{ 
				name: "Keterangan", field: "ParameterDetailValue2",
				editableCellTemplate: '<input type="text" ng-maxlength="50" maxlength="50" ui-grid-editor ng-model="row.entity.ParameterDetailValue2">' 
			},
			{ name: "Action", cellTemplate: '<a ng-click="grid.appScope.KonfirmasiHapus(row,2)">Hapus</a>' },
		],
		onRegisterApi: function (gridApi) {
			$scope.ParamBPHKeteranganList = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.ParamBPHKeteranganList_UIGrid_Paging();
			// });
		}
	};

	$scope.ParamBPHKeteranganList_UIGrid_Paging = function (Limit) {
		ParamBJDBPHFactory.getDataBJDDetail(Limit,'ParamBPHInformation')
			.then(
				function (res) {
					if (!angular.isUndefined(res.data.Result)) {
						console.log("res ParamBPHInformation paging =>", res.data.Result);
						$scope.ParamBJD_BPHDetailData = res.data.Result;
						$scope.ParamBPHKeteranganList_UIGrid.data = res.data.Result;
						$scope.ParamBPHKeteranganList_UIGrid.totalItems = res.data.Total;
						// $scope.ParamBPHKeteranganList_UIGrid.paginationPageSize = $scope.uiGridPageSize;
						// $scope.ParamBPHKeteranganList_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
					}
				}

			);
	}

	/*==========================================================================================*/
	/*										BPH UIGrid End										*/
	/*==========================================================================================*/

	$scope.ParamBJDBPH_Simpan_Clicked = function () {
		var isTampilkanText = 0, isTextTebal = 0, isTextMiring = 0, isGarisBawah = 0;

		//Untuk BJD
		if ($scope.ParamBJD_isTampilkanText)
			isTampilkanText = 1

		if ($scope.ParamBJD_isTextTebal)
			isTextTebal = 1

		if ($scope.ParamBJD_isTextMiring)
			isTextMiring = 1

		if ($scope.ParamBJD_isGarisBawah)
			isGarisBawah = 1

		if ($scope.ParameterDetailIdJumlahCetak == undefined)
			$scope.ParameterDetailIdJumlahCetak = -1;

		if ($scope.ParameterDetailIdisTampilText == undefined)
			$scope.ParameterDetailIdisTampilText = -2;

		if ($scope.ParameterDetailIdisBold == undefined)
			$scope.ParameterDetailIdisBold = -3;

		if ($scope.ParameterDetailIdisItalic == undefined)
			$scope.ParameterDetailIdisItalic = -4;

		if ($scope.ParameterDetailIdisUnderline == undefined)
			$scope.ParameterDetailIdisUnderline = -5;

		var inputData = [
			{ "ParameterDetailId": $scope.ParameterDetailIdJumlahCetak, "ParameterDetailValue": $scope.ParamBJD_BanyakCetak },
			{ "ParameterDetailId": $scope.ParameterDetailIdisTampilText, "ParameterDetailValue": isTampilkanText },
			{ "ParameterDetailId": $scope.ParameterDetailIdisBold, "ParameterDetailValue": isTextTebal },
			{ "ParameterDetailId": $scope.ParameterDetailIdisItalic, "ParameterDetailValue": isTextMiring },
			{ "ParameterDetailId": $scope.ParameterDetailIdisUnderline, "ParameterDetailValue": isGarisBawah },
		];


		ParamBJDBPHFactory.create(inputData)
			.then(
				function (res) {
					ParamBJDBPHFactory.createBJDDetail($scope.ParamBJDKeteranganList_UIGrid.data)
						.then(
							function (res) {
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data BJD berhasil disimpan.",
										type: 'success'
									}
								);

								ParamBJDBPHFactory.getData('ParamBJD')
									.then(
										function (res) {
											$scope.ParamBJDBPH_SetFields(res);
											$scope.ParamBJDKeteranganList_UIGrid_Paging(uiGridPageSize);
										}
									);
							}
						);
				},
				function (err) {
					console.log("BJD Detail Error : ", err);
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				}
			);

		//Untuk BPH
		isTampilkanText = 0;
		isTextTebal = 0;
		isTextMiring = 0;
		isGarisBawah = 0;

		if ($scope.ParamBPH_isTampilkanText)
			isTampilkanText = 1

		if ($scope.ParamBPH_isTextTebal)
			isTextTebal = 1

		if ($scope.ParamBPH_isTextMiring)
			isTextMiring = 1

		if ($scope.ParamBPH_isGarisBawah)
			isGarisBawah = 1

		if ($scope.ParameterDetailIdJumlahCetakBPH == undefined)
			$scope.ParameterDetailIdJumlahCetakBPH = -6;

		if ($scope.ParameterDetailIdisTampilTextBPH == undefined)
			$scope.ParameterDetailIdisTampilTextBPH = -7;

		if ($scope.ParameterDetailIdisBoldBPH == undefined)
			$scope.ParameterDetailIdisBoldBPH = -8;

		if ($scope.ParameterDetailIdisItalicBPH == undefined)
			$scope.ParameterDetailIdisItalicBPH = -9;

		if ($scope.ParameterDetailIdisUnderlineBPH == undefined)
			$scope.ParameterDetailIdisUnderlineBPH = -10;

		var inputData = [
			{ "ParameterDetailId": $scope.ParameterDetailIdJumlahCetakBPH, "ParameterDetailValue": $scope.ParamBPH_BanyakCetak },
			{ "ParameterDetailId": $scope.ParameterDetailIdisTampilTextBPH, "ParameterDetailValue": isTampilkanText },
			{ "ParameterDetailId": $scope.ParameterDetailIdisBoldBPH, "ParameterDetailValue": isTextTebal },
			{ "ParameterDetailId": $scope.ParameterDetailIdisItalicBPH, "ParameterDetailValue": isTextMiring },
			{ "ParameterDetailId": $scope.ParameterDetailIdisUnderlineBPH, "ParameterDetailValue": isGarisBawah },
		];


		ParamBJDBPHFactory.create(inputData)
			.then(
				function (res) {
					debugger;
					var asasdasd = $scope.ParamBPHKeteranganList_UIGrid.data;
					ParamBJDBPHFactory.createBPHDetail($scope.ParamBPHKeteranganList_UIGrid.data)
						.then(
							function (res) {
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data BPH berhasil disimpan.",
										type: 'success'
									}
								);
								ParamBJDBPHFactory.getData('ParamBPH')
									.then(
										function (res) {
											$scope.ParamBJDBPH_SetFieldsBPH(res);
											$scope.ParamBPHKeteranganList_UIGrid_Paging(uiGridPageSize);
										}
									);
							}
						);
				},
				function (err) {
					console.log("BPH Detail Error : ", err);
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				}
			);
	}
});