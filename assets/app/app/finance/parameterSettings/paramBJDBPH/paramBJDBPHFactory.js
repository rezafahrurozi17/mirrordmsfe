angular.module('app')
	.factory('ParamBJDBPHFactory', function ($http, CurrentUser) {
	return{
		getData: function(BJDBPHType){
			var url = '/api/fe/FinanceBJDBPH/getdata/?bjdbphtype=' + BJDBPHType;
			console.log("url FInance BJD BPH : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataBJDDetail: function(limit,BJDBPHType){
			var url = '/api/fe/FinanceBJDBPH/GetBJDDataDetail/?start=1&limit='+limit+'&bjdbphtype=' + BJDBPHType;
			console.log("url FInance BJD BPH : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/FinanceBJDBPH/create/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bjd ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		createBJDDetail : function(inputData){
			var url = '/api/fe/FinanceBJDBPH/CreateDetailData/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bjd ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		createBPHDetail : function(inputData){
			var url = '/api/fe/FinanceBJDBPH/CreateDetailDataBPH/';
			var param = JSON.stringify(inputData);
			console.log("object input finance bph ", param);
			var res=$http.post(url, param);
			return res;
		},
	}
});