angular.module('app')
	.factory('PreprintedKuitansiFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/financepreprintedreceipt/getdata';
			console.log("url Preprinted Kuitansi : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/FinancePreprintedReceipt/create/';
			var param = JSON.stringify(inputData);
			console.log("object input preprinted Receipt ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});