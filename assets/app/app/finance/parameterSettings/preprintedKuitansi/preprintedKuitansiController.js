var app = angular.module('app');
app.controller('PreprintedKuitansiController', function ($scope, $http, $filter, CurrentUser, PreprintedKuitansiFactory,bsNotify, $timeout) {
	
	PreprintedKuitansiFactory.getData()
	.then(
		function(res){
			$scope.PreprintedKuitansi_Data = res.data.Result;
			$scope.PreprintedKuitansi_UIGrid.data = $scope.PreprintedKuitansi_Data;
			if($scope.PreprintedKuitansi_UIGrid.data == null || $scope.PreprintedKuitansi_UIGrid.data == undefined || $scope.PreprintedKuitansi_UIGrid.data == ""){
				$scope.SimpanDisabled = true;
			}
		}
	);
	
	$scope.SimpanDisabled = false;
	$scope.PreprintedKuitansi_Simpan_Clicked = function(){
		
		//alert("Tipe bisnis \"Unit\" selalu preprinted");
		bsNotify.show({
			title:"warning",
			content:"Unit" +" "+ "selalu preprinted",
			type:"warning"
		});
		PreprintedKuitansiFactory.create($scope.PreprintedKuitansi_UIGrid.data)
		.then(
			function(res){
				bsNotify.show({
					title:"success",
					content:"Data Berhasil Di Simpan.",
					type:"success"
				});
				PreprintedKuitansiFactory.getData()
				.then(
					function(res){
						$scope.PreprintedKuitansi_Data = res.data.Result;
						$scope.PreprintedKuitansi_UIGrid.data = $scope.PreprintedKuitansi_Data;
					}
				);
			},
			function(err){
				console.log("Preprinted Kuitansi Error : ", err);
			}
		)
		
	}
	
	//=============================UI Grid Begin===============================//
	$scope.PreprintedKuitansi_UIGrid = {
		data: $scope.PreprintedKuitansi_Data,
		columnDefs: [
						{name:"ParameterDetailId",field:"ParameterDetailId",visible:false},
						{name:"Tipe Bisnis",field:"BusinessType"},
						{name:"Preprinted Kuitansi", field:"isPreprintedReceipt", type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled=\'row.entity.BusinessType.toString().includes("Unit")\' ng-model="row.entity.isPreprintedReceipt">' },
					],
		onRegisterApi: function(gridApi) {
			$scope.PreprintedKuitansiGridApi = gridApi;
		}
	};
	//=============================UI Grid End===============================//
});