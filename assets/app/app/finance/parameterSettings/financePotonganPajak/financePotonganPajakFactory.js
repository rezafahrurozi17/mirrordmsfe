angular.module('app')
	.factory('FinancePotonganPajakFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
	return{
		getDataNomorPph: function(Start, Limit) {
			//var url = '/api/fe/FinanceNomorPajak/start/'+Start+'/limit/'+Limit+'/periodmonth/'+periodMonth+'/periodyear/' + periodYear;
			var url = '/api/fe/FinanceNomorPph/?start='+Start+'&limit='+Limit;
			console.log("getDataNomorPph url :", url);
			var res=$http.get(url);
			return res;
		},
		
		simpanNomorPphData : function(inputData){
			var url = '/api/fe/FinanceNomorPph/submitdata/';
			var param = JSON.stringify(inputData);
			console.log("object input kodeisian ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});