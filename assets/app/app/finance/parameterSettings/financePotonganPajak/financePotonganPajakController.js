var app = angular.module('app');
app.controller('FinancePotonganPajakController', function ($scope, $http, $filter, uiGridConstants, CurrentUser, FinancePotonganPajakFactory, $timeout, bsNotify) {
	// $scope.Show_MainPajak = true;
	var user = CurrentUser.user();
	$scope.Show_LihatPajak = true;
	$scope.dateOptionsYear = {
		startingDay: 1,
		format: 'yyyy',
		// disableWeekend: 1
	};
	$scope.dateOptionsMonth = {
		startingDay: 1,
		format: ' MMM',
		// disableWeekend: 1
	};

	if (user.OrgCode.substring(3, 9) == '000000')
		$scope.Pajak_HO_Show = true;
	else
		$scope.Pajak_HO_Show = false;

	$scope.ToMainPajak = function () {
		$scope.Show_MainPajak = true;
		$scope.Show_TambahPajak = false;
		$scope.Show_LihatPajak = false;
	}

	$scope.LihatPajak_DaftarNomorFakturPajak = true;

	//===================================UI Grid Nomor Pajak begin===================================//
	//===================================Irfan-fakriardian|April 2019===================================//
	var paginationOptions_FinancePotonganPajak = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}



	$scope.GetListPotonganPajak = function (start, limit) {
		FinancePotonganPajakFactory.getDataNomorPph(start, limit
		)
			.then(
				function (res) {
					console.log("Get data nomor pajak", res.data.result);
					$scope.data = res.data.Result;
					$scope.NomorBuktiPotonganPajak_UIGrid.data = res.data.Result;
					$scope.NomorBuktiPotonganPajak_UIGrid.totalItems = res.data.Total;
					$scope.NomorBuktiPotonganPajak_UIGrid.paginationPageSize = paginationOptions_FinancePotonganPajak.pageSize;
					$scope.NomorBuktiPotonganPajak_UIGrid.paginationPageSizes = [100, 25, 50];
					$scope.NomorBuktiPotonganPajak_UIGrid.paginationCurrentPage = paginationOptions_FinancePotonganPajak.pageNumber;
				}

			);
	}

	var uiGridPageSize = 10;
	$scope.NomorBuktiPotonganPajak_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		rowSelection: false,
		enableColumnResizing: true,
		useCustomPagination: false,
		useExternalPagination: true,
		enableFiltering: true,
		columnDefs: [
			{ name: "TaxTypeId", field: "TaxTypeId", visible: false },
			{ name: "GL Account Name", field: "GLAccountName", width: 147, enableCellEdit: false },
			{ name: "Jenis Pajak PPH", field: "TaxType", width: 410, enableCellEdit: false },
			{
				name: "Kode Isian", field: "KodeIsian", width: 90,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: "string",
				editableCellTemplate: '<input type="text" ui-grid-editor maxlength="20" ng-model="row.entity.KodeIsian">'
			},
			{ name: "Pratinjau", field: "Pratinjau", width: 250, enableCellEdit: false },
			{ name: "Kode Jasa Default", field: "KodeJasaDefault", width: 80, enableCellEdit: false },
			{
				name: "Kode Jasa", field: "KodeJasa", width: 60,
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true, type: "string",
				editableCellTemplate: '<input type="text" ui-grid-editor maxlength="20" ng-model="row.entity.KodeJasa">'
			},


		],
		data: $scope.data,
		onRegisterApi: function (gridApi) {
			$scope.GridApiNomorPajak = gridApi;
			//$scope.GridApiNomorPajak.grid.registerRowsProcessor( $scope.singleFilter, 200 );
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	paginationOptions_FinancePotonganPajak.pageNumber = pageNumber;
			// 	paginationOptions_FinancePotonganPajak.pageSize = pageSize;
			// 	$scope.GetListPotonganPajak(pageNumber, pageSize);

			// 	// $scope.NomorBuktiPotonganPajak_UIGrid.data = $scope.NomorBuktiPotonganPajak_UIGrid_Paging();
			// }
			// );
			gridApi.core.on.renderingComplete($scope, function () {
				console.log('first load');
				$scope.GetListPotonganPajak(1, 10);
			});
		}
	};

	$scope.SubmitSimpanPajak_Clicked = function () {
		var data = [{ "PPHSettingList": $scope.NomorBuktiPotonganPajak_UIGrid.data }]
		FinancePotonganPajakFactory.simpanNomorPphData(data)
			.then
			(
				function (res) {
					console.log(res);
					bsNotify.show({
						title: "Berhasil",
						content: "Simpan Berhasil",
						type: 'success'
					});
					$scope.GetListPotonganPajak(1, 10);
				},

				function (err) {
					// alert("Persetujuan gagal");
					bsNotify.show({
						title: "Gagal",
						content: "Simpan Gagal",
						type: 'danger'
					});
				}
			);
	}

	$scope.optionsTambahPotonganPajak_CustomerSelectKolom = [{ name: "GLAccountName", value: "GLAccountName" }, { name: "TaxType", value: "TaxType" }];
	$scope.TambahPotonganPajak_CustomerFilter_Clicked = function () {
		var gridapi = $scope.GridApiNomorPajak;
		if ($scope.TambahPotonganPajak_CustomerSelectKolom == null || $scope.TambahPotonganPajak_CustomerSelectKolom == '')
			gridapi.grid.clearAllFilters();
		else
			gridapi.grid.clearAllFilters();

		if ($scope.TambahPotonganPajak_CustomerSelectKolom == 'GLAccountName') {
			nomor = 1;
		}
		else if ($scope.TambahPotonganPajak_CustomerSelectKolom == 'TaxType') {
			nomor = 2;
		}

		else {
			bsNotify.show({
				title: "Warning",
				content: "Pilih Kriteria.",
				type: "warning"
			});
			nomor = 0;
		};

		if (!angular.isUndefined($scope.TambahPotonganPajak_CustomerSelectKolom) && nomor != 0) {
			gridapi.grid.columns[nomor].filters[0].term = $scope.TambahPotonganPajak_CustomerFilter;
		}
		gridapi.grid.queueGridRefresh();
	}
});