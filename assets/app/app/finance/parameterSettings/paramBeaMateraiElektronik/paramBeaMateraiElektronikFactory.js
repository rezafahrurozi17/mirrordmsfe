angular.module('app')
	.factory('ParamBeaMateraiElektronikFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/parambeamateraielektronik/getdata';
			console.log("url excess difference : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/parambeamateraielektronik/create/';
			var param = JSON.stringify(inputData);
			console.log("object input excess difference ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});