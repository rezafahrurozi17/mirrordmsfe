var app = angular.module('app');
app.controller('ParamBeaMateraiElektronikController', function ($scope, $http, $filter, CurrentUser, ParamBeaMateraiElektronikFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.regex = /^[.]*$/;
	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 2);
	console.log($scope.allowApprove);
	if ($scope.allowApprove == false) {
		$scope.allowSimpan = false;
	}
	else {
		if ($scope.user.OrgCode.substring(3, 9) == '000000')
			$scope.allowSimpan = true;
		else
			$scope.allowSimpan = false;
	}

	ParamBeaMateraiElektronikFactory.getData()
		.then(
			function (res) {
				console.log("res data : ", res.data.Result);
				$scope.ParameterDetailIdisEnable = res.data.Result[0].ParameterDetailId;
				$scope.ParameterDetailIdFormat = res.data.Result[1].ParameterDetailId;

				if (res.data.Result[0].ParameterDetailValue == 1)
					$scope.ParamBeaMateraiElektronik_isEnable = true;
				else
					$scope.ParamBeaMateraiElektronik_isEnable = false;

				$scope.ParamBeaMateraiElektronik_Template = res.data.Result[1].ParameterDetailValue;
			}
		);

	$scope.ParamBeaMateraiElektronik_Simpan_Clicked = function () {
		if ($scope.allowSimpan) {
			var isParamBeaMateraiElektronik = 0;
			var ParamDetailIdisEnable = -1;
			var ParamDetailIdFormat = -2;

			if ($scope.ParamBeaMateraiElektronik_isEnable)
				isParamBeaMateraiElektronik = 1

			if (!angular.isUndefined($scope.ParameterDetailIdisEnable))
				ParamDetailIdisEnable = $scope.ParameterDetailIdisEnable;

			if (!angular.isUndefined($scope.ParameterDetailIdFormat))
				ParamDetailIdFormat = $scope.ParameterDetailIdFormat;

			var inputData = [
				{ "ParameterDetailId": ParamDetailIdisEnable, "ParameterDetailValue": isParamBeaMateraiElektronik },
				{ "ParameterDetailId": ParamDetailIdFormat, "ParameterDetailValue": $scope.ParamBeaMateraiElektronik_Template },
			];

			ParamBeaMateraiElektronikFactory.create(inputData)
				.then(
					function (res) {
						//alert("Data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: "success"
						});
						ParamBeaMateraiElektronikFactory.getData()
							.then(
								function (res) {
									$scope.ParameterDetailIdisEnable = res.data.Result[0].ParameterDetailId;
									$scope.ParameterDetailIdFormat = res.data.Result[1].ParameterDetailId;
									if (res.data.Result[0].ParameterDetailValue == 1)
										$scope.ParamBeaMateraiElektronik_isEnable = true;
									else
										$scope.ParamBeaMateraiElektronik_isEnable = false;

									$scope.ParamBeaMateraiElektronik_Template = res.data.Result[1].ParameterDetailValue;
								}
							);
					},
					function (err) {
						console.log("Param Bea Materai Elektronik Error : ", err);
					}
				)
		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Anda tidak berhak",
				type: "warning"
			});
		}
	}
});