var app = angular.module('app');
app.controller('PPhProgresifController', function ($scope, $http, $filter, CurrentUser, PPhProgresifFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 2);
	console.log($scope.allowApprove);
	if ($scope.allowApprove == false) {
		$scope.allowSimpan = false;
	}
	else {
		if ($scope.user.OrgCode.substring(3, 9) == '000000')
			$scope.allowSimpan = true;
		else
			$scope.allowSimpan = false;
	}

	PPhProgresifFactory.getDataMaster()
		.then(
			function (res) {
				$scope.PPhProgresif_PersentaseDpp = res.data.Result[0].ParamValue;
				$scope.PPhProgresif_PersentaseDppParamId = res.data.Result[0].ParameterDetailId;
			}
		);

	PPhProgresifFactory.getDataDetail()
		.then(
			function (res) {
				if (res.data.Result == 0) {
					var tarifPajakDefault = [];
					tarifPajakDefault.push({ 'ParameterDetailId': '-1', 'StartNominal': '0', 'EndNominal': '50000000', 'PPnNPWP': '', 'PPnNonNPWP': '' });
					tarifPajakDefault.push({ 'ParameterDetailId': '-2', 'StartNominal': '50000001', 'EndNominal': '250000000', 'PPnNPWP': '', 'PPnNonNPWP': '' });
					tarifPajakDefault.push({ 'ParameterDetailId': '-3', 'StartNominal': '250000001', 'EndNominal': '500000000', 'PPnNPWP': '', 'PPnNonNPWP': '' });
					tarifPajakDefault.push({ 'ParameterDetailId': '-4', 'StartNominal': '500000001', 'EndNominal': '', 'PPnNPWP': '', 'PPnNonNPWP': '' });

					$scope.PPhProgresif_UIGrid.data = tarifPajakDefault;
				}
				else {
					$scope.PPh21ProgresifDetail = res.data.Result;
					angular.forEach($scope.PPh21ProgresifDetail, function (value) {
						value.PPnNPWP = value.PPnNPWP.toString().replace('.', ',');
						value.PPnNonNPWP = value.PPnNonNPWP.toString().replace('.', ',');
					});
					$scope.PPhProgresif_UIGrid.data = $scope.PPh21ProgresifDetail;
				}
			}
		);

	$scope.PPhProgresif_Simpan_Clicked = function () {
		if (!$scope.allowSimpan) {
			bsNotify.show({
				title: "Warning",
				content: "Anda tidak berhak",
				type: "warning"
			});
			return false;
		}

		var inputDataMaster = [];

		if ($scope.PPhProgresif_PersentaseDppParamId == undefined)
			$scope.PPhProgresif_PersentaseDppParamId = -1;

		inputDataMaster = [
			{
				"ParameterDetailId": $scope.PPhProgresif_PersentaseDppParamId,
				"ParamValue": $scope.PPhProgresif_PersentaseDpp.toString().replace(',', '.')
			}
		];

		PPhProgresifFactory.createMaster(inputDataMaster)
			.then(
				function (res) {
					PPhProgresifFactory.getDataDetail()
						.then(
							function (res) {

								angular.forEach($scope.PPhProgresif_UIGrid.data, function (value) {
									value.PPnNPWP = value.PPnNPWP.toString().replace(',', '.');
									value.PPnNonNPWP = value.PPnNonNPWP.toString().replace(',', '.');
								});

								PPhProgresifFactory.createDetail($scope.PPhProgresif_UIGrid.data)
									.then(
										function (res) {
											// alert("Data berhasil disimpan");

											bsNotify.show(
												{
													title: "Berhasil",
													content: "Data berhasil disimpan.",
													type: 'success'
												}
											);

											PPhProgresifFactory.getDataMaster()
												.then(
													function (res) {
														$scope.PPhProgresif_PersentaseDpp = res.data.Result[0].ParamValue.toString().replace('.', ',');
														$scope.PPhProgresif_PersentaseDppParamId = res.data.Result[0].ParameterDetailId;
													}
												);

											PPhProgresifFactory.getDataDetail()
												.then(
													function (res) {
														$scope.PPh21ProgresifDetail = res.data.Result;
														angular.forEach($scope.PPh21ProgresifDetail, function (value) {
															value.PPnNPWP = value.PPnNPWP.toString().replace('.', ',');
															value.PPnNonNPWP = value.PPnNonNPWP.toString().replace('.', ',');
														});
														$scope.PPhProgresif_UIGrid.data = $scope.PPh21ProgresifDetail;
													}
												);
										},
										function (err) {
											console.log("PPh 21 Progresif Error : ", err);
											bsNotify.show(
												{
													title: "Gagal",
													content: "Gagal simpan data.",
													type: 'danger'
												}
											);
										}
									)
							}
						);
				},
				function (err) {
					console.log("PPh 21 Progresif Error : ", err);
					bsNotify.show(
						{
							title: "Gagal",
							content: "Gagal simpan data.",
							type: 'danger'
						}
					);
				}
			)


	}

	//=============================UI Grid Begin===============================//
	$scope.PPhProgresif_UIGrid = {
		data: $scope.PPh21ProgresifDetail,
		columnDefs: [
			{ name: "ParameterDetailId", field: "ParameterDetailId", visible: false },
			{ name: "Batas Bawah DPP", field: "StartNominal", enableCellEdit: false, cellFilter: 'rupiahC' },
			{ name: "Batas Atas DPP", field: "EndNominal", enableCellEdit: false, cellFilter: 'rupiahC' },
			{
				name: "Tarif Pajak (%) (dengan NPWP)", field: "PPnNPWP", cellFilter: "noSymbol",
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				},
				editableCellTemplate: '<input type="text" ng-maxlength="5" maxlength="5" ui-grid-editor percent-allow-zero ng-model="row.entity.PPnNPWP"> ',
				cellTemplate: '<div>{{row.entity.PPnNPWP}}</div>'
			},
			{
				name: "Tarif Pajak (%) (tanpa NPWP)", field: "PPnNonNPWP", cellFilter: "noSymbol",
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				},
				editableCellTemplate: '<input ng-dblclick="alert(9999)" type="text" ng-maxlength="5" maxlength="5" ui-grid-editor percent-allow-zero ng-model="row.entity.PPnNonNPWP">',
				cellTemplate: '<div>{{row.entity.PPnNonNPWP}}</div>'
			},
		],
		onRegisterApi: function (gridApi) {
			$scope.PPhProgresifGridApi = gridApi;
		}
	};
	//=============================UI Grid End===============================//


});

app.filter('noSymbol', function () {
	return function (val) {
		val = val.toString().replace(/([^0-9.])*/gi, '');
		return val;
	};
});


app.filter('rupiahC', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			var valS = val.toString().split('.');
			val = valS[0];
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			if (valS.length > 1) {
				val = val + ',' + valS[1];
			}

		}
		return val;
	};
});