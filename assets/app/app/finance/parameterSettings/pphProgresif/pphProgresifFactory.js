angular.module('app')
	.factory('PPhProgresifFactory', function ($http, CurrentUser) {
	return{
		getDataMaster: function(){
			var url = '/api/fe/FinancePPh21Progresif/GetMasterData';
			console.log("url master data  : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataDetail: function(){
			var url = '/api/fe/FinancePPh21Progresif/GetDetailData';
			console.log("url detail data  : ", url);
			var res=$http.get(url);
			return res;
		},
		
		createMaster : function(inputData){
			var url = '/api/fe/FinancePPh21Progresif/createmaster/';
			var param = JSON.stringify(inputData);
			console.log("object input Data ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		createDetail : function(inputData){
			var url = '/api/fe/FinancePPh21Progresif/createdetail/';
			var param = JSON.stringify(inputData);
			console.log("object input Data ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});