angular.module('app')
	.factory('PeriodeReversalFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/financereversalperiod/getdata';
			console.log("url periode reversal : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/financereversalperiod/create/';
			var param = JSON.stringify(inputData);
			console.log("object input periode reversal ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});