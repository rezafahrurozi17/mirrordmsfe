var app = angular.module('app');
app.controller('PeriodeReversalController', function ($scope, $http, $filter, CurrentUser, PeriodeReversalFactory, $timeout, bsNotify) {
	
	PeriodeReversalFactory.getData()
	.then(
		function(res){
			console.log("res : ", res);
			$scope.PeriodeReversal_PeriodeReversalParamId = res.data.Result[0].ParameterDetailId;
			$scope.PeriodeReversal_PeriodeReversal = res.data.Result[0].ReversalPeriod;
			$scope.PeriodeReversalBulanSamaId = res.data.Result[1].ParameterDetailId;
			if(res.data.Result[1].ReversalPeriod == 1)
				$scope.PeriodeReversal_BulanSama = true;
			else
				$scope.PeriodeReversal_BulanSama = false;
			$scope.PeriodeReversalBackdatePembukuanParamId = res.data.Result[2].ParameterDetailId;
			$scope.PeriodeReversal_BackdatePembukuan = res.data.Result[2].ReversalPeriod;
		}
	);
	
	$scope.PeriodeReversal_Simpan_Clicked = function(){
		var BulanYangSama = 0;
		if($scope.PeriodeReversal_BulanSama)
			BulanYangSama = 1;
		
		if ($scope.PeriodeReversal_PeriodeReversalParamId == undefined)
			$scope.PeriodeReversal_PeriodeReversalParamId = -1;
		
		if ($scope.PeriodeReversalBulanSamaId == undefined)
			$scope.PeriodeReversalBulanSamaId = -2;
		
		if ($scope.PeriodeReversalBackdatePembukuanParamId == undefined)
			$scope.PeriodeReversalBackdatePembukuanParamId = -3;
		
		var createData = [
			{"ParameterDetailId":$scope.PeriodeReversal_PeriodeReversalParamId, "ReversalPeriod":$scope.PeriodeReversal_PeriodeReversal},
			{"ParameterDetailId":$scope.PeriodeReversalBulanSamaId, "ReversalPeriod":BulanYangSama},
			{"ParameterDetailId":$scope.PeriodeReversalBackdatePembukuanParamId, "ReversalPeriod":$scope.PeriodeReversal_BackdatePembukuan}
		];
		debugger;
		PeriodeReversalFactory.create(createData)
		.then(
			function(res){
				// alert("Data berhasil disimpan");
				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				PeriodeReversalFactory.getData()
				.then(
					function(res){
						console.log("res : ", res);
						$scope.PeriodeReversal_PeriodeReversalParamId = res.data.Result[0].ParameterDetailId;
						$scope.PeriodeReversal_PeriodeReversal = res.data.Result[0].ReversalPeriod;
						$scope.PeriodeReversalBulanSamaId = res.data.Result[1].ParameterDetailId;
						if(res.data.Result[1].ReversalPeriod == 1)
							$scope.PeriodeReversal_BulanSama = true;
						else
							$scope.PeriodeReversal_BulanSama = false;
						$scope.PeriodeReversalBackdatePembukuanParamId = res.data.Result[2].ParameterDetailId;
						$scope.PeriodeReversal_BackdatePembukuan = res.data.Result[2].ReversalPeriod;
					}
				);
			},
			function(err){
				console.log("Preprinted Kuitansi Error : ", err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
			}
		)
	}
});