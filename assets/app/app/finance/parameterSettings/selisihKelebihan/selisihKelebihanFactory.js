angular.module('app')
	.factory('SelisihKelebihanFactory', function ($http, CurrentUser) {
	return{
		// getData: function(){
		// 	var url = '/api/fe/financeexcessdifference/getdata';
		// 	console.log("url excess difference : ", url);
		// 	var res=$http.get(url);
		// 	return res;
		// },

		getData: function(){
			var url = '/api/fe/FinanceOIOE/GetData';
			//console.log("url excess difference : ", url);
			var res=$http.get(url);
			return res;
		},

		
		
		// create : function(inputData){
		// 	var url = '/api/fe/financeexcessdifference/create/';
		// 	var param = JSON.stringify(inputData);
		// 	console.log("object input excess difference ", param);
		// 	var res=$http.post(url, param);
		// 	return res;
		// }

		create : function(inputData){
			console.log('factory',inputData)
			return $http.post('/api/fe/FinanceOIOE/Create', inputData);
		}
	}
});