var app = angular.module('app');
app.controller('SelisihKelebihanController', function ($scope, $http, $filter, CurrentUser, SelisihKelebihanFactory, $timeout, bsNotify) {
	
	$scope.NilaiMaksOE = 0;
	$scope.NilaiMaksOI = 0;
	// $scope.insertOE = null;
	// $scope.insertOI = null;




	SelisihKelebihanFactory.getData()
	.then(
		function(res){
			console.log('res',res)
			// $scope.ParameterDetailId = res.data.Result[0].ParameterDetailId;
			// $scope.SelisihKelebihan_NilaiMinimalSelisih = res.data.Result[0].ExcessDifferenceAmount;
			$scope.GridOE.data = res.data.FinanceOEModel;
			$scope.GridOI.data = res.data.FinanceOIModel;
		}
	);
	
	
	$scope.tambahNilaiOE = function() {
		$scope.insertOE = [];
		$scope.OEtoPush = {};
		$scope.OEtoPush.MaxAmount = angular.copy($scope.NilaiMaksOE);
		// $scope.OEtoPush.ParameterDetailId = angular.copy($scope.GridOE.data[0].ParameterDetailId);
		// $scope.OEtoPush.ParameterMasterId = angular.copy($scope.GridOE.data[0].ParameterMasterId);
		// $scope.OEtoPush.ParameterMasterName = angular.copy($scope.GridOE.data[0].ParameterMasterName);
		console.log('push',$scope.OEtoPush);
		$scope.GridOE.data.push($scope.OEtoPush);
		$scope.insertOE.push($scope.OEtoPush);
		console.log('insertOE',$scope.insertOE);
		$scope.NilaiMaksOE = 0;
	}

	$scope.tambahNilaiOI = function() {
		$scope.insertOI = [];
		$scope.OItoPush = {};
		$scope.OItoPush.MaxAmount = angular.copy($scope.NilaiMaksOI);
		// $scope.OItoPush.ParameterDetailId = angular.copy($scope.GridOI.data[0].ParameterDetailId);
		// $scope.OItoPush.ParameterMasterId = angular.copy($scope.GridOI.data[0].ParameterMasterId);
		// $scope.OItoPush.ParameterMasterName = angular.copy($scope.GridOI.data[0].ParameterMasterName);
		console.log('push',$scope.OItoPush);
		$scope.GridOI.data.push($scope.OItoPush);
		$scope.insertOI.push($scope.OItoPush);
		console.log('insertOE',$scope.insertOI);
		$scope.NilaiMaksOI = 0;
	}

	$scope.LoadingSimpan = false;
	$scope.simpanOIOE = function() {
		$scope.insertOIOE = {};
		$scope.LoadingSimpan = true;
		
		$scope.insertOIOE.FinanceOEModel = $scope.insertOE;
		$scope.insertOIOE.FinanceOIModel = $scope.insertOI;

		if(typeof $scope.insertOIOE.FinanceOEModel != 'undefined'){
			$scope.insertOIOE.FinanceOEModel = $scope.insertOE;
		}else{
			$scope.insertOIOE.FinanceOEModel=[];
		}

		if(typeof $scope.insertOIOE.FinanceOIModel != 'undefined'){
			$scope.insertOIOE.FinanceOIModel = $scope.insertOI;
		}else{
			$scope.insertOIOE.FinanceOIModel = [];
		}
		
		console.log('disimpan',$scope.insertOIOE);
		SelisihKelebihanFactory.create($scope.insertOIOE)
		.then(
			function(res){
				// alert("Data berhasil disimpan");

				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				$scope.LoadingSimpan = false;

				SelisihKelebihanFactory.getData()
				.then(
					function(res){
						$scope.GridOE.data = res.data.FinanceOEModel;
						$scope.GridOI.data = res.data.FinanceOIModel;
					}
				);
				$scope.insertOIOE = {};
				$scope.insertOI = [];
				$scope.OItoPush = {};
				$scope.insertOE = [];
				$scope.OEtoPush = {};
			},
			function(err){
				console.log("Selisih Kelebihan Error : ", err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
				$scope.LoadingSimpan = false;
			}
		)
	

	}
	
	$scope.SelisihKelebihan_Simpan_Clicked = function(){
		if($scope.ParameterDetailId == undefined)
			$scope.ParameterDetailId = -1;
		
		var inputData = [{"ParameterDetailId":$scope.ParameterDetailId, "ExcessDifferenceAmount":$scope.SelisihKelebihan_NilaiMinimalSelisih}];
		SelisihKelebihanFactory.create(inputData)
		.then(
			function(res){
				// alert("Data berhasil disimpan");

				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);

				SelisihKelebihanFactory.getData()
				.then(
					function(res){
						$scope.ParameterDetailId = res.data.Result[0].ParameterDetailId;
						$scope.SelisihKelebihan_NilaiMinimalSelisih = res.data.Result[0].ExcessDifferenceAmount;
					}
				);
			},
			function(err){
				console.log("Selisih Kelebihan Error : ", err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
			}
		)
	}
	
	//=============================UI Grid Begin===============================//
	$scope.PreprintedKuitansi_UIGrid = {
		data: $scope.PreprintedKuitansi_Data,
		columnDefs: [
						{name:"ParameterDetailId",field:"ParameterDetailId",visible:false},
						{name:"Tipe Bisnis",field:"BusinessType"},
						{name:"Preprinted Kuitansi", field:"isPreprintedReceipt", type: 'boolean',cellTemplate: '<input type="checkbox" ng-model="row.entity.isPreprintedReceipt">' },
					],
		onRegisterApi: function(gridApi) {
			$scope.PreprintedKuitansiGridApi = gridApi;
		}
	};

	$scope.GridOE = {
		// enableSorting: true,
		// enableRowSelection: true,
		// multiSelect: false,
		// enableSelectAll: false,
		// enableColumnResizing: true,
		//showTreeExpandNoChildren: true,
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,

		//data: 'dataBelumBilling',
		columnDefs: [
			{ displayName: 'Tanggal awal', name: 'Tgl Awal', field: 'StartDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
			{ displayName: 'Tanggal Akhir', name: 'Tgl Akhir', field: 'EndDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
			{ displayName: 'Nilai Maksimal', name: 'Max Amount', field: 'MaxAmount'}
		]
	};

	$scope.GridOI = {
		// enableSorting: true,
		// enableRowSelection: true,
		// multiSelect: false,
		// enableSelectAll: false,
		// enableColumnResizing: true,
		//showTreeExpandNoChildren: true,
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,

		//data: 'dataBelumBilling',
		columnDefs: [
			{ displayName: 'Tanggal awal', name: 'Tgl Awal', field: 'StartDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
			{ displayName: 'Tanggal Akhir', name: 'Tgl Akhir', field: 'EndDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
			{ displayName: 'Nilai Maksimal', name: 'Max Amount', field: 'MaxAmount' }
		]
	};
	//=============================UI Grid End===============================//
});