angular.module('app')
	.factory('ParamAnggaranBelanjaFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	return{
		getData: function(){
			var url = '/api/fe/financeisbudget/getdata/?OutletId=' + user.OutletId;
			console.log("url excess difference : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/financeisbudget/create/';
			var param = JSON.stringify(inputData);
			console.log("object input excess difference ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});