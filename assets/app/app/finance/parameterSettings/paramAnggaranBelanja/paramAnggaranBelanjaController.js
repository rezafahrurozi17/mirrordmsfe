var app = angular.module('app');
app.controller('ParamAnggaranBelanjaController', function ($scope, $http, $filter, CurrentUser, ParamAnggaranBelanjaFactory, $timeout,bsNotify) {
	
	ParamAnggaranBelanjaFactory.getData()
	.then(
		function(res){
			$scope.ParameterDetailId = res.data.Result[0].ParameterDetailId;
			if(res.data.Result[0].isBudget == 1)
				$scope.AnggaranBelanja_isEnable = true;
			else
				$scope.AnggaranBelanja_isEnable = false;
		}
	);
	
	$scope.LoadingSimpan = false;
	$scope.AnggaranBelanja_Simpan_Clicked = function(){
		var isAnggaranBelanja = 0;
		$scope.LoadingSimpan = true;
		
		if($scope.AnggaranBelanja_isEnable)
			isAnggaranBelanja = 1
		
		var inputData = [{"ParameterDetailId":$scope.ParameterDetailId, "isBudget":isAnggaranBelanja}];
		
		ParamAnggaranBelanjaFactory.create(inputData)
		.then(
			function(res){
				//alert("Data berhasil disimpan");
				bsNotify.show({
					title: "Berhasil",
					content: 'Data berhasil disimpan.',
					type: 'Success'
				});
				$scope.LoadingSimpan = false;
				ParamAnggaranBelanjaFactory.getData()
				.then(
					function(res){
						$scope.ParameterDetailId = res.data.Result[0].ParameterDetailId;
						if(res.data.Result[0].isBudget == 1)
							$scope.AnggaranBelanja_isEnable = true;
						else
							$scope.AnggaranBelanja_isEnable = false;
					}
				);
			},
			function(err){
				console.log("Selisih Kelebihan Error : ", err);
				$scope.LoadingSimpan = false;
			}
		)
	}
});