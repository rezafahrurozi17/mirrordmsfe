angular.module('app')
	.factory('FinancePajakFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
		
	return{

		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},
		
		getDataNomorPajak: function(Start, Limit, periodMonth, periodYear, outletId) {
			//var url = '/api/fe/FinanceNomorPajak/start/'+Start+'/limit/'+Limit+'/periodmonth/'+periodMonth+'/periodyear/' + periodYear;
			var url = '/api/fe/FinanceNomorPajak/?start='+Start+'&limit='+Limit+'&periodmonth='+periodMonth+'&periodyear=' + periodYear+'&OutletId='+outletId;
			console.log("getDataNomorPajak url :", url);
			var res=$http.get(url);
			return res;
		},
		
		// filter
		getFilterDataNomorPajak: function(Start, Limit, periodMonth, periodYear, outletId, filterdata, flag) {
			//var url = '/api/fe/FinanceNomorPajak/start/'+Start+'/limit/'+Limit+'/periodmonth/'+periodMonth+'/periodyear/' + periodYear;
			var url = '/api/fe/FinanceNomorPajak/?start='+Start+'&limit='+Limit+'&periodmonth='+periodMonth+'&periodyear=' + periodYear+'&OutletId='+outletId +'&FilterData=' +filterdata;
			console.log("getDataNomorPajak url :", url);
			var res=$http.get(url);
			return res;
		},

		deleteDataNomorPajak: function(taxInvoiceId, outletId) {
			var url = '/api/fe/FinanceNomorPajak/deletedata/?invoiceid=' + taxInvoiceId + '&OutletId=' + outletId;
			console.log("deleteDataNomorPajak url :", url);
			var res=$http.delete(url);
			return res;
		},
		
		createNomorPajakData : function(inputData){
			var url = '/api/fe/FinanceNomorPajak/submitdata/';
			var param = JSON.stringify(inputData);
			console.log("object input nomorpajakdata ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});