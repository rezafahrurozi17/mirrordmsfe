var app = angular.module('app');
app.controller('FinancePajakController', function ($scope, $http, $filter, uiGridConstants, CurrentUser, FinancePajakFactory, $timeout, bsNotify) {
	// $scope.Show_MainPajak = true;
	var user = CurrentUser.user();
	$scope.Show_LihatPajak = true;
	$scope.dateOptionsYear =  {
            startingDay: 1,
            format: 'yyyy',
            // disableWeekend: 1
		};
	$scope.dateOptionsMonth =  {
		startingDay: 1,
		format: ' MMM',
		// disableWeekend: 1
	};

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.Tambah = checkRbyte($scope.ByteEnable, 1);
	console.log("cek right",$scope.ByteEnable);
	console.log("cek Tambah",$scope.Tambah);
	if($scope.Tambah == true){
		$scope.Show_Tambah = true;
	}else{
		$scope.Show_Tambah = false;
	}
		

	if(user.OrgCode.substring(3, 9) == '000000')
		$scope.Pajak_HO_Show = false;
	else
		$scope.Pajak_HO_Show = true;

	$scope.FinancePajak_getDataBranch = function ()
	{
		FinancePajakFactory.getDataBranchComboBox()
		.then
		(
			function (res)
			{
				$scope.optionsLihatPajak_SelectBranch = res.data;
				$scope.optionsTambahPajak_SelectBranch = res.data;
				console.log("user.OutletId", user.OutletId);
				console.log("res data branch", res.data);

				// res.data.some(function(obj, i){
					// if(obj.value == user.OutletId)
						// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
				// });
				//$scope.MainInvoiceMasuk_SelectBranch = user.OutletId;
				$scope.TambahPajak_SelectBranch = user.OutletId;
				$scope.LihatPajak_SelectBranch = user.OutletId;
				console.log("$scope.MainInvoiceMasuk_SelectBranch : ", $scope.MainInvoiceMasuk_SelectBranch);

				res.data.some(function(obj, i){
					if(obj.value == user.OutletId){
						$scope.TambahPajak_MainNamaBranch = obj.name;
						$scope.LihatPajak_MainNamaBranch = obj.name;
					}
				});
			},
			function (err)
			{
				console.log("err=>", err);
			}
		);
	};

	$scope.FinancePajak_getDataBranch();

	$scope.ToTambahPajak_Clicked = function(){
		$scope.Show_MainPajak = false;
		$scope.Show_TambahPajak = true;
		$scope.Show_Simpan = true;
		$scope.EditPajak = false;
		$scope.Show_LihatPajak = false;
		$scope.TambahPajak_InvoiceId = 0;
	}

	$scope.ToLihatPajak_Clicked = function(){
		$scope.Show_MainPajak = false;
		$scope.Show_TambahPajak = false;
		$scope.Show_LihatPajak = true;
	}

	$scope.ToMainPajak = function(){
		$scope.Show_MainPajak = true;
		$scope.Show_TambahPajak = false;
		$scope.Show_LihatPajak = false;
	}

	$scope.TambahPajak_Batal_Clicked = function(){
		$scope.ClearFields();
		$scope.Show_TambahPajak = false;
		$scope.Show_LihatPajak = true;
		if(user.OrgCode.substring(3, 9) == '000000')
			$scope.Pajak_HO_Show = false;
		else
			$scope.Pajak_HO_Show = true;
	}

	// $scope.TambahPajak_Simpan_Clicked = function(){
	// 	$scope.SetDatatoSave();
	// 	FinancePajakFactory.createNomorPajakData($scope.DataToSave)
	// 	.then(
	// 		function(res){
	// 			debugger;
	// 			bsNotify.show({
	// 				title:"Berhasil",
	// 				content:"Data berhasil disimpan.",
	// 				type:"success"
	// 			});

	// 			// alert("Data berhasil disimpan");
	// 			$scope.ToLihatPajak_Clicked();
	// 			$scope.Show_NomorAkhirDigunakan = false;
	// 			$scope.Show_Simpan = false;
	// 			$scope.ClearFields();
	// 			$scope.LihatPajak_Cari_Clicked();
	// 			//$scope.NomorPajak_UIGrid_Paging(1);
	// 		},
	// 		function (err) {
	// 			console.log("err=>", err);
	// 			if (err != null) {
	// 				bsNotify.show({
	// 					title: "Error",
	// 					content: err.data.Message,
	// 					type: 'danger'
	// 				});
	// 			}
	// 		}
	// 	);
	// }
	
	$scope.ModalDisplayNomorPajak = function (){
		var no_awal = $scope.TambahPajak_NomorAwal
		var no_akhir = $scope.TambahPajak_NomorAkhir
		$scope.NomorAwalDisplay = no_awal.substring(0,3) + "." + no_awal.substring(3,5) + "." + no_awal.substring(5);
		$scope.NomorAkhirDisplay = no_akhir.substring(0,3) + "." + no_akhir.substring(3,5) + "." + no_akhir.substring(5);

		var no_head = (Number(no_akhir.substring(0,3)) - Number(no_awal.substring(0,3))).toString()
		var no_akhir_head = no_head.concat(no_akhir.substring(5))
		$scope.JumlahNomor = Number(no_akhir_head) - Number(no_awal.substring(5)) + 1

		if ($scope.EditPajak == false) {
			$scope.NomorAkhirDigunakanDisplay = "-";
		}
		else {
			var no_akhir_d = $scope.TambahPajak_NomorAkhirDigunakan
			$scope.NomorAkhirDigunakanDisplay = no_akhir_d.substring(0,3) + "." + no_akhir_d.substring(3,5) + "." + no_akhir_d.substring(5);
		}
	}

	$scope.TambahPajak_Simpan_Clicked = function(){
		$scope.ModalTanggalBooking = $filter('date')(new Date($scope.TambahPajak_TanggalBooking), 'dd/MM/yyyy');
		$scope.ModalBulanPajak = $filter('date')(new Date($scope.TambahPajak_BulanPeriodPajak), 'MM');
		$scope.ModalTahunPajak = $filter('date')(new Date($scope.TambahPajak_TahunPeriodPajak), 'yyyy');
		$scope.ModalDisplayNomorPajak();
		if ($scope.EditPajak == false) {
			angular.element('#ModalTambahNomorPajak').modal('show');
		}
		else {
			angular.element('#ModalEditNomorPajak').modal('show');
		}
	}

	$scope.LihatPajak_DaftarNomorFakturPajak = true;
	$scope.LihatPajak_Cari_Clicked = function(){
		//$scope.LihatPajak_DaftarNomorFakturPajak = true;
		$scope.NomorPajak_UIGrid_Paging(1);
	}

	$scope.Batal_ModalTambahNomorPajak = function () {
		angular.element('#ModalTambahNomorPajak').modal('hide');
	};

	$scope.Tambah_ModalTambahNomorPajak = function(){
		$scope.SetDatatoSave();
		FinancePajakFactory.createNomorPajakData($scope.DataToSave)
		.then(
			function(res){
				debugger;
				bsNotify.show({
					title:"Berhasil",
					content:"Data berhasil disimpan.",
					type:"success"
				});

				// alert("Data berhasil disimpan");
				$scope.ToLihatPajak_Clicked();
				$scope.Show_NomorAkhirDigunakan = false;
				$scope.Show_Simpan = false;
				$scope.ClearFields();
				$scope.LihatPajak_Cari_Clicked();
				//$scope.NomorPajak_UIGrid_Paging(1);
			},
			function (err) {
				console.log("err=>", err);
				if (err != null) {
					bsNotify.show({
						title: "Error",
						content: err.data.Message,
						type: 'danger'
					});
				}
			}
		);
	}

	$scope.ToEditPajak = function(dataPajak){
		var periodPajak;
		$scope.EditPajak = true;
		$scope.Show_NomorAkhirDigunakan = true;
		$scope.Show_Simpan = true;
		periodPajak = dataPajak.TaxPeriodYear + '-' + ('00' + dataPajak.TaxPeriodMonth).slice(-2) + '-01';
		console.log("dataPajak = ", dataPajak);
		$scope.Show_LihatPajak = false;
		$scope.Show_TambahPajak = true;
		$scope.TambahPajak_InvoiceId = dataPajak.TaxInvoiceId;
		$scope.TambahPajak_TanggalBooking = dataPajak.BookingDate.setMinutes(dataPajak.BookingDate.getMinutes() + 420);
		$scope.TambahPajak_BulanPeriodPajak = new Date(periodPajak);
		$scope.TambahPajak_TahunPeriodPajak = new Date(periodPajak);
		$scope.TambahPajak_NomorAwal = dataPajak.BeginNo;
		$scope.TambahPajak_NomorAkhir = dataPajak.EndNo;
		$scope.TambahPajak_NomorAkhirDigunakan = dataPajak.MaxTaxNumber;
		$scope.TambahPajak_SelectBranch = dataPajak.OutletId;
		$scope.TambahPajak_MainNamaBranch = dataPajak.OutletName;
		$scope.Pajak_HO_Show = true;
	}

	$scope.Batal_ModalEditNomorPajak = function () {
		angular.element('#ModalEditNomorPajak').modal('hide');
	};

	$scope.ToHapusPajak = function(dataPajak){
		$scope.LihatPajak_NomorAwal = dataPajak.BeginNo;
		$scope.LihatPajak_NomorAkhir = dataPajak.EndNo;
		$scope.TambahPajak_InvoiceId = dataPajak.TaxInvoiceId;
		angular.element('#ModalHapusNomorPajak').modal('show');
	}

	$scope.Batal_ModalHapusNomorPajak = function () {
		angular.element('#ModalHapusNomorPajak').modal('hide');
	};

	$scope.Hapus_ModalHapusNomorPajak = function(){
		FinancePajakFactory.deleteDataNomorPajak($scope.TambahPajak_InvoiceId, $scope.LihatPajak_SelectBranch)
		.then(
			function(res){
				$scope.TambahPajak_InvoiceId = 0;
				bsNotify.show({
					title: "Berhasil",
					content: "Data berhasil dihapus",
					type: 'success'
				});
				angular.element('#ModalHapusNomorPajak').modal('hide');
				//$scope.NomorPajak_UIGrid_Paging(1);
				$scope.LihatPajak_Cari_Clicked();
			},
			function(err){
				$scope.TambahPajak_InvoiceId = 0;
				bsNotify.show({
					title: "Gagal",
					content: err.data.Message,
					type: 'danger'
				});
				angular.element('#ModalHapusNomorPajak').modal('hide');
			}
		)
	}

	$scope.SetDatatoSave = function(){
		console.log("Tanggal booking : ", $scope.TambahPajak_TanggalBooking );
		$scope.DataToSave = [
			{
				"TaxInvoiceId" : $scope.TambahPajak_InvoiceId,
				"BookingDate" : $filter('date')(new Date($scope.TambahPajak_TanggalBooking), 'yyyy-MM-dd'),
				"PeriodMonth" : $filter('date')(new Date($scope.TambahPajak_BulanPeriodPajak), 'MM'),
				"PeriodYear": $filter('date')(new Date($scope.TambahPajak_TahunPeriodPajak), 'yyyy'),
				"BeginNo": $scope.TambahPajak_NomorAwal,
				"EndNo": $scope.TambahPajak_NomorAkhir,
				"OutletId" : $scope.TambahPajak_SelectBranch
			}
		];
	}

	$scope.ClearFields = function(){
		$scope.TambahPajak_TanggalBooking = "";
		$scope.TambahPajak_InvoiceId = "";
		$scope.TambahPajak_BulanPeriodPajak = "";
		$scope.TambahPajak_TahunPeriodPajak = "";
		$scope.TambahPajak_NomorAwal = "";
		$scope.TambahPajak_NomorAkhir = "";
		$scope.TambahPajak_NomorAkhirDigunakan = "";

	}
	//===================================Filter section begin===================================//
	$scope.optionsLihatFinancePajak_SelectKolom = [{name:"Tanggal Booking", value:"Tanggal Booking"}, {name:"Bulan Periode Pajak", value:"Bulan Periode Pajak"}, {name:"Tahun Periode Pajak", value:"Tahun Periode Pajak"}];

	// $scope.LihatFinancPajak_FilterKolom_Changed = function(){
	// 	$scope.LihatFinancePajak_TextFilter = "";
	// }

	// OLD FILTER
	// $scope.LihatFinancePajak_Filter_Clicked = function(){
	// 	var nomor;
	// 	$scope.GridApiNomorPajak.grid.clearAllFilters();
	// 	$scope.GridApiNomorPajak.grid.refresh();

	// 	if ($scope.LihatFinancePajak_SelectKolom == 'Tanggal Booking')
	// 		nomor = 1;
	// 	else if ($scope.LihatFinancePajak_SelectKolom == 'Bulan Periode Pajak')
	// 		nomor = 2;
	// 	else if ($scope.LihatFinancePajak_SelectKolom == 'Tahun Periode Pajak')
	// 		nomor = 3
	// 	else
	// 		nomor = 0

	// 	console.log("nomor dan filter", nomor, $scope.LihatFinancePajak_TextFilter);
	// 	if(nomor != 0 && $scope.LihatFinancePajak_TextFilter != "")
	// 	{
	// 		$scope.GridApiNomorPajak.grid.columns[nomor].filters[0].term = $scope.LihatFinancePajak_TextFilter;
	// 	}

	// }

	// NEW FILTER 
	$scope.LihatFinancePajak_Filter_Clicked = function(){		
		$scope.GetFilter();
	}
	
	$scope.GetFilter = function(){
		var tmpFlag = 0;
		var BulanPeriodPajak = 0;
		var TahunPeriodPajak = 0;

		if(!angular.isUndefined($scope.LihatPajak_BulanPeriodPajak) && $scope.LihatPajak_BulanPeriodPajak != null)
			BulanPeriodPajak = $filter('date')(new Date($scope.LihatPajak_BulanPeriodPajak.toString()), 'MM');
		if(!angular.isUndefined($scope.LihatPajak_BulanPeriodPajak) && $scope.LihatPajak_TahunPeriodPajak != null)
			TahunPeriodPajak = $filter('date')(new Date($scope.LihatPajak_TahunPeriodPajak.toString()), 'yyyy');

		if($scope.LihatFinancePajak_SelectKolom != '' || $scope.LihatFinancePajak_SelectKolom != undefined){
			FinancePajakFactory.getFilterDataNomorPajak(1, paginationOptions_financePajak.pageSize, BulanPeriodPajak, TahunPeriodPajak, $scope.LihatPajak_SelectBranch, 
				$scope.LihatFinancePajak_TextFilter +'|'+ $scope.LihatFinancePajak_SelectKolom)
			.then(
				function(res){
					$scope.NomorPajak_UIGrid.data = res.data.Result;
					$scope.NomorPajak_UIGrid.totalItems = res.data.Total;
					$scope.NomorPajak_UIGrid.paginationPageSize = paginationOptions_financePajak.pageSize;
					$scope.NomorPajak_UIGrid.paginationPageSizes = [10,25,50];
					$scope.NomorPajak_UIGrid.paginationCurrentPage = paginationOptions_financePajak.pageNumber;
				}
			);
		}else{
			FinancePajakFactory.getFilterDataNomorPajak(1, paginationOptions_financePajak.pageSize, BulanPeriodPajak, TahunPeriodPajak, $scope.LihatPajak_SelectBranch, 
				$scope.LihatFinancePajak_TextFilter +'|'+ $scope.LihatFinancePajak_SelectKolom)
			.then(
				function(res){
					$scope.NomorPajak_UIGrid.data = res.data.Result;
					$scope.NomorPajak_UIGrid.totalItems = res.data.Total;
					$scope.NomorPajak_UIGrid.paginationPageSize = paginationOptions_financePajak.pageSize;
					$scope.NomorPajak_UIGrid.paginationPageSizes = [10,25,50];
					$scope.NomorPajak_UIGrid.paginationCurrentPage = paginationOptions_financePajak.pageNumber;
				}
			);
		}
	}
	
	/*
	$scope.singleFilter = function( renderableRows ){
		console.log("singleFIlter", $scope.LihatFinancePajak_SelectKolom);
		if($scope.LihatFinancePajak_SelectKolom != null && $scope.LihatFinancePajak_TextFilter != "")
		{
			var matcher = new RegExp($scope.LihatFinancePajak_TextFilter);
			renderableRows.forEach( function( row ) {
				var match = false;
				//['BookingDate', 'TaxPeriodMonth', 'TaxPeriodYear'].forEach(function( field ){
					//console.log("row.entity", row.entity[field]);
					if (row.entity[$scope.LihatFinancePajak_SelectKolom] == $scope.LihatFinancePajak_TextFilter ){
						match = true;
					}
				//});
				if ( !match ){
					row.visible = false;
				}
			});
		}
		return renderableRows;
	};*/
	//===================================Filter section end===================================//

	//===================================UI Grid Nomor Pajak begin===================================//
	var paginationOptions_financePajak = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	var uiGridPageSize = 10;
	$scope.NomorPajak_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination : true,
		enableFiltering: true,
		columnDefs: [
						{name:"TaxInvoiceId",field:"TaxInvoiceId",visible:false},
						{name:"Tanggal Booking",field:"BookingDate", cellFilter:'date:\"dd-MM-yyyy\"', width:180},
						{name:"Bulan Periode Pajak",field:"TaxPeriodMonth", width:180},
						{name:"Tahun Periode Pajak",field:"TaxPeriodYear", width:180},
						{name:"Nomor Awal",field:"BeginNo", width:180},
						{name:"Nomor Akhir",field:"EndNo", width:180},
						{name:"Nomor Terakhir Digunakan",field:"MaxTaxNumber", width:200},
						{name:"Sisa",field:"LeftNumber", width:180},
						{name:"OutletId", field:"OutletId",visible:false},
						{name:"OutletName", field:"OutletName",visible:false},
						{name:"Action", width:130, pinnedRight:true,
							cellTemplate:'<div><a ng-click="grid.appScope.ToEditPajak(row.entity)">&nbsp;Edit</a>   <a ng-click="grid.appScope.ToHapusPajak(row.entity)">Hapus</a></div>'},
					],
		onRegisterApi: function(gridApi) {
			$scope.GridApiNomorPajak = gridApi;
			//$scope.GridApiNomorPajak.grid.registerRowsProcessor( $scope.singleFilter, 200 );
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_financePajak.pageNumber = pageNumber;
				paginationOptions_financePajak.pageSize = pageSize;

				$scope.NomorPajak_UIGrid.data = $scope.NomorPajak_UIGrid_Paging();
			}
			);
		}
	};

	$scope.NomorPajak_UIGrid_Paging = function(){
		var BulanPeriodPajak = 0;
		var TahunPeriodPajak = 0;

		if(!angular.isUndefined($scope.LihatPajak_BulanPeriodPajak) && $scope.LihatPajak_BulanPeriodPajak != null)
			BulanPeriodPajak = $filter('date')(new Date($scope.LihatPajak_BulanPeriodPajak.toString()), 'MM');
		if(!angular.isUndefined($scope.LihatPajak_BulanPeriodPajak) && $scope.LihatPajak_TahunPeriodPajak != null)
			TahunPeriodPajak = $filter('date')(new Date($scope.LihatPajak_TahunPeriodPajak.toString()), 'yyyy');

		FinancePajakFactory.getDataNomorPajak(paginationOptions_financePajak.pageNumber, paginationOptions_financePajak.pageSize,
												BulanPeriodPajak,
												TahunPeriodPajak,
												$scope.LihatPajak_SelectBranch
											)
	  .then(
		function(res)
		{
			console.log("Get data nomor pajak", res.data.result);
			$scope.NomorPajak_UIGrid.data = res.data.Result;
			$scope.NomorPajak_UIGrid.totalItems = res.data.Total;
			$scope.NomorPajak_UIGrid.paginationPageSize = paginationOptions_financePajak.pageSize;
			$scope.NomorPajak_UIGrid.paginationPageSizes = [10,25,50];
			$scope.NomorPajak_UIGrid.paginationCurrentPage = paginationOptions_financePajak.pageNumber;
		}

	  );
	}

	//===================================UI Grid Nomor Pajak end===================================//

	$scope.TanggalBookingOK = true;
	$scope.TanggalBookingChanged = function () {
		// IncomingPaymentFactory.getDataPeriodeReversal()
		// 	.then(
		// 		function (res) {
		// 			$scope.loading = false;
		// 			angular.forEach(res.data.Result, function (value, key) {
		// 				if (value.ReversalName == "Backdate Pembukuan") {
		// 					var backdate = new Date().getTime() - (value.ReversalPeriod * 24 * 60 * 60 * 1000)
		// 					$scope.backdatePeriod = value.ReversalPeriod;
		// 					$scope.backdate = backdate;
		// 				}
		// 			});

		// 		},
		// 		function (err) {
		// 			console.log("err=>", err);
		// 		}
		// 	);
		// console.log($scope.TanggalBookingOK);
		// debugger;`
		var dt = $scope.TambahPajak_TanggalBooking;
		var now = new Date();
		// if (dt < $scope.backdate) {
		// 	$scope.errTanggalBooking = "Tanggal Booking tidak boleh lebih awal " + $scope.backdatePeriod + " hari dari tanggal hari ini";
		// 	$scope.TanggalBookingOK = false;
		// }
		// else
		if (dt > now) {
			$scope.errTanggalBooking = "Tanggal Booking tidak boleh lebih dari tanggal hari ini";
			$scope.TanggalBookingOK = false;
		}
		else {
			$scope.TanggalBookingOK = true;
		}
		// console.log('asdasd ' + $scope.TanggalBookingOK);
	}

	$scope.BulanPeriodOK = true;
	$scope.BulanPeriodChanged = function () {
		// IncomingPaymentFactory.getDataPeriodeReversal()
		// 	.then(
		// 		function (res) {
		// 			$scope.loading = false;
		// 			angular.forEach(res.data.Result, function (value, key) {
		// 				if (value.ReversalName == "Backdate Pembukuan") {
		// 					var backdate = new Date().getTime() - (value.ReversalPeriod * 24 * 60 * 60 * 1000)
		// 					$scope.backdatePeriod = value.ReversalPeriod;
		// 					$scope.backdate = backdate;
		// 				}
		// 			});

		// 		},
		// 		function (err) {
		// 			console.log("err=>", err);
		// 		}
		// 	);
		// console.log($scope.TanggalBookingOK);
		// debugger;`
		var dt = $scope.LihatPajak_BulanPeriodPajak;
		var dtMonth = dt.getMonth();
		var now = new Date();
		var newMonth = now.getMonth();

		if(dt != undefined)
		{
			if (dtMonth > newMonth) {
				$scope.errBulanPeriod = "Periode tidak boleh lebih dari bulan ini";
				$scope.BulanPeriodOK = false;
			}
			else {
				$scope.BulanPeriodOK = true;
			}
		}
		else {
			$scope.BulanPeriodOK = true;
		}
	}

	$scope.TahunPeriodOK = true;
	$scope.TahunPeriodChanged = function () {
		// IncomingPaymentFactory.getDataPeriodeReversal()
		// 	.then(
		// 		function (res) {
		// 			$scope.loading = false;
		// 			angular.forEach(res.data.Result, function (value, key) {
		// 				if (value.ReversalName == "Backdate Pembukuan") {
		// 					var backdate = new Date().getTime() - (value.ReversalPeriod * 24 * 60 * 60 * 1000)
		// 					$scope.backdatePeriod = value.ReversalPeriod;
		// 					$scope.backdate = backdate;
		// 				}
		// 			});

		// 		},
		// 		function (err) {
		// 			console.log("err=>", err);
		// 		}
		// 	);
		// console.log($scope.TanggalBookingOK);
		// debugger;`
		var dt = $scope.LihatPajak_TahunPeriodPajak;
		var dtYear = dt.getFullYear();
		var now = new Date();
		var newYear = now.getFullYear();

		if(dt != undefined)
		{
			if (dtYear > newYear) {
				$scope.errTahunPeriod = "Tahun tidak boleh lebih dari Tahun hari ini";
				$scope.TahunPeriodOK = false;
			}
			else {
				$scope.TahunPeriodOK = true;
			}
		}
		else {
			$scope.TahunPeriodOK = true;
		}
	}
});