angular.module('app')
	.factory('RefundCGFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/financerefundcg/getdata';
			console.log("url refund cg : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/financerefundcg/create/';
			var param = JSON.stringify(inputData);
			console.log("object input refund cg ", param);
			var res=$http.post(url, param);
			return res;
		}
	}
});