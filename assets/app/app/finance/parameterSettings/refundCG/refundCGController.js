var app = angular.module('app');
app.controller('RefundCGController', function ($scope, $http, $filter, CurrentUser, RefundCGFactory, $timeout, bsNotify) {
	
	$scope.GetData = function(){
		RefundCGFactory.getData()
		.then(
			function(res){
				console.log("res : ", res);
				if(res.data.Result.length != 0){
					var a = res.data.Result[0].RefundCGPercentage;
					var b = res.data.Result[1].RefundCGPercentage;
					var c = res.data.Result[2].RefundCGPercentage;
					$scope.RefundCG_RefundCGUnitParamId = res.data.Result[0].ParameterDetailId;
					$scope.RefundCG_RefundCGUnit = a.toString().replace('.', ',');
					$scope.RefundCG_RefundCGServiceParamId = res.data.Result[1].ParameterDetailId;
					$scope.RefundCG_RefundCGService = b.toString().replace('.', ',');
					$scope.RefundCG_RefundCGPartsParamId = res.data.Result[2].ParameterDetailId;
					$scope.RefundCG_RefundCGParts = c.toString().replace('.', ',');
				}
			}
		);
	}

	$scope.GetData();
	
	$scope.LoadingSimpan = false;
	$scope.RefundCG_Simpan_Clicked = function(){
		$scope.LoadingSimpan = true;
		if($scope.RefundCG_RefundCGUnitParamId == undefined)
			$scope.RefundCG_RefundCGUnitParamId = -1
		
		if($scope.RefundCG_RefundCGServiceParamId == undefined)
			$scope.RefundCG_RefundCGServiceParamId = -2
		
		if($scope.RefundCG_RefundCGPartsParamId == undefined)
			$scope.RefundCG_RefundCGPartsParamId = -3
		
		var submitOK = true;
		if($scope.RefundCG_RefundCGUnit == '' || $scope.RefundCG_RefundCGUnit == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Refund CG Unit harus diisi",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
			submitOK = false;
		}

		if($scope.RefundCG_RefundCGService == '' || $scope.RefundCG_RefundCGService == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Refund CG Service harus diisi",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
			submitOK = false;
		}

		if($scope.RefundCG_RefundCGParts == '' || $scope.RefundCG_RefundCGParts == null){
			bsNotify.show(
				{
					title: "Warning",
					content: "Refund CG Parts harus diisi",
					type: 'warning'
				}
			);
			$scope.LoadingSimpan = false;
			submitOK = false;
		}

		if(!submitOK) return submitOK;
		
		var createData = [
			{"ParameterDetailId":$scope.RefundCG_RefundCGUnitParamId, "RefundCGPercentage":$scope.RefundCG_RefundCGUnit.replace(',', '.')},
			{"ParameterDetailId":$scope.RefundCG_RefundCGServiceParamId, "RefundCGPercentage":$scope.RefundCG_RefundCGService.replace(',', '.')},
			{"ParameterDetailId":$scope.RefundCG_RefundCGPartsParamId, "RefundCGPercentage":$scope.RefundCG_RefundCGParts.replace(',', '.')}
		];
		
		RefundCGFactory.create(createData)
		.then(
			function(res){
				// alert("Data berhasil disimpan");

				bsNotify.show(
					{
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: 'success'
					}
				);
				$scope.LoadingSimpan = false;
				$scope.GetData();
			},
			function(err){
				console.log("Preprinted Kuitansi Error : ", err);
				bsNotify.show(
					{
						title: "Gagal",
						content: "Gagal simpan data.",
						type: 'danger'
					}
				);
				$scope.LoadingSimpan = false;
			}
		)
	}
});