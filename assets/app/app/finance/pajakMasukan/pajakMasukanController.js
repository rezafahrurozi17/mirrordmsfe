// import { start } from "repl";

var app = angular.module('app');
app.controller('PajakMasukanController', function ($scope, $http, $filter, uiGridConstants, bsNotify, CurrentUser, PajakMasukanFactory, $timeout, ngDialog) {
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.optionsPajakMasukan_SelectKolom =
		[{ name: "Nomor Invoice Masuk", value: "IncomingInvoiceNumber" },
		{ name: "Nama Vendor", value: "VendorName" },
		{ name: "Nomor Instruksi Pembayaran", value: "PaymentInstructionNumber" },
		{ name: "Status Instruksi Pembayaran", value: "PaymentInstructionStatus" },
		{ name: 'Nomor Faktur Pajak', value: 'TaxInvoiceNumber'},
		{ name: 'Tipe Bisnis', value: 'BusinessType'},
		{ name: "Approval Status", value: "ApprovalStatus"},
		{ name: "Perekam", value: "Perekam"},
		{ name: "Status Download CSV", value: "DownloadCSVStatus"}];
	$scope.PajakMasukan_SelectKolom = "IncomingInvoiceNumber";

	var user = CurrentUser.user();

	if (user.OrgCode.substring(3, 9) == '000000'){
		$scope.PajakMasukan_HO_Show = true;
		$scope.PajakMasukan_NamaBranch_Disabled = true;
	}
	else{
		$scope.PajakMasukan_HO_Show = false;
		$scope.PajakMasukan_NamaBranch_Disabled = false;
	}

	$scope.PajakMasukan_getDataBranch = function () {
		PajakMasukanFactory.getDataBranchComboBox()
			.then
			(
			function (res) {
				$scope.optionsPajakMasukan_SelectBranch = res.data;
				$scope.PajakMasukan_SelectBranch = user.OutletId;
				res.data.some(function (obj, i) {
					if (obj.value == user.OutletId)
						$scope.PajakMasukan_NamaBranch = obj.name;
					$scope.PajakMasukan_SelectBranch = obj.value;
				});

			},
			function (err) {
				console.log("err=>", err);
			}
			);
	};

	$scope.PajakMasukan_getDataBranch();
	$scope.PajakMasukan_TextFilter = "";
	$scope.PajakMasukan_SelectKolom = "";
	$scope.filter = "20160101|20990101|" + $scope.PajakMasukan_TextFilter + "|" + $scope.PajakMasukan_SelectKolom;

	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		//   $scope.gridData=[];


	});

	var reminder = 0;
	if (!reminder) {
		//$('#PajakMasukan_ReminderDJP').modal('show');
		ngDialog.openConfirm({
			template: '\
						 <div class="ngdialog-buttons">\
						 <p><h4><b>Reminder!!</b></h4></p>\
						 <hr>\
						 <h5 class="text-center">Apakah Anda sudah mengupdate status DJP Anda?</h5>\
						 <hr>\
						 </div>',
		 plain: true,
			controller: 'ParamKuitansiTTUSController',
			scope: $scope
		});
		reminder = 1;
	}

	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.PajakMasukan_ListData_Grid_UIGrid = {
		enableHorizontalScrollbar: 1,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: false,
		useExternalPagination: false,
		enableSelectAll: true,
		enableGridMenu: true,
		exporterMenuAllData: true,
		exporterMenuPdf: false,
		exporterCsvFilename: 'PajakMasukan.csv',
		exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

		//showGridFooter: true,
		showColumnFooter: true,
		enableFiltering: true,
		enableColumnResizing: true,
		enableSorting: false,
		enableRowSelection: true,
		enableSelectAll: true,
		columnDefs: [
			{ name: 'Tanggal Invoice Masuk', field: 'InvoiceDate', cellFilter: 'date:\"dd/MM/yyyy\"', width: 100 },
			{ name: 'Nomor Invoice Masuk', field: 'IncomingInvoiceNumber', width: 100 },
			{ name: 'Nomor Invoice dari Vendor', field: 'invoicevendornumber', width: 100 },
			{ name: 'Nomor Faktur Pajak', field: 'TaxInvoiceNumber', width: 100 },
			{ name: 'Tanggal Faktur Pajak', field: 'DocTaxDate', cellFilter: 'date:\"dd/MM/yyyy\"', width: 100 },
			{ name: 'Masa Pajak', field: 'MasaPajak', width: 100 },
			{ name: 'Tahun Pajak', field: 'TahunPajak', width: 100 },
			{ name: 'Tipe Bisnis', field: 'BusinessType', width: 100 },
			{ name: 'Nama Npwp', field: 'NPWPName', width: 100 },
			{ name: 'Alamat Npwp', field: 'NPWPAddress', width: 100 },
			{ name: 'NPWP', field: 'NPWP', width: 100 },
			{ name: 'DPP', field: 'DPP', cellFilter: 'rupiahC', width: 100 },
			{ name: 'PPN', field: 'PPN', cellFilter: 'rupiahC', width: 100 },
			{ name: 'Tanggal Instruksi Pembayaran', field: 'PaymentInstructionDateApproved', cellFilter: 'date:\"dd/MM/yyyy\"', width: 100 },
			{ name: 'Nomor Instruksi Pembayaran', field: 'PaymentInstructionNumber', width: 100 },
			{ name: 'Status Instruksi Pembayaran', field: 'PaymentInstructionStatus', width: 100 },
			{ name: 'Status Download CSV', field: 'DownloadCSVStatus', width: 100 },
			{ name: "Approval Status", field: "ApprovalStatus", enableFiltering: true, width:100  },
			{ name: "Tanggal Approval", field: "TanggalApproval", cellFilter: 'date:\"dd/MM/yyyy\"', enableCellEdit: false, width:100 },
			{ name: "Tanggal Rekam", field: "TanggalRekam", enableCellEdit: false, width: 100 },
			{ name: "Perekam", field: "Perekam", enableCellEdit: false, width: 100 },



			{ name: 'Nama Vendor', field: 'VendorName', visible: false },
			{ name: 'CSVFilePath', field: 'CSVFilePath', visible: false },
			{ name: 'TaxId', field: 'TaxId', visible: false },
			{ name: 'VendorId', field: 'VendorId', visible: false },
			{ name: 'PaymentId', field: 'PaymentId', visible: false },
			{ name: 'TaxDocDate', field: 'TaxDate', visible: false, cellFilter: 'date:\"dd/MM/yyyy\"' },
			{ name: 'PaymentTypeId', field: 'PaymentTypeId', visible: false }
		],
		onRegisterApi: function (gridApi) {
			$scope.gridPajakMasukan = gridApi;
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// 	$scope.getDataPajakMasukan(pageNumber, pageSize, $scope.filter);
			// });

			//gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );

		}
	};

	$scope.singleFilter = function (renderableRows) {
		var matcher = new RegExp($scope.PajakMasukan_TextFilter);
		renderableRows.forEach(function (row) {
			var match = false;

			[$scope.PajakMasukan_SelectKolom].forEach(function (field) {
				if (row.entity[field].match(matcher)) {
					match = true;
				}
			});
			if (!match) {
				row.visible = false;
			}
		});
		return renderableRows;
	};

	//----------------------------------
	// Process
	//----------------------------------
	//get data from factory
	$scope.getDataPajakMasukan = function (Start, Limit, Filter) {

		PajakMasukanFactory.getDataPajakMasukan(Start, Limit, Filter, $scope.PajakMasukan_SelectBranch)
			.then(
				function (res) {
					console.log(res);
					$scope.PajakMasukan_ListData_Grid_UIGrid.data = res.data.Result;
					$scope.PajakMasukan_ListData_Grid_UIGrid.totalItems = res.data.Total;
					$scope.PajakMasukan_ListData_Grid_UIGrid.paginationPageSize = Limit;
					$scope.PajakMasukan_ListData_Grid_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.PajakMasukan_ListData_Grid_UIGrid.pageNumber = Start;
				},
				function (err) {
					console.log("err=>", err);
				}
			);

	}

	$scope.PajakMasukan_Download_Clicked = function () {
		var PaymentNoList = [];
		angular.forEach($scope.gridPajakMasukan.selection.getSelectedRows(), function (value, key) {
			console.log("tanggal pajak :", value.TaxDate);
			PaymentNoList.push({
				"TaxDocumentNumber": value.TaxInvoiceNumber,
				"PIId": value.PaymentId,
			})

		});
		$scope.BuatFakturPajak_BuatCSV(0, 0, PaymentNoList, new Date(), 0);
	}

	$scope.BuatFakturPajak_BuatCSV = function (VendorId, PaymentId, PaymentNoList, TaxDocDate, PaymentTypeId) {
		var TaxDate = $filter('date')(new Date(TaxDocDate.toLocaleString()), "yyyyMMdd");
		PajakMasukanFactory.getCSV(VendorId, PaymentId, PaymentNoList, TaxDate, PaymentTypeId)
			.then(
				function (res) {

					var filename = 'testcsv.csv';
					var contentType = 'text/csv;charset=utf-8';
					var blob = new Blob([res.data], { type: contentType });
					saveAs(blob, 'Pajak Masukan' + '.csv');
					$scope.PajakMasukan_Cari_Clicked();
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	$scope.PajakMasukanTanggalAwalOK = true;
	$scope.PajakMasukanTanggalAwalChanged = function () {

		console.log($scope.PajakMasukanTanggalAwalOK);
		var dt = $scope.PajakMasukan_TanggalAwal;
		var now = new Date();
		if (dt > now) {
			$scope.errPajakMasukanTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.PajakMasukanTanggalAwalOK = false;
		}
		else {
			$scope.PajakMasukanTanggalAwalOK = true;
		}
		console.log('asdasd ' + $scope.PajakMasukanTanggalAwalOK);
	}

	$scope.PajakMasukanTanggalAkhirOK = true;
	$scope.PajakMasukanTanggalAkhirChanged = function () {

		console.log($scope.PajakMasukanTanggalAkhirOK);
		var dt = $scope.PajakMasukan_TanggalAkhir;
		var now = new Date();
		if (dt > now) {
			$scope.errPajakMasukanTanggalAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.PajakMasukanTanggalAkhirOK = false;
		}
		else {
			$scope.PajakMasukanTanggalAkhirOK = true;
		}
		console.log('asdasd ' + $scope.PajakMasukanTanggalAkhirOK);
	}


	function getYYYYMMDD(date) {
		var year = '' + date.getFullYear();
		var month = '' + (date.getMonth() + 1);
		var day = '' + date.getDate();

		if (month < 10) { month = '0' + month };
		if (day < 10) { day = '0' + day };
		return year + month + day;

	}

	$scope.PajakMasukan_Cari_Clicked = function () {

		if ((typeof $scope.PajakMasukan_TanggalAwal != 'undefined') && (typeof $scope.PajakMasukan_TanggalAkhir != 'undefined')) {
			$scope.filter = getYYYYMMDD($scope.PajakMasukan_TanggalAwal) + "|" + getYYYYMMDD($scope.PajakMasukan_TanggalAkhir) + "|" + $scope.PajakMasukan_TextFilter + "|" + $scope.PajakMasukan_SelectKolom;
			$scope.getDataPajakMasukan(1, 10, $scope.filter);
		}
		else {
			alert("data tanggal tidak lengkap");
		};

	}

	// $scope.PajakMasukan_Filter_Clicked = function() {
	//     $scope.gridPajakMasukan.grid.refresh();
	// }

	$scope.PajakMasukan_Filter_Clicked = function () {
		$scope.PajakMasukan_Cari_Clicked();
	}

	$scope.PajakMasukan_Excel_Clicked = function () {
		$scope.PajakMasukan_DownloadExcelTemplate();
	}

	$scope.PajakMasukan_DownloadExcelTemplate = function () {
		var excelData = [];
		var fileName = "";
		var eachData = {};
		var allData = [];
		$scope.filter = getYYYYMMDD($scope.PajakMasukan_TanggalAwal) + "|" + getYYYYMMDD($scope.PajakMasukan_TanggalAkhir) + "|" + $scope.PajakMasukan_TextFilter + "|" + '';
		PajakMasukanFactory.getDataPajakMasukan(1, 0, $scope.filter, $scope.PajakMasukan_SelectBranch)
			.then(
				function (res) {
					console.log(res);
					allData = res.data.Result;
					var TaxIdArray = [];
					angular.forEach($scope.gridPajakMasukan.selection.getSelectedRows(), function (value, key) {
						TaxIdArray.push(value.TaxInvoiceNumber);
					});

					angular.forEach(allData, function (value, key) {
						console.log("value : ", value);
						var TanggalInvoiceMasuk = '-';
						if (value.InvoiceDate !== null) {
							TanggalInvoiceMasuk = value.InvoiceDate;
						}

						var InvoiceMasuk = '-';
						if (value.IncomingInvoiceNumber !== null) {
							console.log("masuk invoice masuk");
							InvoiceMasuk = value.IncomingInvoiceNumber;
						}

						var InvoiceVendorNumber = '-';
						if (value.invoicevendornumber !== null)
							InvoiceVendorNumber = value.invoicevendornumber;

						var NomorFakturPajak = '-';
						if (value.TaxInvoiceNumber !== null)
							NomorFakturPajak = value.TaxInvoiceNumber;

						var TanggalFakturPajak = '-';
						if (value.DocTaxDate !== null)
							TanggalFakturPajak = value.DocTaxDate;

						var MasaPajak = '-';
						if (value.MasaPajak !== null)
							MasaPajak = value.MasaPajak;

						var TahunPajak = '-';
						if (value.TahunPajak !== null)
							TahunPajak = value.TahunPajak;

						var TipeBisnis = '-';
						if (value.BusinessType !== null)
							TipeBisnis = value.BusinessType;

						var NamaNpwp = '-';
						if (value.NPWPName !== null)
							NamaNpwp = value.NPWPName;

						var Npwp = '-';
						if (value.NPWP !== null)
							Npwp = value.NPWP;

						var AlamatNpwp = '-';
						if (value.NPWPAddress !== null)
							AlamatNpwp = value.NPWPAddress;

						var DPP = 0;
						if (value.DPP !== null)
							DPP = value.DPP;

						var PPN = 0;
						if (value.PPN !== null)
							PPN = value.PPN;

						var TanggalInstruksiPembayaran = '-';
						if (value.PaymentInstructionDateApproved !== null)
							TanggalInstruksiPembayaran = value.PaymentInstructionDateApproved;

						var NomorInstruksiPembayaran = '-';
						if (value.PaymentInstructionNumber !== null)
							NomorInstruksiPembayaran = value.PaymentInstructionNumber;

						var StatusInstruksiPembayaran = '-';
						if (value.PaymentInstructionStatus !== null)
							StatusInstruksiPembayaran = value.PaymentInstructionStatus;

						var StatusDownloadCSV = '-'
						if (value.DownloadCSVStatus !== null)
							StatusDownloadCSV = value.DownloadCSVStatus;

						var ApprovalStatus = '-';
						if (value.ApprovalStatus !== null)
						ApprovalStatus = value.ApprovalStatus;

						var TanggalApproval = '-';
						if (value.TanggalApproval !== null)
						TanggalApproval = value.TanggalApproval;

						var TanggalRekam = '-';
						if (value.TanggalRekam !== null)
						TanggalRekam = value.TanggalRekam;

						var Perekam = '-'
						if (value.Perekam !== null)
						Perekam = value.Perekam;


						if (TaxIdArray.includes(value.TaxInvoiceNumber)) {
							eachData = {
								"Tanggal Invoice Masuk": TanggalInvoiceMasuk,
								"Nomor Invoice Masuk": InvoiceMasuk,
								"Nomor Invoice dari Vendor": InvoiceVendorNumber,
								"Nomor Faktur Pajak": NomorFakturPajak,
								"Tanggal Faktur Pajak": TanggalFakturPajak,
								"Masa Pajak": MasaPajak,
								"Tahun Pajak": TahunPajak,
								"Tipe Bisnis": TipeBisnis,
								"Nama Npwp": NamaNpwp,
								"Alamat Npwp": AlamatNpwp,
								"Npwp": Npwp,
								"DPP": DPP,
								"PPN": PPN,
								"Tanggal Instruksi Pembayaran": TanggalInstruksiPembayaran,
								"Nomor Instruksi Pembayaran": NomorInstruksiPembayaran,
								"Status Instruksi Pembayaran": StatusInstruksiPembayaran,
								"Status Download CSV": StatusDownloadCSV,
								
								"Approval Status": ApprovalStatus,
								"Tanggal Approval": TanggalApproval,
								"Tanggal Rekam": TanggalRekam,
								"Perekam": Perekam
							};
							excelData.push(eachData);
						}
					});

					console.log("excel Data :", excelData);
					fileName = "DaftarPajakMasukan";

					XLSXInterface.writeToXLSX(excelData, fileName);
				},
				function (err) {
					console.log("err=>", err);
					return false;
				}
			);
	}

	// ============================ BEGIN | NOTO | 190412 | Pajak Masukan | Upload Status DJP ============================
	$scope.PajakMasukan_Upload_Clicked = function () {
		$scope.Hide_LihatPajakMasukan = true;

		$scope.PajakMasukan_ExcelData = [];
		$scope.PajakMasukan_UploadData_UIGrid.data = [];
	}

	$scope.PajakMasukan_Kembali_Clicked = function () {
		$scope.Hide_LihatPajakMasukan = false;
	}
	// ============================ END | NOTO | 190412 | Pajak Masukan | Upload Status DJP ============================

	// ============================ BEGIN | NOTO | 190415 | Pajak Masukan | Download Template ============================
	$scope.PajakMasukan_DownloadTemplate_Clicked = function () {
		var excelData = [{ "NOMOR_FAKTUR_PAJAK": "", "APPROVAL_STATUS": "", "TANGGAL_APPROVAL": "", "TANGGAL_REKAM": "", "PEREKAM": "" }];
		var fileName = "PajakMasukan_StatusDJP_Template";
		XLSXInterface.writeToXLSX(excelData, fileName);
	}
	// ============================ END | NOTO | 190415 | Pajak Masukan | Download Template ============================

	// ============================ BEGIN | NOTO | 190415 | Pajak Masukan | Validasi & Submit ============================
	$scope.PajakMasukan_UploadData_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "Nomor Faktur Pajak", field: "NOMOR_FAKTUR_PAJAK", enableFiltering: true },
			{ name: "Approval Status", field: "APPROVAL_STATUS", enableFiltering: true },
			{ name: "Tanggal Approval", field: "TANGGAL_APPROVAL", enableCellEdit: false },
			{ name: "Tanggal Rekam", field: "TANGGAL_REKAM", enableCellEdit: false },
			{ name: "Perekam", field: "PEREKAM", enableCellEdit: false },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPajakMasukanDJP = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.PajakMasukan_UploadData_UIGrid.paginationCurrentPage = pageNumber;
				$scope.PajakMasukan_UploadData_UIGrid.paginationPageSize = pageSize;
				$scope.PajakMasukan_UploadData_UIGrid.paginationPageSizes = [10, 25, 50];
			});
		}
	};

	$scope.PajakMasukan_Upload_File_Clicked = function () {
		var myEl = angular.element(document.querySelector('#uploadPajakMasukanDJP')); //ambil elemen dari dokumen yang di-upload 

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			$scope.PajakMasukan_ExcelData = json;
			$scope.PajakMasukan_FileName_Current = myEl[0].files[0].name;
			$scope.PajakMasukan_UploadData_UIGrid.data = $scope.PajakMasukan_ExcelData;
			$scope.PajakMasukan_UploadData_UIGrid.totalItems = $scope.PajakMasukan_ExcelData.length;

			if (!$scope.validateColumn($scope.PajakMasukan_ExcelData[0])) {
				msg = ("Kolom file excel tidak sesuai");
				bsNotify.show({
					title: "Warning",
					content: msg,
					type: "warning"
				});
				return;
			}
		});

		// angular.forEach($scope.PajakMasukan_UploadData_UIGrid.data, function (value) {
		// 	if (value.APPROVAL_STATUS == '1')
		// 		value.APPROVAL_STATUS == 'Belum Approve';
		// 	else if (value.APPROVAL_STATUS == '2')
		// 		value.APPROVAL_STATUS == 'Siap Approve';
		// 	else if (value.APPROVAL_STATUS == '3')
		// 		value.APPROVAL_STATUS == 'Approval Sukses';
		// 	else if (value.APPROVAL_STATUS == '4')
		// 		value.APPROVAL_STATUS == 'Belum Approve';
		// 	else if (value.APPROVAL_STATUS == '5')
		// 		value.APPROVAL_STATUS == 'Reject';
		// 	else if (value.APPROVAL_STATUS == '6')
		// 		value.APPROVAL_STATUS == 'Batal';
		// });
	}

	$scope.validateColumn = function (inputExcelData) {
		if (angular.isUndefined(inputExcelData.NOMOR_FAKTUR_PAJAK)
			|| angular.isUndefined(inputExcelData.APPROVAL_STATUS)
			|| angular.isUndefined(inputExcelData.TANGGAL_APPROVAL)
			|| angular.isUndefined(inputExcelData.TANGGAL_REKAM)
			|| angular.isUndefined(inputExcelData.PEREKAM)) {
			return false;
		}
		return true;
	}

	$scope.PajakMasukan_Upload_Simpan_Clicked = function () {
		var data = [{ "TipePajak": "PajakMasukan", "DJPList": $scope.PajakMasukan_ExcelData }]
		PajakMasukanFactory.uploadDJPStatus(data).then
			(
			function (res) {
				bsNotify.show({
					title: "Berhasil",
					content: "Berhasil simpan",
					type: 'success'
				});

				$scope.PajakMasukan_Kembali_Clicked();
				$scope.PajakMasukan_Cari_Clicked();
			},
			function (err) {
				if (err != null) {
					bsNotify.show({
						title: "Gagal simpan",
						content: err.data.Message,
						type: 'warning'
					});
					
					// ngDialog.openConfirm({
					// 	template: '\
					// 				 <div class="ngdialog-buttons">\
					// 				 <p><h4><b>Gagal simpan</b></h4></p>\
					// 				 <hr>\
					// 				 <h5 class="text-center">' + err.data.Message + '</h5>\
					// 				 <hr>\
					// 				 </div>',
					// 	plain: true,
					// 	//controller: 'ParamKuitansiTTUSController',
					// 	scope: $scope
					// });
				}
			}
			);
	}
	// ============================ END | NOTO | 190415 | Pajak Masukan | Validasi & Submit ============================
});

app.filter('rupiahC', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				// val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
				val = String(val).split("").reverse().join("")
					.replace(/(\d{3}\B)/g, "$1.")
					.split("").reverse().join("");
			}
			return val;
		}

	};
});

