angular.module('app')
	.factory('PajakMasukanFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user();
		var factory = {};
		var debugMode = true;

		factory.getDataBranchComboBox = function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.getDataPajakMasukan = function (Start, Limit, Filter, OutletId) {
			var url = '/api/fe/FinancePajakMasukan/getalldata/?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter + '&outletid=' + OutletId;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Parameter :start=' + Start + '&limit=' + Limit + '&filterData=' + Filter); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
		factory.getDownloadFile = function (url) {

			var urlSplit = url.split("/");
			console.log(url);
			console.log(urlSplit[urlSplit.length - 1]);
			var anchor = angular.element('<a/>');
			anchor.attr({
				// href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
				href: url,
				target: '_blank'
				, download: urlSplit[urlSplit.length - 1]
			})[0].click();
			//$http.get(url).success(function(data, status, headers, config) {});
		}

		factory.updateCSVStatus = function (inputData) {
			var url = '/api/fe/FinancePajakMasukan/submitdata';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke updateCSVStatus') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		}

		factory.getCSV = function (VendorId, PaymentId, PaymentNoList, TaxDocDate, PaymentTypeId) {
			var url = '/api/fe/FinancePajakMasukan/Cetak/';
			console.log("getCSV : ", url);
			var inputData = [{
				"VendorId": VendorId,
				"PaymentId": PaymentId,
				"PaymentList": PaymentNoList,
				"TaxDocDate": TaxDocDate,
				"PaymentTypeId": PaymentTypeId,

				// "billingid":BillingId,
				// "billingcode":BillingCode,
				// "taxdocDate":TaxDocDate,
				// "outletid":OutletId

			}];
			//var res=$http.get(url, {responseType: 'arraybuffer'});
			var res = $http.post(url, inputData, { responseType: 'arraybuffer' });
			return res;
		}

		factory.uploadDJPStatus = function (inputData) {
			var url = '/api/fe/FinancePajakMasukan/UploadStatusDJP/';
			var param = JSON.stringify(inputData);
			return $http.post(url, param);
		}

		return factory;
	});