angular.module('app')
	.factory('MasterPricingDealerToBranchPriceAccessoriesFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

	
		return {
			VerifyData: function (KlasifikasiPE, IsiGrid, TypePE) {
				//var url = '/api/fe/IncomingInstruction';
				var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
				var url = '/api/fe/MasterPEPriceAccessories/Verify/';
				var param = JSON.stringify(inputData);

				if (debugMode) { console.log('Masuk ke submitData') };
				if (debugMode) { console.log('url :' + url); };
				if (debugMode) { console.log('Parameter POST :' + param) };
				var res = $http.post(url, param);

				return res;
			},

			getData: function (start, limit, filterData) {
	// var url = '/api/fe/MasterPEPriceAccessories/SelectData/start/' + start + '/limit/' + limit + '/filterData/' + filterData;
				var url = '/api/fe/MasterPEPriceAccessories/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;

				var res = $http.get(url);
				return res;
			},

			Submit : function(KlasifikasiPE, IsiGrid, TypePE) {
				var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
				var url = '/api/fe/MasterPEPriceAccessories/Save/';
				var param = JSON.stringify(inputData);

				if (debugMode) { console.log('Masuk ke submitData') };
				if (debugMode) { console.log('url :' + url); };
				if (debugMode) { console.log('Parameter POST :' + inputData) };
				var res = $http.post(url, inputData);
				return res;
			},

			getDaVehicleType: function (param) {
				var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
				return res;
			},

			getDataModel: function () {
				var res = $http.get('/api/sales/MUnitVehicleModelTomas');
				return res;
			},



		}
	});
