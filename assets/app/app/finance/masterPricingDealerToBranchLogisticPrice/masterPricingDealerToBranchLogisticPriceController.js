var app = angular.module('app');
app.controller('MasterPricingDealerToBranchLogisticPriceController', function ($scope, $http, $filter, bsAlert, CurrentUser, MasterPricingDealerToBranchLogisticPriceFactory, $timeout) {
	$scope.optionsKolomFilterSatu_LogisticPrice = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_LogisticPrice = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_LogisticPrice = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_LogisticPrice = [
			{name:"Vendor", value:"Vendor"}, {name:"Description", value:"Description"},
			{name:"Sales Price", value:"SalesPrice"}, 
			{name:"Effective Date From", value:"EffectiveDateFrom"},
			{name:"Effective Date To", value:"EffectiveDateTo"}, {name:"Remarks", value:"Remarks"}
	];
	angular.element('#LogisticPriceModal').modal('hide');
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'LogisticPrice';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.cekKolom = '';
	$scope.ShowFieldGenerate_LogisticPrice = false;

	$scope.MasterSellingPriceFU_LogisticPrice_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],	
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'Vendor', displayName: 'Vendor', field: 'Vendor',enableCellEdit: false , enableHiding: false},
			{ name: 'Description', displayName: 'Description', field: 'Description',enableCellEdit: false , enableHiding: false},			
			{ name: 'SalesPrice', displayName: 'Sales Price', field: 'SalesPrice',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number" },
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date From', field: 'EffectiveDateFrom',enableCellEdit: false , enableHiding: false},
			{ name: 'EffectiveDateTo', displayName: 'Effective Date To', field: 'EffectiveDateTo',enableCellEdit: false, enableHiding: false },  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks',enableCellEdit: true , enableHiding: false}, 
			{ name: 'LogisticPriceId',displayName:'Id', field: 'LogisticPriceId', visible: false, enableHiding: false },		
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_LogisticPrice_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_LogisticPrice' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				$scope.MasterSelling_LogisticPrice_Upload_Clicked();
				myEl.val('');	
            });
	}

	$scope.validateColumn = function(inputExcelData){
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.Vendor)) {
			$scope.cekKolom = $scope.cekKolom + ' Vendor \n';
		}
		
		if (angular.isUndefined(inputExcelData.Description)) {
			$scope.cekKolom = $scope.cekKolom + ' Description \n';
		}
		if	(angular.isUndefined(inputExcelData.SalesPrice)) {
			$scope.cekKolom = $scope.cekKolom + ' SalesPrice \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}
		if ( angular.isUndefined(inputExcelData.Vendor) || 
			angular.isUndefined(inputExcelData.Description) ||
			angular.isUndefined(inputExcelData.SalesPrice) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.ComboBulkAction_LogisticPrice_Changed = function()
	{
		if ($scope.ComboBulkAction_LogisticPrice == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_LogisticPrice_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_LogisticPrice_grid.data.splice(index, 1);
			// });

			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedRows(), function (data, index) {
				counter++;
			});

			//if ($scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedCount() > 0) {
			if (counter > 0 ) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
					function() {
						$scope.OK_Button_Clicked();
					},
					function() {
		
					}
				);
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_LogisticPrice_grid.data.splice($scope.MasterSellingPriceFU_LogisticPrice_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_LogisticPrice = "";
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_LogisticPrice = "";
		angular.element('#LogisticPriceModal').modal('hide');
	}

	$scope.MasterSelling_LogisticPrice_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingLogisticPrice";

		if ($scope.MasterSellingPriceFU_LogisticPrice_grid.data.length == 0) {
				excelData.push({ 
						Vendor : "",
						Description : "",
		//				DIOName: row.DIOName,
						SalesPrice: "",
						EffectiveDateFrom: "",
						EffectiveDateTo: "",
						Remarks: "" });
				fileName = "MasterSellingLogisticPriceTemplate";
		}
		else {
		//$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_LogisticPrice_grid.data.forEach(function(row) {
				excelData.push({ 
						Vendor : row.Vendor,
						Description : row.Description,
		//				DIOName: row.DIOName,
						SalesPrice: row.SalesPrice,
						EffectiveDateFrom: row.EffectiveDateFrom,
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks:row.Remarks });
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_LogisticPrice_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.grid.clearAllFilters();
		MasterPricingDealerToBranchLogisticPriceFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_LogisticPrice_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_LogisticPrice_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_LogisticPrice_grid.data);		
		MasterPricingDealerToBranchLogisticPriceFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_LogisticPrice_grid.data = [];
		$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_LogisticPrice = "";
		$scope.TextFilterSatu_LogisticPrice = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_LogisticPrice' ) );
		myEl.val('');
	}

	$scope.MasterSelling_LogisticPrice_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_LogisticPrice_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_LogisticPrice_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_LogisticPrice_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_LogisticPrice_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_LogisticPrice_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_LogisticPrice_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_LogisticPrice,
			Text1 : $scope.TextFilterSatu_LogisticPrice,
			AndOr : $scope.Filter_LogisticPrice_AndOr,
			Filter2 : $scope.KolomFilterDua_LogisticPrice,
			Text2 : $scope.TextFilterDua_LogisticPrice,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingDealerToBranchLogisticPriceFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_LogisticPrice_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_LogisticPrice_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_LogisticPrice_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_LogisticPrice;
		$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_LogisticPrice != "") {
			$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.grid.getColumn($scope.ComboFilterGrid_LogisticPrice).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_LogisticPrice_gridAPI.grid.clearAllFilters();

	}

});