var app = angular.module('app');
app.controller('CetakUlangController', function ($scope, $q, $http, CurrentUser, $filter, cetakUlangFactory, $timeout, uiGridConstants, bsNotify) {
	$scope.optionsLihatIncomingPayment_DaftarIncomingPayment_Search = [{ name: "No Formulir Cetak", value: "Nomor Formulir Cetak" }, { name: "Tanggal Formulir Cetak", value: "TanggalRef" }, { name: "Status Pengajuan", value: "Status Pengajuan" }];
	$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Kuitansi Penagihan)", value: "Unit - Payment (Kuitansi Penagihan)" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }, { name: "Cash Delivery", value: "Cash Delivery" }];
	//$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }, { name: "Cetak Ulang", value: "Cetak Ulang" }, { name: "Pengajuan", value: "Pengajuan" }];
	$scope.optionsCetakUlang_TipeFormulirCetak_Search = [{ name: "Bukti Jaminan Dealer", value: "Bukti Jaminan Dealer" }, { name: "Bukti Pencatatan Hutang", value: "Bukti Pencatatan Hutang" }, { name: "Kuitansi & TTUS", value: "Kuitansi" }];
	$scope.optionsLihatAdvancePaymentTipeAdvancePayment = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK-BPKB", value: "STNK-BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "General Transaction", value: "General Transaction" }];
	$scope.optionsLihatAdvancePaymentMetodePembayaran = [{ name: "Cash", value: "Cash" }, { name: "Bank Transfer", value: "Bank Transfer" }];
	$scope.user = CurrentUser.user();

	var paginationOptions_LihatCetakUlang_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOptions = { format: dateFormat }

	$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
	console.log($scope.ByteEnable);
	$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
	if ($scope.allowApprove == true &&
		!(
			$scope.user.RoleName == 'Admin Unit' ||
			$scope.user.RoleName == 'Admin Service' ||
			$scope.user.RoleName == 'Admin Service BP' ||
			$scope.user.RoleName == 'Admin Service GR')
		)
		$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Cetak Ulang", value: "Cetak Ulang" },{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }, { name: "Pengajuan", value: "Pengajuan" }];
	else
		$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Cetak Ulang", value: "Cetak Ulang" }, { name: "Pengajuan", value: "Pengajuan" }];

	//$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" },{ name: "Cetak Ulang", value: "Cetak Ulang" }, { name: "Pengajuan", value: "Pengajuan" }];	

	//Cetak Ulang HO
    if ($scope.user.OrgCode.substring(3, 9) == '000000') {
		$scope.optionsLihatIncomingPayment_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }, { name: "Cetak Ulang", value: "Cetak Ulang" }, { name: "Pengajuan", value: "Pengajuan" }];
	}

	$scope.Show_ShowLihatIncomingPayment = true;
	$scope.uiGridPageSize = 10;
	$scope.ShowCetakUlangData = true;
	$scope.ShowIncomingPaymentHeader = true;
	$scope.ShowIncomingPaymentData = false;
	$scope.Show_LihatAdvancePayment = false;
	$scope.ShowBatal = false;
	$scope.EditMode = false;
	$scope.DocumentId = 0;
	$scope.success = true;
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		$scope.loading = false;
		// $scope.SetMainGridADH();
	});


	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.xRole = { selected: [] };

	$scope.currentIPParentId = 0;
	$scope.currentCustomerId = 0;

	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = new Date();
	$scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo = new Date();
	//-- payment instruction
	$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
	$scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit = null;

	$scope.optionsTambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
	$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = null;

	$scope.TambahInstruksiPembayaran_TipeRefundOptions = [{ name: "Customer Guarantee", value: "Customer Guarantee" }, { name: "PPh 23", value: "PPh 23" }, { name: "PPN WAPU", value: "PPN WAPU" }, { name: "Titipan", value: "Titipan" }];
	$scope.TambahInstruksiPembayaran_TipeRefund = null;

	$scope.optionsTambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = [{ name: "Bea Materai", value: "Bea Materai" }] //,{name:"Biaya Bank", value:"Biaya Bank"}];
	$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = null;

	$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendorOptions = [{ name: "One Time Vendor", value: "One Time Vendor" }, { name: "Pilih Vendor", value: "Pilih Vendor" }];
	$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = null;
	//--payment instruction 


	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	var uiGridPageSize = 10;

	function checkRbyte(rb, b) {
		var p = rb & Math.pow(2, b);
		return ((p == Math.pow(2, b)));
	};

	$scope.tmpFlag = false;
	$scope.LihatCetakUlang_UIGrid = {

		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,

		enableSorting: true,
		onRegisterApi: function (gridApi) {
			$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIPParentId = 0;
				}
				else {
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {

				paginationOptions_LihatCetakUlang_UIGrid.pageNumber = pageNumber;
				paginationOptions_LihatCetakUlang_UIGrid.pageSize = pageSize;
				$timeout(function(){
					if($scope.tmpFlag == false){
						$scope.GetListIncoming1(pageNumber, pageSize);
					}else{
						$scope.GetListIncoming(pageNumber, pageSize);
					}
				},100);
				
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
			gridApi.core.on.renderingComplete($scope, function () {
				if ($scope.user.RoleName == 'ADH' || $scope.user.RoleName == 'ADH BP' ) {
					$scope.GetListIncomingADH();
				}
				else if ($scope.user.RoleName == 'Cashier' || $scope.user.RoleName == 'Cashier BP' ||$scope.user.RoleName == 'Admin Unit' || $scope.user.RoleName == 'Admin Service GR'
				        || $scope.user.RoleName == 'Admin Service BP') {
					$scope.optionsCetakUlang_TipeFormulirCetak_Search = [{ name: "Kuitansi", value: "Kuitansi" }];
					$scope.CetakUlang_TipeFormulirCetak_Search = "Kuitansi";
					$scope.CetakUlang_TipeFormulirCetak_Disabled = true;
					$scope.GetListIncoming();
				}
				else if ($scope.user.RoleName == 'Admin Finance' ||$scope.user.RoleName == 'Admin Finance BP' || $scope.user.RoleName == 'Finance HO') {
					$scope.optionsCetakUlang_TipeFormulirCetak_Search = [{ name: "Bukti Jaminan Dealer", value: "Bukti Jaminan Dealer" }, { name: "Bukti Pencatatan Hutang", value: "Bukti Pencatatan Hutang" }];

					$scope.GetListIncoming();
				}
				else {
					$scope.GetListIncoming();
				}
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs:
			[
				// { name: 'No Formulir Cetak', field: 'ReceiptNo', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailCetakUlang(row)">{{row.entity.ReceiptNo}}</a>' },
				{ name: 'No Formulir Cetak', field: 'ReceiptNo', visible: true, width:'10%' },
				{ name: 'Tanggal Ref Formulir Cetak', field: 'TanggalRef', cellFilter: 'date:\"dd/MM/yyyy\"', width:'10%'  },
				{ name: 'No Incoming Payment', field: 'CetakId', visible: false, width:'10%'  },
				{ name: 'Tipe Formulir Cetak', field: 'TipeFormCetak', visible: true, width:'10%'  },
				{ name: 'Total Pencetakan', field: 'PrintCounter', width:'10%'  },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan', width:'10%'  },
				{ name: 'Tanggal Pengajuan', field: 'TglPengajuan', width:'10%'  },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan', width:'10%'  },
				{ name: 'Alasan Pengajuan', field: 'AlasanPengajuan', width:'10%'  },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan', width:'10%'  },
				{ name: 'CustomerVendorId', field: 'CustomerVendorId', visible: false, width:'10%'  },
				{ name: 'Nama Pelanggan / Vendor', field: 'CustomerVendorName', visible: true, width:'10%'  },
				{ name: 'Type', field: 'IncomingPaymentType', visible: false , width:'10%' },
				{ name: 'MetodeBayar', field: 'MetodePembayaran', visible: false , width:'10%' },
				{ name: 'LastPrinted', field: 'LastPrinted', visible: false, width:'10%'  },
				{ name: 'LastApproved', field: 'LastApproved', visible: false, width:'10%'  },
			]
	};

	$scope.LihatIncomingPayment_RincianPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Metode Pembayaran', field: 'PaymentMethod', visible: true },
					{ name: 'Tanggal Pembayaran', field: 'PaymentDate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nama Bank Pengirim/Credit/Debit Card', field: 'SenderBankId' },
					{ name: 'Nama Pengirim', field: 'SenderName' },
					{ name: 'Nomor Cheque/Credit/Debit Card', field: 'NoCheque' },
					{ name: 'Nomor Rekening Penerima', field: 'ReceiverAccNumber' },
					{ name: 'Nama Bank Penerima', field: 'BankPenerima' },
					{ name: 'Nama Pemegang Rekening', field: 'Penerima' },
					{ name: 'Keterangan', field: 'Keterangan' },
					{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_Biaya_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tipe Biaya Lain-Lain', field: 'OtherChargesType', visible: true },
					{ name: 'Debet / Kredit', field: 'DebitCreditType' },
					{ name: 'Nominal', field: 'Nominal', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_DaftarWorkOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tanggal WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nomor Billing', field: 'NoBilling', visible: true },
					{ name: 'Nomor Polisi', field: 'NoPolisi', visible: true },
					{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'Nominal Billing', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_WoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Down Payment', field: 'NominalDP', visible: true, cellFilter: 'currency:"":2' },
					{ name: 'Own Risk', field: 'OwnRisk', visible: false },
					{ name: 'Biaya Materai', field: 'MateraiDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_WoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'WoId', field: 'NoWO', visible: false },
					{ name: 'Nomor WO', displayName: 'Nomor WO', field: 'WoCode', visible: true },
					{ name: 'Tgl WO', displayName: 'Tanggal WO', field: 'TglWo', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Billing', field: 'NominalBilling', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'currency:"":2, visible:false' },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaNominal', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_Appointment_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Appointment', field: 'NoAppointment', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_Materai_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', field: 'SOId', visible: false },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'SONumber', visible: true },
					{ name: 'Down Payment', field: 'NominalDP', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', displayname: "No SO", field: 'SOId', visible: false },
					{ name: 'No SO', displayname: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiBilling', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidNominal', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaNominal', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_SoDown_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'No SO', displayName: "Nomor SO", field: 'SOId', visible: true },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'currency:"":2' },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'currency:"":2' },
					{ name: 'Sisa Pembayaran', field: 'SisaDP', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'SOId', displayName: "No SO", field: 'SOId', visible: false },
					{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Billing', field: 'TotalBilling', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'BiayaMaterai', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalBilling', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SO', displayName: "Nomor SO", field: 'SONumber', visible: true },
					{ name: 'Tgl SO', displayName: "Tanggal SO", field: 'SODate', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Down Payment', field: 'NominalDP', cellFilter: 'currency:"":2' },
					{ name: 'BiayaMaterai', field: 'MateraiDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'TotalDP', cellFilter: 'currency:"":2', visible: false },
					{ name: 'Total Terbayar', field: 'PaidDP', cellFilter: 'currency:"":2' },
					{ name: 'Saldo', field: 'SisaDP', cellFilter: 'currency:"":2' },
					{ name: 'Pembayaran', field: 'PaymentAmount', cellFilter: 'currency:"":2' }
				]
		};

	$scope.LihatIncomingPayment_InformasiPembayaran_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'No SPK', field: 'SPKId', visible: false },
					{ name: 'Tgl SPK', displayName: "Tanggal SPK", field: 'TanggalSPK', visible: true, cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: 'Nominal Booking Fee', field: 'NominalBookingFee', cellFilter: 'currency:"":0.00' }
				]
		};

	$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid =
		{
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: true,
			multiselect: false,
			enableFiltering: true,
			enableSorting: false,
			enableRowSelection: false,
			enableSelectAll: false,
			columnDefs:
				[
					{ name: 'Nomor Rangka', field: 'NoRangka', visible: true },
					{ name: 'No SPK', displayName: 'Nomor SPK', field: 'NoSPK', visible: true },
					{ name: 'No SO', displayName: 'Nomor SO', field: 'NoSO', visible: true },
					{ name: 'Nominal', field: 'Nominal' },
					{ name: 'Biaya Materai', field: 'Materai', visible: false },
					{ name: 'Total Yang Harus Dibayar', field: 'Total' }
				]
		};

	//-- payment instruction --
	$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		multiselect: false,
		useCustomPagination: false,
		canSelectRows: false,
		enableSelectAll: false,
		showGridFooter: true,
		showColumnFooter: true,
		columnDefs: [
			{
				name: "CostCenter", displayName: "Cost Center", field: "CostCenter", enableCellEdit: false, enableHiding: false,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >Sub Total</div>'
			},
			{
				name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0',
				aggregationType: uiGridConstants.aggregationTypes.sum,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
			},
			//	{ name: "Keterangan", displayName: "Keterangan", field: "Keterangan", enableCellEdit: false, enableHiding: false },
			// {
			// 	name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
			// 	cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarGeneralTransactionRow(row)" ng-disabled = "Show_TambahInstruksiPembayaran_EditDraft">Hapus</a>'
			// }
		],
		// ,gridFooterTemplate:
		// 	'<div class="ui-grid-cell-contents" >Sub Total {{TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total}}/div>',
		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarPajak_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		multiselect: false,
		useCustomPagination: false,
		canSelectRows: false,
		enableSelectAll: false,
		columnDefs: [
			{ name: "JenisPajak", displayName: "Jenis Pajak", field: "JenisPajak", visible: true },
			{ name: "TarifPajak", displayName: "Tarif Pajak %", field: "TarifPajak", enableCellEdit: false, enableHiding: false },
			{ name: "DebetKredit", displayName: "Debet/Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
			{ name: "NominalPajak", displayName: "NominalPajak", field: "NominalPajak", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "NomorDokumenPajak", displayName: "Nomor Dokumen Pajak", field: "NomorDokumenPajak", enableCellEdit: false, enableHiding: false },
			{ name: "TanggalDokumenPajak", displayName: "Tanggal Dokumen Pajak", field: "TanggalDokumenPajak", enableCellEdit: false, enableHiding: false },
		],
		onRegisterApi: function (gridApi) {
			//$scope.TambahInstruksiPembayaran_DaftarTagihanDibayar_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		multiselect: false,
		canSelectRows: false,
		columnDefs: [
			{ name: "RangkaId", displayName: "Id Nomor Rangka", field: "RangkaId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "RangkaNo", displayName: "Nomor Rangka", field: "RangkaNumber", enableCellEdit: false, enableHiding: false },
			{ name: "PreviousBalance", displayName: "Saldo Accrued Sebelumnya", field: "PreviousBalance", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "BillAmount", displayName: "Nominal Tagihan Free Service", field: "BillAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "TaxAmount", displayName: "Nominal Pajak", field: "TaxAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "SubTotal", displayName: "SubTotal", field: "SubTotal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "AccruedBalance", displayName: "Saldo Accrued", field: "AccruedBalance", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "Description", displayName: "Keterangan", field: "Description", cellEditableCondition: $scope.IsEditable, enableHiding: false, visible: true }
			//{name: "DescriptionShow", displayName: "Description", field: "Description", enableCellEdit: false, enableHiding: false, visible: false }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: true,
		displaySelectionCheckbox: true,
		enableRowSelection: true,
		multiselect: true,
		canSelectRows: true,

		columnDefs: [
			{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
			{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
			{ name: "BranchName", displayName: "Nama Branch", field: "BranchName", enableCellEdit: false, enableHiding: false },
			{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
			{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
			{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_gridAPI = gridApi;

			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				$scope.TotalNominal = 0;
				if (row.isSelected == true) {
					var mode = 'Lihat';
					if ($scope.EditMode == true) {
						mode = '';
						$scope.TotalNominal = row.entity.Nominal;
					}
					cetakUlangFactory.getDataForAccruedFrame(1, uiGridPageSize, row.entity.InvoiceId, '', mode).then(
						function (res) {
							$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = res.data.Result;
						},
						function (err) {
							console.log('error --> ', err);
						}
					);
				}
				$scope.CalculateNominalPembayaran();
			});
		}
	};

	$scope.getGLList = function () {
		$scope.currentCustomerId = 0;
		cetakUlangFactory.getGLList(1, 1000, "GL")
			.then(
				function (res) {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransactionOption = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				});
	}

	$scope.getCostCenterList = function () {
		cetakUlangFactory.getCostCenterList(1, 1000, "COSTCENTER")
			.then(
				function (res) {
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_CostCenterGeneralTransactionOption = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}
	$scope.getPajakList = function () {
		cetakUlangFactory.getPajakList(1, 1000, "PAJAK")
			.then(
				function (res) {
					$scope.DataTipePajak = res.data.Result;
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_JenisPajakGeneralTransactionOption = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		multiselect: false,
		useCustomPagination: false,
		canSelectRows: false,
		enableSelectAll: false,
		columnDefs: [
			{ name: "PajakId", displayName: "Id Pajak", field: "TaxId", visible: false },
			{ name: "JenisPajak", displayName: "Jenis Pajak", field: "TaxType", enableCellEdit: false, enableHiding: false },
			{ name: "TarifPajak", displayName: "Tarif Pajak", field: "TaxFee", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
			{ name: "DebetKredit", displayName: "Debet / Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal Pajak", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
			{ name: "NoDokPajak", displayName: "Nomor Dokumen Pajak", field: "TaxNumber", enableCellEdit: false, enableHiding: false },
			{ name: "TglDokPajak", displayName: "Tanggal Dokumen Pajak", field: "TaxDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
			//{name:"NoReferensi", displayName:"Nomor Referensi", field:"RefferenceNumber", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0'},
			// {name:"Action",displayName:"Action", field:"Action",enableHiding:false, visible:true, 
			// 	cellTemplate: '<a ng-click="grid.appScope.HapusDaftarPajakGeneralTransaction(row)">Hapus</a>'
			// }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_gridAPI = gridApi;

		}
	};

	$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_DaftarTagihan_UIGrid = {

		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		multiselect: false,
		useCustomPagination: false,
		canSelectRows: false,
		enableSelectAll: false,
		columnDefs: [
			{ name: "InvoiceId", displayName: "Nomor Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false },
			{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "VendorName", displayName: "Nama Vendor", field: "VendorName", enableCellEdit: false, enableHiding: false },
			{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
			{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
			{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
			{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_DaftarTagihan_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentIncomingInvoiceNumber = 0;
				}
				else {
					$scope.currentIncomingInvoiceNumber = row.entity.InvoiceId;
				}
			});
		}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.SetComboBranch = function () {
		var ho;
		// if ($scope.right($scope.user.OrgCode,6) == '000000')
		// {
		// 	$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
		ho = $scope.user.OrgId;
		// 	$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" },{ name: "Aksesoris", value: "Aksesoris" },
		// 		{ name: "Purna Jual", value: "Purna Jual" },{ name: "Karoseri", value: "Karoseri" },{ name: "STNK/BPKB", value: "STNK/BPKB" },
		// 		{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" },{ name: "Swapping", value: "Swapping" },
		// 		{ name: "Ekspedisi", value: "Ekspedisi" },{ name: "Parts", value: "Parts" },{ name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
		// 		{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" },{ name: "Refund", value: "Refund" },{ name: "General Transaction", value: "General Transaction" },
		// 		{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" } , { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" } 
		// 		, { name: "Accrued Free Service", value: "Accrued Free Service" }];
		// }
		// else
		// {
		// 	$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
		// 	ho = 0;
		// 	$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Aksesoris", value: "Aksesoris" },
		// 		{ name: "Purna Jual", value: "Purna Jual" },{ name: "Karoseri", value: "Karoseri" },{ name: "STNK/BPKB", value: "STNK/BPKB" },
		// 		{ name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" },{ name: "Swapping", value: "Swapping" },
		// 		{ name: "Ekspedisi", value: "Ekspedisi" },{ name: "Parts", value: "Parts" },{ name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
		// 		{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" },{ name: "Refund", value: "Refund" },{ name: "General Transaction", value: "General Transaction" },
		// 		{ name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" } , { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" } 
		// 		, { name: "Accrued Free Service", value: "Accrued Free Service" }];			
		// }

		cetakUlangFactory.getDataBranch(ho).then(
			function (resBr) {
				$scope.optionsMain_NamaBranch = resBr.data.Result;
				//console.log(JSON.stringify(res.data));		
				//console.log('cur user ' + JSON.stringify(CurrentUser) );	
				$scope.optionsTambahInstruksiPembayaran_NamaBranch = resBr.data.Result;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};

	$scope.CalculateNominalPembayaran = function () {
		var TotalCharges = 0;

		$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data.forEach(function (row) {
			if (row.DebitCredit == 'D' || row.DebitCredit == 'Debet') {
				TotalCharges += row.Nominal;
			}
			else {
				TotalCharges -= row.Nominal;
			}
		});

		$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data.forEach(function (row) {
			if (row.DebetKredit == 'D' || row.DebetKredit == 'Debet') {
				TotalCharges += row.Nominal;
			}
			else {
				TotalCharges -= row.Nominal;
			}
		});

		$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data.forEach(function (row) {
			TotalCharges += row.Nominal;
		});

		TotalCharges -= $scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment;
		//nuse tambah
		//var nominalPembayaranInternal = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = $scope.TotalNominal + TotalCharges;

	}

	$scope.IsEditable = function () {
		return $scope.EditMode;
	};

	$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction_Selected_Changed = function () {
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction;
	}

	$scope.ToTambahInstruksiPembayaran = function () {
		$scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran = new Date();
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
		$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
		$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
		$scope.TambahInstruksiPembayaran_NamaBranch = $scope.user.OrgId;
		$scope.TotalNominal = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }
			, { name: "Accrued Free Service", value: "Accrued Free Service" }];
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = null;

		$scope.optionsTambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = [{ name: "Nomor Invoice Masuk", value: "Nomor Invoice Masuk" }, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }, { name: "Nominal Billing", value: "Nominal Billing" }];
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search = {};

		//$scope.TambahInstruksiPembayaran_TipeRefundOptions = [{ name: "Customer Guarantee", value: "Customer Guarantee" },{ name: "PPh 23", value: "PPh 23" },{ name: "PPN WAPU", value: "PPN WAPU" }, { name: "Titipan", value: "Titipan" }];
		$scope.TambahInstruksiPembayaran_TipeRefund = null;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
		$scope.DocumentId = 0;
		$scope.Show_TambahInstruksiPembayaran_EditDraft = false;
		$scope.ShowTambahInstruksiPembayaran = true;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
		$scope.Show_Main_DaftarList = false;
		$scope.ShowLihatInstruksiPembayaran = false;
		$scope.EditMode = true;
	};

	$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		multiselect: false,
		useCustomPagination: false,
		canSelectRows: false,
		enableSelectAll: false,
		columnDefs: [
			{ name: "PajakId", displayName: "Id Pajak", field: "TaxId", visible: true },
			{ name: "JenisPajak", displayName: "Jenis Pajak", field: "TaxType", enableCellEdit: false, enableHiding: false },
			{ name: "TarifPajak", displayName: "Tarif Pajak", field: "TaxFee", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "DebetKredit", displayName: "Debet / Kredit", field: "DebetKredit", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal Pajak", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "NoDokPajak", displayName: "Nomor Dokumen Pajak", field: "TaxNumber", enableCellEdit: false, enableHiding: false },
			{ name: "TglDokPajak", displayName: "Tanggal Dokumen Pajak", field: "TaxDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
			{ name: "NoReferensi", displayName: "Nomor Referensi", field: "RefferenceNumber", enableCellEdit: false, enableHiding: false }
		],
		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_DaftarPajak_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		multiselect: false,
		canSelectRows: true,
		columnDefs: [
			{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "InvoiceNumber", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
			{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
			{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal 1", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
			{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
			{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_DaftarTagihan_gridAPI = gridApi;
			//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {				
			//console.log("masuk setting grid upload dan tambah");
			//if ($scope.LihatOutgoingPayment_TanggalOutgoingPayment != '') {
			//$scope.SetDataOutgoingMaster(pageNumber, $scope.formatDate($scope.LihatOutgoingPayment_TanggalOutgoingPayment));
			//}
			//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
			//});

			/* --- ini remark sementara 
			gridApi.selection.on.rowSelectionChanged($scope,function(row){
				if (row.isSelected==true)
				{
					$scope.TotalNominal = row.entity.Nominal;
					$scope.CalculateNominalPembayaran();
					//alert('masuk sini');
					$scope.PrepareGridTaxList(1, uiGridPageSize,  row);
					//console.log("data diinput --", row.entity.InvoiceId);
				}
			});	 */
		}
	};

	$scope.PrepareGridTaxList = function (start, limit, row) {
		var InvoiceId = 0;

		if (!angular.isUndefined(row.entity)) {
			InvoiceId = row.entity.InvoiceId;
		}
		else if (!angular.isUndefined(row)) {
			InvoiceId = row.InvoiceId;
		}

		cetakUlangFactory.getIncoiveTaxList(start, limit, InvoiceId, '').then(
			function (res) {
				$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
			},
			function (err) {
				console.log('error --> ', err);
			}
		);
	};

	$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		multiselect: false,
		canSelectRows: false,
		columnDefs: [
			{ name: "ChargesId", displayName: "ID", field: "ChargesId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "ChargesType", enableCellEdit: false, enableHiding: false },
			{ name: "DebitKredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "Description", displayName: "Description", field: "Description", enableCellEdit: false, enableHiding: false, visible: false }
			// ,
			// {name:"Action", displayName:"Action",  enableCellEdit: false, enableHiding: false ,
			// 	cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarBiayaLainLain(row)">Hapus</a>'
			// }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_Refund_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		enableFullRowSelection: true,
		multiselect: true,
		canSelectRows: true,
		columnDefs: [
			{ name: "WOSOSPKNumber", displayName: "Nomor SPK/WO/SO Batal", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
			{ name: "WOSPKDate", displayName: "Tanggal SPK/WO/SO Batal", field: "DocumentDate", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal Customer Guarantee", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
			{ name: "WOSOSPKId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false },
			{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_Refund_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		displaySelectionCheckbox: false,
		enableFullRowSelection: true,
		multiselect: false,
		canSelectRows: false,
		columnDefs: [
			{
				name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
				cellTemplate: '<div><input name="AcrcruedSubsidiSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.SubsidiGridSelected(row)"></div>'
			},
			{ name: "SubsidyId", displayName: "SubsidyId", field: "SubsidyId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "Subsidi", displayName: "Subsidi", field: "SubsidyType", enableCellEdit: false, enableHiding: false },
			{ name: "Nominal", displayName: "Nominal Subsidi", field: "SubsidyAmount", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
		],
		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI = gridApi;
		}
	};

	$scope.SubsidiGridSelected = function (row) {
		$scope.TotalNominal = row.entity.SubsidyAmount;
		$scope.CalculateNominalPembayaran();
		$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data.forEach(function (dt) {
			if (dt.SubsidyId == $scope.SelectedGridId) {
				dt.radidata = false;
			}
		});
		$scope.SelectedGridId = row.entity.SubsidyId;
		row.entity.radidata = true;
		//alert(' dipilih ' + row.entity.SubsidyType);
	};

	$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid = {
		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		enableFiltering: true,
		enableSelectAll: false,
		showGridFooter: true,
		displaySelectionCheckbox: false,
		enableFullRowSelection: false,
		multiselect: false,
		canSelectRows: false,
		columnDefs: [
			{ name: "RefundId", displayName: "RefundId", field: "RefundId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "RefundType", displayName: "Tipe Refund", field: "RefundType", enableCellEdit: false, enableHiding: false },
			{
				name: "ReferenceNumber", displayName: "Nomor Referensi", field: "ReferenceNumber", enableCellEdit: false, enableHiding: false,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
			},
			{
				name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2',
				aggregationType: uiGridConstants.aggregationTypes.sum,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
			},
			{ name: "ReferenceId", displayName: "ReferenceId", field: "ReferenceId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false }
			//,
			// {name:"Action", displayName:"Action",field:"Action", enableCellEdit:false,  visible:true,
			// 	cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarRefund(row)">hapus</a>' 
			// }
		],

		onRegisterApi: function (gridApi) {
			$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_gridAPI = gridApi;
		}
	};

	$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran_Selected_Changed = function () {
		if ($scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran == "Cash") {
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = true;
		}
		else {
			$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima_Show = false;
		}
	};

	$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_Selected_Changed = function () {
		if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor != "Pilih Vendor") {
			if ($scope.EditMode == true) {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK;
			}
		}
		if ($scope.EditMode == true) {
			if ($scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor == "Pilih Vendor") {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = false;
			}
			else {
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_EnableDisable = false;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor_Cari_EnableDisable = false;

				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = "";
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_Changed();
			}
		}
		else {
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Kabupaten_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama_EnableDisable = false;
		}
	}

	$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor_Change = function () {
		if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor == "One Time Vendor") {
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaVendor_EnableDisable = false;
			$scope.LogicCariBtn_TopSearch();

			//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
			//$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;	
			if ($scope.EditMode == true) {
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = "";		//Data hasil dari WebAPI
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = "";
			}
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat_EnableDisable = $scope.EditMode;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota_EnableDisable = $scope.EditMode;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor_EnableDisable = $scope.EditMode;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon_EnableDisable = $scope.EditMode;		//Data hasil dari WebAPI
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_EnableDisable = $scope.EditMode;
			//}
		}
		else {
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaVendor_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor_EnableDisable = false;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon_EnableDisable = false;		//Data hasil dari WebAPI
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP_EnableDisable = false;
		}
	};

	$scope.SetColumnUIGridDaftarTagihan = function () {
		$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
		if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
			$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
				{ name: "InvoiceId", displayName: "Nomor Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false },
				{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
				{ name: "VendorName", displayName: "Nama Vendor", field: "VendorName", enableCellEdit: false, enableHiding: false },
				{ name: "NPWP", displayName: "NPWP", field: "NPWP", enableCellEdit: false, enableHiding: false },
				{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
				{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
				{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
				{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }];
		}
		else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {

		}
		else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Refund") ||
			($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Subsidi DP/Rate") ||
			($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "Accrued Komisi Sales")) {
			if ($scope.EditMode == true) {
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Part")) {
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						{
							name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
							cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
						},
						{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
						{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
						{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
						{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},				
						{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: true, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						{
							name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false,
							cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentId}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
						},
						{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
						{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }//,
						//	{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
					];
				}
				else {
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [];
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						{
							name: "radidata", displayName: "", field: "radidata", enableCellEdit: false, enableHiding: false, visible: true, type: "boolean", width: 25,
							cellTemplate: '<div><input name="OtherSelected" ng-model="row.entity.radidata" type="checkbox" ng-change="grid.appScope.OtherGridSelected(row)"></div>'
						},
						{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
						{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
						{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
						{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						//	{name:"DPP", displayName:"DPP",field:"DPP",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},				
						//{name:"BungaDF", displayName:"Bunga DF",field:"BungaDF",enableCellEdit: true, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},		
						{
							name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false,
							cellTemplate: '<div class="ui-grid-cell-contents"><table> <tr><td width="70%">{{row.entity.AdvancePaymentId}}</td><td align="right"><button ng-click="grid.appScope.ShowModalAdvancePayment(row)">Search</button></td></tr></table> </div>'
						},
						{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
						{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }//,
						//{name:"AdvancePaymentId", displayName:"Nomor Advance Payment", field:"AdvancePaymentId", enableCellEdit: false, enableHiding: false}
					];
				}
			}
			else {
				if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Part")) {
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
						{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
						{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
						{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						{ name: "DPP", displayName: "DPP", field: "DPP", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},				
						{ name: "BungaDF", displayName: "Bunga DF", field: "BungaDF", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
						{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
						{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
					];
				}
				else {
					$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.columnDefs = [
						{ name: "InvoiceId", displayName: "Id Invoice Masuk", field: "InvoiceId", enableCellEdit: false, enableHiding: false, visible: false },
						{ name: "InvoiceNo", displayName: "Nomor Invoice Masuk", field: "IncomingInvoiceNumber", enableCellEdit: false, enableHiding: false },
						{ name: "InvoiceReceivedDate", displayName: "Tanggal Invoice Masuk", field: "InvoiceReceivedDate", enableCellEdit: false, enableHiding: false },
						{ name: "MaturityDate", displayName: "Tanggal Jatuh Tempo", field: "MaturityDate", enableCellEdit: false, enableHiding: false },
						{ name: "Nominal", displayName: "Nominal Invoice", field: "Nominal", enableCellEdit: false, enableHiding: false, enableFiltering: true, cellFilter: 'number: 0' },
						//{name:"DPP", displayName:"DPP",field:"DPP",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPN", displayName:"PPN",field:"PPN",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},
						//{name:"PPh22", displayName:"PPh 22",field:"PPh22",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},				
						//{name:"BungaDF", displayName:"Bunga DF",field:"BungaDF",enableCellEdit: false, enableHiding: false, enableFiltering:true, cellFilter: 'number: 0'},		
						{ name: "AdvancePaymentId", displayName: "Nomor Advance Payment", field: "AdvancePaymentId", enableCellEdit: false, enableHiding: false },
						{ name: "AdvanceNominal", displayName: "Nominal Advance Payment", field: "AdvanceNominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
						{ name: "Saldo", displayName: "Saldo", field: "Saldo", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
					];
				}
			}
		}
	}

	$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed = function () {
		$scope.DisableTambahInstruksiPembayaran_TujuanPembayaran_TopSearch();
		$scope.Disable_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop();
		$scope.Disable_Show_TambahInstruksiPembayaran_Refund();

		if ($scope.EditMode == true) {
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
		}

		if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != null) &&
			($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "undefined")) //.length>0)
		{
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				//20170512, eric, begin
				$scope.getGLList();
				$scope.getCostCenterList();
				$scope.getPajakList();
				//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails=true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Vendor";

				//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = true;
				$scope.show_TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch=true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = "";
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = "Pilih Vendor";
			}
			else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") ||
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales")) {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;

				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nomor SO";
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Pelanggan";
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;

				$scope.TambahInstruksiPembayaran_ButtonTambah = false;
				$scope.TambahInstruksiPembayaran_Bottom = true;
				$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService = true;
				$scope.Show_TambahInstruksiPembayaran_DaftarRangka_AccruedFreeService = true;
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaranGeneralTransaction = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
				//$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
				var Mode = '';
				if ($scope.EditMode == true) {
					Mode = "Edit";
				}
				//alert('come in');
				cetakUlangFactory.getDataForAccruedFreeService(1, uiGridPageSize, $scope.DocumentId, '', Mode).then(
					function (resFS) {
						$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = resFS.data.Result;
						$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
					},
					function (errFS) {
						console.log('Error -->', errFS);
					}
				);
			}
			else // if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran=="Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran=="Parts") )
			{
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = true;
				$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = true;
				//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = true;
				$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label = "Nama Vendor";
			}
		}
	};

	$scope.SetRefundGridColumn = function () {
		$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [];
		$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];

		if ($scope.TambahInstruksiPembayaran_TipeRefund == "Customer Guarantee") {
			$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor SPK/WO/SO Batal", value: "Nomor SPK/WO/SO Batal" },
			{ name: "Tanggal SPK/WO/SO Batal", value: "Tanggal SPK/WO/SO Batal" }, { name: "Nominal Down Payment", value: "Nominal Down Payment" }];
			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
			$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Work Order / Sales Order Batal";
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
				{ name: "WOSOSPKNumber", displayName: "Nomor SPK/WO/SO Batal", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "WOSPKDate", displayName: "Tanggal SPK/WO/SO Batal", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "Nominal", displayName: "Nominal Customer Guarantee", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "WOSOSPKId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
			]
		}
		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPh 23") {
			$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Registrasi BP PPh 23", value: "Nomor Registrasi BP PPh 23" },
			{ name: "Tanggal Registrasi BP PPh 23", value: "Tanggal Registrasi BP PPh 23" }, { name: "Nomor Billing", value: "Nomor Billing" },
			{ name: "Tanggal Billing", value: "Tanggal Billing" }];
			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
			$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Bukti Potong PPh 23";
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
				{ name: "DocumentNumber", displayName: "Nomor Registrasi BP PPh 23", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "DocumentDate", displayName: "Tanggal Registrasi BP PPh 23", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "BillingNo", displayName: "Nomor Billing", field: "BillingNo", enableCellEdit: false, enableHiding: false },
				{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "BuktiPotongNo", displayName: "Nomor Bukti Potong", field: "BuktiPotongNumber", enableCellEdit: false, enableHiding: false },
				{ name: "BuktiPotongDate", displayName: "Tanggal Bukti Potong", field: "BuktiPotongDate", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "PPh 23", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
			]
		}
		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "PPN WAPU") {
			$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Registrasi SSP", value: "Nomor Registrasi SSP" },
			{ name: "Tanggal Registrasi SSP", value: "Tanggal Registrasi SSP" }, { name: "Nomor Billing", value: "Nomor Billing" },
			{ name: "Tanggal Billing", value: "Tanggal Billing" }];
			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
			$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Surat Setoran Pajak WAPU";
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
				// {name:"DocumentNumber", displayName:"Nomor Registrasi SSP", field:"DocumentNumber", enableCellEdit: false, enableHiding: false},
				// {name:"DocumentDate", displayName:"Tanggal Registrasi SSP", field:"DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},
				// {name:"BillingNo", displayName:"Nomor Billing", field:"BillingNo", enableCellEdit: false, enableHiding: false},
				// {name:"BillingDate", displayName:"Tanggal Billing", field:"BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},
				// {name:"PPN", displayName:"PPN", field:"Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2'},							
				// {name:"NTPN", displayName:"NTPN", field:"NTPN", enableCellEdit: false, enableHiding: false},
				// {name:"DocumentId", displayName:"Id", field:"DocumentId", enableCellEdit: false, enableHiding: false, visible:false},
				// {name:"DocumentType", displayName:"DocumentType", field:"DocumentType", enableCellEdit: false, enableHiding: false, visible:false},
				// {name:"InnerType", displayName:"InnerType", field:"InnerType", enableCellEdit: false, enableHiding: false, visible:false},
				// {name:"BuktiPotongDate", displayName:"Tanggal Bayar", field:"PaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"'},

				{ name: "DocumentNumber", displayName: "Nomor Registrasi SSP", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "DocumentDate", displayName: "Tanggal Registrasi SSP", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "BillingNo", displayName: "Nomor Billing", field: "BillingNo", enableCellEdit: false, enableHiding: false },
				{ name: "BillingDate", displayName: "Tanggal Billing", field: "BillingDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "PPN", displayName: "PPN", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "NTPN", displayName: "NTPN", field: "NTPN", enableCellEdit: false, enableHiding: false },
				{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false },
				{ name: "BuktiPotongDate", displayName: "Tanggal Bayar", field: "PaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
			]
		}
		else if ($scope.TambahInstruksiPembayaran_TipeRefund == "Titipan") {
			$scope.optionsTambahInstruksiPembayaran_Refund_SearchDropDown = [{ name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" },
			{ name: "Tanggal Incoming Payment", value: "Tanggal Incoming Payment" }, { name: "Nominal Selisih Kelebihan", value: "Nominal Selisih Kelebihan" }];
			$scope.TambahInstruksiPembayaran_Refund_SearchDropDown = null;
			$scope.TambahInstruksiPembayaran_Refund_Header = "Daftar Incoming Payment";
			$scope.TambahInstruksiPembayaran_Refund_UIGrid.columnDefs = [
				{ name: "DocumentNumber", displayName: "Nomor Incoming Payment", field: "DocumentNumber", enableCellEdit: false, enableHiding: false },
				{ name: "DocumentDate", displayName: "Tanggal Incoming Payment", field: "DocumentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "Nominal", displayName: "Nominal Selisih Kelebihan Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "DocumentId", displayName: "Id", field: "DocumentId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "InnerType", displayName: "InnerType", field: "InnerType", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "DocumentType", displayName: "DocumentType", field: "DocumentType", enableCellEdit: false, enableHiding: false, visible: false }
			]
		}
	};

	$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed = function () {
		$scope.SetRefundGridColumn();
		// if ($scope.EditMode == false) {
		// 	$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.columnDefs[6].visible = false;
		// 	//alert('masuk sini');
		// }

		if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == 0 || $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "") {
			if ($scope.EditMode != false) {
				alert('Nama Pelanggan belum dipilih !');
			}
		}
		else {
			var filter;

			if ($scope.EditMode == false) {
				filter = [{
					VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId:
						$scope.DocumentId
				}];
				cetakUlangFactory.getDataForRefund(1, uiGridPageSize, filter).then(
					function (res) {
						$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = res.data.Result
					},
					function (err) {
						console.log('Error -->', err);
					}
				);
			}
			else {
				filter = [{
					VendorId: $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor, RefundType: $scope.TambahInstruksiPembayaran_TipeRefund, PIId:
						0
				}];
				cetakUlangFactory.getDataForRefund(1, uiGridPageSize, filter).then(
					function (res) {
						$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = res.data.Result;
					},
					function (err) {
						console.log('Error -->', err);
					}
				);
			}
		}
	};

	$scope.Disable_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = function () {
		$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
		$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
		$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
		$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
	};

	$scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed = function () {
		$scope.Disable_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop();
		$scope.Show_LihatInstruksiPembayaran_Tagihan = false;
		$scope.Show_LihatInstruksiPembayaran_Refund = false;
		$scope.Disable_Show_TambahInstruksiPembayaran_Refund();

		if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != null) {
			if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
				$scope.Show_LihatInstruksiPembayaran_Refund = true;
			}
			else if ($scope.LihatInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = true;

			}
			else {
				$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
				$scope.Show_LihatInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.Show_LihatInstruksiPembayaran_Tagihan = true;
			}
		}
	};

	$scope.DisableTambahInstruksiPembayaran_TujuanPembayaran_TopSearch = function () {

		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
		//$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch=false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
		$scope.Show_TambahInstruksiPembayaran_Tagihan = false;
		$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_All = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = false;

		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_TopSearch = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;

		$scope.TambahInstruksiPembayaran_ButtonTambah = false;
		$scope.TambahInstruksiPembayaran_Bottom = false;
		$scope.Show_TambahInstruksiPembayaran_DaftarTagihan_AccruedFreeService = false;
		$scope.Show_TambahInstruksiPembayaran_DaftarRangka_AccruedFreeService = false;

	};

	$scope.Disable_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = function () {
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
		$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = false;
		//harja, begin
		Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = false;
		$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = false;
		$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
		$scope.TambahInstruksiPembayaran_DaftarPajak = false;
		//harja, end
	};

	$scope.Disable_Show_TambahInstruksiPembayaran_Refund = function () {
		$scope.Show_TambahInstruksiPembayaran_Refund = false;
		$scope.Show_TambahInstruksiPembayaran_Refund_WoSo = false;
		$scope.Show_TambahInstruksiPembayaran_Refund_Wo = false;
		$scope.Show_TambahInstruksiPembayaran_Refund_Ip = false;
		$scope.Show_TambahInstruksiPembayaran_Refund_Tambah = false;
	};

	$scope.LogicCariBtn_TopSearch = function () {
		$scope.TambahInstruksiPembayaran_DaftarPajak = false;
		$scope.TambahInstruksiPembayaran_ButtonTambah = false;
		var lanjut = false;
		$scope.success = true;
		//alert('isi ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor);
		if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
			//alert(' masuk ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor + ' - ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor );	
			if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor == "Pilih Vendor") &&
				(($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "") ||
					(typeof $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "undefined"))) {
				lanjut = false;
			}
			else {
				lanjut = true;
			}
		}
		else {
			if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "") ||
				(typeof $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor == "undefined")) {
				//alert(' dianggap kosong ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor );
				lanjut = false;
			}
			else {
				lanjut = true;
			}
		}

		if (lanjut == false) {
			alert($scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_Label + ' masih kosong !');
			$scope.success = false;
		}
		else {
			$scope.TambahInstruksiPembayaran_Bottom = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = false;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiTop = true;
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
				var mode;
				if ($scope.EditMode == true) { mode = 'Edit' }
				else { mode = 'Lihat' }

				cetakUlangFactory.getDataForAccrued(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, mode).then(
						function (result) {
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = result.data.Result[0].
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = result.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = result.data.Result[0].BillingNumber;
							$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data = result.data.Result;
							if (mode == 'Lihat' && result.data.Result.length > 0) {
								$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[0].radidata = true;
								// $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].radioShow = $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].SubsidyId;
								// alert( ' isi data --' + $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[1].SubsidyType);
								$scope.SelectedGridId = $scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.data[0].SubsidyId;
								$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_UIGrid.columnDefs[0].visible = false;
								$scope.TambahInstruksiPembayaran_AcrcruedSubsidi_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							}

							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = result.data.Result[0].VendorCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = result.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = result.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = result.data.Result[0].VendorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = result.data.Result[0].Handphone;
							$scope.success = true;
						},
						function (err) {
							console.log('error -->', err);
							$scope.success = false;
						}
					);
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") {
				$scope.success = true;
			}
			else if (($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") ||
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales")) {
				cetakUlangFactory.getVendorInfo($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor).then(
						function (res) {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].Id;
							$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data.Result[0].Name;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI

							if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
								$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK = res.data.Result[0].NameSPK;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_TeleponSPK = res.data.Result[0].PhoneSPK;
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KTPSPK = res.data.Result[0].KTPSPK;
							}
							$scope.success = true;
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NomorPelanggan = res.data[0].Id;		
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Alamat = res.data[0].Address;	
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_KabupatenKota = res.data[0].City;	
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaPelanggan = res.data[0].Name;	
							// $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Telepon = res.data[0].Phone;		//Data hasil dari WebAPI
							//}
						},
						function (err) {
							console.log("err=>", err);
							$scope.success = false;
						}
					);
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor != "One Time Vendor") {
					cetakUlangFactory.getVendorInfo($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor).then(
							function (res) {
								if (res.data.Total > 0) {
									$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = res.data.Result[0].VendorCode;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].Address;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].City;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].Name;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = res.data.Result[0].NPWP;
								}
								$scope.success = true;
							},
							function (err) {
								console.log('Error -->', err);
								$scope.success = false;
							}
						)
				}
				else { $scope.success = true; }

				if ($scope.success == true) {
					$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = true;
					$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
					$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
					$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
					$scope.TambahInstruksiPembayaran_Bottom = true;
				}
			}
			else  //( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
			// 		 ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Free Service") ||
			{
				cetakUlangFactory.getVendorInfo($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran,
					$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor).then(
						function (res) {
							$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_VendorCode = res.data.Result[0].VendorCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor = res.data.Result[0].Id;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].Address;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].City;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].Name;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].Phone;		//Data hasil dari WebAPI

							cetakUlangFactory.getDataInvoiceList(1, 0, $scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NomorVendor,
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, $scope.DocumentId).then(
									function (res) {
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = res.data.Result;
										$scope.success = true;

										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data.forEach(function (row) {
											row.Saldo = row.Nominal;
										});
									},
									function (err) {
										console.log("err=>", err);
										$scope.success = false;
									}
								);
						},
						function (err) {
							console.log('Error -->', err);
							$scope.success = false;
						}
					)
			}
			$scope.SetColumnUIGridDaftarTagihan();
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran != "General Transaction") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = false;
				$scope.Show_TambahInstruksiPembayaran_Tagihan = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedSubsidi = false;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = false;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = false;
				$scope.TambahInstruksiPembayaran_DaftarPajak = false;
				$scope.Show_TambahInstruksiPembayaran_Refund = false;
				$scope.TambahInstruksiPembayaran_ButtonTambah = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedKomisi_VendorSPK = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = false;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = false;
			}
			if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Refund") {
				$scope.Show_TambahInstruksiPembayaran_Refund_DaftarRefund = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop = true;
				$scope.Show_TambahInstruksiPembayaran_Refund = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;

				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "General Transaction") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransactionDetails = true;
				//$scope.TambahInstruksiPembayaran_DaftarPajak = true;
				$scope.show_TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = true;
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
				$scope.TambahInstruksiPembayaran_Bottom = true;
				$scope.show_TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_TypeVendor_TopSearch = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Subsidi DP/Rate") {
				//alert('masuk ke sini subsidi');
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedSubsidi = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
			}
			else if ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Accrued Komisi Sales") {
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiAccruedKomisi_VendorSPK = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_Nama = true;
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
			}
			else //if ( ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Unit") || ($scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran == "Part")  )
			{
				$scope.Show_TambahInstruksiPembayaran_Tagihan = true;
				$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
				$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop = true;
				$scope.TambahInstruksiPembayaran_DaftarPajak = true;
				$scope.Show_TambahInstruksiPembayaran_BiayaLainLain = true;
			}
		}
	};

	$scope.LihatDetailCetakUlang = function (row) {
		console.log($scope.CetakUlang_TipeFormulirCetak_Search);

		$scope.ShowCetakUlangData = false;
		$scope.ShowIncomingPaymentHeader = false;
		$scope.ShowIncomingPaymentData = true;
		$scope.Show_Show_LihatIncomingPayment_RincianPembayaran = true;
		$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = true;
		$scope.Show_Show_LihatIncomingPayment_FootKuitansi = true;
		$scope.ShowBatal = true;
		$scope.Show_LihatAdvancePayment = false;


		if ($scope.CetakUlang_TipeFormulirCetak_Search == "Kuitansi") {
			$scope.currentIPParentId = row.entity.CetakId;
			$scope.currentCustomerId = row.entity.CustomerVendorId;
			$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = row.entity.IncomingPaymentType;
			//$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged();

			//cetakUlangFactory.getCustomersByCustomerId($scope.currentCustomerId)
			cetakUlangFactory.getCustomerDataByIPParentId($scope.currentIPParentId)
				.then
				(
				function (res) {
					//Prospect
					console.log(res.data.Result[0].Alamat);
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_ProspectId = res.data.Result[0].ProspectCode;
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Alamat = res.data.Result[0].Alamat;
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Handphone = res.data.Result[0].Handphone;
					//Branch
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_KodeBranch = res.data.Result[0].KodeBranch;
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaBranch = res.data.Result[0].NamaBranch;
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_Alamat = res.data.Result[0].Alamat;
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaPt = res.data.Result[0].CustomerName;
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_Telepon = res.data.Result[0].Handphone;
					//PelangganBranch
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = res.data.Result[0].ProspectCode;
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NamaPelangganBranch = res.data.Result[0].CustomerName;
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = res.data.Result[0].Alamat;
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_HandphoneTelepon = res.data.Result[0].Handphone;
					//Toyota								
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_ToyotaId = res.data.Result[0].ProspectCode;
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Alamat = res.data.Result[0].Alamat;
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = res.data.Result[0].CustomerName;
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Handphone = res.data.Result[0].Handphone;
				},
				function (err) {
					console.log("err=>", err);
					//Prospect
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_ProspectId = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Alamat = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_KabupatenKota = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_NamaPelanggan = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Prospect_Handphone = "";
					//Branch
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_NomorPelangganKodeBranch = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaBranch = "";
					$scope.LihatIncomingPayment_InformasiPelangan_PelangganBranch_Alamat = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_KabupatenKota = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_NamaPt = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Branch_Telepon = "";
					//Toyota								
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_ToyotaId = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Alamat = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_KabupatenKota = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_NamaPelanggan = "";
					$scope.LihatIncomingPayment_InformasiPelangan_Toyota_Handphone = "";
				}
				);

			cetakUlangFactory.GetListRincianPembayaran(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("1");
					$scope.LihatIncomingPayment_RincianPembayaran_UIGrid.data = res.data.Result;
					$scope.LihatIncomingPayment_NomorKuitansi = res.data.Result[0].ReceiptNo;
					$scope.LihatIncomingPayment_KetKuitansi = res.data.Result[0].ReceiptDesc;
					console.log("2");
				},
				function (err) {
					console.log("err=>", err);
				}
				);

			cetakUlangFactory.GetListRincianBiaya(1, uiGridPageSize, $scope.currentIPParentId)
				.then
				(
				function (res) {
					console.log("3");
					$scope.LihatIncomingPayment_Biaya_UIGrid.data = res.data.Result;
					console.log("4");
				},
				function (err) {
					console.log("err=>", err);
				}
				);

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {
				cetakUlangFactory.GetLihatIncomingPaymentSPK(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("Unit - Booking Fee");
						//console.log(res.data.Result[0].TanggalSPK);							
						$scope.LihatIncomingPayment_InformasiPembayaran_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSpk = res.data.Result[0].NoSPK;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("Unit - Down Payment");
						$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSpk = res.data.Result[0].NoSPK;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("Unit - Full Payment");
						$scope.LihatIncomingPayment_DaftarSalesOrder_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSO = res.data.Result[0].NoSPK;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping")
			//or ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Ekspedisi")
			{
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("Unit - Swapping");
						$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("Unit - Order Pengurusan Dokumen");
						$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			};

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("5");
						$scope.LihatIncomingPayment_DaftarWorkOrderDibayar_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_InformasiPembayaran_WoBill_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorWo = res.data.Result[0].NoWO;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}
			else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentWO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("5");
						$scope.LihatIncomingPayment_InformasiPembayaran_Appointment_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_InformasiPembayaran_WoDown_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorWo = res.data.Result[0].NoWO;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}

			if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("5");
						$scope.LihatIncomingPayment_InformasiPembayaran_SoBill_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_DaftarSalesOrderDibayar_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					},
					function (err) {
						console.log("err=>", err);
					}
					);

			}
			else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
				cetakUlangFactory.GetLihatIncomingPaymentSO(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						console.log("5");
						$scope.LihatIncomingPayment_InformasiPembayaran_Materai_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_InformasiPembayaran_SoDown_UIGrid.data = res.data.Result;
						$scope.LihatIncomingPayment_Top_NomorSo = res.data.Result[0].SONumber;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}
			else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
				cetakUlangFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						$scope.getDataVendor();
						console.log("5");
						$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
						$scope.NamaVendor = res.data.Result[0].VendorName
						console.log(res.data.Result[0].VendorName);
						console.log($scope.NamaVendor);
						$scope.LihatIncomingPayment_Top_NamaLeasing = $scope.NamaVendor;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}
			else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
				cetakUlangFactory.GetLihatIncomingPaymentVendor(1, uiGridPageSize, $scope.currentIPParentId)
					.then
					(
					function (res) {
						$scope.getDataVendor();
						console.log("5");
						$scope.LihatIncomingPayment_DaftarNoRangkaDibayar_UIGrid.data = res.data.Result;
						$scope.NamaVendor = res.data.Result[0].VendorName
						console.log(res.data.Result[0].VendorName);
						console.log($scope.NamaVendor);
						$scope.LihatIncomingPayment_Top_NamaLeasing = $scope.NamaVendor;
					},
					function (err) {
						console.log("err=>", err);
					}
					);
			}
		}
		else if ($scope.CetakUlang_TipeFormulirCetak_Search == "Bukti Jaminan Dealer") {
			console.log("AdvancedPayment");
			$scope.ShowCetakUlangData = false;
			$scope.ShowIncomingPaymentHeader = false;
			$scope.ShowIncomingPaymentData = false;
			$scope.Show_Show_LihatIncomingPayment_RincianPembayaran = false;
			$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = false;
			$scope.Show_Show_LihatIncomingPayment_FootKuitansi = false;
			$scope.Show_LihatAdvancePayment = true;
			$scope.ShowBatal = true;

			cetakUlangFactory.getVendorByName(row.entity.CustomerVendorName)
				.then
				(
				function (res) {
					console.log("Data Vendor ==> ", res);
					//$scope.LihatAdvancePayment_TujuanPembayaran_VendorId = res.data.Result[0].VendorId;
					$scope.LihatAdvancePayment_TujuanPembayaran_NamaVendor = res.data.Result[0].VendorName;
					$scope.LihatAdvancePayment_TujuanPembayaran_NomorVendor = res.data.Result[0].VendorNo;
					$scope.LihatAdvancedPayment_TujuanPembayaran_Alamat = res.data.Result[0].Address;
					$scope.LihatAdvancePayment_TujuanPembayaran_KabupatenKota = res.data.Result[0].KabKota;
					$scope.LihatAdvancePayment_TujuanPembayaran_Telepon = res.data.Result[0].Phone;
				}
				)

			cetakUlangFactory.getDataAdvbyId(row.entity.CetakId)
				.then
				(
				function (res) {
					$scope.LihatAdvancePayment_POId_Selected = res.data.Result[0].POId;
					$scope.POListbyId_UIGrid_Paging(1);
					$scope.LihatAdvancePayment_APId = res.data.Result[0].AdvancedPaymentId;
					$scope.LihatAdvancePayment_MetodePembayaran = res.data.Result[0].APPaymentMethod;
					$scope.LihatAdvancePayment_MetodePembayaran_NominalPembayaran = res.data.Result[0].Nominal;
					$scope.LihatAdvancedPayment_MetodePembayaran_Keterangan = res.data.Result[0].Description;
					$scope.LihatAdvancedPayment_TanggalJatuhTempo = res.data.Result[0].MaturityDate;
					$scope.LihatAdvancePayment_MetodePembayaran_NamaPenerima = res.data.Result[0].ReceiverName;
					$scope.LihatAdvancePayment_NomorAdvancePayment = res.data.Result[0].AdvancedPaymentNumber;
					$scope.LihatAdvancedPayment_TanggalAdvancePayment = res.data.Result[0].APDate;
					$scope.LihatAdvancePayment_Footer_NamaPimpinan = res.data.Result[0].LeaderName;
					$scope.LihatAdvancePayment_Footer_NamaFinance = res.data.Result[0].FinanceName;
					$scope.LihatAdvancePayment_TipeAdvancePayment = res.data.Result[0].APPaymentTypeName;
				}
				)
		}
		else if ($scope.CetakUlang_TipeFormulirCetak_Search == "Bukti Pencatatan Hutang") {
			console.log("Payment Instruction");
			$scope.currentIPParentId = row.entity.CetakId;
			$scope.currentCustomerId = row.entity.CustomerVendorId;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_EnableDisable = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = "";
			console.log(" isi : ", row.entity.IncomingPaymentType);
			$scope.SetComboBranch();
			$scope.DocumentId = row.entity.CetakId;
			$scope.ToTambahInstruksiPembayaran();
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran = row.entity.IncomingPaymentType;
			$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = false;
			$scope.EditMode = false;
			$scope.success = true;
			$scope.TambahInstruksiPembayaran_EditDraft_EnableDisable = true;
			$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = true;
			$scope.Show_TambahInstruksiPembayaran_EditDraft = true;
			$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid_Total = 0;
			$scope.Show_TambahInstruksiPembayaran_TujuanPembayaran_CariBtn_TopSearch = false;

			$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran_Selected_Changed();
			// cetakUlangFactory.getDataBranch().then(
			// 	function(resBr) {
			// 		$scope.optionsTambahInstruksiPembayaran_NamaBranch = resBr.data.Result;
			// 		$scope.TambahInstruksiPembayaran_NamaBranch_EnableDisable = false;
			// 	},
			// 	function (errBr) {
			// 		console.log('error -->', errBr);
			// 	}
			// );


			$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor_EnableDisable = true;
			//tambah disable all//
			//alert('isi row ' + row.entity.DocumentType);
			cetakUlangFactory.getMasterLihat(1, uiGridPageSize, row.entity.CetakId, 0).then(
				function (res) {
					//alert('masuk - call master');
					if ($scope.TambahInstruksiPembayaran_EditDraft_EnableDisable == true) {
						var tanggal = new Date(res.data.Result[0].PaymentInstructionDate);
						tanggal.setMinutes(tanggal.getMinutes() + 420);
						$scope.TambahInstruksiPembayaran_TanggalInstruksiPembayaran = tanggal;
					}
					$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = res.data.Result[0].NamaPimpinan;
					$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = res.data.Result[0].NamaFinance;
					$scope.TambahInstruksiPembayaran_NamaBranch = res.data.Result[0].OutletId;
					if (row.entity.IncomingPaymentType == "General Transaction") {
						console.log(' Vendor type ', res.data.Result[0].VendorType);
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeVendor = res.data.Result[0].VendorType;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						if (res.data.Result[0].VendorType == "Pilih Vendor") {
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
							$scope.LogicCariBtn_TopSearch();
						}
						else { $scope.success = true; }
						if ($scope.success == true) {
							$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak_EnableDisable = true;
							$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak_EnableDisable = true;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Alamat = res.data.Result[0].VendorAddress;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_KabupatenKota = res.data.Result[0].VendorCity;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_Telepon = res.data.Result[0].VendorPhone;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NPWP = res.data.Result[0].VendorNPWP;
							$scope.currentIncomingInvoiceNumber = res.data.Result[0].IncomingInvoiceNumber;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_KodeGlGeneralTransaction = res.data.Result[0].GLCode;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaGlGeneralTransaction = res.data.Result[0].GLCode;

							$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalPPh21 = res.data.Result[0].NominalPPh21;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;

							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].NominalPembayaran;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

							$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = res.data.Result[0].Description;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdAdvPayment = res.data.Result[0].AdvancePaymentId;
							$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorAdvPayment = res.data.Result[0].AdvancedPaymentNumber;
							$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = res.data.Result[0].AdvanceNominal;
							$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = res.data.Result[0].Nominal;

							cetakUlangFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.CetakId).then(
								function (resChg) {
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								},
								function (errChg) {
									console.log('error -->', errChg);
								}
							);
							cetakUlangFactory.getMasterLihatCostCenter(1, uiGridPageSize, row.entity.CetakId).then(
								function (resCC) {
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data = resCC.data.Result;
									$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.columnDefs[2].visible = false;
								},
								function (errCC) {
									console.log('error -->', errCC);
								}
							);

							cetakUlangFactory.getIncoiveTaxList(1, uiGridPageSize, row.entity.CetakId, row.entity.DocumentType).then(
								function (resTax) {
									$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = resTax.data.Result;
									$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.columnDefs[7].visible = false;
								},
								function (errTax) {
									console.log('error -->', errTax);
								}
							);
						}
					}
					else if (row.entity.DocumentType == "Accrued Free Service") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].Nominal;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;

						cetakUlangFactory.getDataForAccruedFreeService(1, uiGridPageSize, row.entity.CetakId, '', 'Lihat').then(
							function (resFS) {
								$scope.TambahInstruksiPembayaran_DaftarTagihanAccruedFreeService_UIGrid.data = resFS.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
								$scope.success = true;
							},
							function (errFS) {
								console.log('Error -->', errFS);
								$scope.success = false;
							}
						);

						cetakUlangFactory.getDataForAccruedFrame(1, uiGridPageSize, row.entity.CetakId, '', 'Lihat').then(
							function (resFrame) {
								$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = [];
								$scope.TambahInstruksiPembayaran_DaftarRangkaAccruedFreeService_UIGrid.data = resFrame.data.Result;
							},
							function (errFrame) {
								console.log('error --> ', errFrame);
							}
						);

						cetakUlangFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.CetakId).then(
							function (resChg) {
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							},
							function (errChg) {
								console.log('error -->', errChg);
							}
						);
					}
					else if (row.entity.DocumentType == "Accrued Subsidi DP/Rate") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].SOId;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorNameTyped;
						$scope.LogicCariBtn_TopSearch();

						$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].NominalPembayaran;
						$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan = res.data.Result[0].Description;
						$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = res.data.Result[0].BillingNo;

						cetakUlangFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.CetakId).then(
							function (resChg) {
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
								$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
								//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
							},
							function (errChg) {
								console.log('error -->', errChg);
							}
						);
					}
					else if (row.entity.DocumentType == "Accrued Komisi Sales") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].SOId;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.LogicCariBtn_TopSearch();
						if ($scope.success == true) {
							//TambahInstruksiPembayaran_TujuanPembayaran_InformasiPelangganTop_NamaVendorSPK
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor = res.data.Result[0].VendorType;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_JenisVendor_Selected_Changed();
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
							$scope.TotalNominal = res.data.Result[0].Nominal;
							//$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
							if (res.data.Result[0].VendorType != "Pilih Vendor") {
								//	alert('masuk sini ' + res.data.Result[0].VendorNameTyped );
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Alamat = res.data.Result[0].VendorAddress;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_KabupatenKota = res.data.Result[0].VendorCity;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Telepon = res.data.Result[0].VendorPhone;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_NoKTP = res.data.Result[0].VendorKTP;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_InformasiVendor_Nama = res.data.Result[0].VendorNameTyped;
							}
							else {
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihIdVendor = res.data.Result[0].VendorId;
								$scope.TambahInstruksiPembayaran_AccruedSubsidi_PilihNamaVendor = res.data.Result[0].VendorNameTyped;
							}
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = res.data.Result[0].NominalPPh21;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = res.data.Result[0].Nominal;
							$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain = res.data.Result[0].Description;

							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].NominalPembayaran;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;

							cetakUlangFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.CetakId).then(
								function (resChg) {
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								},
								function (errChg) {
									console.log('error -->', errChg);
								}
							);
						}
					}
					else if (row.entity.DocumentType == "Refund") {
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;

						// var filter = [ {PIId: row.entity.CetakId , 
						// 	RefundType : row.entity.RefundType, VendorId:  row.entity.VendorId }];
						$scope.LogicCariBtn_TopSearch();
						if ($scope.success == true) {
							if ($scope.TambahInstruksiPembayaran_TipeRefund != res.data.Result[0].RefundType) {
								$scope.TambahInstruksiPembayaran_TipeRefund = res.data.Result[0].RefundType;
							}
							else {
								$scope.TambahInstruksiPembayaran_TipeRefund_Selected_Changed();
							}
							$scope.Show_TambahInstruksiPembayaran_RincianPembayaran = true;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].PaymentAmount;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
							$scope.TambahInstruksiPembayaran_Refund_Keterangan = res.data.Result[0].Description;
						}
					}
					else {  //other unit
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_InformasiVendorTop_NamaVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = res.data.Result[0].VendorName;
						$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = res.data.Result[0].VendorId;
						$scope.LogicCariBtn_TopSearch();
						//alert('pas 1 ' + $scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran);
						//$scope.SetColumnUIGridDaftarTagihan();
						if ($scope.success == true) {
							cetakUlangFactory.getDataInvoiceList(1, uiGridPageSize, $scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor,
								$scope.TambahInstruksiPembayaran_TujuanPembayaran_TipeInstruksiPembayaran, $scope.DocumentId).then(
									function (resInv) {
										//console.log('data invoice -->' , JSON.stringify(resInv));
										$scope.TempDataGrid1 = resInv.data.Result;
										$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = resInv.data.Result;
									},
									function (errInv) {
										console.log('error -->', errInv);
									}
								);

							cetakUlangFactory.getMasterLihatCharges(1, uiGridPageSize, row.entity.CetakId).then(
								function (resChg) {
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = resChg.data.Result;
									$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[5].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.columnDefs[6].visible = false;
									//$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
								},
								function (errChg) {
									console.log('error -->', errChg);
								}
							);
							//console.log("nuse debug masuk other");
							cetakUlangFactory.getIncoiveTaxList(1, uiGridPageSize, row.entity.CetakId, 'Unit').then(
								function (res) {
									$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = res.data.Result;
								},
								function (err) {
									console.log('error --> ', err);
								}
							);
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = res.data.Result[0].PaymentAmount;
							$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = res.data.Result[0].SaldoAnggaran;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = res.data.Result[0].NamaPenerima;
							$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = res.data.Result[0].PaymentMethod;
						}
					}
				},
				function (err) {
					console.log('error -->', err);
					$scope.success = false;
				}
			);

			if ($scope.success == true) {
				$scope.DocumentId = row.entity.CetakId;
			}

		}
	};

	$scope.POListbyId_UIGrid_Paging = function (pageNumber) {
		cetakUlangFactory.getDataPObyId(pageNumber, $scope.uiGridPageSize,
			$scope.LihatAdvancePayment_POId_Selected)
			.then
			(
			function (res) {
				console.log("res =>", res.data.Result);
				$scope.POListbyId_UIGrid.data = res.data.Result;
				$scope.POListbyId_UIGrid.totalItems = res.data.Total;
				$scope.POListbyId_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				$scope.POListbyId_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
			}

			);
	}

	$scope.POListbyId_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		multiSelect: false,
		columnDefs: [
			{ name: "IdPO", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO" },
			{ name: "Tanggal PO", field: "TanggalPO" },
			{ name: "Nominal PO", field: "NominalPO" },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiPOListbyId = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.POListbyId_UIGrid_Paging(pageNumber);
			});
		}
	};


	$scope.ToLihatIncomingPayment = function () {
		$scope.ShowIncomingPaymentData = false;

		$scope.LihatDaftarIncomingPayment_SearchCategoryOptions = [{ name: "Semua Kolom", value: "Semua Kolom" }, { name: "Nama Branch Penerima", value: "Nama Branch Penerima" }, { name: "Nomor Incoming Payment", value: "Nomor Incoming Payment" }, { name: "Tanggal Incoming Payment", value: "Tanggal Incoming Payment" }, { name: "Nominal", value: "Nominal" }];
		$scope.LihatDaftarIncomingPayment_SearchCategory = $scope.LihatDaftarIncomingPayment_SearchCategoryOptions[0];

		$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions = [{ name: "Unit - Booking Fee", value: "Unit - Booking Fee" }, { name: "Unit - Down Payment", value: "Unit - Down Payment" }, { name: "Unit - Full Payment", value: "Unit - Full Payment" }, { name: "Unit - Payment (Kuitansi Penagihan)", value: "Unit - Payment (Kuitansi Penagihan)" }, { name: "Unit - Swapping", value: "Unit - Swapping" }, { name: "Unit - Order Pengurusan Dokumen", value: "Unit - Order Pengurusan Dokumen" }, { name: "Unit - Purna Jual", value: "Unit - Purna Jual" }, { name: "Service - Down Payment", value: "Service - Down Payment" }, { name: "Service - Full Payment", value: "Service - Full Payment" }, { name: "Parts - Down Payment", value: "Parts - Down Payment" }, { name: "Parts - Full Payment", value: "Parts - Full Payment" }, { name: "Bank Statement - Unknown", value: "Bank Statement - Unknown" }, { name: "Bank Statement - Card", value: "Bank Statement - Card" }, { name: "Refund Leasing", value: "Refund Leasing" }, { name: "Interbranch - Penerima", value: "Interbranch - Penerima" }];
		//$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = $scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPaymentOptions[0];
		$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = {};
		DisableAll_LihatIncomingPayment_InformasiPelanggan();
		DisableAll_LihatIncomingPayment_InformasiPembayaran();
		Disable_LihatIncomingPayment_Interbranch_InformasiBranch();
		Disable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		Disable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		Disable_LihatIncomingPayment_DaftarArCreditDebit();
		Disable_LihatIncomingPayment_RincianPembayaran();
		Disable_LihatIncomingPayment_FootKuitansi();
		$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar = "";
		Disable_LihatIncomingPayment_DaftarSalesOrder();
		Disable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		Disable_LihatIncomingPayment_Top_NomorSpk();
		Disable_LihatIncomingPayment_Top_NamaLeasing();
		Disable_LihatIncomingPayment_Top_NomorSo();
		Disable_LihatIncomingPayment_Top_NomorWo();
		Disable_LihatIncomingPayment_Top_NamaAsuransi();
		Disable_LihatIncomingPayment_Top_NamaBank();
	};

	$scope.BackToMainInstruksiPembayaran = function () {
		$scope.DocumentId = 0;
		$scope.EditMode = false;
		$scope.TotalNominal = 0;
		$scope.SelectedGridId = -1;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
		$scope.TambahInstruksiPembayaran_GeneralTransaction_Saldo = 0;
		$scope.TambahInstruksiPembayaran_Bottom = false;
		$scope.ShowTambahInstruksiPembayaran = false;
		$scope.ShowLihatInstruksiPembayaran = false;
		$scope.Show_LihatInstruksiPembayaran_PengajuanReversal = false;
		$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = 0;
		$scope.TambahInstruksiPembayaran_Bottom_NamaPimpinan = "";
		$scope.TambahInstruksiPembayaran_Bottom_NamaFinance = "";
		$scope.TambahInstruksiPembayaran_BiayaLainLain_Nominal = 0;
		$scope.TambahInstruksiPembayaran_BiayaLainLain_Keterangan = "";
		$scope.TambahInstruksiPembayaran_BiayaLainLain_DebetKredit = {};
		$scope.TambahInstruksiPembayaran_BiayaLainLain_TipeBiayaLain = {};
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_SaldoAnggaran = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = 0;
		$scope.TambahInstruksiPembayaran_RincianPembayaran_MetodePembayaran = {};
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_KeteranganInfoLain = "";
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_NominalKomisi = 0;
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_PPH21 = 0;
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
		$scope.TambahInstruksiPembayaran_RincianPembayaran_NamaPenerima = "";
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_NomorBilling = "";
		$scope.TambahInstruksiPembayaran_AccruedSubsidi_Keterangan = "";

		//general transaction
		$scope.TambahInstruksiPembayaran_GeneralTransaction_NominalAdvPayment = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_GeneralTransaction_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_Nominal = 0;
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_Keterangan = "";
		$scope.TambahInstruksiPembayaran_GeneralTransaction_DPP = "";
		$scope.TambahInstruksiPembayaran_GeneralTransaction_NomorDokPajak = "";
		$scope.TambahInstruksiPembayaran_GeneralTransaction_TanggalDokPajak = "";
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NominalPembayaran = "";
		$scope.TambahInstruksiPembayaran_GeneralTransaction_KetObjekPajak = "";

		$scope.TambahInstruksiPembayaran_TotalDaftarTagihan = 0;
		$scope.TambahInstruksiPembayaran_DaftarPajak_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_DaftarBiayaLainLain_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_Refund_DaftarRefund_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_DaftarTagihan_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_GeneralTransaction_DaftarPajak_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_Refund_UIGrid.data = [];
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_NamaPelangganVendor = "";
		$scope.TambahInstruksiPembayaran_TujuanPembayaran_IdPelangganVendor = "";
		$scope.LihatInstruksiPembayaran_DaftarInstruksiPembayaran_Record = "";
	};

	$scope.LihatIncomingPayment_Batal = function () {
		$scope.ShowIncomingPaymentData = false;
		$scope.ShowIncomingPaymentHeader = true;
		$scope.ShowCetakUlangData = true;
		$scope.ShowBatal = false;
		$scope.Show_LihatAdvancePayment = false;
		$scope.BackToMainInstruksiPembayaran();
		DisableAll_LihatIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatIncomingPayment_InformasiPembayaran();//ini semua info pembayaran di hide

		$scope.Show_Show_LihatIncomingPayment_RincianPembayaran = false;
		$scope.Show_Show_LihatIncomingPayment_BiayaBiaya = false;
		$scope.Show_Show_LihatIncomingPayment_FootKuitansi = false;

	}

	$scope.CariIncomingPaymentData = function () {
		if (($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment != null) &&
			($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo != null)) {
			//if ($scope.CetakUlang_TipeFormulirCetak_Search == "Kuitansi")
			//{
			//	//$scope.ShowCetakUlangData = true;
			//	$scope.ShowIncomingPaymentData = true;
			//	$scope.Show_LihatAdvancePayment = false;

			//}
			//else if ($scope.CetakUlang_TipeFormulirCetak_Search == "Bukti Jaminan Dealer")
			//{
			//	//$scope.ShowCetakUlangData = true;
			//	$scope.ShowIncomingPaymentData = false;
			//	$scope.Show_LihatAdvancePayment = true;
			//}

			$scope.GetListIncoming1(1, uiGridPageSize);

		}
		else {
			alert("Tgl Awal/Akhir Blm Diisi");
		}
	};

	$scope.GetListIncoming = function (Start, Limit) {
		// var Start = paginationOptions_LihatCetakUlang_UIGrid.pageNumber;
		// var Limit = paginationOptions_LihatCetakUlang_UIGrid.pageSize;
		// var newString  =  "";
		// if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
		// 	newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-')
		// }else{
		// 	newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
		// }
		// console.log("replace =>>", newString);
		
		if( ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search != undefined) && 
				($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined)){
			if( $scope.LihatIncomingPayment_DaftarIncomingPayment_Search == 'TanggalRef' ){
				console.log("masuk ke sini gak ?");
				if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
					newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-');
				}else{
					newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
				}
				console.log("replace if =>>", newString);
			}else{
				console.log("atau masuk ke sini ?");
				if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
					newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
				}else{
					newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
				}
				console.log("replace else =>>", newString);
			}
			var tmpFlag1 = 0;
			if( $scope.LihatIncomingPayment_DaftarIncomingPayment_Search == 'TanggalRef'){
				tmpFlag1=1;
			}else{
				tmpFlag1=0;
			}
			console.log("replace =>>", newString);
	

		var Start = 1;
		var Limit = 10;

		cetakUlangFactory.GetListCetakUlang(Start, Limit, $filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'), $scope.CetakUlang_TipeFormulirCetak_Search, 
			newString, $scope.LihatIncomingPayment_DaftarIncomingPayment_Search,tmpFlag1)
			.then(
				function (res) {
					$scope.tmpFlag = true;
					$scope.LihatCetakUlang_UIGrid.data = res.data.Result;
					// for(var i in res.data.Result){
					// 	res.data.Result[i].TanggalRef = $scope.changeFormatDate(res.data.Result[i].TanggalRef);
					// }
					$scope.LihatCetakUlang_UIGrid.totalItems = res.data.Total;
					$scope.LihatCetakUlang_UIGrid.paginationPageSize = Limit;
					$scope.LihatCetakUlang_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatCetakUlang_UIGrid.paginationCurrentPage = Start;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
					$scope.tmpFlag = false;
				}
			)	
	
		}else{
			$scope.tmpFlag = false;
			$scope.GetListIncoming1(1, uiGridPageSize);
		}

		
	};

	$scope.changeFormatDate = function(item) {
		var tmpAppointmentDate = item;
		console.log("changeFormatDate item", item);
		tmpAppointmentDate = new Date(tmpAppointmentDate);
		var finalDate
		if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
			var yyyy = tmpAppointmentDate.getFullYear().toString();
			var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = tmpAppointmentDate.getDate().toString();
			finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
		} else {
			finalDate = '';
		}
		console.log("changeFormatDate finalDate", finalDate);
		return finalDate;
	};

	$scope.GetListIncoming1 = function (Start, Limit) {
		var Start = paginationOptions_LihatCetakUlang_UIGrid.pageNumber;
		var Limit = paginationOptions_LihatCetakUlang_UIGrid.pageSize;

		cetakUlangFactory.GetListCetakUlang(Start, Limit, $filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'), $scope.CetakUlang_TipeFormulirCetak_Search)
			.then(
				function (res) {
					$scope.LihatCetakUlang_UIGrid.data = res.data.Result;
					$scope.LihatCetakUlang_UIGrid.totalItems = res.data.Total;
					$scope.LihatCetakUlang_UIGrid.paginationPageSize = Limit;
					$scope.LihatCetakUlang_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatCetakUlang_UIGrid.paginationCurrentPage = Start;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.SetMainGridADH = function () {
		console.log($scope.user.OrgCode);
		$scope.GetListIncomingADH();
	};

	$scope.GetListIncomingADH = function () {
		var Start = paginationOptions_LihatCetakUlang_UIGrid.pageNumber;
		var Limit = paginationOptions_LihatCetakUlang_UIGrid.pageSize;
		cetakUlangFactory.GetListCetakUlang(Start, Limit, "1970-01-01",
			$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'),
			$scope.CetakUlang_TipeFormulirCetak_Search, "diajukan", "Status Pengajuan")
			.then(
				function (res) {
					$scope.LihatCetakUlang_UIGrid.data = res.data.Result;
					$scope.LihatCetakUlang_UIGrid.totalItems = res.data.Total;
					$scope.LihatCetakUlang_UIGrid.paginationPageSize = Limit;
					$scope.LihatCetakUlang_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatCetakUlang_UIGrid.paginationCurrentPage = Start;

					// var d = new Date(res.data.Result[0].StartDate);
					// var StartDate = new Date(d.getFullYear(), d.getMonth() + 1, d.getDate());
					// var StartDate = new Date(res.data.Result[0].StartDate);

					// $scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment = StartDate;
					$scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text = "diajukan";
					$scope.LihatIncomingPayment_DaftarIncomingPayment_Search = "Status Pengajuan";

					// console.log($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment + '   ' + $scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo);

					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.Pengajuan_LihatIncomingPayment_PengajuanReversalModal = function () {
		var data;
		data =
			[{
				"ApprovalTypeName": "IncomingPayment Reversal",
				"DocumentId": $scope.currentIPParentId,
				"ReversalReason": $scope.LihatIncomingPayment_PengajuanReversal_AlasanReversal
			}]

		cetakUlangFactory.submitDataApproval(data)
			.then
			(
			function (res) {
				console.log(res);
				$scope.GetListIncoming();
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	};

	// $scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged = function () {
	// console.log("LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar_ValueChanged");
	// //console.log(LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar);
	// //if ($scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar != "") {
	// //	$scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment = $scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar;
	// Enable_LihatIncomingPayment_FootKuitansi();
	// Enable_LihatIncomingPayment_RincianPembayaran();
	// //alert(nanti jangan lupa program biar value ni tabel nongol);
	// //}
	// };

	$scope.LihatIncomingPayment_RincianPembayaran_TipeIncomingPayment_Selected_Changed = function () {

		DisableAll_LihatIncomingPayment_InformasiPelanggan();//ini semua info pelanggan di hide
		DisableAll_LihatIncomingPayment_InformasiPembayaran();//ini semua info pebayaran di hide
		Disable_LihatIncomingPayment_Interbranch_InformasiBranch();
		Disable_LihatIncomingPayment_DaftarNoRangkaDibayar();
		Disable_LihatIncomingPayment_DaftarSalesOrderDibayar();
		Disable_LihatIncomingPayment_DaftarArCreditDebit();
		//Disable_LihatIncomingPayment_RincianPembayaran();
		//Disable_LihatIncomingPayment_FootKuitansi();
		//$scope.LihatIncomingPayment_DaftarIncomingPayment_SelectedDaftar="";
		Disable_LihatIncomingPayment_DaftarSalesOrder();
		Disable_LihatIncomingPayment_DaftarWorkOrderDibayar();
		Disable_LihatIncomingPayment_Top_NomorSpk();
		Disable_LihatIncomingPayment_Top_NamaLeasing();
		Disable_LihatIncomingPayment_Top_NomorSo();
		Disable_LihatIncomingPayment_Top_NomorWo();
		Disable_LihatIncomingPayment_Top_NamaAsuransi();
		Disable_LihatIncomingPayment_Top_NamaBank();

		if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Booking Fee") {

			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_InformasiPembayaran_Spk();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();

		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Down Payment") {
			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_DaftarSalesOrder();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorSpk();
			Enable_LihatIncomingPayment_InformasiPelangan_Prospect();
			Enable_LihatIncomingPayment_DaftarSalesOrder();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Payment (Kuitansi Penagihan)") {
			Enable_LihatIncomingPayment_Top_NamaLeasing();
			Enable_LihatIncomingPayment_DaftarNoRangkaDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Refund Leasing") {
			Enable_LihatIncomingPayment_Top_NamaLeasing();
			Enable_LihatIncomingPayment_DaftarNoRangkaDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Swapping") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_Branch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Order Pengurusan Dokumen") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Unit - Purna Jual") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Down Payment") {

			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			Enable_LihatIncomingPayment_InformasiPembayaran_Appointment();
			Enable_LihatIncomingPayment_InformasiPembayaran_WoDown();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Service - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorWo();
			Enable_LihatIncomingPayment_InformasiPelangan_Toyota();
			Enable_LihatIncomingPayment_InformasiPembayaran_WoBill();
			Enable_LihatIncomingPayment_DaftarWorkOrderDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Down Payment") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_PelangganBranch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoDown();
			Enable_LihatIncomingPayment_InformasiPembayaran_Materai();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Parts - Full Payment") {
			Enable_LihatIncomingPayment_Top_NomorSo();
			Enable_LihatIncomingPayment_InformasiPelangan_PelangganBranch();
			Enable_LihatIncomingPayment_InformasiPembayaran_SoBill();
			Enable_LihatIncomingPayment_DaftarSalesOrderDibayar();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Unknown") {

			//cari apa isinya
			//emang kosong
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Bank Statement - Card") {
			Enable_LihatIncomingPayment_Top_NamaBank();
			Enable_LihatIncomingPayment_DaftarArCreditDebit();
			Enable_LihatIncomingPayment_FootKuitansi();
			Enable_LihatIncomingPayment_RincianPembayaran();
		}
		else if ($scope.LihatIncomingPayment_TujuanPembayaran_TipeIncomingPayment == "Interbranch - Penerima") {
			Enable_LihatIncomingPayment_Interbranch_InformasiBranch();
		}
	};



	//Filter LihatIncomingPayment
	// $scope.LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked = function () {
	// 	console.log("LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked");
	// 	if (($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != "") &&
	// 		($scope.LihatIncomingPayment_DaftarIncomingPayment_Search != "")) {
	// 		// var nomor;
	// 		// var value = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 		// $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 		// console.log('filter text -->', $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text);
	// 		// if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "No Formulir Cetak") {
	// 		// 	nomor = 1;
	// 		// }
	// 		// if ($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "Tgl Formulir Cetak") {
	// 		// 	nomor = 2;
	// 		// }
	// 		// console.log("nomor --> ", nomor);
	// 		// $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 		$scope.GetListIncoming(1, uiGridPageSize);
	// 	}
	// 	else {
	// 		$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 		//alert("inputan filter belum diisi !");
	// 	}
	// };

	//NEW Filter View
	// $scope.LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked = function () {
	// 	if (($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != "") &&
	// 		($scope.LihatIncomingPayment_DaftarIncomingPayment_Search != null && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search != undefined)) {
	// 		var nomor;
	// 		var value = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
			
	// 		var newString  =  "";
	// 		if( $scope.LihatIncomingPayment_DaftarIncomingPayment_Search == 'Tgl Formulir Cetak'){
	// 			console.log("masuk ke sini gak ?");
	// 			if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
	// 				newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-');
	// 			}else{
	// 				newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 			}
	// 			console.log("replace if =>>", newString);
	// 		}else{
	// 			console.log("atau masuk ke sini ?");
	// 			if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
	// 				newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 			}else{
	// 				newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 			}
	// 			console.log("replace else =>>", newString);
	// 		}
	// 		cetakUlangFactory.GetListCetakUlang(1, uiGridPageSize, $filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment), 'yyyy-MM-dd'),
	// 		$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'), $scope.CetakUlang_TipeFormulirCetak_Search, 
	// 		newString, $scope.LihatIncomingPayment_DaftarIncomingPayment_Search)
	// 		.then(
	// 			function (res) {
	// 				console.log("data cetak ulang",res.data.Result);
	// 				$scope.tmpFlag= true;
	// 				$scope.LihatCetakUlang_UIGrid.data = res.data.Result;
	// 				$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 				$scope.LihatCetakUlang_UIGrid.grid.getColumn($scope.LihatIncomingPayment_DaftarIncomingPayment_Search).filters[0].term = value;
	// 					$timeout(function(){
	// 						console.log("data 1", $scope.LihatCetakUlang_UIGrid.data);
	// 						console.log("data 2", $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.core.getVisibleRows().length);
							
	// 						$scope.LihatCetakUlang_UIGrid.totalItems = $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.core.getVisibleRows().length;	
	// 						$scope.LihatCetakUlang_UIGrid.paginationPageSize = $scope.uiGridPageSize;
	// 						$scope.LihatCetakUlang_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
	// 					},100);
	// 			},
	// 			function (err) {
	// 				console.log("err=>", err);
	// 			}
	// 		)
			
	// 	}else{
	// 		$scope.tmpFlag = false;
	// 		$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 		$scope.GetListIncoming(1, uiGridPageSize);

	// 	}
	// }

	$scope.LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked = function () {
		$scope.GetListIncoming(1,uiGridPageSize);
	}

	// $scope.LihatIncomingPayment_DaftarIncomingPayment_Filter_Clicked = function () {
	// 	if (($scope.LihatIncomingPayment_DaftarIncomingPayment_Search == "TanggalRef")) {
	// 		var nomor;
	// 		var value = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 		var newString  =  "";

	// 		console.log("masuk ke sini gak ?");
	// 		if($scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != '' && $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text != undefined){
	// 			newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-');
	// 		}else{
	// 			newString = $scope.LihatIncomingPayment_DaftarIncomingPayment_Search_Text;
	// 		}
	// 		console.log("replace if =>>", newString);

	// 		var tmpFlag1 = 0;
	// 		if( $scope.LihatIncomingPayment_DaftarIncomingPayment_Search == 'TanggalRef'){
	// 			tmpFlag1=1;
	// 		}else{
	// 			tmpFlag1=0;
	// 		}

	// 		cetakUlangFactory.GetListCetakUlang(1, uiGridPageSize, $filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPayment), 'yyyy-MM-dd'),
	// 		$filter('date')(new Date($scope.LihatIncomingPayment_TujuanPembayaran_TanggalIncomingPaymentTo), 'yyyy-MM-dd'), $scope.CetakUlang_TipeFormulirCetak_Search, 
	// 		newString, $scope.LihatIncomingPayment_DaftarIncomingPayment_Search,tmpFlag1)
	// 		.then(
	// 			function (res) {
	// 				console.log("data alokasi",res.data.Result);
	// 				$scope.tmpFlag = true;
	// 				$scope.LihatCetakUlang_UIGrid.data = res.data.Result;
	// 				$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 				console.log(">>>>>>>>>>>>", $scope.LihatIncomingPayment_DaftarIncomingPayment_Search, value, $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.getColumn($scope.LihatIncomingPayment_DaftarIncomingPayment_Search));
	// 				// $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.getColumn($scope.LihatIncomingPayment_DaftarIncomingPayment_Search).filters[0].term = value;
	// 					$timeout(function(){
	// 						console.log("data 1", $scope.LihatCetakUlang_UIGrid.data);
	// 						console.log("data 2", $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.core.getVisibleRows().length);
							
	// 						$scope.LihatCetakUlang_UIGrid.totalItems = $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.core.getVisibleRows().length;	
	// 						$scope.LihatCetakUlang_UIGrid.paginationPageSize = $scope.uiGridPageSize;
	// 						$scope.LihatCetakUlang_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];
	// 					},100);
	// 			},
	// 			function (err) {
	// 				console.log("err=>", err);
	// 			}
	// 		)
			
	// 	}else{
	// 		$scope.tmpFlag = false;
	// 		$scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.grid.clearAllFilters();
	// 		$scope.GetListIncoming(1, uiGridPageSize);

	// 	}
	// }

	/*===============================Bulk Action Section, Begin===============================*/
	$scope.LihatIncomingPayment_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");
		if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Setuju") {
			// angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
			// 	$scope.ModalTolakIncomingPayment_IPId = value.CetakId;
			// 	$scope.ModalTolakCetakUlang_IPParentId = value.CetakId;
			// 	$scope.ModalTolakCetakUlang_Tipe = value.TipeFormCetak;
			// 	if (value.StatusPengajuan == "Diajukan") {
			// 		$scope.CreateApprovalData("Disetujui");
			// 		cetakUlangFactory.updateApprovalData($scope.ApprovalData)
			// 			.then(
			// 				function (res) {
			// 					// alert("Berhasil Disetujui");
			// 					bsNotify.show({
			// 						title: "Berhasil",
			// 						content: "Berhasil Disetujui",
			// 						type: 'success'
			// 					});
			// 					$scope.TolakCetakUlangBatal_Modal();
			// 				},

			// 				function (err) {
			// 					// alert("Persetujuan gagal");
			// 					bsNotify.show({
			// 						title: "Gagal",
			// 						content: "Persetujuan gagal",
			// 						type: 'danger'
			// 					});
			// 				}
			// 			);
			// 	}
			// });

			// ============================ BEGIN | NOTO | 190201 | ASYNC INSIDE LOOP | ADH - Setuju ============================
			var loopPromises = [];
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value) {
				var deferred = $q.defer();
				loopPromises.push(deferred.promise);
				//sample of a long-running operation inside loop...
				setTimeout(function () {
					deferred.resolve();
					//do async here
					//OLD
			// 		var StatusPengajuan = (value.StatusPengajuan || "").toString();
			// 		if (StatusPengajuan.toLowerCase() == "diajukan") {
			// 			// $scope.ModalTolakIncomingPayment_IPId = value.CetakId;
			// 			// $scope.ModalTolakCetakUlang_IPParentId = value.CetakId;
			// 			// $scope.ModalTolakCetakUlang_Tipe = value.TipeFormCetak;
			// 			$scope.ModalPengajuanCetakUlang_NoForm = value.ReceiptNo;
			// 			$scope.ModalSetujuhCetakUlang_IPParentId = value.CetakId;
			// 			$scope.ModalPengajuanCetakUlang_TglRef = $filter('date')(new Date(), 'dd-MM-yyyy');
			// 			$scope.ModalPengajuanCetakUlang_Tipe = value.TipeFormCetak;
			// 			$scope.ModalPengajuanCetakUlang_TotalPencetakan = value.PrintCounter;
			// 			$scope.ModalPengajuanCetakUlang_TanggalPengajuan = $filter('date')(new Date(), 'dd-MM-yyyy');
			// 			$scope.ModalPengajuanCetakUlang_Tipe = value.TipeFormCetak; 
	
			// 			// Alokasi unknown
			// 			if (value.ReceiptNo.toLowerCase() == "null") {
			// 				value.ReceiptNo = "";
			// 			}
	
			// 			console.log("tipe cetakan :", $scope.ModalTolakCetakUlang_Tipe );
			// 			cetakUlangFactory.updateApprovalData($scope.ApprovalData)
			// 				.then(
			// 					function (res) {
			// 						// alert("Berhasil Disetujui");
			// 						bsNotify.show({
			// 							title: "Berhasil",
			// 							content: value.ReceiptNo + " Berhasil Disetujui",
			// 							type: 'success'
			// 						});
			// 						console.log("tipe cetak ulang ?>>>>>>>", $scope.ModalPengajuanCetakUlang_Tipe);
			// 						$scope.TolakCetakUlangBatal_Modal();
			// 					},
	
			// 					function (err) {
			// 						// alert("Persetujuan gagal");
			// 						bsNotify.show({
			// 							title: "Gagal",
			// 							content: value.ReceiptNo + " Persetujuan gagal",
			// 							type: 'danger'
			// 						});
			// 					}
			// 				);
						
			// 		}

			// 		else {
			// 			bsNotify.show({
			// 				title: "Warning",
			// 				content: "Status cetak ulang dokumen "+ value.ReceiptNo +" belum diajukan.",
			// 				type: 'warning'
			// 			});
			// 		}

			// 	}, 2000);

			// });
			// New
			$scope.ModalPengajuanCetakUlang_NoForm = value.ReceiptNo;
			$scope.ModalSetujuhCetakUlang_IPParentId = value.CetakId;
			$scope.ModalPengajuanCetakUlang_TglRef = $filter('date')(new Date(), 'dd-MM-yyyy');
			$scope.ModalPengajuanCetakUlang_Tipe = value.TipeFormCetak;
			$scope.ModalPengajuanCetakUlang_TotalPencetakan = value.PrintCounter;
			$scope.ModalPengajuanCetakUlang_TanggalPengajuan = $filter('date')(new Date(), 'dd-MM-yyyy');
				if (value.StatusPengajuan == "Diajukan") {
					$scope.CreateApprovalDataSetujui("Disetujui");
					cetakUlangFactory.updateApprovalData($scope.ApprovalData)
						.then(
							function (res) {
								// alert("Berhasil Disetujui");
								bsNotify.show({
									title: "Berhasil",
									content: value.ReceiptNo + " Berhasil Disetujui",
									type: 'success'
								});
								console.log("tipe cetak ulang ?>>>>>>>", $scope.ModalPengajuanCetakUlang_Tipe);
								$scope.TolakCetakUlangBatal_Modal();
							},

							function (err) {
								// alert("Persetujuan gagal");
								bsNotify.show({
									title: "Gagal",
									content: value.ReceiptNo + " Persetujuan gagal",
									type: 'danger'
								});
							}
						);
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Status cetak ulang dokumen "+ value.ReceiptNo +" belum diajukan.",
						type: 'warning'
					});
				}

			}, 2000);

		});
			// ============================ END | NOTO | 190201 | ASYNC INSIDE LOOP | ADH - Tolak ============================
		}
		else if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI);
			
			var isPengajuan = true;
			var noRei = '';
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value) {
				if (value.StatusPengajuan != "Diajukan" && isPengajuan)
				{
					isPengajuan = false;
					noRei = value.ReceiptNo
				}
			});

			if(isPengajuan){
				angular.element('#ModalTolakCetakUlang').modal('show');
			}
			else
			{
				bsNotify.show({
					title: "Warning",
					content: noRei + " Status pengajuan harus diajukan",
					type: 'warning'
				});
				// return false;
			}
		}
		else if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Pengajuan") {
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value) {
				if (value.TipeFormCetak != "Kuitansi") {
					$scope.ModalKuitansi_Status_Hidden = true;
				}
			});

			angular.element('#ModalPengajuanCetakUlang').modal('show');
			// console.log("grid api ap list ", $scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI);
			// angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
			// 	console.log(value);
			// 	$scope.ModalPengajuanCetakUlang_NoForm = value.ReceiptNo;
			// 	$scope.ModalPengajuanCetakUlang_IPParentId = value.CetakId;
			// 	//$scope.ModalPengajuanCetakUlang_TglRef = value.TanggalRef;
			// 	$scope.ModalPengajuanCetakUlang_TglRef = $filter('date')(new Date(), 'dd-MM-yyyy');
			// 	$scope.ModalPengajuanCetakUlang_Tipe = value.TipeFormCetak;
			// 	$scope.ModalPengajuanCetakUlang_TotalPencetakan = value.PrintCounter;
			// 	//$scope.ModalPengajuanCetakUlang_TanggalPengajuan = value.TglPengajuan;
			// 	$scope.ModalPengajuanCetakUlang_TanggalPengajuan = $filter('date')(new Date(), 'dd-MM-yyyy');

			// 	//if(value.StatusPengajuan = null)
			// 	//angular.element('#ModalPengajuanCetakUlang').modal();
			// 	angular.element('#ModalPengajuanCetakUlang').modal('show');
			// }
			// );
		}
		else if ($scope.LihatIncomingPayment_DaftarBulkAction_Search == "Cetak Ulang") {
			angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
				var StatusPengajuan = (value.StatusPengajuan || "").toString();
				if (StatusPengajuan.toLowerCase() == "disetujui") {
					console.log(value);
					if (value.LastPrinted < value.LastApproved) {
						$scope.ModalPengajuanCetakUlang_IPParentId = value.CetakId;
						if (value.TipeFormCetak == "Kuitansi") {
							if(value.IncomingPaymentType == "Interbranch - Penerima"){
								$scope.CetakKuitansiTTUS();
							}else{
								$scope.CetakKuitansi();
							}
						}
						else if (value.TipeFormCetak == "Bukti Pencatatan Hutang") {
							$scope.CetakBPH(value.MetodePembayaran);
						}
						else { //bpj
							$scope.Report_Cetak_Clicked($scope.ModalPengajuanCetakUlang_IPParentId, 0,
								value.MetodePembayaran)
						}
					}
					else {
						// alert("Dokumen ini sudah pernah cetak ulang, silakan melakukan pengajuan kembali.");
						bsNotify.show({
							title: "Warning",
							content: "Dokumen "+ value.ReceiptNo +" sudah pernah cetak ulang, silakan melakukan pengajuan kembali.",
							type: 'warning'
						});
					}
				}
				else {
					bsNotify.show({
						title: "Warning",
						content: "Status cetak ulang dokumen "+ value.ReceiptNo +" belum disetujui.",
						type: 'warning'
					});
				}
			}
			);
		}
		$scope.LihatIncomingPayment_DaftarBulkAction_Search = "";
	}

	$scope.Report_Cetak_Clicked = function (APId, APTypeId, APPaymentMethod) {
		var pdfFile = "";
		var endDate = "";
		console.log("masuk cetak dengan apid :", APId);
		if (APPaymentMethod == 'Bank Transfer') {
			cetakUlangFactory.getCetakReportAdvancePaymentBJD(APId, APTypeId)
				.then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);

						console.log("pdf", fileURL);
						$scope.GetListIncoming();
						printJS(fileURL);
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}
		else {
			cetakUlangFactory.getCetakReportAdvancePaymentBJDCash(APId, APTypeId)
				.then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileURL = URL.createObjectURL(file);

						console.log("pdf", fileURL);
						$scope.GetListIncoming();
						printJS(fileURL);
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}
	};

	$scope.CetakBPH = function (metodebayar) {
		if (metodebayar == "Cash") {
			cetakUlangFactory.cetakBPHCash($scope.ModalPengajuanCetakUlang_IPParentId, 1, 1, $scope.user.OrgId).then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileURL = URL.createObjectURL(file);
					console.log("pdf", fileURL);
					$scope.GetListIncoming();
					printJS(fileURL);
					//printJS({printable: file,showModal:true,type: 'pdf'})
				},
				function (err) {
					console.log("err=>", err);
				}
			);
		}
		else {
			cetakUlangFactory.cetakBPHBankTransfer($scope.ModalPengajuanCetakUlang_IPParentId, 1, 1, $scope.user.OrgId).then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileURL = URL.createObjectURL(file);
					console.log("pdf", fileURL);
					$scope.GetListIncoming();
					printJS(fileURL);
					//printJS({printable: file,showModal:true,type: 'pdf'})
				},
				function (err) {
					console.log("err=>", err);
				}
			);
		}
	}

	$scope.CetakKuitansi = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak Kuitansi");
		console.log($scope.ModalPengajuanCetakUlang_IPParentId);

		cetakUlangFactory.getCetakKuitansi($scope.ModalPengajuanCetakUlang_IPParentId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);

					$scope.GetListIncoming();
					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.CetakKuitansiTTUS = function () {
		var pdffile = "";
		var enddate = "";
		console.log("Cetak Kuitansi");
		console.log($scope.ModalPengajuanCetakUlang_IPParentId);

		cetakUlangFactory.getCetakKuitansiTTUS($scope.ModalPengajuanCetakUlang_IPParentId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);

					$scope.GetListIncoming();
					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.CreateApprovalData = function (ApprovalStatus) {
		var KuitansiType = 0;
		console.log ("modal pengajuan: ",$scope.ModalPengajuanCetakUlang_Tipe );

		if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Jaminan Dealer')
		KuitansiType = 3901;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Pencatatan Hutang')
		KuitansiType = 3902;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Kuitansi')
		KuitansiType = 3903;

		$scope.ApprovalData = [
			{
				"ApprovalTypeName": "Cetak Ulang",
				"RejectedReason": $scope.ModalCetakUlang_AlasanPenolakan,
				"DocumentId": $scope.ModalTolakCetakUlang_IPParentId,
				"ApprovalStatusName": ApprovalStatus,
				"KuitansiType": KuitansiType
			}
		];
	}

	$scope.CreateApprovalDataSetujui = function (ApprovalStatus) {
		var KuitansiType = 0;

		if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Jaminan Dealer')
		KuitansiType = 3901;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Pencatatan Hutang')
		KuitansiType = 3902;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Kuitansi')
		KuitansiType = 3903;

		$scope.ApprovalData = [
			{
				"ApprovalTypeName": "Cetak Ulang",
				"ApprovalReason": $scope.ModalCetakUlang_AlasanPenolakan,
				"DocumentId": $scope.ModalSetujuhCetakUlang_IPParentId,
				"ApprovalStatusName": ApprovalStatus,
				"KuitansiType": KuitansiType
			}
		];
	}

	$scope.TolakCetakUlangBatal_Modal = function () {
		angular.element('#ModalTolakCetakUlang').modal('hide');
		$scope.LihatIncomingPayment_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListIncoming();
	}

	$scope.TolakCetakUlangTolak_Modal = function () {
		var alasan = angular.isUndefined($scope.ModalCetakUlang_AlasanPenolakan) ? '' : $scope.ModalCetakUlang_AlasanPenolakan;
		if(alasan == ''){
			bsNotify.show({
				title: "Warning",
				content: "Alasan pengajuan harus diisi",
				type: 'warning'
			});
			return false;
		}
		
		// ============================ BEGIN | NOTO | 190201 | ASYNC INSIDE LOOP | ADH - Tolak ============================
		var loopPromises = [];
		angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value) {
			var deferred = $q.defer();
			loopPromises.push(deferred.promise);
			//sample of a long-running operation inside loop...
			setTimeout(function () {
				deferred.resolve();
				//do async here
				$scope.ModalTolakCetakUlang_NoForm = value.ReceiptNo;
				$scope.ModalTolakCetakUlang_IPParentId = value.CetakId;
				$scope.ModalTolakCetakUlang_TanggalRef = $filter('date')(new Date(value.TanggalRef), 'dd-MM-yyyy');
				$scope.ModalTolakCetakUlang_Tipe = value.TipeFormCetak;
				$scope.ModalTolakCetakUlang_TotalPencetakan = value.PrintCounter;
				$scope.ModalPengajuanCetakUlang_TanggalPengajuan = $filter('date')(new Date(value.TglPengajuan), 'dd-MM-yyyy');

				$scope.CreateApprovalData("Ditolak");
				cetakUlangFactory.updateApprovalData($scope.ApprovalData)
					.then(
						function (res) {
							//alert("Berhasil tolak");
							bsNotify.show({
								title: "Berhasil",
								content: value.ReceiptNo + " Berhasil tolak",
								type: 'success'
							});
							$scope.TolakCetakUlangBatal_Modal();
						},
		
						function (err) {
							//alert("Penolakan gagal");
							bsNotify.show({
								title: "Warning",
								content: value.ReceiptNo + " Penolakan gagal",
								type: 'warning'
							});
						}
					);

			}, 2000);

		});
		// ============================ END | NOTO | 190201 | ASYNC INSIDE LOOP | ADH - Tolak ============================
	}

	$scope.CreateApprovalDataPengajuan = function (ApprovalStatus) {
		var KuitansiType = 0;

		if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Jaminan Dealer')
			KuitansiType = 3901;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Bukti Pencatatan Hutang')
		KuitansiType = 3902;
		else if ($scope.ModalPengajuanCetakUlang_Tipe == 'Kuitansi')
		KuitansiType = 3903;

		$scope.ApprovalData = [
			{
				"ApprovalTypeName": "Cetak Ulang",
				"ReversalReason": $scope.ModalCetakUlang_AlasanPengajuan,
				"DocumentId": $scope.ModalPengajuanCetakUlang_IPParentId,
				"ApprovalStatusName": ApprovalStatus,
				"KuitansiType": KuitansiType,
				"HistoryLost": $scope.HistoryLost
			}
		];
	}

	$scope.PengajuanCetakUlangBatal_Modal = function () {
		angular.element('#ModalPengajuanCetakUlang').modal('hide');
		$scope.LihatIncomingPayment_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListIncoming();
	}

	$scope.PengajuanCetakUlangPengajuan_Modal = function () {
		var alasan = angular.isUndefined($scope.ModalCetakUlang_AlasanPengajuan) ? '' : $scope.ModalCetakUlang_AlasanPengajuan;
		if(alasan == ''){
			bsNotify.show({
				title: "Warning",
				content: "Alasan pengajuan harus diisi",
				type: 'warning'
			});
			return false;
		}

		// ============================ BEGIN | NOTO | 190201 | ASYNC INSIDE LOOP | Kasir - Pengajuan ============================
		var loopPromises = [];
		angular.forEach($scope.LihatIncomingPayment_DaftarIncomingPayment_gridAPI.selection.getSelectedRows(), function (value) {
			var deferred = $q.defer();
			loopPromises.push(deferred.promise);
			//sample of a long-running operation inside loop...
			setTimeout(function () {
				deferred.resolve();
				//do async here
				console.log(value);
				var StatusPengajuan = (value.StatusPengajuan || "").toString();
				if (StatusPengajuan.toLowerCase() == "diajukan") {
					console.log(value);
					bsNotify.show({
						title: "Warning",
						content: "Status dokumen "+ value.ReceiptNo +" sudah diajukan.",
						type: 'warning'
					});
				}
				else {
					if ($scope.ModalKuitansi_Hilang == true) {
						$scope.HistoryLost = 1;
					}
					else {
						$scope.HistoryLost = 0;
					}
	
					$scope.ModalPengajuanCetakUlang_NoForm = value.ReceiptNo;
					$scope.ModalPengajuanCetakUlang_IPParentId = value.CetakId;
					$scope.ModalPengajuanCetakUlang_TglRef = $filter('date')(new Date(), 'dd-MM-yyyy');
					$scope.ModalPengajuanCetakUlang_Tipe = value.TipeFormCetak;
					$scope.ModalPengajuanCetakUlang_TotalPencetakan = value.PrintCounter;
					$scope.ModalPengajuanCetakUlang_TanggalPengajuan = $filter('date')(new Date(), 'dd-MM-yyyy');
	
					$scope.CreateApprovalDataPengajuan("Diajukan");
					cetakUlangFactory.submitDataApproval($scope.ApprovalData)
						.then(
							function (res) {
								console.log('Pengajuan ... ' + value.ReceiptNo);
								bsNotify.show({
									title: "Berhasil",
									content: value.ReceiptNo + " Berhasil Pengajuan",
									type: 'success'
								});
								$scope.PengajuanCetakUlangBatal_Modal();
							},
							function (err) {
								// alert("Pengajuan gagal");
								bsNotify.show({
									title: "Berhasil",
									content: value.ReceiptNo + " Pengajuan gagal",
									type: 'danger'
								});
							}
						);
	
				}
			}, 2000);

		});
		angular.element('#ModalPengajuanCetakUlang').modal('hide');
		// $q.all(loopPromises).then(function () {
		// 	console.log('foreach loop completed. Do something after it...');
		// });
		// ============================ END | NOTO | 190201 | ASYNC INSIDE LOOP | Kasir - Pengajuan ============================	

	}

	$scope.ClearActionFields = function () {
		$scope.ModalPengajuanCetakUlang_NoForm = "";
		$scope.ModalPengajuanCetakUlang_IPParentId = "";
		$scope.ModalPengajuanCetakUlang_TglRef = "";
		$scope.ModalPengajuanCetakUlang_Tipe = "";
		$scope.ModalPengajuanCetakUlang_TotalPencetakan = "";
		$scope.ModalPengajuanCetakUlang_TanggalPengajuan = "";
		$scope.ModalCetakUlang_AlasanPengajuan = "";
		$scope.ModalTolakCetakUlang_NoForm = "";
		$scope.ModalTolakCetakUlang_IPParentId = "";
		$scope.ModalTolakIncomingPayment_TanggalRef = "";
		$scope.ModalTolakCetakUlang_Tipe = "";
		$scope.ModalTolakIncomingPayment_TotalPencetakan = "";
		$scope.ModalTolakIncomingPayment_TanggalPengajuan = "";
		$scope.ModalCetakUlang_AlasanPenolakan = "";
	}
	/*===============================Bulk Action Section, End===============================*/
});
