
angular.module('app')
	.factory('cetakUlangFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

		factory.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			var finalDate
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
            return finalDate;
		}

		factory.getCustomersByCustomerId = function (CustomerId) {
			var url = '/api/fe/Customers/GetData?start=1&limit=1&filterData=' + CustomerId;
			if (debugMode) { console.log('Masuk ke getdata factory function dengan Nomor Customer Id :' + CustomerId); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}

		factory.getCustomerDataByIPParentId = function(IPParentId){
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//SPKId = SPKId.replace(regex, "~"); 			
			//var url = '/api/fe/Customers/GetBySPKId/' + SPKId;
			var url = '/api/fe/Customers/GetByIPParentId?IPParentId=' + IPParentId;
			console.log ("execute GetByIPParentId");
			return $http.get(url);
		}		
		
			  
		factory.GetListIncoming = function (start, limit, TranDate, TranDateTo, SearchText, SearchType) {
			debugger;
			var url = '/api/fe/FinanceIncomingPayments/GetListIncoming/?start=' + start + '&limit=' + limit + '&TranDate=' + TranDate + '|' + TranDateTo + '|' + SearchText + '|' + SearchType;
			console.log("execute GetListIncoming");
			console.log(url);
			return $http.get(url);
		}

		factory.GetListCetakUlang = function(start, limit, TranDate, TranDateTo, Tipe, SearchText, SearchType, flag){
		// var url = '/api/fe/CetakUlang/GetListCetakUlang/start/' + start + '/limit/' + limit + '/Filter/' + TranDate + '|' + TranDateTo + '|' + Tipe+ '|' + SearchText + '|' + SearchType;
		if(flag == 1){
			SearchText = this.changeFormatDate(SearchText)
		}
		var url = '/api/fe/CetakUlang/GetListCetakUlang?start=' + start + '&limit=' + limit + '&Filter=' + TranDate + '|' + TranDateTo + '|' + Tipe + '|' + SearchText + '|' + SearchType;
		console.log ("execute GetListIncoming");
		return $http.get(url);
		}
				
		factory.GetListRincianPembayaran = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetListRincianPembayaran/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetListRincianPembayaran");
		return $http.get(url);
		}

		factory.GetListRincianBiaya = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetListRincianBiaya/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetListRincianBiaya");
		return $http.get(url);
		}

		factory.GetLihatIncomingPaymentWO = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentWO/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetLihatIncomingPaymentWO");
		return $http.get(url);
		}	

		factory.GetLihatIncomingPaymentSO = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentSO/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetLihatIncomingPaymentSO");
		return $http.get(url);
		}	

		factory.GetLihatIncomingPaymentVendor = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentVendor/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetLihatIncomingPaymentVendor");
		return $http.get(url);
		}	
		
		factory.GetLihatIncomingPaymentSPK = function(start, limit, IPParentId){
		var url = '/api/fe/FinanceIncomingPayments/GetLihatIncomingPaymentSPK/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
		console.log ("execute GetLihatIncomingPaymentSPK");
		return $http.get(url);
		}			
		
		factory.submitDataApproval = function (inputData) {
			var url = '/api/fe/FinanceApproval/SubmitData/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}		
		
		factory.updateApprovalData = function(inputData){
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		},	

		factory.getCetakKuitansi=function(IPParentId) {
			var url = '/api/fe/FinanceRptKuitansi/Cetak/?IPParentId=' + IPParentId;
			console.log(url);
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},		

		factory.getCetakKuitansiTTUS=function(IPParentId) {
			var url = '/api/fe/FinanceRptTTUS/Cetak/?IPParentId=' + IPParentId;
			console.log(url);
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},		
 
//--------------------Advanced Payment-------------------//		
	factory.getDataAdvbyId=function(Id)
	{
			var url = '/api/fe/AdvancePayment/GetDataById?Id=' + Id;
			//var url = '/api/fe/dummypo/getbypoid/start/' + Start + '/limit/'+ Limit + '/poid/' + POId;
			console.log("url getDataAdv : ", url);
			var res=$http.get(url);
			return res;		
	}

		factory.getDataPObyId=function(Start, Limit, POId){
			var url = '/api/fe/FinancePO/GetPOADPbyPOId?start=' + Start + '&limit='+ Limit + '&poid=' + POId;
			//var url = '/api/fe/dummypo/getbypoid/start/' + Start + '/limit/'+ Limit + '/poid/' + POId;
			console.log("url getDataPO : ", url);
			var res=$http.get(url);
			return res;
		},
		
		factory.getVendorByName=function(VendorName){
			var url = '/api/fe/FinanceVendor/GetDatabyVendorName?VendorName=' + VendorName;
			//var url = '/api/fe/dummyvendor/getdata/vendorname/' + VendorName;
			console.log("url getDataVendor : ", url);
			var res=$http.get(url);
			return res;
		},

		factory.GetDataAdvancedPaymentById=function(AdvancedId){
			var url = '/api/fe/FinancePO/GetPOADPbyPOId?start=' + Start + '&limit='+ Limit + '&poid=' + POId;
			//var url = '/api/fe/AdvancePayment/GetDataAdvancedPaymentById/AdvancedId/' + AdvancedId;
			console.log("url GetDataAdvancedPaymentById : ", url);
			var res=$http.get(url);
			return res;
		},	

		factory.getCetakReportAdvancePaymentBJD=function(APId, PaymentTypeId) {
			var url = '/api/fe/FinanceRptCetakBJD/Cetak/?PaymentId=' + APId + '&PaymentTypeId='+PaymentTypeId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},
		
		factory.getCetakReportAdvancePaymentBJDCash=function(APId, PaymentTypeId) {
			var url = '/api/fe/FinanceRptCetakBJD/CetakCash/?PaymentId=' + APId + '&PaymentTypeId='+PaymentTypeId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		},

		//----------------------- Payment Instruction -------------------------------------//

		factory.getDataTransactionType= function(){
		var url = '/api/fe/FinanceParam/TipeTransaksiIP';
		var res=$http.get(url);  
		return res;
		}

		factory.getDataForRefund=function(start, limit, FilterData) {
			console.log('isi data ',JSON.stringify(FilterData) );
			var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData));
			//var res=$http.get('/api/fe/PaymentInstructionRefund/SelectData/Start/' + start + '/Limit/' + limit + '/filterData=' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.cetakBPHCash=function(pnPaymentId, userId , pnAPTypeId, outletId ) {
			var url = '/api/fe/FFinanceRptPrintBPHCash/Cetak?pnPaymentId=' + pnPaymentId + '&userId=' +  userId + '&pnAPTypeId=' +  pnAPTypeId + '&outletId=' +  outletId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		}

		factory.cetakBPHBankTransfer=function(pnPaymentId, userId , pnAPTypeId, outletId ) {
			var url = '/api/fe/FFinanceRptPrintBPH/Cetak?pnPaymentId=' + pnPaymentId + '&userId=' +  userId + '&pnAPTypeId=' +  pnAPTypeId + '&outletId=' +  outletId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		}

		factory.getDataForAccruedFreeService=function(start, limit, Id, Code, Type) {
			var FilterData = [{LookupId : Id, Mode : Type, LookUpCode : Code }];
			var url = '/api/fe/PaymentInstructionAccruedFreeService/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData);
			console.log("url getDtaForAccruedFreeService: ", url);
			var res=$http.get(url);
			//var res=$http.get('/api/fe/PaymentInstructionAccruedFreeService/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.getDataForAccruedFrame=function(start, limit, Id, Code, Type) {
			var FilterData = [{LookupId : Id, Mode : Type, LookUpCode : Code }];
			var url = '/api/fe/PaymentInstructionFrame/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData);
			console.log("url getDataForAccruedFrame: ", url);
			var res=$http.get(url);
		// var res=$http.get('/api/fe/PaymentInstructionFrame/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.getDataForAccrued=function(start, limit, Id,  Code, Type) {
			var FilterData = [{LookupId : Id, Mode : Type, LookUpCode : Code }];
			//var res=$http.get('/api/fe/PaymentInstructionAccrued/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(FilterData));
			var res=$http.get('/api/fe/PaymentInstructionAccrued/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.getVendorInfo= function(tipe, name , id) {
			var FilterData = [{PIType : tipe, VendorName : name, VendorId: id}];
			var url = '/api/fe/PaymentInstructionVendor/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			var res=$http.get(url);
			console.log("getVendorInfo url : ", url);
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		};

		factory.getBiayaMaterai=function(amount) {
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var res=$http.get('/api/fe/ParamBeaMateraiElektronik/GetNominal?nominal=' + amount);
			//console.log('res=>',res);
			return res;     
		}

		factory.getSOVendorList= function(tipe, name) {
			var FilterData = [{PIType : tipe, VendorName : name, ShowAll : 1}];
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var url = '/api/fe/PaymentInstructionVendor/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			console.log("url getSOVendorList", url);
			var res=$http.get(url);
			//console.log('res=>',res);
			return res;
		};    

		factory.getIncoiveTaxList=function(start,limit,Id, Type){
			var data = [{ReffId: Id, TaxType : Type}];
			//var res=$http.get('/api/fe/PaymentInstructionTax/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data));
			var res=$http.get('/api/fe/PaymentInstructionTax/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data));
			return res;
		};

		factory.getDataInvoiceList= function(start,limit,Id, Type, DocId ) {
			var Data = [{VendorId : Id, PIType : Type , PIId: DocId}];
			var res=$http.get('/api/fe/PaymentInstructionInvoice/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(Data));
			//var res=$http.get('/api/fe/PaymentInstructionInvoice/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(Data));
			return res;
		};

		//utk main approval
		factory.getDataForMainGrid= function(start,limit, FilterData ) {
		//var url = '/api/fe/PaymentInstructionApproval/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + FilterData;
		var url = '/api/fe/PaymentInstructionApproval/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + FilterData;
		var res=$http.get(url);  
		return res;	
		}; 

		factory.getDataBranch=function(orgId) {
		//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
		var res=$http.get(url);  
		return res;			
		};

		factory.getAdvancePaymentList = function(start, limit , FilterData) {
			var data = [{Type : FilterData}];
			//var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + JSON.stringify(data) );
			var res=$http.get('/api/fe/PaymentInstructionAdvance/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + JSON.stringify(data) );
			//console.log('res=>',res);
			return res;
		};

		factory.getMasterLihat=function(start, limit, id , outlet)
		{
        var inputData = [{PIId: id, OutletId: outlet}];      
      	var url = '/api/fe/PaymentInstruction/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(inputData);
        //var url = '/api/fe/PaymentInstruction/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(inputData);
      	var res=$http.get(url);  
		
        return res;
		};

    factory.getMasterLihatCostCenter=function(start, limit, id)
		{ 
      	var url = '/api/fe/PaymentInstructionCostCenter/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + id;
        //var url = '/api/fe/PaymentInstructionCostCenter/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + id;
      	var res=$http.get(url);  
		
        return res;
		};

    factory.getOtherInfoKomisi=function(start, limit, SoId, KTP) 
    {
        var filter = [{SoId : SoId, KTP : KTP}];
      	var url = '/api/fe/PaymentInstructionCommision/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + JSON.stringify(filter);
        //var url = '/api/fe/PaymentInstructionCommision/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(filter);
      	var res=$http.get(url);  
		
        return res;
    };

    factory.getMasterLihatCharges=function(start, limit, id)
		{ 
      	var url = '/api/fe/PaymentInstructionCharges/SelectData?start=' + start + '&limit=' + limit + '&FilterData=' + id;
        //var url = '/api/fe/PaymentInstructionCharges/SelectData/start/' + start + '/Limit/' + limit + '/FilterData/' + id;
      	var res=$http.get(url);  
		
        return res;
		};

		factory.getGLList = function (Start, Limit, Filter) {
			//var url = '/api/fe/GeneralLedgers/SelectData/start/' + Start + '/Limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/GeneralLedgers/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
			if (debugMode) { console.log('Masuk ke getdata factory function '); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
    factory.getCostCenterList = function (Start, Limit, Filter) {
			//var url = '/api/fe/CostCenters/SelectData/start/' + Start + '/Limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/CostCenters/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
			if (debugMode) { console.log('Masuk ke getdata factory function '); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
    factory.getIncomingInvoiceList = function (Start, Limit, Filter) {
			//var url = '/api/fe/IncomingInvoice/SelectData/Start/' + Start + '/limit/' + Limit + '/filterData/' + Filter;
      var url = '/api/fe/IncomingInvoice/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
			if (debugMode) { console.log('Masuk ke getdata factory function '); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}
    factory.getPajakList = function (Start, Limit, Filter) {
			//var url = '/api/fe/Pajak/SelectData/start/' + Start + '/limit/' + Limit + '/filterData/' + Filter;
      	var url = '/api/fe/Pajak/SelectData?start=' + Start + '&limit=' + Limit + '&filterData=' + Filter;
			if (debugMode) { console.log('Masuk ke getdata factory function '); };
			if (debugMode) { console.log('url :' + url); };
			return $http.get(url);
		}


		return factory;
		
	});