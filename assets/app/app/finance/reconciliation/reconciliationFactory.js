angular.module('app')
	.factory('ReconciliationFactory', function ($http, CurrentUser) {
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

		factory.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			var finalDate
            // console.log("changeFormatDate item", item);
			// tmpAppointmentDate = new Date(tmpAppointmentDate);
			// console.log("changeFormatDate tmpAppointmentDate", tmpAppointmentDate);
            // if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
            //     var yyyy = tmpAppointmentDate.getFullYear().toString();
            //     var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            //     var dd = tmpAppointmentDate.getDate().toString();
            //     finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            // } else {
            //     finalDate = '';
            // }
			// console.log("changeFormatDate finalDate", finalDate);
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
            return finalDate;
		}

		factory.getReconParent = function (start, limit, TranDate) {
			//var url = '/api/fe/Reconciliation/?start='+start+'&limit='+limit+'&filterData='+TranDate;
			var url = '/api/fe/Reconciliation/getReconParent/start/' + start + '/limit/' + limit + '/TranDate/' + TranDate;
			console.log("execute getReconParent");
			return $http.get(url);
		}

		factory.getReconMappingTemp = function (id) {
			var url = '/api/fe/Reconciliation/GetReconMappingTempDetailMIT/start/1/limit/100/filterdata/' + id;
			return $http.get(url);
		}

		factory.getLihatReconParent = function (start, limit, TranDateFrom, TranDateTo, Branch, SearchText, SearchType, flag) {
			if(flag == 1){
				SearchText = this.changeFormatDate(SearchText)
			}
			var url = '/api/fe/Reconciliation/getLihatReconParent/start/' + start + '/limit/' + limit + '/TranDate/' + TranDateFrom + '|' + TranDateTo + '|' + Branch + '|' + SearchText + '|' + SearchType;
			console.log("execute getLihatReconParent");
			return $http.get(url);
		}

		factory.getReconDetail = function (start, limit, IPParentId) {
			var url = '/api/fe/Reconciliation/GetDetailRecon/start/' + start + '/limit/' + limit + '/IPParentId/' + IPParentId;
			console.log("execute getReconDetail");
			return $http.get(url);
		}
		factory.getReconMITDetail = function (start, limit, InstructionId) {
			var url = '/api/fe/Reconciliation/GetDetailReconMIT/start/' + start + '/limit/' + limit + '/InstructionId/' + InstructionId;
			console.log("execute getReconDetailMIT", url);
			return $http.get(url);
		}

		factory.getReconMITRevDetail = function (start, limit, InstructionId) {
			var url = '/api/fe/Reconciliation/GetDetailReconMITRev/start/' + start + '/limit/' + limit + '/InstructionId/' + InstructionId;
			console.log("execute getReconMITRevDetail", url);
			return $http.get(url);
		}

		factory.GetReconMITMapping = function (start, limit, ReconId) {
			var url = '/api/fe/Reconciliation/GetReconMITMapping/start/' + start + '/limit/' + limit + '/ReconId/' + ReconId;
			console.log("execute GetReconMITMapping", url);
			return $http.get(url);
		}
		factory.submitRecon = function (inputData) {
			var url = '/api/fe/Reconciliation/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}
		factory.submitReconMapping = function (inputData) {
			var url = '/api/fe/Reconciliation/submitReconMapping/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitReconMapping') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.submitDataApprovalReconciliation = function (inputData) {
			console.log("submitDataApprovalReconciliation");
			var url = '/api/fe/FinanceApproval/SubmitData/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.updateApprovalData = function (inputData) {
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res = $http.put(url, param);
			return res;
		}

		factory.getDataBranchComboBox = function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		}

		factory.getExcelReportRekon = function (RekonId) {
			var url = '/api/fe/FinanceRptRekon/Excel/?RekonId=' + RekonId;
			console.log('get --> ', url);
			var res = $http.get(url);
			return res;
		}

		factory.getCetakReportRekon = function (RekonId) {
			var url = '/api/fe/FinanceRptRekon/Cetak/?RekonId=' + RekonId;
			console.log('get --> ', url);
			var res = $http.get(url, { responseType: 'arraybuffer' });
			return res;
		}

		factory.getDataBranch = function () {
			//var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData=0';
			var res = $http.get(url);
			return res;
		};

		return factory;

	});