angular.module('app')
  .factory('DaftarTransaksiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
			  
		var da_json=[
				{Id: "1",  Tipe_Transaksi: "INT",DebetKredit: "Debet",Nominal: 1000,Assignment: "-",Keterangan: "Pooling",Referensi_Dokumen: "-",Nominal_Pada_Ref: 1000,Status: "Clear"},
				{Id: "2",  Tipe_Transaksi: "CHG",DebetKredit: "Kredit",Nominal: 1000,Assignment: "Incoming Payment",Keterangan: "-",Referensi_Dokumen:  "-",Nominal_Pada_Ref: 1000,Status: "Clear"},
				{Id: "3",  Tipe_Transaksi: "TRF",DebetKredit: "Kredit",Nominal: 1000,Assignment: "-",Keterangan: "Pooling",Referensi_Dokumen:  "-",Nominal_Pada_Ref: 1000,Status: "Clear"},
			  ];	  
		res=da_json;
		
        return res;
      },
      create: function(DaftarTransaksi) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: DaftarTransaksi.Name,
                                            Description: DaftarTransaksi.Description}]);
      },
      update: function(DaftarTransaksi){
        return $http.put('/api/fw/Role', [{
                                            Id: DaftarTransaksi.Id,
                                            //pid: negotiation.pid,
                                            Name: DaftarTransaksi.Name,
                                            Description: DaftarTransaksi.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });