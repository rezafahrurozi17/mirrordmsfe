angular.module('app')
	.controller('ReconciliationController', function ($scope, $http, $filter, CurrentUser, ReconciliationFactory, DaftarTransaksiFactory, $timeout, uiGridConstants, bsNotify) {
		//20170517, nuse, begin 
		// $scope.ShowReconciliationMain=true;
		$scope.ShowLihatReconciliation = true;
		$scope.ShowLihatHeader = true;
		$scope.ShowReconciliationMainTable = true;
		//20170517, nuse, end
		$scope.options_DaftarReconciliationSearch = [{ name: "Nomor Reconciliation", value: "Nomor Reconciliation" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" }, { name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" }];
		$scope.options_TambahReconciliationSearch = [{ name: "Nomor Instruksi Bank", value: "Nomor Instruksi Penerimaan" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" }, { name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" }];
		$scope.optionsLihatReconciliation_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
		$scope.optionsTambahReconciliation_TipeMapping_Search = [{ name: "TRF", value: "TRF" }, { name: "MIT Reversal", value: "MIT Reversal" }];
		$scope.parent = $scope;

		//New Pagination Tambah
		$scope.TambahReconDetail_UIGridData = [];
		$scope.TambahReconMITReversal_UIGridData = [];
		$scope.TambahReconMIT_UIGridData = [];

		//New Pagination Lihat
		$scope.LihatReconDetail_UIGridData = [];
		$scope.LihatReconMITReversal_UIGridData = [];
		$scope.LihatReconMIT_UIGridData = [];

		$scope.user = CurrentUser.user();
		console.log(CurrentUser);
		console.log($scope.user);

		var gridData = [];
		var currentOutletId = 0;
		var uiGridPageSize = 10;
		var uiGridPageNumberReconParent = 1;
		var uiGridPageSizeReconParent = 10;
		var uiGridPageNumberReconData = 1;
		var uiGridPageSizeReconData = 10;
		var currentIncomingInstructionId = 0;
		var currentReconId = 0;
		var currentBeginBalance = 0;
		var currentEndBalance = 0;
		var currentNoRek = 0;
		var currentReconciliationId = 0;
		var currentTglBank = "";
		var Cetak = "N";
		var currentReconMappingMITId = 0;
		//Tambah
		var LimitPagination = 0;
		var StartPagination = 0;
		var LimitPaginationRev = 0;
		var StartPaginationRev = 0;
		var LimitPaginationMIT = 0;
		var StartPaginationMIT = 0;

		//Lihat
		var LihatLimitPagination = 0;
		var LihatStartPagination = 0;
		var LihatLimitPaginationRev = 0;
		var LihatStartPaginationRev = 0;
		var LihatLimitPaginationMIT = 0;
		var LihatStartPaginationMIT = 0;
		$scope.LihatReconciliation_PengajuanReversalDisabled = false;
		$scope.SubData = {};

		$scope.tmpIncomingInstructionId = 0;


		// $scope.givePattern = function (item, event) {
		// 	console.log("item", item);
		// 	if (event.which > 37 && event.which < 40) {
		// 		event.preventDefault();
		// 		return false;
		// 	} else {
		// 		$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		// 		return;
		// 	}
		// };

		$scope.givePattern = function (item, event) {
			console.log("item", item);
			console.log("event", event.which);
			console.log("event", event);
			if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105)){
				event.preventDefault();
				console.log("disini", event.which);
				console.log("$scope.TambahReconciliation_NominalTRF", $scope.TambahReconciliation_NominalTRF);
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.replace(/[^0-9.]+/g,'');
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.replace(event.key,"");
				return false;
			} else {
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF;
				return;
			}
		};

		$scope.TambahReconciliation_NominalTRF_Blur = function () {
			console.log("test",$scope.TambahReconciliation_NominalTRF);
			console.log("test",parseFloat($scope.TambahReconciliation_NominalTRF).toFixed(2));
			var SisaFloat = "";
			if ($scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-3,1) == "."){
				SisaFloat = $scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-3);
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.substr(0,$scope.TambahReconciliation_NominalTRF.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahReconciliation_NominalTRF);
			}
			else if ($scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-2,1) == "."){
				SisaFloat = $scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-2);
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.substr(0,$scope.TambahReconciliation_NominalTRF.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahReconciliation_NominalTRF);
			}
			var val = $scope.TambahReconciliation_NominalTRF.split(".").join("");
			console.log("cari penyebab",val);
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");
	
			var str_array = val.split('.');
	
			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}
	
				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}
	
			if (!NominalValid) {
				$scope.TambahReconciliation_NominalTRF_Blur(val);
			}
			else {
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahReconciliation_NominalTRF = val+".00";
					$scope.TambahReconciliation_NominalTRFforParseFloat = val.split(".").join("")+".00";
				}
				else{
					$scope.TambahReconciliation_NominalTRF = val+SisaFloat;
					$scope.TambahReconciliation_NominalTRFforParseFloat = val.split(".").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahReconciliation_NominalTRFforParseFloat);
				}
			}
		}

		var TambahReconciliation_DaftarTransaksi_AllClear = false;
		$scope.LihatReconciliation_TanggalReconciliationFrom = new Date();
		$scope.LihatReconciliation_TanggalReconciliationTo = new Date();

		$scope.ByteEnable = JSON.parse($scope.tab.item).ByteEnable;
		console.log($scope.ByteEnable);
		$scope.allowApprove = checkRbyte($scope.ByteEnable, 4);
		if ($scope.allowApprove == true)
			$scope.optionsLihatReconciliation_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
		else
			$scope.optionsLihatReconciliation_DaftarBulkAction_Search = [];

		function checkRbyte(rb, b) {
			var p = rb & Math.pow(2, b);
			return ((p == Math.pow(2, b)));
		};

		function addSeparatorsNF(nStr, inD, outD, sep) {
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1) {
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr)) {
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		$scope.TanggalReconciliationFromOK = true;
		$scope.TanggalReconciliationFromChanged = function () {

			console.log($scope.TanggalReconciliationFromOK);
			var dt = $scope.LihatReconciliation_TanggalReconciliationFrom;
			var now = new Date();
			if (dt > now) {
				$scope.errTanggalReconciliationFrom = "Tanggal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalReconciliationFromOK = false;
			}
			else {
				$scope.TanggalReconciliationFromOK = true;
			}
			console.log('asdasd ' + $scope.TanggalReconciliationFromOK);
		}

		$scope.TanggalReconciliationToOK = true;
		$scope.TanggalReconciliationToChanged = function () {

			console.log($scope.TanggalReconciliationToOK);
			var dt = $scope.LihatReconciliation_TanggalReconciliationTo;
			var now = new Date();
			if (dt > now) {
				$scope.errTanggalReconciliationTo = "Tanggal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalReconciliationToOK = false;
			}
			else {
				$scope.TanggalReconciliationToOK = true;
			}
			console.log('asdasd ' + $scope.TanggalReconciliationToOK);
		}

		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.getDataBranchCB();
			// ReconciliationFactory.getDataBranch($scope.user.OutletId)
			// .then(
			// 	function(res)
			// 	{
			// 		console.log(res.data.Result[0].OutletName);
			// 		$scope.LihatReconciliation_NamaBranch_SearchDropdown = $scope.user.OutletId;
			// 	}
			// );
		});

		$scope.TanggalInstruksiBankChanged = function () {

			console.log($scope.TanggalInstruksiBankOK);
			var dt = $scope.TambahReconciliation_TanggalInstruksiPembayaran;
			var now = new Date();
			if (dt > now) {
				$scope.errTanggalInstruksiBank = "Tanggal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalInstruksiBankOK = false;
			}
			else {
				$scope.TanggalInstruksiBankOK = true;
			}
			console.log('asdasd ' + $scope.TanggalInstruksiBankOK);
		}


		$scope.getDataBranchCB = function () {
			//ReconciliationFactory.getDataBranchComboBox()
			ReconciliationFactory.getDataBranch()
				.then
				(
				function (res) {
					$scope.loading = false;
					$scope.optionsLihatReconciliation_NamaBranch_Search = res.data.Result;
					$scope.LihatReconciliation_NamaBranch_SearchDropdown = $scope.user.OutletId;
					console.log("OUTLETID ==>", $scope.LihatReconciliation_NamaBranch_SearchDropdown);
					
					var Branch = $.grep($scope.optionsLihatReconciliation_NamaBranch_Search, function (element, index){
						return element.BranchId == $scope.LihatReconciliation_NamaBranch_SearchDropdown;
						
					});
					console.log("data nih ==>",  Branch);	

					$scope.LihatReconciliation = Branch[0].BranchName;
					 
				},
				function (err) {
					console.log("err=>", err);
				}
				);
		};

		$scope.ToLihatReconciliation = function () {

			$scope.ShowReconciliationMain = false;
			$scope.ShowLihatReconciliation = true;

		};

		$scope.LihatReconciliation_Batal_Click = function () {
			$scope.ShowLihatReconciliation = true;
			$scope.ShowLihatHeader = true;
			$scope.ShowReconciliationMainTable = false;
			$scope.ShowReconciliationMainDetail = false;
			$scope.LihatReconciliation_TotalTRFDebet = 0;
			$scope.LihatReconciliation_TotalTRFKredit = 0;
			$scope.LihatReconciliation_TotalMITDebet = 0;
			$scope.LihatReconciliation_TotalMITKredit = 0;
			$scope.LihatReconciliation_SelisihMITDebet = 0;
			$scope.LihatReconciliation_SelisihMITKredit = 0;
			$scope.LihatReconciliation_SaldoAkhirSebelumnya = 0;
			$scope.LihatReconMITDetail_UIGrid.data = [];
			$scope.LihatReconMITReversal_UIGrid.data = [];
			$scope.LihatReconDetail_UIGrid.data = [];
			$scope.LihatReconMIT_UIGrid.data = [];

			$scope.LihatReconciliation_Kembali();
		};

		$scope.LihatReconciliation_Cari = function () {
			//$scope.LihatReconciliation_DaftarReconciliationToRepeat=ReconciliationFactory.getData();
			console.log("LihatReconciliation_Cari");
			if (($scope.LihatReconciliation_TanggalReconciliationFrom != null) && ($scope.LihatReconciliation_TanggalReconciliationFrom != null)) {
				$scope.ShowReconciliationMainTable = true;
				$scope.ShowLihatHeader = true;
				$scope.ShowReconciliationMainDetail = false;
				$scope.getLihatDataParentByDate(uiGridPageNumberReconParent, uiGridPageSizeReconParent);
			}
			else {

				bsNotify.show({
					title: "Warning",
					content: "Tanggalawal/akhir belum diisi",
					type: 'warning'
				});
			}
		};
		
		// Ketika klik kemballi tampilan awal tidak get ulang 
		$scope.LihatReconciliation_Kembali = function () {
			//$scope.LihatReconciliation_DaftarReconciliationToRepeat=ReconciliationFactory.getData();
			console.log("LihatReconciliation_Kembali");
			if (($scope.LihatReconciliation_TanggalReconciliationFrom != null) && ($scope.LihatReconciliation_TanggalReconciliationFrom != null)) {
				$scope.ShowReconciliationMainTable = true;
				$scope.ShowLihatHeader = true;
				$scope.ShowReconciliationMainDetail = false;
				$scope.getLihatDataParentByDate(uiGridPageNumberReconParent, uiGridPageSizeReconParent);
			}
			else {

				bsNotify.show({
					title: "Warning",
					content: "Tanggalawal/akhir belum diisi",
					type: 'warning'
				});
			}
		};

		$scope.ToTambahReconciliation = function () {
			$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
			$scope.TambahReconciliation_Simpan_EnableDisable = false;

			$scope.TambahReconciliation_DaftarReconciliation = "";
			$scope.TambahReconciliation_TanggalInstruksiPembayaran = undefined;
			//20170517, nuse, begin
			$scope.ShowLihatReconciliation = false;
			//20170517, nuse, end
			$scope.ShowReconciliationMain = false;
			$scope.ShowTambahReconciliation = true;
			$scope.ShowReconciliationMainDetail = false;

		};

		$scope.clearData = function (){
				//====== Clear Data ========================================================
				
				$scope.TambahReconciliation_TotalMITDebet = "";
				$scope.TambahReconciliation_TotalMITKredit = "";
				$scope.TambahReconciliation_DaftarReconciliationToRepeat = "";

				$scope.TambahReconciliation_NomorRekeningBank = "";
				$scope.TambahReconciliation_SaldoAwal = "";
				$scope.TambahReconciliation_TanggalBankStatement = "";
				$scope.TambahReconciliation_SaldoAkhir = "";
				$scope.TambahReconciliation_SaldoAkhirSebelumnya = "";
				$scope.TambahReconciliation_DaftarTransaksiToRepeat = "";

				$scope.LihatReconciliation_NomorRekeningBank = "";
				$scope.LihatReconciliation_SaldoAwal = "";
				$scope.LihatReconciliation_TanggalBankStatement = "";
				$scope.LihatReconciliation_SaldoAkhir = "";
				$scope.LihatReconciliation_SaldoAkhirSebelumnya = "";
				$scope.LihatReconciliation_DaftarTransaksiToRepeat = "";

				$scope.LihatReconciliation_TanggalReconciliation = "";
				$scope.LihatReconciliation_NomorReconciliation = "";
				$scope.LihatReconciliation_NamaBranch = "";

				$scope.TambahReconParent_UIGrid.data = [];
				$scope.TambahReconDetail_UIGrid.data = [];	
				$scope.LihatReconParent_UIGrid.data = [];
				$scope.LihatReconDetail_UIGrid.data = [];
				$scope.TambahReconMITReversal_UIGrid.data = [];

				$scope.TambahReconMIT_UIGrid.data = [];
				$scope.TambahReconMITDetail_UIGrid.data = [];

				$scope.TambahReconciliation_TotalTRFDebet = "";
				$scope.TambahReconciliation_TotalTRFKredit = "";
				$scope.TambahReconciliation_NoTRF = "";
				$scope.TambahReconciliation_NominalTRF = "";
				$scope.TambahReconciliation_SelisihMITDebet = "";
				$scope.TambahReconciliation_SelisihMITKredit = "";
				$scope.TambahReconciliation_TotalMITDebet = "";
				$scope.TambahReconciliation_TotalMITKredit = "";


		}
		$scope.TambahReconciliation_Cari = function () {
			//$scope.TambahReconciliation_DaftarReconciliationToRepeat=ReconciliationFactory.getData();
			console.log("TambahReconciliation_Cari");
			if ($scope.TambahReconciliation_TanggalInstruksiPembayaran != null) {
				$scope.getDataParentByDate(uiGridPageNumberReconData, uiGridPageSizeReconData);
				$scope.TambahReconciliation_TipeMapping_SearchDropdown = "TRF";
				$scope.LihatReconciliation_DaftarReconciliationToRepeat = "";
				$scope.clearData();
			
			}
			else {
				bsNotify.show({
					title: "Warning",
					content: "Tanggal instruksi pembayaran belum diisi",
					type: 'warning'
				});
			}
		};

		$scope.GetReconMappingTemp = function () {
			ReconciliationFactory.getReconMappingTemp($scope.tmpIncomingInstructionId)
				.then(
					function (res) {
						// var jsonMapping = removeDuplicates(res.data.Result).sort(function (a, b) {
						// 	return (a['IncomingInstructionDetailId'] > b['IncomingInstructionDetailId']) ? 1 : ((a['IncomingInstructionDetailId'] < b['IncomingInstructionDetailId']) ? -1 : 0);
						// });;
						angular.forEach(res.data.Result, function (value, key) {
							value.Nominal = value.Nominal.toString().replace(',0000', '');
						});
						$scope.TambahReconMITDetail_UIGrid.data = res.data.Result;
						$scope.TambahReconMITDetail_UIGrid.totalItems = res.data.Total;
						$scope.TambahReconMITDetail_UIGrid.paginationPageSize = 10;
						$scope.TambahReconMITDetail_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.loading = false;
						// $scope.CheckAllClear();

					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.getDataParentByDate = function (Start, Limit) {
			ReconciliationFactory.getReconParent(Start, Limit, $filter('date')(new Date($scope.TambahReconciliation_TanggalInstruksiPembayaran), 'yyyy-MM-dd'))
				.then(
					function (res) {
						$scope.TambahReconParent_UIGrid.data = res.data.Result;
						$scope.TambahReconParent_UIGrid.totalItems = res.data.Total;
						$scope.TambahReconParent_UIGrid.paginationPageSize = Limit;
						$scope.TambahReconParent_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.loading = false;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};

		$scope.getLihatDataParentByDate = function (Start, Limit) {
			console.log('masuk');
			
			var newString  =  "";
			if($scope.LihatReconciliation_DaftarReconciliation_Text != '' && $scope.LihatReconciliation_DaftarReconciliation_Text != undefined){
				newString = $scope.LihatReconciliation_DaftarReconciliation_Text.toString().replace(/\//g, '-').toString().replace(/\./g, '-')
			}else{
				newString = $scope.LihatReconciliation_DaftarReconciliation_Text;
			}
			console.log("replace =>>", newString);

			var tmpFlag = 0;
			if( $scope.LihatReconciliation_DaftarReconciliationSearch == 'Tanggal Bank Statement'){
				tmpFlag=1;
			}else{
				tmpFlag=0;
			}
			ReconciliationFactory.getLihatReconParent(Start, Limit, $filter('date')(new Date($scope.LihatReconciliation_TanggalReconciliationFrom), 'yyyy-MM-dd'),
				$filter('date')(new Date($scope.LihatReconciliation_TanggalReconciliationTo), 'yyyy-MM-dd'), $scope.LihatReconciliation_NamaBranch_SearchDropdown, 
				newString, $scope.LihatReconciliation_DaftarReconciliationSearch,tmpFlag)
				.then(
					function (res) {
						console.log(res.data.Result);
						$scope.LihatReconParent_UIGrid.data = res.data.Result;
						// for(var i in res.data.Result){
						// 	res.data.Result[i].BankStatementDate = $scope.changeFormatDate(res.data.Result[i].BankStatementDate);
						// }
						$scope.LihatReconParent_UIGrid.totalItems = res.data.Total;
						$scope.LihatReconParent_UIGrid.paginationPageSize = Limit;
						$scope.LihatReconParent_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.loading = false;
					},
					function (err) {
						console.log("err=>", err);
					}
				)
		};
		$scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
                var yyyy = tmpAppointmentDate.getFullYear().toString();
                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = tmpAppointmentDate.getDate().toString();
                finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
            } else {
                finalDate = '';
            }
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };
 
		$scope.TambahReconDetail_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			onRegisterApi: function (gridApi) {
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);

					//New FE pagination

					$scope.TambahReconDetail_UIGridGetPagination(pageNumber,pageSize);

					//$scope.getDataReconDetail(pageNumber,pageSize);

					// ReconciliationFactory.getReconDetail(pageNumber, pageSize, $scope.currentIncomingInstructionId)
					// 	.then(
					// 		function (res) {
					// 			$scope.TambahReconDetail_UIGrid.data = res.data.Result;
					// 			$scope.TambahReconDetail_UIGrid.totalItems = res.data.Total;
					// 			$scope.TambahReconDetail_UIGrid.paginationPageSize = pageSize;
					// 			$scope.TambahReconDetail_UIGrid.paginationPageSizes = [10, 25, 50];
					// 			TambahReconciliation_DaftarTransaksi_AllClear = res.data.Result[0].OverallStatus;

					// 			$scope.CheckAllClear();
					// 			$scope.loading = false;
					// 		},
					// 		function (err) {
					// 			console.log("err=>", err);
					// 		}
					// 	); //OLD Pagination
				});
			},
			columnDefs: [
				{ name: 'No TRF', displayName: 'No TRF', field: 'IncomingInstructionDetailId' },
				{ name: 'Tanggal TRF', displayName: 'Tanggal TRF', field: 'IncomingDate', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Tipe Transaksi', field: 'TransactionType' },
				{ name: 'Debit/Kredit', field: 'DebitCredit' },
				{ name: 'Nominal', field: 'Nominal', aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>', cellFilter: 'number: 2' },
				{ name: 'Assignment', field: 'Assignment' },
				{ name: 'Keterangan', field: 'Keterangan' },
				//{ name: 'Nominal pada MIT', displayName:"Nominal pada MIT", field: 'NominalMIT', aggregationType:uiGridConstants.aggregationTypes.sum, type:'number', cellFilter: 'currency:"":0' },
				{ name: 'Status', field: 'Status' },
				{ name: 'IPDetailId', field: 'IPDetailId', visible: false },
			]
		};

		//NEW Pagination Lihat
		$scope.LihatReconDetail_UIGridGetPagination = function(LihatstartingPage,LihatlimitPage){
			console.log("LihatstartingPage",LihatstartingPage);
			console.log("LihatstartingPage",LihatlimitPage);
			console.log("LihatReconDetail_UIGridPagination",$scope.LihatReconDetail_UIGridPagination);
			$scope.LihatReconDetail_UIGridData = [];
			if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage < $scope.LihatReconDetail_UIGridPagination.length) 
				{LihatStartPagination = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPagination = LihatstartingPage*LihatlimitPage;}
			else if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconDetail_UIGridPagination.length)
				{LihatStartPagination = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPagination = $scope.LihatReconDetail_UIGridPagination.length;}
			else if (LihatstartingPage == 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconDetail_UIGridPagination.length)
				{LihatStartPagination = LihatstartingPage;
					LihatLimitPagination = $scope.LihatReconDetail_UIGridPagination.length;}
			else 
				{LihatStartPagination = LihatstartingPage;
					LihatLimitPagination = LihatstartingPage*LihatlimitPage;}

			for (var i = LihatStartPagination-1; i <LihatLimitPagination;i++ ){
				
				$scope.LihatReconDetail_UIGridData.push($scope.LihatReconDetail_UIGridPagination[i]);
				console.log("$scope.LihatReconDetail_UIGridData",$scope.LihatReconDetail_UIGridPagination[i]);
			}

			$scope.LihatReconDetail_UIGrid.data =  $scope.LihatReconDetail_UIGridData;
			$scope.LihatReconDetail_UIGrid.totalItems = $scope.LihatReconDetail_UIGridPagination.length;
		}

		$scope.LihatReconMITReversal_UIGridGetPagination = function(LihatstartingPage,LihatlimitPage){
			console.log("LihatstartingPage",LihatstartingPage);
			console.log("LihatstartingPage",LihatlimitPage);
			console.log("LihatReconMITReversal_UIGridPagination",$scope.LihatReconMITReversal_UIGridPagination);
			$scope.LihatReconMITReversal_UIGridData = [];
			if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage < $scope.LihatReconMITReversal_UIGridPagination.length) 
				{LihatStartPaginationRev = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPaginationRev = LihatstartingPage*LihatlimitPage;}
			else if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconMITReversal_UIGridPagination.length)
				{LihatStartPaginationRev = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPaginationRev = $scope.LihatReconMITReversal_UIGridPagination.length;}
			else if (LihatstartingPage == 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconMITReversal_UIGridPagination.length)
				{LihatStartPaginationRev = LihatstartingPage;
					LihatLimitPaginationRev = $scope.LihatReconMITReversal_UIGridPagination.length;}
			else 
				{LihatStartPaginationRev = LihatstartingPage;
					LihatLimitPaginationRev = LihatstartingPage*LihatlimitPage;}

			for (var i = LihatStartPaginationRev-1; i <LihatLimitPaginationRev;i++ ){
				
				$scope.LihatReconMITReversal_UIGridData.push($scope.LihatReconMITReversal_UIGridPagination[i]);
				console.log("$scope.LihatReconMITReversal_UIGridData",$scope.LihatReconMITReversal_UIGridPagination[i]);
			}

			$scope.LihatReconMITReversal_UIGrid.data =  $scope.LihatReconMITReversal_UIGridData;
			$scope.LihatReconMITReversal_UIGrid.totalItems = $scope.LihatReconMITReversal_UIGridPagination.length;
		}

		$scope.LihatReconMIT_UIGridGetPagination = function(LihatstartingPage,LihatlimitPage){
			console.log("LihatstartingPage",LihatstartingPage);
			console.log("LihatstartingPage",LihatlimitPage);
			console.log("LihatReconMIT_UIGridPagination",$scope.LihatReconMIT_UIGridPagination);
			$scope.LihatReconMIT_UIGridData = [];
			if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage < $scope.LihatReconMIT_UIGridPagination.length) 
				{LihatStartPaginationMIT = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPaginationMIT = LihatstartingPage*LihatlimitPage;}
			else if (LihatstartingPage > 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconMIT_UIGridPagination.length)
				{LihatStartPaginationMIT = (LihatstartingPage-1)*LihatlimitPage+1;
					LihatLimitPaginationMIT = $scope.LihatReconMIT_UIGridPagination.length;}
			else if (LihatstartingPage == 1 && LihatstartingPage*LihatlimitPage > $scope.LihatReconMIT_UIGridPagination.length)
				{LihatStartPaginationMIT = LihatstartingPage;
					LihatLimitPaginationMIT = $scope.LihatReconMIT_UIGridPagination.length;}
			else 
				{LihatStartPaginationMIT = LihatstartingPage;
					LihatLimitPaginationMIT = LihatstartingPage*LihatlimitPage;}

			for (var i = LihatStartPaginationMIT-1; i <LihatLimitPaginationMIT;i++ ){
				
				$scope.LihatReconMIT_UIGridData.push($scope.LihatReconMIT_UIGridPagination[i]);
				console.log("$scope.LihatReconMIT_UIGridData",$scope.LihatReconMIT_UIGridPagination[i]);
			}

			$scope.LihatReconMIT_UIGrid.data =  $scope.LihatReconMIT_UIGridData;
			$scope.LihatReconMIT_UIGrid.totalItems = $scope.LihatReconMIT_UIGridPagination.length;
		}

		//NEW Pagination
		$scope.TambahReconDetail_UIGridGetPagination = function(startingPage,limitPage){
			console.log("startingPage",startingPage);
			console.log("startingPage",limitPage);
			console.log("TambahReconDetail_UIGridPagination",$scope.TambahReconDetail_UIGridPagination);
			$scope.TambahReconDetail_UIGridData = [];
			if (startingPage > 1 && startingPage*limitPage < $scope.TambahReconDetail_UIGridPagination.length) 
				{StartPagination = (startingPage-1)*limitPage+1;
					LimitPagination = startingPage*limitPage;}
			else if (startingPage > 1 && startingPage*limitPage > $scope.TambahReconDetail_UIGridPagination.length)
				{StartPagination = (startingPage-1)*limitPage+1;
					LimitPagination = $scope.TambahReconDetail_UIGridPagination.length;}
			else if (startingPage == 1 && startingPage*limitPage > $scope.TambahReconDetail_UIGridPagination.length)
				{StartPagination = startingPage;
					LimitPagination = $scope.TambahReconDetail_UIGridPagination.length;}
			else if (startingPage > 1 && startingPage*limitPage >= $scope.TambahReconDetail_UIGridPagination.length) 
				{StartPagination = (startingPage-1)*limitPage+1;
					LimitPagination = startingPage*limitPage;}
			else 
				{StartPagination = startingPage;
					LimitPagination = startingPage*limitPage;}

			for (var i = StartPagination-1; i <LimitPagination;i++ ){
				
				$scope.TambahReconDetail_UIGridData.push($scope.TambahReconDetail_UIGridPagination[i]);
				console.log("$scope.TambahReconDetail_UIGridData",$scope.TambahReconDetail_UIGridPagination[i]);
			}

			$scope.TambahReconDetail_UIGrid.data =  $scope.TambahReconDetail_UIGridData;
			$scope.TambahReconDetail_UIGrid.totalItems = $scope.TambahReconDetail_UIGridPagination.length;
		}

		//Rev
		$scope.TambahReconMITReversal_UIGridGetPagination = function(startingPageRev,limitPageRev){
			console.log("startingPageRev",startingPageRev);
			console.log("startingPageRev",limitPageRev);
			console.log("TambahReconMITReversal_UIGridPagination",$scope.TambahReconMITReversal_UIGridPagination);
			$scope.TambahReconMITReversal_UIGridData = [];
			if (startingPageRev > 1 && startingPageRev*limitPageRev < $scope.TambahReconMITReversal_UIGridPagination.length) 
				{StartPaginationRev = (startingPageRev-1)*limitPageRev+1;
					LimitPaginationRev = startingPageRev*limitPageRev;}
			else if (startingPageRev > 1 && startingPageRev*limitPageRev > $scope.TambahReconMITReversal_UIGridPagination.length)
				{StartPaginationRev = (startingPageRev-1)*limitPageRev+1;
					LimitPaginationRev = $scope.TambahReconMITReversal_UIGridPagination.length;}
			else if (startingPageRev == 1 && startingPageRev*limitPageRev > $scope.TambahReconMITReversal_UIGridPagination.length)
				{StartPaginationRev = startingPageRev;
					LimitPaginationRev = $scope.TambahReconMITReversal_UIGridPagination.length;}
			else 
				{StartPaginationRev = startingPageRev;
					LimitPaginationRev = startingPageRev*limitPageRev;}

			for (var i = StartPaginationRev-1; i <LimitPaginationRev;i++ ){
				
				$scope.TambahReconMITReversal_UIGridData.push($scope.TambahReconMITReversal_UIGridPagination[i]);
				console.log("$scope.TambahReconMITReversal_UIGridData",$scope.TambahReconMITReversal_UIGridPagination[i]);
			}

			$scope.TambahReconMITReversal_UIGrid.data =  $scope.TambahReconMITReversal_UIGridData;
			$scope.TambahReconMITReversal_UIGrid.totalItems = $scope.TambahReconMITReversal_UIGridPagination.length;
		}

		//MIT
		$scope.TambahReconMIT_UIGridGetPagination = function(startingPageMIT,limitPageMIT){
			console.log("startingPageMIT",startingPageMIT);
			console.log("startingPageMIT",limitPageMIT);
			console.log("TambahReconMIT_UIGridPagination",$scope.TambahReconMIT_UIGridPagination);
			$scope.TambahReconMIT_UIGridData = [];
			if (startingPageMIT > 1 && startingPageMIT*limitPageMIT < $scope.TambahReconMIT_UIGridPagination.length) 
				{StartPaginationMIT = (startingPageMIT-1)*limitPageMIT+1;
					LimitPaginationMIT = startingPageMIT*limitPageMIT;}
			else if (startingPageMIT > 1 && startingPageMIT*limitPageMIT > $scope.TambahReconMIT_UIGridPagination.length)
				{StartPaginationMIT = (startingPageMIT-1)*limitPageMIT+1;
					LimitPaginationMIT = $scope.TambahReconMIT_UIGridPagination.length;}
			else if (startingPageMIT == 1 && startingPageMIT*limitPageMIT > $scope.TambahReconMIT_UIGridPagination.length)
				{StartPaginationMIT = startingPageMIT;
					LimitPaginationMIT = $scope.TambahReconMIT_UIGridPagination.length;}
			else 
				{StartPaginationMIT = startingPageMIT;
					LimitPaginationMIT = startingPageMIT*limitPageMIT;}

			for (var i = StartPaginationMIT-1; i <LimitPaginationMIT;i++ ){
				
				$scope.TambahReconMIT_UIGridData.push($scope.TambahReconMIT_UIGridPagination[i]);
				console.log("$scope.TambahReconMIT_UIGridData",$scope.TambahReconMIT_UIGridPagination[i]);
			}

			$scope.TambahReconMIT_UIGrid.data =  $scope.TambahReconMIT_UIGridData;
			$scope.TambahReconMIT_UIGrid.totalItems = $scope.TambahReconMIT_UIGridPagination.length;
		}

		$scope.TambahReconMIT_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
			onRegisterApi: function (gridApi) {
				$scope.TambahReconMIT_UIGrid_gridApi = gridApi;

				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);
					//$scope.getDataReconDetail(pageNumber,pageSize);

					//NEW Pagination
					$scope.TambahReconMIT_UIGridGetPagination(pageNumber,pageSize);

					// ReconciliationFactory.getReconMITDetail(pageNumber, pageSize, $scope.currentIncomingInstructionId)
					// 	.then(
					// 		function (res) {
					// 			$scope.TambahReconMIT_UIGrid.data = res.data.Result;
					// 			$scope.TambahReconMIT_UIGrid.totalItems = res.data.Total;
					// 			$scope.TambahReconMIT_UIGrid.paginationPageSize = pageSize;
					// 			$scope.TambahReconMIT_UIGrid.paginationPageSizes = [10, 25, 50];
					// 			TambahReconciliation_DaftarTransaksi_AllClear = res.data.Result[0].OverallStatus;

					// 			$scope.CheckAllClear();
					// 			$scope.loading = false;
					// 		},
					// 		function (err) {
					// 			console.log("err=>", err);
					// 		}
					// 	);
				});

				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("masuk");
					if (row.isSelected == false) {
						$scope.currentReconMappingMITId = undefined;
						$scope.currentDocId = undefined;
						$scope.currentDocNo = undefined;
						$scope.currentReconMappingMITSelected = undefined;
						$scope.currentDebitCredit = undefined;
						$scope.currentStatus = undefined;
						console.log("false");
					}
					else {
						console.log("changed", row);
						$scope.currentReconMappingMITId = row.entity.ReconMappingMITId;
						$scope.currentDocId = row.entity.DocId;
						$scope.currentDocNo = row.entity.DocNo;
						$scope.currentDebitCredit = row.entity.DebitCredit;
						$scope.currentStatus = row.entity.Status;;
						$scope.currentReconMappingMITSelected = row.entity;
						$scope.currentPaymentTotal = row.entity.PaymentTotal;
						// $scope.GetReconMITMapping(1, uiGridPageSize, row.entity.ReconMappingMITId);
					}
				});
			},
			//expandableRowTemplate: 'app/finance/reconciliation/prePickingET.html',
			//expandableRowHeight: 150,
			//expandableRowScope: {
			//  subGridVariable: 'subGridScopeVariable'
			//},
			columnDefs: [
				{ name: 'Tanggal Dokumen', field: 'TranDate', headerCellClass: $scope.highlightFilteredHeader, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nomor Dokumen', field: 'DocNo', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Tipe Dokumen', field: 'DocDesc', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Debit/Kredit', field: 'DebitCredit', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Nominal', field: 'PaymentTotal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Total Mapping', field: 'TotalNominal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'IPParentId', field: 'DocId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Status', field: 'Status' }
			]
		};

		$scope.TambahReconMITReversal_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			onRegisterApi: function (gridApi) {
				$scope.TambahReconMITReversal_UIGrid_gridApi = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber REV ==>", pageNumber);
					console.log("pageSize ==>", pageSize);
				
					$scope.TambahReconMITReversal_UIGridGetPagination(pageNumber,pageSize);
				});
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("masuk");
					if (row.isSelected == false) {
						console.log("false");
					}
					else {
						console.log("changed", row);
						//$scope.currentReconMappingMITId = row.entity.ReconMappingMITId
						//$scope.GetReconMITMapping(1, uiGridPageSize, row.entity.ReconMappingMITId);
					}
				});
			},
			//expandableRowTemplate: 'app/finance/reconciliation/prePickingET.html',
			//expandableRowHeight: 150,
			//expandableRowScope: {
			//  subGridVariable: 'subGridScopeVariable'
			//},
			columnDefs: [
				{ name: 'No Rev', field: 'DocId', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Tanggal Dokumen', field: 'TranDate', headerCellClass: $scope.highlightFilteredHeader, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nomor Dokumen', field: 'DocNo', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Tipe Dokumen', field: 'DocDesc', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Debit/Kredit', field: 'DebitCredit', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Nominal', field: 'PaymentTotal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'IPParentId', field: 'DocId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Status', field: 'Status', headerCellClass: $scope.highlightFilteredHeader },
			]
		};

		$scope.TambahReconMITDetail_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			onRegisterApi: function (gridApi) {
				$scope.gridApiHeader = gridApi;
			},

			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			columnDefs: [
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false },
				{ name: 'ReconMappingMITDetailId', field: 'ReconMappingMITDetailId', visible: false },
				{ name: 'DocId', displayName: "Nomor Dokumen MIT", field: 'DocId', visible: false },
				{ name: 'DocNo', enableCellEdit: false, displayName: "Nomor Dokumen MIT", field: 'DocNo', visible: true },
				{ name: 'Tipe Mapping', enableCellEdit: false, field: 'MappingType', visible: true },
				{ name: 'ID TRF', displayName: "ID TRF", field: 'IncomingInstructionDetailId', visible: false },
				{ name: 'Nomor TRF/MIT Rev', enableCellEdit: false, displayName: "Nomor TRF/MIT Rev", field: 'NoTRF', visible: true },
				{ name: 'Nominal', field: 'Nominal', enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2'},
				{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteRowBiaya(row)">Hapus</button>' },
			]
		};


		$scope.LihatReconDetail_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			onRegisterApi: function (gridApi) {
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);

					
					$scope.LihatReconDetail_UIGridGetPagination(pageNumber,pageSize);
					
					// OLD pagination
					// ReconciliationFactory.getReconDetail(pageNumber, pageSize, $scope.currentIncomingInstructionId)
					// 	.then(
					// 		function (res) {
					// 			$scope.LihatReconDetail_UIGrid.data = res.data.Result;
					// 			$scope.LihatReconDetail_UIGrid.totalItems = res.data.Total;
					// 			$scope.LihatReconDetail_UIGrid.paginationPageSize = pageSize;
					// 			$scope.LihatReconDetail_UIGrid.paginationPageSizes = [10, 25, 50];
								
					// 			$scope.loading = false;
					// 		},
					// 		function (err) {
					// 			console.log("err=>", err);
					// 		}
					// 	);
				});
			},
			columnDefs: [
				{ name: 'No TRF', displayName: 'No TRF', field: 'IncomingInstructionDetailId' },
				{ name: 'Tipe Transaksi', field: 'TransactionType' },
				{ name: 'Debit/Kredit', field: 'DebitCredit' },
				{ name: 'Nominal', field: 'Nominal', aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'number: 2',footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>' },
				{ name: 'Assignment', field: 'Assignment' },
				{ name: 'Nominal Pada MIT', displayName: "Nominal Pada MIT", field: 'NominalMIT', aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number', cellFilter: 'number: 2',footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>' },
				{ name: 'Status', field: 'Status' },
			]
		};


		$scope.LihatReconMIT_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
			onRegisterApi: function (gridApi) {
				$scope.LihatReconMIT_UIGrid_gridApi = gridApi;
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("masuk");
					if (row.isSelected == false) {
						console.log("false");
					}
					else {
						console.log("changed", row);
						console.log("reconid", $scope.currentReconciliationId);
						$scope.currentReconMappingMITId = row.entity.ReconMappingMITId;
						$scope.currentDocId = row.entity.DocId;
						$scope.currentDocNo = row.entity.DocNo;
						$scope.GetLihatReconMITMapping(1, uiGridPageSize, row.entity.ReconMappingMITId+"|"+$scope.currentReconciliationId);
					}
				});

				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", uiGridPageSize);
					$scope.LihatReconMIT_UIGridGetPagination(pageNumber, pageSize);
				});
			},
			columnDefs: [
				{ name: 'Tanggal Dokumen', field: 'TranDate', headerCellClass: $scope.highlightFilteredHeader, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nomor Dokumen', field: 'DocNo', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Tipe Dokumen', field: 'DocDesc', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Debit/Kredit', field: 'DebitCredit', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Nominal', field: 'PaymentTotal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Total Mapping', field: 'TotalNominal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'IPParentId', field: 'DocId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
			]
		};

		$scope.LihatReconMITReversal_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
			onRegisterApi: function (gridApi) {
				$scope.LihatReconMITReversal_UIGrid_gridApi = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", uiGridPageSize);
					$scope.LihatReconMITReversal_UIGridGetPagination(pageNumber, pageSize);
				});
			},
			columnDefs: [
				{ name: 'No Rev', field: 'DocId' },
				{ name: 'Tanggal Dokumen', field: 'TranDate', headerCellClass: $scope.highlightFilteredHeader, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Nomor Dokumen', field: 'DocNo', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Tipe Dokumen', field: 'DocDesc', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Debit/Kredit', field: 'DebitCredit', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'Nominal', field: 'PaymentTotal', cellFilter: 'number: 2', headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'IPParentId', field: 'DocId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false, headerCellClass: $scope.highlightFilteredHeader },
			]
		};

		$scope.LihatReconMITDetail_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			onRegisterApi: function (gridApi) {
				$scope.gridApiHeader = gridApi;
			},

			enableSorting: false,
			enableRowSelection: true,
			enableSelectAll: false,
			columnDefs: [
				{ name: 'ReconMappingMITId', field: 'ReconMappingMITId', visible: false },
				{ name: 'ReconMappingMITDetailId', field: 'ReconMappingMITDetailId', visible: false },
				{ name: 'DocId', displayName: "Nomor Dokumen MIT", field: 'DocId', visible: false },
				{ name: 'DocNo', enableCellEdit: false, displayName: "Nomor Dokumen MIT", field: 'DocNo', visible: true },
				{ name: 'Tipe Mapping', enableCellEdit: false, field: 'MappingType', visible: true },
				{ name: 'Nomor TRF/MIT Rev', enableCellEdit: false, displayName: "Nomor TRF/MIT Rev", field: 'IncomingInstructionDetailId', visible: true },
				{ name: 'Nominal', enableCellEdit: false, field: 'Nominal', cellFilter: 'number: 2' },
			]
		};

		$scope.CheckAllClear = function () {
			console.log("CheckAllClear");
			console.log("hasil", $scope.TambahReconciliation_SelisihMITDebet)
			console.log("hasil", $scope.TambahReconciliation_SelisihMITKredit)
			console.log("hasil", TambahReconciliation_DaftarTransaksi_AllClear)

			if ($scope.clearOK ==true ) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}
			else {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
				$scope.TambahReconciliation_Simpan_EnableDisable = false;
			}

			if (!$scope.TambahReconDetail_UIGrid.data.length && !$scope.TambahReconMITReversal_UIGrid.data.length ) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}

		};

		$scope.TambahReconParent_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,
			multiSelect: false,
			displaySelectionCheckbox: false,
			canSelectRows: true,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			enableRowSelection: true,
			enableSelectAll: false,
			enableFullRowSelection: true,
			enableSorting: false,
			onRegisterApi: function (gridApi) {
				$scope.TambahReconParent_UIGrid_gridAPI = gridApi;
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("masuk");
					if (row.isSelected == false) {
						console.log("false");
						$scope.currentIncomingInstructionId = 0;
						$scope.currentBeginBalance = 0;
						$scope.currentEndBalance = 0;
						$scope.currentNoRek = "";
						$scope.currentNoRekText = "";
						$scope.currentTglBank = "";
						$scope.TambahReconciliation_SaldoAwal = 0;
						$scope.TambahReconciliation_NomorRekeningBank = "";
						$scope.TambahReconciliation_TanggalBankStatement = "";
						$scope.TambahReconciliation_SaldoAkhir = 0;
						$scope.TambahReconciliation_SaldoAkhirSebelumnya = 0;
						$scope.TambahReconMITDetail_UIGrid.data = [];
						$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
						$scope.TambahReconciliation_Simpan_EnableDisable = false;
					}
					else {
						console.log("changed");
						$scope.currentIncomingInstructionId = row.entity.IncomingInstructionId;
						$scope.currentBeginBalance = row.entity.BeginBalance;
						$scope.currentEndBalance = row.entity.EndBalance;
						$scope.currentNoRek = row.entity.AccNumber;
						$scope.currentNoRekText = row.entity.AccountNo;
						// $scope.YesterdayBalance = row.entity.YesterdayBalance;
						console.log($scope.currentNoRek);
						console.log($scope.currentBeginBalance);
						console.log($scope.currentEndBalance);
						// console.log($scope.YesterdayBalance);
						$scope.currentTglBank = row.entity.BankStatementDate2;
						$scope.TambahReconciliation_SaldoAwal = addSeparatorsNF($scope.currentBeginBalance.toFixed(2), '.', '.', ',');
						$scope.TambahReconciliation_NomorRekeningBank = $scope.currentNoRekText;
						$scope.TambahReconciliation_NomorRekeningBank = $scope.currentNoRekText;
						$scope.TambahReconciliation_TanggalBankStatement = $scope.currentTglBank;
						$scope.TambahReconciliation_SaldoAkhir = addSeparatorsNF($scope.currentEndBalance.toFixed(2), '.', '.', ',');
						// $scope.TambahReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF($scope.YesterdayBalance.toFixed(2), '.', '.', ',');
						// $scope.GetDataReconDetail(1, uiGridPageSize);//pagination BackEnd old
						$scope.GetDataReconDetail(1, 1000); //pagination FE
						$scope.tmpIncomingInstructionId = row.entity.IncomingInstructionId;

					}
				});
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					$scope.getDataParentByDate(pageNumber, pageSize);
					uiGridPageNumberReconData = pageNumber;
					uiGridPageSizeReconData = pageSize;
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);
				});
			},
			columnDefs: [
				{ name: 'IncomingInstructionId', field: 'IncomingInstructionId', visible: false },
				{ name: 'Nomor Instruksi Bank', field: 'IncomingInstructionNumber' },
				{ name: 'Nomor Rekening Bank', field: 'AccountNo', visible: true },
				{ name: 'Tgl Bank Statement', field: 'BankStatementDate', visible: true, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Tgl Bank Statement2', field: 'BankStatementDate2', visible: false, cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'AccNumber', field: 'AccNumber', visible: false },
				{ name: 'IncomingInstructionId', field: 'IncomingInstructionId', visible: false },
				{ name: 'BeginBalance', field: 'BeginBalance', visible: false, cellFilter: 'rupiahC' },
				{ name: 'EndBalance', field: 'EndBalance', visible: false, cellFilter: 'rupiahC' }
			],
			// onRegisterApi: function (gridApi) {
			// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			// $scope.getDataParentByDate(pageNumber,pageSize);
			// console.log("pageNumber ==>", pageNumber);
			// console.log("pageSize ==>", pageSize);
			// });
			// },
		};

		$scope.CheckClearAllGrid = function () {
			var trsBank = $scope.TambahReconDetail_UIGridPagination;
			var trsMITRev = $scope.TambahReconMITReversal_UIGridPagination;
			var trsMIT = $scope.TambahReconMIT_UIGridPagination;
			$scope.clearOK = true;
			
			angular.forEach(trsBank, function (value, key) {
				if (value.Status != "Clear") {
					$scope.clearOK = false;
					return false;
				}
			});

			angular.forEach(trsMITRev, function (value, key) {
				if (value.Status != "Clear") {
					$scope.clearOK = false;
					return false;
				}
			});

			angular.forEach(trsMIT, function (value, key) {
				if (value.Status != "Clear") {
					$scope.clearOK = false;
					return false;
				}
			});
			console.log("disini",trsMIT);
			console.log("disini forech",$scope.clearOK);
			if (!$scope.TambahReconDetail_UIGrid.data.length && !$scope.TambahReconMITReversal_UIGrid.data.length ) {
				$scope.clearOK = true;
			}
			console.log("disini foreach",$scope.clearOK);

			if ($scope.clearOK == true) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}
			else {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
				$scope.TambahReconciliation_Simpan_EnableDisable = false;
			}
			
			if (!$scope.TambahReconDetail_UIGrid.data.length && !$scope.TambahReconMITReversal_UIGrid.data.length ) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}
		}

		$scope.GetReconMITMapping = function (Start, Limit, ReconId) {
			ReconciliationFactory.GetReconMITMapping(Start, Limit, ReconId)
				.then(
					function (res) {
						$scope.TambahReconMITDetail_UIGrid.data = res.data.Result;
						$scope.TambahReconMITDetail_UIGrid.totalItems = res.data.Total;
						$scope.TambahReconMITDetail_UIGrid.paginationPageSize = Limit;
						$scope.TambahReconMITDetail_UIGrid.paginationPageSizes = [10, 25, 50];
						// $scope.TambahReconciliation_TotalTRF=res.data.Result[0].TotalTRF;
						// $scope.TambahReconciliation_Selisih=res.data.Result[0].Selisih;
						// $scope.TambahReconciliation_TotalTRFMIT=res.data.Result[0].TotalTRFMIT;

					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};

		$scope.GetLihatReconMITMapping = function (Start, Limit, ReconId) {
			ReconciliationFactory.GetReconMITMapping(Start, Limit, ReconId)
				.then(
					function (res) {
						$scope.LihatReconMITDetail_UIGrid.data = res.data.Result;
						$scope.LihatReconMITDetail_UIGrid.totalItems = res.data.Total;
						$scope.LihatReconMITDetail_UIGrid.paginationPageSize = Limit;
						$scope.LihatReconMITDetail_UIGrid.paginationPageSizes = [10, 25, 50];
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};


		$scope.TambahReconciliation_Tambah = function () {
			console.log('tambah');
			console.log('test',parseFloat($scope.TambahReconciliation_NominalTRFforParseFloat).toFixed(2));
			console.log('test',$scope.TambahReconciliation_NominalTRFforParseFloat);
			console.log('test',$scope.currentPaymentTotal);

			if (angular.isUndefined($scope.currentDocId)) {
				bsNotify.show({
					title: "Warning",
					content: "MIT belum dipilih",
					type: 'warning'
				});
			}
			else if(parseFloat($scope.TambahReconciliation_NominalTRFforParseFloat).toFixed(2) > $scope.currentPaymentTotal){
				bsNotify.show({
					title: "Warning",
					content: "Nominal Mapping Melebihi Nominal MIT",
					type: 'warning'
				});
			}
			else {
				var NoMITRevExist = 0;
				var NoMITTRFExist = 0;
				var DKDifferentRevExist = 0;
				var DKDifferentTRFExist = 0;
				var noTRF = $scope.TambahReconciliation_NoTRF;
				//begin karna $scope.TambahReconciliation_NominalTRF input text jadi harus diakalin lagi jadi float
				var SisaFloat = "";
				if ($scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-3,1) == "."){
					SisaFloat = $scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-3);
					$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.substr(0,$scope.TambahReconciliation_NominalTRF.length-3);
					console.log("testSisaFloat",SisaFloat);
					console.log("testSisaFloat",$scope.TambahReconciliation_NominalTRF);
				}
				else if ($scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-2,1) == "."){
					SisaFloat = $scope.TambahReconciliation_NominalTRF.substr($scope.TambahReconciliation_NominalTRF.length-2);
					$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF.substr(0,$scope.TambahReconciliation_NominalTRF.length-2);
					console.log("testSisaFloat",SisaFloat);
					console.log("testSisaFloat",$scope.TambahReconciliation_NominalTRF);
				}
				$scope.nominalTRF = parseFloat($scope.TambahReconciliation_NominalTRF.split(".").join("")+SisaFloat);
				$scope.TambahReconciliation_NominalTRF = $scope.TambahReconciliation_NominalTRF +SisaFloat;

				console.log("nominal TRF >>", $scope.TambahReconciliation_NominalTRF);
				console.log("SisaFloat >>", SisaFloat);
				console.log("nominal TRF >>", $scope.nominalTRF);
				var saldo = parseFloat($scope.TambahReconciliation_NominalTRFforParseFloat);
				var loopGo = true;
				var trsMITNominal = 0;

				if (angular.isUndefined($scope.TambahReconciliation_TipeMapping_SearchDropdown) || $scope.TambahReconciliation_TipeMapping_SearchDropdown == '' || $scope.TambahReconciliation_TipeMapping_SearchDropdown == null){
					loopGo = false;
					bsNotify.show({
						title: "Warning",
						content: "Tipe Mapping harus diisi",
						type: 'warning'
					});
				}

				if (angular.isUndefined($scope.TambahReconciliation_NoTRF) || $scope.TambahReconciliation_NoTRF == '' || $scope.TambahReconciliation_NoTRF == null){
					loopGo = false;
					bsNotify.show({
						title: "Warning",
						content: "No TRF / No Rev harus diisi",
						type: 'warning'
					});
				}

				if (angular.isUndefined($scope.TambahReconciliation_NominalTRF) || $scope.TambahReconciliation_NominalTRF == '' || $scope.TambahReconciliation_NominalTRF == null){
					loopGo = false;
					bsNotify.show({
						title: "Warning",
						content: "Nominal harus diisi",
						type: 'warning'
					});
				}

				if ($scope.currentStatus == "Clear") {
					loopGo = false;
					bsNotify.show({
						title: "Warning",
						content: "Status MIT " + $scope.currentDocNo + " sudah Clear",
						type: 'warning'
					});
				}

				if (!loopGo) {
					return false;
				}
				loopGo = true;

				if($scope.TambahReconciliation_TipeMapping_SearchDropdown == "MIT Reversal"){
					
					if(!$scope.TambahReconMITReversal_UIGrid.data.length){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Daftar Transaksi MIT Reversal Kosong",
							type: 'warning'
						});
					}
				
					angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value, key) {
						console.log("value",value);
						console.log("$scope.currentDocNo",$scope.currentDocNo);
						if(loopGo)	
						{
							if (value.DocNo == $scope.currentDocNo && parseFloat(value.Nominal).toFixed(2) == $scope.nominalTRF && value.NoTRF == noTRF && value.MappingType == $scope.TambahReconciliation_TipeMapping_SearchDropdown) {
								loopGo = false;
								bsNotify.show({
									title: "Warning",
									content: "Mapping MIT " + $scope.currentDocNo + " sudah Ada",
									type: 'warning'
								});
							}
						}
					});

					angular.forEach($scope.TambahReconMITReversal_UIGridPagination, function (value2, key2) {
						
						if (value2.DocId == noTRF ) {
							NoMITRevExist++;

							if (value2.DebitCredit != $scope.currentDebitCredit){
								DKDifferentRevExist++;
							}
						}
					});

					if (NoMITRevExist == 0){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Nomor MIT Rev " + noTRF + " Tidak Valid",
							type: 'warning'
						});
					}

					if (DKDifferentRevExist > 0){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Tipe Debet / Kredit Tidak Sesuai",
							type: 'warning'
						});
					}
				}

				if (!loopGo) {
					return false;
				}
				loopGo = true;

				if ($scope.TambahReconciliation_TipeMapping_SearchDropdown == "TRF") {

					angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value2, key2) {
						
						if (value2.IncomingInstructionDetailId == noTRF ) {
							NoMITTRFExist++;

							if (value2.DebitCredit == 'Debet'){
								value2.DebitCredit = 'Debit';
							}

							if (value2.DebitCredit != $scope.currentDebitCredit){
								DKDifferentTRFExist++;
							}

							if (value2.Status == "Clear"){
								loopGo = false;
								bsNotify.show({
									title: "Warning",
									content: "Status TRF " + $scope.TambahReconciliation_NoTRF + " sudah Clear",
									type: 'warning'
								});
							}
						}

						
					});
					console.log("lewat 1");
					console.log("loopGo lewat 1",loopGo);

					if (NoMITTRFExist == 0){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Nomor TRF " + noTRF + " Tidak Valid",
							type: 'warning'
						});
					}

					if (DKDifferentTRFExist > 0){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Tipe Debet / Kredit Tidak Sesuai",
							type: 'warning'
						});
					}

					if (!loopGo) {
						return false;
					}
					loopGo = true;
	
					if(!$scope.TambahReconDetail_UIGrid.data.length){
						loopGo = false;
						bsNotify.show({
							title: "Warning",
							content: "Daftar Transaksi Bank Kosong",
							type: 'warning'
						});
					}
					else{

						angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value, key) {
							if(loopGo)	
							{
								if (value.DocNo == $scope.currentDocNo && parseFloat(value.Nominal).toFixed(2) == $scope.nominalTRF && value.NoTRF == noTRF && value.MappingType == $scope.TambahReconciliation_TipeMapping_SearchDropdown ) {
										loopGo = false;
										bsNotify.show({
											title: "Warning",
											content: "Mapping MIT " + $scope.currentDocNo + " sudah Ada",
											type: 'warning'
										});
									}
								}
						});

						angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value, key) {
							if (loopGo) {
								if (value.DebitCredit == 'Debet'){
									value.DebitCredit = 'Debit';
								}

								if (value.TransactionType == 'TRF' && noTRF == value.IncomingInstructionDetailId) {
									if (parseFloat(value.Nominal).toFixed(2) == $scope.nominalTRF) { 

										if(value.DebitCredit != $scope.currentDebitCredit ){
											loopGo = false;
											bsNotify.show({
												title: "Warning",
												content: "Tipe Debet / Kredit Tidak Sesuai",
												type: 'warning'
											});
										}
										else if (value.DebitCredit == $scope.currentDebitCredit && value.Status != 'Clear') {
											value.Status = 'Clear';
										}
										else {
											loopGo = false;
											bsNotify.show({
												title: "Warning",
												content: "Status TRF " + $scope.TambahReconciliation_NoTRF + " sudah Clear",
												type: 'warning'
											});
										}
									}
									else if (parseFloat(value.Nominal).toFixed(2) < $scope.nominalTRF){
										loopGo = false;
										bsNotify.show({
											title: "Warning",
											content: "Nominal Mapping Melebihi Nominal TRF",
											type: 'warning'
										});
									}
								}				
							}
						});
						console.log("lewat 2");

						if (!loopGo) {
							return false;
						}
						loopGo = true;

						angular.forEach($scope.TambahReconMIT_UIGridPagination, function (value, key) {
							console.log("value",value)
							console.log("currentDocNo",$scope.currentDocNo)
							if(loopGo)	
							{
								if (value.DocId == $scope.currentDocId && value.DocNo == $scope.currentDocNo) {
									value.TotalNominal = $scope.nominalTRF;
								}
								
								if (parseFloat(value.PaymentTotal).toFixed(2) == $scope.nominalTRF && value.DocId == $scope.currentDocId && value.DocNo == $scope.currentDocNo) {
									trsMITNominal = parseFloat(value.PaymentTotal).toFixed(2);
									
									if (value.Status != 'Clear') {
										value.Status = 'Clear';
									}
									else {
										loopGo = false;
										bsNotify.show({
											title: "Warning",
											content: "Status MIT " + $scope.currentDocNo + " sudah Clear",
											type: 'warning'
										});
									}
								}
								
								console.log("trsMITNominal",trsMITNominal);
							}
							//Many To One
							else if (value.DocId == $scope.currentReconMappingMITSelected.DocId){
								value.TotalNominal = $scope.nominalTRF;
							}
						});
					}
				}
				else {
					angular.forEach($scope.TambahReconMITReversal_UIGridPagination, function (value2, key2) {
						console.log("value2",value2);
						console.log("nominalTRF",$scope.nominalTRF);
						console.log("currentDebitCredit",$scope.currentDebitCredit);
						if (value2.PaymentTotal.toFixed(2) == $scope.nominalTRF && value2.DocId == noTRF ) {
							if(value2.DebitCredit != $scope.currentDebitCredit ){
								loopGo = false;
								bsNotify.show({
									title: "Warning",
									content: "Tipe Debet / Kredit Tidak Sesuai ",
									type: 'warning'
								});
							}
							else if (value2.DebitCredit == $scope.currentDebitCredit && value2.Status != 'Clear') {
								value2.Status = 'Clear';
							}
							else {
								loopGo = false;
								bsNotify.show({
									title: "Warning",
									content: "Status MIT Reversal " + value2.DocNo + " sudah Clear",
									type: 'warning'
								});
							}
						}
					});
					if (!loopGo) {
						return false;
					}
					loopGo = true;

					angular.forEach($scope.TambahReconMIT_UIGridPagination, function (value, key) {
						console.log("value",value)
						if(loopGo)	
						{
							if (value.PaymentTotal.toFixed(2) == $scope.nominalTRF && value.DocId == $scope.currentDocId && value.DocNo == $scope.currentDocNo ) {

								if (value.Status != 'Clear') {
									value.Status = 'Clear';
									value.TotalNominal = $scope.nominalTRF;
								}
								else {
									loopGo = false;
									bsNotify.show({
										title: "Warning",
										content: "Status MIT " + $scope.currentDocNo + " sudah Clear",
										type: 'warning'
									});
								}
							}
						}
						//Many To One
						else if (value.DocId == $scope.currentReconMappingMITSelected.DocId){
							value.TotalNominal = $scope.nominalTRF;
						}
					});
				}

				if (!loopGo) {
					return false;
				}

				$scope.RemappingOneToMany();

				if($scope.StateOneToMany)
				{
					$scope.ReconCreateDataMapping();
					$scope.CheckClearAllGrid();
	
					$scope.TambahReconMITDetail_UIGrid.data.push({
						DocId: $scope.currentDocId,
						DocNo: $scope.currentDocNo,
						MappingType: $scope.TambahReconciliation_TipeMapping_SearchDropdown,
						ReconMappingMITId: $scope.currentReconMappingMITId,
						IncomingInstructionDetailId: noTRF,
						NoTRF: $scope.TambahReconciliation_NoTRF,
						Nominal: saldo
					});	
				}
				else{
					return false;
				}
				
			}
		};


		$scope.RemappingOneToMany = function(){
			//Daftar Transaksi Bank
			// var totalDebit = 0;
			// var totalKredit = 0;
			// var selisihDebit = 0;
			// var selisihKredit = 0;
			var noTRF = $scope.TambahReconciliation_NoTRF;
			//var nominalTRF = parseFloat($scope.TambahReconciliation_NominalTRF);
			$scope.StateOneToMany = true;
			// begin many to one
			angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value, key) {
				var totalNominalMapping = 0;
				if ($scope.TambahReconciliation_TipeMapping_SearchDropdown == "TRF" && value.TransactionType == 'TRF' && noTRF == value.IncomingInstructionDetailId) {
					if (value.Nominal.toFixed(2) != $scope.nominalTRF) {
						angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value, key) {
							if (value.NoTRF == noTRF){
								totalNominalMapping += value.Nominal;
							}
						});
						if((totalNominalMapping+$scope.nominalTRF).toFixed(2) == value.Nominal){
							if (value.Status != 'Clear') {
								value.Status = 'Clear';
							}
						}
						else if((totalNominalMapping+$scope.nominalTRF).toFixed(2) > value.Nominal){
							bsNotify.show({
								title: "Warning",
								content: "Nominal Mapping Melebihi Nominal TRF",
								type: 'warning'
							});
							$scope.StateOneToMany= false;
						}
					}
					// //ilangin ajah ngerubah field2 total dari mapping2in
					// if (value.Status == 'Clear' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
					// 	totalDebit += value.Nominal;
					// }
					// else if (value.Status == 'Clear' && value.DebitCredit == 'Kredit') {
					// 	totalKredit += value.Nominal;
					// }
					// else if (value.Status == '' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
					// 	selisihDebit += value.Nominal;
					// }
					// else if (value.Status == '' && value.DebitCredit == 'Kredit') {
					// 	selisihKredit += value.Nominal;
					// }
				}					
				// else if(value.TransactionType == 'TRF'){
				// 	if (value.Status == 'Clear' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
				// 		totalDebit += value.Nominal;
				// 	}
				// 	else if (value.Status == 'Clear' && value.DebitCredit == 'Kredit') {
				// 		totalKredit += value.Nominal;
				// 	}
				// 	else if (value.Status == '' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
				// 		selisihDebit += value.Nominal;
				// 	}
				// 	else if (value.Status == '' && value.DebitCredit == 'Kredit') {
				// 		selisihKredit += value.Nominal;
				// 	}
				// }
			});
			// end many to one
			
			// begin one to many
			angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value1, key) {
				var totalNominalMapping = 0;
					if ($scope.TambahReconciliation_TipeMapping_SearchDropdown == "TRF" && value1.DocId == $scope.currentDocId && value1.DocNo == $scope.currentDocNo) {
						angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value2, key) {
							if (value1.IncomingInstructionDetailId == value2.IncomingInstructionDetailId){
								totalNominalMapping += value2.Nominal;
							}
						});
						if((totalNominalMapping+$scope.nominalTRF).toFixed(2) == $scope.currentPaymentTotal){
							
							angular.forEach($scope.TambahReconMIT_UIGridPagination, function (value3, key) {
								if (value3.DocId == $scope.currentDocId && value3.DocNo == $scope.currentDocNo) {
									if (value3.Status != 'Clear') {
										value3.Status = 'Clear';
									}
									value3.TotalNominal = (totalNominalMapping+$scope.nominalTRF).toFixed(2);
								}
							});
						}
						else if((totalNominalMapping+$scope.nominalTRF).toFixed(2) > $scope.currentPaymentTotal){
							bsNotify.show({
								title: "Warning",
								content: "Nominal Mapping Melebihi Nominal TRF",
								type: 'warning'
							});
							$scope.StateOneToMany= false;

							angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value4, key) {
								if (value4.IncomingInstructionDetailId == noTRF){
									value4.Status = '';
								}
							});

							angular.forEach($scope.TambahReconMIT_UIGridPagination, function (value3, key) {
								if (value3.DocId == $scope.currentDocId && value3.DocNo == $scope.currentDocNo) {
									value3.TotalNominal = totalNominalMapping;
								}
							});
						}
					}
			});
			// end one to many
			return true;
			// $scope.TambahReconciliation_TotalMITDebet = totalDebit;
			// $scope.TambahReconciliation_TotalMITKredit = totalKredit;
			// $scope.TambahReconciliation_SelisihMITDebet = selisihDebit;
			// $scope.TambahReconciliation_SelisihMITKredit = selisihKredit;
		}

		$scope.GetDataReconDetail = function (Start, Limit) {
			ReconciliationFactory.getReconDetail(Start, Limit, $scope.currentIncomingInstructionId)
				.then(
					function (res) {
						$scope.TambahReconDetail_UIGridPagination = res.data.Result;
						$scope.TambahReconDetail_UIGrid.paginationCurrentPage = 1;
						if (res.data.Result.length != 0) {
							$scope.TambahReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF(res.data.Result[0].YesterdayBalance.toFixed(2), '.', ',', '.');
						}
						
						// OLD Pagination
						// $scope.TambahReconDetail_UIGrid.data = res.data.Result;
						// $scope.TambahReconDetail_UIGrid.totalItems = res.data.Total;
						$scope.TambahReconDetail_UIGrid.paginationPageSize = 10;
						// NEW Pagination
						$scope.TambahReconDetail_UIGridGetPagination(1,$scope.TambahReconDetail_UIGrid.paginationPageSize);

						if (res.data.Total == 0){
							$scope.TambahReconciliation_TotalTRF = 0;
							$scope.TambahReconciliation_Selisih = 0;
							$scope.TambahReconciliation_TotalTRFMIT = 0;
							$scope.TambahReconciliation_TotalTRFDebet = 0;
							$scope.TambahReconciliation_TotalTRFKredit = 0;
						}
						else{
							$scope.TambahReconciliation_TotalTRF = addSeparatorsNF(res.data.Result[0].TotalTRF.toFixed(2), '.', '.', ',');
							$scope.TambahReconciliation_Selisih = addSeparatorsNF(res.data.Result[0].Selisih.toFixed(2), '.', '.', ',');
							$scope.TambahReconciliation_TotalTRFMIT = addSeparatorsNF(res.data.Result[0].TotalTRFMIT.toFixed(2), '.', '.', ',');
							$scope.TambahReconciliation_TotalTRFDebet = addSeparatorsNF(res.data.Result[0].TotalTRFDebit.toFixed(2), '.', '.', ',');
							$scope.TambahReconciliation_TotalTRFKredit = addSeparatorsNF(res.data.Result[0].TotalTRFCredit.toFixed(2), '.', '.', ',');
							// TambahReconciliation_DaftarTransaksi_AllClear = res.data.Result[0].OverallStatus;
						}

						// $scope.CheckAllClear();
						$scope.loading = false;

						ReconciliationFactory.getReconMITRevDetail(Start, Limit, $scope.currentIncomingInstructionId)
							.then(
								function (res2) {
									// OLD Pagination
									// $scope.TambahReconMITReversal_UIGrid.data = res2.data.Result;
									// $scope.TambahReconMITReversal_UIGrid.totalItems = res2.data.Total;
									// $scope.TambahReconMITReversal_UIGrid.paginationPageSize = Limit;

									// NEW Pagination
									$scope.TambahReconMITReversal_UIGridPagination = res2.data.Result;
									$scope.TambahReconMITReversal_UIGrid.paginationPageSize = 10;
									$scope.TambahReconMITReversal_UIGrid.paginationCurrentPage = 1;
									$scope.TambahReconMITReversal_UIGrid.paginationPageSizes = [10, 25, 50];
									$scope.loading = false;

									$scope.TambahReconMITReversal_UIGridGetPagination(1,$scope.TambahReconMITReversal_UIGrid.paginationPageSize);

									ReconciliationFactory.getReconMITDetail(Start, Limit, $scope.currentIncomingInstructionId)
										.then(
											function (res) {

												var TotalTRFDebet = 0;
												if (!angular.isNumber($scope.TambahReconciliation_TotalTRFDebet) && ($scope.TambahReconciliation_TotalTRFDebet != undefined)) {
													TotalTRFDebet = $scope.TambahReconciliation_TotalTRFDebet.replace(/,/g, "");
												}
												else {
													TotalTRFDebet = $scope.TambahReconciliation_TotalTRFDebet;
												}

												var TotalTRFKredit = 0;
												if (!angular.isNumber($scope.TambahReconciliation_TotalTRFKredit) && ($scope.TambahReconciliation_TotalTRFKredit != undefined)) {
													TotalTRFKredit = $scope.TambahReconciliation_TotalTRFKredit.replace(/,/g, "");
												}
												else {
													TotalTRFKredit = $scope.TambahReconciliation_TotalTRFKredit;
												}

												console.log("1", TotalTRFDebet);
												console.log("2", TotalTRFKredit);
												
												//OLD pagination
												// $scope.TambahReconMIT_UIGrid.data = res.data.Result;
												// $scope.TambahReconMIT_UIGrid.totalItems = res.data.Total;
												// $scope.TambahReconMIT_UIGrid.paginationPageSize = Limit;
												$scope.TambahReconMIT_UIGrid.paginationPageSizes = [10, 25, 50];
												$scope.TambahReconMIT_UIGrid.paginationCurrentPage = 1;

												// NEW Pagination
												$scope.TambahReconMIT_UIGridPagination = res.data.Result;
												$scope.TambahReconMIT_UIGridGetPagination(1,$scope.TambahReconMIT_UIGrid.paginationPageSize);

												if (res.data.Total == 0){
													$scope.TambahReconciliation_TotalMITDebet = 0;
													$scope.TambahReconciliation_TotalMITKredit = 0;
													$scope.TambahReconciliation_SelisihMITDebet = 0;
													$scope.TambahReconciliation_SelisihMITKredit = 0;
													
												}
												else{
													$scope.TambahReconciliation_TotalMITDebet = addSeparatorsNF(res.data.Result[0].TotalMITDebit.toFixed(2), '.', ',', '.');
													$scope.TambahReconciliation_TotalMITKredit = addSeparatorsNF(res.data.Result[0].TotalMITCredit.toFixed(2), '.', ',', '.');
													$scope.TambahReconciliation_SelisihMITDebet = addSeparatorsNF((parseFloat(TotalTRFDebet).toFixed(2) - res.data.Result[0].TotalMITDebit.toFixed(2)).toFixed(2), '.', ',', '.');
													$scope.TambahReconciliation_SelisihMITKredit = addSeparatorsNF((parseFloat(TotalTRFKredit).toFixed(2) - res.data.Result[0].TotalMITCredit.toFixed(2)).toFixed(2), '.', ',', '.');	
												}
												$scope.loading = false;

												$scope.GetReconMappingTemp();
												$scope.CheckClearAllGrid();
											}
										)

								}
							);

					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};

		$scope.deleteRowBiaya = function (row) {
			console.log("delete", row);
			//clear
			$scope.TambahReconMIT_UIGrid_gridApi.selection.clearSelectedRows();
			$scope.currentReconMappingMITId = undefined;
			$scope.currentDocId = undefined;
			$scope.currentDocNo = undefined;
			$scope.currentReconMappingMITSelected = undefined;
			$scope.currentDebitCredit = undefined;
			$scope.currentStatus = undefined;

			var index = $scope.TambahReconMITDetail_UIGrid.data.indexOf(row.entity);
			console.log(index);
			console.log($scope.TambahReconMITDetail_UIGrid.data.length);
			$scope.TambahReconMITDetail_UIGrid.data.splice(index, 1);

			$scope.ReconCreateDataMapping();

			// data =
			// 	[{
			// 		ReconMappingMITId: $scope.currentReconMappingMITId,
			// 		ReconMappingXML: "",
			// 		ReconMappingList: $scope.RekonDataMapping
			// 	}]

			// $scope.submitRekonDataMapping(data);

			console.log('nomiiiii -> ' + row.entity.Nominal);
			//TambahReconDetail_UIGrid
			//TambahReconMIT_UIGrid
			//TambahReconMIT_UIGrid

			// //ilangin ajah ngerubah field2 total dari mapping2in
			// var totalDebit = 0;
			// var totalKredit = 0;
			// var selisihDebit = 0;
			// var selisihKredit = 0;
			angular.forEach($scope.TambahReconDetail_UIGridPagination, function (value, key) {
				if (value.TransactionType == 'TRF') {
					if (value.Nominal == row.entity.Nominal && row.entity.NoTRF == value.IncomingInstructionDetailId) {
						value.Status = '';
					}

					if (row.entity.NoTRF == value.IncomingInstructionDetailId) {
						value.Status = '';
					}

			// 		if (value.Status == 'Clear' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
			// 			totalDebit += value.Nominal;
			// 		}
			// 		else if (value.Status == 'Clear' && value.DebitCredit == 'Kredit') {
			// 			totalKredit += value.Nominal;
			// 		}
			// 		else if (value.Status == '' && (value.DebitCredit == 'Debet' || value.DebitCredit == 'Debit')) {
			// 			selisihDebit += value.Nominal;
			// 		}
			// 		else if (value.Status == '' && value.DebitCredit == 'Kredit') {
			// 			selisihKredit += value.Nominal;
			// 		}
				}
			});

			// angular.forEach($scope.TambahReconMITReversal_UIGrid.data, function (value, key) {
			// 	if (value.PaymentTotal == row.entity.Nominal && row.entity.NoTRF == value.DocId) {
			// 		value.Status = '';
			// 	}
			// });

			// angular.forEach($scope.TambahReconMIT_UIGrid.data, function (value, key) {
			// 	if (value.PaymentTotal == row.entity.Nominal && row.entity.DocNo == value.DocNo) {
			// 		value.Status = '';
			// 		value.TotalNominal = 0;
			// 	}
			// });

			angular.forEach($scope.TambahReconMITReversal_UIGridPagination, function (value, key) {
				if (row.entity.NoTRF == value.DocId) {
					value.Status = '';
				}
			});

			angular.forEach($scope.TambahReconMIT_UIGridPagination, function (value, key) {
				if (row.entity.DocNo == value.DocNo) {
					value.Status = '';
					value.TotalNominal = value.TotalNominal - row.entity.Nominal;
				}
			});

			// //ilangin ajah ngerubah field2 total dari mapping2in
			// $scope.TambahReconciliation_TotalMITDebet = totalDebit;
			// $scope.TambahReconciliation_TotalMITKredit = totalKredit;
			// $scope.TambahReconciliation_SelisihMITDebet = selisihDebit;
			// $scope.TambahReconciliation_SelisihMITKredit = selisihKredit;

			// if (($scope.TambahReconciliation_SelisihMITDebet == 0) && ($scope.TambahReconciliation_SelisihMITKredit == 0)) {
			// 	TambahReconciliation_DaftarTransaksi_AllClear = true;
			// }
			// else {
			// 	TambahReconciliation_DaftarTransaksi_AllClear = false;
			// }

			if (TambahReconciliation_DaftarTransaksi_AllClear == true) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}
			else {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
				$scope.TambahReconciliation_Simpan_EnableDisable = false;
			}
		};


		$scope.GetSubData = function () {
			// var obj = 
			// 	{
			// 		"IPParentId" : 1001
			// 	};
			// $scope.SubData.push(obj);
			ReconciliationFactory.getReconMITDetail(1, 2, 5)
				.then(
					function (res) {
						console.log("satu");
						//console.log(res);
						//console.log(res.data);
						//console.log(res.data.Result);
						//$scope.SubData.push(res.data.Result);
						$scope.SubData = res;
						console.log("Sub2", $scope.SubData);
						$scope.$apply(function () {
							$scope.SubData = res.data.Result;
						});
					})

		};

		$scope.GetLihatReconDetail = function (Start, Limit) {
			ReconciliationFactory.getReconDetail(Start, Limit, $scope.currentIncomingInstructionId+"|"+$scope.currentReconciliationId)
				.then(
					function (res) {
						
						// NEW Pagination
						$scope.LihatReconDetail_UIGridPagination = res.data.Result;
						$scope.LihatReconDetail_UIGrid.paginationCurrentPage = 1;
						$scope.LihatReconDetail_UIGrid.paginationPageSize = 10;
						$scope.LihatReconDetail_UIGridGetPagination(1,$scope.LihatReconDetail_UIGrid.paginationPageSize);
						
						//OLD Pagination
						// $scope.LihatReconDetail_UIGrid.data = res.data.Result;
						// $scope.LihatReconDetail_UIGrid.totalItems = res.data.Total;
						// $scope.LihatReconDetail_UIGrid.paginationPageSize = Limit;
						// $scope.LihatReconDetail_UIGrid.paginationPageSizes = [10, 25, 50];
						//$scope.LihatReconciliation_TotalTRF= addSeparatorsNF(res.data.Result[0].TotalTRF, '.','.',','); 
						//$scope.LihatReconciliation_Selisih= addSeparatorsNF(res.data.Result[0].Selisih, '.','.',','); 
						//$scope.LihatReconciliation_TotalTRFMIT= addSeparatorsNF(res.data.Result[0].TotalTRFMIT, '.','.',','); 
						$scope.LihatReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF(res.data.Result[0].YesterdayBalance.toFixed(2), '.', '.', ',');
						$scope.LihatReconciliation_TotalTRFDebet = addSeparatorsNF(res.data.Result[0].TotalTRFDebit.toFixed(2), '.', '.', ',');
						$scope.LihatReconciliation_TotalTRFKredit = addSeparatorsNF(res.data.Result[0].TotalTRFCredit.toFixed(2), '.', '.', ',');
						//$scope.CheckAllClear(); 							
						$scope.loading = false;


						ReconciliationFactory.getReconMITRevDetail(Start, Limit, $scope.currentIncomingInstructionId+"|"+$scope.currentReconciliationId)
							.then(
								function (res2) {
									$scope.LihatReconMITReversal_UIGridPagination = res2.data.Result;
									$scope.LihatReconMITReversal_UIGrid.paginationCurrentPage = 1;
									$scope.LihatReconMITReversal_UIGrid.paginationPageSize = 10;
									$scope.LihatReconMITReversal_UIGridGetPagination(1,$scope.LihatReconMITReversal_UIGrid.paginationPageSize);

									$scope.loading = false;

									ReconciliationFactory.getReconMITDetail(Start, Limit, $scope.currentIncomingInstructionId+"|"+$scope.currentReconciliationId)
										.then(
											function (res) {

												var TotalTRFDebet = 0;
												if (!angular.isNumber($scope.LihatReconciliation_TotalTRFDebet) && ($scope.LihatReconciliation_TotalTRFDebet != undefined)) {
													TotalTRFDebet = $scope.LihatReconciliation_TotalTRFDebet.replace(/,/g, "");
												}
												else {
													TotalTRFDebet = $scope.LihatReconciliation_TotalTRFDebet;
												}

												var TotalTRFKredit = 0;
												if (!angular.isNumber($scope.LihatReconciliation_TotalTRFKredit) && ($scope.LihatReconciliation_TotalTRFKredit != undefined)) {
													TotalTRFKredit = $scope.LihatReconciliation_TotalTRFKredit.replace(/,/g, "");
												}
												else {
													TotalTRFKredit = $scope.LihatReconciliation_TotalTRFKredit;
												}

												console.log("1", TotalTRFDebet);
												console.log("2", TotalTRFKredit);

												$scope.LihatReconMIT_UIGridPagination = res.data.Result;
												$scope.LihatReconMIT_UIGrid.paginationCurrentPage = 1;
												$scope.LihatReconMIT_UIGrid.paginationPageSize = 10;
												$scope.LihatReconMIT_UIGridGetPagination(1,$scope.LihatReconMIT_UIGrid.paginationPageSize);

												// $scope.LihatReconMIT_UIGrid.data = res.data.Result;
												// $scope.LihatReconMIT_UIGrid.totalItems = res.data.Total;
												// $scope.LihatReconMIT_UIGrid.paginationPageSize = Limit;
												// $scope.LihatReconMIT_UIGrid.paginationPageSizes = [10, 25, 50];

												$scope.LihatReconciliation_TotalMITDebet = addSeparatorsNF(res.data.Result[0].TotalMITDebit.toFixed(2), '.', '.', ',');
												$scope.LihatReconciliation_TotalMITKredit = addSeparatorsNF(res.data.Result[0].TotalMITCredit.toFixed(2), '.', '.', ',');
												$scope.LihatReconciliation_SelisihMITDebet = addSeparatorsNF((parseFloat(TotalTRFDebet).toFixed(2) - res.data.Result[0].TotalMITDebit.toFixed(2)).toFixed(2), '.', '.', ',');
												$scope.LihatReconciliation_SelisihMITKredit = addSeparatorsNF((parseFloat(TotalTRFKredit).toFixed(2) - res.data.Result[0].TotalMITCredit.toFixed(2)).toFixed(2), '.', '.', ',');
												$scope.loading = false;
											}
										)

								}
							);

					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};

		$scope.links = {
            sortFun: function(firstDateString, secondDateString) {
				debugger;
				var a=new Date(firstDateString);
				var b=new Date(secondDateString);
				if (a < b) {
					return -1;
				}
				else if (a > b) {
					return 1;
				}
				else {
					return 0;
				}
			}
        };

		$scope.LihatReconParent_UIGrid = {
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: true,
			useExternalPagination: true,

			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,

			enableSorting: true,
			onRegisterApi: function (gridApi) {
				$scope.LihatReconParent_gridAPI = gridApi;
				gridApi.selection.on.rowSelectionChanged($scope, function (row) {
					console.log("masuk");
					if (row.isSelected == false) {
						console.log("false");
						$scope.currentIncomingInstructionId = 0;
						$scope.currentBeginBalance = 0;
						$scope.currentEndBalance = 0;
						$scope.currentNoRek = "";
						$scope.currentNoRekText = "";
						$scope.currentTglBank = "";
						$scope.LihatReconciliation_SaldoAwal = 0;
						$scope.LihatReconciliation_NomorRekeningBank = "";
						$scope.LihatReconciliation_TanggalBankStatement = "";
						$scope.LihatReconciliation_SaldoAkhir = 0;
						$scope.LihatReconciliation_SaldoAkhirSebelumnya = 0;
					}
					else {
						console.log("changed");
						$scope.currentIncomingInstructionId = row.entity.IncomingInstructionId;
						$scope.currentReconId = row.entity.ReconciliationId;
						$scope.currentBeginBalance = row.entity.BeginBalance;
						$scope.currentEndBalance = row.entity.EndBalance;
						$scope.currentNoRek = row.entity.AccNumber;
						$scope.currentNoRekText = row.entity.AccountNo;
						$scope.YesterdayBalance = row.entity.YesterdayBalance;
						console.log($scope.currentNoRek);
						$scope.currentTglBank = row.entity.BankStatementDate;
						$scope.LihatReconciliation_SaldoAwal = addSeparatorsNF($scope.currentBeginBalance.toFixed(2), '.', '.', ',');
						$scope.LihatReconciliation_NomorRekeningBank = $scope.currentNoRek;
						$scope.LihatReconciliation_TanggalBankStatement = $scope.currentTglBank;
						$scope.LihatReconciliation_SaldoAkhir = addSeparatorsNF($scope.currentEndBalance.toFixed(2), '.', '.', ',');
						//$scope.LihatReconciliation_SaldoAkhirSebelumnya=addSeparatorsNF($scope.YesterdayBalance, '.','.',',');  
						$scope.LihatReconciliation_TanggalReconciliation = row.entity.ReconciliationDate;
						$scope.LihatReconciliation_NomorReconciliation = row.entity.ReconciliationNo;
						$scope.LihatReconciliation_NamaBranch = $scope.LihatReconciliation_NamaBranch_SearchDropdown;
						$scope.GetLihatReconDetail(1, uiGridPageSize);
					}
				});

				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					
					$scope.getLihatDataParentByDate(pageNumber, pageSize);
					uiGridPageSizeReconParent = pageSize;
					uiGridPageNumberReconParent = pageNumber;
					console.log("pageNumber ==>", pageNumber);
					console.log("pageSize ==>", pageSize);
				});
			},
			enableRowSelection: true,
			enableSelectAll: false,
			columnDefs: [
				{ name: 'Nomor Instruksi Penerimaan', field: 'IncomingInstructionId', visible: false },
				{ name: 'Tgl Reconciliation', field: 'ReconciliationDate', cellFilter: 'date:\"dd/MM/yyyy\"', 'sortingAlgorithm': $scope.links.sortFun },
				{ name: 'ReconciliationId', field: 'ReconciliationId', visible: false },
				{ name: 'Nomor Reconciliation', field: 'ReconciliationNo', cellTemplate: '<a ng-click="grid.appScope.LihatDetailReconciliaton(row)">{{row.entity.ReconciliationNo}}</a>' },
				{ name: 'No Rekening Bank', field: 'AccountNo' },
				{ name: 'AccNumber', field: 'AccNumber', visible: false },
				{ name: 'Tgl Bank Statement', field: 'BankStatementDate', cellFilter: 'date:\"dd/MM/yyyy\"', 'sortingAlgorithm': $scope.links.sortFun },
				{ name: 'Tipe Pengajuan', field: 'TipePengajuan' },
				{ name: 'Tgl Pengajuan', field: 'TglPengajuan', cellFilter: 'date:\"dd/MM/yyyy\"' },
				{ name: 'Status Pengajuan', field: 'StatusPengajuan' },
				{ name: 'Alasan Pengajuan', field: 'AlasanPengajuan' },
				{ name: 'Alasan Penolakan', field: 'AlasanPenolakan' },
				{ name: 'BeginBalance', field: 'BeginBalance', visible: false, cellFilter: 'rupiahC' },
				{ name: 'EndBalance', field: 'EndBalance', visible: false, cellFilter: 'rupiahC' },
				{ name: 'ReconReverseNo', field: 'ReconReverseNo', visible: false }
			]
		};

		$scope.LihatDetailReconciliaton = function (row) {
			console.log("changed");
			$scope.ShowReconciliationMainDetail = true;
			$scope.ShowReconciliationMainTable = false;
			$scope.ShowLihatHeader = false;
			$scope.currentIncomingInstructionId = row.entity.IncomingInstructionId;
			$scope.currentReconId = row.entity.ReconciliationId;
			$scope.currentBeginBalance = row.entity.BeginBalance;
			$scope.currentEndBalance = row.entity.EndBalance;
			$scope.currentNoRek = row.entity.AccNumber;
			$scope.currentNoRekText = row.entity.AccountNo;
			$scope.currentReconciliationId = row.entity.ReconciliationId;
			// $scope.YesterdayBalance = row.entity.YesterdayBalance;
			console.log($scope.currentNoRek);
			$scope.currentTglBank = row.entity.BankStatementDate;
			$scope.LihatReconciliation_SaldoAwal = addSeparatorsNF($scope.currentBeginBalance.toFixed(2), '.', '.', ',');
			$scope.LihatReconciliation_NomorRekeningBank = $scope.currentNoRekText;
			$scope.LihatReconciliation_TanggalBankStatement = $scope.currentTglBank;
			$scope.LihatReconciliation_SaldoAkhir = addSeparatorsNF($scope.currentEndBalance.toFixed(2), '.', '.', ',');
			// $scope.LihatReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF($scope.YesterdayBalance.toFixed(2), '.', '.', ',');
			$scope.LihatReconciliation_TanggalReconciliation = row.entity.ReconciliationDate;
			$scope.LihatReconciliation_NomorReconciliation = row.entity.ReconciliationNo;
			$scope.LihatReconciliation_NamaBranch = $scope.LihatReconciliation_NamaBranch_SearchDropdown;
			
			// $scope.GetLihatReconDetail(1, uiGridPageSize); 	//OLD
			$scope.GetLihatReconDetail(1, 1000); //new FE pagination
			$scope.LihatReconMITDetail_UIGrid.data = [];
			$scope.LihatReconciliation_ReconReverseNo = row.entity.ReconReverseNo;
			if (row.entity.TipePengajuan == "Reversal"){
				$scope.LihatReconciliation_PengajuanReversalDisabled = true;
			}
			else{
				$scope.LihatReconciliation_PengajuanReversalDisabled = false;
			}
		}

		$scope.TambahReconciliation_DaftarReconciliationIndividual_SelectedValue = function () {

			console.log("individual");
			$scope.TambahReconciliation_NomorRekeningBank = $scope.TambahReconciliation_DaftarReconciliationIndividual.Nomor_Rekening_Bank;
			$scope.TambahReconciliation_SaldoAwal = addSeparatorsNF($scope.TambahReconciliation_DaftarReconciliationIndividual.Saldo_Awal.toFixed(2), '.', ',', '.');
			$scope.TambahReconciliation_TanggalBankStatement = new Date($scope.TambahReconciliation_DaftarReconciliationIndividual.Tanggal_Bank_Statement);
			$scope.TambahReconciliation_SaldoAkhir = addSeparatorsNF($scope.TambahReconciliation_DaftarReconciliationIndividual.Saldo_Akhir.toFixed(2), '.', ',', '.');
			$scope.TambahReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF($scope.TambahReconciliation_DaftarReconciliationIndividual.Saldo_Akhir_Sebelumnya.toFixed(2), '.', ',', '.');
			$scope.TambahReconciliation_DaftarTransaksiToRepeat = DaftarTransaksiFactory.getData();


			for (var zzz = 0; zzz < $scope.TambahReconciliation_DaftarTransaksiToRepeat.length; ++zzz) {
				if ($scope.TambahReconciliation_DaftarTransaksiToRepeat[zzz].Status == "Clear") {
					TambahReconciliation_DaftarTransaksi_AllClear = true;
				}
				else {
					TambahReconciliation_DaftarTransaksi_AllClear = false;
					break;
				}
			}
			if (TambahReconciliation_DaftarTransaksi_AllClear == true) {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = true;
				$scope.TambahReconciliation_Simpan_EnableDisable = true;
			}
			else {
				$scope.TambahReconciliation_SimpanCetak_EnableDisable = false;
				$scope.TambahReconciliation_Simpan_EnableDisable = false;
			}

		};

		$scope.LihatReconciliation_DaftarReconciliationIndividual_SelectedValue = function () {
			//alert($scope.TambahReconciliation_DaftarReconciliationIndividual.Nomor_Rekening_Bank);
			$scope.LihatReconciliation_NomorRekeningBank = $scope.LihatReconciliation_DaftarReconciliationIndividual.Nomor_Rekening_Bank;
			$scope.LihatReconciliation_SaldoAwal = addSeparatorsNF($scope.LihatReconciliation_DaftarReconciliationIndividual.Saldo_Awal.toFixed(2), '.', '.', ',');
			$scope.LihatReconciliation_TanggalBankStatement = new Date($scope.LihatReconciliation_DaftarReconciliationIndividual.Tanggal_Bank_Statement);
			$scope.LihatReconciliation_SaldoAkhir = addSeparatorsNF($scope.LihatReconciliation_DaftarReconciliationIndividual.Saldo_Akhir.toFixed(2), '.', '.', ',');
			$scope.LihatReconciliation_SaldoAkhirSebelumnya = addSeparatorsNF($scope.LihatReconciliation_DaftarReconciliationIndividual.Saldo_Akhir_Sebelumnya.toFixed(2), '.', '.', ',');
			$scope.LihatReconciliation_DaftarTransaksiToRepeat = DaftarTransaksiFactory.getData();

		};

		$scope.BackToMainReconciliation = function () {
			//20170517, nuse, begin
			// $scope.ShowTambahReconciliation=false;
			// $scope.ShowLihatReconciliation=false;
			// $scope.ShowReconciliationMain=true;
			$scope.ShowTambahReconciliation = false;
			$scope.ShowLihatReconciliation = true;
			//20170517, nuse, end
			$scope.LihatReconciliation_DaftarReconciliationToRepeat = "";
			$scope.TambahReconciliation_DaftarReconciliationToRepeat = "";

			$scope.TambahReconciliation_NomorRekeningBank = "";
			$scope.TambahReconciliation_SaldoAwal = "";
			$scope.TambahReconciliation_TanggalBankStatement = "";
			$scope.TambahReconciliation_SaldoAkhir = "";
			$scope.TambahReconciliation_SaldoAkhirSebelumnya = "";
			$scope.TambahReconciliation_DaftarTransaksiToRepeat = "";

			$scope.LihatReconciliation_NomorRekeningBank = "";
			$scope.LihatReconciliation_SaldoAwal = "";
			$scope.LihatReconciliation_TanggalBankStatement = "";
			$scope.LihatReconciliation_SaldoAkhir = "";
			$scope.LihatReconciliation_SaldoAkhirSebelumnya = "";
			$scope.LihatReconciliation_DaftarTransaksiToRepeat = "";

			$scope.LihatReconciliation_TanggalReconciliation = "";
			$scope.LihatReconciliation_NomorReconciliation = "";
			$scope.LihatReconciliation_NamaBranch = "";

			$scope.TambahReconParent_UIGrid.data = [];
			$scope.TambahReconDetail_UIGrid.data = [];
			$scope.LihatReconParent_UIGrid.data = [];
			$scope.LihatReconMITDetail_UIGrid.data = [];
			$scope.LihatReconMITReversal_UIGrid.data = [];
			$scope.LihatReconDetail_UIGrid.data = [];
			$scope.LihatReconMIT_UIGrid.data=[];

			$scope.TambahReconMIT_UIGrid.data = [];
			$scope.TambahReconMITDetail_UIGrid.data = [];

			$scope.TambahReconciliation_TotalTRFDebet = "";
			$scope.TambahReconciliation_TotalTRFKredit = "";
			$scope.TambahReconciliation_NoTRF = "";
			$scope.TambahReconciliation_NominalTRF = "";
			$scope.TambahReconciliation_SelisihMITDebet = "";
			$scope.TambahReconciliation_SelisihMITKredit = "";
			$scope.getLihatDataParentByDate(uiGridPageNumberReconParent, uiGridPageSizeReconParent);


			//nanti cari cara clear selected value



		};

		// $scope.TambahReconciliation_SimpanCetak_Click = function () {
		// //alert('simpan cetak');
		// Rekon_Simpan();
		// //$scope.BackToMainReconciliation();
		// };

		$scope.TambahReconciliation_Simpan_Click = function () {
			//alert('simpan doang');
			Rekon_Simpan();
		};

		function Rekon_Simpan() {
			$scope.CheckClearAllGrid();
			if (!$scope.clearOK) {
				bsNotify.show({
					title: "Warning",
					content: "Semua status transaksi harus Clear",
					type: 'warning'
				});
				return false;
			}

			var data;

			$scope.ReconCreateData();

			data = [{
				OutletId: $scope.currentOutletId,
				InstructionDate: $scope.TambahReconciliation_TanggalInstruksiPembayaran,
				ReversalReason: "",
				IncomingInstructionId: $scope.currentIncomingInstructionId,
				ReconDetailXML: "",
				ReconList: $scope.RekonData
			}]

			$scope.submitRekonData(data);
		};

		// function Rekon_Reverse() {

		// var data;

		// $scope.RekonData = [];

		// data = [{
		// ReversalReason:$scope.LihatReconciliation_PengajuanReversal_AlasanReversal.Value,
		// IncomingInstructionId:$scope.currentIncomingInstructionId,
		// ReconDetailXML:"",
		// ReconList:$scope.RekonData
		// }]

		// $scope.submitRekonData(data);
		// };
		//OLD
		// $scope.ReconCreateData = function () {
		// 	$scope.RekonData = [];
		// 	angular.forEach($scope.TambahReconDetail_UIGrid.data, function (value, key) {
		// 		var obj =
		// 			{
		// 				"IncomingInstructionDetailId": value.IncomingInstructionDetailId,
		// 				"IPDetailId": value.IPDetailId,
		// 				"Assignment": value.Assignment
		// 			};
		// 		console.log("Object of SO ==>", obj);
		// 		console.log("Object of Value ==>", value);
		// 		$scope.RekonData.push(obj);
		// 	});
		// };
		//NEW abe
		$scope.ReconCreateData = function () {
			$scope.RekonData = [];
			angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value, key) {
				var obj =
					{
						"IncomingInstructionDetailId": value.IncomingInstructionDetailId,
						"MappingType": value.MappingType,
						"DocId": value.DocId,
						"DocNo": value.DocNo,
						"Nominal": parseFloat(value.Nominal)
					};
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.RekonData.push(obj);
			});
		};

		$scope.ReconCreateDataMapping = function () {
			$scope.RekonDataMapping = [];
			angular.forEach($scope.TambahReconMITDetail_UIGrid.data, function (value, key) {
				var obj =
					{
						"ReconMappingMITId": value.ReconMappingMITId,
						"MappingType": value.MappingType,
						"DocId": value.DocId,
						"DocNo": value.DocNo,
						"IncomingInstructionDetailId": value.IncomingInstructionDetailId,
						"Nominal": value.Nominal
					};
				console.log("Object of SO ==>", obj);
				console.log("Object of Value ==>", value);
				$scope.RekonDataMapping.push(obj);
			});
			console.log("RekonDataMapping",$scope.RekonDataMapping);
		};

		$scope.submitRekonData = function (data) {
			ReconciliationFactory.submitRecon(data)
				.then
				(
				function (res) {
					console.log(res);
					$scope.currentReconciliationId = res.data.Result[0].ReconciliationId;

					bsNotify.show({
						title: "Berhasil",
						content: "Simpan Berhasil",
						type: 'success'
					});
					if ($scope.Cetak == "Y") {
						$scope.CetakRekon($scope.currentReconciliationId);
					}
					$scope.BackToMainReconciliation();
					//$scope.TambahIncomingPayment_Batal();
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) {
						bsNotify.show({
							title: "Warning",
							content: err.data.Message,
							type: 'warning'
						});
					}
				}
				);
		};

		$scope.submitRekonDataMapping = function (data) {
			ReconciliationFactory.submitReconMapping(data)
				.then
				(
				function (res) {
					console.log("berhasil", res);

					ReconciliationFactory.getReconMITDetail(1, 10, $scope.currentIncomingInstructionId)
						.then(
							function (res) {

								var TotalTRFDebet = 0;
								if (!angular.isNumber($scope.TambahReconciliation_TotalTRFDebet) && ($scope.TambahReconciliation_TotalTRFDebet != undefined)) {
									TotalTRFDebet = $scope.TambahReconciliation_TotalTRFDebet.replace(/,/g, "");
								}
								else {
									TotalTRFDebet = $scope.TambahReconciliation_TotalTRFDebet;
								}

								var TotalTRFKredit = 0;
								if (!angular.isNumber($scope.TambahReconciliation_TotalTRFKredit) && ($scope.TambahReconciliation_TotalTRFKredit != undefined)) {
									TotalTRFKredit = $scope.TambahReconciliation_TotalTRFKredit.replace(/,/g, "");
								}
								else {
									TotalTRFKredit = $scope.TambahReconciliation_TotalTRFKredit;
								}

								$scope.TambahReconMIT_UIGrid.data = res.data.Result;
								$scope.TambahReconMIT_UIGrid.totalItems = res.data.Total;
								$scope.TambahReconMIT_UIGrid.paginationPageSize = 10;
								$scope.TambahReconMIT_UIGrid.paginationPageSizes = [10, 25, 50];
								$scope.TambahReconciliation_TotalMITDebet = addSeparatorsNF(res.data.Result[0].TotalMITDebit.toFixed(2), '.', '.', ',');
								$scope.TambahReconciliation_TotalMITKredit = addSeparatorsNF(res.data.Result[0].TotalMITCredit.toFixed(2), '.', '.', ',');
								$scope.TambahReconciliation_SelisihMITDebet = addSeparatorsNF((parseFloat(TotalTRFDebet).toFixed(2) - res.data.Result[0].TotalMITDebit.toFixed(2)).toFixed(2), '.', '.', ',');
								$scope.TambahReconciliation_SelisihMITKredit = addSeparatorsNF((parseFloat(TotalTRFKredit).toFixed(2) - res.data.Result[0].TotalMITCredit.toFixed(2)).toFixed(2), '.', '.', ',');
								$scope.loading = false;

								$scope.CheckAllClear();

							}
						)
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) {
						console.log($scope.TambahReconMITDetail_UIGrid.data.length);
						$scope.TambahReconMITDetail_UIGrid.data.splice($scope.TambahReconMITDetail_UIGrid.data.length - 1, 1);
						bsNotify.show({
							title: "Warning",
							content: err.data.Message,
							type: 'warning'
						});
					}
				}
				);
		};

		$scope.LihatReconciliation_PengajuanReversal_Click = function () {
			angular.element('#ModalLihatReconciliation_PengajuanReversal').modal('show');
		};

		$scope.Batal_LihatReconciliation_PengajuanReversalModal = function () {
			angular.element('#ModalLihatReconciliation_PengajuanReversal').modal('hide');
		};

		$scope.Pengajuan_LihatReconciliation_PengajuanReversalModal = function () {
			var data;
			data =
				[{
					"ApprovalTypeName": "Reconciliation Reversal",
					"DocumentId": $scope.currentReconId,
					"ReversalReason": $scope.LihatReconciliation_PengajuanReversal_AlasanReversal
				}];

			ReconciliationFactory.submitDataApprovalReconciliation(data)
				.then
				(
				function (res) {
					console.log(res);
					bsNotify.show({
						title: "Berhasil",
						content: "Pengajuan Reversal Berhasil",
						type: 'success'
					});
					$scope.LihatReconciliation_Cari();

				},
				function (err) {
					console.log("err=>", err);
				}
				);

			// alert('diajukan'); ngapaa yak?
			angular.element('#ModalLihatReconciliation_PengajuanReversal').modal('hide');
			$scope.BackToMainReconciliation();
		};

		$scope.TambahReconciliation_DaftarReconciliation_Filter_Clicked = function () {
			if (($scope.TambahReconciliation_DaftarReconciliation_Text != "") &&
				($scope.TambahReconciliation_DaftarReconciliationSearch != "")) {
				var nomor;
				var value = $scope.TambahReconciliation_DaftarReconciliation_Text;
				$scope.TambahReconParent_UIGrid_gridAPI.grid.clearAllFilters();
				console.log('filter text -->', $scope.TambahReconciliation_DaftarReconciliation_Text);
				console.log('filter category -->', $scope.TambahReconciliation_DaftarReconciliationSearch);
				if ($scope.TambahReconciliation_DaftarReconciliationSearch == "Nomor Instruksi Penerimaan") {
					nomor = 2;
				}
				if ($scope.TambahReconciliation_DaftarReconciliationSearch == "Nomor Rekening Bank") {
					nomor = 3;
				}
				if ($scope.TambahReconciliation_DaftarReconciliationSearch == "Tanggal Bank Statement") {
					nomor = 4;
				}
				console.log("nomor --> ", nomor);
				$scope.TambahReconParent_UIGrid_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahReconciliation_DaftarReconciliation_Text;
				//$scope.TambahReconParent_UIGrid_gridAPI.grid.getColumn($scope.TambahReconciliation_DaftarReconciliationSearch).filters[0].term=$scope.TambahReconciliation_DaftarReconciliation_Text;
			}
			else {
				$scope.TambahReconParent_UIGrid_gridAPI.grid.clearAllFilters();
				//alert("inputan filter belum diisi !");
			}
		};


		$scope.LihatReconciliation_Filter_Clicked = function () {
			console.log("KLICK FILTER");
			$scope.getLihatDataParentByDate(uiGridPageNumberReconParent, uiGridPageSizeReconParent);
		}

		/*===============================Bulk Action Section, Begin===============================*/
		$scope.LihatReconciliation_DaftarBulkAction_Changed = function () {
			console.log("Bulk Action");
			if ($scope.LihatReconciliation_DaftarBulkAction_Search == "Setuju") {
				angular.forEach($scope.LihatReconParent_gridAPI.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolakReconciliation_IPId = value.IncomingInstructionId;
					if (value.StatusPengajuan == "Diajukan") {
						$scope.CreateApprovalData("Disetujui");
						ReconciliationFactory.updateApprovalData($scope.ApprovalData)
							.then(
								function (res) {
									bsNotify.show({
										title: "Berhasil",
										content: "Reversal Berhasil Disetujui",
										type: 'success'
									});
									$scope.Batal_LihatReconciliation_TolakModal();
								},

								function (err) {
									bsNotify.show({
										title: "Gagal",
										content: "Persetujuan Reversal Gagal",
										type: 'danger'
									});
								}
							);
					}
				});
			}
			else if ($scope.LihatReconciliation_DaftarBulkAction_Search == "Tolak") {
				console.log("grid api ap list ", $scope.LihatReconParent_gridAPI);
				angular.forEach($scope.LihatReconParent_gridAPI.selection.getSelectedRows(), function (value, key) {
					console.log(value);
					$scope.ModalTolakReconciliation_Tanggal = $filter('date')(new Date(value.ReconciliationDate), 'dd/MM/yyyy');
					$scope.ModalTolakReconciliation_NomorRecon = value.ReconciliationNo;
					$scope.ModalTolakReconciliation_NoRek = value.AccNumber;
					$scope.ModalTolakReconciliation_TglStatement = $filter('date')(new Date(value.BankStatementDate), 'dd/MM/yyyy');
					$scope.ModalTolakReconciliation_TipePengajuan = value.TipePengajuan;
					$scope.ModalTolakReconciliation_TanggalPengajuan = $filter('date')(new Date(value.TglPengajuan), 'dd/MM/yyyy');
					$scope.ModalTolakReconciliation_AlasanPengajuan = value.AlasanPengajuan;

					if (value.StatusPengajuan == "Diajukan")
						angular.element('#ModalTolakReconciliation').modal('show');
				}
				);
			}
		}

		$scope.CreateApprovalData = function (ApprovalStatus) {
			$scope.ApprovalData = [
				{
					"ApprovalTypeName": "Reconciliation Reversal",
					"RejectedReason": $scope.ModalTolakReconciliation_AlasanPenolakan,
					"DocumentId": $scope.currentReconId,
					"ApprovalStatusName": ApprovalStatus,
				}
			];
		}

		$scope.Batal_LihatReconciliation_TolakModal = function () {
			angular.element('#ModalTolakReconciliation').modal('hide');
			$scope.LihatReconciliation_DaftarBulkAction_Search = "";
			$scope.ClearActionFields();
			$scope.getLihatDataParentByDate(uiGridPageNumberReconParent, uiGridPageSizeReconParent);
		}

		$scope.Pengajuan_LihatReconciliation_TolakModal = function () {
			$scope.CreateApprovalData("Ditolak");
			ReconciliationFactory.updateApprovalData($scope.ApprovalData)
				.then(
					function (res) {
						bsNotify.show({
							title: "Berhasil",
							content: "Berhasil tolak",
							type: 'success'
						});
						$scope.Batal_LihatReconciliation_TolakModal();
					},

					function (err) {
						bsNotify.show({
							title: "Warning",
							content: "Penolakan gagal",
							type: 'warning'
						});
					}
				);
		}

		$scope.ClearActionFields = function () {
			$scope.ModalTolakReconciliation_Tanggal = "";
			$scope.ModalTolakReconciliation_NomorRecon = "";
			$scope.ModalTolakReconciliation_NoRek = "";
			$scope.ModalTolakReconciliation_TglStatement = "";
			$scope.ModalTolakReconciliation_TipePengajuan = "";
			$scope.ModalTolakReconciliation_TanggalPengajuan = "";
			$scope.ModalTolakReconciliation_AlasanPengajuan = "";
		}
		/*===============================Bulk Action Section, End===============================*/
		/*=================================Cetak Begin===================================*/

		$scope.LihatReconciliation_Cetak_Click = function () {
			$scope.CetakRekon($scope.currentReconciliationId);
		}

		$scope.TambahReconciliation_SimpanCetak_Click = function () {
			console.log("TambahReconciliation_SimpanCetak_Click");
			$scope.Cetak = "Y";
			Rekon_Simpan();
		}

		$scope.CetakRekon = function (RekonId) {
			var pdffile = "";
			var enddate = "";
			console.log("Cetak Rekon");
			$scope.currentReconciliationId = RekonId;
			ReconciliationFactory.getCetakReportRekon($scope.currentReconciliationId)
				.then(
					function (res) {
						var file = new Blob([res.data], { type: 'application/pdf' });
						var fileurl = URL.createObjectURL(file);

						console.log("pdf", fileurl);

						printJS(fileurl);
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		};
		/*=================================Cetak End===================================*/

	});
app.filter('rupiah', function () {
	return function (val) {
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
		}
		return val;
	};
});


app.filter('rupiahC', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
		}
		return val;
	};
});

function removeDuplicates(json_all) {
	var arr = [],
		collection = [];

	$.each(json_all, function (index, value) {
		if ($.inArray(value.IncomingInstructionDetailId, arr) == -1) {
			value.Nominal = value.Nominal.toString().replace('.0000', '');
			arr.push(value.IncomingInstructionDetailId);
			collection.push(value);
		}
	});
	return collection;
}
