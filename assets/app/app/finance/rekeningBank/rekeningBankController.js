var app = angular.module('app');
app.controller('RekeningBankController', function ($scope, $http, $filter, CurrentUser, RekeningBankFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.MainRekeningBank_Show = true;
	$scope.MaintenanceRekeningBank_Show = false;
	$scope.ArrayBank = [];
	$scope.ArrayMIT = [];
	$scope.Mode = "";
	$scope.ActId;

	RekeningBankFactory.getAllBankData()
		.then(
			function (res) {
				var bankList = [];
				angular.forEach(res.data.Result, function (value, key) {
					bankList.push({ "name": value.BankName, "value": value.BankId });
				});
				$scope.optionsRekeningBank = bankList;
			} 
		);

	RekeningBankFactory.getGL(1)
		.then(
			function (res) {
				var GLAccountBankList = [];
				angular.forEach(res.data.Result, function (value, key) {
					GLAccountBankList.push({ "name": value.GLAccountCode, "value": value.GLAccountId });
				});

				$scope.optionsGLAccountBank = GLAccountBankList;
				$scope.ArrayBank = res.data.Result;
			},
			function (err) {
				console.log('error -->', err)
			}
		);

	RekeningBankFactory.getGL(0).then(
		function (res) {
			var GLAccountMITList = [];
			angular.forEach(res.data.Result, function (value, key) {
				GLAccountMITList.push({ "name": value.GLAccountCode, "value": value.GLAccountId });
			});

			$scope.optionsGLAccountMIT = GLAccountMITList;
			$scope.ArrayMIT = res.data.Result;
		},
		function (err) {
			console.log('error -->', err)
		}
	);

	$scope.ToTambahRekeningBank = function () {
		$scope.MainRekeningBank_Show = false;
		$scope.MaintenanceRekeningBank_Show = true;
		$scope.Mode = "tambah";
		$scope.ActId = 0;

		$scope.mRekeningBank_AccountNo = "";
		$scope.mRekeningBank_AccountName = "";
		$scope.mRekeningBank_AccountBranch = "";
		$scope.mRekeningBank_BankId = null;
		$scope.mRekeningBank_isEscrowAcc = false;

		//Noto - 2018/05/07 - Set default berdasarkan user login, jangan set null
		//$scope.Tambah_NamaBranch = null;

		$scope.GLAccountMIT = null;
		$scope.GLAccountBank = null;
		$scope.GLAccountBankName = "";
		$scope.GLAccountMITName = "";
	}

	$scope.GLAccountBankName_Changed = function () {
		for (i = 0; i < $scope.ArrayBank.length; i++) {
			if ($scope.ArrayBank[i].GLAccountId == $scope.GLAccountBank) {
				$scope.GLAccountBankName = $scope.ArrayBank[i].GLAccountName;
			}
		}
	}

	$scope.GLAccountMIT_Changed = function () {
		for (i = 0; i < $scope.ArrayMIT.length; i++) {
			if ($scope.ArrayMIT[i].GLAccountId == $scope.GLAccountMIT) {
				$scope.GLAccountMITName = $scope.ArrayMIT[i].GLAccountName;
			}
		}
	}

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.Main_Cari = function () {
		RekeningBankFactory.getData($scope.Main_NamaBranch).then(
			function (res) {
				$scope.MainRekeningBank_UIGrid.data = res.data.Result;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.SetComboBranch = function () {
		var ho;
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.Main_NamaBranch_EnableDisable = true;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
			ho = 0;
		}

		RekeningBankFactory.getDataBranch(ho).then(
			function (resBr) {
				$scope.optionsMain_NamaBranch = resBr.data.Result;
				$scope.optionsTambah_NamaBranch = resBr.data.Result;
				$scope.Main_NamaBranch = $scope.user.OutletId;
				$scope.Tambah_NamaBranch = $scope.user.OutletId;
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	};

	$scope.SetComboBranch();

	$scope.Main_Back = function () {
		$scope.Mode = "";
		$scope.MainRekeningBank_Show = true;
		$scope.MaintenanceRekeningBank_Show = false;
	}

	$scope.Main_Save = function () {
		if ((!angular.isNumber($scope.mRekeningBank_BankId)) ||
			(!angular.isNumber($scope.GLAccountBank)) ||
			(!angular.isNumber($scope.GLAccountMIT)) ||
			(!angular.isNumber($scope.Tambah_NamaBranch)) ||
			($scope.mRekeningBank_AccountNo == '') ||
			($scope.mRekeningBank_AccountName == '') ||
			($scope.mRekeningBank_AccountBranch == '')) {
			// alert('Data tidak lengkap !');
			bsNotify.show({
				title: "Warning Message",
				content: "Data tidak lengkap !",
				type: 'warning',
				timeout: 2000,
			});
		}
		else {
			var BankAccountData = [{
				AccountId: $scope.ActId,
				AccountNo: $scope.mRekeningBank_AccountNo,
				AccountName: $scope.mRekeningBank_AccountName,
				AccountBranch: $scope.mRekeningBank_AccountBranch,
				BankId: $scope.mRekeningBank_BankId,
				isEscrowAcc: $scope.mRekeningBank_isEscrowAcc,
				OutletId: $scope.Tambah_NamaBranch,
				GLIdMIT: $scope.GLAccountMIT,
				GLIdBank: $scope.GLAccountBank
			}];

			if ($scope.Mode == "tambah") {
				RekeningBankFactory.create(BankAccountData).then(
					function (resBr) {
						$scope.Main_Cari();
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Berhasil menambah data Rekening",
								type: 'success'
							}
						);

						$scope.MainRekeningBank_Show = true;
						$scope.MaintenanceRekeningBank_Show = false;
						$scope.Main_Cari();
					},
					function (err) {
						console.log('error -->', err);
						bsNotify.show(
							{
								title: "Warning",
								content: err.data.Message,
								type: 'warning'
							}
						);
						return false;
					}
				);
			}
			else {
				RekeningBankFactory.update(BankAccountData).then(
					function (resBr) {
						$scope.Main_Cari();
						bsNotify.show(
							{
								title: "Berhasil",
								content: "Berhasil mengubah data Rekening",
								type: 'success'
							}
						);
						$scope.MainRekeningBank_Show = true;
						$scope.MaintenanceRekeningBank_Show = false;
						$scope.Main_Cari();
					},
					function (err) {
						console.log('error -->', err);
						bsNotify.show(
							{
								title: "Warning",
								content: err.data.Message,
								type: 'warning'
							}
						);
					}
				);
			}

		}
	}

	$scope.MainRekeningBank_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: true,
		enableSelectAll: true,

		columnDefs: [
			{ name: "Id", displayName: "Id", field: "AccountId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "AccountNo", displayName: "Nomor Rekening", field: "AccountNo", enableCellEdit: false, enableHiding: false },
			{ name: "NamaBank", displayName: "Nama Bank", field: "BankName", enableCellEdit: false, enableHiding: false, visible: true },
			{ name: "AccountBranch", displayName: "Cabang", field: "AccountBranch", enableCellEdit: false, enableHiding: false },
			{ name: "BankId", displayName: "BankId", field: "BankId", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "AccountName", displayName: "Nama Pemegang Rekening", field: "AccountName", enableCellEdit: false, enableHiding: false, visible: true },
			{ name: "OutletId", displayName: "OutletId", field: "OutletId", enableCellEdit: false, enableHiding: false, visible: false },
			{
				name: "isEscrowAcc", displayName: "Escrow Account", field: "isEscrowAcc", enableCellEdit: false, enableHiding: false, visible: true,
				type: 'boolean', cellTemplate: '<input type="checkbox" ng-model="row.entity.isEscrowAcc" disabled="disabled">'
			},
			{ name: "GLIdBank", displayName: "GLIdBank", field: "GLIdBank", enableCellEdit: false, enableHiding: false, visible: false },
			{ name: "GLIdMIT", displayName: "GLIdMIT", field: "GLIdMIT", enableCellEdit: false, enableHiding: false, visible: false },

			{ name: "GLAccountNameBank", displayName: "Nama GL Account Bank", field: "GLAccountNameBank", enableCellEdit: false, enableHiding: false },
			{ name: "GLAccountNameMIT", displayName: "Nama GL Account MIT", field: "GLAccountNameMIT", enableCellEdit: false, enableHiding: false },
			// {
			// 	name: "Action", displayName: "Action", field: "Action", enableCellEdit: false,
			// 	cellTemplate: ' <a ng-click="grid.appScope.EditRekeningBank(row)">Edit</a>&nbsp&nbsp<a ng-click="grid.appScope.DeleteRekeningBank(row)">Hapus</a>'
			// },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.EditRekeningBank(row)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.MainRekeningBank_UIGrid = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("$scope.MainRekeningBank_UIGrid.selection.getSelectedCount()", $scope.MainRekeningBank_UIGrid.selection.getSelectedCount());
				console.log("row", row);
				$scope.selectedRows = $scope.MainRekeningBank_UIGrid.selection.getSelectedRows();
			});
		}
	};

	$scope.actDel = function () {
		$scope.RekeningBank_TotalSelectedData = $scope.MainRekeningBank_UIGrid.selection.getSelectedCount();
		angular.element('#FinanceRekeningBankModal').modal('show');
	}

	$scope.FinanceRekeningBankOK_Button_Clicked = function () {
		var deleteData = [];
		angular.forEach($scope.MainRekeningBank_UIGrid.selection.getSelectedRows(), function (row, index) {
			deleteData.push(row.AccountId);
		});
		RekeningBankFactory.delete(deleteData).
			then(
				function (res) {
					RekeningBankFactory.getData($scope.Main_NamaBranch).then(
						function (res) {
							$scope.MainRekeningBank_UIGrid.data = res.data.Result;
							bsNotify.show(
								{
									title: "Berhasil",
									content: "Berhasil menghapus rekening bank",
									type: 'success'
								}
							);
						},
						function (err) {
							console.log('error -->', err);
							bsNotify.show(
								{
									title: "Gagal",
									content: "Gagal menghapus data rekening bank",
									type: 'danger'
								}
							);
						}
					);
					$scope.MainRekeningBank_UIGrid.selection.clearSelectedRows();
				},
				function (err) {
					bsNotify.show({
						title: "Warning",
						content: err.data.Message,
						type: 'warning'
					});
				}
			);
	}

	$scope.FinanceRekeningBankCancel_Button_Clicked = function () {
		angular.element('#FinanceRekeningBankModal').modal('hide');
	}

	//{name:"Preprinted Kuitansi", field:"isPreprintedReceipt", type: 'boolean',cellTemplate: '<input type="checkbox" ng-model="row.entity.isPreprintedReceipt">' },

	$scope.EditRekeningBank = function (row) {
		$scope.Mode = "edit";
		$scope.ActId = row.entity.AccountId;
		$scope.mRekeningBank_AccountNo = row.entity.AccountNo;
		$scope.mRekeningBank_AccountName = row.entity.AccountName;
		$scope.mRekeningBank_AccountBranch = row.entity.AccountBranch;
		$scope.mRekeningBank_BankId = row.entity.BankId;
		$scope.mRekeningBank_isEscrowAcc = row.entity.isEscrowAcc;
		$scope.Tambah_NamaBranch = row.entity.OutletId;
		$scope.GLAccountMIT = row.entity.GLIdMIT;
		$scope.GLAccountBank = row.entity.GLIdBank;
		$scope.GLAccountBankName = row.entity.GLAccountNameBank;
		$scope.GLAccountMITName = row.entity.GLAccountNameMIT;
		$scope.MainRekeningBank_Show = false;
		$scope.MaintenanceRekeningBank_Show = true;
	};

	$scope.DeleteRekeningBank = function (row) {
		$scope.ModalDelete_NoRek = row.entity.AccountNo;
		$scope.ModalDelete_Id = row.entity.AccountId;
		angular.element('#ModalDeleteData').modal('setting', { closeable: false }).modal('show');
	};

	$scope.Delete_Tidak = function () {
		angular.element('#ModalDeleteData').modal('hide');
	}

	$scope.Delete_Ya = function () {
		RekeningBankFactory.delete($scope.ModalDelete_Id).then(
			function (res) {
				alert('Berhasil Hapus Data');
				angular.element('#ModalDeleteData').modal('hide');
				$scope.Main_Cari();
			},
			function (err) {
				alert('Gagal Hapus Data !');
				console.log('error -->', err)
			}
		);
	}
});