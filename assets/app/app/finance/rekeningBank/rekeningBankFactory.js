angular.module('app')
	.factory('RekeningBankFactory', function ($http, CurrentUser) {
	return{
		getData: function(BranchId){
			var url = '/api/fe/FinanceBankAccount/start/1/limit/100/filterData/'+BranchId;
			console.log("url BankAccount : ", url);
			var res=$http.get(url);
			return res; 
		},
		
		create:function(BankAccountData){
			var url = '/api/fe/FinanceBankAccount/create/';
			//var ArrayBankAccountData = [BankAccountData];
			var param = JSON.stringify(BankAccountData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		}, 

		getDataBranch:function(orgId) {
			//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + orgId;
			var res=$http.get(url);  
			return res;			
   		},

		getGL: function(tipe){
			var url = '/api/fe/FinanceBankAccount/GetGL/tipe/' + tipe;
			console.log("url BankAccount : ", url);
			var res=$http.get(url);
			return res; 
		},
		
		update:function(BankAccountData){
			var url = '/api/fe/FinanceBankAccount/Update/';
			//var ArrayBankAccountData = [BankAccountData];
			var param = JSON.stringify(BankAccountData);
			console.log("update data ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		// delete:function(id){
		// 	var url = '/api/fe/FinanceBankAccount/DeleteData/Id/' + id;
		// 	console.log("url delete Data : ", url);
		// 	// var res=$http.get(url); //, {data:id, headers:{'content-type' : 'application/json'}});
		// 	return res;
		// },

		delete:function(d){
			var url = '/api/fe/FinanceBankAccount/DeleteData/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:d, headers:{'content-type' : 'application/json'}});
			return res;
		},
		
		getAllBankData:function(){
			var url = '/api/fe/FinanceBank/GetAllData';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		}
	}
});