angular.module('app')
  .factory('RekapIncomingPaymentCreditDebitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var debugMode = true;
    return {
		
    getDataRekap: function(Start, Limit, TranDate, BankName) {
		//var url = '/api/fe/RekapIncomingPaymentDebitCredit/bydate/?start='+Start+'&limit='+Limit+'&filterData='+TranDate+'/'+BankName;
		var url = '/api/fe/RekapIncomingPaymentDebitCredit/GetRekapByDate/start/' + Start + '/limit/' + Limit + '/filterData/' + TranDate + '|' + BankName;
    	//if (debugMode){console.log('Masuk ke getdata factory function dengan Sales Name :'+SalesName);};
    	//{console.log('url :'+url);};
		console.log(url);

      	var res=$http.get(url);
		return res;
    },

	  submitRekapData: function(inputData){
		  var url = '/api/fe/RekapIncomingPaymentDebitCredit/SubmitData';
		  var param = JSON.stringify(inputData);
			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };		  
		  var res=$http.post(url, param);
		  return res;
	  },
	
	//to supply combo box data
	getDataBankComboBox: function () {
		var url = '/api/fe/AccountBank/GetDataBankComboBox/';
		//if (debugMode) { console.log('Masuk ke getdata factory GetDataBankComboBox'); };
		//if (debugMode) { console.log('url :' + url); };
		var res = $http.get(url);
		return res;
	},
	
	getDataEDC:function(){
		var url = '/api/fe/FinanceEDC/getdata/';
		console.log("masuk get data finance EDC");
		var res=$http.get(url);
		return res;
	}
	
    //return {
      // getData: function() {
        // var res=$http.get('/api/fw/Role');
        // //console.log('res=>',res);
		
		// var da_json=[
				// {Id: "1",  NomorIncomingPayment: "income1",NomorKartu: "kartu1",NamaPelanggan: "didit1",Nominal: 100,TipeKartu: "Credit Card"},
				// {Id: "2",  NomorIncomingPayment: "income2",NomorKartu: "kartu2",NamaPelanggan: "didit2",Nominal: 100,TipeKartu: "Debit Card"},
				// {Id: "3",  NomorIncomingPayment: "income3",NomorKartu: "kartu3",NamaPelanggan: "didit3",Nominal: 100,TipeKartu: "Credit Card"},
				// {Id: "4",  NomorIncomingPayment: "income4",NomorKartu: "kartu4",NamaPelanggan: "didit4",Nominal: 100,TipeKartu: "Debit Card"},
				// {Id: "5",  NomorIncomingPayment: "income5",NomorKartu: "kartu5",NamaPelanggan: "didit5",Nominal: 100,TipeKartu: ""},
			  // ];
		// res=da_json;
		
        // return res;
      // },
      // create: function(rekapIncomingPaymentCreditDebit) {
        // return $http.post('/api/fw/Role', [{
                                            // AppId: 1,
                                            // ParentId: 0,
                                            // Name: IncomingPayment.Name,
                                            // Description: IncomingPayment.Description}]);
      // },
      // update: function(rekapIncomingPaymentCreditDebit){
        // return $http.put('/api/fw/Role', [{
                                            // Id: IncomingPayment.Id,
                                            // //pid: negotiation.pid,
                                            // Name: IncomingPayment.Name,
                                            // Description: IncomingPayment.Description}]);
      // },
      // delete: function(id) {
        // return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      // },
    }

  });