angular.module('app')
.controller('RekapIncomingPaymentCreditDebitController', function($scope, $http, $filter, CurrentUser, RekapIncomingPaymentCreditDebitFactory,$timeout,bsNotify) {
	$scope.RekapIncomingPaymentKreditDebit_Tabel=true;
   $scope.$on('$viewContentLoaded', function() {
		$scope.loading=false;
     
    });
    //----------------------------------
    // Initialization
    //----------------------------------
	$scope.user = CurrentUser.user();
	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};
	//New Pagination
	$scope.Rekap_UIGrid_UIGridData = [];
	// $scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment = new Date();
	// $scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment .setSeconds(0);
	// $scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment .setMinutes(0);
	// $scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment .setHours(0);
	
	var gridData = [];
	var uiGridPageSize =10;
	var LimitPagination = 0;
	var StartPagination = 0;
	
  $scope.getDataByDate = function(Start, Limit) {
		console.log("masuk1a");
				RekapIncomingPaymentCreditDebitFactory.getDataRekap(Start, Limit, $filter('date')(new Date($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment), 'yyyy-MM-dd'), $scope.RekapIncomingPaymentKreditDebit_NamaBank)
				.then(
						function(res){
				//new pagination
				$scope.Rekap_UIGrid_UIGridPagination = res.data.Result;
				// $scope.Rekap_UIGrid.data=res.data.Result;
				// $scope.Rekap_UIGrid.totalItems = res.data.Total;
				$scope.Rekap_UIGrid.paginationCurrentPage = 1;
				$scope.Rekap_UIGrid.paginationPageSize = 10;
				$scope.Rekap_UIGrid_UIGridGetPagination(1,$scope.Rekap_UIGrid.paginationPageSize);

				if($scope.Rekap_UIGrid_UIGridPagination.length > 0){
					$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = false;
				}else{
					$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;
				}
				// $scope.Rekap_UIGrid.paginationPageSizes = [Limit];				
								$scope.loading=false;
				//console.log("masuk1c");
				var TotalCreditCardTot=0;
				var TotalDebitCardTot=0;
			
			if($scope.Rekap_UIGrid_UIGridPagination.length > 0){
				for (var k = 0; k < res.data.Result['0'].TotalData; k++)
			
				{
						console.log("masukBARU");

							var val=res.data.Result[k].Value;
							//console.log(val);
							if (val == "Credit Card")
							{
							TotalCreditCardTot+=res.data.Result[k].PaymentTotal;
							}
							else
							{
							TotalDebitCardTot+=res.data.Result[k].PaymentTotal;
							}
							console.log(TotalDebitCardTot);
							$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = TotalCreditCardTot;
							$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = TotalDebitCardTot;
							
				}		
			}else{
				
				$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = "";
				$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = "";
			}
				},
						function(err){
								console.log("err=>",err);
						}
				)
		//console.log($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment);
        //console.log("masuk1b");
		
    };
	

		//================================ Start Get Yang Baru ==============================

		// $scope.getDataByDateTot = function(Start, Limit) {
		// 	console.log("masuk1a");
		// 	//console.log($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment);
		// 			//console.log("masuk1b");
		// 	RekapIncomingPaymentCreditDebitFactory.getDataRekap(Start, Limit, $filter('date')(new Date($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment), 'yyyy-MM-dd'), $scope.RekapIncomingPaymentKreditDebit_NamaBank)
		// 			.then(
		// 					function(res){
					
		// 			var TotalCreditCardTot=0;
		// 			var TotalDebitCardTot=0;
		// 			for (var k = 0; k < res.data.Result['0'].TotalData; k++)
				
		// 			{
		// 					//console.log("masuk1d");
	
		// 						 //Perform your desired thing over here
		// 						var val=res.data.Result[k].Value;  //to getData
		// 						//console.log(val);
		// 						if (val == "Credit Card")
		// 						{
		// 						TotalCreditCardTot+=res.data.Result[k].PaymentTotal;
		// 						}
		// 						else
		// 						{
		// 						TotalDebitCardTot+=res.data.Result[k].PaymentTotal;
		// 						}
		// 						console.log(TotalDebitCardTot);
		// 						$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = TotalCreditCardTot;
		// 						$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = TotalDebitCardTot;
								
		// 			 }			
		// 					},
		// 					function(err){
		// 							console.log("err=>",err);
		// 					}
		// 			)
		// 	};


		//================================= END GET NEW =====================================

	 $scope.RekapIncomingPaymentKreditDebit_Simpan = function(){
	
		 // var RekapData="";
		// for (var k = 0; k < $scope.Rekap_UIGrid.totalItems; k++)
		// {
		    // //Perform your desired thing over here
			// RekapData = RekapData + $scope.Rekap_UIGrid.data[k].IPParentId + "/"  + $scope.Rekap_UIGrid.data[k].Value + ";";
		 // }
		// console.log("Data ==>", RekapData);		 
		// var Rekap_SubmitData = [{
			// Data:RekapData
		// }];
		// console.log("Data ==>", $scope.RekapData);
		// console.log("data di ui grid >>>>", $scope.Rekap_UIGrid.data.length);
		// if($scope.Rekap_UIGrid.data != null || $scope.Rekap_UIGrid != undefined){
			
		var data;
		$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = false;
		$scope.RekapCreateData();
		
		if($scope.Rekap_UIGrid.data == "" || $scope.Rekap_UIGrid.data == null || $scope.Rekap_UIGrid.data == undefined){
			bsNotify.show({
				title: "Warning",
				content: "Tidak Ada Data yang direkap!",
				type:"warning"
			})
			return false;
		}

		data = 
		[{
			RekapDetailXML:"",
			RekapList:$scope.RekapData,
			TotalDebit:$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard,
			TotalCredit:$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard,
			CardProvider:$scope.RekapIncomingPaymentKreditDebit_NamaBank,
			IncomingDate:$filter('date')(new Date($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment), 'yyyy-MM-dd')
		}]
		
		RekapIncomingPaymentCreditDebitFactory.submitRekapData(data)
			.then(
				function (res) {
					console.log(res);
					//alert("Simpan Berhasil");
					$scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment = '';
					$scope.RekapIncomingPaymentKreditDebit_NamaBank = '';
					$scope.Rekap_UIGrid.data = [];
					$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = '';
					$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = '';
					bsNotify.show({
						title: "Success Message",
						content: "Simpan Berhasil.",
						type: 'success'
					});
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) 
					{
						alert(err.data.Message);
						$scope.Rekap_UIGrid.data = [];
						$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = '';
						$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = '';
					}						
				}
			);	
			// }	else{
				
			// $scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;
			// }
	 };

	//$scope.Rekap_UIGrid = { };
	$scope.Rekap_UIGrid = {
		enableSorting:false,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination : true,
		enableColumnResizing: true,

		onRegisterApi: function(gridApi){
			//$scope.gridApi_DaftarSpk = gridApi;
			//gridApi.selection.on.rowSelectionChanged($scope,function(row){
		    gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
			console.log("rowEntity",rowEntity);
			console.log("$scope.Rekap_UIGrid.data",$scope.Rekap_UIGrid.data);
			console.log("Rekap_UIGrid_UIGridPagination",$scope.Rekap_UIGrid_UIGridPagination);
			var TotalCreditCard=0;
			var TotalDebitCard=0;
			for (var k = 0; k < $scope.Rekap_UIGrid_UIGridPagination.length; k++)
			{
				  console.log("$scope.Rekap_UIGrid_UIGridPagination",$scope.Rekap_UIGrid_UIGridPagination[k]);
				  var val=$scope.Rekap_UIGrid_UIGridPagination[k].Value;  //to getData
				  console.log(val);
				  if (val == "Credit Card")
				  {
					TotalCreditCard+=$scope.Rekap_UIGrid_UIGridPagination[k].PaymentTotal;
				  }
				  else
				  {
					TotalDebitCard+=$scope.Rekap_UIGrid_UIGridPagination[k].PaymentTotal;
				  }
				  console.log(TotalDebitCard);
				  $scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = TotalCreditCard;
				  $scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = TotalDebitCard;								  
			}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log("pageNumber");
				console.log(pageNumber);
				$scope.Rekap_UIGrid_UIGridGetPagination(pageNumber,pageSize);
			});
			// gridApi.core.on.renderingComplete($scope, function () {
			// 	$scope.getDataByDate(1,pageSize);
			// });
		// gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
			// var msg = 'rows changed ' + rows.length;
			// console.log(msg);
		// });
		  
		},
		//enableCellSelection: true,
		//enableSelectAll: false,
		columnDefs: [
			{ name:'IPParentId',    field:'IPParentId', visible:false, width:'20%'},
			{ name:'Nomor Incoming Payment',    field:'IncomingNo', width:'20%'},
			{ name:'Nomor Kartu',    field:'CardNo', width:'20%'},
			{ name:'Nama Pelanggan',    field:'CustomerName', width:'20%'},
			{ name:'Nominal',    field:'PaymentTotal', cellFilter: 'currency:"":0', width:'20%'},
			{ name:'TipeKartu',   field:'Value', width: '20%',
			enableCellEdit: true,
			editableCellTemplate: 'ui-grid/dropdownEditor',
			cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				            return 'canEdit';
				        },
			//cellFilter: 'mapType', 
			editDropdownValueLabel: 'Debit_or_Credit', 
			editDropdownOptionsArray: [
			{ id: 'Debit Card', Debit_or_Credit: 'Debit Card' },
			{ id: 'Credit Card', Debit_or_Credit: 'Credit Card' }
			]},
		]
	};

	// Change
	$scope.RekapIncomingPaymentKreditDebit_NamaBank_Change = function(){
		console.log("ganti >>>>>>",$scope.RekapIncomingPaymentKreditDebit_NamaBank);
			$scope.Rekap_UIGrid.data = [];
				$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;
				$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = 0;
				$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = 0;
	}

	$scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment_Change = function(){
		console.log("ganti tanggal >>>>>>>>");
		$scope.Rekap_UIGrid.data = [];
			$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;
			$scope.RekapIncomingPaymentKreditDebit_TotalDebitCard = 0;
			$scope.RekapIncomingPaymentKreditDebit_TotalCreditCard = 0;
	}
	
	$scope.getDataEDC = function(){
		console.log("GetDataBank");
		var dataBank = [];
		RekapIncomingPaymentCreditDebitFactory.getDataEDC()
			.then(
			function (res) {
				$scope.loading = false;
				angular.forEach(res.data.Result, function(value, key){
					dataBank.push({name:value.EDCName, value:value.EDCId});
				});
				
				$scope.RekapIncomingPaymentKreditDebit_NamaBankOption = dataBank;
			},
			function (err) {
				console.log("err=>", err);
				}
		)
	}
	

	$scope.getDataEDC();
	
	$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;

	$scope.RekapIncomingPaymentKreditDebit_Cari = function () {
		console.log("data di ui grid aaaaaaaaaaaaaa >>>>", $scope.Rekap_UIGrid.length);
		if($scope.Rekap_UIGrid.data != null && $scope.Rekap_UIGrid.data != undefined){
			$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = false;

			if ($scope.RekapIncomingPaymentKreditDebit_TanggalIncomingPayment != null)
			{
				console.log("disini >>>?>?>?");
					$scope.getDataByDate(1, 1000);
					// $scope.getDataByDateTot(1, 1000);
					$scope.RekapIncomingPaymentKreditDebit_Tabel=true;
			}
			else
			{
				//alert("Tgl Blm Diisi");
				bsNotify.show({
					title: "Warning",
					content: "Tanggal belum diisi",
					type: 'warning'
				});
			}	
		}else{
			$scope.RekapIncomingPaymentKreditDebit_Simpan_Disabled = true;
		}
	};
	
	$scope.RekapCreateData = function(){
		$scope.RekapData = [];
		angular.forEach($scope.Rekap_UIGrid.data, function(value, key){
		var obj = {"IPParentId" : value.IPParentId, "Value" : value.Value};
		console.log("Object of SO ==>", obj);
		console.log("Object of Value ==>", value);
		$scope.RekapData.push(obj);
	});
	};		
	$scope.Rekap_UIGrid_UIGridGetPagination = function(startingPage,limitPage){
		console.log("startingPage",startingPage);
		console.log("startingPage",limitPage);
		console.log("Rekap_UIGrid_UIGridPagination",$scope.Rekap_UIGrid_UIGridPagination);
		$scope.Rekap_UIGrid_UIGridData = [];
		if (startingPage > 1 && startingPage*limitPage < $scope.Rekap_UIGrid_UIGridPagination.length) 
			{StartPagination = (startingPage-1)*limitPage+1;
				LimitPagination = startingPage*limitPage;}
		else if (startingPage > 1 && startingPage*limitPage > $scope.Rekap_UIGrid_UIGridPagination.length)
			{StartPagination = (startingPage-1)*limitPage+1;
				LimitPagination = $scope.Rekap_UIGrid_UIGridPagination.length;}
		else if (startingPage == 1 && startingPage*limitPage > $scope.Rekap_UIGrid_UIGridPagination.length)
			{StartPagination = startingPage;

				LimitPagination = $scope.Rekap_UIGrid_UIGridPagination.length;}
		else 
			{StartPagination = startingPage;
				LimitPagination = startingPage*limitPage;}

		for (var i = StartPagination-1; i <LimitPagination;i++ ){
			
			$scope.Rekap_UIGrid_UIGridData.push($scope.Rekap_UIGrid_UIGridPagination[i]);
			console.log("$scope.Rekap_UIGrid_UIGridData",$scope.Rekap_UIGrid_UIGridPagination[i]);
		}

		$scope.Rekap_UIGrid.data =  $scope.Rekap_UIGrid_UIGridData;
		$scope.Rekap_UIGrid.totalItems = $scope.Rekap_UIGrid_UIGridPagination.length;
	}
	// $scope.TambahIncomingPayment_DaftarRekap = function(pageNumber){
	  // RekapIncomingPaymentCreditDebitFactory.getDataRekap(pageNumber, uiGridPageSize)
	  // .then(
		// function(res)
		// {
			// $scope.Rekap_UIGrid.data = res.data.Result;
			// $scope.Rekap_UIGrid.totalItems = res.data.Total;
			// $scope.Rekap_UIGrid.paginationPageSize = uiGridPageSize;
			// $scope.Rekap_UIGrid.paginationPageSizes = [uiGridPageSize];
		// }
	  // );
	// }

	// $scope.RekapIncomingPaymentKreditDebit_Simpan = function () {

		// alert('simpan');
		// $scope.List_DaftarIncomingPaymentKreditDebit_To_Repeat="";
		// $scope.RekapIncomingPaymentKreditDebit_TotalCreditCard="";
		// $scope.RekapIncomingPaymentKreditDebit_TotalDebitCard="";
		// $scope.RekapIncomingPaymentKreditDebit_Tabel=false;
	// };
	

})

	.filter('mapType', function() {
		console.log("filter");
	  var cardHash = {
		'debit': 'Debit Card',
		'credit': 'Credit Card'
	  };

	  return function(input) {
		if (!input) {
		  return '';
		} else {
		  return cardHash[input];
		}
	  };	
	}
);  

