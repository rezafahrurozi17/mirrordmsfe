angular.module('app')
  .factory('OutgoingPaymentFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
	
	factory.getPaymentInstructionCharges= function(start, limit, paymentId) {
    //var url = '/api/fe/PaymentInstructionCharges/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + paymentId;
		var url = '/api/fe/PaymentInstructionCharges/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + paymentId;
		var res=$http.get(url);  

	 	/*	var da_json=[
				{PIChargesId: 1, OtherCharges: "Biaya Bank", DebitCredit: "D",Nominal: 1000},
				{PIChargesId: 2, OtherCharges: "Bea Materai", DebitCredit: "K",Nominal: 2000},
				{PIChargesId: 3, OtherCharges: "Biaya Bank", DebitCredit: "D",Nominal: 3000}			  ];
		var res=da_json; 
				console.log('masuk ke factory');*/
        return res;		
	};

	factory.updateApprovalData= function(inputData){
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		};	

	// factory.updateApprovalData= function(inputData){
	// 		var url = '/api/fe/financeapproval/ApproveRejectData/';
	// 		var param = JSON.stringify(inputData);
	// 		console.log("object input update approval ", param);
	// 		var res=$http.post(url, param);
	// 		return res;
	// 	};
		
	factory.getDataBranch=function(orgId) {
    //var url = '/api/fe/Branch/SelectData/Start/0/Limit/0/filterData/0';
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData='+orgId;
		var res=$http.get(url);  
    return res;			
	};

	//utk main approval
	factory.getDataOutgoingPaymentRequest= function(start,limit, filterData ) {
    //var url = '/api/fe/OutgoingPayment/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + filterData;
		var url = '/api/fe/OutgoingPayment/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
		var res=$http.get(url);  
    return res;	
	};

	factory.getDataIncomingInstruction= function(start, limit, filterData, uid, outletId) {		
		//var url = '/api/fe/PaymentInstruction/SelectData/start/'+ start + '/limit/' + limit + '/filterData/' + filterData; // + '/uid/' + uid;
    var url = '/api/fe/OutgoingPaymentInstruction/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData + '&OutletId=' +outletId;
		console.log("getDataIncomingInstruction :", url);
		var res=$http.get(url);  
		//return res;
		
/* 		var da_json=[
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 1",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 1",Nominal_Pembayaran: 1000},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 2",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 2",Nominal_Pembayaran: 1000},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 3",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 3",Nominal_Pembayaran: 1000},
			  ];
		res=da_json; */
		
        return res;
	};	
	
	//New Filter
	factory.changeFormatDate = function(item) {
		var tmpAppointmentDate = item;
		var finalDate
		tmpAppointmentDate = item.split('-');
		finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
		return finalDate;
	}
	
	factory.getDataFilter= function(start, limit, filterData, searchText, searchType, outletId, flag) {		
		if(flag == 1){
			searchText = this.changeFormatDate(searchText);
			console.log("cek tanggal", searchText)
		}
		var url = '/api/fe/OutgoingPaymentInstruction/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData +'|'+ searchText +'|'+ searchType + '&OutletId=' +outletId;
		console.log("getDataIncomingInstruction :", url);
		var res=$http.get(url);  
        return res;
    };	

	factory.getIncomingVendorBank= function(paymentId) {		
		//var url = '/api/fe/PaymentInstruction/SelectData/start/'+ start + '/limit/' + limit + '/filterData/' + filterData; // + '/uid/' + uid;
    var url = '/api/fe/OutgoingPaymentInstruction/SelectVendorAccount?paymentId=' + paymentId;
		console.log("getIncomingVendorBank :", url);
		var res=$http.get(url);  
		//return res;
		
/* 		var da_json=[
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 1",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 1",Nominal_Pembayaran: 1000},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 2",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 2",Nominal_Pembayaran: 1000},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 3",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 3",Nominal_Pembayaran: 1000},
			  ];
		res=da_json; */
		
        return res;
    };

    factory.getDataInstructionBasedOutgoing=function(start, limit, filterData, uid) {		
      //var url = '/api/fe/OutgoingPaymentSelectData/Start/' + start + '/limit/' + limit + '/filterData/' + filterData;
			 var url = '/api/fe/OutgoingPayment/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
      var res=$http.get(url);  
		
        return res;
    };	

		factory.getOutgoingPaymentMasterLihat=function(start, limit, id)
		{
      	var url = '/api/fe/OutgoingPayment/OutgoingPaymentId?OutgoingPaymentId=' + id;
				//var url = '/api/fe/OutgoingPayment?OutgoingPaymentId=' + id;
      	var res=$http.get(url);  
		
        return res;
		};

		factory.getOutgoingPaymentChargesLihat=function(start, limit, filterData)
		{
      	//var url = '/api/fe/OutgoingPaymentCharges/SelectData/start/' + start + '/limit/' + limit + '/filterData/' + filterData;
				var url = '/api/fe/OutgoingPaymentCharges/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
      	var res=$http.get(url);  
		
        return res;
		};
		factory.getOutgoingPaymentDetailLihat=function(start, limit, filterData)
		{
      	//var url = '/api/fe/OutgoingPaymentDetail/SelectData/start/' + start + '/limit/' + limit + '/filterData/' + filterData;
				var url = '/api/fe/OutgoingPaymentDetail/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
      	var res=$http.get(url);  
		
        return res;
		};

		factory.setOutgoingPaymentReversal=function(reverse)
		{
        var url = '/api/fe/OutgoingPaymentApproval/SubmitData/';
				//var url = '/api/fe/OutgoingPaymentApproval';

	  	  if (debugMode){console.log('Masuk ke reverse')};
	  	  if (debugMode){console.log('url :'+url);};
  		  if (debugMode){console.log('Parameter POST :'+ JSON.stringify(reverse)) };
      	var res=$http.post(url, JSON.stringify(reverse));
      	
    		return res;
		};

	factory.getData= function() {
		var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 1",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 1",Nominal_Pembayaran: 1000,Selected: false},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 2",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 2",Nominal_Pembayaran: 1000,Selected: false},
				{Nomor_Instruksi_Pembayaran: "Nomor_Instruksi_Pembayaran 3",Tanggal_Instruksi_Pembayaran: new Date("2/17/2017"),Nama_Pelanggan_Vendor: "nama 3",Nominal_Pembayaran: 1000,Selected: false},
			  ];
		res=da_json;
		
        return res;
      };	

	  factory.deleteUUID= function(uid) {
        return $http.delete('/api/fe/OutgoingPayment/uid/' + uid);
      };
	  
	  factory.PutTempPI=function(uid, type, id) {
		return $http.put('/api/fe/OutgoingPayment/uid/' + uid + '/type/' + type + '/id/' +id);  
	  };
	  
		factory.getOutgoingPaymentNewNumber=function(tanggal) {
			var filter = [{ValueDate: tanggal ,FormatTrxCode: 'OPA' }];
			//var url = '/api/fe/GenerateNumber/SelectData/start/0/limit/0/filterData/' + JSON.stringify(filter);
			var url = '/api/fe/GenerateNumber/SelectData?start=0&limit=0&filterData=' + JSON.stringify(filter);
			var res=$http.get(url);  
			return res;
		};

		factory.getDataBank=function() {
			//var url = '/api/fe/Bank/SelectData/start/0/limit/0/filterData/0';
			var url = '/api/fe/Bank/SelectData?start=0&limit=0&filterData=0';
			var res=$http.get(url);  
			return res;			
		};

	  factory.getDataBankAccount= function(id){
		//var url = '/api/fe/BankAccount/SelectData/start/0/limit/0/filterData/' + id;
		var url = '/api/fe/BankAccount/SelectData?start=0&limit=0&filterData=' + id;
		var res=$http.get(url);  
		return res;
	  };	  

		factory.CheckValidForReverse = function(id , outletid)
		{
			var url = '/api/fe/OutgoingPayment/CheckValidReverse?id=' + id + '&outletid=' + outletid;
			var res=$http.get(url);  
			return res;		
		}
	  
    factory.create= function(outgoingPayment) {
        var url = '/api/fe/OutgoingPayment/SubmitData/';
				//var url = '/api/fe/OutgoingPayment';

	  	  if (debugMode){console.log('Masuk ke submitData')};
	  	  if (debugMode){console.log('url :'+url);};
  		  if (debugMode){console.log('Parameter POST :'+ JSON.stringify(outgoingPayment)) };
      	var res=$http.post(url, JSON.stringify(outgoingPayment));
      	
    		return res;
    };
	  
      factory.update= function(outgoingPayment){
        return $http.put('/api/fw/Role', [{
                                            Id: outgoingPayment.Id,
                                            //pid: negotiation.pid,
                                            Name: outgoingPayment.Name,
                                            Description: outgoingPayment.Description}]);
      };
	  
	  
      factory.delete= function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      };
	  
	  return factory;
  });
  