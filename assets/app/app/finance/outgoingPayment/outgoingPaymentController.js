angular.module('app')
	.controller('OutgoingPaymentController', function ($scope, $http, $filter, CurrentUser, OutgoingPaymentFactory, DaftarTransaksiFactory, RincianPembayaranLihatFactory, $timeout, uiGridConstants, bsNotify) {
		$scope.ShowTambahOutgoingPayment = false;
		$scope.ShowLihatOutgoingPayment = false;
		$scope.ShowLihatOutgoingPayment_BiayaLainLain = false;
		$scope.Show_LihatOutgoingPayment_DaftarInstruksiPembayaran = false;
		$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = true;
		var uiGridPageSize = 25;
		$scope.OutgoingPaymentId = 0;
		$scope.currentUUID = "";
		$scope.user = CurrentUser.user();
		var ArrBankAcct = {};
		$scope.TotalNominal = 0;
		$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total = 0;
		
		$scope.MainOutgoingPayment_TanggalOutgoingPayment = new Date();
		$scope.MainOutgoingPayment_TanggalOutgoingPaymentEnd = new Date();
		$scope.maxDate = Date.now();
		//$scope.options_DaftarOutgoingPaymentSearch = [{ name: "Nomor Instruksi Penerimaan", value: "Nomor Instruksi Penerimaan" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" },{ name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" } ];
		var dateNow = new Date();
		$scope.maxDate = new Date(dateNow.getFullYear(),dateNow.getMonth(), dateNow.getDate());
		$scope.LihatOutgoingPayment_TipeInstruksiPembayaran = {};

		$scope.optionsLihatMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
		// $scope.optionsLihatOutgoingPayment_SearchDropdown = [{ name: "Nomor Outgoing Payment", value: "Nomor Outgoing Payment" },{ name: "Tipe Instruksi Pembayaran", value: "Tipe Instruksi Pembayaran" },{ name: "Nama Pelanggan/Vendor", value: "Nama Pelanggan/Vendor" },{ name: "Metode Pembayaran", value: "Metode Pembayaran" },{ name: "Nominal", value: "Nominal" }];
		// $scope.LihatOutgoingPayment_SearchDropdown={};

		$scope.optionsTambahOutgoingPayment_BiayaLainLain_DebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
		$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = {};
		$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = true;
		$scope.optionsMainOutgoingPayment_SearchDropdown = [{ name: "Nomor Outgoing Payment", value: "Nomor Outgoing Payment" }, { name: "Tipe Outgoing Payment", value: "Tipe Outgoing Payment" }, { name: "Nama Pelanggan/Vendor", value: "Nama Pelanggan/Vendor" }, { name: "Metode Pembayaran", value: "Metode Pembayaran" }, { name: "Nominal", value: "Nominal" }];
		$scope.MainOutgoingPayment_SearchDropdown = {};
		$scope.optionsMainOutgoingPayment_BulkAction_SearchDropdown = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
		$scope.MainOutgoingPayment_BulkAction_SearchDropdown = {};

		function checkRbyte(rb, b) {
			var p = rb & Math.pow(2, b);
			return ((p == Math.pow(2, b)));
		};

		function addCommas(nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		/*
		To use addSeparatorsNF, you need to pass it the following arguments: 
		nStr: The number to be formatted, as a string or number. No validation is done, so don't input a formatted number. If inD is something other than a period, then nStr must be passed in as a string. 
		inD: The decimal character for the input, such as '.' for the number 100.2 
		outD: The decimal character for the output, such as ',' for the number 100,2 
		sep: The separator character for the output, such as ',' for the number 1,000.2
		*/
		function addSeparatorsNF(nStr, inD, outD, sep) {
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1) {
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr)) {
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		$scope.givePattern = function (item, event) {
			console.log("item", item);
			console.log("event", event.which);
			console.log("event", event);
			if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105) ){
				event.preventDefault();
				console.log("disini", event.which);
				console.log("$scope.TambahOutgoingPayment_BiayaLainLain_Nominal", $scope.TambahOutgoingPayment_BiayaLainLain_Nominal);
				$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.replace(/[^0-9.]+/g,'');
				$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.replace(event.key,"");
				return false;
			} else {
				$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal;
				return;
			}
		};

		var rByte = JSON.parse($scope.tab.item).ByteEnable;
		$scope.allowApprove = checkRbyte(rByte, 4);

		console.log($scope.allowApprove);

		if ($scope.allowApprove == false) {
			$scope.optionsMainOutgoingPayment_BulkAction_SearchDropdown = [];
		}

		$scope.SetComboBranch = function () {
			var ho = 0;
			if ($scope.user.OrgCode.substring(3, 9) == '000000') {
				$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
				ho = $scope.user.OrgId;
				$scope.TambahOutgoingPayment_TipeInstruksiPembayaranOptions =
					[{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
					{ name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" },
					{ name: "Advance Payment", value: "Advance Payment" }, { name: "Cash Pick Up", value: "Cash Pick Up" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" },{ name: "Aksesoris Standard", value: "Aksesoris Standard" }];
				$scope.LihatOutgoingPayment_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
				{ name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" },
				{ name: "Advance Payment", value: "Advance Payment" }, { name: "Cash Pick Up", value: "Cash Pick Up" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" },{ name: "Aksesoris Standard", value: "Aksesoris Standard" }];
			}
			else {
				$scope.TambahOutgoingPayment_TipeInstruksiPembayaranOptions =
					[{ name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
					{ name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" },
					{ name: "Advance Payment", value: "Advance Payment" }, { name: "Cash Pick Up", value: "Cash Pick Up" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" },{ name: "Aksesoris Standard", value: "Aksesoris Standard" }];
				$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
				ho = 0;
				$scope.LihatOutgoingPayment_TipeInstruksiPembayaranOptions = [{ name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" }, { name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" }, { name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" }, { name: "General Transaction", value: "General Transaction" },
				{ name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" },

				{ name: "Advance Payment", value: "Advance Payment" }, { name: "Cash Pick Up", value: "Cash Pick Up" }, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" },{ name: "Aksesoris Standard", value: "Aksesoris Standard" }];
			}
			OutgoingPaymentFactory.getDataBranch(ho).then(
				function (res) {
					$scope.optionsTambahOutgoingPayment_RincianPembayaran_NamaBranch = res.data.Result;
					$scope.optionsTambahOutgoingPayment_NamaBranch = res.data.Result;
					$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch = $scope.user.OutletId;
					$scope.TambahOutgoingPayment_NamaBranch = $scope.user.OutletId;
					//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					/*
					if (CurrentUser.role == "Finance" ) 
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					else
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
					*/
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};


		$scope.SetComboBranch();
		//$scope.Show_LihatOutgoingPayment_DaftarOutgoingPayment=false;
		$scope.LihatOutgoingPayment_DaftarOutgoingPayment_Record = "";
		$scope.LihatOutgoingPayment_DaftarInstruksiPembayaran_ToRepeat = "";
		$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch = $scope.user.OrgId;

		$scope.ClearObjectTambahOutgoingPayment = function () {

			$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer = "";

			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer = {};

			$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BilyetGiro = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BilyetGiro = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NomorBilyetGiro_BilyetGiro = "";

			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data = [];
			$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = [];

			$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [];
			$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = [];
			$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = "";
			$scope.TambahOutgoingPayment_BiayaLainLain_Keterangan = "";

		};


		$scope.ToTambahOutgoingPayment = function () {
			//$scope.ShowLihatOutgoingPayment=false;
			$scope.TambahOutgoingPayment_NomorOutgoingPayment = '';
			$scope.Show_TambahOutgoingPayment_RincianPembayaran = true;
			$scope.Show_TambahOutgoingPayment_BiayaLainLain = true;
			$scope.ShowTambahOutgoingPayment = true;
			$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = false;
			$scope.TambahOutgoingPayment_TanggalOutgoingPayment = new Date();
			$scope.Show_TambahOutgoingPayment_DaftarInstruksiPembayaran = false;
			//$scope.Show_TambahOutgoingPayment_BiayaLainLain=false;
			//$scope.Show_TambahOutgoingPayment_BiayaLainLain_BiayaBungaDF = false;


			$scope.TambahOutgoingPayment_TipeInstruksiPembayaran = {};

			$scope.optionsTambahOutgoingPayment_InstruksiPembayaran_SearchDropdown = [{ name: "No Instruksi Pembayaran/Advance Payment ", value: "NoInstruksiAdv" }, { name: "Tanggal Instruksi Pembayaran", value: "Tanggal" }, { name: "Nama Pelanggan/Vendor", value: "NamaVendor" }, { name: "Nominal Pembayaran", value: "Nominal" }];
			$scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown = {};

			$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }]; //,{ name: "Bilyet Giro", value: "Bilyet Giro" }];
			$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = {};

			// OutgoingPaymentFactory.getDataBank().then(
			// 	function (res) {
			// 		//alert('isi data bank!');
			// 		$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransferOptions = [];
			// 		$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransferOptions = res.data.Result;
			// 	},
			// 	function (err) {
			// 		console.log('error -->', err)
			// 	}
			// );

			OutgoingPaymentFactory.getDataBankAccount(0).then(
				function (res) {
					$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransferOptions = res.data.Result;
					ArrBankAcct = res.data.Result;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
			$scope.ClearObjectTambahOutgoingPayment();
			$scope.ShowPengajuan = false;
		};

		$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer_Selected_Changed = function () {
			console.log('isi array -->', JSON.stringify(ArrBankAcct));
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPengirim_BankTransfer = "";
			for (var i = 0; i < ArrBankAcct.length; i++) {
				console.log(' ke --> ', ArrBankAcct[i].value)
				console.log(' isi combo -->', $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer);
				if (ArrBankAcct[i].value == $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer) {
					console.log('sama -->', ArrBankAcct[i].AccountName);
					$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPengirim_BankTransfer = ArrBankAcct[i].AccountName;
					$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = ArrBankAcct[i].BankName;

					break;
				}
				else {
					console.log('tidak sama !!!');
				}
			}
		};
		$scope.NominalTxt = 'Nominal'; //CR4 P2 Finance
		$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain_Selected_Changed = function () {//CR4 P2 Finance
			//alert('changee-->',$scope.TambahOutgoingPayment_TipeInstruksiPembayaran );
			if ( ($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain == "Biaya Bunga DF" ) && 
				( ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Unit" ) || ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Parts") )){
					$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = false;
					$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = "";
					$scope.NominalTxt = 'Nominal Pembayaran'
			}
			else {
				$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = true;
				$scope.NominalTxt = 'Nominal'
			}
		};

		$scope.right = function (str, chr) {
			return str.slice(str.length - chr, str.length);
		}

		$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed = function () {
			$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_BankTransfer = false;
			$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_Cash = false;
			$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_BilyetGiro = false;
			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer = {};
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPengirim_BankTransfer = "";


			if ($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
				$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_BankTransfer = true;
				if ($scope.ModalLihatOutgoingPayment != true) {
					if (
						($scope.right($scope.user.OrgCode, 6) == '000000' &&
							$scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Refund') ||
						$scope.TambahOutgoingPayment_TipeInstruksiPembayaran != 'Refund'
					) {
						// User HO AND Refund OR Not Refund
						$scope.TambahProposal_RekeningPengirim_Show = true;
						console.log("Show Rekening Pengirim: ", $scope.TambahProposal_RekeningPengirim_Show);
					}
					else {
						// User Non-HO AND Refund
						// $scope.TambahProposal_RekeningPengirim_Show = false;
						$scope.TambahProposal_RekeningPengirim_Show = true; // Waiting for user test.
						console.log("Show Rekening Pengirim: ", $scope.TambahProposal_RekeningPengirim_Show);
					}
				}
				if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Koreksi Administratif"){// CR4 Finance
					$scope.Show_TambahOutgoingPayment_BiayaLainLain = true;
					$scope.optionsTambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [{ name: "Biaya Bank", value: "Biaya Bank" }];
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = [];
				}
			}
			else if ($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
				$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_Cash = true;
				$scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_Cash = $scope.TambahOutgoingPayment_TanggalOutgoingPayment;
				if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Koreksi Administratif"){// CR4 Finance
					$scope.Show_TambahOutgoingPayment_BiayaLainLain = false;
					$scope.optionsTambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [{ name: "Biaya Bank", value: "Biaya Bank" }];
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = [];
				}
			}
			else if ($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bilyet Giro") {
				$scope.Show_TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_BilyetGiro = true;
			}
		};

		$scope.LihatOutgoingPayment_Back_Click = function () {
			console.log("nama branch >>>>>>>>>>>",$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch);
			$scope.ShowTambahOutgoingPayment = false;
			$scope.ModalLihatOutgoingPayment = false;
			$scope.Show_TambahOutgoingPayment_CashPickUp = false;
			$scope.Show_LihatOutgoingPayment_RincianPembayaran = false;
			$scope.ShowLihatOutgoingPayment_BiayaLainLain = false;
			$scope.Show_LihatOutgoingPayment_DaftarInstruksiPembayaran = false;
			$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = true;
			//$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash_EnableDisable = true;
			//	$scope.Show_LihatOutgoingPayment_RincianPembayaran = false;
			//	$scope.Show_TambahOutgoingPayment_RincianPembayaran_Detail = false;
		};

		$scope.BackToMainOutgoingPayment = function () {
			$scope.ShowTambahOutgoingPayment = false;
			//$scope.ShowLihatOutgoingPayment=true;
			$scope.ModalLihatOutgoingPayment = false;
			$scope.Show_TambahOutgoingPayment_CashPickUp = false;
			$scope.Show_LihatOutgoingPayment_RincianPembayaran = false;
			$scope.ShowLihatOutgoingPayment_BiayaLainLain = false;
			$scope.Show_LihatOutgoingPayment_DaftarInstruksiPembayaran = false;
			$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = true;
			//$scope.Show_LihatOutgoingPayment_RincianPembayaran = false;
			//$scope.Show_TambahOutgoingPayment_RincianPembayaran_Detail = false;
			$scope.OutgoingPaymentId = 0;
			$scope.currentUUID = "";
			$scope.SetComboBranch();
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total = 0;
			$scope.options_DaftarOutgoingPaymentSearch = [{ name: "Nomor Instruksi Penerimaan", value: "Nomor Instruksi Penerimaan" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" }, { name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" }];

			$scope.LihatOutgoingPayment_TipeInstruksiPembayaranOptions = [{ name: "Unit", value: "Unit" }, { name: "Aksesoris", value: "Aksesoris" }, { name: "Purna Jual", value: "Purna Jual" }, { name: "Karoseri", value: "Karoseri" }, { name: "STNK/BPKB", value: "STNK/BPKB" }, { name: "Order Pengurusan Dokumen", value: "Order Pengurusan Dokumen" }, { name: "Swapping", value: "Swapping" }, { name: "Ekspedisi", value: "Ekspedisi" },
			{ name: "Parts", value: "Parts" }, { name: "Order Pekerjaan Luar", value: "Order Pekerjaan Luar" },
			{ name: "Order Permintaan Bahan", value: "Order Permintaan Bahan" }, { name: "Refund", value: "Refund" },
			{ name: "General Transaction", value: "General Transaction" },
			{ name: "Accrued Free Service", value: "Accrued Free Service" }, { name: "Accrued Komisi Sales", value: "Accrued Komisi Sales" }, { name: "Accrued Subsidi DP/Rate", value: "Accrued Subsidi DP/Rate" },
			{ name: "Advance Payment", value: "Advance Payment" }, { name: "Cash Pick Up", value: "Cash Pick Up" }
				, { name: "Interbranch", value: "Interbranch" }, { name: "Koreksi Administratif", value: "Koreksi Administratif" },{ name: "Aksesoris Standard", value: "Aksesoris Standard" }];
			$scope.LihatOutgoingPayment_TipeInstruksiPembayaran = {};

			// $scope.optionsLihatOutgoingPayment_SearchDropdown = [{ name: "Nomor Outgoing Payment", value: "Nomor Outgoing Payment" },{ name: "Tipe Instruksi Pembayaran", value: "Tipe Instruksi Pembayaran" },{ name: "Nama Pelanggan/Vendor", value: "Nama Pelanggan/Vendor" },{ name: "Metode Pembayaran", value: "Metode Pembayaran" },{ name: "Nominal", value: "Nominal" }];
			// $scope.LihatOutgoingPayment_SearchDropdown={};

			//$scope.Show_LihatOutgoingPayment_DaftarOutgoingPayment=false;
			$scope.LihatOutgoingPayment_DaftarOutgoingPayment_Record = "";
			$scope.LihatOutgoingPayment_DaftarInstruksiPembayaran_ToRepeat = "";

			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer = {};
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPengirim_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPengirim_BankTransfer = "";


			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_Nama_BankTransfer = "";
			$scope.Show_TambahOutgoingPayment_RincianPembayaran = false;
			$scope.Show_TambahOutgoingPayment_BiayaLainLain = false;

			//$scope.SetDataMainOutgoingPaymentRequest(pagenumber, uiGridPageSize, startdate, endDate, branch);
		};

		$scope.SetDataIncomingInstruction = function (pagenumber, tipeInstruksi) {
			OutgoingPaymentFactory.getDataIncomingInstruction(pagenumber, uiGridPageSize, tipeInstruksi, $scope.currentUUID, $scope.TambahOutgoingPayment_NamaBranch)
				.then(
					function (res) {
						console.log('data --> ', JSON.stringify(res.data));
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
						//$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
						//$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.paginationPageSize = uiGridPageSize;
						//$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.paginationPageSizes = [uiGridPageSize];
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.TambahOutgoingPayment_Cari = function () {
			$scope.NominalTxt = 'Nominal' //CR4 P2 Finance
			//alert('carii - tambah ');
			/*
			if ($scope.currentUUID != "") {
				OutgoingPaymentFactory.deleteUUID($scope.currentUUID);
			}
			*/
			//	$scope.Show_LihatOutgoingPayment_RincianPembayaran = false;
			//$scope.Show_TambahOutgoingPayment_RincianPembayaran_Detail = true;
			$scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash = undefined;
			$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash = undefined;
			$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = 0;
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total = 0;

			if(($scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash == null || $scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash == undefined)
			 && ($scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash == "" || $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash == undefined)
			 && ($scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash == "" || $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash == undefined)){
				$scope.TambahOutgoingPayment_Simpan_Disabled = true;
				$scope.Keterangan_Outgoing_Simpan = true;
				$scope.Nominal_Pembayar = true;
			}
			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != "Cash Pick Up"){
				$scope.TambahOutgoingPayment_Simpan_Disabled = false;
				$scope.Keterangan_Outgoing_Simpan = false;
				$scope.Nominal_Pembayar = false;
			}

			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Parts" || $scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Unit"){
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.columnDefs[5].visible= true;
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.columnDefs[6].visible= false;
			}else{
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.columnDefs[5].visible= false;
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.columnDefs[6	].visible= true;
			}
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.columns[3].hideColumn();
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.columns[4].hideColumn();

			$scope.currentUUID = '12345';  // $scope.generateUUID();
			//console.log('uuid --> ', $scope.currentUUID);  
			$scope.optionsTambahOutgoingPayment_InstruksiPembayaran_SearchDropdown = [{ name: "No Instruksi Pembayaran/Advance Payment ", value: "NoInstruksiAdv" }, { name: "Tanggal Instruksi Pembayaran", value: "Tanggal" }, { name: "Nama Pelanggan/Vendor", value: "NamaVendor" }, { name: "Nominal Pembayaran", value: "Nominal" }];
			if (($scope.TambahOutgoingPayment_TipeInstruksiPembayaran.length > 0) && ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != "Cash Pick Up")) {
				if ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Interbranch") {
					$scope.Show_TambahOutgoingPayment_RincianPembayaran = true;
					$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash_EnableDisable = false;
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = true;
					$scope.Show_TambahOutgoingPayment_BiayaLainLain = false;
					$scope.Show_TambahOutgoingPayment_CashPickUp = false;
				}else if ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Koreksi Administratif") {
					$scope.Show_TambahOutgoingPayment_RincianPembayaran = true;
					$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash_EnableDisable = false;
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = true;
					// $scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = "Cash";
					$scope.Show_TambahOutgoingPayment_BiayaLainLain = false;
					$scope.Show_TambahOutgoingPayment_CashPickUp = false;
				}
				else {
					$scope.Show_TambahOutgoingPayment_RincianPembayaran = true;
					//alert('masuk 2');
					$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash_EnableDisable = false;
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = true;
					//$scope.Show_TambahOutgoingPayment_BiayaLainLain_BiayaBungaDF = false;
					$scope.Show_TambahOutgoingPayment_BiayaLainLain = true;
					$scope.Show_TambahOutgoingPayment_CashPickUp = false;
					if (($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != "Unit") && ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != "Parts")) {
						$scope.optionsTambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [{ name: "Biaya Bank", value: "Biaya Bank" }];
						//$scope.Show_TambahOutgoingPayment_BiayaLainLain_BiayaBungaDF = false;
					}
					else if ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Unit") {
						$scope.optionsTambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [{ name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Bunga DF", value: "Biaya Bunga DF" }];
						$scope.optionsTambahOutgoingPayment_InstruksiPembayaran_SearchDropdown = [{ name: "No Instruksi Pembayaran/Advance Payment ", value: "NoInstruksiAdv" }, { name: "Tanggal Instruksi Pembayaran", value: "Tanggal" }, { name: "Nama Pelanggan/Vendor", value: "NamaVendor" }, { name: "Nominal Pembayaran", value: "Nominal" }, { name: "Nomor Rangka", value: "NomorRangka" }];
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.columns[3].showColumn();
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.columns[3].visible = true;
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.columns[4].showColumn();
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.columns[4].visible = true;
			
						//$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.refresh();

					}
					else {
						$scope.optionsTambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [{ name: "Biaya Bank", value: "Biaya Bank" }, { name: "Biaya Bunga DF", value: "Biaya Bunga DF" }];
						//$scope.Show_TambahOutgoingPayment_BiayaLainLain_BiayaBungaDF = true;
					}
					$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = {};
				}
				$scope.SetDataIncomingInstruction(1, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
				//$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_ToRepeat=RincianPembayaranLihatFactory.getDataIncomingInstruction();
				$scope.Show_TambahOutgoingPayment_DaftarInstruksiPembayaran = true;
				/*
				var Dummy_DaftarInstruksiPembayaranYangAkanDibayar=[{ Nomor_Instruksi_Pembayaran: null,Tanggal_Instruksi_Pembayaran: null,Nama_Pelanggan_Vendor: null,Nominal_Pembayaran: null}];
				Dummy_DaftarInstruksiPembayaranYangAkanDibayar.splice(0, 1);
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat=Dummy_DaftarInstruksiPembayaranYangAkanDibayar;
				*/
				//nanti bikin masing punya tabel atas pake get

			}
			else if (($scope.TambahOutgoingPayment_TipeInstruksiPembayaran.length > 0) && ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up")) {
				$scope.optionsTambahOutgoingPayment_InstruksiPembayaran_SearchDropdown = [{ name: "Tanggal Instruksi Pembayaran", value: "Tanggal" }, { name: "Nama Pelanggan/Vendor", value: "NamaVendor" }, { name: "Nominal Pembayaran", value: "Nominal" }, { name: "Nomor Rangka", value: "NomorRangka" }];
				$scope.Show_TambahOutgoingPayment_RincianPembayaran = true;
				$scope.Show_TambahOutgoingPayment_DaftarInstruksiPembayaran = false;
				$scope.Show_TambahOutgoingPayment_BiayaLainLain_BiayaBungaDF = false;
				$scope.Show_TambahOutgoingPayment_BiayaLainLain = false;
				$scope.Show_TambahOutgoingPayment_CashPickUp = true;
				$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = false;
				$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = "Cash";
				//alert('masuk 3');
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.columns[3].showColumn();
				$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash_EnableDisable = true;

				$scope.TambahOutgoingPayment_Simpan_Disabled = true;
				$scope.Keterangan_Outgoing_Simpan = true;
				$scope.Nominal_Pembayar = true;

				//$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash_EnableDisable = false;	
				// OutgoingPaymentFactory.getDataBank().then(
				// 	function(res) {
				// 		$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransferOptions = res.data.Result;
				// 	},
				// 	function (err) {
				// 		console.log('error -->', err)
				// 	}
				// );

				OutgoingPaymentFactory.getDataBankAccount($scope.user.OrgId).then(
					function (res) {
						$scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_CashOptions = res.data.Result;
						ArrBankAcct = res.data.Result;
					},
					function (err) {
						console.log('error -->', err)
					}
				);

			}

			//20170524, nuse, end
			else {
				//alert('select type first');
				bsNotify.show({
					title: "Warning",
					content: "Pilih tipe instruksi pembayaran!",
					type: 'warning'
				});
			}
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.refresh();
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.refresh();

			//CR 4 Phase 2
			$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = []
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data = [];
			//End CR 4 Phase 2
		};

		$scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash_Selected_Changed = function () {
			$scope.TambahOutgoingPayment_NamaPemegangRekening = "";
			$scope.TambahOutgoingPayment_NamaBankPenerima = "";
			for (var i = 0; i < ArrBankAcct.length; i++) {

				if (ArrBankAcct[i].name == $scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash) {
					$scope.TambahOutgoingPayment_NamaBankPenerima = ArrBankAcct[i].BankName;
					$scope.TambahOutgoingPayment_NamaPemegangRekening = ArrBankAcct[i].AccountName;
					break;
				}
			}
		};


		$scope.LihatOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash_Selected_Changed = function () {
			$scope.LihatOutgoingPayment_NamaBankPenerima = "";
			$scope.LihatOutgoingPayment_NamaPemegangRekening = "";
			for (var i = 0; i < ArrBankAcct.length; i++) {

				if (ArrBankAcct[i].name == $scope.LihatOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash) {
					$scope.LihatOutgoingPayment_NamaBankPenerima = ArrBankAcct[i].BankName;
					$scope.LihatOutgoingPayment_NamaPemegangRekening = ArrBankAcct[i].AccountName;
					break;
				}
			}
		};

		$scope.ProsesSimpan = false;
		$scope.TambahOutgoingPayment_Simpan = function () {
			var dataSimpan = {};
			var nominal = 0;
			var deskripsi = "";
			var noRekPenerima = 0;
			var noRekPengirim = 0;
			var BilyetGiro = "";
			var ReceiverBankId = "";
			var ReceiverBankName = "";
			var paymentdate;
			$scope.ProsesSimpan = true;
			// if ($scope.TambahOutgoingPayment_NomorOutgoingPayment == '') {
			// 	OutgoingPaymentFactory.getOutgoingPaymentNewNumber($scope.TambahOutgoingPayment_TanggalOutgoingPayment).then(
			// 		function (res) {
			// 			$scope.TambahOutgoingPayment_NomorOutgoingPayment = res.data.Result[0].Number;
			// 		},
			// 		function (err) {
			// 			console.log('error -->', err)
			// 		}
			// 	);
			// }

			if (($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != "Cash Pick Up") &&
				($scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.length > 0)) {
				if ($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") {
					nominal = $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer;
					deskripsi = $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BankTransfer;
					noRekPenerima = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer;
					if ($scope.TambahProposal_RekeningPengirim_Show == true) {
						noRekPengirim = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer;
					}
					else {
						noRekPengirim = null;
					}
					ReceiverBankId = $scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer;
					ReceiverBankName = $scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer;

					$scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer = $filter('date')(new Date($scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer), 'yyyy-MM-dd');
					paymentdate = $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer;
				}
				else if ($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Cash") {
					if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Order Pekerjaan Luar"){
						nominal = $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash.replace(/\,/g,"");
					}else{
						nominal = $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash;						
					}
					deskripsi = $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash;
					ReceiverBankId = "0";
					ReceiverBankName = "";
					$scope.TambahOutgoingPayment_TanggalOutgoingPayment = $filter('date')(new Date($scope.TambahOutgoingPayment_TanggalOutgoingPayment), "yyyy-MM-dd H:mm:ss");
					paymentdate = $scope.TambahOutgoingPayment_TanggalOutgoingPayment;
					console.log("cek payment date cash >>>>>>>>>>>>>>>>>>>",paymentdate);
					//noRekPenerima = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer;
					//noRekPengirim = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer;
				}
				else //Bilyet Giro
				{
					nominal = $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BilyetGiro;
					deskripsi = $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BilyetGiro;
					//noRekPenerima = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer;
					//noRekPengirim = $scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer;
					BilyetGiro = $scope.TambahOutgoingPayment_RincianPembayaran_NomorBilyetGiro_BilyetGiro;
					paymentdate = $scope.TambahOutgoingPayment_TanggalOutgoingPayment;
					//tanggal.setMinutes(tanggal.getMinutes() + 420);
				}

				//noRekPengirim = $scope.TambahInstruksiPenerimaan_NomorRekening ;
				//console.log('isi rek' , $scope.TambahInstruksiPenerimaan_NomorRekening );
				if (
					($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") &&
					(
						(noRekPengirim == null) ||
						(noRekPengirim == '') ||
						(noRekPengirim == undefined) ||
						(angular.isUndefined(noRekPengirim)) ||
						(noRekPengirim == []) ||
						(noRekPengirim == {}) ||
						(!angular.isNumber(noRekPengirim))
					) &&
					($scope.TambahProposal_RekeningPengirim_Show == true)
				) {
					//alert('Nomor Rekening Pengirim harus diisi dulu ');
					bsNotify.show({
						title: "Warning",
						content: "Nomor Rekening Pengirim harus diisi.",
						type: 'warning'
					});
					$scope.ProsesSimpan = false;
				}else if(($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer") && 
						($scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer == null || $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer == undefined ||
						$scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer == "" || $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer == "Invalid Date")){
							bsNotify.show({
								title: "Warning",
								content: "Tanggal Pembayaran harus diisi.",
								type: 'warning'
							});
							$scope.ProsesSimpan = false;
				}
				else if ((deskripsi == null) ||
					(deskripsi == '') ||
					(deskripsi == undefined) ||
					(angular.isUndefined(deskripsi))) {
					//alert('Keterangan harus diisi ');
					bsNotify.show({
						title: "Warning",
						content: "Keterangan harus diisi.",
						type: 'warning'
					});
					$scope.ProsesSimpan = false;
				}

				else {
					//var tanggal = new Date( res.data[0].OutgoingPaymentDate);
					console.log('minus', nominal);
					if (nominal.substring(0, 1) == "-") {
						console.log('nominal minus');
						//alert('Nomor Rekening Pengirim harus diisi dulu ');
						bsNotify.show({
							title: "Warning",
							content: "Nominal Pembayaran Kurang dari 0.",
							type: 'warning'
						});
					$scope.ProsesSimpan = false;
					}
					else {
						// paymentdate.setMinutes(paymentdate.getMinutes() + 420);
						// $scope.paymentdate = $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer;
						console.log("cek payment date cash >>>>>>>>>>>>>>>>>>>",paymentdate);						
						dataSimpan = [{
							OutgoingType: $scope.TambahOutgoingPayment_TipeInstruksiPembayaran,
							OutgoingPaymentDate: $scope.TambahOutgoingPayment_TanggalOutgoingPayment,
							PaymentDate: paymentdate,
							PaymentMethod: $scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran,
							Nominal: nominal,
							Currency: "IDR",
							Description: deskripsi,
							SenderAccNumber: noRekPengirim,
							ReceiverAccNumber: noRekPenerima,
							ReceiverBankId: ReceiverBankId,
							ReceiverName: ReceiverBankName,
							outletId: $scope.user.OutletId,
							//				BilyetGiro :BilyetGiro,
							Grid: JSON.stringify($scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data),
							ChargesGrid: JSON.stringify($scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data),
							OutgoingPaymentNumber: $scope.TambahOutgoingPayment_NomorOutgoingPayment
						}];
						console.log('masuk sini -->', JSON.stringify(dataSimpan));

						//pengajuan backdate
						if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up" && $scope.ShowPengajuan){
							dataSimpan[0].BackdateReason = $scope.LihatOutgoingPayment_PengajuanBackDate_AlasanBackDate;
							dataSimpan[0].PaymentDate = $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_Cash;
						} 

						OutgoingPaymentFactory.create(dataSimpan).then(
							function (res) {
								//$scope.loading=false;
								//alert('Data berhasil disimpan');
								bsNotify.show({
									title: "Success",
									content: "Data berhasil disimpan.",
									type: 'success'
								});
							$scope.ProsesSimpan = false;
								$scope.MainOutgoingPayment_Cari();
								$scope.BackToMainOutgoingPayment();
								console.log(res);
								$scope.LihatOutgoingPayment_PengajuanBackDate_AlasanBackDate = null;
							},
							function (err) {
								//alert(' Data gagal disimpan ');
								bsNotify.show({
									title: "Gagal",
									content: "Data gagal disimpan.",
									type: 'danger'
								});
							$scope.ProsesSimpan = false;
								console.log("err=>", err);
								//console.log("err=>", err);
								if (err != null) {
									//alert(err.data.Message);
									bsNotify.show({
										title: "Error",
										content: err.data.Message,
										type: 'danger'
									});
								$scope.ProsesSimpan = false;
								}
							}
						);
					}
				}
			}
			else if ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up") {
				paymentdate = $scope.TambahOutgoingPayment_TanggalOutgoingPayment;
				paymentdate.setMinutes(paymentdate.getMinutes() + 420);
				nominal = $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash;
				deskripsi = $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash;
				noRekPenerima = $scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash;
				noRekPengirim = 0;

				dataSimpan = [{
					OutgoingType: $scope.TambahOutgoingPayment_TipeInstruksiPembayaran,
					OutgoingPaymentDate: $scope.TambahOutgoingPayment_TanggalOutgoingPayment,
					PaymentMethod: $scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran,
					Nominal: nominal,
					Currency: "IDR",
					Description: deskripsi,
					SenderAccNumber: noRekPengirim,
					ReceiverAccNumber: noRekPenerima,
					outletId: $scope.user.OutletId,
					PaymentDate: paymentdate,
					//				BilyetGiro :BilyetGiro,
					// Grid:"",
					// ChargesGrid : "",
					OutgoingPaymentNumber: $scope.TambahOutgoingPayment_NomorOutgoingPayment
				}];
				
				if(($scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash == null && $scope.TambahOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash == undefined) || 
					($scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash == "" || $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash == null || $scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash == undefined)){
					bsNotify.show({
						title: "warning",
						content: "Data Harus Di isi Lengkap",
						type: 'warning'
					});
					$scope.ProsesSimpan = false;
				}else{
						console.log('masuk sini -->', JSON.stringify(dataSimpan));

						//pengajuan backdate
						if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up" && $scope.ShowPengajuan){
							dataSimpan[0].BackdateReason = $scope.LihatOutgoingPayment_PengajuanBackDate_AlasanBackDate;
							dataSimpan[0].PaymentDate = $scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_Cash;
						} 

						OutgoingPaymentFactory.create(dataSimpan).then(
						function (res) {
							//$scope.loading=false;
							//alert('Data berhasil disimpan');
							bsNotify.show({
								title: "Success",
								content: "Data berhasil disimpan.",
								type: 'success'
							});
							$scope.ProsesSimpan = false;
							$scope.MainOutgoingPayment_Cari();
							$scope.BackToMainOutgoingPayment();
							console.log(res);
							$scope.LihatOutgoingPayment_PengajuanBackDate_AlasanBackDate = null;
						},
						function (err) {
							console.log("err=>", err);

							if (err != null) {
								//alert(err.data.Message);
								bsNotify.show({
									title: "Error",
									content: err.data.Message,
									type: 'danger'
								});
							$scope.ProsesSimpan = false;
							}
							else {
								//alert('Data gagal disimpan !');
								bsNotify.show({
									title: "Gagal",
									content: "Data gagal disimpan.",
									type: 'danger'
								});
								$scope.ProsesSimpan = false;
							}
						}
					);
				}
			}
			else {
				//alert("Semua data harus diisi !");
				bsNotify.show({
					title: "Warning",
					content: "Semua data harus diisi !",
					type: 'warning'
				});
				$scope.ProsesSimpan = false;
			}
		};

		$scope.fixedNominal = function (n, p) {
            // n adalah angka dengan desimalnya
            // p adalah jumlah digit dibelakang koma yg di inginkan
            return (+(Math.round(+(n + 'e' + p)) + 'e' + -p)).toFixed(p);
        }
		
		$scope.CalculateNominalPembayaran = function () {
			var TotalCharges = 0;
			var totNominal = 0;
			var tot1 = 0;

			$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.forEach(function (row) {
				if (row.DebitCredit == 'D' || row.DebitCredit == 'Debet') {
					TotalCharges += parseFloat(row.Nominal);
				}
				else {
					TotalCharges -= parseFloat(row.Nominal).toFixed(2);
				}
			});
			tot1 = parseFloat(parseFloat($scope.TotalNominal).toFixed(2));
			$scope.fixedNominal($scope.TotalNominal, 2);
			var tot2 =parseFloat($scope.fixedNominal($scope.TotalNominal, 2));
			console.log("cek nilai bbb>>>>>>>>", tot2);
			console.log(' isi total nominal', $scope.TotalNominal);
			console.log(' isi tot1', tot1);
			console.log(' isi total Charges', $scope.TotalCharges);
			// totNominal = tot1 + parseFloat(TotalCharges);
			if($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain == 'Biaya Bunga DF'){
				totNominal = parseFloat(TotalCharges);	
			}else{
				totNominal = tot2 + parseFloat(TotalCharges);
			}
			
			$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = addSeparatorsNF(totNominal.toFixed(2), '.', '.', ','); //$scope.TotalNominal + TotalCharges;
			console.log('isi tot', totNominal);
				$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = // parseFloat($scope.TotalNominal) + parseFloat(TotalCharges);
					addSeparatorsNF(totNominal.toFixed(2), '.', '.', ',');
			console.log('isi nominal', $scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
			console.log(' isi total nominal', $scope.TotalNominal);
			//console.log('tot nominal', addSeparatorsNF(totNominal, '.','.',','));
			//$scope.addCommas($scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
		}

		// Baru Ditambah 
		$scope.TambahOutgoingPayment_BiayaLainLain_NominalBlur = function () {
			console.log("test",$scope.TambahOutgoingPayment_BiayaLainLain_Nominal);
			console.log("test",parseFloat($scope.TambahOutgoingPayment_BiayaLainLain_Nominal).toFixed(2));
			var SisaFloat = "";
			if ($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-3,1) == "."){
				SisaFloat = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-3);
				$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr(0,$scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahOutgoingPayment_BiayaLainLain_Nominal);
			}
			else if ($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-2,1) == "."){
				SisaFloat = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr($scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-2);
				$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.substr(0,$scope.TambahOutgoingPayment_BiayaLainLain_Nominal.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahOutgoingPayment_BiayaLainLain_Nominal);
			}
			var val = $scope.TambahOutgoingPayment_BiayaLainLain_Nominal.split(".").join("");
			console.log("cari penyebab",val);
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");
	
			var str_array = val.split('.');
	
			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}
	
				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}
	
			if (!NominalValid) {
				$scope.TambahOutgoingPayment_BiayaLainLain_NominalBlur(val);
			}
			else {
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = val+".00";
					$scope.TambahOutgoingPayment_BiayaLainLain_NominalforParseFloat = val.split(".").join("")+".00";
				}
				else{
					$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = val+SisaFloat;
					$scope.TambahOutgoingPayment_BiayaLainLain_NominalforParseFloat = val.split(".").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahOutgoingPayment_BiayaLainLain_NominalforParseFloat);
				}
			}
		}

		$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranTambah = function () {
			console.log($scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Parts' || $scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Unit'){
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.columnDefs[7].visible = true;
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.columnDefs[8].visible = false;
			}else{
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.columnDefs[7].visible = false;
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.columnDefs[8].visible = true;
			}

			if ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran != 'Refund') {
				$scope.Show_TambahRekeningPenerimaAll = true;
			} else { $scope.Show_TambahRekeningPenerimaAll = false; }
			if ($scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedCount() > 0) {
				//console.log('count -->', $scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedCount());
				var MetodBayar = "";
				var found = 0;	
				var bedaMetod = 0;
				var beda_namaPelanggan = 0;

				var selected_Data = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedRows();


				// berikut loop untuk membandingkan data yg di pilih, hrs sama nama pelanggan nya & method pembayarannya -------------------------------- start
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedRows().forEach(function (row) {
					// Cek Metode Pembayaran Beda
					MetodBayar = row.PaymentMethod;
					console.log('parah', $scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedRows())
					$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.getSelectedRows().forEach(function (Xrow) {
						// if(MetodBayar == ""){
						// 	MetodBayar = row.PaymentMethod;
						// }else{
							if(MetodBayar != Xrow.PaymentMethod){
								bedaMetod = 1;
							}
						// }

						if (row.VendorName != Xrow.VendorName) {
							beda_namaPelanggan = 1;
						}
					})
					
				});
				// berikut loop untuk membandingkan data yg di pilih, hrs sama nama pelanggan nya & method pembayarannya -------------------------------- end



				if (bedaMetod == 0 && beda_namaPelanggan == 0){
					// $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.forEach(function (obj) {

					// berikut loop untuk cek membandingkan data yang di pilih, dengan data yang sudah ada di tabel bawah (yang akan di bayar) --------------------------------------------- start
					var unit_sudah_ada = 0;
					var lainnya_sudah_ada = 0;
					var bedaMetod2 = 0;
					var beda_namaPelanggan2 = 0;


						for (var i=0; i<$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.length; i++){
							var obj = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data[i];
							for (var j=0; j < selected_Data.length; j++) {
								var row = selected_Data[j];

								if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Unit"){
									if (row.PaymentId == obj.PaymentId && row.FrameNo == obj.FrameNo) {
										unit_sudah_ada = 1
									}
								} else {
									if (row.PaymentId == obj.PaymentId && row.DataType == obj.DataType) {
										lainnya_sudah_ada = 1
									}
								}

								if (obj.PaymentMethod != row.PaymentMethod) {
									bedaMetod2 = 1;
								}
								if (obj.VendorName != row.VendorName) {
									beda_namaPelanggan2 = 1;
								}
							}

							
						}
					// berikut loop untuk cek membandingkan data yang di pilih, dengan data yang sudah ada di tabel bawah (yang akan di bayar) --------------------------------------------- end

					if (bedaMetod2 == 0 && beda_namaPelanggan2 == 0) {
						if (unit_sudah_ada == 0 && lainnya_sudah_ada == 0) {

							for (var j=0; j < selected_Data.length; j++) {
								var row = selected_Data[j];
	
								// console.log("found: ", found)
							$scope.optionsMetodePembayaran = [{ name: "Bank Transfer", value: "Bank Transfer" }, { name: "Cash", value: "Cash" }];
							$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = row.PaymentMethod;
							if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Koreksi Administratif"){ //CR$ p2 Finance
								$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = true;
							}else{
								$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = false;
							}
							if (row.PaymentMethod == "Bank Transfer") {
								$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BankTransfer = row.Description;								
								$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain_EnableDisable = true;
								$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = true;
								$scope.TambahOutgoingPayment_BiayaLainLain_Nominal_EnableDisable = true;
	
							}
							else if (row.PaymentMethod == "Cash") {
								$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash = row.Description;
								$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain_EnableDisable = false;
								$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = false;
								$scope.TambahOutgoingPayment_BiayaLainLain_Nominal_EnableDisable = false;
	
							}
	
							$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total += row.Nominal;
							$scope.TotalNominal = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total;
							$scope.CalculateNominalPembayaran();
							$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.push({
								$$hashKey: row.$$hashKey,
								Id: 0,
								PaymentId: row.PaymentId,
								InstructionDate: row.PaymentInstructionDate,
								FrameNo: row.FrameNo,
								VendorName: row.VendorName,
								PaymentMethod: row.PaymentMethod,
								Nominal: row.Nominal,
								DataType: row.DataType,
								BillingNo: row.BillingNo,
								VendorId: row.VendorId,
								SOId: row.SOId,
								AccountTo: row.AccountTo,
								BankToId: row.BankToId,
								BankToName: row.BankToName,
								VendorNameOrDF: row.VendorNameOrDF
							});
	
							OutgoingPaymentFactory.getIncomingVendorBank(row.PaymentId).then(
								function (resVendor) {
									//$scope.loading=false;
									//alert('test sini');
									if ($scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.length == 0) {
										$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = "";
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = "";
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = "";
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_Nama_BankTransfer = "";
									}
									console.log(resVendor);
									if (resVendor.data.Result.length > 0) {
										//alert('data bank ada');
										if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Refund"){
											$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = row.AccountTo;
											$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = row.VendorName;
											$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = row.BankToId;
											$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_Nama_BankTransfer = row.BankToName;	
										}else{
											// $scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransferOptions.push(resVendor.data.Result);
											$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = resVendor.data.Result[0].AccountNumber;
											$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = resVendor.data.Result[0].AccountName;
											$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = resVendor.data.Result[0].BankId.toString();
										}
									}
									else {
										$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = row.AccountTo;
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = row.VendorName;
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = row.BankToId;
										$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_Nama_BankTransfer = row.BankToName;
									}
									//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = "25";        
								},
								function (err) {
									console.log("err=>", err);
								}
							);
							}
							
	
						} else {
							bsNotify.show({
								title:"Warning",
								content:"Data yang di pilih sudah ada.",
								type:"warning"
							});
						}

					}
					
				}

				if (beda_namaPelanggan == 1 || beda_namaPelanggan2 == 1 || bedaMetod == 1 || bedaMetod2 == 1) {
					bsNotify.show({
						title:"Warning",
						content:"Nama Pelanggan/Vendor/Penerima harus sama dan metode pembayaran harus sama.",
						type:"warning"
					});
				}
				
				// if (bedaMetod == 1 || bedaMetod2 == 1){
				// 	bsNotify.show({
				// 		title:"Warning",
				// 		content:"Nama Pelanggan/Vendor/Penerima harus sama dan metode pembayaran harus sama.",
				// 		type:"warning"
				// 	});
				// }
				if(found == 1){
					bsNotify.show({
						title:"Warning",
						content:"Nama Pelanggan/Vendor/Penerima harus sama dan metode pembayaran harus sama.",
						type:"warning"
					});
				}
				
				// cr4 p2 koreksi start 
				if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Koreksi Administratif'){
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = false;
				}else{
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_EnableDisable = true;
				}
				// cr4 p2 koreksi end 

				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.selection.clearSelectedRows();
			}
		};

		$scope.generateUUID = function () {
			var d = new Date().getTime();
			if (window.performance && typeof window.performance.now === "function") {
				d += performance.now();; //use high-precision timer if available
			}
			var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				var r = (d + Math.random() * 16) % 16 | 0;
				d = Math.floor(d / 16);
				return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
			});
			return uuid;
		};

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.LihatOutgoingPayment_DaftarBiayaLainLain_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			enableFiltering: true,
			columnDefs: [
				{ name: "Id", displayName: "Nomor Dibayar", field: "OPChargesId", visible: false },
				{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "OtherChargesType", enableCellEdit: false, enableHiding: false },
				{ name: "DebetCredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "Description", displayName: "Deskripsi", field: "Description", enableCellEdit: false, visible: false }
			],
			/*
					public int OPChargesId { get; set; }
				public string OtherChargesType { get; set; }
				public float Nominal { get; set; }
				public string Currency { get; set; }
				public string Description { get; set; }
				public string DebitCredit {get;set;}
				public int TotalData { get; set; }*/

			onRegisterApi: function (gridApi) {
				$scope.TambahOutgoingPayment_DaftarBiayaLainLain_gridAPI = gridApi;

				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {	
				//$scope.SetDataIncomingInstruction(pageNumber, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
				//console.log("masuk setting grid upload dan tambah");
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//}); 
			}
		};

		$scope.LihatOutgoingPayment_DaftarRincianPembayaran_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			multiselect: false,
			canSelectRows: false,
			enableFiltering: true,
			columnDefs: [
				{ name: "Id", displayName: "Nomor Dibayar", field: "Id", visible: false },
				{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "OtherChargesType", enableCellEdit: false, enableHiding: false },
				{ name: "DebetCredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' },
				{ name: "Description", displayName: "Deskripsi", field: "Description", enableCellEdit: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.LihatOutgoingPayment_DaftarRincianPembayaran_gridAPI = gridApi;
			}
		};

		$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			enableFiltering: true,
			columnDefs: [
				{ name: "Id", displayName: "Nomor Dibayar", field: "ChargesId", visible: false },
				{ name: "ChargesType", displayName: "Tipe Biaya Lain Lain", field: "OtherChargesType", enableCellEdit: false, enableHiding: false },
				{ name: "DebetCredit", displayName: "Debet / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' },
				{ name: "Description", displayName: "Deskripsi", field: "Description", enableCellEdit: false, visible: false },
				{
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarBiayaLainLain(row)">hapus</a>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahOutgoingPayment_DaftarBiayaLainLain_gridAPI = gridApi;

				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {	
				//$scope.SetDataIncomingInstruction(pageNumber, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
				//console.log("masuk setting grid upload dan tambah");
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//}); 
			}
		};

		$scope.ShowLihatDetailOutgoingPaymentFromMain = function (row) {
			//angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal('hide');
			console.log("Lihat detail: ", row.entity);
			var nominal = 0;
			$scope.ModalLihatOutgoingPayment = true;
			$scope.ShowLihatOutgoingPayment_BiayaLainLain = true;
			//$scope.Show_LihatOutgoingPayment_DaftarInstruksiPembayaran=true;
			$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = false;
			$scope.OutgoingPaymentId = row.entity.DocumentId;

			//ApprovalStatusDesc == "Diajukan" && ApprovalTypeDesc == "Reversal"
			$scope.LihatOutgoingPayment_NomorOutgoingPaymentReversal = "";
			
			if($scope.LihatOutgoingPayment_TipeInstruksiPembayaran == "Parts" || $scope.LihatOutgoingPayment_TipeInstruksiPembayaran == "Unit" ){
				$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.columnDefs[6].visible = true;
				$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.columnDefs[7].visible = false;
			}else{
				$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.columnDefs[6].visible = false;
				$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.columnDefs[7].visible = true;
			}

			$scope.LihatOutgoingPayment_NomorOutgoingPaymentReversal = row.entity.OutgoingPaymentRevNumber;
			OutgoingPaymentFactory.getOutgoingPaymentMasterLihat(1, uiGridPageSize, row.entity.DocumentId).then(
				function (res) {
					//	console.log('si ', JSON.stringify(res));
					console.log("Master lihat: ", res.data);
					$scope.LihatOutgoingPayment_NomorOutgoingPayment = res.data[0].OutgoingPaymentNumber;
					$scope.Hide_LihatOutgoingPayment_Pembatalan = false;
					if ($scope.LihatOutgoingPayment_NomorOutgoingPaymentReversal) {
						$scope.Hide_LihatOutgoingPayment_Pembatalan = true;
					}

					if (row.entity.ApprovalStatusDesc == "Diajukan" && row.entity.ApprovalTypeDesc == "Reversal") {
						$scope.Hide_LihatOutgoingPayment_Pembatalan = true;
					}
					if (row.entity.ApprovalStatusDesc == "Diajukan" && row.entity.ApprovalTypeDesc == "Backdate") {
						$scope.Hide_LihatOutgoingPayment_Pembatalan = true;
					}
					OutgoingPaymentFactory.getDataBranch($scope.user.OrgId).then(
						function (resBR) {
							$scope.optionsLihatOutgoingPayment_NamaBranch = resBR.data.Result;
						},
						function (errBr) {
							console.log('error -->', errBr)
						}
					);
					$scope.LihatOutgoingPayment_NamaBranch = res.data[0].outletId;
					$scope.LihatOutgoingPayment_TipeInstruksiPembayaran = res.data[0].OutgoingType;
					var tanggal = new Date(res.data[0].OutgoingPaymentDate);
					var tanggalPayment = new Date(res.data[0].PaymentDate);
					//tanggal.setMinutes(tanggal.getMinutes() + 420);
					$scope.LihatOutgoingPayment_TanggalOutgoingPayment = tanggal;
					$scope.Hide_LihatRincianPembayaran = false;
					$scope.Hide_LihatRincianPembayaran_cashPickUp = true;
					$scope.hide_LihatOutgoingPayment_CashPickUp = true;
					$scope.LihatOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.data = res.data;
					$scope.TambahOutgoingPayment_NomorOutgoingPayment = res.data[0].OutgoingPaymentNumber;
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = res.data[0].PaymentMethod;
					$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran_Selected_Changed();
					$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_Cash = res.data[0].Description;
					nominal = res.data[0].Nominal;
					if (res.data[0].PaymentMethod == "Cash") {
						//$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = res.data[0].Nominal;	
						$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = addSeparatorsNF(nominal.toFixed(2), '.', ',', '.');
						$scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_Cash = tanggalPayment;
					}
					else {
						var ReceiverBankId = res.data[0].ReceiverBankId;
						if (!(ReceiverBankId == null || ReceiverBankId == undefined)) {
							$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = ReceiverBankId.toString();
						}
						else {
							$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = "";
						}
						//$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = res.data[0].Nominal;
						$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer = addSeparatorsNF(nominal.toFixed(2), '.', ',', '.');
						$scope.TambahOutgoingPayment_RincianPembayaran_Keterangan_BankTransfer = res.data[0].Description;
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = res.data[0].ReceiverName;
						$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = res.data[0].ReceiverAccNumber;
						$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer = res.data[0].SenderAccNumber;
						$scopeTambahOutgoingPayment_RincianPembayaran_PaymentDate_BankTransfer = tanggalPayment;
						$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer_Selected_Changed();
					}

					console.log('Lihat Outgoing Payment: Tipe Instruksi -->', $scope.LihatOutgoingPayment_TipeInstruksiPembayaran);
					if ($scope.LihatOutgoingPayment_TipeInstruksiPembayaran != "Cash Pick Up") {
						$scope.Hide_LihatOutgoingPayment_DaftarInstruksiPembayaran = false;
						$scope.Hide_LihatOutgoingPayment_DaftarBiayaLainLain = false;
						OutgoingPaymentFactory.getOutgoingPaymentDetailLihat(1, uiGridPageSize, row.entity.DocumentId).then(
							function (res) {
								console.log(' data --> instruksi -- ', JSON.stringify(res.data));
								$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.data = res.data.Result;

							},
							function (err) {
								console.log('error -->', err);
							}
						);

						OutgoingPaymentFactory.getOutgoingPaymentChargesLihat(1, uiGridPageSize, row.entity.DocumentId).then(
							function (res) {
								//console.log(' data --> charges -- ', JSON.stringify(res.data));
								$scope.LihatOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = res.data.Result;

							},
							function (err) {
								console.log('error -->', err);
							}
						);
					}
					else {
						OutgoingPaymentFactory.getDataBankAccount($scope.user.OrgId).then(
							function (res) {
								$scope.LihatOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_CashOptions = res.data.Result;
								ArrBankAcct = res.data.Result;
							},
							function (err) {
								console.log('error -->', err)
							}
						);

						nominal = res.data[0].Nominal;
						$scope.Hide_LihatOutgoingPayment_DaftarInstruksiPembayaran = true;
						$scope.Hide_LihatOutgoingPayment_DaftarBiayaLainLain = true;
						$scope.Hide_LihatRincianPembayaran = true;
						$scope.Hide_LihatRincianPembayaran_cashPickUp = false;
						$scope.hide_LihatOutgoingPayment_CashPickUp = false;
						$scope.LihatOutgoingPayment_RincianPembayaran_MetodePembayaran = "Cash";
						var tanggalPaymentCP = new Date(res.data[0].PaymentDate);
						$scope.LihatOutgoingPayment_RincianPembayaran_NominalPembayaran_Cash = addSeparatorsNF(nominal, '.', '.', ',');
						$scope.LihatOutgoingPayment_RincianPembayaran_PaymentDate_Cash = tanggalPaymentCP;
						$scope.LihatOutgoingPayment_RincianPembayaran_Keterangan_Cash = res.data[0].Description;
						$scope.LihatOutgoingPayment_Cash_RincianPembayaran_NoRekeningPenerima_Cash = res.data[0].ReceiverAccNumber;
						$scope.LihatOutgoingPayment_NamaBankPenerima = res.data[0].ReceiverBankName;
						$scope.LihatOutgoingPayment_NamaPemegangRekening = res.data[0].ReceiverName;
					}
					if($scope.LihatOutgoingPayment_TipeInstruksiPembayaran == "Koreksi Administratif"){
						$scope.Hide_LihatOutgoingPayment_DaftarInstruksiPembayaran = false;
						if($scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran == "Bank Transfer"){
							$scope.Hide_LihatOutgoingPayment_DaftarBiayaLainLain = false;
						}else{
							$scope.Hide_LihatOutgoingPayment_DaftarBiayaLainLain = true;
						}
						
					}

					if($scope.LihatOutgoingPayment_TipeInstruksiPembayaran == "Unit"){
						$scope.LihatOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.columns[3].showColumn();
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.columns[4].showColumn();
					
					}else{
						$scope.LihatOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.columns[3].hideColumn();
						$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI.grid.columns[4].hideColumn();
					}
				},
				function (err) {
					console.log('error -->', err);
				}
			);
			//$scope.BackToMainOutgoingPayment();
		};

		$scope.SetDataMainOutgoingPaymentRequest = function (pagenumber, uiGridPageSize, startdate, endDate, branch) {
			var parameter = [{ TanggalAwal: startdate, TanggalAkhir: endDate, Outlet: branch }];
			OutgoingPaymentFactory.getDataOutgoingPaymentRequest(pagenumber, uiGridPageSize, JSON.stringify(parameter))
				.then(
					function (res) {
						//console.log('data --> ',JSON.stringify(res.data));
						$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
						//$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
						//$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.paginationPageSize = uiGridPageSize;
						//$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.paginationPageSizes = [uiGridPageSize];
					},
					function (err) {
						console.log("err=>", err);
					}
				);
		}

		$scope.MainOutgoingPayment_BulkAction_SearchDropdown_Changed = function () {
			if ($scope.MainOutgoingPayment_BulkAction_SearchDropdown == "Setuju") {
				angular.forEach($scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolak_APId = value.DocumentId;
					$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
					
					// cek Approval Status Desc
					OutgoingPaymentFactory.CheckValidForReverse($scope.ModalTolak_APId, $scope.user.OutletId).then(
						function (res) {
							console.log('isi ', JSON.stringify(res));
							if (res.data.Result[0].ApprovalStatusDesc == "Recon") {
								//alert('Data Outgoing sudah di Reconcile');
								bsNotify.show({
									title: "Warning",
									content: "Data Outgoing sudah di Reconcile.",
									type: 'warning'
								});
								return
							}
							else if (res.data.Result[0].ApprovalStatusDesc == "Close") {
								bsNotify.show({
									title: "Warning",
									content: "Data Outgoing sudah dilakukan Closing.",
									type: 'warning'
								});
								return
							}
							else {
								if (value.ApprovalStatusDesc == "Diajukan") {
									$scope.CreateApprovalData("Disetujui");
									OutgoingPaymentFactory.updateApprovalData($scope.ApprovalData)
										.then(
											function (res) {
												//alert("Berhasil Disetujui");
												bsNotify.show({
													title: "Berhasil",
													content: "Berhasil Disetujui.",
													type: 'success'
												});
												$scope.Batal_Lihat_TolakModal();
											},
			
											function (err) {
												//alert("Persetujuan gagal");
												bsNotify.show({
													title: "Gagal",
													content: "Persetujuan gagal.",
													type: 'danger'
												});
											}
										);
								}
							}
						},
						function (err) {
							//alert('Reversal gagal diajukan, Error --> ', err);
							bsNotify.show({
								title: "Gagal",
								//content: 'Reversal gagal diajukan, Error --> ', err,
								content: 'Reversal gagal diajukan, Error --> ' + err,
								type: 'danger'
							});
						}
					);
					
				});
			}
			else if ($scope.MainOutgoingPayment_BulkAction_SearchDropdown == "Tolak") {
				angular.forEach($scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI.selection.getSelectedRows(), function (value, key) {
					$scope.ModalTolak_APId = value.DocumentId;
					$scope.ModalTolak_Tanggal = value.OutgoingPaymentDate;
					$scope.ModalTolak_NomorAP = value.OutgoingPaymentNumber;
					$scope.ModalTolak_TipeOutgoing = value.OutgoingPaymentType;
					$scope.ModalTolak_NamaVendor = value.VendorName;
					$scope.ModalTolak_Nominal = value.Nominal;
					$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
					$scope.ModalTolak_TanggalPengajuan = value.ApprovalCreateDate;
					$scope.ModalTolak_AlasanPengajuan = value.ReversalReason;

					if (value.ApprovalStatusDesc == "Diajukan")
						angular.element('#ModalTolakApprovalOutgoing').modal('setting', { closeable: false }).modal('show');
				}
				);
			}
		}

		$scope.CreateApprovalData = function (ApprovalStatus) {
			$scope.ApprovalData = [
				{
					"ApprovalTypeName": $scope.ModalTolak_TipePengajuan +'|OutgoingPayment_TT',
					"RejectedReason": $scope.ModalTolak_AlasanPenolakan,
					"DocumentId": $scope.ModalTolak_APId,
					"ApprovalStatusName": ApprovalStatus,
				}
			];
		}

		$scope.Batal_Lihat_TolakModal = function () {
			angular.element('#ModalTolakApprovalOutgoing').modal('hide');
			$scope.Main_BulkAction = "";
			$scope.ClearActionFields();
			$scope.SetDataMainOutgoingPaymentRequest(1, uiGridPageSize, $scope.formatDate($scope.MainOutgoingPayment_TanggalOutgoingPayment),
				$scope.formatDate($scope.MainOutgoingPayment_TanggalOutgoingPaymentEnd),
				$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch);
		}

		$scope.Pengajuan_Lihat_TolakModal = function () {
			$scope.CreateApprovalData("Ditolak");
			OutgoingPaymentFactory.updateApprovalData($scope.ApprovalData)
				.then(
					function (res) {
						//alert("Berhasil tolak");
						bsNotify.show({
							title: "Berhasil",
							content: "Berhasil tolak.",
							type: 'success'
						});
						$scope.Batal_Lihat_TolakModal();
					},

					function (err) {
						//alert("Penolakan gagal");
						bsNotify.show({
							title: "Gagal",
							content: "Penolakan gagal.",
							type: 'danger'
						});
					}
				);
		}

		$scope.ClearActionFields = function () {
			$scope.ModalTolak_APId = "";
			$scope.ModalTolak_Tanggal = "";
			$scope.ModalTolak_NomorAP = "";
			$scope.ModalTolak_TipeAP = "";
			$scope.ModalTolak_NamaVendor = "";
			$scope.ModalTolak_Nominal = "";
			$scope.ModalTolak_TipePengajuan = "";
			$scope.ModalTolak_TanggalPengajuan = "";
			$scope.ModalTolak_AlasanPengajuan = "";
		}

		$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: true,
			canSelectRows: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "ApprovalId", displayName: "Approval Id", field: "ApprovalId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "OutgoingPaymentDate", type: "date", displayName: "Tanggal Outgoing Payment", field: "OutgoingPaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd/MM/yyyy\"', width: '10%' },
				{
					name: "Id", displayName: "Nomor Outgoing Payment", field: "OutgoingPaymentNumber", enableHiding: false, visible: true, width: '10%',
					cellTemplate: '<a ng-click="grid.appScope.ShowLihatDetailOutgoingPaymentFromMain(row)">{{row.entity.OutgoingPaymentNumber}}</a>'
				},
				{ name: "TipeApprovalId", displayName: "TipeApprovalId", field: "ApprovalType", enableHiding: false, visible: false, width: '10%' },
				{ name: "TipeOutgoing", displayName: "Tipe Outgoing Payment", field: "OutgoingPaymentType", enableHiding: false, width: '10%' },
				{ name: "VendorName", displayName: "Nama Pelanggan / Vendor / Penerima", field: "VendorName", enableCellEdit: false, enableHiding: false, width: '10%' },
				{ name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false, width: '10%' },
				{ name: "Nominal Pembayaran", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2', width:'10%'  },
				{ name: "TipeApproval", displayName: "Tipe Pengajuan", field: "ApprovalTypeDesc", enableHiding: false, width: '10%' },
				{ name: "TanggalPengajuan", type: "date", displayName: "Tanggal Pengajuan", field: "ApprovalCreateDate", enableCellEdit: false, enableHiding: false, cellFilter: 'nullDateFilter', width: '10%' },
				{ name: "StatusPengajuan", displayName: "Status Pengajuan", field: "ApprovalStatusDesc", enableCellEdit: false, enableHiding: false, width: '10%' },
				{ name: "AlasanPengajuan", displayName: "Alasan Pengajuan", field: "ReversalReason", enableCellEdit: false, enableHiding: false, width: '10%' },
				{ name: "AlasanPenolakan", displayName: "Alasan Penolakan", field: "RejectedReason", enableCellEdit: false, enableHiding: false, width: '10%' },
				{ name: "Nomor Outgoing Payment Reversal", displayName: "OutgoingPaymentRevNumber", field: "OutgoingPaymentRevNumber", visible: false, width: '10%' },
			],

			onRegisterApi: function (gridApi) {
				$scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI = gridApi;
			}
		};

		$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: false,
			showGridFooter: true,
			showColumnFooter: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "Id", displayName: "Nomor Dibayar", field: "Id", visible: false },
				{ name: "PaymentId", displayName: "PaymentId", field: "PaymentId", visible: false },
				{ name: "PaymentId", displayName: "Nomor Instruksi Pembayaran/Advance Payment", field: "BillingNo", visible: true },
				{ name: "InstructionDate", displayName: "Tanggal Instruksi Pembayaran/Advance Payment", field: "InstructionDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "FrameNo", displayName: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, enableHiding: false },
			//	{ name: "VendorNameOrDF", displayName: "Nama Pelanggan / Vendor / Penerima", field: "VendorNameOrDF", enableCellEdit: false, enableHiding: false },
				{ 
					name: "VendorName", displayName: "Nama Pelanggan / Vendor / Penerima", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<div ng-show="row.entity.VendorNameOrDF == null">{{row.entity.VendorName}}</div><div ng-show="row.entity.VendorNameOrDF != null">{{row.entity.VendorNameOrDF}}</div>'
				},
				{
					name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false,
					// footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
				},
				{
					name: "NominalParts", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false,
					aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number',
					// footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_UIGrid }}</div>',
					footerCellFilter: 'number: 2',
					cellFilter: 'number: 4'
				},
				{
					name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false,
					aggregationType: uiGridConstants.aggregationTypes.sum, type: 'number',
					// footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer_UIGrid }}</div>',
					footerCellFilter: 'number: 2',
					cellFilter: 'number: 2'
				},
				{ name: "Type", displayName: "Type", field: "DataType", visible: false },
				{
					name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<a ng-click="grid.appScope.DeleteGrid_DaftarInstruksiPembayaranYangAkanDibayar(row)">hapus</a>'
				},
				{ name: "VendorId", displayName: "VendorId", field: "VendorId", visible: false },
				{ name: "SOId", displayName: "SOId", field: "SOId", visible: false },
				{ name: "AccountTo", displayName: "Nomor Rekening Tujuan", field: "AccountTo", visible: false },
				{ name: "BankToId", displayName: "Kode Bank Tujuan", field: "BankToId", visible: false },
				{ name: "BankToName", displayName: "Nama Bank Tujuan", field: "BankToName", visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_gridAPI = gridApi;

				/*	gridApi.selection.on.rowSelectionChanged($scope,function(row){				
			
						$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data =[];				
						if (row.isSelected==true)
						{	
							$scope.SetGrid_DaftarBiayaLainLain(1 ,uiGridPageSize, row.entity.PaymentId);				
						}								
					});*/
				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {	
				//$scope.SetDataIncomingInstruction(pageNumber, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
				//console.log("masuk setting grid upload dan tambah");
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//}); 
			}
		};
		//CR4 Phase 2
		$scope.selectDebetKredit = function (data) {
			console.log('datadebet',data);
			console.log('data di fild',$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit);
		}
		//End CR4 Phase 2

		$scope.TambahOutgoingPayment_TambahBiayaLainLain = function () {
			var saldo = parseFloat($scope.TambahOutgoingPayment_BiayaLainLain_NominalforParseFloat);
			//saldo = saldo;
			console.log(' isi saldo ', saldo);
			var found = 0;
			$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.BiayaId);
				console.log('isi biaya -->', $scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya);
				if($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya =="Titipan" && $scope.TambahIncomingPayment_BiayaBiaya_Assignment !=obj.Assignment){
					found = 0;
				}else
				if ($scope.TambahIncomingPayment_BiayaBiaya_TipeBiaya == obj.BiayaId){
					found = 1;
				}
					
			});

			if (($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain != "") &&
				($scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit != "") &&
				(saldo > 0)) {
				// if (($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain == "Biaya Bank") &&
				// 	($scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit != "") &&
				// 	(saldo > 100000)) {
				// 	bsNotify.show({
				// 		title: "Warning",
				// 		content: "Nominal tidak boleh lebih dari 100.000.",
				// 		type: 'warning'
				// 	});
				// }else {					
					//CR4 Phase 2
					if ($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain == 'Biaya Bunga DF'){
						saldo = saldo - $scope.TotalNominal;
						console.log('saldo',saldo);
						// console.log('nominal',$scope.TambahOutgoingPayment_BiayaLainLain_Nominal);
						// console.log('dari atas',$scope.TambahOutgoingPayment_RincianPembayaran_NominalPembayaran_BankTransfer);
						// console.log('$scope.TotalNominal',$scope.TotalNominal);
						
						// tot1 = parseFloat(parseFloat($scope.TotalNominal).toFixed(2));
						// $scope.fixedNominal($scope.TotalNominal, 2);
						// var tot2 =parseFloat($scope.fixedNominal($scope.TotalNominal, 2));

						// console.log('tot2',tot2);
						if(saldo > 0){
							$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = "Debet";
							$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = true;
						}else{
							$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = "Kredit";
							saldo = Math.abs(saldo);
							$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit_EnableDisable = true;
						}						
					}
					//End CR4 Phase 2
					if(found == 0){
						$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.push({
							ChargesId: 0,
							OtherChargesType: $scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain,
							DebitCredit: $scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit,
							Nominal: saldo,
							Description: $scope.TambahOutgoingPayment_BiayaLainLain_Keterangan
						});

						$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [];
						$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = [];
						$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = "";
						$scope.TambahOutgoingPayment_BiayaLainLain_Keterangan = "";
						$scope.CalculateNominalPembayaran();
					}else{
						bsNotify.show({
							title: "Warning",
							content: "Tipe Charges Tidak Boleh Sama.",
							type: 'warning'
						});
					}
				// }
			}else if($scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain == "Biaya Bunga DF" && saldo > 0 && ($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Parts' || $scope.TambahOutgoingPayment_TipeInstruksiPembayaran == 'Unit')){
				//CR4 Phase 2
				var index = $scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.some(function(x, i) {
					if (x.OtherChargesType == "Biaya Bunga DF") return (index = i);
				});

				if(index){
					bsNotify.show({
						title: "Warning",
						content: "Biaya Bunga DF sudah ada di daftar biaya lain lain.",
						type: 'warning'
					});
				}else{
					saldo = saldo - $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total.toFixed(2);
					console.log('saldo',saldo,' = ',$scope.TotalNominal);
					if(saldo > 0){
						$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = "Debet";
					}else{
						$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = "Kredit";
						saldo = Math.abs(saldo);
					}						
					
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.push({
						ChargesId: 0,
						OtherChargesType: $scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain,
						DebitCredit: $scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit,
						Nominal: saldo,
						Description: $scope.TambahOutgoingPayment_BiayaLainLain_Keterangan
					});
	
					$scope.TambahOutgoingPayment_BiayaLainLain_TipeBiayaLain = [];
					$scope.TambahOutgoingPayment_BiayaLainLain_DebetKredit = [];
					$scope.TambahOutgoingPayment_BiayaLainLain_Nominal = "";
					$scope.TambahOutgoingPayment_BiayaLainLain_Keterangan = "";
					$scope.CalculateNominalPembayaran();
					//End CR4 Phase 2
				}
			}
			else {
				//alert("Isian biaya lain lain tidak lengkap !");
				bsNotify.show({
					title: "Warning",
					content: "Isian biaya lain lain tidak lengkap.",
					type: 'warning'
				});
			}
		};

		$scope.SetGrid_DaftarBiayaLainLain = function (start, limit, paymentId) {
			console.log('isi nya -->', paymentId);
			OutgoingPaymentFactory.getPaymentInstructionCharges(start, limit, paymentId).then(
				//OutgoingPaymentFactory.getDataIncomingInstruction(start,limit, paymentId, 'test').then(
				function (res) {
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.totalItems = res.data.Total;		//Total keseluruhan data, diambil dari SP
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.paginationPageSize = uiGridPageSize;
					$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.paginationPageSizes = [uiGridPageSize];
				},
				function (err) {
					console.log("err=>", err);
				}
			);
		};

		$scope.DeleteGrid_DaftarBiayaLainLain = function (row) {
			var index = $scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.indexOf(row.entity);
			$scope.TambahOutgoingPayment_DaftarBiayaLainLain_UIGrid.data.splice(index, 1);
			$scope.CalculateNominalPembayaran();
		};

		$scope.DeleteGrid_DaftarInstruksiPembayaranYangAkanDibayar = function (row) {
			var index = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.indexOf(row.entity);
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total -= row.entity.Nominal;
			$scope.TotalNominal = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total;
			$scope.CalculateNominalPembayaran();
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data.splice(index, 1);
		};

		$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid = {//aaaaa
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: true,
			displaySelectionCheckbox: true,
			enableColumnResizing: true,
			multiselect: true,
			showGridFooter: true,
			canSelectRows: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "BillingNo", displayName: "Nomor Instruksi Pembayaran/Advance Payment", field: "BillingNo", visible: true },
				{ name: "InstructionDate", displayName: "Tanggal Instruksi Pembayaran/Advance Payment", field: "PaymentInstructionDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "NoRangka", displayName: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, enableHiding: false },
				//{ name: "VendorNameOrDF", displayName: "Nama Pelanggan/Vendor/Penerima", field: "VendorNameOrDF", enableCellEdit: false, enableHiding: false },
				{ 
					name: "VendorName", displayName: "Nama Pelanggan / Vendor / Penerima", enableCellEdit: false, enableHiding: false,
					cellTemplate: '<div ng-show="row.entity.VendorNameOrDF == null">{{row.entity.VendorName}}</div><div ng-show="row.entity.VendorNameOrDF != null">{{row.entity.VendorNameOrDF}}</div>'
				},
				{
					name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >Sub Total</div>'
				},
				{
					name: "NominalParts", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 4',
					aggregationType: uiGridConstants.aggregationTypes.sum,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
				},
				{
					name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2',
					aggregationType: uiGridConstants.aggregationTypes.sum,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
				},
				{ name: "Type", displayName: "Type", field: "DataType", visible: false },
				{ name: "Id", displayName: "Nomor Instruksi Pembayaran", field: "PaymentId", visible: false },
				{ name: "VendorId", displayName: "Vendor Id", field: "VendorId", visible: false },
				{ name: "SOId", displayName: "SO Id", field: "SOId", visible: false },
				{ name: "AccountTo", displayName: "Nomor Rekening Tujuan", field: "AccountTo", visible: false },
				{ name: "BankToId", displayName: "Kode Bank Tujuan", field: "BankToId", visible: false },
				{ name: "BankToName", displayName: "Nama Bank Tujuan", field: "BankToName", visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI = gridApi;

				//gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {	
				//$scope.SetDataIncomingInstruction(pageNumber, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran);
				//console.log("masuk setting grid upload dan tambah");
				//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
				//}); 
			}
		};

		$scope.LihatOutgoingPayment_DaftarInstruksiPembayaran_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			useExternalPagination: false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: true,
			columnDefs: [
				{ name: "MetodePembayaran", displayName: "Metode Pembayaran", field: "PaymentMethod", visible: true },
				{ name: "PaymentDate", displayName: "Tanggal Pembayaran", field: "PaymentDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "NomorRekeningPengirim", displayName: "Nomor Rekening Pengirim", field: "SenderAccount", enableCellEdit: false, enableHiding: false },
				{ name: "NamaBankPengirim", displayName: "Nama Bank Pengirim", field: "SenderBankName", enableCellEdit: false, enableHiding: false },
				{ name: "Referensi", displayName: "Referensi", field: "Refference", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "NomorRekeningPenerima", displayName: "Nomor Rekening Penerima", field: "ReceiverAccount", enableCellEdit: false, enableHiding: false },
				{ name: "NamaBankPenerima", displayName: "Nama Bank Penerima", field: "ReceiverBankName", enableCellEdit: false, enableHiding: false },
				{ name: "NamaPenerima", displayName: "Nama Penerima", field: "ReceiverName", enableCellEdit: false, enableHiding: false },
				{ name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2' }
			],

			onRegisterApi: function (gridApi) {
				$scope.LihatOutgoingPayment_DaftarInstruksiPembayaran_gridApi = gridApi;
			}
		};

		// if($scope.LihatOutgoingPayment_TipeInstruksiPembayaran != "Unit"){
		// 	$scope.LihatOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.columns[3].hideColumn();
		// }else{
		// 	$scope.LihatOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.columns[3].showColumn()	
		// }

		$scope.LihatOutgoingPayment_TipeInstruksiPembayaran_Changed = function(){
			$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid.data =[];
		}

		$scope.LihatOutgoingPayment_DaftarOutgoingPayment_UIGrid = {
			// nomorRangkaUnit = false,
			paginationPageSizes: null,
			useCustomPagination: true,
			useExternalPagination: false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			showGridFooter: true,
			showColumnFooter: true,
			multiselect: false,
			canSelectRows: true,

			// columnDefs: [
			// 	{ name: "OutgoingPaymentId", displayName: "Nomor Instruksi Pembayaran / Advance Payment", field: "DetailNumber", visible: true },
			// 	{ name: "OutgoingPaymentType", displayName: "Tanggal Instruksi Pembayaran / Advance Payment", field: "InstructionDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
			// 	{ name: "VendorName", displayName: "Nama Pelanggan", field: "VendorName", enableCellEdit: false, enableHiding: false },
			// 	{ name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false, visible: false },
			// 	{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0' }
			// ],
			columnDefs: [
				{ name: "BillingNo", displayName: "Nomor Instruksi Pembayaran/Advance Payment", field: "DetailNumber", visible: true },
				{ name: "InstructionDate", displayName: "Tanggal Instruksi Pembayaran/Advance Payment", field: "InstructionDate", enableCellEdit: false, enableHiding: false, cellFilter: 'date: \"dd-MM-yyyy\"' },
				{ name: "NoRangka", displayName: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, enableHiding: false },
				{ name: "VendorNameOrDF", displayName: "Nama Pelanggan/Vendor/Penerima", field: "VendorNameOrDF", enableCellEdit: false, enableHiding: false },
				{ name: "VendorName", displayName: "Nama Pelanggan/Vendor/Penerima", field: "VendorName", enableCellEdit: false, enableHiding: false,visible: false },
				{
					name: "MetodeBayar", displayName: "Metode Pembayaran", field: "PaymentMethod", enableCellEdit: false, enableHiding: false,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >Sub Total</div>'
				},
				{
					name: "NominalParts", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 4',
					aggregationType: uiGridConstants.aggregationTypes.sum,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
				},
				{
					name: "Nominal", displayName: "Nominal Pembayaran", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2',
					aggregationType: uiGridConstants.aggregationTypes.sum,
					footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>'
				},
				{ name: "Type", displayName: "Type", field: "DataType", visible: false },
				{ name: "Id", displayName: "Nomor Instruksi Pembayaran", field: "PaymentId", visible: false },
				{ name: "VendorId", displayName: "Vendor Id", field: "VendorId", visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.LihatOutgoingPayment_DaftarOutgoingPayment_gridAPI = gridApi;

				gridApi.core.on.renderingComplete($scope, function () {
					$scope.MainOutgoingPayment_Cari();
				});
			}
		};

		// OLD Filter
		$scope.TambahOutgoingPayment_Filter = function () {
			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Unit" || $scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Aksesoris"){
				$scope.FilterBE();	
			}else{
				if (($scope.TambahOutgoingPayment_TextFilter != "") &&
					($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown != "")) {

					/*
									{name:"BillingNo",displayName:"Nomor Instruksi Pembayaran/Advance Payment", field:"BillingNo", visible:true},
									{name:"InstructionDate",displayName:"Tanggal Instruksi Pembayaran/Advance Payment", field:"PaymentInstructionDate", enableCellEdit: false, enableHiding: false ,  cellFilter: 'date: \"dd-MM-yyyy\"'},
									{name:"NoRangka",displayName:"Nomor Rangka", field:"FrameNo",enableCellEdit: false, enableHiding: false},
									{name:"VendorName",displayName:"Nama Pelanggan/Vendor/Penerima", field:"VendorName",enableCellEdit: false, enableHiding: false},
									{name:"MetodeBayar", displayName:"Metode Pembayaran", field:"PaymentMethod", enableCellEdit: false, enableHiding: false,
										footerCellTemplate: '<div class="ui-grid-cell-contents" >Sub Total</div>' },
									{name:"Nominal", displayName:"Nominal Pembayaran",field:"Nominal",enableCellEdit: false, enableHiding: false, cellFilter: 'number: 2', 
										aggregationType: uiGridConstants.aggregationTypes.sum	,
										footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | number:2 }}</div>' 
									},
									{name:"Type",displayName:"Type", field:"DataType", visible:false},
									{name:"Id",displayName:"Nomor Instruksi Pembayaran", field:"PaymentId", visible:false},
									{name:"VendorId",displayName:"Vendor Id", field:"VendorId", visible:false}*/

					var nomor;
					if ($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "NoInstruksiAdv") {
						nomor = 1;
					}
					if ($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "Tanggal") {
						nomor = 2;
					}
					if ($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "NomorRangka") {
						nomor = 3;
					}
					if ($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "NamaVendor") {
						nomor = 4;
					}
					if ($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "Nominal") {
						nomor = 6;
					}
					$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahOutgoingPayment_TextFilter;
				}
				else {
					$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.clearAllFilters();
				}
			}
		}

		//New Filter
		var tmpFlag = 0
		$scope.FilterBE = function () {
			if($scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown == "Tanggal"){
				tmpFlag = 1;
			}else{
				tmpFlag = 0;
			}
			OutgoingPaymentFactory.getDataFilter(1, uiGridPageSize, $scope.TambahOutgoingPayment_TipeInstruksiPembayaran, $scope.TambahOutgoingPayment_TextFilter, $scope.TambahOutgoingPayment_InstruksiPembayaran_SearchDropdown , $scope.TambahOutgoingPayment_NamaBranch, tmpFlag)
			.then(
				function(res){
					$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.data = res.data.Result
				},
				function(err){
					console.log("Err",err)
				}	
			)
		}

		$scope.TambahOutgoingPayment_TipeInstruksiPembayaran_Selected_Changed = function () {
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_UIGrid.data = [];
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaran_gridAPI.grid.clearAllFilters();
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_UIGrid.data = [];
			$scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total = 0;
			$scope.TotalNominal = $scope.TambahOutgoingPayment_DaftarInstruksiPembayaranYangAkanDibayar_ToRepeat_Total;
			$scope.TambahOutgoingPayment_RincianPembayaran_MetodePembayaran = {};
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaRekeningPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NoRekeningPengirim_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_BankTransfer = "";
			$scope.TambahOutgoingPayment_RincianPembayaran_NamaBankPenerima_Nama_BankTransfer = "";	
			$scope.CalculateNominalPembayaran();

			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up"){
				$scope.typeCashPickUp = false;
			}else{
				$scope.typeCashPickUp = true;
			}
		}

		$scope.MainOutgoingPayment_Filter = function () {
			console.log('text --> ', $scope.MainOutgoingPayment_TextFilter);

			// {name:"ApprovalId", displayName:"Approval Id", field:"ApprovalId", enableCellEdit: false, enableHiding: false, visible:false },
			// {name:"OutgoingPaymentDate",displayName:"Tanggal Outgoing Payment", field:"OutgoingPaymentDate", enableCellEdit: false, enableHiding: false},
			// {name:"Id",displayName:"Nomor Outgoing Payment", field:"OutgoingPaymentNumber",enableHiding:false, visible:true, 
			// 	cellTemplate: '<a ng-click="grid.appScope.ShowLihatDetailOutgoingPaymentFromMain(row)">{{row.entity.OutgoingPaymentNumber}}</a>'
			// },
			// {name:"TipeApprovalId",displayName:"TipeApprovalId", field:"ApprovalType",enableHiding:false , visible:false},
			// {name:"TipeOutgoing",displayName:"Tipe Outgoing Payment", field:"OutgoingPaymentType",enableHiding:false},
			// {name:"VendorName", displayName:"Nama Pelanggan / Vendor / Penerima", field:"VendorName", enableCellEdit: false, enableHiding: false}, 
			// {name:"MetodeBayar", displayName:"Metode Pembayaran", field:"PaymentMethod", enableCellEdit: false, enableHiding: false},
			// {name:"Nominal Pembayaran", displayName:"Nominal Pembayaran",field:"Nominal",enableCellEdit: false, enableHiding: false, cellFilter: 'number: 0'},
			// {name:"TipeApproval",displayName:"Tipe Pengajuan", field:"ApprovalTypeDesc",enableHiding:false},
			// {name:"TanggalPengajuan",displayName:"Tanggal Pengajuan", field:"ApprovalCreateDate", enableCellEdit: false, enableHiding: false},
			// {name:"StatusPengajuan",displayName:"Status Pengajuan", field:"ApprovalStatusDesc",enableCellEdit: false, enableHiding: false},
			// {name:"AlasanPengajuan",displayName:"Alasan Pengajuan", field:"ReversalReason",enableCellEdit: false, enableHiding: false},
			// {name:"AlasanPenolakan",displayName:"Alasan Penolakan", field:"RejectedReason",enableCellEdit: false, enableHiding: false},	

			if (($scope.MainOutgoingPayment_TextFilter != "") &&
				($scope.MainOutgoingPayment_SearchDropdown != "")) {
				var nomor;
				if ($scope.MainOutgoingPayment_SearchDropdown == "Nomor Outgoing Payment") {
					nomor = 3;
				}
				if ($scope.MainOutgoingPayment_SearchDropdown == "Tipe Outgoing Payment") {
					nomor = 5;
				}
				if ($scope.MainOutgoingPayment_SearchDropdown == "Nama Pelanggan/Vendor") {
					nomor = 6;
				}
				if ($scope.MainOutgoingPayment_SearchDropdown == "Metode Pembayaran") {
					nomor = 7;
				}
				if ($scope.MainOutgoingPayment_SearchDropdown == "Nominal") {
					nomor = 8;
				}
				console.log("nomor --> ", nomor);
				$scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.columns[nomor].filters[0].term = $scope.MainOutgoingPayment_TextFilter;
			}
			else {
				$scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.clearAllFilters();
			}
		};

		$scope.MainOutgoingPayment_Cari = function () {
			if (((isNaN($scope.MainOutgoingPayment_TanggalOutgoingPayment) != true) &&
				(angular.isUndefined($scope.MainOutgoingPayment_TanggalOutgoingPayment) != true)) &&
				((isNaN($scope.MainOutgoingPayment_TanggalOutgoingPaymentEnd) != true) &&
					(angular.isUndefined($scope.MainOutgoingPayment_TanggalOutgoingPaymentEnd) != true)))
			//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
			{
				//alert($scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch);
				console.log("nama branch >>>>>>>>>>>",$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch);
				$scope.Show_MainOutgoingPayment_DaftarOutgoingPayment = true;
				$scope.SetDataMainOutgoingPaymentRequest(1, uiGridPageSize, $scope.formatDate($scope.MainOutgoingPayment_TanggalOutgoingPayment),
					$scope.formatDate($scope.MainOutgoingPayment_TanggalOutgoingPaymentEnd),
					$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch);
				$scope.MainOutgoingPayment_BulkAction_SearchDropdown = null;
			}
			else {
				//alert('Tanggal Outgoing Payment harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: "Tanggal Outgoing Payment harus diisi !",
					type: 'warning'
				});
			}
			console.log("Clear GRID AWAL >>>>>>>>>>>>>>>>>");
			$scope.MainOutgoingPayment_DaftarOutgoingPayment_gridAPI.grid.clearAllFilters();
		};

		$scope.BtnTest = function () {
			$scope.BackToMainOutgoingPayment();
			$scope.MainOutgoingPayment_Cari();
		}

		$scope.LihatOutgoingPayment_PengajuanReversal_Click = function () {
			OutgoingPaymentFactory.CheckValidForReverse($scope.OutgoingPaymentId, $scope.user.OutletId).then(
				function (res) {
					console.log('isi ', JSON.stringify(res));
					if (res.data.Result[0].ApprovalStatusDesc == "Recon") {
						//alert('Data Outgoing sudah di Reconcile');
						bsNotify.show({
							title: "Warning",
							content: "Data Outgoing sudah di Reconcile.",
							type: 'warning'
						});
					}
					else if (res.data.Result[0].ApprovalStatusDesc == "Close") {
						bsNotify.show({
							title: "Warning",
							content: "Data Outgoing sudah dilakukan Closing.",
							type: 'warning'
						});
					}
					else {
						$scope.LihatOutgoingPayment_PengajuanReversal_AlasanReversal = "";
						angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal("show");
					}
				},
				function (err) {
					//alert('Reversal gagal diajukan, Error --> ', err);
					bsNotify.show({
						title: "Gagal",
						//content: 'Reversal gagal diajukan, Error --> ', err,
						content: 'Reversal gagal diajukan, Error --> ' + err,
						type: 'danger'
					});
				}
			);
		};

		$scope.Batal_LihatOutgoingPayment_PengajuanReversalModal = function () {
			angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal('hide');
		};

		$scope.Pengajuan_LihatOutgoingPayment_PengajuanReversalModal = function () {
			if ($scope.LihatOutgoingPayment_PengajuanReversal_AlasanReversal == "") {
				//alert(' alasan reverse harus diisi');
				bsNotify.show({
					title: "Warning",
					content: "Alasan reverse harus diisi!",
					type: 'warning'
				});
			}
			else {
				var reverse = [
					{
						"DocumentId": $scope.OutgoingPaymentId,
						"SourceTable": "OutgoingPayment_TT",
						"ReversalReason": $scope.LihatOutgoingPayment_PengajuanReversal_AlasanReversal
					}
				];
				OutgoingPaymentFactory.setOutgoingPaymentReversal(reverse).then(
					function (res) {
						//alert('reversal berhasil diajukan');
						if (typeof res.data.ResponseCode != 'undefined') {
							bsNotify.show({
								title: "Gagal",
								content: res.data.ResponseMessage,
								type: 'danger'
							});
							angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal('hide');
						}
						else {
							bsNotify.show({
								title: "Berhasil",
								content: "Reversal berhasil diajukan.",
								type: 'success'
							});
							angular.element('#ModalLihatOutgoingPayment_PengajuanReversal').modal('hide');

							$scope.MainOutgoingPayment_Cari();
							$scope.BackToMainOutgoingPayment();
						}

					},
					function (err) {
						console.log(err.data.Message);
						//alert('Reversal gagal diajukan, Error --> ', err);
						bsNotify.show({
							title: "Gagal",
							content: 'Reversal gagal diajukan, Error --> ' + err.data.Message,
							type: 'danger'
						});
					}
				);
			}
		};

		$scope.CheckPengajuan = function(){
			var dateDock = angular.copy(new Date($scope.TambahOutgoingPayment_TanggalOutgoingPayment));
			var dateCash = angular.copy(new Date($scope.TambahOutgoingPayment_RincianPembayaran_PaymentDate_Cash));
			
			dateDock.setHours(0,0,0,0);
			dateCash.setHours(0,0,0,0);

			if($scope.TambahOutgoingPayment_TipeInstruksiPembayaran == "Cash Pick Up"){
				if(dateCash < dateDock){
					$scope.ShowPengajuan = true;
				}else{
					$scope.ShowPengajuan = false;
				}
			}
		}

		$scope.TambahOutgoingPayment_Pengajuan = function (){
			angular.element('#ModalLihatOutgoingPayment_PengajuanBackDate').modal('setting', { closeable: false }).modal('show');
		}

		$scope.Batal_LihatOutgoingPayment_PengajuanBackDateModal = function (){
			angular.element('#ModalLihatOutgoingPayment_PengajuanBackDate').modal('setting', { closeable: false }).modal('hide');

		}

		$scope.Pengajuan_LihatOutgoingPayment_PengajuanBackDateModal = function (){
			$scope.TambahOutgoingPayment_Simpan();
			angular.element('#ModalLihatOutgoingPayment_PengajuanBackDate').modal('setting', { closeable: false }).modal('hide');
		}
		/*
		$scope.SetDataOutgoingMaster= function(pageNumber, TanggalAwal, TanggalAkhir, OutletId) {
			console.log('masuk sini');
			OutgoingPaymentFactory.getDataOutgoingPaymentRequest(pageNumber, uiGridPageSize, Tanggal).then(
				function(res) {
						$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
						$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.totalItems = res.data.Total;		//Total keseluruhan data, diambil dari SP
						$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.paginationPageSize = uiGridPageSize;
						$scope.MainOutgoingPayment_DaftarOutgoingPayment_UIGrid.paginationPageSizes = [uiGridPageSize];
				},
				function(err) {
					console.log("err=>", err);
				}
			)
		};
		*/
	});


app.filter('rupiahCIP', function () {
	return function (val) {
		if (angular.isDefined(val)) {
			var valS = val.toString().split('.');
			val = valS[0];
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			if (valS.length > 1) {
				val = val + ',' + valS[1].substring(0, 2);
			}

		}
		return val;
	};
});
app.filter('nullDateFilter', function ($filter) {
	return function (input) {
		//var originalFilter = $filter('date:\"dd-MM-yyyy\"');
		return input == 'Invalid Date' ? '' : $filter('date')(input, "dd-MM-yyyy");
	};
});
