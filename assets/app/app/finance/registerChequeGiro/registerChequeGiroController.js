var app = angular.module('app');
app.controller('registerChequeGiroController', function ($scope, $http, CurrentUser, $filter, registerChequeGiroFactory, $timeout, uiGridConstants, bsNotify) {
    $scope.Show_MainRegisterChequeGiro = true;
    $scope.TambahRegDokPajak_TanggalRegistrasi = new Date();
    var dateFormat = 'dd/MM/yyyy';
    $scope.TambahRegisterChequeGiro_AccountNo = "";
    $scope.RegisterChequeGiroData = [];
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
    };

    var paginationOptions = {
        pageNumber: 1,
        pageSize: 10,
        sort: null
    }

    $scope.optionsMainRegisterChequeGiro_SelectKolom = [
        { name: 'Nomor Cheque/Giro', value: 'Nomor Cheque/Giro' },
        { name: 'Nomor Rekening Bank', value: 'Nomor Rekening Bank' },
        { name: 'Nama Bank', value: 'Nama Bank' },
        { name: 'Status', value: 'Status' }];

    $scope.optionsMainRegisterChequeGiro_SelectStatus = [
        { name: 'Hilang', value: -1 },
        { name: 'Batal', value: 0 }];

    var user = CurrentUser.user();
    if (user.OrgCode.substring(3, 9) == '000000') {
        $scope.RegisterChequeGiro_HO_Show = true;
    }
    else {
        $scope.RegisterChequeGiro_HO_Show = false;
    }

    $scope.RegisterChequeGiro_getDataBranch = function () {
        registerChequeGiroFactory.getDataBranchComboBox()
            .then
            (
            function (res) {
                $scope.optionsMainRegisterChequeGiro_SelectBranch = res.data;
                $scope.optionsTambahRegisterChequeGiro_SelectBranch = res.data;


                res.data.some(function (obj, i) {
                    if (obj.value == user.OutletId) {
                        $scope.MainRegisterChequeGiro_NamaBranch = obj.name;
                        $scope.TambahRegisterChequeGiro_NamaBranch = obj.name;

                        $scope.TambahRegisterChequeGiro_SelectBranch = obj.value;
                        $scope.MainRegisterChequeGiro_SelectBranch = obj.value;
                    }
                });


                $scope.getDataRegisterChequeGiro(1, 10);
            },
            function (err) {
                console.log("err=>", err);
            }
            );
    };

    $scope.getDataRegisterChequeGiro = function (start, limit) {
        var SearchType = $scope.MainRegisterChequeGiro_SelectKolom,
            SearchText = $scope.MainRegisterChequeGiro_TextFilter,
            filterdata = (angular.isUndefined(SearchType) ? '' : SearchType.replace('/', ' ')) + '|' + (angular.isUndefined(SearchText) ? '' : SearchText.replace(/\//g,""));
            console.log("Cek replace", filterdata)
        registerChequeGiroFactory.getDataRegisterChequeGiro(start, limit, filterdata, $scope.MainRegisterChequeGiro_SelectBranch)
            .then(
                function (res) {
                    $scope.RegisterChequeGiroList_UIGrid.data = res.data.Result;
					$scope.RegisterChequeGiroList_UIGrid.totalItems = res.data.Total;
					$scope.RegisterChequeGiroList_UIGrid.paginationPageSize = limit;
					$scope.RegisterChequeGiroList_UIGrid.paginationPageSizes = [10, 25, 50];
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
    }

    $scope.RegisterChequeGiro_getDataBranch();

    $scope.MainRegisterChequeGiro_Cari_Clicked = function () {
        $scope.getDataRegisterChequeGiro(1, 10);
    }

    $scope.TambahRegisterChequeGiro_Clicked = function () {
        $scope.Show_MainRegisterChequeGiro = false;
        $scope.Show_TambahRegisterChequeGiro = true;
        $scope.ClearForm();
    }

    $scope.BatalRegisterChequeGiro_Clicked = function () {
        $scope.Show_MainRegisterChequeGiro = true;
        $scope.Show_TambahRegisterChequeGiro = false;
    }


    $scope.RegisterChequeGiroList_UIGrid = {
        paginationPageSizes: [10, 25, 50],
        useCustomPagination: true,
        useExternalPagination: true,
        rowSelection: true,
        enableFiltering: true,
        enableColumnResizing: true,
        columnDefs: [
            { name: "Id", field: "Id", visible: false, width: 150 },
            { name: "OutletId", field: "OutletId", visible: false, width: 150 },
            { name: "Nomor Cheque/Giro", field: "ChequeGiroNo", visible: true, width: 150 },
            { name: "Nomor Rekening Bank", field: "AccountNo", width: 150 },
            { name: "Nama Bank", field: "BankName", visible: true, width: 150 },
            { name: "Status", field: "Status", width: 150 },
            { name: "Alasan Hilang", field: "AlasanHilang", width: 150 },
            { name: "Alasan Batal", field: "AlasanBatal", width: 150 },
            { name: 'Action', field: 'IncomingNo', visible: true, cellTemplate: '<a ng-click="grid.appScope.RegisterChequeGiroUpdateClick(row)">Update Status</a>' },

        ],

        onRegisterApi: function (gridApi) {
            $scope.GridApiRegisterChequeGiroList = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions.pageNumber = pageNumber;
				paginationOptions.pageSize = pageSize;
                $scope.getDataRegisterChequeGiro(paginationOptions.pageNumber, paginationOptions.pageSize);
            });
        }
    };

    $scope.RegisterChequeGiroUpdateClick = function (row) {
        $scope.RegisterChequeGiro_Id = row.entity.Id;
        // $scope.MainRegisterChequeGiro_SelectStatus = row.entity.Status;
        // $scope.MainRegisterChequeGiro_Keterangan = row.entity.Keterangan;
        angular.element('#Modal_RegisterChequeGiro_UpdateStatus').modal('setting', { closeable: false }).modal('show');
    }

    $scope.Batal_Modal_RegisterChequeGiro_UpdateStatus = function () {
        angular.element('#Modal_RegisterChequeGiro_UpdateStatus').modal('setting', { closeable: false }).modal('hide');
    }

    $scope.Update_Modal_RegisterChequeGiro_UpdateStatus = function () {
        var Data = [],
            StatusOk = true;

        if (($scope.MainRegisterChequeGiro_SelectStatus == ''
            || angular.isUndefined($scope.MainRegisterChequeGiro_SelectStatus))
            && $scope.MainRegisterChequeGiro_SelectStatus != 0) {
            bsNotify.show({
                title: "Warning",
                content: "Status harus dipilih",
                type: "warning"
            });
            StatusOk = false;
        }

        if ($scope.MainRegisterChequeGiro_Keterangan == '' || angular.isUndefined($scope.MainRegisterChequeGiro_Keterangan)) {
            bsNotify.show({
                title: "Warning",
                content: "Keterangan harus diisi",
                type: "warning"
            });
            StatusOk = false;
        }

        if (StatusOk) {
            Data = {
                "Id": $scope.RegisterChequeGiro_Id,
                "StatusCode": $scope.MainRegisterChequeGiro_SelectStatus,
                "Description": $scope.MainRegisterChequeGiro_Keterangan
            }
            $scope.RegisterChequeGiroData.push(Data);

            registerChequeGiroFactory.createRegisterChequeGiro($scope.RegisterChequeGiroData)
                .then(
                    function (res) {
                        console.log("res", res);
                        if (res.data.Result.length == 0) {
                            //alert("data berhasil disimpan");
                            bsNotify.show({
                                title: "Berhasil",
                                content: "Data berhasil diupdate.",
                                type: "success"
                            });
                            $scope.ClearForm();
                            angular.element('#Modal_RegisterChequeGiro_UpdateStatus').modal('setting', { closeable: false }).modal('hide');
                            $scope.getDataRegisterChequeGiro(paginationOptions.pageNumber, paginationOptions.pageSize);
                        }
                        else {
                            // alert(res.data.Result[0].Message);
                            bsNotify.show({
                                title: "Warning",
                                content: 'Gagal update',
                                type: "warning"
                            });
                        }

                    },
                    function (err) {
                        console.log(err);
                    }
                );
        }
    }

    $scope.ChequeGiroData = [];
    $scope.ChequeGiroNoList_UIGrid = {
        paginationPageSizes: [10, 25, 50],
        //useCustomPagination: true,
        //useExternalPagination: true,
        //rowSelection: true,
        data: $scope.ChequeGiroData,
        enableFiltering: true,
        enableColumnResizing: true,
        columnDefs: [

            { name: "Nomor Cheque/Giro", field: "ChequeGiroNo", visible: true, width: 800 },
            {
                name: "Action",
                cellTemplate: '<a ng-click="grid.appScope.TambahRegisterChequeGiro_DeleteChequeGiro(row)">Hapus</a>'
            }

        ],

        onRegisterApi: function (gridApi) {
            $scope.GridApiRegisterChequeGiroList = gridApi;
            // gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
            // 	$scope.RegisterChequeGiroList_UIGrid_Paging(pageNumber);
            // });
        }
    };

    $scope.TambahRegisterChequeGiro_DeleteChequeGiro = function (row) {
        $scope.ChequeGiroData.splice(row.entity.indexOf, 1);
    }
    $scope.TambahRegisterChequeGiro_Tambah_Clicked = function () {
        var Data = [];
        var isExists = false;

        if ($scope.TambahRegisterChequeGiro_NomorChequeGiro == null || $scope.TambahRegisterChequeGiro_NomorChequeGiro == '') {
            bsNotify.show({
                title: "Warning",
                content: "Nomor Cheque/Giro harus diisi !",
                type: "warning"
            });
        }
        else {
            $scope.ChequeGiroData.some(function (obj, i) {
				if (obj.ChequeGiroNo == $scope.TambahRegisterChequeGiro_NomorChequeGiro) {
					isExists = true;
				}
            });
            
            if (!isExists) {
            Data = {
                "ChequeGiroNo": $scope.TambahRegisterChequeGiro_NomorChequeGiro
            }
            $scope.ChequeGiroData.push(Data);
            $scope.TambahRegisterChequeGiro_NomorChequeGiro = '';
            $scope.GridApiRegisterChequeGiroList.grid.refresh();
            }
            else {
				//alert("Nomor PO sudah dipilih.");
				bsNotify.show({
					title: "Warning",
					content: "Nomor Cheque/Giro Sudah ada di dalam list.",
					type: "warning"
				});
			}

        }


    }

    $scope.ClearForm = function () {
        $scope.TambahRegisterChequeGiro_NomorChequeGiro = '';
        $scope.ChequeGiroData = [];
        $scope.ChequeGiroNoList_UIGrid.data = $scope.ChequeGiroData;
        $scope.TambahRegisterChequeGiro_NoRekening = "";
        $scope.TambahRegisterChequeGiro_NamaBank = "";
        $scope.RegisterChequeGiroData = [];
        $scope.TambahRegisterChequeGiro_AccountNo = "";
        $scope.RegisterChequeGiro_Id = 0;
    }

    $scope.TambahRegisterChequeGiro_Simpan_Clicked = function () {
        if (!$scope.CreateRegisterChequeGiroData())
            return;
        registerChequeGiroFactory.createRegisterChequeGiro($scope.RegisterChequeGiroData)
            .then(
                function (res) {
                    console.log("res", res);
                    if (res.data.Result.length == 0) {
                        //alert("data berhasil disimpan");
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Data berhasil disimpan.",
                            type: "success"
                        });
                        $scope.ClearForm();
                        $scope.Show_MainRegisterChequeGiro = true;
                        $scope.Show_TambahRegisterChequeGiro = false;
                        $scope.getDataRegisterChequeGiro(paginationOptions.pageNumber, paginationOptions.pageSize);
                    }
                    else {
                        // alert(res.data.Result[0].Message);
                        bsNotify.show({
                            title: "Warning",
                            content: res.data.Result[0].Message,
                            type: "warning"
                        });
                    }

                },
                function (err) {
                    console.log(err);
                }
            );
    }

    $scope.CreateRegisterChequeGiroData = function () {
        var RegisterCGList = [];
        if ($scope.TambahRegisterChequeGiro_NamaBank == null || $scope.TambahRegisterChequeGiro_NamaBank == '') {
            bsNotify.show({
                title: "Warning",
                content: "Rekening Bank harus diisi !",
                type: "warning"
            });
            return false;
        }
        else if ($scope.ChequeGiroData.length == 0) {
            bsNotify.show({
                title: "Warning",
                content: "Nomor Cheque/Giro Wajib ditambahkan!",
                type: "warning"
            });
            return false;
        }
        else {
            angular.forEach($scope.ChequeGiroData, function (value, key) {
                RegisterCGList.push(
                    {
                        "ChequeGiroNo": value.ChequeGiroNo,
                    }
                );
            });
            $scope.RegisterChequeGiroData = [{
                "AccountId": $scope.TambahRegisterChequeGiro_NoRekening,
                "AccountNo": $scope.TambahRegisterChequeGiro_AccountNo,
                "BankName": $scope.TambahRegisterChequeGiro_NamaBank,
                "ChequeGiroNo": null,
                "OutletId": $scope.TambahRegisterChequeGiro_SelectBranch,
                "RegisterCGList": RegisterCGList

            }]


        }
        return true;
    }

    $scope.getDataBankAccount = function () {
        registerChequeGiroFactory.getDataBankAccountComboBox()
            .then(
                function (res) {
                    $scope.loading = false;
                    $scope.TambahRegisterChequeGiro_NoRekening_Options = res.data;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
    }

    $scope.TambahRegisterChequeGiro_SelectBranch_Change = function(){
        $scope.TambahRegisterChequeGiro_NoRekening = "";
        registerChequeGiroFactory.getDataBankAccountComboBoxNew($scope.TambahRegisterChequeGiro_SelectBranch)
            .then(
                function (res) {
                    console.log("cek bank Account", res.data)
                    $scope.TambahRegisterChequeGiro_NoRekening_Options = res.data;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
    }

    $scope.getDataBankAccount();

    $scope.TambahRegisterChequeGiro_NoRekening_SelectedChange = function () {
        if ($scope.TambahRegisterChequeGiro_NoRekening != "" && !angular.isUndefined($scope.TambahRegisterChequeGiro_NoRekening)) {
            registerChequeGiroFactory.getDataAccountBank($scope.TambahRegisterChequeGiro_NoRekening)
                .then(
                    function (res) {
                        $scope.TambahRegisterChequeGiro_NamaBank = res.data.Result[0].AccountBankName;
                        $scope.TambahRegisterChequeGiro_AccountNo = res.data.Result[0].AccountBankNo;
                    }
                );
        }
    }

    $scope.Report_Excel_Clicked = function() {
        var SearchType = $scope.MainRegisterChequeGiro_SelectKolom,
            SearchText = $scope.MainRegisterChequeGiro_TextFilter,
            filterdata = (angular.isUndefined(SearchType) ? '' : SearchType.replace('/', ' ')) + '|' + (angular.isUndefined(SearchText) ? '' : SearchText);

        registerChequeGiroFactory.getPDFReportRegisterChequeGiro(filterdata, $scope.MainRegisterChequeGiro_SelectBranch)
            .then(
                function (res) {
                    var contentType = 'application/pdf';
                    var blob = new Blob([res.data], { type: contentType });
                    saveAs(blob, 'Report_RegisterChequeGiro.pdf');
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
    }

});