angular.module('app')
    .factory('registerChequeGiroFactory', function ($http, CurrentUser) {
        var user = CurrentUser.user();

        return {
            getDataBankAccountComboBox: function () {
                var url = '/api/fe/AccountBank/GetDataComboBox/';

                var res = $http.get(url);
                return res;
            },

            getDataBankAccountComboBoxNew: function (id) {
                var url = '/api/fe/AccountBank/GetDataComboBox/'+id;
                var res = $http.get(url);
                return res;
            },

            getDataRegisterChequeGiro: function (start, limit, filterdata, outletid) {
                var url = '/api/fe/RegisterChequeGiro/SelectAll/start/' + start + '/limit/' + limit + '/FilterData/' + filterdata + '/OutletId/' + outletid;
                var res = $http.get(url);
                return res;
            },

            getDataAccountBank: function (AccountId) {
                var url = '/api/fe/AccountBank/GetDataRekening/' + AccountId;
                var res = $http.get(url);
                return res;
            },

            getDataBranchComboBox: function () {
                console.log("factory.getDataBranchComboBox");
                var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
                var res = $http.get(url);
                return res;
            },
            createRegisterChequeGiro: function (inputData) {
                var url = '/api/fe/RegisterChequeGiro/SubmitData';
                var param = JSON.stringify(inputData);
                var res = $http.post(url, param);
                return res;
            },

            getPDFReportRegisterChequeGiro: function (filterdata, outletid) {
                var url = '/api/fe/RegisterChequeGiro/Cetak/FilterData/' + filterdata + '/OutletId/' + outletid;
                console.log('get --> ', url);
                var res = $http.get(url, { responseType: 'arraybuffer' });
                return res;
            }
        }

    });