angular.module('app')
  .factory('DaftarTransaksiFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var factory={};
    var debugMode=true;
	
	factory.getData= function(start, limit, IncomingInstructionId) {
		//20170409, Harja, begin
		var url = '/api/fe/IncomingInstruction/SelectData?start=' + start + '&limit=' + limit + '&IncomingInstructionId=' + IncomingInstructionId;
		//var url = 'http://localhost:9001/webapi2/api/IncomingInstruction/?start=' + start + '&limit=' + limit + '&IncomingInstructionId=' + IncomingInstructionId;
    var res=$http.get(url);
		return res;				
/*          var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
	  
		var da_json=[
				{ Tipe_Transaksi: "INT",DebetKredit: "Debet",Nominal: 1000,Assignment: "Incoming Payment",Keterangan: "Pooling"},
				{ Tipe_Transaksi: "CHG",DebetKredit: "Kredit",Nominal: 1000,Assignment: "Incoming Payment",Keterangan: "-"},
				{ Tipe_Transaksi: "TRF",DebetKredit: "Kredit",Nominal: 1000,Assignment: "Incoming Payment",Keterangan: "Pooling"},
			  ];	  
		res=da_json; */
		//20170409, Harja, End
      };
	  	  
	factory.getDataIncomingInstruction= function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[
				{Id: "1",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 1",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 1",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
				{Id: "2",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 2",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 2",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
				{Id: "3",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 3",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 3",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
			  ];
		res=da_json;
		
        return res;
      };	  
		  
	 	  //20170409, Harja, End
      factory.create= function(DaftarTransaksi) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: DaftarTransaksi.Name,
                                            Description: DaftarTransaksi.Description}]);
      };
      factory.update= function(DaftarTransaksi){
        return $http.put('/api/fw/Role', [{
                                            Id: DaftarTransaksi.Id,
                                            //pid: negotiation.pid,
                                            Name: DaftarTransaksi.Name,
                                            Description: DaftarTransaksi.Description}]);
      };
      factory.delete= function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      };
	  
	 return factory;
   
  });