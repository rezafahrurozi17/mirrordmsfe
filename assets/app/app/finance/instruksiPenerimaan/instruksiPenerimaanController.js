angular.module('app')
	.controller('InstruksiPenerimaanController', function ($scope, $stateParams, $http, $filter, CurrentUser, InstruksiPenerimaanFactory, DaftarTransaksiFactory, $timeout, uiGridConstants, bsNotify) {
		$scope.ShowTambahInstruksiPenerimaan = false;
		$scope.ShowLihatInstruksiPenerimaan = true;
		//$scope.options_DaftarInstruksiPenerimaanSearch = [{ name: "Nomor Instruksi Penerimaan", value: "Nomor Instruksi Penerimaan" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" },{ name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" } ];
		$scope.optionsMain_SearchDropdown = [{ name: "Nomor Instruksi Bank", value: "NomorInstruksi" }, { name: "Nomor Rekening Bank", value: "NomorRekeningBank" }, { name: "Tanggal Bank Statement", value: "TanggalBankStatement" }];
		var uiGridPageSize = 1000;
		$scope.IncomingInstructionId = 0;
		$scope.currentOutletId = 0;
		$scope.MetodeInput = '';
		$scope.EditMode = false;
		$scope.file;
		$scope.dataBank = [];
		$scope.user = CurrentUser.user();
		$scope.AccountData = {};
		$scope.ShowLihatInstruksiPenerimaan = false;
		$scope.Show_Main_DaftarList = true; 
		$scope.ShowTambahInstruksiPenerimaan = false;
		$scope.Main_TanggalStart = new Date();
		$scope.Main_TanggalEnd = new Date();
		$scope.maxDate = new Date();
		$scope.TanggalMainStartV = true;
		$scope.TanggalMainEndV = true;
		$scope.errorSubmit = 0;
		//console.log('testing');

		$scope.TanggalStatementChanged = function () {


			var dt = $scope.TambahInstruksiPenerimaan_TanggalBankStatement;

			var now = new Date();

			if (dt > now) {
				$scope.errTanggalStatement = "Tanggal awal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalStatementV = false;
			}
			else {
				$scope.TanggalStatementV = true;

			}

		}

		  
        $scope.$on('$viewContentLoaded', function(){
            function rightsBit(byte){
                var right = {};
                
                right = {
                    new : (byte & Math.pow(2, 1)) == Math.pow(2, 1),
                    edit : (byte & Math.pow(2, 2)) == Math.pow(2, 2),
                    delete : (byte & Math.pow(2, 3)) == Math.pow(2, 3)
                }
                return right;
            };
    
            $scope.allowed = rightsBit($stateParams.tab.menu.ByteEnable);
            console.log("rightByte", $scope.allowed,$stateParams.tab);
		});
		
		$scope.TanggalChanged = function () {


			var dts = $scope.Main_TanggalStart;
			var dte = $scope.Main_TanggalEnd;
			var now = new Date();

			if (dte < dts) {
				$scope.errTanggal = "Tanggal awal tidak boleh lebih dari tanggal akhir";
				$scope.TanggalMainStartV = false;
			}
			else if (dts > now) {
				$scope.errTanggal = "Tanggal awal tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalMainStartV = false;
			}
			else {
				$scope.TanggalMainStartV = true;

			}

		}

		$scope.TanggalChangedEnd = function () {


			var dts = $scope.Main_TanggalStart;
			var dte = $scope.Main_TanggalEnd;
			var now = new Date();

			if (dte > now) {
				$scope.errTanggalEnd = "Tanggal akhir tidak boleh lebih dari tanggal hari ini";
				$scope.TanggalMainEndV = false;
			}
			else {
				$scope.TanggalMainEndV = true;
			}
			console.log($scope.TanggalMainEndV);

		}

		function addSeparatorsNF(nStr, inD, outD, sep) {
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1) {
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr)) {
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		$scope.givePattern = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				return;
			}
		};

		$scope.givePattern2 = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPenerimaan_SaldoAkhir = $scope.TambahInstruksiPenerimaan_SaldoAkhir.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				return;
			}
		};

		$scope.givePattern3 = function (item, event) {
			console.log("item", item);
			if (event.which > 37 && event.which < 40) {
				event.preventDefault();
				return false;
			} else {
				$scope.TambahInstruksiPenerimaan_Nominal = $scope.TambahInstruksiPenerimaan_Nominal.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				return;
			}
		};
		var paginationOptions_Main_Daftar = {
			pageNumber: 1,
			pageSize: 10,
			sort: null
		}

		$scope.links = {
            sortFun: function(firstDateString, secondDateString) {
				// debugger;
				var a=new Date(firstDateString);
				var b=new Date(secondDateString);
				if (a < b) {
					return -1;
				}
				else if (a > b) {
					return 1;
				}
				else {
					return 0;
				}
			}
        };


		$scope.Main_Daftar_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			//useCustomPagination: true,
			//useExternalPagination : false,
			enableSelectAll: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			multiselect: true,
			canSelectRows: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "IdInstruksi", displayName: "Nomor Instruksi Bank", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "DocumentDate", displayName: "Tanggal Instruksi Bank", field: "IncomingDate", enableCellEdit: false, enableHiding: true, cellFilter: 'date:\"dd-MM-yyyy\"', 'sortingAlgorithm': $scope.links.sortFun },
				{
					name: "NomorInstruksi", displayName: "Nomor Instruksi Bank", field: "IncomingInstructionNumber", enableCellEdit: false, enableHiding: false, visible: true,
					cellTemplate: '<a ng-click="grid.appScope.ShowLihatDetailFromMain(row)">{{row.entity.IncomingInstructionNumber}}</a>'
				},
				{ name: "NomorRekeningBank", displayName: "Nomor Rekening Bank", field: "Account", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "TanggalBankStatement", type: 'date', displayName: "Tanggal Bank Statement", field: "BankStatementDate", enableCellEdit: false, enableHiding: false, visible: true, cellFilter: 'date:\"dd-MM-yyyy\"', 'sortingAlgorithm': $scope.links.sortFun },
				{ name: "ReconActive", displayName: "ReconActive", field: "ReconActive", enableCellEdit: false, enableHiding: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.Main_Daftar_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					//console.log("masuk setting grid upload dan tambah");
					//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
					paginationOptions_Main_Daftar.pageNumber = pageNumber;
					paginationOptions_Main_Daftar.pageSize = pageSize;
				});
			}
		};

		$scope.SetComboBranch = function () {
			InstruksiPenerimaanFactory.getDataBranch().then(
				function (res) {
					$scope.optionsTambahInstruksiPenerimaan_NamaBranch = res.data.Result;
					$scope.TambahInstruksiPenerimaan_NamaBranch = $scope.user.OrgId;
					//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					/*
					if (CurrentUser.role == "Finance" ) 
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					else
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
					*/
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};

		$scope.SetComboBranch();

		$scope.SetDataMainRequest = function (pageNumber, limit, startDate, endDate, Branch) {
			var filter = [{ TanggalAwal: startDate, TanggalAkhir: endDate, Outlet: Branch }];
			InstruksiPenerimaanFactory.getAllDataIncomingInstruction(pageNumber, limit, JSON.stringify(filter))
				.then(
					function (res) {
						$scope.Main_Daftar_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
						$scope.Main_Daftar_UIGrid.totalItems = res.data.Total;		//Total keseluruhan data, diambil dari SP
						$scope.Main_Daftar_UIGrid.paginationPageSize = paginationOptions_Main_Daftar.pageSize;
						$scope.Main_Daftar_UIGrid.paginationPageSizes = [10, 25, 50];
						$scope.Main_Daftar_UIGrid.paginationCurrentPage = paginationOptions_Main_Daftar.pageNumber;
					},
					function (err) {
						console.log("err=>", err);
					}
				)
		};

		$scope.Main_Filter = function () {
			if (($scope.Main_TextFilter != "") &&
				($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != "")) {
				var nomor;
				var myDate;
				var value = $scope.Main_TextFilter;
				$scope.Main_Daftar_gridAPI.grid.clearAllFilters();

				if ($scope.Main_SearchDropdown == "TanggalBankStatement") {
					myDate = new Date(value);
					if (isNaN(myDate)) {
						myDate = new Date(value.substr(6, 4) + "-" + value.substr(3, 2) + "-" + value.substr(0, 2));
					}
					value = $filter('date')(new Date(myDate.toLocaleString()), 'MMM dd yyyy')
				}

				console.log("value : ", value);
				// if ($scope.Main_SearchDropdown == "Nomor Instruksi Bank") {
				// 	nomor = 3;
				// }				
				// if ($scope.Main_SearchDropdown == "Nomor Rekening Bank") {
				// 	nomor =4;
				// }
				// if ($scope.Main_SearchDropdown == "Nomor Rekening Bank") {
				// 	nomor =5;
				// }					
				console.log("kolom : ", $scope.Main_Daftar_gridAPI.grid.getColumn($scope.Main_SearchDropdown));
				$scope.Main_Daftar_gridAPI.grid.getColumn($scope.Main_SearchDropdown).filters[0].term = value;
			}
			else {
				$scope.Main_Daftar_gridAPI.grid.clearAllFilters();
				//alert("inputan filter belum diisi !");
			}
		};

		$scope.Main_Cari = function () {
			if (($scope.Main_TanggalStart != null) && ($scope.Main_TanggalEnd != null)) {
				$scope.SetDataMainRequest(1, uiGridPageSize, $scope.formatDate($scope.Main_TanggalStart),
					$scope.formatDate($scope.Main_TanggalEnd),
					$scope.Main_NamaBranch);
			}
			else {
				//alert('Tanggal Instruksi Bank harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Tanggal Instruksi Bank harus diisi !',
					type: 'warning'
				});
			}
		};

		$scope.ToEditInstruksiPenerimaan = function () {
			$scope.EditMode = true;
			$scope.LihatInstruksiEditMode = true;
			$scope.PrepareGridDaftarTransaksiLayout('edit');
		};

		$scope.ShowLihatDetailFromMain = function (row) {
			$scope.ShowLihatInstruksiPenerimaan = true;
			$scope.Show_Main_DaftarList = false;
			$scope.ShowTambahInstruksiPenerimaan = false;
			$scope.EditMode = false;
			$scope.LihatInstruksiEditMode = false;

			InstruksiPenerimaanFactory.getDataBranch().then(
				function (res) {
					$scope.optionsLihat_NamaBranch = res.data.Result;
					//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch = branch;
				},
				function (err) {
					console.log('error -->', err)
				}
			);
			$scope.IncomingInstructionId = row.entity.IncomingInstructionId;
			//$scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanTextFilter = "";
			$scope.LihatInstruksiPenerimaan_NomorRekeningBank = "";
			$scope.LihatInstruksiPenerimaan_SaldoAwal = "";
			$scope.LihatInstruksiPenerimaan_TanggalBankStatement = "";
			$scope.LihatInstruksiPenerimaan_SaldoAkhir = "";
			// $scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = row.entity.LastEndBalance;
			$scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = "";
			$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = [];
			$scope.PrepareGridDaftarTransaksiLayout('lihat');
			$scope.PrepareGridDaftarTransaksi("lihat", 1, row.entity.IncomingInstructionId);
			$scope.PrepareDataInstruksi("lihat", row.entity.IncomingInstructionId);

			$scope.InstruksiPenerimaan_IsReconActive = row.entity.ReconActive;
		};

		// $scope.DataBank = function(){
		// 	InstruksiPenerimaanFactory.getDataBank($scope.TambahInstruksiPenerimaan_NomorRekening).then(
		// 		function(res){
		// 			$scope.dataBank = res.data.Result;
		// 			for (var i=0; i<$scope.dataBank.length;i++){
		// 				if ($scope.dataBank[i].isActivateCent == true){
		// 					substring
		// 				}
		// 			}
		// 		},function(err){
		// 			console.log('error -->', err)
		// 		}
		// 	);
		// }
		$scope.BrowseFileUpload = function () {
			var reader = new FileReader();
			// $scope.DataBank();
			reader.onload = function (e) {
				$scope.ParseMT(reader.result);
			};
			//reader.readAsText($scope.file); 
			reader.readAsDataURL($scope.file);
		}

		$scope.ParseMT = function (isiMT) {
			//alert('isi data -> '+ isiMT);
			var norek = $scope.TambahInstruksiPenerimaan_NomorRekening;
			console.log('isi rek', $scope.TambahInstruksiPenerimaan_NomorRekening);
			if ((norek == null) ||
				(norek == '') ||
				(norek == undefined) ||
				(angular.isUndefined(norek)) ||
				(norek == []) ||
				(norek == {}) ||
				(!angular.isNumber(norek))

			) {
				//alert('Nomor Rekening harus diisi dulu ');
				bsNotify.show({
					title: "Warning",
					content: 'Nomor Rekening harus diisi terlebih dahulu',
					type: 'warning'
				});
				return false;
			}
			else {
				var isiBase64 = isiMT.split("base64,")[1];
				var closing;
				var opening;
				var nom;
				var strclosing;
				var stropening;
				var strNom;
				InstruksiPenerimaanFactory.parseDataMT($scope.TambahInstruksiPenerimaan_NomorRekening, isiBase64).then(
					function (res) {
						console.log("data res 1", res.data.Result)
						// var cocok = false;
						// var AccountNumber = res.data.Result[0].AccountNumber;
						// angular.forEach($scope.options_InstruksiPenerimaanNomorRekening, function (value, key) {
						// 	if (!cocok) {
						// 		if (value.name == AccountNumber && value.value == $scope.TambahInstruksiPenerimaan_NomorRekening) {
						// 			cocok = true;
						// 		}
						// 	}
						// });

						// if (!cocok) {
						// 	bsNotify.show({
						// 		title: "Warning",
						// 		content: 'Nomor Rekening yang dipilih tidak sesuai dengan Nomor Rekening pada file MT940',
						// 		type: 'warning'
						// 	});
						// 	return false;
						// }
						//CR4 P2 Finance Start
						$scope.AccountNumber;
						if($scope.TambahInstruksiPenerimaan_Command == "Upload Bank Statement"){
							console.log('TambahInstruksiPenerimaan_NomorRekening',$scope.TambahInstruksiPenerimaan_NomorRekening)
							for(var x in $scope.options_InstruksiPenerimaanNomorRekening){
								console.log('value',$scope.options_InstruksiPenerimaanNomorRekening[x].value,$scope.TambahInstruksiPenerimaan_NomorRekening)
								if($scope.options_InstruksiPenerimaanNomorRekening[x].value == $scope.TambahInstruksiPenerimaan_NomorRekening){
									var Norekening = $scope.options_InstruksiPenerimaanNomorRekening[x].name.replace(/[^0-9]/g,'');
									if(res.data.Result[0].AccountNumber == Norekening){
										$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = res.data.Result;
										$scope.AccountNumber = Norekening;
										break;
									}else{
										$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = [];
										bsNotify.show({
											title: "Warning",
											content: 'Nomor rekening bank yang diupload tidak sesuai dengan nomor rekening bank yang anda pilih',
											type: 'warning'
										});
										return false;
									}
								}
							}
						}
						//CR4 P2 Finance End

						InstruksiPenerimaanFactory.getDataBank($scope.TambahInstruksiPenerimaan_NomorRekening).then(
							function(res2){
								$scope.dataBank = res2.data.Result;
								for (var i=0; i<$scope.dataBank.length;i++){
									if ($scope.dataBank[i].isActivateCent == true){
										console.log('isi data --> ', JSON.stringify(res.data));
										if(res.data == "ERROR"){
											bsNotify.show({
												title: "Gagal",
												content: 'Gagal Parse data MT940.',
												type: 'danger'
											});
										}else{
											$scope.TambahInstruksiPenerimaan_TanggalBankStatement = null;
											$scope.TambahInstruksiPenerimaan_SaldoAkhir = 0;
											$scope.TambahInstruksiPenerimaan_SaldoAwal = 0;
											if (res.data.Result.length > 0) {
												var tanggal = new Date(res.data.Result[0].BankStatementDate);
												newtanggal = $scope.changeFormatDate(tanggal);
												// tanggal.setMinutes(tanggal.getMinutes() + 420);
												$scope.TambahInstruksiPenerimaan_TanggalBankStatement =newtanggal;
												$scope.newTglBankStatement = $scope.TambahInstruksiPenerimaan_TanggalBankStatement;
												console.log("cek tanggal", $scope.TambahInstruksiPenerimaan_TanggalBankStatement)
												console.log('closing ', res.data.Result[0].ClosingBalance.toString());
												console.log('OpeningBalance ', res.data.Result[0].OpeningBalance);
												InstruksiPenerimaanFactory.getDataBankAccount($scope.TambahInstruksiPenerimaan_NomorRekening + '|' + $scope.TambahInstruksiPenerimaan_TanggalBankStatement)
												.then(
													function(res){
														$scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya = addSeparatorsNF(res.data.Result[0].LastEndBalance,  '.', '.', ',');
													}
												);
												closing = res.data.Result[0].ClosingBalance.toString();
												opening = res.data.Result[0].OpeningBalance.toString();
												// nom = res.data.Result[0].Nominal.toString();
												// console.log("nom",nom)
												$scope.TambahInstruksiPenerimaan_SaldoAkhir = addSeparatorsNF(closing, '.', '.', ',');
												$scope.TambahInstruksiPenerimaan_SaldoAwal = addSeparatorsNF(opening, '.', '.', ',');
												
												// strclosing = closing.substring(0, closing.length -2); // gak jadi motong 2 digit belakang
												// stropening = opening.substring(0, opening.length -2); // gak jadi motong 2 digit belakang
												// strNom = nom.substring(0, nom.length -2); // gak jadi motong 2 digit belakang
												console.log("strNom", strNom)
												for (var k=0; k<res.data.Result.length; k++){
													// res.data.Result[k].Nominal = strNom; // gak jadi motong 2 digit belakang
												nom = res.data.Result[k].Nominal.toString();
												res.data.Result[k].Nominal = nom;
												}
												console.log("test cek data result", res.data.Result)
												// $scope.TambahInstruksiPenerimaan_SaldoAkhir = addSeparatorsNF(strclosing, '.', '.', ','); // gak jadi motong 2 digit belakang
												// $scope.TambahInstruksiPenerimaan_SaldoAwal = addSeparatorsNF(stropening, '.', '.', ','); // gak jadi motong 2 digit belakang

											}
											
										}
									}else{
										console.log('isi data --> ', JSON.stringify(res.data));
										if(res.data == "ERROR"){
											bsNotify.show({
												title: "Gagal",
												content: 'Gagal Parse data MT940.',
												type: 'danger'
											});
										}else{
											$scope.TambahInstruksiPenerimaan_TanggalBankStatement = null;
											$scope.TambahInstruksiPenerimaan_SaldoAkhir = 0;
											$scope.TambahInstruksiPenerimaan_SaldoAwal = 0;
											if (res.data.Result.length > 0) {
												var tanggal = new Date(res.data.Result[0].BankStatementDate);
												newtanggal = $scope.changeFormatDate(tanggal);
												// tanggal.setMinutes(tanggal.getMinutes() + 420);
												$scope.TambahInstruksiPenerimaan_TanggalBankStatement =newtanggal;
												$scope.newTglBankStatement = $scope.TambahInstruksiPenerimaan_TanggalBankStatement;
												console.log("cek tanggal", $scope.TambahInstruksiPenerimaan_TanggalBankStatement)
												console.log('closing ', res.data.Result[0].ClosingBalance);
												console.log('OpeningBalance ', res.data.Result[0].OpeningBalance);
												closing = res.data.Result[0].ClosingBalance;
												opening = res.data.Result[0].OpeningBalance;
												$scope.TambahInstruksiPenerimaan_SaldoAkhir = addSeparatorsNF(closing, '.', '.', ',');
												$scope.TambahInstruksiPenerimaan_SaldoAwal = addSeparatorsNF(opening, '.', '.', ',');
												
											}
											$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = res.data.Result;
										}
									}
								}


							},function(err){
								console.log('error -->', err)
							}
						);
						
						
						
					},
					function (err) {
						//alert('Gagal Parse data MT940 !');
						bsNotify.show({
							title: "Gagal",
							content: 'Gagal Parse data MT940.',
							type: 'danger'
						});
						console.log('err ', err);
					}
				);
			}
		}

		$scope.PrepareDataInstruksi = function (mode, id) {
			InstruksiPenerimaanFactory.getDataIncomingInstruction(id).then(
				function (res) {
					//alert('masuk sini loh');
					//console.log('isi ', JSON.stringify(res));
					var tanggal = new Date(res.data[0].IncomingDate);
					tanggal.setMinutes(tanggal.getMinutes() + 420);
					$scope.LihatInstruksiPenerimaan_TanggalInstruksiPenerimaan = tanggal;
					$scope.LihatInstruksiPenerimaan_NomorInstruksiBank = res.data[0].IncomingInstructionNumber;
					$scope.Lihat_NamaBranch = res.data[0].OutletId;
					$scope.LihatInstruksiPenerimaan_NomorRekeningBank = res.data[0].Account;
					$scope.LihatInstruksiPenerimaan_AccountNumber = res.data[0].AccNumber;
					$scope.currentOutletId = res.data[0].OutletId;
					$scope.LihatInstruksiPenerimaan_SaldoAwal = addSeparatorsNF(res.data[0].BeginBalance, '.', '.', ',');
					$scope.LihatInstruksiPenerimaan_SaldoAkhir = addSeparatorsNF(res.data[0].EndBalance, '.', '.', ',');
					var tanggal2 = new Date(res.data[0].BankStatementDate);
					tanggal2.setMinutes(tanggal2.getMinutes() + 420);
					$scope.LihatInstruksiPenerimaan_TanggalBankStatement = tanggal2;
					$scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = addSeparatorsNF(res.data[0].LastEndBalance, '.', '.', ',');
					console.log("awal>>>>>>>>>",$scope.LihatInstruksiPenerimaan_SaldoAwal)
					console.log("awal>>>>>>>>>",$scope.LihatInstruksiPenerimaan_SaldoAkhir)

				},
				function (err) {
					console.log('Error ', err);
				}
			);
		};

		$scope.formatDate = function (date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('');
		};

		$scope.SetDataGridToText = function (NomorRekeningBank, saldoawal, saldoakhir, saldoakhirSebelum, Tanggal) {
			$scope.LihatInstruksiPenerimaan_NomorRekeningBank = NomorRekeningBank;
			$scope.LihatInstruksiPenerimaan_SaldoAwal = addSeparatorsNF(saldoawal, '.', '.', ',');
			$scope.LihatInstruksiPenerimaan_TanggalBankStatement = new Date(Tanggal);
			$scope.LihatInstruksiPenerimaan_SaldoAkhir = addSeparatorsNF(saldoakhir, '.', '.', ',');
			$scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = saldoakhirSebelum;
		};


		$scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual_SelectedValue = function () {
			//alert($scope.TambahInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Nomor_Rekening_Bank);
			$scope.LihatInstruksiPenerimaan_NomorRekeningBank = $scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Nomor_Rekening_Bank;
			$scope.LihatInstruksiPenerimaan_SaldoAwal =
				addSeparatorsNF($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Saldo_Awal, '.', '.', ',');

			$scope.LihatInstruksiPenerimaan_TanggalBankStatement = new Date($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Tanggal_Bank_Statement);
			$scope.LihatInstruksiPenerimaan_SaldoAkhir =
				addSeparatorsNF($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Saldo_Akhir, '.', '.', ',');

			$scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = $scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanIndividual.Saldo_Akhir_Sebelumnya;
			$scope.LihatInstruksiPenerimaan_DaftarTransaksiToRepeat = DaftarTransaksiFactory.getData();
		};

		$scope.ToTambahInstruksiPenerimaan = function () {
			$scope.ShowLihatInstruksiPenerimaan = false;
			$scope.ShowTambahInstruksiPenerimaan = true;
			$scope.Show_Main_DaftarList = false;
			$scope.options_InstruksiPenerimaanDebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
			$scope.options_InstruksiPenerimaanAssignment = [{ name: "Incoming Payment", value: "Incoming Payment" }, { name: "Unknown", value: "Unknown" }, { name: "Card", value: "Card" }, { name: "Transaksi Cash & Bank", value: "Transaksi Cash & Bank" }, { name: "Outgoing Payment", value: "Outgoing Payment" }];
			$scope.options_InstruksiPenerimaanTipeTransaksi = [{ name: "INT", value: "INT" }, { name: "CHG", value: "CHG" }, { name: "TRF", value: "TRF" }, { name: "TAX", value: "TAX" }];
			$scope.TambahInstruksiPenerimaan_TipeTransaksi = {};

			$scope.ShowTambahInstruksiPenerimaan_Assignment = false;
			$scope.ShowTambahInstruksiPenerimaan_Keterangan = false;
			$scope.UploadSelected = false;
			$scope.TambahInstruksiPenerimaan_Command = "";
			$scope.TambahInstruksiPenerimaan_NamaFile = "";
			$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = [];
			//$scope.PrepareGridDaftarTransaksiLayout('tambah');
			$scope.TambahInstruksiPenerimaan_TanggalInstruksiPenerimaan = new Date();
			InstruksiPenerimaanFactory.getDataBankAccount(0)
				.then(
					function (res) {
						$scope.options_InstruksiPenerimaanNomorRekening = res.data.Result;
						$scope.TambahInstruksiPenerimaan_NomorRekening = {};
						$scope.AccountData = res.data.Result;
					},
					function (err) {
						console.log("err=>", err);
					}
				);
			$scope.SetComboBranch();
			//$scope.TambahInstruksiPenerimaan_DaftarTransaksiToRepeat=DaftarTransaksiFactory.getData();
		};

		$scope.TambahInstruksiPenerimaan_TipeTransaksi_SelectedChanged = function () {
			$scope.options_InstruksiPenerimaanDebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
			$scope.TambahInstruksiPenerimaan_DebetKredit = "";
			if ($scope.TambahInstruksiPenerimaan_TipeTransaksi == "TRF") {
				$scope.ShowTambahInstruksiPenerimaan_Assignment = true;
				$scope.ShowTambahInstruksiPenerimaan_Keterangan = true;

			}
			else {
				$scope.ShowTambahInstruksiPenerimaan_Assignment = false;
				$scope.ShowTambahInstruksiPenerimaan_Keterangan = false;
			}

			if($scope.TambahInstruksiPenerimaan_TipeTransaksi == "CHG" || $scope.TambahInstruksiPenerimaan_TipeTransaksi == "TAX"){
				$scope.options_InstruksiPenerimaanDebetKredit = [{ name: "Debet", value: "Debet" }];//, { name: "Kredit", value: "Kredit" }];
			}else if($scope.TambahInstruksiPenerimaan_TipeTransaksi == "INT"){
				$scope.options_InstruksiPenerimaanDebetKredit = [{ name: "Kredit", value: "Kredit" }];
			}else{
				$scope.options_InstruksiPenerimaanDebetKredit = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
			}
		};

		//awal baru Ditambahkan 
		$scope.TanggalStatementChanged = function () {
			if(($scope.TambahInstruksiPenerimaan_NomorRekening != null) && ($scope.TambahInstruksiPenerimaan_TanggalBankStatement != undefined) ){
				$scope.GetDataNominal();
			}else{
				console.log("err=>", err);
			}
		}

		$scope.GetDataNominal = function (){
			var newTgl = "";
			newTgl =  $scope.changeFormatDate( $scope.TambahInstruksiPenerimaan_TanggalBankStatement); 
			$scope.newTglBankStatement =  $scope.changeFormatDate( $scope.TambahInstruksiPenerimaan_TanggalBankStatement); 
			// newTgl = $scope.TambahInstruksiPenerimaan_TanggalBankStatement.toString().replace(/\//g, '-');
			console.log("newTGL >>>>>>", newTgl);
			InstruksiPenerimaanFactory.getDataBankAccount($scope.TambahInstruksiPenerimaan_NomorRekening + '|' + newTgl)
			.then(
				function(res){
					$scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya = addSeparatorsNF(res.data.Result[0].LastEndBalance,  '.', '.', ',');
				}
			);
		}

		$scope.TambahInstruksiPenerimaan_TanggalBankStatement_Disable = true;

		$scope.TambahInstruksiPenerimaan_NomorRekening_Changed = function () {
			$scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya = 0;
			$scope.TambahInstruksiPenerimaan_TanggalBankStatement = "";
			for (var i = 0; i < $scope.AccountData.length; i++) {
				if ($scope.AccountData[i].value == $scope.TambahInstruksiPenerimaan_NomorRekening) {
					$scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya = $scope.AccountData[i].LastEndBalance;
					$scope.TambahInstruksiPenerimaan_TanggalBankStatement_Disable = false;
					break;
				}
				$scope.TambahInstruksiPenerimaan_TanggalBankStatement_Disable = true;
			}
		}

		$scope.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			console.log("changeFormatDate item", item);
			tmpAppointmentDate = new Date(tmpAppointmentDate);
			var finalDate
			if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
				var yyyy = tmpAppointmentDate.getFullYear().toString();
				var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
				var dd = tmpAppointmentDate.getDate().toString();
				finalDate = (yyyy + '-' +(mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]));
			} else {
				finalDate = '';
			}
			console.log("changeFormatDate finalDate", finalDate);
			return finalDate;
		};

		//akhir baru ditambah

		function addCommas(nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}


		$scope.TambahInstruksiPenerimaan_Command_Reset = function () {
			$scope.TambahInstruksiPenerimaan_UploadSection = false;
			$scope.TambahInstruksiPenerimaan_DataTransaksi = false;
			$scope.TambahInstruksiPenerimaan_TipeTransaksi = "";
			$scope.TambahInstruksiPenerimaan_Assignment = "";
			$scope.TambahInstruksiPenerimaan_Nominal = "";
			$scope.TambahInstruksiPenerimaan_Keterangan = "";
			$scope.TambahInstruksiPenerimaan_DebetKredit = "";
			$scope.UploadSelected = false;
		};

		$scope.TambahInstruksiPenerimaan_Command_ValueChanged = function () {
			$scope.TambahInstruksiPenerimaan_Command_Reset();

			if ($scope.TambahInstruksiPenerimaan_Command == "Upload Bank Statement") {
				$scope.TambahInstruksiPenerimaan_UploadSection = true;
				$scope.UploadSelected = true;
				$scope.TambahInstruksiPenerimaan_SaldoAwal_Disable = true;
				$scope.TambahInstruksiPenerimaan_SaldoAkhir_Disable = true;
				$scope.TambahInstruksiPenerimaan_TanggalBankStatement_Disable = true;

				$scope.PrepareGridDaftarTransaksiLayout('upload');
			}
			else if ($scope.TambahInstruksiPenerimaan_Command == "Input Data Transaksi") {
				$scope.TambahInstruksiPenerimaan_DataTransaksi = true;
				$scope.TambahInstruksiPenerimaan_SaldoAwal_Disable = false;
				$scope.TambahInstruksiPenerimaan_SaldoAkhir_Disable = false;
				$scope.TambahInstruksiPenerimaan_TanggalBankStatement_Disable = false;
				$scope.PrepareGridDaftarTransaksiLayout('input');
			}

		};

		$scope.BackToMainInstruksiPenerimaan = function () {
			$scope.ShowTambahInstruksiPenerimaan = false;
			$scope.ShowLihatInstruksiPenerimaan = false;
			$scope.Show_Main_DaftarList = true;
			$scope.TambahInstruksiPenerimaan_Command_Reset();
			$scope.IncomingInstructionId = 0;
			$scope.EditMode = false;
			$scope.LihatInstruksiEditMode = false;

			//$scope.options_DaftarInstruksiPenerimaanSearch = [{ name: "Nomor Instruksi Penerimaan", value: "Nomor Instruksi Penerimaan" }, { name: "Nomor Rekening Bank", value: "Nomor Rekening Bank" },{ name: "Tanggal Bank Statement", value: "Tanggal Bank Statement" } ];

			//$scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanToRepeat="";
			$scope.LihatInstruksiPenerimaan_NomorRekeningBank = "";
			$scope.LihatInstruksiPenerimaan_AccountNumber = "";
			$scope.LihatInstruksiPenerimaan_SaldoAwal = "";
			$scope.LihatInstruksiPenerimaan_TanggalBankStatement = "";
			$scope.LihatInstruksiPenerimaan_SaldoAkhir = "";
			$scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya = "";
			//$scope.LihatInstruksiPenerimaan_DaftarTransaksiToRepeat="";
			$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = [];
			$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = [];
			// $scope.Main_Daftar_UIGrid.data = [];
			$scope.InstruksiPenerimaan_DaftarMasterInstruksiPenerimaan_UIGrid.data = [];

			$scope.TambahInstruksiPenerimaan_Command = "";
			$scope.TambahInstruksiPenerimaan_TanggalBankStatement = "";
			$scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya = "";
			$scope.TambahInstruksiPenerimaan_SaldoAwal = "";
			$scope.TambahInstruksiPenerimaan_SaldoAkhir = "";
			$scope.TambahInstruksiPenerimaan_TipeTransaksi = "";
			$scope.TambahInstruksiPenerimaan_Assignment = "";
			$scope.TambahInstruksiPenerimaan_Nominal = "";
			$scope.TambahInstruksiPenerimaan_Keterangan = "";
			$scope.TambahInstruksiPenerimaan_DebetKredit = "";
		};
		//
		$scope.LihatInstruksiPenerimaan_Pembatalan_Click = function () {
			if ($scope.IncomingInstructionId != 0) {
				InstruksiPenerimaanFactory.CheckValidReversal($scope.IncomingInstructionId, $scope.user.OutletId)
					.then(
						function (res) {
							console.log("Cek Split>>>>>", res.data.Result[0].FileName.split(" ")[3]);
							if (res.data.Result[0].FileName == "Recon") {
								//alert('Data Instruksi Bank sudah di Reconcile');
								bsNotify.show({
									title: "Gagal",
									content: 'Data Instruksi Bank sudah di Reconcile.',
									type: 'danger'
								});
							}
							else if(res.data.Result[0].FileName.split(" ")[3] == "Closing."){
								bsNotify.show({
									title: "Gagal",
									content: res.data.Result[0].FileName,
									type: 'danger'
								});
							}
							else if(res.data.Result[0].FileName == "Closing"){
								bsNotify.show({
									title: "Gagal",
									content: 'Data Instruksi Bank sudah di Closing.',
									type: 'danger'
								});
							}
							else {
								angular.element('#ModalLihatInstruksiPenerimaan_Pembatalan').modal('show');
							}
						},
						function (err) {
							console.log("err=>", err);
						}
					);
			}
			else {
				//alert('Belum ada data Instruksi yang dipilih untuk dibatalkan');
				bsNotify.show({
					title: "Gagal",
					content: 'Belum ada data Instruksi yang dipilih untuk dibatalkan.',
					type: 'danger'
				});
			}
		};

		$scope.Tidak_LihatInstruksiPenerimaan_PembatalanModal = function () {
			angular.element('#ModalLihatInstruksiPenerimaan_Pembatalan').modal('hide');
		};

		$scope.Ya_LihatInstruksiPenerimaan_PembatalanModal = function () {
			angular.element('#ModalLihatInstruksiPenerimaan_Pembatalan').modal('hide');

			InstruksiPenerimaanFactory.CancelData($scope.IncomingInstructionId)
				.then(
					function (res) {
						//alert('dibatalkan');
						bsNotify.show({
							title: "Batal",
							content: 'Dibatalkan.',
							type: 'danger'
						});
						$scope.Main_Cari();
						$scope.BackToMainInstruksiPenerimaan();
					},
					function (err) {
						console.log("err=>", err);
					}
				)
		};

		// $scope.TambahInstruksiPenerimaan_TambahDaftarTransaksi = function () {
		// 	if($scope.TambahInstruksiPenerimaan_TipeTransaksi=="TRF")
		// 	{
		// 		$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data.push(
		// 			{ Tipe_Transaksi: $scope.TambahInstruksiPenerimaan_TipeTransaksi,DebetKredit: $scope.TambahInstruksiPenerimaan_DebetKredit,
		// 				Nominal: $scope.TambahInstruksiPenerimaan_Nominal,Assignment: $scope.TambahInstruksiPenerimaan_Assignment,
		// 				Keterangan: $scope.TambahInstruksiPenerimaan_Keterangan });
		// 	}
		// 	else
		// 	{
		// 		$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data.push({ Tipe_Transaksi: $scope.TambahInstruksiPenerimaan_TipeTransaksi,DebetKredit: $scope.TambahInstruksiPenerimaan_DebetKredit,Nominal: $scope.TambahInstruksiPenerimaan_Nominal,Assignment: "-",Keterangan: "-" });
		// 	}
		// 	$scope.TambahInstruksiPenerimaan_TipeTransaksi={};
		// 	$scope.TambahInstruksiPenerimaan_TipeTransaksi="";
		// 	$scope.TambahInstruksiPenerimaan_Assignment="";
		// 	$scope.TambahInstruksiPenerimaan_Nominal="";
		// 	$scope.TambahInstruksiPenerimaan_Keterangan="";
		// 	$scope.TambahInstruksiPenerimaan_DebetKredit="";
		// };

		// $scope.TambahInstruksiPenerimaan_Simpan = function () {
		// 	alert('simpan');
		// 	$scope.BackToMainInstruksiPenerimaan();
		//TambahInstruksiPenerimaan_Simpa
		// };

		$scope.SetComboBranch = function () {
			InstruksiPenerimaanFactory.getDataBranch().then(
				function (res) {
					$scope.optionsMain_NamaBranch = res.data.Result;
					//$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch = branch;
					$scope.Main_NamaBranch = $scope.user.OutletId;
					$scope.Main_NamaBranch_EnableDisable = false;
					/*
					if (CurrentUser.role == "Finance" ) 
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = true;
					else
						$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch_EnableDisable = false;
					*/
				},
				function (err) {
					console.log('error -->', err)
				}
			);
		};
		$scope.SetComboBranch();

		// $scope.LihatInstruksiPenerimaan_Filter = function() {
		// 	if ( ($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanTextFilter != "") &&
		// 		($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanSearch != "") )
		// 	{
		// 		var nomor;		
		// 		if ($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanSearch == "Nomor Instruksi Penerimaan") {
		// 			nomor = 0;
		// 		}
		// 		if ($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanSearch == "Nomor Rekening Bank") {
		// 			nomor = 2;
		// 		}
		// 		if ($scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanSearch == "Tanggal Bank Statement") {
		// 			nomor = 3;
		// 		}			
		// 		console.log("nomor --> ", nomor);
		// 		$scope.DaftarMasterInstruksiPenerimaan_gridAPI.grid.columns[nomor].filters[0].term=$scope.LihatInstruksiPenerimaan_DaftarInstruksiPenerimaanTextFilter;
		// 	}
		// 	else {
		// 		alert("inputan filter belum diisi !");
		// 	}
		// };

		var paginationOptions_EditDaftarTransaksi = {
			pageNumber: 1,
			pageSize: 10,
			sort: null
		}

		$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 25, 50],
			useCustomPagination: false,
			useExternalPagination: false,
			enableSelectAll: false,
			enableColumnResizing: true,
			multiselect: false,
			canSelectRows: true,
			cellEditableCondition: function ($scope) {
				//alert(' isi trf -->' + $scope.row.entity.TransactionType );
				return ($scope.row.entity.TransactionType == "TRF");
				//return $scope.TransactionCanEdit($scope.row);
			},
			columnDefs: [
				{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
				{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableCellEdit: false, enableHiding: false },
				{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
				{
					name: "Assignment", displayName: "Assignment", field: "Assignment", enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', enableHiding: false,
					editDropdownValueLabel: 'Assignment', editDropdownOptionsArray: [
						{ id: 'Incoming Payment', Assignment: 'Incoming Payment' },
						{ id: 'Unknown', Assignment: 'Unknown' },
						{ id: 'Card', Assignment: 'Card' },
						{ id: 'Transaksi Cash & Bank', Assignment: 'Transaksi Cash & Bank' },
						{ id: "Outgoing Payment", Assignment: "Outgoing Payment" }
					],
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}
				},
				//{name:"Assignment", displayName:"Assignment", field:"Assignment", enableCellEdit: true, enableHiding: false},				
				{
					name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: true, enableHiding: false,
					cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
						return 'canEdit';
					}
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.InstruksiPenerimaan_EditDaftarTransaksi_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					//console.log("masuk setting grid upload dan tambah");
					//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
					paginationOptions_EditDaftarTransaksi.pageNumber = pageNumber;
					paginationOptions_EditDaftarTransaksi.pageSize = pageSize;
				});
			}
		};

		var paginationOptions_DaftarTransaksi = {
			pageNumber: 1,
			pageSize: 10,
			sort: null
		}
		$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid = {
			paginationPageSize: 10,
			paginationPageSizes: [10, 15, 25],
			useCustomPagination: false,
			useExternalPagination: false,
			enableSelectAll: false,
			multiselect: false,
			canSelectRows: true,
			enableColumnResizing: true,
			// cellEditableCondition: function(row) {
			//       return $scope.TransactionCanEdit(row);
			//  },
			columnDefs: [
				{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
				{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableHiding: false },
				{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableHiding: false },
				{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableHiding: false, cellFilter: 'rupiahC' },
				{ name: "Assignment", displayName: "Assignment", field: "Assignment", enableHiding: false },
				{ name: "Keterangan", displayName: "Keterangan", field: "Description", enableHiding: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.InstruksiPenerimaan_DaftarTransaksi_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					//console.log("masuk setting grid upload dan tambah");
					//$scope.PrepareGridDaftarTransaksi('lihat', pageNumber,$scope.IncomingInstructionId );	//Panggil function untuk set ulang data setiap perubahan page
					paginationOptions_DaftarTransaksi.pageNumber = pageNumber;
					paginationOptions_DaftarTransaksi.pageSize = pageSize;
				});
			}
		};

		var paginationOptions_TambahDaftarTransaksi = {
			pageNumber: 1,
			pageSize: 10,
			sort: null
		}
		$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid = {
			paginationPageSizes: [10, 15, 25],
			useCustomPagination: false,
			useExternalPagination: false,
			enableSelectAll: false,
			enableColumnResizing: true,
			multiselect: false,
			cellEditableCondition: function ($scope) {
				//alert(' isi trf -->' + $scope.row.entity.TransactionType );
				return ($scope.row.entity.TransactionType == "TRF");
				//return $scope.TransactionCanEdit($scope.row);
			},
			columnDefs: [
				{ name: "NomorInstruksi", displayName: "Nomor Instruksi Penerimaan", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false }
			],
			onRegisterApi: function (gridApi) {
				$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid_gridAPI = gridApi;
				gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
					//console.log("masuk setting grid upload dan tambah");
					//$scope.PrepareGridDaftarTransaksi('tambah', pageNumber,$scope.IncomingInstructionId );
					//Panggil function untuk set ulang data setiap perubahan page
					paginationOptions_TambahDaftarTransaksi.pageNumber = pageNumber;
					paginationOptions_TambahDaftarTransaksi.pageSize = pageSize;
				});
			}
		};

		$scope.TransactionCanEdit = function (row) {
			if (row.entity.TransactionType == "TRF")
				return true;
			else
				return false;
		}

		$scope.PrepareGridDaftarTransaksiLayout = function (layout) {
			var data = $scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data;
			$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = [];
			$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = [];
			$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.columnDefs = [];
			$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.columnDefs = [];
			//$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = [];
			//console.log('set layout -->',layout);
			$scope.MetodeInput = layout;
			if (layout == 'upload') {
				$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.columnDefs = [
					{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
					{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableCellEdit: false, enableHiding: false },
					{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
					{
						name: "Assignment", displayName: "Assignment", field: "Assignment", enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', enableHiding: false,
						editDropdownValueLabel: 'Assignment', editDropdownOptionsArray: [
							{ id: 'Incoming Payment', Assignment: 'Incoming Payment' },
							{ id: 'Unknown', Assignment: 'Unknown' },
							{ id: 'Card', Assignment: 'Card' },
							{ id: 'Transaksi Cash & Bank', Assignment: 'Transaksi Cash & Bank' }
							, { id: "Outgoing Payment", Assignment: "Outgoing Payment" }
						],
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}
					},
					{
						name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: true, enableHiding: false,
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}
					}
					//{name:"Status",displayName:"Status", field:"StatusCode",enableCellEdit: false},
					//{name:"Action", displayName:"Action",enableCellEdit: false}
				];
			}
			else if (layout == 'lihat') {
				$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.columnDefs = [
					{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
					{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableCellEdit: false, enableHiding: false },
					{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
					{ name: "Assignment", displayName: "Assignment", field: "Assignment", enableCellEdit: false, enableHiding: false },
					{ name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: false, enableHiding: false }
				];
			}
			else if (layout == 'edit') {
				$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.columnDefs = [
					{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
					{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableCellEdit: false, enableHiding: false },
					{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
					// {name:"Assignment", displayName:"Assignment", field:"Assignment", enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor',enableHiding: false,
					// 	editDropdownValueLabel: 'Assignment', editDropdownOptionsArray: [
					// 	  { id: 'Incoming Payment', Assignment: 'Incoming Payment' },
					// 	  { id: 'Unknown', Assignment: 'Unknown' },
					// 	  { id: 'Card', Assignment: 'Card' },
					// 	  { id: 'Transaksi Cash & Bank', Assignment: 'Transaksi Cash & Bank' }
					// 	]
					// },
					{
						name: "Assignment", displayName: "Assignment", field: "Assignment", enableCellEdit: true, enableHiding: false,
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}
					},
					{
						name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: true, enableHiding: false,
						cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
							return 'canEdit';
						}
					}

					//{name:"Status",displayName:"Status", field:"StatusCode",enableCellEdit: false},
					//{name:"Action", displayName:"Action",enableCellEdit: false}
				];
				//alert('masuk sini');
				$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = data;
				//$scope.InstruksiPenerimaan_DaftarTransaksi_gridAPI.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
				//console.log(' isi data ', JSON.stringify(data));
				// for(i = 0; i < data.length; i++)
				// {
				// 	$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data.push({
				// 		Id: data[0].Id,
				// 		TransactionType : data[0].TransactionType,
				// 		DebitCredit : data[0].DebitCredit,
				// 		Nominal : data[0].Nominal,
				// 		Assignment : data[0].Assignment,
				// 		Description : "su su" //data[0].Keterangan
				// 	});
				// }
			}
			else //tambah
			{
				$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.columnDefs = [
					{ name: "Id", field: "IncomingInstructionDetailId", visible: false },
					{ name: "TipeTransaksi", displayName: "Tipe Transaksi", field: "TransactionType", enableCellEdit: false, enableHiding: false },
					{ name: "DebetKredit", displayName: "Debit / Kredit", field: "DebitCredit", enableCellEdit: false, enableHiding: false },
					{ name: "Nominal", displayName: "Nominal", field: "Nominal", enableCellEdit: false, enableHiding: false, cellFilter: 'rupiahC' },
					{ name: "Assignment", displayName: "Assignment", field: "Assignment", enableCellEdit: false, enableHiding: false },
					{ name: "Keterangan", displayName: "Keterangan", field: "Description", enableCellEdit: false, enableHiding: false },
					//{name:"Status",displayName:"Status", field:"StatusCode",enableCellEdit: false},
					{
						name: "Action", displayName: "Action", enableCellEdit: false, enableHiding: false,
						cellTemplate: '<a ng-click="grid.appScope.DeleteGridDaftarTransaksiRow(row)">hapus</a>'
					}
				];
			}
		};

		$scope.PrepareGridDaftarTransaksi = function (grid, pageNumber, IncomingInstructionId) {
			InstruksiPenerimaanFactory.getDataIncomingInstructionDetail(pageNumber, uiGridPageSize, IncomingInstructionId)
				.then(
					function (res) {
						console.log('isi ', JSON.stringify(res));
						if (grid == 'tambah') {
							$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
							$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
							$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.paginationPageSize = paginationOptions_TambahDaftarTransaksi.uiGridPageSize;
							$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.paginationPageSizes = [10, 15, 25];
							$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.paginationCurrentPage = paginationOptions_TambahDaftarTransaksi.pageNumber;
						}
						else {
							$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
							$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
							$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.paginationPageSize = uiGridPageSize;
							$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.paginationPageSizes = [10, 15, 25];
							$scope.InstruksiPenerimaan_DaftarTransaksi_UIGrid.paginationCurrentPage = paginationOptions_DaftarTransaksi.pageNumber;

							$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data = res.data.Result;			//Data hasil dari WebAPI
							$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.totalItems = res.data.TotalData;		//Total keseluruhan data, diambil dari SP
							$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.paginationPageSize = uiGridPageSize;
							$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.paginationPageSizes = [10, 15, 25];
							$scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.paginationCurrentPage = paginationOptions_EditDaftarTransaksi.pageNumber;

						}
					},
					function (err) {
						console.log("err=>", err);
					}
				)
		};

		// Inpuut Decimal
		$scope.TambahInstruksiPenerimaan_SaldoAwalBlur = function(){
			console.log("test",$scope.TambahInstruksiPenerimaan_SaldoAwal);
			console.log("test",parseFloat($scope.TambahInstruksiPenerimaan_SaldoAwal).toFixed(2));
			var SisaFloat = "";
			if ($scope.TambahInstruksiPenerimaan_SaldoAwal.substr($scope.TambahInstruksiPenerimaan_SaldoAwal.length-3,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_SaldoAwal.substr($scope.TambahInstruksiPenerimaan_SaldoAwal.length-3);
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal.substr(0,$scope.TambahInstruksiPenerimaan_SaldoAwal.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_SaldoAwal);
			}
			else if ($scope.TambahInstruksiPenerimaan_SaldoAwal.substr($scope.TambahInstruksiPenerimaan_SaldoAwal.length-2,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_SaldoAwal.substr($scope.TambahInstruksiPenerimaan_SaldoAwal.length-2);
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal.substr(0,$scope.TambahInstruksiPenerimaan_SaldoAwal.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_SaldoAwal);
			}
			var val = $scope.TambahInstruksiPenerimaan_SaldoAwal.split(".").join("");

			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1,")
				.split("").reverse().join("");

			var str_array = val.split('.');

			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}

				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}

			if (!NominalValid) {
				$scope.TambahInstruksiPenerimaan_SaldoAwalBlur(val);
			}
			else {
				// $scope.TambahInvoiceMasuk_Nominal = val;
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahInstruksiPenerimaan_SaldoAwal = val+".00";
					$scope.TambahInstruksiPenerimaan_SaldoAwalforParseFloat = val.split(".").join("")+".00";
				}
				else{
					$scope.TambahInstruksiPenerimaan_SaldoAwal = val+SisaFloat;
					$scope.TambahInstruksiPenerimaan_SaldoAwalforParseFloat = val.split(".").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahInstruksiPenerimaan_SaldoAwalforParseFloat);
				}
			}
		}

		$scope.givePatternSaldoAwal = function (item, event) {
			console.log("item", item);
			console.log("event", event.which);
			console.log("event", event);
			if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105)){
				event.preventDefault();
				console.log("disini", event.which);
				console.log("$scope.TambahInstruksiPenerimaan_SaldoAwal", $scope.TambahInstruksiPenerimaan_SaldoAwal);
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal.replace(/[^0-9.]+/g,'');
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal.replace(event.key,"");
				return false;
			} else {
				$scope.TambahInstruksiPenerimaan_SaldoAwal = $scope.TambahInstruksiPenerimaan_SaldoAwal;
				return;
			}
		};

		$scope.TambahInstruksiPenerimaan_NominalBlur = function(){
			console.log("test",$scope.TambahInstruksiPenerimaan_Nominal);
			console.log("test",parseFloat($scope.TambahInstruksiPenerimaan_Nominal).toFixed(2));
			var SisaFloat = "";
			if ($scope.TambahInstruksiPenerimaan_Nominal.substr($scope.TambahInstruksiPenerimaan_Nominal.length-3,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_Nominal.substr($scope.TambahInstruksiPenerimaan_Nominal.length-3);
				$scope.TambahInstruksiPenerimaan_Nominal = $scope.TambahInstruksiPenerimaan_Nominal.substr(0,$scope.TambahInstruksiPenerimaan_Nominal.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_Nominal);
			}
			else if ($scope.TambahInstruksiPenerimaan_Nominal.substr($scope.TambahInstruksiPenerimaan_Nominal.length-2,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_Nominal.substr($scope.TambahInstruksiPenerimaan_Nominal.length-2);
				$scope.TambahInstruksiPenerimaan_Nominal = $scope.TambahInstruksiPenerimaan_Nominal.substr(0,$scope.TambahInstruksiPenerimaan_Nominal.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_Nominal);
			}
			var val = $scope.TambahInstruksiPenerimaan_Nominal.split(".").join("");

			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1,")
				.split("").reverse().join("");

			var str_array = val.split('.');

			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}

				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}

			if (!NominalValid) {
				$scope.TambahInstruksiPenerimaan_NominalBlur(val);
			}
			else {
				// $scope.TambahInvoiceMasuk_Nominal = val;
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahInstruksiPenerimaan_Nominal = val+".00";
					$scope.TambahInstruksiPenerimaan_NominalforParseFloat = val.split(".").join("")+".00";
				}
				else{
					$scope.TambahInstruksiPenerimaan_Nominal = val+SisaFloat;
					$scope.TambahInstruksiPenerimaan_NominalforParseFloat = val.split(".").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahInstruksiPenerimaan_NominalforParseFloat);
				}
			}
		}
		
		$scope.TambahInstruksiPenerimaan_SaldoAkhirBlur = function(){
			console.log("test",$scope.TambahInstruksiPenerimaan_SaldoAkhir);
			console.log("test",parseFloat($scope.TambahInstruksiPenerimaan_SaldoAkhir).toFixed(2));
			var SisaFloat = "";
			if ($scope.TambahInstruksiPenerimaan_SaldoAkhir.substr($scope.TambahInstruksiPenerimaan_SaldoAkhir.length-3,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_SaldoAkhir.substr($scope.TambahInstruksiPenerimaan_SaldoAkhir.length-3);
				$scope.TambahInstruksiPenerimaan_SaldoAkhir = $scope.TambahInstruksiPenerimaan_SaldoAkhir.substr(0,$scope.TambahInstruksiPenerimaan_SaldoAkhir.length-3);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_SaldoAkhir);
			}
			else if ($scope.TambahInstruksiPenerimaan_SaldoAkhir.substr($scope.TambahInstruksiPenerimaan_SaldoAkhir.length-2,1) == "."){
				SisaFloat = $scope.TambahInstruksiPenerimaan_SaldoAkhir.substr($scope.TambahInstruksiPenerimaan_SaldoAkhir.length-2);
				$scope.TambahInstruksiPenerimaan_SaldoAkhir = $scope.TambahInstruksiPenerimaan_SaldoAkhir.substr(0,$scope.TambahInstruksiPenerimaan_SaldoAkhir.length-2);
				console.log("testSisaFloat",SisaFloat);
				console.log("testSisaFloat",$scope.TambahInstruksiPenerimaan_SaldoAkhir);
			}
			var val = $scope.TambahInstruksiPenerimaan_SaldoAkhir.split(".").join("");

			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1,")
				.split("").reverse().join("");

			var str_array = val.split('.');

			var NominalValid = true;
			var satu = 0;
			var dua = 0;
			for (var i = 0; i < str_array.length; i++) {
				if (str_array[i].length == 1) {
					satu = satu + 1;
				}
				else if (str_array[i].length == 2) {
					dua = dua + 1;
				}

				if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
					NominalValid = false;
					break;
				}
			}

			if (!NominalValid) {
				$scope.TambahInstruksiPenerimaan_SaldoAkhirBlur(val);
			}
			else {
				// $scope.TambahInvoiceMasuk_Nominal = val;
				if (SisaFloat == "undefined" || !SisaFloat){
					$scope.TambahInstruksiPenerimaan_SaldoAkhir = val+".00";
					$scope.TambahInstruksiPenerimaan_SaldoAkhirforParseFloat = val.split(".").join("")+".00";
				}
				else{
					$scope.TambahInstruksiPenerimaan_SaldoAkhir = val+SisaFloat;
					$scope.TambahInstruksiPenerimaan_SaldoAkhirforParseFloat = val.split(".").join("") + SisaFloat;
					console.log("testing",val+SisaFloat);
					console.log("testing",$scope.TambahInstruksiPenerimaan_SaldoAkhirforParseFloat);
				}
			}
		}

		$scope.TambahInstruksiPenerimaan_TambahDaftarTransaksi = function () {
			var nominal = $scope.TambahInstruksiPenerimaan_Nominal.toString().replace(/,/g, "");
			
			//onsole.log("data default -> ", $scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data)
			if (($scope.TambahInstruksiPenerimaan_TipeTransaksi != "") &&
				($scope.TambahInstruksiPenerimaan_Nominal != "") && ($scope.TambahInstruksiPenerimaan_DebetKredit != "") && (nominal > 0)) {
				// if (($scope.TambahInstruksiPenerimaan_TipeTransaksi == "TRF") && (
				// 	($scope.TambahInstruksiPenerimaan_Assignment == "") || ($scope.TambahInstruksiPenerimaan_Keterangan == ""))
				// ) {
				// 	//alert('Data Assignment dan Keterangan harus diisi !');
				// 	bsNotify.show({
				// 		title: "Warning",
				// 		content: 'Data Assignment dan Keterangan harus diisi !',
				// 		type: 'warning'
				// 	});
				// }
				// else {
					$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data.push(
						{
							IncomingInstructionDetailId: '0',
							TransactionType: $scope.TambahInstruksiPenerimaan_TipeTransaksi,
							DebitCredit: $scope.TambahInstruksiPenerimaan_DebetKredit,
							Nominal: nominal,
							Assignment: $scope.TambahInstruksiPenerimaan_Assignment,
							Description: $scope.TambahInstruksiPenerimaan_Keterangan
						});
					
					var temp = $scope.TambahInstruksiPenerimaan_SaldoAwal;
					$scope.TambahInstruksiPenerimaan_SaldoAkhir = 0;
					for(var x in $scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data){
						var aa = $scope.TambahInstruksiPenerimaan_SaldoAwal.toString();
						var bb = $scope.TambahInstruksiPenerimaan_SaldoAkhir.toString();
						var cc = $scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[x].Nominal.toString();
						var dd = temp.toString();

						var saldoAwal = parseFloat(aa.replace(/,/g, ''), 10);
						var saldoAkhir = parseFloat(bb.replace(/,/g, ''), 10);
						var nominalF = parseFloat(cc.replace(/,/g, ''), 10);
						var tempF = parseFloat(dd.replace(/,/g, ''), 10);

						console.log('SALDO AKHIR F', saldoAkhir)
						console.log('SALDO AWAL F', saldoAwal)
						console.log('Nominal F', nominalF)
						console.log('Temp F', tempF)
						//debugger;
						
						if($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[x].DebitCredit == 'Kredit'){
							temp = (tempF + nominalF).toString();

						}else if($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[x].DebitCredit == 'Debet'){
							temp = (tempF - nominalF).toString();
						}
					}
					console.log('TEMP', temp)
					var Total = parseFloat(temp.replace(/,/g, ''), 10).toFixed(2);
					console.log('Total', Total)
					$scope.TambahInstruksiPenerimaan_SaldoAkhir = Total;
					$scope.TambahInstruksiPenerimaan_SaldoAkhirBlur();
					
					$scope.TambahInstruksiPenerimaan_TipeTransaksi = "";
					$scope.TambahInstruksiPenerimaan_Assignment = "";
					$scope.TambahInstruksiPenerimaan_Nominal = "";
					$scope.TambahInstruksiPenerimaan_Keterangan = "";
					$scope.TambahInstruksiPenerimaan_DebetKredit = "";
				// }
			}
			else {
				//alert('Semua data harus diisi !');
				bsNotify.show({
					title: "Warning",
					content: 'Semua data harus diisi !',
					type: 'warning'
				});
			}
		};

		$scope.submitData = function (data) {
			InstruksiPenerimaanFactory.submitData(data)
				.then(
					function (res) {
						//$scope.loading=false;
						bsNotify.show({
							title: "Berhasil",
							//content: 'Reversal gagal diajukan, Error --> ', err,
							content: 'Data berhasil disimpan.',
							type: 'success'
						});
						console.log(res);
						$scope.errorSubmit = 0;
						$scope.BackToMainInstruksiPenerimaan();
						$scope.Main_Cari();

					},
					function (err) {
						console.log("err=>", err);
						if (err != null) {
							bsNotify.show({
								title: "Gagal",
								content: err.data.Message,
								type: 'danger'
							});
						}
						$scope.errorSubmit = 1;
					}
				);
		}

		$scope.TambahInstruksiPenerimaan_Simpan = function () {
			var data;
			console.log("cek payment date", $scope.newTglBankStatement);
			console.log("scope.EditMode", $scope.EditMode);

			//Cr4 P2 Finance Start
			console.log('$scope.AccountNumber',$scope.AccountNumber)
			if($scope.TambahInstruksiPenerimaan_Command == "Upload Bank Statement"){
				for(var x in $scope.options_InstruksiPenerimaanNomorRekening){
					console.log('value',$scope.options_InstruksiPenerimaanNomorRekening[x].value,$scope.TambahInstruksiPenerimaan_NomorRekening)
					if($scope.options_InstruksiPenerimaanNomorRekening[x].value == $scope.TambahInstruksiPenerimaan_NomorRekening){
						var Norekening = $scope.options_InstruksiPenerimaanNomorRekening[x].name.replace(/[^0-9]/g,'');
						console.log('valuee',$scope.options_InstruksiPenerimaanNomorRekening[x].name, $scope.AccountNumber)
						if(Norekening != $scope.AccountNumber){
							bsNotify.show({
								title: "Warning",
								content: 'Nomor rekening bank yang diupload tidak sesuai dengan nomor rekening bank yang anda pilih',
								type: 'warning'
							});
							return false;
						}
					}
				}
			}
			//Cr4 P2 Finance End

			if ($scope.EditMode == true) {
				console.log('masuk sini1');
				var lihatawalsaldo = parseFloat($scope.LihatInstruksiPenerimaan_SaldoAwal.replace(/\,/g,""));
				var lihatakhirsaldo = parseFloat($scope.LihatInstruksiPenerimaan_SaldoAkhir.replace(/\,/g,""));
				var lihatsldakhirsebelm = parseFloat($scope.LihatInstruksiPenerimaan_SaldoAkhirSebelumnya.replace(/\,/g,""));
				var BiayaK = 0;
				var BiayaD = 0;
				var selisih = 0;


				


				// if (!angular.isNumber(lihatawalsaldo) && (lihatawalsaldo != undefined)) {
				// 	console.log("edit Saldo Awal", lihatawalsaldo);
				// 	lihatawalsaldo = lihatawalsaldo.replace(/./g, "");
				// 	console.log("edit Saldo Awal", lihatawalsaldo);
				// }

				// if (!angular.isNumber(lihatakhirsaldo) && (lihatakhirsaldo != undefined)) {
				// 	console.log("edt Saldo Akhir", lihatakhirsaldo);
				// 	lihatakhirsaldo = lihatakhirsaldo.replace(/./g, "");
				// 	console.log("edit Saldo Akhir", lihatakhirsaldo);
				// }

				data = [{
					InstructionId: $scope.IncomingInstructionId,
					// NoRekBank: $scope.LihatInstruksiPenerimaan_NomorRekeningBank,
					NoRekBank: $scope.LihatInstruksiPenerimaan_AccountNumber,
					OutletId: $scope.currentOutletId,
					BankStatementDate: $scope.LihatInstruksiPenerimaan_TanggalBankStatement,
					IncomingInstructionDate: $scope.LihatInstruksiPenerimaan_TanggalInstruksiPenerimaan,
					MetodeInput: $scope.MetodeInput, //"upload , lihat , tambah 
					NamaFile: $scope.TambahInstruksiPenerimaan_NamaFile,
					SaldoAwal: lihatawalsaldo,
					SaldoAkhir: lihatakhirsaldo,
					SaldoAkhirSebelum: lihatsldakhirsebelm,
					DataGrid: JSON.stringify($scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data)
				}]
				//Tambahan CR4
				for (var i in $scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data){
					if($scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data[i].DebitCredit == "Kredit"){
						BiayaK += parseFloat($scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data[i].Nominal);
						 console.log('1');
						// break;
					}else
					if($scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data[i].DebitCredit == "Debet"){
						BiayaD += parseFloat($scope.InstruksiPenerimaan_EditDaftarTransaksi_UIGrid.data[i].Nominal);
					}	
				}
				
				
				selisih = parseFloat(lihatakhirsaldo - (lihatawalsaldo + (BiayaK-BiayaD))).toFixed(2);
				console.log('lihatakhirsaldo',lihatakhirsaldo);
				console.log('lihatawalsaldo',lihatawalsaldo);
				console.log('BiayaK',BiayaK);
				console.log('BiayaD',BiayaD);
				
				if(parseFloat(lihatakhirsaldo).toFixed(2) != parseFloat(lihatawalsaldo + (BiayaK-BiayaD)).toFixed(2)){
					bsNotify.show({
						title: "Peringatan",
						content: "Terjadi selisih sebesar Rp " + selisih,
						type: 'warning'
					});
				}else{
					
					$scope.submitData(data);
					if ($scope.errorSubmit == 0){
						$scope.BackToMainInstruksiPenerimaan();
					};	
				}
				//End Tambahan CR4
			}
			else {
				if (($scope.TambahInstruksiPenerimaan_NomorRekening != "") && ($scope.TambahInstruksiPenerimaan_NomorRekening != null)
					&& ($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data != [])) {
					var namafile = '';
					var awalsaldo = $scope.TambahInstruksiPenerimaan_SaldoAwal;
					var akhirsaldo = $scope.TambahInstruksiPenerimaan_SaldoAkhir;
					var akhirsebelumnya = parseFloat($scope.TambahInstruksiPenerimaan_SaldoAkhirSebelumnya.toString().replace(/\,/g, ""))
					var lihatawalsaldo = parseFloat($scope.TambahInstruksiPenerimaan_SaldoAwal.replace(/\,/g,""));
					var lihatakhirsaldo = parseFloat($scope.TambahInstruksiPenerimaan_SaldoAkhir.replace(/\,/g,""));
					var BiayaK = 0;
					var BiayaD = 0;
					var selisih = 0;

					// if (!angular.isNumber(awalsaldo) && (awalsaldo != undefined)) {
					// 	console.log("Saldo Awal", awalsaldo);
					// 	awalsaldo = awalsaldo.replace(/,/g, "");
					// 	console.log("Saldo Awal", awalsaldo);
					// }

					// if (!angular.isNumber(akhirsaldo) && (akhirsaldo != undefined)) {
					// 	console.log("Saldo Akhir", akhirsaldo);
					// 	akhirsaldo = akhirsaldo.replace(/,/g, "");
					// 	console.log("Saldo Akhir", akhirsaldo);
					// }

					
				

					if ($scope.MetodeInput == 'upload') {
						namafile = $scope.TambahInstruksiPenerimaan_NamaFile;
					}

					data = [{
						InstructionId: $scope.IncomingInstructionId,
						NoRekBank: $scope.TambahInstruksiPenerimaan_NomorRekening,
						OutletId: $scope.currentOutletId,
						BankStatementDate: $scope.newTglBankStatement,
						IncomingInstructionDate: $scope.TambahInstruksiPenerimaan_TanggalInstruksiPenerimaan,
						MetodeInput: $scope.MetodeInput, //"upload , lihat , tambah 
						NamaFile: namafile,
						SaldoAwal: awalsaldo,
						SaldoAkhir: akhirsaldo,
						SaldoAkhirSebelum: akhirsebelumnya, //.replace(/,/g,""),
						DataGrid: JSON.stringify($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data)
					}]
					//Tambahan CR4 P2
					for (var i in $scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data){
						console.log('sini1')
						if($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].DebitCredit == "Kredit"){
							BiayaK += parseFloat($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].Nominal);
							console.log('1',$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].Nominal);
							// break;
						}else
						if($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].DebitCredit == "Debet"){
							BiayaD += parseFloat($scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].Nominal);
							console.log('2',$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data[i].Nominal);
						}	
					}
				
				
					selisih = parseFloat(lihatakhirsaldo - (lihatawalsaldo + (BiayaK-BiayaD))).toFixed(2);
					console.log('lihatakhirsaldo',lihatakhirsaldo);
					console.log('lihatawalsaldo',lihatawalsaldo);
					console.log('BiayaK',BiayaK);
					console.log('BiayaD',BiayaD);
					console.log('BiayaK-BiayaD',(BiayaK-BiayaD))
					console.log('lanjut',(lihatawalsaldo + (BiayaK-BiayaD)))
					console.log('hasil',lihatakhirsaldo - (lihatawalsaldo + (BiayaK-BiayaD)))
					
					if(parseFloat(lihatakhirsaldo).toFixed(2) != parseFloat(lihatawalsaldo + (BiayaK-BiayaD)).toFixed(2)){
						bsNotify.show({
							title: "Peringatan",
							content: "Terjadi selisih sebesar Rp " + selisih,
							type: 'warning'
						});
					}else{
						
						$scope.submitData(data);
						if ($scope.errorSubmit == 0){
							$scope.BackToMainInstruksiPenerimaan();
						};	
					}
				//End Tambahan CR4 P2
					// $scope.submitData(data);
					// if ($scope.errorSubmit == 0){
					// 	$scope.BackToMainInstruksiPenerimaan();
					// };
				}
				else {
					//alert("Semua data harus diisi !");
					bsNotify.show({
						title: "Warning",
						//content: 'Reversal gagal diajukan, Error --> ', err,
						content: 'Semua data harus diisi !',
						type: 'warning'
					});
				}
			}
		};

		$scope.DeleteGridDaftarTransaksiRow = function (row) {
			var index = $scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data.indexOf(row.entity);
			$scope.InstruksiPenerimaan_TambahDaftarTransaksi_UIGrid.data.splice(index, 1);
		};

		$scope.InstruksiPenerimaan_DaftarMasterInstruksiPenerimaan_UIGrid = {
			paginationPageSizes: [10, 15, 25],
			useCustomPagination: true,
			useExternalPagination: true,
			enableSelectAll: false,
			multiselect: false,
			enableRowSelection: true,
			enableFiltering: true,
			columnDefs: [
				{ name: "NomorInstruksi", displayName: "Nomor Instruksi Penerimaan", field: "IncomingInstructionId", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "NomorRekeningBank", displayName: "Nomor Rekening Bank", field: "Account", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "TanggalBankStatement", displayName: "Tanggal Bank Statement", field: "BankStatementDate", enableCellEdit: false, enableHiding: false, visible: true },
				{ name: "SaldoAwal", field: "BeginBalance", enableCellEdit: false, visible: false },
				{ name: "SaldoAkhir", field: "EndBalance", enableCellEdit: false, visible: false },
				{ name: "SaldoAkhirSebelum", field: "LastEndBalance", enableCellEdit: false, visible: false }
			],

			onRegisterApi: function (gridApi) {
				$scope.DaftarMasterInstruksiPenerimaan_gridAPI = gridApi;

				// gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				// 	var tanggal;
				// 	// if ($scope.LihatInstruksiPenerimaan_TanggalInstruksiPenerimaan != null) {
				// 	// 	tanggal = $scope.formatDate($scope.LihatInstruksiPenerimaan_TanggalInstruksiPenerimaan);
				// 	// }
				// 	// else {
				// 	// 	tanggal = $scope.formatDate(new Date());
				// 	//}
				// 	$scope.InstruksiPenerimaan_DaftarMasterInstruksiPenerimaan_UIGrid.data = $scope.SetDataMainRequest(pageNumber,tanggal );
				// });	

				// gridApi.selection.on.rowSelectionChanged($scope,function(row){
				// 	if (row.isSelected==true)
				// 	{
				// 		$scope.SetDataGridToText(row.entity.Account, row.entity.BeginBalance, row.entity.EndBalance, row.entity.LastEndBalance, row.entity.BankStatementDate);
				// 		$scope.IncomingInstructionId = row.entity.IncomingInstructionId;
				// 		$scope.PrepareGridDaftarTransaksi('lihat', 1,row.entity.IncomingInstructionId);
				// 		console.log("data diinput --", row.entity.Account);
				// 	}
				// });	
			}
		};

	});

angular.module("app").directive("filesInput", function () {
	return {
		require: "ngModel",
		link: function postLink(scope, elem, attrs, ngModel) {
			elem.on("change", function (e) {
				var files = elem[0].files[0];
				scope.file = (e.srcElement || e.target).files[0];
				scope.TambahInstruksiPenerimaan_NamaFile = files.name;
				ngModel.$setViewValue(files);
				elem.val(null);
			})
		}
	}
})

app.filter('rupiahC', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
}); 