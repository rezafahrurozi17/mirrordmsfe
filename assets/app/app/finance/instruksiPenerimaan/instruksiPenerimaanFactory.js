angular.module('app')
  .factory('InstruksiPenerimaanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
	
	// factory.getData= function() {
  //       var res=$http.get('/api/fw/Role');
  //       //console.log('res=>',res);
		
	// 	var da_json=[
	// 			{Id: "1",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 1",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 1",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
	// 			{Id: "2",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 2",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 2",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
	// 			{Id: "3",Nomor_Instruksi_Penerimaan: "Nomor_InstruksiPenerimaan 3",Nomor_Rekening_Bank: "Nomor_Rekening_Bank 3",Tanggal_Bank_Statement: new Date("2/13/2017"),Saldo_Awal: 1000,Saldo_Akhir: 1000,Saldo_Akhir_Sebelumnya: 1000},
	// 		  ];
	// 	res=da_json;
		
  //       return res;
  //     };
//
	  factory.getDataBranch=function() {
			//var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
      var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData=0';
      var res=$http.get(url);  
      return res;			
		};	
		//get data bank
		factory.getDataBank = function(value){
			var url = '/api/fe/FinanceBank/start/1/limit/100/filterData/'+value;
			console.log("url TaxType : ", url); 
			var res=$http.get(url);
			return res;
		},	
	  
	factory.submitData= function(inputData) {
    	//var url = '/api/fe/IncomingInstruction';
			var url = '/api/fe/IncomingInstruction/SubmitData/';
    	var param = JSON.stringify(inputData);

		if (debugMode){console.log('Masuk ke submitData')};
		if (debugMode){console.log('url :'+url);};
		if (debugMode){console.log('Parameter POST :'+param)};
      	var res=$http.post(url, param);
      	
		return res;
    };

	factory.CancelData= function(inputData) {
    	//var url = '/api/fe/IncomingInstructionCancel';
			var url = '/api/fe/IncomingInstructionCancel/InsertData/';
			var data = [{Id : inputData}];
    	var param = JSON.stringify( data);

			if (debugMode){console.log('Masuk ke submitData')};
			if (debugMode){console.log('url :'+url);};
			if (debugMode){console.log('Parameter POST :'+param)};
					var res=$http.post(url, param);
					
			return res;
    };	

		factory.CheckValidReversal=function(id , outletid) {
				if (debugMode){console.log('Parameter Reversal : id'+ id + ' outlet : ' + outletid)};
				if (outletid == undefined)
				{
					outletid = 0;
				}
        var res=$http.get('/api/fe/IncomingInstruction/CheckReversal?id=' + id + '&outletid=' + outletid);
				//var res=$http.get('/api/fe/IncomingInstruction/id/' + id);
        return res;			
		}
	
		factory.parseDataMT=function(norek, inputData) {
    	var url = '/api/fe/IncomingInstructionUpload';
			var data = [{AccountNo: norek, data : inputData}];
    	var param = JSON.stringify( data);

			 if (debugMode){console.log('Parameter POST :'+param)};

			 // var res=$http.get('/api/fe/IncomingInstructionUpload/SelectData?start=0&limit=' + norek + '&filterData=' + param);
				var res=$http.post('/api/fe/IncomingInstructionUpload/UploadData/' , param);
				//var res=$http.get('/api/fe/IncomingInstructionUpload/SelectData/start/0/limit/' + norek + '/filterData/' + param);
        return res;

		};
		factory.getDataIncomingInstruction= function(id) {
        var res=$http.get('/api/fe/IncomingInstruction?id=' + id);
				//var res=$http.get('/api/fe/IncomingInstruction/id/' + id);
        return res;
      };
	
	factory.getAllDataIncomingInstruction= function(start, limit, filter) {
        var res=$http.get('/api/fe/IncomingInstruction/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filter);
				//var res=$http.get('/api/fe/IncomingInstruction/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + filter);
        return res;
      };
	  
	  factory.getDataIncomingInstructionDetail= function( start, limit, filter) {
        var res=$http.get('/api/fe/IncomingInstructionDetail/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filter);
				//var res=$http.get('/api/fe/IncomingInstructionDetail/SelectData/Start/' + start + '/Limit/' + limit + '/filterData/' + filter);
        return res;
      };
	  
	  factory.getDataTransactionType= function(){
		var url = '/api/fe/FinanceParam/TipeTransaksiIP';
		var res=$http.get(url);  
		return res;
	  };
	  
	  factory.getDataBankAccount= function(filter){
			var url = '/api/fe/BankAccount/SelectData?start=1&limit=25&filterData=' + filter;
			//var url = '/api/fe/BankAccount/SelectData/Start/1/Limit/25/filterData/' + filter;
			var res=$http.get(url);  
			return res;
	  };
	  
	factory.getdataAssignInstruksi= function(){
		var url = '/api/fe/FinanceParam/TipeAssignmentIP';
		var res=$http.get(url);  
		return res;  
	  };
	  
      // factory.create= function(instruksiPenerimaan) {
      //   return $http.post('/api/fw/Role', [{
      //                                       AppId: 1,
      //                                       ParentId: 0,
      //                                       Name: instruksiPenerimaan.Name,
      //                                       Description: instruksiPenerimaan.Description}]);
      // };
      // factory.update= function(instruksiPenerimaan){
      //   return $http.put('/api/fw/Role', [{
      //                                       Id: instruksiPenerimaan.Id,
      //                                       //pid: negotiation.pid,
      //                                       Name: instruksiPenerimaan.Name,
      //                                       Description: instruksiPenerimaan.Description}]);
      // };
      // factory.delete= function(id) {
      //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      // };
	  
	  return factory;
  });