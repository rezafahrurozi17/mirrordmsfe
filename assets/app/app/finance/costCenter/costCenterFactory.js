angular.module('app')
	.factory('CostCenterFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/FinanceCostCenter/start/1/limit/100';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(CostCenterData){
			var url = '/api/fe/FinanceCostCenter/create/';
			var ArrayCostCenterData = [CostCenterData];
			var param = JSON.stringify(ArrayCostCenterData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(CostCenterData){
			var url = '/api/fe/FinanceCostCenter/Update/';
			var ArrayCostCenterData = [CostCenterData];
			var param = JSON.stringify(ArrayCostCenterData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/fe/FinanceCostCenter/deletedata/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});