var app = angular.module('app');
app.controller('CostCenterController', function ($scope, $http, $filter, bsNotify, CurrentUser, CostCenterFactory, $timeout) {
	
	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function() {
		//$scope.loading = true;
		$scope.gridData = [];
	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mCostCenter = null; //Model
	$scope.xRole = { selected: [] };

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOption = { format: dateFormat }


	$scope.cekObject = function() {
		console.log($scope.mCostCenter);
	}

	$scope.onValidateSave = function(model,mode){
		console.log("cek notif>>>>",model)
		if(mode == "create"){
			$scope.creat(model);
			bsNotify.show({
				title: "Berhasil",
				content: "Data Berhasil disimpan",
				type: 'success'
			});
			$scope.getData();
			$scope.formApi.setMode('grid');
		}else{
			$scope.update(model);
			bsNotify.show({
				title: "Berhasil",
				content: "Data Berhasil diupdate",
				type: 'success'
			});
			$scope.getData();
			$scope.formApi.setMode('grid');
		}
		
	}

	$scope.creat = function(model){
		CostCenterFactory.create(model)
		.then(
			function(res){
				// $scope.grid.data = res.data.Result;
			}
		);
	}
	$scope.update = function(model){
		CostCenterFactory.update(model)
		.then(
			function(res){
				// $scope.grid.data = res.data.Result;
			}
		);
	}

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	$scope.getData = function() {
		CostCenterFactory.getData()
		.then(
			function(res){
				$scope.grid.data = res.data.Result;
			}
		);
	}

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function(rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function(rows) {
			console.log("onSelectRows=>", rows);
		}
	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.gridActionTemplate = '<div align="center" class="ui-grid-cell-contents">\
    <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Ubah" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
	</div>';
	
	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,

		columnDefs: [
			{ name: 'id', field: 'CostCenterId', visible: false },
			{ name: 'Kode', field: 'CostCenterCode' },
			{ name: 'Cost Center', field: 'CostCenterName' },

		]
	};
});