var app = angular.module('app');
app.controller('MasterPricingDealerToBranchAccPackageController', function ($scope, $http, $filter, CurrentUser, bsNotify, bsAlert, MasterPricingDealerToBranchAccPackageFactory, $timeout) {
	$scope.optionsKolomFilterSatu_AccPackage = [ 
																																													{name: "Model", value: "Model"}, 
																																													{name: "Tipe", value:"Tipe"} ,
																																													// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																													{name: "Katashiki", value:"Katashiki"} ,
																																													{name: "Suffix", value:"Suffix"} ,
																																													{name: "Kode Paket Accessories", value:"AccPackageCode"},
																																													{name: "Nama Detail Accessories", value:"AccDetailName"}	
																																												];

	$scope.optionsKolomFilterDua_AccPackage = [
																																												{name: "Model", value: "Model"}, 
																																												{name: "Tipe", value: "Tipe"}, 
																																												// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																												{name: "Katashiki", value:"Katashiki"} ,
																																												{name: "Suffix", value:"Suffix"} ,
																																												{name: "Kode Paket Accessories", value:"AccPackageCode"},
																																												{name: "Nama Detail Accessories", value:"AccDetailName"}
																																											];

	$scope.optionsComboBulkAction_AccPackage = [{name:"Delete" , value:"Delete"}];

	$scope.optionsComboFilterGrid_AccPackage = [
																																													{name:"Model", value:"Model"}, 
																																													{name:"Tipe", value:"Tipe"}, 
																																													// {name:"Nama Model", value:"NamaModel"},
																																													{name:"Katashiki", value:"Katashiki"},
																																													{name:"Suffix", value:"Suffix"},
																																													{name: "Kode Paket Accessories", value:"AccPackageCode"},
																																													{name: "Nama Detail Accessories", value:"AccDetailName"},
																																													{name: "Pricing Cluster Code", value: "PricingClusterCode" }, 
																																													{name:"Sales Price", value:"SalesPrice"},
																																													{name:"Effective Date Start", value:"EffectiveDateFrom"},
																																													{name:"Effective Date End", value:"EffectiveDateTo"},
																																													{name:"Remarks", value:"Remarks"}   
																																												];
	$scope.btnUpload = "hide";
	angular.element('#AccPackageModal').modal('hide');
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'AccPackage';
	$scope.Filter_AccPackage_AndOr = 'And';
	$scope.cekKolom = '';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.ShowFieldGenerate_AccPackage = true;
	$scope.MasterSelling_ExcelData = [];
	$scope.MasterSellingPriceFU_AccPackage_grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Model', displayName: 'Model', field: 'Model' },
			{ width: '15%', enableHiding: false, enableCellEdit: false, name: 'Tipe', displayName: 'Tipe', field: 'Tipe' },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Katashiki', displayName: 'Katashiki', field: 'Katashiki'},
			{ width: '06%', enableHiding: false, enableCellEdit: false, name: 'Suffix', displayName: 'Suffix', field: 'Suffix' },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Kode Paket Accessories', displayName: 'Kode Paket Aksesoris', field: 'AccPackageCode' },	
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Nama Detail Accessories', displayName: 'Nama Paket Aksesoris', field: 'AccPackageName'},
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Pricing Cluster Code', displayName: 'Pricing Cluster Code', field: 'PricingClusterCode' },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Sales Price', displayName: 'Sales Price', field: 'SalesPrice', cellFilter: 'rupiahC' ,  type:"number" },	
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Effective Date From', displayName: 'Effective Date Start', field: 'EffectiveDateFrom' },//,cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Effective Date To', displayName: 'Effective Date End', field: 'EffectiveDateTo' },//,cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '25%', enableHiding: false, enableCellEdit: false, name: 'Remarks', displayName: 'Remark', field: 'Remarks' }
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_AccPackage_gridAPI = gridApi;
			}
	};

	$scope.filterModel = [];
	MasterPricingDealerToBranchAccPackageFactory.getDataModel().then(function (res) {
		$scope.optionsModel = res.data.Result;
		console.log('$scope.optionsModel ==>', $scope.optionsModel)
		return $scope.optionsModel;
	});

	$scope.onSelectModelChanged = function (ModelSelected) {
		console.log('on model change ===>', ModelSelected);
		
		if (ModelSelected == undefined || ModelSelected == null) {
			$scope.filterModel.VehicleModelName = "";
			$scope.filterModel.VehicleModelId = "";
			
		} else {
			$scope.filterModel.VehicleModelName = ModelSelected.VehicleModelName;
			MasterPricingDealerToBranchAccPackageFactory.getDaVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
				$scope.optionsTipe = [];
				$scope.filterModel.VehicleTypelName = "";
				$scope.filterModel.VehicleTypeId = "";
				$scope.optionsTipe = res.data.Result;
				return $scope.optionsTipe;
			});
		}
	}

	$scope.onSelectTipeChanged = function (TipeSelected) {
		console.log('TipeSelected ===>', TipeSelected);
		if (TipeSelected == undefined || TipeSelected == null) {
			$scope.filterModel.VehicleTypelName = "";
			$scope.filterModel.VehicleTypeId = "";
		} else {
			$scope.filterModel.VehicleTypelName = TipeSelected.Description;
			$scope.filterModel.VehicleTypeId = TipeSelected.VehicleTypeId;
		}
	}

	$scope.FilterGridMasterAccPackage = function () {
		var inputfilter = $scope.textFilterMasterAccPackage;
		console.log('$scope.textFilterMasterAccPackage ===>', $scope.textFilterMasterAccPackage);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_AccPackage_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterAccPackage + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_AccPackage_grid.data = $scope.MasterSellingPriceFU_AccPackage_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data if ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data)

		} else {
			$scope.MasterSellingPriceFU_AccPackage_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data else ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data)
		}
	};

	$scope.fixDate = function (date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() + "-" +
				('0' + (date.getMonth() + 1)).slice(-2) + "-" +
				('0' + date.getDate()).slice(-2)
			return fix;
		} else {
			return null;
		}
	};


	$scope.MasterSelling_AccPackage_Generate_Clicked = function () {

		var tglStart;
		var tglEnd;

		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined || $scope.filterModel.TanggalFilterStart == null) {
			tglStart = "";
		} else {
			tglStart = $scope.fixDate($scope.filterModel.TanggalFilterStart)
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined || $scope.filterModel.TanggalFilterEnd == null) {
			tglEnd = "";
		} else {
			tglEnd = $scope.fixDate($scope.filterModel.TanggalFilterEnd)
		}

		if (($scope.filterModel.VehicleModelName == "" || $scope.filterModel.VehicleModelName == undefined) && ($scope.filterModel.VehicleTypelName == "" || $scope.filterModel.VehicleTypelName == undefined)){
			var filter = [{
				PEClassification: $scope.JenisPE,
				AndOr: $scope.Filter_AccPackage_AndOr,
				StartDate: tglStart,
				EndDate: tglEnd,
				PEType: $scope.TypePE
			}];
		} else if (($scope.filterModel.VehicleTypelName == "" || $scope.filterModel.VehicleTypelName == undefined)){
			var filter = [{
				PEClassification: $scope.JenisPE,
				Filter1: 'Model',
				Text1: $scope.filterModel.VehicleModelName,
				AndOr: $scope.Filter_AccPackage_AndOr,
				StartDate: tglStart,
				EndDate: tglEnd,
				PEType: $scope.TypePE
			}];
		}else{
			var filter = [{
				PEClassification: $scope.JenisPE,
				Filter1: 'Model',
				Text1: $scope.filterModel.VehicleModelName,
				AndOr: $scope.Filter_AccPackage_AndOr,
				Filter2: 'Tipe',
				Text2: $scope.filterModel.VehicleTypelName,
				StartDate: tglStart,
				EndDate: tglEnd,
				PEType: $scope.TypePE
			}];
		}

		


		MasterPricingDealerToBranchAccPackageFactory.getData(1, 100000, JSON.stringify(filter))
			.then(
				function (res) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data = res.data.Result;			//Data hasil dari WebAPI
					$scope.MasterSellingPriceFU_AccPackage_grid.totalItems = res.data.Total;
					$scope.MasterSellingPriceFU_AccPackage_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_AccPackage_grid.data);
				},
				function (err) {
					console.log('error -->', err)
				}
			);
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_AccPackage' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
			for (i = 0; i < json.length; i++) {
					var tglUpload = json[i].EffectiveDateFrom //format 20191231
					var thn = tglUpload.substring(0, 4);
					var bln = tglUpload.substring(4, 6);
					var tgl = tglUpload.substring(6, 8);
					var tgl_jadi = tgl + "-" + bln + "-" + thn;
					json[i].EffectiveDateFrom = tgl_jadi;

					var tglUploadTo = json[i].EffectiveDateTo //format 20191231
					var thnTo = tglUploadTo.substring(0, 4);
					var blnTo = tglUploadTo.substring(4, 6);
					var tglTo = tglUploadTo.substring(6, 8);

					var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
					json[i].EffectiveDateTo = tgl_jadiTo;
			}

				$scope.btnUpload = "hide";
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

				$scope.MasterSelling_AccPackage_Upload_Clicked();
				myEl.val('');
            });
	}
	
	$scope.ComboBulkAction_AccPackage_Changed = function()
	{
		if ($scope.ComboBulkAction_AccPackage == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_BBN_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_BBN_grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_AccPackage_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
				function() {
					$scope.OK_Button_Clicked();
				},
				function() {
		
				// $scope.PGA_BGTGLAccount_UIGrid.data.push(row_data);
				}
			)
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_AccPackage_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_AccPackage_grid.data.splice($scope.MasterSellingPriceFU_AccPackage_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_AccPackage = "";
		
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_AccPackage = "";
		angular.element('#AccPackageModal').modal('hide');
	}	

	$scope.validateColumn = function(inputExcelData){
		console.log("inputExcelData : ", inputExcelData);
		console.log("No_Material : ", inputExcelData.Model);
		console.log("Nama_Material : ", inputExcelData.NamaModel);
		console.log("Standard_Cost : ", inputExcelData.FreeService);
		// ini harus di rubah
		$scope.cekKolom = '';
		// if (angular.isUndefined(inputExcelData.Model)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Model \n';
		// }
		// if (angular.isUndefined(inputExcelData.NamaModel)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' NamaModel \n';
		// }		
		if (angular.isUndefined(inputExcelData.Katashiki)) {
			$scope.cekKolom = $scope.cekKolom + ' Katashiki \n';
		}
		if 	(angular.isUndefined(inputExcelData.Suffix)){
			$scope.cekKolom = $scope.cekKolom + ' Suffix \n';
		}
		if 	(angular.isUndefined(inputExcelData.AccPackageCode)){
			$scope.cekKolom = $scope.cekKolom + ' AccPackageCode \n';
		}
		if	(angular.isUndefined(inputExcelData.AccPackageName)) {
			$scope.cekKolom = $scope.cekKolom + ' AccPackageName \n';
		}
		if (angular.isUndefined(inputExcelData.PricingClusterCode)) {
			$scope.cekKolom = $scope.cekKolom + ' PricingClusterCode \n';
		}
		if 	(angular.isUndefined(inputExcelData.SalesPrice)){
			$scope.cekKolom = $scope.cekKolom + ' SalesPrice \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		// if	(angular.isUndefined(inputExcelData.Remarks)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		// }

		if ( 
			// angular.isUndefined(inputExcelData.Model) || 
			// angular.isUndefined(inputExcelData.NamaModel) ||
			angular.isUndefined(inputExcelData.Katashiki) ||
			angular.isUndefined(inputExcelData.Suffix) || 
			angular.isUndefined(inputExcelData.AccPackageCode) ||
			angular.isUndefined(inputExcelData.AccPackageName) ||
			angular.isUndefined(inputExcelData.PricingClusterCode) ||
			angular.isUndefined(inputExcelData.SalesPrice) || 
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.MasterSelling_AccPackage_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingAccPackage";
		if ($scope.MasterSellingPriceFU_AccPackage_grid.data.length == 0){
				excelData.push({ 
						Model: "",
						Tipe: "",
						// NamaModel: "",
						Katashiki: "",
						Suffix : "",						
				 	AccPackageCode : "",
						AccPackageName: "",
				 	PricingClusterCode: "",
						SalesPrice: "",
						EffectiveDateFrom: "",													
						EffectiveDateTo: "",
						Remarks: "" });
			fileName = "MasterSellingAccPackageTemplate";		
		}
		else {

			for (var i = 0; i < $scope.MasterSellingPriceFU_AccPackage_grid.data.length; i++) {

				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Model == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].Model = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Tipe == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].Tipe = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Katashiki == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].Katashiki = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Suffix == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].Suffix = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].AccPackageCode == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].AccPackageCode = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].AccPackageName == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].AccPackageName = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].PricingClusterCode == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].PricingClusterCode = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].SalesPrice == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].SalesPrice = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo = " ";
				}
				if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Remarks == null) {
					$scope.MasterSellingPriceFU_AccPackage_grid.data[i].Remarks = " ";
				}
			}

		$scope.MasterSellingPriceFU_AccPackage_grid.data.forEach(function(row) {
			excelData.push({ 
						Model: row.Model,
						Tipe: row.Tipe,
						// NamaModel: row.NamaModel,
						Katashiki: row.Katashiki,
						Suffix : row.Suffix,
						AccPackageCode: row.AccPackageCode,
						AccPackageName: row.AccPackageName,
			  	PricingClusterCode: row.PricingClusterCode,
						SalesPrice : row.SalesPrice,						
						EffectiveDateFrom: row.EffectiveDateFrom,													
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks: row.Remarks });
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);
		// angular.forEach($scope.MasterSellingPriceFU_AccPackage_gridAPI.selection.getSelectedRows(), function(value, key){	
		// 	excelData.push(
		// 	);

		// 	// for (var i = 0; i < $scope.MasterSellingPriceFU_AccPackage_grid.columnDefs.length; i++) {

		// 	// console.log(i);
		// 	// // more statements
		// 	// }
		// 	excelData = [{"Nama_GL_Biaya":"", "Kode_GL_Biaya":"", "Cost_Center":"", "Periode_Awal":"", "Periode_Akhir":"", "Anggaran_Maksimal":""}];
		// 		$scope.ModalTolak_APId = value.DocumentId;
		// 		$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
		// 	});;;
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_AccPackage_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);

		for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
			var setan = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSelling_ExcelData[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		MasterPricingDealerToBranchAccPackageFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_AccPackage_grid.data = res.data.Result;			//Data hasil dari WebAPI

				var dataValid = undefined;
				for (i = 0; i < $scope.MasterSellingPriceFU_AccPackage_grid.data.length; i++) {
					if ($scope.MasterSellingPriceFU_AccPackage_grid.data[i].Remarks != "") {
						dataValid = false;
					}
				}

				if (dataValid == false) {
					bsNotify.show(
						{
							type: 'warning',
							title: "Data tidak valid!",
							content: "Cek detail kesalahan di kolom Remarks pada tabel",
						}
					);
				}

				$scope.MasterSellingPriceFU_AccPackage_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		for (i = 0; i < $scope.MasterSellingPriceFU_AccPackage_grid.data.length; i++) {
			var setan = $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
			$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
			$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo);
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_AccPackage_grid.data);		
		MasterPricingDealerToBranchAccPackageFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				bsNotify.show({
					title: "Customer Data",
					content: "Data berhasil di simpan",
					type: 'success'
				});
				$scope.MasterSelling_AccPackage_Generate_Clicked();
			},
			function (err) {
				bsNotify.show(
					{
						type: 'danger',
						title: "Data gagal disimpan!",
						content: err.data.Message.split('-')[1],
					}
				); 
			}
		);
		//                        12/31/2019   ===> 0/1/2
		for (i = 0; i < $scope.MasterSellingPriceFU_AccPackage_grid.data.length; i++) {
			var setan = $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom.split('/');
			var to_submit = "" + setan[1] + "-" + setan[0] + "-" + setan[2];
			$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo.split('/');
			var to_submit2 = "" + setan2[1] + "-" + setan2[0] + "-" + setan2[2];
			$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_AccPackage_grid.data[i].EffectiveDateTo);
		}
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_AccPackage_grid.data = [];
		$scope.MasterSellingPriceFU_AccPackage_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_AccPackage = "";
		$scope.TextFilterSatu_AccPackage = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_AccPackage' ) );
		myEl.val('');	
	}

	

	$scope.MasterSelling_AccPackage_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_AccPackage;
		$scope.MasterSellingPriceFU_AccPackage_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_AccPackage != "") {
			$scope.MasterSellingPriceFU_AccPackage_gridAPI.grid.getColumn($scope.ComboFilterGrid_AccPackage).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_AccPackage_gridAPI.grid.clearAllFilters();
	}

});