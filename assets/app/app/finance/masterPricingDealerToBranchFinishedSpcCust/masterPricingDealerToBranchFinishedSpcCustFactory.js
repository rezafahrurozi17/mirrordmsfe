angular.module('app')
	.factory('MasterPricingDealerToBranchFinishedSpcCustFactory', function ($http, CurrentUser) {
 var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
		
	
	return {
		VerifyData: function (KlasifikasiPE, IsiGrid, TypePE) {
			//var url = '/api/fe/IncomingInstruction';
			var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
			var url = '/api/fe/MasterPESellingFinishedSpcCust/Verify/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		},

		getData: function (start, limit, filterData) {
			//var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
			var url = '/api/fe/MasterPESellingFinishedSpcCust/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
			var res = $http.get(url);
			return res;
		},

		Submit: function (KlasifikasiPE, IsiGrid, TypePE) {
			//var url = '/api/fe/IncomingInstructionCancel';
			var url = '/api/fe/MasterPESellingFinishedSpcCust/Submit/';
			var inputData = [{ Classification: KlasifikasiPE, Grid: IsiGrid, JenisPE: TypePE }];
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			var res = $http.post(url, param);

			return res;
		},

		getDaVehicleType: function (param) {
			var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
			return res;
		},

		getDataModel: function () {
			var res = $http.get('/api/sales/MUnitVehicleModelTomas');
			return res;
		},

	}
});