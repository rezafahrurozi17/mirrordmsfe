/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('informasiPembayaranLeasingFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;
		factory.changeFormatDate = function(item) {
			var tmpAppointmentDate = item;
			var finalDate
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + ''+tmpAppointmentDate[1]+''+tmpAppointmentDate[0];
            return finalDate;
		}

		factory.getSalesOrderBySPKId = function(start, limit, SPKId, IncomingType)
		{
			var find = "/";
			var regex = new RegExp(find, "g");
			SPKId = SPKId.replace(regex, "~"); 					
			var url = '/api/fe/AlokasiBFDPUnit/GetAlokasiSOBySPKId/start/' + start + '/limit/' + limit + '/spkid/' + SPKId + '|' + IncomingType;
			console.log ("execute GetAlokasiSOBySPKId");
			return $http.get(url);
		}
		
		factory.GetInformasiLeasing = function(start, limit, TranDate, TranDateTo, LeasingId, OutletId)
		{
			var url = '/api/fe/Leasing/GetInformasiLeasing?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + TranDateTo + '|' + LeasingId + '|' + OutletId;
			console.log ("execute GetInformasiLeasing");
			return $http.get(url);
		}

		factory.GetInformasiLeasingFilter = function(start, limit, TranDate, TranDateTo, LeasingId, OutletId, SearchText, SearchType, flag)
		{
			if(flag == 1){
				SearchText = this.changeFormatDate(SearchText);
			}
			var url = '/api/fe/Leasing/GetInformasiLeasing?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + TranDateTo + '|' + LeasingId + '|' + OutletId + '|' + SearchText + '|' + SearchType;
			console.log ("execute GetInformasiLeasing");
			return $http.get(url);
		}
		 
		factory.getLeasingComboBox = function () {
			console.log("factory.getLeasingComboBox");
			  var url = '/api/fe/Leasing/SelectData?start=0&limit=0&FilterData=0';
			  var res=$http.get(url);  
			  return res;	
		}				 
		
		
		return factory;
	});