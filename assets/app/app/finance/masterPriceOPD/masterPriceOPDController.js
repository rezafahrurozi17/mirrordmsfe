var app = angular.module('app');
app.controller('MasterPriceOPDController', function ($scope, $http, $filter, CurrentUser,bsAlert, MasterPriceOPDFactory, bsNotify, $timeout) {
	$scope.optionsKolomFilterSatu_PriceOPD = [
														{name:"Model", value:"Model"}
														,{name:"Tipe", value:"Tipe"}  
														,{name:"Katashiki", value:"Katashiki"}  
														,{name:"Suffix", value:"Suffix"}
														,{name:"Service Code", value:"JasaCode"}
														,{name:"Description", value:"JasaName"} 
														,{name:"Pricing Cluster Code", value:"PriceArea"} 
														,{name:"Price", value:"Price"}
														,{name:"Effective Date Start", value:"EffectiveDateFrom"} 
														,{name:"Effective Date End", value:"EffectiveDateTo"}
														,{name:"Remarks", value:"Remarks"}
													];

	$scope.optionsKolomFilterDua_PriceOPD = [
														{name:"Model", value:"Model"}
														,{name:"Tipe", value:"Tipe"}  
														,{name:"Katashiki", value:"Katashiki"}  
														,{name:"Suffix", value:"Suffix"}
														,{name:"Service Code", value:"JasaCode"}
														,{name:"Description", value:"JasaName"} 
														,{name:"Pricing Cluster Code", value:"PriceArea"} 
														,{name:"Price", value:"Price"}
														,{name:"Effective Date Start", value:"EffectiveDateFrom"} 
														,{name:"Effective Date End", value:"EffectiveDateTo"}
														,{name:"Remarks", value:"Remarks"}
													];

	$scope.optionsComboBulkAction_PriceOPD = [{name:"Delete" , value:"Delete"}];

	$scope.optionsComboFilterGrid_PriceOPD = [
														,{name:"Model", value:"Model"}
														,{name:"Tipe", value:"Tipe"}  
														,{name:"Katashiki", value:"Katashiki"}  
														,{name:"Suffix", value:"Suffix"}
														,{name:"Service Code", value:"JasaCode"}
														,{name:"Description", value:"JasaName"} 
														,{name:"Pricing Cluster Code", value:"PriceArea"} 
														,{name:"Price", value:"Price"}
														,{name:"Effective Date Start", value:"EffectiveDateFrom"} 
														,{name:"Effective Date End", value:"EffectiveDateTo"}
														,{name:"Remarks", value:"Remarks"}
													];

	$scope.MasterSelling_FileName_Current = '';
	var dateFormat = 'dd-MM-yyyy';
	$scope.dateOptions = { format: dateFormat };
	$scope.JenisPE = 'PriceOpd';
	$scope.cekKolom = '';
	angular.element('#PriceOPDModal').modal('hide');
	$scope.Filter_PriceOPD_AndOr = 'And';
	$scope.TypePE = 1; //0 = dealer to branch , 1 = tam to dealer
	$scope.ShowFieldGenerate_PriceOPD = true;
	$scope.MasterSelling_ExcelData = [];


	$scope.filterModel = [];
	MasterPriceOPDFactory.getDataModel().then(function (res) {
		$scope.optionsModel = res.data.Result;
		console.log('$scope.optionsModel ==>', $scope.optionsModel)
		return $scope.optionsModel;
	});

	$scope.onSelectModelChanged = function (ModelSelected) {
		console.log('on model change ===>', ModelSelected);
		$scope.optionsTipe = [];
		$scope.filterModel.VehicleTypelName = "";
		$scope.filterModel.VehicleTypeId = "";
		if (ModelSelected == undefined || ModelSelected == null) {
			$scope.filterModel.VehicleModelName = "";
		} else {
			$scope.filterModel.VehicleModelName = ModelSelected.VehicleModelName;
			MasterPriceOPDFactory.getDaVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
				$scope.optionsTipe = res.data.Result;
				return $scope.optionsTipe;
			});
		}
	}

	$scope.onSelectTipeChanged = function (TipeSelected) {
		console.log('TipeSelected ===>', TipeSelected);
		if (TipeSelected == undefined || TipeSelected == null) {
			$scope.filterModel.VehicleTypelName = "";
		} else {
			$scope.filterModel.VehicleTypelName = TipeSelected.Description;
			$scope.filterModel.VehicleTypeId = TipeSelected.VehicleTypeId;
		}
	}

	$scope.FilterGridMasterPriceOPD = function () {
		var inputfilter = $scope.textFilterMasterPriceOPD;
		console.log('$scope.textFilterMasterPriceOPD ===>', $scope.textFilterMasterPriceOPD);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_PriceOPD_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterPriceOPD + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_PriceOPD_grid.data = $scope.MasterSellingPriceFU_PriceOPD_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_PriceOPD_grid.data if ===>', $scope.MasterSellingPriceFU_PriceOPD_grid.data)

		} else {
			$scope.MasterSellingPriceFU_PriceOPD_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_PriceOPD_grid.data else ===>', $scope.MasterSellingPriceFU_PriceOPD_grid.data)
		}
	};


	$scope.MasterSelling_PriceOPD_Generate_Clicked = function () {

		var tglStart;
		var tglEnd;

		if($scope.filterModel.VehicleModelId == null || $scope.filterModel.VehicleModelId == undefined || $scope.filterModel.VehicleModelId == ""){
			bsNotify.show(
				{
					title: "Warning",
					content: "Model Wajib Diisi!",
					type: 'warning'
				}
			);
			return false;
		}

		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined || $scope.filterModel.TanggalFilterStart == null) {
			tglStart = "";
		} else {
			tglStart = $scope.fixDate($scope.filterModel.TanggalFilterStart)
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined || $scope.filterModel.TanggalFilterEnd == null) {
			tglEnd = "";
		} else {
			tglEnd = $scope.fixDate($scope.filterModel.TanggalFilterEnd)
		}

		if($scope.filterModel.VehicleTypeId == null || $scope.filterModel.VehicleTypeId == undefined || $scope.filterModel.VehicleTypeId == ""){
			var filter = [{
				PEClassification: $scope.JenisPE,
				Filter1: 'Model',
				Text1: $scope.filterModel.VehicleModelName,
				AndOr: $scope.Filter_PriceOPD_AndOr,
				Filter2: '',
				Text2: $scope.filterModel.VehicleTypeId,
				StartDate: tglStart,
				EndDate: tglEnd,
				PEType: $scope.TypePE
			}];
		}else{
			var filter = [{
				PEClassification: $scope.JenisPE,
				Filter1: 'Model',
				Text1: $scope.filterModel.VehicleModelName,
				AndOr: $scope.Filter_PriceOPD_AndOr,
				Filter2: 'VehicleTypeId',
				Text2: $scope.filterModel.VehicleTypeId,
				StartDate: tglStart,
				EndDate: tglEnd,
				PEType: $scope.TypePE
			}];
		}
		
		MasterPriceOPDFactory.getData(1, 100000, JSON.stringify(filter))
			.then(
				function (res) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data = res.data.Result;			//Data hasil dari WebAPI
					$scope.MasterSellingPriceFU_PriceOPD_grid.totalItems = res.data.Total;
					$scope.MasterSellingPriceFU_PriceOPD_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_PriceOPD_grid.data);
				},
				function (err) {
					console.log('error -->', err)
				}
			);
	}


	$scope.MasterSellingPriceFU_PriceOPD_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],		
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,			
		columnDefs: [
			// { name: 'Id',displayName:'Id', field: 'Id', visible: false, enableHiding: false },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Model', displayName: 'Model', field: 'Model' },
			{ width: '15%', enableHiding: false, enableCellEdit: false, name: 'Tipe', displayName: 'Tipe', field: 'Tipe'  },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Katashiki', displayName: 'Katashiki', field: 'Katashiki' },
			{ width: '06%', enableHiding: false, enableCellEdit: false, name: 'Suffix', displayName: 'Suffix', field: 'Suffix' },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Service Code', displayName: 'Service Code', field: 'JasaCode'  },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Description', displayName: 'Description', field: 'JasaName'  },
			{ width: '13%', enableHiding: false, enableCellEdit: false, name: 'Pricing Cluster Code', displayName: 'Pricing Cluster Code', field: 'PriceArea'  },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Price', displayName: 'Price', field: 'Price'  , cellFilter: 'rupiahC' ,  type:"number"},
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Effective Date From', displayName: 'Effective Date Start', field: 'EffectiveDateFrom',cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '10%', enableHiding: false, enableCellEdit: false, name: 'Effective Date To', displayName: 'Effective Date End', field: 'EffectiveDateTo',cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ width: '25%', enableHiding: false, enableCellEdit: false, name: 'Remarks', displayName: 'Remark', field: 'Remarks' }

		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_PriceOPD_gridAPI = gridApi;
			}
	};



		

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceOPD' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				
				for (var i in $scope.MasterSelling_ExcelData){
					$scope.MasterSelling_ExcelData[i].PriceArea = angular.copy($scope.MasterSelling_ExcelData[i].PricingClusterCode);
					$scope.MasterSelling_ExcelData[i].JasaCode = angular.copy($scope.MasterSelling_ExcelData[i].ServiceCode);
					$scope.MasterSelling_ExcelData[i].JasaName = angular.copy($scope.MasterSelling_ExcelData[i].Description);
				}
		
				$scope.MasterPriceOPD_Upload_Clicked();
				myEl.val('');
            });
	}

	$scope.validateColumn = function(inputExcelData){
		console.log("inputExcelData : ", inputExcelData);
		// console.log("No_Material : ", inputExcelData.Model);
		// console.log("Nama_Material : ", inputExcelData.NamaModel);
		// console.log("Standard_Cost : ", inputExcelData.FreeService);
		// ini harus di rubah

		$scope.cekKolom = '';
		// if (angular.isUndefined(inputExcelData.Model)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Model \n';
		// }
		// if (angular.isUndefined(inputExcelData.Tipe)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Tipe \n';
		// }
		if (angular.isUndefined(inputExcelData.Katashiki)) {
			$scope.cekKolom = $scope.cekKolom + ' Katashiki \n';
		} 
		if 	(angular.isUndefined(inputExcelData.Suffix)){
			$scope.cekKolom = $scope.cekKolom + ' Suffix \n';
		}
		if (angular.isUndefined(inputExcelData.ServiceCode)) {
			$scope.cekKolom = $scope.cekKolom + ' ServiceCode \n';
		}	
		if (angular.isUndefined(inputExcelData.Description)) {
			$scope.cekKolom = $scope.cekKolom + ' Description \n';
		}	
		// if (angular.isUndefined(inputExcelData.PriceArea)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' PriceArea \n';
		// }						
		if 	(angular.isUndefined(inputExcelData.Price)){
			$scope.cekKolom = $scope.cekKolom + ' Price \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		// if	(angular.isUndefined(inputExcelData.Remarks)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		// }		

		if (
			// angular.isUndefined(inputExcelData.Model) || 
			// angular.isUndefined(inputExcelData.Tipe) ||
			angular.isUndefined(inputExcelData.Katashiki) ||
		 angular.isUndefined(inputExcelData.Suffix) || 
			angular.isUndefined(inputExcelData.ServiceCode) || 
			angular.isUndefined(inputExcelData.Description) ||
			// angular.isUndefined(inputExcelData.PriceArea) ||
			angular.isUndefined(inputExcelData.Price) || 
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) 
			// angular.isUndefined(inputExcelData.Remarks)
			){
			return(false);
		}
	
		return(true);
	}

	$scope.MasterSelling_PriceOPD_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterPriceOPD";
		if ($scope.MasterSellingPriceFU_PriceOPD_grid.data.length == 0){
				excelData.push({ 
						Model : "",
						Tipe: "",
						Katashiki: "",
						Suffix : "",
						ServiceCode: "",
						Description: "",
						PricingClusterCode:"",
						Price: "",
						EffectiveDateFrom: "",													
						EffectiveDateTo: "",
						Remarks: "" });
			fileName = "MasterPriceOPDTemplate";		
		}
		else {

			for (var i = 0; i < $scope.MasterSellingPriceFU_PriceOPD_grid.data.length; i++) {

				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Model == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Model = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Tipe == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Tipe = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Katashiki == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Katashiki = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Suffix == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Suffix = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].JasaCode == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].JasaCode = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].JasaName == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].JasaName = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].PriceArea == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].PriceArea = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Price == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Price = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].EffectiveDateFrom == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].EffectiveDateFrom = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].EffectiveDateTo == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].EffectiveDateTo = " ";
				}
				if ($scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Remarks == null) {
					$scope.MasterSellingPriceFU_PriceOPD_grid.data[i].Remarks = " ";
				}
			}

		$scope.MasterSellingPriceFU_PriceOPD_grid.data.forEach(function(row) {
			excelData.push({ 
						Model: row.Model,
						Tipe: row.Tipe,
						Katashiki: row.Katashiki,
						Suffix : row.Suffix,
						ServiceCode: row.JasaCode,
						Description : row.JasaName,
						PricingClusterCode: row.PriceArea,
						Price: row.Price,
						EffectiveDateFrom: row.EffectiveDateFrom,													
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks: row.Remarks 
					});
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);

		XLSXInterface.writeToXLSX(excelData, fileName);

	}

	$scope.MasterPriceOPD_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}
		

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		// for (var i = 0; i < Grid.length; i++) {
		// 	Grid[i].PricingClusterCode = Grid[i].PriceArea;
		// }
		console.log('upload ==>',Grid);
		// for (var i in Grid){
			// Grid[i].JasaCode = angular.copy(Grid[i].ServiceCode);
			// Grid[i].JasaName = angular.copy(Grid[i].Description);
		// }
		MasterPriceOPDFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_PriceOPD_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceOPD_grid.totalItems = res.data.Total;	
			}
		);
	}
	
	$scope.VerifyDataInput = function () {
		MasterPriceOPDFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_PriceOPD_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceOPD_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_PriceOPD_grid.data);
		for (var i = 0; i < Grid.length; i++) {
			Grid[i].PricingClusterCode = Grid[i].PriceArea ;
		}
		console.log('goto database==>',Grid);
		MasterPriceOPDFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){		
				bsNotify.show({
					title: "Sukses",
					content: "Data Berhasil Disimpan",
					type: 'success'
				});
				if($scope.filterModel.VehicleModelId == "" || $scope.filterModel.VehicleModelId == undefined || $scope.filterModel.VehicleModelId == null){
					// $scope.MasterSelling_PriceAccessories_Generate_Clicked();
				}else{
					$scope.MasterSelling_PriceOPD_Generate_Clicked();					
				}
				
			},
			function (err) {
				bsNotify.show({
					title: "Gagal",
					content: err.data.Message,
					type: 'danger'
				});
   }
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_PriceOPD_grid.data = [];
		$scope.MasterSellingPriceFU_PriceOPD_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_PriceOPD = "";
		$scope.TextFilterSatu_PriceOPD = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceOPD' ) );
		myEl.val('');	
	}

	$scope.MasterPriceOPD_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterPriceOPD_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterPriceOPD_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterPriceOPD_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterPriceOPD_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterPriceOPD_TanggalFilterEnd) != true) ) 
		{
			tglEnd =  $scope.formatDate($scope.MasterPriceOPD_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
						Filter1 : $scope.KolomFilterSatu_PriceOPD,
						Text1 : $scope.TextFilterSatu_PriceOPD,
						AndOr : $scope.Filter_PriceOPD_AndOr,
						Filter2 : $scope.KolomFilterDua_PriceOPD,
						Text2 : $scope.TextFilterDua_PriceOPD,		
						StartDate : tglStart ,
						EndDate :tglEnd,
						PEType : $scope.TypePE}];
		MasterPriceOPDFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_PriceOPD_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceOPD_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterPriceOPD_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_PriceOPD;
		$scope.MasterSellingPriceFU_PriceOPD_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_PriceOPD != "") {
			$scope.MasterSellingPriceFU_PriceOPD_gridAPI.grid.getColumn($scope.ComboFilterGrid_PriceOPD).filters[0].term=value;
		}
	}

	$scope.ComboBulkAction_PriceOPD_Changed = function()
	{
		if ($scope.ComboBulkAction_PriceOPD == "Delete") {
			var counter =0;
			angular.forEach($scope.MasterSellingPriceFU_PriceOPD_gridAPI.selection.getSelectedRows(), function (data, index) {
				counter++;
			});


			if (counter > 0) {
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
				function() {
					$scope.OK_Button_Clicked();
				},
				function() {
		
				// $scope.PGA_BGTGLAccount_UIGrid.data.push(row_data);
				}
			)
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_PriceOPD_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_PriceOPD_grid.data.splice($scope.MasterSellingPriceFU_PriceOPD_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_PriceOPD = "";
		
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_PriceOPD = {};
		angular.element('#PriceOPDModal').modal('hide');
	}

});