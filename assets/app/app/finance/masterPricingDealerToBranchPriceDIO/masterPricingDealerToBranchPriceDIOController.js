var app = angular.module('app');
app.controller('MasterPricingDealerToBranchPriceDIOController', function ($scope, $http, $filter, bsAlert, CurrentUser, MasterPricingDealerToBranchPriceDIOFactory, $timeout) {
	$scope.optionsKolomFilterSatu_PriceDIO = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_PriceDIO = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_PriceDIO = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_PriceDIO = [{name:"Kode DIO", value:"DIOCode"}, {name:"Nama DIO", value:"DIOName"},
			{name:"Sales Price", value:"SalesPrice"}, {name:"Effective Date From", value:"EffectiveDateFrom"},
			{name:"Effective Date To", value:"EffectiveDateTo"}, {name:"Remarks", value:"Remarks"}
	];
	angular.element('#PriceDIOModal').modal('hide');
	$scope.MasterSelling_FileName_Current = '';
	$scope.cekKolom = '';
	$scope.JenisPE = 'PriceDIO';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_PriceDIO = false;

	$scope.MasterSellingPriceFU_PriceDIO_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],		
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'DIOCode', displayName: 'Kode DIO', field: 'DIOCode',enableCellEdit: false , enableHiding: false},
			{ name: 'DIOName', displayName: 'Nama DIO', field: 'DIOName',enableCellEdit: false , enableHiding: false},
			{ name: 'SalesPrice', displayName: 'Sales Price', field: 'SalesPrice',enableCellEdit: true , enableHiding: false,cellFilter: 'rupiahC' ,  type:"number"},
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date From', field: 'EffectiveDateFrom',enableCellEdit: false, enableHiding: false },
			{ name: 'EffectiveDateTo', displayName: 'Effective Date To', field: 'EffectiveDateTo',enableCellEdit: false, enableHiding: false },  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks',enableCellEdit: true, enableHiding: false }, 
			{ name: 'PriceDIOId',displayName:'Id', field: 'PriceDIOId', visible: false , enableHiding: false},		
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_PriceDIO_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceDIO' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;
				$scope.MasterSelling_PriceDIO_Upload_Clicked();
				myEl.val('');
            });
	}

	$scope.validateColumn = function(inputExcelData){
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.DIOCode)) {
			$scope.cekKolom = $scope.cekKolom + ' DIOCode \n';
		}
		
		if (angular.isUndefined(inputExcelData.DIOName)) {
			$scope.cekKolom = $scope.cekKolom + ' DIOName \n';
		}
		if 	(angular.isUndefined(inputExcelData.SalesPrice)){
			$scope.cekKolom = $scope.cekKolom + ' SalesPrice \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}

		if ( angular.isUndefined(inputExcelData.DIOCode) || 
			angular.isUndefined(inputExcelData.DIOName) ||
			angular.isUndefined(inputExcelData.SalesPrice) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.ComboBulkAction_PriceDIO_Changed = function()
	{
		if ($scope.ComboBulkAction_PriceDIO == "Delete") {
			var index;
			// $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_PriceDIO_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_PriceDIO_grid.data.splice(index, 1);
			// });
			//alert('masuk sini!');
			var counter = 0;

			angular.forEach($scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedRows(), function (data, index) {
				counter++;
			});

			//if ($scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount() > 0) {
			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
					function() {
						$scope.DeleteRow();
					},
					function() {
		
					}
				);
			}
		}
	}
	$scope.DeleteRow = function(){
		angular.forEach($scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_PriceDIO_grid.data.splice($scope.MasterSellingPriceFU_PriceDIO_grid.data.lastIndexOf(data), 1);
		});
		$scope.ComboBulkAction_PriceDIO = "";
	}

	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_PriceDIO = "";
		angular.element('#PriceDIOModal').modal('hide');
	}

	$scope.MasterSelling_PriceDIO_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingPriceDIO";

		if ($scope.MasterSellingPriceFU_PriceDIO_grid.data.length == 0) {
				excelData.push({ 
						DIOCode: "",
						DIOName: "",
						SalesPrice: "",
						EffectiveDateFrom: "",
						EffectiveDateTo: "",
						Remarks: "" });
				fileName = "MasterSellingPriceDIOTemplate";
		}
		else {
		//$scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_PriceDIO_grid.data.forEach(function(row) {
				excelData.push({ 
						DIOCode: row.DIOCode,
						DIOName: row.DIOName,
						SalesPrice: row.SalesPrice,
						EffectiveDateFrom: row.EffectiveDateFrom,
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks:row.Remarks });
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_PriceDIO_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_PriceDIO_gridAPI.grid.clearAllFilters();
		MasterPricingDealerToBranchPriceDIOFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_PriceDIO_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceDIO_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_PriceDIO_grid.data);		
		MasterPricingDealerToBranchPriceDIOFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_PriceDIO_grid.data = [];
		$scope.MasterSellingPriceFU_PriceDIO_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_PriceDIO = "";
		$scope.TextFilterSatu_PriceDIO = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_PriceDIO' ) );
		myEl.val('');		
	}

	$scope.MasterSelling_PriceDIO_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_PriceDIO_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_PriceDIO_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_PriceDIO_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_PriceDIO_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_PriceDIO_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_PriceDIO_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_PriceDIO,
			Text1 : $scope.TextFilterSatu_PriceDIO,
			AndOr : $scope.Filter_PriceDIO_AndOr,
			Filter2 : $scope.KolomFilterDua_PriceDIO,
			Text2 : $scope.TextFilterDua_PriceDIO,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingDealerToBranchPriceDIOFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_PriceDIO_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_PriceDIO_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_PriceDIO_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_PriceDIO;
		$scope.MasterSellingPriceFU_PriceDIO_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_PriceDIO != "") {
			$scope.MasterSellingPriceFU_PriceDIO_gridAPI.grid.getColumn($scope.ComboFilterGrid_PriceDIO).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_PriceDIO_gridAPI.grid.clearAllFilters();

	}

});