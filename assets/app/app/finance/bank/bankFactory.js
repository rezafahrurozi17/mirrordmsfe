angular.module('app')
	.factory('BankFactory', function ($http, CurrentUser) {
		var user = CurrentUser.user();
	return{
		getData: function(){
			var url = '/api/fe/FinanceBank/start/1/limit/100';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataMT940: function(BankId){
			var url = '/api/fe/FinanceBank/GetMTData/?BankId=' + BankId + '&OutletId=' + user.OutletId;
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(BankData){
			var url = '/api/fe/FinanceBank/create/';
			var ArrayBankData = [BankData];
			var param = JSON.stringify(ArrayBankData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(BankData){
			var url = '/api/fe/FinanceBank/Update/';
			var ArrayBankData = [BankData];
			var param = JSON.stringify(ArrayBankData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/fe/FinanceBank/deletedata/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});