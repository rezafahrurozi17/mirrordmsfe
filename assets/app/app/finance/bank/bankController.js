var app = angular.module('app');
app.controller('BankController', function ($scope, $http, $filter, CurrentUser, BankFactory, $timeout, bsNotify) {
	var user = CurrentUser.user();
	var CurrentStatus = '';
	// //----------------------------------
	// // Start-Up
	// //----------------------------------
	// $scope.$on('$viewContentLoaded', function() {
	// //$scope.loading = true;
	// $scope.gridData = [];
	// });
	// //----------------------------------
	// // Initialization
	// //----------------------------------
	// $scope.user = CurrentUser.user();
	//$scope.mBank = null; //Model
	// $scope.xRole = { selected: [] };

	// var dateFormat = 'dd/MM/yyyy';

	// $scope.dateOption = { format: dateFormat }


	// $scope.cekObject = function() {
	// console.log($scope.mBank);
	// }

	// //----------------------------------
	// // Get Data
	// //----------------------------------
	// var gridData = [];
	// $scope.getData = function() {
	// BankFactory.getData()
	// .then(
	// function(res){
	// $scope.grid.data = res.data.Result;
	// }
	// );
	// }

	// function roleFlattenAndSetLevel(node, lvl) {
	// for (var i = 0; i < node.length; i++) {
	// node[i].$$treeLevel = lvl;
	// gridData.push(node[i]);
	// if (node[i].child.length > 0) {
	// roleFlattenAndSetLevel(node[i].child, lvl + 1)
	// } else {

	// }
	// }
	// return gridData;
	// }
	// $scope.selectRole = function(rows) {
	// console.log("onSelectRows=>", rows);
	// $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
	// }
	// $scope.onSelectRows = function(rows) {
	// console.log("onSelectRows=>", rows);
	// }

	$scope.optionsBank_SelectKolom = [{ name: "Kode Bank", value: "BankCode" }, { name: "Nama Bank", value: "BankName" }];

	$scope.MainBank_Show = true;
	$scope.AddBank_Show = false;
	angular.element('#FinanceBankModal').hide('');

	$scope.MainBank_Tambah_Clicked = function () {
		$scope.MainBank_Show = false;
		$scope.AddBank_Show = true;
		$scope.TambahBank_GetMTList(0);
		CurrentStatus = 'New';
	}

	$scope.BankMT940_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,
		rowSelection: false,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "TranTypeId", field: "TranTypeId", visible: false },
			{ name: "Transaction Type", field: "TransactionType", enableCellEdit: false},
			{ name: "Description", field: "Description", enableCellEdit: false},
			{
				name: "Mapping", field: "Mapping", 
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, enableCellEdit: true
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.BankMT940GridAPI = gridApi;
		}
	};

	$scope.AddBank_Kembali_Clicked = function () {
		$scope.MainBank_Show = true;
		$scope.AddBank_Show = false;
	}

	$scope.AddBank_Simpan_Clicked = function () {
		if (CurrentStatus == 'New') {
			var BankData = {
				"BankId": 0,
				"BankCode": $scope.BankCode,
				"BankName": $scope.BankName,
				"isActivateCent": $scope.isActivateCent,
				"BankMTModel": $scope.BankMT940_UIGrid.data
			};

			BankFactory.create(BankData)
				.then(
					function (res) {
						//alert("Data berhasil disimpan");
						bsNotify.show(
							{
								title: "Sukses",
								content: "Data berhasil disimpan",
								type: 'success'
							}
						);
						$scope.AddBank_ClearFields();
						$scope.MainBank_GetList();
						$scope.AddBank_Kembali_Clicked();
					},
					function (err) {
						console.log("Bunga DF Error : ", err);
						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);
		}
		else if (CurrentStatus == 'Update') {
			var BankData = {
				"BankId": $scope.BankId,
				"BankCode": $scope.BankCode,
				"BankName": $scope.BankName,
				"isActivateCent": $scope.isActivateCent,
				"BankMTModel": $scope.BankMT940_UIGrid.data
			};

			BankFactory.update(BankData)
				.then(
					function (res) {
						//alert("Data berhasil disimpan");
						bsNotify.show(
							{
								title: "Sukses",
								content: "Data berhasil disimpan",
								type: 'success'
							}
						);
						$scope.AddBank_ClearFields();
						$scope.MainBank_GetList();
						$scope.AddBank_Kembali_Clicked();
					},
					function (err) {
						console.log("Bunga DF Error : ", err);
						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak ditemukan.",
								type: 'danger'
							}
						);
					}
				);

		}
	}

	$scope.AddBank_ClearFields = function () {
		$scope.BankId = 0;
		$scope.BankCode = '';
		$scope.BankName = '';
		CurrentStatus = '';
		$scope.isActivateCent = false;
	}


	$scope.MainBank_EditData = function (row) {
		$scope.MainBank_Show = false;
		$scope.AddBank_Show = true;
		CurrentStatus = 'Update'

		$scope.BankId = row.entity.BankId;
		$scope.BankCode = row.entity.BankCode;
		$scope.BankName = row.entity.BankName;
		$scope.isActivateCent = row.entity.isActivateCent;
		$scope.TambahBank_GetMTList($scope.BankId);
	}

	$scope.actDel = function () {
		$scope.Bank_TotalSelectedData = $scope.MainBankGridAPI.selection.getSelectedCount();
		angular.element('#FinanceBankModal').modal('show');
	}

	$scope.FinanceBankOK_Button_Clicked = function () {
		var deleteData = [];
		angular.forEach($scope.MainBankGridAPI.selection.getSelectedRows(), function (row, index) {
			deleteData.push(row.BankId);
		});
		BankFactory.delete(deleteData).
			then(
				function (res) {
					$scope.MainBank_GetList();
					$scope.MainBankGridAPI.selection.clearSelectedRows();
				},
				function (err) {
					bsNotify.show({
						title: "Warning",
						content: err.data.Message,
						type: 'warning'
					});
				}
			);
	}

	$scope.FinanceBankCancel_Button_Clicked = function () {
		angular.element('#FinanceBankModal').modal('hide');
	}

	$scope.actRefresh = function () {
		$scope.MainBank_GetList();
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.MainBank_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		paginationPageSize: 10,
		enableSorting: true,
		enableRowSelection: false,
		multiSelect: true,
		enableSelectAll: true,
		enableFiltering: true,

		columnDefs: [
			{ name: 'id', field: 'BankId', visible: false },
			{ name: 'Kode Bank', field: 'BankCode' },
			{ name: 'Nama Bank', field: 'BankName' },
			{ name: 'Aktifkan 2 Dijit Belakang Koma', field: 'isActivateCent', type: 'boolean', cellTemplate: '<input type="checkbox" ng-disabled="true" ng-model="row.entity.isActivateCent">', enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.MainBank_EditData(row)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'
			}

		],
		onRegisterApi: function (gridApi) {
			$scope.MainBankGridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("$scope.MainBankGridAPI.selection.getSelectedCount()", $scope.MainBankGridAPI.selection.getSelectedCount());
				console.log("row", row);
				$scope.selectedRows = $scope.MainBankGridAPI.selection.getSelectedRows();
			});
		}

	};

	$scope.MainBank_GetList = function () {
		BankFactory.getData()
			.then(
				function (res) {
					$scope.MainBank_UIGrid.data = res.data.Result;
					$scope.MainBankGridAPI.pagination.seek(1);
				}
			);
	}

	$scope.MainBank_GetList();

	$scope.TambahBank_GetMTList = function (BankId) {
		BankFactory.getDataMT940(BankId)
			.then(
				function (res) {
					$scope.BankMT940_UIGrid.data = res.data.Result;
				}
			)
	}

	$scope.Bank_Filter_Clicked = function () {
		var nomor;
		var value = $scope.Bank_TeksFilter;

		$scope.MainBankGridAPI.grid.clearAllFilters();
		$scope.MainBankGridAPI.grid.refresh();

		if (value != "") {
			if ($scope.Bank_SelectKolom == 'BankCode') {
				nomor = 2;
			}
			else if ($scope.Bank_SelectKolom == 'BankName') {
				nomor = 3;
			}

			$scope.MainBankGridAPI.grid.columns[nomor].filters[0].term = value;
			$scope.MainBankGridAPI.grid.refresh();
		}

	}

	$scope.Bank_SelectKolom_Changed = function () {
		$scope.Bank_TeksFilter = "";
	}
});