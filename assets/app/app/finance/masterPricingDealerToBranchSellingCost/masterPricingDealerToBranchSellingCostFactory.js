angular.module('app')
	.factory('MasterPricingDealerToBranchSellingCostFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	var factory={};
	var debugMode=true;
		
	factory.VerifyData= function(KlasifikasiPE, IsiGrid, TypePE ) {
    	//var url = '/api/fe/IncomingInstruction';
		var inputData =  [{Classification : KlasifikasiPE , Grid : IsiGrid , JenisPE : TypePE}];
			var url = '/api/fe/MasterPESellingCost/Verify/';
    	var param = JSON.stringify(inputData);

		if (debugMode){console.log('Masuk ke submitData')};
		if (debugMode){console.log('url :'+url);};
		if (debugMode){console.log('Parameter POST :'+param)};
      	var res=$http.post(url, param);
      	
		return res;
    };

	factory.getData=function(start,limit,filterData) {
			//var url = '/api/fe/Branch/SelectData/start/0/limit/25/filterData/0';
      var url = '/api/fe/MasterPESellingCost/SelectData?start=' + start + '&limit=' + limit + '&filterData=' + filterData;
      var res=$http.get(url);  
      return res;			
    };		

	factory.Submit= function(KlasifikasiPE, IsiGrid, TypePE) {
    	//var url = '/api/fe/IncomingInstructionCancel';
			var inputData =  [{Classification : KlasifikasiPE , Grid : IsiGrid , JenisPE : TypePE}];
			var url = '/api/fe/MasterPESellingCost/Save/';
    		var param = JSON.stringify( inputData);

			if (debugMode){console.log('Masuk ke submitData')};
			if (debugMode){console.log('url :'+url);};
			if (debugMode){console.log('Parameter POST :'+inputData)};
					var res=$http.post(url, inputData);
					
			return res;
    };
	return factory;
});