var app = angular.module('app');
app.controller('MasterPricingDealerToBranchSellingCostController', function ($scope, $http, $filter, CurrentUser, MasterPricingDealerToBranchSellingCostFactory, $timeout) {
	$scope.optionsKolomFilterSatu_SellingCost = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_SellingCost = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_SellingCost = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_SellingCost = [
			{name:"Dealer Name", value:"DealerName"},
			{name:"Province", value:"Province"}, {name:"Price Area", value:"PriceArea"}, 
			{name:"Delivery Cost", value:"DeliveryCost"},
			{name:"Adm Cost", value:"AdmCost"},
			{name:"Inventory Cost", value:"InventoryCost"},
			{name:"Effective Date From", value:"EffectiveDateFrom"},
			{name:"Effective Date To", value:"EffectiveDateTo"}, {name:"Remarks", value:"Remarks"}
	];
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'SellingCost';
	angular.element('#SellingCostModal').modal('hide');
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_SellingCost = false;
	$scope.cekKolom = '';
	$scope.MasterSellingPriceFU_SellingCost_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],	
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ name: 'DealerName', displayName: 'Dealer Name', field: 'DealerName',enableCellEdit: false , enableHiding: false},			
			{ name: 'Province', displayName: 'Province', field: 'Province',enableCellEdit: false , enableHiding: false},
			{ name: 'PriceArea', displayName: 'Price Area', field: 'PriceArea',enableCellEdit: false, enableHiding: false },
			{ name: 'DeliveryCost', displayName: 'Delivery Cost', field: 'DeliveryCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number" },	
			{ name: 'InventoryCost', displayName: 'Inventory Cost', field: 'InventoryCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number" },
			{ name: 'AdmCost', displayName: 'Adm Cost', field: 'AdmCost',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number" },
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date From', field: 'EffectiveDateFrom',enableCellEdit: false , enableHiding: false},
			{ name: 'EffectiveDateTo', displayName: 'Effective Date To', field: 'EffectiveDateTo',enableCellEdit: false , enableHiding: false},  //,cellFilter: 'date:\"dd-MM-yyyy\"'
			{ name: 'Remarks', displayName: 'Remarks', field: 'Remarks',enableCellEdit: true, enableHiding: false }, 
			{ name: 'SellingCostId',displayName:'Id', field: 'SellingCostId', visible: false, enableHiding: false },		
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_SellingCost_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_SellingCost' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

				$scope.MasterSelling_SellingCost_Upload_Clicked();
				myEl.val('');
            });
	}

	$scope.validateColumn = function(inputExcelData){
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.DeliveryCost)) {
			$scope.cekKolom = $scope.cekKolom + ' DeliveryCost \n';
		}
		
		if (angular.isUndefined(inputExcelData.DealerName)) {
			$scope.cekKolom = $scope.cekKolom + ' DealerName \n';
		}
		if 	(angular.isUndefined(inputExcelData.Province)){
			$scope.cekKolom = $scope.cekKolom + ' Province \n';
		}
		if (angular.isUndefined(inputExcelData.PriceArea)) {
			$scope.cekKolom = $scope.cekKolom + ' PriceArea \n';
		}
		
		if (angular.isUndefined(inputExcelData.InventoryCost)) {
			$scope.cekKolom = $scope.cekKolom + ' InventoryCost \n';
		}
		if 	(angular.isUndefined(inputExcelData.AdmCost)){
			$scope.cekKolom = $scope.cekKolom + ' AdmCost \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}

		if ( angular.isUndefined(inputExcelData.DeliveryCost) || 
			angular.isUndefined(inputExcelData.DealerName) ||
			angular.isUndefined(inputExcelData.Province) ||
			angular.isUndefined(inputExcelData.PriceArea) ||
			angular.isUndefined(inputExcelData.InventoryCost) ||
			angular.isUndefined(inputExcelData.AdmCost) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.ComboBulkAction_SellingCost_Changed = function()
	{
		if ($scope.ComboBulkAction_SellingCost == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_BBN_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_BBN_grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_SellingCost_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				angular.element('#SellingCostModal').modal('show');
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_SellingCost_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_SellingCost_grid.data.splice($scope.MasterSellingPriceFU_SellingCost_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_SellingCost = "";
		angular.element('#SellingCostModal').modal('hide');
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_SellingCost = "";
		angular.element('#SellingCostModal').modal('hide');
	}		

	$scope.MasterSelling_SellingCost_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingSellingCost";

		if ($scope.MasterSellingPriceFU_SellingCost_grid.data.length == 0){
				excelData.push({ 
						DealerName : "",
						Province: "",
						PriceArea: "",
						DeliveryCost : "",
						InventoryCost : "",
						AdmCost : "",
						EffectiveDateFrom: "",
						EffectiveDateTo: "",
						Remarks: "" });
			fileName = "MasterSellingSellingCostTemplate";		
		}
		else {
		//$scope.MasterSellingPriceFU_SellingCost_gridAPI.selection.getSelectedRows().forEach(function(row) {
			$scope.MasterSellingPriceFU_SellingCost_grid.data.forEach(function(row) {
				excelData.push({ 
						DealerName : row.DealerName,
						Province: row.Province,
						PriceArea: row.PriceArea,
						DeliveryCost : row.DeliveryCost,
						InventoryCost : row.InventoryCost,
						AdmCost : row.AdmCost,
						EffectiveDateFrom: row.EffectiveDateFrom,
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks:row.Remarks });
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_SellingCost_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_SellingCost_gridAPI.grid.clearAllFilters();
		MasterPricingDealerToBranchSellingCostFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
//				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_SellingCost_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_SellingCost_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_SellingCost_grid.data);		
		MasterPricingDealerToBranchSellingCostFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_SellingCost_grid.data = [];
		$scope.MasterSellingPriceFU_SellingCost_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_SellingCost = "";
		$scope.TextFilterSatu_SellingCost = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_SellingCost' ) );
		myEl.val('');		
	}

	$scope.MasterSelling_SellingCost_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_SellingCost_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_SellingCost_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_SellingCost_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_SellingCost_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_SellingCost_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_SellingCost_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_SellingCost,
			Text1 : $scope.TextFilterSatu_SellingCost,
			AndOr : $scope.Filter_SellingCost_AndOr,
			Filter2 : $scope.KolomFilterDua_SellingCost,
			Text2 : $scope.TextFilterDua_SellingCost,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingDealerToBranchSellingCostFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_SellingCost_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_SellingCost_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_SellingCost_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_SellingCost;
		$scope.MasterSellingPriceFU_SellingCost_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_SellingCost != "") {
			$scope.MasterSellingPriceFU_SellingCost_gridAPI.grid.getColumn($scope.ComboFilterGrid_SellingCost).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_SellingCost_gridAPI.grid.clearAllFilters();

	}

});