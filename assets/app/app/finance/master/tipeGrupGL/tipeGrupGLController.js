var app = angular.module('app');
app.controller('TipeGrupGLController', function ($scope, $http, $filter, CurrentUser, TipeGrupGLFactory, $timeout, bsNotify) {

	var limit = 10;
	$scope.getData = function () {
		TipeGrupGLFactory.getData(1,limit)
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$scope.grid.paginationCurrentPage = 1;
					
					$timeout(function(){
						$scope.grid.totalItems = res.data.Total;
						$scope.grid.paginationPageSize = limit;
						$scope.grid.paginationPageSizes = [10, 25, 50];
					},100);
					console.log("test",$scope.grid);
		
				}
			);
	}
// Get Data Pagingnation
$scope.getData2 = function (start, limit) {
	TipeGrupGLFactory.getData(start, limit)
		.then(
			function (res) {
				$scope.grid.data = res.data.Result;
				
				$timeout(function(){
					$scope.grid.totalItems = res.data.Total;
					$scope.grid.paginationPageSize = limit;
					$scope.grid.paginationPageSizes = [10, 25, 50];
				},100);
			
			}
		);
}

	$scope.optionsTipeGrupGL_MappingGroup = [];
	$scope.gridDetail_Data = [];
	$scope.Show_MainGrupGL = true;
	$scope.Show_TambahGrupGL = false;
	$scope.getData();

	$scope.LihatGrupGL_Edit = function (row) {
		//$scope.SetDataDetail();
		$scope.GLGroupTypeName = row.entity.GLGroupTypeName;
		$scope.GLGroupTypeId = row.entity.GLGroupTypeId;
		$scope.GetDataDetail($scope.GLGroupTypeId);
		$scope.Show_MainGrupGL = false;
		$scope.Show_TambahGrupGL = true;
		$scope.Show_TambahGrupGL_ButtonSimpan = false;
		$scope.Show_TambahGrupGL_UbahSimpan = true;
	}
	////////////

	$scope.TipeGrupGL_GetMappingGroup = function () {
		TipeGrupGLFactory.getDataMappingGroup()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.optionsTipeGrupGL_MappingGroup.push({ name: value.Label, value: { 'MappingGroupId': value.ValueId, 'MappingGroupName': value.Label } });
					});
				}
			);
	}
	$scope.TipeGrupGL_GetMappingGroup();

	$scope.LihatGrupTypeGL_Hapus = function (row) {
		$scope.HapusGrupGL_GLGrupName = row.entity.GLGroupTypeName;
		$scope.HapusGrupGL_GLGrupId = row.entity.GLGroupTypeId;
		angular.element('#ModalHapusGrupGL').modal('show');
	}

	$scope.Batal_ModalHapusGrupGL = function () {
		$scope.HapusTranGL_GLGrupName = "";
		$scope.HapusTranGL_GLGrupId = "";
		angular.element('#ModalHapusGrupGL').modal('hide');
	}

	$scope.Hapus_ModalHapusGrupGL = function () {
		TipeGrupGLFactory.delete($scope.HapusGrupGL_GLGrupId)
			.then(
				function (res) {
					//alert("Data berhasil dihapus");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil dihapus.",
						type: "success"
					});
					$scope.getData();
					$scope.Batal_ModalHapusGrupGL();
				}
			);
	}
	//////////
	$scope.MainTipeGrupGL_Tambah_Clicked = function () {
		$scope.Show_MainGrupGL = false;
		$scope.Show_TambahGrupGL = true;
		$scope.Show_TambahGrupGL_ButtonSimpan = true;
		$scope.Show_TambahGrupGL_UbahSimpan = false;
		//$scope.SetDataDetail();
		$scope.gridDetail_Data = [];
		$scope.gridDetail.data = $scope.gridDetail_Data;
		//$scope.optionsTipeGrupGL_MappingGroup = [];
		$scope.TipeGrupGL_MappingGroup = "";
		$scope.TipeGrupGL_GLList = "";
		$scope.optionsTipeGrupGL_GLList = [];

	}

	$scope.TambahGrupGL_ClearFields = function () {
		$scope.gridDetail_Data = [];
		$scope.TipeGrupGL_MappingGroup = [];
		$scope.TipeGrupGL_GLList = [];
		$scope.GLGroupTypeName = "";
		$scope.TipeGrupGL_NamaGL = "";
	}

	$scope.TambahGrupGL_Batal_Clicked = function () {
		$scope.Show_TambahGrupGL = false;
		$scope.Show_MainGrupGL = true;
		$scope.TambahGrupGL_ClearFields();
		$scope.getData();
	}

	$scope.TambahGrupGL_Simpan_Clicked = function () {
		var GroupTypeGLDetailData = [];
		angular.forEach($scope.gridDetail_Data, function (value, key) {
			GroupTypeGLDetailData.push(
				{
					"GLAccountMappingGroupId": value.GLAccountMappingGroupId,
					"GLAccountMappingGroupName": value.GLAccountMappingGroupName,
					"GLAccountId": value.GLAccountId,
					"GLAccountCode": value.GLAccountCode,
					"GLAccountName": value.GLAccountName,
					"COAOverrideName": value.COAOverrideName,
					"GroupingId": value.GroupingId
				}
			);
		});

		var inputData = [
			{
				"GLGroupTypeName": $scope.GLGroupTypeName,
				"GLGroupTypeDetail": GroupTypeGLDetailData
			}
		];

		TipeGrupGLFactory.create(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
					$scope.Show_TambahGrupGL = false;
					$scope.Show_MainGrupGL = true;
					$scope.TambahGrupGL_ClearFields();
					$scope.getData(1, 10);
				}
			);
	}

	$scope.TambahGrupGL_Ubah_Clicked = function () {
		var GroupTypeGLDetailData = [];
		angular.forEach($scope.gridDetail_Data, function (value, key) {
			GroupTypeGLDetailData.push(
				{
					"GLAccountMappingGroupName": value.GLAccountMappingGroupName,
					"GLAccountMappingGroupId": value.GLAccountMappingGroupId,
					"GLAccountId": value.GLAccountId,
					"GLAccountCode": value.GLAccountCode,
					"GLAccountName": value.GLAccountName,
					"COAOverrideName": value.COAOverrideName,
					"GroupingId": value.GroupingId
				}
			);
		});

		var inputData = [
			{
				"GLGroupTypeId": $scope.GLGroupTypeId,
				"GLGroupTypeName": $scope.GLGroupTypeName,
				"GLGroupTypeDetail": GroupTypeGLDetailData
			}
		];

		TipeGrupGLFactory.update(inputData)
			.then(
				function (res) {
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
					//alert("Data berhasil disimpan");
					$scope.Show_TambahGrupGL = false;
					$scope.Show_MainGrupGL = true;
					$scope.TambahGrupGL_ClearFields();
					$scope.getData();
				}
			);
	}

	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		useCustomPagination: true,
		useExternalPagination: true,
		paginationPageSize : null,
		paginationPageSizes: [10, 25, 50],

		columnDefs: [
			{ name: 'id', field: 'GLGroupTypeId', visible: false },
			{ name: 'Tipe Grup GL', field: 'GLGroupTypeName' },
			{
				name: 'Action',
				cellTemplate: '<a ng-click="grid.appScope.LihatGrupGL_Edit(row)"><i class="fa fa-fw fa-lg fa-pencil" style="margin-left:8px;margin-bottom:-15px;"></i></a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.MainTipeGrupGLGridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("$scope.MainMasterEDCGridAPI.selection.getSelectedCount()", $scope.MainTipeGrupGLGridAPI.selection.getSelectedCount());
				console.log("row", row);
				$scope.selectedRows = $scope.MainTipeGrupGLGridAPI.selection.getSelectedRows();
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$timeout(function(){
					$scope.getData2(pageNumber, pageSize);
				},100)
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		}
	};

	$scope.GetDataDetail = function (glGroupTypeId) {
		TipeGrupGLFactory.getDataDetail(glGroupTypeId)
			.then(
				function (res) {
					console.log("result", res.data.Result);
					$scope.gridDetail_Data = res.data.Result;
					$scope.gridDetail.data = $scope.gridDetail_Data;
				}
			);
	}

	$scope.actDel = function () {
		$scope.totalData = 0;
		$scope.totalData = $scope.MainTipeGrupGLGridAPI.selection.getSelectedCount()

		angular.element('#ModalHapusGrupGL').modal('show');
	}

	$scope.TipeGrupGLOK_Button_Clicked = function () {
		var deleteData = [];
		angular.forEach($scope.MainTipeGrupGLGridAPI.selection.getSelectedRows(), function (row, index) {
			deleteData.push(row.GLGroupTypeId);
		});
		TipeGrupGLFactory.delete(deleteData).
			then(
				function (res) {
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil dihapus.",
						type: "success"
					});
					$scope.getData();
					$scope.MainTipeGrupGLGridAPI.selection.clearSelectedRows();
				}
			);
	}

	$scope.TipeGrupGLCancel_Button_Clicked = function () {
		angular.element('#ModalHapusGrupGL').modal('hide');
	}


	$scope.SetDataDetail = function () {
		var GLListArray = [];

		TipeGrupGLFactory.getDataGLList()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						GLListArray.push({ "name": value.COAOverrideName, "value": { "GLAccountId": value.GLAccountId, "GLAccountName": value.GLAccountName, "GLAccountCode": value.GLAccountCode, "COAOverrideName": value.COAOverrideName, "GroupingId": value.GroupingId } });
					});

					$scope.optionsTipeGrupGL_GLList = GLListArray;
				}
			);
	}

	$scope.UpdateMapping = function () {
		var GLListArray = [];
		if ($scope.TipeGrupGL_MappingGroup != undefined) {
			TipeGrupGLFactory.getDataPGAPerMappingGroup($scope.TipeGrupGL_MappingGroup.MappingGroupId)
				.then(
					function (res) {
						//var temp = [];
						angular.forEach(res.data.Result, function (value, key) {
							GLListArray.push({ "name": value.COAOverrideName, "value": { "GLAccountId": value.GLAccountId, "GLAccountName": value.GLAccountName, "GLAccountCode": value.GLAccountCode, "COAOverrideName": value.COAOverrideName, "GroupingId": value.GroupingId } });
							// temp.push({

							// 	"GroupingId": value.GroupingId, "Parameter": value.COAOverrideName, "Description": value.COAOverrideDesc,
							// 	"GLAccountID": value.GLAccountId, "GLAccountNo": value.GLAccountCode, "GLAccountName": value.GLAccountName
							// });
						});
						//$scope.PGA_MappingGLAccount_UIGrid.data = temp;
						$scope.optionsTipeGrupGL_GLList = GLListArray;
					}
				);

		}
		else {
			$scope.optionsTipeGrupGL_GLList = [];
		}

	}

	$scope.TipeGrupGL_GLList_Changed = function () {
		if (!angular.isUndefined($scope.TipeGrupGL_GLList)) {
			$scope.TipeGrupGL_NamaGL = $scope.TipeGrupGL_GLList.GLAccountName;
		}
	}

	$scope.tipeGrupGL_Delete_Clicked = function (row) {
		var index = 0;
		angular.forEach($scope.gridDetail_Data, function (value, key) {
			if (value.GLAccountId == row.entity.GLAccountId) {
				$scope.gridDetail_Data.splice(index, 1);
				$scope.gridDetail.data = $scope.gridDetail_Data;
			}
			index++;
		});
	}



	$scope.TambahGrupGL_Tambah_Clicked = function () {
		console.log("selected gl :", $scope.TipeGrupGL_GLList);

		$scope.gridDetail_Data.push({
			"GLAccountMappingGroupId": $scope.TipeGrupGL_MappingGroup.MappingGroupId,
			"GLAccountMappingGroupName": $scope.TipeGrupGL_MappingGroup.MappingGroupName,
			"GLAccountId": $scope.TipeGrupGL_GLList.GLAccountId,
			"GLAccountCode": $scope.TipeGrupGL_GLList.GLAccountCode,
			"GLAccountName": $scope.TipeGrupGL_GLList.GLAccountName,
			"COAOverrideName": $scope.TipeGrupGL_GLList.COAOverrideName,
			"GroupingId": $scope.TipeGrupGL_GLList.GroupingId
		});
		console.log("gridDetail :", $scope.gridDetail_Data);
		$scope.gridDetail.data = $scope.gridDetail_Data;
		$scope.GridApiGridDetail.grid.refresh();
	}

	$scope.gridDetail = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: 'id', field: 'GLAccountId', visible: false },
			{ name: 'Mapping Group Code', field: 'GLAccountMappingGroupId', visible: false },
			{ name: 'Mapping Group', field: 'GLAccountMappingGroupName' },
			{ name: 'Parameter', field: 'COAOverrideName' },
			{ name: 'GL Account Code', field: 'GLAccountCode', visible: false },
			{ name: 'GroupingId', field: 'GroupingId', visible: false },
			{ name: 'Deskripsi', field: 'GLAccountName' },
			{
				name: 'Action',
				cellTemplate: '<a ng-click="grid.appScope.tipeGrupGL_Delete_Clicked(row)">Hapus</a>'
			},
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiGridDetail = gridApi;
		}
	}
});