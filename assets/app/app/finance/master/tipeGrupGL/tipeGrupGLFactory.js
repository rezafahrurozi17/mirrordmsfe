angular.module('app')
	.factory('TipeGrupGLFactory', function ($http, CurrentUser) {
		var mappingGroupId = 3850;
		return{
		
		getData: function(start, limit){
			var url = '/api/fe/FinanceGLGroupType/start/'+start+'/limit/'+limit;
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create: function(inputData) {
            var url = '/api/fe/FinanceGLGroupType/Create/';
			var param = JSON.stringify(inputData);
			console.log("object input GL Group Type ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update: function(inputData) {
            var url = '/api/fe/FinanceGLGroupType/Update/';
			var param = JSON.stringify(inputData);
			console.log("object input GL Group Type ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(grupId){
			var url = '/api/fe/FinanceGLGroupType/DeleteData/';
			console.log("url delete data : ", url);
			//var res=$http.delete(url);
			var res=$http.delete(url, {data:grupId, headers:{'content-type' : 'application/json'}});
			return res;	
		},
		
		getDataDetail:function(groupTypeId){
			var url = '/api/fe/FinanceGLGroupTypeDetail/start/1/limit/100/GroupTypeId/' + groupTypeId;
			console.log("url GroupTypeDetailId : ", url);
			var res=$http.get(url);
			return res;	
		},
		
		getDataGLList:function(){
			var url = '/api/fe/FinanceGLGroupTypeDetail/GetGLList';
			console.log("url Get GL List : ", url);
			var res=$http.get(url);
			return res;	
		},

		getDataMappingGroup : function(){
			var url = '/api/fe/FinanceParameterGLAccount/GetParamInternal?groupId='+mappingGroupId;
			console.log("url Mapping Group : ", url);
			var response=$http.get(url);
			return response;
		},

		getDataPGAPerMappingGroup : function(mappingValueId){
			var url = '/api/fe/FinanceParameterGLAccount/GetDataPerMappingGroup?valueId='+mappingValueId;
			console.log("url Data Per Mapping Group : ", url);
			var response=$http.get(url);
			return response;
		},
	}
});