var app = angular.module('app');
app.controller('MasterEDCController', function ($scope, $http, $filter, CurrentUser, MasterEDCFactory, $timeout,bsNotify) {
	
	$scope.HapusEDC_EDCId=0;
	$scope.user = CurrentUser.user();

	$scope.getData = function() {
		MasterEDCFactory.getData($scope.LihatMasterEDC_NamaBranch_SearchDropdown)
		.then(
			function(res){
				$scope.grid.data = res.data.Result;
			}
		);
	}

	
	//$scope.SetComboBranch();
	// $scope.gridDetail_Data = [];
	$scope.Show_MainEDC = true;
	//$scope.user = CurrentUser.user();
	// $scope.Show_TambahEDC = false;
	// $scope.getData();

	$scope.$on('$viewContentLoaded', function () {
		//$scope.getDataBranchCB();
		$scope.SetComboBranch();
	});	
	
	
	$scope.LihatEDC_Edit =function(row){
		$scope.currentNoRekPengirim_Search="";
		$scope.currentEDCId="";
		$scope.TambahMasterEDC_NamaBranch_SearchDropdown = row.entity.OutletId;		
		$scope.getDataBankAccount($scope.TambahMasterEDC_NamaBranch_SearchDropdown);		
		console.log(row.entity.AccountId);	
		console.log($scope.MasterEDC_optionsNoRekening);
		$scope.MasterEDC_NamaEDC = row.entity.EDCName;
		$scope.currentNoRekPengirim_Search = row.entity.AccountId;
		$scope.currentEDCId = row.entity.EDCId;
		console.log($scope.currentNoRekPengirim_Search);
		$scope.MasterEDC_NoRekening = $scope.currentNoRekPengirim_Search;
		$scope.Show_MainEDC = false;
		$scope.Show_TambahEDC = true;
		$scope.Show_TambahEDC_ButtonSimpan = true;
		$scope.Show_TambahEDC_UbahSimpan = true;
	}

	$scope.getDataBankAccount = function (OutletId) 
	{
		MasterEDCFactory.getDataBankAccountComboBox(OutletId)
			.then(
			function (res) {
				$scope.loading = false;
				$scope.MasterEDC_optionsNoRekening = res.data;
			},
			function (err) {
				console.log("err=>", err);
			}
			);
	}
	
	
	$scope.LihatEDC_Hapus = function(row){
		$scope.HapusEDC_EDCName = row.entity.EDCName;
		$scope.HapusEDC_EDCId = row.entity.EDCId;
		angular.element('#ModalHapusEDC').modal('show');
	}
	
	$scope.Batal_ModalHapusEDC = function(){
		$scope.HapusEDC_EDCName = "";
		$scope.HapusEDC_EDCId = "";
		angular.element('#ModalHapusEDC').modal('hide');
	}
	
	$scope.Hapus_ModalHapusEDC = function(){
		MasterEDCFactory.delete($scope.HapusEDC_EDCId)
		.then(
			function(res){
				//alert("Data berhasil dihapus");
				bsNotify.show({
					title:"Berhasil",
					content:"Data berhasil dihapus.",
					type:"success"
				});
				$scope.getData($scope.LihatMasterEDC_NamaBranch_SearchDropdown);
				$scope.Batal_ModalHapusEDC();
			}
		);
	}

	$scope.MainTipeEDC_Tambah_Clicked = function(){
		$scope.Show_MainEDC = false;
		$scope.Show_TambahEDC = true;
		$scope.Show_TambahEDC_ButtonSimpan = true;
		$scope.Show_TambahEDC_UbahSimpan = false;
	}

	$scope.LihatMasterEDC_Cari = function()
	{
		$scope.getData();
	}

	$scope.TambahMasterEDC_InformasiBranch_Selected_Changed = function(){
		console.log("TambahMasterEDC_NamaBranch_SearchDropdown");
		if(angular.isUndefined($scope.TambahMasterEDC_NamaBranch_SearchDropdown) || $scope.TambahMasterEDC_NamaBranch_SearchDropdown == "")
		{
			console.log("TambahMasterEDC_NamaBranch_SearchDropdown");
		}
		else
		{
			$scope.getDataBankAccount($scope.TambahMasterEDC_NamaBranch_SearchDropdown);
			$scope.MasterEDC_NoRekening = "";			
		}
	}

	// $scope.TambahMasterEDC_Cari = function()
	// {
	// 	$scope.getDataBankAccount($scope.TambahMasterEDC_NamaBranch_SearchDropdown);
	// }	
	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}
	$scope.SetComboBranch = function () {
		var ho;
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.Main_NamaBranch_EnableDisable = true;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
			ho = 0;
		}

		MasterEDCFactory.getDataBranch(ho)
		.then
		(
			function (res) 
			{
				$scope.loading = false;
				$scope.optionsLihatMasterEDC_NamaBranch_Search = res.data.Result;
				$scope.optionsTambahMasterEDC_NamaBranch_Search = res.data.Result;
				$scope.LihatMasterEDC_NamaBranch_SearchDropdown =  $scope.user.OutletId;	
				$scope.TambahMasterEDC_NamaBranch_SearchDropdown =  $scope.user.OutletId;	
				$scope.getDataBankAccount($scope.TambahMasterEDC_NamaBranch_SearchDropdown);											
			},
			function (err) 
			{
				console.log("err=>", err);
			}
		);
	};

	
	
	
	
	$scope.TambahEDC_ClearFields = function(){
		$scope.MasterEDC_NamaEDC = "";
		$scope.MasterEDC_NoRekening = "";
		//$scope.TambahMasterEDC_NamaBranch_SearchDropdown = "";
		$scope.currentEDCId = "";
	}
	
	$scope.TambahEDC_Batal_Clicked = function(){
		$scope.Show_TambahEDC = false;
		$scope.Show_MainEDC = true;
		$scope.TambahEDC_ClearFields();
		$scope.getData();
	}
	
	$scope.LoadingSimpan = false;
	$scope.TambahEDC_Simpan_Clicked = function(){
		$scope.LoadingSimpan = true;
		
		var inputData = [
			{
				"EDCId" : $scope.currentEDCId,
				"EDCName" : $scope.MasterEDC_NamaEDC,
				"AccountId" : $scope.MasterEDC_NoRekening,
				"OutletId" : $scope.TambahMasterEDC_NamaBranch_SearchDropdown
			}
		];
		
		MasterEDCFactory.create(inputData)
		.then(
			function(res){
				//alert("Data berhasil disimpan");
				bsNotify.show({
					title:"Berhasil",
					content:"Data berhasil disimpan.",
					type:"success"
				});
				$scope.LoadingSimpan = false;
				$scope.Show_TambahEDC = false;
				$scope.Show_MainEDC = true;
				$scope.TambahEDC_ClearFields();
				$scope.getData();
			},
			function (err) 
			{
				console.log("err=>", err);
				if (err != null) 
				{
					alert(err.data.Message);
				}
				$scope.LoadingSimpan = false;	
			}
		);
	}
	
	// $scope.TambahEDC_Ubah_Clicked = function(){
	// 	var GroupTypeGLDetailData = [];
	// 	angular.forEach($scope.gridDetail_Data, function(value, key){
	// 		GroupTypeGLDetailData.push(
	// 			{
	// 				"GLAccountId" : value.GLAccountId,
	// 				"GLAccountCode" : value.GLAccountCode,
	// 				"GLAccountName" : value.GLAccountName
	// 			}
	// 		);
	// 	});
		
	// 	var inputData = [
	// 		{
	// 			"GLGroupTypeId" : $scope.GLGroupTypeId,
	// 			"GLGroupTypeName" : $scope.GLGroupTypeName,
	// 			"GLGroupTypeDetail" : GroupTypeGLDetailData
	// 		}
	// 	];
		
	// 	masterEDCFactory.update(inputData)
	// 	.then(
	// 		function(res){
	// 			alert("Data berhasil disimpan");
	// 			$scope.Show_TambahEDC = false;
	// 			$scope.Show_MainEDC = true;
	// 			$scope.TambahEDC_ClearFields();
	// 			$scope.getData();
	// 		}
	// 	);
	// }
	$scope.TipeMasterEDCOK_Button_Clicked = function(){
		var deleteData = [];
		angular.forEach($scope.MainMasterEDCGridAPI.selection.getSelectedRows(), function(row, index){
			deleteData.push(row.EDCId);
		});
		MasterEDCFactory.delete(deleteData).
		then(
			function(res){
				$scope.MainMasterEDC_GetList();
				$scope.MainMasterEDCGridAPI.selection.clearSelectedRows();
			}
		);
	}
	
	$scope.MasterEDCCancel_Button_Clicked = function(){
		angular.element('#MasterEDCModal').modal('hide');
	}

	$scope.actDel = function(){
		$scope.MasterEDC_TotalSelectedData = $scope.MainMasterEDCGridAPI.selection.getSelectedCount();
		angular.element('#MasterEDCModal').modal('show');
	}
	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		paginationPageSizes: [10, 25, 50],

		columnDefs: [
			{ name: 'id', field: 'EDCId', visible: false },
			{ name: 'Nama Mesin EDC', field: 'EDCName' },
			{ name: 'No Rekening', field: 'AccountId', visible:false },
			{ name: 'Branch', field: 'OutletId', visible:false },
			{ name: 'Nomor Rekening', field: 'AccountNo' },
			{
				name: "Action",
				              cellTemplate: '<a ng-click="grid.appScope.LihatEDC_Edit(row)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'
			}

		],
		onRegisterApi: function (gridApi) {
			$scope.MainMasterEDCGridAPI= gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("$scope.MainMasterEDCGridAPI.selection.getSelectedCount()", $scope.MainMasterEDCGridAPI.selection.getSelectedCount());
				console.log("row", row);
				$scope.selectedRows = $scope.MainMasterEDCGridAPI.selection.getSelectedRows();
			});
		}
	};

	$scope.MainMasterEDC_GetList = function(){
		MasterEDCFactory.getData($scope.LihatMasterEDC_NamaBranch_SearchDropdown)
		.then(
			function(res){
				$scope.grid.data = res.data.Result;
			}
		);
	}

	$scope.tipeTranGL_Delete_Clicked = function(row){
		$scope.gridDetail_Data.splice(row.entity.indexOf, 1);
		$scope.gridDetail.data = $scope.gridDetail_Data;
	}
	
	// $scope.GetDataDetail = function(glGroupTypeId){
	// 	masterEDCFactory.getDataDetail(glGroupTypeId)
	// 	.then(
	// 		function(res){
	// 			console.log("result", res.data.Result);
	// 			$scope.gridDetail_Data = res.data.Result;
	// 			$scope.gridDetail.data = $scope.gridDetail_Data;
	// 		}
	// 	);
	// }
	
	
	
	// $scope.SetDataDetail = function(){
	// 	var GLListArray = [];
		
	// 	masterEDCFactory.getDataGLList()
	// 	.then(
	// 		function(res){
	// 			angular.forEach(res.data.Result, function(value, key){
	// 				GLListArray.push({"name":value.GLAccountCode, "value":{"GLAccountId":value.GLAccountId, "GLAccountName":value.GLAccountName, "GLAccountCode":value.GLAccountCode}});
	// 			});
				
	// 			$scope.optionsTipeEDC_GLList = GLListArray;
	// 		}
	// 	);
	// }
	
	// $scope.TipeEDC_GLList_Changed = function(){		
	// 	if(!angular.isUndefined($scope.TipeEDC_GLList)){
	// 		$scope.TipeEDC_NamaGL = $scope.TipeEDC_GLList.GLAccountName;
	// 	}
	// }
	
	// $scope.TipeEDC_Delete_Clicked = function(row){
	// 	var index = 0;
	// 	angular.forEach($scope.gridDetail_Data, function(value, key){
	// 		if(value.GLAccountId == row.entity.GLAccountId){
	// 			$scope.gridDetail_Data.splice(index, 1);
	// 			$scope.gridDetail.data = $scope.gridDetail_Data;
	// 		}
	// 		index++;
	// 	});
	// }
	
	
	
	// $scope.TambahEDC_Tambah_Clicked = function(){
	// 	console.log("selected gl :",$scope.TipeEDC_GLList);

	// 	$scope.gridDetail_Data.push({
	// 			"GLAccountId" : $scope.TipeEDC_GLList.GLAccountId,
	// 			"GLAccountCode" : $scope.TipeEDC_GLList.GLAccountCode,
	// 			"GLAccountName" : $scope.TipeEDC_GLList.GLAccountName
	// 		});
	// 	console.log("gridDetail :",$scope.gridDetail_Data);
	// 	$scope.gridDetail.data = $scope.gridDetail_Data;
	// 	$scope.GridApiGridDetail.grid.refresh();
	// }
	
	// $scope.gridDetail = {
	// 	enableSorting: true,
	// 	enableRowSelection: true,
	// 	multiSelect: true,
	// 	enableSelectAll: true,
	// 	columnDefs: [
    //           { name:'id', field: 'GLAccountId', visible:false },
    //           { name:'Nomor GL Account',  field: 'GLAccountCode'},
    //           { name:'Nama GL Account', field: 'GLAccountName'},
	// 		  { name:'Action', 
	// 			cellTemplate:'<a ng-click="grid.appScope.TipeEDC_Delete_Clicked(row)">Hapus</a>'},
	// 	],
	// 	onRegisterApi: function(gridApi) {
	// 		$scope.GridApiGridDetail = gridApi;
	// 	}
	// }
});