angular.module('app')
	.factory('MasterEDCFactory', function ($http, CurrentUser) {
	return{
		getData: function(Branch){
			var url = '/api/fe/FinanceEDC/GetMasterData/start/1/limit/100/Branch/' + Branch;
			console.log("url EDC : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create: function(inputData) {
            var url = '/api/fe/FinanceEDC/Create/';
			var param = JSON.stringify(inputData);
			console.log("object input EDC ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update: function(inputData) {
            var url = '/api/fe/FinanceEDC/Update/';
			var param = JSON.stringify(inputData);
			console.log("object update EDC ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(EDCId){
			var url = '/api/fe/FinanceEDC/DeleteData/';
			console.log("url delete data : ", url);
			var res=$http.delete(url, {data:EDCId, headers:{'content-type' : 'application/json'}});
			//console.log("oke deh coy "+EDCId);
			//var res=$http.delete(url);
			return res;	
		},

		getDataBranch:function(ho) {
		var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData='+ho;
		var res=$http.get(url);  
		return res;			
		},			
		
		getDataBankAccountComboBox:function (OutletId) {
			var url = '/api/fe/FinanceEDC/GetDataComboBox/Branch/' + OutletId;
			console.log('Masuk ke getdata factory getDataBankAccountComboBox'); 
			console.log('url :' + url); 
			var res = $http.get(url);
			return res;
		}
		
	}
});