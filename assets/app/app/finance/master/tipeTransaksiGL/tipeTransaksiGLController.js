var app = angular.module('app');
app.controller('TipeTransaksiGLController', function ($scope, $http, $filter, CurrentUser, TipeTransaksiGLFactory, $timeout,bsNotify) {
	$scope.optionsTambahTranTypeGL_DC = [{ name: "Debet", value: { "text": "Debet", "DC": "D" } }, { name: "Kredit", value: { "text": "Credit", "DC": "C" } }];
	var limit = 10;
	$scope.getData = function () {
		TipeTransaksiGLFactory.getData(1, limit)
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$scope.grid.paginationCurrentPage = 1;
					$timeout(function(){
						$scope.grid.totalItems = res.data.Total;
						$scope.grid.paginationPageSize = limit;
						$scope.grid.paginationPageSizes = [10, 25, 50];
					},100);
					
					console.log("Cek Res", res)
					console.log("Cek Grid", $scope.grid)
					console.log("Cek data", res.data.Result)
					console.log("Cek total", $scope.grid.totalItems)
					console.log("Cek total2", res.data.Total)
				}
			);
	}

	// Get Data Pagingnation
	$scope.getData2 = function (start,  limit) {
		TipeTransaksiGLFactory.getData(start, limit)
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;
					$timeout(function(){
						$scope.grid.totalItems = res.data.Total;
						$scope.grid.paginationPageSize = limit;
						$scope.grid.paginationPageSizes = [10, 25, 50];
					},100);
					
					console.log("Cek Res", res)
					console.log("Cek Grid", $scope.grid)
					console.log("Cek data", res.data.Result)
					console.log("Cek total", $scope.grid.totalItems)
					console.log("Cek total2", res.data.Total)
				}
			);
	}
	//angular.element('#TipeTransaksiGLModal').modal('hide');
	$scope.gridDetail_Data = [];
	$scope.Show_MainTranTypeGL = true;
	$scope.Show_TambahTranTypeGL = false;
	$scope.getData();
	$scope.actRefresh = function(){
		$scope.MainTipeTransaksiGL_GetList();
	}
	$scope.LihatTranTypeGL_Edit = function (row) {
		$scope.SetDataDetail();
		$scope.TambahTranTypeGL_TipeTransaksiGL = row.entity.GLTranTypeName;
		$scope.TambahTranTypeGL_TranTypeId = row.entity.TranId;
		$scope.TambahTranTypeGL_Deskripsi = row.entity.Description;
		$scope.GetDataDetail($scope.TambahTranTypeGL_TranTypeId);
		$scope.Show_MainTranTypeGL = false;
		$scope.Show_TambahTranTypeGL = true;
		$scope.Show_TambahTranTypeGL_ButtonSimpan = false;
		$scope.Show_TambahTranTypeGL_UbahSimpan = true;
	}

	$scope.LihatTranTypeGL_Hapus = function (row) {
		$scope.HapusTranGL_GLTranName = row.entity.GLTranTypeName;
		$scope.HapusTranGL_GLTranId = row.entity.TranId;
		angular.element('#ModalHapusTranGL').modal('show');
	}

	$scope.Batal_ModalHapusTranGL = function () {
		$scope.HapusTranGL_GLTranName = "";
		$scope.HapusTranGL_GLTranId = "";
		angular.element('#ModalHapusTranGL').modal('hide');
	}

	$scope.Hapus_ModalHapusTranGL = function () {
		TipeTransaksiGLFactory.delete($scope.HapusTranGL_GLTranId)
			.then(
				function (res) {
					//alert("Data berhasil dihapus");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil dihapus.",
						type: "success"
					});
					$scope.getData();
					$scope.Batal_ModalHapusTranGL();
				}
			);
	}

	$scope.MainTranType_Tambah_Clicked = function () {
		$scope.Show_MainTranTypeGL = false;
		$scope.Show_TambahTranTypeGL = true;
		$scope.Show_TambahTranTypeGL_ButtonSimpan = true;
		$scope.Show_TambahTranTypeGL_UbahSimpan = false;
		$scope.SetDataDetail();
		$scope.gridDetail_Data = [];
		$scope.gridDetail.data = $scope.gridDetail_Data;
	}

	$scope.TambahTranTypeGL_ClearFields = function () {
		$scope.gridDetail_Data = [];
		$scope.TambahTranTypeGL_TipeGrupGL = [];
		$scope.TambahTranTypeGL_TipeTransaksiGL = "";
		$scope.TambahTranTypeGL_Deskripsi = "";
		$scope.TambahTranTypeGL_TranTypeId = "";
	}

	$scope.TambahTranTypeGL_Batal_Clicked = function () {
		$scope.Show_TambahTranTypeGL = false;
		$scope.Show_MainTranTypeGL = true;
		$scope.TambahTranTypeGL_ClearFields();
		$scope.getData();
	}

	$scope.TambahTranTypeGL_Simpan_Clicked = function () {
		var TranTypeGLDetailData = [];
		var statusError = "";
		var Err = "";

		if( ($scope.TambahTranTypeGL_TipeTransaksiGL == null ||  $scope.TambahTranTypeGL_TipeTransaksiGL == undefined ||  $scope.TambahTranTypeGL_TipeTransaksiGL == "") ||
			($scope.TambahTranTypeGL_Deskripsi == null || $scope.TambahTranTypeGL_Deskripsi == undefined || $scope.TambahTranTypeGL_Deskripsi == "" ) || 
			($scope.TambahTranTypeGL_DC == null || $scope.TambahTranTypeGL_DC == undefined || $scope.TambahTranTypeGL_DC == "") || 
			($scope.TambahTranTypeGL_TipeGrupGL == null || $scope.TambahTranTypeGL_TipeGrupGL == undefined || $scope.TambahTranTypeGL_TipeGrupGL == "" )){
				bsNotify.show({
					title: "Warning",
					content: "Form Harus di Isi Semua",
					type: "warning"
				});
				return;
			}
		
		console.log("Cek data Grid", $scope.gridDetail.data.length);
		if($scope.gridDetail.data.length == 0){
			bsNotify.show({
				title: "Warning",
				content: "Data Tidak Boleh Kosong.",
				type: "warning"
			});
			return;
		}
		// console.log("Cek dataTypename", $scope.grid.data[0].GLTranTypeName)
		angular.forEach($scope.gridDetail_Data, function (value, key) {
			TranTypeGLDetailData.push(
				{
					"GLGroupTypeId": value.GLGroupTypeId,
					"DC": value.DC,
				}
			);
		});

		var inputData = [
			{
				"GLTranTypeName": $scope.TambahTranTypeGL_TipeTransaksiGL,
				"Description": $scope.TambahTranTypeGL_Deskripsi,
				"GLTranTypeDetail": TranTypeGLDetailData,
			}
		];

		TipeTransaksiGLFactory.create(inputData)
			.then(
				function (res) {
					console.log("Cek data Gagal", res.data)
					statusError = res.data;
					if(statusError == true){
						bsNotify.show({
							title: "Berhasil",
							content: "Data berhasil disimpan.",
							type: "success"
						});
					}else{
						Err = statusError.split(" ");
						if(Err[4] == "sudah"){
							bsNotify.show({
								title: "Warning",
								content: $scope.TambahTranTypeGL_TipeTransaksiGL + " Data Sudah Terdaftar.",
								type: "warning"
							});
							return;
						}
					}
					console.log("Lihat Pesan error", Err[4]);
					
					$scope.Show_TambahTranTypeGL = false;
					$scope.Show_MainTranTypeGL = true;
					$scope.TambahTranTypeGL_ClearFields();
					$scope.getData();
				}
			);
	}

	$scope.TambahTranTypeGL_Ubah_Clicked = function () {
		var TranTypeGLDetailData = [];
		angular.forEach($scope.gridDetail_Data, function (value, key) {
			TranTypeGLDetailData.push(
				{
					"GLGroupTypeId": value.GLGroupTypeId,
					"DC": value.DC,
				}
			);
		});

		var inputData = [
			{
				"TranId": $scope.TambahTranTypeGL_TranTypeId,
				"GLTranTypeName": $scope.TambahTranTypeGL_TipeTransaksiGL,
				"Description": $scope.TambahTranTypeGL_Deskripsi,
				"GLTranTypeDetail": TranTypeGLDetailData,
			}
		];

		TipeTransaksiGLFactory.update(inputData)
			.then(
				function (res) {
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
					$scope.Show_TambahTranTypeGL = false;
					$scope.Show_MainTranTypeGL = true;
					$scope.TambahTranTypeGL_ClearFields();
					$scope.getData();
				}
			);
	}

	$scope.TipeTransaksiGLOK_Button_Clicked = function () {
		var deleteData = [];
		angular.forEach($scope.MainTipeTransaksiGLGridAPI.selection.getSelectedRows(), function (row, index) {
			deleteData.push(row.TranId);
		});
		TipeTransaksiGLFactory.delete(deleteData).
			then(
				function (res) {
					$scope.MainTipeTransaksiGL_GetList();
					$scope.MainTipeTransaksiGLGridAPI.selection.clearSelectedRows();
				}
			);
	}

	$scope.TipeTransaksiGLCancel_Button_Clicked = function () {
		angular.element('#TipeTransaksiGLModal').modal('hide');
	}

	$scope.actDel = function () {
		$scope.TipeTransaksiGL_TotalSelectedData = $scope.MainTipeTransaksiGLGridAPI.selection.getSelectedCount();
		angular.element('#TipeTransaksiGLModal').modal('show');
	}
	$scope.grid = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		useCustomPagination: true,
		useExternalPagination: true,
		paginationPageSize : null,
		paginationPageSizes: [10, 25, 50],

		columnDefs: [
			{ name: 'id', field: 'TranId', visible: false },
			{ name: 'Tipe Transaksi GL', field: 'GLTranTypeName' },
			{ name: 'Deskripsi', field: 'Description' },
			{
				name: "Action",
				              cellTemplate: '<a ng-click="grid.appScope.LihatTranTypeGL_Edit(row)"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.MainTipeTransaksiGLGridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("$scope.MainTipeTransaksiGLGridAPI.selection.getSelectedCount()", $scope.MainTipeTransaksiGLGridAPI.selection.getSelectedCount());
				console.log("row", row);
				$scope.selectedRows = $scope.MainTipeTransaksiGLGridAPI.selection.getSelectedRows();
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$timeout(function(){
					$scope.getData2(pageNumber, pageSize);
				},100)
				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		}
	};

	$scope.GetDataDetail = function (TranTypeId) {
		TipeTransaksiGLFactory.getDataDetail(TranTypeId)
			.then(
				function (res) {
					console.log("result", res.data.Result);
					$scope.gridDetail_Data = res.data.Result;
					$scope.gridDetail.data = $scope.gridDetail_Data;
				}
			);
	}



	$scope.SetDataDetail = function () {
		var GLListArray = [];

		TipeTransaksiGLFactory.getDataGLGroupTypeList()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						GLListArray.push({ "name": value.GLGroupTypeName, "value": { "GLGroupTypeId": value.GLGroupTypeId, "GLGroupTypeName": value.GLGroupTypeName } });
					});

					$scope.optionsTambahTranTypeGL_TipeGrupGL = GLListArray;
				}
			);
	}

	$scope.tipeTranGL_Delete_Clicked = function (row) {
		$scope.gridDetail_Data.splice(row.entity.indexOf, 1);
		$scope.gridDetail.data = $scope.gridDetail_Data;
	}



	$scope.TambahTranTypeGL_Tambah_Clicked = function () {
		console.log("selected gl :", $scope.TipeGrupGL_GLList);
		if( ($scope.TambahTranTypeGL_TipeTransaksiGL == null ||  $scope.TambahTranTypeGL_TipeTransaksiGL == undefined ||  $scope.TambahTranTypeGL_TipeTransaksiGL == "") ||
			($scope.TambahTranTypeGL_Deskripsi == null || $scope.TambahTranTypeGL_Deskripsi == undefined || $scope.TambahTranTypeGL_Deskripsi == "" ) || 
			($scope.TambahTranTypeGL_DC == null || $scope.TambahTranTypeGL_DC == undefined || $scope.TambahTranTypeGL_DC == "") || 
			($scope.TambahTranTypeGL_TipeGrupGL == null || $scope.TambahTranTypeGL_TipeGrupGL == undefined || $scope.TambahTranTypeGL_TipeGrupGL == "" )){
				bsNotify.show({
					title: "Warning",
					content: "Form Harus di Isi Semua",
					type: "warning"
				});
				return;
			}

		$scope.gridDetail_Data.push({
			"GLGroupTypeId": $scope.TambahTranTypeGL_TipeGrupGL.GLGroupTypeId,
			"DC": $scope.TambahTranTypeGL_DC.DC,
			"DCName": $scope.TambahTranTypeGL_DC.text,
			"GLGroupTypeName": $scope.TambahTranTypeGL_TipeGrupGL.GLGroupTypeName,
		});
		console.log("gridDetail :", $scope.gridDetail_Data);
		$scope.gridDetail.data = $scope.gridDetail_Data;
		$scope.GridApiGridDetail.grid.refresh();
	}

	$scope.gridDetail = {
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		paginationPageSizes: [10, 25, 50],
		columnDefs: [
			{ name: 'GLGroupTypeId', field: 'GLGroupTypeId', visible: false },
			{ name: 'DC', field: 'DC', visible: false },
			{ name: 'Debet/Kredit', field: 'DCName' },
			{ name: 'Tipe Grup GL', field: 'GLGroupTypeName' },

			{
				name: 'Action',
				cellTemplate: '<a ng-click="grid.appScope.tipeTranGL_Delete_Clicked(row)">Hapus</a>'
			},
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiGridDetail = gridApi;
		}
	}

	$scope.MainTipeTransaksiGL_GetList = function () {
		TipeTransaksiGLFactory.getData()
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;
				}
			);
	}
});