angular.module('app')
	.factory('TipeTransaksiGLFactory', function ($http, CurrentUser) {
	return{
		getData: function(start, limit){
			var url = '/api/fe/FinanceGLTranType/start/'+start+'/limit/'+limit;
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create: function(inputData) {
            var url = '/api/fe/FinanceGLTranType/Create/';
			var param = JSON.stringify(inputData);
			console.log("object input GL Group Type ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update: function(inputData) {
            var url = '/api/fe/FinanceGLTranType/Update/';
			var param = JSON.stringify(inputData);
			console.log("object input GL Group Type ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(TranId){
			var url = '/api/fe/FinanceGLTranType/DeleteData/' + TranId;
			//console.log("url delete data : ", url);
			//var res=$http.delete(url);
			var res=$http.delete(url, {data:TranId, headers:{'content-type' : 'application/json'}});
			return res;	
		},
		
		getDataDetail:function(TranTypeId){
			var url = '/api/fe/FinanceGLTranType/GetDetailData/start/1/limit/100/TranId/' + TranTypeId;
			console.log("url GroupTypeDetailId : ", url);
			var res=$http.get(url);
			return res;	
		},
		
		getDataGLGroupTypeList:function(){
			var url = '/api/fe/FinanceGLGroupType/GetAllData';
			console.log("url Get All GL List : ", url);
			var res=$http.get(url);
			return res;	
		},
	}
});