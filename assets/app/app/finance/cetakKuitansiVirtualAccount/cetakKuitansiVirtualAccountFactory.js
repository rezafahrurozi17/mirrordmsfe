angular.module('app')
  .factory('CetakKuitansiVirtualAccountFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		
		
        return res;
      },
      create: function(cetakKuitansiVirtualAccount) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: cetakKuitansiVirtualAccount.Name,
                                            Description: cetakKuitansiVirtualAccount.Description}]);
      },
      update: function(cetakKuitansiVirtualAccount){
        return $http.put('/api/fw/Role', [{
                                            Id: cetakKuitansiVirtualAccount.Id,
                                            //pid: negotiation.pid,
                                            Name: cetakKuitansiVirtualAccount.Name,
                                            Description: cetakKuitansiVirtualAccount.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });