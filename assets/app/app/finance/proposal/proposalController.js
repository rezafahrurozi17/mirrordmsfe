
var app = angular.module('app');
//20170425, eric, begin
//Nama controller disini harus case sensitive sama dengan route.js
app.controller('proposalController', function ($scope, $http, CurrentUser, $filter, proposalFactory, $timeout, uiGridConstants, bsNotify) {
	$scope.optionsLihatProposal_DaftarProposal_Search = [{ name: "Nomor Proposal Payment", value: "Nomor Proposal Payment" }, { name: "Nomor Rekening Pengirim", value: "Nomor Rekening Pengirim" }, { name: "Nama Bank Pengirim", value: "Nama Bank Pengirim" }, { name: "Nominal Total", value: "Nominal Total" }];
	$scope.optionsLihatProposal_DaftarBulkAction_Search = [{ name: "Setuju", value: "Setuju" }, { name: "Tolak", value: "Tolak" }];
	// $scope.optionsLihatProposal_DK_Search = [{ name: "Debet", value: "Debet" }];
	// //$scope.optionsLihatProposal_BiayaDK_Search = [{ name: "Debet", value: "Debet" }, { name: "Kredit", value: "Kredit" }];
	// $scope.optionsLihatProposal_TipeBiaya_Search = [{ name: "Bea Materai", value: "Bea Materai" }, { name: "Other Income", value: "Other Income" }, { name: "Other Expense", value: "Other Expense" }, { name: "PPN Masukan", value: "PPN Masukan" }, { name: "PPV", value: "PPV" }];
	// $scope.optionsLihatProposal_NoGL_Search = [{ name: "PPh 22 Terbayar WAPU", value: "PPh 22 Terbayar WAPU" }, { name: "PPh 23 Terbayar", value: "PPh 23 Terbayar" }, { name: "PPN Keluaran WAPU", value: "PPN Keluaran WAPU" }];
	$scope.optionsTambahProposal_DaftarInstruksi_Search = [{ name: "Nomor Instruksi Pembayaran / Advance", value: "Nomor Instruksi Pembayaran" }, { name: "Tanggal Instruksi Pembayaran / Advance", value: "Tanggal Instruksi Pembayaran" }, { name: "Tipe Instruksi Pembayaran", value: "Tipe Instruksi Pembayaran" }, { name: "Tipe Advance Payment", value: "Tipe Advance Payment" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }, { name: "Nama Pelanggan / Vendor", value: "Nama Pelanggan" }, { name: "Nominal Pembayaran", value: "Nominal Pembayaran" }];
	// $scope.optionsTambahProposal_DaftarAP_Search = [{ name: "Tgl Instruksi Pembayaran", value: "Tgl Instruksi Pembayaran" }, { name: "No Instruksi Pembayaran", value: "No Instruksi Pembayaran" }, { name: "No Branch/Vendor", value: "No Branch/Vendor" }, { name: "Nama Branch/Vendor", value: "Nama Branch/Vendor" }];
	$scope.EditMode = false;
	$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
	//$scope.optionsTambahProposal_NoGiroCheque_Search = [];
	$scope.submitProposalDataNew = [];
	$scope.TambahProposal_NoRekPengirim_Search = "";
	$scope.TambahProposal_NoGiroCheque = undefined;
	var user = CurrentUser.user();
	$scope.Cetak = "N";

	var paginationOptions_LihatProposal_DaftarProposal = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	//TambahProposal_GroupInstruksiUntukProposal_UIGrid
	var paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	//TambahProposal_DaftarInstruksiUntukProposal_UIGrid
	var paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	//TambahProposal_DaftarInstruksi_UIGrid
	var paginationOptions_TambahProposal_DaftarInstruksi_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}

	//LihatProposal_GroupInstruksiUntukProposal_UIGrid
	var paginationOptions_LihatProposal_GroupInstruksiUntukProposal_UIGrid = {
		paginationPageSize: 25,
		paginationPageSizes: [25, 50, 75],
		sort: null
	}

	//LihatProposal_DaftarInstruksiUntukProposal_UIGrid
	var paginationOptions_LihatProposal_DaftarInstruksiUntukProposal_UIGrid = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}


	console.log("Proposal");
	$scope.Show_ShowLihatProposal = true;
	$scope.Show_ShowTemp = false;
	$scope.uiGridPageSize = 10;
	$scope.currentOutletId = 0;
	$scope.currentProposalNo = "";
	$scope.currentProposalId = 0;
	$scope.currentTanggalProposal = "";
	$scope.currentNamaBranch_Search = "";
	$scope.currentNoRekPengirim_Search = "";
	$scope.currentNamaBankPengirim = "";
	$scope.currentNamaRekPengirim = "";
	$scope.currentNoGiroCheque = "";
	$scope.LihatProposal_TujuanPembayaran_TanggalProposal = new Date();
	$scope.LihatProposal_TujuanPembayaran_TanggalProposalTo = new Date();
	$scope.user = CurrentUser.user();
	$scope.ProposalId = 0;

	$scope.parent = $scope;
	$scope.TambahProposal_NoRekPengirim_Pilih = "Edit";

	var dateFormat = 'dd/MM/yyyy';
	$scope.dateOption = { format: dateFormat }

	//----------------------------------
	// Start-Up
	//----------------------------------

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		//$scope.getDataBranchCB();
		$scope.ProposalPayment_GetDataBranchByOutletId();
		//console.log($scope.LihatProposal_NamaBranch_Search);
		//$scope.LihatProposal_NamaBranch_Search="1";
		//console.log($scope.LihatProposal_NamaBranch_Search);
		//   $scope.gridData=[];

	});

	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}

	$scope.ProposalPayment_GetDataBranchByOutletId = function () {
		var ho;
		//RoleName == 'HO'
		if ($scope.right($scope.user.OrgCode, 6) == '000000') {
			$scope.LihatProposal_NamaBranch_isDisabled = true;
			$scope.TambahProposal_NamaBranch_isDisabled = true;
			$scope.BulkAction_Enabled = false;
			ho = $scope.user.OrgId;
		}
		else {
			$scope.LihatProposal_NamaBranch_isDisabled = false;
			$scope.TambahProposal_NamaBranch_isDisabled = false;
			$scope.BulkAction_Enabled = true;
			ho = 0;
		}

		proposalFactory.getBranchByOutletId(ho)
			.then(
				function (res) {
					$scope.optionsLihatProposal_NamaBranch = res.data.Result;
					$scope.optionsTambahProposal_NamaBranch = res.data.Result;
					$scope.LihatProposal_NamaBranch = $scope.user.OutletId;
					$scope.LihatData_NamaBranch = res.data.Result[0].BranchName;
					$scope.TambahProposal_NamaBranch_Search = $scope.user.OutletId;
					$scope.tmpBranch = $scope.TambahProposal_NamaBranch_Search;
					$scope.currentOutletId = $scope.user.OutletId;
				}
			);
	}

	$scope.getDataBranchCB = function () {
		proposalFactory.getDataBranchComboBox()
			.then
			(
				function (res) {
					//$scope.loading = false;
					$scope.optionsLihatProposal_NamaBranch_Search = res.data;
					$scope.optionsTambahProposal_NamaBranch_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.getDataBankAccount = function (CustomerId) {
		proposalFactory.getDataBankAccountComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsTambahProposal_NoRekPengirim_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.getDataCG = function (CustomerId) {
		proposalFactory.getDataCGComboBox($scope.ProposalId, $scope.TambahProposal_NamaBranch_Search)
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsTambahProposal_NoGiroCheque_Search = res.data.Result;

					// angular.forEach($scope.optionsTambahProposal_NoGiroCheque_Search, function(key, value) {
					// 	if (key.name == $scope.currentNoGiroCheque) {
					// 		$scope.currentNoGiroCheque = key.value;
					// 	}
					// });

					for (var i in $scope.optionsTambahProposal_NoGiroCheque_Search){
						if($scope.optionsTambahProposal_NoGiroCheque_Search[i].name == $scope.currentNoGiroCheque){
							$scope.currentNoGiroCheque = $scope.optionsTambahProposal_NoGiroCheque_Search[i].value;
						}
					}
			
					$scope.TambahProposal_NoGiroCheque = $scope.currentNoGiroCheque;

				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.Lihat_CariProposalData = function () {
		$scope.LihatProposal_TujuanPembayaran_TanggalProposal_Changed();
		if (!$scope.LihatTanggalProposalOK) {
			bsNotify.show({
				title: "Warning",
				content: $scope.errLihatTanggalProposal,
				type: "warning"
			});
			return false;
		}

		if ((!isNaN($scope.LihatProposal_TujuanPembayaran_TanggalProposal))
			&& (!isNaN($scope.LihatProposal_TujuanPembayaran_TanggalProposalTo))) {
			$scope.GetListProposal(1);
			$scope.ShowProposalData = true;
			Enable_LihatProposal();
		}
		else {
			bsNotify.show({
				title: "Warning",
				content: "Tgl Awal/Akhir Blm Diisi",
				type: "warning"
			});
			//alert("Tgl Awal/Akhir Blm Diisi");
		}
	};

	$scope.LihatProposal_NamaBranch_Search_SelectedChange = function () {
		console.log($scope.LihatProposal_NamaBranch_Search);
	}

	$scope.GetListProposal = function (Start) {
		console.log("LihatProposal_NamaBranch",$scope.LihatProposal_NamaBranch);
		if ($scope.LihatProposal_NamaBranch == undefined){
			$scope.LihatProposal_NamaBranch = user.OutletId;
		}

		proposalFactory.GetListProposal(Start, paginationOptions_LihatProposal_DaftarProposal.pageSize, $filter('date')(new Date($scope.LihatProposal_TujuanPembayaran_TanggalProposal), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.LihatProposal_TujuanPembayaran_TanggalProposalTo), 'yyyy-MM-dd'), $scope.LihatProposal_NamaBranch, $scope.LihatProposal_DaftarProposal_Search_Text, $scope.LihatProposal_DaftarProposal_Search)
			.then(
				function (res) {
					$scope.LihatProposal_DaftarProposal_UIGrid.data = res.data.Result;
					$scope.LihatProposal_DaftarProposal_UIGrid.totalItems = res.data.Total;
					$scope.LihatProposal_DaftarProposal_UIGrid.paginationPageSize = paginationOptions_LihatProposal_DaftarProposal.pageSize;
					$scope.LihatProposal_DaftarProposal_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.LihatProposal_DaftarProposal_UIGrid.paginationCurrentPage = paginationOptions_LihatProposal_DaftarProposal.pageNumber;
					$scope.loading = false;
				},
				function (err) {
					console.log("err=>", err);
				}
			)
	};

	$scope.LihatProposal_DaftarProposal_UIGrid = {
		paginationPageSizes: null,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		multiselect: false,
		enableFiltering: true,

		enableSorting: false,
		onRegisterApi: function (gridApi) {
			$scope.LihatProposal_DaftarProposal_gridAPI = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentProposalId = 0;
				}
				else {
					$scope.currentProposalId = row.entity.ProposalId;
				}
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				paginationOptions_LihatProposal_DaftarProposal.pageNumber = pageNumber;
				paginationOptions_LihatProposal_DaftarProposal.pageSize = pageSize;
				$scope.GetListProposal(pageNumber);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		enableColumnResizing: true,
		columnDefs:
			[
				{ name: 'Tanggal Proposal Payment', field: 'TanggalProposal', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
				{ name: 'ProposalId', field: 'ProposalId', visible: false },
				{ name: 'Nomor Proposal Payment', field: 'ProposalNo', visible: true, cellTemplate: '<a ng-click="grid.appScope.LihatDetailProposal(row)">{{row.entity.ProposalNo}}</a>' },
				{ name: 'Nomor Rekening Pengirim', field: 'NoRekPengirim' },
				{ name: 'IdRekPengirim', field: 'IdRekPengirim', visible: false },
				{ name: 'IdBranch', field: 'IdBranch', visible: false },
				{ name: 'BranchName', field: 'NamaBranch', visible: false },
				{ name: 'Nama Bank Pengirim', field: 'BankPengirim' },
				{ name: 'Nama Pengirim', field: 'NamaPengirim', visible: false },
				{ name: 'Nominal Total', field: 'Nominal', cellFilter: 'rupiahC' },
				{ name: 'GiroChequeName', field: 'GiroChequeName', visible: false },
				{ name: 'Status', field: 'StatusDraft' }
			]
	};

	$scope.LihatDetailProposal = function (row) {
		console.log('instrucktiontype = ', $scope.LihatProporsal_InstructionType, '-----------', row);
		$scope.ShowProposalDataLihat = true;
		$scope.LihatProposal_NamaBankPengirim_EnableDisable = false;
		$scope.LihatProposal_NamaRekPengirim_EnableDisable = false;
		$scope.Show_InstructionTypeDisable = false;

		$scope.ProposalId = row.entity.ProposalId;
		console.log("cek id proposal", $scope.ProposalId);
		$scope.Show_ShowLihatProposal = false;
		$scope.Show_ShowTemp = false;
		$scope.TambahProposal_MetodePembayaran = "Bank Transfer";
		$scope.LihatProposal_Top_NomorProposal = row.entity.ProposalNo;
		$scope.currentProposalNo = row.entity.ProposalNo;
		$scope.currentProposalId = row.entity.ProposalId;
		$scope.currentNamaBranch_Search = row.entity.IdBranch;
		$scope.currentNoRekPengirim_Search = row.entity.IdRekPengirim;
		//$scope.TambahProposal_NoRekPengirim_Search = row.entity.NoRekPengirim;
		$scope.currentNamaBankPengirim = row.entity.BankPengirim;
		$scope.currentNamaRekPengirim = row.entity.NamaPengirim;
		$scope.currentNoGiroCheque = row.entity.GiroChequeName;
		$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.data = [];
		$scope.LihatProposal_TanggalProposal = row.entity.TanggalProposal;
		$scope.LihatProposal_NoRekPengirim = row.entity.NoRekPengirim;
		$scope.LihatProposal_NoGiroCheque = row.entity.GiroChequeName;
		$scope.LihatProposal_NamaRekPengirim = row.entity.NamaPengirim;
		$scope.LihatProposal_NamaBankPengirim = row.entity.BankPengirim;
		$scope.LihatDataProposalNamaBranch = row.entity.NamaBranch;
		$scope.tmpStatusDraft = row.entity.StatusDraft;
		console.log(row.entity.StatusDraft);
		if (row.entity.StatusDraft == "Draft") {
			$scope.LihatProposal_EditDraft_EnableDisable = true;
			$scope.SimpanCetak_EnabledDisabled = true;
			$scope.Simpan_EnabledDisabled = true;
		}
		else if (row.entity.StatusDraft == "Lengkap") {
			$scope.LihatProposal_EditDraft_EnableDisable = false;
			$scope.LihatProposal_Batal_EnableDisable = true;
		}
		else {
			$scope.LihatProposal_EditDraft_EnableDisable = false;
			$scope.LihatProposal_Batal_EnableDisable = false;
		}

		console.log(row.entity.ProposalNo);

		proposalFactory.GetLihatRekapInstructionByProposalId(1, $scope.uiGridPageSize, row.entity.ProposalNo)
			.then
			(
				function (res) {
					for(var i in res.data.Result){
						res.data.Result[i].ProposalId = row.entity.ProposalId;
					}
					$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
					$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
					$scope.LihatProposal_NoRekTujuan = res.data.Result[0].RecvAccountName;
					$scope.LihatProposal_NamaPenerimaRefund = res.data.Result[0].VendorName;
					$scope.LihatProposal_NamaBankRefund = res.data.Result[0].RecvBankName;
					console.log("list instruction by proposal id :", res.data.Result);
					//$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_LihatProposal_GroupInstruksiUntukProposal_UIGrid.pageSizeageSize;
					//$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
					//$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_LihatProposal_GroupInstruksiUntukProposal_UIGrid.pageNumber;

				}
			);
	};

	$scope.TambahProposal_DaftarInstruksi_UIGrid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		// useCustomPagination: true,
		// useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarInstruksi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.BillingId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahProposal_DaftarSpkTarget_UIGrid.data = $scope.TambahProposal_UnitFullPaymentSPK_Paging(pageNumber);
				console.log(pageNumber);
				console.log(pageSize);
				scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSize = pageSize;
				$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSizes = [10, 25, 50];
				$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationCurrentPage = pageNumber;

				// proposalFactory.GetListInstructionForProposal(pageNumber, pageSize, $filter('date')(new Date('2018'), 'yyyy-MM-dd'),
				// 	$scope.TambahProposal_NamaBranch_Search)
				// 	.then
				// 	(
				// 	function (res) {

				// 		$scope.TambahProposal_DaftarInstruksi_UIGrid.data = res.data.Result;
				// 		$scope.TambahProposal_DaftarInstruksi_UIGrid.totalItems = res.data.Total;
				// 		$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSize = pageSize;
				// 		$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSizes = [10, 25, 50];
				// 		$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationCurrentPage = pageNumber;
				// 	}
				// 	);
			})
		},
		enableRowSelection: true,
		enableSelectAll: false,
		enableColumnResizing: true,
		columnDefs:
			[
				{ name: "PaymentId", field: "PaymentId", visible: false },
				{ name: 'No Instruksi Pembayaran / Advanced Payment', field: 'PaymentNo' },
				{ name: 'Tanggal Instruksi Pembayaran / Advanced Payment', field: 'TanggalPayment', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
				{ name: 'Tipe Instruksi Pembayaran', field: 'InstructionType', visible: true },
				{ name: 'Tipe Advanced Payment', field: 'AdvancedType', visible: true },
				{ name: 'Nomor Rangka', field: 'NoRangka', visible: true },
				{ name: 'Tanggal Jatuh Tempo', field: 'MaturityDate', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
				{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
				{ name: 'Nama Vendor', field: 'VendorNameOrDF' },
				{ name: 'Nama Vendor1', field: 'VendorName' , visible: false},
				{ name: 'Nama Bank', field: 'RecvBankName' },
				{ name: 'No Rekening', field: 'RecvAccountName' },
				{ name: 'PaymentType', field: 'PaymentType', visible: false },
				{ name: 'POId', field: 'POId', visible: false }
			]
	};
	//disini
	$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid = {
		paginationPageSizes: null,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableRowSelection: true,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "PaymentId", field: "PaymentId", visible: false },
			{ name: 'Nomor Instruksi Pembayaran / Advanced Payment', field: 'PaymentNo' },
			{ name: 'Tanggal Instruksi Pembayaran / Advanced Payment', field: 'TanggalPayment', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
			{ name: 'Tanggal Jatuh Tempo', field: 'MaturityDate', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
			{ name: 'Nama Pelanggan / Vendor', field: 'VendorName', visible: false },
			{ name: 'Nama Bank', field: 'RecvBankName', visible: false },
			{ name: 'No Rekening', field: 'RecvAccountName', visible: false },
			{ name: 'PaymentType', field: 'PaymentType', visible: false },
			{ name: 'Tipe Instruksi Pembayaran', field: 'InstructionType' },
			{ name: 'Tipe Advanced Payment', field: 'AdvancedType' },
			{ name: 'Nomor Rangka', field: 'NoRangka' },
			{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
			{ name: 'ProposalDate', field: 'ProposalDate', visible: false },
			{ name: 'ProposalId', field: 'ProposalId', visible: false },
			{ name: 'ProposalNo', field: 'ProposalNo', visible: false },
			{ name: 'SenderBankName', field: 'SenderBankName', visible: false },
			{ name: 'SenderAccountNo', field: 'SenderAccountNo', visible: false },
			{ name: 'SenderAccountName', field: 'SenderAccountName', visible: false },
			{ name: 'SenderGiroCheque', field: 'SenderGiroCheque', visible: false },
			{ name: 'SenderBranch', field: 'SenderBranch', visible: false },
			{ name: 'POId', field: 'POId', visible: false },
			{ name: 'Action', cellTemplate: '<a ng-click="grid.appScope.deleteDaftarInstruksiUntukProposal(row)">Hapus</a>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiTambahProposal_DaftarInstruksiUntukProposal = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				//$scope.TambahProposal_DaftarSpkTarget_UIGrid.data = $scope.TambahProposal_UnitFullPaymentSPK_Paging(pageNumber);
				console.log(pageNumber);
				console.log(pageSize);
				// proposalFactory.GetInstructionByProposalId(pageNumber, pageSize, $scope.currentProposalNo)
				// .then
				// (
				// 	function(res)
				// 	{
				// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
				// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
				// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];			
				// 	}
				// );				
			}
			);
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
			}
			)
		}
	};

	$scope.TambahProposal_DaftarInstruksiUntukProposalTemp_UIGrid = {
		paginationPageSizes: null,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableRowSelection: true,
		columnDefs: [
			{ name: "PaymentId", field: "PaymentId", visible: false },
			{ name: 'Nomor Instruksi Pembayaran / Advanced Payment', field: 'PaymentNo' },
			{ name: 'Tanggal Instruksi Pembayaran / Advanced Payment', field: 'TanggalPayment', cellFilter: 'nullDateFilter' },
			{ name: 'Nama Pelanggan / Vendor', field: 'VendorName', visible: false },
			{ name: 'Nama Bank', field: 'RecvBankName', visible: false },
			{ name: 'No Rekening', field: 'RecvAccountName', visible: false },
			{ name: 'PaymentType', field: 'PaymentType', visible: false },
			{ name: 'Tipe Instruksi Pembayaran', field: 'InstructionType' },
			{ name: 'Tipe Advanced Payment', field: 'AdvancedType' },
			{ name: 'Nomor Rangka', field: 'NoRangka' },
			{ name: 'Tanggal Jatuh Tempo', field: 'MaturityDate', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
			{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
			{ name: 'ProposalDate', field: 'ProposalDate', visible: false },
			{ name: 'ProposalId', field: 'ProposalId', visible: false },
			{ name: 'ProposalNo', field: 'ProposalNo', visible: false },
			{ name: 'SenderBankName', field: 'SenderBankName', visible: false },
			{ name: 'SenderAccountNo', field: 'SenderAccountNo', visible: false },
			{ name: 'SenderAccountName', field: 'SenderAccountName', visible: false },
			{ name: 'SenderGiroCheque', field: 'SenderGiroCheque', visible: false },
			{ name: 'SenderBranch', field: 'SenderBranch', visible: false },
			{ name: 'POId', field: 'POId', visible: false }

		],
	};

	$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid = {
		paginationPageSizes: null,
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		//enableRowSelection: true,
		// enableFullRowSelection: true,
		columnDefs: [
			{ name: 'ProposalNo', field: 'ProposalNo', visible: false },
			{ name: 'Nama Vendor', field: 'VendorNameOrDF' },
			{ name: 'Nama Vendor1', field: 'VendorName', visible: false },
			{ name: 'Nama Bank', field: 'RecvBankName' },
			{ name: 'Nomor Rekening', field: 'RecvAccountName' },
			{ name: 'Total Nominal Pembayaran', field: 'TotalNominal', cellFilter: 'rupiahC' },
			{ name: 'Action', cellTemplate: '<a ng-click="grid.appScope.deleteGroupInstruksiUntukProposal(row)">Hapus</a>' }
		],
		onRegisterApi: function (gridApi) {
			$scope.gridApi_GroupInstruksi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentVendor = "";
				}
				else {
					$scope.currentVendor = row.entity.VendorName;
					if(row.entity.RecvAccountName != null){
						$scope.NoRekBank = row.entity.RecvAccountName;
					}else{
						$scope.NoRekBank = "";
					}
				}
				console.log("currentVendor -> " + $scope.currentVendor);
				console.log("Cek Norek Bank -> " + $scope.NoRekBank);
				if ($scope.EditMode == false) {
					proposalFactory.GetRekapDetailInstructionByProposalId(1, paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize, $scope.currentVendor +'|'+ $scope.NoRekBank)
						.then
						(
							function (res) {
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
								$scope.TambahProposal_NoRekTujuan = res.data.Result[0].RecvAccountName;
								$scope.LihatProposal_NamaPenerimaRefund = res.data.Result[0].VendorName;
								$scope.LihatProposal_NamaBankRefund = res.data.Result[0].RecvBankName;
								$scope.TambahProporsal_InstructionType = $scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data[0].InstructionType;
								console.log('instrucktiontype = ', $scope.TambahProporsal_InstructionType);
								if ($scope.TambahProporsal_InstructionType == "Refund") {
									if ($scope.right($scope.user.OrgCode, 6) == '000000') {
										console.log('masuk');
										$scope.Show_InstructionTypeDisable = false;
										$scope.Show_InstructionTambahNamaRefund = false;
									}
									else {
										$scope.Show_InstructionTypeDisable = true;
									}
								}
								else {
									console.log('bukan refund');
									$scope.Show_InstructionTambahNamaRefund = true;
								}
							}
						);
				}
				else {
					// proposalFactory.GetInstructionByProposalId(1, $scope.uiGridPageSize, row.entity.ProposalNo, row.entity.VendorName, $scope.NoRekBank)
					// 	.then
					// 	(
					// 		function (res) {
					// 			console.log("list instruction by proposal id :", res.data);
					// 			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
					// 			$scope.TambahProposal_NoRekTujuan = res.data.Result[0].AccountTo;
					// 			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
					// 			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
					// 			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
					// 			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
					// 		}
					// 	);
					proposalFactory.GetRekapDetailInstructionByProposalId(1, paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize, $scope.currentVendor +'|'+ $scope.NoRekBank)
						.then
						(
							function (res) {
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
								$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
								$scope.TambahProposal_NoRekTujuan = res.data.Result[0].RecvAccountName;
								$scope.LihatProposal_NamaPenerimaRefund = res.data.Result[0].VendorName;
								$scope.LihatProposal_NamaBankRefund = res.data.Result[0].RecvBankName;
								$scope.TambahProporsal_InstructionType = $scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data[0].InstructionType;
								console.log('instrucktiontype = ', $scope.TambahProporsal_InstructionType);
								if ($scope.TambahProporsal_InstructionType == "Refund") {
									if ($scope.right($scope.user.OrgCode, 6) == '000000') {
										console.log('masuk');
										$scope.Show_InstructionTypeDisable = false;
										$scope.Show_InstructionTambahNamaRefund = false;
									}
									else {
										$scope.Show_InstructionTypeDisable = true;
									}
								}
								else {
									console.log('bukan refund');
									$scope.Show_InstructionTambahNamaRefund = true;
								}
							}
						);
				}


			});
		}
	};

	$scope.refreshRekap = function () {

		var data;
		$scope.ProposalSubmitTempData = [];

		if($scope.EditMode != true){
			$scope.TambahProposal_ProposalListTempData('INSERT');
		}else{
			$scope.TambahProposal_ProposalListTempData('UBAH');
		}
		$scope.ProposalSubmitTempData = $scope.CreateProposalSubmitTempData;

		data =
			[{
				OutletId: $scope.currentOutletId,
				ProposalTempXML: "",
				ProposalTempList: $scope.ProposalSubmitTempData
			}]

		proposalFactory.submitTempProposal(data)
			.then
			(
				function (res) {
					console.log(res);

					proposalFactory.GetRekapInstructionByProposalId(1, paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize, $scope.currentOutletId)
						.then
						(
							function (res) {
								if ($scope.EditMode) {
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.filter(
										function (word) {
											return angular.isDefined(word.ProposalNo);
										});
									angular.forEach(res.data.Result, function (value, key) {
										this.push(value);
									}, $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data);
									//	$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.push(res.data.Result);
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems + res.data.Total;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageNumber;
									$scope.gridApi_GroupInstruksi.grid.refresh();
								}
								else {
									$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
								}

								$scope.AddTambahProposal_DaftarInstruksiUntukProposal_data();
							}

						);

				},
				function (err) {
					if (err != null) {
						console.log("err=>", err);
					}
				}
			);



	}
	// klik edit draft
	$scope.RekapEditDraft = function () {

		var data;
		// $scope.ProposalSubmitTempDataDraft = [];

		$scope.TambahProposal_ProposalListTempDataDraft('EDIT');
		
		$scope.ProposalSubmitTempDataDraft = $scope.CreateProposalSubmitTempDataDraft;

		data =
			[{
				OutletId: $scope.currentOutletId,
				ProposalTempXML: "",
				ProposalTempList: $scope.ProposalSubmitTempDataDraft
			}]

		proposalFactory.submitTempProposal(data)
			.then
			(
				function (res) {
					console.log(res);
					console.log('jalan ini 3')
					proposalFactory.GetRekapInstructionByProposalId(1, paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize, $scope.currentOutletId)
						.then
						(
							function (res) {
								if ($scope.EditMode) {
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.filter(
										function (word) {
											return angular.isDefined(word.ProposalNo);
										});
									// angular.forEach(res.data.Result, function (value, key) {
									// 	this.push(value);
									// }, $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data);
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
									// //	$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.push(res.data.Result);
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems + res.data.Total;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageNumber;
									$scope.gridApi_GroupInstruksi.grid.refresh();
								}
								else {
									$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
									$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
								}

								$scope.AddTambahProposal_DaftarInstruksiUntukProposal_data();
							}

						);

				},
				function (err) {
					if (err != null) {
						console.log("err=>", err);
					}
				}
			);



	}
	$scope.AddTambahProposal_DaftarInstruksiUntukProposal_data = function () {
		console.log('zzzz =>', $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data)
		angular.forEach($scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data, function (value, key) {
			//CR4 p2 Finance BungaDF Start
			var FilterData = value.VendorName +'|'+value.RecvAccountName;
		
			if (angular.isUndefined(value.ProposalNo)) {
				proposalFactory.GetRekapDetailInstructionByProposalId(1, 100, FilterData)
					.then
					(
						function (res) {
							angular.forEach(res.data.Result, function (value, key) {
								this.push(value);
							}, $scope.TambahProposal_DaftarInstruksiUntukProposal_data);
							//$scope.TambahProposal_DaftarInstruksiUntukProposal_data.push(res.data.Result);
						}
					);
			}
			else {
				proposalFactory.GetInstructionByProposalId(1, 100, value.ProposalNo, value.VendorName)
					.then
					(
						function (res) {
							angular.forEach(res.data.Result, function (value, key) {
								this.push(value);
							}, $scope.TambahProposal_DaftarInstruksiUntukProposal_data);
							//$scope.TambahProposal_DaftarInstruksiUntukProposal_data.push(res.data.Result);
						}
					);
			}
		});
	}

	$scope.deleteGroupInstruksiUntukProposal = function (row) {
		var index = $scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.indexOf(row.entity);
		$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data.splice(index, 1);

		//delete daftar instruksi by row.entity.VendorName
		var vendorName = row.entity.VendorName;
		var arrDetail = $scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data;
		var arrTemp = [];


		angular.forEach(arrDetail, function (value, key) {
			if (value.VendorName != vendorName) {
				arrTemp.push(value);
			}
		});

		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = arrTemp;
		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = 0;

		proposalFactory.deleteTempProposal(vendorName)
			.then(
				function (res) {
					console.log("Vendor Name deleted : " + vendorName);
				}
			);

		// arrDetail.forEach(function(result, index){
		// 	if(result[index] === )
		// });

		// $scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.length = 0;
		// $scope.refreshRekap();
	};

	$scope.deleteDaftarInstruksiUntukProposal = function (row) {
		var index = $scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data.indexOf(row.entity);
		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data.splice(index, 1);
		var data;
		$scope.ProposalSubmitTempData = [];

		$scope.TambahProposal_DeleteProposalListTempData('DELETE', row.entity.PaymentId, row.entity.PaymentType, row.entity.TanggalPayment, row.entity.MaturityDate, row.entity.POId);
		$scope.ProposalSubmitTempData = $scope.CreateProposalSubmitTempData;

		data =
			[{
				OutletId: $scope.currentOutletId,
				ProposalTempXML: "",
				ProposalTempList: $scope.ProposalSubmitTempData
			}]

		proposalFactory.submitTempProposal(data)
			.then
			(
				function (res) {
					console.log(res);

					proposalFactory.GetRekapInstructionByProposalId(1, paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize, $scope.currentOutletId)
						.then
						(
							function (res) {
								$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
								$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
								$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
								$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
								$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageNumber;
							}
						);

				},
				function (err) {
					if (err != null) {
						console.log("err=>", err);
					}
				}
			)


		// //$scope.refreshRekap();
	};

	$scope.TambahDaftarInstruksi_AddItem = function () {
		console.log("TambahDaftarInstruksi_AddItem");
		$scope.TambahProposal_DaftarInstruksiUntukProposalTemp_UIGrid.data = [];
		$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
		$scope.gridApi_DaftarInstruksi.selection.getSelectedRows().forEach(function (row) {
			var found = 0;

			$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.PaymentId);
				if (row.PaymentId == obj.PaymentId)
					found = 1;
			});
			if (found == 0) {
				$scope.TambahProposal_DaftarInstruksiUntukProposalTemp_UIGrid.data.push
					(
						{
							PaymentId: row.PaymentId,
							PaymentNo: row.PaymentNo,
							TanggalPayment: new Date(row.TanggalPayment),
							MaturityDate: row.MaturityDate,
							VendorName: row.VendorName,
							RecvBankName: row.RecvBankName,
							RecvAccountName: row.RecvAccountName,
							PaymentType: row.PaymentType,
							Nominal: row.Nominal,
							InstructionType: row.InstructionType,
							AdvancedType: row.AdvancedType,
							NoRangka: row.NoRangka,
							POId: row.POId,
							AccountTo: row.AccountTo,
							VendorNameOrDF: row.VendorNameOrDF
						}
					)
			}
			if (row.InstructionType == "Unit") {
				$scope.TambahProposal_NoRekTujuan_HOUnit_Hide = true;
			}
			else {
				$scope.TambahProposal_NoRekTujuan_HOUnit_Hide = false;
			}
		}
		)
		$scope.refreshRekap();


	};

	//UI Grid Lihat
	$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: true,
		onRegisterApi: function (gridApi) {
			$scope.gridApi_DaftarInstruksi_Lihat = gridApi;//................
			$scope.gridApi.refresh();
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				//$scope.currentSPKIdSource = row.entity.SPKId;
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
				var msg = 'rows changed ' + rows.length;
				console.log(msg);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		enableColumnResizing: true,
		columnDefs:
			[
				// { name: "PaymentId",field:"PaymentId",visible:false},
				// { name: 'No Instruksi Pembayaran / Advanced Payment', field: 'PaymentNo' },
				// { name: 'Tanggal Instruksi Pembayaran / Advanced Payment', field: 'TanggalPayment', cellFilter: 'date:\"dd/MM/yyyy\"' },
				// { name: 'Tanggal Jatuh Tempo', field: 'MaturityDate', cellFilter: 'date:\"dd/MM/yyyy\"' },
				// { name: 'Nama Pelanggan / Vendor', field: 'VendorName' },
				// { name: 'Nama Bank', field: 'RecvBankName' },
				// { name: 'No Rekening', field: 'RecvAccountName' },
				// { name: 'PaymentType', field: 'PaymentType', visible:false },
				// { name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'currency:"":0' },
				{ name: "PaymentId", field: "PaymentId", visible: false },
				{ name: 'Nomor Instruksi Pembayaran / Advanced Payment', field: 'PaymentNo' },
				{ name: 'Tanggal Instruksi Pembayaran / Advanced Payment', field: 'TanggalPayment', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
				{ name: 'Nama Pelanggan / Vendor', field: 'VendorName', visible: false },
				{ name: 'Nama Bank', field: 'RecvBankName', visible: false },
				{ name: 'No Rekening', field: 'RecvAccountName', visible: false },
				{ name: 'PaymentType', field: 'PaymentType', visible: false },
				{ name: 'Tipe Instruksi Pembayaran', field: 'InstructionType' },
				{ name: 'Tipe Advanced Payment', field: 'AdvancedType' },
				{ name: 'Nomor Rangka', field: 'NoRangka' },
				{ name: 'Tanggal Jatuh Tempo', field: 'MaturityDate', type: "date", cellFilter: 'date:"dd/MM/yyyy"' },
				{ name: 'Nominal Pembayaran', field: 'Nominal', cellFilter: 'rupiahC' },
				{ name: 'ProposalDate', field: 'ProposalDate', visible: false },
				{ name: 'ProposalId', field: 'ProposalId', visible: false },
				{ name: 'ProposalNo', field: 'ProposalNo', visible: false },
				{ name: 'SenderBankName', field: 'SenderBankName', visible: false },
				{ name: 'SenderAccountNo', field: 'SenderAccountNo', visible: false },
				{ name: 'SenderAccountName', field: 'SenderAccountName', visible: false },
				{ name: 'SenderGiroCheque', field: 'SenderGiroCheque', visible: false },
				{ name: 'SenderBranch', field: 'SenderBranch', visible: false }
			]
	};

	$scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid = {
		paginationPageSizes: [10, 25, 50],
		useCustomPagination: true,
		useExternalPagination: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableRowSelection: true,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: 'ProposalNo', field: 'ProposalNo', visible: false },
			{ name: 'Nama Vendor', field: 'VendorNameOrDF' },
			{ name: 'Nama Vendor1', field: 'VendorName', visible: false },
			{ name: 'Nama Bank', field: 'RecvBankName' },
			{ name: 'Nomor Rekening', field: 'RecvAccountName' },
			{ name: 'Total Nominal Pembayaran', field: 'TotalNominal', cellFilter: 'rupiahC' },
			//{ name: 'Action', cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteDaftarInstruksiUntukProposal(row)">Hapus</button>'}
		],
		onRegisterApi: function (gridApi) {
			$scope.gridApi_LihatGroupInstruksi = gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				if (row.isSelected == false) {
					$scope.currentVendor = "";
				}
				else {
					// $scope.currentVendor = row.entity.VendorName;
					if(row.entity.RecvAccountName != null){
						$scope.NoRekBank = row.entity.RecvAccountName;
					}else{
						$scope.NoRekBank = "";
					}
				}
				console.log($scope.currentVendor);
				proposalFactory.GetInstructionByProposalId(1, $scope.uiGridPageSize, row.entity.ProposalNo, row.entity.VendorName ,$scope.NoRekBank)
					.then
					(
						function (res) {
							console.log("list instruction by proposal id :", res.data);
							$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
							$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
							$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_LihatProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
							$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
							$scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_LihatProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
							$scope.LihatProporsal_InstructionType = $scope.LihatProposal_DaftarInstruksiUntukProposal_UIGrid.data[0].InstructionType;
							console.log('instrucktiontype = ', $scope.LihatProporsal_InstructionType);
							if ($scope.LihatProporsal_InstructionType == "Refund") {
								if ($scope.right($scope.user.OrgCode, 6) == '000000') {
									console.log('masuk');
									$scope.Show_InstructionTypeDisable = true;
									$scope.Show_InstructionTambahNamaRefund = false;
								}
								else {
									$scope.Show_InstructionTypeDisable = true;
								}
							}
							else {
								console.log('bukan refund');
								$scope.Show_InstructionTambahNamaRefund = true;
							}
						}
					);

			});
		}
	};


	$scope.TambahProposal_NoRekPengirim_Search_SelectedChange = function () {
		console.log("Nomor Rekening Pengirim:", $scope.TambahProposal_NoRekPengirim_Search);
		if ($scope.TambahProposal_NoRekPengirim_Search !== "" && $scope.TambahProposal_NoRekPengirim_Search !== undefined) {
			proposalFactory.getDataAccountBank($scope.TambahProposal_NoRekPengirim_Search)
				.then(
					function (res) {
						console.log("No")
						$scope.TambahProposal_NoRekPengirim = res.data.Result[0].AccountBankNo;
						$scope.TambahProposal_NamaBankPengirim = res.data.Result[0].AccountBankName;
						$scope.TambahProposal_NamaRekPengirim = res.data.Result[0].AccountName;
						$scope.TambahProposal_NamaBankPengirim_EnabledDisabled = false;
						$scope.TambahProposal_NamaRekPengirim_EnabledDisabled = false;
						console.log('nama bank', $scope.TambahProposal_NamaBankPengirim);
					}
				);
		}
		else {
			$scope.TambahProposal_NoRekPengirim = "";
			$scope.TambahProposal_NamaBankPengirim = "";
			$scope.TambahProposal_NamaRekPengirim = "";
		}
	}

	$scope.TambahProposal_CG_Search_SelectedChange = function () {
		console.log("Cheque/Giro Status:", $scope.TambahProposal_NoGiroCheque);
		if ($scope.TambahProposal_NoGiroCheque !== 0 && $scope.TambahProposal_NoGiroCheque !== undefined &&  $scope.TambahProposal_NoGiroCheque !== "") {
			$scope.TambahProposal_NoRekPengirim_Pilih = "Lihat"
			proposalFactory.getDataCGAccountBank($scope.TambahProposal_NoGiroCheque)
				.then(
					function (res) {
						console.log("No")
						$scope.TambahProposal_NamaBankPengirim = res.data.Result[0].AccountBankName;
						$scope.TambahProposal_NamaRekPengirim = res.data.Result[0].AccountName;
						$scope.TambahProposal_NoRekPengirim = res.data.Result[0].AccountBankNo;
						$scope.TambahProposal_NoRekPengirim_Search = res.data.Result[0].AccountBankId;
						$scope.TambahProposal_NoChequeGiro = $scope.optionsTambahProposal_NoGiroCheque_Search.find(function (cg) { return cg.value == $scope.TambahProposal_NoGiroCheque }).name;
						$scope.TambahProposal_NamaBankPengirim_EnabledDisabled = false;
						$scope.TambahProposal_NamaRekPengirim_EnabledDisabled = false;
					}
				);
		}
		else {
			$scope.TambahProposal_NoRekPengirim_Pilih = "Edit"
			$scope.TambahProposal_NamaBankPengirim = "";
			$scope.TambahProposal_NamaRekPengirim = "";
			$scope.TambahProposal_NoRekPengirim = "";
			$scope.TambahProposal_NoRekPengirim_Search = undefined;
		}
	}
	
	$scope.ClearData = function(){
		//Grid
		$scope.TambahProposal_DaftarInstruksi_UIGrid.data = [];
		$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = [];
		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = [];
		//Form
		$scope.TambahProposal_DaftarInstruksi_Search_Text = "";
		$scope.TambahProposal_DaftarInstruksi_SearchDropdown = "";
		$scope.TambahProposal_NoGiroCheque = "";
		$scope.TambahProposal_NamaBranch_Search = $scope.tmpBranch;
		console.log("Cek branch",$scope.TambahProposal_NamaBranch_Search)
	}

	$scope.ToTambahProposal = function () {
		$scope.ClearData();
		$scope.EditMode = false;
		$scope.Show_ShowLihatProposal = false;
		$scope.Show_ShowTambahProposal = true;
		// if ($scope.right($scope.user.OrgCode, 6) == '000000') {
		// 	$scope.Show_InstructionTypeDisable = false;
		// 	$scope.TambahProposal_NamaRekPengirim_EnableDisable = false;
		// 	$scope.TambahProposal_NamaBankPengirim_EnableDisable = false;
		// 	$scope.TambahProposal_NoRekPengirim_EnabledDisabled = false;
		// }
		// else {
		// 	$scope.Show_InstructionTypeDisable = true;
		// }
		$scope.Show_ShowTemp = false;
		$scope.SimpanCetak_EnabledDisabled = true;
		$scope.Simpan_EnabledDisabled = true;
		$scope.Show_TambahProposal_FootBalance = true;
		Enable_TambahProposal_DaftarInstruksi();
		$scope.TambahProposal_TanggalProposal = new Date();
		$scope.TambahProposal_MetodePembayaran = "Bank Transfer";
		$scope.getDataCG();
		$scope.ClearGridData();
		
		$scope.getDataBankAccount();
		// var data;
		// $scope.ProposalSubmitTempData = [];

		// $scope.TambahProposal_DeleteProposalListTempData('DELETE', 0, 'X', $filter('date')(new Date(), 'yyyy-MM-dd'), $filter('date')(new Date(), 'yyyy-MM-dd'));
		// $scope.ProposalSubmitTempData = $scope.CreateProposalSubmitTempData;

		// data =
		// 	[{
		// 		OutletId: $scope.currentOutletId,
		// 		ProposalTempXML: "",
		// 		ProposalTempList: $scope.ProposalSubmitTempData
		// 	}]

		// proposalFactory.submitTempProposal(data)
		// 	.then
		// 	(
		// 	function (res) {
		// 		console.log('submitTempProposal -> ' + res);

		// 		proposalFactory.GetRekapInstructionByProposalId(1, 10, $scope.currentOutletId)
		// 			.then
		// 			(
		// 			function (res) {
		// 				$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = res.data.Result;
		// 				$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
		// 				$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageSize;
		// 				$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
		// 				$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_GroupInstruksiUntukProposal_UIGrid.pageNumber;
		// 			}
		// 			);

		// 	},
		// 	function (err) {
		// 		if (err != null) {
		// 			console.log("err=>", err);
		// 		}
		// 	}
		// 	)
	};


	$scope.LoadingSimpan = false;
	$scope.TambahProposal_Simpan = function () {
		$scope.Cetak = 'N';
		$scope.SimpanData(0);
	}

	$scope.TambahProposal_SimpanCetak = function () {
		$scope.Cetak = 'Y';
		$scope.SimpanData(0);
	}

	$scope.TambahProposal_SimpanDraft = function () {
		$scope.SimpanData(1);
	}

	$scope.SimpanData = function (isDraft) {
		$scope.LoadingSimpan = true;
		var data;

		$scope.ProposalSubmitData = [];
		$scope.SimpanCetak_EnabledDisabled = false;
		$scope.Simpan_EnabledDisabled = false;

		$scope.TambahProposal_ProposalListData();
		$scope.ProposalSubmitData = $scope.CreateProposalSubmitData;
		$scope.TambahProposal_ProposalListData_New();

		if (isDraft == 0 && ((angular.isUndefined($scope.ProposalSubmitData)) || ($scope.ProposalSubmitData == ""))) {
			console.log($scope.ProposalSubmitData);
			//alert("Data Proposal Blm Lengkap !");
			bsNotify.show({
				title: "Warning",
				content: "Data Proposal Blm Lengkap.",
				type: "warning"
			});
			$scope.LoadingSimpan = false;
			$scope.SimpanCetak_EnabledDisabled = true;
			$scope.Simpan_EnabledDisabled = true;
			return;
		}
		// if (isDraft == 0 && ((angular.isUndefined($scope.TambahProposal_NamaBankPengirim)) || ($scope.TambahProposal_NamaBankPengirim == ""))) {
		// 	console.log($scope.TambahProposal_NamaBankPengirim);
		// 	//alert("Nomor Rekening Belum Lengkap !");
		// 	bsNotify.show({
		// 		title: "Warning",
		// 		content: "Nomor Rekening Belum Lengkap.",
		// 		type: "warning"
		// 	});
		// 	$scope.SimpanCetak_EnabledDisabled = true;
		// 	$scope.Simpan_EnabledDisabled = true;
		// 	return;
		// }

		data =
			[{
				ProposalId: $scope.currentProposalId,
				OutletId: $scope.currentOutletId,
				ProposalDate: $scope.TambahProposal_TanggalProposal,
				SenderAccountId: $scope.TambahProposal_NoRekPengirim_Search,
				GiroChequeName: $scope.TambahProposal_NoChequeGiro,
				GiroChequeId: $scope.TambahProposal_NoGiroCheque,
				IsDraft: isDraft,
				AccountTo: $scope.TambahProposal_NoRekTujuan,
				ProposalDetailXML: "",
				ProposalList: $scope.submitProposalDataNew
			}]

		proposalFactory.submitProposal(data)
			.then
			(
				function (res) {
					console.log(res);
					console.log(res.data.Result[0].ProposalId);
					$scope.currentProposalId = res.data.Result[0].ProposalId;
					//alert("Simpan Berhasil");
					bsNotify.show({
						title: "Berhasil",
						content: "Data berhasil disimpan.",
						type: "success"
					});
					$scope.LoadingSimpan = false;
					$scope.ProposalId = 0;
					$scope.SimpanCetak_EnabledDisabled = true;
					$scope.Simpan_EnabledDisabled = true;
					if ($scope.Cetak == "Y") {
						$scope.LihatProposal_Cetak();
					}
					$scope.ClearGridData();
					$scope.TambahProposal_Batal();
					$scope.Lihat_CariProposalData();
				},
				function (err) {
					console.log("err=>", err);
					if (err != null) {
						//alert(err.data.Message);
						bsNotify.show({
							title: "Error",
							content: err.data.Message,
							type: "danger"
						});
						$scope.LoadingSimpan = false;
						$scope.SimpanCetak_EnabledDisabled = true;
						$scope.Simpan_EnabledDisabled = true;
					}
				}
			)
	}
	//Pembatalan
	$scope.lihat_ModalBatalProposal = function () {
		angular.element('#ModalBatalProposal').modal('show');
	}

	$scope.Cancel_ModalBatalProposal = function () {
		angular.element('#ModalBatalProposal').modal('hide');
	};

	$scope.Batal_ModalBatalProposal = function () {
		proposalFactory.batalProposal($scope.currentProposalId)
			.then(
				function (res) {
					$scope.Cancel_ModalBatalProposal();


					bsNotify.show({
						title: "Berhasil",
						content: "Proposal Payment berhasil dibatalkan.",
						type: "success"
					});
					$scope.Cancel_ModalBatalProposal();
					$scope.LihatProposal_Batal();
					$scope.GetListProposal(1);
				},
				function (err) {
					console.log("Error : ", err);
					//alert(err.data.Message);
					bsNotify.show({
						title: "Warning",
						content: err.data.Message,
						type: "warning"
					});
					$scope.Cancel_ModalBatalProposal();
				}
			);


	}

	$scope.LihatProposal_EditDraft = function () {
		$scope.ToTambahProposal();
		$scope.ProposalPayment_GetDataBranchByOutletId();
		$scope.RekapEditDraft();
		$scope.EditMode = false;
		// $scope.Show_ShowLihatProposal = false;
		// $scope.Show_ShowTambahProposal = true;	
		$scope.ShowProposalDataLihat = false;
		$scope.Show_TambahProposal_FootBalance = true;
		$scope.Show_ShowTemp = false;

		Enable_TambahProposal_DaftarInstruksi();

		console.log("1");
		//$scope.currentNamaBranch_Search = "" + $scope.currentNamaBranch_Search + "";
		console.log($scope.currentNamaBranch_Search);
		console.log($scope.currentNoRekPengirim_Search);
		console.log("2");

		$scope.TambahProposal_TanggalProposal = $scope.LihatProposal_TanggalProposal;
		$scope.TambahProposal_MetodePembayaran = "Bank Transfer";
		$scope.TambahProposal_NamaBranch_Search = $scope.currentNamaBranch_Search;

		console.log($scope.TambahProposal_NoRekPengirim_Search);
		//$scope.TambahProposal_NoRekPengirim_Search = $scope.currentNoRekPengirim_Search;
		//$scope.TambahProposal_NoRekPengirim_Search = 
		console.log($scope.TambahProposal_NoRekPengirim_Search);
		console.log("A");

		$scope.TambahProposal_NoRekTujuan = $scope.LihatProposal_NoRekTujuan;
		$scope.TambahProposal_NamaBankPengirim = $scope.currentNamaBankPengirim;
		$scope.TambahProposal_NamaRekPengirim = $scope.currentNamaRekPengirim;
		$scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data = $scope.LihatProposal_GroupInstruksiUntukProposal_UIGrid.data

		// proposalFactory.GetInstructionByProposalId(1, $scope.uiGridPageSize, $scope.currentProposalNo)
		// 	.then
		// 	(
		// 	function (res) {
		// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = res.data.Result;
		// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.totalItems = res.data.Total;
		// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSize = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
		// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationPageSizes = [10, 25, 50];
		// 		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
		// 	}
		// 	);

		//$scope.TambahProposal_NoRekPengirim_Search = $scope.currentNoRekPengirim_Search;	
		$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
		angular.forEach($scope.TambahProposal_GroupInstruksiUntukProposal_UIGrid.data, function (value, key) {
			if (angular.isUndefined(value.ProposalNo)) {
				proposalFactory.GetRekapDetailInstructionByProposalId(1, 100, value.VendorName)
					.then
					(
						function (res) {
							angular.forEach(res.data.Result, function (value, key) {
								this.push(value);
							}, $scope.TambahProposal_DaftarInstruksiUntukProposal_data);
							//$scope.TambahProposal_DaftarInstruksiUntukProposal_data.push(res.data.Result);
						}
					);
			}
			else {
				proposalFactory.GetInstructionByProposalId(1, 100, value.ProposalNo, value.VendorName,value.RecvAccountName)
					.then
					(
						function (res) {
							angular.forEach(res.data.Result, function (value, key) {
								this.push(value);
							}, $scope.TambahProposal_DaftarInstruksiUntukProposal_data);
							//$scope.TambahProposal_DaftarInstruksiUntukProposal_data.push(res.data.Result);
						}
					);
			}
		});
	}

	$scope.TambahProposal_ProposalListData = function () {
		$scope.CreateProposalSubmitData = [];
		angular.forEach($scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data, function (value, key) {
			var obj =
			{
				"PaymentId": value.PaymentId,
				"PaymentType": value.PaymentType,
				"POId": value.POId,
				"AccountTo": value.AccountTo
			};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateProposalSubmitData.push(obj);
		});
	}

	$scope.TambahProposal_ProposalListData_New = function () {
		$scope.submitProposalDataNew = [];
		angular.forEach($scope.TambahProposal_DaftarInstruksiUntukProposal_data, function (value, key) {
			var obj =
			{
				"PaymentId": value.PaymentId,
				"PaymentType": value.PaymentType,
				"POId": value.POId
			};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.submitProposalDataNew.push(obj);
		});
	}

	$scope.TambahProposal_ProposalListTempData = function (type) {
		$scope.CreateProposalSubmitTempData = [];
		angular.forEach($scope.TambahProposal_DaftarInstruksiUntukProposalTemp_UIGrid.data, function (value, key) {
			var obj =
			{
				"PaymentId": value.PaymentId,
				"PaymentType": value.PaymentType,
				"PaymentNo": value.PaymentNo,
				"TanggalPayment": value.TanggalPayment,
				"MaturityDate": value.MaturityDate,
				"VendorName": value.VendorName,
				"RecvBankName": value.RecvBankName,
				"RecvAccountName": value.RecvAccountName,
				"Nominal": value.Nominal,
				"InstructionType": value.InstructionType,
				"AdvancedType": value.AdvancedType,
				"NoRangka": value.NoRangka,
				"Type": type,
				"POId": value.POId,
				"VendorNameOrDF": value.VendorNameOrDF,
			};
			console.log("Object of Id ==>", obj);
			console.log("Object of Value ==>", value);
			$scope.CreateProposalSubmitTempData.push(obj);
		});
	}

	// klik edit draft
	$scope.TambahProposal_ProposalListTempDataDraft = function (type) {
		$scope.CreateProposalSubmitTempDataDraft = [];
		// angular.forEach($scope.TambahProposal_DaftarInstruksiUntukProposalTemp_UIGrid.data, function (value, key) {
			var obj =
			{
				"PaymentId": $scope.ProposalId,
				"PaymentType":null,
				"PaymentNo": null,
				"TanggalPayment":new Date(),
				"MaturityDate":null,
				"VendorName": null,
				"RecvBankName": null,
				"RecvAccountName": null,
				"Nominal": 0,
				"InstructionType": null,
				"AdvancedType": null,
				"NoRangka": null,
				"Type": type,
				"POId": 0,
				"VendorNameOrDF": null,
			};
			console.log("Object of Id ==>", obj);
			// console.log("Object of Value ==>", value);
			$scope.CreateProposalSubmitTempDataDraft.push(obj);
		// });
	}
	
	$scope.tmpStatusDraft = '';
	$scope.Tambah_CariProposalData = function () {
		if($scope.tmpStatusDraft != 'Draft'){
			$scope.ProposalId = 0 
		}
		proposalFactory.GetListInstructionForProposal(1, paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize, $filter('date')(new Date($scope.TambahProposal_TanggalProposal), 'yyyy-MM-dd'),
			$scope.TambahProposal_NamaBranch_Search, $scope.ProposalId)
			.then
			(
				function (res) {
					$scope.TambahProposal_DaftarInstruksi_UIGrid.data = res.data.Result;
					$scope.TambahProposal_DaftarInstruksi_UIGrid.totalItems = res.data.Total;
					$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSize = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageSize;
					$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationPageSizes = [10, 25, 50];
					$scope.TambahProposal_DaftarInstruksi_UIGrid.paginationCurrentPage = paginationOptions_TambahProposal_DaftarInstruksiUntukProposal_UIGrid.pageNumber;
				}
			);
	};

	$scope.Pengajuan_LihatProposal_PengajuanReversalModal = function () {
		console.log("Pengajuan Reversal");
		var data;
		data =
			[{
				"ApprovalTypeName": "Proposal Reversal",
				"DocumentId": $scope.currentProposalId,
				"ReversalReason": $scope.LihatProposal_PengajuanReversal_AlasanReversal
			}]

		proposalFactory.submitDataApproval(data)
			.then
			(
				function (res) {
					console.log(res);
					$scope.GetListProposal(1);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};

	$scope.LihatProposal_Batal = function () {
		$scope.Show_ShowLihatProposal = true;
		$scope.Show_ShowTambahProposal = false;
		$scope.Show_ShowTemp = false;
		$scope.ShowProposalData = true;
		$scope.ShowProposalDataLihat = false;
		$scope.LihatProporsal_InstructionType = undefined;
		$scope.currentProposalNo = "";
		$scope.currentProposalId = 0;
		$scope.currentTanggalProposal = "";
		$scope.currentNamaBranch_Search = "";
		$scope.currentNoRekPengirim_Search = "";
		$scope.currentNamaBankPengirim = "";
		$scope.currentNamaRekPengirim = "";
		$scope.currentNoGiroCheque = "";
		$scope.ProposalId = 0;


	}

	$scope.TambahProposal_Batal = function () {
		$scope.Show_ShowLihatProposal = true;
		$scope.Show_ShowTambahProposal = false;
		$scope.Show_ShowTemp = false;
		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = [];
		$scope.TambahProposal_DaftarInstruksi_UIGrid.data = [];
		$scope.ProposalSubmitData = [];
		$scope.TambahProposal_NamaBankPengirim = "";
		$scope.TambahProposal_NamaRekPengirim = "";
		$scope.TambahProposal_NoRekPengirim_Search = "";
		$scope.TambahProposal_NoGiroCheque = undefined;
		$scope.TambahProposal_NoRekPengirim_Search = undefined;
		$scope.TambahProposal_NoRekTujuan = "";
		$scope.currentProposalNo = "";
		$scope.currentProposalId = 0;
		$scope.currentTanggalProposal = "";
		$scope.currentNamaBranch_Search = "";
		$scope.currentNoRekPengirim_Search = "";
		$scope.currentNamaBankPengirim = "";
		$scope.currentNamaRekPengirim = "";
		$scope.currentNoGiroCheque = "";
		$scope.ProposalId = 0;
	}

	$scope.ClearGridData = function () {
		$scope.TambahProposal_DaftarInstruksiUntukProposal_UIGrid.data = [];
		$scope.TambahProposal_DaftarInstruksi_UIGrid.data = [];
		$scope.ProposalSubmitData = [];
		$scope.TambahProposal_DaftarInstruksiUntukProposal_data = [];
		$scope.submitProposalDataNew = [];
	}


	//Filter LihatProposal
	$scope.LihatProposal_DaftarProposal_Filter_Clicked = function () {
		//UNFIX || contains like???
		console.log("LihatProposal_DaftarProposal_Filter_Clicked");
		if (($scope.LihatProposal_DaftarProposal_Search_Text != "") &&
			($scope.LihatProposal_DaftarProposal_Search != "")) {
			var nomor;
			var value = $scope.LihatProposal_DaftarProposal_Search_Text;
			$scope.LihatProposal_DaftarProposal_gridAPI.grid.clearAllFilters();
			console.log('filter text -->', $scope.LihatProposal_DaftarProposal_Search_Text);
			if ($scope.LihatProposal_DaftarProposal_Search == "Nomor Proposal Payment") {
				nomor = 3;
			}
			else if ($scope.LihatProposal_DaftarProposal_Search == "Nomor Rekening Pengirim") {
				nomor = 4;
			}
			else if ($scope.LihatProposal_DaftarProposal_Search == "Nama Bank Pengirim") {
				nomor = 8;
			}
			else if ($scope.LihatProposal_DaftarProposal_Search == "Nominal Total") {
				nomor = 10;
			}
			console.log("nomor --> ", nomor);
			$scope.LihatProposal_DaftarProposal_gridAPI.grid.columns[nomor].filters[0].term = $scope.LihatProposal_DaftarProposal_Search_Text;
		}
		else {
			$scope.LihatProposal_DaftarProposal_gridAPI.grid.clearAllFilters();
		}
	};

	$scope.TambahProposal_DaftarInstruksi_Clicked = function () {
		console.log("TambahProposal_DaftarInstruksi_Clicked");
		if (($scope.TambahProposal_DaftarInstruksi_Search_Text != "") &&
			($scope.TambahProposal_DaftarInstruksi_SearchDropdown != "")) {
			var nomor;
			var value = $scope.TambahProposal_DaftarInstruksi_Search_Text;
			$scope.gridApi_DaftarInstruksi.grid.clearAllFilters();
			console.log('filter text -->', $scope.TambahProposal_DaftarInstruksi_Search_Text);
			if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Nomor Instruksi Pembayaran") {
				nomor = 2;
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Tanggal Instruksi Pembayaran") {
				nomor = 3;
				var dateString = $scope.TambahProposal_DaftarInstruksi_Search_Text;
				var dateParts = dateString.split("/");
				var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
				value = dateObject.toDateString();
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Tipe Instruksi Pembayaran") {
				nomor = 4;
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Tipe Advance Payment") {
				nomor = 5;
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Nomor Rangka") {
				nomor = 6;
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Tanggal Jatuh Tempo") {
				nomor = 7;
				var dateString = $scope.TambahProposal_DaftarInstruksi_Search_Text;
				var dateParts = dateString.split("/");
				var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
				value = dateObject.toDateString();
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Nama Pelanggan") {
				nomor = 8;
			}
			else if ($scope.TambahProposal_DaftarInstruksi_SearchDropdown == "Nominal Pembayaran") {
				nomor = 9;
			}
			console.log($scope.TambahProposal_DaftarInstruksi_SearchDropdown);
			console.log("nomor --> ", nomor);
			$scope.gridApi_DaftarInstruksi.grid.columns[nomor].filters[0].term = value;
		}
		else {
			$scope.gridApi_DaftarInstruksi.grid.clearAllFilters();
		}
	};

	/*===============================Bulk Action Section, Begin===============================*/
	$scope.LihatProposal_DaftarBulkAction_Changed = function () {
		console.log("Bulk Action");
		if ($scope.LihatProposal_DaftarBulkAction_Search == "Setuju") {
			angular.forEach($scope.LihatProposal_DaftarProposal_gridAPI.selection.getSelectedRows(), function (value, key) {
				$scope.ModalTolakProposal_ProposalId = value.ProposalId;
				$scope.ModalTolakProposal_TipePengajuan = value.TipePengajuan;
				console.log(value.StatusPengajuan);
				if (value.StatusPengajuan == "Diajukan") {
					$scope.CreateApprovalData("Disetujui");
					proposalFactory.updateApprovalData($scope.ApprovalData)
						.then(
							function (res) {
								//alert("Berhasil Disetujui");
								bsNotify.show({
									title: "Berhasil",
									content: "Berhasil Disetujui.",
									type: "success"
								});
								$scope.Batal_LihatProposal_TolakModal();
								$scope.GetListProposal(1);
							},

							function (err) {
								//alert("Persetujuan gagal");
								bsNotify.show({
									title: "Gagal",
									content: "Persetujuan gagal.",
									type: "danger"
								});
							}
						);
				}
			});
		}
		else if ($scope.LihatProposal_DaftarBulkAction_Search == "Tolak") {
			console.log("grid api ap list ", $scope.LihatProposal_DaftarProposal_gridAPI);
			angular.forEach($scope.LihatProposal_DaftarProposal_gridAPI.selection.getSelectedRows(), function (value, key) {
				console.log(value);
				$scope.ModalTolakProposal_ProposalId = value.ProposalId;
				$scope.ModalTolakProposal_TipePengajuan = value.TipePengajuan;
				$scope.ModalTolakProposal_TanggalProposal = value.TanggalProposal;
				$scope.ModalTolakProposal_NoProposal = value.NoProposal;
				$scope.ModalTolakProposal_TanggalPengajuan = value.TglPengajuan;
				$scope.ModalTolakProposal_AlasanPengajuan = value.AlasanPengajuan;
				if (value.StatusPengajuan == "Diajukan")
					angular.element('#ModalTolakProposal').modal();
			}
			);
		}
	}

	$scope.CreateApprovalData = function (ApprovalStatus) {
		if ($scope.ModalTolakProposal_TipePengajuan == "Reversal") {
			$scope.ModalTolakProposal_TipePengajuan = "Proposal Reversal";
		}

		$scope.ApprovalData =
			[
				{
					"ApprovalTypeName": $scope.ModalTolakProposal_TipePengajuan,
					"RejectedReason": $scope.ModalTolakProposal_AlasanPenolakan,
					"DocumentId": $scope.ModalTolakProposal_ProposalId,
					"ApprovalStatusName": ApprovalStatus
				}
			];
	}

	$scope.Batal_LihatProposal_TolakModal = function () {
		angular.element('#ModalTolakProposal').modal('hide');
		$scope.LihatProposal_DaftarBulkAction_Search = "";
		$scope.ClearActionFields();
		$scope.GetListProposal(1);
	}

	$scope.Pengajuan_LihatProposal_TolakModal = function () {
		$scope.CreateApprovalData("Ditolak");
		proposalFactory.updateApprovalData($scope.ApprovalData)
			.then(
				function (res) {
					//alert("Berhasil tolak");
					bsNotify.show({
						title: "Berhasil",
						content: "Berhasil tolak.",
						type: "success"
					});
					$scope.Batal_LihatProposal_TolakModal();
					$scope.GetListProposal(1);
				},

				function (err) {
					//alert("Penolakan gagal");
					bsNotify.show({
						title: "Gagal",
						content: "Penolakan gagal.",
						type: "danger"
					});
				}
			);
	}

	$scope.ClearActionFields = function () {
		$scope.ModalTolakProposal_TanggalProposal = "";
		$scope.ModalTolakProposal_NoProposal = "";
		$scope.ModalTolakProposal_TanggalPengajuan = "";
		$scope.ModalTolakProposal_AlasanPengajuan = "";
	}
	/*===============================Bulk Action Section, End===============================*/
	/*=================================Cetak Proposal Payment Begin===================================*/
	$scope.LihatProposal_Cetak = function () {
		var pdffile = "";
		var enddate = "";
		$scope.ProposalPayment_ProposalId = $scope.currentProposalId;
		console.log($scope.ProposalPayment_ProposalId)

		proposalFactory.getCetakReportProposalPayment($scope.ProposalPayment_ProposalId)
			.then(
				function (res) {
					var file = new Blob([res.data], { type: 'application/pdf' });
					var fileurl = URL.createObjectURL(file);

					console.log("pdf", fileurl);


					printJS(fileurl);
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	};
	/*==================================Cetak Proposal Payment End====================================*/

	$scope.LihatTanggalProposalOK = true;
	$scope.LihatProposal_TujuanPembayaran_TanggalProposal_Changed = function () {
		var dt = $scope.LihatProposal_TujuanPembayaran_TanggalProposal;
		var dtTo = $scope.LihatProposal_TujuanPembayaran_TanggalProposalTo;

		var now = new Date();

		if (dt > dtTo) {
			$scope.errLihatTanggalProposal = "Tanggal proposal payment awal tidak boleh lebih besar dari tanggal proposal payment akhir";
			$scope.LihatTanggalProposalOK = false;
		}
		else if (dt > now || dtTo > now) {
			$scope.errLihatTanggalProposal = "Tanggal tidak boleh lebih besar dari tanggal hari ini";
			$scope.LihatTanggalProposalOK = false;
		}
		else {
			$scope.LihatTanggalProposalOK = true;
		}
	}

	//First load
	$scope.ProposalPayment_GetDataBranchByOutletId();
	$scope.Lihat_CariProposalData();
	$scope.LihatProposal_MetodePembayaran = "Bank Transfer";
});

app.filter('rupiahCC', function () {
	return function (val) {
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
		}
		return val;
	};
});

app.filter('nullDateFilter', function ($filter) {
	return function (input) {
		//var originalFilter = $filter('date:\"dd-MM-yyyy\"');
		var dd = new Date('1900-01-02');
		return input < dd ? '' : $filter('date')(input, "dd-MM-yyyy");
	};
});