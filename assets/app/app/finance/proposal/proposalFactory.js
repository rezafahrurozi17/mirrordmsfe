/*
Edit by : 
	20170414, eric, edit for incoming payment swapping & ekspedisi
*/

angular.module('app')
	.factory('proposalFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;

		factory.submitProposal = function (inputData) 
		{
			var url = '/api/fe/Proposal/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitProposal') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}

		factory.submitTempProposal = function (inputData) 
		{
			var url = '/api/fe/Proposal/submitProposalTemp';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitTempProposal') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}	
		
		factory.deleteTempProposal = function (vendorName) 
		{
			var url = '/api/fe/Proposal/DeleteData?OutletID=0&VendorName=' + vendorName;
			var res=$http.delete(url);
			return res;
		}	

		factory.GetListProposal = function(start, limit, TranDate, TranDateTo, Branch,SearchText,SearchType)
		{
			var url = '/api/fe/Proposal/GetListProposal/?start=' + start + '&limit=' + limit + '&StartDate=' + TranDate + '&EndDate=' + TranDateTo + '&OutletId=' + Branch+'&FilterData='+SearchText+'|'+SearchType;
			console.log ("execute GetListProposal");
			return $http.get(url);
		}
	
		factory.updateApprovalData = function(inputData)
		{
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		}

		factory.GetListInstructionForProposal = function(start, limit, TranDate, Branch, ProposalId)
		{
			var url = '/api/fe/Proposal/GetListInstructionForProposal/start/' + start + '/limit/' + limit + '/FilterData/' + TranDate + '|' + Branch + '|' + ProposalId;
			console.log ("execute GetListInstructionForProposal");
			return $http.get(url);
		}		

		factory.GetLihatRekapInstructionByProposalId = function(start, limit, ProposalNo)
		{
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//ProposalNo = ProposalNo.replace(regex, "~"); 		
			var url = '/api/fe/Proposal/GetLihatRekapInstructionByProposalId?start=' + start + '&limit=' + limit + '&FilterData=' + ProposalNo;
			console.log ("execute GetLihatRekapInstructionByProposalId : ", url);
			return $http.get(url);
		}	

		factory.GetInstructionByProposalId = function(start, limit, ProposalNo, VendorName , Norek)
		{
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//ProposalNo = ProposalNo.replace(regex, "~"); 		
			//var url = '/api/fe/Proposal/GetInstructionByProposalId?start=' + start + '&limit=' + limit + '&FilterData=' + ProposalNo +'|' + VendorName;
			var url = '/api/fe/Proposal/GetInstructionByProposalId?start=' + start + '&limit=' + limit + '&FilterData=' + ProposalNo +'|'+VendorName +'|'+ Norek;
			console.log ("execute GetListInstructionForProposal : ", url);
			return $http.get(url);
		}	

		factory.GetRekapInstructionByProposalId = function(start, limit, FilterData)
		{
			//var find = "/";
			//var regex = new RegExp(find, "g");
			//ProposalNo = ProposalNo.replace(regex, "~"); 		
			var url = '/api/fe/Proposal/GetRekapInstructionByProposalId/start/' + start + '/limit/' + limit + '/FilterData/' + FilterData;
			console.log ("execute GetRekapInstructionForProposal : ", url);
			return $http.get(url);
		}	

		factory.GetRekapDetailInstructionByProposalId = function(start, limit, FilterData)
		{
			var url = '/api/fe/Proposal/GetRekapDetailInstructionByProposalId?start='+ start + '&limit=' + limit + '&FilterData=' + FilterData;
			console.log ("execute GetRekapDetailInstructionForProposal : ", url);
			return $http.get(url);
		}	
		
		factory.getDataBranchComboBox = function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		}				 
		
		factory.submitDataApproval = function (inputData) {
			var url = '/api/fe/FinanceApproval/';
			var param = JSON.stringify(inputData);

			if (debugMode) { console.log('Masuk ke submitData') };
			if (debugMode) { console.log('url :' + url); };
			if (debugMode) { console.log('Parameter POST :' + param) };
			return $http.post(url, param);
		}		
		
		factory.updateApprovalData = function(inputData){
			var url = '/api/fe/financeapproval/approverejectdata/';
			var param = JSON.stringify(inputData);
			console.log("object input update approval ", param);
			var res=$http.put(url, param);
			return res;
		}		
		
		factory.getDataAccountBank = function (AccountId) {
			console.log("getDataAccountBank");
			console.log(AccountId);
			var url = '/api/fe/AccountBank/GetDataRekening/' + AccountId;
			var res = $http.get(url);
			return res;
		}

		factory.getDataCGAccountBank = function (CGId) {
			console.log("getDataAccountBank");
			//console.log(AccountId);
			var url = '/api/fe/RegisterChequeGiro/GetDataRekening/' + CGId;
			var res = $http.get(url);
			return res;
		}
		
		factory.getDataBankAccountComboBox = function () {
			var url = '/api/fe/AccountBank/GetDataComboBox/';
			if (debugMode) { console.log('Masuk ke getdata factory getDataBankAccountComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}
		
		factory.getDataCGComboBox = function (proposalId, outletId) {
			var url = '/api/fe/RegisterChequeGiro/GetDataComboBox/FilterData/' + proposalId + '/OutletId/'+ outletId;
			if (debugMode) { console.log('Masuk ke getdata factory getDataBankAccountComboBox'); };
			if (debugMode) { console.log('url :' + url); };
			var res = $http.get(url);
			return res;
		}
		
		factory.getCetakReportProposalPayment=function(proposalId) {
			var url = '/api/fe/FinanceRptProposalPayment/Cetak/?ProposalId=' + proposalId;
			var res=$http.get(url , {responseType: 'arraybuffer'});

			return res;
		};
		
		factory.getBranchByOutletId= function(outletId){
			//var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
			var url = '/api/fe/Branch/SelectData?start=0&limit=0&FilterData=' + outletId;
			var res=$http.get(url);  
			return res;	
		};

		factory.getStatusBungaDF=function(outletId, pitype) {
			var url = '/api/fe/PaymentInstruction/BungaDF?outletId=' + outletId + '&PIType=' + pitype;
		  //var url = '/api/fe/PaymentInstruction/SelectData/Start/' + start + '/Limit/' + limit + '/FilterData/' + JSON.stringify(inputData);
			var res=$http.get(url);  
		  
		  return res;  
	  };
	  factory.batalProposal = function(proposalid) {
		var url = '/api/fe/Proposal/DeleteData/' + proposalid;
		var res=$http.delete(url);
		return res;
	}

		return factory;
	});