angular.module('app')
	.factory('PenerimaanCashSPKFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataMaster=function(outletId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PenerimaanCashSPK/SelectData?outletId=' + outletId;
      var res=$http.get(url);  
      return res;			
    };

    factory.getDataUserList=function(outletId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PenerimaanCashSPK/SelectDataUserList?outletId=' + outletId;
      var res=$http.get(url);  
      return res;			
    };

    factory.GetEmployeeList=function(outletId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/PenerimaanCashSPK/SelectEmployeeList?outletId=' + outletId;
      var res=$http.get(url);  
      return res;			
    };

factory.submitData= function(inputData) {
    	//var url = '/api/fe/IncomingInstruction';PenerimaanCashSPK
        var url = '/api/fe/PenerimaanCashSPK/SubmitData/';
    	var param = JSON.stringify(inputData);

	if (debugMode){console.log('Masuk ke submitData')};
	if (debugMode){console.log('url :'+url);};
	if (debugMode){console.log('Parameter POST :'+param)};
      	var res=$http.post(url, param);
      	
        return res;
    };


	return factory;
});