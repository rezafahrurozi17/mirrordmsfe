angular.module('app')
  .factory('FinancePersetujuanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        //var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
		
		var da_json=[
				{Id: "1",  TanggalPengajuan: new Date("2/27/2017"),TipePengajuan: "Cetak Ulang Kuitansi",NomorReferensi: "NomorReferensi1",Pengaju: "pengaju",AlasanPengajuan: "alasan",Selected: false},
				{Id: "2",  TanggalPengajuan: new Date("2/27/2017"),TipePengajuan: "Cetak Ulang Kuitansi",NomorReferensi: "NomorReferensi2",Pengaju: "pengaju",AlasanPengajuan: "alasan",Selected: false},
				{Id: "3",  TanggalPengajuan: new Date("2/27/2017"),TipePengajuan: "Cetak Ulang BPH",NomorReferensi: "NomorReferensi3",Pengaju: "pengaju",AlasanPengajuan: "alasan",Selected: false},
				{Id: "4",  TanggalPengajuan: new Date("2/27/2017"),TipePengajuan: "Alokasi Unknown",NomorReferensi: "NomorReferensi4",Pengaju: "pengaju",AlasanPengajuan: "alasan",Selected: false},
				{Id: "5",  TanggalPengajuan: new Date("2/27/2017"),TipePengajuan: "Alokasi Unknown",NomorReferensi: "NomorReferensi5",Pengaju: "pengaju",AlasanPengajuan: "alasan",Selected: false},
			  ];
		res=da_json;
		
        return res;
      },
      create: function(financePersetujuan) {
        return $http.post('/api/fw/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: financePersetujuan.Name,
                                            Description: financePersetujuan.Description}]);
      },
      update: function(financePersetujuan){
        return $http.put('/api/fw/Role', [{
                                            Id: financePersetujuan.Id,
                                            //pid: negotiation.pid,
                                            Name: financePersetujuan.Name,
                                            Description: financePersetujuan.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });