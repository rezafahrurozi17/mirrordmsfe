angular.module('app')
.controller('FinancePersetujuanController', function($scope, $http, CurrentUser,FinancePersetujuanFactory,$timeout) {
    
	$scope.optionFinancePersetujuan_TipePengajuan = [{ name: "Semua", value: "Semua" }, { name: "Alokasi Unknown", value: "Alokasi Unknown" }, { name: "Cetak Ulang BPH", value: "Cetak Ulang BPH" },{ name: "Cetak Ulang Kuitansi", value: "Cetak Ulang Kuitansi" },{ name: "Cetak Ulang TTUS", value: "Cetak Ulang TTUS" },{ name: "Distribusi Booking Fee", value: "Distribusi Booking Fee" },{ name: "Instruksi Pembayaran", value: "Instruksi Pembayaran" },{ name: "Reversal Alokasi Unknown", value: "Reversal Alokasi Unknown" },{ name: "Reversal Incoming Payment", value: "Reversal Incoming Payment" },{ name: "Reversal Instruksi Pembayaran", value: "Reversal Instruksi Pembayaran" },{ name: "Reversal Outgoing Payment", value: "Reversal Outgoing Payment" },{ name: "Reversal Reconciliation", value: "Reversal Reconciliation" },{ name: "Reversal Transaksi Cash & Bank", value: "Reversal Transaksi Cash & Bank" }];
	
	$scope.FinancePersetujuan_Cari = function () {
		$scope.ShowFinancePersetujuan_DaftarPengajuanPersetujuan=true;
		$scope.ShowFinancePersetujuan_Action=true;
		
		$scope.options_FinancePersetujuan_DaftarPengajuanPersetujuanSearchBy = [{ name: "Tanggal Pengajuan", value: "Tanggal Pengajuan" },{ name: "Nomor Referensi", value: "Nomor Referensi" },  { name: "Nama Pengaju", value: "Nama Pengaju" }];
		$scope.FinancePersetujuan_DaftarPengajuanPersetujuanToRepeat=FinancePersetujuanFactory.getData();
		$scope.FinancePersetujuan_SelectedChanged();
	};
	
	
	$scope.FinancePersetujuan_SetujuFinancePersetujuan = function () {
		alert('setuju');
	};
	
	$scope.FinancePersetujuan_TolakFinancePersetujuan = function () {
		angular.element('#ModalFinancePersetujuan_AlasanFinancePenolakan').modal();
	};
	
	$scope.Batal_AlasanFinancePenolakanModal = function () {
		angular.element('#ModalFinancePersetujuan_AlasanFinancePenolakan').modal('hide');
	};
	
	$scope.Tolak_AlasanFinancePenolakanModal = function () {
		alert('ditolak');
		angular.element('#ModalFinancePersetujuan_AlasanFinancePenolakan').modal('hide');
	};
	
	$scope.FinancePersetujuan_SelectedChanged = function () {

		var count_selected=0;
		$scope.FinancePersetujuan_SetujuFinancePersetujuan_EnableDisable=false;
		$scope.FinancePersetujuan_TolakFinancePersetujuan_EnableDisable=false;
		
		for(var i = 0; i < $scope.FinancePersetujuan_DaftarPengajuanPersetujuanToRepeat.length; ++i)
		{
			if($scope.FinancePersetujuan_DaftarPengajuanPersetujuanToRepeat[i].Selected == true)
			{	
				count_selected++;
			}
			if(count_selected>1)
			{
				break;
			}
		}
		
		if(count_selected==1)
		{
			$scope.FinancePersetujuan_SetujuFinancePersetujuan_EnableDisable=true;
			$scope.FinancePersetujuan_TolakFinancePersetujuan_EnableDisable=true;
		}
		else if(count_selected>1)
		{
			$scope.FinancePersetujuan_SetujuFinancePersetujuan_EnableDisable=true;
			$scope.FinancePersetujuan_TolakFinancePersetujuan_EnableDisable=false;
		}
		
		
	};


	
	
});
