
var app = angular.module('app');
//20170425, eric, begin
//Nama controller disini harus case sensitive sama dengan route.js
app.controller('informasiBuktiPotongController', function ($scope, $http, CurrentUser, $filter, informasiBuktiPotongFactory, $timeout, uiGridConstants) {
	$scope.optionsLihatInformasi_DaftarInformasi_Search = [{ name: "Semua Kolom", value: "Semua Kolom" }];

	console.log("informasiBuktiPotong");
	$scope.ShowInformasiInsurancePPH23 = true;
	$scope.uiGridPageSize = 10;
	$scope.currentOutletId = 0;
	$scope.LihatTanggalInformasi = new Date();
	$scope.LihatTanggalInformasiTo = new Date();
	$scope.dateOptions = {
		format: "dd/MM/yyyy"
	};


	//----------------------------------
	// Start-Up
	//----------------------------------

	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading=true;
		$scope.loading = false;
		$scope.getDataInsuranceCB();
		//   $scope.gridData=[];

	});
	
	$scope.getDataInsuranceCB = function () 
	{	
		informasiBuktiPotongFactory.getInsuranceComboBox()
		.then
		(
			function (res) 
			{
				$scope.loading = false;
				console.log(res.data.Result);
				$scope.optionsLihatInformasi_NamaInsurancePPH23_Search = res.data.Result;
				console.log($scope.optionsLihatInformasi_NamaInsurancePPH23_Search);
			},
			function (err) 
			{
				console.log("err=>", err);
			}
		);
	};
	
	$scope.CariInformasiData = function () {
		// if (($scope.LihatTanggalInformasi != null)
		// && ($scope.LihatTanggalInformasiTo != null))
		// {
			// $scope.GetInformasiInsurancePPH23(1, $scope.uiGridPageSize);
			// $scope.ShowInformasiData = true;
		// }
		// else
		// {
			// alert("Tgl Awal/Akhir Blm Diisi");
		// }
		
		if (angular.isUndefined($scope.LihatTanggalInformasi)==true)
		{
			alert("Tgl Awal Blm Diisi");
		}
		if (angular.isUndefined($scope.LihatTanggalInformasiTo)==true)
		{
			alert("Tgl Akhir Blm Diisi");
		}
		else if (angular.isUndefined($scope.LihatInformasi_NamaInsurancePPH23_Search)==true)
		{
			alert("Nama Asuransi Blm Diisi");
		}
		else
		{
			$scope.GetInformasiInsurancePPH23(1, $scope.uiGridPageSize);
			$scope.ShowInformasiData = true;
		}			
	};
	
	$scope.LihatInformasi_NamaInsurancePPH23_Search_SelectedChange  = function () {
		console.log($scope.LihatInformasi_NamaInsurancePPH23_Search);
	}
	
    $scope.GetInformasiInsurancePPH23 = function(Start, Limit) {
		//console.log($scope.LihatClearing_NamaBranch_Search);
		informasiBuktiPotongFactory.GetInformasiInsurancePPH23(Start, Limit, $filter('date')(new Date($scope.LihatTanggalInformasi.toLocaleString()), 'yyyy-MM-dd'),
		$filter('date')(new Date($scope.LihatTanggalInformasiTo.toLocaleString()), 'yyyy-MM-dd'), $scope.LihatInformasi_NamaInsurancePPH23_Search)
        .then(
            function(res){
			$scope.LihatInformasi_DaftarInformasi_UIGrid.data=res.data.Result;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.totalItems = res.data.Total;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSize = Limit;
				$scope.LihatInformasi_DaftarInformasi_UIGrid.paginationPageSizes = [Limit];				
                $scope.loading=false;				
            },
            function(err){
                console.log("err=>",err);
            }
        )
    };		
		
	$scope.LihatInformasi_DaftarInformasi_UIGrid = {
		paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination : true,
		multiselect: false,
		enableFiltering: true,
		enableSorting: false,
		onRegisterApi: function (gridApi) {$scope.LihatInformasi_DaftarInformasi_gridAPI= gridApi;
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log(row);
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.GetInformasiInsurancePPH23(pageNumber,pageSize);

				console.log("pageNumber ==>", pageNumber);
				console.log("pageSize ==>", pageSize);
			});
		},
		enableRowSelection: true,
		enableSelectAll: false,
		columnDefs: 
		[
			{ name: 'Nomor Billing', field: 'NoBilling' },		
			{ name: 'Nama Pelanggan', field: 'NamaPelanggan' },			
			{ name: 'NPWP', displayName:'NPWP', field: 'NPWP' },		
			{ name: 'No Bukti Potong', field: 'NoBuktiPotong' },
			{ name: 'Tgl Bukti Potong', field: 'TglBuktiPotong', cellFilter: 'date:\"dd-MM-yyyy\"'  },			
			{ name: 'DPP', displayName:'DPP', field: 'DPP', cellFilter: 'currency:"":0'  },
			{ name: 'PPH23', displayName:'PPH23', field: 'PPH23', cellFilter: 'currency:"":0'  }			
		]
	};
	
	$scope.LihatInformasi_DaftarInformasi_Filter_Clicked = function() 
	{
		console.log("LihatInformasi_DaftarInformasi_Filter_Clicked");
		if ( ($scope.LihatInformasi_DaftarInformasi_Search_Text != "") &&
			($scope.LihatInformasi_DaftarInformasi_Search != "") )
		{		
			var nomor;		
			var value = $scope.LihatInformasi_DaftarInformasi_Search_Text;
			$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.clearAllFilters();
			console.log('filter text -->',$scope.LihatInformasi_DaftarInformasi_Search_Text );
			nomor = 0;
			console.log($scope.LihatInformasi_DaftarInformasi_Search);
			console.log("nomor --> ", nomor);
			 if (nomor == 0) 
			 {
				console.log("masuk 0");
				$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[1].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[2].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[3].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[4].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[5].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				// $scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[6].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
				}
			 else
			 {
				console.log("masuk else");			
				$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.columns[nomor].filters[0].term=$scope.LihatInformasi_DaftarInformasi_Search_Text;
			}
		}
		else {
			$scope.LihatInformasi_DaftarInformasi_gridAPI.grid.clearAllFilters();
			//alert("inputan filter belum diisi !");
		}
	};		
		
});
