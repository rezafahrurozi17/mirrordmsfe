/*
*/

angular.module('app')
	.factory('informasiBuktiPotongFactory', function ($http, CurrentUser) 
	{
		var currentUser = CurrentUser.user;
		var factory = {};
		var debugMode = true;
		
		factory.GetInformasiInsurancePPH23 = function(start, limit, TranDate, TranDateTo, InsuranceId)
		{
			var url = '/api/fe/InsurancePPH23/GetInformasiInsurancePPH23?start=' + start + '&limit=' + limit + '&filterData=' + TranDate + '|' + TranDateTo + '|' + InsuranceId;
			console.log ("execute GetInformasiInsurancePPH23");
			return $http.get(url);
		}
		 
		factory.getInsuranceComboBox = function () {
			console.log("factory.getInsuranceComboBox");
			  var url = '/api/fe/Insurance/SelectData?start=0&limit=0&FilterData=0';
			  var res=$http.get(url);  
			  return res;	
		}				 
		
		
		
		return factory;
	});