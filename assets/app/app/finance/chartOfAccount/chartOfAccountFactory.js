angular.module('app')
	.factory('ChartOfAccountFactory', function ($http, CurrentUser) {
	return{
		getData: function(){
			var url = '/api/fe/FinanceCOA/start/1/limit/1000';
			console.log("url TaxType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create:function(COAData){
			var url = '/api/fe/FinanceCOA/create/';
			var ArrayCOAData = [COAData];
			var param = JSON.stringify(ArrayCOAData);
			console.log("object input saveData", param);
			var res=$http.post(url, param);
			return res;
		},
		
		update:function(COAData){
			var url = '/api/fe/FinanceCOA/Update/';
			var ArrayCOAData = [COAData];
			var param = JSON.stringify(ArrayCOAData);
			console.log("update data ", param);
			var res=$http.put(url, param);
			return res;
		},
		
		delete:function(id){
			var url = '/api/fe/FinanceCOA/deletedata/';
			console.log("url delete Data : ", url);
			var res=$http.delete(url, {data:id, headers:{'content-type' : 'application/json'}});
			return res;
		}
	}
});