var app = angular.module('app');
app.controller('ChartOfAccountController', function ($scope, $http, $filter, CurrentUser, ChartOfAccountFactory, $timeout, bsNotify, $httpParamSerializer) {

	//----------------------------------
	// Start-Up
	//----------------------------------
	$scope.$on('$viewContentLoaded', function () {
		//$scope.loading = true;
		$scope.gridData = [];
	});
	//----------------------------------
	// Initialization
	//----------------------------------
	$scope.user = CurrentUser.user();
	$scope.mCOA = null; //Model
	$scope.xRole = { selected: [] };

	var dateFormat = 'dd/MM/yyyy';

	$scope.dateOption = { format: dateFormat }


	$scope.cekObject = function () {
		console.log($scope.mCOA);
	}

	$scope.actButtonCaption = {
		new: 'Tambah', cancel: 'Kembali'
	}

	//----------------------------------
	// Get Data
	//----------------------------------
	var gridData = [];
	$scope.getData = function () {
		ChartOfAccountFactory.getData()
			.then(
				function (res) {
					$scope.grid.data = res.data.Result;
				}
			);
	}

	function roleFlattenAndSetLevel(node, lvl) {
		for (var i = 0; i < node.length; i++) {
			node[i].$$treeLevel = lvl;
			gridData.push(node[i]);
			if (node[i].child.length > 0) {
				roleFlattenAndSetLevel(node[i].child, lvl + 1)
			} else {

			}
		}
		return gridData;
	}
	$scope.selectRole = function (rows) {
		console.log("onSelectRows=>", rows);
		$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
	}
	$scope.onSelectRows = function (rows) {
		console.log("onSelectRows=>", rows);
	}
	//----------------------------------
	// Grid Setup
	//----------------------------------
	$scope.grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,

		columnDefs: [
			{ name: 'id', field: 'GLAccountId', visible: false },
			{ name: 'Nomor GL Account', displayName: 'Nomor GL Account', field: 'GLAccountCode', enableCellEdit: false },
			{ name: 'Nama GL Account', displayName: 'Nama GL Account', field: 'GLAccountName', enableCellEdit: false },
			{ name: 'Keterangan', field: 'GLDesc', enableCellEdit: false }
		]
	};

	function MsgBox(p) {
		if (p == "sukses") {
			bsNotify.show(
				{
					title: "Sukses",
					content: "Data berhasil disimpan",
					type: 'success'
				}
			);
		}
		else {
			bsNotify.show(
				{
					title: "Gagal",
					content: "Data tidak ditemukan.",
					type: 'danger'
				}
			);
		}
	}

	$scope.doCustomSave = function (mdl, mode) {
		if (mode == "create") {
			ChartOfAccountFactory.create($scope.mCOA).then(
				function (res) {
					if(res.data == true){
						bsNotify.show({
							title: "Sukses",
							content: "Data berhasil disimpan",
							type: 'success'
						});
					}else{
						bsNotify.show({
							title: "Warning",
							content: "GLAccountCode / GLAccountName Sudah Ada",
							type: 'warning'
						});
						return false;
					}
					ChartOfAccountFactory.getData().then(
						function (ress) {
							gridData = [];
							$scope.grid.data = ress.data.Result;
							$scope.loading = false;
							// MsgBox('sukses');
							// return ress.data;
						},
						function (err) {
							// MsgBox('error');
						}
					);


				},
				function (err) {
					// MsgBox('error');
				}
			)
		}
		else if (mode == "update") {
			ChartOfAccountFactory.update($scope.mCOA).then(
				function (res) {
					if(res.data == true){
						bsNotify.show({
							title: "Sukses",
							content: "Data berhasil disimpan",
							type: 'success'
						});
					}else{
						bsNotify.show({
							title: "Warning",
							content: "GLAccountCode / GLAccountName Sudah Ada",
							type: 'warning'
						});
						return false;
					}
					ChartOfAccountFactory.getData().then(
						function (ress) {
							gridData = [];
							$scope.grid.data = ress.data.Result;
							$scope.loading = false;
							// MsgBox('sukses');
							// return res.data;
						},
						function (err) {
							// MsgBox('error');
						}
					);


				},
				function (err) {
					// MsgBox('error');
				}
			)
		}


		$scope.formApi.setMode("grid");
	}

	$scope.actDel = function () {
		alert(34343434);
	}

$scope.gridActionTemplate = '<div align="center" class="ui-grid-cell-contents">\
    <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Ubah" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
  </div>';
});