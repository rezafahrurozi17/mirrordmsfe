var app = angular.module('app');
app.controller('MasterPricingTAMToDealerFinishedUnitController', function ($scope, $http, $filter, CurrentUser, MasterPricingTAMToDealerFinishedUnitFactory, $timeout) {
	$scope.optionsKolomFilterSatu_FinishedUnitTAM = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsKolomFilterDua_FinishedUnitTAM = [{name: "Dealer Name", value:"DealerName"}, 
	 {name: "Province", value:"Province"} ,	 {name: "Price Area", value:"PriceArea"} ,
		{name: "Model", value: "Model"}, {name: "Nama Model / Grade",value:"NamaModel"}, 
		{name: "Katashiki", value:"Katashiki"} ,{name: "Suffix", value:"Suffix"} ];
	$scope.optionsComboBulkAction_FinishedUnitTAM = [{name:"Delete" , value:"Delete"}];
	$scope.optionsComboFilterGrid_FinishedUnitTAM = [{name:"Dealer Name", value:"DealerName"} ,{name:"Province", value:"Province"}
		,{name:"Price List", value:"PriceArea"} ,{name:"Model", value:"Model"}  ,{name:"Tahun", value:"Tahun"}
		,{name:"Nama Model/Grade", value:"NamaModel"} ,{name:"Katashiki", value:"Katashiki"}  ,{name:"Suffix", value:"Suffix"}
		,{name:"Dealer Margin", value:"DealerMargin"} ,{name:"DMS", value:"DMS"} ,{name:"Free Service", value:"FreeService"}
		,{name:"SRUT", value:"SRUT"} ,{name:"Kode Paket", value:"KodePaket"} ,{name:"Paket", value:"PaketDIO"}
		,{name:"Effective Date Start", value:"EffectiveDateFrom"} ,{name:"Effective Date End", value:"EffectiveDateTo"}
		,{name:"Remarks", value:"Remarks"}];
	$scope.MasterSelling_FileName_Current = '';
	var dateFormat = 'dd-MM-yyyy';
	$scope.dateOptions = { format: dateFormat };
	$scope.JenisPE = 'FinishingUnit';
	$scope.cekKolom = '';
	angular.element('#FinishedUnitTAMModal').modal('hide');
	$scope.Filter_FinishedUnitTAM_AndOr = 'And';
	$scope.TypePE = 1; //0 = dealer to branch , 1 = tam to dealer
	$scope.ShowFieldGenerate_FinishedUnitTAM = true;
	$scope.MasterSelling_ExcelData = [];
	$scope.MasterSellingPriceFU_FinishedUnitTAM_grid = {
		paginationPageSize : 10,
		paginationPageSizes: [10,25,50],		
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,			
		columnDefs: [
			{ name: 'Id',displayName:'Id', field: 'Id', visible: false, enableHiding: false },
			{ name: 'DealerName', displayName: 'Dealer Name', field: 'DealerName',enableCellEdit: false, enableHiding: false },
			{ name: 'Province', displayName: 'Province', field: 'Province',enableCellEdit: false, enableHiding: false },
			{ name: 'PriceArea', displayName: 'Price List', field: 'PriceArea',enableCellEdit: false , enableHiding: false},
			{ name: 'Model', displayName: 'Model', field: 'Model', enableCellEdit: false  , enableHiding: false},
			{ name: 'Tahun', displayName: 'Tahun', field: 'Tahun', enableCellEdit: false  , enableHiding: false},
			{ name: 'NamaModel', displayName: 'Nama Model/Grade', field: 'NamaModel',enableCellEdit: false, enableHiding: false},
			{ name: 'Katashiki', displayName: 'Katashiki', field: 'Katashiki',enableCellEdit: false, enableHiding: false },
			{ name: 'Suffix', displayName: 'Suffix', field: 'Suffix',enableCellEdit: false , enableHiding: false},
			{ name: 'DealerMargin', displayName: 'Dealer Margin', field: 'DealerMargin',enableCellEdit: true, enableHiding: false , cellFilter: 'rupiahC' ,  type:"number" },
			{ name: 'DMS', displayName: 'DMS', field: 'DMS',enableCellEdit: true , enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"},
			{ name: 'FreeService', displayName: 'Free Service', field: 'FreeService',enableCellEdit: true , enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"},
			{ name: 'SRUT', displayName: 'SRUT', field: 'SRUT',enableCellEdit: true , enableHiding: false , cellFilter: 'rupiahC' ,  type:"number"},
			{ name: 'KodePaket', displayName: 'Kode Paket', field: 'KodePaket',enableCellEdit: false , enableHiding: false},	
			{ name: 'PaketDIO', displayName: 'Paket', field: 'PaketDIO',enableCellEdit: false, enableHiding: false },	
			{ name: 'EffectiveDateFrom', displayName: 'Effective Date Start', field: 'EffectiveDateFrom', enableHiding: false,enableCellEdit: true,cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'EffectiveDateTo', displayName: 'Effective Date End', field: 'EffectiveDateTo', enableHiding: false,enableCellEdit: true,cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: 'Remarks', displayName: 'Remark', field: 'Remarks',enableCellEdit: true, enableHiding: false }
		],

			onRegisterApi: function (gridApi) {
				$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI = gridApi;
			}
	};

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function(ExcelFile){
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_FinishedUnitTAM' ) ); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function(json){
				console.log("myEl : ", myEl);
				console.log("json : ", json);
				$scope.MasterSelling_ExcelData = json;
				$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

				$scope.MasterSelling_FinishedUnitTAM_Upload_Clicked();
				myEl.val('');
            });
	}

	$scope.validateColumn = function(inputExcelData){
		console.log("inputExcelData : ", inputExcelData);
		console.log("No_Material : ", inputExcelData.Model);
		console.log("Nama_Material : ", inputExcelData.NamaModel);
		console.log("Standard_Cost : ", inputExcelData.FreeService);
		// ini harus di rubah

		$scope.cekKolom = '';
		if (angular.isUndefined(inputExcelData.Model)) {
			$scope.cekKolom = $scope.cekKolom + ' Model \n';
		}
		if (angular.isUndefined(inputExcelData.NamaModel)) {
			$scope.cekKolom = $scope.cekKolom + ' NamaModel \n';
		}	
		if (angular.isUndefined(inputExcelData.Tahun)) {
			$scope.cekKolom = $scope.cekKolom + ' Tahun \n';
		}	
		if (angular.isUndefined(inputExcelData.DealerName)) {
			$scope.cekKolom = $scope.cekKolom + ' DealerName \n';
		}	
		if (angular.isUndefined(inputExcelData.Province)) {
			$scope.cekKolom = $scope.cekKolom + ' Province \n';
		}						
		if (angular.isUndefined(inputExcelData.Katashiki)) {
			$scope.cekKolom = $scope.cekKolom + ' Katashiki \n';
		}
		if 	(angular.isUndefined(inputExcelData.Suffix)){
			$scope.cekKolom = $scope.cekKolom + ' Suffix \n';
		}
		if 	(angular.isUndefined(inputExcelData.PriceArea)){
			$scope.cekKolom = $scope.cekKolom + ' PriceArea \n';
		}
		if	(angular.isUndefined(inputExcelData.DealerMargin)) {
			$scope.cekKolom = $scope.cekKolom + ' DealerMargin \n';
		}

		if	(angular.isUndefined(inputExcelData.FreeService)) {
			$scope.cekKolom = $scope.cekKolom + ' FreeService \n';
		}	
		if	(angular.isUndefined(inputExcelData.SRUT)) {
			$scope.cekKolom = $scope.cekKolom + ' SRUT \n';
		}		
		if	(angular.isUndefined(inputExcelData.KodePaket)) {
			$scope.cekKolom = $scope.cekKolom + ' KodePaket \n';
		}
		if	(angular.isUndefined(inputExcelData.PaketDIO)) {
			$scope.cekKolom = $scope.cekKolom + ' PaketDIO \n';
		}	
		if	(angular.isUndefined(inputExcelData.DMS)) {
			$scope.cekKolom = $scope.cekKolom + ' DMS \n';
		}					
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if	(angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if	(angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}		

		if ( angular.isUndefined(inputExcelData.Model) || 
			angular.isUndefined(inputExcelData.NamaModel) ||
			angular.isUndefined(inputExcelData.Tahun) ||
		 	angular.isUndefined(inputExcelData.DealerName) || 
			angular.isUndefined(inputExcelData.Province) || 
			angular.isUndefined(inputExcelData.PriceArea) ||
			angular.isUndefined(inputExcelData.Katashiki) ||
			angular.isUndefined(inputExcelData.Suffix) || 
			angular.isUndefined(inputExcelData.DealerMargin) ||
			angular.isUndefined(inputExcelData.FreeService) ||
			angular.isUndefined(inputExcelData.SRUT) || 
			angular.isUndefined(inputExcelData.KodePaket) ||
			angular.isUndefined(inputExcelData.PaketDIO) ||
			angular.isUndefined(inputExcelData.DMS) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) || 
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)){
			return(false);
		}
	
		return(true);
	}

	$scope.MasterSelling_FinishedUnitTAM_Download_Clicked=function()
	{
		var excelData = [];
		var fileName = "";		
		fileName = "MasterSellingFinishedUnitTAM";
		if ($scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data.length == 0){
				excelData.push({ 
						DealerName : "",
						Province: "",
						PriceArea: "",
						Model: "",
						Tahun:"",
						NamaModel: "",
						Katashiki: "",
						Suffix : "",
						DealerMargin: "",
						DMS: "",
						FreeService: "",
						SRUT : "",						
						KodePaket: "",		
						PaketDIO: "",
						EffectiveDateFrom: "",													
						EffectiveDateTo: "",
						Remarks: "" });
			fileName = "MasterSellingFinishedUnitTAMTemplate";		
		}
		else {
		//$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.selection.getSelectedRows().forEach(function(row) {
		$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data.forEach(function(row) {
			excelData.push({ 
						DealerName : row.DealerName,
						Province: row.Province,
						PriceArea: row.PriceArea,
						Model: row.Model,
						Tahun : row.Tahun,
						NamaModel: row.NamaModel,
						Katashiki: row.Katashiki,
						Suffix : row.Suffix,
						DealerMargin: row.DealerMargin,
						DMS: row.DMS,
						FreeService: row.FreeService,
						SRUT : row.SRUT,						
						KodePaket: row.KodePaket,		
						PaketDIO: row.PaketDIO,
						EffectiveDateFrom: row.EffectiveDateFrom,													
						EffectiveDateTo: row.EffectiveDateTo,
						Remarks: row.Remarks });
			});
		}
		console.log('isi nya ',JSON.stringify(excelData) );
		console.log(' total row ', excelData[0].length);
		console.log(' isi row 0 ', excelData[0]);
		// angular.forEach($scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.selection.getSelectedRows(), function(value, key){	
		// 	excelData.push(
		// 	);

		// 	// for (var i = 0; i < $scope.MasterSellingPriceFU_FinishedUnitTAM_grid.columnDefs.length; i++) {

		// 	// console.log(i);
		// 	// // more statements
		// 	// }
		// 	excelData = [{"Nama_GL_Biaya":"", "Kode_GL_Biaya":"", "Cost_Center":"", "Periode_Awal":"", "Periode_Akhir":"", "Anggaran_Maksimal":""}];
		// 		$scope.ModalTolak_APId = value.DocumentId;
		// 		$scope.ModalTolak_TipePengajuan = value.ApprovalTypeDesc;
		// 	});;;
		XLSXInterface.writeToXLSX(excelData, fileName);	
	}

	$scope.MasterSelling_FinishedUnitTAM_Upload_Clicked = function() {
		if ($scope.MasterSelling_ExcelData.length == 0)
		{
			alert("file excel kosong !");
			return;
		}

		if(!$scope.validateColumn($scope.MasterSelling_ExcelData[0])){
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}		

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);
		var Grid;

		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		MasterPricingTAMToDealerFinishedUnitFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.totalItems = res.data.Total;	
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked=function() {
		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data);		
		MasterPricingTAMToDealerFinishedUnitFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function(res){
				alert("Berhasil simpan Data");
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_Batal_Clicked=function() {
		$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data = [];
		$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_FinishedUnitTAM = "";
		$scope.TextFilterSatu_FinishedUnitTAM = "";
		var myEl = angular.element( document.querySelector( '#uploadSellingFile_FinishedUnitTAM' ) );
		myEl.val('');	
	}

	$scope.MasterSelling_FinishedUnitTAM_Generate_Clicked=function() {
		var tglStart;
		var tglEnd;

		if  (  (isNaN($scope.MasterSelling_FinishedUnitTAM_TanggalFilterStart) != true) && 
			 (angular.isUndefined($scope.MasterSelling_FinishedUnitTAM_TanggalFilterStart) != true) )
		{
			tglStart = $scope.formatDate($scope.MasterSelling_FinishedUnitTAM_TanggalFilterStart)
		}
		else {
			tglStart = "";
		}

		if  ( (isNaN($scope.MasterSelling_FinishedUnitTAM_TanggalFilterEnd) != true) && 
			 (angular.isUndefined($scope.MasterSelling_FinishedUnitTAM_TanggalFilterEnd) != true) ) 
			 //$scope.TambahOutgoingPayment_RincianPembayaran_NamaBranch != "")
		{
			tglEnd =  $scope.formatDate($scope.MasterSelling_FinishedUnitTAM_TanggalFilterEnd);
		}
		else {
			tglEnd = "";
		}

		var filter = [{PEClassification : $scope.JenisPE,
			Filter1 : $scope.KolomFilterSatu_FinishedUnitTAM,
			Text1 : $scope.TextFilterSatu_FinishedUnitTAM,
			AndOr : $scope.Filter_FinishedUnitTAM_AndOr,
			Filter2 : $scope.KolomFilterDua_FinishedUnitTAM,
			Text2 : $scope.TextFilterDua_FinishedUnitTAM,		
			StartDate : tglStart ,
			EndDate :tglEnd,
			PEType : $scope.TypePE}];
		MasterPricingTAMToDealerFinishedUnitFactory.getData(1,25, JSON.stringify(filter))
		.then(
			function(res){
				$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data = res.data.Result;			//Data hasil dari WebAPI
				$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.totalItems = res.data.Total;	
			},
			function (err) {
				console.log('error -->', err)
			}
		);
	}

	$scope.MasterSelling_FinishedUnitTAM_Cari_Clicked=function() {
		var value = $scope.TextFilterGrid_FinishedUnitTAM;
		$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_FinishedUnitTAM != "") {
			$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.grid.getColumn($scope.ComboFilterGrid_FinishedUnitTAM).filters[0].term=value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.grid.clearAllFilters();
	}

	$scope.ComboBulkAction_FinishedUnitTAM_Changed = function()
	{
		if ($scope.ComboBulkAction_FinishedUnitTAM == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_LogisticPrice_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_LogisticPrice_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_LogisticPrice_grid.data.splice(index, 1);
			// });
			var counter =0;
			angular.forEach($scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.selection.getSelectedRows(), function (data, index) {
				counter++;
			});


			//if ($scope.MasterSellingPriceFU_Karoseri_gridAPI.selection.getSelectedCount() > 0) {
			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_Karoseri_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				angular.element('#FinishedUnitTAMModal').modal('show');
			}
		}
	}

	$scope.OK_Button_Clicked = function()
	{
		angular.forEach($scope.MasterSellingPriceFU_FinishedUnitTAM_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data.splice($scope.MasterSellingPriceFU_FinishedUnitTAM_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_FinishedUnitTAM = {};
		angular.element('#FinishedUnitTAMModal').modal('hide');
	}
	$scope.DeleteCancel_Button_Clicked = function()
	{
		$scope.ComboBulkAction_FinishedUnitTAM = {};
		angular.element('#FinishedUnitTAMModal').modal('hide');
	}

});