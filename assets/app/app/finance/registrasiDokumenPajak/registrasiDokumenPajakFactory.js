	angular.module('app')
	.factory('RegistrasiDokumenPajakFactory', function ($http, CurrentUser) {
	var user=CurrentUser.user();
	
	return{
		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},
		
		getDataRDP:function(start, limit, dateStart, dateEnd, docTypeId, OutletId, searchText, searchType){
			var url = '/api/fe/FinanceTaxDocRegistration/GetData/?start=' + start + '&limit='+ limit + '&datestart=' + dateStart + '&dateend=' + dateEnd + '&TaxDocTypeId=' + docTypeId + '&OutletId=' + OutletId + '&filterData=' + searchText + '|' + searchType;
			console.log("url get Data RDP : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataTaxDocType:function(){
			var url = '/api/fe/FinanceParam/TaxDocType';
			console.log("url get Data TaxDocType : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataBilling:function(start, limit, docTypeId, customerName, OutletId){
			var url = '/api/fe/FinanceTaxDocRegistration/GetBillingData/?start=' + start + '&limit='+ limit + '&TaxDocTypeId=' + docTypeId + '&CustomerName=' + customerName + '&OutletId=' + OutletId;
			console.log("url get Data Billing : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataCustomer:function(customerName, OutletId){
			var url = '/api/fe/FinanceCustomer/GetDataByCustName/?CustomerName=' + customerName + '&outletId='+OutletId;
			console.log("url get Data Customer Name : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataRegNo:function(){
			var url = '/api/fe/FinanceTaxDocRegistration/GetRegNoData/?OutletId=' + user.OutletId;
			console.log("url getDataRegNo : ", url);
			var res=$http.get(url);
			return res;
		},
		
		create : function(inputData){
			var url = '/api/fe/financeTaxDocRegistration/SubmitData/';
			var param = JSON.stringify(inputData);
			console.log("object input Tax Doc Registration ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		getExcelReportTaxDocRegistration:function(dateStart,dateEnd, docTypeId,branch) {
        var url = '/api/fe/FinanceRptTaxDocRegistration/Excel/?DateStart=' + dateStart + '&dateend=' + dateEnd + "&doctypeid=" + docTypeId+"&branch="+branch;
        console.log('get --> ', url);
        var res=$http.get(url);
        return res;
		},

		uploadData: function (data) {
			var url = '/api/fe/FinanceTaxDocRegistration/UploadData';
			var param = JSON.stringify(data);
			var res = $http.post(url, param);
			return res;
		},
		
		editData: function (data) {
			var url = '/api/fe/FinanceTaxDocRegistration/EditData';
			var param = JSON.stringify(data);
			var res = $http.post(url, param);
			return res;
		},
		
		deleteData: function (data) {
			var url = '/api/fe/FinanceTaxDocRegistration/DeleteData/' + data;
			var res = $http.delete(url);
			return res;
		}
	}
});