angular.module('app')
	.factory('BeaMateraiElektronikFactory', function ($http, CurrentUser) {
	return{
		changeFormatDate : function(item) {
			var tmpAppointmentDate = item;
			var finalDate
            // console.log("changeFormatDate item", item);
			// tmpAppointmentDate = new Date(tmpAppointmentDate);
			// console.log("changeFormatDate tmpAppointmentDate", tmpAppointmentDate);
            // if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
            //     var yyyy = tmpAppointmentDate.getFullYear().toString();
            //     var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            //     var dd = tmpAppointmentDate.getDate().toString();
            //     finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            // } else {
            //     finalDate = '';
            // }
			// console.log("changeFormatDate finalDate", finalDate);
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + ''+tmpAppointmentDate[1]+''+tmpAppointmentDate[0];
            return finalDate;
		},
		
		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},
		
		getData:function(Start, Limit, dateStart, dateEnd, OutletId){
			var url = '/api/fe/FinanceEStampDuty/?start=' + Start + '&limit='+ Limit + '&DateStart=' + dateStart + '&DateEnd=' + dateEnd + '&OutletId=' + OutletId;
			console.log("url get Data Bea Masuk : ", url);
			var res=$http.get(url);
			return res;
		},
		
		createData:function(inputData){
			var url = '/api/fe/FinanceEStampDuty/SubmitDataNew/';
			var param = JSON.stringify(inputData);
			console.log("object input estampduty ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		deleteData: function(eStampDutyId, OutletId) {
			var url = '/api/fe/FinanceEStampDuty/deletedata/?EStampDutyId=' + eStampDutyId + '&OutletId=' + OutletId;
			var res=$http.delete(url);
			return res;
		},
		
		updateData:function(inputData){
			var url = '/api/fe/FinanceEStampDuty/UpdateDataNew/';
			var param = JSON.stringify(inputData);
			console.log("object input update estampduty ", param);
			var res=$http.put(url, param);
			return res;
		},

		filterData: function(start, limit, dateStart, dateEnd, OutletId,  searchText, searchType, flag){
			if(flag == 1){
				searchText = this.changeFormatDate(searchText);
			}
			var url = '/api/fe/FinanceEStampDuty/GetDataNew/?start=' + start + '&limit=' + limit + '&DateStart=' + dateStart + '&DateEnd=' + dateEnd + '&OutletId=' + OutletId + '&Filter=' + searchText + '|' + searchType;
			console.log("url DummyPOByDate : ", url);
			var res=$http.get(url);
			return res; 
		},
	}
});