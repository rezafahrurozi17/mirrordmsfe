var app = angular.module('app');
app.controller('BeaMateraiElektronikController', function ($scope,bsNotify, $http, $filter, CurrentUser, BeaMateraiElektronikFactory, $timeout) {
	$scope.uiGridPageSize = 10;
	$scope.Show_MainBeaMateraiElektronik = true;
	var dateFormat = 'dd/MM/yyyy';
	var user = CurrentUser.user();
	
	
	// $scope.startMinOption = new Date();
	// $scope.startMaxOption = null;
	$scope.dateOptions = { 
		format: dateFormat,
	}
	$scope.dateOptionsTgl = { 
		format: dateFormat,
	}
	$scope.dateOptionsMulai = { 
		format: dateFormat,
	}
	$scope.dateOptionsAkhir = { 
		format: dateFormat,
	}
	$scope.dateOptionsEditTgl = { 
		format: dateFormat,
	}
	$scope.dateOptionsEditAwal = { 
		format: dateFormat,
	}
	$scope.dateOptionsEditAkhir = { 
		format: dateFormat,
	}
	
	$scope.optionsMainBeaMateraiElektronik_SelectKolom = [{name:"Tanggal", value:"Tanggal"}, {name:"Nomor Ijin Pembubuhan", value:"NomorPembubuhan"}, {name:"Berlaku Mulai", value:"BerlakuMulai"}, {name:"Berlaku Sampai", value:"BerlakuSampai"}];
	
	if(user.OrgCode.substring(3, 9) == '000000')
	{
		$scope.BeaMateraiElektronik_HO_Show = false;
		$scope.TambahSelectBranch_EnabledDisabled = true;
	}
		
	else
	{
		$scope.BeaMateraiElektronik_HO_Show = true;
		$scope.TambahSelectBranch_EnabledDisabled = false;
	}
		
	
	$scope.BeaMateraiElektronik_getDataBranch = function () 
	{	
		BeaMateraiElektronikFactory.getDataBranchComboBox()
		.then
		(
			function (res) 
			{
				$scope.optionsMainBeaMateraiElektronik_SelectBranch = res.data;
				$scope.optionsTambahBeaMateraiElektronik_SelectBranch = res.data;
				console.log("user.OutletId", user.OutletId);
				console.log("res data branch", res.data);
				
				// res.data.some(function(obj, i){
					// if(obj.value == user.OutletId)
						// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
				// });
				$scope.MainBeaMateraiElektronik_SelectBranch = user.OutletId.toString();
				$scope.TambahBeaMateraiElektronik_SelectBranch = user.OutletId.toString();
				console.log("$scope.MainAdvancedPayment_SelectBranch : ", $scope.MainBeaMateraiElektronik_SelectBranch);
				
				res.data.some(function(obj, i){
					if(obj.value == user.OutletId){
						$scope.MainBeaMateraiElektronik_NamaBranch = obj.name;
						$scope.TambahBeaMateraiElektronik_NamaBranch = obj.name;
					}
				});
			},
			function (err) 
			{
				console.log("err=>", err);
			}
		);
	};
	
	$scope.BeaMateraiElektronik_getDataBranch();

	$scope.MainBeaMateraiElektronikTanggalAwalOK = true;
	$scope.MainBeaMateraiElektronikTanggalAwalChanged = function () {
		console.log($scope.MainBeaMateraiElektronikTanggalAwalOK);
		var dt = $scope.MainBeaMateraiElektronik_TanggalAwal;
		var now = new Date();
		 if (dt > now) {
			$scope.errMainBeaMateraiElektronikTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.MainBeaMateraiElektronikTanggalAwalOK = false;
		}
		else {
			$scope.MainBeaMateraiElektronikTanggalAwalOK = true;
		}
		console.log('asdasd ' + $scope.MainBeaMateraiElektronikTanggalAwalOK);
	}

	$scope.MainBeaMateraiElektronikTanggalAkhirOK = true;
	$scope.Dis_Simpan = false;
	$scope.Simpan_Disable = false;
	$scope.CekValidasiTanaggal = function(){
		var st = $scope.TambahBeaMateraiElektronik_TanggalAwal;
		var ed = $scope.TambahBeaMateraiElektronik_TanggalAkhir;
		var dt = $scope.TambahBeaMateraiElektronik_Tanggal;
		var now = new Date();
		var Esd = $scope.LihatBeaMateraiElektronik_TanggalAwal;
		var Eed = $scope.LihatBeaMateraiElektronik_TanggalAkhir;

		console.log("cek tanggal awal", st);
		console.log("cek tanggal akhir", ed);
		console.log("Cek Button DIS",$scope.Dis_Simpan);
		 if (dt > now) {
			$scope.errTambahBeaMateraiElektronikTanggalChanged = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.TambahBeaMateraiElektronikTanggalOK = false;
		}else if(st > ed){
			$scope.Dis_Simpan = true;
			$scope.TambahBeaMateraiElektronikTanggalOK = false;
		}else if(Esd > Eed){
			bsNotify.show({
				title: "Warning",
				content: "Tanggal Awal tidak boleh lebih dari tanggal Akhri.",
				type: 'warning'
			});
			$scope.Simpan_Disable = true;
			$scope.TambahBeaMateraiElektronikTanggalOK = false;
		}
		else {
			$scope.TambahBeaMateraiElektronikTanggalOK = true;
			$scope.Simpan_Disable = false;
			$scope.Dis_Simpan = false;
		}
	}
	$scope.EditBeaMateraiElektronikTanggalAwalChanged = function(){
		$scope.CekValidasiTanaggal();
	}
	$scope.EditBeaMateraiElektronikTanggalAkhirChanged = function(){
		$scope.CekValidasiTanaggal();		
	}
	$scope.TambahBeaMateraiElektronikTanggalAwalChanged = function(){
		$scope.CekValidasiTanaggal();
	}
	$scope.TambahBeaMateraiElektronikTanggalAkhirChanged = function(){
		$scope.CekValidasiTanaggal();
	}

	$scope.MainBeaMateraiElektronikTanggalAkhirChanged = function () {
		
		console.log($scope.MainBeaMateraiElektronikTanggalAkhirOK);
		var dt = $scope.MainBeaMateraiElektronik_TanggalAkhir;
		var now = new Date();
		 if (dt > now) {
			$scope.errMainBeaMateraiElektronikTanggalAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.MainBeaMateraiElektronikTanggalAkhirOK = false;
		}
		else {
			$scope.MainBeaMateraiElektronikTanggalAkhirOK = true;
		}
		console.log('asdasd ' + $scope.MainBeaMateraiElektronikTanggalAkhirOK);
	}

	$scope.TambahBeaMateraiElektronikTanggalOK = true;

	var today = new Date();
	$scope.startMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());
	$scope.AwalMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());
	$scope.AkhirMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());

	$scope.EditTglMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());
	$scope.EditAwalMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());
	$scope.EditAkhirMinOption = new Date(today.getFullYear(),today.getMonth(),today.getDate());
	
	$scope.TambahBeaMateraiElektronikTanggalChanged = function () {
		console.log($scope.TambahBeaMateraiElektronikTanggalOK);
		var dt = $scope.TambahBeaMateraiElektronik_Tanggal;
		var now = new Date();
		 if (dt > now) {
			$scope.errTambahBeaMateraiElektronikTanggalChanged = "Tanggal tidak boleh lebih dari tanggal hari ini";
			$scope.TambahBeaMateraiElektronikTanggalOK = false;
		}
		else {
			$scope.TambahBeaMateraiElektronikTanggalOK = true;
			$scope.Dis_Simpan = false;
		}
		console.log('asdasd ' + $scope.TambahBeaMateraiElektronikTanggalOK);
	}

	// $scope.TambahBeaMateraiElektronikTanggalAwalOK = true;
	// $scope.TambahBeaMateraiElektronikTanggalAwalChanged = function () {
		
	// 	console.log($scope.TambahBeaMateraiElektronikTanggalAwalOK);
	// 	var dt = $scope.TambahBeaMateraiElektronik_TanggalAwal;
	// 	var now = new Date();
	// 	 if (dt > now) {
	// 		$scope.errTambahBeaMateraiElektronikTanggalAwal = "Tanggal tidak boleh lebih dari tanggal hari ini";
	// 		$scope.TambahBeaMateraiElektronikTanggalAwalOK = false;
	// 	}
	// 	else {
	// 		$scope.TambahBeaMateraiElektronikTanggalAwalOK = true;
	// 	}
	// 	console.log('asdasd ' + $scope.TambahBeaMateraiElektronikTanggalAwalOK);
	// }

	// $scope.TambahBeaMateraiElektronikTanggalAkhirOK = true;
	// $scope.TambahBeaMateraiElektronikTanggalAkhirChanged = function () {
		
	// 	console.log($scope.TambahBeaMateraiElektronikTanggalAkhirOK);
	// 	var dt = $scope.TambahBeaMateraiElektronik_TanggalAkhir;
	// 	var now = new Date();
	// 	 if (dt > now) {
	// 		$scope.errTambahBeaMateraiElektronikTanggalAkhir = "Tanggal tidak boleh lebih dari tanggal hari ini";
	// 		$scope.TambahBeaMateraiElektronikTanggalAkhirOK = false;
	// 	}
	// 	else {
	// 		$scope.TambahBeaMateraiElektronikTanggalAkhirOK = true;
	// 	}
	// 	console.log('asdasd ' + $scope.TambahBeaMateraiElektronikTanggalAkhirOK);
	// }
	
//=========================Main Page Section begin=============================//
	$scope.MainBeaMateraiElektronik_Cari_Clicked = function(){
		if(isNaN($scope.MainBeaMateraiElektronik_TanggalAwal) || isNaN($scope.MainBeaMateraiElektronik_TanggalAkhir))
			//{
				return;
			//}
			// else if(new Date($scope.MainBeaMateraiElektronik_TanggalAwal) > new Date()){
			// 	bsNotify.show({
			// 		title:"Warning",
			// 		content:"Tanggal tidak bisa lebih besar dari hari ini.",
			// 		type:"warning"
			// 	});
			// }
			// else if(new Date($scope.MainBeaMateraiElektronik_TanggalAkhir) > new Date()){
			// 	bsNotify.show({
			// 		title:"Warning",
			// 		content:"Tanggal tidak bisa lebih besar dari hari ini.",
			// 		type:"warning"
			// 	});
			// }
			
		console.log("debug masuk cari clicked");
		$scope.BeaMateraiElektronikList_UIGrid_Paging(1);
		
	}

	//Clear Field
	$scope.clearField = function(){
		$scope.TambahBeaMateraiElektronik_Tanggal = "";
		$scope.TambahBeaMateraiElektronik_NomorIjinPembubuhan = "";
		$scope.TambahBeaMateraiElektronik_TanggalAwal = "";
		$scope.TambahBeaMateraiElektronik_TanggalAkhir = "";
	}

	$scope.TambahBeaMateraiElektronik_Clicked = function(){
		$scope.clearField();
		$scope.Show_MainBeaMateraiElektronik = false;
		$scope.Show_TambahBeaMateraiElektronik = true;
	}
	
	$scope.MainBeaMateraiElektronik_Edit_Clicked = function(row){
		console.log("create date row ", row.entity.CreateDate);
		$scope.LihatBeaMateraiElektronik_EStampDutyId = row.entity.EStampDutyId;
		var createDate = new Date(row.entity.CreateDate);
		createDate.setMinutes(createDate.getMinutes() + 420);
		$scope.LihatBeaMateraiElektronik_Tanggal = createDate;
		$scope.LihatBeaMateraiElektronik_NomorIjinPembubuhan = row.entity.EStampDutyNo;
		var StartDate = new Date(row.entity.StartDate);
		StartDate.setMinutes(StartDate.getMinutes() + 420);
		$scope.LihatBeaMateraiElektronik_TanggalAwal = StartDate;
		var EndDate = new Date(row.entity.EndDate);
		EndDate.setMinutes(EndDate.getMinutes() + 420);
		$scope.LihatBeaMateraiElektronik_TanggalAkhir = EndDate;
		$scope.LihatBeaMateraiElektronik_SelectBranch = row.entity.OutletId;
		$scope.LihatBeaMateraiElektronik_NamaBranch = row.entity.OutletName;
		$scope.Show_MainBeaMateraiElektronik = false;
		$scope.Show_LihatBeaMateraiElektronik = true;
	}
	
	$scope.MainBeaMateraiElektronik_Delete_Clicked = function(row){
		$scope.MainBeaMateraiElektronik_Id = row.entity.EStampDutyId;
		$scope.MainBeaMateraiElektronik_NomorIjinPembubuhan = row.entity.EStampDutyNo;
		angular.element('#ModalHapusBeaMateraiElektronik').modal('show');
	}
	
//=========================Main Page Section End=============================//
//=========================Filter Section Begin=============================//
//OLD Filter
// $scope.MainBeaMateraiElektronik_Filter_Clicked = function(){
// 		var nomor;
// 		$scope.GridApiBeaMateraiElektronikList.grid.clearAllFilters();
// 		$scope.GridApiBeaMateraiElektronikList.grid.refresh();
		
// 		if ($scope.MainBeaMateraiElektronik_SelectKolom == 'Tanggal')
// 			nomor = 1;
// 		else if ($scope.MainBeaMateraiElektronik_SelectKolom == 'Nomor Ijin Pembubuhan')
// 			nomor = 2;
// 		else if ($scope.MainBeaMateraiElektronik_SelectKolom == 'Berlaku Mulai')
// 			nomor = 3;
// 		else if ($scope.MainBeaMateraiElektronik_SelectKolom == 'Berlaku Sampai')
// 			nomor = 4;
// 		else
// 			nomor = 0;
		
// 		console.log("nomor dan filter", nomor, $scope.MainBeaMateraiElektronik_TextFilter);
// 		if(nomor != 0 && $scope.MainBeaMateraiElektronik_TextFilter != "")
// 		{
// 			console.log("masuk if");
// 			$scope.GridApiBeaMateraiElektronikList.grid.columns[nomor].filters[0].term = $scope.MainBeaMateraiElektronik_TextFilter;
// 			$scope.GridApiBeaMateraiElektronikList.grid.refresh();
// 		}
// 	}

// NEW FILTER
var tmpflag = 0
$scope.MainBeaMateraiElektronik_Filter_Clicked = function(){
	if(($scope.MainBeaMateraiElektronik_SelectKolom == "Tanggal") || ($scope.MainBeaMateraiElektronik_SelectKolom == "BerlakuMulai") || ($scope.MainBeaMateraiElektronik_SelectKolom == "BerlakuSampai") ){
		tmpflag = 1
	}else{
		tmpflag = 0;
	}
	BeaMateraiElektronikFactory.filterData(paginationOptions_BeaMateraiElektronik.pageNumber, paginationOptions_BeaMateraiElektronik.pageSize, 
		$filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAwal), 'yyyy-MM-dd'),
		$filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAkhir), 'yyyy-MM-dd'),
		$scope.MainBeaMateraiElektronik_SelectBranch, $scope.MainBeaMateraiElektronik_TextFilter, $scope.MainBeaMateraiElektronik_SelectKolom, tmpflag)
		.then(
			function(res){
				$scope.BeaMateraiElektronikList_UIGrid.data = res.data.Result;
				$scope.BeaMateraiElektronikList_UIGrid.totalItems = res.data.Total;
				$scope.BeaMateraiElektronikList_UIGrid.paginationPageSize = paginationOptions_BeaMateraiElektronik.pageSize;
				$scope.BeaMateraiElektronikList_UIGrid.paginationPageSizes = [10,25,50];
				$scope.BeaMateraiElektronikList_UIGrid.paginationCurrentPage = paginationOptions_BeaMateraiElektronik.pageNumber;
			}
		);
}
//=========================Filter Section End=============================//
//=========================Edit Section Begin=============================//
	
	$scope.LihatBeaMateraiElektronik_Simpan_Clicked = function(){
		$scope.LihatBeaMateraiElektronikCreateData();
		BeaMateraiElektronikFactory.updateData($scope.LihatBeaMateraiElektronikData)
		.then(
			function(res){
				console.log("cek notif nihh >>>>>>>>>>>>",res.data)
				if(res.data == "Sudah Ada Bea Materai Elektronik Pada Tanggal Tersebut"){
					bsNotify.show({
						title:"warning",
						content:"Sudah Ada Bea Materai Elektronik Pada Tanggal Tersebut",
						type:"warning"
					});
					return;
				}else if(res.data == "Sudah Ada Nomor Bea Materai Elektronik Yang Sama"){
					bsNotify.show({
						title:"warning",
						content:"Sudah Ada Nomor Bea Materai Elektronik Yang Sama",
						type:"warning"
					});
					return;
				}else{
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title:"success",
						content:"Data berhasil disimpan",
						type:"success"
					});
				}
				// alert("Data berhasil disimpan");
				$scope.Show_MainBeaMateraiElektronik = true;
				$scope.Show_LihatBeaMateraiElektronik = false;
				$scope.MainBeaMateraiElektronik_Cari_Clicked();
			}
		);
	}
	
	$scope.LihatBeaMateraiElektronikCreateData = function(){
		$scope.LihatBeaMateraiElektronikData = [
			{
				"EStampDutyId":$scope.LihatBeaMateraiElektronik_EStampDutyId,
				"CreateDate": $filter('date')(new Date($scope.LihatBeaMateraiElektronik_Tanggal), "yyyy-MM-dd"),
				"EStampDutyNo": $scope.LihatBeaMateraiElektronik_NomorIjinPembubuhan,
				"StartDate": $filter('date')(new Date($scope.LihatBeaMateraiElektronik_TanggalAwal), "yyyy-MM-dd"),
				"EndDate": $filter('date')(new Date($scope.LihatBeaMateraiElektronik_TanggalAkhir), "yyyy-MM-dd"),
				"OutletId" : $scope.LihatBeaMateraiElektronik_SelectBranch
			}
		];
	}
	
	$scope.LihatBeaMateraiElektronik_Batal_Clicked = function(){
		$scope.Show_MainBeaMateraiElektronik = true;
		$scope.Show_LihatBeaMateraiElektronik = false;
	}
//=========================Edit Section End=============================//	
//=========================Delete Modal Section Begin=============================//
	
	$scope.Batal_ModalHapusBeaMateraiElektronik = function () {
		angular.element('#ModalHapusBeaMateraiElektronik').modal('hide');
		$scope.MainBeaMateraiElektronik_NomorIjinPembubuhan = "";
		$scope.MainBeaMateraiElektronik_Id = 0;
		$scope.BeaMateraiElektronikList_UIGrid_Paging(1);
	};
	
	$scope.Hapus_ModalHapusBeaMateraiElektronik = function () {
		BeaMateraiElektronikFactory.deleteData($scope.MainBeaMateraiElektronik_Id, $scope.MainBeaMateraiElektronik_SelectBranch)
		.then(
			function(res){
				alert("Data berhasil dihapus");
				$scope.BeaMateraiElektronikList_UIGrid_Paging(1);
			},
			function(err){
				alert("Data gagal dihapus");
				console.log(err);
			}
		);
		$scope.Batal_ModalHapusBeaMateraiElektronik();
	};
//=========================Delete Modal Section End=============================//
//=========================Tambah Page Section begin=============================//
	$scope.TambahBeaMateraiElektronik_CheckFields = function(){
		var passed = false;
		
		if(angular.isUndefined($scope.TambahBeaMateraiElektronik_Tanggal)){
			alert("Tanggal harus diisi");
			return(passed);
		}

		if(angular.isUndefined($scope.TambahBeaMateraiElektronik_SelectBranch)){
			alert("Branch harus diisi");
			return(passed);
		}
		
		if(angular.isUndefined($scope.TambahBeaMateraiElektronik_NomorIjinPembubuhan) || $scope.TambahBeaMateraiElektronik_NomorIjinPembubuhan == ""){
			alert("Nomor ijin pembubuhan harus diisi");
			return(passed);
		}
		
		if(angular.isUndefined($scope.TambahBeaMateraiElektronik_TanggalAwal)){
			alert("Tanggal awal berlaku harus diisi");
			return(passed);
		}
		
		if(angular.isUndefined($scope.TambahBeaMateraiElektronik_TanggalAkhir)){
			alert("Tanggal akhir berlaku harus diisi");
			return(passed);
		}
		
		passed = true;
		return(passed);
		
	}
	
	$scope.TambahBeaMateraiElektronik_Simpan_Clicked = function(){
		if(!$scope.TambahBeaMateraiElektronik_CheckFields())
			return;
		
		$scope.TambahBeaMateraiElektronikCreateData();
		BeaMateraiElektronikFactory.createData($scope.TambahBeaMateraiElektronikData)
		.then(
			function(res){
				console.log("cek notif nihh >>>>>>>>>>>>",res.data)
				if(res.data == "Sudah Ada Bea Materai Elektronik Pada Tanggal Tersebut"){
					bsNotify.show({
						title:"warning",
						content:"Sudah Ada Bea Materai Elektronik Pada Tanggal Tersebut",
						type:"warning"
					});
					return;
				}else if(res.data == "Sudah Ada Nomor Bea Materai Elektronik Yang Sama"){
					bsNotify.show({
						title:"warning",
						content:"Sudah Ada Nomor Bea Materai Elektronik Yang Sama",
						type:"warning"
					});
					return;
				}else{
					//alert("Data berhasil disimpan");
					bsNotify.show({
						title:"success",
						content:"Data berhasil disimpan",
						type:"success"
					});
				}
				
				$scope.Show_MainBeaMateraiElektronik = true;
				$scope.Show_TambahBeaMateraiElektronik = false;
				$scope.MainBeaMateraiElektronik_Cari_Clicked();
			}
		);
	}
	
	$scope.TambahBeaMateraiElektronikCreateData = function(){
		$scope.TambahBeaMateraiElektronikData = [
			{
				"CreateDate": $filter('date')(new Date($scope.TambahBeaMateraiElektronik_Tanggal), "yyyy-MM-dd"),
				"EStampDutyNo": $scope.TambahBeaMateraiElektronik_NomorIjinPembubuhan,
				"StartDate": $filter('date')(new Date($scope.TambahBeaMateraiElektronik_TanggalAwal), "yyyy-MM-dd"),
				"EndDate": $filter('date')(new Date($scope.TambahBeaMateraiElektronik_TanggalAkhir), "yyyy-MM-dd"),
				"OutletId" : $scope.TambahBeaMateraiElektronik_SelectBranch
			}
		];
	}
	
	$scope.TambahBeaMateraiElektronik_Batal_Clicked = function(){
		$scope.Show_MainBeaMateraiElektronik = true;
		$scope.Show_TambahBeaMateraiElektronik = false;
	}
//==========================Tambah Page Section end==============================//
//=========================UI Grid Section begin=============================//
var paginationOptions_BeaMateraiElektronik = {
	pageNumber: 1,
	pageSize: 10,
	sort: null
}
	$scope.BeaMateraiElektronikList_UIGrid = {
		paginationPageSizes: [10,25,50],
		useCustomPagination: true,
		useExternalPagination : true,
		rowSelection : true,
		enableFiltering: true,
		columnDefs: [
						{name:"EStampDutyId",field:"EStampDutyId",visible:false},	
						{name:"OutletId",field:"OutletId",visible:false},	
						{name:"OutletName",field:"OutletName",visible:false},	
						{name:"Tanggal",field:"CreateDate", cellFilter:'date:\"dd-MM-yyyy\"'},	
						{name:"Nomor Ijin Pembubuhan",field:"EStampDutyNo"},
						{name:"Berlaku Mulai",field:"StartDate", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Berlaku Sampai",field:"EndDate", cellFilter:'date:\"dd-MM-yyyy\"'},
						{name:"Action",
							cellTemplate:'<a ng-click="grid.appScope.MainBeaMateraiElektronik_Edit_Clicked(row)">Edit</a>   <a ng-click="grid.appScope.MainBeaMateraiElektronik_Delete_Clicked(row)">Hapus</a>'},
					],
		onRegisterApi: function(gridApi) {
			$scope.GridApiBeaMateraiElektronikList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.BeaMateraiElektronikList_UIGrid_Paging(pageNumber);
				paginationOptions_BeaMateraiElektronik.pageNumber = pageNumber;
				paginationOptions_BeaMateraiElektronik.pageSize = pageSize;
			});
		}
	};
	
	$scope.BeaMateraiElektronikList_UIGrid_Paging = function(pageNumber){
		  BeaMateraiElektronikFactory.getData(paginationOptions_BeaMateraiElektronik.pageNumber, paginationOptions_BeaMateraiElektronik.pageSize, 
											$filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAwal), 'yyyy-MM-dd'),
											$filter('date')(new Date($scope.MainBeaMateraiElektronik_TanggalAkhir), 'yyyy-MM-dd'),
											$scope.MainBeaMateraiElektronik_SelectBranch)
		  .then(
			function(res)
			{
				console.log("res =>", res.data.Result);
				$scope.BeaMateraiElektronikList_UIGrid.data = res.data.Result;
				$scope.BeaMateraiElektronikList_UIGrid.totalItems = res.data.Total;
				$scope.BeaMateraiElektronikList_UIGrid.paginationPageSize = paginationOptions_BeaMateraiElektronik.pageSize;
				$scope.BeaMateraiElektronikList_UIGrid.paginationPageSizes = [10,25,50];
				$scope.BeaMateraiElektronikList_UIGrid.paginationCurrentPage = paginationOptions_BeaMateraiElektronik.pageNumber;
			}
			
		);
	}

//==========================UI Grid Section end==============================//
});