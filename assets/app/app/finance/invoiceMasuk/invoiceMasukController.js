var app = angular.module('app');
app.controller('InvoiceMasukController', function ($scope, $http, $filter, uiGridConstants, CurrentUser, InvoiceMasukFactory, $timeout, bsNotify) {
	$scope.uiGridPageSize = 10;
	$scope.Show_MainInvoiceMasuk = true;
	var user = CurrentUser.user();
	$scope.user = CurrentUser.user();
	console.log("data user", user);
	$scope.InvoiceMasukTambah_POList = [];
	$scope.InvoiceMasuk_JenisInvoiceMasuk = [];
	$scope.InvoiceMasuk_JenisPajakData = [];
	$scope.MainInvoiceMasuk_TanggalAwal = new Date();
	$scope.MainInvoiceMasuk_TanggalAkhir = new Date();
	$scope.dateNow = Date.now();
	$scope.EditMode = false;
	$scope.Loading = false;
	$scope.NominalInvoice_Disable = false;
	$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = [];
	$scope.optionsTambahInvoiceMasukTipeInvoiceMasukNotice = [{name:'Notice',value:'Notice'},{name:'Unnotice',value:'Unnotice'}]
	var paginationOptions_Main = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	}
	var uiGridPageSize = 10;
	if (user.OrgCode.substring(3, 9) == '000000'){
		$scope.InvoiceMasuk_HO_Show = true;
		$scope.NamaBranch = false;
	}
	else{
		$scope.InvoiceMasuk_HO_Show = false;
		$scope.NamaBranch = true;
	}

	$scope.TambahInvoiceMasuk_RincianPajak_ShowNomorDokumen = true;
	$scope.TambahInvoiceMasuk_RincianPajak_ShowTanggalDokumen = true;
	/*==============================================================================================================*/
	/*										Comma Separator setup, Begin											*/
	/*==============================================================================================================*/
	$scope.InvMasuk_RincianPajak_DPP = function (item, event) {
		console.log("item", item);
		if (event.which > 37 && event.which < 40) {
			event.preventDefault();
			return false;
		} else {
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			return;
		}
	};

	function addSeparatorsNF(nStr, inD, outD, sep) {
		nStr += '';
		var dpos = nStr.indexOf(inD);
		var nStrEnd = '';
		if (dpos != -1) {
			nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
			nStr = nStr.substring(0, dpos);
		}
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(nStr)) {
			nStr = nStr.replace(rgx, '$1' + sep + '$2');
		}
		return nStr + nStrEnd;
	}
	/*==============================================================================================================*/
	/*										Comma Separator setup, end												*/
	/*==============================================================================================================*/

	$scope.InvoiceMasuk_getDataBranch = function () {
		InvoiceMasukFactory.getDataBranchComboBox()
			.then
			(
			function (res) {
				$scope.optionsMainInvoiceMasuk_SelectBranch = res.data;
				$scope.optionsTambahInvoiceMasuk_SelectBranch = res.data;
				console.log("user.OutletId", user.OutletId);
				console.log("res data branch", res.data);
				console.log(res.data[0].value);
				console.log('di atas gan');
				// res.data.some(function(obj, i){
				// if(obj.value == user.OutletId)
				// $scope.MainInvoiceMasuk_SelectBranch = {name:obj.name, value:obj.value};
				// });
				$scope.MainInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
				$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
				console.log("$scope.MainInvoiceMasuk_SelectBranch : ", $scope.MainInvoiceMasuk_SelectBranch);
				res.data.some(function (obj, i) {
					if (obj.value == user.OutletId)
						$scope.MainInvoiceMasuk_NamaBranch = obj.name;
				});
			},
			function (err) {
				console.log("err=>", err);
			}
			);

		if (user.OrgCode.substring(3, 9) == '000000') {
			$scope.Main_NamaBranch_EnableDisable = true;
		}
		else {
			$scope.Main_NamaBranch_EnableDisable = false;
		}
	};

	$scope.InvoiceMasuk_getDataBranch();
	$scope.TambahInvoiceMasuk_GetDataBranchByOutletId = function () {
		InvoiceMasukFactory.getBranchByOutletId(user.OutletId)
			.then(
				function (res) {
					console.log("res data BranchName : ", res.data.Result[0]);
					$scope.TambahInvoiceMasuk_MainNamaBranch = res.data.Result[0].BranchName;
				}
			);
	}

	// $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs = [
	// 	{ name: "POId", field: "POId", visible: false },
	// 	{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
	// 	{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
	// 	{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
	// 	{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
	// 	{ name: "DPP 111111111111", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false },
	// 	{
	// 		name: "DPP Invoice", field: "DPPInvoice",
	// 		enableCellEdit: true, type: 'number',
	// 		cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
	// 			return 'canEdit';
	// 		}, cellFilter: 'rupiahC'
	// 	},
	// 	{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
	// 	{
	// 		name: "Action",
	// 		cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
	// 	}
	// ];

	$scope.InvoiceMasukPajak_UIGrid_ColumnDefs = [
		{ name: "TaxTypeId", field: "TaxTypeId", visible: false },
		{ name: "Jenis Pajak", field: "TaxType" },
		{ name: "Tarif Pajak", field: "TaxPercent" },
		{ name: "Debit/Kredit", field: "DC" },
		{ name: "Nominal Pajak", field: "TaxNominal", cellFilter: 'rupiahC' },
		{ name: "Nomor Dokumen Pajak", field: "TaxDocNo" },
		{ name: "Tanggal Dokumen Pajak", field: "TaxDocDate", cellFilter: 'nullDateFilter' },
		{ name: "Nomor Referensi", field: "RefNo" },
		{
			name: "Action",
			cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePajak(row)">hapus</a>'
		},
		{ name: "TaxDPP", field: "TaxDPP", visible: false },
	];
	//==========================LIHAT SECTION BEGIN==========================//
	$scope.ShowLihatInvoiceMasukById = function (row) {
		$scope.Show_MainInvoiceMasuk = false;
		$scope.Show_TambahInvoiceMasuk = false;
		$scope.Show_LihatInvoiceMasuk = true;
		$scope.Show_LihatInvoiceMasukSTNK = false;
		$scope.Show_LihatInvoiceMasuk_Detail = true;
		$scope.Show_LihatInvoiceMasuk_RincianPajak = true;
		var InvoiceMasukTypeId = 0;

		$scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nama Pekerjaan", field: "OPLWorkName", visible: false }, //CR OPL in Finance
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "DPP Invoice", field: "DPPInvoice",aggregationType: uiGridConstants.aggregationTypes.sum,
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'rupiahC'
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
			{ name: "OPLId ", field: "OPLId", visible: false} //CR OPL in Finance
		];

		//$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs;
		$scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR1", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "DPP Invoice", field: "DPPInvoice",
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'rupiahC',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false }
		];

		// cr5p #37 Start
		$scope.InvoiceMasukPODitagihAksesorisStd_UIGrid_ColumnDefs = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Pekerjaan", field: "AccessoriesName", enableCellEdit: false}, //CR OPL in Finance
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, visible: true },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false,
				footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
			},
			{
				name: "DPP Invoice", field: "DPPInvoice",
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'rupiahC',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number:2}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
			{ name: "OPLId ", field: "OPLId", visible: false},//CR OPL in Finance
			{ name: "Kode Pekerjaan ", field: "AccessoriesCode", visible: false},
			{ name: "AccessoriesId ", field: "AccessoriesId", visible: false}
		];// cr5p #37 End

		$scope.LihatInvoiceMasuk_TipeInvoiceMasuk = row.entity.InvoiceTypeName;

		if ($scope.LihatInvoiceMasuk_TipeInvoiceMasuk == 'STNK/BPKB')
		{
			$scope.Show_LihatInvoiceMasukSTNK = true;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb;
		}
		else if ($scope.LihatInvoiceMasuk_TipeInvoiceMasuk == 'Aksesoris Standard') {
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagihAksesorisStd_UIGrid_ColumnDefs;
		}else
		{
			$scope.Show_LihatInvoiceMasukSTNK = false;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs;
		}

		$scope.InvoiceMasukPajak_UIGrid_ColumnDefs = [
			{ name: "TaxTypeId", field: "TaxTypeId", visible: false },
			{ name: "Jenis Pajak", field: "TaxType" },
			{ name: "Tarif Pajak", field: "TaxPercent" },
			{ name: "Debit/Kredit", field: "DC" },
			{ name: "Nominal Pajak", field: "TaxNominal", cellFilter: 'rupiahC' },
			{ name: "Nomor Dokumen Pajak", field: "TaxDocNo" },
			{ name: "Tanggal Dokumen Pajak", field: "TaxDocDate", cellFilter: 'nullDateFilter' },
			{ name: "Nomor Referensi", field: "RefNo" },
			{ name: "TaxDPP", field: "TaxDPP", visible: false },
		];

		$scope.InvoiceMasukPajak_UIGrid.columnDefs = $scope.InvoiceMasukPajak_UIGrid_ColumnDefs;
		$scope.LihatInvoiceMasuk_TipeNotice = row.entity.Notice;
		$scope.LihatInvoiceMasuk_InvoiceId = row.entity.InvoiceId;
		$scope.LihatInvoiceMasuk_InvoiceTypeId = row.entity.InvoiceTypeId;
		$scope.LihatInvoiceMasuk_TanggalInvoiceMasuk = row.entity.InvoiceDate;
		$scope.LihatInvoiceMasuk_NomorIM = row.entity.InvoiceNo;
		
		$scope.LihatInvoiceMasuk_NamaBranch = row.entity.OutletName;

		$scope.InvoiceMasuk_JenisInvoiceMasuk.some(function (obj, i) {
			if (obj.value.InvoiceType == $scope.LihatInvoiceMasuk_TipeInvoiceMasuk)
				InvoiceMasukTypeId = obj.value.InvoiceId;
		});

		if ($scope.LihatInvoiceMasuk_TipeInvoiceMasuk != "Accrued Free Service") {
			$scope.Show_LihatInvoiceMasuk_OtherType = true;
			$scope.Show_LihatInvoiceMasuk_PORef = true;
			$scope.Show_LihatInvoiceMasuk_Rangka = false;
			$scope.Show_LihatInvoiceMasuk_FreeService = false;

			InvoiceMasukFactory.getVendorById($scope.LihatInvoiceMasuk_InvoiceId, $scope.LihatInvoiceMasuk_InvoiceTypeId)
				.then(
					function (res) {
						$scope.LihatInvoiceMasuk_VendorId = res.data.Result[0].VendorId;
						$scope.LihatInvoiceMasuk_NamaVendor = res.data.Result[0].VendorName;
						$scope.LihatInvoiceMasuk_NomorVendor = res.data.Result[0].VendorNo;
						$scope.LihatInvoiceMasuk_Alamat = res.data.Result[0].Address;
						$scope.LihatInvoiceMasuk_KabupatenKota = res.data.Result[0].KabKota;
						$scope.LihatInvoiceMasuk_Telepon = res.data.Result[0].Phone;
						$scope.LihatInvoiceMasuk_NPWP = res.data.Result[0].NPWP;
					}
				);

			InvoiceMasukFactory.getInvoiceMasukPOResult(row.entity.InvoiceId, InvoiceMasukTypeId)
				.then(
					function (res) {
						$scope.InvoiceMasukPODitagih_UIGrid.data = res.data.Result;
						
						$scope.TotalDPPAll = 0;
						$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
							console.log("isi row",row);
							if ( row.DPPInvoice != undefined ){
								$scope.TotalDPPAll += row.DPPInvoice ;
							}
							console.log("Cek Kalkulasi",$scope.TotalDPPAll);
							row.Selisih = row.DPP - row.DPPInvoice;
							console.log("Cek Selisih", row.Selisih)
							// Cr5p 37 Start
							if($scope.LihatInvoiceMasuk_TipeInvoiceMasuk == "Aksesoris Standard"){
								row.Selisih = row.Selisih.toFixed(2);
							}// Cr5p 37 End
						});
					}
				);
		}
		else {
			$scope.Show_LihatInvoiceMasuk_OtherType = false;
			$scope.Show_LihatInvoiceMasuk_PORef = false;
			$scope.Show_LihatInvoiceMasuk_Rangka = true;
			$scope.Show_LihatInvoiceMasuk_FreeService = true;
			InvoiceMasukFactory.getFreeServiceBranchData(row.entity.InvoiceId)
				.then(
					function (res) {
						console.log("Data Vendor ==> ", res);
						$scope.LihatInvoiceMasuk_FreeServiceBranch = res.data.Result[0].BranchName;
						$scope.LihatInvoiceMasuk_FreeServiceNPWP = res.data.Result[0].BranchNPWP;
						$scope.LihatInvoiceMasuk_FreeServiceAlamat = res.data.Result[0].BranchAddress;
						$scope.LihatInvoiceMasuk_FreeServiceKabupatenKota = res.data.Result[0].BranchCity;
						$scope.LihatInvoiceMasuk_FreeServiceTelepon = res.data.Result[0].BranchPhone;
					}
				);

			InvoiceMasukFactory.getInvoiceMasukPOResult(row.entity.InvoiceId, InvoiceMasukTypeId)
				.then(
					function (res) {
						console.log("daftar rangka : ", res.data.Result);
						$scope.InvoiceMasukRangka_UIGrid_View.data = res.data.Result;
					}
				);
		}

		InvoiceMasukFactory.getInvoiceMasukTaxResult(row.entity.InvoiceId)
			.then(
				function (res) {
					console.log("res", res);
					console.log("Result", res.data.Result);
					$scope.InvoiceMasukPajak_UIGrid.data = res.data.Result;
				}
			);
		$scope.LihatInvoiceMasuk_NomorInvoiceVendor = row.entity.InvoiceVendorNo;
		$scope.LihatInvoiceMasuk_TanggalJatuhTempo = row.entity.MaturityDate;
		$scope.LihatInvoiceMasuk_Keterangan = row.entity.Description;
		$scope.LihatInvoiceMasuk_Nominal = addSeparatorsNF(row.entity.Nominal, '.', '.', ',');

		//CR OPL in Finance Start
		if($scope.LihatInvoiceMasuk_TipeInvoiceMasuk == 'Order Pekerjaan Luar'){
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[5].visible = true;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[3].visible = false;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[4].visible = false;
		}else if($scope.LihatInvoiceMasuk_TipeInvoiceMasuk == 'Aksesoris Standard'){
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[5].visible = true;
		}else{
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[5].visible = false;
		}//CR OPL in Finance End
	}

	$scope.LihatInvoiceMasuk_Kembali_Clicked = function () {
		$scope.Show_MainInvoiceMasuk = true;
		$scope.Show_TambahInvoiceMasuk = false;
		$scope.Show_LihatInvoiceMasuk = false;
		$scope.Show_LihatInvoiceMasukSTNK = false;
		$scope.InvoiceMasukClearUIGridData();
		$scope.TambahInvoiceMasuk_ClearFields();
	}

	$scope.LihatInvoiceMasuk_Pembatalan_Clicked = function () {
		angular.element('#ModalHapusInvoiceMasuk').modal('show');
	}

	$scope.Batal_ModalHapusInvoiceMasuk = function () {
		angular.element('#ModalHapusInvoiceMasuk').modal('hide');
	};

	$scope.TambahInvoiceMasuk_Batal_Clicked = function () {
		$scope.TambahInvoiceMasuk_ClearFields();
		$scope.TambahInvoiceMasuk_TipeInvoiceMasuk = "";
		$scope.Show_MainInvoiceMasuk = true;
		$scope.Show_LihatInvoiceMasuk = false;
		$scope.Show_LihatInvoiceMasukSTNK = false;
		$scope.Show_TambahInvoiceMasuk = false;
		$scope.Show_TambahInvoiceMasuk_Vendor_Detail = false;
		$scope.Show_TambahInvoiceMasuk_Pembayaran_1 = false;
		$scope.Show_TambahInvoiceMasuk_Pembayaran_2 = false;
	}

	$scope.Hapus_ModalHapusInvoiceMasuk = function () {
		InvoiceMasukFactory.deleteDataInvoiceMasuk($scope.LihatInvoiceMasuk_InvoiceId)
			.then(
				function (res) {
					$scope.Batal_ModalHapusInvoiceMasuk();
					//alert("Invoice berhasil dihapus");
					bsNotify.show({
						title: "Berhasil",
						content: "Invoice berhasil dibatalkan.",
						type: "success"
					});
					$scope.InvoiceMasukList_UIGrid_Paging(1, uiGridPageSize);
					$scope.LihatInvoiceMasuk_Kembali_Clicked();
				},
				function (err) {
					console.log("Error : ", err);
					//alert(err.data.Message);
					bsNotify.show({
						title: "Warning",
						content: err.data.Message,
						type: "warning"
					});
					$scope.Batal_ModalHapusInvoiceMasuk();
				}
			);


	}
	//==========================LIHAT SECTION END==========================//

	//===================================Filter section begin===================================//
	$scope.optionsMainInvoiceMasuk_SelectKolom = [{ name: "Nomor Invoice Masuk", value: "Nomor Invoice Masuk" }, { name: "Tanggal Jatuh Tempo", value: "Tanggal Jatuh Tempo" }, { name: "Nama Vendor", value: "Nama Vendor" }, { name: "Nominal Invoice", value: "Nominal Invoice" }];

	$scope.MainInvoiceMasuk_FilterKolom_Changed = function () {
		$scope.MainInvoiceMasuk_TextFilter = "";
	}

	$scope.MainInvoiceMasuk_Filter_Clicked = function () {
		$scope.InvoiceMasukList_UIGrid_Paging(1, uiGridPageSize);

		// var nomor;
		// $scope.GridApiInvoiceMasukList.grid.clearAllFilters();
		// $scope.GridApiInvoiceMasukList.grid.refresh();

		// if ($scope.MainInvoiceMasuk_SelectKolom == 'Nomor Invoice Masuk')
		// nomor = 2;
		// else if ($scope.MainInvoiceMasuk_SelectKolom == 'Tanggal Jatuh Tempo')
		// nomor = 4;
		// else if ($scope.MainInvoiceMasuk_SelectKolom == 'Nama Vendor')
		// nomor = 5
		// else if ($scope.MainInvoiceMasuk_SelectKolom == 'Nominal Invoice')
		// nomor = 6
		// else
		// nomor = 0

		// console.log("nomor dan filter", nomor, $scope.MainInvoiceMasuk_TextFilter);
		// if(nomor != 0 && $scope.MainInvoiceMasuk_TextFilter != "")
		// {
		// console.log("masuk if");
		// $scope.GridApiInvoiceMasukList.grid.columns[nomor].filters[0].term = $scope.MainInvoiceMasuk_TextFilter;
		// $scope.GridApiInvoiceMasukList.grid.refresh();
		// }

	}

	//aaaaa==============================
	
	 	// $scope.TambahInvoiceMasuk_POFilter_Clicked1 = function () {
			
		// 	if (($scope.TambahInvoiceMasuk_POTextFilter != "") &&
		// 		($scope.TambahInstruksiPembayaran_TujuanPembayaran_DaftarTagihan_Search != "")) {
		// 		var nomor;
		// 		var myDate;
		// 		var value = $scope.TambahInvoiceMasuk_POTextFilter;
		// 		$scope.Main_Daftar_gridAPI.grid.clearAllFilters();

		// 		if ($scope.Main_SearchDropdown == "TanggalBankStatement") {
		// 			myDate = new Date(value);
		// 			if (isNaN(myDate)) {
		// 				myDate = new Date(value.substr(6, 4) + "-" + value.substr(3, 2) + "-" + value.substr(0, 2));
		// 			}
		// 			value = $filter('date')(new Date(myDate.toLocaleString()), 'MMM dd yyyy')
		// 		}

		// 		console.log("value : ", value);
		// 		// if ($scope.Main_SearchDropdown == "Nomor Instruksi Bank") {
		// 		// 	nomor = 3;
		// 		// }				
		// 		// if ($scope.Main_SearchDropdown == "Nomor Rekening Bank") {
		// 		// 	nomor =4;
		// 		// }
		// 		// if ($scope.Main_SearchDropdown == "Nomor Rekening Bank") {
		// 		// 	nomor =5;
		// 		// }					
		// 		console.log("kolom : ", $scope.Main_Daftar_gridAPI.grid.getColumn($scope.Main_SearchDropdown));
		// 		$scope.Main_Daftar_gridAPI.grid.getColumn($scope.Main_SearchDropdown).filters[0].term = value;
		// 	}
		// 	else {
		// 		$scope.Main_Daftar_gridAPI.grid.clearAllFilters();
		// 		//alert("inputan filter belum diisi !");
		// 	}
		//  }


	//=================================== 


	// filter baru =================
	 	$scope.TambahInvoiceMasuk_POFilter_Clicked1 = function () {
			 var value;
			 var myDate;
			 var tmpFlag = 0;
			 
			 if (($scope.TambahInvoiceMasuk_POTextFilter != "") &&
				($scope.TambahInvoiceMasuk_POSelectKolom != null && $scope.TambahInvoiceMasuk_POSelectKolom != undefined)) {
				value = $scope.TambahInvoiceMasuk_POTextFilter;
				console.log("Input Text Filter ==>>", $scope.TambahInvoiceMasuk_POTextFilter );
				console.log("ComboBox Filter ==>>", $scope.TambahInvoiceMasuk_POSelectKolom );
				// if ($scope.TambahInvoiceMasuk_POSelectKolom == "Tanggal PO") {
				// 	myDate = new Date(value);
				// 	if (isNaN(myDate)) {
				// 		myDate = new Date(value.substr(6, 4) + "-" + value.substr(3, 2) + "-" + value.substr(0, 2));
				// 	}
				// 	value = $filter('date')(new Date(myDate), 'dd MMM yyyy')
				// 	// value = $filter('date')(new Date(myDate), 'yyyy-MM-dd')
				// 	console.log("isi value : ", value);

				// }
				if( $scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal PO'|| $scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal GR'){
					tmpFlag=1;
				}else{
					tmpFlag=0;
				}
				$scope.tmptotalData;
				InvoiceMasukFactory.getDataPOByVendorName(1, uiGridPageSize,
				$scope.TambahInvoiceMasuk_NamaVendor, $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
				$scope.TambahInvoiceMasuk_SelectBranch , $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice, $scope.TambahInvoiceMasuk_POTextFilter, $scope.TambahInvoiceMasuk_POSelectKolom,tmpFlag,$scope.IsNonPKP)
				.then(
					function (res) {
						console.log("res =>", res.data.Result);
						$scope.InvoiceMasukTambah_POList = res.data.Result;
						for(var i in res.data.Result){
							res.data.Result[i].TanggalPO = $scope.changeFormatDate(res.data.Result[i].TanggalPO);
						}
						$scope.InvoiceMasukPO_UIGrid.data = $scope.InvoiceMasukTambah_POList;
						$scope.InvoiceMasukPOGridApi.grid.clearAllFilters();
						$scope.InvoiceMasukPOGridApi.grid.getColumn($scope.TambahInvoiceMasuk_POSelectKolom).filters[0].term = value;

						$timeout(function(){
							console.log("data", $scope.InvoiceMasukPO_UIGrid.data);
							console.log("data", $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length);
							
							// $scope.InvoiceMasukPO_UIGrid.data = $scope.InvoiceMasukTambah_POList;
							$scope.InvoiceMasukPO_UIGrid.totalItems = res.data.Total;	
							$scope.InvoiceMasukPO_UIGrid.paginationPageSize = 250;
							$scope.InvoiceMasukPO_UIGrid.paginationPageSizes = [250, 500, 1000];
		
							// $scope.InvoiceMasukPOGridApi.grid.totalItems = $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length;	
						},100);	
					}
	
				);

				// $scope.InvoiceMasukPOGridApi.grid.clearAllFilters();
				// $scope.InvoiceMasukPOGridApi.grid.getColumn($scope.TambahInvoiceMasuk_POSelectKolom).filters[0].term = value;
				// $timeout(function(){
				// 	console.log("data", $scope.InvoiceMasukPO_UIGrid.data);
				// 	console.log("data", $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length);
					
				// 	// $scope.InvoiceMasukPO_UIGrid.data = $scope.InvoiceMasukTambah_POList;
				// 	$scope.InvoiceMasukPO_UIGrid.totalItems = $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length;	
				// 	$scope.InvoiceMasukPO_UIGrid.paginationPageSize = $scope.uiGridPageSize;
				// 	$scope.InvoiceMasukPO_UIGrid.paginationPageSizes = [$scope.uiGridPageSize];

				// 	// $scope.InvoiceMasukPOGridApi.grid.totalItems = $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length;	
				// },100);	
			}
			else {
				$scope.InvoiceMasukPOGridApi.grid.clearAllFilters();
				$timeout(function(){
					
					// $scope.InvoiceMasukPO_UIGrid.data = $scope.InvoiceMasukTambah_POList;
					$scope.InvoiceMasukPO_UIGrid.totalItems = $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length;	
					$scope.InvoiceMasukPO_UIGrid.paginationPageSize = 250;
					$scope.InvoiceMasukPO_UIGrid.paginationPageSizes = [250, 500, 1000];

					// $scope.InvoiceMasukPOGridApi.grid.totalItems = $scope.InvoiceMasukPOGridApi.core.getVisibleRows().length;	
				},100);
			$scope.InvoiceMasukPO_UIGrid_Paging(1, $scope.uiGridPageSize);

				//alert("inputan filter belum diisi !");
			}
			// $scope.InvoiceMasukPO_UIGrid_Paging(1);
			
		 }
		 
		 $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
                var yyyy = tmpAppointmentDate.getFullYear().toString();
                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = tmpAppointmentDate.getDate().toString();
                finalDate = (dd[1] ? dd : "0" + dd[0])+ '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + yyyy;
            } else {
                finalDate = '';
            }
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };
        
	//==============================

	// $scope.TambahInvoiceMasuk_POFilter_Clicked = function () {
	// 	$scope.InvoiceMasukPOGridApi.grid.clearAllFilters();

	// 	if ($scope.TambahInvoiceMasuk_POSelectKolom == 'Nomor PO')
	// 		nomor = 2;
	// 	else if ($scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal PO')
	// 		nomor = 3;
	// 	else if ($scope.TambahInvoiceMasuk_POSelectKolom == 'Nomor GR')
	// 		nomor = 4
	// 	else if ($scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal GR')
	// 		nomor = 5
	// 	else
	// 		nomor = 0

	// 	if (!angular.isUndefined($scope.TambahInvoiceMasuk_POSelectKolom) && nomor != 0) {
		
	// 		var dataFilter = $scope.TambahInvoiceMasuk_POTextFilter.split('');
	// 		$scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.listTerm= dataFilter;
	// 		$scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.term= $scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.listTerm.join(',');
	// 		$scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.condition= new RegExp($scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.listTerm.join('|'));
	// 		$scope.InvoiceMasukPOGridApi.grid.columns[nomor].filter.forEach( function( dat ) {
	// 			var entities = $scope.InvoiceMasukPO_UIGrid.data.filter( function( row ) {
	// 			  return row.NomorPO === dat;
	// 			}); 
				
	// 			if( entities.length > 0 ) {
	// 			  $scope.InvoiceMasukPOGridApi.selection.selectRow(entities[0]);
	// 			}
	// 		  });
	// 		//$scope.InvoiceMasukPOGridApi.grid.columns[nomor].filters[1].term='214/NRG/1810-000003' //$scope.TambahInvoiceMasuk_POTextFilter;
			
	// 	}
	// 	$scope.InvoiceMasukPOGridApi.grid.queueGridRefresh();
	// }
	//===================================Filter section end===================================//

	$scope.TambahInvoiceMasuk_Clicked = function () {
		$scope.Loading = false;
		$scope.NominalInvoice_Disable = false;
		$scope.tmpDataOpl = []; //CR OPL in FInance
		// $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs = [
		// 	{ name: "POId", field: "POId", visible: false },///>>>>>>>>>>>
		// 	{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
		// 	{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
		// 	{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
		// 	{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
		// 	{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false,
		// 	  footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
		// 	},
		// 	{
		// 		name: "DPP Invoice1", displayName:'DPP Invoice', field: "DPPInvoice",  aggregationType: uiGridConstants.aggregationTypes.sum,
		// 		cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
		// 			return 'canEdit';
		// 		}, 
		// 		enableCellEdit: true, type: 'number',cellFilter: 'currency:"":0',
		// 	},
		// 	{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
		// 	{
		// 		name: "Action",
		// 		cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
		// 	}
		// ];

		$scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "DPP Invoice", field: "DPPInvoice",
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'rupiahC',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
			}
		];

		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk == 'STNK/BPKB')
		{
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb;
			$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];

		}
		else
		{
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs;
			$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];

		}
		

		$scope.InvoiceMasukPajak_UIGrid_ColumnDefs = [
			{ name: "TaxTypeId", field: "TaxTypeId", visible: false },
			{ name: "Jenis Pajak", field: "TaxType" },
			{ name: "Tarif Pajak", field: "TaxPercent" },
			{ name: "Debit/Kredit", field: "DC" },
			{ name: "Nominal Pajak", field: "TaxNominal", cellFilter: 'rupiahC' },
			{ name: "Nomor Dokumen Pajak", field: "TaxDocNo" },
			{ name: "Tanggal Dokumen Pajak", field: "TaxDocDate", cellFilter: 'nullDateFilter' },
			{ name: "Nomor Referensi", field: "RefNo" },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePajak(row)">hapus</a>'
			},
			{ name: "TaxDPP", field: "TaxDPP", visible: false },
		];

		$scope.InvoiceMasukPajak_UIGrid.columnDefs = $scope.InvoiceMasukPajak_UIGrid_ColumnDefs;

		$scope.Show_MainInvoiceMasuk = false;
		$scope.Show_TambahInvoiceMasuk = true;
		$scope.Show_LihatInvoiceMasuk = false;
		$scope.Show_LihatInvoiceMasukSTNK = false;
		$scope.Show_TambahInvoiceMasuk_OtherType = false;
		$scope.Show_TambahInvoiceMasuk_FreeService = false;
		$scope.Show_TambahInvoiceMasuk_PORef = false;
		$scope.Show_TambahInvoiceMasuk_RincianPajak = false;
		$scope.Show_TambahInvoiceMasuk_Rangka = false;
		$scope.Show_TambahInvoiceMasuk_Detail = false;
		$scope.TambahInvoiceMasuk_TanggalInvoiceMasuk = new Date().toLocaleString();
		$scope.x1.$setPristine();
		$scope.x1.$setUntouched();

		//$scope.Show_TambahInvoiceMasuk_FreeService = false;
		//$scope.InitiateComboBoxJenisPajak();
		//$scope.InitiateComboBoxJenisInvoiceMasuk();
		$scope.TambahInvoiceMasuk_GetDataBranchByOutletId();
	}
	$scope.right = function (str, chr) {
		return str.slice(str.length - chr, str.length);
	}
	$scope.InitiateComboBoxJenisInvoiceMasuk = function () {
		var jenisInvoiceMasuk = [];
		InvoiceMasukFactory.getInvoiceMasukType()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {

						if ($scope.right($scope.user.OrgCode, 6) != '000000') {
							if (value.Label.toLowerCase() != 'unit') {
								jenisInvoiceMasuk.push({ "name": value.Label, "value": { "InvoiceType": value.Label, "InvoiceId": value.Value } });
							}

						}
						else {
							jenisInvoiceMasuk.push({ "name": value.Label, "value": { "InvoiceType": value.Label, "InvoiceId": value.Value } });
						}

					});
					console.log("Jenis Invoice Masuk ", jenisInvoiceMasuk);
					$scope.InvoiceMasuk_JenisInvoiceMasuk = jenisInvoiceMasuk;
					$scope.optionsTambahInvoiceMasukTipeInvoiceMasuk = jenisInvoiceMasuk;
				}
			);
	}

	$scope.InitiateComboBoxJenisInvoiceMasuk();

	$scope.TambahInvoiceMasuk_TipeInvoiceMasuk_Changed = function () {
		if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
			$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
		if (angular.isUndefined($scope.TambahInvoiceMasuk_TipeInvoiceMasuk)) {
			return;
		}

		$scope.Show_TambahInvoiceMasuk_RincianPajak = true;
		$scope.Show_TambahInvoiceMasuk_Detail = true;
		$scope.NominalInvoice_Disable = false;

		$scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice = "";
		$scope.TambahInvoiceMasuk_NomorInvoiceVendor = "";
		$scope.TambahInvoiceMasuk_VendorId = 0;
		$scope.TambahInvoiceMasuk_Nominal = "";
		$scope.TambahInvoiceMasuk_TanggalJatuhTempo = "";
		$scope.TambahInvoiceMasuk_Keterangan = "";
		$scope.TambahInvoiceMasuk_NomorVendor = "";
		$scope.TambahInvoiceMasuk_NamaVendor = "";
		$scope.TambahInvoiceMasuk_NPWP = "";
		$scope.TambahInvoiceMasuk_Alamat = "";
		$scope.TambahInvoiceMasuk_KabupatenKota = "";
		$scope.TambahInvoiceMasuk_Telepon = "";
		$scope.TaxPercent = "";
		$scope.invoiceMasukData = [];
		$scope.InvoiceMasukClearUIGridData();
		$scope.TambahInvoiceMasuk_OtherTypeNamaVendor = '';
		$scope.InvoiceMasukPO_UIGrid.data = [];
		$scope.TambahInvoiceMasuk_ClearFieldsTax();

		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType != "Unit"){
			$scope.NominalInvoice_Disable = true;
		}

		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType != "Accrued Free Service") {
			$scope.Show_TambahInvoiceMasuk_OtherType = true;
			$scope.Show_TambahInvoiceMasuk_FreeService = false;
			$scope.Show_TambahInvoiceMasuk_PORef = true;
			$scope.Show_TambahInvoiceMasuk_Rangka = false;
			//$scope.Show_TambahInvoiceMasuk_FreeService = false;
		}
		else {
			$scope.Show_TambahInvoiceMasuk_PORef = false;
			$scope.Show_TambahInvoiceMasuk_Rangka = true;
			if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Accrued Free Service"){
				$scope.Show_TambahInvoiceMasuk_OtherType = true;
			}else{
				$scope.Show_TambahInvoiceMasuk_OtherType = false;
			}
			$scope.Show_TambahInvoiceMasuk_FreeService = true;
			$scope.getDataBranchCB();
			//$scope.Show_TambahInvoiceMasuk_FreeService = true;
		}
		var taxType = [];
		$scope.InitiateComboBoxJenisPajak();

		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "STNK/BPKB") {
			$scope.Show_TambahTipeNotice = true;
			
				$scope.InvoiceMasukPO_UIGrid.columnDefs = [
					{ name: "POId", field: "POId", visible: false },
					{ name: "Nomor PO", field: "NomorPO",filter: {
						type: uiGridConstants.filter.SELECT} },
					{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
					{ name: "Nomor Rangka1", field: "FrameNo" },
					{ name: "Nomor Rangka", field: "FrameNo" },
					{ name: "Nomor GR", field: "NomorGR" },
					{ name: "Tanggal GR", field: "TanggalGR" },
					{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC' }
				];
				$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];

			//$scope.Show_TambahInvoiceMasuk_FreeService = false;
		}else if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Aksesoris Standard"){
			$scope.Show_TambahTipeNotice =false;
			$scope.InvoiceMasukPO_UIGrid.columnDefs = [
				{ name: "POId", field: "POId", visible: false },
				{ name: "Nomor PO", field: "NomorPO",filter: {
					type: uiGridConstants.filter.SELECT} },
				{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "Pekerjaan", field: "AccessoriesName", visible: true }, //CR5P 37
				{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false},
				{ name: "Nomor GR", field: "NomorGR" },
				{ name: "Tanggal GR", field: "TanggalGR" },
				{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC' },
				{ name: "Status Upload", field: "Status Upload", visible: false},
				{ name: "Kode Pekerjaan", field: "AccessoriesCode", visible: false},
				{ name: "AccessoriesId ", field: "AccessoriesId", visible: false} //CR5P 37
			];
		}
		else
		{
			$scope.Show_TambahTipeNotice =false;
			$scope.InvoiceMasukPO_UIGrid.columnDefs = [
				{ name: "POId", field: "POId", visible: false },
				{ name: "Nomor PO", field: "NomorPO",filter: {
					type: uiGridConstants.filter.SELECT} },
				{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
				{ name: "Nama Pekerjaan", field: "OPLWorkName", visible: false }, //CR OPL in Finance
				{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, visible: false },
				{ name: "Nomor GR", field: "NomorGR" },
				{ name: "Tanggal GR", field: "TanggalGR" },
				{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC' },
				{ name: "OPLId ", field: "OPLId", visible: false} //CR OPL in Finance
			];
			if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris Standard'){
				$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" },{ name: "Pekerjaan", value: "AccessoriesName" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];
			}else{
				$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];

			}

		}

		$scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nama Pekerjaan", field: "OPLWorkName", visible: false, enableCellEdit: false}, //CR OPL in Finance
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, visible: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false,
			  footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
			},
			{
				name: "DPPInvoice",displayName:'DPP Invoice', aggregationType: uiGridConstants.aggregationTypes.sum,
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'currency:"":2',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number:2}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'currency:"":2', enableCellEdit: false },
			{ name: "OPLId ", field: "OPLId", visible: false}, //CR OPL in Finance
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
			}
		];

		$scope.InvoiceMasukPODitagihAksesorisStd_UIGrid_ColumnDefs = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Pekerjaan", field: "AccessoriesName", enableCellEdit: false}, //CR OPL in Finance
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false, visible: true },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false,
			  footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
			},
			{
				name: "DPPInvoice",displayName:'DPP Invoice', aggregationType: uiGridConstants.aggregationTypes.sum,
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'currency:"":2',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number:2}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'currency:"":2', enableCellEdit: false },
			{ name: "OPLId ", field: "OPLId", visible: false},//CR OPL in Finance
			{ name: "Kode Pekerjaan ", field: "AccessoriesCode", visible: false},
			{ name: "AccessoriesId ", field: "AccessoriesId", visible: false}, 
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
			}
		];

		$scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb = [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO", enableCellEdit: false },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "Nomor Rangka1", field: "FrameNo", enableCellEdit: false },
			{ name: "Nomor Rangka", field: "FrameNo" },
			{ name: "Nomor GR", field: "NomorGR", enableCellEdit: false },
			{ name: "Tanggal GR", field: "TanggalGR", cellFilter: 'date:\"dd-MM-yyyy\"', enableCellEdit: false },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "DPP Invoice", field: "DPPInvoice",
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}, cellFilter: 'rupiahC',
				footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalDPPAll | number}}</div>',
			},
			{ name: "Selisih", field: "Selisih", cellFilter: 'rupiahC', enableCellEdit: false },
			{
				name: "Action",
				cellTemplate: '<a ng-click="grid.appScope.TambahInvoiceMasuk_DeletePODitagih(row)">hapus</a>'
			}
		];

		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'STNK/BPKB')
		{
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefsStnkBpkb;
			$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];

		}
		else
		{
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs;
			if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris Standard'){
				$scope.InvoiceMasukPODitagih_UIGrid.columnDefs = $scope.InvoiceMasukPODitagihAksesorisStd_UIGrid_ColumnDefs;
				$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" },{ name: "Pekerjaan", value: "AccessoriesName" }, { name: "Nomor Rangka", value: "Nomor Rangka" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];
			}else{
				$scope.optionsTambahInvoiceMasuk_POSelectKolom = [{ name: "Nomor PO", value: "Nomor PO" }, { name: "Tanggal PO", value: "Tanggal PO" }, { name: "Nomor GR", value: "Nomor GR" }, { name: "Tanggal GR", value: "Tanggal GR" }];
			}
		}
		//CR OPL in Finance Start
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar'){
			$scope.InvoiceMasukPO_UIGrid.columnDefs[3].visible = true;
			$scope.InvoiceMasukPO_UIGrid.columnDefs[5].visible = false;
			$scope.InvoiceMasukPO_UIGrid.columnDefs[6].visible = false;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[3].visible = true;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[4].visible = false;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[6].visible = false;
		}else if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris Standard'){
			if($scope.modeUploadAksesorisStandard == 1){
				$scope.InvoiceMasukPO_UIGrid.columnDefs[8].visible = true;
			}else{
				$scope.InvoiceMasukPO_UIGrid.columnDefs[8].visible = false;
			}
		}else{
			$scope.InvoiceMasukPO_UIGrid.columnDefs[3].visible = false;
			$scope.InvoiceMasukPODitagih_UIGrid.columnDefs[3].visible = false;
		}//CR OPL in Finance End

		//CR5P #10
		$scope.IsNonPKP = 0;
		// CR5 #38 Finance Start
		$scope.DaftraNomorRangka = [];
		// CR5 #38 Finance End
	}

	$scope.getDataBranchCB = function () {
		InvoiceMasukFactory.getDataAllBranchGroupComboBox()
			.then(
				function (res) {
					$scope.loading = false;
					$scope.optionsTambahInvoiceMasuk_NamaBranchGroup_Search = res.data;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
	}

	$scope.TambahInvoiceMasuk_FreeServiceBranch_Selected_Changed = function () {
		console.log($scope.TambahInvoiceMasuk_FreeServiceBranch);
		if ($scope.TambahInvoiceMasuk_FreeServiceBranch !== "") {
			InvoiceMasukFactory.getDataBranch($scope.TambahInvoiceMasuk_FreeServiceBranch)
				.then(
					function (res) {
						console.log("GetDataBranch")
						$scope.TambahInvoiceMasuk_FreeServiceBranchId = res.data.Result[0].OutletId;
						$scope.TambahInvoiceMasuk_FreeServiceNPWP = res.data.Result[0].NPWP;
						$scope.TambahInvoiceMasuk_FreeServiceAlamat = res.data.Result[0].Alamat;
						//$scope.Show_TambahIncomingPayment_Interbranch_InformasiBranch_KodeBranch = res.data.Result[0].OutletId;
						$scope.TambahInvoiceMasuk_FreeServiceKabupatenKota = res.data.Result[0].Kota;
						$scope.TambahInvoiceMasuk_FreeServiceTelepon = res.data.Result[0].Telepon;
					}
				);
		}
	}
$scope.tmptotalData = 0;
	$scope.MainInvoiceMasuk_Cari_Clicked = function () {
		if($scope.MainInvoiceMasuk_SelectBranch == "" || $scope.MainInvoiceMasuk_SelectBranch == undefined || $scope.MainInvoiceMasuk_SelectBranch == null){
			$scope.MainInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
		$scope.InvoiceMasukList_UIGrid.paginationCurrentPage = 1;
		if ((new Date($scope.MainInvoiceMasuk_TanggalAwal) > new Date()) ||
			(new Date($scope.MainInvoiceMasuk_TanggalAkhir) > new Date())) {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal tidak bisa lebih besar dari hari ini.",
				type: "warning"
			});
		}
		else if (new Date($scope.MainInvoiceMasuk_TanggalAwal) > new Date($scope.MainInvoiceMasuk_TanggalAkhir)) {

			bsNotify.show({
				title: "Warning",
				content: "Tanggal awal tidak boleh lebih dari tanggal akhir.",
				type: "warning"
			});
		}
		else {
			$scope.InvoiceMasukList_UIGrid_Paging(1, uiGridPageSize);
		}

	}

	$scope.TambahInvoiceMasuk_Cari_Clicked = function () {
		if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
			$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
		if ($scope.TambahInvoiceMasuk_OtherTypeNamaVendor == "" && $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId != "OneTimeVendor") {
			//alert("Nama Vendor harus diisi");
			bsNotify.show({
				title: "Warning",
				content: "Nama Vendor harus diisi",
				type: 'warning'
			});
			return;
		}
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'STNK/BPKB')
		{
			if ($scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice == "" || $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice == null) {
				//alert("Nama Vendor harus diisi");
				bsNotify.show({
					title: "Warning",
					content: "Tipe Notice atau Unnotice wajib dipilih.",
					type: 'warning'
				});
				return;
			}
		}

		console.log('Test Nama Vendor');
		console.log($scope.TambahInvoiceMasuk_OtherTypeNamaVendor);
		console.log('Test Disini');
		console.log($scope.TambahInvoiceMasuk_TipeInvoiceMasuk);

		// if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Parts"){
			$scope.tmpReplece = $scope.TambahInvoiceMasuk_OtherTypeNamaVendor.replace(/\&/g, "%26");
			console.log("Cek Replace", $scope.tmpReplece)	
		// }else{
		// 	$scope.tmpReplece = $scope.TambahInvoiceMasuk_OtherTypeNamaVendor
		// }
		
		// InvoiceMasukFactory.getVendorById($scope.TambahInvoiceMasuk_OtherTypeNamaVendor, $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId, $scope.TambahInvoiceMasuk_SelectBranch)
		// 	.then(
		InvoiceMasukFactory.getVendorInfo($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType,
			$scope.tmpReplece, $scope.TambahInvoiceMasuk_OtherTypeNamaVendor_TujuanPembayaran_IdPelangganVendor, $scope.TambahInvoiceMasuk_SelectBranch).then(
				function (res) {
					if (angular.isUndefined(res.data.Result) || res.data.Result.length == 0) {
						//alert("Data vendor tidak ditemukan");
						bsNotify.show({
							title: "Warning",
							content: "Data vendor tidak ditemukan",
							type: 'warning'
						});
					} 
					else {
						console.log("Data Vendor ==> ", res);
						angular.forEach(res.data.Result, function (value, key) {
							if (value.Name == $scope.TambahInvoiceMasuk_OtherTypeNamaVendor) {
								$scope.TambahInvoiceMasuk_VendorId = value.Id;
								$scope.TambahInvoiceMasuk_NomorVendor = value.VendorCode;
								$scope.TambahInvoiceMasuk_NamaVendor = value.Name;
								$scope.TambahInvoiceMasuk_NPWP = value.NPWP;
								$scope.TambahInvoiceMasuk_Alamat = value.Address;
								$scope.TambahInvoiceMasuk_KabupatenKota = value.City;
								$scope.TambahInvoiceMasuk_Telepon = value.Phone;
								if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Accrued Free Service'){ //CR5P #38 Start
									$scope.TambahInvoiceMasuk_FreeServiceBranchId = value.Id;
									$scope.TambahInvoiceMasuk_FreeServiceBranch = value.Id.toString();
									$scope.TambahInvoiceMasuk_FreeServiceNPWP = value.NPWP;
									$scope.TambahInvoiceMasuk_FreeServiceAlamat = value.Address;
									$scope.TambahInvoiceMasuk_FreeServiceKabupatenKota = value.City;
									$scope.TambahInvoiceMasuk_FreeServiceTelepon = value.Phone;
								}//CR5P #38 End
							}
						});


						$scope.InvoiceMasukPO_UIGrid_Paging(1, $scope.uiGridPageSize);

					}
				}
			)
	}

	$scope.TambahInvoiceMasuk_FreeService_Cari_Clicked = function () {
		InvoiceMasukFactory.getBranchData($scope.TambahInvoiceMasuk_FreeServiceNamaBranch)
			.then(
				function (res) {
					console.log("Data Vendor ==> ", res);
					$scope.TambahInvoiceMasuk_FreeServiceBranchId = res.data.Result[0].BranchId;
					$scope.TambahInvoiceMasuk_FreeServiceBranch = res.data.Result[0].BranchName;
					$scope.TambahInvoiceMasuk_FreeServiceNPWP = res.data.Result[0].Npwp;
					$scope.TambahInvoiceMasuk_FreeServiceAlamat = res.data.Result[0].Address;
					$scope.TambahInvoiceMasuk_FreeServiceKabupatenKota = res.data.Result[0].CityRegencyName;
					$scope.TambahInvoiceMasuk_FreeServiceTelepon = res.data.Result[0].PhoneNumber;
				}
			);
	}
	
	$scope.onSelectRows = function(rows){
		console.log("onSelectRows=>", rows);
	}

	// =============================== Auto Kalkulasi Nominal Invoice Start =============================
	
	$scope.CalculationNominalInvoice = function(){
		var TotHitung = 0;
		var TotTaxD = 0;
		var TotTaxK = 0;
		var SisaFloat = "";
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Accrued Free Service"){
			$scope.InvoiceMasukRangka_UIGrid.data.forEach(function (row) {
				console.log("isi row",row);
				if ( row.FreeServiceNominal != undefined ){
					TotHitung += row.FreeServiceNominal ;
				}
				TotHitung = TotHitung.toFixed(2)
				TotHitung = parseFloat(TotHitung);
				console.log("TotalPembayaranAll",TotHitung);
			});
		}else{
			$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
				console.log("isi row",row);
				if ( row.DPPInvoice != undefined ){
					TotHitung += row.DPPInvoice ;
				}
				console.log("TotalPembayaranAll",TotHitung);
			});
		}
		
		for (var j in $scope.InvoiceMasukPajak_UIGrid.data){
			if($scope.InvoiceMasukPajak_UIGrid.data[j].DC == "D"){
				TotTaxD += parseFloat($scope.InvoiceMasukPajak_UIGrid.data[j].TaxNominal);	
			}else{
				TotTaxK += parseFloat($scope.InvoiceMasukPajak_UIGrid.data[j].TaxNominal);	
			}
		}
		var NomInvoiceCopy = angular.copy($scope.TambahInvoiceMasuk_Nominal);
		
		if (NomInvoiceCopy.substr(NomInvoiceCopy.length-3,1) == "."){
			SisaFloat = NomInvoiceCopy.substr(NomInvoiceCopy.length-3);
			NomInvoiceCopy = NomInvoiceCopy.substr(0,NomInvoiceCopy.length-3);
			
		}
		else if (NomInvoiceCopy.substr(NomInvoiceCopy.length-2,1) == "."){
			SisaFloat = NomInvoiceCopy.substr(NomInvoiceCopy.length-2);
			NomInvoiceCopy = NomInvoiceCopy.substr(0,NomInvoiceCopy.length-2);
			
		}
		var val = NomInvoiceCopy.split(".").join("");
		
		val = val.replace(/[a-zA-Z\s]/g, '');
		val = String(val).split("").reverse().join("")
			.replace(/(\d{3}\B)/g, "$1.")
			.split("").reverse().join("");

		if (val == ""){
			val = "0.00";
		}

		if (SisaFloat == "undefined" || !SisaFloat){
			NomInvoiceCopy = val.split(".").join("") +".00";
		}
		else{
			NomInvoiceCopy = val.split(".").join("") + SisaFloat;
		}

		$scope.TambahInvoiceMasuk_Nominal = NomInvoiceCopy ;
		NomInvoiceCopy = NomInvoiceCopy.replace(/\,/g, "");
		
		var	NomInvoiceCopy2 = parseFloat(NomInvoiceCopy);
		var teskal = (TotHitung + TotTaxD - TotTaxK);
		var testofix = teskal.toFixed(2);
		
		console.log("nom copy", NomInvoiceCopy)
		console.log("nom copy2", NomInvoiceCopy2)
		console.log("total kalkulasi 1", teskal)
		console.log("total kalkulasi 3", testofix)
		console.log("total kalkulasi 2",(TotHitung + TotTaxD - TotTaxK))
		$scope.TambahInvoiceMasuk_Nominal = testofix;
		$scope.GetFormatCurrency();
	}

	$scope.GetFormatCurrency = function(){
		$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.toString();
		var SisaFloat= "";
		if ($scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-3,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-3);
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.substr(0,$scope.TambahInvoiceMasuk_Nominal.length-3);
			
		}
		else if ($scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-2,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-2);
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.substr(0,$scope.TambahInvoiceMasuk_Nominal.length-2);
			
		}
		var val = $scope.TambahInvoiceMasuk_Nominal.split(".").join("");
		
		val = val.replace(/[a-zA-Z\s]/g, '');
		val = String(val).split("").reverse().join("")
			.replace(/(\d{3}\B)/g, "$1.")
			.split("").reverse().join("");

		var str_array = val.split('.');

		var NominalValid = true;
		var satu = 0;
		var dua = 0;
		for (var i = 0; i < str_array.length; i++) {
			if (str_array[i].length == 1) {
				satu = satu + 1;
			}
			else if (str_array[i].length == 2) {
				dua = dua + 1;
			}

			if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
				NominalValid = false;
				break;
			}
		}
		if (!NominalValid) {
			$scope.TambahInvoiceMasuk_NominalBlur(val);
		}
		else {
			if(val == ""){
				val = "0.00";
			}

			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_Nominal = val+".00";
				$scope.TambahInvoiceMasuk_NominalforParseFloat = val.split(".").join("")+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_Nominal = val+SisaFloat;
				$scope.TambahInvoiceMasuk_NominalforParseFloat = val.split(".").join("") + SisaFloat;
				console.log("testing",val+SisaFloat);
				console.log("testing",$scope.TambahInvoiceMasuk_NominalforParseFloat);
			}
		}
	}
	// =============================== Auto Kalkulasi Nominal Invoice End =============================

	$scope.TambahInvoiceMasuk_TambahPO_Clicked = function () {
		// var currentRow = $scope.InvoiceMasukPOGridApi.selection.getSelectedRows()[0];
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar'){ //CR OPL in Finance Start
			var currentRow = $scope.tmpDataOpl;
		}else{
			var currentRow = $scope.InvoiceMasukPOGridApi.selection.getSelectedRows();
		} //CR OPL in Finance End

		var isExists = false;
		console.log("current row", currentRow);
		// if ($scope.onSelectRows){
		// 	$scope.onSelectRows(currentRow);
		// }

		// $scope.InvoiceMasukPOGridApi.selection.getSelectedRows().forEach(function (row) {
		// 	$scope.InvoiceMasukPODitagih_UIGridInvoiceMasukPODitagih_UIGrid.data.forEach(function (obj) {
		// 		console.log('isi grid -->', obj.NoWO);
		// 		if (row.NomorPO == obj.NomorPO)
		// 			found = 1;
		// 	});
		// });
		if (!angular.isUndefined(currentRow)) {
			// $scope.InvoiceMasukPODitagih_Data.some(function (obj, i) {
			// 	if (obj.NomorPO == currentRow.NomorPO && obj.FrameNo == currentRow.FrameNo) {
			// 		isExists = true;
			// 	}
			// });
			for (var l in currentRow){
				$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (obj) {
					if (obj.NomorPO == currentRow[l].NomorPO && obj.FrameNo == currentRow[l].FrameNo) {
						console.log("obj NomorPO", obj.NomorPO)
						isExists = true;
					}
				});
			}
			

			if (!isExists) {
				// currentRow['DPPInvoice'] = currentRow['DPP'];
				// currentRow['DPPInvoice'] = 0;
				// currentRow['Selisih'] = 0;
				for(var j in currentRow){
					// $scope.InvoiceMasukPODitagih_Data.push(currentRow);//ccccccc
					$scope.InvoiceMasukPODitagih_UIGrid.data.push({
						POId: currentRow[j].POId,
						NomorPO: currentRow[j].NomorPO,
						TanggalPO: currentRow[j].TanggalPO,
						NomorGR: currentRow[j].NomorGR,
						FrameNo: currentRow[j].FrameNo,
						TanggalGR: currentRow[j].TanggalGR,
						DPP: currentRow[j].DPP,
						DPPInvoice: currentRow[j].DPP,
						Selisih: currentRow[j].Selisih,
						OPLWorkName: currentRow[j].OPLWorkName, //CR OPL in Finance
						OPLId: currentRow[j].OPLId, //CR OPL in Finance
						AccessoriesName: currentRow[j].AccessoriesName, //CR5P 37
						AccessoriesCode: currentRow[j].AccessoriesCode, //CR5P 37
						AccessoriesId: currentRow[j].AccessoriesId //CR5P 37
					});//ccccccc
				}
				
				$scope.TotalDPPAll = 0;
				$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
						console.log("isi row",row);
						if ( row.DPPInvoice != undefined ){
							$scope.TotalDPPAll += row.DPPInvoice ;
						}
						console.log("Cek Kalkulasi",$scope.TotalDPPAll);
						row.Selisih = row.DPP - row.DPPInvoice;
						console.log("Cek Selisih", row.Selisih)
				});
				
			}
			else {
				//alert("Nomor PO sudah dipilih.");
				bsNotify.show({
					title: "Warning",
					content: "Nomor PO sudah dipilih.",
					type: "warning"
				});
			}
		}
		else {
			//alert("Harus ada data yang dipilih !");
			bsNotify.show({
				title: "Warning",
				content: "Harus ada data yang dipilih !",
				type: "warning"
			});
			return;
		}

		$scope.autoCalculationDpp();
		$scope.CalculationNominalInvoice();
	}


	$scope.TambahInvoiceMasuk_DeletePODitagih = function (row) {

		if($scope.isModeUpload == 1){
			bsNotify.show({
				title: "Warning",
				content: "Data tidak dapat dihapus.",
				type: "warning"
			});

			return false;
		}

		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar'){ //CR OPL in Finance Start
			for(var i = 0; i <  $scope.InvoiceMasukPODitagih_UIGrid.data.length; i++){
				if($scope.InvoiceMasukPODitagih_UIGrid.data[i].NomorPO == row.entity.NomorPO){
					$scope.InvoiceMasukPODitagih_UIGrid.data.splice(i, 1);
					i=-1; continue;
				}
			}
			$scope.InvoiceMasukPODitagih_UIGrid.data.splice(i, 1);
		}else{ //CR OPL in Finance End
			var index = $scope.InvoiceMasukPODitagih_UIGrid.data.indexOf(row.entity);
			$scope.InvoiceMasukPODitagih_UIGrid.data.splice(index, 1);
		}
		$scope.TotalDPPAll = 0;
			$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
					console.log("isi row",row);
					if ( row.DPPInvoice != undefined ){
						$scope.TotalDPPAll += row.DPPInvoice ;
					}
					console.log("TotalPembayaranAll",$scope.TotalDPPAll);
			});

		$scope.autoCalculationDpp();
		$scope.CalculationNominalInvoice();
	}

	// CR5 #38 Finance Start
	$scope.DaftraNomorRangka = [];
	// CR5 #38 Finance End

	$scope.TambahInvoiceMasuk_TambahRangka_Clicked = function () {
		InvoiceMasukFactory.getFrameData($scope.TambahInvoiceMasuk_Rangka_NomorRangka + '|' + $scope.TambahInvoiceMasuk_SelectBranch)
			.then(
				function (res) {
					if (res.data.Result.length == 0) {
						//alert("Data tidak ditemukan");
						bsNotify.show({
							title: "Warning",
							content: "Data tidak ditemukan.",
							type: "warning"
						});
					}
					else {
						if ($scope.InvoiceMasukRangka_Data.length > 0){
							// angular.forEach($scope.InvoiceMasukRangka_Data, function(value){
							// 	if (value.FrameNo != res.data.Result[0].FrameNo){
							// 		$scope.InvoiceMasukRangka_Data.push({ "FrameId": res.data.Result[0].SOId, "FrameNo": res.data.Result[0].FrameNo });
							// 		$scope.DaftraNomorRangka.push({ "name": res.data.Result[0].FrameNo, "value": res.data.Result[0].FrameNo });
							// 	}
							// 	else {
							// 		bsNotify.show({
							// 			title: "Warning",
							// 			content: "Data sudah ada",
							// 			type: "warning"
							// 		});
							// 	}
							// })
							// CR5 #38 Finance Start Perbaikan metode yg lama
							var dataSama = false;
							$scope.InvoiceMasukRangka_Data.some(function(x, i) {
								if (x.FrameNo == res.data.Result[0].FrameNo) return (dataSama = true);
							});

							if(dataSama){
								bsNotify.show({
									title: "Warning",
									content: "Data sudah ada",
									type: "warning"
								});
							}else{
								$scope.InvoiceMasukRangka_Data.push({ "FrameId": res.data.Result[0].SOId, "FrameNo": res.data.Result[0].FrameNo });
								$scope.DaftraNomorRangka.push({ "name": res.data.Result[0].FrameNo, "value": res.data.Result[0].FrameNo });
							}
							// CR5 #38 Finance End
						}
						else { 
							$scope.InvoiceMasukRangka_Data.push({ "FrameId": res.data.Result[0].SOId, "FrameNo": res.data.Result[0].FrameNo });
							// CR5 #38 Finance Start
							$scope.DaftraNomorRangka.push({ "name": res.data.Result[0].FrameNo, "value": res.data.Result[0].FrameNo });
							// CR5 #38 Finance End
						}
						$scope.InvoiceMasukRangka_UIGrid.data = $scope.InvoiceMasukRangka_Data;
					}
				},
				function (err) {
					//alert("Data tidak ditemukan");
					bsNotify.show({
						title: "Warning",
						content: "Data tidak ditemukan.",
						type: "warning"
					});
				}
			);
		$scope.autoCalculationDpp();
	}

	$scope.TambahInvoiceMasuk_DeleteRangka = function (row) {
		var index = $scope.InvoiceMasukRangka_Data.indexOf(row.entity);
		$scope.InvoiceMasukRangka_Data.splice(index, 1);
		console.log('row',row.entity);
		// CR5 #38 Finance Start
		
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Accrued Free Service"){
			//untuk hapus nomor rangka di rincian pajak
			for(var i = 0; i < $scope.DaftraNomorRangka.length; i++){
				if($scope.DaftraNomorRangka[i].value == row.entity.FrameNo){
					$scope.DaftraNomorRangka.splice(i,1)
				}
			}
			//untuk hapus daftar pajak yg sudah di input dengan nomor rangka yg di hapus dari daftar nomor rangka
			for(var i = 0; i < $scope.InvoiceMasukPajak_Data.length; i++){
				if($scope.InvoiceMasukPajak_Data[i].RefNo == row.entity.FrameNo){
					$scope.InvoiceMasukPajak_Data.splice(i,1)
				}
			}
			$scope.TambahDaftarNomorRangka = null;
		}


		// CR5 #38 Finance End
		$scope.autoCalculationDpp(); //Harus ditaruh di akhir function
		$scope.CalculationNominalInvoice();
	}

	// OLD
	// $scope.TambahInvoiceMasuk_Simpan_Clicked = function () {
	// 	if (!$scope.CreateInvoiceMasukData())
	// 		return;
	// 	InvoiceMasukFactory.createInvoiceMasuk($scope.invoiceMasukData)
	// 		.then(
	// 			function (res) {
	// 				console.log("res", res);
	// 				if (res.data.Result.length == 0) {
	// 					//alert("data berhasil disimpan");
	// 					bsNotify.show({
	// 						title: "Berhasil",
	// 						content: "data berhasil disimpan.",
	// 						type: "success"
	// 					});
	// 					$scope.TambahInvoiceMasuk_ClearFields();
	// 					$scope.Show_MainInvoiceMasuk = true;
	// 					$scope.Show_TambahInvoiceMasuk = false;
	// 					if ($scope.MainInvoiceMasuk_TanggalAwal != "" && $scope.MainInvoiceMasuk_TanggalAkhir != "")
	// 						$scope.InvoiceMasukList_UIGrid_Paging(1, uiGridPageSize);
	// 				}
	// 				else {
	// 					// alert(res.data.Result[0].Message);
	// 					bsNotify.show({
	// 						title: "Warning",
	// 						content: res.data.Result[0].Message,
	// 						type: "warning"
	// 					});
	// 				}

	// 			},
	// 			function (err) {
	// 				console.log(err);
	// 			}
	// 		);
	// }

	// NEW

	$scope.SimpanData = function(){
		if (!$scope.CreateInvoiceMasukData())
			return;
		InvoiceMasukFactory.createInvoiceMasuk($scope.invoiceMasukData)
			.then(
				function (res) {
					console.log("res", res);
					if (res.data.Result.length == 0) {
						//alert("data berhasil disimpan");
						bsNotify.show({
							title: "Berhasil",
							content: "data berhasil disimpan.",
							type: "success"
						});
						$scope.TambahInvoiceMasuk_ClearFields();
						$scope.Show_MainInvoiceMasuk = true;
						$scope.Show_TambahInvoiceMasuk = false;
						if ($scope.MainInvoiceMasuk_TanggalAwal != "" && $scope.MainInvoiceMasuk_TanggalAkhir != "")
							$scope.InvoiceMasukList_UIGrid_Paging(1, uiGridPageSize);
					}
					else {
						// alert(res.data.Result[0].Message);
						var err = '';
						if(err != null){
							var errMsg = '';
							if (res.data.Result[0].Message.split('#').length > 1) {
								errMsg = res.data.Result[0].Message.split('#')[0];
							}
							bsNotify.show({
								title: "Warning",
								content: errMsg,
								type: "warning"
							});
						}
						$scope.Loading = false;
						return false;
					}

				},
				function (err) {
					console.log(err);
				}
			);
	}

	$scope.TambahInvoiceMasuk_Simpan_Clicked = function(){
		var TotHitung = 0;
		var TotTaxD = 0;
		var TotTaxK = 0;
		var posisi = 0;
		var SisaFloat = "";
		$scope.Loading = true;
		if(($scope.TambahInvoiceMasuk_NomorInvoiceVendor == null || $scope.TambahInvoiceMasuk_NomorInvoiceVendor == undefined || $scope.TambahInvoiceMasuk_NomorInvoiceVendor == "") || 
			($scope.TambahInvoiceMasuk_TanggalJatuhTempo == null || $scope.TambahInvoiceMasuk_TanggalJatuhTempo == undefined || $scope.TambahInvoiceMasuk_TanggalJatuhTempo == "") || 
			($scope.TambahInvoiceMasuk_Nominal == null || $scope.TambahInvoiceMasuk_Nominal == undefined || $scope.TambahInvoiceMasuk_Nominal == "" || $scope.InvoiceMasukPODitagih_Data == undefined )
			||($scope.TambahInvoiceMasuk_TipeInvoiceMasuk == null || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk == undefined || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk == "" )){
				bsNotify.show(
					{
						title: "Gagal",
						content: "Isi Data Harus Lengkap",
						type: 'danger'
					}
				);
				$scope.Loading = false;
				return true;
		}
		if($scope.TambahInvoiceMasuk_Nominal == 0 ){
			bsNotify.show(
				{
					title: "Gagal",
					content: "Nominal Invoice Tidak Sesuai",
					type: 'warning'
				}
			);
			$scope.Loading = false;
			return true;
		}
		if($scope.TambahInvoiceMasuk_TanggalJatuhTempo == "" || $scope.TambahInvoiceMasuk_TanggalJatuhTempo == undefined ||
		   $scope.TambahInvoiceMasuk_TanggalJatuhTempo == null || $scope.TambahInvoiceMasuk_TanggalJatuhTempo == "Invalid Date"){
			bsNotify.show(
				{
					title: "Gagal",
					content: "Tanggal Jatuh Tempo harus diisi.",
					type: 'warning'
				}
			);
			$scope.Loading = false;
			return true;
		}
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'STNK/BPKB')
		{
			if ($scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice == "" || $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice == null) {
				//alert("Nama Vendor harus diisi");
				bsNotify.show({
					title: "Warning",
					content: "Tipe Notice atau Unnotice wajib dipilih.",
					type: 'warning'
				});
				$scope.Loading = false;
				return;
			}
		}	

		//---------------- cek kl list daftar po ditagih kosong ------------------- start
		console.log('kuyussss',$scope.InvoiceMasukPODitagih_UIGrid.data)

		if ($scope.InvoiceMasukPODitagih_UIGrid.data.length == 0) {
			bsNotify.show({
				title: "Gagal",
				content: "Daftar PO yang ditagih tidak boleh kosong.",
				type: 'warning'
			});
			$scope.Loading = false;
			return;
		}

		//---------------- cek kl list daftar po ditagih kosong ------------------- end

		// angular.forEach($scope.InvoiceMasukPODitagih_UIGrid.data, function (obj) {
		// 	if(obj.DPPInvoice == 0 || (obj.DPPInvoice != "" && !angular.isUndefined(obj.DPPInvoice))){
				
		// 		for (var i=0; i<$scope.InvoiceMasukPODitagihGridApi.grid.columns.length; i++){
		// 			if ($scope.InvoiceMasukPODitagihGridApi.grid.columns[i].field == 'DPPInvoice'){
		// 				posisi = i;
		// 			}
		// 		}
		// 		// TotHitung += $scope.InvoiceMasukPODitagihGridApi.grid.columns[posisi];
		// 		TotHitung += $scope.TotalDPPAll;
				
				
				
		// 	}
		// });
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Accrued Free Service"){
			if($scope.TambahInvoiceMasuk_FreeServiceBranch == null || $scope.TambahInvoiceMasuk_FreeServiceBranch == undefined || $scope.TambahInvoiceMasuk_FreeServiceBranch == ""){
				bsNotify.show({
					title: "Warning",
					content: "Nama Branch Wajib Dipilih",
					type: 'warning'
				});
				$scope.Loading = false;
				return;
			}

			$scope.InvoiceMasukRangka_UIGrid.data.forEach(function (row) {
				console.log("isi row",row);
				if ( row.FreeServiceNominal != undefined ){
					TotHitung += row.FreeServiceNominal ;
				}
				console.log("TotalPembayaranAll",TotHitung);
			});
		}else{
			$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
				console.log("isi row",row);
				if ( row.DPPInvoice != undefined ){
					TotHitung += row.DPPInvoice ;
				}
				console.log("TotalPembayaranAll",TotHitung);
			});
		}
		
		var tmpTotHitung = TotHitung.toFixed(2);
		TotHitung = parseFloat(tmpTotHitung);
		console.log("cek tipe data", tmpTotHitung)
		console.log("cek tipe data", TotHitung)
		for (var j in $scope.InvoiceMasukPajak_UIGrid.data){
			if($scope.InvoiceMasukPajak_UIGrid.data[j].DC == "D"){
				TotTaxD += parseFloat($scope.InvoiceMasukPajak_UIGrid.data[j].TaxNominal);	
			}else{
				TotTaxK += parseFloat($scope.InvoiceMasukPajak_UIGrid.data[j].TaxNominal);	
			}
		}
		console.log("Nominla Invoice ",$scope.TambahInvoiceMasuk_Nominal);
		console.log("Nominla Nominal DPP", TotHitung)
		console.log("Nominla Nominal Pajak D", TotTaxD);
		console.log("Nominla Nominal Pajak K", TotTaxK);

		// SisaFloat = $scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-3);
		// console.log("sisa float", SisaFloat)
		var NomInvoiceCopy = angular.copy($scope.TambahInvoiceMasuk_Nominal);
		
		if (NomInvoiceCopy.substr(NomInvoiceCopy.length-3,1) == "."){
			SisaFloat = NomInvoiceCopy.substr(NomInvoiceCopy.length-3);
			NomInvoiceCopy = NomInvoiceCopy.substr(0,NomInvoiceCopy.length-3);
			
		}
		else if (NomInvoiceCopy.substr(NomInvoiceCopy.length-2,1) == "."){
			SisaFloat = NomInvoiceCopy.substr(NomInvoiceCopy.length-2);
			NomInvoiceCopy = NomInvoiceCopy.substr(0,NomInvoiceCopy.length-2);
			
		}
		var val = NomInvoiceCopy.split(".").join("");
		
		val = val.replace(/[a-zA-Z\s]/g, '');
		val = String(val).split("").reverse().join("")
			.replace(/(\d{3}\B)/g, "$1.")
			.split("").reverse().join("");

		if (val == ""){
			val = "0.00";
		}

		if (SisaFloat == "undefined" || !SisaFloat){
			NomInvoiceCopy = val.split(".").join("") +".00";
		}
		else{
			NomInvoiceCopy = val.split(".").join("") + SisaFloat;
		}

		$scope.TambahInvoiceMasuk_Nominal = NomInvoiceCopy ;
		NomInvoiceCopy = NomInvoiceCopy.replace(/\,/g, "");
		
		// var	NomInvoiceCopy1 = parseFloat(NomInvoiceCopy).toFixed(2);
		var	NomInvoiceCopy2 = parseFloat(NomInvoiceCopy);
		var teskal = (parseFloat(TotHitung) + TotTaxD - TotTaxK);
		var testofix = teskal.toFixed(2);
			
			console.log("nom copy", NomInvoiceCopy)
			// console.log("nom copy1", NomInvoiceCopy1)
			console.log("nom copy2", NomInvoiceCopy2)
			console.log("total kalkulasi 1", teskal)
			console.log("total kalkulasi 3", testofix)
			console.log("total kalkulasi 2",(TotHitung + TotTaxD - TotTaxK))

		// Cr5 #38 Finance Start
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Accrued Free Service"){
			for(var i = 0; i< $scope.InvoiceMasukRangka_UIGrid.data.length; i++){
				var index = null;
				console.log("isi row",$scope.InvoiceMasukRangka_UIGrid.data[i]);
				for(var z = 0; z < $scope.InvoiceMasukPajak_Data.length; z++){
					if($scope.InvoiceMasukPajak_Data[z].TaxType.includes("PPh 23")){
						if ($scope.InvoiceMasukPajak_Data[z].RefNo != $scope.InvoiceMasukRangka_UIGrid.data[i].FrameNo) {
							index = z
						}else{
							index = null
							break;
						}
					}
				}

				if(index != null){
					if(NomInvoiceCopy2 != testofix){
						bsNotify.show(
							{
								title: "Gagal",
								content: "Nominal Invoice Tidak Sesuai!",
								type: 'danger'
							}
						);
						$scope.Loading = false;
						return false;
					}else{
						angular.element('#ModalSimpanFreeService').modal('show');
						$scope.Loading = false;
						return false;
					}
				}
			}
		}
		// Cr5 #38 Finance End

		if(NomInvoiceCopy2 != testofix){
			bsNotify.show(
				{
					title: "Gagal",
					content: "Nominal Invoice Tidak Sesuai!",
					type: 'danger'
				}
			);
			$scope.Loading = false;
			return false;
		}else{
			$scope.SimpanData();
		}
	}

	$scope.InvoiceMasukClearUIGridData = function () {
		$scope.InvoiceMasukPODitagih_Data = [];
		$scope.InvoiceMasukPODitagih_UIGrid.data = $scope.InvoiceMasukPODitagih_Data;
		$scope.InvoiceMasukPajak_Data = [];
		$scope.InvoiceMasukPajak_UIGrid.data = $scope.InvoiceMasukPajak_Data;
		$scope.InvoiceMasukRangka_Data = [];
		$scope.InvoiceMasukRangka_UIGrid = {};
		$scope.InvoiceMasukRangka_UIGrid.data = [];
		$scope.TambahInvoiceMasuk_Rangka_NomorRangka = "";
	}

	$scope.TambahInvoiceMasuk_ClearFields = function () {
		$scope.TambahInvoiceMasuk_TanggalInvoiceMasuk = "";
		$scope.TambahInvoiceMasuk_NomorInvoiceVendor = "";
		$scope.TambahInvoiceMasuk_VendorId = 0;
		$scope.TambahInvoiceMasuk_TipeInvoiceMasuk = undefined;
		$scope.TambahInvoiceMasuk_Nominal = "";
		$scope.TambahInvoiceMasuk_TanggalJatuhTempo = "";
		$scope.TambahInvoiceMasuk_Keterangan = "";
		$scope.TambahInvoiceMasuk_NomorVendor = "";
		$scope.TambahInvoiceMasuk_NamaVendor = "";
		$scope.TambahInvoiceMasuk_NPWP = "";
		$scope.TambahInvoiceMasuk_Alamat = "";
		$scope.TambahInvoiceMasuk_KabupatenKota = "";
		$scope.TambahInvoiceMasuk_Telepon = "";
		$scope.TaxPercent = "";
		$scope.invoiceMasukData = [];
		$scope.InvoiceMasukClearUIGridData();
		$scope.TambahInvoiceMasuk_OtherTypeNamaVendor = '';
		$scope.InvoiceMasukPO_UIGrid.data = [];
		$scope.TambahInvoiceMasuk_ClearFieldsTax();
		$scope.TotalDPPAll = 0;
		$scope.TotalTagihan = 0;
		
	}

	$scope.TambahInvoiceMasuk_ClearFieldsTax = function () {
		$scope.TambahInvoiceMasuk_RincianPajak_DPP = '';
		$scope.TambahInvoiceMasuk_RincianPajak_JenisPajak = undefined;
		$scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen = '';
		$scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen = undefined;
		$scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi = '';
		$scope.TambahInvoiceMasuk_RincianPajak_Keterangan = '';
		$scope.TambahInvoiceMasuk_isDokumenPajak_isDisable = false;
	}

	$scope.CreateInvoiceMasukData = function () {
		// $scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.replace(/\./g, '').replace(/\,/g, '.');
		// $scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.split(",").join("");
		var InvoicePOListData = [];
		var InvoiceTaxListData = [];
		var InvoiceFreeServiceData = [];
		var VendorId = 0;
		var exitForEach = false;
		console.log("$scope.TambahInvoiceMasuk_Nominal",$scope.TambahInvoiceMasuk_Nominal);

		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType != "Accrued Free Service") {
			angular.forEach($scope.InvoiceMasukPODitagih_UIGrid.data, function (value, key) {
				if (!exitForEach) {
					if (isNaN(value.DPPInvoice)) {
						//alert("DPP Invoice pada PO yang Ditagih harus diisi !");
						bsNotify.show({
							title: "Warning",
							content: "DPP Invoice pada PO yang Ditagih harus diisi !",
							type: "warning"
						});
						exitForEach = true;
					}

					if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Parts" || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Order Permintaan Bahan"){
						InvoicePOListData.push(
							{
								"POId": value.POId,
								"InvoiceDPP": value.DPPInvoice,
								"DifferenceDPP": value.Selisih,
								"FrameId": 0,
								"FreeServiceNominal": 0,
								"PODPP": value.DPP,
								"FrameNo":value.NomorGR
							}
						);
					}else if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Aksesoris Standard"){
						InvoicePOListData.push(
							{
								"POId": value.POId,
								"InvoiceDPP": value.DPPInvoice,
								"DifferenceDPP": value.Selisih,
								"FrameId": 0,
								"FreeServiceNominal": 0,
								"PODPP": value.DPP,
								"FrameNo":value.FrameNo,
								"AccessoriesName": value.AccessoriesName, //CR5P 37
								"AccessoriesCode": value.AccessoriesCode, //CR5P 37
								"AccessoriesId": value.AccessoriesId //CR5P 37
							}
						);
					}else{
						InvoicePOListData.push(
							{
								"POId": value.POId,
								"InvoiceDPP": value.DPPInvoice,
								"DifferenceDPP": value.Selisih,
								"FrameId": 0,
								"FreeServiceNominal": 0,
								"PODPP": value.DPP,
								"FrameNo":value.FrameNo
							}
						);
					}
				}
			});

			if (isNaN($scope.TambahInvoiceMasuk_Nominal) || $scope.TambahInvoiceMasuk_Nominal == "" || $scope.TambahInvoiceMasuk_Nominal == null) {
				//alert("DPP Invoice pada PO yang Ditagih harus diisi !");
				bsNotify.show({
					title: "Warning",
					content: "Nominal Invoice harus diisi !",
					type: "warning"
				});
				exitForEach = true;
			}

			if (exitForEach){
				return false;
			}
			VendorId = $scope.TambahInvoiceMasuk_VendorId;
		}
		else {
			angular.forEach($scope.InvoiceMasukRangka_Data, function (value, key) {
				InvoicePOListData.push(
					{
						"POId": 0,
						"InvoiceDPP": 0,
						"DifferenceDPP": 0,
						"FrameId": value.FrameId,
						"FreeServiceNominal": value.FreeServiceNominal
					}
				);
			});
			VendorId = $scope.TambahInvoiceMasuk_FreeServiceBranchId;

			InvoiceFreeServiceData = [
				{
					"BranchName": $scope.TambahInvoiceMasuk_FreeServiceBranch,
					"BranchNPWP": $scope.TambahInvoiceMasuk_FreeServiceNPWP,
					"BranchAddress": $scope.TambahInvoiceMasuk_FreeServiceAlamat,
					"BranchCity": $scope.TambahInvoiceMasuk_FreeServiceKabupatenKota,
					"BranchPhone": $scope.TambahInvoiceMasuk_FreeServiceTelepon
				}
			];
		}


		angular.forEach($scope.InvoiceMasukPajak_Data, function (value, key) {
			InvoiceTaxListData.push(
				{
					"TaxTypeId": value.TaxTypeId,
					"TaxAmount": value.TaxNominal,
					"DocTaxNo": value.TaxDocNo,
					"RefNo": value.RefNo,
					"DocTaxDate": $filter('date')(new Date(value.TaxDocDate), "yyyy-MM-dd"),
					"TaxPercent": value.TaxPercent,
					"TaxDPP": value.TaxDPP
				}
			);
		});
		console.log("Tipe invoice masuk : ", $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId);
		$scope.invoiceMasukData = [
			{
				"OutletId": $scope.TambahInvoiceMasuk_SelectBranch,//user.OutletId,
				"InvoiceDate": $filter('date')(new Date($scope.TambahInvoiceMasuk_TanggalInvoiceMasuk), "yyyy-MM-dd"),
				"InvoiceVendorNo": $scope.TambahInvoiceMasuk_NomorInvoiceVendor,
				"VendorId": VendorId,
				"InvoiceTypeId": $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
				"Nominal": $scope.TambahInvoiceMasuk_Nominal,
				"MaturityDate": $filter('date')(new Date($scope.TambahInvoiceMasuk_TanggalJatuhTempo), "yyyy-MM-dd"),
				"Description": $scope.TambahInvoiceMasuk_Keterangan,
				"InvoicePOList": InvoicePOListData,
				"InvoiceTaxList": InvoiceTaxListData,
				"InvoiceFreeServiceBranch": InvoiceFreeServiceData,
				"Notice" : $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice
			}
		];
		return true;
	}

	$scope.TambahInvoiceMasuk_Batal_Clicked = function () {
		$scope.Show_MainInvoiceMasuk = true;
		$scope.Show_TambahInvoiceMasuk = false;
		$scope.TambahInvoiceMasuk_ClearFields();
	}
	//==========================PAJAK SECTION BEGIN==========================//
	$scope.filterBy = function(actual, expected) {
		return _.contains(expected, actual); // uses underscore library contains method
	};
	
	$scope.InitiateComboBoxJenisPajak = function () {
		var jenisPajakData = [];
		$scope.InvoiceMasuk_JenisPajakData = [];
		$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = [];
		InvoiceMasukFactory.getTaxList()
			.then(
				function (res) {
					angular.forEach(res.data.Result, function (value, key) {
						$scope.InvoiceMasuk_JenisPajakData.push({ "name": value.TaxType, "value": { "TaxType": value.TaxType, "TaxTypeId": value.TaxTypeId, "DC": value.DC, "NPWPAmount": value.NPWPAmount, "NonNPWPAmount": value.NonNPWPAmount, "isDocTax": value.isDocTax, "TaxDesc": value.TaxDesc } });//
					});
					if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Unit'){
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Unit') || obj.value.TaxDesc == 'PPh Pasal 22 - Prepaid';
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris'||$scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Purna Jual'){
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Aksesoris');
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Karoseri'){
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Unit');
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'STNK/BPKB' || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pengurusan Dokumen'||$scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Ekspedisi'){
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Unit') || (obj.value.TaxDesc == 'PPh Pasal 23' && obj.value.NPWPAmount ==2);
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Swapping') {
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Unit') || obj.name == 'PPh 22 5% - Pajak Pembelian Barang Sangat Mewah';
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Parts') {
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Parts');
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar') {
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Service') || (obj.value.TaxDesc == 'PPh Pasal 23' && obj.value.NPWPAmount ==2);
						},true);
					}
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Permintaan Bahan') {
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Service');
						},true);
					}else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris Standard') {
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Aksesoris') || (obj.value.TaxDesc == 'PPh Pasal 23' && obj.value.NPWPAmount ==2);
						},true);
					
					}// CR5 #38 Finance Start
					else if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Accrued Free Service'){
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $filter('filter')($scope.InvoiceMasuk_JenisPajakData, function(obj) {
							return obj.name.includes('PPN Unit') || (obj.value.TaxType == 'PPh 23 2% - Jasa Perawatan Kendaraan atau Transportasi'); //PPh 23 2% - Jasa Perawatan Kendaraan atau Transportasi
						},true);
					}// CR5 #38 Finance End
					else{
						$scope.optionsTambahInvoiceMasukRincianPajakJenisPajak = $scope.InvoiceMasuk_JenisPajakData;
					}
					
				}
			);
	}

	$scope.InitiateComboBoxJenisPajak();

	$scope.TambahInvoiceMasuk_DeletePajak = function (row) {
		// CR5 #38 Finance Start
		var index = $scope.InvoiceMasukPajak_Data.indexOf(row.entity);
		$scope.InvoiceMasukPajak_Data.splice(index, 1);

		if(row.entity.TaxType.includes("PPh 23")){
			$scope.DaftraNomorRangka.push({ "name": row.entity.RefNo, "value": row.entity.RefNo });
		}// CR5 #38 Finance End
		$scope.CalculationNominalInvoice();
	}
	var found = 0;
	$scope.TambahInvoiceMasuk_TambahPajak_Clicked = function () {
		var PajakData = [];
		var TaxNominal = 0;
		var found = 0;
		var SisaFloat ="";
		if ($scope.TambahInvoiceMasuk_RincianPajak_DPP == null || $scope.TambahInvoiceMasuk_RincianPajak_DPP == '' || $scope.TambahInvoiceMasuk_RincianPajak_DPP == undefined) {
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = "0.00";
		}
		$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();

		if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_RincianPajak_DPP);
		}
		else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_RincianPajak_DPP);
		}
		$scope.TambahInvoiceMasuk_RincianPajak_DPP = parseFloat($scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("")+SisaFloat);
		
		if ($scope.TambahInvoiceMasuk_RincianPajak_DPP == null || $scope.TambahInvoiceMasuk_RincianPajak_DPP == '' || $scope.TambahInvoiceMasuk_RincianPajak_DPP == undefined) {
			bsNotify.show({
				title: "Warning",
				content: "DPP harus diisi !",
				type: "warning"
			});

			$scope.TambahInvoiceMasuk_RincianPajak_DPP = "0.00";
		}
		else if ($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak == null || $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak == undefined || $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak == '') {
			bsNotify.show({
				title: "Warning",
				content: "Jenis Pajak harus diisi !",
				type: "warning"
			});
			
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();
			if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				
			}
			else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				
			}
			var val = $scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("");
			
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");

			if (val == ""){
				val = "0.00";
			}

			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+SisaFloat;
			}

		}
		else if ($scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi == null || $scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi == '') {
			bsNotify.show({
				title: "Warning",
				content: "Nomor referensi harus diisi !",
				type: "warning"
			});
			
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();
			if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				
			}
			else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				
			}
			var val = $scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("");
			
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");

			if (val == ""){
				val = "0.00";
			}
	
			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+SisaFloat;
			}

		}
		else if ($scope.TambahInvoiceMasuk_isDokumenPajak_isDisable == false && ($scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen == null || $scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen == undefined || $scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen == '')) {
			bsNotify.show({
				title: "Warning",
				content: "Nomor dokumen pajak harus diisi !",
				type: "warning"
			});
			
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();
			if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				
			}
			else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				
			}
			var val = $scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("");
			
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");

			if (val == ""){
				val = "0.00";
			}
	
			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+SisaFloat;
			}
		}
		else if ($scope.TambahInvoiceMasuk_isDokumenPajak_isDisable == false && ($scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen == null || $scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen == undefined || $scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen == '')) {
			bsNotify.show({
				title: "Warning",
				content: "Tanggal dokumen pajak harus diisi !",
				type: "warning"
			});
			
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();
			if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
				
			}
			else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
				SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
				
			}
			var val = $scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("");
			
			val = val.replace(/[a-zA-Z\s]/g, '');
			val = String(val).split("").reverse().join("")
				.replace(/(\d{3}\B)/g, "$1.")
				.split("").reverse().join("");
			
			if (val == ""){
				val = "0.00";
			}
	
			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+SisaFloat;
			}
		}
		else {
			if ($scope.TambahInvoiceMasuk_NPWP != "00.000.000.0-000.000") {
				TaxNominal = ($scope.TambahInvoiceMasuk_RincianPajak_DPP * $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NPWPAmount) / 100.00;
				$scope.TaxPercent = $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NPWPAmount;
			}
			else {
				TaxNominal = ($scope.TambahInvoiceMasuk_RincianPajak_DPP * $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NonNPWPAmount) / 100.00;
				$scope.TaxPercent = $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NonNPWPAmount;
			}

			// // CR5P #10 Finance Start
			// if((($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Parts" || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Order Permintaan Bahan") && $scope.IsNonPKP == 0)){
			// 	TaxNominal = ($scope.TambahInvoiceMasuk_RincianPajak_DPP * $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NPWPAmount) / 100.00;
			// 	$scope.TaxPercent = $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NPWPAmount;
			// }else if((($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Parts" || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Order Permintaan Bahan") && $scope.IsNonPKP == 1)){
			// 	TaxNominal = ($scope.TambahInvoiceMasuk_RincianPajak_DPP * $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NonNPWPAmount) / 100.00;
			// 	$scope.TaxPercent = $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.NonNPWPAmount;
			// }
			// // CR5P #10 Finance End

			PajakData = {
				"TaxTypeId": $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxTypeId,
				"TaxType": $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxType,
				"TaxPercent": $scope.TaxPercent,
				"DC": $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.DC,
				"TaxNominal": TaxNominal.toFixed(2),
				"TaxDocNo": $scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen,
				"TaxDocDate": $filter('date')(new Date($scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen), 'yyyy-MM-dd'),
				"RefNo": $scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi,
				"TaxDPP": $scope.TambahInvoiceMasuk_RincianPajak_DPP
			};
			$scope.InvoiceMasukPajak_UIGrid.data.forEach(function (obj) {
				console.log('isi grid -->', obj.TaxTypeId);
				console.log('isi biaya -->', $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak);
				console.log("found ==", found )
				if ($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxType == obj.TaxType){
					found = 1;
				}
				// CR5 #38 Finance Start Bisa pake PPh 23 yg sama berulangkali
				if($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxType == obj.TaxType && $scope.modeFreeServicePph23){
					found = 0;
				}// CR5 #38 Finance End
			});
			//CR5P #10
			if((($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Parts" || $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == "Order Permintaan Bahan") 
				&& $scope.IsNonPKP == 1 && $scope.TaxPercent == 0)){
				bsNotify.show({
					title: "Warning",
					content: "Nominal Pajak tidak boleh 0.",
					type: 'warning'
				});
				return;
			}


			// Cr5 #38 Start
			if($scope.modeFreeServicePph23){
				for(var i = 0; i < $scope.DaftraNomorRangka.length; i++){
					if($scope.DaftraNomorRangka[i].value == $scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi){
						$scope.DaftraNomorRangka.splice(i,1);
						$scope.TambahDaftarNomorRangka = null;
					}
				}
			}
			// Cr5 #38 End

			if (found == 0) {
				if(PajakData.TaxDocDate == null || PajakData.TaxDocDate == "Invalid Date"){
					PajakData.TaxDocDate = "";
				}
				$scope.InvoiceMasukPajak_Data.push(PajakData);
				$scope.TambahInvoiceMasuk_ClearFieldsTax();
				console.log("InvoiceMasukPajakData", $scope.InvoiceMasukPajak_Data);
			}
		}
		$scope.CalculationNominalInvoice();
	}

	$scope.TambahInvoiceMasuk_RincianPajak_JenisPajak_Changed = function () {
		if ($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak != undefined) {
			$scope.TambahInvoiceMasuk_RincianPajak_Keterangan = $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxDesc;
			$scope.InvoiceMasuk_JenisPajakData.some(function (obj, i) {
				if ($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxTypeId == obj.value.TaxTypeId) {
					console.log("isDocTax : ", obj.value.isDocTax);
					if (!obj.value.isDocTax) {
						$scope.TambahInvoiceMasuk_isDokumenPajak_isDisable = true;
						$scope.TambahInvoiceMasuk_isDokumenPajak_Show = true;
						$scope.TambahInvoiceMasuk_RincianPajak_NomorDokumen = '';
						$scope.TambahInvoiceMasuk_RincianPajak_ShowNomorDokumen = false;
						$scope.TambahInvoiceMasuk_RincianPajak_TanggalDokumen = undefined;
						$scope.TambahInvoiceMasuk_RincianPajak_ShowTanggalDokumen = false;
					}
					else {
						$scope.TambahInvoiceMasuk_RincianPajak_ShowNomorDokumen = true;
						$scope.TambahInvoiceMasuk_RincianPajak_ShowTanggalDokumen = true;
						$scope.TambahInvoiceMasuk_isDokumenPajak_isDisable = false;
						$scope.TambahInvoiceMasuk_isDokumenPajak_Show = true;
					}
				}
			});
		}
		//CR5 #38 Start
		if ($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Accrued Free Service' && 
		$scope.TambahInvoiceMasuk_RincianPajak_JenisPajak.TaxDesc == 'PPh Pasal 23'){
			$scope.modeFreeServicePph23 = true;
			$scope.TambahDaftarNomorRangka = null;
			$scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi = null;
		}else{
			$scope.modeFreeServicePph23 = false;
			$scope.TambahDaftarNomorRangka = null;
			$scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi = null;
		}
		//CR5 #38 End

		$scope.autoCalculationDpp(); //Harus ditaruh di akhir function
	}

	$scope.TambahInvoiceMasuk_NominalBlur = function () {
		console.log("test",$scope.TambahInvoiceMasuk_Nominal);
		console.log("test",parseFloat($scope.TambahInvoiceMasuk_Nominal).toFixed(2));
		var SisaFloat = "";
		if ($scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-3,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-3);
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.substr(0,$scope.TambahInvoiceMasuk_Nominal.length-3);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_Nominal);
		}
		else if ($scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-2,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_Nominal.substr($scope.TambahInvoiceMasuk_Nominal.length-2);
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.substr(0,$scope.TambahInvoiceMasuk_Nominal.length-2);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_Nominal);
		}
		var val = $scope.TambahInvoiceMasuk_Nominal.split(".").join("");

		val = val.replace(/[a-zA-Z\s]/g, '');
		val = String(val).split("").reverse().join("")
			.replace(/(\d{3}\B)/g, "$1.")
			.split("").reverse().join("");

		var str_array = val.split('.');

		var NominalValid = true;
		var satu = 0;
		var dua = 0;
		for (var i = 0; i < str_array.length; i++) {
			if (str_array[i].length == 1) {
				satu = satu + 1;
			}
			else if (str_array[i].length == 2) {
				dua = dua + 1;
			}

			if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
				NominalValid = false;
				break;
			}
		}

		if (!NominalValid) {
			$scope.TambahInvoiceMasuk_NominalBlur(val);
		}
		else {
			// $scope.TambahInvoiceMasuk_Nominal = val;
			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_Nominal = val+".00";
				$scope.TambahInvoiceMasuk_NominalforParseFloat = val.split(".").join("")+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_Nominal = val+SisaFloat;
				$scope.TambahInvoiceMasuk_NominalforParseFloat = val.split(".").join("") + SisaFloat;
				console.log("testing",val+SisaFloat);
				console.log("testing",$scope.TambahInvoiceMasuk_NominalforParseFloat);
			}
		}
	}

	$scope.givePatternNomInvoice = function (item, event) {
		console.log("item", item);
		console.log("event", event.which);
		console.log("event", event);
		if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105)){
			event.preventDefault();
			console.log("disini", event.which);
			console.log("$scope.TambahInvoiceMasuk_Nominal", $scope.TambahInvoiceMasuk_Nominal);
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.replace(/[^0-9.]+/g,'');
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal.replace(event.key,"");
			return false;
		} else {
			$scope.TambahInvoiceMasuk_Nominal = $scope.TambahInvoiceMasuk_Nominal;
			return;
		}
	};

	//==========================PAJAK SECTION END==========================//

	//==========================UI GRID BEGIN==========================//

	$scope.InvoiceMasukList_UIGrid = {
		//paginationPageSizes: null,
		useCustomPagination: true,
		useExternalPagination: true,
		rowSelection: true,
		enableFiltering: true,
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableColumnResizing: true,
		columnDefs: [
			{ name: "InvoiceId", field: "InvoiceId", visible: false },
			{ name: "OutletId", field: "OutletId", visible: false },
			{ name: "OutletName", field: "OutletName", visible: false },
			{ name: "Tanggal Invoice Masuk", field: "InvoiceDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{
				name: "Nomor Invoice Masuk", field: "InvoiceNo",
				cellTemplate: '<a ng-click="grid.appScope.ShowLihatInvoiceMasukById(row)">{{row.entity.InvoiceNo}}</a>'
			},
			{ name: "Tipe Invoice Masuk", field: "InvoiceTypeName" },
			{ name: "Nomor Invoice Vendor", field: "InvoiceVendorNo" },
			{ name: "Tanggal Jatuh Tempo", field: "MaturityDate", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nama Vendor", field: "VendorName" },
			{ name: "Nominal Invoice", field: "Nominal", cellFilter: 'rupiahC' },
			{ name: "Description", field: "Description", visible: false },
		],
		onRegisterApi: function (gridApi) {
			$scope.GridApiInvoiceMasukList = gridApi;
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				$scope.InvoiceMasukList_UIGrid_Paging(pageNumber, pageSize);
				paginationOptions_Main.pageNumber = pageNumber;
				paginationOptions_Main.pageSize = pageSize;
			});
			gridApi.core.on.renderingComplete($scope, function () {
				$scope.MainInvoiceMasuk_SelectBranch = user.OutletId;
				$scope.InvoiceMasukList_UIGrid_Paging(1, 10);
			});

		}
	};

	$scope.InvoiceMasukList_UIGrid_Paging = function (pageNumber, pageSize) {
		var filterString = '';
		var columnName = '';
		var columnValue = '';
		console.log("uigrid paging $scope.MainInvoiceMasuk_SelectKolom : ", $scope.MainInvoiceMasuk_SelectKolom);
		if ($scope.MainInvoiceMasuk_SelectKolom != undefined)
			columnName = $scope.MainInvoiceMasuk_SelectKolom;

		if ($scope.MainInvoiceMasuk_TextFilter != undefined)
			columnValue = $scope.MainInvoiceMasuk_TextFilter;

		filterString = columnName + '|' + columnValue;

		InvoiceMasukFactory.getDataIM(pageNumber, pageSize,
			$filter('date')(new Date($scope.MainInvoiceMasuk_TanggalAwal), 'yyyy-MM-dd'),
			$filter('date')(new Date($scope.MainInvoiceMasuk_TanggalAkhir), 'yyyy-MM-dd'),
			filterString,
			$scope.MainInvoiceMasuk_SelectBranch)
			.then(
				function (res) {
					console.log("res =>", res.data.Result);
					$scope.InvoiceMasukList_UIGrid.data = res.data.Result;
					$scope.InvoiceMasukList_UIGrid.totalItems = res.data.Total;
					$scope.InvoiceMasukList_UIGrid.paginationPageSize = pageSize;
					$scope.InvoiceMasukList_UIGrid.paginationPageSizes = [10, 25, 50];
				}

			);
	}


	$scope.InvoiceMasukPO_UIGrid = {};

	$scope.InvoiceMasukPO_UIGrid = {

		paginationPageSize: 250,
		paginationPageSizes: [250, 500, 1000],
		enableColumnResizing: true,
		paginationPageSizes: null,
		// useCustomPagination: true,
		// useExternalPagination: true,
		enableFiltering: true,
		multiSelect: true,
		columnDefs: [
			{ name: "POId", field: "POId", visible: false },
			{ name: "Nomor PO", field: "NomorPO",filter: {
				type: uiGridConstants.filter.SELECT} },
			{ name: "Tanggal PO", field: "TanggalPO", cellFilter: 'date:\"dd-MM-yyyy\"' },
			{ name: "Nomor GR", field: "NomorGR" },
			{ name: "Tanggal GR", field: "TanggalGR" },
			{ name: "DPP ", field: "DPP", cellFilter: 'rupiahC' }
		],
		onRegisterApi: function (gridApi) {
			$scope.InvoiceMasukPOGridApi = gridApi;
			console.log('TEST',gridApi)
			gridApi.selection.on.rowSelectionChanged($scope, function (row) {
				console.log("cek Selected", row)
				if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar'){ $scope.multiSelectedOPL(row.entity.NomorPO,row.isSelected,'single');} //CR OPL in Finance
			});
			gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
				console.log("Next paging",pageNumber,pageSize);
				var tmppageNumber = 1;
				for(var i = 1; i<pageNumber; i++){
					tmppageNumber +=10
				}
				// if(pageNumber > 1 ){
				// 	pageNumber += 10;
				// }
				console.log("Next paging",tmppageNumber,pageSize);
				// $scope.InvoiceMasukPO_UIGrid_Paging(pageNumber, pageSize);
				// console.log("Next paging",pageNumber);
			});
			gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) { //CR OPL in Finance Start
				if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar'){ $scope.multiSelectedOPL(rows,null,'multi');}
				console.log('wewe',rows);
			}); //CR OPL in Finance End
		}
	};

	$scope.InvoiceMasukPO_UIGrid_Paging = function (pageNumber, pageSize) {
		
		var tmpFlag = 0;
		if( $scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal PO'|| $scope.TambahInvoiceMasuk_POSelectKolom == 'Tanggal GR'){
			tmpFlag=1;
		}else{
			tmpFlag=0;
		}
		// 1, $scope.tmptotalData,
		// 		$scope.TambahInvoiceMasuk_NamaVendor, $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
		// 		$scope.TambahInvoiceMasuk_SelectBranch , $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice, $scope.TambahInvoiceMasuk_POTextFilter, $scope.TambahInvoiceMasuk_POSelectKolom,tmpFlag
		InvoiceMasukFactory.getDataPOByVendorName(pageNumber, pageSize,
			$scope.tmpReplece + "|" + $scope.TambahInvoiceMasuk_OtherTypeNamaVendor_TujuanPembayaran_IdPelangganVendor , $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
			$scope.TambahInvoiceMasuk_SelectBranch , $scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice, $scope.TambahInvoiceMasuk_POTextFilter, $scope.TambahInvoiceMasuk_POSelectKolom, tmpFlag, $scope.IsNonPKP)
			.then(
				function (res) {
					console.log("res =>", res.data.Result);
					$scope.InvoiceMasukTambah_POList = res.data.Result;
					for(var i in res.data.Result){
						res.data.Result[i].TanggalPO = $scope.changeFormatDate(res.data.Result[i].TanggalPO);
					}
					$scope.InvoiceMasukPO_UIGrid.data = $scope.InvoiceMasukTambah_POList;
					$scope.InvoiceMasukPO_UIGrid.totalItems = res.data.Total;
					$scope.InvoiceMasukPO_UIGrid.paginationPageSize = 250;
					// $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Order Pekerjaan Luar' ? $scope.InvoiceMasukPO_UIGrid.paginationPageSizes = [25, 50, 250] :
						$scope.InvoiceMasukPO_UIGrid.paginationPageSizes = [250, 500, 1000]; //CR OPL in Finance

					$scope.tmptotalData = res.data.Total;

				}

			);
	}

	$scope.InvoiceMasukPODitagih_UIGrid = {};
	$scope.InvoiceMasukPODitagih_Data = [];
	$scope.TotalDPPAll = 0;

	$scope.InvoiceMasukPODitagih_UIGrid = {
		data: $scope.InvoiceMasukPODitagih_Data,
		enableColumnResizing: true,
		showColumnFooter: true,
		columnDefs: $scope.InvoiceMasukPODitagih_UIGrid_ColumnDefs,
		onRegisterApi: function (gridApi) {
			// $scope.InvoiceMasukPODitagihGridApi = gridApi;
			console.log("gridAPI  :", gridApi);
			if (gridApi.edit != undefined) {
				$scope.InvoiceMasukPODitagihGridApi = gridApi;
				gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDefs, newValue, oldValue) {
					// console.log("test", rowEntity);
					// console.log("colDef ", colDefs);
					// if(newValue > rowEntity.DPP){
					// 	var newValue = 0;   
					// 	rowEntity[colDefs.name] = newValue;
					// 		bsNotify.show(
					// 			{
					// 				title: "Peringatan",
					// 				content: "Nominal Pembayaran tidak boleh lebih dari Saldo.",
					// 				type: 'warning'
					// 			}
					// 		);  
					// }else if(newValue <= 0){
					// 	var newValue = 0;
					// 		rowEntity[colDefs.name] = newValue;   
					// 		bsNotify.show(
					// 			{
					// 				title: "Peringatan",
					// 				content: "Nominal Pembayaran tidak boleh kurang atau sama dengan 0.",
					// 				type: 'warning'
					// 			}
					// 		);      
					// }
					$scope.TotalDPPAll = 0;
					$scope.InvoiceMasukPODitagih_UIGrid.data.forEach(function (row) {
							console.log("isi row",row);
							if ( row.DPPInvoice != undefined ){
								$scope.TotalDPPAll += row.DPPInvoice ;
							}
							console.log("TotalPembayaranAll",$scope.TotalDPPAll);
					});

					// rowEntity.Selisih = rowEntity.DPP - rowEntity.DPPInvoice;
					rowEntity.Selisih = rowEntity.DPP - rowEntity.DPPInvoice;
					console.log("Selisih", rowEntity.Selisih)
					$scope.autoCalculationDpp();
					$scope.CalculationNominalInvoice();
				});
			}
		}	
	};

	$scope.InvoiceMasukPajak_UIGrid = {};
	$scope.InvoiceMasukPajak_Data = [];

	$scope.InvoiceMasukPajak_UIGrid = {
		enableColumnResizing: true,
		data: $scope.InvoiceMasukPajak_Data,
		columnDefs: $scope.InvoiceMasukPajak_UIGrid_ColumnDefs,
		onRegisterApi: function (gridApi) {
			$scope.InvoiceMasukPajakGridApi = gridApi;
		}
	};

	$scope.InvoiceMasukRangka_UIGrid = {};
	$scope.InvoiceMasukRangka_UIGrid_View = {};
	$scope.InvoiceMasukRangka_Data = [];

	$scope.InvoiceMasukRangka_UIGrid = {
		data: $scope.InvoiceMasukRangka_Data,
		enableColumnResizing: true,
		showColumnFooter: true,
		columnDefs: [
			{ name: "FrameId", field: "FrameId", visible: false },
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false},
			{
				name: "Nominal Tagihan Free Service", field: "FreeServiceNominal", cellFilter: 'currency:"":2',aggregationType: uiGridConstants.aggregationTypes.sum,
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				},footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.TotalTagihan | number:2}}</div>',
			},
			{
				name: "Action",
				cellTemplate: '<button class="btn primary" ng-click="grid.appScope.TambahInvoiceMasuk_DeleteRangka(row)">Hapus</button>', enableCellEdit: false
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.InvoiceMasukRangkaGridApi = gridApi;
			gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDefs, newValue, oldValue) {
				$scope.TotalTagihan = 0;
					$scope.InvoiceMasukRangka_UIGrid.data.forEach(function (row) {
							console.log("isi row",row);
							if ( row.FreeServiceNominal != undefined ){
								$scope.TotalTagihan += row.FreeServiceNominal ;
							}
							console.log("Total Tagihan Free Service",$scope.TotalTagihan);
					});

				$scope.autoCalculationDpp();
				$scope.CalculationNominalInvoice();
			});
		}
	};

	// View Free Service
	$scope.InvoiceMasukRangka_UIGrid_View = {
		data: $scope.InvoiceMasukRangka_Data,
		enableColumnResizing: true,
		columnDefs: [
			{ name: "FrameId", field: "FrameId", visible: false },
			{ name: "Nomor Rangka", field: "FrameNo", enableCellEdit: false },
			{
				name: "Nominal Tagihan Free Service", field: "FreeServiceNominal", cellFilter: 'rupiahC',
				enableCellEdit: true, type: 'number',
				cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'canEdit';
				}
			},
			{
				name: "Action",
				cellTemplate: '<button class="btn primary" ng-click="grid.appScope.TambahInvoiceMasuk_DeleteRangka(row)">Hapus</button>', enableCellEdit: false,
				visible : false
			}
		],
		onRegisterApi: function (gridApi) {
			$scope.InvoiceMasukRangkaGridApi = gridApi;
		}
	}; 

	$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer = {

		paginationPageSizes: [25, 50, 100],
		paginationPageSize: 25,
		useCustomPagination: false,
		useExternalPagination: false,
		displaySelectionCheckbox: false,
		enableColumnResizing: true,
		canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		enableFullRowSelection: true,
		columnDefs: [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false }
		],

		onRegisterApi: function (gridApi) {
			$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI = gridApi;
		}
	};


	$scope.TambahInvoiceMasuk_TujuanPembayaran_NamaPelangganVendor_Click = function () {
		// if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
		// 	$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		// }
		$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer.columnDefs = [
			{ name: "Id", displayName: "Id", field: "Id", enableCellEdit: false, enableHiding: false, visible: false },
			//{name:"InvoiceMasukNumber", displayName:"Nomor Advanced Payment", field:"InvoiceMasukNumber", enableCellEdit: false, enableHiding: false},
			{ name: "DataCode", displayName: "Code", field: "VendorCode", enableCellEdit: false, enableHiding: false },
			{ name: "DataName", displayName: "Name", field: "Name", enableCellEdit: false, enableHiding: false },
			{ name: "DataAlamat", displayName: "Address", field: "Address", enableCellEdit: false, enableHiding: false }
		];
		angular.element('#ModalTambahInvoiceMasuk_GetSOVendorCustomer').modal('setting', { closeable: false }).modal('show');

		// InvoiceMasukFactory.getSOVendorList($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType, '', $scope.TambahInvoiceMasuk_SelectBranch).then(
		// 	function (res) {
		// 		$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer.data = res.data.Result;
		// 		angular.element('#ModalTambahInvoiceMasuk_GetSOVendorCustomer').modal('setting', { closeable: false }).modal('show');
		// 	},
		// 	function (err) {
		// 		console.log("err=>", err);
		// 	}
		// );

	};

	$scope.Batal_TambahInvoiceMasuk_GetSOVendorCustomer = function () {
		angular.element('#ModalTambahInvoiceMasuk_GetSOVendorCustomer').modal('hide');
		if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
			$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
	}

	$scope.Pilih_TambahInvoiceMasuk_GetSOVendorCustomer = function () {
		if ($scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.selection.getSelectedCount() > 0) {
			$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.selection.getSelectedRows().forEach(function (row) {
				$scope.TambahInvoiceMasuk_OtherTypeNamaVendor = row.Name;
				$scope.TambahInvoiceMasuk_OtherTypeNamaVendor_TujuanPembayaran_IdPelangganVendor = row.Id;
			});

			angular.element('#ModalTambahInvoiceMasuk_GetSOVendorCustomer').modal('hide');
		}
		else {
			//alert('Belum ada data yang dipilih');
			bsNotify.show({
				title: "Warning",
				content: 'Belum ada data yang dipilih !',
				type: 'warning'
			});
		}
		if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
			$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
	}
	$scope.optionsTambahInvoiceMasukNamaVendor_SelectKolom = [{ name: "Code", value: "Code" }, { name: "Name", value: "Name" }, { name: "Address", value: "Address" }];
	// $scope.TambahInvoiceMasukNamaVendor_Filter_Clicked = function () {
	// 	if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == null || $scope.TambahInvoiceMasukNamaVendor_SelectKolom == '')
	// 		$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();
	// 	else
	// 		$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();

	// 	if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Code')
	// 		nomor = 2;
	// 	else if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Name')
	// 		nomor = 3;
	// 	else if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Address')
	// 		nomor = 4
	// 	else
	// 		nomor = 0

	// 	if (!angular.isUndefined($scope.TambahInvoiceMasukNamaVendor_SelectKolom) && nomor != 0) {
	// 		$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInvoiceMasukNamaVendor_TextFilter;
	// 	}
	// 	$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.queueGridRefresh();
	// }
	//New FIlter Vendor
	$scope.TambahInvoiceMasukNamaVendor_Filter_Clicked = function () {
		if($scope.TambahInvoiceMasuk_SelectBranch == "" || $scope.TambahInvoiceMasuk_SelectBranch == undefined || $scope.TambahInvoiceMasuk_SelectBranch == null){
			$scope.TambahInvoiceMasuk_SelectBranch = $scope.user.OutletId.toString();
		}
		if($scope.TambahInvoiceMasukNamaVendor_TextFilter == undefined || $scope.TambahInvoiceMasukNamaVendor_TextFilter == null || $scope.TambahInvoiceMasukNamaVendor_TextFilter == "" ){
			$scope.tmpReplece = $scope.TambahInvoiceMasukNamaVendor_TextFilter;
		}else{
			$scope.tmpReplece = $scope.TambahInvoiceMasukNamaVendor_TextFilter.replace(/\&/g, "%26");
		}
		InvoiceMasukFactory.getSOVendorList($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType, $scope.tmpReplece, $scope.TambahInvoiceMasuk_SelectBranch).then(
				function (res) {
					$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer.data = res.data.Result;
				},
				function (err) {
					console.log("err=>", err);
				}
			);
			if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == null || $scope.TambahInvoiceMasukNamaVendor_SelectKolom == '')
				$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();
			else
				$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.clearAllFilters();
	
			if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Code')
				nomor = 2;
			else if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Name')
				nomor = 3;
			else if ($scope.TambahInvoiceMasukNamaVendor_SelectKolom == 'Address')
				nomor = 4
			else
				nomor = 0
	
			if (!angular.isUndefined($scope.TambahInvoiceMasukNamaVendor_SelectKolom) && nomor != 0) {
				$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.columns[nomor].filters[0].term = $scope.TambahInvoiceMasukNamaVendor_TextFilter;
			}
			$scope.ModalTambahInvoiceMasuk_GetSOVendorCustomer_gridAPI.grid.queueGridRefresh();
	}

	$scope.TambahInvoiceMasuk_TipeInvoiceMasukNotice_Changed = function(){
		$scope.InvoiceMasukPO_UIGrid.data = [];
		$scope.InvoiceMasukPODitagih_UIGrid.data = [];
	}
	
	$scope.TambahInvoiceMasuk_RincianPajak_DPP_Blur = function () {
		console.log("test",$scope.TambahInvoiceMasuk_RincianPajak_DPP);
		console.log("test",parseFloat($scope.TambahInvoiceMasuk_RincianPajak_DPP).toFixed(2));

		if ($scope.TambahInvoiceMasuk_RincianPajak_DPP == null || $scope.TambahInvoiceMasuk_RincianPajak_DPP == '' || $scope.TambahInvoiceMasuk_RincianPajak_DPP == undefined) {
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = "0.00";
		}

		$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.toString();
		var SisaFloat = "";
		if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-3);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_RincianPajak_DPP);
		}
		else if ($scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2,1) == "."){
			SisaFloat = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr($scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.substr(0,$scope.TambahInvoiceMasuk_RincianPajak_DPP.length-2);
			console.log("testSisaFloat",SisaFloat);
			console.log("testSisaFloat",$scope.TambahInvoiceMasuk_RincianPajak_DPP);
		}
		var val = $scope.TambahInvoiceMasuk_RincianPajak_DPP.split(".").join("");
		console.log("cari penyebab",val);
		val = val.replace(/[a-zA-Z\s]/g, '');
		val = String(val).split("").reverse().join("")
			.replace(/(\d{3}\B)/g, "$1.")
			.split("").reverse().join("");

		var str_array = val.split('.');

		var NominalValid = true;
		var satu = 0;
		var dua = 0;
		for (var i = 0; i < str_array.length; i++) {
			if (str_array[i].length == 1) {
				satu = satu + 1;
			}
			else if (str_array[i].length == 2) {
				dua = dua + 1;
			}

			if (satu > 1 || dua > 1 || (satu > 0 && dua > 0)) {
				NominalValid = false;
				break;
			}
		}

		if (!NominalValid) {
			$scope.TambahInvoiceMasuk_RincianPajak_DPP_Blur(val);
		}
		else {
			if(val == ""){
				val = "0.00";
			}

			if (SisaFloat == "undefined" || !SisaFloat){
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+".00";
				$scope.TambahInvoiceMasuk_RincianPajak_DPPforParseFloat = val.split(".").join("")+".00";
			}
			else{
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = val+SisaFloat;
				$scope.TambahInvoiceMasuk_RincianPajak_DPPforParseFloat = val.split(".").join("") + SisaFloat;
				console.log("testing",val+SisaFloat);
				console.log("testing",$scope.TambahInvoiceMasuk_RincianPajak_DPPforParseFloat);
			}
		}
	};

	$scope.givePattern = function (item, event) {
		console.log("item", item);
		console.log("event", event.which);
		console.log("event", event);
		if(item.includes('.')){
			var tmpSplit = item.split('.');
			if(tmpSplit[1].length > 2){
				tmpSplit[1] = tmpSplit[1].substr(0,2);
				$scope.TambahInvoiceMasuk_RincianPajak_DPP = tmpSplit[0] + '.' + tmpSplit[1];
				return false;
			}	
		}
		if (event.which != 190 && event.which != 110 && (event.which < 48 || event.which > 57) && (event.which < 96 || event.which > 105)){
			event.preventDefault();
			console.log("disini", event.which);
			console.log("$scope.TambahInvoiceMasuk_RincianPajak_DPP", $scope.TambahInvoiceMasuk_RincianPajak_DPP);
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.replace(/[^0-9.]+/g,'');
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP.replace(event.key,"");
			return false;
		} else {
			$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TambahInvoiceMasuk_RincianPajak_DPP;
			return;
		}
	};

	//CR OPL in Finance Start
	$scope.tmpDataOpl = [];
	$scope.multiSelectedOPL= function (noPo,status,action) {
		console.log('NoPo',noPo,status)
		if(action == 'single'){

			if($scope.tmpDataOpl.length > 0){
				for(var i = 0; i <  $scope.tmpDataOpl.length; i++){
					if($scope.tmpDataOpl[i].NomorPO == noPo){
						$scope.tmpDataOpl.splice(i, 1);
						i=-1; continue;
					}
				}
			}
			
			for (var i = 0; i < $scope.InvoiceMasukPOGridApi.grid.rows.length; i++) {
				if(noPo == $scope.InvoiceMasukPOGridApi.grid.rows[i].entity.NomorPO){
					console.log('CEKKED',$scope.InvoiceMasukPOGridApi.grid.rows[i].entity.$$hashKey)
					Object.assign($scope.InvoiceMasukPOGridApi.grid.rows[i], {isSelected:status})
					console.log("SELECTEDDD",$scope.InvoiceMasukPOGridApi);
				}
			}

			if(status == true){
				InvoiceMasukFactory.getDataPOByVendorName(1, 1000,
					$scope.tmpReplece, $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
					$scope.TambahInvoiceMasuk_SelectBranch , '', noPo, 'Nomor PO',0)
					.then(
						function (res) {
							console.log("res =>", res.data.Result);
							
							for(var i in res.data.Result){
								res.data.Result[i].TanggalPO = $scope.changeFormatDate(res.data.Result[i].TanggalPO);
								$scope.tmpDataOpl.push(res.data.Result[i]) ;
							}
							
						}
					);
			}else{
				if($scope.tmpDataOpl.length > 0){
					for(var i = 0; i <  $scope.tmpDataOpl.length; i++){
						if($scope.tmpDataOpl[i].NomorPO == noPo){
							$scope.tmpDataOpl.splice(i, 1);
							i=-1; continue;
						}
					}
				}
			}
		}else{
			$scope.tmpDataOpl = [];
			$scope.tmpDataOpl = Object.assign($scope.InvoiceMasukPOGridApi.selection.getSelectedRows());
			// var tmpArrayOpl = Object.assign($scope.InvoiceMasukPOGridApi.selection.getSelectedRows());
			// var obj={};
			// var uniqueArr=[];
			// for(var i = 0; i < tmpArrayOpl.length; i++){ 
			//    if(!obj.hasOwnProperty(tmpArrayOpl[i].NomorPO)){
			// 	   obj[tmpArrayOpl[i].NomorPO] = tmpArrayOpl[i].NomorPO;
			// 	   uniqueArr.push(tmpArrayOpl[i].NomorPO);
			//    }
			// }
			// console.log('uniqueArr',uniqueArr)
			// console.log('$scope.InvoiceMasukPOGridApi.selection.getSelectedRows()',$scope.InvoiceMasukPOGridApi.selection.getSelectedRows())

			// for(var i in uniqueArr){
			// 	InvoiceMasukFactory.getDataPOByVendorName(1, 1000,
			// 		$scope.tmpReplece, $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceId,
			// 		$scope.TambahInvoiceMasuk_SelectBranch , '', uniqueArr[i], 'Nomor PO',0)
			// 		.then(
			// 			function (res) {
			// 				console.log("res =>", res.data.Result);
							
			// 				for(var i in res.data.Result){
			// 					res.data.Result[i].TanggalPO = $scope.changeFormatDate(res.data.Result[i].TanggalPO);
			// 					$scope.tmpDataOpl.push(res.data.Result[i]) ;
			// 				}
							
			// 			}
			// 		);
			// }
		}
		
	}//CR OPL in Finance End
	$scope.autoCalculationDpp = function (){
		var totFreeService = 0;
		console.log('AUTO DPP',$scope.TambahInvoiceMasuk_RincianPajak_JenisPajak)
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType != 'Unit' ){
			if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Accrued Free Service' && !$scope.modeFreeServicePph23){ // ppn
				$scope.InvoiceMasukRangka_UIGrid.data.forEach(function (row) {
					console.log("isi row",row);
					if ( row.FreeServiceNominal != undefined ){
						totFreeService += row.FreeServiceNominal ;
					}
					console.log("TotalPembayaranAll",totFreeService);
				});

				if($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak != undefined || $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak != null){
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = totFreeService.toFixed(2);
				}else{
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = 0;
				}	
			}else if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Accrued Free Service' && $scope.modeFreeServicePph23){ //pph 23
				if($scope.TambahDaftarNomorRangka != null || $scope.TambahDaftarNomorRangka != undefined){
					for(var i in $scope.InvoiceMasukRangka_UIGrid.data){
						if($scope.InvoiceMasukRangka_UIGrid.data[i].FrameNo == $scope.TambahDaftarNomorRangka){
							$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.InvoiceMasukRangka_UIGrid.data[i].FreeServiceNominal.toFixed(2);
						}
					}
				}else{
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = 0;
				}
			}
			else{
				if($scope.TambahInvoiceMasuk_RincianPajak_JenisPajak != undefined || $scope.TambahInvoiceMasuk_RincianPajak_JenisPajak != null){
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.TotalDPPAll.toFixed(2);
				}else{
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = 0;
				}	
			}
		$scope.TambahInvoiceMasuk_RincianPajak_DPP_Blur(); 
		}
	}

	// CR5 #10 Finance Start
	$scope.TambahInvoiceMasuk_IsNonPkp_Changed = function(){
		$scope.InvoiceMasukPO_UIGrid.data = [];
		$scope.InvoiceMasukClearUIGridData();
	}
	// CR5 #10 Finance End

	// CR5P #37
	$scope.modeUploadAksesorisStandard = 0;
	$scope.isModeUpload;
	$scope.modeUpload = function(){
		console.log('aktif', $scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType,$scope.modeUploadAksesorisStandard)
		if($scope.TambahInvoiceMasuk_TipeInvoiceMasuk.InvoiceType == 'Aksesoris Standard' && $scope.modeUploadAksesorisStandard == 1){
			$scope.isModeUpload = 1
		}else{
			$scope.isModeUpload = 0
		}

		$scope.TambahInvoiceMasuk_TipeInvoiceMasuk_Changed();
		// if($scope.modeUploadAksesorisStandard == 1){
		// 	$scope.InvoiceMasukPO_UIGrid.columnDefs[9].visible = true;
		// }else{
		// 	$scope.InvoiceMasukPO_UIGrid.columnDefs[9].visible = false;
		// }
	}

	$scope.downloadTemplateAksesorisStandard_Clicked = function(){
		var excelData = [];
		var fileName = "InvoiceMasukAksesorisStandard-Template";

		excelData.push({
			Model: 0,
			Tipe: 0,
			Katashiki: 0
		});

		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.loadXLS = function(){
		var myEl = angular.element(document.querySelector('#uploadAksesorisStandard')); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);
		$scope.AksesorisStandard_ExcelData = [];

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {

			console.log("myEl : ", myEl);
			console.log("json : ", json);
			$scope.AksesorisStandard_ExcelData = json;
			// $scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

			// $scope.MasterSelling_BBN_Upload_Clicked();
			myEl.val('');
		});
	}
	// CR5 #38 Finance Start
	$scope.TambahDaftaNomorRangka_Changed = function (){
		console.log('norangka', $scope.TambahDaftarNomorRangka);
		if($scope.modeFreeServicePph23){
			for(var i in $scope.InvoiceMasukRangka_UIGrid.data){
				if($scope.InvoiceMasukRangka_UIGrid.data[i].FrameNo == $scope.TambahDaftarNomorRangka){
					$scope.TambahInvoiceMasuk_RincianPajak_DPP = $scope.InvoiceMasukRangka_UIGrid.data[i].FreeServiceNominal.toFixed(2);
					$scope.TambahInvoiceMasuk_RincianPajak_NomorReferensi = $scope.TambahDaftarNomorRangka;
				}
			}
		}

	}

	$scope.Batal_ModalSimpanFreeService = function () {
		angular.element('#ModalSimpanFreeService').modal('hide');
	};

	$scope.Simpan_ModalSimpanFreeService = function (){
		$scope.SimpanData();
		angular.element('#ModalSimpanFreeService').modal('hide');
	} // CR5 #38 Finance End

	//==========================UI GRID END==========================//
});
app.filter('rupiahC', function () {
	return function (val) {
		console.log('value ' + val);
		if (angular.isDefined(val)) {
			while (/(\d+)(\d{3})/.test(val.toString())) {
				val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
			}
			return val;
		}

	};
});

app.filter('nullDateFilter', function ($filter) {
	return function (input) {
		//var originalFilter = $filter('date:\"dd-MM-yyyy\"');
		return input == 'Invalid Date' ? '' : $filter('date')(input, "dd-MM-yyyy");
	};
});
