angular.module('app')
	.factory('InvoiceMasukFactory', function ($http, CurrentUser) {
	var user = CurrentUser.user();
	
	return{
		changeFormatDate : function(item) {
			var tmpAppointmentDate = item;
			var finalDate
            // console.log("changeFormatDate item", item);
			// tmpAppointmentDate = new Date(tmpAppointmentDate);
			// console.log("changeFormatDate tmpAppointmentDate", tmpAppointmentDate);
            // if (tmpAppointmentDate !== null || tmpAppointmentDate !== 'undefined') {
            //     var yyyy = tmpAppointmentDate.getFullYear().toString();
            //     var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            //     var dd = tmpAppointmentDate.getDate().toString();
            //     finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            // } else {
            //     finalDate = '';
            // }
			// console.log("changeFormatDate finalDate", finalDate);
			tmpAppointmentDate = item.split('-');
			finalDate = tmpAppointmentDate[2] + '-'+tmpAppointmentDate[1]+'-'+tmpAppointmentDate[0];
            return finalDate;
        },
		getDataBranchComboBox:function () {
			console.log("factory.getDataBranchComboBox");
			var url = '/api/fe/AccountBank/GetDataBranchComboBox/';
			var res = $http.get(url);
			return res;
		},

		getDataAllBranchGroupComboBox : function () {
			console.log("factory.getDataAllBranchGroupComboBox");
			var url = '/api/fe/AccountBank/getDataAllBranchGroupComboBox/';
			var res = $http.get(url);
			return res;
		},

		getDataBranch : function (BranchId) {
			var url = '/api/fe/AccountBank/GetDataBranch/' + BranchId;
			var res = $http.get(url);
			return res;
		},
		
		getDataIM:function(Start, Limit, dateStart, dateEnd, filterString, outletId){
			var url = '/api/fe/invoicemasukCR/?start=' + Start + '&limit='+ Limit + '&datestart=' + dateStart + '&dateend=' + dateEnd + '&outletid=' + outletId + '&filterString=' + filterString;
			console.log("url get Data IM : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getDataPOByVendorName: function(start, limit, vendorName, invoiceMasukTypeId, outletId,notice, searchText, searchType, flag,isNonPkp ){
			
			if(flag == 1){
				searchText = this.changeFormatDate(searchText);
			}
			var url = '/api/fe/financepo/getbyvendornameCR/?start=' + start + '&limit=' + limit + '&vendorname=' + vendorName + '&outletid=' + outletId + '&incominginvoicetypeid=' + invoiceMasukTypeId+'&notice='+notice + '|' +searchText + '|' + searchType+'&nonPkp='+isNonPkp;
			console.log("url DummyPOByDate : ", url);
			var res=$http.get(url);
			return res; 
		},
		
		getVendorByName:function(VendorName, invoiceMasukTypeId, outletId){
			var url = '/api/fe/financevendor/getdatabyvendorname/?VendorName=' + VendorName + '&outletid=' + outletId + '&incominginvoicetypeId=' + invoiceMasukTypeId;
			console.log("url getDataVendor : ", url);
			var res=$http.get(url);
			return res;
		},
		getVendorById:function(InvoiceId, InvoiceTypeId, outletId){
			var url = '/api/fe/financevendor/GetVendorbyInvoiceId/?InvoiceId=' + InvoiceId + '&InvoiceTypeId=' + InvoiceTypeId;
			console.log("url getDataVendorId : ", url);
			var res=$http.get(url);
			return res;
		},

		getVendorInfo:function(tipe, name , id, outletId) {
			var FilterData = [{PIType : tipe, VendorName : name, VendorId: id,ShowAll : 0, OutletId: outletId}];
			var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			var res=$http.get(url);
			console.log("getVendorInfo url : ", url);
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			//console.log('res=>',res);
			return res;
		},
		
		getTaxList:function(){
			var url = '/api/fe/financetaxtype/getalldata';
			console.log("url getTaxList : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getInvoiceMasukType:function(){
			var url = '/api/fe/InvoiceMasuk/GetInvoiceMasukType/';
			console.log("url getTaxList : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getInvoiceMasukPOResult:function(invoiceId, invoiceTypeId){
			var url = '/api/fe/InvoiceMasuk/GetInvoiceMasukPOResultType/?InvoiceId=' + invoiceId + '&InvoiceTypeId=' + invoiceTypeId;
			console.log("url getTaxList : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getInvoiceMasukTaxResult:function(invoiceId){
			var url = '/api/fe/InvoiceMasuk/GetInvoiceMasukTaxResultType/' + invoiceId;
			console.log("url getTaxList : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getBranchData:function(branchName){
			var url = '/api/fe/FinanceBranch/GetBranchByName/' + branchName;
			console.log("url getBranchData : ", url);
			var res=$http.get(url);
			return res;
		},
		
		getFreeServiceBranchData:function(invoiceId){
			var url = '/api/fe/InvoiceMasuk/GetInvoiceMasukFreeServiceBranch/?InvoiceId=' + invoiceId;
			console.log("url getFreeServiceBranchData : ", url);
			var res=$http.get(url);
			return res;
		},
		
		createInvoiceMasuk:function(inputData){
			var url = '/api/fe/InvoiceMasuk/SubmitDataCR/';
			var param = JSON.stringify(inputData);
			console.log("object input invoice masuk ", param);
			var res=$http.post(url, param);
			return res;
		},
		
		getFrameData:function(frameNo){
			var url = '/api/fe/FinanceFrame/GetFrameByFrameNo/' + frameNo;
			console.log("url getFrameData : ", url);
			var res=$http.get(url);
			return res;
		},
		
		deleteDataInvoiceMasuk: function(invoiceId) {
			var url = '/api/fe/InvoiceMasuk/deletedata/' + invoiceId;
			var res=$http.delete(url);
			return res;
		},
		getSOVendorList: function(tipe, name, outletId) {
			var FilterData = [{PIType : tipe, VendorName : name, ShowAll : 0, OutletId: outletId}];
			//var res=$http.get('/api/fe/PaymentInstructionVendor/SelectData/Start/1/Limit/20/filterData/' + JSON.stringify(FilterData));
			var url = '/api/fe/PaymentInstructionVendorCR/SelectData?start=1&limit=20&filterData=' + JSON.stringify(FilterData);
			console.log("url getSOVendorList", url);
			var res=$http.get(url);
			//console.log('res=>',res);
			return res;
		},		
		getBranchByOutletId: function(outletId){
			var url = '/api/fe/FinanceBranch/GetBranchByOutletId/?OutletId=' + outletId;
			console.log("url getBranchByOutletId : ", url);
			var res=$http.get(url);
			return res;
		},
	}
});
