angular.module('app')
.controller('NotifikasiIncomingPaymentController', function ($scope, $http, $filter, CurrentUser, NotifikasiIncomingPaymentFactory, $timeout, bsNotify) {
	$scope.user = CurrentUser.user();
	$scope.MainNotifikasiIncomingPayment_Show = true;
	//$scope.NotifikasiIncomingPayment_TemplateEmail_Disabled = false;
	$scope.UserData={};
	$scope.Edit = false;
	$scope.ActiveRow = {};
	$scope.NotifikasiIncomingPayment_Tag_UIGrid = {
		paginationPageSizes: [10,25,50],
		paginationPageSize: 10,
		useCustomPagination: false,
		useExternalPagination : false,
		displaySelectionCheckbox: false,
		//canSelectRows: true,
		enableFiltering: true,
		enableSelectAll: false,
		multiSelect: false,
		//enableFullRowSelection: true,	 
		columnDefs: [
				{name:"Id", displayName:"Id", field:"TagId", enableCellEdit: false, enableHiding: false, visible:false},
				{name:"Tag", displayName:"Tag", field:"Tag", enableCellEdit: false, enableHiding: false},
				{name:"TagDescription", displayName:"Keterangan",field:"TagDescription", enableCellEdit:false	}
			],
		onRegisterApi: function(gridApi) {
			$scope.NotifikasiIncomingPayment_Tag_gridAPI = gridApi;
		}
	};

	$scope.RefreshData=function()
	{
		NotifikasiIncomingPaymentFactory.getDataMaster($scope.user.OrgId).then(
			function(res) {
				//onsole.log('data ', JSON.stringify(res));
				if (res.data.Result.length > 0) {
					$scope.NotifikasiIncomingPayment_EmailEnabled = res.data.Result[0].NotifyByEmailEnabled;
					$scope.NotifikasiIncomingPayment_SMSEnabled = res.data.Result[0].NotifyBySMSEnabled;
					$scope.NotifikasiIncomingPayment_JudulEmail =  res.data.Result[0].EmailHeader;
					$scope.NotifikasiIncomingPayment_TemplateEmail = res.data.Result[0].EmailBody;
					$scope.NotifikasiIncomingPayment_TemplateSMS = res.data.Result[0].SMSBody;
				}
			}, 
			function(err) {
				console.log('err ', err);
			}
		);

		var dataNotif = [];
		dataNotif.push({'Tag': '<#1>', 'TagDescription': 'Nomor SPK'});
		dataNotif.push({'Tag': '<#2>', 'TagDescription': 'Nomor Incoming Payment/Alokasi Unknown/Kuitansi'});
		dataNotif.push({'Tag': '<#3>', 'TagDescription': 'Nominal yang diterima untuk pembayaran pada kuitansi'});

		$scope.NotifikasiIncomingPayment_Tag_UIGrid.data = dataNotif;

		// NotifikasiIncomingPaymentFactory.getDataTagList($scope.user.OrgId).then(
		// 	function(res) {
		// 			$scope.NotifikasiIncomingPayment_Tag_UIGrid.data = res.data.Result;
		// 	}, 
		// 	function(err) {
		// 		console.log('err ', err);
		// 	}
		// );
	}

	$scope.NotifikasiIncomingPayment_Simpan_Clicked = function()
	{
			data = [{NotifyByEmailEnabled:$scope.NotifikasiIncomingPayment_EmailEnabled,
					NotifyBySMSEnabled:$scope.NotifikasiIncomingPayment_SMSEnabled,
					EmailHeader:$scope.NotifikasiIncomingPayment_JudulEmail,
					EmailBody:$scope.NotifikasiIncomingPayment_TemplateEmail,
					SMSBody:$scope.NotifikasiIncomingPayment_TemplateSMS,
					OutletId:$scope.user.OrgId,
					TagList:  JSON.stringify($scope.NotifikasiIncomingPayment_Tag_UIGrid.data)          
				}]	

		NotifikasiIncomingPaymentFactory.submitData(data).then(
            function(res){
                //$scope.loading=false;
                bsNotify.show(
					{
						title: "Berhasil",
						content: "Berhasil menambah data",
						type: 'success'
					}
				);
                
            },
            function(err){
                console.log("err=>",err);
            }
        );
	}

	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.RefreshData();
});