angular.module('app')
	.factory('NotifikasiIncomingPaymentFactory', function ($http, CurrentUser) {
    var currentUser = CurrentUser.user;
	  var factory={};
	  var debugMode=true;
		
    factory.getDataMaster=function(outletId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/NotifikasiIncomingPayment/SelectData?outletId=' + outletId;
      var res=$http.get(url);  
      return res;			
    };

    factory.getDataTagList=function(outletId) {
      //var url = '/api/fe/Branch/SelectData/Start/0/limit/0/FilterData/0';
      var url = '/api/fe/NotifikasiIncomingPayment/SelectDataTagList?outletId=' + outletId;
      var res=$http.get(url);  
      return res;			
    };

factory.submitData= function(inputData) {
    	//var url = '/api/fe/IncomingInstruction';NotifikasiIncomingPayment
        var url = '/api/fe/NotifikasiIncomingPayment/SubmitData/';
    	var param = JSON.stringify(inputData);

	if (debugMode){console.log('Masuk ke submitData')};
	if (debugMode){console.log('url :'+url);};
	if (debugMode){console.log('Parameter POST :'+param)};
      	var res=$http.post(url, param);
      	
        return res;
    };


	return factory;
});