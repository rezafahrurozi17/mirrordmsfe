var app = angular.module('app');
app.controller('MasterPricingDealerToBranchBBNController', function ($scope, $http, $filter, CurrentUser, bsNotify, bsAlert, MasterPricingDealerToBranchBBNFactory, $timeout) {
	$scope.optionsKolomFilterSatu_BBN = [
																																						// {name: "Dealer Name", value:"DealerName"}, 
																																						// {name: "Province", value:"Province"} ,	 
																																						{ name: "Model", value: "Model" },
																																						{ name: "Tipe", value: "Tipe" },
																																						{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
																																						// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																						{ name: "Katashiki", value: "Katashiki" },
																																						{ name: "Suffix", value: "Suffix" }
																																					];

	$scope.optionsKolomFilterDua_BBN = [
																																					// {name: "Dealer Name", value:"DealerName"}, 
																																					{ name: "Model", value: "Model" },
																																					// {name: "Province", value:"Province"} ,	 
																																					{ name: "Tipe", value: "Tipe" },
																																					{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
																																					// {name: "Nama Model / Grade",value:"NamaModel"}, 
																																					{ name: "Katashiki", value: "Katashiki" },
																																					{ name: "Suffix", value: "Suffix" }
																																				];

	$scope.optionsComboBulkAction_BBN = [{ name: "Delete", value: "Delete" }];

	$scope.optionsComboFilterGrid_BBN = [
																																						{ name: "Model", value: "Model" },
																																						{ name: "Tipe", value: "Tipe" },
																																						{ name: "Katashiki", value: "Katashiki" },
																																						{ name: "Suffix", value: "Suffix" },
																																						{ name: "Pricing Cluster Code", value: "PricingClusterCode" },
																																						{ name: "BBN", value: "BBN" },
																																						{ name: "BBN Service Fee", value: "BBNServiceFee" },
																																						{ name: "Effective Date From", value: "EffectiveDateFrom" },
																																						{ name: "Effective Date To", value: "EffectiveDateTo" },
																																						{ name: "Remarks", value: "Remarks" }
																																					];
																																					
	$scope.btnUpload = "hide";
	$scope.MasterSelling_FileName_Current = '';
	$scope.JenisPE = 'BBN';
	$scope.cekKolom = '';
	$scope.TypePE = 0; //0 = dealer to branch , 1 = tam to dealer
	$scope.MasterSelling_ExcelData = [];
	$scope.ShowFieldGenerate_BBN = true;
	angular.element('#BBNPriceModal').modal('hide');


	$scope.MasterSellingPriceFU_BBN_grid = {
		paginationPageSize: 10,
		paginationPageSizes: [10, 25, 50],
		enableSorting: true,
		enableRowSelection: true,
		multiSelect: true,
		enableSelectAll: true,
		enableColumnResizing: true,
		enableFiltering: true,
		columnDefs: [
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'Model', displayName: 'Model', field: 'Model' },
			{ width: '15%', enableCellEdit: false,  enableHiding: false, name: 'Tipe', displayName: 'Tipe', field: 'Tipe' },
			{ width: '13%', enableCellEdit: false,  enableHiding: false, name: 'Katashiki', displayName: 'Katashiki', field: 'Katashiki' },
			{ width: '06%', enableCellEdit: false,  enableHiding: false, name: 'Suffix', displayName: 'Suffix', field: 'Suffix' },
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'Pricing Cluster Code', displayName: 'Pricing Cluster Code', field: 'PricingClusterCode' },
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'BBN', displayName: 'BBN', field: 'BBN', cellFilter: 'rupiahC', type: "number" },
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'BBN Service Fee', displayName: 'BBN Service Fee', field: 'BBNServiceFee', cellFilter: 'rupiahC', type: "number" },
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'Effective Date From', displayName: 'Effective Date From', field: 'EffectiveDateFrom', cellFilter: 'date:\"dd-MM-yyyy\"'},
			{ width: '10%', enableCellEdit: false,  enableHiding: false, name: 'Effective Date To', displayName: 'Effective Date To', field: 'EffectiveDateTo' , cellFilter: 'date:\"dd-MM-yyyy\"'}, 
			{ width: '25%', enableCellEdit: false,  enableHiding: false, name: 'Remarks', displayName: 'Remarks', field: 'Remarks' },
		],

		onRegisterApi: function (gridApi) {
			$scope.MasterSellingPriceFU_BBN_gridAPI = gridApi;
		}
	};



	$scope.fixDate = function (date) {
		if (date != null || date != undefined) {
			var fix = date.getFullYear() + "-" +
				('0' + (date.getMonth() + 1)).slice(-2) + "-" +
				('0' + date.getDate()).slice(-2)
			return fix;
		} else {
			return null;
		}
	};

	$scope.MasterSelling_BBN_Generate_Clicked = function () {

		if ($scope.filterModel.TanggalFilterStart == 'Invalid Date' || $scope.filterModel.TanggalFilterStart == undefined || $scope.filterModel.TanggalFilterStart == null) {
			$scope.filterModel.TanggalFilterStart = "";
		} else {
			$scope.filterModel.TanggalFilterStart = $scope.fixDate($scope.filterModel.TanggalFilterStart)
		}

		if ($scope.filterModel.TanggalFilterEnd == 'Invalid Date' || $scope.filterModel.TanggalFilterEnd == undefined || $scope.filterModel.TanggalFilterEnd == null) {
			$scope.filterModel.TanggalFilterEnd = "";
		} else {
			$scope.filterModel.TanggalFilterEnd = $scope.fixDate($scope.filterModel.TanggalFilterEnd)
		}

		console.log('model ===>', $scope.filterModel.VehicleModelId);
		console.log('tipe ===>', $scope.filterModel.VehicleTypeId);
		console.log('date_from ===>', $scope.filterModel.TanggalFilterStart);
		console.log('date_to ===>', $scope.filterModel.TanggalFilterEnd);
		console.log('filterModel ===>', $scope.filterModel);

		var filter = [{
			StartDate: $scope.filterModel.TanggalFilterStart,
			EndDate: $scope.filterModel.TanggalFilterEnd,
			VehicleModelId: $scope.filterModel.VehicleModelId,
			VehicleTypeId: $scope.filterModel.VehicleTypeId
		}];


		MasterPricingDealerToBranchBBNFactory.getData(1, 10000, JSON.stringify(filter))
			.then(
				function (res) {
					$scope.MasterSellingPriceFU_BBN_grid.data = res.data.Result;            //Data hasil dari WebAPI
					$scope.MasterSellingPriceFU_BBN_grid.totalItems = res.data.Total;
					$scope.MasterSellingPriceFU_BBN_grid.dataTemp = angular.copy($scope.MasterSellingPriceFU_BBN_grid.data);

				},
				function (err) {
					console.log('error -->', err)
				}
			);
	}



	

	$scope.filterModel = [{VehicleModelId:'', VehicleTypeId:'', TanggalFilterEnd:'', TanggalFilterStart:''}];
	MasterPricingDealerToBranchBBNFactory.getDataModel().then(function (res) {
		$scope.optionsModel = res.data.Result;
		console.log('$scope.optionsModel ==>', $scope.optionsModel)
		return $scope.optionsModel;
	});

	$scope.onSelectModelChanged = function (ModelSelected) {
		console.log('on model change ===>', ModelSelected);
		
		if (ModelSelected == undefined || ModelSelected == null) {
			$scope.filterModel.VehicleModelName = "";
			$scope.filterModel.VehicleModelId = "";
			$scope.optionsTipe = [];
			$scope.filterModel.VehicleTypelName = "";
			$scope.filterModel.VehicleTypeId = "";

		} else {
			$scope.filterModel.VehicleModelName = ModelSelected.VehicleModelName;
			$scope.filterModel.VehicleModelId = ModelSelected.VehicleModelId;
			MasterPricingDealerToBranchBBNFactory.getDaVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
				$scope.optionsTipe = [];
				$scope.filterModel.VehicleTypelName = "";
				$scope.filterModel.VehicleTypeId = "";
				$scope.optionsTipe = res.data.Result;
				return $scope.optionsTipe;
			});
		}
		
	}

	$scope.onSelectTipeChanged = function (TipeSelected) {
		console.log('TipeSelected ===>', TipeSelected);
		if (TipeSelected == undefined || TipeSelected == null) {
			$scope.filterModel.VehicleTypelName = "";
			$scope.filterModel.VehicleTypeId = "";

		} else {
			$scope.filterModel.VehicleTypelName = TipeSelected.Description;
			$scope.filterModel.VehicleTypeId = TipeSelected.VehicleTypeId;
		}
	}

	$scope.FilterGridMasterBBN = function () {
		var inputfilter = $scope.textFilterMasterBBN;
		console.log('$scope.textFilterMasterBBN ===>', $scope.textFilterMasterBBN);

		var tempGrid = angular.copy($scope.MasterSellingPriceFU_BBN_grid.dataTemp);
		var objct = '{"' + $scope.selectedFilterMasterBBN + '":"' + inputfilter + '"}'
		if (inputfilter == "" || inputfilter == null) {
			$scope.MasterSellingPriceFU_BBN_grid.data = $scope.MasterSellingPriceFU_BBN_grid.dataTemp;
			console.log('$scope.MasterSellingPriceFU_BBN_grid.data if ===>', $scope.MasterSellingPriceFU_BBN_grid.data)

		} else {
			$scope.MasterSellingPriceFU_BBN_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
			console.log('$scope.MasterSellingPriceFU_BBN_grid.data else ===>', $scope.MasterSellingPriceFU_BBN_grid.data)
		}
	};



	$scope.formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('');
	};

	$scope.loadXLS = function (ExcelFile) {
		$scope.MasterSelling_ExcelData = [];
		var myEl = angular.element(document.querySelector('#uploadSellingFile_BBN')); //ambil elemen dari dokumen yang di-upload 
		console.log("myEl : ", myEl);

		XLSXInterface.loadToJson(myEl[0].files[0], function (json) {
			for (i = 0; i < json.length; i++) {
				var tglUpload = json[i].EffectiveDateFrom //format 20191231
				var thn = tglUpload.substring(0, 4);
				var bln = tglUpload.substring(4, 6);
				var tgl = tglUpload.substring(6, 8);
				var tgl_jadi = tgl + "-" + bln + "-" + thn;
				json[i].EffectiveDateFrom = tgl_jadi;

				var tglUploadTo = json[i].EffectiveDateTo //format 20191231
				var thnTo = tglUploadTo.substring(0, 4);
				var blnTo = tglUploadTo.substring(4, 6);
				var tglTo = tglUploadTo.substring(6, 8);

				var tgl_jadiTo = tglTo + "-" + blnTo + "-" + thnTo;
				json[i].EffectiveDateTo = tgl_jadiTo;
			}

			$scope.btnUpload = "hide";
			console.log("myEl : ", myEl);
			console.log("json : ", json);
			$scope.MasterSelling_ExcelData = json;
			$scope.MasterSelling_FileName_Current = myEl[0].files[0].name;

			$scope.MasterSelling_BBN_Upload_Clicked();
			myEl.val('');
		});
	}

	$scope.validateColumn = function (inputExcelData) {
		// ini harus di rubah

		// $scope.cekKolom = '';
		// if (angular.isUndefined(inputExcelData.Model)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Model \n';
		// }

		// if (angular.isUndefined(inputExcelData.Tipe)) {
		// 	$scope.cekKolom = $scope.cekKolom + ' Tipe \n';
		// }

		if (angular.isUndefined(inputExcelData.Katashiki)) {
			$scope.cekKolom = $scope.cekKolom + ' Katashiki \n';
		}
		if (angular.isUndefined(inputExcelData.Suffix)) {
			$scope.cekKolom = $scope.cekKolom + ' Suffix \n';
		}
		if (angular.isUndefined(inputExcelData.PricingClusterCode)) {
			$scope.cekKolom = $scope.cekKolom + ' PricingClusterCode\n';
		}
		if (angular.isUndefined(inputExcelData.BBN)) {
			$scope.cekKolom = $scope.cekKolom + ' BBN \n';
		}
		if (angular.isUndefined(inputExcelData.BBNServiceFee)) {
			$scope.cekKolom = $scope.cekKolom + ' BBNServiceFee \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateFrom)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateFrom \n';
		}
		if (angular.isUndefined(inputExcelData.EffectiveDateTo)) {
			$scope.cekKolom = $scope.cekKolom + ' EffectiveDateTo \n';
		}
		if (angular.isUndefined(inputExcelData.Remarks)) {
			$scope.cekKolom = $scope.cekKolom + ' Remarks \n';
		}

		if (
			// angular.isUndefined(inputExcelData.Model) ||
			// angular.isUndefined(inputExcelData.Tipe) ||
			angular.isUndefined(inputExcelData.Katashiki) ||
			angular.isUndefined(inputExcelData.Suffix) ||
			angular.isUndefined(inputExcelData.PricingClusterCode) ||
			angular.isUndefined(inputExcelData.BBN) ||
			angular.isUndefined(inputExcelData.BBNServiceFee) ||
			angular.isUndefined(inputExcelData.EffectiveDateFrom) ||
			angular.isUndefined(inputExcelData.EffectiveDateTo) ||
			angular.isUndefined(inputExcelData.Remarks)) 
			{
			alert('Template file tidak sesuai !');
			return (false);
		}

		return (true);
	}

	$scope.ComboBulkAction_BBN_Changed = function () {
		if ($scope.ComboBulkAction_BBN == "Delete") {
			//var index;
			// $scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {
			// 	index = $scope.MasterSellingPriceFU_BBN_grid.data.indexOf(row.entity);
			// 	$scope.MasterSellingPriceFU_BBN_grid.data.splice(index, 1);
			// });
			var counter = 0;
			angular.forEach($scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows(), function (data, index) {
				//$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
				counter++;
			});

			if (counter > 0) {
				//$scope.TotalSelectedData = $scope.MasterSellingPriceFU_PriceDIO_gridAPI.selection.getSelectedCount();
				$scope.TotalSelectedData = counter;
				bsAlert.alert({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'OK',
					confirmButtonColor: '#8CD4F5',
					cancelButtonText: 'Cancel',
				},
					function() {
						$scope.OK_Button_Clicked();
					},
					function() {
		
					}
				);
			}
		}
	}

	$scope.OK_Button_Clicked = function () {
		angular.forEach($scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows(), function (data, index) {
			$scope.MasterSellingPriceFU_BBN_grid.data.splice($scope.MasterSellingPriceFU_BBN_grid.data.lastIndexOf(data), 1);
		});

		$scope.ComboBulkAction_BBN = "";
		angular.element('#BBNPriceModal').modal('hide');
	}
	$scope.DeleteCancel_Button_Clicked = function () {
		$scope.ComboBulkAction_BBN = "";
		angular.element('#BBNPriceModal').modal('hide');
	}

	$scope.MasterSelling_BBN_Download_Clicked = function () {
		var excelData = [];
		var fileName = "";
		fileName = "MasterSellingBBN";

		if ($scope.MasterSellingPriceFU_BBN_grid.data.length == 0) {
			excelData.push({
				Model: "",
				Tipe: "",
				Katashiki: "",
				Suffix: "",
				PricingClusterCode: "",
				BBN: "",
				BBNServiceFee: "",
				EffectiveDateFrom: "",
				EffectiveDateTo: "",
				Remarks: ""
			});
			fileName = "MasterSellingBBNTemplate";
		}
		else {
			//$scope.MasterSellingPriceFU_BBN_gridAPI.selection.getSelectedRows().forEach(function(row) {

			for (var i = 0; i < $scope.MasterSellingPriceFU_BBN_grid.data.length; i++) {

				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Model == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].Model = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Tipe == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].Tipe = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Katashiki == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].Katashiki = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Suffix == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].Suffix = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].PricingClusterCode == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].PricingClusterCode = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].BBN == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].BBN = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].BBNServiceFee == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].BBNServiceFee = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo = " ";
				}
				if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Remarks == null) {
					$scope.MasterSellingPriceFU_BBN_grid.data[i].Remarks = " ";
				}
			}

			$scope.MasterSellingPriceFU_BBN_grid.data.forEach(function (row) {
				excelData.push({
					Model: row.Model,
					Tipe: row.Tipe,
					Katashiki: row.Katashiki,
					Suffix: row.Suffix,
					PricingClusterCode: row.PricingClusterCode,
					BBN: row.BBN,
					BBNServiceFee: row.BBNServiceFee,
					EffectiveDateFrom: row.EffectiveDateFrom,
					EffectiveDateTo: row.EffectiveDateTo,
					Remarks: row.Remarks
				});
			});
		}
		// console.log('isi nya ',JSON.stringify(excelData) );
		// console.log(' total row ', excelData[0].length);
		// console.log(' isi row 0 ', excelData[0]);
		XLSXInterface.writeToXLSX(excelData, fileName);
	}

	$scope.MasterSelling_BBN_Upload_Clicked = function () {
		if ($scope.MasterSelling_ExcelData.length == 0) {
			alert("file excel kosong !");
			return;
		}

		if ($scope.MasterSelling_ExcelData[0].Remarks == null) {
			$scope.MasterSelling_ExcelData[0].Remarks = " ";
		}

		
		if (!$scope.validateColumn($scope.MasterSelling_ExcelData[0])) {
			alert("Kolom file excel tidak sesuai !\n" + $scope.cekKolom);
			return;
		}

		console.log("isi Excel Data :", $scope.MasterSelling_ExcelData);

		for (i = 0; i < $scope.MasterSelling_ExcelData.length; i++) {
			var setan = $scope.MasterSelling_ExcelData[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateFrom);

			var setan2 = $scope.MasterSelling_ExcelData[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
			$scope.MasterSelling_ExcelData[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSelling_ExcelData[i].EffectiveDateFrom ===>', $scope.MasterSelling_ExcelData[i].EffectiveDateTo);
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSelling_ExcelData);
		$scope.MasterSellingPriceFU_BBN_gridAPI.grid.clearAllFilters();
		MasterPricingDealerToBranchBBNFactory.VerifyData($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				//				console.log('isi data', JSON.stringify(res));
				$scope.MasterSellingPriceFU_BBN_grid.data = res.data.Result;			//Data hasil dari WebAPI

				var dataValid = undefined;
				for (i = 0; i < $scope.MasterSellingPriceFU_BBN_grid.data.length; i++) {
					if ($scope.MasterSellingPriceFU_BBN_grid.data[i].Remarks != "") {
						dataValid = false;
					}
				}

				if (dataValid == false) {
					bsNotify.show(
						{
							type: 'warning',
							title: "Data tidak valid!",
							content: "Cek detail kesalahan di kolom Remarks pada tabel",
						}
					);
				}

				$scope.MasterSellingPriceFU_BBN_grid.totalItems = res.data.Total;
			}
		);
	}

	$scope.MasterSelling_Simpan_Clicked = function () {

		for (i = 0; i < $scope.MasterSellingPriceFU_BBN_grid.data.length; i++) {
			var setan = $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom.split('-');
			var to_submit = "" + setan[1] + "/" + setan[0] + "/" + setan[2];
			$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom = to_submit;
			console.log('$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_BBN_grid.data[0].EffectiveDateFrom);

			var setan2 = $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo.split('-');
			var to_submit2 = "" + setan2[1] + "/" + setan2[0] + "/" + setan2[2];
			$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo = to_submit2;
			console.log('$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_BBN_grid.data[0].EffectiveDateTo);
		}

		var Grid;
		Grid = JSON.stringify($scope.MasterSellingPriceFU_BBN_grid.data);

		console.log('data yg dikirim ke backend ===>', Grid);
		MasterPricingDealerToBranchBBNFactory.Submit($scope.JenisPE, Grid, $scope.TypePE).then(
			function (res) {
				bsNotify.show({
					title: "Customer Data",
					content: "Data berhasil di simpan",
					type: 'success'
				});
				$scope.MasterSelling_BBN_Generate_Clicked();
			},
			function (err) {
				bsNotify.show(
					{
						type: 'danger',
						title: "Data gagal disimpan!",
						content: err.data.Message.split('-')[1],
					}
				); 
			}
		);

		//                        12/31/2019   ===> 0/1/2
		// for (i = 0; i < $scope.MasterSellingPriceFU_BBN_grid.data.length; i++) {
		// 	var setan = $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom.split('/');
		// 	var to_submit = "" + setan[1] + "-" + setan[0] + "-" + setan[2];
		// 	$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom = to_submit;
		// 	console.log('$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom);

		// 	var setan2 = $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo.split('/');
		// 	var to_submit2 = "" + setan2[1] + "-" + setan2[0] + "-" + setan2[2];
		// 	$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo = to_submit2;
		// 	console.log('$scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateFrom ===>', $scope.MasterSellingPriceFU_BBN_grid.data[i].EffectiveDateTo);
		// }
	}

	$scope.MasterSelling_Batal_Clicked = function () {
		$scope.MasterSellingPriceFU_BBN_grid.data = [];
		$scope.MasterSellingPriceFU_BBN_gridAPI.grid.clearAllFilters();
		$scope.TextFilterGrid = "";
		$scope.TextFilterDua_BBN = "";
		$scope.TextFilterSatu_BBN = "";
		var myEl = angular.element(document.querySelector('#uploadSellingFile_BBN'));
		myEl.val('');
	}


	$scope.MasterSelling_BBN_Cari_Clicked = function () {
		var value = $scope.TextFilterGrid_BBN;
		$scope.MasterSellingPriceFU_BBN_gridAPI.grid.clearAllFilters();
		if ($scope.ComboFilterGrid_BBN != "") {
			$scope.MasterSellingPriceFU_BBN_gridAPI.grid.getColumn($scope.ComboFilterGrid_BBN).filters[0].term = value;
		}
		// else {
		// 	$scope.MasterSellingPriceFU_BBN_gridAPI.grid.clearAllFilters();

	}

});
