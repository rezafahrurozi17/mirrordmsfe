angular.module('app')
	.controller('ReopenPeriodeController', function ($scope, bsNotify, bsAlert,$http, $filter, CurrentUser,ReportDailyCashBankFactory, ReopenPeriodeFactory, $timeout) {
        $scope.user = CurrentUser.user();
		if ($scope.user.OrgCode.substring(3, 9) == '000000')
			$scope.ReopenPeriode_Branch_isDisabled = true;
		else
			$scope.ReopenPeriode_Branch_isDisabled = false;

        $scope.dateOptions = {
            format: "MM/yyyy"
        };
        console.log('USERRR', $scope.user);
        $scope.optionsLihatTransaksiCashBank_BulkAction = [{ name: "Pengajuan", value: "Pengajuan" },{ name: "Setujui", value: "Setujui" }, { name: "Tolak", value: "Tolak" }];

        if($scope.user.RoleName == 'Cashier' || $scope.user.RoleName == 'Admin Finance'){
            $scope.optionsLihatTransaksiCashBank_BulkAction = [{ name: "Pengajuan", value: "Pengajuan" }];
        }else{
            $scope.optionsLihatTransaksiCashBank_BulkAction = [{ name: "Pengajuan", value: "Pengajuan" },{ name: "Setujui", value: "Setujui" }, { name: "Tolak", value: "Tolak" }]; 
        }
        ReopenPeriodeFactory.getBranchByOutletId($scope.user.OutletId)
        .then(
            function (res) {
                //console.log("res data BranchName : ", res.data.Result[0]);

                $scope.optionsReopenPeriode_NamaBranch = res.data.Result;
                $scope.ReopenPeriode_Branch = $scope.user.OutletId;
            }
        );

		$scope.AccountId = 0;
        
        $scope.ReopenPeriode_NamaGLAccount_Changed = function () {
			console.log("masuk ke NamaGLAccountChange");

			angular.forEach(dataCOA, function (value, key) {
				if (value.GLAccountId == $scope.ReopenPeriode_NamaGLAccount) {
					$scope.AccountId = value.AccountId;
					$scope.ReopenPeriode_NomorGLAccount = value.GLAccountCode;
				}
			});
		}

       

            $scope.grid = {
                useExternalPagination: true,
                paginationPageSizes: [10, 25, 50],
                enableRowSelection: true,
                enableSelectAll: false,

                displaySelectionCheckbox: false,
                enableColumnResizing: true,
                canSelectRows: true,
                multiSelect: false,
                // enableFullRowSelection: true,
    
                columnDefs: [
                    { name: "ReopenId", displayName: "ReopenId", field: "ReopenId", enableHiding: false, visible: false},
                    { name: "Tanggal", displayName: "Tanggal", field: "Tanggal", enableHiding: false, visible: true, cellFilter: 'date:\"dd-MMM-yyyy\"' },
                    { 
                        name: "IsOpen", displayName: "Status", field: "IsOpen", 
                        cellTemplate: '<div><a ng-hide="row.entity.IsOpen == 1"> Closed</a><a ng-hide="row.entity.IsOpen == 0"> Open</a></div>'
                    },
                    { name: "StatusPengajuan", displayName: "Pengajuan Reopen", field: "StatusPengajuan", enableHiding: false, visible: true },
                    { name: "AlasanPengajuan", displayName: "Alasan", field: "AlasanPengajuan", enableHiding: false, visible: true },
                    { name: "ADHApproved", displayName: "ADH", field: "ADHApproved", type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled=\'true\' ng-model="row.entity.ADHApproved">' },
                    { name: "FinanceHoApproved", displayName: "FInance HO", field: "FinanceHoApproved", type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled=\'true\' ng-model="row.entity.FinanceHoApproved">'  },
                    { name: "AccountingHoApproved", displayName: "AccountingHO", field: "AccountingHoApproved", type: 'boolean',cellTemplate: '<input type="checkbox" ng-disabled=\'true\' ng-model="row.entity.AccountingHoApproved">'  },
                    
                ],
                onRegisterApi: function (gridApi) {
                    $scope.grid1Api = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    });
                }
            };

        var dataCOA = [];
        $scope.BranchName_Changed = function(){
        dataCOA = [];
        ReopenPeriodeFactory.getCOACashBankList($scope.ReopenPeriode_Branch)
            .then(
                function (res) {
                    console.log("res COA Cash Bank List", res);
                    dataCOA = res.data.Result;
                    var dataOption = [];
                    angular.forEach(res.data.Result, function (value, key) {
                        dataOption.push({ "name": value.GLAccountName, "value": value.GLAccountId });
                    });
                    $scope.optionsReopenPeriodeNamaGLAccount = dataOption;
                    $scope.grid.data = [];
                    $scope.ReopenPeriode_NamaGLAccount = null;
                }
            );
        }

        $scope.Cari_Clicked = function(){
            ReopenPeriodeFactory.getData($scope.ReopenPeriode_NamaGLAccount,$filter('date')(new Date($scope.ReopenPeriode_Tanggal), 'yyyy-MM'),$scope.ReopenPeriode_Branch)
            .then(
                function (res) {
                    console.log('RES',res);
                    $scope.grid.data = res.data;
                }
            );
        }

        $scope.ReopenPeriode_BulkAction_SearchDropdown_Changed = function () {
			if ($scope.ReopenPeriode_BulkAction == "Pengajuan") {
                angular.forEach($scope.grid1Api.selection.getSelectedRows(), function (value, key) {
                    if(value.IsOpen == 0 && value.StatusPengajuan != 'Pengajuan' ){
                        angular.element('.ui.modal.ModalReopenPeriode_Pengajuan').modal('show');
                    }
                })
			}
			else if ($scope.ReopenPeriode_BulkAction == "Setujui") {
                bsAlert.alert({
                    title: "Apakah anda yakin akan melakukan approval Reopen Periode" ,
                    text: "",
                    type: "question",
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                },
                function() {
                    $scope.SetujuiReopen();
                },
                function() {
                    
                })
			}
            else if ($scope.ReopenPeriode_BulkAction == "Tolak") {
				angular.element('.ui.modal.ModalReopenPeriode_Tolak').modal('show');
			}
		}

        $scope.PengajuanReopen = function(){
            angular.forEach($scope.grid1Api.selection.getSelectedRows(), function (value, key) {
                console.log('selected', value.Tanggal);

                var date = new Date(value.Tanggal);
                date.setHours(7, 0, 1);

                var body = ({
                    GLId: $scope.ReopenPeriode_NamaGLAccount,
                    Tanggal: date,
                    AlasanPengajuan: $scope.PengajuanAlasanReopen,
                    OutletId: $scope.ReopenPeriode_Branch
                });
                console.log(body);
                ReopenPeriodeFactory.reopenPeriode(body)
                        .then(
                            function (res) {
                                // alert("Berhasil Disetujui");
                                bsNotify.show({
                                    title: "Berhasil",
                                    content: "Berhasil Pengajuan",
                                    type: 'success'
                                });
                                $scope.Cari_Clicked();
                                angular.element('.ui.modal.ModalReopenPeriode_Pengajuan').modal('hide');
                                $scope.PengajuanAlasanReopen = null;
                                $scope.BatalPengajuanReopen();
                            },

                            function (err) {
                                // alert("Persetujuan gagal");
                                bsNotify.show(
                                    {
                                        title: "Notifikasi",
                                        content: "Pengajuan gagal. " + err.data.Message,
                                        type: 'danger'
                                    }
                                );

                                $scope.BatalPengajuanReopen();
                            }
                        );
            });
        }

        $scope.BatalPengajuanReopen = function(){
            angular.element('.ui.modal.ModalReopenPeriode_Pengajuan').modal('hide');
            angular.element('.ui.modal.ModalReopenPeriode_Tolak').modal('hide');
        }

        $scope.SetujuiReopen = function(){
            angular.forEach($scope.grid1Api.selection.getSelectedRows(), function (value, key) {
                console.log('selected', value);
                if(value.StatusPengajuan == "Pengajuan"){
                    var body = ({
                        ReopenId: value.ReopenId,
                        IsApproved: 1 //Setujui
                    });

                    ReopenPeriodeFactory.setujuiTolakReopenPeriode(body)
                        .then(
                            function (res) {
                                // alert("Berhasil Disetujui");
                                bsNotify.show({
                                    title: "Berhasil",
                                    content: "Berhasil diSetujui",
                                    type: 'success'
                                });
                                $scope.Cari_Clicked();
                                $scope.BatalPengajuanReopen();
                            },

                            function (err) {
                                // alert("Persetujuan gagal");
                                bsNotify.show(
                                    {
                                        title: "Notifikasi",
                                        content: "Setujui gagal. " + err.data.Message,
                                        type: 'danger'
                                    }
                                );
                                $scope.BatalPengajuanReopen();
                            }
                        );
                }
            });
        }

        $scope.TolakReopen = function(){
            angular.forEach($scope.grid1Api.selection.getSelectedRows(), function (value, key) {
                console.log('selected', value);
                if(value.StatusPengajuan == "Pengajuan" || value.StatusPengajuan == "Disetujui"){
                    var body = ({
                        ReopenId: value.ReopenId,
                        IsApproved: 2, //Tolak,
                        RejectReason: $scope.TolakReopenPeriode
                    });

                    ReopenPeriodeFactory.setujuiTolakReopenPeriode(body)
                        .then(
                            function (res) {
                                // alert("Berhasil Disetujui");
                                bsNotify.show({
                                    title: "Berhasil",
                                    content: "Berhasil diTolak",
                                    type: 'success'
                                });
                                $scope.Cari_Clicked();
                                $scope.TolakReopenPeriode = null;
                                $scope.BatalPengajuanReopen();
                            },

                            function (err) {
                                // alert("Persetujuan gagal");
                                bsNotify.show(
                                    {
                                        title: "Notifikasi",
                                        content: "Ditolak gagal. " + err.data.Message,
                                        type: 'danger'
                                    }
                                );
                                $scope.BatalPengajuanReopen();
                            }
                        );
                }
            });
        }
});