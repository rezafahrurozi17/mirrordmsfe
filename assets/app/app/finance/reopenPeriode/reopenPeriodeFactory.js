angular.module('app')
	.factory('ReopenPeriodeFactory', function ($http, CurrentUser) {
        var factory={};

        factory.getBranchByOutletId= function(outletId){
            var url = '/api/fe/Branch/SelectData?start=0&limit=0&filterData='+outletId;
		    var res=$http.get(url);  
            return res;
        };

        factory.getData= function(Glid, Periode,OutletId){
			var url = '/api/fe/ReopenPeriode/GenerateDate?GLId='+Glid+'&period='+ Periode+'&OutletId='+OutletId;
			console.log("url getData : ", url);
			var res=$http.get(url);
			return res; 
		};

        factory.reopenPeriode= function(body){
			var url = '/api/fe/ReopenPeriode';
            var param = JSON.stringify(body);
			console.log("object", param);
			var res = $http.post(url, param);
			return res;
		};

		factory.setujuiTolakReopenPeriode= function(body){
			var url = '/api/fe/ReopenPeriode/ApproveReject';
            var param = JSON.stringify(body);
			console.log("object", param);
			var res = $http.put(url, param);
			return res;
		};

		factory.getCOACashBankList = function(outletId){
			var url = '/api/fe/FinanceCOA/GetDataByPurposeAndOutlet/?StrGlType=GL Cash Bank&OutletId='+outletId;
		    var res=$http.get(url);  
            return res;
		}
        
        return factory;
});