angular.module('app')
  .factory('Parameter', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/branchOrder/parameter');
        //console.log('res=>',res);
        return res;
      },
      create: function(parameter) {
        return $http.post('/branchOrder/parameter/create', {
                                            code: parameter.code,
                                            name: parameter.name,
                                            desc: parameter.desc});
      },
      update: function(parameter){
        return $http.post('/branchOrder/parameter/update', {
                                            id: parameter.id,
                                            code: parameter.code,
                                            name: parameter.name,
                                            desc: parameter.desc});
      },
      delete: function(id) {
        return $http.post('/branchOrder/parameter/delete',{id:id});
      },
    }
  });