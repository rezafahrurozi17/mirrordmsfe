angular.module('app')
  .factory('OAP', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }

        console.log("Get Data..");
        console.log("$http.get(/api/ds/OAPRAPs/?start=1&limit=100&"+$httpParamSerializer(filter)+")");
        console.log("================================================================");
        var res=$http.get('/api/ds/OAPRAPs/?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;
      },
      getSSR: function(filter){
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }

        console.log("Get Data SSR..");
        console.log("$http.get(/api/ds/StandardStockRatios/?start=1&limit=100&"+$httpParamSerializer(filter)+")");
        console.log("================================================================");
        var res=$http.get('/api/ds/StandardStockRatios/?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;
      },
      update: function(oap) {
        var params = [];
        for (var i=0; i<oap.length; i++) {
          var temp={};
          temp["NID"] = oap[i]["NID"];
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          temp["JanId"] = oap[i]["JanId"];
          temp["FebId"] = oap[i]["FebId"];
          temp["MarId"] = oap[i]["MarId"];
          temp["AprId"] = oap[i]["AprId"];
          temp["MayId"] = oap[i]["MayId"];
          temp["JunId"] = oap[i]["JunId"];
          temp["JulId"] = oap[i]["JulId"];
          temp["AugId"] = oap[i]["AugId"];
          temp["SepId"] = oap[i]["SepId"];
          temp["OctId"] = oap[i]["OctId"];
          temp["NovId"] = oap[i]["NovId"];
          temp["DecId"] = oap[i]["DecId"];
          temp["AreaId"] = oap[i]["AreaId"];
          temp["Period"] = oap[i]["Period"];
          temp["OutletId"] = oap[i]["OutletId"];
          temp["OutletCode"] = oap[i]["OutletCode"];
          temp["OutletName"] = oap[i]["OutletName"];
          temp["GroupDealerId"] = oap[i]["GroupDealerId"];
          temp["GroupDealerName"] = oap[i]["GroupDealerName"];
          temp["VehicleModelName"] = oap[i]["VehicleModelName"];
          temp["ProvinceId"] = oap[i]["ProvinceId"];
          temp["ProvinceName"] = oap[i]["ProvinceName"];
          temp["OAPRAPDocumentTypeId"] = oap[i]["OAPRAPDocumentTypeId"];
          temp["OAPRAPTargetTypeId"] = oap[i]["OAPRAPTargetTypeId"];
          temp["Jan"] = oap[i]["Jan"];
          temp["Feb"] = oap[i]["Feb"];
          temp["Mar"] = oap[i]["Mar"];
          temp["Apr"] = oap[i]["Apr"];
          temp["May"] = oap[i]["May"];
          temp["Jun"] = oap[i]["Jun"];
          temp["Jul"] = oap[i]["Jul"];
          temp["Aug"] = oap[i]["Aug"];
          temp["Sep"] = oap[i]["Sep"];
          temp["Oct"] = oap[i]["Oct"];
          temp["Nov"] = oap[i]["Nov"];
          temp["Dec"] = oap[i]["Dec"];

          // "VehicleModelId":49,"JanId":47890,"FebId":47891,"MarId":47892,
          // "AprId":47893,"MayId":47894,"JunId":47895,"JulId":47896,"AugId":47897,
          // "SepId":47898,"OctId":47899,"NovId":47900,"DecId":47901,"AreaId":32,
          // "Period":2016,"OutletId":49,"OutletCode":"AUT001117","OutletName":"AUTO2000 A. Yani",
          // "GroupDealerId":1,"GroupDealerName":"AUTO2000","VehicleModelName":"FORTUNER",
          // "ProvinceId":28,"ProvinceName":"Jawa Timur","OAPRAPDocumentTypeId":1,
          // "OAPRAPTargetTypeId":2,"Jan":0,"Feb":0,"Mar":0,"Apr":10,"May":11,"Jun":12,
          // "Jul":10,"Aug":0,"Sep":10,"Oct":0,"Nov":0,"Dec":0
          params.push(temp);
        }
        // for (var key in oap) {
        //   if (key == '$$hashKey') {
        //     delete oap[key];
        //   }
        // }

        console.log("Update Data..");
        console.log("$http.put('/api/ds/OAPRAPs/Multiple', "+JSON.stringify(params)+")");
        console.log("================================================================");
        return $http.put('/api/ds/OAPRAPs/Multiple', params);
      },
      create: function(oap) {
        var params = [];
        for (var i=0; i<oap.length; i++) {
          var temp={};
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          temp["AreaId"] = oap[i]["AreaId"];
          temp["Period"] = oap[i]["Period"];
          temp["OutletId"] = oap[i]["OutletId"];
          temp["OutletCode"] = oap[i]["OutletCode"];
          temp["OutletName"] = oap[i]["OutletName"];
          temp["GroupDealerId"] = oap[i]["GroupDealerId"];
          temp["GroupDealerName"] = oap[i]["GroupDealerName"];
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          temp["VehicleModelName"] = oap[i]["VehicleModelName"];
          temp["OAPRAPDocumentTypeId"] = oap[i]["OAPRAPDocumentTypeId"];
          temp["OAPRAPTargetTypeId"] = oap[i]["OAPRAPTargetTypeId"];
          temp["Year"] = oap[i]["Year"];
          temp["Jan"] = oap[i]["Jan"];
          temp["Feb"] = oap[i]["Feb"];
          temp["Mar"] = oap[i]["Mar"];
          temp["Apr"] = oap[i]["Apr"];
          temp["May"] = oap[i]["May"];
          temp["Jun"] = oap[i]["Jun"];
          temp["Jul"] = oap[i]["Jul"];
          temp["Aug"] = oap[i]["Aug"];
          temp["Sep"] = oap[i]["Sep"];
          temp["Oct"] = oap[i]["Oct"];
          temp["Nov"] = oap[i]["Nov"];
          temp["Dec"] = oap[i]["Dec"];
          params.push(temp);
        }

        console.log("Create Data..");
        console.log("$http.put('/api/ds/OAPRAPs/Multiple', "+JSON.stringify(params)+")");
        console.log("================================================================");
        // {"Jan":"0.0651","Feb":"0.0854","Mar":"0.0900","Apr":"0.0400","May":"0.0400","Jun":"0.0400","Jul":"0.0400","Aug":"0.0400","Sep":"0.0400","Oct":"0.0400","Nov":"0.0400","Dec":"0.0400","OutletId":51,"GroupDealerId":1,"GroupDealerName":"AUTO2000","OutletName":"AUTO2000 Alam Sutera","OutletCode":"AUT001250","VehicleModelName":"AVANZA","VehicleModelId":28,"OAPRAPDocumentTypeId":1,"OAPRAPTargetTypeId":3,"Year":2016,"$$hashKey":"uiGrid-0078"},
        return $http.post('/api/ds/OAPRAPs/Multiple', params);
      },
      updateSSR: function(oap) {
        var params = [];
        for (var i=0; i<oap.length; i++) {
          var temp={};
          // temp["NID"] = oap[i]["NID"];          
          temp["JanId"] = oap[i]["JanId"];
          temp["FebId"] = oap[i]["FebId"];
          temp["MarId"] = oap[i]["MarId"];
          temp["AprId"] = oap[i]["AprId"];
          temp["MayId"] = oap[i]["MayId"];
          temp["JunId"] = oap[i]["JunId"];
          temp["JulId"] = oap[i]["JulId"];
          temp["AugId"] = oap[i]["AugId"];
          temp["SepId"] = oap[i]["SepId"];
          temp["OctId"] = oap[i]["OctId"];
          temp["NovId"] = oap[i]["NovId"];
          temp["DecId"] = oap[i]["DecId"];
          temp["OutletId"] = oap[i]["OutletId"];          
          temp["Period"] = oap[i]["Period"];
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          // temp["AreaId"] = oap[i]["AreaId"];
          temp["OutletCode"] = oap[i]["OutletCode"];
          temp["OutletName"] = oap[i]["OutletName"];
          // temp["GroupDealerId"] = oap[i]["GroupDealerId"];
          // temp["GroupDealerName"] = oap[i]["GroupDealerName"];
          // temp["VehicleModelName"] = oap[i]["VehicleModelName"];
          // temp["OAPRAPDocumentTypeId"] = oap[i]["OAPRAPDocumentTypeId"];
          // temp["StandardStockRatioTypeId"] = oap[i]["OAPRAPTargetTypeId"];
          temp["Jan"] = oap[i]["Jan"];
          temp["Feb"] = oap[i]["Feb"];
          temp["Mar"] = oap[i]["Mar"];
          temp["Apr"] = oap[i]["Apr"];
          temp["May"] = oap[i]["May"];
          temp["Jun"] = oap[i]["Jun"];
          temp["Jul"] = oap[i]["Jul"];
          temp["Aug"] = oap[i]["Aug"];
          temp["Sep"] = oap[i]["Sep"];
          temp["Oct"] = oap[i]["Oct"];
          temp["Nov"] = oap[i]["Nov"];
          temp["Dec"] = oap[i]["Dec"];
          params.push(temp);
        }

        console.log("Update Data SSR..");
        console.log("$http.put('/api/ds/StandardStockRatios/Multiple', "+JSON.stringify(params)+")");
        console.log("================================================================");
        // console.log("$http.put('/api/ds/StandardStockRatios/Multiple', "+JSON.stringify(oap)+")");
        return $http.put('/api/ds/StandardStockRatios/Multiple', params);
      },
      createSSR: function(oap) {
        var params = [];
        for (var i=0; i<oap.length; i++) {
          var temp={};          
          // temp["JanId"] = oap[i]["JanId"];
          // temp["FebId"] = oap[i]["FebId"];
          // temp["MarId"] = oap[i]["MarId"];
          // temp["AprId"] = oap[i]["AprId"];
          // temp["MayId"] = oap[i]["MayId"];
          // temp["JunId"] = oap[i]["JunId"];
          // temp["JulId"] = oap[i]["JulId"];
          // temp["AugId"] = oap[i]["AugId"];
          // temp["SepId"] = oap[i]["SepId"];
          // temp["OctId"] = oap[i]["OctId"];
          // temp["NovId"] = oap[i]["NovId"];
          // temp["DecId"] = oap[i]["DecId"];
          // temp["AreaId"] = oap[i]["AreaId"];
          temp["Year"] = oap[i]["Year"];
          // temp["Period"] = oap[i]["Period"];
          temp["OutletId"] = oap[i]["OutletId"];
          temp["OutletCode"] = oap[i]["OutletCode"];
          temp["OutletName"] = oap[i]["OutletName"];
          // temp["GroupDealerId"] = oap[i]["GroupDealerId"];
          // temp["GroupDealerName"] = oap[i]["GroupDealerName"];
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          temp["VehicleModelName"] = oap[i]["VehicleModelName"];
          // temp["OAPRAPDocumentTypeId"] = oap[i]["OAPRAPDocumentTypeId"];
          // temp["StandardStockRatioTypeId"] = oap[i]["OAPRAPTargetTypeId"];
          temp["Jan"] = oap[i]["Jan"];
          temp["Feb"] = oap[i]["Feb"];
          temp["Mar"] = oap[i]["Mar"];
          temp["Apr"] = oap[i]["Apr"];
          temp["May"] = oap[i]["May"];
          temp["Jun"] = oap[i]["Jun"];
          temp["Jul"] = oap[i]["Jul"];
          temp["Aug"] = oap[i]["Aug"];
          temp["Sep"] = oap[i]["Sep"];
          temp["Oct"] = oap[i]["Oct"];
          temp["Nov"] = oap[i]["Nov"];
          temp["Dec"] = oap[i]["Dec"];
          params.push(temp);
        }

        console.log("Create Data SSR..");
        console.log("$http.put('/api/ds/StandardStockRatios/Multiple', "+JSON.stringify(params)+")");
        console.log("================================================================");
        return $http.post('/api/ds/StandardStockRatios/Multiple', params);
      },

    }
  });