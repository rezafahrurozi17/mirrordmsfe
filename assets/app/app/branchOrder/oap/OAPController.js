angular.module('app')
    .controller('OAPController', function($scope, $http, CurrentUser, uiGridConstants, OrgChart, ProductModel, Region, OAP, OAPPeriod, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        //$scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mOAP = {year:0, type:1, field:1, detail:[]}; //Model
    $scope.xOAP = {};
    $scope.xOAP.selected=[];
    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        return res;
    }

    var alerts = [];    
    var mDataCopy;

    $scope.typeData = [ {text:"OAP",value:1},
                        {text:"RAP",value:2}];

    // $scope.yearData = [ {name:"2016",id:2016},
    //                     {name: "2017",id:2017}];

    $scope.yearData = [];

    $scope.fieldData = [];
    $timeout(function(){
        if($scope.checkRights(11))
            $scope.fieldData.push({name: "Retail Sales",id: 1});
        if($scope.checkRights(12))
            $scope.fieldData.push({name: "Market Polreg",id: 2});
        if($scope.checkRights(13))
            $scope.fieldData.push({name: "Outlet Share",id: 3});
        if($scope.checkRights(14))
            $scope.fieldData.push({name: "Outlet Sales Contribution",id: 4});
        if($scope.checkRights(15))
            $scope.fieldData.push({name: "Standard Stock Ratio",id: 5});
    });                        

    $scope.filter = {year:null,outletId:null,fieldId:null,typeId:null,areaId:null,provinceId:null};
    var mode = 0;
    var editMode = 1;
    var viewMode = 0;
    var uploadMode = false;
    var processingMode = 2;
    var editableMode = false;

    var outletObj = null;
    var outletData = [];
    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.orgData= [];

    OrgChart.getDataTree().then(
        function(res){
            orgTreeData = res.data.Result;
            $scope.orgData = res.data.Result;
            if ($scope.orgData.length==1 && $scope.orgData[0].child.length==1) {
                $scope.filter.outletId = $scope.orgData[0].child[0].id;
            }
            for(i in $scope.orgData){
                for(ch in $scope.orgData[i].child){
                    outletData.push($scope.orgData[i].child[ch]);
                }
            }
            // if($scope.orgData.child && $scope.orgData.child.length==1){
            //     $scope.filter.outletId = $scope.orgData[0].id;
            // }
            Region.getAreaProvince().then(
                function(res){
                    var areaProvRes = res.data.Result;
                    for(var i in areaProvRes){
                        var filteredOutlet = _.find(outletData, function(o){
                            return o.AreaId.indexOf(areaProvRes[i].AreaId)>=0 && o.ProvinceId==areaProvRes[i].ProvinceId;
                        });
                        if(filteredOutlet != null)
                            areaProvince.push(areaProvRes[i]);
                    }
                    filterArea();
                    filterProvince();
                    $scope.loading=false;
                }
            );

            $scope.loading=false;
            return res.data;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(filter[key]==null || filter[key]==undefined) continue;
                if(Array.isArray(data[i][key]) && !Array.isArray(filter[key])){
                    if(data[i][key].indexOf(filter[key])<0){
                        valid=false; break;
                    }
                }else if(!Array.isArray(data[i][key]) && Array.isArray(filter[key])){
                    if(filter[key].indexOf(data[i][key])<0){
                        valid=false; break;
                    }
                }else if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function getAreaProvinceFromHO(HO){
        $scope.provinceData = [];
        $scope.areaData = [];
        var dataSrc = HO.child;
        for(var i in dataSrc){
            var provinceObj = _.find(areaProvince,{ProvinceId: dataSrc[i].ProvinceId});
            var newObj = {ProvinceId:provinceObj.ProvinceId, ProvinceName:provinceObj.ProvinceName};
            if(_.findIndex($scope.provinceData, newObj)<0)
                $scope.provinceData.push(newObj);
            $scope.provinceData = _.sortBy($scope.provinceData, ['ProvinceName']);

            var areaObj = _.find(areaProvince,function(o){
                return dataSrc[i].AreaId.indexOf(o.AreaId)>=0;
            });
            var newObj = {AreaId:areaObj.AreaId, Description:areaObj.Description};
            if(_.findIndex($scope.areaData, newObj)<0)
                $scope.areaData.push(newObj);
            $scope.areaData = _.sortBy($scope.areaData, ['Description']);
        }
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
        if($scope.areaData.length==1){
            $scope.filter.areaId = $scope.areaData[0].AreaId;
        }
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
        if($scope.provinceData.length==1){
            $scope.filter.provinceId = $scope.provinceData[0].ProvinceId;
        }
    }
    function filterDealer(filter){
        $scope.orgData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = orgTreeData;
        }else{
            for(var i in orgTreeData){
                var inserted = false;
                for(var chIdx in orgTreeData[i].child){
                    var valid = true;
                    for(key in filter){
                        if(filter[key]==null || filter[key]==undefined) continue;
                        if(Array.isArray(orgTreeData[i].child[chIdx][key])){
                            if(orgTreeData[i].child[chIdx][key].indexOf(filter[key])<0){
                                valid=false; break;
                            }
                        }else if(orgTreeData[i].child[chIdx][key]!=filter[key]){
                            valid=false; break;
                        }
                    }
                    if(valid){
                        if(!inserted){
                            var parent = angular.copy(orgTreeData[i]);
                            parent.child = [];
                            parent.listOutlet = [];
                            dataSrc.push(parent);
                            inserted=true;
                        }
                        dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
                    }
                }
            }
        }
        $scope.orgData = dataSrc;
    }

    var outletSel;
    $scope.changeFilterCombo = function(selected,sender){
        $timeout(function(){
            if(sender.name=='outletId'){
                outletSel = selected;
                if($scope.filter.outletId!=null && $scope.filter.outletId!=undefined){
                    if(selected!=null && ('HeadOfficeId' in selected)){
                        getAreaProvinceFromHO(selected);
                        outletObj = _.find(outletData,{OutletId:selected.HeadOfficeId});
                        return;
                    }
                    else if(selected!=null){
                        outletObj = _.find(outletData,{OutletId:$scope.filter.outletId});
                        if(selected.OutletId==selected.parent.HeadOfficeId){
                            var parentObj = _.find(orgTreeData,{HeadOfficeId:selected.OutletId});
                            getAreaProvinceFromHO(parentObj);
                            return;
                        }
                    }
                    else{
                        outletObj = null;
                    }
                }else{
                    outletObj = null;
                }
            }else if(outletObj==null || outletObj.parent.HeadOfficeId!=outletObj.OutletId){
                filterDealer({ProvinceId:$scope.filter.provinceId, AreaId:$scope.filter.areaId});
            }
            if(sender.name!='regionId'){
                if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:$scope.filter.areaId, ProvinceId:outletObj.ProvinceId});
                else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
                else{
                    filterProvince({AreaId:$scope.filter.areaId});
                }                
            }

            if(sender.name!='areaId'){
                if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:$scope.filter.provinceId, AreaId:outletObj.AreaId});
                else if(($scope.filter.provinceId==null || $scope.filter.provinceId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
                else{
                    filterArea({ProvinceId:$scope.filter.provinceId});
                }                
            }
        })
    }

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;
            
            $scope.customActionSettings[0].visible = mode==editMode;
            $scope.customActionSettings[1].visible = mode==editMode;
            $scope.customActionSettings[0].enable = mode!=processingMode;
            $scope.customActionSettings[1].enable = mode!=processingMode;
            
            $scope.customActionSettings[2].visible = mode==viewMode;
            $scope.customActionSettings[3].visible = mode==viewMode;
            $scope.customActionSettings[4].visible = ($scope.grid.data.length>0) && (mode == viewMode);
            $scope.grid.enableCellEditOnFocus = mode=editMode;

        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                changeMode(processingMode);
                // if($scope.filter.typeId==3){
                if($scope.filter.fieldId==5){
                    if(uploadMode){
                        OAP.createSSR($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });
                    }                        
                    else {
                        OAP.updateSSR($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });                    
                    }                        
                }else if($scope.filter.fieldId==3 || $scope.filter.fieldId==4){
                    for (var i=0; i<$scope.grid.data.length; i++) {
                        $scope.grid.data[i]['Jan'] = parseFloat(parseFloat($scope.grid.data[i]['Jan'])/100).toFixed(4);
                        $scope.grid.data[i]['Feb'] = parseFloat(parseFloat($scope.grid.data[i]['Feb'])/100).toFixed(4);
                        $scope.grid.data[i]['Mar'] = parseFloat(parseFloat($scope.grid.data[i]['Mar'])/100).toFixed(4);
                        $scope.grid.data[i]['Apr'] = parseFloat(parseFloat($scope.grid.data[i]['Apr'])/100).toFixed(4);
                        $scope.grid.data[i]['May'] = parseFloat(parseFloat($scope.grid.data[i]['May'])/100).toFixed(4);
                        $scope.grid.data[i]['Jun'] = parseFloat(parseFloat($scope.grid.data[i]['Jun'])/100).toFixed(4);
                        $scope.grid.data[i]['Jul'] = parseFloat(parseFloat($scope.grid.data[i]['Jul'])/100).toFixed(4);
                        $scope.grid.data[i]['Aug'] = parseFloat(parseFloat($scope.grid.data[i]['Aug'])/100).toFixed(4);
                        $scope.grid.data[i]['Sep'] = parseFloat(parseFloat($scope.grid.data[i]['Sep'])/100).toFixed(4);
                        $scope.grid.data[i]['Oct'] = parseFloat(parseFloat($scope.grid.data[i]['Oct'])/100).toFixed(4);
                        $scope.grid.data[i]['Nov'] = parseFloat(parseFloat($scope.grid.data[i]['Nov'])/100).toFixed(4);
                        $scope.grid.data[i]['Dec'] = parseFloat(parseFloat($scope.grid.data[i]['Dec'])/100).toFixed(4);
                    }
                    if(uploadMode) {
                        // console.log("upload : "+JSON.stringify($scope.grid.data));
                        OAP.create($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });
                    }
                    else {
                        OAP.update($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });                    
                    }
                }else{
                    if(uploadMode) {
                        // console.log("upload : "+JSON.stringify($scope.grid.data));
                        OAP.create($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });
                    }
                    else {
                        OAP.update($scope.grid.data).then(function(){
                            $scope.getData();
                            changeMode(viewMode);
                            mode = viewMode;
                        });                    
                    }
                }
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];

                alerts=[];
                if ($scope.filter.year==null || $scope.filter.outletId==null || $scope.filter.fieldId==null || $scope.filter.typeId==null) {
                    $scope.showAlert(alerts.join('\n'), alerts.length);
                } else {
                    if (outletSel==undefined || outletSel.level == 1){
                        angular.forEach($scope.orgData, function(org, orgIdx){
                            // console.log("org : "+JSON.stringify(org));
                            angular.forEach(org.child, function(dlr, dlrIdx){
                                // console.log("dlr : "+JSON.stringify(dlr));
                                angular.forEach($scope.productModelData, function(model, modelIdx){
                                    if (dlr.parent.id == outletSel.id) {
                                        if($scope.filter.fieldId==3){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:'0%', Feb:'0%', Mar:'0%', Apr:'0%', May:'0%', Jun:'0%', Jul:'0%', Aug:'0%', Sep:'0%', Oct:'0%', Nov:'0%', Dec:'0%'});
                                        } else if($scope.filter.fieldId==4){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:'0%', Feb:'0%', Mar:'0%', Apr:'0%', May:'0%', Jun:'0%', Jul:'0%', Aug:'0%', Sep:'0%', Oct:'0%', Nov:'0%', Dec:'0%'});
                                        } else if($scope.filter.fieldId==5){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:parseFloat(0), Feb:parseFloat(0), Mar:parseFloat(0), Apr:parseFloat(0), May:parseFloat(0), Jun:parseFloat(0), Jul:parseFloat(0), Aug:parseFloat(0), Sep:parseFloat(0), Oct:parseFloat(0), Nov:parseFloat(0), Dec:parseFloat(0)});
                                        } else {
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                                        }  
                                    }                          
                                });
                            });
                        });
                    } else if (outletSel.level == 2) {
                        angular.forEach($scope.orgData, function(org, orgIdx){
                            // console.log("org : "+JSON.stringify(org));
                            angular.forEach(org.child, function(dlr, dlrIdx){
                                // console.log("dlr : "+JSON.stringify(dlr));
                                angular.forEach($scope.productModelData, function(model, modelIdx){
                                    if (dlr.OutletId == outletSel.OutletId) {
                                        if($scope.filter.fieldId==3){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:'0%', Feb:'0%', Mar:'0%', Apr:'0%', May:'0%', Jun:'0%', Jul:'0%', Aug:'0%', Sep:'0%', Oct:'0%', Nov:'0%', Dec:'0%'});
                                        } else if($scope.filter.fieldId==4){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:'0%', Feb:'0%', Mar:'0%', Apr:'0%', May:'0%', Jun:'0%', Jul:'0%', Aug:'0%', Sep:'0%', Oct:'0%', Nov:'0%', Dec:'0%'});
                                        } else if($scope.filter.fieldId==5){
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:parseFloat(0), Feb:parseFloat(0), Mar:parseFloat(0), Apr:parseFloat(0), May:parseFloat(0), Jun:parseFloat(0), Jul:parseFloat(0), Aug:parseFloat(0), Sep:parseFloat(0), Oct:parseFloat(0), Nov:parseFloat(0), Dec:parseFloat(0)});
                                        } else {
                                            generatedData.push({OutletCode:dlr.OutletCode, Outlet:dlr.title, Dealer:org.title, Model:model.VehicleModelName, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                                        }    
                                    }
                                });
                            });
                        });
                    }
                    var sel = _.find($scope.fieldData, { 'id': $scope.filter.fieldId });
                    XLSXInterface.writeToXLSX(generatedData, $scope.typeData[$scope.filter.typeId-1].text+'-'+sel.name+'-'+$scope.filter.year);
                }                                
            },
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                alerts=[];
                if ($scope.filter.year==null || $scope.filter.outletId==null || $scope.filter.fieldId==null || $scope.filter.typeId==null) {
                    $scope.showAlert(alerts.join('\n'), alerts.length);
                } else {
                    document.getElementById('uploadOAP').click();
                }
            },
            title: 'Load File',
            rightsBit: 1
        },{
            func: function(string) {
                uploadMode=false;
                changeMode(editMode);
            },
            visible: false,
            title: 'Edit',
            rightsBit: 2
        }
    ];
    function validateXLS(data){
        var result = [];
        var error = [];

        $scope.grid.data = [];        
        if($scope.filter.fieldId==3 || $scope.filter.fieldId==4){

            $scope.grid.columnDefs = angular.copy(colDef2);
        }
        else if($scope.filter.fieldId==5){
            var ssrColDef = angular.copy(colDef1);
            for (var i=13; i<25; i++) {
                delete ssrColDef[i].aggregationHideLabel;
                delete ssrColDef[i].aggregationType;
            }
            ssrColDef[ssrColDef.length-1].visible = false;

            // $scope.grid.columnDefs = angular.copy(colDef1);
            $scope.grid.columnDefs = angular.copy(ssrColDef);
        }
        else {

            $scope.grid.columnDefs = angular.copy(colDef1);  
        }

        $timeout(function(){
            for(var i=0; i<data.length; i++){
                var dOutlet = _.find(outletData, { 'title': data[i].Outlet });
                var dModel = _.find($scope.productModelData, { 'name': data[i].Model });
                if(dOutlet == undefined){
                    error.push('Outlet '+data[i].Outlet+' tidak ditemukan.');
                    continue;
                }
                if(dModel == undefined){
                    error.push('Model '+data[i].Model+' tidak ditemukan.');
                    continue;
                }

                if($scope.filter.fieldId==3 || $scope.filter.fieldId==4){
                    var newObj = {
                        Jan: isNaN(parseFloat(data[i].Jan.split('%')[0])) ? 0 : (parseFloat(data[i].Jan.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Jan.split('%')[0]).toFixed(2)),
                        Feb: isNaN(parseFloat(data[i].Feb.split('%')[0])) ? 0 : (parseFloat(data[i].Feb.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Feb.split('%')[0]).toFixed(2)),
                        Mar: isNaN(parseFloat(data[i].Mar.split('%')[0])) ? 0 : (parseFloat(data[i].Mar.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Mar.split('%')[0]).toFixed(2)),
                        Apr: isNaN(parseFloat(data[i].Apr.split('%')[0])) ? 0 : (parseFloat(data[i].Apr.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Apr.split('%')[0]).toFixed(2)),
                        May: isNaN(parseFloat(data[i].May.split('%')[0])) ? 0 : (parseFloat(data[i].May.split('%')[0]) < 0 ? 0 : parseFloat(data[i].May.split('%')[0]).toFixed(2)),
                        Jun: isNaN(parseFloat(data[i].Jun.split('%')[0])) ? 0 : (parseFloat(data[i].Jun.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Jun.split('%')[0]).toFixed(2)),
                        Jul: isNaN(parseFloat(data[i].Jul.split('%')[0])) ? 0 : (parseFloat(data[i].Jul.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Jul.split('%')[0]).toFixed(2)),
                        Aug: isNaN(parseFloat(data[i].Aug.split('%')[0])) ? 0 : (parseFloat(data[i].Aug.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Aug.split('%')[0]).toFixed(2)),
                        Sep: isNaN(parseFloat(data[i].Sep.split('%')[0])) ? 0 : (parseFloat(data[i].Sep.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Sep.split('%')[0]).toFixed(2)),
                        Oct: isNaN(parseFloat(data[i].Oct.split('%')[0])) ? 0 : (parseFloat(data[i].Oct.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Oct.split('%')[0]).toFixed(2)),
                        Nov: isNaN(parseFloat(data[i].Nov.split('%')[0])) ? 0 : (parseFloat(data[i].Nov.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Nov.split('%')[0]).toFixed(2)),
                        Dec: isNaN(parseFloat(data[i].Dec.split('%')[0])) ? 0 : (parseFloat(data[i].Dec.split('%')[0]) < 0 ? 0 : parseFloat(data[i].Dec.split('%')[0]).toFixed(2)),
                        // Jan: (data[i].Jan=='-' || data[i].Jan < 0) ? 0 : (isNaN(data[i].Jan.split('%')[0]) ? 0 : parseFloat(data[i].Jan.split('%')[0]).toFixed(2)),
                        // Feb: (data[i].Feb=='-' || data[i].Feb < 0) ? 0 : (isNaN(data[i].Feb.split('%')[0]) ? 0 : parseFloat(data[i].Feb.split('%')[0]).toFixed(2)),
                        // Mar: (data[i].Mar=='-' || data[i].Mar < 0) ? 0 : (isNaN(data[i].Mar.split('%')[0]) ? 0 : parseFloat(data[i].Mar.split('%')[0]).toFixed(2)),
                        // Apr: (data[i].Apr=='-' || data[i].Apr < 0) ? 0 : (isNaN(data[i].Apr.split('%')[0]) ? 0 : parseFloat(data[i].Apr.split('%')[0]).toFixed(2)),
                        // May: (data[i].May=='-' || data[i].May < 0) ? 0 : (isNaN(data[i].May.split('%')[0]) ? 0 : parseFloat(data[i].May.split('%')[0]).toFixed(2)),
                        // Jun: (data[i].Jun=='-' || data[i].Jun < 0) ? 0 : (isNaN(data[i].Jun.split('%')[0]) ? 0 : parseFloat(data[i].Jun.split('%')[0]).toFixed(2)),
                        // Jul: (data[i].Jul=='-' || data[i].Jul < 0) ? 0 : (isNaN(data[i].Jul.split('%')[0]) ? 0 : parseFloat(data[i].Jul.split('%')[0]).toFixed(2)),
                        // Aug: (data[i].Aug=='-' || data[i].Aug < 0) ? 0 : (isNaN(data[i].Aug.split('%')[0]) ? 0 : parseFloat(data[i].Aug.split('%')[0]).toFixed(2)),
                        // Sep: (data[i].Sep=='-' || data[i].Sep < 0) ? 0 : (isNaN(data[i].Sep.split('%')[0]) ? 0 : parseFloat(data[i].Sep.split('%')[0]).toFixed(2)),
                        // Oct: (data[i].Oct=='-' || data[i].Oct < 0) ? 0 : (isNaN(data[i].Oct.split('%')[0]) ? 0 : parseFloat(data[i].Oct.split('%')[0]).toFixed(2)),
                        // Nov: (data[i].Nov=='-' || data[i].Nov < 0) ? 0 : (isNaN(data[i].Nov.split('%')[0]) ? 0 : parseFloat(data[i].Nov.split('%')[0]).toFixed(2)),
                        // Dec: (data[i].Dec=='-' || data[i].Dec < 0) ? 0 : (isNaN(data[i].Dec.split('%')[0]) ? 0 : parseFloat(data[i].Dec.split('%')[0]).toFixed(2)),
                        OutletId: dOutlet.OutletId,
                        GroupDealerId: dOutlet.parent.id,
                        GroupDealerName: dOutlet.parent.title,
                        OutletName: dOutlet.title,
                        OutletCode: dOutlet.code,
                        VehicleModelName: dModel.name,
                        VehicleModelId: dModel.id,
                        OAPRAPDocumentTypeId: parseInt($scope.filter.typeId),
                        OAPRAPTargetTypeId: $scope.filter.fieldId,
                        Year: $scope.filter.year
                    };
                                    
                    // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    // console.log($scope.gridApi);
                }
                else if($scope.filter.fieldId==5){
                    var newObj = {
                        JanId: null,
                        FebId: null,
                        MarId: null,
                        AprId: null,
                        MayId: null,
                        JunId: null,
                        JulId: null,
                        AugId: null,
                        SepId: null,
                        OctId: null,
                        NovId: null,
                        DecId: null,
                        Jan: isNaN(parseFloat(data[i].Jan)) ? 0 : (parseFloat(data[i].Jan) < 0 ? 0 : parseFloat(data[i].Jan).toFixed(2)),
                        Feb: isNaN(parseFloat(data[i].Feb)) ? 0 : (parseFloat(data[i].Feb) < 0 ? 0 : parseFloat(data[i].Feb).toFixed(2)),
                        Mar: isNaN(parseFloat(data[i].Mar)) ? 0 : (parseFloat(data[i].Mar) < 0 ? 0 : parseFloat(data[i].Mar).toFixed(2)),
                        Apr: isNaN(parseFloat(data[i].Apr)) ? 0 : (parseFloat(data[i].Apr) < 0 ? 0 : parseFloat(data[i].Apr).toFixed(2)),
                        May: isNaN(parseFloat(data[i].May)) ? 0 : (parseFloat(data[i].May) < 0 ? 0 : parseFloat(data[i].May).toFixed(2)),
                        Jun: isNaN(parseFloat(data[i].Jun)) ? 0 : (parseFloat(data[i].Jun) < 0 ? 0 : parseFloat(data[i].Jun).toFixed(2)),
                        Jul: isNaN(parseFloat(data[i].Jul)) ? 0 : (parseFloat(data[i].Jul) < 0 ? 0 : parseFloat(data[i].Jul).toFixed(2)),
                        Aug: isNaN(parseFloat(data[i].Aug)) ? 0 : (parseFloat(data[i].Aug) < 0 ? 0 : parseFloat(data[i].Aug).toFixed(2)),
                        Sep: isNaN(parseFloat(data[i].Sep)) ? 0 : (parseFloat(data[i].Sep) < 0 ? 0 : parseFloat(data[i].Sep).toFixed(2)),
                        Oct: isNaN(parseFloat(data[i].Oct)) ? 0 : (parseFloat(data[i].Oct) < 0 ? 0 : parseFloat(data[i].Oct).toFixed(2)),
                        Nov: isNaN(parseFloat(data[i].Nov)) ? 0 : (parseFloat(data[i].Nov) < 0 ? 0 : parseFloat(data[i].Nov).toFixed(2)),
                        Dec: isNaN(parseFloat(data[i].Dec)) ? 0 : (parseFloat(data[i].Dec) < 0 ? 0 : parseFloat(data[i].Dec).toFixed(2)),
                        // Jan: (data[i].Jan=='-' || data[i].Jan < 0) ? 0 : (isNaN(data[i].Jan) ? 0 : parseFloat(data[i].Jan).toFixed(2)),
                        // Feb: (data[i].Feb=='-' || data[i].Feb < 0) ? 0 : (isNaN(data[i].Feb) ? 0 : parseFloat(data[i].Feb).toFixed(2)),
                        // Mar: (data[i].Mar=='-' || data[i].Mar < 0) ? 0 : (isNaN(data[i].Mar) ? 0 : parseFloat(data[i].Mar).toFixed(2)),
                        // Apr: (data[i].Apr=='-' || data[i].Apr < 0) ? 0 : (isNaN(data[i].Apr) ? 0 : parseFloat(data[i].Apr).toFixed(2)),
                        // May: (data[i].May=='-' || data[i].May < 0) ? 0 : (isNaN(data[i].May) ? 0 : parseFloat(data[i].May).toFixed(2)),
                        // Jun: (data[i].Jun=='-' || data[i].Jun < 0) ? 0 : (isNaN(data[i].Jun) ? 0 : parseFloat(data[i].Jun).toFixed(2)),
                        // Jul: (data[i].Jul=='-' || data[i].Jul < 0) ? 0 : (isNaN(data[i].Jul) ? 0 : parseFloat(data[i].Jul).toFixed(2)),
                        // Aug: (data[i].Aug=='-' || data[i].Aug < 0) ? 0 : (isNaN(data[i].Aug) ? 0 : parseFloat(data[i].Aug).toFixed(2)),
                        // Sep: (data[i].Sep=='-' || data[i].Sep < 0) ? 0 : (isNaN(data[i].Sep) ? 0 : parseFloat(data[i].Sep).toFixed(2)),
                        // Oct: (data[i].Oct=='-' || data[i].Oct < 0) ? 0 : (isNaN(data[i].Oct) ? 0 : parseFloat(data[i].Oct).toFixed(2)),
                        // Nov: (data[i].Nov=='-' || data[i].Nov < 0) ? 0 : (isNaN(data[i].Nov) ? 0 : parseFloat(data[i].Nov).toFixed(2)),
                        // Dec: (data[i].Dec=='-' || data[i].Dec < 0) ? 0 : (isNaN(data[i].Dec) ? 0 : parseFloat(data[i].Dec).toFixed(2)),
                        OutletId: dOutlet.OutletId,
                        GroupDealerId: dOutlet.parent.id,
                        GroupDealerName: dOutlet.parent.title,
                        OutletName: dOutlet.title,
                        OutletCode: dOutlet.code,
                        VehicleModelName: dModel.name,
                        VehicleModelId: dModel.id,
                        OAPRAPDocumentTypeId: parseInt($scope.filter.typeId),
                        OAPRAPTargetTypeId: $scope.filter.fieldId,
                        Year: $scope.filter.year
                    };
                } else {
                    var newObj = {
                        Jan: isNaN(parseFloat(data[i].Jan)) ? 0 : (parseFloat(data[i].Jan) < 0 ? 0 : parseFloat(data[i].Jan)),
                        Feb: isNaN(parseFloat(data[i].Feb)) ? 0 : (parseFloat(data[i].Feb) < 0 ? 0 : parseFloat(data[i].Feb)),
                        Mar: isNaN(parseFloat(data[i].Mar)) ? 0 : (parseFloat(data[i].Mar) < 0 ? 0 : parseFloat(data[i].Mar)),
                        Apr: isNaN(parseFloat(data[i].Apr)) ? 0 : (parseFloat(data[i].Apr) < 0 ? 0 : parseFloat(data[i].Apr)),
                        May: isNaN(parseFloat(data[i].May)) ? 0 : (parseFloat(data[i].May) < 0 ? 0 : parseFloat(data[i].May)),
                        Jun: isNaN(parseFloat(data[i].Jun)) ? 0 : (parseFloat(data[i].Jun) < 0 ? 0 : parseFloat(data[i].Jun)),
                        Jul: isNaN(parseFloat(data[i].Jul)) ? 0 : (parseFloat(data[i].Jul) < 0 ? 0 : parseFloat(data[i].Jul)),
                        Aug: isNaN(parseFloat(data[i].Aug)) ? 0 : (parseFloat(data[i].Aug) < 0 ? 0 : parseFloat(data[i].Aug)),
                        Sep: isNaN(parseFloat(data[i].Sep)) ? 0 : (parseFloat(data[i].Sep) < 0 ? 0 : parseFloat(data[i].Sep)),
                        Oct: isNaN(parseFloat(data[i].Oct)) ? 0 : (parseFloat(data[i].Oct) < 0 ? 0 : parseFloat(data[i].Oct)),
                        Nov: isNaN(parseFloat(data[i].Nov)) ? 0 : (parseFloat(data[i].Nov) < 0 ? 0 : parseFloat(data[i].Nov)),
                        Dec: isNaN(parseFloat(data[i].Dec)) ? 0 : (parseFloat(data[i].Dec) < 0 ? 0 : parseFloat(data[i].Dec)),
                        // Jan: (data[i].Jan=='-' || data[i].Jan < 0) ? 0 : (isNaN(data[i].Jan) ? 0 : parseFloat(data[i].Jan)),
                        // Feb: (data[i].Feb=='-' || data[i].Feb < 0) ? 0 : (isNaN(data[i].Feb) ? 0 : parseFloat(data[i].Feb)),
                        // Mar: (data[i].Mar=='-' || data[i].Mar < 0) ? 0 : (isNaN(data[i].Mar) ? 0 : parseFloat(data[i].Mar)),
                        // Apr: (data[i].Apr=='-' || data[i].Apr < 0) ? 0 : (isNaN(data[i].Apr) ? 0 : parseFloat(data[i].Apr)),
                        // May: (data[i].May=='-' || data[i].May < 0) ? 0 : (isNaN(data[i].May) ? 0 : parseFloat(data[i].May)),
                        // Jun: (data[i].Jun=='-' || data[i].Jun < 0) ? 0 : (isNaN(data[i].Jun) ? 0 : parseFloat(data[i].Jun)),
                        // Jul: (data[i].Jul=='-' || data[i].Jul < 0) ? 0 : (isNaN(data[i].Jul) ? 0 : parseFloat(data[i].Jul)),
                        // Aug: (data[i].Aug=='-' || data[i].Aug < 0) ? 0 : (isNaN(data[i].Aug) ? 0 : parseFloat(data[i].Aug)),
                        // Sep: (data[i].Sep=='-' || data[i].Sep < 0) ? 0 : (isNaN(data[i].Sep) ? 0 : parseFloat(data[i].Sep)),
                        // Oct: (data[i].Oct=='-' || data[i].Oct < 0) ? 0 : (isNaN(data[i].Oct) ? 0 : parseFloat(data[i].Oct)),
                        // Nov: (data[i].Nov=='-' || data[i].Nov < 0) ? 0 : (isNaN(data[i].Nov) ? 0 : parseFloat(data[i].Nov)),
                        // Dec: (data[i].Dec=='-' || data[i].Dec < 0) ? 0 : (isNaN(data[i].Dec) ? 0 : parseFloat(data[i].Dec)),
                        OutletId: dOutlet.OutletId,
                        GroupDealerId: dOutlet.parent.id,
                        GroupDealerName: dOutlet.parent.title,
                        OutletName: dOutlet.title,
                        OutletCode: dOutlet.code,
                        VehicleModelName: dModel.name,
                        VehicleModelId: dModel.id,
                        OAPRAPDocumentTypeId: parseInt($scope.filter.typeId),
                        OAPRAPTargetTypeId: $scope.filter.fieldId,
                        Year: $scope.filter.year
                    };
                }
                var ttl = 0;
                ttl +=  newObj['Jan']+newObj['Feb']+newObj['Mar']+newObj['Apr']+
                        newObj['May']+newObj['Jun']+newObj['Jul']+newObj['Aug']+
                        newObj['Sep']+newObj['Oct']+newObj['Nov']+newObj['Dec'];
                newObj.Total = ttl;

                result.push(newObj);
            }            
        });
        
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadOAP' ) );

        var a = myEl[0].files[0].name.split(".");
        var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        if (fileExt === "xlsx") {
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $timeout(function(){
                    myEl.value = '';
                    document.getElementById('uploadOAP').value = '';
                    // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    $scope.grid.data = validateXLS(json);
                    mDataCopy =  angular.copy($scope.grid.data);

                    uploadMode = true;
                    changeMode(editMode);
                });
            });
        } else {
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "File yang diupload bukan format .xlsx",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );   
        }
    };
    //----------------------------------
    // Get Data
    //----------------------------------
    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );

    $scope.showAlert = function(message, number){
        $.bigBox({
            title: 'Mohon Input Filter',
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            // number: ''
        });
    };

    $scope.getData = function() {
        // if($scope.filter.typeId==3)
        // {year:null,outletId:null,fieldId:null,typeId:null,areaId:null,provinceId:null};
        $scope.grid.data = [];
        alerts=[];
        if ($scope.filter.year==null || $scope.filter.outletId==null || $scope.filter.fieldId==null || $scope.filter.typeId==null) {
            $scope.showAlert(alerts.join('\n'), alerts.length);
            $timeout(function(){
              $scope.loading=false;
            })
        } else {
            if($scope.filter.fieldId==5) {
                var ssrColDef = angular.copy(colDef1);
                for (var i=13; i<25; i++) {
                    delete ssrColDef[i].aggregationHideLabel;
                    delete ssrColDef[i].aggregationType;
                }
                ssrColDef[ssrColDef.length-1].visible = false;

                // $scope.grid.columnDefs = angular.copy(colDef1);
                $scope.grid.columnDefs = angular.copy(ssrColDef);
                OAP.getSSR({year:$scope.filter.year, OutletId:outletObj.OutletId}).then(
                    function(res){
                        // console.log('OAP.getSSR : '+JSON.stringify(res.data.Result));
                        var total = 0;
                        for (var i in res.data.Result) {
                            res.data.Result[i]['Jan'] = parseFloat(res.data.Result[i]['Jan']).toFixed(2);
                            res.data.Result[i]['Feb'] = parseFloat(res.data.Result[i]['Feb']).toFixed(2);
                            res.data.Result[i]['Mar'] = parseFloat(res.data.Result[i]['Mar']).toFixed(2);
                            res.data.Result[i]['Apr'] = parseFloat(res.data.Result[i]['Apr']).toFixed(2);
                            res.data.Result[i]['May'] = parseFloat(res.data.Result[i]['May']).toFixed(2);
                            res.data.Result[i]['Jun'] = parseFloat(res.data.Result[i]['Jun']).toFixed(2);
                            res.data.Result[i]['Jul'] = parseFloat(res.data.Result[i]['Jul']).toFixed(2);
                            res.data.Result[i]['Aug'] = parseFloat(res.data.Result[i]['Aug']).toFixed(2);
                            res.data.Result[i]['Sep'] = parseFloat(res.data.Result[i]['Sep']).toFixed(2);
                            res.data.Result[i]['Oct'] = parseFloat(res.data.Result[i]['Oct']).toFixed(2);
                            res.data.Result[i]['Nov'] = parseFloat(res.data.Result[i]['Nov']).toFixed(2);
                            res.data.Result[i]['Dec'] = parseFloat(res.data.Result[i]['Dec']).toFixed(2);

                            total=0;
                            total+=
                            parseFloat(res.data.Result[i]['Jan'])+parseFloat(res.data.Result[i]['Feb'])+
                            parseFloat(res.data.Result[i]['Mar'])+parseFloat(res.data.Result[i]['Apr'])+
                            parseFloat(res.data.Result[i]['May'])+parseFloat(res.data.Result[i]['Jun'])+
                            parseFloat(res.data.Result[i]['Jul'])+parseFloat(res.data.Result[i]['Aug'])+
                            parseFloat(res.data.Result[i]['Sep'])+parseFloat(res.data.Result[i]['Oct'])+
                            parseFloat(res.data.Result[i]['Nov'])+parseFloat(res.data.Result[i]['Dec']);
                            // res.data.Result[i]['Jan']+res.data.Result[i]['Feb']+
                            // res.data.Result[i]['Mar']+res.data.Result[i]['Apr']+
                            // res.data.Result[i]['May']+res.data.Result[i]['Jun']+
                            // res.data.Result[i]['Jul']+res.data.Result[i]['Aug']+
                            // res.data.Result[i]['Sep']+res.data.Result[i]['Oct']+
                            // res.data.Result[i]['Nov']+res.data.Result[i]['Dec'];
                            res.data.Result[i].Total = total;
                        }                   
                        $scope.grid.data = res.data.Result;

                        mDataCopy =  angular.copy($scope.grid.data);

                        $scope.loading=false;
                        changeMode(viewMode);
                        return res.data;
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
            }
            else {
                if($scope.filter.fieldId==3 || $scope.filter.fieldId==4){
                    $scope.grid.columnDefs = angular.copy(colDef2);
                } else {
                    $scope.grid.columnDefs = angular.copy(colDef1);
                }
                $scope.filter.outletId = outletObj.OutletId;
                OAP.getData($scope.filter).then(
                    function(res){
                        // console.log('OAP.getData : '+JSON.stringify(res.data.Result));
                        console.log(JSON.stringify(res.data.Result[0]));
                        var total = 0;
                        for (var i in res.data.Result) {
                            if($scope.filter.fieldId==3 || $scope.filter.fieldId==4){
                                res.data.Result[i]['Jan'] = parseFloat(res.data.Result[i]['Jan']*100).toFixed(2);
                                res.data.Result[i]['Feb'] = parseFloat(res.data.Result[i]['Feb']*100).toFixed(2);
                                res.data.Result[i]['Mar'] = parseFloat(res.data.Result[i]['Mar']*100).toFixed(2);
                                res.data.Result[i]['Apr'] = parseFloat(res.data.Result[i]['Apr']*100).toFixed(2);
                                res.data.Result[i]['May'] = parseFloat(res.data.Result[i]['May']*100).toFixed(2);
                                res.data.Result[i]['Jun'] = parseFloat(res.data.Result[i]['Jun']*100).toFixed(2);
                                res.data.Result[i]['Jul'] = parseFloat(res.data.Result[i]['Jul']*100).toFixed(2);
                                res.data.Result[i]['Aug'] = parseFloat(res.data.Result[i]['Aug']*100).toFixed(2);
                                res.data.Result[i]['Sep'] = parseFloat(res.data.Result[i]['Sep']*100).toFixed(2);
                                res.data.Result[i]['Oct'] = parseFloat(res.data.Result[i]['Oct']*100).toFixed(2);
                                res.data.Result[i]['Nov'] = parseFloat(res.data.Result[i]['Nov']*100).toFixed(2);
                                res.data.Result[i]['Dec'] = parseFloat(res.data.Result[i]['Dec']*100).toFixed(2);
                            }

                            total=0;
                            total+=
                            parseFloat(res.data.Result[i]['Jan'])+parseFloat(res.data.Result[i]['Feb'])+
                            parseFloat(res.data.Result[i]['Mar'])+parseFloat(res.data.Result[i]['Apr'])+
                            parseFloat(res.data.Result[i]['May'])+parseFloat(res.data.Result[i]['Jun'])+
                            parseFloat(res.data.Result[i]['Jul'])+parseFloat(res.data.Result[i]['Aug'])+
                            parseFloat(res.data.Result[i]['Sep'])+parseFloat(res.data.Result[i]['Oct'])+
                            parseFloat(res.data.Result[i]['Nov'])+parseFloat(res.data.Result[i]['Dec']);
                            // res.data.Result[i]['Jan']+res.data.Result[i]['Feb']+
                            // res.data.Result[i]['Mar']+res.data.Result[i]['Apr']+
                            // res.data.Result[i]['May']+res.data.Result[i]['Jun']+
                            // res.data.Result[i]['Jul']+res.data.Result[i]['Aug']+
                            // res.data.Result[i]['Sep']+res.data.Result[i]['Oct']+
                            // res.data.Result[i]['Nov']+res.data.Result[i]['Dec'];
                            res.data.Result[i].Total = total;
                        }
                        $scope.grid.data = res.data.Result;

                        mDataCopy =  angular.copy($scope.grid.data);

                        $scope.loading=false;
                        changeMode(viewMode);
                        return res.data;
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
            }
        }
        
    }

    OAPPeriod.getData().then(
        function(res){
            // $scope.yearData = res.data.Result;
            var temp = res.data.Result;
            $scope.yearData = [];
            var obj={};
            for (var i in temp) {
                if (!obj[temp[i].Year]) {
                    obj[temp[i].Year] = {};
                    obj[temp[i].Year]["name"] = temp[i].Year;
                    obj[temp[i].Year]["id"] = temp[i].Year;
                    $scope.yearData.push(obj[temp[i].Year]);
                }                     
            }
            
            console.log("$scope.yearData : "+JSON.stringify($scope.yearData));
            $scope.loading=false;
            return res.data;
        }
    );

    $scope.formApi={};

    $scope.gridApi={};

    var colDef1 = [
            { name:'OAPRAPDocumentTypeId',    field:'OAPRAPDocumentTypeId', visible:false },
            { name:'OAPRAPTargetTypeId',    field:'OAPRAPTargetTypeId', visible:false },
            { name:'ProvinceId',    field:'ProvinceId', visible:false },
            { name:'ProvinceName',    field:'ProvinceName', visible:false },
            { name:'AreaId',    field:'AreaId', visible:false },
            { name:'Year',    field:'Year', visible:false },
            { name:'Period',    field:'Period', visible:false },
            { name:'GroupDealerId', field:'GroupDealerId', visible: false},
            { name:'OutletCode', field:'OutletCode', enableCellEdit: false},
            // { name:'Dealer', field:'GroupDealerName', enableCellEdit: false},
            { name:'OutletId', field:'OutletId', visible:false},
            // { name:'OutletCode', field:'OutletCode', visible:false},
            { name:'Outlet', field:'OutletName', enableCellEdit: false},
            { name:'VehicleModelId', field:'VehicleModelId', visible:false},
            { name:'Model', field: 'VehicleModelName', enableCellEdit: false},
            { name:'Jan', field: 'Jan', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum},
            { name:'Feb', field: 'Feb', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Mar', field: 'Mar', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Apr', field: 'Apr', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'May', field: 'May', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jun', field: 'Jun', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jul', field: 'Jul', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Aug', field: 'Aug', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Sep', field: 'Sep', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Oct', field: 'Oct', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Nov', field: 'Nov', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Dec', field: 'Dec', enableCellEdit: true, type:'number', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'JanId', field: 'JanId' , visible: false},
            { name:'FebId', field: 'FebId' , visible: false},
            { name:'MarId', field: 'MarId' , visible: false},
            { name:'AprId', field: 'AprId' , visible: false},
            { name:'MayId', field: 'MayId' , visible: false},
            { name:'JunId', field: 'JunId' , visible: false},
            { name:'JulId', field: 'JulId' , visible: false},
            { name:'AugId', field: 'AugId' , visible: false},
            { name:'SepId', field: 'SepId' , visible: false},
            { name:'OctId', field: 'OctId' , visible: false},
            { name:'NovId', field: 'NovId' , visible: false},
            { name:'DecId', field: 'DecId' , visible: false},
            { name:'Total', field: 'Total' }
        ];

    var colDef2 = [
            { name:'OAPRAPDocumentTypeId',    field:'OAPRAPDocumentTypeId', visible:false },
            { name:'OAPRAPTargetTypeId',    field:'OAPRAPTargetTypeId', visible:false },
            { name:'ProvinceId',    field:'ProvinceId', visible:false },
            { name:'ProvinceName',    field:'ProvinceName', visible:false },
            { name:'AreaId',    field:'AreaId', visible:false },
            { name:'Year',    field:'Year', visible:false },
            { name:'Period',    field:'Period', visible:false },
            { name:'GroupDealerId', field:'GroupDealerId', visible: false},
            { name:'OutletCode', field:'OutletCode', enableCellEdit: false},
            // { name:'Dealer', field:'GroupDealerName', enableCellEdit: false},
            { name:'OutletId', field:'OutletId', visible:false},
            // { name:'OutletCode', field:'OutletCode', visible:false},
            { name:'Outlet', field:'OutletName', enableCellEdit: false},
            { name:'VehicleModelId', field:'VehicleModelId', visible:false},
            { name:'Model', field: 'VehicleModelName', enableCellEdit: false},
            { name:'Jan', field: 'Jan', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Feb', field: 'Feb', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Mar', field: 'Mar', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Apr', field: 'Apr', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'May', field: 'May', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Jun', field: 'Jun', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Jul', field: 'Jul', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Aug', field: 'Aug', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Sep', field: 'Sep', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Oct', field: 'Oct', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Nov', field: 'Nov', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'Dec', field: 'Dec', enableCellEdit: true, type:'number', cellFilter:'percentage'},
            { name:'JanId', field: 'JanId' , visible: false},
            { name:'FebId', field: 'FebId' , visible: false},
            { name:'MarId', field: 'MarId' , visible: false},
            { name:'AprId', field: 'AprId' , visible: false},
            { name:'MayId', field: 'MayId' , visible: false},
            { name:'JunId', field: 'JunId' , visible: false},
            { name:'JulId', field: 'JulId' , visible: false},
            { name:'AugId', field: 'AugId' , visible: false},
            { name:'SepId', field: 'SepId' , visible: false},
            { name:'OctId', field: 'OctId' , visible: false},
            { name:'NovId', field: 'NovId' , visible: false},
            { name:'DecId', field: 'DecId' , visible: false},
            { name:'Total', field: 'Total', visible: false }
        ];

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        showGridFooter: true,
        showColumnFooter: true,
        multiSelect: true,
        enableCellEdit: false,
        enableSelectAll: true,
        enableCellEditOnFocus: false,
        cellEditableCondition: function($scope) {
            return editableMode;
        },
        columnDefs: colDef1
    };

    $scope.afterRegisterGridApi = function(gridApi) {
        $scope.gridApi = gridApi;
        console.log($scope.gridApi);

        $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            console.log('Inside afterCellEdit..');
            if (newValue<0) {
                newValue = 0;
                rowEntity[colDef.field] = newValue;
            }
            rowEntity.Total+=(newValue-oldValue);
        });
        // uiGridApi.core.registerColumnsProcessor(function(columns, rows){
        //     angular.forEach(columns, function(col){
        //         var filterFormat = col.colDef.filterFormat; 
        //         if ( filterFormat ){ col.filterFormat = filterFormat; }
        //     });
        //     return rows;
        // }, 40);
    };
});
