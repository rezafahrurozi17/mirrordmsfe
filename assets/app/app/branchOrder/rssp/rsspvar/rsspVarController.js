angular.module('app')
    .controller('RSSPVarController', function($scope, $http, CurrentUser, RSSPVar,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRSSPVar = null; //Model
    $scope.cRsspVar = null; //Collection
    $scope.xRSSPVar = {};
    $scope.xRSSPVar.selected=[];
    $scope.varTypeData = [];
    $scope.parentData = [];
    // $scope.varTypeData = [
    //     {VariableInputTypeId:1, VariableType:'Data Entry'},
    //     {VariableInputTypeId:2, VariableType:'Formula'},
    //     {VariableInputTypeId:3, VariableType:'Validation'},
    //     {VariableInputTypeId:4, VariableType:'Constants'},
    //     {VariableInputTypeId:5, VariableType:'Data'},
    // ];
    // $scope.varFormatData = [
    //     {FormatTypeId:1, TypeFormat:'Number'},
    //     {FormatTypeId:2, TypeFormat:'Decimal'},
    //     {FormatTypeId:3, TypeFormat:'Percentage'},
    //     {FormatTypeId:4, TypeFormat:'Text'},
    // ];
    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];
    
    RSSPVar.getType().then( 
        function(res){
            // console.log("res type", res);
            $scope.varTypeData = res.data.Result;
            $scope.loading=false;
            return res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );


    RSSPVar.getFormat().then(
        function(res){
            // console.log("res format", res);
            $scope.varFormatData = res.data.Result;
            $scope.loading=false;
            return res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    
    $scope.getData = function() {
        RSSPVar.getDataTree().then(
            function(res){
                console.log(res.data.Result);
                $scope.grid.data = [];
                $scope.parentData = [];
                for(row in res.data.Result){
                    $scope.parentData.push(res.data.Result[row]);
                    res.data.Result[row].$$treeLevel = 0;
                    $scope.grid.data.push(res.data.Result[row]);
                    for(child in res.data.Result[row].Child){
                        res.data.Result[row].Child[child].$$treeLevel = 1;
                        $scope.grid.data.push(res.data.Result[row].Child[child]);
                        for(gchild in res.data.Result[row].Child[child].Child){
                            res.data.Result[row].Child[child].Child[gchild].$$treeLevel = 2;
                            $scope.grid.data.push(res.data.Result[row].Child[child].Child[gchild]);
                        }
                    }
                }
                console.log(res.data.Result);
                $scope.loading=false;
                return res.data.Result;
                // return res.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        ); 
    } 
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        showTreeExpandNoChildren: false,
        enableRowHeaderSelection: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'AdjustmentRatioTypeId', visible:false }, 
            { name:'pid', field:'ParentId', visible:false },
            { name:'Variable Name', width:'20%', field: 'AdjustmentName' },
            { name:'Type', width:'15%', field: 'VariableType' },
            { name:'Format', width:'15%', field: 'TypeFormat' },
            { name:'Field Name', field:'BEFieldName'},
            { name:'Description', field: 'Description' },
            // { name:'StatusCode',    field:'StatusCode', visible:false },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
