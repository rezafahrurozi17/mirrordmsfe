angular.module('app')
  .factory('RSSPVar', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/AdjustmentRatioMasterConstantas/?start=1&limit=100');
        return res;
      },
      getDataTree: function() {
        var res=$http.get('/api/ds/AdjustmentRatioMasterConstantas/GetTree/?start=1&limit=100');
        return res;
      },
      getType: function() {
        return $http.get('/api/ds/Variabels');
      },
      getFormat: function() {
        return $http.get('/api/ds/FormatTypes');
      },
      find: function(rsspvar) {
        console.log("rsspvar find -->", rsspvar);
        return $http.get('/api/ds/AdjustmentRatioMasterConstantas/?start=1&limit=10&parameterName=', rsspvar);
      },
      create: function(rsspvar) {
        console.log("rsspvar factory",rsspvar);
        console.log("rsspvar factory string",JSON.stringify(rsspvar));
        return $http.post('/api/ds/AdjustmentRatioMasterConstantas/Multiple', [rsspvar]);
        
      },
      update: function(rsspvar){
        return $http.put('/api/ds/AdjustmentRatioMasterConstantas/Multiple', [rsspvar]);
      },
      delete: function(id) {
      var aa = [];
      var bb = [];
      
       for (var i = 0; i < id.length ; i++) {
         var cc={};
         cc.AdjustmentRatioTypeId=id[i];
         aa.push(cc);
       };

       if (aa.length>0) {
            bb=aa;
          } else {
            var cc={};
             cc.AdjustmentRatioTypeId = id;
             bb.push(cc);
          } 
          console.log("delete==>",JSON.stringify(bb));
          console.log("delete2==>",bb);
          return $http.delete('/api/ds/AdjustmentRatioMasterConstantas/Multiple', {data:bb,headers: {'Content-Type': 'application/json'}});
        },
    }
  });