angular.module('app')
    .controller('KonstantaController', function($scope, $http, CurrentUser, Konstanta, Region, RSSPVar, GetsudoPeriod, OrgChart, ProductModel, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mKonstanta = {}; //Model
    $scope.xKonstanta = {};
    $scope.xKonstanta.selected=[];
    $scope.areaData = [];
    $scope.provinceData = [];
    // $scope.dealerData = [];
    $scope.outletData = [];
    // $scope.outletData
    $scope.startData = [];
    $scope.modelData = [];
    $scope.typeData = [];
    $scope.typeColorData = [];
    $scope.konstantData = [];

    // $scope.mKonstanta = {OutletId:null,GroupDealerId:null,AreaId:null,ProvinceId:null,VehicleModelId:null,VehicleTypeId:null,VehicleTypeColerId:null};
    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    $scope.orgData= [];



    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.startDateOptions = {
        startingDay: 1,
        format: dateFormat,
        disableWeekend: 1
    };


    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];
    $scope.getData = function() {
        Konstanta.getData().then(
            function(res){
                // $scope.grid.data = res.data.Result;
                // $scope.mKonstanta = res.data.Result;
                var gdata = res.data.Result;
                for (var i = 0; i < gdata.length; i++) {
                    gdata[i].RatioPriority = gdata[i].Priority;
                    gdata[i].StartPeriod = gdata[i].Period;
                    // gdata[i].Period = new Date(gdata[i].Period);
                } 

                $scope.grid.data = gdata;
                $scope.loading=false;
                return gdata;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    GetsudoPeriod.getData().then(
        function(res){
            var gets = res.data.Result;
            $scope.periodData = gets;

            $scope.loading=false;
            return gets;
        }
    );
    
    OrgChart.getDataTree().then(
        function(res){

            orgTreeData = res.data.Result;

                // $scope.orgData = res.data.Result;
                // for(i in $scope.orgData){
                //     for(ch in $scope.orgData[i].child){
                //         outletData.push($scope.orgData[i].child[ch]);
                //     }
                // }
                // if($scope.orgData.length==1){
                //     $scope.mKonstanta.OutletId = $scope.orgData[0].id;
                // }

            $scope.loading=false;
            return res.data;
        }
    );
    Region.getAreaProvince().then(
        function(res){
            areaProvince = res.data.Result;
            filterArea();
            filterProvince();
            $scope.loading=false;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
    }

    function filterDealer(filter){
        $scope.orgData = [];
        var dataSrc = [];
        // console.log('filterDealer=',filter);
        if(filter==undefined || filter==null){
            dataSrc = orgTreeData;
        }else{
            for(var i in orgTreeData){
                var inserted = false;
                for(var chIdx in orgTreeData[i].child){
                    var valid = true;
                    for(key in filter){
                        if(orgTreeData[i].child[chIdx][key]!=filter[key]){
                            valid=false; break;
                            // console.log("orgTreeData[i].child[chIdx][key]",orgTreeData[i].child[chIdx][key]);
                        }
                    }
                    if(valid){
                        if(!inserted){
                            var parent = angular.copy(orgTreeData[i]);
                            parent.child = [];
                            parent.listOutlet = [];
                            dataSrc.push(parent);
                            inserted=true;
                        }
                        dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
                        
                    }
                }
            }
        }
        $scope.orgData = dataSrc;
        // console.log('$scope.orgData',$scope.orgData);
    }

    $scope.changeArea = function(selected){
        // console.log("changeArea selected", selected);
        if(selected==null){
            filterProvince();
            filterDealer($scope.mKonstanta.ProvinceId==null?null:{ProvinceId:$scope.mKonstanta.ProvinceId});
        }else{
            filterProvince({AreaId:selected});
            filterDealer($scope.mKonstanta.ProvinceId==null?{AreaId:selected}:{AreaId:selected,ProvinceId:$scope.mKonstanta.ProvinceId});
        }

                // /////////////

                var xca = $scope.provinceData;
                // console.log("xca", xca);
                for(i=0; i<xca.length; i++){
                    if (xca[i].ProvinceId == $scope.mKonstanta.ProvinceId) {
                        var yes = true;
                    }
                }

                if(!yes){
                    $scope.mKonstanta.ProvinceId = null;
                }
    }
    $scope.changeProvince = function(selected){
        if(selected==null){
            filterArea();
            filterDealer($scope.mKonstanta.AreaId==null?null:{AreaId:$scope.mKonstanta.AreaId});
        }else{
            filterArea({ProvinceId:selected});
            filterDealer($scope.mKonstanta.AreaId==null?{ProvinceId:selected}:{ProvinceId:selected,AreaId:$scope.mKonstanta.AreaId});
        }

                var xcp = $scope.orgData;
                // console.log("xcp", xcp);
                for(i=0; i<xcp.length; i++){
                    if (xcp[i].GroupDealerId == $scope.mKonstanta.GroupDealerId) {
                        var yes = true;
                    }
                }

                if(!yes){
                    $scope.mKonstanta.GroupDealerId = null;
                    $scope.mKonstanta.OutletId = null;
                }
    }

    $scope.onShowDetail = function(){
        $timeout(function(){
            $scope.selectedModel = _.find($scope.productModelData,{VehicleModelId:$scope.mKonstanta.VehicleModelId});
            $scope.selectedType = _.find($scope.selectedModel.listVehicleType, {VehicleTypeId:$scope.mKonstanta.VehicleTypeId})
            $scope.outletData = _.find($scope.orgData, {GroupDealerId: $scope.mKonstanta.GroupDealerId});
            // var pData = _.find($scope.periodData, {GetsudoPeriodId: $scope.mKonstanta.GetsudoPeriodId});
            // $scope.StartPeriod = pData.StartPeriod; 

            // mKonstanta.required = false;        
            // $scope.resetForm(true);

        });  
    }

    $scope.selectDealer = function(xdealer){
        $scope.outletData = xdealer;

        // /////////////

                var xdr = $scope.outletData.child;
                // console.log("xdr", xdr);
                for(i=0; i<xdr.length; i++){
                    if (xdr[i].OutletId == $scope.mKonstanta.OutletId) {
                        var yes = true;
                    }
                }

                if(!yes){
                    $scope.mKonstanta.OutletId = null;
                }

                // console.log("$scope.mKonstanta.OutletId ==>",$scope.mKonstanta.OutletId);

                // /////////////
    }

    $scope.selectPeriod = function(period){
        // console.log("period=>", JSON.stringify(period));
        $scope.mKonstanta.Period = period.StartPeriod;

    }

    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;
            $scope.loading=false;
            return res.data.Result;
        }
    );
    
    RSSPVar.find(4).then(
        function(res){
            $scope.konstantData = res.data.Result;
            $scope.loading=false;
            return res.data.Result; 
        },
        function(err){
            console.log("err=>",err);
        }
    ); 

    $scope.onSelectRows = function(rows){
        // console.log("onSelectRows=>",rows);
    }

    $scope.selectModel = function(selected){
        $scope.selectedModel = selected;
        
        var xmd = $scope.selectedModel.listVehicleType;
        // console.log("xmd xmd xmd xmd", xmd);
        for(i=0; i<xmd.length; i++){
            if (xmd[i].VehicleTypeId == $scope.mKonstanta.VehicleTypeId) {
                var yes = true;
            }
        }

        if(!yes){
            $scope.mKonstanta.VehicleTypeId = null;
            $scope.mKonstanta.VehicleTypeColorId = null;
        }
    }

    $scope.selectType = function(selected){
        $scope.selectedType = selected;

        var xtp = $scope.selectedType.listColor;
        // console.log("xtp", xtp);
        for(i=0; i<xtp.length; i++){
            if (xtp[i].VehicleTypeColorId == $scope.mKonstanta.VehicleTypeColorId) {
                var yes = true;
            }
        }

        if(!yes){
            $scope.mKonstanta.VehicleTypeColorId = null;
        }

    }

    $scope.onBeforeNewMode = function(){
        // console.log("sampai onBeforeNewMode");
        $scope.mKonstanta = {};
        // $scope.StartPeriod = null;
    }

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'id',         field:'AdjustmentRatioRelationId', visible:false },
            { name:'PeriodId',   field:'GetsudoPeriodId',           visible:false }, //
            { name:'Period',     field:'Period',                    visible:false },
            // { name:'startPeriod',   field:'StartPeriod'                     }, //,visible:false
            { name:'rsspVarId',  field:'AdjustmentRatioTypeId',     visible:false },
            { name:'areaId',     field:'AreaId',                    visible:false },
            { name:'provinceId', field:'ProvinceId',                visible:false },
            { name:'dealerId',   field:'GroupDealerId',             visible:false },
            { name:'outletId',   field:'OutletId',                  visible:false },
            { name:'modelId',    field:'VehicleModelId',            visible:false },
            { name:'typeId',     field:'VehicleTypeId',             visible:false },
            { name:'colorId',    field:'VehicleTypeColorId',        visible:false },
            { name:'Konstanta',  field: 'AdjustmentName' }, 
            { name:'Nilai',      field: 'AdjustmentRatioValue' },
            { name:'Prioritas',  field: 'Priority' },
            { name:'Area',       field: 'AreaName' },
            { name:'Provinsi',   field: 'ProvinceName' },
            { name:'Dealer',     field: 'GroupDealerName' },
            { name:'Outlet',     field: 'OutletName' },
            { name:'Model',      field: 'VehicleModelName' },
            { name:'Tipe',       field: 'VehicleTypeName' },
            { name:'Tipe Warna', field: 'TypeColor' },
        ]
    };
});
