angular.module('app')
  .factory('Konstanta', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/AdjustmentRatioConstantas/?start=1&limit=1000');
        return res;
      },
      create: function(obj) {
        return $http.post('/api/ds/AdjustmentRatioConstantas/Multiple', [{
                                                                            // "AdjustmentRatioRelationId":obj.AdjustmentRatioRelationId,
                                                                            "AreaId":obj.AreaId,
                                                                            "ProvinceId":obj.ProvinceId,
                                                                            "GroupDealerId":obj.GroupDealerId,
                                                                            "OutletId":obj.OutletId,
                                                                            "VehicleModelId":obj.VehicleModelId,
                                                                            "VehicleTypeColorId":obj.VehicleTypeColorId,
                                                                            "AdjustmentRatioTypeId":obj.AdjustmentRatioTypeId,
                                                                            "AdjustmentRatioValue":obj.AdjustmentRatioValue,
                                                                            "Period":obj.Period,
                                                                            "RatioPriority":obj.RatioPriority,
                                                                                      // "StatusCode":1,
                                                                            "VehicleTypeId":obj.VehicleTypeId,
                                                                            "GetsudoPeriodId":obj.GetsudoPeriodId

                                                                        }]);
      },

      update: function(obj){
        return $http.put('/api/ds/AdjustmentRatioConstantas/Multiple', [{
                                                                            "AdjustmentRatioRelationId":obj.AdjustmentRatioRelationId,
                                                                            "AreaId":obj.AreaId,
                                                                            "ProvinceId":obj.ProvinceId,
                                                                            "GroupDealerId":obj.GroupDealerId,
                                                                            "OutletId":obj.OutletId,
                                                                            "VehicleModelId":obj.VehicleModelId,
                                                                            "VehicleTypeColorId":obj.VehicleTypeColorId,
                                                                            "AdjustmentRatioTypeId":obj.AdjustmentRatioTypeId,
                                                                            "AdjustmentRatioValue":obj.AdjustmentRatioValue,
                                                                            "Period":obj.Period,
                                                                            "RatioPriority":obj.RatioPriority,
                                                                            // "StatusCode":1,
                                                                            "VehicleTypeId":obj.VehicleTypeId,
                                                                            "GetsudoPeriodId":obj.GetsudoPeriodId

                                                                        }]);
      },

      delete: function(id) {

        var aa = [];
        var bb = [];
        
         for (var i = 0; i < id.length ; i++) {
           var cc={};
           cc.AdjustmentRatioRelationId=id[i];
           aa.push(cc);
         };

         if (aa.length>0) {
              bb=aa;
            } else {
              var cc={};
               cc.AdjustmentRatioRelationId = id;
               bb.push(cc);
            }

        return $http.delete('/api/ds/AdjustmentRatioConstantas/Multiple', {data:bb,headers: {'Content-Type': 'application/json'}});
      },
    }
  });