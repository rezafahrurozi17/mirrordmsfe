angular.module('app')
    .controller('RSSPInternalController', function($scope, $http, uiGridConstants, CurrentUser, OrgChart, GetsudoPeriod, ProductModel, CustomerFleet, InternalOutlet, RSSPInternal, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRSSPInternal = {}; //Model
    $scope.xRSSPInternal = {};
    $scope.xRSSPInternal.selected=[];
    $scope.selectedModel = {};
    $scope.selectedType = {};

    $scope.tempRSSP = {selOutletId:null, selVehicleModelId:null, selVehicleTypeId:null, selColorId:null};
    
    $scope.filter = {GetsudoPeriodId:null, CustomerCode:null, OutletId:null, VehicleModelId:null,  VehicleTypeId:null, productType:null, ColorId:null};
    // $scope.filter = {period:null, CustomerCode:null, OutletCode:null, VehicleModelName:null, GroupDealerId:null};
    $scope.periodActive = false;
    var mDataCopy;

    $scope.orgId = null;
    var mode = 0;
    var addMode = 2;
    var editMode = 1;
    var viewMode = 0;

    $scope.hideNewButton = false;

    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    var editableMode = false;

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;

            $scope.customActionSettings[0].visible = mode==editMode || mode==addMode;
            $scope.customActionSettings[1].visible = mode==editMode || mode==addMode;
            $scope.customActionSettings[2].visible = mode==addMode;
            $scope.customActionSettings[3].visible = mode==addMode;
            $scope.customActionSettings[4].visible = ($scope.grid.data.length>0) && (mode == viewMode) && ($scope.periodActive);
            $scope.grid.enableCellEditOnFocus = mode=editMode;
            mode = mode;
        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
                $scope.mRSSPInternal = angular.copy(mDataCopy);
                $scope.hideNewButton=false;
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                // $scope.mRSSPInternal.detail = $scope.grid.data;
                var updateDt = [];
                for (var i in $scope.mRSSPInternal) {
                    if ($scope.mRSSPInternal[i]!==null && $scope.mRSSPInternal[i]!==undefined && !angular.equals($scope.mRSSPInternal[i], {})) {
                        // console.log("N : "+$scope.mRSSPInternal[i].N+", "+
                        //             "N1 : "+$scope.mRSSPInternal[i].N1+", "+
                        //             "N2 : "+$scope.mRSSPInternal[i].N2+", "+
                        //             "N3 : "+$scope.mRSSPInternal[i].N3+", "+
                        //             "N4 : "+$scope.mRSSPInternal[i].N4+", "+
                        //             "N5 : "+$scope.mRSSPInternal[i].N5);
                        var temp = {
                            "idx": $scope.mRSSPInternal[i].idx,
                            "NId": $scope.mRSSPInternal[i].NId,
                            "VehicleModelId": $scope.mRSSPInternal[i].VehicleModelId,
                            "N1Id": $scope.mRSSPInternal[i].N1Id,
                            "N2Id": $scope.mRSSPInternal[i].N2Id,
                            "N3Id": $scope.mRSSPInternal[i].N3Id,
                            "N4Id": $scope.mRSSPInternal[i].N4Id,
                            "N5Id": $scope.mRSSPInternal[i].N5Id,
                            "Period": $scope.mRSSPInternal[i].Period,
                            "PeriodDetail": $scope.mRSSPInternal[i].PeriodDetail,
                            "OutletTAMId": $scope.mRSSPInternal[i].OutletTAMId,
                            "OutletCode": $scope.mRSSPInternal[i].OutletCode,
                            "OutletName": $scope.mRSSPInternal[i].OutletName,
                            "GroupDealerId": $scope.mRSSPInternal[i].GroupDealerId,
                            "GroupDealerName": $scope.mRSSPInternal[i].GroupDealerName,
                            "VehicleModelName": $scope.mRSSPInternal[i].VehicleModelName,
                            "VehicleTypeId": $scope.mRSSPInternal[i].VehicleTypeId,
                            "KatashikiCode": $scope.mRSSPInternal[i].KatashikiCode,
                            "SuffixCode": $scope.mRSSPInternal[i].SuffixCode,
                            "ColorId": $scope.mRSSPInternal[i].ColorId,
                            "ColorCode": $scope.mRSSPInternal[i].ColorCode,
                            "ColorName": $scope.mRSSPInternal[i].ColorName,
                            "N": $scope.mRSSPInternal[i].N,
                            "N1": $scope.mRSSPInternal[i].N1,
                            "N2": $scope.mRSSPInternal[i].N2,
                            "N3": $scope.mRSSPInternal[i].N3,
                            "N4": $scope.mRSSPInternal[i].N4,
                            "N5": $scope.mRSSPInternal[i].N5,
                        };
                        updateDt.push(temp);
                    }
                }

                RSSPInternal.update(updateDt).then(function(){
                    changeMode(viewMode);
                    $scope.hideNewButton=false;
                });;
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];
                angular.forEach($scope.orgData, function(org, orgIdx){
                    angular.forEach(org.child, function(dlr, dlrIdx){
                        angular.forEach(dlr.child, function(outlet, outletIdx){
                            angular.forEach($scope.productModelData, function(model, modelIdx){
                                generatedData.push({Dealer:dlr.title, Outlet:outlet.title, Model:model.name, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                            });
                        });
                    });
                });
                XLSXInterface.writeToXLSX(generatedData, 'RSSPInternal - '+$scope.yearData[$scope.mRSSPInternal.year].name);
            },
            visible:false,
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                document.getElementById('uploadRSSPInternal').click();
            },
            visible:false,
            title: 'Load File',
            rightsBit: 1
        },{
            func: function(string) {
                changeMode(editMode);
                $scope.hideNewButton=true;
            },
            visible: false,
            title: 'Edit',
            rightsBit: 2
        }
    ];

    $scope.customDelete = function(selected) {
        RSSPInternal.delete(selected).then(function(res){
            console.log(res);
            $scope.getData();
        });
    };

    // $scope.customBulkSettings = [
    //     {   
    //         func: function(string) {
    //             RSSPInternal.delete($scope.xRSSPInternal.selected).then(function(res){
    //                 console.log(res);
    //                 RSSPInternal.getData($scope.filter).then(
    //                     function(res){
    //                         // console.log(JSON.stringify(res.data));                
    //                         $scope.grid.data = res.data.Result;                
    //                         $scope.mRSSPInternal = res.data.Result;
    //                         $scope.loading=false;
    //                         changeMode(viewMode);
    //                         return res.data;
    //                     },
    //                     function(err){
    //                         console.log("err=>",err);
    //                     }
    //                 );
    //             });
    //         },
    //         title: 'Delete Selected',
    //         visible:false,
    //         color: '#ccc'
    //     }
    // ];

    var outletData = [];
    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];

    GetsudoPeriod.getData().then(
        function(res){
            // console.log("GetsudoPeriod.getData() : "+JSON.stringify(res.data));
            for (var i in res.data.Result) {
                // console.log(res.data.Result[i].StartPeriod);
                // var temp = new Date(res.data.Result[i].StartPeriod);
                // var year = temp.getFullYear().toString();
                // var mnth = (temp.getMonth()<10 ? '0'+parseInt(temp.getMonth()+1) : (temp.getMonth()+1).toString());
                // var date = (temp.getDate()<10 ? '0'+parseInt(temp.getDate()) : temp.getDate().toString());
                // res.data.Result[i].period = parseInt(year+mnth+date);

                // console.log(res.data.Result[i].GetsudoPeriodCode);

                // Return in int
                // res.data.Result[i].period = parseInt(res.data.Result[i].GetsudoPeriodCode+'01');

                // Return in datetime
                res.data.Result[i].period = res.data.Result[i].StartPeriod;
            }
            $scope.periodData = res.data.Result;
            // console.log($scope.periodData);            
            $scope.loading=false;
            return res.data.Result;
        }
    );
    // CustomerFleet.getData().then(
    //     function(res){
    //         // console.log("CustomerFleet.getData() : "+JSON.stringify(res.data));
    //         $scope.InternalData = res.data.Result;
    //         $scope.loading=false;
    //         return res.data.Result;
    //     }
    // );
    // OrgChart.getDataByRoleUser($scope.user).then(
    InternalOutlet.getData().then(
        function(res){
            // console.log("OrgChart.getDataByRoleUser() : "+JSON.stringify(res.data.Result));
            $scope.orgData = res.data.Result;
            $scope.orgData2 = res.data.Result;
            $scope.loading=false;
            return res.data.Result;
        }
    );
    ProductModel.getDataTree().then(
        function(res){
            // console.log("ProductModel.getData() : "+JSON.stringify(res.data));
            $scope.productModelData = res.data.Result;
            // ProductModel.getDataType().then(
            //     function(xres){
            //         $scope.productModelTypeData = xres.data.Result;
            //         ProductModel.getDataTypeColor().then(
            //             function(yres){
            //                 $scope.productModelTypeColorData = xres.data.Result;
            //                 $scope.loading=false;
            //             }
            //         );                    
            //     }
            // );            
            return res.data.Result;
        }
    );

    function findParentOfTypeColor(id){
        console.log(id);
        var res = {model:null, type:null, typeColor:null};
        for(var i=0; i<$scope.productModelData.length; i++){
            // console.log("$scope.productModelData[i] : "+JSON.stringify($scope.productModelData[i]));
            for(var j=0; j<$scope.productModelData[i].listVehicleType.length; j++){
                for(var k=0; k<$scope.productModelData[i].listVehicleType[j].listColor.length; k++){
                    // console.log($scope.productModelData[i].listVehicleType[j].listColor[k].ColorId,'vs',id);
                    if($scope.productModelData[i].listVehicleType[j].listColor[k].ColorId==id){
                        console.log('true');
                        res.model = $scope.productModelData[i].VehicleModelName;
                        res.typeId = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.VehicleTypeId;
                        // res.typeColor = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.KatashikiCode+' '+
                        //                 $scope.productModelData[i].listVehicleType[j].listColor[k].parent.SuffixCode+' '+
                        //                 $scope.productModelData[i].listVehicleType[j].listColor[k].ColorName;
                        res.ColorId = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorId;
                        res.ColorCode = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorCode;
                        res.ColorName = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorName;
                        // res.katashiki = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.KatashikiCode;
                        // res.suffix = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.SuffixCode;
                        break;
                    }
                }
                if(res.model!=null) break;
            }
            if(res.model!=null) break;
        }
        return res;
    };
    function getFlexMonth(month){
        if(month<-12) return months[(24+month)]
        else if(month<0) return months[(12+month)]
        else if(month>11) return months[Math.abs(12-month)]
        else return months[month];
    }
    $scope.filsudoPeriodId = function(selected){
       // {
       //    "VehicleModelId": 30,
       //    "NId": 6,
       //    "N1Id": 7,
       //    "N2Id": 8,
       //    "N3Id": 9,
       //    "N4Id": 10,
       //    "N5Id": 11,
       //    "Period": "2016-04-01T00:00:00",
       //    "PeriodDetail": "042016",
       //    "OutletTAMId": null,
       //    "OutletCode": null,
       //    "OutletName": null,
       //    "GroupDealerId": null,
       //    "GroupDealerName": null,
       //    "VehicleModelName": "FORTUNER2",
       //    "VehicleTypeId": 1045,
       //    "KatashikiCode": "F123",
       //    "SuffixCode": "456",
       //    "ColorId": 2015,
       //    "ColorCode": "RED",
       //    "ColorName": "RED",
       //    "N": 1,
       //    "N1": 2,
       //    "N2": 3,
       //    "N3": 4,
       //    "N4": 5,
       //    "N5": 6
       //  }
        console.log("selected Prd=>",selected);
        if(selected){
            //$scope.filter.getsudoPeriodId = selected.GetsudoPeriodId;
            var dt = new Date(selected.StartPeriod);
            var chMonth = dt.getMonth() + 1;
            console.log("chMonth filsudoPeriodId : "+chMonth);
            for (var i=6; i<12; i++) {
                $scope.grid.columnDefs[i].displayName = getFlexMonth(chMonth-1).substring(0,3);
                chMonth++;
            }
            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            console.log($scope.grid.columnDefs);
            $scope.periodActive = selected.ActiveFlag;
            $scope.grid.data = [];
            changeMode(viewMode);
        }else{
            $scope.periodActive = false;
            $scope.grid.data = [];
            changeMode(viewMode);
        }
    };

    $scope.filOutlet = function(selected){
        console.log('tree selected',selected);
        if(selected){
            $scope.filter.org = selected.OutletTAMId;
            // $scope.filter.org = selected.OutletId;
        }else{
            $scope.filter.org = null;
        }
    };
    
    $scope.filproductType = function(selected){
        console.log(selected);
        if(selected){
            $scope.filter.productType = selected;
        }else{
            $scope.filter.productType = null;
        }
    };

    $scope.selPer = {selPer:null};
    $scope.beforeNew = function() {
        $timeout(function(){
            $scope.selectedModel = {};
            $scope.selectedType={};
            $scope.gridDetail.data = [];
        });
        var sel = _.find($scope.periodData, { 'ActiveFlag': true });
        // console.log(sel);
        $scope.selPer.selPer = sel.period;
        $scope.selectsudoPeriodId(sel);
    }

    // var mthArr = [];

    var selPeriod, selCusId, selCusNm, selCusCd, selGrpDealId, selOutId, selOutCd, selOutNm, selModelId, selModel, selKat, selSuf, selType, selColor;
    $scope.selectsudoPeriodId = function(selected){
        // console.log(selected);
        var dt = new Date(selected.StartPeriod);
        var chMonth = dt.getMonth() + 1;
        console.log("chMonth selectsudoPeriodId : "+chMonth);
        // mthArr = [];
        for (var i=7; i<13; i++) {
            // mthArr.push($scope.periodData[chMonth-1].period);
            $scope.gridDetail.columnDefs[i].displayName = getFlexMonth(chMonth-1).substring(0,3);
            chMonth++;
        }
        $scope.gridDetailApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        console.log($scope.gridDetail.columnDefs);
        // selPeriod = $scope.mRSSPInternal.Period;
        selPeriod = $scope.selPer.selPer;
    };
    // $scope.selectCustomer = function(selected){
    //     // console.log(selected);
    //     selCusId = selected.Id;
    //     $scope.mRSSPInternal.CustomerName = selected.CustomerName;
    //     selCusNm = selected.CustomerName;
    //     $scope.mRSSPInternal.CustomerCode = selected.CustomerCode;
    //     selCusCd = selected.CustomerCode;
    // };

    $scope.selectOutlet = function(selected){
        // console.log(selected);
        // $scope.mRSSPInternal.orgId = selected.id;
                
        // $scope.mRSSPInternal.GroupDealerId = selected.pid;
        // $scope.mRSSPInternal.OutletCode = selected.OutletCode;
        // $scope.mRSSPInternal.OutletName = selected.OutletName;

        if (selected) {
            selGrpDealId = selected.pid;
            selOutId = selected.OutletTAMId;
            selOutCd = selected.OutletCode;
            selOutNm = selected.Name;
        } else {
            selGrpDealId = null;
            selOutId = null;
            selOutCd = null;
            selOutNm = null;
        }
        
    };

    $scope.selectModel = function(selected) {
        // console.log("selectModel selected : "+JSON.stringify(selected));
        if (selected) {
            $scope.selectedModel=selected;
            selModelId = selected.VehicleModelId;
            selModel = selected.VehicleModelName;
        } else {
            $scope.selectedModel = {};
            selModelId = null;
            selModel = null;
        }
        
        $scope.tempRSSP.selVehicleTypeId = null;
        $scope.selectedType={};
        // $scope.mRSSPInternal.VehicleTypeId = null;
    };
    $scope.selectType = function(selected) {
        // console.log("selectType selected : "+JSON.stringify(selected));
        if (selected) {
            selType = selected.Description;
            selKat = selected.KatashikiCode;
            selSuf = selected.SuffixCode;
            $scope.selectedType=selected;

            for(var i in $scope.selectedType.listColor){
                $scope.selectedType.listColor[i].TheId =    {
                                                                OutletTAMId : selOutId,
                                                                OutletCode : selOutCd,
                                                                OutletName : selOutNm,
                                                                VehicleModelId : selModelId,
                                                                VehicleModelName : selModel,
                                                                VehicleTypeId : selected.VehicleTypeId,
                                                                TypeColor : selKat+selSuf+$scope.selectedType.listColor[i].ColorName,
                                                                TypeName : selType,
                                                                ColorId : $scope.selectedType.listColor[i].ColorId,
                                                                ColorCode : $scope.selectedType.listColor[i].ColorCode,
                                                                ColorName : $scope.selectedType.listColor[i].ColorName,
                                                                // CustomerFleetId : selCusId,
                                                                // CustomerName : selCusNm,
                                                                // CustomerCode : selCusCd,
                                                                KatashikiCode : selKat,
                                                                SuffixCode : selSuf,
                                                                Period : selPeriod
                                                            };
            }
        } else {
            selType = null;
            selKat = null;
            selSuf = null;
            $scope.selectedType={};
        }        

        // $scope.mRSSPInternal.ColorId = null;      
    };
    $scope.selectMultiColor = function(selected){
        var newData = [];
        console.log('selected',selected);     

        if (selected!==undefined) {
            for(var i=0; i<selected.length; i++){
                // var obj = findParentOfTypeColor(selected[i]);
                // console.log(JSON.stringify(obj));
                newData.push({
                    OutletTAMId : selected[i].OutletTAMId,
                    OutletCode : selected[i].OutletCode,
                    OutletName : selected[i].OutletName,
                    VehicleModelId : selected[i].VehicleModelId,
                    VehicleModelName: selected[i].VehicleModelName,
                    VehicleTypeId: selected[i].VehicleTypeId,
                    TypeColor: selected[i].TypeColor,                    
                    TypeName: selected[i].TypeName,
                    ColorId: selected[i].ColorId,
                    ColorCode: selected[i].ColorCode,
                    ColorName: selected[i].ColorName,
                    // CustomerFleetId : selected[i].CustomerFleetId,
                    // CustomerName : selected[i].CustomerName,
                    // CustomerCode : selected[i].CustomerCode,
                    KatashikiCode: selected[i].KatashikiCode,
                    SuffixCode: selected[i].SuffixCode,
                    Period: selected[i].Period,
                    N:0,
                    N1:0,
                    N2:0,
                    N3:0,
                    N4:0,
                    N5:0,
                });
            }
            $scope.gridDetail.data = newData;  
        }      
        
        // $scope.mRSSPInternal.productColorId = selected;
    }
    function validateXLS(data){
        var result = [];
        var error = [];
        for(var i=0; i<data.length; i++){
            var dOutlet = _.find(outletData, { 'title': data[i].Outlet });
            var dModel = _.find($scope.productModelData, { 'name': data[i].Model });
            if(dOutlet == undefined){
                error.push('Outlet '+data[i].Outlet+' tidak ditemukan.');
                continue;
            }
            if(dModel == undefined){
                error.push('Model '+data[i].Model+' tidak ditemukan.');
                continue;
            }
            var newObj = {
                m1: data[i].Jan,
                m2: data[i].Feb,
                m3: data[i].Mar,
                m4: data[i].Apr,
                m5: data[i].May,
                m6: data[i].Jun,
                m7: data[i].Jul,
                m8: data[i].Aug,
                m9: data[i].Sep,
                m10: data[i].Oct,
                m11: data[i].Nov,
                m12: data[i].Dec,
                orgId: dOutlet.id,
                dealerName: dOutlet.parent.title,
                outletName: dOutlet.title,
                modelName: dModel.name,
                productModelId: dModel.id
            };
            result.push(newObj);
        }
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadRSSPInternal' ) );
        XLSXInterface.loadToJson(myEl[0].files[0], function(json){
            $timeout(function(){
                myEl.value = '';
                $scope.grid.data = validateXLS(json);

                mDataCopy =  angular.copy($scope.grid.data);
                changeMode(editMode);
            });
        });
    };
    $scope.getData = function() {
        console.log("mau get data");
        var error=[];
        // $scope.filsudoPeriodId();
        if ($scope.filter.GetsudoPeriodId==null) {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
                content: error.join('<br>'),
                // number: error.length
            });   
        } else {
            RSSPInternal.getData($scope.filter).then(
                function(res){
                    // console.log(JSON.stringify(res.data));      
                    var temp = [];
                    for (var i in res.data.Result) {    
                        console.log(res.data.Result[i]);
                        if (res.data.Result[i]!==null) {
                            // res.data.Result[i]["OutletName"] = outName;
                            res.data.Result[i]["TypeName"] = res.data.Result[i]["VehicleTypeName"];
                            
                            temp.push(res.data.Result[i]);
                        }
                    }

                    // $scope.grid.data = res.data.Result;   
                    // $scope.mRSSPFleet = res.data.Result;
                    $scope.grid.data = temp;                
                    $scope.mRSSPInternal = temp;

                    mDataCopy =  angular.copy($scope.grid.data);

                    // $scope.mRSSPInternal = res.data.Result;
                    $scope.loading=false;
                    changeMode(viewMode);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    $scope.onBeforeCancel = function(){
        console.log('before cancel..');
        $timeout(function(){
            $scope.gridDetail.data = [];    
        });
    };

    $scope.doCustomSave = function(mdl,mode){
        console.log("inside before save");
        console.log("mdl : "+JSON.stringify(mdl));
        console.log("mode : "+mode);
        var grdData = $scope.gridDetail.data;
        var lastArr = [];
        
        for (var i in grdData) {                
            var temp = {};
            for (var j=0; j<6; j++) {
                // {
                //     "NId": 2,
                //     "N1Id": 3,
                //     "N2Id": 4,
                //     "N3Id": 5,
                //     "N4Id": 6,
                //     "N5Id": 7,
                //     "GroupDealerId": 999,
                //     "OutletId": 999,
                //     "Period": 20160401,
                //     "VehicleModelId": 49,
                //     "VehicleTypeColorId": 14,
                //     "KatashikiCode": "F456",
                //     "SuffixCode": "789",
                //     "ColorId": 2252,
                //     "N": 100,
                //     "N1": 200,
                //     "N2": 300,
                //     "N3": 400,
                //     "N4": 500,
                //     "N5": 600,
                //     "CustomerFleetId": null,
                //     "HOReviewState": true
                // }
                             
                if (j==0) {
                    temp["N"] = grdData[i]["N"];
                    temp["NId"] = null;
                } else {
                    temp["N"+j] = grdData[i]["N"+j];
                    temp["N"+j+"Id"] = null;
                }
            }
            temp.OutletTAMId = grdData[i].OutletTAMId;
            temp.OutletCode = grdData[i].OutletCode;
            temp.GroupDealerId = selGrpDealId;
            temp.VehicleModelId = grdData[i].VehicleModelId;
            temp.VehicleModelName = grdData[i].VehicleModelName;
            temp.KatashikiCode = grdData[i].KatashikiCode;
            temp.SuffixCode = grdData[i].SuffixCode;
            temp.Period = grdData[i].Period;
            temp.VehicleTypeColorId = grdData[i].TypeId;
            temp.TypeColor = grdData[i].TypeColor;            
            temp.ColorId = grdData[i].ColorId[i];
            temp.ColorCode = grdData[i].ColorCode;
            temp.ColorName = grdData[i].ColorName;
            // temp.CustomerFleetId = grdData[i].CustomerFleetId;
            // temp.CustomerName = grdData[i].CustomerName;
            // temp.CustomerCode = grdData[i].CustomerCode;
            temp.HOReviewState = false;
            lastArr.push(temp);
        }
        console.log("lastArr : "+JSON.stringify(lastArr));

        if (mode=='create') {
            RSSPInternal.create(lastArr).then(function(res){
                console.log(res);
            });
        } else {

        }
    }

    // {
    //   "VehicleModelId": 30,
    //   "NId": 6,
    //   "N1Id": 7,
    //   "N2Id": 8,
    //   "N3Id": 9,
    //   "N4Id": 10,
    //   "N5Id": 11,
    //   "Period": "2016-04-01T00:00:00",
    //   "PeriodDetail": "042016",
    //   "OutletTAMId": null,
    //   "OutletCode": null,
    //   "OutletName": null,
    //   "GroupDealerId": null,
    //   "GroupDealerName": null,
    //   "VehicleModelName": "FORTUNER2",
    //   "VehicleTypeId": 1045,
    //   "KatashikiCode": "F123",
    //   "SuffixCode": "456",
    //   "ColorId": 2015,
    //   "ColorCode": "RED",
    //   "ColorName": "RED",
    //   "N": 1,
    //   "N1": 2,
    //   "N2": 3,
    //   "N3": 4,
    //   "N4": 5,
    //   "N5": 6
    // }

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableCellEdit: false,
        enableSelectAll: true,
        enableCellEditOnFocus: false,
        cellEditableCondition: function($scope) {
            return editableMode;
        },
        columnDefs: [
            // {
            //   "VehicleModelId": 30,
            //   "NId": 6,
            //   "N1Id": 7,
            //   "N2Id": 8,
            //   "N3Id": 9,
            //   "N4Id": 10,
            //   "N5Id": 11,
            //   "Period": "2016-04-01T00:00:00",
            //   "PeriodDetail": "042016",
            //   "OutletTAMId": null,
            //   "OutletCode": null,
            //   "OutletName": null,
            //   "GroupDealerId": null,
            //   "GroupDealerName": null,
            //   "VehicleModelName": "FORTUNER2",
            //   "VehicleTypeId": 1045,
            //   "KatashikiCode": "F123",
            //   "SuffixCode": "456",
            //   "ColorId": 2015,
            //   "ColorCode": "RED",
            //   "ColorName": "RED",
            //   "N": 1,
            //   "N1": 2,
            //   "N2": 3,
            //   "N3": 4,
            //   "N4": 5,
            //   "N5": 6
            // }
            { name:'id',    field:'id', visible:false },
            // { name:'customerFleetId', field:'CustomerId', visible:false},
            // { name:'Customer', field:'CustomerName', enableCellEdit: false},
            { name:'OutletName', field:'OutletName'},
            { name:'Model', field: 'VehicleModelName', enableCellEdit: false},
            // { name:'Type Color', field: 'TypeColor', enableCellEdit: false},
            { name:'Type', field: 'TypeName', enableCellEdit: false},
            { name:'Name Color', field: 'ColorName', enableCellEdit: false},
            // { name:'Katashiki', field: 'KatashikiCode', enableCellEdit: false},
            { name:'Suffix', field: 'SuffixCode', enableCellEdit: false},
            { name:'Jan', field: 'N', type: 'number' , enableCellEdit: false},
            { name:'Feb', field: 'N1', type: 'number' , enableCellEdit: true},
            { name:'Mar', field: 'N2', type: 'number' , enableCellEdit: true},
            { name:'Apr', field: 'N3', type: 'number' , enableCellEdit: true},
            { name:'May', field: 'N4', type: 'number' , enableCellEdit: true},
            { name:'Jun', field: 'N5', type: 'number' , enableCellEdit: true},
        ]
    };

    var editCellTemplateN = '<div class="form-group" style="margin-top:4px;"><input type="number" min="0" name="total" placeholder="N1"'+
                                          ' ng-model="row.entity.N"'+
                                          ' class="form-control" style="height:25px;" /> </div>';
    var editCellTemplateN1 = '<div class="form-group" style="margin-top:4px;"><input type="number" min="0" name="total" placeholder="N1"'+
                                          ' ng-model="row.entity.N1"'+
                                          ' class="form-control" style="height:25px;" /> </div>';

    $scope.afterRegisterGridApi = function(gridApi) {
        $scope.gridApi = gridApi;
        // console.log($scope.gridApi);

        $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            // console.log('Inside afterCellEdit..');
            if (newValue<0) {
                newValue = 0;
                rowEntity[colDef.field] = newValue;
            }
        });
    };
                                         
    $scope.gridDetailApi = {};
    $scope.gridDetail = {
        enableSorting: true,
        enableSelectAll: true,
        enableCellEdit: false,
        enableCellEditOnFocus: true, // set any editable column to allow edit on focus
        enableCellEditOnFocus: true,
        columnDefs: [
            { name:'id',    field:'id', visible:false },
            { name:'OutletCode', field:'OutletCode', visible:false},
            { name:'OutletName', field:'OutletName'},
            { name:'Model', field: 'VehicleModelName'},
            { name:'Type', field: 'TypeName'},
            { name:'Name Color', field: 'ColorName'},
            { name:'Suffix', field: 'SuffixCode'},  
            // { name:'Jan', field: 'N', type: 'number', editableCellTemplate : editCellTemplateN, enableCellEdit: true},
            { name:'Jan', field: 'N', type: 'number', enableCellEdit: false},
            { name:'Feb', field: 'N1', type: 'number', enableCellEdit: true},
            { name:'Mar', field: 'N2', type: 'number', enableCellEdit: true },
            { name:'Apr', field: 'N3', type: 'number', enableCellEdit: true },
            { name:'May', field: 'N4', type: 'number', enableCellEdit: true },
            { name:'Jun', field: 'N5', type: 'number', enableCellEdit: true },
            // { name:'Jul', field: 'm7', visible:false},
            // { name:'Aug', field: 'm8', visible:false},
            // { name:'Sep', field: 'm9', visible:false},
            // { name:'Oct', field: 'm10', visible:false},
            // { name:'Nov', field: 'm11', visible:false},
            // { name:'Dec', field: 'm12', visible:false},
        ]
    };

    $scope.gridDetail.onRegisterApi = function(gridApi) {
      // set gridApi on scope
      $scope.gridDetailApi = gridApi;              

      $scope.gridDetailApi.selection.on.rowSelectionChanged($scope,function(row) {
          //scope.selectedRows=null;
          $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
          // console.log("bsform selected=>",scope.selectedRows);                  
      });
      $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
          //scope.selectedRows=null;
          $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();                  
      });
      $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
          // console.log('Inside afterCellEdit grid detail..');
          if (newValue<0 || newValue==null) {
              newValue = 0;
              rowEntity[colDef.field] = newValue;
          }
      });
    };

});
