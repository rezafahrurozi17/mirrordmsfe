angular.module('app')
  .factory('RSSPInternal', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {        
        // var res=$http.get('/api/ds/RSSPDetailOrders/?start=1&limit=10');
        var filter = angular.copy(param);
        console.log(filter);
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
          if(key == 'productModel' || key == 'productType')
            delete filter[key];
        }
        var res=$http.get('/api/ds/RSSPDetailOrders/?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;

        // var defer = $q.defer();
        //     defer.resolve(
        //       {
        //           "Result":[{
        //               "VehicleModelId": 30,
        //               "NId": 6,
        //               "N1Id": 7,
        //               "N2Id": 8,
        //               "N3Id": 9,
        //               "N4Id": 10,
        //               "N5Id": 11,
        //               "Period": "2016-04-01T00:00:00",
        //               "PeriodDetail": "042016",
        //               "OutletTAMId": null,
        //               "OutletCode": null,
        //               "OutletName": null,
        //               "GroupDealerId": null,
        //               "GroupDealerName": null,
        //               "VehicleModelName": "FORTUNER2",
        //               "VehicleTypeId": 1045,
        //               "KatashikiCode": "F123",
        //               "SuffixCode": "456",
        //               "ColorId": 2015,
        //               "ColorCode": "RED",
        //               "ColorName": "RED",
        //               "N": 1,
        //               "N1": 2,
        //               "N2": 3,
        //               "N3": 4,
        //               "N4": 5,
        //               "N5": 6
        //             }],
        //           "Start":1,
        //           "Limit":10,
        //           "Total":1
                
        //       }
        //     );
        // return defer.promise;
      },
      create: function(rsspinternal) {
        console.log("rsspinternal : "+JSON.stringify(rsspinternal));
        return $http.post('/api/ds/RSSPDetailOrders/Multiple', rsspinternal);
      },
      update: function(rsspinternal) {
        console.log(rsspinternal);
        // return $http.put('/api/ds/RSSPDetailOrders/', {
        //   "VehicleModelId": rsspinternal.VehicleModelId,
        //   "NId": rsspinternal.NId,
        //   "N1Id": rsspinternal.N1Id,
        //   "N2Id": rsspinternal.N2Id,
        //   "N3Id": rsspinternal.N3Id,
        //   "N4Id": rsspinternal.N4Id,
        //   "N5Id": rsspinternal.N5Id,
        //   "Period": rsspinternal.Period,
        //   "PeriodDetail": rsspinternal.PeriodDetail,                  
        //   "OutletTAMId": rsspinternal.OutletTAMId,
        //   "OutletCode": rsspinternal.OutletCode,
        //   "OutletName": rsspinternal.OutletName,
        //   "GroupDealerId": rsspinternal.GroupDealerId,
        //   "GroupDealerName": rsspinternal.GroupDealerName,
        //   "VehicleModelName": rsspinternal.VehicleModelName,
        //   "VehicleTypeId": rsspinternal.VehicleTypeId,
        //   "KatashikiCode": rsspinternal.KatashikiCode,
        //   "SuffixCode": rsspinternal.SuffixCode,
        //   "ColorId": rsspinternal.ColorId,
        //   "ColorCode": rsspinternal.ColorCode,
        //   "ColorName": rsspinternal.ColorName,
        //   "N": rsspinternal.N,
        //   "N1": rsspinternal.N1,
        //   "N2": rsspinternal.N2,
        //   "N3": rsspinternal.N3,
        //   "N4": rsspinternal.N4,
        //   "N5": rsspinternal.N5
        // })
        return $http.put('/api/ds/RSSPDetailOrders/Multiple', rsspinternal);
      },
      delete: function(id) {
        console.log("delete "+JSON.stringify(id));
        var aa = [];
        var bb = [];
        
        for (var i = 0; i < id.length ; i++) {
          var cc={};
          cc.idx = id[i].idx;
          cc.VehicleModelId = id[i].VehicleModelId;
          cc.NId = id[i].NId;
          cc.N1Id = id[i].N1Id;
          cc.N2Id = id[i].N2Id;
          cc.N3Id = id[i].N3Id;
          cc.N4Id = id[i].N4Id;
          cc.N5Id = id[i].N5Id;
          cc.Period = id[i].Period;
          cc.PeriodDetail = id[i].PeriodDetail;
          cc.OutletTAMId = id[i].OutletTAMId;
          cc.OutletCode = id[i].OutletCode;
          cc.OutletName = id[i].OutletName;
          cc.GroupDealerId = id[i].GroupDealerId;
          cc.GroupDealerName = id[i].GroupDealerName;
          cc.VehicleModelName = id[i].VehicleModelName;
          cc.VehicleTypeId = id[i].VehicleTypeId;
          cc.KatashikiCode = id[i].KatashikiCode;
          cc.SuffixCode = id[i].SuffixCode;
          cc.ColorId = id[i].ColorId;
          cc.ColorCode = id[i].ColorCode;
          cc.ColorName = id[i].ColorName;
          cc.N = id[i].N;
          cc.N1 = id[i].N1;
          cc.N2 = id[i].N2;
          cc.N3 = id[i].N3;
          cc.N4 = id[i].N4;
          cc.N5 = id[i].N5;
          aa.push(cc);
        };

        // if (aa.length>0) {
        //   bb=aa;
        // } else {
        //   var cc={};
        //    cc.Id = id;
        //    bb.push(cc);
        // } 

        return $http.delete('/api/ds/RSSPDetailOrders/Multiple', {data:aa,headers: {'Content-Type': 'application/json'}});
        // return res;
      }
    }
  });