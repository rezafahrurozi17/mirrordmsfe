angular.module('app')
  .factory('RSSPTemplate', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/RSSPHeaderFormulaTemps/?start=1&limit=10');
        return res;
      },
      getDetail: function(Id) {
        var res=$http.get('/api/ds/DetailFormulaTypeColors/?Id='+Id+'&typeId=2');
        return res;
      },
      findByModel: function(id) {
        var res=$http.get('/api/ds/DetailFormulaTypeColors/?Id='+id+'&typeId=1');
        return res;
      },
      create: function(rssptemp) {
        return $http.post('/api/ds/RSSPHeaderFormulaTemps/PostData/', [rssptemp]);
      },
      update: function(rssptemp){

        return $http.put('/api/ds/DetailFormulaTypeColors/UpdateData/', [rssptemp]);
      },
      delete: function(id) {

        var aa = [];
        var bb = [];
      
         for (var i = 0; i < id.length ; i++) {
           var cc={};
           cc.Id=id[i];
           aa.push(cc);
         };

         if (aa.length>0) {
              bb=aa;
            } else {
              var cc={};
               cc.Id = id;
               bb.push(cc);
            } 
          console.log("delete==>",JSON.stringify(bb));
          console.log("delete2==>",bb); 
          return $http.delete('/api/ds/RSSPHeaderFormulaTemps/Delete/', {data:bb,headers: {'Content-Type': 'application/json'}});
          // console.log("bb",bb);

        // return $http.post('/api/ds/RSSPHeaderFormulaTemps/Multiple/',{id:id});
      },
    }
  });