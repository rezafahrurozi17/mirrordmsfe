angular.module('app')
    .controller('RSSPTemplateController', function($scope, $http, CurrentUser, RSSPTemplate, RSSPVar, hotRegisterer, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRSSPTemp = null; //Model
    $scope.xRSSPTemp = {};
    $scope.xRSSPTemp.selected=[];
    var visibilityArr = ['Always', 'Market', 'SPK', 'Reference', 'Hide'];
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        RSSPTemplate.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onShowDetail = function(row){
        RSSPTemplate.getDetail(row.Id).then(
            function(res){
                if(res.data.Result.length>0){
                    for(var i in res.data.Result){
                        res.data.Result[i].Visibility = visibilityArr[res.data.Result[i].Visibility];
                    }
                    $scope.renderHOTVarStructure(res.data.Result,false);
                }
                else
                    $scope.detailData = [{}];
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onValidateSave = function(obj){
        obj.FormulaDetail = $scope.detailData;
        obj.FormulaDetail.splice(0,3);
        for(var i in obj.FormulaDetail){
            obj.FormulaDetail[i].Visibility = visibilityArr.indexOf(obj.FormulaDetail[i].Visibility);
            if(obj.FormulaDetail[i].AdjustmentRatioTypeId=='' || obj.FormulaDetail[i].AdjustmentRatioTypeId==null || obj.FormulaDetail[i].AdjustmentRatioTypeId==undefined){
                obj.FormulaDetail[i].AdjustmentRatioTypeId=0;
            }
        }
        return true;
    }
    $scope.rsspVarData = [];
    RSSPVar.getDataTree().then(
        function(res){
            for(i in res.data.Result){
                if(res.data.Result[i].ParentId==0){
                    var newObj = res.data.Result[i];
                    newObj.name = newObj.AdjustmentName;
                    newObj.id = newObj.AdjustmentRatioTypeId;
                    $scope.rsspVarData.push(newObj); 
                }
            }
            $scope.loading=false;
            return res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'id',    field:'Id', visible:false },
            { name:'Template Name', width:'30%', field: 'Name' },
            { name:'Description', field: 'Description' },
        ]
    };
    $scope.detailData = [{}];
    $scope.renderHOTVarStructure = function(data, addSpace){
        var lastPrnId = 0;
        var mergeCells = [];
        var cell = [];
        var addedRow = 0;
        $scope.detailData = [];
        data.splice(0,0,{},{},{});
        for(var i=0; i<data.length; i++){
            if(i>2){
                if(data[i].ParentId==0 || data[i].ParentId==null || data[i].ParentId==undefined){
                    data[i].ParentName = data[i].AdjustmentName;
                    mergeCells.push({row: i+addedRow, col: 0, rowspan: 1, colspan: 2});
                }else{
                    if(data[i].ParentName.length>5 && lastPrnId!=data[i].ParentId){
                        if(addSpace){
                            $scope.detailData.push({});
                            $scope.detailData.push({
                                AdjustmentRatioTypeId : data[i].ParentId,
                                ParentName: data[i].ParentName,
                                Visibility: 'Tampilkan',
                            });                        
                            mergeCells.push({row: i+addedRow+1, col: 0, rowspan: 1, colspan: 2});
                            addedRow+=2;
                        }
                        for(ctr=i; ctr<data.length; ctr++){
                            if(data[ctr].ParentId==data[i].ParentId){
                                data[ctr].ParentName = data[ctr].AdjustmentName;
                                mergeCells.push({row: ctr+addedRow, col: 0, rowspan: 1, colspan: 2});                            
                            }
                        }
                        lastPrnId = data[i].ParentId;
                    }else if(data[i].ParentName.length<6 && lastPrnId!=data[i].ParentId){
                        var ctr=i;
                        for(; ctr<data.length; ctr++){
                            if(data[ctr].ParentId!=data[i].ParentId)
                                break;
                        }
                        mergeCells.push({row: i+addedRow, col: 0, rowspan: ctr-i, colspan: 1});
                        cell.push({row: i+addedRow, col: 0, className: "htMiddle"});
                    }
                }
            }
            $scope.detailData.push(data[i]);
        }
        $scope.mRSSPTemp.FormulaDetail = $scope.detailData;
        $scope.hotRSSPTemp = hotRegisterer.getInstance('hotRSSPTemp');
        $scope.hotRSSPTemp.updateSettings({
            mergeCells: mergeCells,
            cell: cell,
            height: 800,
            hiddenRows: {
                copyPasteEnabled: true,
                indicators: false,
                rows: [0,1,2]
            }
        });
    }
    $scope.confirmVarSelect = function(){
        var newData = [];
        for(var i in $scope.selectionOpt.selectedItems){
            if($scope.selectionOpt.selectedItems[i].Child.length>0){
                for(var c in $scope.selectionOpt.selectedItems[i].Child)
                    newData.push({
                            ParentId: $scope.selectionOpt.selectedItems[i].id,
                            ParentName: $scope.selectionOpt.selectedItems[i].name,
                            AdjustmentRatioTypeId: $scope.selectionOpt.selectedItems[i].Child[c].AdjustmentRatioTypeId,
                            AdjustmentName: $scope.selectionOpt.selectedItems[i].Child[c].AdjustmentName,
                            VariableType: $scope.selectionOpt.selectedItems[i].Child[c].VariableType,
                            Visibility: 'Always',
                    });
            }else{
                newData.push({
                        AdjustmentRatioTypeId: $scope.selectionOpt.selectedItems[i].id,
                        AdjustmentName: $scope.selectionOpt.selectedItems[i].name,
                        VariableType: $scope.selectionOpt.selectedItems[i].VariableType,
                        Visibility: 'Always',
                });
            }
        }
        $scope.renderHOTVarStructure(newData,true);
    }
    $scope.selectionOpt = {
        helpMessage: 'Pilih nama variabel yang akan digunakan dalam template',
        filterPlaceHolder: 'Cari nama variabel...',
        labelAll: 'Semua variabel',
        labelSelected: 'Variabel terpilih',
        height: 700,
        items: $scope.rsspVarData,
        selectedItems: []
    };
    $scope.hotSettings = {
        colWidths: [50,180,100,110,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40],
        nestedHeaders: [
            ['','','','','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE'],
            ['','','','','','','','','','','','','','',{label:'Data Histori', colspan:2},'Est',{label:'Periode Pemesanan',colspan:5},{label:'Rencana OAP/RAP',colspan:7},'',''],
            [{label:'Nama Variabel', colspan:2}, 'Tipe', 'Tampilan', 'N-12', 'N-11', 'N-10', 'N-9', 'N-8', 'N-7', 'N-6', 'N-5', 'N-4', 'N-3', 'N-2', 'N-1', 'N', 'N+1', 'N+2', 'N+3', 'N+4', 'N+5', 'N+6', 'N+7', 'N+8', 'N+9', 'N+10', 'N+11', 'N+12', 'ID', 'PRNID']
        ],
        fixedColumnsLeft:2,
        height: 500,
        width: angular.element(document.getElementById('rsspDetailContainer')).clientWidth,
        hiddenColumns: {
          columns: [4,5,6,29,30],
          indicators: false
        },
        columns: [
                  { data: 'ParentName', type: 'text', editor: false},
                  { data: 'AdjustmentName', type: 'text', editor: false},
                  { data: 'VariableType', type: 'text', editor: false},
                  { data: 'Visibility', type: 'dropdown', source: visibilityArr},
                  { data: 'M_12' ,type: 'numeric'},
                  { data: 'M_11' ,type: 'numeric'},
                  { data: 'M_10' ,type: 'numeric'},
                  { data: 'M_9' ,type: 'numeric'},
                  { data: 'M_8' ,type: 'numeric'},
                  { data: 'M_7' ,type: 'numeric'},
                  { data: 'M_6' ,type: 'numeric'},
                  { data: 'M_5' ,type: 'numeric'},
                  { data: 'M_4' ,type: 'numeric'},
                  { data: 'M_3' ,type: 'numeric'},
                  { data: 'M_2' ,type: 'numeric'},
                  { data: 'M_1' ,type: 'numeric'},
                  { data: 'M' ,type: 'numeric'},
                  { data: 'M1' ,type: 'numeric'},
                  { data: 'M2' ,type: 'numeric'},
                  { data: 'M3' ,type: 'numeric'},
                  { data: 'M4' ,type: 'numeric'},
                  { data: 'M5' ,type: 'numeric'},
                  { data: 'M6' ,type: 'numeric'},
                  { data: 'M7' ,type: 'numeric'},
                  { data: 'M8' ,type: 'numeric'},
                  { data: 'M9' ,type: 'numeric'},
                  { data: 'M10' ,type: 'numeric'},
                  { data: 'M11' ,type: 'numeric'},
                  { data: 'M12' ,type: 'numeric'},
                  { data: 'AdjustmentRatioTypeId', type: 'numeric', editor: false},
                  { data: 'ParentId', type: 'numeric', editor: false},
        ],
    };
});
