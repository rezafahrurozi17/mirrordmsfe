angular.module('app')
    .controller('RSSPController', function($scope, $http, $window, CurrentUser, uiGridConstants, GetsudoPeriod, OrgChart, RSSP, RSSPTemplate, ProductModel, hotRegisterer, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log('user=',$scope.user);

    $scope.mRSSP = {};
    $scope.xRSSP = {};
    $scope.xRSSP.selected=[];

    $scope.filter = {GetsudoPeriodId : null, orgId : null};
    $scope.summary = {type:1, data:[{text:"RS",value:1},{text:"WS",value:2}]};
    $scope.selectedProductModel = {};
    $scope.comments = [];
    $scope.productModelData = [];
    $scope.showComposition = false;
    $scope.totalSPK = 16;
    $scope.avgSPK = 5.33;
    $scope.totalOS = 16;
    $scope.avgOS = 5.33;
    $scope.totalRS = 16;
    $scope.avgRS = 5.33;
    $scope.totalCL = 16;
    $scope.avgCL = 5.33;
    $scope.Visibility = {market:1, spk:1, reference:1};
    $scope.comment = {message:''};
    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        return res;
    }
    $scope.hideEditButton = false;
        
    var productTree = [];
    var selectedGetsudo = null;
    var selectedMonth = null;
    var hotData = [{}];
    var fixCtr = 2;
    var headerObject = {};
    var alerts = [];
    var dataBEModel = [];
    var dataBEModelIdx = {};
    var dataBEType = [{GetsudoPeriodId:null, OutletId:null, VehicleModelId:null, data:[]}];
    var dataBETypeArr = [];
    var dataBETypeArrIdx = {};
    var lastSelectedTree = {};
    var modelTemplate = {};
    var typeColorTemplate = {};
    var typeColorAdjusted = {};
    var typeColorCompare = [];
    var replaceOWith = '';
    var sumRS = [];
    var sumWS = [];
    var sumModelRS = [];
    var sumModelWS = [];
    var rowIndexes = {};
    var getsudoName = '';
    var getsudoMonth = 0;
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    //----------------------------------
    // Rendering
    //----------------------------------
    function getFlexMonth(month){
        if(month<-12) return months[(24+month)]
        else if(month<0) return months[(12+month)]
        else if(month>11) return months[Math.abs(12-month)]
        else return months[month];
    }
    var createDetailHeader = function(modelName, typeName){
        var rsspHeader = [];
        var nMonth = getsudoMonth-1;
        var row1 = {ParentName: modelName, AdjustmentName: null, M:'N'};
        var row2 = {ParentName: typeName, AdjustmentName: null, M:getFlexMonth(nMonth).substring(0,3)};
        var mergeCells = [];
        var cell = [];

        var hiddenColumns = {
            columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,22,23,24,25,26,27,28,29,30,31,32],
            indicators:false
        };
        var nestedHeader1 = [{label: 'Getsudo', colspan:2},'',''];
        var nestedHeader2 = [{label: getsudoName, colspan:2},'',''];
        for(var i=0; i<12; i++){
            nestedHeader1.push('');
            nestedHeader2.push('');
        }
        nestedHeader1.push('',{label:'Periode Pemesanan',colspan:5});
        nestedHeader2.push('Est',{label:'Fix',colspan:fixCtr}, {label:'Flexible',colspan:5-fixCtr});
        for(var i=0; i<11; i++){
            nestedHeader1.push('');
            nestedHeader2.push('');
        }
        var nestedHeaders = [];
        nestedHeaders.push(nestedHeader1);
        nestedHeaders.push(nestedHeader2);
        for(var m=1; m<6; m++){
            row1['M'+m] = 'N+'+m;
            row2['M'+m] = getFlexMonth(nMonth+m).substring(0,3);
        }
        mergeCells.push({row: 0, col: 0, rowspan: 1, colspan: 2});
        mergeCells.push({row: 1, col: 0, rowspan: 1, colspan: 2});
        cell.push({row: 0, col: 1, className: "htCenter"});
        cell.push({row: 1, col: 1, className: "htCenter"});
        rsspHeader.push(row1);
        rsspHeader.push(row2);
        rsspHeader.push({Visibility:0});
        return {data:rsspHeader, mergeCells:mergeCells, cell:cell, nestedHeaders:nestedHeaders, hiddenColumns: hiddenColumns};
    }
    var createHeader = function(modelName, typeName){
        var rsspHeader = [];
        var nMonth = new Date(selectedMonth).getMonth();
        getsudoMonth = nMonth+1;
        getsudoName = getFlexMonth(getsudoMonth);
        var row1 = {ParentName: modelName, AdjustmentName: null, M:'N'};
        var row2 = {ParentName: typeName, AdjustmentName: null, M:getFlexMonth(nMonth).substring(0,3)};
        var mergeCells = [];
        var cell = [];

        var hiddenColumns = {
            columns: [2,3,4],
            indicators:false
        };
        var colspanHistory = 2;
        var colspanFuture = 4;
        switch(nMonth){
        /* Jan */ case  0: hiddenColumns.columns.push(5,6,7,8,9,10,11,12,28,29,30,31,32); colspanHistory = 4; colspanFuture = 6; replaceOWith = 'N'; break;
        /* Feb */ case  1: hiddenColumns.columns.push(5,6,7,8,9,10,11,12,27,28,29,30,31,32); colspanHistory = 4; colspanFuture = 5; replaceOWith = 'N'; break;
        /* Mar */ case  2: hiddenColumns.columns.push(5,6,7,8,9,10,11,12,26,27,28,29,30,31,32); colspanHistory = 4; colspanFuture = 4; replaceOWith = 'N'; break;
        /* Apr */ case  3: hiddenColumns.columns.push(5,6,7,8,9,10,11,12,25,26,27,28,29,30,31,32); colspanHistory = 4; colspanFuture = 3; replaceOWith = 'N'; break;
        /* May */ case  4: hiddenColumns.columns.push(5,6,7,8,9,10,11,24,25,26,27,28,29,30,31,32); colspanHistory = 5; colspanFuture = 2; replaceOWith = 'M'; break;
        /* Jun */ case  5: hiddenColumns.columns.push(5,6,7,8,9,10,23,24,25,26,27,28,29,30,31,32); colspanHistory = 6; colspanFuture = 1; replaceOWith = 'L'; break;
        /* Jul */ case  6: hiddenColumns.columns.push(5,6,7,8,9,22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 7; colspanFuture = 0; replaceOWith = 'K'; break;
        /* Aug */ case  7: hiddenColumns.columns.push(5,6,7,8,22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 8; colspanFuture = 0; replaceOWith = 'J'; break;
        /* Sep */ case  8: hiddenColumns.columns.push(5,6,7,22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 9; colspanFuture = 0; replaceOWith = 'I'; break;
        /* Oct */ case  9: hiddenColumns.columns.push(5,6,22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 10; colspanFuture = 0; replaceOWith = 'H'; break;
        /* Nov */ case 10: hiddenColumns.columns.push(5,22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 11; colspanFuture = 0; replaceOWith = 'G'; break;
        /* Dec */ case 11: hiddenColumns.columns.push(22,23,24,25,26,27,28,29,30,31,32); colspanHistory = 12; colspanFuture = 0; replaceOWith = 'F'; break;
        }
        var nestedHeader1 = [{label: 'Getsudo', colspan:2},'','','M-12'];
        var nestedHeader2 = [{label: getsudoName, colspan:2},'','','M-12'];
        for(var i=0; i<(12-colspanHistory); i++){
            nestedHeader1.push('');
            nestedHeader2.push('');
        }
        nestedHeader1.push({label:'Data', colspan:colspanHistory},{label:'Periode Pemesanan',colspan:5});
        nestedHeader2.push({label:'Histori', colspan:colspanHistory-1},'Est',{label:'Fix',colspan:fixCtr}, {label:'Flexible',colspan:5-fixCtr});
        nestedHeader1.push({label:'Period',colspan:colspanFuture});
        nestedHeader2.push({label:'OAP/RAP',colspan:colspanFuture});
        for(var i=0; i<(7-colspanFuture); i++){
            nestedHeader1.push('');
            nestedHeader2.push('');
        }
        var nestedHeaders = [];
        nestedHeaders.push(nestedHeader1);
        nestedHeaders.push(nestedHeader2);
        for(var m=12; m>0; m--){
            row1['M_'+m] = 'N-'+m;
            row2['M_'+m] = getFlexMonth(nMonth-m).substring(0,3);
        }
        for(var m=1; m<6; m++){
            row1['M'+m] = 'N+'+m;
            row2['M'+m] = getFlexMonth(nMonth+m).substring(0,3);
        }
        for(var m=6; m<13; m++){
            row1['M'+m] = 'N+'+m;
            row2['M'+m] = getFlexMonth(nMonth+m).substring(0,3);
        }
        mergeCells.push({row: 0, col: 0, rowspan: 1, colspan: 2});
        mergeCells.push({row: 1, col: 0, rowspan: 1, colspan: 2});
        cell.push({row: 0, col: 1, className: "htCenter"});
        cell.push({row: 1, col: 1, className: "htCenter"});
        rsspHeader.push(row1);
        rsspHeader.push(row2);
        rsspHeader.push({Visibility:0});
        return {data:rsspHeader, mergeCells:mergeCells, cell:cell, nestedHeaders:nestedHeaders, hiddenColumns: hiddenColumns};
    }
    function _renderHOT(data){
        var lastPrnId = 0;
        var mergeCells = [];
        var cell = [];
        var dataInto = [];
        var hiddenRows = [];
        var isValidationRow = 0;
        for(var i = 0; i<3; i++){
            dataInto.push(data[i]);
        }
        for(var i = 3; i<data.length; i++){
            if(data[i].ParentId==0 || data[i].ParentId==null || data[i].ParentId==undefined){
                data[i].ParentName = data[i].AdjustmentName;
                mergeCells.push({row: i, col: 0, rowspan: 1, colspan: 2});
            }else{
                if(data[i].ParentName.length>5 && lastPrnId!=data[i].ParentId){
                    for(ctr=i; ctr<data.length; ctr++){
                        if(data[ctr].ParentId==data[i].ParentId){
                            data[ctr].ParentName = data[ctr].AdjustmentName;
                            mergeCells.push({row: ctr, col: 0, rowspan: 1, colspan: 2});                            
                        }
                    }
                }else if(data[i].ParentName.length<6 && lastPrnId!=data[i].ParentId){
                    var ctr=i;
                    for(; ctr<data.length; ctr++){
                        if(data[ctr].ParentId!=data[i].ParentId)
                            break;
                    }
                    mergeCells.push({row: i, col: 0, rowspan: ctr-i, colspan: 1});
                    cell.push({row: i, col: 0, className: "htMiddle",});
                }
                lastPrnId = data[i].ParentId;
            }
            if(data[i].Visibility == 4){
                hiddenRows.push(i);
            }
            if(data[i].VariableInputTypeId == 3){
                isValidationRow = i;
            }
            if(data[i].ParentName=='RS' && data[i].AdjustmentName=='Adjustment'){
                rowIndexes.rsAdj = i;
            }
            if(data[i].ParentName=='WS' && data[i].AdjustmentName=='Adjustment'){
                rowIndexes.wsAdj = i;
            }
            dataInto.push(data[i]);
        }
        return {data:dataInto, mergeCells:mergeCells, cell:cell, hiddenRows:hiddenRows, validationRow:isValidationRow};
    }
    function getCellValue(row,col){
        var rawValue = $scope.hotRSSP.getDataAtCell(row, col);
        if(rawValue && rawValue[0]=='='){
            var cellId=$scope.hotRSSP.plugin.utils.translateCellCoords({row: row, col: col});
            var formula=rawValue.substr(1).toUpperCase();
            rawValue = $scope.hotRSSP.plugin.parse(formula, {row: row, col: col, id: cellId}).result;
        }
        return rawValue;
    }
    function _updateSettings(instanceName, settings, dataScope, heightAdj){
        $scope[instanceName] = hotRegisterer.getInstance(instanceName);
        if(dataScope){
            $scope[dataScope] = settings.data;
            $scope[instanceName].updateSettings({data:$scope[dataScope]});
        }
        if(heightAdj!=undefined)
        settings.height = (settings.data.length+heightAdj) * 25;
        settings.cells = function (row, col, prop) {
            var cellProperties = {};
            if ($scope[instanceName].getDataAtRowProp(row, 'VariableInputTypeId') == 1 && col>15) {
                cellProperties.editor = 'numeric';
                cellProperties.className = 'editingCell';
            } else {
                cellProperties.editor = false;
            }
            var ParentName = $scope[instanceName].getDataAtRowProp(row, 'ParentName');
            var AdjustmentName = $scope[instanceName].getDataAtRowProp(row, 'AdjustmentName');
            /*if(ParentName && AdjustmentName && ParentName=='WS' && AdjustmentName=='Adjustment' && col==16){
                cellProperties.editor = 'numeric';
                cellProperties.className = 'editingCell';
            }*/
            if(ParentName == null && AdjustmentName == null){
                cellProperties.className = 'emptyRow';
            }
            if(row in [0,1]){
                cellProperties.className = 'headerCell';
            }
            return cellProperties;
        };
        settings.hiddenRows = {
            copyPasteEnabled: true,
            indicators: false,
            rows: settings.hiddenRows
        };
        $scope[instanceName].updateSettings(settings);
        Handsontable.hooks.add('afterChange', function(arr,src){
            if(src=='loadData') return;
            if(!lastSelectedTree.treeLevel){
                updateBEValue();
            } else if(lastSelectedTree.treeLevel==2){
                updateBEDetailValue();
                typeColorAdjusted[lastSelectedTree.id] = 1;
            }
            recalcSum();
            alerts = [];
            for(var i=16; i<22; i++){
                var validationValue = getCellValue(settings.validationRow, i);
                if((validationValue!=null) && (typeof validationValue == 'string') && (validationValue.length>2)){
                    var checkVal = validationValue.toLowerCase();
                    if(checkVal!='yes' && checkVal!='true' && checkVal!='' && checkVal!=null){
                        if(alerts.indexOf(validationValue)<0)
                            alerts.push(validationValue);
                    }
                }
            }
            if(alerts.length>0)
                $scope.showAlert(alerts.join('\n'), alerts.length);
        }, $scope[instanceName]);
    }
    function mergeHeaderAndData(header, data){
        data.mergeCells = header.mergeCells.concat(data.mergeCells);
        data.cell = header.cell.concat(data.cell);
        data.nestedHeaders = header.nestedHeaders;
        data.hiddenColumns = header.hiddenColumns;
        return data;
    }
    var renderHOTVarStructure = function(data,headerObject){
        var renderObjModel = _renderHOT(headerObject.data.concat(data));
        hotData = renderObjModel.data;
        renderObjModel = mergeHeaderAndData(headerObject, renderObjModel);
        _updateSettings('hotRSSP',renderObjModel);
    }
    $scope.doCustomSave = function(){
        RSSP.updateDetail(dataBEModel);
        constructBEDetailArr();
        if(dataBETypeArr.length>0)
            RSSP.updateTypeColor(dataBETypeArr);
        $scope.getData();
    }
    //----------------------------------
    // Merge Template vs Data
    //----------------------------------
    function loadXLSToDataObject(jsonXLS,template,dataObj){
        angular.forEach(template, function(temp, index){
            if(temp.BEFieldName!=null && temp.BEFieldName!=''){
                var dataIdx = _.findIndex(dataObj, {Data: temp.BEFieldName});
                if(dataIdx>=0){
                    for(i=12; i>0; i--){
                        dataObj[dataIdx]['NMin'+i] = forceNumberIfNaN(jsonXLS[index+2]['N-'+i]);
                    }
                    dataObj[dataIdx]['N'] = forceNumberIfNaN(jsonXLS[index+2]['N']);
                    for(i=1; i<13; i++){
                        dataObj[dataIdx]['N'+i] = forceNumberIfNaN(jsonXLS[index+2]['N+'+i]);
                    }                    
                }
            }
        });
        return dataObj;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#rsspWS' ) );
        XLSXInterface.loadToJson(myEl[0].files[0], function(json){
            for(var key in json){
                if(key==$scope.selectedProductModel.name){
                    dataBEModel = loadXLSToDataObject(json[key], modelTemplate[$scope.mRSSP.VehicleModelId], dataBEModel);
                }else{
                    dataBEType[0].data[key] = loadXLSToDataObject(json[key], typeColorTemplate[$scope.mRSSP.VehicleModelId], dataBEType[0].data[key]);
                }
            }
            $timeout(function(){
                prepareSum();
                $timeout(function() {
                    if($scope.gridSumApi.selection.selectRow){
                        $scope.gridSumApi.selection.selectRow($scope.gridSum.data[0]);
                    }
                });                    
            });
        });
    };
    var rsspWriteXLSX = function(json,fileName,multiSheet,sheetOpt){
        function  constructSheetFromJson(json,cellOptions){
            function createCell(val){
                var cell = {v: val};
                if(cell.v != null){
                    if(typeof cell.v === 'number') cell.t = 'n';
                    else if(cell.v.indexOf('=')>=0){
                        cell.f = cell.v.substr(1);
                        cell.t = 'n';
                        // cell.s = { fill:{
                        //                 patternType: 'solid',
                        //                 fgColor: { theme: 8, tint: 0.3999755851924192, rgb: '9ED2E0' },
                        //                 bgColor: { indexed: 64 } 
                        //             }
                        //          };
                        delete cell.v;
                    }
                    else cell.t = 's';                    
                }
                return cell;
            };
            var ws = {};
            var colMax = 0;
            for(var R = 0; R < json.length; R++){
                var C = 0;
                for(var fld in json[R]){
                    if(colMax<C) colMax = C;
                    if(R==0){
                        var cell = createCell(fld);
                        var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                        ws[cell_ref] = cell;
                    }
                    var cell = createCell(json[R][fld]);
                    if(cellOptions){
                        if(cellOptions[R][fld])
                            cell.s = cellOptions[R][fld];
                    }
                    var cell_ref = XLSX.utils.encode_cell({c:C,r:R+1});
                    ws[cell_ref] = cell;
                    C++;
                }
            };
            var range = {s: {c:0, r:0 }, e: {c:colMax, r:json.length}};
            ws['!ref'] = XLSX.utils.encode_range(range);
            return ws;
        };
        function s2ab(s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        };
        if(multiSheet!=undefined && multiSheet.length>0){
            var wb = {
                SheetNames : multiSheet,
                Sheets : {}
            };
            for(i in multiSheet){
                wb.Sheets[multiSheet[i]] = constructSheetFromJson(json[i],sheetOpt[i]);
            }
        }
        var wbout = XLSX.write(wb, {bookType:'xlsx', cellFormula:true, cellStyles:true, bookSST:true, type: 'binary'});
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName+".xlsx");
    }
    function createRow(obj,isDetail){
        obj = obj || {};
        var newRow = {};
        newRow.GETSUDO = obj.GETSUDO || '';
        newRow[getsudoName] = obj[getsudoName] || '';
        newRow.Keterangan = obj.Keterangan || '';
        newRow.Group = obj.Group || '';
        for(var i=0; i<24; i++){
            if(i<12 && !isDetail)
                newRow['N-'+(12-i)] = obj['N-'+(12-i)] || 0;
            else if(i==12)
                newRow['N'] = obj['N'] || 0;
            else if(i>12 && (!isDetail || i<18))
                newRow['N+'+(i-12)] = obj['N+'+(i-12)] || 0;
        }
        return newRow;
    };
    function createSheetFromTemplateAndData(template, data, col1, col2, isDetail){
        var sheetObj = [];
        var mergeCells = [];
        var lastPrnId = 0;
        var newRow = {};
        newRow.GETSUDO = col1;
        newRow[getsudoName] = col2;
        for(var i=12; i>0; i--){
            newRow['N-'+i] = getFlexMonth(getsudoMonth-i-1);
        }
        newRow['N'] = getFlexMonth(getsudoMonth-1);
        for(var i=1; i<13; i++){
            newRow['N+'+i] = getFlexMonth(getsudoMonth+i-1);
        }
        sheetObj.push(createRow(newRow,isDetail));
        sheetObj.push(createRow({},isDetail));
        angular.forEach(template, function(temp, index){
            var beIdx = _.findIndex(data, {Data : temp.BEFieldName});
            var newRow = {};
            if((temp.ParentId==0 || temp.ParentId==null) && (temp.AdjustmentRatioTypeId==0 || temp.AdjustmentRatioTypeId==null)){

            }else if(!temp.ParentId && temp.AdjustmentName){
                newRow.GETSUDO = temp.AdjustmentName;
            }else{
                if(temp.ParentName.length>5){
                    if(lastPrnId!=temp.ParentId){
                        newRow.GETSUDO = temp.ParentName;
                    }else{
                        newRow.GETSUDO = temp.AdjustmentName;
                    }
                }else{
                    if(lastPrnId!=temp.ParentId){
                        newRow.GETSUDO = temp.ParentName;
                        newRow[getsudoName] = temp.AdjustmentName;
                    }else{
                        newRow[getsudoName] = temp.AdjustmentName;
                    }
                }
            }
            lastPrnId = temp.ParentId;
            for(var i=12; i>0; i--){
                if(temp['M_'+i]!=null && temp['M_'+i].length>2)
                    newRow['N-'+i] = temp['M_'+i];
                else if(beIdx>=0)
                    newRow['N-'+i] = forceNumberIfNaN(data[beIdx]['NMin'+i]);
            }
            if(temp['M']!=null && temp['M'].length>2)
                newRow['N'] = temp['M'];
            else if(beIdx>=0)
                newRow['N'] = forceNumberIfNaN(data[beIdx]['N']);

            for(var i=1; i<12; i++){
                if(temp['M'+i]!=null && temp['M'+i].length>2)
                    newRow['N+'+i] = temp['M'+i];
                else if(beIdx>=0)
                    newRow['N+'+i] = forceNumberIfNaN(data[beIdx]['N'+i]);
            }
            sheetObj.push(createRow(newRow,isDetail));
        })
        return sheetObj;
    }
    function createHeaderStyle(obj){
        var res = [];
        for(var i=0; i<2; i++){
            var newObj = {};
            for(var key in obj){
                newObj[key] = '';
            }
            res.push(newObj);
        }
        return res;
    }
    function constructWorkSheet(){
        var sheets = [];
        var sheetsName = [];
        var sheetsOpt = [];
        var sheetObj = createSheetFromTemplateAndData(modelTemplate[$scope.mRSSP.VehicleModelId], dataBEModel, 'Model', $scope.selectedProductModel.name, false);
        sheetsOpt.push(createHeaderStyle(sheetObj[0]));
        sheets.push(sheetObj);
        sheetsName.push($scope.selectedProductModel.name);
        angular.forEach($scope.gridSum.data, function(vehicle, index){
            if(vehicle.$$treeLevel==2){
                var typeKey = vehicle.parent.KatashikiCode+'#'+vehicle.parent.SuffixCode+'#'+vehicle.ColorCode;
                var sheetObj = createSheetFromTemplateAndData(typeColorTemplate[$scope.mRSSP.VehicleModelId], dataBEType[0].data[typeKey], vehicle.parent.name, vehicle.name, true);
                sheetsOpt.push(createHeaderStyle(sheetObj[0]));
                sheets.push(sheetObj);
                sheetsName.push(typeKey);
            }
        });
        return {sheets: sheets, sheetsName: sheetsName, sheetsOpt: sheetsOpt};
    }
    $scope.download = function(){
        var constructObj = constructWorkSheet();
        XLSXInterface.writeToXLSX(constructObj.sheets, 'Getsudo '+getsudoName+' '+$scope.selectedProductModel.name, constructObj.sheetsName, constructObj.sheetsOpt);
    }
    $scope.upload = function(){
        document.getElementById('rsspWS').click();
    }
    function forceNumberIfNaN(n){
        var res = parseFloat(n);
        return isNaN(res)?0.0:res;
    }
    var mergeTemplate = function(template){
        if(hotData.length==0 || template.length==0 || dataBEModel.length==0) return;
        angular.forEach(template, function(value, index){
            for(var i=2; i>0; i--){
                if(value['M_'+i]!=null && (typeof value['M_'+i]=='string') && value['M_'+i].trim().length>0){
                    hotData[index+3]['M_'+i] = value['M_'+i].replace(/\$/g,'');
                }
            }
            if(value['M']!=null && (typeof value['M']=='string') && value['M'].trim().length>0){
                hotData[index+3]['M'] = value['M'].replace(/\$/g,'');
            }
            for(var i=1; i<12; i++){
                if(value['M'+i]!=null && (typeof value['M'+i]=='string') && value['M'+i].trim().length>0){
                    hotData[index+3]['M'+i] = value['M'+i].replace(/\$/g,'');
                }
            }
        });
        $scope.hotRSSP.updateSettings({
            data: hotData
        });
    }
    var mergeDataFromBackEnd = function(data){
        if(hotData.length==0 || data.length==0) return;
        dataBEModelIdx = {};
        angular.forEach(data, function(value, index){
            var idx = _.findIndex(hotData, {BEFieldName: value.Data});
            if (idx>=0){
                dataBEModelIdx[value.Data] = {hotIdx: idx, beIdx: index};
                for(var i=12; i>0; i--){
                    hotData[idx]['M_'+i] = forceNumberIfNaN(value['NMin'+i]);
                }
                hotData[idx].M = forceNumberIfNaN(value.N);
                for(var i=1; i<12; i++){
                    hotData[idx]['M'+i] = forceNumberIfNaN(value['N'+i]);
                }
            }else{
                console.log('Notyet mapped : '+value.Data+' =>',value);
            }
        });
        $scope.hotRSSP.updateSettings({
            data: hotData
        });
    };
    var getSummaryType = function(idType){
        var id = idType.split('-');
        var data = _.find($scope.selectedProductModel.child, {name: id[0], SuffixCode: id[1]});
        var sumVariable = {};
        var res = [];
        for(var i in data.child){
            var typeKey = data.KatashikiCode+'#'+data.SuffixCode+'#'+data.child[i].ColorCode;
            for(j in dataBEType[0].data[typeKey]){
                if(!(dataBEType[0].data[typeKey][j].Data in sumVariable)){
                    sumVariable[dataBEType[0].data[typeKey][j].Data] = {N1:0, N2:0, N3:0, N4:0, N5:0};
                }
                sumVariable[dataBEType[0].data[typeKey][j].Data].N1 += dataBEType[0].data[typeKey][j].N1;
                sumVariable[dataBEType[0].data[typeKey][j].Data].N2 += dataBEType[0].data[typeKey][j].N2;
                sumVariable[dataBEType[0].data[typeKey][j].Data].N3 += dataBEType[0].data[typeKey][j].N3;
                sumVariable[dataBEType[0].data[typeKey][j].Data].N4 += dataBEType[0].data[typeKey][j].N4;
                sumVariable[dataBEType[0].data[typeKey][j].Data].N5 += dataBEType[0].data[typeKey][j].N5;
            }
        }
        for(key in sumVariable){
            res.push({Data: key, N1: sumVariable[key].N1, N2: sumVariable[key].N2, N3: sumVariable[key].N3, N4: sumVariable[key].N4, N5: sumVariable[key].N5});
        }
        return res;
    }
    var updateBEValue = function(){
        for(var key in dataBEModelIdx){
            var val = getCellValue(dataBEModelIdx[key].hotIdx, 16);
            dataBEModel[dataBEModelIdx[key].beIdx]['N'] = forceNumberIfNaN(val);
            for(var i = 1; i<12; i++){
                var val = getCellValue(dataBEModelIdx[key].hotIdx, 16+i);
                dataBEModel[dataBEModelIdx[key].beIdx]['N'+i] = forceNumberIfNaN(val);
            }
        }
    };
    var updateBEDetailValue = function(){
        for(var key in dataBEModelIdx){
            var val = getCellValue(dataBEModelIdx[key].hotIdx, 16);
            dataBEType[0].data[lastSelectedTree.id][dataBEModelIdx[key].beIdx]['N'] = forceNumberIfNaN(val);
            for(var i = 1; i<5; i++){
                var val = getCellValue(dataBEModelIdx[key].hotIdx, 16+i);
                dataBEType[0].data[lastSelectedTree.id][dataBEModelIdx[key].beIdx]['N'+i] = forceNumberIfNaN(val);
            }
        }
    };
    var constructBEDetailArr = function(){
        for(var key in typeColorAdjusted){
            dataBETypeArr.push(dataBEType[0].data[key]);
        }
    }
    //----------------------------------
    // Data Operation
    //----------------------------------
    $scope.getData = function() {
        if($scope.filter.orgId!=null & $scope.filter.GetsudoPeriodId!=null)
            RSSP.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
    }
    GetsudoPeriod.getData().then(
        function(res){
            $scope.periodData = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );
    OrgChart.getDataTree().then(
        function(res){
            $scope.orgData = res.data.Result;
            if($scope.orgData.length==1){
                // $scope.filter.orgId = $scope.orgData[0].id;
                if ($scope.orgData[0].child.length==1)
                    $scope.filter.orgId = $scope.orgData[0].child[0].id;
                else
                    $scope.filter.orgId = $scope.orgData[0].id;
            }
            $scope.loading=false;
            return res.data;
        }
    );
    ProductModel.getDataTree().then(
        function(res){
            for(var i in res.data.Result){
                $scope.productModelData.push({
                    VehicleModelId:res.data.Result[i].VehicleModelId, 
                    VehicleModelName:res.data.Result[i].VehicleModelName,
                    VehicleModelCode:res.data.Result[i].VehicleModelCode
                });
            }
            productTree = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );

    var resizeHot=function() {
        if($scope.hotRSSP)
            $scope.hotRSSP.render();
    };
    angular.element($window).bind('resize', resizeHot);

    var prepareSum = function(){
        var data = $scope.selectedProductModel;
        sumRS = [];
        sumWS = [];
        var modelRS = {id:data.name, name:data.name, n1:0, n2:0, n3:0, n4:0, n5:0, $$treeLevel:0};
        var modelWS = {id:data.name, name:data.name, n1:0, n2:0, n3:0, n4:0, n5:0, $$treeLevel:0};
        sumRS.push(modelRS);
        sumWS.push(modelWS);
        var datas = _.sortBy(data.child, 'name');

        typeColorCompare = [];
        for(var key in dataBEType[0].data){
            var spkComp = _.find(dataBEType[0].data[key], {Data: "SPKComp"});
            typeColorCompare.push({id: key, spkComp: spkComp.N});
        }
        typeColorCompare = _.sortBy(typeColorCompare, ['spkComp']);
        for(var j in datas){
            var checkChildFirst = true;
            var typeRS = {parent:modelRS,id:data.name+'-'+datas[j].SuffixCode, name:datas[j].name+'-'+datas[j].SuffixCode, KatashikiCode:datas[j].KatashikiCode, SuffixCode:datas[j].SuffixCode, n1:0, n2:0, n3:0, n4:0, n5:0, $$treeLevel:1};
            var typeWS = {parent:modelWS,id:data.name+'-'+datas[j].SuffixCode, name:datas[j].name+'-'+datas[j].SuffixCode, KatashikiCode:datas[j].KatashikiCode, SuffixCode:datas[j].SuffixCode, n1:0, n2:0, n3:0, n4:0, n5:0, $$treeLevel:1};
            for(var k in datas[j].child){
                var typeKey = datas[j].KatashikiCode+'#'+datas[j].SuffixCode+'#'+datas[j].child[k].ColorCode;
                if(dataBEType[0].data[typeKey]!=null){
                    if(checkChildFirst){
                        sumRS.push(typeRS);
                        sumWS.push(typeWS);
                        checkChildFirst = false;
                    }
                    var rowRS = _.find(dataBEType[0].data[typeKey],{Data: 'RS Adjustment'});
                    var rowWS = _.find(dataBEType[0].data[typeKey],{Data: 'WS Adjustment'});
                    sumRS.push({$$treeLevel:2, id:typeKey, name:datas[j].child[k].name, ColorCode:datas[j].child[k].ColorCode, parent:typeRS, n1: rowRS.N1, n2: rowRS.N2, n3: rowRS.N3, n4: rowRS.N4, n5: rowRS.N5});                    
                    sumWS.push({$$treeLevel:2, id:typeKey, name:datas[j].child[k].name, ColorCode:datas[j].child[k].ColorCode, parent:typeRS, n1: rowWS.N1, n2: rowWS.N2, n3: rowWS.N3, n4: rowWS.N4, n5: rowWS.N5});   
                    typeRS.n1 += rowRS.N1;
                    typeRS.n2 += rowRS.N2;
                    typeRS.n3 += rowRS.N3;
                    typeRS.n4 += rowRS.N4;
                    typeRS.n5 += rowRS.N5;
                    typeWS.n1 += rowWS.N1;
                    typeWS.n2 += rowWS.N2;
                    typeWS.n3 += rowWS.N3;
                    typeWS.n4 += rowWS.N4;
                    typeWS.n5 += rowWS.N5;
                    modelRS.n1+= rowRS.N1;                   
                    modelRS.n2+= rowRS.N2;                   
                    modelRS.n3+= rowRS.N3;                   
                    modelRS.n4+= rowRS.N4;                   
                    modelRS.n5+= rowRS.N5;                   
                    modelWS.n1+= rowWS.N1;                   
                    modelWS.n2+= rowWS.N2;                   
                    modelWS.n3+= rowWS.N3;                   
                    modelWS.n4+= rowWS.N4;                   
                    modelWS.n5+= rowWS.N5;                   
                }
            }
        }
        var rowRS = _.find(dataBEModel,{Data: 'RetailSalesAdjustment'});
        var rowWS = _.find(dataBEModel,{Data: 'WholeSalesAdjustment'});
        sumModelRS = [{id:data.name, name:data.name, n1:rowRS.N1, n2:rowRS.N2, n3:rowRS.N3, n4:rowRS.N4, n5:rowRS.N5, $$treeLevel:0},
                      {id:'Gap', name:'Gap', n1:rowRS.N1-modelRS.n1, n2:rowRS.N2-modelRS.n2, n3:rowRS.N3-modelRS.n3, n4:rowRS.N4-modelRS.n4, n5:rowRS.N5-modelRS.n5, $$treeLevel:0}];
        sumModelWS = [{id:data.name, name:data.name, n1:rowWS.N1, n2:rowWS.N2, n3:rowWS.N3, n4:rowWS.N4, n5:rowWS.N5, $$treeLevel:0},
                      {id:'Gap', name:'Gap', n1:rowWS.N1-modelWS.n1, n2:rowWS.N2-modelWS.n2, n3:rowWS.N3-modelWS.n3, n4:rowWS.N4-modelWS.n4, n5:rowWS.N5-modelWS.n5, $$treeLevel:0}];
        $scope.changeTypeSum();
    };
    $scope.redistribute = function(){
        for(var n=1; n<6; n++){
            if(sumModelRS[1]['n'+n] > 0){
                var equalCtr = 0;
                var lastQty = null;
                var distributed = 0;
                for(var j=typeColorCompare.length-1; j>0; j--){
                    if(lastQty==null || lastQty<=typeColorCompare[j].spkComp){
                        lastQty = typeColorCompare[j].spkComp;
                        equalCtr++;
                    }else break;
                }
                var distributeVal = Math.ceil(sumModelRS[1]['n'+n]/equalCtr);
                for(var j=typeColorCompare.length-1; j>=typeColorCompare.length-equalCtr; j--){
                    var rsIdx = _.findIndex(dataBEType[0].data[typeColorCompare[j].id],{Data: 'RS Adjustment'});
                    var valToAdd = (sumModelRS[1]['n'+n]-distributed<distributeVal)?sumModelRS[1]['n'+n]-distributed:distributeVal;
                    dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n] += valToAdd;
                    distributed += valToAdd;
                    if(distributed==sumModelRS[1]['n'+n]) break;
                }
            }else if(sumModelRS[1]['n'+n] < 0){
                var distributed = 0;
                var qtyToSubstact = Math.abs(sumModelRS[1]['n'+n]);
                function decreaseQty(){
                    var lastQty = null;
                    var equalCtr = 0;
                    for(j=0; j<typeColorCompare.length; j++){
                        var rsIdx = _.findIndex(dataBEType[0].data[typeColorCompare[j].id],{Data: 'RS Adjustment'});
                        if(dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n]==0) continue;
                        if(lastQty==null || lastQty>=typeColorCompare[j].spkComp){
                            lastQty = typeColorCompare[j].spkComp;
                            equalCtr++;
                        }else break;
                    }
                    var distributeVal = Math.ceil(qtyToSubstact/equalCtr);
                    if(distributeVal!=Infinity) {
                        for(var j=0; j<typeColorCompare.length; j++){
                            var rsIdx = _.findIndex(dataBEType[0].data[typeColorCompare[j].id],{Data: 'RS Adjustment'});
                            if(dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n]==0) continue;
                            var valToSubstract = (distributeVal>dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n])?dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n]:distributeVal;
                            dataBEType[0].data[typeColorCompare[j].id][rsIdx]['N'+n] -= valToSubstract;
                            distributed += valToSubstract;
                            if(distributed==qtyToSubstact) break;
                        }
                        if(qtyToSubstact>distributed){
                            decreaseQty();
                        }
                    }
                }
                decreaseQty();
            }
        }
        prepareSum();
    }
    var recalcSum = function(){
        if(lastSelectedTree.treeLevel==2){
            var selected = $scope.gridSumApi.selection.getSelectedRows();
            var rsIdx = _.findIndex(sumRS, {id: selected[0].id});
            for(var n=1; n<6; n++){
                var delta = $scope.hotRSSP.getDataAtRowProp(rowIndexes.rsAdj, 'M'+n) - sumRS[rsIdx]['n'+n];
                sumRS[rsIdx]['n'+n] = $scope.hotRSSP.getDataAtRowProp(rowIndexes.rsAdj, 'M'+n);
                sumRS[rsIdx].parent['n'+n] = sumRS[rsIdx].parent['n'+n] + delta;
                sumRS[rsIdx].parent.parent['n'+n] = sumRS[rsIdx].parent.parent['n'+n] + delta;

                var delta = $scope.hotRSSP.getDataAtRowProp(rowIndexes.wsAdj, 'M'+n) - sumWS[rsIdx]['n'+n];
                sumWS[rsIdx]['n'+n] = $scope.hotRSSP.getDataAtRowProp(rowIndexes.wsAdj, 'M'+n);
                sumWS[rsIdx].parent['n'+n] = sumWS[rsIdx].parent['n'+n] + delta;
                sumWS[rsIdx].parent.parent['n'+n] = sumWS[rsIdx].parent.parent['n'+n] + delta;
            }            
        }else if (lastSelectedTree.treeLevel==0){
            var rowRS = _.findIndex(hotData,{ParentName: 'RS', AdjustmentName:'Adjustment'});
            var rowWS = _.findIndex(hotData,{ParentName: 'WS', AdjustmentName:'Adjustment'});
            for(var n=1; n<6; n++){
                sumModelRS[0]['n'+n] = hotData[rowRS]['M'+n];
                sumModelWS[0]['n'+n] = hotData[rowWS]['M'+n];
            }
        }
        for(var n=1; n<6; n++){
            sumModelRS[1]['n'+n] = sumModelRS[0]['n'+n] - sumRS[0]['n'+n];
            sumModelWS[1]['n'+n] = sumModelWS[0]['n'+n] - sumWS[0]['n'+n];
        }
        $scope.changeTypeSum();
    }
    $scope.onShowDetail = function(row){
        // selectedMonth = new Date(2016,6,1);
        $scope.selectedProductModel = _.find(productTree, { 'id': $scope.mRSSP.VehicleModelId });
        $scope.mRSSP.GetsudoPeriodId = $scope.filter.GetsudoPeriodId;
        hotData = [];
        dataBEModel = [];
        typeColorAdjusted = {};
        if(!($scope.mRSSP.VehicleModelId in modelTemplate)){
            RSSPTemplate.findByModel($scope.mRSSP.VehicleModelId).then(
                function(res){
                    var splittedArr = _.partition(res.data.Result, {FormulaType:1});
                    modelTemplate[$scope.mRSSP.VehicleModelId] = splittedArr[0];
                    typeColorTemplate[$scope.mRSSP.VehicleModelId] = splittedArr[1];
                }
            );
        }

        var newFilter = {GetsudoPeriodId: $scope.filter.GetsudoPeriodId, OutletId:$scope.filter.orgId, VehicleModelId: $scope.mRSSP.VehicleModelId};
        RSSP.getDetailTypeColor(newFilter).then(
            function(res){
                newFilter.data = res.data.Result;
                dataBEType = [newFilter];
                if(dataBEType.length>0 && dataBEModel.length>0){
                    var vehType = _.find(dataBEModel,{Data:'SalesTypeId'});
                    fixCtr = (vehType && (parseInt(vehType.N) & 1 == 1)) ? 1 : 2;
                    prepareSum();
                    $timeout(function() {
                        if($scope.gridSumApi.selection.selectRow){
                            $scope.gridSumApi.selection.selectRow($scope.gridSum.data[0]);
                        }
                    });                    
                }
                $scope.loading=false;
            }
        );
        dataBEType = [];
        dataBETypeArr = [];
        RSSP.getDetail({GetsudoPeriodId: $scope.filter.GetsudoPeriodId, OutletId:$scope.filter.orgId, VehicleModelId: $scope.mRSSP.VehicleModelId}).then(
            function(res){
                dataBEModel = res.data.Result;
                mergeDataFromBackEnd(dataBEModel);
                if(dataBEType.length>0 && dataBEModel.length>0){
                    var vehType = _.find(dataBEModel,{Data:'SalesTypeId'});
                    fixCtr = (vehType && (parseInt(vehType.N) & 1 == 1)) ? 1 : 2;
                    prepareSum();
                    $timeout(function() {
                        if($scope.gridSumApi.selection.selectRow){
                            $scope.gridSumApi.selection.selectRow($scope.gridSum.data[0]);
                        }
                    });                    
                }
                $scope.loading=false;
                return res.data.Result;
            }
        );	
        RSSP.getComment($scope.mRSSP.NId).then(
            function(res){
                $scope.comments = res.data.Result;
            }
        );
        RSSP.getComposition({GetsudoPeriodId: $scope.filter.GetsudoPeriodId, OutletId:$scope.filter.orgId, VehicleModelId: $scope.mRSSP.VehicleModelId}).then(
            function(res){
                $scope.gridSPK.data = [];
                $scope.gridCL.data = [];
                $scope.gridOS.data = [];
                $scope.gridPolreg.data = [];
            }
        );
    };
    $scope.sendComment = function(event){
        if(event.keyCode==13 || event.key=='Enter'){
            var commentObj = {RSSPTotalModelAdjustmentId: $scope.mRSSP.NId, Comment: $scope.comment.message, UserName:$scope.user.name};
            RSSP.postComment(commentObj).then(function(){
                $scope.comments.push(commentObj);
                $scope.comment.message = '';
            });
        }
    }
    $scope.bulkReview = function(selected){
        var review = [];
        for(var i in selected){
            review.push({NId: selected[i].NId, N1Id: selected[i].N1Id, N2Id: selected[i].N2Id, N3Id: selected[i].N3Id, N4Id: selected[i].N4Id, N5Id: selected[i].N5Id, N6Id: selected[i].N6Id, N7Id: selected[i].N7Id, N8Id: selected[i].N8Id, N9Id: selected[i].N9Id, HOReviewState: true});
        }
        RSSP.postReview(review).then(function(){
            $scope.getData();
        });
    }
    $scope.onSelectRows = function(selected){
        console.log(selected);
    }
    $scope.changeComposition = function(size){
        $timeout(function(){
            $scope.showComposition = true;
        },0);
    };
    $scope.selectPeriod = function(selected){
        selectedGetsudo = selected;
        selectedMonth = selected.EditStartPeriod;
    }
    $scope.selectOutlet = function(selected){
        $scope.filter.orgId = selected.id;
    }
    $scope.showHideData = function(){
        $timeout(function(){
            var visibilityArr = ['Always', 'Market', 'SPK', 'Reference', 'Hide'];
            var hiddenRows = [];
            for(var i in hotData){
                if((hotData[i].Visibility==1) && ($scope.Visibility.market==0)){
                    hiddenRows.push(i);
                }else if((hotData[i].Visibility==2) && ($scope.Visibility.spk==0)){
                    hiddenRows.push(i);
                }else if((hotData[i].Visibility==3) && ($scope.Visibility.reference==0)){
                    hiddenRows.push(i);
                }else if(hotData[i].Visibility==4){
                    hiddenRows.push(i);
                }
            }
            $scope.hotRSSP = hotRegisterer.getInstance('hotRSSP');
            $scope.hotRSSP.updateSettings({
                hiddenRows: {
                    copyPasteEnabled: true,
                    indicators: false,
                    rows: hiddenRows
                }
            });
        });
    }
    $scope.showAlert = function(message, number){
        $.bigBox({
            title: 'Input salah',
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            number: number
        });
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    var mainColDefs = [
        { name:'VehicleModelId',    field:'VehicleModelId', visible:false },
        { name:'Model', field: 'VehicleModelName' },
        { name:'Revisi', field: 'AdjustmentFlag' },
    ];
    for(var i=0; i<6; i++){
        var addName = i ? ('+'+i):'';
        var addField = i ? i:'';
        mainColDefs.push({name: 'N'+addName, field: 'N'+addField, width:40, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum});
    };
    mainColDefs.push({ name:'Total', field: 'Total', aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum });
    mainColDefs.push({ name:'Komentar', field: 'Comment' });
    mainColDefs.push({ name:'HO Review', field: 'HOReviewState' });
    $scope.grid = {
        enableSorting: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        paginationPageSize: 15,
        columnDefs: mainColDefs
    };
    //----------------------------------
    // Handsontable Model & Type Color
    //----------------------------------
    var hotCols = [
        { data: 'ParentName', type: 'text', editor: false},
        { data: 'AdjustmentName', type: 'text', editor: false},
        { data: 'VariableInputTypeId', type: 'text', editor: false},
        { data: 'Visibility', type: 'numeric'}
    ];
    var reverseCols = [];
    for(var i=1; i<13; i++){
        var reverseName = 'M_'+(13-i);
        hotCols.push({data: reverseName, type:'numeric', format: '0.0', editor: false});
        reverseCols.push({data: 'M'+i, type:'numeric', format: '0.0',});
    }
    hotCols.push({data:'M', type:'numeric', format: '0.0'});
    hotCols = hotCols.concat(reverseCols);
    hotCols = hotCols.concat(
        { data: 'AdjustmentRatioTypeId', type: 'numeric'},
        { data: 'ParentId', type: 'numeric'},
        { data: 'VariableType', type: 'numeric'},
        { data: 'FormatTypeId', type: 'numeric'}
    );
    $scope.hotRSSPSettings = {
        colWidths: [50,180,100,110,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45],
        fixedColumnsLeft:2,
        height: 600,
        width: angular.element(document.getElementById('rsspModelContainer')).clientWidth,
        columns: hotCols
    };
    //----------------------------------
    // Misc
    //----------------------------------
    $scope.gridSum = {
        paginationPageSize: 100,
        showTreeExpandNoChildren: false,
        multiSelect: false,
        enableRowHeaderSelection: false,
        columnDefs: [
            { name:'ID', field: 'id', visible: false},
            { name:'Type Color', width:'40%', field: 'name', pinnedLeft:true},
            { name:'N+1', width:40, field: 'n1'},
            { name:'N+2', width:40, field: 'n2'},
            { name:'N+3', width:40, field: 'n3'},
            { name:'N+4', width:40, field: 'n4'},
            { name:'N+5', width:40, field: 'n5'}
        ]
    };
    $scope.gridSumGap = {
        paginationPageSize: 5,
        showTreeExpandNoChildren: false,
        multiSelect: false,
        enableRowHeaderSelection: false,
        columnDefs: [
            { name:'ID', field: 'id', visible: false},
            { name:'Total Model', width:'40%', field: 'name', pinnedLeft:true},
            { name:'N+1', width:40, field: 'n1'},
            { name:'N+2', width:40, field: 'n2'},
            { name:'N+3', width:40, field: 'n3'},
            { name:'N+4', width:40, field: 'n4'},
            { name:'N+5', width:40, field: 'n5'}
        ]
    };
    $scope.changeTypeSum = function(){
        $timeout(function(){
            if($scope.summary.type==1){
                $scope.gridSum.data = sumRS;
                $scope.gridSumGap.data = sumModelRS;
            }else{
                $scope.gridSum.data = sumWS;
                $scope.gridSumGap.data = sumModelWS;
            }
            if($scope.gridSumApi.selection.selectRow){
                var idx = _.find($scope.gridSum.data, {id: lastSelectedTree.id});
                idx = idx < 0 ? 0 : idx;
                $scope.gridSumApi.selection.selectRow($scope.gridSum.data[idx]);
            }

        });
    }
    $scope.gridSum.onRegisterApi = function (gridApi) {
        $scope.gridSumApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(data){
            if(data.treeLevel==0){
                var template = angular.copy(modelTemplate[$scope.mRSSP.VehicleModelId]);
                renderHOTVarStructure(template,createHeader('Model',$scope.selectedProductModel.name));
                mergeDataFromBackEnd(dataBEModel);
                mergeTemplate(modelTemplate[$scope.mRSSP.VehicleModelId]);
            }else{
                if(data.treeLevel==1){
                    var template = angular.copy(typeColorTemplate[$scope.mRSSP.VehicleModelId]);
                    renderHOTVarStructure(template,createDetailHeader('TOTAL',data.entity.name));
                    mergeDataFromBackEnd(getSummaryType(data.entity.name));
                }
                else if(data.treeLevel==2){
                    var template = angular.copy(typeColorTemplate[$scope.mRSSP.VehicleModelId]);
                    renderHOTVarStructure(template,createDetailHeader(data.entity.parent.name,data.entity.name));
                    var typeKey = data.entity.parent.KatashikiCode+'#'+data.entity.parent.SuffixCode+'#'+data.entity.ColorCode;
                    mergeDataFromBackEnd(dataBEType[0].data[typeKey]);
                    mergeTemplate(typeColorTemplate[$scope.mRSSP.VehicleModelId]);
                }
            }
            lastSelectedTree = {treeLevel: data.treeLevel, id: typeKey};
        });
    }
    $scope.gridSPK = {
        enableRowSelection: true,
        multiSelect: true,
        paginationPageSize: 15,
        columnDefs: [
            { name:'Periode', width:'60%', field: 'period'},
            { name:'Total', width:'40%', field: 'total'}
        ]
    };
    $scope.gridOS = angular.copy($scope.gridSPK);
    $scope.gridCL = angular.copy($scope.gridSPK);
    $scope.gridPolreg = angular.copy($scope.gridSPK);

    $scope.gridSPK.onRegisterApi = function (gridApi) {
        $scope.gridSPKApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(data){
            console.log(data);
        });
    }
    $scope.gridOS.onRegisterApi = function (gridApi) {
        $scope.gridOSApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(data){
            console.log(data);
        });
    }
    $scope.gridCL.onRegisterApi = function (gridApi) {
        $scope.gridCLApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(data){
            console.log(data);
        });
    }
    $scope.gridPolreg.onRegisterApi = function (gridApi) {
        $scope.gridPolregApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(data){
            console.log(data);
        });
    }

});