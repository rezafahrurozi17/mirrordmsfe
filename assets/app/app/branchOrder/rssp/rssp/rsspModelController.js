angular.module('app')
    .controller('RSSPModelController', function($scope, $http, CurrentUser, CustomerFleet,GetsudoPeriod, $timeout, hotRegisterer) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    // $scope.mFleet = null; //Model
    // $scope.cFleet = null; //Collection
    // $scope.xFleet = {};
    // $scope.xFleet.selected=[];
    //for ui-select
    $scope.xRole = {};
    $scope.xRole.selected=[];
    $scope.period={};
    $scope.period.selected=null;
    $scope.selectPeriod = function(selected){
        console.log("selected=>",selected);
    }
    $scope.getPeriod = function() {
        GetsudoPeriod.getData().then(
            function(res){
                // $scope.grid.data = res.data;
                console.log("period=>",res.data);
                $scope.periodData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.getPeriod();
    //----------------------------------
    //Action Button
    //----------------------------------
    $scope.showOpt = true;
    $scope.actOpt = function(){
        $scope.showOpt = !$scope.showOpt;
    }
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.cFleet = CustomerFleet.getData().then(
            function(res){
                $scope.grid.data = res.data;
                // console.log("role=>",res.data);
                $scope.fleetData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.titleArr = [
            ['APRIL GETSUDO','','Historical Data','','Est.','Ordering Period','','','','','OAP/RAP Plan'],
            ['','','','','Est.','Fix','Flexible','Flexible','Flexible','Flexible'],
            ['AVANZA','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
            ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    ];
    $scope.dataTitle = [];
    for(var i=0;i<$scope.titleArr.length;i++){
        var titleList=$scope.titleArr[i];
        var row={};
        row.title = titleList[0];
        row.title2 = titleList[1];
        row.m1 = titleList[2];
        row.m2 = titleList[3];
        row.m3 = titleList[4];
        row.m4 = titleList[5];
        row.m5 = titleList[6];
        row.m6 = titleList[7];
        row.m7 = titleList[8];
        row.m8 = titleList[9];
        row.m9 = titleList[10];
        row.m10 = titleList[11];
        row.m11 = titleList[12];
        row.m12 = titleList[13];
        row.m13 = '';
        $scope.dataTitle.push(row);
    }
    //----------------------------------
    var BASEROW = 1,BASECOL = 1;
    //--------------------------------------------------------------------------
    // Helper Function
    //--------------------------------------------------------------------------
    function createDataRow(title,title2,arr1,arr2){
        var res={};
        res.title=title;res.title2=title2;
        for(var i=0;i<arr1.length;i++){
            res['m'+(i+1)] = arr1[i];
        }
        if(arr2){
            for(var j=0;j<arr2.length;j++){
                res['m'+(j+arr1.length+1)] = arr2[j];
            }
        }
        // console.log("res=>",res);
        return res;
    }
    function createMergeCells(){
        function _createMergeCells(row,col,rowspan,colspan){
            return {row:row-BASEROW,col:col-BASECOL,rowspan:rowspan,colspan:colspan};
        }
        var res=[];
        //title-title2
        res.push(_createMergeCells(1,1,2,2)); //APRIL GETSUDO
        res.push(_createMergeCells(1,3,2,2)); //Historical Data
        res.push(_createMergeCells(1,5,2,1)); //Est.
        res.push(_createMergeCells(1,6,1,5)); //Ordering Period
        res.push(_createMergeCells(1,11,2,4)); //OAP/RAP Plan
        res.push(_createMergeCells(3,1,2,2)); //AVANZA
        //row 5-9
        for(var i=5;i<=9;i++){
           res.push(_createMergeCells(i,1,1,2));
        }
        //row 10-12 & 13 //RS
        res.push(_createMergeCells(10,1,3,1)); //RS
        res.push(_createMergeCells(13,1,1,2)); //Blank Row
        //row 14-15 & 16-30 //WS
        res.push(_createMergeCells(14,1,2,1)); //WS
        res.push(_createMergeCells(16,1,1,2)); //StockBaseOnSuggestion
        res.push(_createMergeCells(17,1,1,2)); //StockRatio
        res.push(_createMergeCells(18,1,1,2)); //StockBaseOnAdjustment
        res.push(_createMergeCells(19,1,1,2)); //StockRatio
        res.push(_createMergeCells(20,1,1,2)); //Blank Row
        res.push(_createMergeCells(21,1,2,1)); //O/S
        res.push(_createMergeCells(24,1,2,1)); //C/L
        for(var i=0;i<17;i++){
           res.push(_createMergeCells(26+i,1,1,2));
        }
        return res;
    }
    var borderObj = { top: { width: 1}, right: {width: 1}, bottom: {width: 1}, left: { width: 1} };
    function createBorders(){
        function _createCellBorders(row,col){
            var bor = {row:row-BASEROW,col:col-BASECOL};
            _.merge(bor,borderObj);
            return bor;
        }
        function _createBorders(row,col,nrow,ncol){
            var res=[];
            for(var i=0;i<nrow;i++){
                for(var j=0;j<ncol;j++){
                    res.push(_createCellBorders(row+i,col+j));
                }
            }
            return res;
        }
        var res=[];
        // res = res.concat(_createBorders(1,1,2,14));
        // res = res.concat(_createBorders(6,1,3,14));
        // res = res.concat(_createBorders(10,1,3,14));
        // res = res.concat(_createBorders(14,1,6,14));
        res = res.concat(_createBorders(21,1,5,14)); //Reference LM Displan
        res = res.concat(_createBorders(27,1,4,1));
        res = res.concat(_createBorders(27,5,4,5));
        // res.push(_createCellBorders(32,1));          //Gap vs RS Target OAP/RAP
        // res = res.concat(_createBorders(33,1,2,14));
        // res.push(_createCellBorders(36,1));          //Gap vs RS Target Market
        // res = res.concat(_createBorders(37,1,2,14));
        // res.push(_createCellBorders(40,1));          //Gap vs RS Target Outlet Share
        // res = res.concat(_createBorders(41,1,2,14));
        // console.log("createBorders");
        return res;
    }
    //--------------------------------------------------------------------------
    // Grid Settings
    //--------------------------------------------------------------------------
    $scope.hot_settings = {
        colWidths: [30,160,45,45,45,45,45,45,45,45,45,45,45],
        manualColumnResize: [30,160],
        // rowHeaders: true,
        // colHeaders: true,
        // fixedColumnsLeft:1,
        fixedRowsTop:4,
        formulas:true,
        height: 600,
        width: 1000,
        columns: [
                  { data: 'title', type: 'text'},
                  { data: 'title2', type: 'text' },
                  { data: 'm1' ,type: 'numeric' },
                  { data: 'm2' ,type: 'numeric'},
                  { data: 'm3' ,type: 'numeric'},
                  { data: 'm4' ,type: 'numeric'},
                  { data: 'm5' ,type: 'numeric'},
                  { data: 'm6' ,type: 'numeric'},
                  { data: 'm7' ,type: 'numeric'},
                  { data: 'm8' ,type: 'numeric'},
                  { data: 'm9' ,type: 'numeric'},
                  { data: 'm10' ,type: 'numeric'},
                  { data: 'm11' ,type: 'numeric'},
                  { data: 'm12' ,type: 'numeric'},
                  { data: 'm13' ,type: 'numeric'},
        ],
        mergeCells: createMergeCells(),
        customBorders: createBorders(),
        rowHeights: function (row) {
                      row++
                      if(row==9)
                        return 2;
        }
    };
    //--------------------------------------------------------------------------
    //------------ Update Settings to set each Row/Col Properties (Style/Format/Editor/etc)
    //--------------------------------------------------------------------------
    $timeout(function(){
        $scope.hot_rsspmodel = hotRegisterer.getInstance('hot_rsspmodel');
        // console.log("x=>",$scope.hot_rsspmodel);
        $scope.hot_rsspmodel.updateSettings({
                cells: function (row, col, prop) {
                            // console.log(row,col,prop);
                            var cellProperties = {};
                            // cellProperties.editor = false; //Default is No Editor
                            col++;
                            if(col>=5 && col<=14){
                               cellProperties.className = "hotHeader3"
                            }else if(col<5){
                                cellProperties.className = "hotCell"
                            }
                            //------------------------------------------------------------
                            switch(row+1){
                                case 1:
                                case 2: if(col<=14) cellProperties.className = "htCenter htMiddle hotHeader"
                                        else cellProperties.className = "hotFromData";
                                        break;
                                case 3:
                                case 4: if(col<=14) cellProperties.className = "htCenter htMiddle hotHeaderReverse"
                                        else cellProperties.className = "hotFromData";
                                        break;
                                case 5: cellProperties.className = "hotFromData";break;
                                case 6: if(col>=4){
                                            cellProperties.editor = 'numeric';
                                            cellProperties.className = "hotEditable";
                                        }
                                        break;
                                case 7: cellProperties.format = '0%';break;
                                case 8: break;
                                case 9: cellProperties.className = "hotFromData"; break;
                                case 10: if(col==1){
                                            cellProperties.className = "htCenter htMiddle hotCell";
                                         }
                                case 11: break;
                                case 12: if(col>=5){
                                            cellProperties.editor = 'numeric';
                                            cellProperties.className = "hotEditable";
                                         }
                                         break;
                                case 13: cellProperties.className = "hotFromData"; break;
                                case 14:
                                         if(col==1){
                                            cellProperties.className = "htCenter htMiddle hotCell";
                                         }
                                         break;
                                case 15: break;
                                case 16: cellProperties.format = '0';break;
                                case 17: cellProperties.format = '0.00';break;
                                case 18: break;
                                case 19: cellProperties.format = '0.00';break;
                                case 20: cellProperties.className = "hotFromData"; break;
                                case 21: if(col==1){
                                            cellProperties.className = "htCenter htMiddle hotCell";
                                         }
                                         break;
                                case 22: cellProperties.format = '0.00';break;
                                case 23: break;
                                case 24: if(col==1){
                                            cellProperties.className = "htCenter htMiddle hotCell";
                                         }
                                         break;
                                case 25: cellProperties.format = '0%';break;
                                case 26: cellProperties.className = "hotFromData";break;
                                case 27: cellProperties.className = "hotFromData";break;
                                case 28: cellProperties.className = "hotFromData";break;
                                case 29: cellProperties.className = "hotFromData";break;
                                case 30: cellProperties.className = "hotFromData";break;
                                case 31: cellProperties.className = "hotFromData";break;
                                case 32: if(col==1){
                                           cellProperties.className = "hotCell"
                                         }else{
                                            cellProperties.className = "hotFromData";
                                         }
                                         break;
                                case 33:
                                case 34: if(col>=3 && col<=14){
                                           cellProperties.className = "hotHeader3"
                                         }
                                         break;
                                case 35: cellProperties.className = "hotFromData";break;
                                case 36: if(col==1){
                                           cellProperties.className = "hotCell"
                                         }else{
                                            cellProperties.className = "hotFromData";
                                         }
                                         break;
                                case 37:
                                case 38: if(col>=3  && col<=14){
                                           cellProperties.className = "hotHeader3"
                                         }
                                         break;
                                case 39: cellProperties.className = "hotFromData";break;
                                case 40: if(col==1){
                                           cellProperties.className = "hotCell"
                                         }else{
                                            cellProperties.className = "hotFromData";
                                         }
                                         break;
                                case 41:
                                case 42: if(col>=3  && col<=14){
                                           cellProperties.className = "hotHeader3"
                                         }
                                         break;
                                default:
                            }
                            //------------------------------------------------------------
                            return cellProperties;
                }
        })
    },0);
    //--------------------------------------------------------------------------
    //External Data and Input Data
    var ActualMarketByProvince = [80,80];
    var ActualMarketByProvince_Adj = [80,80];
    var DefaultMarketOAP =     [80,80,80,80,80,80,80,80,80,80];
    var DefaultMarketOAP_Adj = [80,80,80,80,80,80,80,80,80,80];
    var RSProvince =     [50,50,50,50,50,50,50,50,50,50,50,50];
    var PolregProvince = [40,40,40,40,40,40,40,40,40,40,40,40];
    var RetailSales =    [10,10,10,10,10,10,10,10,10,10,10,10];
    var RSTargetOAP =          [10,10,10,10,10,10,10,10,10,10,10,10];
    var OutstandingLastMonth = [0];
    var OutstandingLastMonthUnit = [12,13];
    var ActualSPK =            [10,10];
    var ActualSPKCancel =      [2,2];
    var TargetOAP =            [];
    var RSAdjustment =         [10,10, 8,10,12,10,10,10,10,10,10,10,10];
    var WSSuggestion =         [8,8,8,10];
    var StockLastMonth =       [8];
    //--------------------------------------------------------------------------
    //Parameter
    var OutstandingLevel = 2;
    var StockRatioStd = 0.5;
    var StockRatioMin = 0.2;
    var OutletShareTarget = 0.1;
    var ValueAdjMin = 0.1;
    var ValueAdjMax = 0.8;
    var ValueSuggestMin = 0.1;
    var ValueSuggestMax = 0.8;
    //--- ARRAY RESULT OF CALCULATION -----------------------------------
    var RatioPolregToRS=[];
    var PolregOutlet = [];
    var RatioPolregToRSOutlet=[];
    //-------------------------------------------------------------------
    //---------- Ratio Polreg to RS (Prov.) is not shown - put into Array //Row 7
    function CalcRatioPolregToRS(){
        for(var i=0;i<12;i++){
            RatioPolregToRS.push(PolregProvince[i]/RSProvince[i]);
        }
    }
    CalcRatioPolregToRS();
    //-------------------------------------------------------------------
    //---------- Ratio Polreg to RS (Outlet.) is not shown - put into Array //Row 13
    function CalcRatioPolregToRSOutlet(){
        RatioPolregToRSOutlet.push( ( (RetailSales[0]/RSProvince[0])*PolregProvince[0]) / RetailSales[0] ); //C
        RatioPolregToRSOutlet.push( ( (RetailSales[1]/RSProvince[1])*PolregProvince[1]) / RetailSales[1] ); //D
        RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]) /2 ); //E
        RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //F
        for(var i=4;i<12;i++){
            RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //G-N
        }
    }
    CalcRatioPolregToRSOutlet();
    //-------------------------------------------------------------------
    function CalcPolregOutlet(){ //Row 11
        PolregOutlet.push( ( (RetailSales[0]/RSProvince[0])*PolregProvince[0]) ); //C
        PolregOutlet.push( ( (RetailSales[1]/RSProvince[1])*PolregProvince[1]) ); //D
        for(var i=2;i<12;i++){
            PolregOutlet.push( RSAdjustment[i]*RatioPolregToRSOutlet[i] ); //E-N
        }
    }
    CalcPolregOutlet();

    //-------------------------------------------------------------------------------
    // Setup Grid Data
    //-------------------------------------------------------------------------------
    $scope.dataGrid=[];
    $scope.dataGrid.push({}); //Blank Row 5
    $scope.dataGrid.push(createDataRow('Market Polreg by Province (Adjust)','',ActualMarketByProvince_Adj,DefaultMarketOAP_Adj)); //Row 6; //Input
    $scope.dataGrid.push(
        { //Row 7
            title: 'Outlet Share',
            m1:'=(('+RetailSales[0]+'/'+RSProvince[0]+')*'+PolregProvince[0]+')/C6',
            m2:'=(('+RetailSales[1]+'/'+RSProvince[1]+')*'+PolregProvince[1]+')/D6',
            m3:'='+PolregOutlet[2]+'/E$6',
            m4:'='+PolregOutlet[3]+'/F$6',
            m5:'='+PolregOutlet[4]+'/G$6',
            m6:'='+PolregOutlet[5]+'/H$6',
            m7:'='+PolregOutlet[6]+'/I$6',
            m8:'='+PolregOutlet[7]+'/J$6',
            m9:'='+PolregOutlet[8]+'/K$6',
           m10:'='+PolregOutlet[9]+'/L$6',
           m11:'='+PolregOutlet[10]+'/M$6',
           m12:'='+PolregOutlet[11]+'/N$6',
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 8
            title: 'RS Target Market',
            m1:RetailSales[0],  //Col C
            m2:RetailSales[1],  //Col D
            m3:'=(E$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[2],  //Col E
            m4:'=(F$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[3],  //Col F
            m5:'=(G$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[4],  //Col G
            m6:'=(H$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[5],  //Col H
            m7:'=(I$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[6],  //Col I
            m8:'=(J$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[7],  //Col J
            m9:'=(K$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[8],  //Col K
           m10:'=(L$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[9],  //Col L
           m11:'=(M$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[10],  //Col M
           m12:'=(N$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[11],  //Col N
           m13:'=N8', //Col O -> Dummy?
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Blank Row 9
    //----------------------------------------------------------
    $scope.dataGrid.push(createDataRow('RS','OAP/RAP',RSTargetOAP)); //Row 10; //From External Data
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 11
            title: 'RS',
            title2: 'Suggestion',
            m1:RetailSales[0],  //Col C  --> RS Target Market
            m2:RetailSales[1],  //Col D  --> RS Target Market
            m3:'=D18 + E14 - (('+StockRatioStd+'*F8)-('+StockRatioMin+'*'+StockRatioStd+'*F8))',  //Col E
            //--> StockBaseOnAdjustment(18) + WSSuggestion(14) - ( (StockRatioStd*RSTargetMarket(8)) - (StockRatioMin*StockRatioStd*RSTargetMarket(8)) )
            m4:'=E18 + F14 - (('+StockRatioStd+'*G8)-('+StockRatioMin+'*'+StockRatioStd+'*G8))',  //Col F
         //=IF(G10-F46=AVERAGE(D10:F10),G10, IF(G10-F46>(1+$H$35)*AVERAGE(D10:F10),((1+$H$35)*AVERAGE(D10:F10)),IF(AND(F22>(1+$H$35)*AVERAGE(D10:F10),F22>$C$32*H10),((1+$H$35)*AVERAGE(D10:F10)),IF(G10-F46<$G$35*G14,$G$35*G14,G10-F46))))
            m5:'=IF(G8-F38=AVERAGE(D8:F8),G8, IF(G8-F38>(1+'+ValueSuggestMax+')*AVERAGE(D8:F8),((1+'+ValueSuggestMax+')*AVERAGE(D8:F8)),IF(AND(F21>(1+'+ValueSuggestMax+')*AVERAGE(D8:F8),F21>'+OutstandingLevel+'*H8),((1+'+ValueSuggestMax+')*AVERAGE(D8:F8)),IF(G8-F38<'+ValueSuggestMin+'*G10,'+ValueSuggestMin+'*G10,G8-F38))))',  //Col G
            m6:'=IF(H8-G38=AVERAGE(E8:G8),H8, IF(H8-G38>(1+'+ValueSuggestMax+')*AVERAGE(E8:G8),((1+'+ValueSuggestMax+')*AVERAGE(E8:G8)),IF(AND(G21>(1+'+ValueSuggestMax+')*AVERAGE(E8:G8),G21>'+OutstandingLevel+'*I8),((1+'+ValueSuggestMax+')*AVERAGE(E8:G8)),IF(H8-G38<'+ValueSuggestMin+'*H10,'+ValueSuggestMin+'*H10,H8-G38))))',  //Col H
            m7:'=IF(I8-H38=AVERAGE(F8:H8),I8, IF(I8-H38>(1+'+ValueSuggestMax+')*AVERAGE(F8:H8),((1+'+ValueSuggestMax+')*AVERAGE(F8:H8)),IF(AND(H21>(1+'+ValueSuggestMax+')*AVERAGE(F8:H8),H21>'+OutstandingLevel+'*J8),((1+'+ValueSuggestMax+')*AVERAGE(F8:H8)),IF(I8-H38<'+ValueSuggestMin+'*I10,'+ValueSuggestMin+'*I10,I8-H38))))',  //Col I
            m8:'=IF(J8-I38=AVERAGE(G8:I8),J8, IF(J8-I38>(1+'+ValueSuggestMax+')*AVERAGE(G8:I8),((1+'+ValueSuggestMax+')*AVERAGE(G8:I8)),IF(AND(I21>(1+'+ValueSuggestMax+')*AVERAGE(G8:I8),I21>'+OutstandingLevel+'*K8),((1+'+ValueSuggestMax+')*AVERAGE(G8:I8)),IF(J8-I38<'+ValueSuggestMin+'*J10,'+ValueSuggestMin+'*J10,J8-I38))))',  //Col J
            m9:'=IF(K8-J38=AVERAGE(H8:J8),K8, IF(K8-J38>(1+'+ValueSuggestMax+')*AVERAGE(H8:J8),((1+'+ValueSuggestMax+')*AVERAGE(H8:J8)),IF(AND(J21>(1+'+ValueSuggestMax+')*AVERAGE(H8:J8),J21>'+OutstandingLevel+'*L8),((1+'+ValueSuggestMax+')*AVERAGE(H8:J8)),IF(K8-J38<'+ValueSuggestMin+'*K10,'+ValueSuggestMin+'*K10,K8-J38))))',  //Col K
           m10:'=IF(L8-K38=AVERAGE(I8:K8),L8, IF(L8-K38>(1+'+ValueSuggestMax+')*AVERAGE(I8:K8),((1+'+ValueSuggestMax+')*AVERAGE(I8:K8)),IF(AND(K21>(1+'+ValueSuggestMax+')*AVERAGE(I8:K8),K21>'+OutstandingLevel+'*M8),((1+'+ValueSuggestMax+')*AVERAGE(I8:K8)),IF(L8-K38<'+ValueSuggestMin+'*L10,'+ValueSuggestMin+'*L10,L8-K38))))',  //Col L
           m11:'=IF(M8-L38=AVERAGE(J8:L8),M8, IF(M8-L38>(1+'+ValueSuggestMax+')*AVERAGE(J8:L8),((1+'+ValueSuggestMax+')*AVERAGE(J8:L8)),IF(AND(L21>(1+'+ValueSuggestMax+')*AVERAGE(J8:L8),L21>'+OutstandingLevel+'*N8),((1+'+ValueSuggestMax+')*AVERAGE(J8:L8)),IF(M8-L38<'+ValueSuggestMin+'*M10,'+ValueSuggestMin+'*M10,M8-L38))))',  //Col M
           m12:'=IF(N8-M38=AVERAGE(K8:M8),N8, IF(N8-M38>(1+'+ValueSuggestMax+')*AVERAGE(K8:M8),((1+'+ValueSuggestMax+')*AVERAGE(K8:M8)),IF(AND(M21>(1+'+ValueSuggestMax+')*AVERAGE(K8:M8),M21>'+OutstandingLevel+'*O8),((1+'+ValueSuggestMax+')*AVERAGE(K8:M8)),IF(N8-M38<'+ValueSuggestMin+'*N10,'+ValueSuggestMin+'*N10,N8-M38))))',  //Col N
           m13:'=N11',  //Col O ?? Dummy
        }
    )
    //----------------------------------------------------------
    // RS Adjustment Row 12 --> Input
    $scope.dataGrid.push(createDataRow('RS','Adjustment',RSAdjustment)); //Row 12; //RSAdjustment
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Blank Row 13
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 14
            title: 'WS',
            title2: 'Suggestion',
            m1:WSSuggestion[0],  //Col C
            m2:WSSuggestion[1],  //Col D
            m3:WSSuggestion[2],  //Col E
            m4:WSSuggestion[3],  //Col F
            m5: '=G12+('+StockRatioStd+'*H12)-F16', //Col G  --> RS Adjustment(12)+ ( StockRatioStd*RS Adjustment(12) ) - StockBaseOnSuggestion(16) ??? or - StockBaseOnAdjustment(18) ???
            m6: '=H12+('+StockRatioStd+'*I12)-G16', //Col H
            m7: '=I12+('+StockRatioStd+'*J12)-H16', //Col I
            m8: '=J12+('+StockRatioStd+'*K12)-I16', //Col J
            m9: '=K12+('+StockRatioStd+'*L12)-J16', //Col K
           m10: '=L12+('+StockRatioStd+'*M12)-K16', //Col L
           m11: '=M12+('+StockRatioStd+'*N12)-L16', //Col M
           m12: '=N12+('+StockRatioStd+'*O12)-M16', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 15
            title: 'WS',
            title2: 'Adjustment',
            m1:'=C14',  //Col C  --> =WS Suggestion
            m2:'=D14',  //Col D
            m3:'=E14',  //Col E
            m4:'=F14',  //Col F
            m5:'=G14',  //Col G
            m6:'=H14',  //Col H
            m7:'=I14',  //Col I
            m8:'=J14',  //Col J
            m9:'=K14',  //Col K
           m10:'=L14',  //Col L
           m11:'=M14',  //Col M
           m12:'=N14',  //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 16
            title: 'Stock based on Suggestion',
            m1:'='+StockLastMonth[0]+'+C$14-C$11', //Col C  --> StockLastMonth[0] + WS Suggestion(14) - RS Suggestion(11)
            m2:'=C16+D$14-D$11',  //Col D  --> StockBaseOnSuggestion[16] + WS Suggestion(14) - RS Suggestion(11)
            m3:'=D16+E$14-E$11',  //Col E
            m4:'=E16+F$14-F$11',  //Col F
            m5:'=F16+G$14-G$11',  //Col G
            m6:'=G16+H$14-H$11',  //Col H
            m7:'=H16+I$14-I$11',  //Col I
            m8:'=I16+J$14-J$11',  //Col J
            m9:'=J16+K$14-K$11',  //Col K
           m10:'=K16+L$14-L$11',  //Col L
           m11:'=L16+M$14-M$11',  //Col M
           m12:'=M16+N$14-N$11',  //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 17
            title: 'Stock Ratio',
            m1:'=C16/D$11',  //Col C  -> StockBaseOnSuggestion(16) / RS Suggestion(11)
            m2:'=D16/E$11',  //Col D
            m3:'=E16/F$11',  //Col E
            m4:'=F16/G$11',  //Col F
            m5:'=G16/H$11',  //Col G
            m6:'=H16/I$11',  //Col H
            m7:'=I16/J$11',  //Col I
            m8:'=J16/K$11',  //Col J
            m9:'=K16/L$11',  //Col K
           m10:'=L16/M$11',  //Col L
           m11:'=M16/N$11',  //Col M
           m12:'=N16/O$11',  //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 18
            title: 'Stock based on Adjustment',
            m1: '='+StockLastMonth[0]+'+C15-C12', //Col C  -> StockLastMonth[0] + WS Adjustment(15) - RS Adjustment(12)
            m2: '=C18+D15-D12', //Col D  -> StockBaseOnAdjustment(18) + WS Adjustment(15) - RS Adjustment(12)
            m3: '=D18+E15-E12', //Col E
            m4: '=E18+F15-F12', //Col F
            m5: '=F18+G15-G12', //Col G
            m6: '=G18+H15-H12', //Col H
            m7: '=H18+I15-I12', //Col I
            m8: '=I18+J15-J12', //Col J
            m9: '=J18+K15-K12', //Col K
           m10: '=K18+L15-L12', //Col L
           m11: '=L18+M15-M12', //Col M
           m12: '=M18+N15-N12', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 19
            title: 'Stock Ratio',
            m1:'=C18/D12',  //Col C -> StockBaseOnAdjustment(18) / RS Adjustment(12)
            m2:'=D18/E12',  //Col D
            m3:'=E18/F12',  //Col E
            m4:'=F18/G12',  //Col F
            m5:'=G18/H12',  //Col G
            m6:'=H18/I12',  //Col H
            m7:'=I18/J12',  //Col I
            m8:'=J18/K12',  //Col J
            m9:'=K18/L12',  //Col K
           m10:'=L18/M12',  //Col L
           m11:'=M18/N12',  //Col M
           m12:'=N18/O12',  //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Row 20
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 21
            title: 'O/S',
            title2: 'Unit',
            m1:OutstandingLastMonthUnit[0], //Col C
            m2:OutstandingLastMonthUnit[1], //Col D
            m3:'=D21+E23-E12-E24',  //Col E  //OS Unit(21) + SPK Suggestion(23) - RS Adjustment(12) - C/L Unit(24)
            m4:'=E21+F23-F12-F24',  //Col F
            m5:'=F21+G23-G12-G24',  //Col G
            m6:'=G21+H23-H12-H24',  //Col H
            m7:'=H21+I23-I12-I24',  //Col I
            m8:'=I21+J23-J12-J24',  //Col J
            m9:'=J21+K23-K12-K24',  //Col K
           m10:'=K21+L23-L12-L24',  //Col L
           m11:'=L21+M23-M12-M24',  //Col M
           m12:'=M21+N23-N12-N24',  //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 22
            title: 'O/S',
            title2: 'Ratio',
            m1:"=C21/D12",    //OS Unit(21) / RS Adjustment(12)
            m2:"=D21/E12",
            m3:"=E21/F12",
            m4:"=F21/G12",
            m5:"=G21/H12",
            m6:"=H21/I12",
            m7:"=I21/J12",
            m8:"=J21/K12",
            m9:"=K21/L12",
           m10:"=L21/M12",
           m11:"=M21/N12",
           m12:"=n21/O12",
        }
    );
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 23
            title: 'SPK',
            title2: 'Suggestion',
            m1: ActualSPK[0], //Col C
            m2: ActualSPK[1], //Col D
            m3: '=E$12+E$24+F$8-D$21', //Col E Est. //-> RS Adjustment(12) + C/L Unit(24) + RS Target Market(8) - O/S Unit(21)
            m4: '=F$12+F$24+G$8-E$21', //Col F Fix
            m5: '=G12+G24+ROUNDUP(AVERAGE(D21:F21),0)-F21', //Col G Flexible //-> RS Adjustment(12) + C/L Unit(24) + Average(O/S Unit(21) 3 Month) - O/S Unit(21)
            m6: '=H12+H24+ROUNDUP(AVERAGE(E21:G21),0)-G21', //Col H
            m7: '=I12+I24+ROUNDUP(AVERAGE(F21:H21),0)-H21', //Col I
            m8: '=J12+J24+ROUNDUP(AVERAGE(G21:I21),0)-I21', //Col J
            m9: '=K12+K24+ROUNDUP(AVERAGE(H21:J21),0)-J21', //Col K
           m10: '=L12+L24+ROUNDUP(AVERAGE(I21:K21),0)-K21', //Col L
           m11: '=M12+M24+ROUNDUP(AVERAGE(J21:L21),0)-L21', //Col M
           m12: '=N12+N24+ROUNDUP(AVERAGE(K21:M21),0)-M21', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 24
            title: 'C/L',
            title2: 'Unit',
            m1: ActualSPKCancel[0], //Col C
            m2: ActualSPKCancel[1], //Col D
            m3: '=ROUNDUP(AVERAGE(C25:D25)* (AVERAGE(C23:D23)+AVERAGE(C21:D21)),0)', //Col E //-> Avg(C/L Ratio(25) 3 Month) * Avg(SPK Suggestion(23) 3 Month) + Avg(O/S Unit(21) 3 Mo)
            m4: '=ROUNDUP(AVERAGE(C25:E25)* (AVERAGE(C23:E23)+AVERAGE(C21:E21)),0)', //Col F
            m5: '=ROUNDUP(AVERAGE(D25:F25)* (AVERAGE(D23:F23)+AVERAGE(D21:F21)),0)', //Col G
            m6: '=ROUNDUP(AVERAGE(E25:G25)* (AVERAGE(E23:G23)+AVERAGE(E21:G21)),0)', //Col H
            m7: '=ROUNDUP(AVERAGE(F25:H25)* (AVERAGE(F23:H23)+AVERAGE(F21:H21)),0)', //Col I
            m8: '=ROUNDUP(AVERAGE(G25:I25)* (AVERAGE(G23:I23)+AVERAGE(G21:I21)),0)', //Col J
            m9: '=ROUNDUP(AVERAGE(H25:J25)* (AVERAGE(H23:J23)+AVERAGE(H21:J21)),0)', //Col K
           m10: '=ROUNDUP(AVERAGE(I25:K25)* (AVERAGE(I23:K23)+AVERAGE(I21:K21)),0)', //Col L
           m11: '=ROUNDUP(AVERAGE(J25:L25)* (AVERAGE(J23:L23)+AVERAGE(J21:L21)),0)', //Col M
           m12: '=ROUNDUP(AVERAGE(K25:M25)* (AVERAGE(K23:M23)+AVERAGE(K21:M21)),0)', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
         { //Row 25
            title: 'C/L',
            title2: 'Ratio',
            m1: '=C$24/(C$23+'+OutstandingLastMonth[0]+')', //Col C //-> C/L Unit(24) / ( SPK Suggestion(23) + O/S Unit(21)
            m2: '=D$24/(D$23+C$21)', //Col D
            m3: '=E$24/(E$23+D$21)', //Col E
            m4: '=F$24/(F$23+E$21)', //Col F
            m5: '=G$24/(G$23+F$21)', //Col G
            m6: '=H$24/(H$23+G$21)', //Col H
            m7: '=I$24/(I$23+H$21)', //Col I
            m8: '=J$24/(J$23+I$21)', //Col J
            m9: '=K$24/(K$23+J$21)', //Col K
           m10: '=L$24/(L$23+K$21)', //Col L
           m11: '=M$24/(M$23+L$21)', //Col M
           m12: '=N$24/(N$23+M$21)', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Row 26
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 27
            title:"Reference LM Displan",
            m3:8,
            m4:8,
            m5:8,
            m6:8,
            m7:8,
        }
    );
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 28
            title:"Stock based on LM Displan",
            m3:4,
            m4:2,
            m5:-6,
            m6:3,
            m7:-5,
        }
    );
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 29
            title:"Stock Ratio",
            m3:0.4,
            m4:0.11,
            m5:-0.6,
            m6:0.3,
            m7:-0.5,
        }
    );
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 30
            title:"Reference LM RSSP",
            m3:15,
            m4:15,
            m5:15,
            m6:15,
            m7:15,
        }
    );
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Row 31
    //----------------------------------------------------------
    $scope.dataGrid.push({title:"Gap vs RS Target OAP/RAP"}); //Row 32
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 33
            title: 'Monthly Gap',
            m1:'=C12-C10', //Col C  --> RS Adjustment(12) - RS Target OAP/RAP(10)
            m2:'=D12-D10', //Col D
            m3:'=E12-E10', //Col E
            m4:'=F12-F10', //Col F
            m5:'=G12-G10', //Col G
            m6:'=H12-H10', //Col H
            m7:'=I12-I10', //Col I
            m8:'=J12-J10', //Col J
            m9:'=K12-K10', //Col K
           m10:'=L12-L10', //Col L
           m11:'=M12-M10', //Col M
           m12:'=N12-N10', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 34
            title: 'Accumulation Gap',
            m1:'=C33',     //Col C  --> Monthly Gap(33)
            m2:'=C34+D33', //Col D  --> AccumationGap(34) + Monthly Gap(33)
            m3:'=D34+E33', //Col E
            m4:'=E34+F33', //Col F
            m5:'=F34+G33', //Col G
            m6:'=G34+H33', //Col H
            m7:'=H34+I33', //Col I
            m8:'=I34+J33', //Col J
            m9:'=J34+K33', //Col K
           m10:'=K34+L33', //Col L
           m11:'=L34+M33', //Col M
           m12:'=M34+N33', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push({}); //Row 35
    //--------------------------------------------------------------
    $scope.dataGrid.push({title:"Gap vs RS Target Market"}); //Row 36
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 37
            title: 'Monthly Gap',
            m1:'=C12-C8', //Col C --> RS Adjustment(12) - RS Target Market (8)
            m2:'=D12-D8', //Col D
            m3:'=E12-E8', //Col E
            m4:'=F12-F8', //Col F
            m5:'=G12-G8', //Col G
            m6:'=H12-H8', //Col H
            m7:'=I12-I8', //Col I
            m8:'=J12-J8', //Col J
            m9:'=K12-K8', //Col K
           m10:'=L12-L8', //Col L
           m11:'=M12-M8', //Col M
           m12:'=N12-N8', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 38
            title: 'Accumulation Gap',
            m1:'=C37',     //Col C  --> Monthly Gap(37)
            m2:'=C38+D37', //Col D  --> AccumulationGap(38) + Monthly Gap(37)
            m3:'=D38+E37', //Col E
            m4:'=E38+F37', //Col F
            m5:'=F38+G37', //Col G
            m6:'=G38+H37', //Col H
            m7:'=H38+I37', //Col I
            m8:'=I38+J37', //Col J
            m9:'=J38+K37', //Col K
           m10:'=K38+L37', //Col L
           m11:'=L38+M37', //Col M
           m12:'=M38+N37', //Col N
        }
    )
    //--------------------------------------------------------------
    $scope.dataGrid.push({}); //Row 39
    //--------------------------------------------------------------
    $scope.dataGrid.push({title:"Gap vs RS Target Outlet Share"}); //Row 40
    //--------------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 41
            title: 'Monthly Gap',
            m1:'=C23-C10', //Col C --> RS Adjustment(23) - RS Target Market (10)
            m2:'=D23-D10', //Col D
            m3:'=E23-E10', //Col E
            m4:'=F23-F10', //Col F
            m5:'=G23-G10', //Col G
            m6:'=H23-H10', //Col H
            m7:'=I23-I10', //Col I
            m8:'=J23-J10', //Col J
            m9:'=K23-K10', //Col K
           m10:'=L23-L10', //Col L
           m11:'=M23-M10', //Col M
           m12:'=N23-N10', //Col N
        }
    )
    //----------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 42
            title: 'Accumulation Gap',
            m1:'=C46',     //Col C
            m2:'=C47+D46', //Col D
            m3:'=D47+E46', //Col E
            m4:'=E47+F46', //Col F
            m5:'=F47+G46', //Col G
            m6:'=G47+H46', //Col H
            m7:'=H47+I46', //Col I
            m8:'=I47+J46', //Col J
            m9:'=J47+K46', //Col K
           m10:'=K47+L46', //Col L
           m11:'=L47+M46', //Col M
           m12:'=M47+N46', //Col N
        }
    )
    //------------------------------------------------------------
    // Combine dataTitle with dataGrid
    $scope.data = $scope.dataTitle.concat($scope.dataGrid);
    // console.log("data=>",$scope.data);
    //------------------------------------------------------------
    //------------------------------------------------------------
    // $scope.dataGrid.push(
    //     $scope.dataTitle2[4] //Row 49 - Title Only
    // )
    // $scope.dataGrid.push(
    //     $scope.dataTitle2[5] //Row 50 - Title Only
    // )
    // $scope.dataGrid.push(
    //     { //Row 51
    //         title: 'Market',
    //         m1:'=C6',     //Col C
    //         m2:'=C51+D6', //Col D
    //         m3:'=D51+E6', //Col E
    //         m4:'=E51+F6', //Col F
    //         m5:'=F51+G6', //Col G
    //         m6:'=G51+H6', //Col H
    //         m7:'=H51+I6', //Col I
    //         m8:'=I51+J6', //Col J
    //         m9:'=J51+K6', //Col K
    //        m10:'=K51+L6', //Col L
    //        m11:'=L51+M6', //Col M
    //        m12:'=M51+N6', //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 52
    //         title: 'Polreg',
    //         m1:PolregOutlet[0],     //Col C
    //         m2:'=C52+'+PolregOutlet[1], //Col D
    //         m3:'=D52+'+PolregOutlet[2], //Col E
    //         m4:'=E52+'+PolregOutlet[3], //Col F
    //         m5:'=F52+'+PolregOutlet[4], //Col G
    //         m6:'=G52+'+PolregOutlet[5], //Col H
    //         m7:'=H52+'+PolregOutlet[6], //Col I
    //         m8:'=I52+'+PolregOutlet[7], //Col J
    //         m9:'=J52+'+PolregOutlet[8], //Col K
    //        m10:'=K52+'+PolregOutlet[9], //Col L
    //        m11:'=L52+'+PolregOutlet[10], //Col M
    //        m12:'=M52+'+PolregOutlet[11], //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 53
    //         title: 'Outlet Share',
    //         m1:'=C52/C51',  //Col C
    //         m2:'=D52/D51',  //Col D
    //         m3:'=E52/E51',  //Col E
    //         m4:'=F52/F51',  //Col F
    //         m5:'=G52/G51',  //Col G
    //         m6:'=H52/H51',  //Col H
    //         m7:'=I52/I51',  //Col I
    //         m8:'=J52/J51',  //Col J
    //         m9:'=K52/K51',  //Col K
    //        m10:'=L52/L51',  //Col L
    //        m11:'=M52/M51',  //Col M
    //        m12:'=N52/N51',  //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 54
    //         title: 'Outlet Share Gap',
    //         m1:'=C53-'+OutletShareTarget,  //Col C
    //         m2:'=D53-'+OutletShareTarget,  //Col D
    //         m3:'=E53-'+OutletShareTarget,  //Col E
    //         m4:'=F53-'+OutletShareTarget,  //Col F
    //         m5:'=G53-'+OutletShareTarget,  //Col G
    //         m6:'=H53-'+OutletShareTarget,  //Col H
    //         m7:'=I53-'+OutletShareTarget,  //Col I
    //         m8:'=J53-'+OutletShareTarget,  //Col J
    //         m9:'=K53-'+OutletShareTarget,  //Col K
    //        m10:'=L53-'+OutletShareTarget,  //Col L
    //        m11:'=M53-'+OutletShareTarget,  //Col M
    //        m12:'=N53-'+OutletShareTarget,  //Col N
    //     }
    // )
});
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    // function createDataRow(title,title2,arr1,arr2){
    //     var res={};
    //     res.title=title;res.title2=title2;
    //     for(var i=0;i<arr1.length;i++){
    //         res['m'+(i+1)] = arr1[i];
    //     }
    //     if(arr2){
    //         for(var j=0;j<arr2.length;j++){
    //             res['m'+(j+arr1.length+1)] = arr2[j];
    //         }
    //     }
    //     // console.log("res=>",res);
    //     return res;
    // }
    // function createDataRowFormula(title,title2,c1,c1inc,r1,r1inc,anchor1,op,c2,c2inc,r2,r2inc,anchor2){
    //     var res={};
    //     res.title=title;res.title2=title2;
    //     for(var i=0;i<12;i++){

    //          var f1 = '='+String.fromCharCode(c1+64)+(anchor1?'$':'')+r1;
    //          var f2 = String.fromCharCode(c2+64)+(anchor2?'$':'')+r2;
    //          res['m'+(i+1)] = f1 + op + f2;
    //          if(c1inc) c1++;
    //          if(r1inc) r1++;
    //          if(c2inc) c2++;
    //          if(r2inc) r2++;
    //     }
    //     // console.log("res=>",res);
    //     return res;
    // }
    // function createDataRowFormulaX(title,title2,arr1,cell1,cell2,op,inc_c1,inc_r1,inc_c2,inc_r2){
    //     function _splitCell(cell){
    //         var useS=false;
    //         var c=cell.substring(0,1);
    //         var s=cell.substring(1,2);
    //         var r='';
    //         if(s=='$') {
    //             useS=true;
    //             r=cell.substring(2,4);
    //         }else{
    //             r=cell.substring(1,3);
    //         }
    //         return({c:c,r:r,s:(useS?'$':'')});
    //     }
    //     function _incCol(col){
    //         var c=col.charCodeAt(0);
    //         c++;
    //         return String.fromCharCode(c);
    //     }
    //     var res={};
    //     res.title=title;res.title2=title2;
    //     var cl1 = _splitCell(cell1);
    //     var cl2 = _splitCell(cell2);
    //     // console.log(cl1,cl2);
    //     var arrLen = 0;
    //     if(arr1!=null){
    //         arrLen = arr1.length;
    //         for(var j=0;j<arrLen;j++){
    //             res['m'+(j+1)] = arr1[j];
    //         }
    //     }
    //     for(var i=0;i<12-arrLen;i++){
    //          var f1 = '='+cl1.c + cl1.s + cl1.r;
    //          var f2 = cl2.c + cl2.s + cl2.r;
    //          res['m'+(i+1+arrLen)] = f1 + op + f2;
    //          if(inc_c1) cl1.c = _incCol(cl1.c);
    //          if(inc_r1) cl1.r++;
    //          if(inc_c2) cl2.c = _incCol(cl2.c);
    //          if(inc_r2) cl2.r++;
    //     }
    //     // var res = _splitCell(cell1);
    //     // console.log("res=>",res);
    //     return res;
    // }
    //----------------------------------
    // $scope.titleArr2 = [
    //                         ['RS Adjustment vs RS Target OAP/RAP','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
    //                         ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    //                         ['RS Adjustment vs RS Target Market','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
    //                         ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    //                         ['Cummulative','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
    //                         ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    // ];
    // $scope.dataTitle2 = [];
    // for(var i=0;i<$scope.titleArr2.length;i++){
    //     var titleList=$scope.titleArr2[i];
    //     var row={};
    //     row.title = titleList[0];
    //     row.title2 = titleList[1];
    //     row.m1 = titleList[2];
    //     row.m2 = titleList[3];
    //     row.m3 = titleList[4];
    //     row.m4 = titleList[5];
    //     row.m5 = titleList[6];
    //     row.m6 = titleList[7];
    //     row.m7 = titleList[8];
    //     row.m8 = titleList[9];
    //     row.m9 = titleList[10];
    //     row.m10 = titleList[11];
    //     row.m11 = titleList[12];
    //     row.m12 = titleList[13];
    //     row.m13 = '';
    //     $scope.dataTitle2.push(row);
    // }