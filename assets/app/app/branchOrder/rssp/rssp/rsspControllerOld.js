angular.module('app')
    .controller('RSSPControllerOld', function($scope, $http, CurrentUser, GetsudoPeriod, OrgChart, RSSP, RSSPTemplate, ProductModel, hotRegisterer, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.mRSSP = null; //Model
    $scope.xRSSP = {};
    $scope.xRSSP.selected=[];

    $scope.filter = {periodId : null, orgId : null};
    $scope.selectedProductModel = {};
    $scope.dataModel = [{}];
    $scope.showComposition = false;
    $scope.historyDataOpt = [
                        {name:"3 bulan terakhir",value:"0"},
                        {name: "Tentukan lebih spesifik",value: "1"}
                    ];
    $scope.currentMonth = null;
    $scope.totalSPK = 16;
    $scope.avgSPK = 5.33;
    $scope.totalOS = 16;
    $scope.avgOS = 5.33;
    $scope.totalRS = 16;
    $scope.avgRS = 5.33;
    $scope.totalCL = 16;
    $scope.avgCL = 5.33;
    $scope.visibility = {market:1, spk:1, reference:1};
    $scope.alerts = [];
    $scope.animationsEnabled = true;

    var modelTemplate = {};
    var typeColorTemplate = [];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        if($scope.filter.orgId!=null & $scope.filter.periodId!=null)
            RSSP.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data;
                    $scope.loading=false;
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
    }
    $scope.onShowDetail = function(row){
        if(!(row.productModelId in modelTemplate)){
            RSSPTemplate.findByModel(row.productModelId).then(
                function(res){
                    modelTemplate[row.productModelId] = res.data;
                    $scope.renderHOTVarStructure(res.data);
                }
            );
        }else{
            $scope.renderHOTVarStructure(modelTemplate[row.productModelId]);
        }
        $scope.selectedProductModel = _.find($scope.productModelData, { 'id': row.productModelId });
    };
    $scope.changeComposition = function(size){
        $scope.showComposition = true;
    };
    GetsudoPeriod.getData().then(
        function(res){
            $scope.periodData = res.data;
            $scope.loading=false;
            return res.data;
        }
    );
    OrgChart.getDataByRoleUser($scope.user).then(
        function(res){
            $scope.orgData = res.data;
            $scope.loading=false;
            return res.data;
        }
    );
    ProductModel.getData().then(
        function(res){
            $scope.productModelData = res.data;
            $scope.loading=false;
            return res.data;
        }
    );
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', visible:false },
            { name:'Model', field: 'modelName' },
            { name:'Status', field: 'status' },
            { name:'N', width:50, field: 'm' },
            { name:'N+1', width:50, field: 'm1' },
            { name:'N+2', width:50, field: 'm2' },
            { name:'N+3', width:50, field: 'm3' },
            { name:'N+4', width:50, field: 'm4' },
            { name:'N+5', width:50, field: 'm5' },
            { name:'N+6', width:50, field: 'm6' },
            { name:'N+7', width:50, field: 'm7' },
            { name:'N+8', width:50, field: 'm8' },
            { name:'N+9', width:50, field: 'm9' },
            { name:'Total', field: 'total' },
            { name:'Komentar', field: 'comment' },
            { name:'Status HO', field: 'hoReview' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };

    $scope.selectPeriod = function(selected){
        $scope.currentMonth = Date.parse(selected.name);
        $scope.getData();
    }
    $scope.selectOutlet = function(selected){
        $scope.filter.orgId = selected.id;
        $scope.getData();
    }
    $scope.showHideData = function(){
        var visibilityArr = ['Always', 'Market', 'SPK', 'Reference', 'Hide'];
        var hiddenRows = [];
        console.log($scope.visibility);
        for(var i in $scope.dataModel){
            if(($scope.dataModel[i].visibility==1) && ($scope.visibility.market==0)){
                hiddenRows.push(i);
            }else if(($scope.dataModel[i].visibility==2) && ($scope.visibility.spk==0)){
                hiddenRows.push(i);
            }else if(($scope.dataModel[i].visibility==3) && ($scope.visibility.reference==0)){
                hiddenRows.push(i);
            }else if($scope.dataModel[i].visibility==4){
                hiddenRows.push(i);
            }
        }
        console.log(hiddenRows);
        $scope.hotRSSPModel = hotRegisterer.getInstance('hotRSSPModel');
        $scope.hotRSSPModel.updateSettings({
            hiddenRows: {
                copyPasteEnabled: true,
                indicators: false,
                rows: hiddenRows
            }
        });
    }
    $scope.renderHOTVarStructure = function(data){
        function _renderHOT(data){
            var lastPrnId = 0;
            var mergeCells = [];
            var cell = [];
            var dataInto = [];
            var hiddenRows = [];
            var isValidationRow = 0;
            for(var i in data){
                var n=parseInt(i);
                if(data[i].varPrnId==null || data[i].varPrnId=='' || data[i].varPrnId==undefined){
                    data[i].varPrnName = data[i].varName;
                    mergeCells.push({row: n, col: 0, rowspan: 1, colspan: 2});
                }else{
                    if(data[i].varPrnName.length>5 && lastPrnId!=data[i].varPrnId){
                        for(ctr=n; ctr<data.length; ctr++){
                            if(data[ctr].varPrnId==data[i].varPrnId){
                                data[ctr].varPrnName = data[ctr].varName;
                                mergeCells.push({row: ctr, col: 0, rowspan: 1, colspan: 2});                            
                            }
                        }
                        lastPrnId = data[i].varPrnId;
                    }else if(data[i].varPrnName.length<6 && lastPrnId!=data[i].varPrnId){
                        var ctr=n;
                        for(; ctr<data.length; ctr++){
                            if(data[ctr].varPrnId!=data[i].varPrnId)
                                break;
                        }
                        mergeCells.push({row: n, col: 0, rowspan: ctr-n, colspan: 1});
                        cell.push({row: n, col: 0, className: "htMiddle"});
                    }
                }
                if(data[i].visibility == 4){
                    hiddenRows.push(n);
                }
                if(data[i].varTypeId == 3){
                    isValidationRow = n;
                }
                dataInto.push(data[i]);
            }
            return {data:dataInto, mergeCells:mergeCells, cell:cell, hiddenRows:hiddenRows, validationRow:isValidationRow};
        }
        function _updateSettings(instanceName, settings, dataScope){
            $scope[instanceName] = hotRegisterer.getInstance(instanceName);
            if(dataScope){
                $scope[dataScope] = settings.data;
                $scope[instanceName].updateSettings({data:$scope[dataScope]});
            }
            settings.height = (settings.data.length+2) * 25;
            settings.cells = function (row, col, prop) {
                var cellProperties = {};
                if ($scope[instanceName].getDataAtRowProp(row, 'varTypeId') == 1 && col>15) {
                    cellProperties.editor = 'numeric';
                    cellProperties.className = 'editingCell';
                } else {
                    cellProperties.editor = false;
                }
                var varId = $scope[instanceName].getDataAtRowProp(row, 'rsspVarId');
                if(varId == '' || varId == null){
                    cellProperties.className = 'emptyRow';
                }
                return cellProperties;
            };
            settings.hiddenRows = {
                copyPasteEnabled: true,
                indicators: false,
                rows: settings.hiddenRows
            };
            $scope[instanceName].updateSettings(settings);
            Handsontable.hooks.add('afterChange', function(){
                $scope.alerts = [];
                for(var i=16; i<22; i++){
                    var validationValue = $scope[instanceName].getDataAtRowProp(settings.validationRow, $scope.hotModelSettings.columns[i].data);
                    if(validationValue!='Yes' && validationValue!='' && validationValue!=null){
                        if($scope.alerts.indexOf(validationValue)<0)
                            $scope.alerts.push(validationValue);
                    }
                }
                if($scope.alerts.length>0)
                    $scope.showAlert($scope.alerts.join('\n'));
            }, $scope[instanceName]);
        }
        var splittedArr = _.partition(data, {modeltypecolor:1});
        var renderObjModel = _renderHOT(splittedArr[0]);
        console.log(renderObjModel);
        $scope.dataModel = renderObjModel.data;
        _updateSettings('hotRSSPModel',renderObjModel);

        var renderObjType = _renderHOT(splittedArr[1]);
        angular.forEach($scope.selectedProductModel.child, function(value, key) {
            var instanceName = 'hotRSSPType-'+value.id;
            _updateSettings(instanceName,renderObjType,'data'+instanceName);
            angular.forEach(value.child, function(v, k){
                var instanceName = 'hotRSSPTypeColor-'+v.id;
                _updateSettings(instanceName,renderObjType,'data'+instanceName);
            });
        });
    }
    $scope.showAlert = function(message){
        // $.smallBox({
        //     title: message,
        //     content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
        //     color: "#296191",
        //     iconSmall: "fa fa-thumbs-up bounce animated",
        //     timeout: 4000
        // });
        $.bigBox({
            title: 'Entry salah',
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            number: "3"
        });
    }

    $scope.hotModelSettings = {
        colWidths: [50,180,100,110,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40],
        nestedHeaders: [
            // ['','','','','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','',''],
            ['','','','','','','','','','','','','','',{label:'Data Histori', colspan:2},'Est',{label:'Periode Pemesanan',colspan:5},{label:'Rencana OAP/RAP',colspan:7},'','','',''],
            [{label:'Nama Variabel', colspan:2}, 'Tipe', 'Tampilan', 'N-12', 'N-11', 'N-10', 'N-9', 'N-8', 'N-7', 'N-6', 'N-5', 'N-4', 'N-3', 'N-2', 'N-1', 'N', 'N+1', 'N+2', 'N+3', 'N+4', 'N+5', 'N+6', 'N+7', 'N+8', 'N+9', 'N+10', 'N+11', 'N+12', 'ID', 'PRNID', '', '']
        ],
        fixedColumnsLeft:2,
        height: 900,
        width: angular.element(document.getElementById('rsspModelContainer')).clientWidth,
        hiddenColumns: {
          columns: [2,3,4,5,6,7,8,9,10,11,12,13,26,27,28,29,30,31,32],
          indicators: false
        },
        columns: [
                  { data: 'varPrnName', type: 'text', editor: false},
                  { data: 'varName', type: 'text', editor: false},
                  { data: 'typeName', type: 'text', editor: false},
                  { data: 'visibility', type: 'numeric'},
                  { data: 'm_12' ,type: 'numeric'},
                  { data: 'm_11' ,type: 'numeric'},
                  { data: 'm_10' ,type: 'numeric'},
                  { data: 'm_9' ,type: 'numeric'},
                  { data: 'm_8' ,type: 'numeric'},
                  { data: 'm_7' ,type: 'numeric'},
                  { data: 'm_6' ,type: 'numeric'},
                  { data: 'm_5' ,type: 'numeric'},
                  { data: 'm_4' ,type: 'numeric'},
                  { data: 'm_3' ,type: 'numeric', editor: false},
                  { data: 'm_2' ,type: 'numeric', editor: false},
                  { data: 'm_1' ,type: 'numeric', editor: false},
                  { data: 'm' ,type: 'numeric'},
                  { data: 'm1' ,type: 'numeric'},
                  { data: 'm2' ,type: 'numeric'},
                  { data: 'm3' ,type: 'numeric'},
                  { data: 'm4' ,type: 'numeric'},
                  { data: 'm5' ,type: 'numeric'},
                  { data: 'm6' ,type: 'numeric'},
                  { data: 'm7' ,type: 'numeric'},
                  { data: 'm8' ,type: 'numeric'},
                  { data: 'm9' ,type: 'numeric'},
                  { data: 'm10' ,type: 'numeric'},
                  { data: 'm11' ,type: 'numeric'},
                  { data: 'm12' ,type: 'numeric'},
                  { data: 'rsspVarId', type: 'numeric', editor: false},
                  { data: 'varPrnId', type: 'numeric', editor: false},
                  { data: 'varTypeId', type: 'numeric'},
                  { data: 'varFormatId', type: 'numeric'}
        ],
    };
    $scope.hotTypeSettings = angular.copy($scope.hotModelSettings);
    $scope.hotTypeSettings.height = 300;
    $scope.hotSumSettings = {
        colWidths: [80,40,40,40,40,40],
        colHeaders: ['Type Color','N+1','N+2','N+3','N+4','N+5'],
        fixedColumnsLeft:1,
        height: 400,
        width: angular.element(document.getElementById('rsspSumContainer')).clientWidth,
        columns: [
                  { data: 'productTypeName', type: 'text', editor: false},
                  { data: 'm1' ,type: 'numeric', editor: false},
                  { data: 'm2' ,type: 'numeric', editor: false},
                  { data: 'm3' ,type: 'numeric', editor: false},
                  { data: 'm4' ,type: 'numeric', editor: false},
                  { data: 'm5' ,type: 'numeric', editor: false}
        ],
    };
    $scope.historiDataSetting = {
        colWidths: [40],
        colHeaders: ['Pilih','Periode','Total'],
        height: 500,
        width: angular.element(document.getElementById('optSPK')).clientWidth,
        columns: [
                  { data: 'sel', type: 'checkbox'},
                  { data: 'period' ,type: 'text', editor: false},
                  { data: 'total' ,type: 'numeric', editor: false},
        ],
    };
});