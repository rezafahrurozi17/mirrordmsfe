angular.module('app')
  .factory('RSSP', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        return $http.get('/api/ds/RSSPs/?start=1&limit=100&GetsudoPeriodId='+filter.GetsudoPeriodId+'&OutletId='+filter.orgId);
      },
      getDetail: function(filter) {
        return $http.get('/api/ds/RSSPTotalModelAdjustments?GetsudoPeriodId='+filter.GetsudoPeriodId+'&OutletId='+filter.OutletId+'&VehicleModelId='+filter.VehicleModelId);
      },
      updateDetail: function(data){
        return $http.put('/api/ds/RSSPTotalModelAdjustments/Multiple', data);
      },
      getDetailTypeColor: function(filter) {
        return $http.get('/api/ds/RSSPDetailAdjustments?GetsudoPeriodId='+filter.GetsudoPeriodId+'&OutletId='+filter.OutletId+'&VehicleModelId='+filter.VehicleModelId);
      },
      updateTypeColor: function(rssp){
        return $http.put('/api/ds/RSSPDetailAdjustments/Multiple', rssp);
      },
      getComposition: function(filter){
        return $http.get('/api/ds/ProcessIncludedPeriods?start=1&limit=100&GetsudoPeriodId='+filter.GetsudoPeriodId+'&OutletId='+filter.OutletId);
      },
      postReview: function(review){
        return $http.put('/api/ds/RSSPs/Review/Multiple',review);
      },
      getComment: function(NId){
        return $http.get('/api/ds/RSSPTotalModelComments/'+NId);
      },
      postComment: function(comment){
        return $http.post('/api/ds/RSSPTotalModelComments/Multiple',[comment]);
      }
    }
  });