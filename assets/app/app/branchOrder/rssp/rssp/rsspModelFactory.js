angular.module('app')
  .factory('RSSPModel', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/branch_order/customerfleet');
        //console.log('res=>',res);
        return res;
      },
      create: function(fleet) {
        return $http.post('/branch_order/customerfleet/create', {
                                            code: fleet.code,
                                            name: fleet.name,
                                            desc: fleet.desc});
      },
      update: function(fleet){
        return $http.post('/branch_order/customerfleet/update', {
                                            id: fleet.id,
                                            code: fleet.code,
                                            name: fleet.name,
                                            desc: fleet.desc});
      },
      delete: function(id) {
        return $http.post('/branch_order/customerfleet/delete',{id:id});
      },
    }
  });