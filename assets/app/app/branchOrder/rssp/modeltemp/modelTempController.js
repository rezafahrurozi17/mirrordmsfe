angular.module('app')
    .controller('ModelTemplateController', function($scope, $http, CurrentUser, ModelTemplate, RSSPTemplate, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mModelTemp = null; //Model
    $scope.xModelTemp = {};
    $scope.xModelTemp.selected=[];
    $scope.template = [];
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        ModelTemplate.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    RSSPTemplate.getData().then(
        function(res){
            $scope.template = res.data.Result;
        }
    );
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'id', field:'Id', visible:false},
            { name:'rsspTempHdrId', field:'HeaderFormulaId', visible:false },
            { name:'rsspTempHdrId2', field:'HeaderFormulaDetailId', visible:false },
            { name:'Model Name', width:'20%', field: 'VehicleModelName' },
            { name:'Model Template', field:'Name'},
            { name:'Type Color Template', field: 'NameDetail' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };

});
