angular.module('app')
  .factory('ModelTemplate', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/RSSPHeaderFormulaTotalModels/?start=1&limit=100');
        return res;
      },
      update: function(obj){
        return $http.put('/api/ds/RSSPHeaderFormulaTotalModels/Multiple', [obj]);
      },
    }
  });