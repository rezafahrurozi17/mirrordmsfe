angular.module('app')
    .controller('RSSPFleetController', function($scope, $http, uiGridConstants, CurrentUser, OrgChart, GetsudoPeriod, ProductModel, CustomerFleet, RSSPFleet, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRSSPFleet = {}; //Model
    $scope.modRSSPFleet = {};
    $scope.xRSSPFleet = {};
    $scope.xRSSPFleet.selected=[];
    $scope.selectedModel = {};
    $scope.selectedType = {};

    $scope.tempRSSP = {selCustomerFleetId:null, selOutletId:null, selVehicleModelId:null, selVehicleTypeId:null, selColorId:null};

    // $scope.filter = {period:null, CustomerCode:null, OutletId:$scope.user.orgId, VehicleModelName:null, productType:null, productColor:null};
    $scope.filter = {period:null, CustomerFleetId:null, CustomerCode:null, CustomerName:null, OutletId:null, OutletCode:null, VehicleModelName:null, productType:null, productColor:null};
    $scope.periodActive = false;
    var mDataCopy;

    // $scope.orgId = null;
    var mode = 0;
    var addMode = 2;
    var editMode = 1;
    var viewMode = 0;

    $scope.hideNewButton = false;

    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    var editableMode = false;

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;

            $scope.customActionSettings[0].visible = mode==editMode || mode==addMode;
            $scope.customActionSettings[1].visible = mode==editMode || mode==addMode;
            $scope.customActionSettings[2].visible = mode==addMode;
            $scope.customActionSettings[3].visible = mode==addMode;
            $scope.customActionSettings[4].visible = ($scope.grid.data.length>0) && (mode == viewMode) && ($scope.periodActive);
            $scope.grid.enableCellEditOnFocus = mode=editMode;
            mode = mode;
        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
                $scope.mRSSPFleet = angular.copy(mDataCopy);
                $scope.hideNewButton=false;
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                // $scope.mRSSPFleet.detail = $scope.grid.data;
                var updateDt = [];
                for (var i in $scope.mRSSPFleet) {
                    if ($scope.mRSSPFleet[i]!==null && $scope.mRSSPFleet[i]!==undefined && !angular.equals($scope.mRSSPFleet[i], {})) {
                        console.log(JSON.stringify($scope.mRSSPFleet[i]));
                        // console.log("N : "+$scope.mRSSPFleet[i].N+", "+
                        //             "N1 : "+$scope.mRSSPFleet[i].N1+", "+
                        //             "N2 : "+$scope.mRSSPFleet[i].N2+", "+
                        //             "N3 : "+$scope.mRSSPFleet[i].N3+", "+
                        //             "N4 : "+$scope.mRSSPFleet[i].N4+", "+
                        //             "N5 : "+$scope.mRSSPFleet[i].N5);
                        var temp = {
                            "NId": $scope.mRSSPFleet[i].NId,
                            "N1Id": $scope.mRSSPFleet[i].N1Id,
                            "N2Id": $scope.mRSSPFleet[i].N2Id,
                            "N3Id": $scope.mRSSPFleet[i].N3Id,
                            "N4Id": $scope.mRSSPFleet[i].N4Id,
                            "N5Id": $scope.mRSSPFleet[i].N5Id,
                            "GroupDealerId": $scope.mRSSPFleet[i].GroupDealerId,                    
                            "OutletId": $scope.mRSSPFleet[i].OutletId,
                            "OutletCode": $scope.mRSSPFleet[i].OutletCode,
                            "Period": $scope.mRSSPFleet[i].Period,
                            "VehicleModelId": $scope.mRSSPFleet[i].VehicleModelId,
                            "VehicleModelName": $scope.mRSSPFleet[i].VehicleModelName,
                            "VehicleTypeColorId": $scope.mRSSPFleet[i].VehicleTypeColorId,
                            "TypeColor": $scope.mRSSPFleet[i].TypeColor,
                            "KatashikiCode": $scope.mRSSPFleet[i].KatashikiCode,
                            "SuffixCode": $scope.mRSSPFleet[i].SuffixCode,
                            "ColorId": $scope.mRSSPFleet[i].ColorId,
                            "ColorCode": $scope.mRSSPFleet[i].ColorCode,
                            "ColorName": $scope.mRSSPFleet[i].ColorName,
                            "N": $scope.mRSSPFleet[i].N,
                            "N1": $scope.mRSSPFleet[i].N1,
                            "N2": $scope.mRSSPFleet[i].N2,
                            "N3": $scope.mRSSPFleet[i].N3,
                            "N4": $scope.mRSSPFleet[i].N4,
                            "N5": $scope.mRSSPFleet[i].N5,
                            "CustomerFleetId": $scope.mRSSPFleet[i].CustomerFleetId,
                            "CustomerCode": $scope.mRSSPFleet[i].CustomerCode,
                            "CustomerName": $scope.mRSSPFleet[i].CustomerName,
                            "HOReviewState": $scope.mRSSPFleet[i].HOReviewState
                        }
                        updateDt.push(temp);
                    }
                }
                // RSSPFleet.update($scope.mRSSPFleet).then(function(res){
                RSSPFleet.update(updateDt).then(function(res){
                    console.log(res);
                    changeMode(viewMode);
                    $scope.hideNewButton=false;
                });;
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];
                angular.forEach($scope.orgData, function(org, orgIdx){
                    angular.forEach(org.child, function(dlr, dlrIdx){
                        angular.forEach(dlr.child, function(outlet, outletIdx){
                            angular.forEach($scope.productModelData, function(model, modelIdx){
                                generatedData.push({Dealer:dlr.title, Outlet:outlet.title, Model:model.name, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                            });
                        });
                    });
                });
                XLSXInterface.writeToXLSX(generatedData, 'RSSPFleet - '+$scope.yearData[$scope.mRSSPFleet.year].name);
            },
            visible:false,
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                document.getElementById('uploadRSSPFleet').click();
            },
            visible:false,
            title: 'Load File',
            rightsBit: 1
        },{
            func: function(string) {
                changeMode(editMode);
                $scope.hideNewButton=true;
            },
            visible: false,
            title: 'Edit',
            rightsBit: 2
        }
    ];

    $scope.selPer = {selPer:null};
    $scope.beforeNew = function() {
        $timeout(function(){
            $scope.selectedModel = {};
            $scope.selectedType={};
            $scope.gridDetail.data = [];
        });
        var sel = _.find($scope.periodData, { 'ActiveFlag': true });
        // console.log(sel);
        selPeriod = sel.period;
        $scope.selPer.selPer = sel.period;
        $scope.selectsudoPeriodId(sel);

        if($scope.orgData2.length==1){
            if($scope.orgData2[0].child.length==1) {
                // $scope.selectOutlet($scope.orgData2[0].child[0]);
                $timeout(function(){
                    $scope.tempRSSP.selOutletId = $scope.orgData2[0].child[0].id;
                    selOutId = $scope.orgData2[0].child[0].OutletId;
                    selOutCd = $scope.orgData2[0].child[0].OutletCode;
                    selOutNm = $scope.orgData2[0].child[0].Name;
                });
            }
        }
    }

    $scope.customDelete = function(selected) {
        // console.log("Bisa pak customDelete nya... hahahahahah"+selected);
        RSSPFleet.delete(selected).then(function(res){
            console.log(res);
            $scope.getData();
            // RSSPFleet.getData($scope.filter).then(
            //     function(res){
            //         // console.log(JSON.stringify(res.data));                
            //         $scope.grid.data = res.data.Result;                
            //         $scope.mRSSPFleet = res.data.Result;
            //         // console.log("mRSSPFleet : "+JSON.stringify($scope.mRSSPFleet));
            //         $scope.loading=false;
            //         changeMode(viewMode);
            //         return res.data;
            //     },
            //     function(err){
            //         console.log("err=>",err);
            //     }
            // );
        });
    };

    $scope.customApprove = function(selected) {
        // console.log("Bisa pak customApprove nya... hahahahahah "+selected);
        var updateApp = [];
        for (var i in selected) {
            if (selected[i].HOReviewState == false) {
                var temp = {
                    "NId": selected[i].NId,
                    "N1Id": selected[i].N1Id,
                    "N2Id": selected[i].N2Id,
                    "N3Id": selected[i].N3Id,
                    "N4Id": selected[i].N4Id,
                    "N5Id": selected[i].N5Id,
                    "GroupDealerId": selected[i].GroupDealerId,                    
                    "OutletId": selected[i].OutletId,
                    "OutletCode": selected[i].OutletCode,
                    "Period": selected[i].Period,
                    "VehicleModelId": selected[i].VehicleModelId,
                    "VehicleModelName": selected[i].VehicleModelName,
                    "VehicleTypeColorId": selected[i].VehicleTypeColorId,
                    "TypeColor": selected[i].TypeColor,
                    "KatashikiCode": selected[i].KatashikiCode,
                    "SuffixCode": selected[i].SuffixCode,
                    "ColorId": selected[i].ColorId,
                    "ColorCode": selected[i].ColorCode,
                    "ColorName": selected[i].ColorName,
                    "N": selected[i].N,
                    "N1": selected[i].N1,
                    "N2": selected[i].N2,
                    "N3": selected[i].N3,
                    "N4": selected[i].N4,
                    "N5": selected[i].N5,
                    "CustomerFleetId": selected[i].CustomerFleetId,
                    "CustomerCode": selected[i].CustomerCode,
                    "CustomerName": selected[i].CustomerName,
                    "HOReviewState": true
                }
                // selected[i].HOReviewState = true;
                // updateApp.push(selected[i]);
                updateApp.push(temp);
            }                    
        }
        if (updateApp.length>0) {
            // console.log('Blm di APPROVE : '+JSON.stringify($scope.mRSSPFleet));
            // console.log('Sdh di APPROVE : '+JSON.stringify(updateApp));
            
            RSSPFleet.update(updateApp).then(function(res){
                console.log(res);
                $scope.getData();
                $scope.xRSSPFleet.selected = [];
                changeMode(viewMode);
            });
        } else {
            console.log('sdh di approve semua');
            bsNotify.show(
                {
                    size: 'medium',
                    type: 'warning',
                    title: "Data yang dipilih sudah diapprove",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );  
            // $scope.showAlert(alerts.join('\n'), alerts.length);                    
        } 
    };

    $scope.customReject = function(selected) {
        // console.log("Bisa pak customReject nya... hahahahahah"+selected);
        var updateUnapp = [];
        for (var i in selected) {
            if (selected[i].HOReviewState == true) {
                var temp = {
                    "NId": selected[i].NId,
                    "N1Id": selected[i].N1Id,
                    "N2Id": selected[i].N2Id,
                    "N3Id": selected[i].N3Id,
                    "N4Id": selected[i].N4Id,
                    "N5Id": selected[i].N5Id,
                    "GroupDealerId": selected[i].GroupDealerId,                    
                    "OutletId": selected[i].OutletId,
                    "OutletCode": selected[i].OutletCode,
                    "Period": selected[i].Period,
                    "VehicleModelId": selected[i].VehicleModelId,
                    "VehicleModelName": selected[i].VehicleModelName,
                    "VehicleTypeColorId": selected[i].VehicleTypeColorId,
                    "TypeColor": selected[i].TypeColor,
                    "KatashikiCode": selected[i].KatashikiCode,
                    "SuffixCode": selected[i].SuffixCode,
                    "ColorId": selected[i].ColorId,
                    "ColorCode": selected[i].ColorCode,
                    "ColorName": selected[i].ColorName,
                    "N": selected[i].N,
                    "N1": selected[i].N1,
                    "N2": selected[i].N2,
                    "N3": selected[i].N3,
                    "N4": selected[i].N4,
                    "N5": selected[i].N5,
                    "CustomerFleetId": selected[i].CustomerFleetId,
                    "CustomerCode": selected[i].CustomerCode,
                    "CustomerName": selected[i].CustomerName,
                    "HOReviewState": false
                }
                // selected[i].HOReviewState = false;
                // updateUnapp.push(selected[i]);
                updateUnapp.push(temp);
            }                    
        }
        if (updateUnapp.length>0) {
            console.log('UNAPPROVE : '+JSON.stringify(updateUnapp));
            RSSPFleet.update(updateUnapp).then(function(res){
                console.log(res);
                $scope.getData();
                $scope.xRSSPFleet.selected = [];
                changeMode(viewMode);
            });
        } else {
            console.log('blm di approve semua');
            bsNotify.show(
                {
                    size: 'medium',
                    type: 'warning',
                    title: "Data yang dipilih belum diapprove",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );  
            // $scope.showAlert(alerts.join('\n'), alerts.length);                    
        } 
    };

    // $scope.customBulkSettings = [
    //     {   
    //         func: function(string) {
    //             RSSPFleet.delete($scope.xRSSPFleet.selected).then(function(res){
    //                 console.log(res);
    //                 RSSPFleet.getData($scope.filter).then(
    //                     function(res){
    //                         // console.log(JSON.stringify(res.data));                
    //                         $scope.grid.data = res.data.Result;                
    //                         $scope.mRSSPFleet = res.data.Result;
    //                         // console.log("mRSSPFleet : "+JSON.stringify($scope.mRSSPFleet));
    //                         $scope.loading=false;
    //                         changeMode(viewMode);
    //                         return res.data;
    //                     },
    //                     function(err){
    //                         console.log("err=>",err);
    //                     }
    //                 );
    //             });
    //         },
    //         title: 'Delete Selected',
    //         visible:false,
    //         color: '#ccc'
    //     },{
    //         func: function(string) {
    //             var updateApp = [];
    //             for (var i in $scope.xRSSPFleet.selected) {
    //                 if ($scope.xRSSPFleet.selected[i].HOReviewState == false) {
    //                     var temp = {
    //                         "NId": $scope.xRSSPFleet.selected[i].NId,
    //                         "N1Id": $scope.xRSSPFleet.selected[i].N1Id,
    //                         "N2Id": $scope.xRSSPFleet.selected[i].N2Id,
    //                         "N3Id": $scope.xRSSPFleet.selected[i].N3Id,
    //                         "N4Id": $scope.xRSSPFleet.selected[i].N4Id,
    //                         "N5Id": $scope.xRSSPFleet.selected[i].N5Id,
    //                         "GroupDealerId": $scope.xRSSPFleet.selected[i].GroupDealerId,                    
    //                         "OutletId": $scope.xRSSPFleet.selected[i].OutletId,
    //                         "OutletCode": $scope.xRSSPFleet.selected[i].OutletCode,
    //                         "Period": $scope.xRSSPFleet.selected[i].Period,
    //                         "VehicleModelId": $scope.xRSSPFleet.selected[i].VehicleModelId,
    //                         "VehicleModelName": $scope.xRSSPFleet.selected[i].VehicleModelName,
    //                         "VehicleTypeColorId": $scope.xRSSPFleet.selected[i].VehicleTypeColorId,
    //                         "TypeColor": $scope.xRSSPFleet.selected[i].TypeColor,
    //                         "KatashikiCode": $scope.xRSSPFleet.selected[i].KatashikiCode,
    //                         "SuffixCode": $scope.xRSSPFleet.selected[i].SuffixCode,
    //                         "ColorId": $scope.xRSSPFleet.selected[i].ColorId,
    //                         "ColorCode": $scope.xRSSPFleet.selected[i].ColorCode,
    //                         "ColorName": $scope.xRSSPFleet.selected[i].ColorName,
    //                         "N": $scope.xRSSPFleet.selected[i].N,
    //                         "N1": $scope.xRSSPFleet.selected[i].N1,
    //                         "N2": $scope.xRSSPFleet.selected[i].N2,
    //                         "N3": $scope.xRSSPFleet.selected[i].N3,
    //                         "N4": $scope.xRSSPFleet.selected[i].N4,
    //                         "N5": $scope.xRSSPFleet.selected[i].N5,
    //                         "CustomerFleetId": $scope.xRSSPFleet.selected[i].CustomerFleetId,
    //                         "CustomerCode": $scope.xRSSPFleet.selected[i].CustomerCode,
    //                         "CustomerName": $scope.xRSSPFleet.selected[i].CustomerName,
    //                         "HOReviewState": true
    //                     }
    //                     // $scope.xRSSPFleet.selected[i].HOReviewState = true;
    //                     // updateApp.push($scope.xRSSPFleet.selected[i]);
    //                     updateApp.push(temp);
    //                 }                    
    //             }
    //             if (updateApp.length>0) {
    //                 // console.log('Blm di APPROVE : '+JSON.stringify($scope.mRSSPFleet));
    //                 // console.log('Sdh di APPROVE : '+JSON.stringify(updateApp));
                    
    //                 RSSPFleet.update(updateApp).then(function(res){
    //                     console.log(res);
    //                     $scope.getData();
    //                     $scope.xRSSPFleet.selected = [];
    //                     changeMode(viewMode);
    //                 });
    //             } else {
    //                 console.log('sdh di approve semua');
    //                 bsNotify.show(
    //                     {
    //                         size: 'medium',
    //                         type: 'warning',
    //                         title: "Data yang dipilih sudah diapprove",
    //                         // content: error.join('<br>'),
    //                         // number: error.length
    //                     }
    //                 );  
    //                 // $scope.showAlert(alerts.join('\n'), alerts.length);                    
    //             } 
    //         },
    //         title: 'Approve',
    //         visible:false,
    //         color: '#ccc'
    //     },{
    //         func: function(string) {
    //             var updateUnapp = [];
    //             for (var i in $scope.xRSSPFleet.selected) {
    //                 if ($scope.xRSSPFleet.selected[i].HOReviewState == true) {
    //                     var temp = {
    //                         "NId": $scope.xRSSPFleet.selected[i].NId,
    //                         "N1Id": $scope.xRSSPFleet.selected[i].N1Id,
    //                         "N2Id": $scope.xRSSPFleet.selected[i].N2Id,
    //                         "N3Id": $scope.xRSSPFleet.selected[i].N3Id,
    //                         "N4Id": $scope.xRSSPFleet.selected[i].N4Id,
    //                         "N5Id": $scope.xRSSPFleet.selected[i].N5Id,
    //                         "GroupDealerId": $scope.xRSSPFleet.selected[i].GroupDealerId,                    
    //                         "OutletId": $scope.xRSSPFleet.selected[i].OutletId,
    //                         "OutletCode": $scope.xRSSPFleet.selected[i].OutletCode,
    //                         "Period": $scope.xRSSPFleet.selected[i].Period,
    //                         "VehicleModelId": $scope.xRSSPFleet.selected[i].VehicleModelId,
    //                         "VehicleModelName": $scope.xRSSPFleet.selected[i].VehicleModelName,
    //                         "VehicleTypeColorId": $scope.xRSSPFleet.selected[i].VehicleTypeColorId,
    //                         "TypeColor": $scope.xRSSPFleet.selected[i].TypeColor,
    //                         "KatashikiCode": $scope.xRSSPFleet.selected[i].KatashikiCode,
    //                         "SuffixCode": $scope.xRSSPFleet.selected[i].SuffixCode,
    //                         "ColorId": $scope.xRSSPFleet.selected[i].ColorId,
    //                         "ColorCode": $scope.xRSSPFleet.selected[i].ColorCode,
    //                         "ColorName": $scope.xRSSPFleet.selected[i].ColorName,
    //                         "N": $scope.xRSSPFleet.selected[i].N,
    //                         "N1": $scope.xRSSPFleet.selected[i].N1,
    //                         "N2": $scope.xRSSPFleet.selected[i].N2,
    //                         "N3": $scope.xRSSPFleet.selected[i].N3,
    //                         "N4": $scope.xRSSPFleet.selected[i].N4,
    //                         "N5": $scope.xRSSPFleet.selected[i].N5,
    //                         "CustomerFleetId": $scope.xRSSPFleet.selected[i].CustomerFleetId,
    //                         "CustomerCode": $scope.xRSSPFleet.selected[i].CustomerCode,
    //                         "CustomerName": $scope.xRSSPFleet.selected[i].CustomerName,
    //                         "HOReviewState": false
    //                     }
    //                     // $scope.xRSSPFleet.selected[i].HOReviewState = false;
    //                     // updateUnapp.push($scope.xRSSPFleet.selected[i]);
    //                     updateUnapp.push(temp);
    //                 }                    
    //             }
    //             if (updateUnapp.length>0) {
    //                 console.log('UNAPPROVE : '+JSON.stringify(updateUnapp));
    //                 RSSPFleet.update(updateUnapp).then(function(res){
    //                     console.log(res);
    //                     $scope.xRSSPFleet.selected = [];
    //                     changeMode(viewMode);
    //                 });
    //             } else {
    //                 console.log('blm di approve semua');
    //                 bsNotify.show(
    //                     {
    //                         size: 'medium',
    //                         type: 'warning',
    //                         title: "Data yang dipilih belum diapprove",
    //                         // content: error.join('<br>'),
    //                         // number: error.length
    //                     }
    //                 );  
    //                 // $scope.showAlert(alerts.join('\n'), alerts.length);                    
    //             }                
    //         },
    //         title: 'Unapprove',
    //         visible:false,
    //         color: '#ccc'
    //     }
    // ];

    var outletData = [];
    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];

    GetsudoPeriod.getData().then(
        function(res){
            // console.log("GetsudoPeriod.getData() : "+JSON.stringify(res.data));
            for (var i in res.data.Result) {
                // console.log(res.data.Result[i].StartPeriod);
                // var temp = new Date(res.data.Result[i].StartPeriod);
                // var year = temp.getFullYear().toString();
                // var mnth = (temp.getMonth()<10 ? '0'+parseInt(temp.getMonth()+1) : (temp.getMonth()+1).toString());
                // var date = (temp.getDate()<10 ? '0'+parseInt(temp.getDate()) : temp.getDate().toString());
                // res.data.Result[i].period = parseInt(year+mnth+date);

                // console.log(res.data.Result[i].GetsudoPeriodCode);

                // Return in int
                // res.data.Result[i].period = parseInt(res.data.Result[i].GetsudoPeriodCode+'01');

                // Return in datetime
                res.data.Result[i].period = res.data.Result[i].StartPeriod;
            }
            $scope.periodData = res.data.Result;
            // console.log($scope.periodData);            
            $scope.loading=false;
            return res.data.Result;
        }
    );
    CustomerFleet.getData().then(
        function(res){
            // console.log("CustomerFleet.getData() : "+JSON.stringify(res.data));
            $scope.fleetData = res.data.Result;
            $scope.loading=false;
            return res.data.Result;
        }
    );
    // OrgChart.getDataByRoleUser($scope.user).then(
    var grpDealDef = null;
    OrgChart.getDataTree().then(
        function(res){
            // console.log("OrgChart.getDataTree() : "+JSON.stringify(res.data.Result[0]));
            $scope.orgData = res.data.Result;
            // unremark here
            for (var i=0; i<$scope.orgData.length; i++) {                
                var temp = _.find($scope.orgData[i].listOutlet, { 'OutletId': ($scope.user.roleId==88?null:$scope.user.orgId) });    
                if (temp!==undefined) {
                    // console.log(temp);
                    $scope.filter.GroupDealerId = temp.GroupDealerId;
                    grpDealDef = temp.GroupDealerId;
                    break;
                }                
            }            
            // 
            $scope.orgData2 = res.data.Result;

            if($scope.orgData.length==1){
                if($scope.orgData[0].child.length==1){
                    // $scope.filOutlet($scope.orgData[0].child[0]);
                    $timeout(function(){
                        if ($scope.orgData[0].child[0].parent) {
                            filGrpDeal = $scope.orgData[0].child[0].parent.GroupDealerId;            
                        }
                        $scope.filter.GroupDealerId = filGrpDeal;
                        $scope.filter.OutletId = $scope.orgData[0].child[0].OutletId;
                        $scope.filter.OutletCode = $scope.orgData[0].child[0].OutletCode;
                        console.log($scope.filter.OutletCode);
                        outName = $scope.orgData[0].child[0].Name;
                    });
                }                    
            }
            $scope.loading=false;
            return res.data.Result;
        }
    );
    ProductModel.getDataTree().then(
        function(res){
            // console.log("ProductModel.getData() : "+JSON.stringify(res.data));
            $scope.productModelData = res.data.Result;
            // ProductModel.getDataType().then(
            //     function(xres){
            //         $scope.productModelTypeData = xres.data.Result;
            //         ProductModel.getDataTypeColor().then(
            //             function(yres){
            //                 $scope.productModelTypeColorData = xres.data.Result;
            //                 $scope.loading=false;
            //             }
            //         );                    
            //     }
            // );            
            return res.data.Result;
        }
    );

    function findParentOfTypeColor(id){
        console.log(id);
        var res = {model:null, type:null, typeColor:null};
        for(var i=0; i<$scope.productModelData.length; i++){
            // console.log("$scope.productModelData[i] : "+JSON.stringify($scope.productModelData[i]));
            for(var j=0; j<$scope.productModelData[i].listVehicleType.length; j++){
                for(var k=0; k<$scope.productModelData[i].listVehicleType[j].listColor.length; k++){
                    console.log($scope.productModelData[i].listVehicleType[j].listColor[k].ColorId,'vs',id);
                    if($scope.productModelData[i].listVehicleType[j].listColor[k].ColorId==id){
                        console.log('true');
                        console.log('KatashikiCode : '+$scope.productModelData[i].listVehicleType[j].listColor[k].parent.KatashikiCode);
                        res.model = $scope.productModelData[i].VehicleModelName;
                        res.typeId = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.VehicleTypeId;
                        // res.typeColor = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.KatashikiCode+' '+
                        //                 $scope.productModelData[i].listVehicleType[j].listColor[k].parent.SuffixCode+' '+
                        //                 $scope.productModelData[i].listVehicleType[j].listColor[k].ColorName;
                        res.ColorId = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorId;
                        res.ColorCode = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorCode;
                        res.ColorName = $scope.productModelData[i].listVehicleType[j].listColor[k].ColorName;
                        // res.katashiki = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.KatashikiCode;
                        // res.suffix = $scope.productModelData[i].listVehicleType[j].listColor[k].parent.SuffixCode;
                        break;
                    }
                }
                if(res.model!=null) break;
            }
            if(res.model!=null) break;
        }
        return res;
    };
    function getFlexMonth(month){
        if(month<-12) return months[(24+month)]
        else if(month<0) return months[(12+month)]
        else if(month>11) return months[Math.abs(12-month)]
        else return months[month];
    }
    $scope.filsudoPeriodId = function(selected){
        // {
        //     "Id":1,
        //     "GroupDealerId":999,
        //     "OutletCode":"DUMMY",
        //     "Period":20160401,
        //     "VehicleModelName":"FORTUNER",
        //     "VehicleTypeColorId":14,
        //     "TypeColor":"DK. RED M.M. F456 789",
        //     "KatashikiCode":"F456",
        //     "SuffixCode":"789",
        //     "ColorCode":"3Q3",
        //     "ColorName":"DK. RED M.M.",
        //     "N":100,
        //     "N1":200,
        //     "N2":300,
        //     "N3":400,
        //     "N4":500,
        //     "N5":600,
        //     "CustomerId":6,
        //     "CustomerCode":"BB",
        //     "CustomerName":"Blue Bird",
        //     "HOReviewState":true
        // };

        //$scope.filter.getsudoPeriodId = selected.GetsudoPeriodId;
        console.log("selected Prd=>",selected);
        if(selected){
            $scope.filter.getsudoPeriodId = selected.GetsudoPeriodId;
            var dt = new Date(selected.StartPeriod);
            var chMonth = dt.getMonth() + 1;
            console.log("chMonth filsudoPeriodId : "+chMonth);
            // prev version
            // for (var i=8; i<14; i++) {}
            // 
            for (var i=9; i<15; i++) {
                $scope.grid.columnDefs[i].displayName = getFlexMonth(chMonth-1).substring(0,3);
                chMonth++;
            }
            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            console.log($scope.grid.columnDefs);
            $scope.periodActive = selected.ActiveFlag;
            $scope.grid.data = [];
            changeMode(viewMode);
        }else{
            $scope.filter.getsudoPeriodId = null;
            $scope.filter.period = null;
            $scope.periodActive = false;
            $scope.grid.data = [];
            changeMode(viewMode);
        }
    };

    $scope.filcustomerId = function(selected){
        console.log(selected);
        if(selected){
            $scope.filter.CustomerFleetId = selected.Id;
        }else{
            $scope.filter.CustomerFleetId = null;
        }
    };

    var filGrpDeal = null, outName=null, typeName=null;
    $scope.filOutlet = function(selected){
        console.log(selected);
        if(selected){
            if (selected.parent) {
                filGrpDeal = selected.parent.GroupDealerId;            
            }
            $scope.filter.GroupDealerId = filGrpDeal;
            $scope.filter.OutletId = selected.OutletId;
            $scope.filter.OutletCode = selected.OutletCode;
            outName = selected.Name;
        }else{
            $scope.filter.GroupDealerId = grpDealDef;
            $scope.filter.OutletId = ($scope.user.roleId==88 ? null: ($scope.orgData[0].child.length==1 ? $scope.user.orgId : null));
            $scope.filter.OutletCode = null;
            outName = null;
        }
    };

    $scope.filproductType = function(selected){
        console.log(selected);
        if(selected){
            $scope.filter.productType = selected;
            typeName = selected.Description;
        }else{
            $scope.filter.productType = null;
            typeName = null;
        }
    };

    //var mthArr = [];

    var selPeriod, selCusId=null, selCusNm, selCusCd=null, selOutId, selOutCd, selOutNm, selModelId, selModel, selKat, selSuf, selType, selColor;
    $scope.selectsudoPeriodId = function(selected){
        var dt = new Date(selected.StartPeriod);
        var chMonth = dt.getMonth() + 1;
        // console.log("chMonth selectsudoPeriodId : "+chMonth);
        // mthArr = [];
        // prev version
        // for (var i=7; i<13; i++) {}
        // 
        for (var i=8; i<14; i++) {
	    //console.log("periodData=>",$scope.periodData[chMonth-1]);
            //mthArr.push($scope.periodData[chMonth-1].period);
            $scope.gridDetail.columnDefs[i].displayName = getFlexMonth(chMonth-1).substring(0,3);
            chMonth++;
        }
        $scope.gridDetailApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        // console.log($scope.gridDetail.columnDefs);
        // selPeriod = $scope.mRSSPFleet.Period;
    };

    $scope.saveCusName = {cusName:null};
    $scope.selectCustomer = function(selected){
        // console.log(selected);
        if (selected) {
            selCusId = selected.Id;
            selCusNm = selected.CustomerName;
            selCusCd = selected.CustomerCode;
        } else {
            selCusId = null;
            selCusNm = null;
            selCusCd = null;    
        }
        
        console.log(selCusId);
    };
    $scope.selectOutlet = function(selected){
        // console.log(selected);
        // $scope.mRSSPFleet.orgId = selected.id;
        // $scope.mRSSPFleet.GroupDealerId = selected.pid;
        // $scope.mRSSPFleet.OutletCode = selected.OutletCode;
        if (selected) {
            selOutId = selected.OutletId;
            selOutCd = selected.OutletCode;
            selOutNm = selected.Name;
        } else {
            selOutId = null;
            selOutCd = null;
            selOutNm = null;
        }        
    };

    $scope.selectModel = function(selected) {
        // console.log("selectModel selected : "+JSON.stringify(selected));        
        // $scope.mRSSPFleet.VehicleModelName = selected.VehicleModelName;
        if (selected) {
            $scope.selectedModel=selected;
            selModelId = selected.VehicleModelId;
            selModel = selected.VehicleModelName;
        } else {
            $scope.selectedModel = {};
            selModelId = null;
            selModel = null;
        }

        $scope.tempRSSP.selVehicleTypeId = null;
        $scope.selectedType={};
        // $scope.mRSSPFleet.ColorId = null;
    };
    $scope.selectType = function(selected) {
        if (selected) {
            selType = selected.Description;
            selKat = selected.KatashikiCode;
            selSuf = selected.SuffixCode;
            $scope.selectedType=selected;

            for(var i in $scope.selectedType.listColor){
                $scope.selectedType.listColor[i].TheId =    {
                                                                OutletId : selOutId,
                                                                OutletCode : selOutCd,
                                                                OutletName : selOutNm,
                                                                VehicleModelId : selModelId,
                                                                VehicleModelName : selModel,
                                                                TypeId : selected.VehicleTypeId,
                                                                TypeColor : selKat+selSuf+$scope.selectedType.listColor[i].ColorName,
                                                                TypeName : selType,
                                                                ColorId : $scope.selectedType.listColor[i].ColorId,
                                                                ColorCode : $scope.selectedType.listColor[i].ColorCode,
                                                                ColorName : $scope.selectedType.listColor[i].ColorName,
                                                                CustomerFleetId : selCusId,
                                                                // CustomerName : selCusNm,
                                                                CustomerName : $scope.saveCusName.cusName,
                                                                CustomerCode : selCusCd,
                                                                KatashikiCode : selKat,
                                                                SuffixCode : selSuf,
                                                                Period : selPeriod
                                                            };
            }
        } else {
            selType = null;
            selKat = null;
            selSuf = null;
            $scope.selectedType={};
        }

        // $scope.mRSSPFleet.ColorId = null;      
    };
    $scope.selectMultiColor = function(selected){
        var newData = [];
        console.log('selected.parent : ',selected);     

        if (selected!==undefined) {
            for(var i=0; i<selected.length; i++){
                // var obj = findParentOfTypeColor(selected[i]);
                // console.log(JSON.stringify(obj));
                // var arrObj = selected[i].split('|');
                newData.push({     
                    OutletId : selected[i].OutletId,
                    OutletCode : selected[i].OutletCode,
                    OutletName : selected[i].OutletName,
                    VehicleModelId : selected[i].VehicleModelId,
                    VehicleModelName: selected[i].VehicleModelName,
                    TypeId: selected[i].TypeId,
                    TypeColor: selected[i].TypeColor,
                    TypeName: selected[i].TypeName,
                    ColorId: selected[i].ColorId,
                    ColorCode: selected[i].ColorCode,
                    ColorName: selected[i].ColorName,
                    CustomerFleetId : selected[i].CustomerFleetId,
                    CustomerName : selected[i].CustomerName,
                    CustomerCode : selected[i].CustomerCode,
                    KatashikiCode: selected[i].KatashikiCode,
                    SuffixCode: selected[i].SuffixCode,
                    Period: selected[i].Period,
                    HOReviewState: false,
                    N:0,
                    N1:0,
                    N2:0,
                    N3:0,
                    N4:0,
                    N5:0,                
                });
            }
            $scope.gridDetail.data = newData;
        }            

        // if (selPeriod==null || selOutId==null || selCusId==null) {
        //     return;
        // }
        // var obj = findParentOfTypeColor(selected[selected.length-1]);
        // $scope.gridDetail.data.push({                               
        //     productModelId: selected[selected.length-1],
        //     OutletId : selOutId,
        //     OutletCode : selOutCd,
        //     VehicleModelId : selModelId,
        //     VehicleModelName: selModel,
        //     TypeId: obj.typeId,
        //     TypeColor: selKat+selSuf+obj.ColorName,
        //     ColorId: obj.ColorId,
        //     ColorCode: obj.ColorCode,
        //     ColorName: obj.ColorName,
        //     CustomerFleetId : selCusId,
        //     CustomerName : selCusNm,
        //     CustomerCode : selCusCd,
        //     KatashikiCode: selKat,
        //     SuffixCode: selSuf,
        //     Period: selPeriod,
        //     N:0,
        //     N1:0,
        //     N2:0,
        //     N3:0,
        //     N4:0,
        //     N5:0,                
        // });        
    }
    function validateXLS(data){
        var result = [];
        var error = [];
        for(var i=0; i<data.length; i++){
            var dOutlet = _.find(outletData, { 'title': data[i].Outlet });
            var dModel = _.find($scope.productModelData, { 'name': data[i].Model });
            if(dOutlet == undefined){
                error.push('Outlet '+data[i].Outlet+' tidak ditemukan.');
                continue;
            }
            if(dModel == undefined){
                error.push('Model '+data[i].Model+' tidak ditemukan.');
                continue;
            }
            var newObj = {
                m1: data[i].Jan,
                m2: data[i].Feb,
                m3: data[i].Mar,
                m4: data[i].Apr,
                m5: data[i].May,
                m6: data[i].Jun,
                m7: data[i].Jul,
                m8: data[i].Aug,
                m9: data[i].Sep,
                m10: data[i].Oct,
                m11: data[i].Nov,
                m12: data[i].Dec,
                orgId: dOutlet.id,
                dealerName: dOutlet.parent.title,
                outletName: dOutlet.title,
                modelName: dModel.name,
                productModelId: dModel.id
            };
            result.push(newObj);
        }
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadRSSPFleet' ) );
        XLSXInterface.loadToJson(myEl[0].files[0], function(json){
            $timeout(function(){
                myEl.value = '';
                $scope.grid.data = validateXLS(json);

                mDataCopy =  angular.copy($scope.grid.data);
                changeMode(editMode);
            });
        });
    };
    $scope.getData = function() {
        console.log("mau get data");
        // $scope.filsudoPeriodId();
        if ($scope.filter.getsudoPeriodId==null) {
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
                // content: error.join('<br>'),
                // number: error.length
            });
        }else{
            RSSPFleet.getData($scope.filter).then(
                function(res){
                // console.log(JSON.stringify(res.data));                

                    var temp = [];
                    for (var i in res.data.Result) {    
                        console.log(res.data.Result[i]);
                        if (res.data.Result[i]!==null) {
                            // res.data.Result[i]["OutletName"] = outName;
                            res.data.Result[i]["TypeName"] = res.data.Result[i]["VehicleTypeName"];
                            
                            temp.push(res.data.Result[i]);
                        }
                    }                
                    // $scope.grid.data = res.data.Result;   
                    // $scope.mRSSPFleet = res.data.Result;
                    $scope.grid.data = temp;                
                    $scope.mRSSPFleet = temp;

                    mDataCopy =  angular.copy($scope.grid.data);

                    // console.log("mRSSPFleet : "+JSON.stringify($scope.mRSSPFleet));
                    $scope.loading=false;
                    changeMode(viewMode);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    $scope.onBeforeCancel = function(){
        // console.log('before cancel..');
        $scope.gridDetail.data = [];
    };

    $scope.doCustomSave = function(mdl,mode){
        console.log("inside before save");
        console.log("mdl : "+JSON.stringify(mdl));
        console.log("mode : "+mode);
        var grdData = $scope.gridDetail.data;
        var lastArr = [];
        
        for (var i in grdData) {                
            var temp = {};
            for (var j=0; j<6; j++) {
                // {
                //     "NId": 2,
                //     "N1Id": 3,
                //     "N2Id": 4,
                //     "N3Id": 5,
                //     "N4Id": 6,
                //     "N5Id": 7,
                //     "GroupDealerId": 999,
                //     "OutletId": 999,
                //     "Period": 20160401,
                //     "VehicleModelId": 49,
                //     "VehicleTypeColorId": 14,
                //     "KatashikiCode": "F456",
                //     "SuffixCode": "789",
                //     "ColorId": 2252,
                //     "N": 100,
                //     "N1": 200,
                //     "N2": 300,
                //     "N3": 400,
                //     "N4": 500,
                //     "N5": 600,
                //     "CustomerFleetId": null,
                //     "HOReviewState": true
                // }
                             
                if (j==0) {
                    temp["N"] = grdData[i]["N"];
                    temp["NId"] = null;
                } else {
                    temp["N"+j] = grdData[i]["N"+j];
                    temp["N"+j+"Id"] = null;
                }
            }
            temp.OutletId = grdData[i].OutletId;
            temp.OutletCode = grdData[i].OutletCode;
            // temp.GroupDealerId = $scope.mRSSPFleet.GroupDealerId;
            temp.VehicleModelId = grdData[i].VehicleModelId;
            temp.VehicleModelName = grdData[i].VehicleModelName;
            temp.KatashikiCode = grdData[i].KatashikiCode;
            temp.SuffixCode = grdData[i].SuffixCode;
            temp.Period = grdData[i].Period;
            temp.VehicleTypeColorId = grdData[i].TypeId;
            temp.TypeColor = grdData[i].TypeColor;            
            temp.ColorId = grdData[i].ColorId[i];
            temp.ColorCode = grdData[i].ColorCode;
            temp.ColorName = grdData[i].ColorName;
            temp.CustomerFleetId = grdData[i].CustomerFleetId;
            temp.CustomerName = grdData[i].CustomerName;
            temp.CustomerCode = grdData[i].CustomerCode;
            temp.HOReviewState = false;
            lastArr.push(temp);
        }
        console.log("lastArr : "+JSON.stringify(lastArr));

        if (mode=='create') {
            RSSPFleet.create(lastArr).then(function(res){
                console.log(res);
                $scope.gridDetail.data = [];
            });
        } else {

        }
    }

    // {
    //   "NId": 2,
    //   "N1Id": 3,
    //   "N2Id": 4,
    //   "N3Id": 5,
    //   "N4Id": 6,
    //   "N5Id": 7,
    //   "GroupDealerId": 999,
    //   "OutletId": 999,
    //   "OutletCode": "DUMMY",
    //   "Period": 20160401,
    //   "VehicleModelId": 49,
    //   "VehicleModelName": "FORTUNER",
    //   "VehicleTypeColorId": 14,
    //   "TypeColor": "F456 789 DK. RED M.M.",
    //   "KatashikiCode": "F456",
    //   "SuffixCode": "789",
    //   "ColorId": 2252,
    //   "ColorCode": "3Q3",
    //   "ColorName": "DK. RED M.M.",
    //   "N": 100,
    //   "N1": 200,
    //   "N2": 300,
    //   "N3": 400,
    //   "N4": 500,
    //   "N5": 600,
    //   "CustomerFleetId": null,
    //   "CustomerCode": null,
    //   "CustomerName": "Blue Bird",
    //   "HOReviewState": true
    // }    

    var lblCellTemplate =   '<div class="ui-grid-cell-contents">'+
                            '   {{ row.entity.HOReviewState ? "Disetujui" : "Belum disetujui" }}'+
                            '</div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableCellEdit: false,
        enableSelectAll: true,
        enableCellEditOnFocus: false,
        cellEditableCondition: function($scope) {
            return !$scope.row.entity.HOReviewState && editableMode;
        },
        columnDefs: [
            // {
            //   "NId": 2,
            //   "N1Id": 3,
            //   "N2Id": 4,
            //   "N3Id": 5,
            //   "N4Id": 6,
            //   "N5Id": 7,
            //   "GroupDealerId": 999,
            //   "OutletId": 999,
            //   "OutletCode": "DUMMY",
            //   "Period": 20160401,
            //   "VehicleModelId": 49,
            //   "VehicleModelName": "FORTUNER",
            //   "VehicleTypeColorId": 14,
            //   "TypeColor": "F456 789 DK. RED M.M.",
            //   "KatashikiCode": "F456",
            //   "SuffixCode": "789",
            //   "ColorId": 2252,
            //   "ColorCode": "3Q3",
            //   "ColorName": "DK. RED M.M.",
            //   "N": 100,
            //   "N1": 200,
            //   "N2": 300,
            //   "N3": 400,
            //   "N4": 500,
            //   "N5": 600,
            //   "CustomerFleetId": null,
            //   "CustomerCode": null,
            //   "CustomerName": "Blue Bird",
            //   "HOReviewState": true
            // }
            { name:'id',    field:'id', visible:false },
            { name:'customerFleetId', field:'CustomerId', visible:false},
            { name:'OutletName', field:'OutletName', enableCellEdit: false},
            { name:'Customer Fleet', field:'CustomerName', enableCellEdit: false},
            { name:'Model', field: 'VehicleModelName', enableCellEdit: false},
            { name:'Type', field: 'TypeName', enableCellEdit: false},
            { name:'Name Color', field: 'ColorName', enableCellEdit: false},
            // { name:'Katashiki', field: 'KatashikiCode', enableCellEdit: false},
            { name:'Suffix', field: 'SuffixCode', enableCellEdit: false},
            // { name:'HO Review State', field: 'HOReviewState', enableCellEdit: false},
            { name:'HO Review State', allowCellFocus: false, width:100,
                                    enableColumnMenu:false,enableSorting: false, enableColumnResizing: false,
                                    cellTemplate: lblCellTemplate},
            { name:'Jan', field: 'N', type: 'number' , enableCellEdit: false},
            { name:'Feb', field: 'N1', type: 'number' , enableCellEdit: true},
            { name:'Mar', field: 'N2', type: 'number' , enableCellEdit: true},
            { name:'Apr', field: 'N3', type: 'number' , enableCellEdit: true},
            { name:'May', field: 'N4', type: 'number' , enableCellEdit: true},
            { name:'Jun', field: 'N5', type: 'number' , enableCellEdit: true},
        ]
    };

    var editCellTemplateN = '<div class="form-group" style="margin-top:4px;"><input type="number" min="0" name="total" placeholder="N1"'+
                                          ' ng-model="row.entity.N"'+
                                          ' class="form-control" style="height:25px;" /> </div>';
    var editCellTemplateN1 = '<div class="form-group" style="margin-top:4px;"><input type="number" min="0" name="total" placeholder="N1"'+
                                          ' ng-model="row.entity.N1"'+
                                          ' class="form-control" style="height:25px;" /> </div>';

    // var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
    //                                 ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
    //                                 ' onclick="this.blur()"'+
    //                                 ' ng-click="grid.appScope.gridClickToggleStatus(row.entity)">'+
    //                             '</button>';
                  
    $scope.afterRegisterGridApi = function(gridApi) {
        $scope.gridApi = gridApi;
        // console.log($scope.gridApi);

        $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            // console.log('Inside afterCellEdit..');
            if (newValue<0) {
                newValue = 0;
                rowEntity[colDef.field] = newValue;
            }
        });
    };

    $scope.gridDetailApi = {};
    $scope.gridDetail = {
        enableSorting: true,
        enableSelectAll: true,
        enableCellEdit: false,
        enableCellEditOnFocus: true, // set any editable column to allow edit on focus
        // cellEditableCondition: function($scope) {
        //     console.log('masuk kondisi',mode==editMode);
        //   // put your enable-edit code here, using values from $scope.row.entity and/or $scope.col.colDef as you desire
        //   return true; // in this example, we'll only allow active rows to be edited
        // },
        enableCellEditOnFocus: true,
        columnDefs: [
            { name:'id',    field:'id', visible:false },
            { name:'OutletCode', field:'OutletCode', visible:false},
            { name:'OutletName', field:'OutletName'},
            { name:'Model', field: 'VehicleModelName'},
            { name:'Type', field: 'TypeName'},
            { name:'Name Color', field: 'ColorName'},
            // { name:'Katashiki', field: 'KatashikiCode'},
            { name:'Suffix', field: 'SuffixCode'},  
            // { name:'HO Review State', field: 'HOReviewState', enableCellEdit: false},
            { name:'HO Review State', allowCellFocus: false, width:100,
                                    enableColumnMenu:false,enableSorting: false, enableColumnResizing: false,
                                    cellTemplate: lblCellTemplate},          
            // { name:'Jan', field: 'N', type: 'number', editableCellTemplate : editCellTemplateN, enableCellEdit: true},
            { name:'Jan', field: 'N', type: 'number', enableCellEdit: false},
            { name:'Feb', field: 'N1', type: 'number', enableCellEdit: true},
            { name:'Mar', field: 'N2', type: 'number', enableCellEdit: true },
            { name:'Apr', field: 'N3', type: 'number', enableCellEdit: true },
            { name:'May', field: 'N4', type: 'number', enableCellEdit: true },
            { name:'Jun', field: 'N5', type: 'number', enableCellEdit: true },
            // { name:'Action',    allowCellFocus: false, width:100, pinnedRight:true,
            //                     enableColumnMenu:false,enableSorting: false, enableColumnResizing: false,
            //                     cellTemplate: btnActionEditTemplate
            //                 }
        ]
    };

    $scope.formApi={};

    $scope.gridDetail.onRegisterApi = function(gridApi) {
      // set gridApi on scope
      $scope.gridDetailApi = gridApi;              

      $scope.gridDetailApi.selection.on.rowSelectionChanged($scope,function(row) {
          //scope.selectedRows=null;
          $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();
          // console.log("bsform selected=>",scope.selectedRows);                  
      });
      $scope.gridDetailApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
          //scope.selectedRows=null;
          $scope.selectedDetailRows = $scope.gridDetailApi.selection.getSelectedRows();                  
      });

      $scope.gridDetailApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
          // console.log('Inside afterCellEdit grid detail..');
          if (newValue<0 || newValue==null) {
              newValue = 0;
              rowEntity[colDef.field] = newValue;
          }
      });
    };

});
