angular.module('app')
  .factory('RSSPFleet', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        console.log(param);
        // var res=$http.get('/api/ds/RSSPDetailFleets/?start=1&limit=10&period='+param.period);
        // getsudoperiodid, customerfleetid, outletid, vehiclemodelid, groupdealerid

        // var xparam = '/api/ds/RSSPDetailFleets/?start=1&limit=10&period='+param.period;
        // if (param.CustomerCode != '' && param.CustomerCode != null) {
        //   xparam+='&CustomerCode='+param.CustomerCode;
        // }
        // if (param.OutletCode != '' && param.OutletCode != null) {
        //   xparam+='&OutletCode='+param.OutletCode;
        // }
        // if (param.VehicleModelName != '' && param.VehicleModelName != null) {
        //   xparam+='&VehicleModelName='+param.VehicleModelName;
        // }

        var xparam = '/api/ds/RSSPDetailFleets/?start=1&limit=10&getsudoPeriodId='+param.getsudoPeriodId;
        if (param.CustomerFleetId != '' && param.CustomerFleetId != null) {
          xparam+='&CustomerFleetId='+param.CustomerFleetId;
        }
        if (param.CustomerName != '' && param.CustomerName != null) {
          xparam+='&CustomerName='+param.CustomerName;
        }
        if (param.OutletId != '' && param.OutletId != null) {
          xparam+='&OutletId='+param.OutletId;
        }
        if (param.VehicleModelId != '' && param.VehicleModelId != null) {
          xparam+='&VehicleModelId='+param.VehicleModelId;
        }
        if (param.productColor != '' && param.productColor != null) {
          xparam+='&ColorId='+param.productColor;
        }

        if (param.GroupDealerId != '' && param.GroupDealerId != null) {
          xparam+='&GroupDealerId='+param.GroupDealerId;
        }
        console.log("Get Data..");
        console.log("$http.get("+xparam+")");
        console.log("================================================================");
        var res=$http.get(xparam);
        return res;

        // var defer = $q.defer();
        //     defer.resolve(
        //       {
        //         "data" : {
        //           "Result":[{
        //               "Id":1,
        //               "GroupDealerId":999,
        //               "OutletCode":"DUMMY",
        //               "Period":20160401,
        //               "VehicleModelName":"FORTUNER",
        //               "VehicleTypeColorId":14,
        //               "TypeColor":"DK. RED M.M. F456 789",
        //               "KatashikiCode":"F456",
        //               "SuffixCode":"789",
        //               "ColorCode":"3Q3",
        //               "ColorName":"DK. RED M.M.",
        //               "N":100,
        //               "N1":200,
        //               "N2":300,
        //               "N3":400,
        //               "N4":500,
        //               "N5":600,
        //               "CustomerId":6,
        //               "CustomerCode":"BB",
        //               "CustomerName":"Blue Bird",
        //               "HOReviewState":true
        //           }],
        //           "Start":1,
        //           "Limit":10,
        //           "Total":1
        //         }
        //       }
        //     );
        // return defer.promise;
      },
      create: function(rsspfleet) {
        // console.log(rsspfleet);        
        // return $http.post('/api/ds/RSSPDetailFleets/', {
        //   "NId": rsspfleet.NId,
        //   "N1Id": rsspfleet.N1Id,
        //   "N2Id": rsspfleet.N2Id,
        //   "N3Id": rsspfleet.N3Id,
        //   "N4Id": rsspfleet.N4Id,
        //   "N5Id": rsspfleet.N5Id,
        //   // "GroupDealerId": rsspfleet.GroupDealerId,                    
        //   "OutletId": rsspfleet.OutletId,
        //   "OutletCode": rsspfleet.OutletCode,
        //   "Period": rsspfleet.Period,
        //   "VehicleModelId": rsspfleet.VehicleModelId,
        //   "VehicleModelName": rsspfleet.VehicleModelName,
        //   "VehicleTypeColorId": rsspfleet.VehicleTypeColorId,
        //   "TypeColor": rsspfleet.TypeColor,
        //   "KatashikiCode": rsspfleet.KatashikiCode,
        //   "SuffixCode": rsspfleet.SuffixCode,
        //   "ColorId": rsspfleet.ColorId,
        //   "ColorCode": rsspfleet.ColorCode,
        //   "ColorName": rsspfleet.ColorName,
        //   "N": rsspfleet.N,
        //   "N1": rsspfleet.N1,
        //   "N2": rsspfleet.N2,
        //   "N3": rsspfleet.N3,
        //   "N4": rsspfleet.N4,
        //   "N5": rsspfleet.N5,
        //   "CustomerFleetId": rsspfleet.CustomerFleetId,
        //   "CustomerCode": rsspfleet.CustomerCode,
        //   "CustomerName": rsspfleet.CustomerName,
        //   "HOReviewState": rsspfleet.HOReviewState
        // });
        console.log("Create Data..");
        console.log("$http.post('/api/ds/RSSPDetailFleets/multiple', "+JSON.stringify(rsspfleet)+")");
        console.log("================================================================");
        return $http.post('/api/ds/RSSPDetailFleets/multiple', rsspfleet);
      },
      update: function(rsspfleet) {
        // console.log("update : "+JSON.stringify(rsspfleet));
        // return $http.put('/api/ds/RSSPDetailFleets/', {
        //   "NId": rsspfleet.NId,
        //   "N1Id": rsspfleet.N1Id,
        //   "N2Id": rsspfleet.N2Id,
        //   "N3Id": rsspfleet.N3Id,
        //   "N4Id": rsspfleet.N4Id,
        //   "N5Id": rsspfleet.N5Id,
        //   "GroupDealerId": rsspfleet.GroupDealerId,                    
        //   "OutletId": rsspfleet.OutletId,
        //   "OutletCode": rsspfleet.OutletCode,
        //   "Period": rsspfleet.Period,
        //   "VehicleModelId": rsspfleet.VehicleModelId,
        //   "VehicleModelName": rsspfleet.VehicleModelName,
        //   "VehicleTypeColorId": rsspfleet.VehicleTypeColorId,
        //   "TypeColor": rsspfleet.TypeColor,
        //   "KatashikiCode": rsspfleet.KatashikiCode,
        //   "SuffixCode": rsspfleet.SuffixCode,
        //   "ColorId": rsspfleet.ColorId,
        //   "ColorCode": rsspfleet.ColorCode,
        //   "ColorName": rsspfleet.ColorName,
        //   "N": rsspfleet.N,
        //   "N1": rsspfleet.N1,
        //   "N2": rsspfleet.N2,
        //   "N3": rsspfleet.N3,
        //   "N4": rsspfleet.N4,
        //   "N5": rsspfleet.N5,
        //   "CustomerFleetId": rsspfleet.CustomerFleetId,
        //   "CustomerCode": rsspfleet.CustomerCode,
        //   "CustomerName": rsspfleet.CustomerName,
        //   "HOReviewState": rsspfleet.HOReviewState
        // });
        console.log("Update Data..");
        console.log("$http.put('/api/ds/RSSPDetailFleets/multiple',"+JSON.stringify(rsspfleet)+")");
        console.log("================================================================");
        return $http.put('/api/ds/RSSPDetailFleets/multiple', rsspfleet);
      },
      delete: function(id) {
        console.log("delete "+JSON.stringify(id));
        var aa = [];
        var bb = [];
        
        for (var i = 0; i < id.length ; i++) {
          var cc={};
          cc.NId = id[i].NId;
          cc.N1Id = id[i].N1Id;
          cc.N2Id = id[i].N2Id;
          cc.N3Id = id[i].N3Id;
          cc.N4Id = id[i].N4Id;
          cc.N5Id = id[i].N5Id;
          cc.GroupDealerId = id[i].GroupDealerId;
          cc.OutletId = id[i].OutletId;
          cc.OutletCode = id[i].OutletCode;
          cc.Period = id[i].Period;
          cc.VehicleModelId = id[i].VehicleModelId;
          cc.VehicleModelName = id[i].VehicleModelName;
          cc.VehicleTypeColorId = id[i].VehicleTypeColorId;
          cc.TypeColor = id[i].TypeColor;
          cc.KatashikiCode = id[i].KatashikiCode;
          cc.SuffixCode = id[i].SuffixCode;
          cc.ColorId = id[i].ColorId;
          cc.ColorCode = id[i].ColorCode;
          cc.ColorName = id[i].ColorName;
          cc.N = id[i].N;
          cc.N1 = id[i].N1;
          cc.N2 = id[i].N2;
          cc.N3 = id[i].N3;
          cc.N4 = id[i].N4;
          cc.N5 = id[i].N5;
          cc.CustomerFleetId = id[i].CustomerFleetId;
          cc.CustomerCode = id[i].CustomerCode;
          cc.CustomerName = id[i].CustomerName;
          cc.HOReviewState = id[i].HOReviewState;
          aa.push(cc);
        };

        // if (aa.length>0) {
        //   bb=aa;
        // } else {
        //   var cc={};
        //    cc.Id = id;
        //    bb.push(cc);
        // } 

        console.log("Delete Data..");
        console.log("$http.delete('/api/ds/RSSPDetailFleets/Multiple', {data:"+JSON.stringify(aa)+",headers: {'Content-Type': 'application/json'}})");
        console.log("================================================================");

        return $http.delete('/api/ds/RSSPDetailFleets/Multiple', {data:aa,headers: {'Content-Type': 'application/json'}});
        // return res;
      }
      
    }
  });