angular.module('app')
    .controller('InternalOutletController', function($scope, $http, CurrentUser, InternalOutlet, Region, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mIntOutlet = {}; 
    $scope.cIntOutlet = null; //Collection
    $scope.xIntOutlet = {};
    $scope.xIntOutlet.selected=[];
    $scope.regionData={};

    $scope.mIntOutlet = {AreaId:null,ProvinceId:null};
    var areaProvince = [];
    var temp =[];
    $scope.provinceData= [];
    $scope.province=[];


    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        InternalOutlet.getData().then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
        

    Region.getDataTree().then(function(res){
        //  var xtemp = res.data.Result;
        // for (var i = 0; i <xtemp.length; i++) {
        //     // temp.concat(xtemp[i].child);
        //     for (var j = 0; j < xtemp[i].child.length; j++) {
        //         temp.push(xtemp[i].child[j]);
        //     }
        // }
        $scope.provinceData = res.data.Result;
        temp = res.data.Result;
        $scope.loading=false;
        //return res.data;
    });

    Region.getAreaProvince().then(
        function(res){
            areaProvince = res.data.Result;
            //filterArea();
            //filterProvince();
            $scope.loading=false;
        }
    );

    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }

    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
    }

    function filterProvince(filter){
        $scope.province = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = temp;
        }else{
            dataSrc = filterArray(areaProvince, filter);
          
        }
        if (filter==undefined || filter==null){
            $scope.provinceData = dataSrc;
        }else{
            for(var i=0; i<dataSrc.length; i++){
                var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
                $scope.province.push(provinceObj);
            }

            var xProvince = $scope.province;

            var xprov = [];
            for(var j=0; j<temp.length; j++){
	    	var xprovlist = [];
                var xstat=0;
                for(l=0; l<temp[j].child.length; l++){
                    for(k=0; k< xProvince.length; k++){
                        if(temp[j].child[l].ProvinceId === $scope.province[k].ProvinceId){
                            xprovlist.push(temp[j].child[l]);
                            xstat=1;

                        }   
                    }

                }
                if (xstat==1) {
                    var xj = temp[j]; //.child=[];
                    xj.listProvinceCity=[];
                    xj.listProvinceCity=xprovlist;
                    xprov.push(xj);

                } 
                
            }
            $scope.provinceData = xprov;

        }
    }
    

    $scope.changeArea = function(selected){
        // console.log(" area selected==>",selected);
        if(selected==null){
            filterProvince();
        }else{
            filterProvince({AreaId:selected});
        }
    }
    $scope.changeProvince = function(selected){
        if(selected==null){
            filterArea();
        }else{
            filterArea({ProvinceId:selected.ProvinceId});        
        }
    }

    $scope.onBeforeCancel = function(){
        filterArea();
        filterProvince();
    }

    $scope.onShowDetail = function(){
        $timeout(function(){
            $scope.provinceData = _.find($scope.areaProvince, {AreaId:$scope.mIntOutlet.AreaId});
        });
    }

    $scope.onBeforeNewMode = function(){
        $timeout(function(){
            filterArea();
            filterProvince();
        });  
    }




    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        enableRowHashing:false,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', width:'7%' , visible: false},
            { name:'Kode Outlet', field:'OutletCode' },
            { name:'Nama Outlet', field:'Name' },
            { name:'Alamat', field: 'Address' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };

});
