angular.module('app')
  .factory('InternalOutlet', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/param/OutletTAMs/?start=1&limit=100');
        return res;
      },
      create: function(outlet) {
        // console.log("create outlet ==>", outlet);
        // console.log("create", JSON.stringify(outlet));
        return $http.post('/api/param/OutletTAMs/Multiple', [outlet]);
      },
      update: function(outlet){
        return $http.put('/api/param/OutletTAMs/Multiple', [outlet]);
      },
      delete: function(outlet) {
        // console.log("outlet",outlet);
        var data = [];
        for(var i in outlet){
          data.push({OutletTAMId: outlet[i]});
        }
        // console.log("data delete InternalOutlet ==>",data);
        return $http.delete('/api/param/OutletTAMs/Multiple',{data: data, headers:{'Content-Type':'application/json'}});
      },
    }
  });