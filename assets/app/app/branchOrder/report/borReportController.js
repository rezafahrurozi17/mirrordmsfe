angular.module('app')
.controller('BORReportController', function($scope, CurrentUser, BORReport, hotRegisterer, $timeout) {
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.filter = {};

    $scope.reportData = [ 
                            {name:"RSSP",id:1},
                            {name:"RSSP Internal Order",id:2},
                            {name:"RSSP Fleet",id:3},
                            {name:"RSSP Profiling",id:4},
                            {name:"Achievement Evaluation",id:5},
                            {name:"Report OAP",id:6},
                            {name:"Report RAP",id:7}
                        ];
    $scope.getData = function() {
        BORReport.getData($scope.filter).then(
            function(res){
                renderHOTStructure(res.data.Result);
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    var renderHOTStructure = function(data){
        for(var i=0; i<data.length; i++){
            if(i>2){

            }
        }
    }
    $scope.hotSettings = {
        colWidths: [50,180,100,110,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40],
        nestedHeaders: [
            ['','','','','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE'],
            ['','','','','','','','','','','','','','',{label:'Data Histori', colspan:2},'Est',{label:'Periode Pemesanan',colspan:5},{label:'Rencana OAP/RAP',colspan:7},'',''],
            [{label:'Nama Variabel', colspan:2}, 'Tipe', 'Tampilan', 'N-12', 'N-11', 'N-10', 'N-9', 'N-8', 'N-7', 'N-6', 'N-5', 'N-4', 'N-3', 'N-2', 'N-1', 'N', 'N+1', 'N+2', 'N+3', 'N+4', 'N+5', 'N+6', 'N+7', 'N+8', 'N+9', 'N+10', 'N+11', 'N+12', 'ID', 'PRNID']
        ],
        fixedColumnsLeft:2,
        height: 500,
        width: angular.element(document.getElementById('rsspDetailContainer')).clientWidth,
        hiddenColumns: {
          columns: [4,5,6,7,8,9,10,11,12,13,29,30],
          indicators: false
        },
        columns: [
                  { data: 'ParentName', type: 'text', editor: false},
                  { data: 'AdjustmentName', type: 'text', editor: false},
                  { data: 'VariableType', type: 'text', editor: false},
                  { data: 'M_12' ,type: 'numeric'},
                  { data: 'M_11' ,type: 'numeric'},
                  { data: 'M_10' ,type: 'numeric'},
                  { data: 'M_9' ,type: 'numeric'},
                  { data: 'M_8' ,type: 'numeric'},
                  { data: 'M_7' ,type: 'numeric'},
                  { data: 'M_6' ,type: 'numeric'},
                  { data: 'M_5' ,type: 'numeric'},
                  { data: 'M_4' ,type: 'numeric'},
                  { data: 'M_3' ,type: 'numeric'},
                  { data: 'M_2' ,type: 'numeric'},
                  { data: 'M_1' ,type: 'numeric'},
                  { data: 'M' ,type: 'numeric'},
                  { data: 'M1' ,type: 'numeric'},
                  { data: 'M2' ,type: 'numeric'},
                  { data: 'M3' ,type: 'numeric'},
                  { data: 'M4' ,type: 'numeric'},
                  { data: 'M5' ,type: 'numeric'},
                  { data: 'M6' ,type: 'numeric'},
                  { data: 'M7' ,type: 'numeric'},
                  { data: 'M8' ,type: 'numeric'},
                  { data: 'M9' ,type: 'numeric'},
                  { data: 'M10' ,type: 'numeric'},
                  { data: 'M11' ,type: 'numeric'},
                  { data: 'M12' ,type: 'numeric'},
                  { data: 'AdjustmentRatioTypeId', type: 'numeric', editor: false},
                  { data: 'ParentId', type: 'numeric', editor: false},
        ],
    };
});
