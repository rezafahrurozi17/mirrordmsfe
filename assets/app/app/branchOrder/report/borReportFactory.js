angular.module('app')
  .factory('BORReport', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }
        var url = '';
        switch(filter.ReportType){
          case 1: url = '/api/ds/Reports/?start=1&limit=100&'; break;
          case 2: url = '/api/ds/Reports/?start=1&limit=100&'; break;
          case 3: url = '/api/ds/Reports/?start=1&limit=100&'; break;
          case 4: url = '/api/ds/Reports/?start=1&limit=100&'; break;
        }
        var res=$http.get(url+$httpParamSerializer(filter));
        return res;
      }
    }
  });