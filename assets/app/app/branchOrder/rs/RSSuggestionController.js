angular.module('app')
    .controller('RSSuggestionController', function($scope, $http, CurrentUser, uiGridConstants, OrgChart, ProductModel, Region, RSSuggestion, OAPPeriod, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        // $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRS = {year:0, detail:[]}; //Model
    $scope.xRS = {};
    $scope.xRS.selected=[];

    var alerts = [];
    var mDataCopy;

    // $scope.yearData = [ {name:"2016",id:2016},
    //                     {name: "2017",id:2017}];

    $scope.yearData = [];

    $scope.filter = {year:null, orgId:null, OutletId:null, GroupDealerId:null, areaId:null, provinceId:null};
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.areaId = null;
    $scope.provinceId = null;
    $scope.orgId = null;
    var myMode = 0;
    var editMode = 1;
    var viewMode = 0;
    var uploadMode = false;
    var processingMode = 2;

    var editableMode = false;

    var outletObj = null;
    var outletData = [];

    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.orgData= [];

    OrgChart.getDataTree().then(
        function(res){
            orgTreeData = res.data.Result;
            $scope.orgData = res.data.Result;
            for(i in $scope.orgData){
                for(ch in $scope.orgData[i].child){
                    outletData.push($scope.orgData[i].child[ch]);
                }
            }
            if($scope.orgData.child && $scope.orgData.child.length==1){
                $scope.filter.OutletId = $scope.orgData[0].id;
            }
            Region.getAreaProvince().then(
                function(res){
                    var areaProvRes = res.data.Result;
                    for(var i in areaProvRes){
                        var filteredOutlet = _.find(outletData, function(o){
                            return o.AreaId.indexOf(areaProvRes[i].AreaId)>=0 && o.ProvinceId==areaProvRes[i].ProvinceId;
                        });
                        if(filteredOutlet != null)
                            areaProvince.push(areaProvRes[i]);
                    }
                    filterArea();
                    filterProvince();
                    $scope.loading=false;
                }
            );

            $scope.loading=false;
            return res.data;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(filter[key]==null || filter[key]==undefined) continue;
                if(Array.isArray(data[i][key]) && !Array.isArray(filter[key])){
                    if(data[i][key].indexOf(filter[key])<0){
                        valid=false; break;
                    }
                }else if(!Array.isArray(data[i][key]) && Array.isArray(filter[key])){
                    if(filter[key].indexOf(data[i][key])<0){
                        valid=false; break;
                    }
                }else if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function getAreaProvinceFromHO(HO){
        $scope.provinceData = [];
        $scope.areaData = [];
        var dataSrc = HO.child;
        for(var i in dataSrc){
            var provinceObj = _.find(areaProvince,{ProvinceId: dataSrc[i].ProvinceId});
            var newObj = {ProvinceId:provinceObj.ProvinceId, ProvinceName:provinceObj.ProvinceName};
            if(_.findIndex($scope.provinceData, newObj)<0)
                $scope.provinceData.push(newObj);
            $scope.provinceData = _.sortBy($scope.provinceData, ['ProvinceName']);

            var areaObj = _.find(areaProvince,function(o){
                return dataSrc[i].AreaId.indexOf(o.AreaId)>=0;
            });
            var newObj = {AreaId:areaObj.AreaId, Description:areaObj.Description};
            if(_.findIndex($scope.areaData, newObj)<0)
                $scope.areaData.push(newObj);
            $scope.areaData = _.sortBy($scope.areaData, ['Description']);
        }
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
        if($scope.areaData.length==1){
            console.log("areaData=>",$scope.areaData);
            $scope.filter.areaId = $scope.areaData[0].AreaId;
        }
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
        if($scope.provinceData.length==1){
            // console.log("provinceData=>",$scope.areaData);
            $scope.filter.provinceId = $scope.provinceData[0].ProvinceId;
        }
    }
    function filterDealer(filter){
        $scope.orgData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = orgTreeData;
        }else{
            for(var i in orgTreeData){
                var inserted = false;
                for(var chIdx in orgTreeData[i].child){
                    var valid = true;
                    for(key in filter){
                        if(filter[key]==null || filter[key]==undefined) continue;
                        if(Array.isArray(orgTreeData[i].child[chIdx][key])){
                            if(orgTreeData[i].child[chIdx][key].indexOf(filter[key])<0){
                                valid=false; break;
                            }
                        }else if(orgTreeData[i].child[chIdx][key]!=filter[key]){
                            valid=false; break;
                        }
                    }
                    if(valid){
                        if(!inserted){
                            var parent = angular.copy(orgTreeData[i]);
                            parent.child = [];
                            parent.listOutlet = [];
                            dataSrc.push(parent);
                            inserted=true;
                        }
                        dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
                    }
                }
            }
        }
        $scope.orgData = dataSrc;
    }
    // $scope.changeFilterCombo = function(selected){
    //     $timeout(function(){
    //         if($scope.filter.OutletId!=null && $scope.filter.OutletId!=undefined){
    //             if(selected!=null && selected.HeadOfficeId)
    //                 outletObj = _.find(outletData,{OutletId:selected.HeadOfficeId});
    //             else if(selected!=null)
    //                 outletObj = _.find(outletData,{OutletId:$scope.filter.OutletId});
    //             else
    //                 outletObj = null;
    //         }else{
    //             outletObj = null;
    //         }
    //         if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null)
    //             filterProvince({AreaId:$scope.filter.areaId, ProvinceId:outletObj.ProvinceId});
    //         else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null)
    //             filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
    //         else{
    //             filterProvince({AreaId:$scope.filter.areaId});
    //         }

    //         if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null)
    //             filterArea({ProvinceId:$scope.filter.provinceId, AreaId:outletObj.AreaId});
    //         else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null)
    //             filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
    //         else{
    //             filterArea({ProvinceId:$scope.filter.provinceId});
    //         }

    //         filterDealer({ProvinceId:$scope.filter.provinceId, AreaId:$scope.filter.areaId});
    //     })
    // }

    $scope.changeFilterCombo = function(selected,sender){
        $timeout(function(){
            if(sender.name=='OutletId'){
                outletSel = selected;
                if($scope.filter.OutletId!=null && $scope.filter.OutletId!=undefined){
                    if(selected!=null && ('HeadOfficeId' in selected)){                        
                        getAreaProvinceFromHO(selected);
                        outletObj = _.find(outletData,{OutletId:selected.HeadOfficeId});
                        return;
                    }
                    else if(selected!=null){
                        if(selected.level==2){
                            // $scope.filter.OutletId = selected.id;
                            $scope.filter.GroupDealerId = selected.parent.id;
                        }else{
                            $scope.filter.GroupDealerId = selected.id;
                        }
                        outletObj = _.find(outletData,{OutletId:$scope.filter.OutletId});
                        if(selected.OutletId==selected.parent.HeadOfficeId){
                            var parentObj = _.find(orgTreeData,{HeadOfficeId:selected.OutletId});
                            getAreaProvinceFromHO(parentObj);
                            return;
                        }
                    }
                    else{
                        outletSel = null;
                        $scope.filter.GroupDealerId = null;
                        outletObj = null;
                    }
                }else{
                    outletObj = null;
                }
            }else if(outletObj==null || outletObj.parent.HeadOfficeId!=outletObj.OutletId){
                filterDealer({ProvinceId:$scope.filter.provinceId, AreaId:$scope.filter.areaId});
            }
            if(sender.name!='regionId'){
                // if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null)
                if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:$scope.filter.areaId, ProvinceId:outletObj.ProvinceId});
                else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
                else{
                    filterProvince({AreaId:$scope.filter.areaId});
                }                
            }

            if(sender.name!='areaId'){
                // if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null)
                if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:$scope.filter.provinceId, AreaId:outletObj.AreaId});
                else if(($scope.filter.provinceId==null || $scope.filter.provinceId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
                else{
                    filterArea({ProvinceId:$scope.filter.provinceId});
                }                
            }
        })
    }

    /*
    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.orgData= [];

    Region.getAreaProvince().then(
        function(res){
            areaProvince = res.data.Result;
            filterArea();
            filterProvince();
            $scope.loading=false;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
    }
    function filterDealer(filter){
        $scope.orgData = [];
        var dataSrc = [];
        console.log('filterDealer=',filter);
        if(filter==undefined || filter==null){
            dataSrc = orgTreeData;
        }else{
            for(var i in orgTreeData){
                var inserted = false;
                for(var chIdx in orgTreeData[i].child){
                    var valid = true;
                    for(key in filter){
                        if(orgTreeData[i].child[chIdx][key]!=filter[key]){
                            valid=false; break;
                        }
                    }
                    if(valid){
                        if(!inserted){
                            var parent = angular.copy(orgTreeData[i]);
                            parent.child = [];
                            parent.listOutlet = [];
                            dataSrc.push(parent);
                            inserted=true;
                        }
                        dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
                    }
                }
            }
        }
        $scope.orgData = dataSrc;
        console.log('$scope.orgData',$scope.orgData);
    }
    $scope.changeArea = function(selected){
        console.log("change area");
        if(selected==null){
            // console.log("selected==null");
            filterProvince();
            filterDealer($scope.filter.provinceId==null?null:{ProvinceId:$scope.filter.provinceId});
        }else{
            // console.log("selected!==null");
            filterProvince({AreaId:selected});
            filterDealer($scope.filter.provinceId==null?{AreaId:selected}:{AreaId:selected,ProvinceId:$scope.filter.provinceId});
        }
    }
    $scope.changeProvince = function(selected){
        console.log("change province");
        if(selected==null){
            // console.log("selected==null");
            filterArea();
            filterDealer($scope.filter.areaId==null?null:{AreaId:$scope.filter.areaId});
        }else{
            // console.log("selected!==null");
            filterArea({ProvinceId:selected});
            filterDealer($scope.filter.areaId==null?{ProvinceId:selected}:{ProvinceId:selected,AreaId:$scope.filter.areaId});
        }
    }
    */

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;

            $scope.customActionSettings[0].visible = mode==editMode;
            $scope.customActionSettings[1].visible = mode==editMode;
            $scope.customActionSettings[0].enable = mode!=processingMode;
            $scope.customActionSettings[1].enable = mode!=processingMode;
            
            $scope.customActionSettings[2].visible = mode==viewMode;
            $scope.customActionSettings[3].visible = mode==viewMode;
            $scope.customActionSettings[4].visible = ($scope.grid.data.length>0) && (mode == viewMode);
            $scope.grid.enableCellEditOnFocus = mode=editMode;
            myMode = mode;
        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                changeMode(processingMode);

                if(uploadMode)
                    RSSuggestion.create($scope.grid.data).then(function(){
                        $scope.getData();
                        changeMode(viewMode);
                        mode = viewMode;
                    });
                else
                    RSSuggestion.update($scope.grid.data).then(function(){
                        $scope.getData();
                        changeMode(viewMode);
                        mode = viewMode;
                    });
            },
            title: 'Simpan',
            visible:false
        },{
            func: function(string) {
                var generatedData = [];

                alerts=[];
                // if ($scope.filter.year==null || $scope.filter.GroupDealerId==null) {}
                if ($scope.filter.year==null) {
                    $scope.showAlert(alerts.join('\n'), alerts.length);
                } else {
                    if (outletSel==undefined){
                        angular.forEach($scope.orgData, function(org, orgIdx){
                            angular.forEach(org.child, function(dlr, dlrIdx){                        
                                angular.forEach($scope.productModelData, function(model, modelIdx){
                                    generatedData.push({Dealer:org.title, Outlet:dlr.title, Model:model.VehicleModelName, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                                });
                            });
                        });
                    } else if (outletSel.level == 1){
                        angular.forEach($scope.orgData, function(org, orgIdx){
                            angular.forEach(org.child, function(dlr, dlrIdx){                        
                                angular.forEach($scope.productModelData, function(model, modelIdx){
                                    if (dlr.parent.id == outletSel.id) {
                                        generatedData.push({Dealer:org.title, Outlet:dlr.title, Model:model.VehicleModelName, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                                    }
                                });
                            });
                        });
                    } else if (outletSel.level == 2) {
                        angular.forEach($scope.orgData, function(org, orgIdx){
                            angular.forEach(org.child, function(dlr, dlrIdx){                        
                                angular.forEach($scope.productModelData, function(model, modelIdx){
                                    if (dlr.OutletId == outletSel.OutletId) {
                                        generatedData.push({Dealer:org.title, Outlet:dlr.title, Model:model.VehicleModelName, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                                    }
                                });
                            });
                        });
                    }
                    XLSXInterface.writeToXLSX(generatedData, 'RS Suggestion - '+$scope.filter.year);
                }
            },
            title: 'Download Template',
            rightsBit: 1
        },{
            func: function(string) {
                alerts=[];
                // if ($scope.filter.year==null || $scope.filter.GroupDealerId==null) {}
                if ($scope.filter.year==null) {
                    $scope.showAlert(alerts.join('\n'), alerts.length);
                } else {
                    document.getElementById('uploadRS').click();
                }
            },
            title: 'Load File',
            rightsBit: 1
        },{
            func: function(string) {
                uploadMode=false;
                changeMode(editMode);
            },
            visible: false,
            title: 'Edit',
            rightsBit: 2
        }
    ];
    var outletData = [];
    function validateXLS(data){
        var result = [];
        var error = [];
        for(var i=0; i<data.length; i++){
            var dOutlet = _.find(outletData, { 'title': data[i].Outlet });
            var dModel = _.find($scope.productModelData, { 'name': data[i].Model });
            if(dOutlet == undefined){
                error.push('Outlet '+data[i].Outlet+' tidak ditemukan.');
                continue;
            }
            if(dModel == undefined){
                error.push('Model '+data[i].Model+' tidak ditemukan.');
                continue;
            }
            var newObj = {
                // Jan: parseInt(data[i].Jan),
                // Feb: parseInt(data[i].Feb),
                // Mar: parseInt(data[i].Mar),
                // Apr: parseInt(data[i].Apr),
                // May: parseInt(data[i].May),
                // Jun: parseInt(data[i].Jun),
                // Jul: parseInt(data[i].Jul),
                // Aug: parseInt(data[i].Aug),
                // Sep: parseInt(data[i].Sep),
                // Oct: parseInt(data[i].Oct),
                // Nov: parseInt(data[i].Nov),
                // Dec: parseInt(data[i].Dec),
                Jan: isNaN(parseInt(data[i].Jan)) ? 0 : (parseInt(data[i].Jan) < 0 ? 0 : parseInt(data[i].Jan)),
                Feb: isNaN(parseInt(data[i].Feb)) ? 0 : (parseInt(data[i].Feb) < 0 ? 0 : parseInt(data[i].Feb)),
                Mar: isNaN(parseInt(data[i].Mar)) ? 0 : (parseInt(data[i].Mar) < 0 ? 0 : parseInt(data[i].Mar)),
                Apr: isNaN(parseInt(data[i].Apr)) ? 0 : (parseInt(data[i].Apr) < 0 ? 0 : parseInt(data[i].Apr)),
                May: isNaN(parseInt(data[i].May)) ? 0 : (parseInt(data[i].May) < 0 ? 0 : parseInt(data[i].May)),
                Jun: isNaN(parseInt(data[i].Jun)) ? 0 : (parseInt(data[i].Jun) < 0 ? 0 : parseInt(data[i].Jun)),
                Jul: isNaN(parseInt(data[i].Jul)) ? 0 : (parseInt(data[i].Jul) < 0 ? 0 : parseInt(data[i].Jul)),
                Aug: isNaN(parseInt(data[i].Aug)) ? 0 : (parseInt(data[i].Aug) < 0 ? 0 : parseInt(data[i].Aug)),
                Sep: isNaN(parseInt(data[i].Sep)) ? 0 : (parseInt(data[i].Sep) < 0 ? 0 : parseInt(data[i].Sep)),
                Oct: isNaN(parseInt(data[i].Oct)) ? 0 : (parseInt(data[i].Oct) < 0 ? 0 : parseInt(data[i].Oct)),
                Nov: isNaN(parseInt(data[i].Nov)) ? 0 : (parseInt(data[i].Nov) < 0 ? 0 : parseInt(data[i].Nov)),
                Dec: isNaN(parseInt(data[i].Dec)) ? 0 : (parseInt(data[i].Dec) < 0 ? 0 : parseInt(data[i].Dec)),
                OutletId: dOutlet.OutletId,
                GroupDealerId: dOutlet.parent.id,
                GroupDealerName: dOutlet.parent.title,
                OutletName: dOutlet.title,
                OutletCode: dOutlet.code,
                VehicleModelName: dModel.name,
                VehicleModelId: dModel.id,
                Year: $scope.filter.year
            };
            var ttl = 0;
            ttl +=  newObj['Jan']+newObj['Feb']+newObj['Mar']+newObj['Apr']+
                    newObj['May']+newObj['Jun']+newObj['Jul']+newObj['Aug']+
                    newObj['Sep']+newObj['Oct']+newObj['Nov']+newObj['Dec'];
            newObj.Total = ttl;

            result.push(newObj);
        }
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadRS' ) );

        var a = myEl[0].files[0].name.split(".");
        var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        if (fileExt === "xlsx") {
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $timeout(function(){
                    myEl.value = '';
                    document.getElementById('uploadRS').value = '';
                    $scope.grid.data = validateXLS(json);
                    mDataCopy =  angular.copy($scope.grid.data);

                    uploadMode = true;
                    changeMode(editMode);
                });
            });
        } else {
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "File yang diupload bukan format .xlsx",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );   
        }
    };
    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];
    var outletSel;
    // $scope.selectOutlet = function(selected){
    //     // console.log(selected);
    //     if(selected){
    //         outletSel = selected;
    //         if(selected.level==2){
    //             // $scope.filter.OutletId = selected.id;
    //             $scope.filter.GroupDealerId = selected.parent.id;
    //         }else{
    //             $scope.filter.GroupDealerId = selected.id;
    //         }
    //     } else {
    //         outletSel = null;
    //         $scope.filter.OutletId = null;
    //         $scope.filter.GroupDealerId = null;
    //     }

    //     if(selected!=null){
    //         console.log(selected.id);
    //         outletObj = _.find(outletData,{OutletId:selected.id});
    //         console.log(outletObj);
    //         $scope.filter.areaId = outletObj.AreaId;
    //         $scope.filter.provinceId = outletObj.ProvinceId;
    //     }

    //     // $scope.changeFilterCombo(selected);
    // }
    OrgChart.getDataTree().then(
        function(res){
            // console.log(res);
            orgTreeData = res.data.Result;
            $scope.orgData = res.data.Result;
            for(i in $scope.orgData){
                for(ch in $scope.orgData[i].child){
                    outletData.push($scope.orgData[i].child[ch]);
                }
            }
            if($scope.orgData.length==1){
                $scope.filter.orgId = $scope.orgData[0].id;
                $scope.filter.OutletId = $scope.orgData[0].id;
            }
            $scope.loading=false;
            return res.data;
        }
    );
    // Region.getDataArea().then(
    //     function(res){
    //         $scope.areaData = res.data.Result;
    //         console.log($scope.areaData);
    //         $scope.loading=false;
    //         return res.data;
    //     }
    // );
    // Region.getDataProvince().then(
    //     function(res){
    //         $scope.provinceData = res.data.Result;
    //         console.log($scope.provinceData);
    //         $scope.loading=false;
    //         return res.data;
    //     }
    // );
    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );

    $scope.showAlert = function(message, number){
        $.bigBox({
            title: 'Mohon Input Filter',
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            // number: ''
        });
    };

    $scope.getData = function() {
        console.log("mau get data");
        alerts=[];
        // if ($scope.filter.year==null || $scope.filter.GroupDealerId==null) {}
        if ($scope.filter.year==null) {
            $scope.showAlert(alerts.join('\n'), alerts.length);
        } else {
            RSSuggestion.getData($scope.filter).then(
                function(res){
                    console.log(res.data.Result);
                    var total = 0;
                    for (var i in res.data.Result) {
                        total=0;
                        total+=
                        parseInt(res.data.Result[i]['Jan'])+parseInt(res.data.Result[i]['Feb'])+
                        parseInt(res.data.Result[i]['Mar'])+parseInt(res.data.Result[i]['Apr'])+
                        parseInt(res.data.Result[i]['May'])+parseInt(res.data.Result[i]['Jun'])+
                        parseInt(res.data.Result[i]['Jul'])+parseInt(res.data.Result[i]['Aug'])+
                        parseInt(res.data.Result[i]['Sep'])+parseInt(res.data.Result[i]['Oct'])+
                        parseInt(res.data.Result[i]['Nov'])+parseInt(res.data.Result[i]['Dec']);
                        // console.log(total);
                        res.data.Result[i].Total = total;
                    }
                    $scope.grid.data = res.data.Result;

                    mDataCopy =  angular.copy($scope.grid.data);

                    $scope.loading=false;
                    changeMode(viewMode);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    OAPPeriod.getData().then(
        function(res){
            // $scope.yearData = res.data.Result;
            var temp = res.data.Result;
            $scope.yearData = [];
            var obj={};
            for (var i in temp) {
                if (!obj[temp[i].Year]) {
                    obj[temp[i].Year] = {}; 
                    obj[temp[i].Year]["name"] = temp[i].Year;
                    obj[temp[i].Year]["id"] = temp[i].Year;
                    $scope.yearData.push(obj[temp[i].Year]);
                }                    
            }
            
            console.log("$scope.yearData : "+JSON.stringify($scope.yearData));
            $scope.loading=false;
            return res.data;
        }
    );

    $scope.formApi={};
    
    $scope.grid = {
        enableSorting: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableCellEdit: false,
        enableCellEditOnFocus: true, 
        cellEditableCondition: function($scope) {
            return editableMode; 
        },
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        enableCellEditOnFocus: false,
        columnDefs: [
            { name:'ProvinceId', field:'ProvinceId', visible:false },
            { name:'ProvinceName', field:'ProvinceName', visible:false },
            { name:'AreaId',    field:'AreaId', visible:false },
            { name:'Year',    field:'Year', visible:false },
            { name:'Period',    field:'Period', visible:false },
            { name:'GroupDealerId', field:'GroupDealerId', visible: false},
            { name:'Dealer', field:'GroupDealerName', enableCellEdit: false},
            { name:'OutletId', field:'OutletId', visible:false},
            { name:'OutletCode', field:'OutletCode', visible:false},
            { name:'Outlet', field:'OutletName', enableCellEdit: false},
            { name:'VehicleModelId', field:'VehicleModelId', visible:false},
            { name:'Model', field: 'VehicleModelName', enableCellEdit: false},
            { name:'Jan', field: 'Jan', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Feb', field: 'Feb', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Mar', field: 'Mar', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Apr', field: 'Apr', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'May', field: 'May', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jun', field: 'Jun', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jul', field: 'Jul', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Aug', field: 'Aug', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Sep', field: 'Sep', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Oct', field: 'Oct', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Nov', field: 'Nov', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Dec', field: 'Dec', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'JanId', field: 'JanId' , visible: false},
            { name:'FebId', field: 'FebId' , visible: false},
            { name:'MarId', field: 'MarId' , visible: false},
            { name:'AprId', field: 'AprId' , visible: false},
            { name:'MayId', field: 'MayId' , visible: false},
            { name:'JunId', field: 'JunId' , visible: false},
            { name:'JulId', field: 'JulId' , visible: false},
            { name:'AugId', field: 'AugId' , visible: false},
            { name:'SepId', field: 'SepId' , visible: false},
            { name:'OctId', field: 'OctId' , visible: false},
            { name:'NovId', field: 'NovId' , visible: false},
            { name:'DecId', field: 'DecId' , visible: false},
            { name:'Total', field: 'Total' }
        ]
    };

    $scope.afterRegisterGridApi = function(gridApi) {
        $scope.gridApi = gridApi;
        console.log($scope.gridApi);

        $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            console.log('Inside afterCellEdit..');
            if (newValue<0) {
                newValue = 0;
                rowEntity[colDef.field] = newValue;
            }
            rowEntity.Total+=(newValue-oldValue);
        });
        // uiGridApi.core.registerColumnsProcessor(function(columns, rows){
        //     angular.forEach(columns, function(col){
        //         var filterFormat = col.colDef.filterFormat; 
        //         if ( filterFormat ){ col.filterFormat = filterFormat; }
        //     });
        //     return rows;
        // }, 40);
    };

});
