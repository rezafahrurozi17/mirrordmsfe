angular.module('app')
  .factory('RSSuggestion', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }
        var res=$http.get('/api/ds/RSSuggestions/?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;
      },
      update: function(rs) {
        return $http.put('/api/ds/RSSuggestions/Multiple', rs);
      },
      create: function(rs) {
        return $http.post('/api/ds/RSSuggestions/Multiple', rs);
      },
    }
  });