angular.module('app')
  .factory('GetsudoPeriod', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var res=$http.get('/branchOrder/getsudoperiod');
        var res=$http.get('/api/ds/GetsudoPeriods?Start=1&Limit=1000');//&GetsudoPeriodCode=&Year=
        console.log('res=>',res);
        
        
        return res;
      },
      create: function(getsudoperiod) {
        
            console.log("create==>",JSON.stringify(getsudoperiod));
            console.log("create==>",getsudoperiod.StartPeriod.toString());

            return $http.post('/api/ds/GetsudoPeriods/Multiple', [getsudoperiod]);
      },
      update: function(getsudoperiod){
        
         console.log("update==>",JSON.stringify(getsudoperiod));
         console.log("update==>",getsudoperiod.StartPeriod.toString());
         return $http.put('/api/ds/GetsudoPeriods/Multiple', [getsudoperiod]);
      },
      delete: function(getsudoperiod) {
        // return $http.post('/branchOrder/getsudoperiod/delete',{id:id});
        console.log("delete==>",getsudoperiod);
        // var b =[];
        // for (var i = 0; i < getsudoperiod.length ; i++) {
        //   var c={};
        //   c.GetsudoPeriodId=getsudoperiod[i];
        //   b.push(c);
        // };
        // console.log("delete=>",b);
        return $http.delete('/api/ds/GetsudoPeriods/Multiple/',getsudoperiod);
      },
      statusPutOn: function(getsudoperiod){

        console.log("updateOn==>",getsudoperiod);
        return $http.put('/api/ds/GetsudoPeriodDetails/Open',getsudoperiod);
      },
      statusPutOff: function(getsudoperiod){
        console.log("updateOff==>",getsudoperiod);
        return $http.put('/api/ds/GetsudoPeriodDetails/Close',getsudoperiod);
      },
      getDetailData: function(getsudoperiod) {
        console.log("Detail==>",getsudoperiod);
        var xparam='';
        if (getsudoperiod.areaid != '' && getsudoperiod.areaid != undefined) {
          xparam = xparam + '&areaId='+getsudoperiod.areaid;
        };
        if (getsudoperiod.provinceid != '' && getsudoperiod.provinceid != undefined) {
          xparam = xparam +'&provinceId='+getsudoperiod.provinceid;
        };
        if (getsudoperiod.dealerid != '' && getsudoperiod.dealerid != undefined) {
          xparam = xparam + '&GroupDealerId='+getsudoperiod.dealerid;
        };
        if (getsudoperiod.outletid != '' && getsudoperiod.outletid != undefined) {
          xparam = xparam + '&outletId='+getsudoperiod.outletid;
        };
        console.log("param==>",'/api/ds/GetsudoPeriodDetails?GetsudoPeriodId='+getsudoperiod.prdid+'&start=1&Limit=1000'+xparam);
         return $http.get('/api/ds/GetsudoPeriodDetails?GetsudoPeriodId='+getsudoperiod.prdid+'&start=1&Limit=1000'+xparam);//'&GroupDealerId='+getsudoperiod.dealerid+'&outletId='+getsudoperiod.outletid+

      },
    }
  });