angular.module('app')
    .controller('GetsudoPeriodController', function($scope, $http, CurrentUser, GetsudoPeriod,Region,OrgChart,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        $scope.province();
        $scope.xarea();
        $scope.dealer();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPeriod= {}; //Model
    $scope.cPeriod = null; //Collection
    $scope.xPeriod = {};
    $scope.xPeriod.selected=[];

    $scope.Criteria = {prdid:'',prdCode:'',prdName:'',areaid:'',dealerid:'',yearid:'',provinceid:'',outletid:'',xYear:''};

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.checkRights=function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        return res;
    }
    $timeout(function(){
        console.log('hidde==',!$scope.checkRights(2));

        $scope.actionHide=!$scope.checkRights(2);
    })
    
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.startDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.endDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.startEditDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.endEditDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.startHOReviewDateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };

    $scope.startDateChange = function(dt){
        // console.log("change->",$scope.mPeriod,dt);
        $scope.endDateOptions.minDate = dt;

        $scope.startEditDateOptions.minDate = dt;
        $scope.startEditDateOptions.maxDate = $scope.mPeriod.EndPeriod;

        $scope.endEditDateOptions.minDate = dt;
        $scope.endEditDateOptions.maxDate = $scope.mPeriod.EndPeriod;

        // $scope.startHOReviewDateOptions.minDate = $scope.mPeriod.EditEndPeriod;
        // $scope.startHOReviewDateOptions.maxDate = $scope.mPeriod.EndPeriod;

    }
    $scope.endDateChange = function(dt){
        $scope.startEditDateOptions.minDate = $scope.mPeriod.StartPeriod;
        $scope.startEditDateOptions.maxDate = dt;
        $scope.endEditDateOptions.minDate = $scope.mPeriod.StartPeriod;
        $scope.endEditDateOptions.maxDate = dt;
        // $scope.startHOReviewDateOptions.maxDate = dt;
    }
    $scope.startEditDateChange = function(dt){
        $scope.endEditDateOptions.minDate = dt;
        $scope.startHOReviewDateOptions.minDate = dt;
    }
    $scope.endEditDateChange = function(dt){
        // $scope.startHOReviewDateOptions.minDate = dt;
         $scope.startHOReviewDateOptions.maxDate = dt;
    }
    $scope.startHOReviewDateChange = function(dt){
        //$scope.endDateOptions.minDate = $scope.mPeriod.startDate;
        //console.log("change->",dt);
    }
    //----------------------------------
    // Get Data
    //----------------------------------
    
    $scope.xarea = function() {
        Region.getDataArea().then(
                function(res){
                    console.log("Area=>",res.data);
                    $scope.areaData = res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );

    }
    $scope.dealer = function() {
        OrgChart.getDataTree().then(
                function(res){
                    console.log("Dealer=>",res.data);
                    $scope.dealerData = res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
    }
    $scope.selectDealer = function(xDealer){
        // console.log("select dealeer==>",xDealer);
        $scope.outletData = xDealer.child;
    }
    
    $scope.province = function() {
        Region.getDataProvince().then(
                function(res){
                    console.log("province=>",res.data);
                    $scope.provinceData = res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
    }
    
    $scope.getData = function() {
        GetsudoPeriod.getData().then(
            function(res){
               $scope.grid.data = res.data.Result;
                console.log("dataSudoPeriod=>",res.data);
                $scope.prdData=res.data.Result;
                $scope.GetsudoPeriodData = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.showDetailPanel = false;
    $scope.getDataDetail = function() {
        $scope.loadingDetail = true;
        GetsudoPeriod.getDetailData($scope.Criteria).then(
            function(res){

                // $scope.gridDetail.data = res.data;
                // console.log("detail=>",res.data);
                // $scope.GetsudoPeriodDetailData = res.data;
                // $scope.showDetailPanel = true;
                // $scope.loadingDetail=false;
                // return res.data;
                var xdata = res.data.Result;
                for (var i = 0; i < xdata.length;  i++) {
                    xdata[i].xstat = null;
                    if (xdata[i].OpenState) {
                        xdata[i].xstat = "OPEN"
                    } else{
                        xdata[i].xstat = "CLOSE"
                    };
                };
                console.log("new data detail==>",xdata); 
                $scope.gridDetail.data = xdata; // res.data.Result;
                // console.log("detail=>",res.data.Result);
                $scope.GetsudoPeriodDetailData = xdata; // res.data.Result;
                $scope.showDetailPanel = true;
                $scope.loadingDetail=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.selectRole = function(rows){
        // console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.onShowDetail = function(row,mode){
        console.log("showDetail->",row,mode);
        var d = new Date(row.StartPeriod);

        $scope.Criteria.prdid = row.GetsudoPeriodId;
        $scope.Criteria.prdCode = row.GetsudoPeriodCode;
        $scope.Criteria.prdName = row.GetsudoPeriodName;
        $scope.Criteria.xYear = d.getFullYear();
        console.log("criteria==>",$scope.Criteria);
        $scope.startDateChange(row.StartPeriod);
        $scope.showDetailForm = (mode=='view' ? true:false);
        $scope.gridDetail.data = [];
    }
    $scope.onBeforeCancel = function(row,mode){
        $scope.showDetailForm = false;
        $scope.showDetailPanel = false;
        $scope.getData();

    }
    $scope.searchStat = true;
    $scope.toggleSearch = function(){
        // console.log("toggleSearch->");
        $scope.searchStat = ! $scope.searchStat;
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'id',    field:'id', width:'7%' },

            { name:'code', field:'GetsudoPeriodCode' },
            { name:'name', field:'GetsudoPeriodName' },
            { name:'Start Date', field:'StartPeriod', cellFilter: dateFilter },
            { name:'End Date', field:'EndPeriod', cellFilter: dateFilter },
            { name:'Start Edit', field:'EditStartPeriod', cellFilter: dateFilter },
            { name:'End Edit', field:'EditEndPeriod', cellFilter: dateFilter },
            { name:'Start HO Review', field:'StartDateHOReview', cellFilter: dateFilter },
            { name:'Open', field:'TotalOpen' },
            { name:'Close', field:'TotalClose' },
            // { name:'open',  field: 'status',
            //   cellTemplate: '<label style="margin-top:5px;margin-left:5px;">'+
            //                     '<input type="checkbox"'+
            //                         ' class="checkbox style-0"'+
            //                         ' onclick="this.checked=!this.checked;"'+
            //                         ' ng-true-value="1"'+
            //                         ' ng-false-value="0"'+
            //                         ' ng-model="row.entity.status"'+
            //                         ' ng-checked="row.entity.status==1"'+
            //                         '/>'+
            //                     '<span></span>'+
            //                 '</label>'
            // },
            // { name:'desc',  field: 'desc' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };

    //-----------------------------------
    //GRID DETAIL
    //-----------------------------------
    var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
                                    ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                                    ' onclick="this.blur()"'+
                                    ' ng-click="grid.appScope.gridClickToggleStatus(row.entity)">'+
                                    '<i ng-class="'+
                                                      '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.OpenState,'+
                                                      '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.OpenState,'+
                                                      '}'+
                                    '">'+
                                    '</i>'+
                                '</button>';

    $scope.gridDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering: true,
            paginationPageSizes: [5,10,25,50,75,100],
            paginationPageSize: 10,
            enableHorizontalScrollbar: 2,
            enableVerticalScrollbar: 2,
            enableColumnMenus:false,
            columnDefs: [
              // { name:'id', field: 'id',width:'7%' },
              { name:'area',  field: 'Area'},  //area
              { name:'province', field: 'ProvinceName'}, //province
              { name:'dealer', field: 'GroupDealerName'}, //dealer
              { name:'outlet', field: 'Outlet'}, //outlet
              { name:'status', field: 'xstat'}, //status
              { name:'action', allowCellFocus: false, width:100, pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionEditTemplate
              },
            ]
    };

    $scope.gridDetail.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiDetail = gridApi;

        $scope.gridApiDetail.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedDetailRows = $scope.gridApiDetail.selection.getSelectedRows();
                    console.log('selected rows=>',$scope.selectedRows);
        });
        $scope.gridApiDetail.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedDetailRows = $scope.gridApiDetail.selection.getSelectedRows();
                  console.log('selected rows=>',$scope.selectedRows);
        });
    };

    $scope.gridDetailCols=[];  //= angular.copy(scope.grid.columnDefs);
    var x=-1;
    for(var i=0;i<$scope.gridDetail.columnDefs.length-1;i++){
      if($scope.gridDetail.columnDefs[i].visible==undefined){
        x++;
        $scope.gridDetailCols.push($scope.gridDetail.columnDefs[i]);
        $scope.gridDetailCols[x].idx = i;
      }
    }
    $scope.gridDetailFilterText = '';
    $scope.filterBtnChange = function(col){
      $scope.filterBtnLabel = col.name;
      $scope.filterColIdx = (col.idx)+1;
      console.log("col=>",col,$scope.filterColIdx);
    }
    $timeout(function(){
      $scope.filterBtnChange($scope.gridDetailCols[0]);
    },0);
    // console.log("gridCols=>",$scope.gridDetailCols);
    $scope.filterBtnLabel = $scope.gridDetailCols[0].name;
    //
    //
    
    $scope.gridClickToggleStatus = function(row){

          console.log("grid click status=>",row);
          row.OpenState = !row.OpenState;
            // console.log(row.OpenState);
            // console.log(!row.OpenState);
            var cc = [row.OutletGetsudoId];
            // console.log("===>",cc);
          if (row.OpenState) {
                GetsudoPeriod.statusPutOn(cc).then(
                    function(res){
                        // console.log("hore");
                        $scope.getDataDetail();
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );  
          } else{
            GetsudoPeriod.statusPutOff(cc).then(
                    function(res){
                        $scope.getDataDetail();
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
          };

          

          // row.status = (row.status==0? 1 : 0);
    }
    
    $scope.actOpenAll = function(xdata) {
        // console.log("param",xdata);
        var detId = [];
        for (var i = 0; i < xdata.length; i++) {
            detId.push(xdata[i].OutletGetsudoId);
        };
        console.log("Criteria",$scope.Criteria);
        GetsudoPeriod.statusPutOn(detId).then(
                    function(res){
                        $scope.getDataDetail();
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
        console.log("data Open==>",detId);
    };
    $scope.actCloseAll = function(xdata) {
        // console.log("param",xdata);
        var detId = [];
        for (var i = 0; i < xdata.length; i++) {
            detId.push(xdata[i].OutletGetsudoId);
        };
        GetsudoPeriod.statusPutOff(detId).then(
                    function(res){
                        $scope.getDataDetail();
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
        console.log("data Close==>",detId);
    };
    $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           var filterHeight = 40; // your filter height
           var pageSize = $scope.gridDetail.paginationPageSize;
           if($scope.gridDetail.paginationPageSize > $scope.gridDetail.data.length){
                pageSize = $scope.gridDetail.data.length;
           }
           if(pageSize<4){
              pageSize=3;
           }
           return {
              height: (pageSize * rowHeight + headerHeight)+42 + "px"
           };
    };


});
