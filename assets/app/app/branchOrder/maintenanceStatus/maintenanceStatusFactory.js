angular.module('app')
  .factory('MaintenanceStatus', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var res=$http.get('api/LogJobs/?start=1&limit=10');
        var res=$http.get('/api/ds/DataTransferLists?start=1&limit=100');
            
        console.log('ressss=>',res);
        return res;
      },
      getDataLog: function() {
        // var res=$http.get('api/LogJobs/?start=1&limit=10');
        var res=$http.get('/api/ds/DataTransferLogs?start=1&limit=100');
            
        console.log('ressss=>',res);
        return res;
      },
      getDataCalculation: function() {
        var res=$http.get('/api/ds/AutoCalculationLists?start=1&limit=100');
           
        console.log('ressss=>',res);
        return res;
      },
      getDataCalculationLog: function() {
        var res=$http.get('/api/ds/AutoCalculationLogs?start=1&limit=100');
           
        console.log('ressss=>',res);
        return res;
      },
      startTransfer: function(maintenanceStatus) {
          console.log("starrrt==>",JSON.stringify(maintenanceStatus));
          var res=$http.post('/api/ds/DataTransferStartJobs',maintenanceStatus);

          return res;
      },
      startJob: function(maintenanceStatus){
          console.log("startJob==>",JSON.stringify(maintenanceStatus));
          var res=$http.get('/api/ds/AutoCalculationStartJobs',maintenanceStatus);

          return res;
      },
    }
  });