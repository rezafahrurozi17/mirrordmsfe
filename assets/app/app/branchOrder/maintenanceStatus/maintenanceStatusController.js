angular.module('app')
    .controller('MaintenanceStatusController', function($scope, $http, CurrentUser, MaintenanceStatus,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        $scope.getDataCalculation();
        $scope.showDetailPanel = false;
        $scope.actionHide=true;
        //$scope.showDetailForm = true;
        //$scope.loadingDetail = false;

    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMaintenance= {}; //Model
    var dateFilter='date:"dd/MM/yyyy"';
    
    // var dateFormat='dd/MM/yyyy';
    // var dateFilter='date:"dd/MM/yyyy"';
    //----------------------------------
    // Get Data
    //----------------------------------

    $scope.getData = function() {
        MaintenanceStatus.getData().then(
            function(res){
                // $scope.grid.data = res.data.Result;
                // console.log("GetsudoPeriod=>",res);
                // $scope.GetsudoPeriodData = res.data.Result;
                // $scope.loading=false;
                // return res.data.Result;

                $scope.grid.data = res.data.Result;
                // $scope.gridDetail.data = res.data.Result;
                // console.log("GetsudoPeriod=>",res);
                // $scope.showDetailPanel = true;
                // $scope.showDetailForm = (mode=='view' ? true:false);
                $scope.loading=false;
                return res.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    $scope.getDataCalculation = function() {
        MaintenanceStatus.getDataCalculation().then(
            function(res){
               
                $scope.gridDetail.data = res.data.Result;
                // console.log("Calculation=>",res);
                // $scope.showDetailPanel = true;
                // $scope.showDetailForm = (mode=='view' ? true:false);
                $scope.loading=false;
                return res.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onShowDetail = function(row,mode){
        
        // $scope.showDetailForm = (mode=='view' ? true:false);
        // $scope.gridDetail.data = [];
        // $scope.showDetailPanel = true;
        $scope.showDetailForm = (mode=='view' ? true:false);
    }
    $scope.onStart = function(rows){
        console.log("onSelectRowssss=>",rows);
        
    }
    $scope.onStartJob = function(x){
        console.log("select Job==>",x);
            MaintenanceStatus.startJob(x).then(
                function(res){
                    // console.log("sukses start transfer==>",res);
                    $scope.getDataCalculation();
                }
            );
        
    }
    $scope.onLogJob = function(x){
        // console.log("onSelectRows=>",rows);
        // console.log("select LogJob==>",x);
            // console.log("api=>",$scope.formApi);
            MaintenanceStatus.getDataCalculationLog(x).then(
                function(res){
                    console.log("sukses start transfer==>",res);
                }
            );
        
    }
     $scope.customBulkSettings = [
        {
          func: function() {
            // console.log("func_openall=>",$scope);
            // merubah di bsFromGrid, dibagia custom bulk menambah kiriman parameter selected rows
            console.log("select ==>",xdata);
            // console.log("api=>",$scope.formApi);
            MaintenanceStatus.startTransfer(xdata).then(
                function(res){
                    console.log("sukses start transfer==>",res);
                    $scope.getData();
                }
            );
          },
          title: 'Start'
        },
        // {
        //   func: function(x) {
        //     // console.log("func_closeall=>",$scope);
        //     console.log("select log==>",x);
        //     MaintenanceStatus.getDataLog(x).then(
        //         function(res){
        //             console.log("sukses start transfer==>",res);
        //         }
        //     );
        //   },
        //   title: 'Log'
        // },
    ];
    var xdata=[];
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
        xdata=rows;
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        gridInlineEdit: false,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'id',    field:'id', width:'7%' },
            { name:'Function Name', field:'JobName' },
            { name:'Start Running', field:'StartRunning', cellFilter: dateFilter}, //, cellFilter: dateFilter 
            { name:'Execution Time', field:'ExecutionTime'},
            { name:'Record Count', field:'RecordCount'},
            { name:'Status', field:'Status'},
            { name:'Keterangan', field:'Information'}, 
            
        ]
    };

    //form 2 grid di depan

    $scope.gridDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering : true,
            paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 15,
            columnDefs: [
                // { name:'id',    field:'id', width:'7%' },
                { name:'Job Name', field:'JobName' },
                { name:'Start Running', field:'StartRunning' , cellFilter: dateFilter}, //, cellFilter: dateFilter 
                { name:'Execution Time', field:'ExecutionTime'},
                // { name:'Record Count', field:'RecordCount'},
                { name:'Status', field:'Status'},
                { name:'Keterangan', field:'Information'}, 
            ]
    };

    $scope.gridDetail.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiDetail = gridApi;

        $scope.gridApiDetail.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedDetailRows = $scope.gridApiDetail.selection.getSelectedRows();
                    // console.log('selected rows=>',$scope.selectedRows);
        });
        $scope.gridApiDetail.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedDetailRows = $scope.gridApiDetail.selection.getSelectedRows();
                  // console.log('selected rows=>',$scope.selectedRows);
        });
    };

    $scope.gridDetailCols=[];  //= angular.copy(scope.grid.columnDefs);
    var x=-1;
    for(var i=0;i<$scope.gridDetail.columnDefs.length-1;i++){
      if($scope.gridDetail.columnDefs[i].visible==undefined){
        x++;
        $scope.gridDetailCols.push($scope.gridDetail.columnDefs[i]);
        $scope.gridDetailCols[x].idx = i;
      }
    }
    $scope.gridDetailFilterText = '';
    $scope.filterBtnChange = function(col){
      $scope.filterBtnLabel = col.name;
      $scope.filterColIdx = (col.idx)+1;
      // console.log("col=>",col,$scope.filterColIdx);
    }
    $timeout(function(){
      $scope.filterBtnChange($scope.gridDetailCols[0]);
    },0);
    // console.log("gridCols=>",$scope.gridDetailCols);
    $scope.filterBtnLabel = $scope.gridDetailCols[0].name;
    //
    //end grid depan


    //form 3 grid detail

    $scope.gridFormDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name:'id',    field:'id', width:'7%' },
                { name:'Job Name', field:'JobName' },
                { name:'Start Running', field:'StartRunning', cellFilter: dateFilter}, //, cellFilter: dateFilter 
                { name:'Execution Time', field:'ExecutionTime'},
                // { name:'Record Count', field:'RecordCount'},
                { name:'Status', field:'Status'},
                { name:'Keterangan', field:'Information'}, 
            ]
    };

    $scope.gridFormDetail.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiFormDetail = gridApi;

        $scope.gridApiFormDetail.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedDetailRows = $scope.gridApiFormDetail.selection.getSelectedRows();
                    // console.log('selected rows=>',$scope.selectedRows);
        });
        $scope.gridApiFormDetail.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedDetailRows = $scope.gridApiFormDetail.selection.getSelectedRows();
                  // console.log('selected rows=>',$scope.selectedRows);
        });
    };

    $scope.gridFormDetailCols=[];  //= angular.copy(scope.grid.columnDefs);
    var x=-1;
    for(var i=0;i<$scope.gridFormDetail.columnDefs.length-1;i++){
      if($scope.gridFormDetail.columnDefs[i].visible==undefined){
        x++;
        $scope.gridFormDetailCols.push($scope.gridFormDetail.columnDefs[i]);
        $scope.gridFormDetailCols[x].idx = i;
      }
    }
    $scope.gridFormDetailFilterText = '';
    $scope.filterBtnChange = function(col){
      $scope.filterBtnLabel = col.name;
      $scope.filterColIdx = (col.idx)+1;
      // console.log("col=>",col,$scope.filterColIdx);
    }
    $timeout(function(){
      $scope.filterBtnChange($scope.gridFormDetailCols[0]);
    },0);
    // console.log("gridCols=>",$scope.gridDetailCols);
    $scope.filterBtnLabel = $scope.gridFormDetailCols[0].name;
    //
    //end grid detail

});
