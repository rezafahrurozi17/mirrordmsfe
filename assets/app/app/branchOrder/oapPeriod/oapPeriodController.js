angular.module('app')
    .controller('OAPPeriodController', function($scope, $http, CurrentUser, OAPPeriod,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPeriod= {}; //Model
    $scope.cPeriod = null; //Collection
    $scope.xPeriod = {};
    $scope.xPeriod.selected=[];

    $scope.periodTypeData = [
                               {MasterPeriodOAPRAPType:1,type:'OAP'},
                               {MasterPeriodOAPRAPType:2,type:'RAP'},
                            ];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.dateOptions = {
        // maxDate: new Date(2016, 9, 29),
        // minDate: new Date(),
        startingDay: 1,
        format: dateFormat,
        disableWeekend: 1
    };

    $scope.selected = {id:6,name:"celine"};
    $scope.list = [
                    {id:1,name:"john"},
                    {id:2,name:"bill"},
                    {id:3,name:"charlie"},
                    {id:4,name:"oscar"},
                    {id:5,name:"marie"},
                    {id:6,name:"celine"},
                    {id:7,name:"brad"},
                    {id:8,name:"drew"},
                    {id:9,name:"rebecca"},
                    {id:10,name:"michel"},
                    {id:11,name:"francis"},
                    {id:12,name:"jean"},
                    {id:13,name:"paul"},
                    {id:14,name:"pierre"},
                    {id:15,name:"nicolas"},
                    {id:16,name:"alfred"},
                    {id:17,name:"gerard"},
                    {id:18,name:"louis"},
                    {id:19,name:"albert"},
                    {id:20,name:"edward"},
                    {id:21,name:"benoit"},
                    {id:22,name:"guillaume"},
                    {id:23,name:"nicolas"},
                    {id:24,name:"joseph"},
                    {id:25,name:"edward"}
    ];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        OAPPeriod.getData().then(
            function(res){
                console.log("res data oap period", res);
                $scope.grid.data = res.data.Result;
                console.log("role=>",res.data.Result);
                $scope.OAPPeriodData = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'MasterPeriodOAPRAPId', width:'7%' , visible:false  },
            { name:'type', field:'MasterPeriodOAPRAPType' , cellFilter:'mapPeriodType', editType:'dropdown',
                                                               editDropDownOptionsArray: $scope.periodTypeData,
                                                               // editDropdownIdLabel: 'value',
                                                               editDropdownValueLabel: 'type',
                                                               editableCellTemplate: 'ui-grid/dropdownEditor',
            },
            // { name:'code', field:'code' },
            { name:'Year', field:'Year' },
            { name:'Start Date', field:'StartDate', cellFilter: dateFilter },
            { name:'End Date', field:'EndDate', cellFilter: dateFilter },
            // { name:'open',  field: 'status',
            //   cellTemplate: '<label style="margin-top:5px;margin-left:5px;">'+
            //                     '<input type="checkbox"'+
            //                         ' class="checkbox style-0"'+
            //                         ' onclick="this.checked=!this.checked;"'+
            //                         ' ng-true-value="1"'+
            //                         ' ng-false-value="0"'+
            //                         ' ng-model="row.entity.status"'+
            //                         ' ng-checked="row.entity.status==1"'+
            //                         '/>'+
            //                     '<span></span>'+
            //                 '</label>'
            // },
            // { name:'desc',  field: 'desc' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
