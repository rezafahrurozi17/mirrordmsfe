angular.module('app')
  .factory('OAPPeriod', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/ds/MasterPeriodOAPRAPs?start=1&limit=10');
        return res;
      },
      create: function(oapperiod) {
        return $http.post('/api/ds/MasterPeriodOAPRAPs', oapperiod);      },
      update: function(oapperiod){
        return $http.put('/api/ds/MasterPeriodOAPRAPs/PutMultipleMasterPeriodOAPRAP', oapperiod);
         },
      delete: function(id) {
        console.log("id delete",id);

        var aa = [];
        var bb = [];
        
         for (var i = 0; i < id.length ; i++) {
           var cc={};
           cc.MasterPeriodOAPRAPId=id[i];
           aa.push(cc);
         };

         if (aa.length>0) {
              bb=aa;
            } else {
              var cc={};
               cc.MasterPeriodOAPRAPId = id;
               bb.push(cc);
            } 
            
            console.log("delete==>",JSON.stringify(bb));
            console.log("delete2==>",bb);

        return $http.delete('/api/ds/MasterPeriodOAPRAPs/Multiple', {data:bb,headers: {'Content-Type': 'application/json'}});
      },
    }
  });

  // $http.delete('projects/' + id + '/activityTypes',
  //   {data: activitiesToDelete, headers: {'Content-Type': 'application/json'}});