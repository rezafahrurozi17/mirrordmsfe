angular.module('app')
    .controller('RSSPModelController_Clean_1', function($scope, $http, CurrentUser, CustomerFleet,$timeout, hotRegisterer) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mFleet = null; //Model
    $scope.cFleet = null; //Collection
    $scope.xFleet = {};
    $scope.xFleet.selected=[];

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.cFleet = CustomerFleet.getData().then(
            function(res){
                $scope.grid.data = res.data;
                // console.log("role=>",res.data);
                $scope.fleetData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.titleArr = [
                            ['APRIL GETSUDO','','Historical Data','','Est.','Ordering Period','','','','','OAP/RAP Plan'],
                            ['','','','','Est.','Fix','Flexible','Flexible','Flexible','Flexible'],
                            ['AVANZA','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
                            ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    ];
    $scope.dataTitle = [];
    for(var i=0;i<$scope.titleArr.length;i++){
        var titleList=$scope.titleArr[i];
        var row={};
        row.title = titleList[0];
        row.title2 = titleList[1];
        row.m1 = titleList[2];
        row.m2 = titleList[3];
        row.m3 = titleList[4];
        row.m4 = titleList[5];
        row.m5 = titleList[6];
        row.m6 = titleList[7];
        row.m7 = titleList[8];
        row.m8 = titleList[9];
        row.m9 = titleList[10];
        row.m10 = titleList[11];
        row.m11 = titleList[12];
        row.m12 = titleList[13];
        row.m13 = '';
        $scope.dataTitle.push(row);
    }
    //----------------------------------
    $scope.titleArr2 = [
                            ['RS Adjustment vs RS Target OAP/RAP','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
                            ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
                            ['RS Adjustment vs RS Target Market','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
                            ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
                            ['Cummulative','','N-2','N-1','N','N+1','N+2','N+3','N+4','N+5','N+6','N+7','N+8','N+9',''],
                            ['','','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',''],
    ];
    $scope.dataTitle2 = [];
    for(var i=0;i<$scope.titleArr2.length;i++){
        var titleList=$scope.titleArr2[i];
        var row={};
        row.title = titleList[0];
        row.title2 = titleList[1];
        row.m1 = titleList[2];
        row.m2 = titleList[3];
        row.m3 = titleList[4];
        row.m4 = titleList[5];
        row.m5 = titleList[6];
        row.m6 = titleList[7];
        row.m7 = titleList[8];
        row.m8 = titleList[9];
        row.m9 = titleList[10];
        row.m10 = titleList[11];
        row.m11 = titleList[12];
        row.m12 = titleList[13];
        row.m13 = '';
        $scope.dataTitle2.push(row);
    }
    //----------------------------------
    var BASEROW = 0,BASECOL = 0;
    //--------------------------------------------------------------------------
    // Helper Function
    function createMergeCells(){
        function _createMergeCells(row,col,rowspan,colspan){
            return {row:row,col:col,rowspan:rowspan,colspan:colspan};
        }
        var res=[];
        //title-title2
        res.push(_createMergeCells(BASEROW,  BASECOL   , 2,2));
        res.push(_createMergeCells(BASEROW,  BASECOL+2 , 2,2));
        res.push(_createMergeCells(BASEROW,  BASECOL+4 , 2,1));
        res.push(_createMergeCells(BASEROW,  BASECOL+5 , 1,5));
        res.push(_createMergeCells(BASEROW,  BASECOL+10, 2,4));
        res.push(_createMergeCells(BASEROW+2,BASECOL   , 2,2));
        //row 4-14
        for(var i=0;i<11;i++){
           res.push(_createMergeCells(BASEROW+4+i,BASECOL , 1,2));
        }
        //row 15,18,20,21,24
        res.push(_createMergeCells(BASEROW+15,BASECOL , 2,1));
        res.push(_createMergeCells(BASEROW+18,BASECOL , 2,1));
        res.push(_createMergeCells(BASEROW+20,BASECOL , 1,2));
        res.push(_createMergeCells(BASEROW+21,BASECOL , 3,1));
        res.push(_createMergeCells(BASEROW+24,BASECOL , 2,1));
        //row 26-55
        for(var i=0;i<30;i++){
           res.push(_createMergeCells(BASEROW+26+i,BASECOL , 1,2));
        }
        //row34-35
        res.push(_createMergeCells(BASEROW+34,BASECOL+4 , 1,2));
        res.push(_createMergeCells(BASEROW+35,BASECOL+4 , 1,2));
        //row 38-39
        res.push(_createMergeCells(BASEROW+38,BASECOL , 2,2));
        //row 43-44
        res.push(_createMergeCells(BASEROW+43,BASECOL , 2,2));
        //row 48-49
        res.push(_createMergeCells(BASEROW+48,BASECOL , 2,2));
        return res;
    }
    //--------------------------------------------------------------------------
    $scope.hot_settings = {
        colWidths: [40,200,60,60,60,60,60,60,60,60,60,60,60],
        manualColumnResize: [40,200],
        rowHeaders: true,
        colHeaders: true,
        // fixedColumnsLeft:1,
        fixedRowsTop:4,
        formulas:true,
        height: 600,
        width: 1100,
        columns: [
                  { data: 'title', type: 'text', readOnly: true },
                  { data: 'title2', type: 'text', readOnly: true },
                  { data: 'm1' ,type: 'numeric' },
                  { data: 'm2' ,type: 'numeric'},
                  { data: 'm3' ,type: 'numeric'},
                  { data: 'm4' ,type: 'numeric'},
                  { data: 'm5' ,type: 'numeric'},
                  { data: 'm6' ,type: 'numeric'},
                  { data: 'm7' ,type: 'numeric'},
                  { data: 'm8' ,type: 'numeric'},
                  { data: 'm9' ,type: 'numeric'},
                  { data: 'm10' ,type: 'numeric'},
                  { data: 'm11' ,type: 'numeric'},
                  { data: 'm12' ,type: 'numeric'},
                  { data: 'm13' ,type: 'numeric'},
        ],
        mergeCells: createMergeCells(),
    };
    $timeout(function(){
        $scope.hot_rsspmodel = hotRegisterer.getInstance('hot_rsspmodel');
        // console.log("x=>",$scope.hot_rsspmodel);
        $scope.hot_rsspmodel.updateSettings({
                cells: function (row, col, prop) {
                            // console.log(row,col,prop);
                            var cellProperties = {};
                            // cellProperties.editor = false; //Default is No Editor
                            //------------------------------------------------------------
                            switch(row){
                                case 0:
                                case 1: cellProperties.className = "htCenter htMiddle hotHeaderReverse";break;
                                case 2:
                                case 3: cellProperties.className = "htCenter htMiddle hotHeader";break;
                                case 4: if(col>=2 && col<=3) cellProperties.className = "hotFromData";break;
                                case 5: if(col>=4){
                                            cellProperties.editor = 'numeric';
                                            cellProperties.className = "hotEditable";
                                        }
                                        break;
                                case 6:
                                case 7: if(col>=2 && col<=3) cellProperties.className = "hotFromData";break;
                                case 8: cellProperties.format = '0%';break;
                                case 9: if(col>=2 && col<=3) cellProperties.className = "hotFromData";break;
                                case 10: cellProperties.format = '0.0';break;
                                case 11:
                                case 12: cellProperties.format = '0%';break;
                                case 13: break;
                                case 14: cellProperties.className = "hotFromData";break;
                                case 15: if(col>=2 && col<=3){
                                            cellProperties.className = "hotFromData"
                                         }else if(col==0){
                                            cellProperties.className = "htCenter htMiddle";
                                         }
                                         break;
                                case 16: cellProperties.format = '0.00';
                                         cellProperties.className = "hotBold";
                                         break;
                                case 17: if(col>=2 && col<=3) cellProperties.className = "hotFromData";break;
                                case 18: if(col>=2 && col<=3){
                                            cellProperties.className = "hotFromData"
                                         }else if(col==0){
                                            cellProperties.className = "htCenter htMiddle";
                                         }
                                         break;
                                case 19: cellProperties.format = '0%';break;
                                case 20: if(col>=0 && col<=6) cellProperties.className = "hotFromData";break;
                                case 21: if(col>=2 && col<=3){
                                            cellProperties.className = "hotFromData"
                                         }else if(col==0){
                                            cellProperties.className = "htCenter htMiddle";
                                         }
                                         break;
                                case 22: if(col>=4){
                                            cellProperties.editor = 'numeric';
                                            cellProperties.className = "hotEditable";
                                         }
                                         break;
                                case 23: cellProperties.className = "htCenter htMiddle hotHeader5";break;
                                case 24: if(col>=2 && col<=4) cellProperties.className = "hotFromData";break;
                                case 25: break;
                                case 26: cellProperties.className = "hotHeader6";break;
                                case 27: cellProperties.className = "hotHeader3";break;
                                case 28: cellProperties.className = "hotHeader3";cellProperties.format = '0.00';break;
                                case 29: cellProperties.className = "hotHeader4 hotBold";break;
                                case 30: cellProperties.className = "hotHeader4 hotBold";cellProperties.format = '0.0';break;
                                case 31: break;
                                case 32:
                                case 33:
                                case 34: if(row==34 && col==4) cellProperties.className = "hotHeader3";
                                case 35: if(row==35 && col==4) cellProperties.className = "hotHeader3";
                                case 36: if(col==0) cellProperties.className = "hotHeader3"
                                         else cellProperties.format = '0%';break;
                                case 38:
                                case 39:
                                case 43:
                                case 44:
                                case 48:
                                case 49: cellProperties.className = "htCenter htMiddle hotHeader3";break;
                                case 52:
                                case 53: cellProperties.format = '0%';break;
                                default:
                            }
                            //------------------------------------------------------------
                            return cellProperties;
                }
        })
    },0);
    //External Data and Input Data
    var ActualMarketByProvince = [80,80];
    var ActualMarketByProvince_Adj = [80,80];
    var DefaultMarketOAP =     [80,80,80,80,80,80,80,80,80,80];
    var DefaultMarketOAP_Adj = [80,80,80,80,80,80,80,80,80,80];
    var RSProvince =     [50,50,50,50,50,50,50,50,50,50,50,50];
    var PolregProvince = [40,40,40,40,40,40,40,40,40,40,40,40];
    var RetailSales =    [10,10,10,10,10,10,10,10,10,10,10,10];
    var RSTargetOAP =          [10,10,10,10,10,10,10,10,10,10,10,10];
    var OutstandingLastMonth = [0];
    var OutstandingLastMonthUnit = [12,13];
    var ActualSPK =            [10,10];
    var ActualSPKCancel =      [2,2];
    var TargetOAP =            [];
    var RSAdjustment =         [10,10,8,10,12,10,10,10,10,10,10,10,10];
    var WSSuggestion =         [8,8,8,10];
    var StockLastMonth =       [8];

    //Parameter
    var OutstandingLevel = 2;
    var StockRatioStd = 0.5;
    var StockRatioMin = 0.2;
    var OutletShareTarget = 0.1;
    var ValueAdjMin = 0.1;
    var ValueAdjMax = 0.8;
    var ValueSuggestMin = 0.1;
    var ValueSuggestMax = 0.8;
    //-------------------------------------------------------------------------------
    function createDataRow(title,title2,arr1,arr2){
        var res={};
        res.title=title;res.title2=title2;
        for(var i=0;i<arr1.length;i++){
            res['m'+(i+1)] = arr1[i];
        }
        if(arr2){
            for(var j=0;j<arr2.length;j++){
                res['m'+(j+arr1.length+1)] = arr2[j];
            }
        }
        // console.log("res=>",res);
        return res;
    }
    function createDataRowFormula(title,title2,c1,c1inc,r1,r1inc,anchor1,op,c2,c2inc,r2,r2inc,anchor2){
        var res={};
        res.title=title;res.title2=title2;
        for(var i=0;i<12;i++){

             var f1 = '='+String.fromCharCode(c1+64)+(anchor1?'$':'')+r1;
             var f2 = String.fromCharCode(c2+64)+(anchor2?'$':'')+r2;
             res['m'+(i+1)] = f1 + op + f2;
             if(c1inc) c1++;
             if(r1inc) r1++;
             if(c2inc) c2++;
             if(r2inc) r2++;
        }
        // console.log("res=>",res);
        return res;
    }
    function createDataRowFormulaX(title,title2,arr1,cell1,cell2,op,inc_c1,inc_r1,inc_c2,inc_r2){
        function _splitCell(cell){
            var useS=false;
            var c=cell.substring(0,1);
            var s=cell.substring(1,2);
            var r='';
            if(s=='$') {
                useS=true;
                r=cell.substring(2,4);
            }else{
                r=cell.substring(1,3);
            }
            return({c:c,r:r,s:(useS?'$':'')});
        }
        function _incCol(col){
            var c=col.charCodeAt(0);
            c++;
            return String.fromCharCode(c);
        }
        var res={};
        res.title=title;res.title2=title2;
        var cl1 = _splitCell(cell1);
        var cl2 = _splitCell(cell2);
        // console.log(cl1,cl2);
        var arrLen = 0;
        if(arr1!=null){
            arrLen = arr1.length;
            for(var j=0;j<arrLen;j++){
                res['m'+(j+1)] = arr1[j];
            }
        }
        for(var i=0;i<12-arrLen;i++){
             var f1 = '='+cl1.c + cl1.s + cl1.r;
             var f2 = cl2.c + cl2.s + cl2.r;
             res['m'+(i+1+arrLen)] = f1 + op + f2;
             if(inc_c1) cl1.c = _incCol(cl1.c);
             if(inc_r1) cl1.r++;
             if(inc_c2) cl2.c = _incCol(cl2.c);
             if(inc_r2) cl2.r++;
        }
        // var res = _splitCell(cell1);
        // console.log("res=>",res);
        return res;
    }
    // createDataRowFormulaX('','',ActualMarketByProvince,'A$9','B$10','+',true,true,true,true);
    //-------------------------------------------------------------------------------
    $scope.dataGrid=[];
    // $scope.dataGrid.push(createDataRow('Market Polreg by Province',         '',ActualMarketByProvince,DefaultMarketOAP)); //Row 5;
    $scope.dataGrid.push({}); //Row 5
    $scope.dataGrid.push(createDataRow('Market Polreg by Province (Adjust)','',ActualMarketByProvince_Adj,DefaultMarketOAP_Adj)); //Row 6; //Input
    // $scope.dataGrid.push(createDataRow('RS Province','',RSProvince)); //Row 7;
    $scope.dataGrid.push({}); //Row 7
    // $scope.dataGrid.push(createDataRow('Polreg Province','',PolregProvince)); //Row 8;
    $scope.dataGrid.push({}); //Row 8
    //--- ARRAY RESULT OF CALCULATION -----------------------------------
    var RatioPolregToRS=[];
    var PolregOutlet = [];
    var RatioPolregToRSOutlet=[];
    //-------------------------------------------------------------------
    //---------- Ratio Polreg to RS (Prov.) is not shown - put into Array //Row 7
    function CalcRatioPolregToRS(){
        for(var i=0;i<12;i++){
            RatioPolregToRS.push(PolregProvince[i]/RSProvince[i]);
        }
    }
    CalcRatioPolregToRS();
    //-------------------------------------------------------------------
    //---------- Ratio Polreg to RS (Outlet.) is not shown - put into Array //Row 13
    function CalcRatioPolregToRSOutlet(){
        RatioPolregToRSOutlet.push( ( (RetailSales[0]/RSProvince[0])*PolregProvince[0]) / RetailSales[0] ); //C
        RatioPolregToRSOutlet.push( ( (RetailSales[1]/RSProvince[1])*PolregProvince[1]) / RetailSales[1] ); //D
        RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]) /2 ); //E
        RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //F
        for(var i=4;i<12;i++){
            RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //G-N
        }
    }
    CalcRatioPolregToRSOutlet();
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 9
            // title: 'Ratio Polreg to RS (Prov.)',
           //  m1:RatioPolregToRS[0],   //Col C
           //  m2:RatioPolregToRS[1],   //Col D
           //  m3:RatioPolregToRS[2],   //Col E
           //  m4:RatioPolregToRS[3],   //Col F
           //  m5:RatioPolregToRS[4],   //Col G
           //  m6:RatioPolregToRS[5],   //Col H
           //  m7:RatioPolregToRS[6],   //Col I
           //  m8:RatioPolregToRS[7],   //Col J
           //  m9:RatioPolregToRS[8],   //Col K
           // m10:RatioPolregToRS[9],   //Col L
           // m11:RatioPolregToRS[10],  //Col M
           // m12:RatioPolregToRS[11],  //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 10
            title: 'RS Target Market',
            m1:RetailSales[0],  //Col C
            m2:RetailSales[1],  //Col D
            m3:'=(E$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[2],  //Col E
            m4:'=(F$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[3],  //Col F
            m5:'=(G$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[4],  //Col G
            m6:'=(H$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[5],  //Col H
            m7:'=(I$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[6],  //Col I
            m8:'=(J$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[7],  //Col J
            m9:'=(K$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[8],  //Col K
           m10:'=(L$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[9],  //Col L
           m11:'=(M$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[10],  //Col M
           m12:'=(N$6*'+OutletShareTarget+')/'+RatioPolregToRSOutlet[11],  //Col N
        }
    )
    //---------- Polreg Outlet is not shown - put into Array
    $scope.dataGrid.push(
        { //Row 11
            title: 'Polreg Outlet',
           //  m1:'=('+RetailSales[0]+'/'+RSProvince[0]+')*'+PolregProvince[0], //Col C
           //  m2:'=('+RetailSales[1]+'/'+RSProvince[1]+')*'+PolregProvince[1], //Col D
           //  m3:'='+RSAdjustment[2]+'*'+RatioPolregToRSOutlet[2], //Col E
           //  m4:'='+RSAdjustment[3]+'*'+RatioPolregToRSOutlet[3], //Col F    ---> ini dikali RatioPolregToRS atau RatioPolregToRSOutlet ???????
           //  m5:'='+RSAdjustment[4]+'*'+RatioPolregToRSOutlet[4], //Col G
           //  m6:'='+RSAdjustment[5]+'*'+RatioPolregToRSOutlet[5], //Col H
           //  m7:'='+RSAdjustment[6]+'*'+RatioPolregToRSOutlet[6], //Col I
           //  m8:'='+RSAdjustment[7]+'*'+RatioPolregToRSOutlet[7], //Col J
           //  m9:'='+RSAdjustment[8]+'*'+RatioPolregToRSOutlet[8], //Col K
           // m10:'='+RSAdjustment[9]+'*'+RatioPolregToRSOutlet[9], //Col L
           // m11:'='+RSAdjustment[10]+'*'+RatioPolregToRSOutlet[10], //Col M
           // m12:'='+RSAdjustment[11]+'*'+RatioPolregToRSOutlet[11], //Col N
        }
    )
    function CalcPolregOutlet(){ //Row 11
        PolregOutlet.push( ( (RetailSales[0]/RSProvince[0])*PolregProvince[0]) ); //C
        PolregOutlet.push( ( (RetailSales[1]/RSProvince[1])*PolregProvince[1]) ); //D
        for(var i=2;i<12;i++){
            PolregOutlet.push( RSAdjustment[i]*RatioPolregToRSOutlet[i] ); //E-N
        }
    }
    CalcPolregOutlet();
    //---------------------------------------
    $scope.dataGrid.push(
        { //Row 12
            title: 'Outlet Share',
            m1:'=(('+RetailSales[0]+'/'+RSProvince[0]+')*'+PolregProvince[0]+')/C6',
            m2:'=(('+RetailSales[1]+'/'+RSProvince[1]+')*'+PolregProvince[1]+')/D6',
            m3:'='+PolregOutlet[2]+'/E$6',
            m4:'='+PolregOutlet[3]+'/F$6',
            m5:'='+PolregOutlet[4]+'/G$6',
            m6:'='+PolregOutlet[5]+'/H$6',
            m7:'='+PolregOutlet[6]+'/I$6',
            m8:'='+PolregOutlet[7]+'/J$6',
            m9:'='+PolregOutlet[8]+'/K$6',
           m10:'='+PolregOutlet[9]+'/L$6',
           m11:'='+PolregOutlet[10]+'/M$6',
           m12:'='+PolregOutlet[11]+'/N$6',
        }
    )
    //---------- Ratio Polreg to RS (Outlet.) is not shown - put into Array
    // function CalcRatioPolregToRSOutlet(){ //Row 13
    //     RatioPolregToRSOutlet.push( ( (RetailSales[0]/RSProvince[0])*PolregProvince[0]) / RetailSales[0] ); //C
    //     RatioPolregToRSOutlet.push( ( (RetailSales[1]/RSProvince[1])*PolregProvince[1]) / RetailSales[1] ); //D
    //     RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]) /2 ); //E
    //     RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //F
    //     for(var i=4;i<12;i++){
    //         RatioPolregToRSOutlet.push( (RatioPolregToRSOutlet[0]+RatioPolregToRSOutlet[1]+RatioPolregToRSOutlet[2]) /3 ); //G-N
    //     }
    // }
    // CalcRatioPolregToRSOutlet();
        //---------------------------------------
        $scope.dataGrid.push(
            {}
            // createDataRow('Ratio Polreg to RS (Outlet.)','',RatioPolregToRSOutlet,null)
            // { //Row 13
            //     title: 'Ratio Polreg to RS (Outlet.)',
            //     m1:'=(('+RetailSales[0]+'/'+RSProvince[0]+')*'+PolregProvince[0]+')/'+RetailSales[0], //C
            //     m2:'=(('+RetailSales[1]+'/'+RSProvince[1]+')*'+PolregProvince[1]+')/'+RetailSales[1], //D
            //     m3:'=AVERAGE(C13:D13)',  //E //Est.
            //     m4:'=AVERAGE(C13:E13)',  //F //Fix
            //     m5:'=AVERAGE(C13:E13)',  //G //Flexible
            //     m6:'=AVERAGE(C13:E13)',
            //     m7:'=AVERAGE(C13:E13)',
            //     m8:'=AVERAGE(C13:E13)',
            //     m9:'=AVERAGE(C13:E13)',
            //    m10:'=AVERAGE(C13:E13)',
            //    m11:'=AVERAGE(C13:E13)',
            //    m12:'=AVERAGE(C13:E13)',
            // }
        )
    //---------------------------------------
    $scope.dataGrid.push(createDataRow('RS Target OAP/RAP','',RSTargetOAP)); //Row 14; //From External Data
    //---------------------------------------
    //------ Outstanding Last Month is not shown - get from Array
    $scope.dataGrid.push(
        { //Row 15
           //  title: 'Outstanding Last Month',
           //  m1:OutstandingLastMonth[0],  //Col C
           //  m2:'=C16',  //Col D
           //  m3:'=D16',  //Col E
           //  m4:'=E16',  //Col F
           //  m5:'=F16',  //Col G
           //  m6:'=G16',  //Col H
           //  m7:'=H16',  //Col I
           //  m8:'=I16',  //Col J
           //  m9:'=J16',  //Col K
           // m10:'=K16',  //Col L
           // m11:'=L16',  //Col M
           // m12:'=M16',  //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 16
            title: 'O/S',
            title2: 'Unit',
            m1:OutstandingLastMonthUnit[0], //Col C
            m2:OutstandingLastMonthUnit[1], //Col D
            m3:'=D16+E18-E23-E19',  //Col E  //OS Unit(16) + SPK Suggestion(18) - RS Adjustment(23) - C/L Unit(19)
            m4:'=E16+F18-F23-F19',  //Col F
            m5:'=F16+G18-G23-G19',  //Col G
            m6:'=G16+H18-H23-H19',  //Col H
            m7:'=H16+I18-I23-I19',  //Col I
            m8:'=I16+J18-J23-J19',  //Col J
            m9:'=J16+K18-K23-K19',  //Col K
           m10:'=K16+L18-L23-L19',  //Col L
           m11:'=L16+M18-M23-M19',  //Col M
           m12:'=M16+N18-N23-N19',  //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 17
            title: 'O/S',
            title2: 'Ratio',
            m1:"=C16/D23",    //OS Unit(16) / RS Adjustment(23)
            m2:"=D16/E23",
            m3:"=E16/F23",
            m4:"=F16/G23",
            m5:"=G16/H23",
            m6:"=H16/I23",
            m7:"=I16/J23",
            m8:"=J16/K23",
            m9:"=K16/L23",
           m10:"=L16/M23",
           m11:"=M16/N23",
           m12:"=n16/O23",
        }
    );
    $scope.dataGrid.push(
        { //Row 18
            title: 'SPK',
            title2: 'Suggestion',
            m1: ActualSPK[0], //Col C
            m2: ActualSPK[1], //Col D
            m3: '=E$23+E$19+F$10-D$16', //Col E Est. //-> RS Adjustment(23) + C/L Unit(19) + RS Target Market(10) - O/S Unit
            m4: '=F$23+F$19+G$10-E$16', //Col F Fix
            m5: '=G23+G19+ROUNDUP(AVERAGE(D16:F16),0)-F16', //Col G Flexible //-> RS Adjustment(23) + C/L Unit(19) + Average(O/S Unit 3 Month) - O/S Unit
            m6: '=H23+H19+ROUNDUP(AVERAGE(E16:G16),0)-G16', //Col H
            m7: '=I23+I19+ROUNDUP(AVERAGE(F16:H16),0)-H16', //Col I
            m8: '=J23+J19+ROUNDUP(AVERAGE(G16:I16),0)-I16', //Col J
            m9: '=K23+K19+ROUNDUP(AVERAGE(H16:J16),0)-J16', //Col K
           m10: '=L23+L19+ROUNDUP(AVERAGE(I16:K16),0)-K16', //Col L
           m11: '=M23+M19+ROUNDUP(AVERAGE(J16:L16),0)-L16', //Col M
           m12: '=N23+N19+ROUNDUP(AVERAGE(K16:M16),0)-M16', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 19
            title: 'C/L',
            title2: 'Unit',
            m1: ActualSPKCancel[0], //Col C
            m2: ActualSPKCancel[1], //Col D
            m3: '=ROUNDUP(AVERAGE(C20:D20)* (AVERAGE(C18:D18)+AVERAGE(C16:D16)),0)', //Col E //-> Avg(C/L Ratio 3 Month) * Avg(SPK Suggestion 3 Month) + Avg(O/S Unit 3 Mo)
            m4: '=ROUNDUP(AVERAGE(C20:E20)* (AVERAGE(C18:E18)+AVERAGE(C16:E16)),0)', //Col F
            m5: '=ROUNDUP(AVERAGE(D20:F20)* (AVERAGE(D18:F18)+AVERAGE(D16:F16)),0)', //Col G
            m6: '=ROUNDUP(AVERAGE(E20:G20)* (AVERAGE(E18:G18)+AVERAGE(E16:G16)),0)', //Col H
            m7: '=ROUNDUP(AVERAGE(F20:H20)* (AVERAGE(F18:H18)+AVERAGE(F16:H16)),0)', //Col I
            m8: '=ROUNDUP(AVERAGE(G20:I20)* (AVERAGE(G18:I18)+AVERAGE(G16:I16)),0)', //Col J
            m9: '=ROUNDUP(AVERAGE(H20:J20)* (AVERAGE(H18:J18)+AVERAGE(H16:J16)),0)', //Col K
           m10: '=ROUNDUP(AVERAGE(I20:K20)* (AVERAGE(I18:K18)+AVERAGE(I16:K16)),0)', //Col L
           m11: '=ROUNDUP(AVERAGE(J20:L20)* (AVERAGE(J18:L18)+AVERAGE(J16:L16)),0)', //Col M
           m12: '=ROUNDUP(AVERAGE(K20:M20)* (AVERAGE(K18:M18)+AVERAGE(K16:M16)),0)', //Col N
        }
    )
    $scope.dataGrid.push(
         { //Row 20
            title: 'C/L',
            title2: 'Ratio',
            m1: '=C$19/(C$18+'+OutstandingLastMonth[0]+')', //Col C //-> C/L Unit(19) / ( SPK Suggestion(18) + O/S Unit(16)
            m2: '=D$19/(D$18+C$16)', //Col D
            m3: '=E$19/(E$18+D$16)', //Col E
            m4: '=F$19/(F$18+E$16)', //Col F
            m5: '=G$19/(G$18+F$16)', //Col G
            m6: '=H$19/(H$18+G$16)', //Col H
            m7: '=I$19/(I$18+H$16)', //Col I
            m8: '=J$19/(J$18+I$16)', //Col J
            m9: '=K$19/(K$18+J$16)', //Col K
            m10: '=L$19/(L$18+K$16)', //Col L
            m11: '=M$19/(M$18+L$16)', //Col M
            m12: '=N$19/(N$18+M$15)', //Col N
        }
    )
    $scope.dataGrid.push(createDataRow('Not Used - OAP/RAP Target','',TargetOAP)); //Row 21;
    $scope.dataGrid.push(
        { //Row 22
            title: 'RS',
            title2: 'Suggestion',
            m1:RetailSales[0],  //Col C  --> RS Target Market
            m2:RetailSales[1],  //Col D  --> RS Target Market
            m3:'=D28 + E25 - (('+StockRatioStd+'*F10)-('+StockRatioMin+'*'+StockRatioStd+'*F10))',  //Col E
            //--> StockBaseOnAdjustment(28) + WSSuggestion(25) - ( (StockRatioStd*RSTargetMarket) - (StockRatioMin*StockRatioStd*RSTargetMarket) )
            m4:'=E28 + F25 - (('+StockRatioStd+'*G10)-('+StockRatioMin+'*'+StockRatioStd+'*G10))',  //Col F
            m5:'=IF(G10-F47=AVERAGE(D10:F10),G10, IF(G10-F47>(1+'+ValueSuggestMax+')*AVERAGE(D10:F10),((1+'+ValueSuggestMax+')*AVERAGE(D10:F10)),IF(AND(F16>(1+'+ValueSuggestMax+')*AVERAGE(D10:F10),F16>'+OutstandingLevel+'*H10),((1+'+ValueSuggestMax+')*AVERAGE(D10:F10)),IF(G10-F47<'+ValueSuggestMin+'*G14,'+ValueSuggestMin+'*G14,G10-F47))))',  //Col G
            m6:'=IF(H10-G47=AVERAGE(E10:G10),H10, IF(H10-G47>(1+'+ValueSuggestMax+')*AVERAGE(E10:G10),((1+'+ValueSuggestMax+')*AVERAGE(E10:G10)),IF(AND(G16>(1+'+ValueSuggestMax+')*AVERAGE(E10:G10),G16>'+OutstandingLevel+'*I10),((1+'+ValueSuggestMax+')*AVERAGE(E10:G10)),IF(H10-G47<'+ValueSuggestMin+'*H14,'+ValueSuggestMin+'*H14,H10-G47))))',  //Col H
            m7:'=IF(I10-H47=AVERAGE(F10:H10),I10, IF(I10-H47>(1+'+ValueSuggestMax+')*AVERAGE(F10:H10),((1+'+ValueSuggestMax+')*AVERAGE(F10:H10)),IF(AND(H16>(1+'+ValueSuggestMax+')*AVERAGE(F10:H10),H16>'+OutstandingLevel+'*J10),((1+'+ValueSuggestMax+')*AVERAGE(F10:H10)),IF(I10-H47<'+ValueSuggestMin+'*I14,'+ValueSuggestMin+'*I14,I10-H47))))',  //Col I
            m8:'=IF(J10-I47=AVERAGE(G10:I10),J10, IF(J10-I47>(1+'+ValueSuggestMax+')*AVERAGE(G10:I10),((1+'+ValueSuggestMax+')*AVERAGE(G10:I10)),IF(AND(I16>(1+'+ValueSuggestMax+')*AVERAGE(G10:I10),I16>'+OutstandingLevel+'*K10),((1+'+ValueSuggestMax+')*AVERAGE(G10:I10)),IF(J10-I47<'+ValueSuggestMin+'*J14,'+ValueSuggestMin+'*J14,J10-I47))))',  //Col J
            m9:'=IF(K10-J47=AVERAGE(H10:J10),K10, IF(K10-J47>(1+'+ValueSuggestMax+')*AVERAGE(H10:J10),((1+'+ValueSuggestMax+')*AVERAGE(H10:J10)),IF(AND(J16>(1+'+ValueSuggestMax+')*AVERAGE(H10:J10),J16>'+OutstandingLevel+'*L10),((1+'+ValueSuggestMax+')*AVERAGE(H10:J10)),IF(K10-J47<'+ValueSuggestMin+'*K14,'+ValueSuggestMin+'*K14,K10-J47))))',  //Col K
           m10:'=IF(L10-K47=AVERAGE(I10:K10),L10, IF(L10-K47>(1+'+ValueSuggestMax+')*AVERAGE(I10:K10),((1+'+ValueSuggestMax+')*AVERAGE(I10:K10)),IF(AND(K16>(1+'+ValueSuggestMax+')*AVERAGE(I10:K10),K16>'+OutstandingLevel+'*M10),((1+'+ValueSuggestMax+')*AVERAGE(I10:K10)),IF(L10-K47<'+ValueSuggestMin+'*L14,'+ValueSuggestMin+'*L14,L10-K47))))',  //Col L
           m11:'=IF(M10-L47=AVERAGE(J10:L10),M10, IF(M10-L47>(1+'+ValueSuggestMax+')*AVERAGE(J10:L10),((1+'+ValueSuggestMax+')*AVERAGE(J10:L10)),IF(AND(L16>(1+'+ValueSuggestMax+')*AVERAGE(J10:L10),L16>'+OutstandingLevel+'*N10),((1+'+ValueSuggestMax+')*AVERAGE(J10:L10)),IF(M10-L47<'+ValueSuggestMin+'*M14,'+ValueSuggestMin+'*M14,M10-L47))))',  //Col M
           m12:'=IF(N10-M47=AVERAGE(K10:M10),N10, IF(N10-M47>(1+'+ValueSuggestMax+')*AVERAGE(K10:M10),((1+'+ValueSuggestMax+')*AVERAGE(K10:M10)),IF(AND(M16>(1+'+ValueSuggestMax+')*AVERAGE(K10:M10),M16>'+OutstandingLevel+'*O10),((1+'+ValueSuggestMax+')*AVERAGE(K10:M10)),IF(N10-M47<'+ValueSuggestMin+'*N14,'+ValueSuggestMin+'*N14,N10-M47))))',  //Col N
        }
    )
    //----------------------------------------------------------
    // RS Adjustment Row 23 --> Input
    $scope.dataGrid.push(createDataRow('RS','Adjustment',RSAdjustment)); //Row 23; //RSAdjustment
    //----------------------------------------------------------
    //----> not used
    $scope.dataGrid.push(
        { //Row 24
            // title: 'RS',
            // m1:'', //Col C
            // m2:'', //Col D
           //  m3:'=IF(AND(E23>=0,E23<D30+E26+1),"Yes","No")', //Col E -> problem with "<=" so add+1
           //  m4:'=IF(AND(F23>=0,F23<E30+F26+1),"Yes","No")', //Col F -> problem with "<=" so add+1
           //  m5:'=IF(G23>(1+$H$35)*AVERAGE(D10:F10),"No more than ",IF(G23<$G$35*AVERAGE(D10:F10),"No less than ",IF(G23<G22+1,"Yes","No more/less than suggestion")))', //Col G
           //  m6:'=IF(H23>(1+$H$35)*AVERAGE(E10:G10),"No more than ",IF(H23<$G$35*AVERAGE(E10:G10),"No less than ",IF(H23<H22+1,"Yes","No more/less than suggestion")))', //Col H
           //  m7:'=IF(I23>(1+$H$35)*AVERAGE(F10:H10),"No more than ",IF(I23<$G$35*AVERAGE(F10:H10),"No less than ",IF(I23<I22+1,"Yes","No more/less than suggestion")))', //Col I
           //  m8:'=IF(J23>(1+$H$35)*AVERAGE(G10:I10),"No more than ",IF(J23<$G$35*AVERAGE(G10:I10),"No less than ",IF(J23<J22+1,"Yes","No more/less than suggestion")))', //Col J
           //  m9:'=IF(K23>(1+$H$35)*AVERAGE(H10:J10),"No more than ",IF(K23<$G$35*AVERAGE(H10:J10),"No less than ",IF(K23<K22+1,"Yes","No more/less than suggestion")))', //Col K
           // m10:'=IF(L23>(1+$H$35)*AVERAGE(I10:K10),"No more than ",IF(L23<$G$35*AVERAGE(I10:K10),"No less than ",IF(L23<L22+1,"Yes","No more/less than suggestion")))', //Col L
           // m11:'=IF(M23>(1+$H$35)*AVERAGE(J10:L10),"No more than ",IF(M23<$G$35*AVERAGE(J10:L10),"No less than ",IF(M23<M22+1,"Yes","No more/less than suggestion")))', //Col M
           // m12:'=IF(N23>(1+$H$35)*AVERAGE(K10:M10),"No more than ",IF(N23<$G$35*AVERAGE(K10:M10),"No less than ",IF(N23<N22+1,"Yes","No more/less than suggestion")))', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 25
            title: 'WS',
            title2: 'Suggestion',
            m1:WSSuggestion[0],  //Col C
            m2:WSSuggestion[1],  //Col D
            m3:WSSuggestion[2],  //Col E
            m4:WSSuggestion[3],  //Col F
            m5: '=G23+('+StockRatioStd+'*H23)-F28', //Col G  --> RS Adjustment(23)+ ( StockRatioStd*RSAdjustment(23) ) - StockBaseOnSuggestion(28) ??? or - StockBaseOnAdjustment() ???
            m6: '=H23+('+StockRatioStd+'*I23)-G28', //Col H
            m7: '=I23+('+StockRatioStd+'*J23)-H28', //Col I
            m8: '=J23+('+StockRatioStd+'*K23)-I28', //Col J
            m9: '=K23+('+StockRatioStd+'*L23)-J28', //Col K
           m10: '=L23+('+StockRatioStd+'*M23)-K28', //Col L
           m11: '=M23+('+StockRatioStd+'*N23)-L28', //Col M
           m12: '=N23+('+StockRatioStd+'*O23)-M28', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 26
            title: 'WS',
            title2: 'Adjustment',
            m1:'=C25',  //Col C  --> =WS Suggestion
            m2:'=D25',  //Col D
            m3:'=E25',  //Col E
            m4:'=F25',  //Col F
            m5:'=G25',  //Col G
            m6:'=H25',  //Col H
            m7:'=I25',  //Col I
            m8:'=J25',  //Col J
            m9:'=K25',  //Col K
           m10:'=L25',  //Col L
           m11:'=M25',  //Col M
           m12:'=N25',  //Col N
        }
    )
    //-------------------------------------------
    $scope.dataGrid.push(
        { //Row 27  -- Not Used
            // title: 'Stock Last Month',
            // m1:StockLastMonth[0], //Col C
        }
    )
    //-------------------------------------------
    $scope.dataGrid.push(
        { //Row 28
            title: 'Stock based on Suggestion',
            m1:'='+StockLastMonth[0]+'+C$25-C$22', //Col C  --> StockLastMonth[0] + WS Suggestion(25) - RS Suggestion(22)
            m2:'=C28+D$25-D$22',  //Col D  --> StockBaseOnSuggestion[28] + WS Suggestion(25) - RS Suggestion(22)
            m3:'=D28+E$25-E$22',  //Col E
            m4:'=E28+F$25-F$22',  //Col F
            m5:'=F28+G$25-G$22',  //Col G
            m6:'=G28+H$25-H$22',  //Col H
            m7:'=H28+I$25-I$22',  //Col I
            m8:'=I28+J$25-J$22',  //Col J
            m9:'=J28+K$25-K$22',  //Col K
           m10:'=K28+L$25-L$22',  //Col L
           m11:'=L28+M$25-M$22',  //Col M
           m12:'=M28+N$25-N$22',  //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 29
            title: 'Stock Ratio',
            m1:'=C28/D$22',  //Col C  -> StockBaseOnSuggestion(28) / RS Suggestion(22)
            m2:'=D28/E$22',  //Col D
            m3:'=E28/F$22',  //Col E
            m4:'=F28/G$22',  //Col F
            m5:'=G28/H$22',  //Col G
            m6:'=H28/I$22',  //Col H
            m7:'=I28/J$22',  //Col I
            m8:'=J28/K$22',  //Col J
            m9:'=K28/L$22',  //Col K
           m10:'=L28/M$22',  //Col L
           m11:'=M28/N$22',  //Col M
           m12:'=N28/O$22',  //Col N
        }
    )
    // $scope.dataGrid.push(createDataRowFormulaX('Stock Ratio','',null,'C30','D$23','/',true,false,true,false)); //Row 29
    $scope.dataGrid.push(
        { //Row 30
            title: 'Stock based on Adjustment',
            m1: '='+StockLastMonth[0]+'+C26-C23', //Col C  -> StockLastMonth[0] + WS Adjustment(26) - RS Adjustment(23)
            m2: '=C30+D26-D23', //Col D  -> StockBaseOnAdjustment(30) + WS Adjustment(26) - RS Adjustment(23)
            m3: '=D30+E26-E23', //Col E
            m4: '=E30+F26-F23', //Col F
            m5: '=F30+G26-G23', //Col G
            m6: '=G30+H26-H23', //Col H
            m7: '=H30+I26-I23', //Col I
            m8: '=I30+J26-J23', //Col J
            m9: '=J30+K26-K23', //Col K
           m10: '=K30+L26-L23', //Col L
           m11: '=L30+M26-M23', //Col M
           m12: '=M30+N26-N23', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 31
            title: 'Stock Ratio',
            m1:'=C30/D23',  //Col C -> StockBaseOnAdjustment(30) / RS Adjustment(23)
            m2:'=D30/E23',  //Col D
            m3:'=E30/F23',  //Col E
            m4:'=F30/G23',  //Col F
            m5:'=G30/H23',  //Col G
            m6:'=H30/I23',  //Col H
            m7:'=I30/J23',  //Col I
            m8:'=J30/K23',  //Col J
            m9:'=K30/L23',  //Col K
           m10:'=L30/M23',  //Col L
           m11:'=M30/N23',  //Col M
           m12:'=N30/O23',  //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 32 -- blank row
            // title: '',
        }
    )
    // -------------------------------------------------------
    // --------  Parameter - Not Used Rows--------------------
    // -------------------------------------------------------
    $scope.dataGrid.push(
        { //Row 33
            // title: 'Outstanding Level',
            // m1: OutstandingLevel,
        }
    )
    $scope.dataGrid.push(
        { //Row 34
            // title: 'Stock Ratio Std',
            // m1: StockRatioStd,
            // m2:'',
            // m3:'',
            // m4:'',
            // m5:'Min',
            // m6:'Max',
        }
    )
    $scope.dataGrid.push(
        { //Row 35
            // title: 'Stock Ratio Min',
            // m1: StockRatioMin,
            // m2:'',
            // m3:'Value Adj',
            // m4:'',
            // m5:ValueAdjMin,
            // m6:ValueAdjMax,
        }
    )
    $scope.dataGrid.push(
        { //Row 36
            // title: 'Outlet Share Target',
            // m1: OutletShareTarget,
            // m2:'',
            // m3:'Value Suggest',
            // m4:'',
            // m5:ValueSuggestMin,
            // m6:ValueSuggestMax
        }
    )
    $scope.dataGrid.push(
        { //Row 37 -- Not Used
            // title: 'Level Outstanding Fulfillment',
            // m1: 1,
        }
    )
    $scope.dataGrid.push(
        { //Row 38 -- blank row
            // title: '',
        }
    )
    //--------------------------------------------------------------
    $scope.dataGrid.push(
        $scope.dataTitle2[0] //Row 39 - Title Only //Gap vs RS Target OAP/RAP
    )
    $scope.dataGrid.push(
        $scope.dataTitle2[1] //Row 40 - Title Only
    )
    $scope.dataGrid.push(
        { //Row 41
            title: 'Monthly Gap',
            m1:'=C23-C14', //Col C  --> RS Adjustment(23) - RS Target OAP/RAP(14)
            m2:'=D23-D14', //Col D
            m3:'=E23-E14', //Col E
            m4:'=F23-F14', //Col F
            m5:'=G23-G14', //Col G
            m6:'=H23-H14', //Col H
            m7:'=I23-I14', //Col I
            m8:'=J23-J14', //Col J
            m9:'=K23-K14', //Col K
           m10:'=L23-L14', //Col L
           m11:'=M23-M14', //Col M
           m12:'=N23-N14', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 42
            title: 'Accumulation Gap',
            m1:'=C41',     //Col C
            m2:'=C42+D41', //Col D
            m3:'=D42+E41', //Col E
            m4:'=E42+F41', //Col F
            m5:'=F42+G41', //Col G
            m6:'=G42+H41', //Col H
            m7:'=H42+I41', //Col I
            m8:'=I42+J41', //Col J
            m9:'=J42+K41', //Col K
           m10:'=K42+L41', //Col L
           m11:'=L42+M41', //Col M
           m12:'=M42+N41', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 43 -- blank row
            title: '',
        }
    )
    //--------------------------------------------------------------
    $scope.dataGrid.push(
        $scope.dataTitle2[2] //Row 44 - Title Only //Gap vs RS Target Market
    )
    $scope.dataGrid.push(
        $scope.dataTitle2[3] //Row 45 - Title Only
    )
    $scope.dataGrid.push(
        { //Row 46
            title: 'Monthly Gap',
            m1:'=C23-C10', //Col C --> RS Adjustment(23) - RS Target Market (10)
            m2:'=D23-D10', //Col D
            m3:'=E23-E10', //Col E
            m4:'=F23-F10', //Col F
            m5:'=G23-G10', //Col G
            m6:'=H23-H10', //Col H
            m7:'=I23-I10', //Col I
            m8:'=J23-J10', //Col J
            m9:'=K23-K10', //Col K
           m10:'=L23-L10', //Col L
           m11:'=M23-M10', //Col M
           m12:'=N23-N10', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 47
            title: 'Accumulation Gap',
            m1:'=C46',     //Col C
            m2:'=C47+D46', //Col D
            m3:'=D47+E46', //Col E
            m4:'=E47+F46', //Col F  ---> Problem Here !!!!!!
            m5:'=F47+G46', //Col G
            m6:'=G47+H46', //Col H
            m7:'=H47+I46', //Col I
            m8:'=I47+J46', //Col J
            m9:'=J47+K46', //Col K
           m10:'=K47+L46', //Col L
           m11:'=L47+M46', //Col M
           m12:'=M47+N46', //Col N
        }
    )
    $scope.dataGrid.push(
        { //Row 48 -- blank row
            title: '',
        }
    )
    //--------------------------------------------------------------
    // $scope.dataGrid.push(
    //     $scope.dataTitle2[4] //Row 49 - Title Only
    // )
    // $scope.dataGrid.push(
    //     $scope.dataTitle2[5] //Row 50 - Title Only
    // )
    // $scope.dataGrid.push(
    //     { //Row 51
    //         title: 'Market',
    //         m1:'=C6',     //Col C
    //         m2:'=C51+D6', //Col D
    //         m3:'=D51+E6', //Col E
    //         m4:'=E51+F6', //Col F
    //         m5:'=F51+G6', //Col G
    //         m6:'=G51+H6', //Col H
    //         m7:'=H51+I6', //Col I
    //         m8:'=I51+J6', //Col J
    //         m9:'=J51+K6', //Col K
    //        m10:'=K51+L6', //Col L
    //        m11:'=L51+M6', //Col M
    //        m12:'=M51+N6', //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 52
    //         title: 'Polreg',
    //         m1:PolregOutlet[0],     //Col C
    //         m2:'=C52+'+PolregOutlet[1], //Col D
    //         m3:'=D52+'+PolregOutlet[2], //Col E
    //         m4:'=E52+'+PolregOutlet[3], //Col F
    //         m5:'=F52+'+PolregOutlet[4], //Col G
    //         m6:'=G52+'+PolregOutlet[5], //Col H
    //         m7:'=H52+'+PolregOutlet[6], //Col I
    //         m8:'=I52+'+PolregOutlet[7], //Col J
    //         m9:'=J52+'+PolregOutlet[8], //Col K
    //        m10:'=K52+'+PolregOutlet[9], //Col L
    //        m11:'=L52+'+PolregOutlet[10], //Col M
    //        m12:'=M52+'+PolregOutlet[11], //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 53
    //         title: 'Outlet Share',
    //         m1:'=C52/C51',  //Col C
    //         m2:'=D52/D51',  //Col D
    //         m3:'=E52/E51',  //Col E
    //         m4:'=F52/F51',  //Col F
    //         m5:'=G52/G51',  //Col G
    //         m6:'=H52/H51',  //Col H
    //         m7:'=I52/I51',  //Col I
    //         m8:'=J52/J51',  //Col J
    //         m9:'=K52/K51',  //Col K
    //        m10:'=L52/L51',  //Col L
    //        m11:'=M52/M51',  //Col M
    //        m12:'=N52/N51',  //Col N
    //     }
    // )
    // $scope.dataGrid.push(
    //     { //Row 54
    //         title: 'Outlet Share Gap',
    //         m1:'=C53-'+OutletShareTarget,  //Col C
    //         m2:'=D53-'+OutletShareTarget,  //Col D
    //         m3:'=E53-'+OutletShareTarget,  //Col E
    //         m4:'=F53-'+OutletShareTarget,  //Col F
    //         m5:'=G53-'+OutletShareTarget,  //Col G
    //         m6:'=H53-'+OutletShareTarget,  //Col H
    //         m7:'=I53-'+OutletShareTarget,  //Col I
    //         m8:'=J53-'+OutletShareTarget,  //Col J
    //         m9:'=K53-'+OutletShareTarget,  //Col K
    //        m10:'=L53-'+OutletShareTarget,  //Col L
    //        m11:'=M53-'+OutletShareTarget,  //Col M
    //        m12:'=N53-'+OutletShareTarget,  //Col N
    //     }
    // )
    //--------------------------------------------------------------------------
    // Combine dataTitle with dataGrid
    $scope.data = $scope.dataTitle.concat($scope.dataGrid);
    // console.log("data=>",$scope.data);
});
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------