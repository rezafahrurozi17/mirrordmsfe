angular.module('app')
  .factory('MarketAdj', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        console.log(filter);
        // var res=$http.get('/api/ds/OAPRAPAdjustments/?start=1&limit=100&year='+parseInt(filter.year));
        for(key in filter){
          if(filter[key]==null || filter[key]==undefined)
            delete filter[key];
        }

        console.log("Get Data..");
        console.log("$http.get(/api/ds/OAPRAPAdjustments/?start=1&limit=100&"+$httpParamSerializer(filter)+")");
        console.log("================================================================");
        var res=$http.get('/api/ds/OAPRAPAdjustments/?start=1&limit=100&'+$httpParamSerializer(filter));
        return res;
      },
      update: function(oap) {
        var params = [];
        for (var i=0; i<oap.length; i++) {
          var temp={};
          // temp["JanId"] = oap[i]["JanId"];
          // temp["FebId"] = oap[i]["FebId"];
          // temp["MarId"] = oap[i]["MarId"];
          // temp["AprId"] = oap[i]["AprId"];
          // temp["MayId"] = oap[i]["MayId"];
          // temp["JunId"] = oap[i]["JunId"];
          // temp["JulId"] = oap[i]["JulId"];
          // temp["AugId"] = oap[i]["AugId"];
          // temp["SepId"] = oap[i]["SepId"];
          // temp["OctId"] = oap[i]["OctId"];
          // temp["NovId"] = oap[i]["NovId"];
          // temp["DecId"] = oap[i]["DecId"];
          temp["OutletId"] = oap[i]["OutletId"];
          temp["OutletCode"] = oap[i]["OutletCode"];
          temp["AreaId"] = oap[i]["AreaId"];
          temp["Area"] = oap[i]["Area"];
          temp["ProvinceId"] = oap[i]["ProvinceId"];
          temp["ProvinceName"] = oap[i]["ProvinceName"];
          temp["VehicleModelId"] = oap[i]["VehicleModelId"];
          temp["VehicleModelName"] = oap[i]["VehicleModelName"];
          temp["Period"] = oap[i]["Period"];
          temp["AdjustmentFlag"] = oap[i]["AdjustmentFlag"];
          temp["Year"] = oap[i]["Year"];
          temp["Jan"] = oap[i]["Jan"];
          temp["Feb"] = oap[i]["Feb"];
          temp["Mar"] = oap[i]["Mar"];
          temp["Apr"] = oap[i]["Apr"];
          temp["May"] = oap[i]["May"];
          temp["Jun"] = oap[i]["Jun"];
          temp["Jul"] = oap[i]["Jul"];
          temp["Aug"] = oap[i]["Aug"];
          temp["Sep"] = oap[i]["Sep"];
          temp["Oct"] = oap[i]["Oct"];
          temp["Nov"] = oap[i]["Nov"];
          temp["Dec"] = oap[i]["Dec"];
          params.push(temp);
        }
        console.log("Update Data..");
        console.log("$http.put('/api/ds/OAPRAPAdjustments/Multiple',"+JSON.stringify(params)+")");
        console.log("================================================================");
        return $http.put('/api/ds/OAPRAPAdjustments/Multiple', params);
      },
    }
  });