angular.module('app')
    .controller('MarketAdjController', function($scope, $http, CurrentUser, uiGridConstants, ProductModel, Region, MarketAdj, OrgChart, OAPPeriod, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        // $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMarketAdj = {year:0, detail:[]}; //Model
    $scope.xMarketAdj = {};
    $scope.xMarketAdj.selected=[];

    var alerts = [];
    var mDataCopy;

    // $scope.yearData = [ {name:"2016",id:0},
    //                     {name: "2017",id:1}];

    $scope.yearData = [];

    var editableMode = false;

    $scope.filter = {year:null, VehicleModelId:null, areaId:null, provinceId:null};
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.areaId = null;
    $scope.provinceId = null;
    $scope.productModelId = null;
    var mode = 0;
    var editMode = 1;
    var viewMode = 0;

    var outletData = [];

    var areaProvince = [];
    var provinceCityData = [];
    // var orgTreeData = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    var orgData= [];

    var outletObj = null;

    OrgChart.getDataTree().then(
        function(res){
            // orgTreeData = res.data.Result;
            orgData = res.data.Result;
            console.log(orgData);
            console.log("orgData");
            for(i in orgData){
                for(ch in orgData[i].child){
                    outletData.push(orgData[i].child[ch]);
                }
            }
            console.log(outletData);
            // if(orgData.child && orgData.child.length==1){
            //     $scope.filter.outletId = orgData[0].id;
            // }
            Region.getAreaProvince().then(
                function(xres){
                    console.log("areaProvince");
                    var areaProvRes = xres.data.Result;
                    for(var i in areaProvRes){
                        var filteredOutlet = _.find(outletData, function(o){
                            return o.AreaId.indexOf(areaProvRes[i].AreaId)>=0 && o.ProvinceId==areaProvRes[i].ProvinceId;
                        });
                        console.log("filteredOutlet : ",filteredOutlet);
                        if(filteredOutlet != null){
                            areaProvince.push(areaProvRes[i]);
                        }
                    }
                    filterArea();
                    filterProvince();
                    $scope.loading=false;
                }
            );

            $scope.loading=false;
            return res.data;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(filter[key]==null || filter[key]==undefined) continue;
                if(Array.isArray(data[i][key]) && !Array.isArray(filter[key])){
                    if(data[i][key].indexOf(filter[key])<0){
                        valid=false; break;
                    }
                }else if(!Array.isArray(data[i][key]) && Array.isArray(filter[key])){
                    if(filter[key].indexOf(data[i][key])<0){
                        valid=false; break;
                    }
                }else if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0){
                $scope.areaData.push(areaObj);
	    }
        }
        // $timeout(function(){
        if($scope.areaData.length==1){
            // console.log("areaData=>",$scope.areaData);
            $scope.filter.areaId = $scope.areaData[0].AreaId;
        }
        // });
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0) {
                $scope.provinceData.push(provinceObj);
	    }
        }
        // $timeout(function(){
        if($scope.provinceData.length==1){
            // console.log("provinceData=>",$scope.areaData);
            $scope.filter.provinceId = $scope.provinceData[0].ProvinceId;
        }
        // });
    }

    // $scope.changeFilterCombo = function(selected){
    //     $timeout(function(){
    //         if($scope.user.orgId)
    //             outletObj = _.find(outletData,{OutletId:$scope.user.orgId});            
    //         else
    //             outletObj = null;

    //         if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null)
    //             filterProvince({AreaId:$scope.filter.areaId, ProvinceId:outletObj.ProvinceId});
    //         else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null)
    //             filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
    //         else{
    //             filterProvince({AreaId:$scope.filter.areaId});
    //         }

    //         if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null)
    //             filterArea({ProvinceId:$scope.filter.provinceId, AreaId:outletObj.AreaId});
    //         else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null)
    //             filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
    //         else{
    //             filterArea({ProvinceId:$scope.filter.provinceId});
    //         }

    //         // filterDealer({ProvinceId:$scope.filter.provinceId, AreaId:$scope.filter.areaId});
    //     })
    // }

    $scope.changeFilterCombo = function(selected,sender){
        $timeout(function(){
            // if($scope.user.orgId){
            // if(selected!=null)
            //     outletObj = _.find(outletData,{OutletId:$scope.user.orgId});
            // else
            //     outletObj = null;                            
            // }
            // else{
            //     outletObj = null;                          
            // }

            // outletObj = _.find(outletData,{OutletId:$scope.user.orgId});

            if(sender.name!='regionId'){
                // if($scope.filter.areaId!=null && $scope.filter.areaId!=undefined && outletObj!=null){
                //     filterProvince({AreaId:$scope.filter.areaId, ProvinceId:outletObj.ProvinceId});
                // }
                // else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null){
                //     filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
                // }
                // else{
                    filterProvince({AreaId:$scope.filter.areaId});
                // }                
            }

            if(sender.name!='areaId'){
                // if($scope.filter.provinceId!=null && $scope.filter.provinceId!=undefined && outletObj!=null){
                //     filterArea({ProvinceId:$scope.filter.provinceId, AreaId:outletObj.AreaId});
                // }
                // else if(($scope.filter.areaId==null || $scope.filter.areaId==undefined) && outletObj!=null){
                //     filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
                // }
                // else{
                    filterArea({ProvinceId:$scope.filter.provinceId});
                // }                
            }
        })
    };

    /*
    Region.getAreaProvince().then(
        function(res){
            areaProvince = res.data.Result;
            filterArea();
            filterProvince();
            $scope.loading=false;
        }
    );
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    function filterArea(filter){
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
    }
    function filterProvince(filter){
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
    }
    $scope.changeArea = function(selected){
        console.log("change area");
        if(selected==null){
            filterProvince();
        }else{
            filterProvince({AreaId:selected});
        }
    }
    $scope.changeProvince = function(selected){
        console.log("change province");
        if(selected==null){
            filterArea();
        }else{
            filterArea({ProvinceId:selected});
        }
    }
    */

    function changeMode(mode){
        $timeout(function(){
            editableMode = mode==editMode;

            $scope.customActionSettings[0].visible = mode==editMode;
            $scope.customActionSettings[1].visible = mode==editMode;
            $scope.customActionSettings[2].visible = mode==viewMode;
            $scope.customActionSettings[3].visible = mode==viewMode;
            $scope.customActionSettings[4].visible = ($scope.grid.data.length>0) && (mode == viewMode);
            // $scope.customActionSettings[2].visible = ($scope.grid.data.length>0) && (mode == viewMode);
            $scope.grid.enableCellEditOnFocus = mode=editMode;
        },0);
    };

    $scope.customActionSettings = [
        {   
            func: function(string) {
                changeMode(viewMode);
                $scope.grid.data = angular.copy(mDataCopy);
                $scope.mMarketAdj = angular.copy(mDataCopy);
            },
            title: 'Batal',
            visible:false,
            color: '#ccc'
        },{
            func: function(string) {
                $scope.mMarketAdj.detail = $scope.grid.data;
                var updateArr = [];
                // for (var i in $scope.mMarketAdj) {}
                for (var i in $scope.grid.data) {
                    if ($scope.grid.data[i]["AdjustmentFlag"] == true) {
                        updateArr.push($scope.grid.data[i]);
                    }
                }
                // console.log(updateArr);
                // MarketAdj.update($scope.mMarketAdj).then(function(){
                MarketAdj.update(updateArr).then(function(){
                    $scope.getData();
                    changeMode(viewMode);
                });;
            },
            title: 'Simpan',
            visible:false
        },
        {
            func: function(string) {
                var generatedData = [];
                // angular.forEach($scope.areaData, function(area, orgIdx){
                //     angular.forEach($scope.provinceData, function(province, dlrIdx){
                //         angular.forEach($scope.productModelData, function(model, modelIdx){
                //             generatedData.push({Area:area.Description, Province:province.ProvinceName, Model:model.name, Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0});
                //         });
                //     });
                // });
                // XLSXInterface.writeToXLSX(generatedData, 'Market Adjustment - '+$scope.filter.year);

                alerts=[];
                if ($scope.filter.year==null || $scope.filter.VehicleModelId==null || $scope.filter.areaId==null || $scope.filter.provinceId==null) {
                    $scope.showAlert('Mohon Input Filter', alerts.join('\n'), alerts.length);
                } else if ($scope.grid.data.length==0) {
                    $scope.showAlert('Data masih kosong', alerts.join('\n'), alerts.length);
                } else {
                    if ($scope.mMarketAdj.length>0) {
                        console.log(JSON.stringify($scope.mMarketAdj));
                        for (var i in $scope.mMarketAdj) {
                            var temp = {                            
                                // "OutletCode":$scope.mMarketAdj[i]["OutletCode"],    
                                "Area":$scope.mMarketAdj[i]["Area"],
                                "ProvinceName":$scope.mMarketAdj[i]["ProvinceName"],
                                "Period":$scope.mMarketAdj[i]["Period"],
                                "VehicleModelName":$scope.mMarketAdj[i]["VehicleModelName"],
                                "Jan":$scope.mMarketAdj[i]["Jan"],
                                "Feb":$scope.mMarketAdj[i]["Feb"],
                                "Mar":$scope.mMarketAdj[i]["Mar"],
                                "Apr":$scope.mMarketAdj[i]["Apr"],
                                "May":$scope.mMarketAdj[i]["May"],
                                "Jun":$scope.mMarketAdj[i]["Jun"],
                                "Jul":$scope.mMarketAdj[i]["Jul"],
                                "Aug":$scope.mMarketAdj[i]["Aug"],
                                "Sep":$scope.mMarketAdj[i]["Sep"],
                                "Oct":$scope.mMarketAdj[i]["Oct"],
                                "Nov":$scope.mMarketAdj[i]["Nov"],
                                "Dec":$scope.mMarketAdj[i]["Dec"],
                                // "AdjustmentFlag":$scope.mMarketAdj[i]["AdjustmentFlag"]
                                "MarketPolreg":($scope.mMarketAdj[i]["AdjustmentFlag"] ? 'Adjustment' : 'Suggestion')
                            };
                            generatedData.push(temp);
                        }
                        XLSXInterface.writeToXLSX(generatedData, 'Market Adjustment - '+$scope.filter.year);           
                    } else {
                        $scope.showAlert('Tidak Ada Data', alerts.join('\n'), alerts.length);
                    }
                }
            },
            title: 'Download Template',
            rightsBit: 1
        },
        {
            func: function(string) {
                if ($scope.filter.year==null || $scope.filter.VehicleModelId==null || $scope.filter.areaId==null || $scope.filter.provinceId==null) {
                    $scope.showAlert('Mohon Input Filter', alerts.join('\n'), alerts.length);
                } else if ($scope.grid.data.length==0) {
                    $scope.showAlert('Data masih kosong', alerts.join('\n'), alerts.length);
                } else {
                    document.getElementById('uploadMarketAdj').click();
                }
            },
            title: 'Load File',
            rightsBit: 1
        },
        {
            func: function(string) {
                changeMode(editMode);
            },
            visible: false,
            title: 'Edit',
            rightsBit: 2
        }
    ];
    var outletData = [];
    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.parentSelection = [];
    function validateXLS(data){
        var result = [];
        var error = [];
        for(var i=0; i<data.length; i++){
            // var dOutlet = _.find(outletData, { 'title': data[i].Outlet });
            // if(dOutlet == undefined){
            //     error.push('Outlet '+data[i].Outlet+' tidak ditemukan.');
            //     continue;
            // }

            /*
            var dArea = _.find($scope.areaData, { 'Description': data[i].Area });
            var dProvince = _.find($scope.provinceData, { 'ProvinceName': data[i].Province });
            var dModel = _.find($scope.productModelData, { 'name': data[i].Model });            

            if(dArea == undefined){
                error.push('Area '+data[i].Area+' tidak ditemukan.');
                continue;
            }
            if(dProvince == undefined){
                error.push('Provinsi '+data[i].Province+' tidak ditemukan.');
                continue;
            }
            if(dModel == undefined){
                error.push('Model '+data[i].Model+' tidak ditemukan.');
                continue;
            }
            */

            var dGrid = _.find($scope.mMarketAdj, {
                // 'OutletCode': data[i].OutletCode,
                'Area': data[i].Area,
                'ProvinceName': data[i].ProvinceName,
                'VehicleModelName': data[i].VehicleModelName,
                'AdjustmentFlag': (data[i].MarketPolreg == 'Adjustment' ? true : false)
            });
            if(dGrid == undefined){
                error.push('Data '+data[i].Area+' tidak ditemukan.');
                continue;
            }

            var newObj = {
                Jan: isNaN(parseInt(data[i].Jan)) ? 0 : (parseInt(data[i].Jan) < 0 ? 0 : parseInt(data[i].Jan)),
                Feb: isNaN(parseInt(data[i].Feb)) ? 0 : (parseInt(data[i].Feb) < 0 ? 0 : parseInt(data[i].Feb)),
                Mar: isNaN(parseInt(data[i].Mar)) ? 0 : (parseInt(data[i].Mar) < 0 ? 0 : parseInt(data[i].Mar)),
                Apr: isNaN(parseInt(data[i].Apr)) ? 0 : (parseInt(data[i].Apr) < 0 ? 0 : parseInt(data[i].Apr)),
                May: isNaN(parseInt(data[i].May)) ? 0 : (parseInt(data[i].May) < 0 ? 0 : parseInt(data[i].May)),
                Jun: isNaN(parseInt(data[i].Jun)) ? 0 : (parseInt(data[i].Jun) < 0 ? 0 : parseInt(data[i].Jun)),
                Jul: isNaN(parseInt(data[i].Jul)) ? 0 : (parseInt(data[i].Jul) < 0 ? 0 : parseInt(data[i].Jul)),
                Aug: isNaN(parseInt(data[i].Aug)) ? 0 : (parseInt(data[i].Aug) < 0 ? 0 : parseInt(data[i].Aug)),
                Sep: isNaN(parseInt(data[i].Sep)) ? 0 : (parseInt(data[i].Sep) < 0 ? 0 : parseInt(data[i].Sep)),
                Oct: isNaN(parseInt(data[i].Oct)) ? 0 : (parseInt(data[i].Oct) < 0 ? 0 : parseInt(data[i].Oct)),
                Nov: isNaN(parseInt(data[i].Nov)) ? 0 : (parseInt(data[i].Nov) < 0 ? 0 : parseInt(data[i].Nov)),
                Dec: isNaN(parseInt(data[i].Dec)) ? 0 : (parseInt(data[i].Dec) < 0 ? 0 : parseInt(data[i].Dec)),             
                OutletId: dGrid.OutletId,
                OutletCode: dGrid.OutletCode,
                // AdjustmentFlag: data[i].AdjustmentFlag,
                AdjustmentFlag: (data[i].MarketPolreg == 'Adjustment' ? true : false),
                Area: dGrid.Area,
                AreaId: dGrid.AreaId,
                ProvinceName: dGrid.ProvinceName,
                ProvinceId: dGrid.ProvinceId,
                VehicleModelName: dGrid.VehicleModelName,
                VehicleModelId: dGrid.VehicleModelId,
                Period: dGrid.Period,
                Year: dGrid.Year
            };
            var ttl = 0;
            ttl +=  newObj['Jan']+newObj['Feb']+newObj['Mar']+newObj['Apr']+
                    newObj['May']+newObj['Jun']+newObj['Jul']+newObj['Aug']+
                    newObj['Sep']+newObj['Oct']+newObj['Nov']+newObj['Dec'];
            newObj.Total = ttl;
            
            result.push(newObj);
        }
        if(error.length>0){
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );            
        }
        return result;
    }
    $scope.loadXLS=function(o){
        var myEl = angular.element( document.querySelector( '#uploadMarketAdj' ) );

        var a = myEl[0].files[0].name.split(".");
        var fileExt = (a.length === 1 || (a[0] === "" && a.length === 2) ? "" : a.pop().toLowerCase());

        if (fileExt === "xlsx") {
            XLSXInterface.loadToJson(myEl[0].files[0], function(json){
                $timeout(function(){
                    myEl.value = '';
                    document.getElementById('uploadMarketAdj').value = '';
                    $scope.grid.data = validateXLS(json);                
                    // console.log("$scope.grid.data : "+JSON.stringify($scope.grid.data));
                    mDataCopy =  angular.copy($scope.grid.data);
                    changeMode(editMode);
                });
            });
        } else {
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "File yang diupload bukan format .xlsx",
                    // content: error.join('<br>'),
                    // number: error.length
                }
            );   
        }
    };
    OAPPeriod.getData().then(
        function(res){
            // $scope.yearData = res.data.Result;
            var temp = res.data.Result;
            $scope.yearData = [];
            var obj={};
            for (var i in temp) {
                if (!obj[temp[i].Year]) {
                    obj[temp[i].Year] = {};
                    obj[temp[i].Year]["name"] = temp[i].Year;
                    // obj[temp.Year]["id"] = temp.MasterPeriodOAPRAPId;
                    $scope.yearData.push(obj[temp[i].Year]);
                }                    
            }
            
            console.log("$scope.yearData : "+JSON.stringify($scope.yearData));
            $scope.loading=false;
            return res.data;
        }
    );
    // Region.getDataArea().then(
    //     function(res){
    //         $scope.areaData = res.data.Result;
    //         console.log($scope.areaData);
    //         $scope.loading=false;
    //         return res.data;
    //     }
    // );
    // Region.getDataProvince().then(
    //     function(res){
    //         $scope.provinceData = res.data.Result;
    //         console.log($scope.provinceData);
    //         $scope.loading=false;
    //         return res.data;
    //     }
    // );
    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );

    $scope.showAlert = function(title, message, number){
        $.bigBox({
            title: title,
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            // number: ''
        });
    };

    $scope.getData = function() {
        console.log("mau get data");
        alerts=[];
        // if ($scope.filter.year==null || $scope.filter.VehicleModelId==null || $scope.filter.areaId==null || $scope.filter.provinceId==null) {}
        if ($scope.filter.year==null) {
            $scope.showAlert('Mohon Input Filter', alerts.join('\n'), alerts.length);
        } else {
            MarketAdj.getData($scope.filter).then(
                function(res){
                    console.log("getData : "+JSON.stringify(res.data.Result));
                    var total = 0;
                    for (var i in res.data.Result) {
                        total=0;
                        total+=
                        parseInt(res.data.Result[i]['Jan'])+parseInt(res.data.Result[i]['Feb'])+
                        parseInt(res.data.Result[i]['Mar'])+parseInt(res.data.Result[i]['Apr'])+
                        parseInt(res.data.Result[i]['May'])+parseInt(res.data.Result[i]['Jun'])+
                        parseInt(res.data.Result[i]['Jul'])+parseInt(res.data.Result[i]['Aug'])+
                        parseInt(res.data.Result[i]['Sep'])+parseInt(res.data.Result[i]['Oct'])+
                        parseInt(res.data.Result[i]['Nov'])+parseInt(res.data.Result[i]['Dec']);
                        // console.log(total);
                        res.data.Result[i].Total = total;
                    }
                    $scope.grid.data = res.data.Result;
                    $scope.mMarketAdj = res.data.Result;

                    mDataCopy =  angular.copy($scope.grid.data);
                    
                    $scope.loading=false;
                    changeMode(viewMode);
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    $scope.formApi={};

    var lblCellTemplate =   '<div class="ui-grid-cell-contents">'+
                            '   {{ row.entity.AdjustmentFlag ? "Adjustment" : "Suggestion" }}'+
                            '</div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        showGridFooter: true,
        showColumnFooter: true,
        multiSelect: true,
        enableSelectAll: true,
        enableCellEdit: false,
        enableCellEditOnFocus: false,
        cellEditableCondition: function($scope) {
            return $scope.row.entity.AdjustmentFlag && editableMode;
        },
        // {
        //   "OutletId": 49,
        //   "JanId": 56760,
        //   "FebId": 56761,
        //   "MarId": 56762,
        //   "AprId": 56763,
        //   "MayId": 56764,
        //   "JunId": 56765,
        //   "JulId": 56766,
        //   "AugId": 56767,
        //   "SepId": 56768,
        //   "OctId": 56769,
        //   "NovId": 56770,
        //   "DecId": 56771,
        //   "OutletCode": "AUT001117",
        //   "AreaId": 32,
        //   "Area": "Area 4",
        //   "ProvinceId": 28,
        //   "ProvinceName": "Jawa Timur",
        //   "Period": 2016,
        //   "VehicleModelName": "AVANZA",
        //   "VehicleModelId": 28,
        //   "Jan": 78,
        //   "Feb": 34,
        //   "Mar": 19,
        //   "Apr": 94,
        //   "May": 55,
        //   "Jun": 78,
        //   "Jul": 67,
        //   "Aug": 102,
        //   "Sep": 82,
        //   "Oct": 12,
        //   "Nov": 70,
        //   "Dec": 11,
        //   "AdjustmentFlag": true
        // }

        columnDefs: [
            // { name:'OAPRAPAdjustmentId',    field:'OAPRAPAdjustmentId', visible:false },
            // { name:'PeriodAdj', field:'PeriodAdj', visible:false},
            { name:'AreaId', field:'AreaId', visible:false},
            { name:'Area', field:'Area'},
            { name:'ProvinceId', field:'ProvinceId', visible:false },
            { name:'Province', field:'ProvinceName' },
            { name:'VehicleModelId', field:'VehicleModelId', visible:false},
            { name:'Model', field: 'VehicleModelName' },
            { name:'Market Polreg', allowCellFocus: false, width:100,
                                    enableColumnMenu:false,enableSorting: false, enableColumnResizing: false,
                                    cellTemplate: lblCellTemplate},
            { name:'Jan', field: 'Jan', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Feb', field: 'Feb', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Mar', field: 'Mar', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Apr', field: 'Apr', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'May', field: 'May', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jun', field: 'Jun', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Jul', field: 'Jul', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Aug', field: 'Aug', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Sep', field: 'Sep', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Oct', field: 'Oct', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Nov', field: 'Nov', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Dec', field: 'Dec', type: 'number', enableCellEdit: true, aggregationHideLabel: true, aggregationType: uiGridConstants.aggregationTypes.sum },
            { name:'Total', field: 'Total' }
        ]
    };

    $scope.afterRegisterGridApi = function(gridApi) {
        $scope.gridApi = gridApi;
        console.log($scope.gridApi);

        $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            console.log('Inside afterCellEdit..');
            if (newValue<0) {
                newValue = 0;
                rowEntity[colDef.field] = newValue;
            }
            rowEntity.Total+=(newValue-oldValue);
        });
        // uiGridApi.core.registerColumnsProcessor(function(columns, rows){
        //     angular.forEach(columns, function(col){
        //         var filterFormat = col.colDef.filterFormat; 
        //         if ( filterFormat ){ col.filterFormat = filterFormat; }
        //     });
        //     return rows;
        // }, 40);
    };

});
