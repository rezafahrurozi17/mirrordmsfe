angular.module('app')
  .factory('bsFormFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/sys/role');
        //console.log('res=>',res);
        return res;
      },
      getModel: function() {
        res=$http.get('/images/upload/Model.json');
        //console.log('res=>',res);
        return res;
      },
      create: function(role) {
        return $http.post('/sys/role/create', {
                                            title: role.title,
                                            desc: role.desc});
      },
      update: function(role){
        return $http.post('/sys/role/update', {
                                            id: role.id,
                                            title: role.title,
                                            desc: role.desc});
      },
      delete: function(id) {
        return $http.post('/sys/role/delete',{id:id});
      },
    }
  });