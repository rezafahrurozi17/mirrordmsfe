angular.module('app')
    .controller('bsFormController_manual', function($scope, $http, CurrentUser, bsFormFactory,$timeout,$window) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRole = null; //Model
    $scope.cRole = null; //Collection
    $scope.selectedRows = []; //Hold Grid selectedRows
    $scope.mId = 'id';
    $scope.sm_mode='grid';
    //for ui-select
    $scope.xRole = {};
    $scope.xRole.selected=[];
    $scope.role={};
    $scope.role.selected=null;

    $scope.radoptions = [
                        {name:"Alabama",value:"AL"},
                        {name: "Alaska",value: "AK"},
                        {name: "American Samoa",value: "AS"},
                        {name: "Arizona", value: "AZ"},
                      ];

    //RESIZE LAYOUT
    $timeout(function () {
      // Set height initially
      resizeLayout();
    },0);
    var resizeLayout=function() {
      $timeout(function(){
        // $("#header").outerHeight()
        var mainHeight=
                     $("#page-footer").outerHeight()
                    + $(".nav.nav-tabs").outerHeight()
                    + $(".bs-navbar").outerHeight();
        $("#layoutContainer_bsFormDemoManual").height($(window).height() - mainHeight);
      },0);
    };
    angular.element($window).bind('resize', resizeLayout);
    $scope.$on('$destroy', function() {
         angular.element($window).unbind('resize', resizeLayout);
    });

    //HELPER FUNCTION
    $scope.setMode =function(mode){
        var caption='OK',cancelCaption='Cancel',btype='';
        switch (mode){
            case 'new': caption='Save'; btype='New'; break;
            case 'edit': caption='Update'; btype='Edit'; break;
            case 'del': caption='Delete'; btype='Delete'; break;
            case 'grid': caption='OK'; btype=''; break;
            case 'view': caption='OK';cancelCaption='Back'; btype='View'; break;
        }
        $scope.sm_mode=mode;
        $scope.actionCaption=caption;
        $scope.actionCancelCaption=cancelCaption;
        $scope.actionType=btype;
    }
    $scope.upsert = function (arr, key, newval) {
        var newV = angular.copy(newval,newV);
        console.log("arr=>",arr,"key=>",key,"newval=>",newV);
        var match = _.find(arr, key);
        // console.log("match=>",match);
        if(match){
            var index = _.indexOf(arr, _.find(arr, key));
            arr.splice(index, 1, newV);
        }else{
            // console.log("push new rec=>",newV);
            //arr.push(newV);
            arr.splice(0,0,newV);
        }
    }
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.onPopup = function(){
        $scope.xRole.selected=[];
        $scope.role={};
        $timeout(function() { $scope.$broadcast('show-errors-reset'); });
    }
    $scope.getData = function() {
        $scope.cRole = bsFormFactory.getData().then(
            function(res){
                $scope.grid.data = res.data;
                // console.log("role=>",res.data);
                $scope.roleData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.getData();

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Button Action
    //----------------------------------
    $scope.actNew = function() {
        $scope.mRole = {};
        $scope.mRoleCopy =  angular.copy($scope.mRole);
        $scope.setMode('new');
        $scope.sm_act=$scope.doSave;
        $scope.onPopup();
    }
    $scope.actEdit = function(row) {
        $scope.mRole=angular.copy(row);
        // console.log('parent=>',$scope.$parent);
        $scope.mRoleCopy =  angular.copy($scope.mRole);
        $scope.setMode('edit');
        $scope.sm_act = $scope.doSave;
        $scope.onPopup();
    }
    $scope.actDel = function(){
        $scope.mRole=angular.copy($scope.selectedRows[0]);
        $scope.mRoleCopy =  angular.copy($scope.mRole);
        $scope.sm_act = $scope.doDelete;
        $scope.onPopup();
        if($scope.selectedRows.length>1){
            $scope.setMode('delmulti');
            $scope.sm_show2=true;
        }else{
            $scope.setMode('del');
        }
    }
    $scope.actView = function(row){
        $scope.mRole=angular.copy(row);
        $scope.mRoleCopy =  angular.copy($scope.mRole);
        $scope.setMode('view');
    }
    $scope.actRefresh = function(){
        $scope.grid.data = [];
        $scope.actionType=null;
        //GET DATA
        $scope.loading=true;
        $scope.getData();
    }
    //----------------------------------
    // Backend Operation
    //----------------------------------
    $scope.doSave = function(model) {
        if(model[$scope.mId]){
            bsFormFactory.update(model).then(
                function(result) {
                    var key = {};
                    key[$scope.mId] = model[$scope.mId];
                    var match = _.find($scope.grid.data, key);
                    var xmodel = angular.copy(model);
                    if(match){
                        var index = _.indexOf($scope.grid.data, _.find($scope.grid.data, key));
                        $scope.grid.data.splice(index, 1, xmodel);
                    }
                    $scope.resetForm(true);
                },
                function(err){
                    console.log('update err');
                }
            );
        }else{
            bsFormFactory.create(model).then(
                function(result) {
                    $scope.grid.data.splice(0,0,result.data);
                    $scope.resetForm(true);
                    $scope.gridApi.pagination.seek(1);
                },
                function(err){
                    console.log('create err',err);
                }
            );
        }
    }
    $scope.doDelete = function(model) {
        var id;
        if($scope.selectedRows.length>1){
            $scope.sm_show2=false;
            var id=[];
            for(var i=0;i<$scope.selectedRows.length;i++){
              id.push($scope.selectedRows[i][$scope.mId]);
            }
        }else{
            id = model[$scope.mId];
        }
        bsFormFactory.delete(id).then(
            function(result) {
                var deleteds = _.remove($scope.grid.data,
                                function(row) {
                                    var matches = false;
                                    for(var i=0;i<$scope.selectedRows.length;i++){
                                        if(_.find($scope.selectedRows,row)){
                                            matches = true;
                                        }
                                    }
                                    return matches;
                                }
                             );
                $scope.resetForm(true);
            }
        );
    }
    $scope.resetForm = function(clearSelection){
        resetForm($scope.bsFormDemoManual);
        if($scope.$parent.vm=={}) $scope.$parent.vm.options.resetModel();
        $scope.mRole = {};
        $scope.actionType=null;
        $scope.setMode('grid');
        if(clearSelection) $scope.clearSelection();
    }
    $scope.doCancel = function(){
        $scope.resetForm(false);
    }
    //----------------------------------
    // Grid Setup and Function
    //----------------------------------
    $scope.clearSelection = function() {
        $scope.selectedRows=[];
        $scope.gridApi.selection.clearSelectedRows();
    }
    $scope.gridClickViewDetailHandler = function(row){
        // console.log("grid click view=>",row);
        $scope.actView(row);
    }
    $scope.gridClickEditHandler = function(row){
        // console.log("grid click edit=>",row);
        $scope.actEdit(row);
    }
    var btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,
        enableHorizontalScrollbar: 2,
        enableVerticalScrollbar: 2,
        columnDefs: [
            { name:'id',    field:'id', width:'7%' },
            { name:'name', field:'title' },
            { name:'desc',  field: 'desc' },
            { name:'action', allowCellFocus: false, width:100, pinnedRight:true,
                        enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                        cellTemplate: btnActionEditTemplate}
        ]
    };
    $scope.getTableHeight = function() {
        var rowHeight = 30; // your row height
        var headerHeight = 40; // your header height
        var filterHeight = 40; // your filter height
        var pageSize = $scope.grid.paginationPageSize;
        if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
            pageSize = $scope.grid.data.length;
        }
        if(pageSize<4){
          pageSize=3;
        }
        return {
            height: (pageSize * rowHeight + headerHeight) + 32 + "px"
        };
    }
    $scope.grid.onRegisterApi = function(gridApi) {
        // set gridApi on $scope
        $scope.gridApi = gridApi;

        $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              // console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }

});
