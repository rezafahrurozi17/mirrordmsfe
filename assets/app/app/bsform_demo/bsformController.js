angular.module('app')
    .controller('bsFormController', function($scope, $http, CurrentUser, bsFormFactory,$timeout,bsNotify,OrgChart,RoleRights) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log("user=>",$scope.user);
    $scope.rights;
    $scope.getRoleRights = function(orgid,roleid){
        RoleRights.getRights(orgid,roleid,1).then(function(result) {
            $scope.rights = result.data;
            console.log("getRoleRights=>",$scope.rights);
            //rgetChild($scope.data,null);
            //console.log("getRoleRights=>",$scope.data);
            //$scope.grid.api.setRowData($scope.data);
            //$scope.grid.api.sizeColumnsToFit();
        });
    }
    $scope.getRoleRights($scope.user.orgId,$scope.user.roleId);


    $scope.mRole = null; //Model
    $scope.cRole = null; //Collection
    $scope.xRole = {};
    $scope.xRole.selected=219;
    $scope.role={};
    $scope.role.selected=null;
    $scope.ctlOrg = {};
    $scope.hideNewButton=false;
    $scope.gridHideActionColumn=false;

    $scope.testHideNewButton = function(){
        // $scope.hideNewButton=!$scope.hideNewButton;
        // $scope.gridHideActionColumn=!$scope.gridHideActionColumn;
        $scope.hideEditButton=!$scope.hideEditButton;
    }
    $scope.skipDisable1=true; 
    $scope.toggleSkipDisable = function(){
        $scope.skipDisable1 = !$scope.skipDisable1;
        console.log("skip=>",$scope.skipDisable1);
    }


    $scope.radoptions = [
                        {text:"Alabama",value:"AL"},
                        {text: "Alaska",value: "AK"},
                        {text: "American Samoa",value: "AS"},
                        {text: "Arizona", value: "AZ"},
                      ];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.dateOptions = {
        // maxDate: new Date(2016, 9, 29),
        // minDate: new Date(),
        startingDay: 1,
        format: dateFormat,
        disableWeekend: 1
    };
    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        //console.log("RightsByte=",$scope.myRights,Math.pow(2,bit),p);
        var res= (p==Math.pow(2,bit));
        return res;
    }
    $scope.afterRegisterGridApi = function(gridApi){
        console.log(gridApi);
    }

    // CUSTOM ACTION BUTTON
    $scope.customActionSettings = [
        {
          func: function(string) {
            console.log("func1=>",$scope);
            console.log("api=>",$scope.formApi);
            //$scope.formApi.customAction.visible(1, ! $scope.formApi.customAction.visible(1));
            //$scope.formApi.customAction.enable(1, ! $scope.formApi.customAction.enable(1));
            //$scope.formApi.customAction.label(1,'Action 2xxx');
            $scope.customActionSettings[2].title = 'Action 2xxx';
          },
          title: 'Edit',
          icon: 'fa fa-fw fa-edit',
          color: 'green'
        },
        {
          func: function(string) {
            var bit = $scope.formApi.checkRights(5);
            console.log("func2=>",$scope,bit);

          },
          title: 'Approve',
          icon: 'fa fa-fw fa-plus',
          rightsBit: 4
        },
        {
          func: function(string) {
            console.log("func3=>",$scope);
          },
          title: 'Reject',
          icon: 'fa fa-fw fa-minus',
          rightsBit: 5
        },
    ];
    // CUSTOM BULK MENU
    $scope.customBulkSettings = [
        {
          func: function(string) {
            console.log("func_openall=>",$scope);
            console.log("api=>",$scope.formApi);
            //$scope.formApi.customAction.visible(1, ! $scope.formApi.customAction.visible(1));
            //$scope.formApi.customAction.enable(1, ! $scope.formApi.customAction.enable(1));
            //$scope.formApi.customAction.label(1,'Action 2xxx');
          },
          title: 'Open All'
        },
        {
          func: function(string) {
            console.log("func_closeall=>",$scope);
          },
          title: 'Close All'
        },
        {
          func: function(string) {
            console.log("func_approve=>",$scope);
          },
          title: 'Approve',
        },
        {
          func: function(string) {
            console.log("func_reject=>",$scope);
          },
          title: 'Reject',
        },
    ];
    $scope.formApi = {};
    $scope.formGridApi = {};
    $scope.showAdvsearchBtn = {show:true};

    $scope.chkBoxChange = function(val){
        console.log("chk val=>",val);
    }
    $scope.toggleAdvsearchBtn = function(){
        console.log("toggle=>");
        $scope.formApi.showAdvsearchPanel( !$scope.formApi.showAdvsearchPanel() );
        $scope.sm_show2 = true;
    }
    $scope.onValidateSave = function(model,mode){
        console.log("on-validate-save model=>",model,mode);
        return false;
    }
    $scope.doCustomSave = function(model,mode){
        console.log("do-custom-save model=>",model,mode);
    }

    //----------------- Notification -------------------
    $scope.testNotif = function(){
        bsNotify.show(
            {
                title: "Success Message Example",
                content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                type: 'success'
            }
        );
        bsNotify.show(
            {
                title: "Info: James Simmons liked your comment",
                content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                type: 'info'
            }
        );
        bsNotify.show(
            {
                title: "Warning: Warning Message",
                content: "Someone's at the door...shall one get it sir? <p class='text-align-right'><a href-void class='btn btn-primary btn-sm'>Yes</a> <a href-void class='btn btn-danger btn-sm'>No</a></p>",
                type: 'warning'
            }
        );
        bsNotify.show(
            {
                title: "Danger: James Simmons liked your comment",
                content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                type: 'danger'
            }
        );

        bsNotify.show(
            {
                size: 'big',
                title: "Big Information box",
                content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                number: "1"
            }
        );
        bsNotify.show(
            {
                size: 'big',
                type: 'danger',
                title: "Danger Information box",
                content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                number: "2"
            }
        );
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.onPopup = function(){
        //$scope.xRole.selected=[];
        $scope.role={};
    }
    $scope.getData = function() {
        $scope.cRole = bsFormFactory.getData().then(
            function(res){
                $scope.grid.data = res.data;
                // console.log("role=>",res.data);
                $scope.roleData = res.data;
                $scope.loading=false;
                return res.data;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.mOrg = 20;
    // $scope.mOrg={id:9,level:1};
    // $scope.mOrg={id:154,level:2};
    //$scope.tree1Disabled = true;
    $scope.tree1Changed = function(branch){
        console.log("tree1Changed",branch);
    }
    $scope.getOrgChart = function(){
        //console.log('getorgdata',$scope.user);

        OrgChart.getDataByRoleUser($scope.user).then(function(res) {
          //console.log("orgData=>",res.data);
          $scope.orgData = res.data;
          //console.log("ctlOrg=",$scope.ctlOrg);
        });

        // OrgChart.getData().then(function(res) {
         //   console.log("orgData=>",res.data.Result);
        //   $scope.orgData = res.data.Result;
        //   console.log("ctlOrg=",$scope.ctlOrg);
        //   $timeout(function(){
        //     //$scope.ctlOrg.expand_all();
        //   })
        // });
    }
    $scope.getOrgChart();

    $scope.mOrg2=7;
    $scope.getOrgChart2 = function(){
        //console.log('getorgdata2',$scope.user);
        OrgChart.getData().then(function(res) {
          console.log("orgData2=>",res.data.Result);
          $scope.orgData2 = res.data.Result;
          //$scope.orgData3 = angular.copy(res.data.Result);
          //console.log("ctlOrg=",$scope.ctlOrg);
          $timeout(function(){
            //$scope.ctlOrg.expand_all();
          })
        });
    }
    $scope.getOrgChart2();

    $scope.getModel = function(){
        //console.log('getorgdata2',$scope.user);
        bsFormFactory.getModel().then(function(res) {
          console.log("getModel=>",res.data.Result);
          $scope.orgData3 = res.data.Result;
          // console.log("ctlOrg=",$scope.ctlOrg);
          $timeout(function(){
            //$scope.ctlOrg.expand_all();
          })
        });
    }
    $scope.getModel();

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.roleChanged = function(rows){
        console.log("roleChanged=>",rows);
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.onShowDetail = function(row){
        console.log("onShowDetail=>",row);
    }
    $scope.showSelOrg2 = function(selected,sender){
        console.log("showSelOrg2=>",selected,sender);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'id', width:'7%' },
            { name:'name', field:'title' },
            { name:'desc',  field: 'desc' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});
