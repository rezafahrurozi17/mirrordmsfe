angular.module('app')
  .factory('InputHasil', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    this.formApi = {};
    var ApprovalData = {};
    // console.log(currentUser);
    return {
      getData: function(data) {
        console.log("[Factory]");
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        console.log(data);
        var res=$http.get('/api/as/GetStockOpnameAllInputHasil', {params: {
                                                          LocationId : (data.LocationId==null?"n/a":data.LocationId), 
                                                          SCCId: (data.SCCId==null?"n/a":data.SCCId),
                                                          ICCId: (data.ICCId==null?"n/a":data.ICCId),
                                                          StockOpnameStatusId: (data.StockOpnameStatusId==null?"n/a":data.StockOpnameStatusId),
                                                          StockOpnameNo: (data.StockOpnameNo==null?"n/a":data.StockOpnameNo),
                                                          StartDate: (startDate==null?"n/a":startDate),
                                                          EndDate: (endDate==null?"n/a":endDate),
                                                          Role:(data.RoleId == null ? "n/a" : data.RoleId)
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      updateItem:function(data){
        console.log(' tambah data detail =>', data);
        return $http.put('/api/as/StockOpnameItem',data);
      },
      
      // getData: function(data) {
      //   console.log("[Factory]");
      //   console.log(data);
      //   var res=$http.get('/api/as/StockOpname', {params: {
      //                                                     LocationId : (data.LocationId==null?"n/a":data.LocationId), 
      //                                                     SCCId: (data.SCCId==null?"n/a":data.SCCId),
      //                                                     ICCId: (data.ICCId==null?"n/a":data.ICCId),
      //                                                     StockOpnameStatusId: (data.StockOpnameStatusId==null?"n/a":data.StockOpnameStatusId),
      //                                                     StockOpnameNo: (data.StockOpnameNo==null?"n/a":data.StockOpnameNo),
      //                                                     StartDate: (data.StartDate==null?"n/a":data.StartDate),
      //                                                     EndDate: (data.EndDate==null?"n/a":data.EndDate)
      //                                                   } });
      //   console.log('hasil=>',res);
      //   //res.data.Result = null;
      //   return res;
      // },
      create: function(data) {
        console.log(' tambah data=>', data);

        return $http.post('/api/as/TestVehicle', [{
                                            Id: (data.Id==null?0:data.Id),
                                            VehicleName: data.VehicleName,
                                            VehicleType: data.VehicleType

                                          }]);
      },
      update: function(data){
        console.log('rubah data=>', data);
        return $http.put('/api/as/StockOpname', [data]);
      },
      sendNotif: function(data, recepient, Param) {
        console.log("recepient", recepient);
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: "Request Approval untuk Pengajuan Stock Opname" + 
                "<br>" + data.StockOpnameNo +
                "<br>" + data.CancelReasonDesc,
            RecepientId: recepient,
            Param: Param
        }]);
      },
      delete: function(id) {
        console.log("delete id==>",id);
        return $http.delete('/api/as/TestVehicle',{data:id,headers: {'Content-Type': 'application/json'}});
      }
    }
  });
