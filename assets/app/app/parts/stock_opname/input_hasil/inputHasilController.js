angular.module('app')
  .controller('InputHasilController', function($scope, $http, CurrentUser, InputHasil,StockOpname, ngDialog, $timeout, PartsCurrentUser, bsNotify, $filter,
    PartsGlobal, PrintRpt) {

    $scope.formApi = {};
    $scope.ApprovalData = InputHasil.ApprovalData;
    $scope.ApprovalProcessId = 8110;
    //$scope.gridHideActionColumn = true;
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log("$scope.user", $scope.user)

    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;

      $scope.getFilterComponent();
      $timeout(function(){
        console.log("dev_parts 6 Desember 2017 " , document.lastModified);
                console.log("Approve = ", $scope.checkRights(1));
                console.log("myRights => ", $scope.myRights);
                InputHasil.formApi = $scope.formApi;
                // $scope.GenerateDocNum();

                StockOpname.GetWarehouseStockOpname($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, 655).then(function(res) {
                    var gridData = res.data;
                    console.log("Warehouse", gridData);
                    $scope.mWarehouse = gridData;

                    // $scope.mData.WarehouseId = gridData[0].WarehouseId;

                  },
                  function(err) {
                    console.log("err=>", err);
                  }
                );
            });
      // console.log($scope.user);
    });

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.filter={};
    $scope.xRole = {
      selected: []
    };
    $scope.detail={};

    $scope.getFilterComponent = function() {
      $scope.mStockOpnameStatus=[{Id:1,Name:"Open"},{Id:2,Name:"In Progress"},{Id:3,Name:"Request Approval"},{Id:4,Name:"Completed"},{Id:5,Name:"Canceled"}];
      $scope.mTipeLocation=[{Id:1,Name:"Diluar Blocked Stock"},{Id:2,Name:"Blocked Stock"}];
      $scope.mPartType=[{MaterialTypeId:1,Description:"Parts"},{MaterialTypeId:2,Description:"Bahan"}];
      $scope.mWarehouse=[{WarehouseId:1,WarehouseName:"Gudang 1"},{WarehouseId:2,WarehouseName:"Gudang 2"}];
      // StockOpname.getWarehouse().then(function(rs) {
      //     // console.log("getWarehouseController=>",rs);
      //     $scope.mWarehouse = rs.data.Result;
      // });
      StockOpname.getSCC().then(function(rs) {
          // console.log("getSCC=>",rs);
          $scope.mSCC = rs.data.Result;
      });
      StockOpname.getICC().then(function(rs) {
          // console.log("getICC=>",rs);
          $scope.mICC = rs.data.Result;
      });
      StockOpname.getPartType().then(function(rs) {
          // console.log("getPartType=>",rs);
          // $scope.mPartType = rs.data.Result;
          console.log("Material ", $scope.mPartType);
      });
    };
    $scope.onDeleteFilter = function() {
        // $scope.detail.BinTagNo1 = "";
        // $scope.detail.BinTagNo2 = "";
        // $scope.detail.ICCId = "";
        // $scope.detail.SCCId = "";
        // $scope.detail.MaterialName = "";
        // $scope.detail.MaterialNo = "";
        // $scope.detail.BelumCount = "";
        // $scope.detail.DifferentCount = "";
        // $scope.detail.LocationTypeID = "";
        $scope.detail = {};
    }

    $scope.onDeleteFilterList = function() {
        console.log($scope.Copy);
        $scope.filter.WarehouseId = null;
        $scope.filter.SCCId = null;
        $scope.filter.ICCId = null;
        $scope.filter.StockOpnameStatusId = null;
        $scope.filter.startDate = null;
        $scope.filter.endDate = null;
    }

    // Ubah oleh Okyza untuk Approval
    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        // console.log("myRights => ", $scope.myRights);
        return res;
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.filter.RoleId = $scope.user.RoleId;
    $scope.gridDataTree=[];
    $scope.getData = function(filter) {
      // $scope.mData = {};
      console.log("filter", $scope.filter);

      if(Object.keys($scope.filter).length==0)
      {
        bsNotify.show
        ({
            title: "Stock Opname",
            content: "Filter tidak boleh kosong",
            type: 'warning'
        });
      }
      else if((('WarehouseId' in $scope.filter) || ('SCCId' in $scope.filter) || ('ICCId' in $scope.filter) || ('StockOpnameStatusId' in $scope.filter)) && (!('startDate' in $scope.filter) && !('endDate' in $scope.filter)))
      {
        bsNotify.show({
          title: "Stock Opname",
          content: "Tanggal harus mandatory",
          type: 'warning'
        });
      }
      else
      {
        StockOpname.getData($scope.filter).then(function(res) {
            // console.log("Get Data=>",res.data.Result);
            var gridData = res.data.Result;
            $scope.grid.data = [];
            for (var i in gridData){
              gridData[i]["DocDate"] = setDate(gridData[i]["DocDate"]);
              if(gridData[i].LocationTypeID == 1 ){
                gridData[i].LocationTypeIDDesc = "Diluar Blocked Stock";
              } else {
                gridData[i].LocationTypeIDDesc = "Blocked Stock";
              }
              gridData[i]["CreatedDate"] = $scope.localeDate(gridData[i]["CreatedDate"]);
            }
            for(var j in gridData){
             // debugger
              if(gridData[i].LocationTypeID == $scope.filter.LocationTypeID){
                $scope.grid.data.push(gridData[j])
              }
            }
            $scope.loading = false;
            // var tmpRes = res.data.Result;
            //                 _.map(tmpRes, function(val) {
            //                         if (val.LocationTypeID == "1") {
            //                             val.LocationTypeIDDesc = "Diluar Blocked Stock"
            //                         } else {
            //                             val.LocationTypeIDDesc = "Blocked Stock"
            //                         }
            //                     })
            console.log("getDataAll",gridData);
        },
        function(err) {
            console.log("err=>", err);
        });
      }

    $scope.Search = function(filter)
    {
      console.log("filtered by : ", filter);
      console.log("datagrid : ", $scope.grid_stock_opname_item.data);
      $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.MaterialName, undefined);

      if('BinTagNo1' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.BinTagNo1, undefined);
      }
      if('BinTagNo2' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.BinTagNo2, undefined);
      }
      if('SCCId' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.SCCId, undefined);
      }
      if('ICCId' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.ICCId, undefined);
      }
      if('MaterialNo' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.MaterialNo, undefined);
        console.log('kadaaaaallll');
      }
      if('MaterialName' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.MaterialName, undefined);
      }
      if('BelumCount' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, {DifferentCount : 0});
        console.log('masuk blm count');
      }
      if('DifferentCount' in filter)
      {
        // filter.DifferentCount = 1;
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, {DifferentCount : filter.DifferentCount});
        console.log('masuk blm count crot');
      }
      if('LocationTypeID' in filter)
      {
        $scope.grid_stock_opname_item.data = $filter('filter')($scope.mData.detail, filter.LocationTypeID, undefined);
      }


      console.log($scope.grid_stock_opname_item.data);
      $scope.detail = {};
    }

    }
    $scope.onShowDetail = function(model, mode){
        //$scope.formode = 3;
        console.log("mode=>",model,mode);
        if (mode == 'view') {
            console.log("onBeforeEdit ==>", model, mode);
            $scope.mData = model;
            $scope.pageMode="show";
            $scope.getStockOpnameItemByStockOpnameId(model.StockOpnameId);

        } else if (mode == 'edit') {
            console.log("Approve = ", $scope.checkRights(4));
            InputHasil.ApprovalData = model;
            $scope.pageMode="edit";
            if (model.StockOpnameStatusId == 4) {
              $scope.saveComBtnDis  = true;
              console.log('masuk false',$scope.saveComBtnDis);
            }
            $scope.mData = model;
            $scope.getStockOpnameItemByStockOpnameId(model.StockOpnameId);
        } else if (mode=='new') {
            $scope.mData = {};
            $scope.pageMode="new";
        }
    }

    function setDate(dt){
        var d = "";
        dt = new Date(dt);
        d = dt.getFullYear() + "-" + (dt.getMonth()+1) + "-" + dt.getDate();
        return d;
    }

    $scope.onAfterCancel = function() {
      $scope.grid_stock_opname_item.data=[];
    }

    $scope.getStockOpnameItemByStockOpnameId=function(StockOpnameId){
        StockOpname.getStockOpnameItemByStockOpnameId(StockOpnameId).then(function(rs){
            console.log("getStockOpnameItemByStockOpnameId",rs.data.Result);
            // added by sss on 2017-12-20
            //$scope.saveBtnDis = false;
            //$scope.saveComBtnDis = true;
            var initCount = 0;
            var counting = 0;
            var diffCount = 0;

            console.log('INI ISINYA APA',$scope.Qtyvisik);
            for(var i=0; i<rs.data.Result.length; i++) {
 
                // if(rs.data.Result[i].IsiAwalDif != rs.data.Result[i].QtySystem && rs.data.Result[i].IsiAwalDif != null){
                //   rs.data.Result[i].DifferentCount = 1
                // } else if(rs.data.Result[i].IsiAwalDif == rs.data.Result[i].QtySystem && rs.data.Result[i].IsiAwalDif != null && rs.data.Result[i].StockOpnameStatusId < 3){
                //   rs.data.Result[i].DifferentCount = 0
                // } else if(rs.data.Result[i].IsiAwalDif == null && rs.data.Result[i].StockOpnameStatusId < 3){
                //   rs.data.Result[i].DifferentCount = 0
                // }

                rs.data.Result[i].CreatedDate = $scope.localeDate(rs.data.Result[i].CreatedDate);

                var binTag = 0;
                rs.data.Result.forEach(function(material) {
                    binTag += 1;
                    material.BinTag = '000' + binTag;
                    material.BinTag = material.BinTag.substr(material.BinTag.length - 4);
                });

            }

            for(var i=0; i<rs.data.Result.length; i++) {
                if(rs.data.Result[i].Recount == 0) {
                    initCount++;
                }
                if(rs.data.Result[i].Recount == 3 && $scope.Qtyvisik > 0) {
                    counting++;
                    //rs.data.Result[i].Recount == 3
                }
                // if(rs.data.Result[i].Recount == 3 ) {
                //   counting++;
                //   //rs.data.Result[i].Recount == 3
                // }
                if (rs.data.Result[i].QtyDifferent != 0) {
                    diffCount++;
                }
            }

            // if (initCount>0) {
            //     $scope.saveBtnDis = false;
            //     $scope.saveComBtnDis = true;
            // } else {
            //     if (counting>0) {
            //         $scope.saveBtnDis = true;
            //         $scope.saveComBtnDis = false;
            //         console.log("Masuk Pak eko",rs.data.Result[i]);
            //     } else {
            //         $scope.saveComBtnDis = (diffCount>0 ? true:false);
            //     }
            // }
            //
            _.map(rs.data.Result,function(e){
                if(e.DifferentCount==0){
                    // e.DifferentCount=0; //di comment karena differentcount sudah di cek di SP
                }else{
                }
                e.QtyDifferent = (e.QtyPhysical==null ? null : (e.QtyPhysical-e.QtySystem));
            });
            console.log("rs.data.Result : ",rs.data.Result,$scope.grid_stock_opname_item);
            $scope.BarangTersedia = 0;
            $scope.Qtybeda = 0;
            $scope.Sama = 0;
            for(var i=0; i<rs.data.Result.length; i++) {
              if(rs.data.Result[i].QtyPhysical > 0 && rs.data.Result[i].QtyPhysical != null && rs.data.Result[i].QtyPhysical != undefined) {
                $scope.BarangTersedia ++;
              }
              if(rs.data.Result[i].QtyPhysical != rs.data.Result[i].QtySystem) {
                $scope.Qtybeda ++;
              }
              if(rs.data.Result[i].QtyPhysical == rs.data.Result[i].QtySystem) {
                $scope.Sama ++;
              }
              else {
             }
            }
            console.log ("barang",$scope.BarangTersedia);
            console.log ("Qtybeda",$scope.Qtybeda);
            console.log ("$scope.Sama", $scope.Sama);
            $scope.itemDone = 0;
            $scope.PersentaseItem = 0;
            $scope.itemTotal = rs.data.Result.length;
            console.log ("itemTotal",$scope.itemTotal);
            $scope.mData.itemTotal = $scope.itemTotal;
            $scope.mData.itemDone = $scope.BarangTersedia;
            $scope.PersentaseItem = ($scope.mData.itemDone/ $scope.mData.itemTotal) * 100;
            //$scope.PersentaseSelisih = ($scope.mData.TotalSelisih/$scope.mData.SumHarga) * 100;
            $scope.mData.PersentaseItem = $scope.PersentaseItem;
            //$scope.mData.PersentaseSelisih = $scope.PersentaseSelisih;
            for(var i=0; i<rs.data.Result.length; i++) {
              if(rs.data.Result[i].Recount >= 3) {
                console.log("Masuk sini ngga? ")
                rs.data.Result[i].Recount = 3;}
                else{
                  console.log("atau sini ngga? ")
                }

            }
            console.log("$scope.mData ",rs.data.Result);
            $scope.grid_stock_opname_item.data=rs.data.Result;
            $scope.mData.detail=$scope.grid_stock_opname_item.data;
            $scope.Recounts = 0;
            $scope.Qtypisik = 0;
            $scope.Qtyvisik = 0;
            $scope.totalbeda = 0;
            for(var i=0; i<$scope.mData.detail.length; i++) {
              console.log('Mast',$scope.mData.detail[i].Recount);
              if($scope.mData.detail[i].Recount == 3) {
                $scope.Recounts ++;}
              if($scope.mData.detail[i].QtyPhysical == null) {
                $scope.Qtypisik ++;}
                else{

                }

            }
            console.log('$scope.itemTotal',$scope.itemTotal);
            console.log('$scope.Qtybeda',$scope.Qtybeda);
            console.log('$scope.Sama', $scope.Sama);
            console.log('$scope.Recounts',$scope.Recounts);
            console.log('$scope.Qtypisik',$scope.Qtypisik);
            $scope.totalbeda = $scope.Sama + $scope.Recounts - $scope.Qtypisik;
            console.log('totalbeda',$scope.totalbeda);


            if ($scope.totalbeda < $scope.itemTotal){
              $scope.saveBtnDis = false;
              $scope.saveComBtnDis = true;
              console.log("APA nih 3");
            }
            if ($scope.totalbeda == $scope.itemTotal){
              $scope.saveBtnDis = true;
              $scope.saveComBtnDis = false;
              console.log("APA nih 1");
            }
            if ($scope.Recounts == $scope.itemTotal){
              $scope.saveBtnDis = true;
              $scope.saveComBtnDis = false;
              console.log("APA nih 2");
            }
            if ($scope.Qtybeda == 0){
              $scope.saveBtnDis = true;
              $scope.saveComBtnDis = false;
              console.log("APA nih");
            }
            if(rs.data.Result[0].statusdepan == 3) {
              $scope.saveBtnDis = true;
              $scope.saveComBtnDis = true;
              console.log("cobalah");
            }
        });
    }

    $scope.customSave=function(model,mode){
      if(!model.StockOpnameId){
        console.log("Save Create => ",model);
      }else{
        console.log("Save Update => ",model);
        $scope.doUpdateItem(model);
      }
    }

    $scope.saveBtnDis = false;
    $scope.saveComBtnDis = true;

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    // Ubah oleh Okyza untuk Approval
    $scope.UbahData = function(data){
        var invalidQtyFisik = false;
        console.log("UbahData ", data);
        console.log("invalidQtyFisik=>",invalidQtyFisik);

        data.StockOpnameStatusId = 2;
        console.warn('UbahData', data);
        for(var i=0; i<data.detail.length; i++) {
          if (data.detail[i].Recount > 3) {
            data.detail[i].Recount = 3            
            console.warn('UbahData3',  data.detail[i].Recount);
          }
          else{
            console.warn('UbahData2',  data.detail[i].Recount);
          }
          // cek QtyFisik if empty
          // console.log("data.detail[i] a ==>",data.detail[i]);
          // console.log("data.detail[i]['QtyPhysical']==>",data.detail[i]['QtyPhysical']);
          // if (data.detail[i]['QtyPhysical'] == undefined || data.detail[i]['QtyPhysical'] == null) { 
          //   console.log("data.detail[i] b ==>",data.detail[i]);
          //   console.log("data.detail[i]['QtyPhysical']==>",data.detail[i]['QtyPhysical']);
          //   invalidQtyFisik = true;
          //   break;
          // }
          console.log("CreatedDate==>",data.detail[i].CreatedDate);
          // console.log("CreatedDate==>",$scope.localeDate(data.detail[i].CreatedDate));
          // data.detail[i].CreatedDate = $scope.localeDate(data.detail[i].CreatedDate);
        }

        console.log("invalidQtyFisik=>",invalidQtyFisik);
        // if QtyFisik is empty
        if (invalidQtyFisik) {
          PartsGlobal.showAlert({
              title: "Stock Opname",
              content: "Data Qty Fisik ada yg tidak valid ",
              type: 'warning'
          });
          return -1;
        }
      
        // open remark below
        // data.CreatedDate = $scope.localeDate(data.CreatedDate);
        StockOpname.update(data).then(function(res) {
            // console.log("Get Data=>",res.data.Result);
            var gridData = res.data;
            console.warn("Apa isinya ", gridData);
            console.warn("Detail isinya ", data.detail);
            $scope.doUpdateItem(data);
            if(res.data.ResponseCode == 23) {
                bsNotify.show({
                    title: "Stock Opname",
                    content: "Data berhasil diubah",
                    type: 'success'
                });
            }
            else{
                bsNotify.show({
                    title: "Stock Opname",
                    content: "Data gagal diubah",
                    type: 'danger'
                });
            }
            $scope.goBack();
        },
        function(err) {
            console.log("err=>", err);
        });
        // open remark upper

    }

    // Ubah oleh Okyza untuk Approval
    function checkTime(i) {
      if (i < 10) {
          i = "0" + i;
      }
      return i;
    }
    $scope.UbahApproval = function(data){
     
      $scope.tanggalhariini = new Date();
      data.tanggalhariini = $scope.tanggalhariini;
      data.DocDate = new Date(data.DocDate);
      
        var day = $scope.tanggalhariini.getDate();
        var month = $scope.tanggalhariini.getMonth()+1;
        var year = $scope.tanggalhariini.getFullYear();
        day = checkTime(day);
        month = checkTime(month);
        data.DocDate = year + '-' + month + '-' + day;  
        console.log("UbahApproval ", data);
      InputHasil.ApprovalData = data;
      //StockOpname.update(data);
      PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){
        console.log("Updated : ", data.DocDate);
        $scope.Approver = res.data.Result[0].Approvers;
        InputHasil.ApprovalData['Approver'] = $scope.Approver;
        console.log("Approver : ", $scope.Approver);
      });
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/stock_opname/input_hasil/crud/permohonanapproval.html\'\"></div>',
        //templateUrl: '/app/parts/pomaintenance/alasan.html',
        plain: true,
        controller: 'InputHasilController',
        // scope: $scope
      });
    }

    // Ubah oleh Okyza untuk Approval
    $scope.KirimApproval = function(data)
    {
      data.StockOpnameStatusId = 3;
      console.log("kirim approval..",data);

      // open remark below
      StockOpname.update(data).then(function(res) {
          // console.log("Get Data=>",res.data.Result);
          var gridData = res.data;
          console.log("Apa isinya ", gridData);
          console.log("ini data", data);
          console.log("WarehouseId", data.WarehouseId);
          for (var i = 0; i < data.detail.length; i++) {
            data.detail[i].StockOpnameStatusId = data.StockOpnameStatusId;
            // data.detail[i].CreatedDate = $scope.localeDate(data.detail[i].CreatedDate);
          }
          //$scope.doUpdateItem(data.detail);
          InputHasil.updateItem(data.detail);
          if(res.data.ResponseCode == 23)
          {
            if (data.WarehouseId==1016 || data.WarehouseId==1018){
              InputHasil.sendNotif(data, 1128, 43).then(
                function(res) {
                  console.log('terkirim ke KABENG GR')
                });
            }
            else if (data.WarehouseId==1017 || data.WarehouseId==1019){
               InputHasil.sendNotif(data, 1129, 43).then(
              function(res) {
                console.log('terkirim ke KABENG BP')
              });
            }

            // else  {
            //   InputHasil.sendNotifACC(row[0], 1050, 43).then(  //untuk ngirim ke accounting
            //     function(res) {
            //         console.log('terkirim ke Accounting')
            //     });
            // }
            bsNotify.show(
              {
                  title: "Stock Opname",
                  content: "Data berhasil diubah",
                  type: 'success'
              }
            );
          }
          else{
            bsNotify.show(
              {
                  title: "Stock Opname",
                  content: "Data gagal diubah",
                  type: 'danger'
              }
            );
          }
          InputHasil.formApi.setMode("grid");
      },
      function(err) {
          console.log("err=>", err);
      });
      // open remark upper

      // $scope.goBack();
    }

    // Ubah oleh Okyza untuk Approval
    $scope.onBulkApproveIH = function(row)
    {
      console.log("Approve");
      PartsCurrentUser.actApproval({
        ProcessId: 8904, DataId: row[0].StockOpnameId, ApprovalStatus: 1
      })
      
      // if(row.nextstep == row.maxstep)
      //   row[0].StockOpnameStatusId = 4;

      console.log("row : ",row);

      // added by sss on 2017-12-22
      // StockOpname.getStockOpnameItemByStockOpnameId(row[0].StockOpnameId).then(function(rs){
      //     console.log("getStockOpnameItemByStockOpnameId",rs.data.Result);
      //     _.map(rs.data.Result,function(e){
      //         if(e.QtySystem==e.QtyPhysical){
      //             e.DifferentCount=0;
      //         }else{
      //             e.DifferentCount=(e.QtyPhysical==null?0:1);
      //         }
      //         e.QtyDifferent = (e.QtyPhysical==null ? null : (e.QtyPhysical-e.QtySystem));
      //     });
      //     console.log("rs.data.Result : ",rs.data.Result);
      //     row[0].detail = angular.copy(rs.data.Result);

      //     var tempDet = angular.copy(row[0]);
      //     InputHasil.update(row[0]).then(function(res) {
      //         var create = res.data;
      //         console.log(res.data);

      //         // added by sss on 2017-12-21
      //         for (var i in tempDet.detail){
      //             console.log("tempDet.detail[i] : ",tempDet.detail[i]);
      //             tempDet.detail[i].StockOpnameStatusId = 4;
      //         }
      //         console.log("tempDet : ",tempDet);
      //         //
      //         $scope.doUpdateItem(tempDet);
      //         bsNotify.show(
      //           {
      //             title: "Input Hasil",
      //             content: "Data berhasil di-approve",
      //             type: 'success'
      //           }
      //         );
      //       },
      //       function(err) {
      //         console.log("err=>", err);
      //       }
      //     );
      // });

      bsNotify.show({
        title: "Input Hasil",
        content: "Data berhasil di-approve",
        type: 'success'
      });

      // InputHasil.update(row[0]).then(function(res) {
      //     var create = res.data;
      //     console.log(res.data);
      //     $scope.doUpdateItem(row[0]);
      //     bsNotify.show(
      //       {
      //         title: "Input Hasil",
      //         content: "Data berhasil di-approve",
      //         type: 'success'
      //       }
      //     );
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
    }
    // Ubah oleh Okyza untuk Approval
    $scope.onBulkRejectIH = function(row)
    {
      console.log("Reject");
      PartsCurrentUser.actApproval({
        ProcessId: 8904, DataId: row[0].StockOpnameId, ApprovalStatus: 2
      })
      
      // row[0].StockOpnameStatusId = 3;

      // roww[0].CreatedDate = $scope.localeDate(row[0].CreatedDate);
      InputHasil.update(row[0]).then(function(res) {
        var create = res.data;
        console.log(res.data);
        bsNotify.show(
        {
          title: "Input Hasil",
          content: "Data berhasil di-approve",
          type: 'success'
        }
        );
      },
      function(err) {
        console.log("err=>", err);
      }
      );
    }

    $scope.dotSeparateNumber = function(val){
      while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+'.'+'$2');
      }
      return val;

      // var i = parseFloat(amount);
      // if(isNaN(i)) { i = 0.00; }
      // var minus = '';
      // if(i < 0) { minus = '-'; }
      // i = Math.abs(i);
      // i = parseInt((i + .005) * 100);
      // i = i / 100;
      // s = new String(i);
      // if(s.indexOf('.') < 0) { s += '.00'; }
      // if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
      // s = minus + s;
      // return s;
    }

    $scope.doUpdateItem=function(model){
      var recountMax = 0;
      console.log("model==>",model);
      for(var i=0; i<model.detail.length; i++)
      {
        if(model.detail[i].QtyPhysical == null){
          if (model.detail[i].Recount == 0){
              model.detail[i].Recount = 0;
          }
          else{
            model.detail[i].Recount = model.detail[i].Recount;
          }
        }if(model.detail[i].QtyPhysical != model.detail[i].QtySystem && model.detail[i].QtyPhysical != null){
          model.detail[i].Recount = model.detail[i].Recount + 1;
        }
        if(model.detail[i].QtyPhysical == model.detail[i].QtySystem){
          if (model.detail[i].Recount == 0){
              model.detail[i].Recount = 0;
          }
          else{
            model.detail[i].Recount = model.detail[i].Recount;
          }
        }
        if(model.detail[i].Recount == 3)
        {
          recountMax++;
          $scope.goBack();
        }
      }

      if (recountMax>0) {
        bsNotify.show(
          {
            title: "Input Hasil",
            content: "Recount sudah mencapai batas maksimal",
            type: 'success'
          }
        );
      }
      for(var i=0; i<model.detail.length; i++)
      { 
        if (model.detail[i].QtyPhysical == null || model.detail[i].QtyPhysical == undefined){
          model.detail[i].QtyPhysical = model.detail[i].IsiAwalPis;
          model.detail[i].QtyDifferent = model.detail[i].IsiAwalDif;
        }
        else{}
        // model.detail[i].CreatedDate = $scope.localeDate(model.detail[i].CreatedDate);
      }
      
      console.log ("pam pam param pam", model.detail);
      InputHasil.updateItem(model.detail).then(function(rs){

      });
    }

    $scope.AskPrintApproval = function()
    {
      PartsCurrentUser.PrintOutApproval_Ask(20, 7, "Aloha").success(function(res) {
        console.warn(res.Result[0].Results);
        if (res.Result[0].Results === "0")
        {
          bsNotify.show(
            {
              title: "Input Hasil",
              content: "Approval Cetakan berhasil dikirim",
              type: 'success'
            }
          );
        }
        else
        {

        }
      }).error(function(res) {
        console.warn("error cetakan");
      });
    }

    $scope.CetakDifferentList = function(){

      PartsCurrentUser.PrintOutNeedToAsk(20, $scope.mData.StockOpnameId).success(function(res) {
          console.warn(res.Result[0].Results);
          if (res.Result[0].Results === "0")
          {
            console.log("Langsung cetak ", $scope.mData);
            $scope.printDifferentList($scope.mData.StockOpnameId);
          }
          else
          {
            ngDialog.openConfirm ({
              template:'<div ng-include=\"\'app/parts/stock_opname/input_hasil/crud/permohonanapproval.1.html\'\"></div>',
              //templateUrl: '/app/parts/pomaintenance/alasan.html',
              plain: true,
              controller: 'InputHasilController',
            });

          }

      }).error(function(res) {
          console.warn("error cetakan");
      });
      // ngDialog.openConfirm ({
      //   template:'<div ng-include=\"\'app/parts/goodsissue/helper/report_dialog.html\'\"></div>',
      //   plain: true,
      //   // width: 800px,
      //   controller: 'GoodsIssueController',
      //  });

    };

    $scope.CetakBeritaAcara = function(){
      PartsCurrentUser.PrintOutNeedToAsk(20, $scope.mData.StockOpnameId).success(function(res) {
          console.warn(res.Result[0].Results);
          if (res.Result[0].Results === "0")
          {
            console.log("Langsung cetak ", $scope.mData);
            $scope.printBeritaAcaraStockOpname($scope.mData.StockOpnameId);
          }
          else
          {
            ngDialog.openConfirm ({
              template:'<div ng-include=\"\'app/parts/stock_opname/input_hasil/crud/permohonanapproval.1.html\'\"></div>',
              //templateUrl: '/app/parts/pomaintenance/alasan.html',
              plain: true,
              controller: 'InputHasilController',
            });

          }

      }).error(function(res) {
          console.warn("error cetakan");
      });
    };

    // cetak - SSRS
    $scope.printDifferentList = function(StockOpnameId) {
      console.log('StockOpnameId =', StockOpnameId);
      var data = $scope.mData;
      // TODO: Update URL ke Stock Opname
      $scope.dataCetakDifferentList = 'as/PrintDifferentList/' + StockOpnameId;
      $scope.cetakan($scope.dataCetakDifferentList);
    };
    $scope.printBeritaAcaraStockOpname = function(StockOpnameId) {
      console.log('StockOpnameId =', StockOpnameId);
      var data = $scope.mData;
      // TODO: Update URL ke Stock Opname
      $scope.dataBeritaAcaraStockOpname = 'as/BeritaAcaraStockOpname/' + StockOpnameId;
      $scope.cetakan($scope.dataBeritaAcaraStockOpname);
    };

    $scope.cetakan = function(data) {
      var pdfFile = null;

      PrintRpt.print(data).success(function(res) {
          var file = new Blob([res], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);

          console.log("pdf", fileURL);
          //$scope.content = $sce.trustAsResourceUrl(fileURL);
          pdfFile = fileURL;

        if (pdfFile != null) {
          //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
          var ua = navigator.userAgent;
          if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
            var link = document.createElement('a');
            link.href = fileURL;
            //link.download="erlangga_file.pdf";
            link.click();
          }
          else {
            printJS(pdfFile);
          }
        }
        else
          console.log("error cetakan", pdfFile);
      }).error(function(res) {
          console.log("error cetakan", pdfFile);
      });
    };

    $scope.goBack = function()
    {
      $scope.formApi.setMode('grid');
      InputHasil.formApi.setMode("grid");
    };

    //----------------------------------
    // Grid Setup
    //----------------------------------

    $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
    '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.StockOpnameStatusId > 2 || (row.entity.StockOpnameStatusId == 2 && grid.appScope.$parent.user.RoleId != 1134 && grid.appScope.$parent.user.RoleId != 1167)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    '</div>';

    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [{
          name: 'Id',
          field: 'StockOpnameId',
          width: '7%',
          visible: false
        },
        {
          name: 'Tipe Material',
          field: 'MaterialTypeId',
          cellFilter: 'filterMaterialType',
          width: '10%'
        },
        {
          name: 'No Stock Opname',
          field: 'StockOpnameNo',
          width: '15%'
        },
        {
          name: 'Tanggal Stock Opname',
          field: 'DocDate',
          cellFilter: 'date:\'yyyy-MM-dd\'',
          width: '10%'
        },
        {
          name: 'Lokasi Gudang',
          field: 'WarehouseName',
          width: '12%'
        },
        {
          displayName: 'SCC',
          name: 'SCC',
          field: 'SCCName',
          width: '10%'
        },
        {
          displayName: 'ICC',
          name: 'ICC',
          field: 'ICCName',
          width: '10%'
        },
        {
          name: 'Tipe Lokasi Stock',
          field: 'LocationTypeIDDesc',
          width: '15%'
        },
        {
          name: 'Status',
          field: 'StockOpnameStatusId',
          width: '10%',
          visible: false
        },
        {
          name: 'Status',
          width: '8%',
          cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.StatusName(row.entity.StockOpnameStatusId)}}</div>',
        },
        // {
        //   name: 'Action',
        //   width: '8%',
        //   pinnedRight: true,
        //   cellTemplate: $scope.gridActionButtonTemplate
        // }
      ]
    };

    $scope.grid_stock_opname_item = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue, row) {


                console.log('rowEntity', rowEntity);
                console.log('colDef',colDef);
                console.log('newValue',newValue);
                console.log('oldValue',oldValue);
                console.log('Position',row);

                //rowEntity.DifferentCount = 0;
                if (rowEntity.Recount >= 3) {
                  bsNotify.show(
                    {
                      title: "Input Hasil",
                      content: "Recount sudah mencapai batas maksimal",
                      type: 'info'
                    });
                  rowEntity.QtyPhysical = oldValue;
                  //colDef.enableCellEdit = false;
                  return 0;
                }

                rowEntity.QtyDifferent = rowEntity.QtySystem - rowEntity.QtyPhysical;

                // if(rowEntity.DifferentCount == 0)
                // {
                //   colDef.enableCellEdit = false;
                //   // rowEntity.DifferentCount = true;
                //   // rowEntity.Recount = rowEntity.Recount + 1;
                // }
            });
            },
            enableSelectAll: true,
            // cellEditableCondition: function($scope) {
            //     if ($scope.col.field == "QtyPhysical") {
            //         if ($scope.row.entity["QtySystem"] == $scope.row.entity["QtyPhysical"] ) {
            //             return false;
            //         } else {
            //             return true;
            //         }
            //         return true;

            //     }
            // },
            columnDefs: [
                { name: 'No Bin Tag', field: 'BinTag', enableCellEdit: false, width: '8%' },
                { name: 'No Material', field: 'NoMaterial', enableCellEdit: false, width: '10%' },
                { name: 'Nama Material', field: 'NamaMaterial', enableCellEdit: false, width: '20%' },
                { displayName:'ICC', name: 'ICC', field: 'ICCName', enableCellEdit: false, width: '12%' },
                { name: 'Lokasi Rak', field: 'ShelfName', enableCellEdit: false, width: '12%' },
                { name: 'Qty Sistem', field: 'QtySystem', enableCellEdit: false, visible: false},
                { name: 'Qty Fisik', field: 'QtyPhysical', type: 'number', width: '10%' },
                { name: 'Satuan', field: 'UomName', enableCellEdit: false, width: '10%' },
                { name: 'QtyDifferent', field: 'QtyDifferent', visible: false},  // ddg 13-12
                { name: 'Different Count',field:'DifferentCount', width: '10%', cellTemplate: '<center><input type="checkbox" ng-model="row.entity.DifferentCount" ng-true-value="1" ng-false-value="0" disabled/></center>', enableCellEdit: false }, //<input type="checkbox"  ng-model="row.entity.HasilSOQ" disabled>
                // { name: 'Qty Selisih', field: 'QtyDifferent' },
                // { name: 'Selisih Nilai', field: 'AmountDifferent' },
                { name: 'ReCount', field: 'Recount', enableCellEdit: false, width: '8%' },
            ]
        };

        // $scope.grid_stock_opname_item.onRegisterApi=function(gridApi){
        //     console.log('gridApi',gridApi);
        //     $scope.grid_stock_opname_item=gridApi;
        //     gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
        //         console.log('rowEntity',rowEntity);
        //         console.log('gridApi',gridApi);
        //         for(var i =0; i < gridApi.data.length;i++){
        //             if (rowEntity.NoMaterial == gridApi.data[i].NoMaterial){
        //                 gridApi.data[i].QtyDifferent=gridApi.data[i].QtyPhysical - gridApi.data[i].QtySystem;

        //                 if(gridApi.data[i].QtyDifferent==0){
        //                     gridApi.data[i].DifferentCount=0;
        //                 }else{
        //                     gridApi.data[i].DifferentCount=1;
        //                 }

        //                 if(gridApi.data[i].DifferentCount==1){
        //                     gridApi.data[i].Recount = gridApi.data[i].Recount +1;
        //                 }else{
        //                     gridApi.data[i].Recount = gridApi.data[i].Recount;
        //                 }
        //             }
        //         }
        //     });
        // }

        $scope.LocationType = function(LocationTypeID) {
          var LocationType;
          switch (LocationTypeID) {
              case 1:
              LocationType = "Diluar Bloked stock";
                  break;
              case 2:
              LocationType = "Blocked Stock";
                  break;
          }
          return LocationType;
      }



        $scope.StatusName = function(StockOpnameStatusId) {
            var statusName;
            switch (StockOpnameStatusId) {
                case 1:
                    statusName = "Open";
                    break;
                case 2:
                    statusName = "In Progress";
                    break;
                case 3:
                    statusName = "Request Approval";
                    break;
                case 4:
                    statusName = "Completed";
                    break;
                case 5:
                    statusName = "Canceled";
                    break;
            }
            return statusName;
        }

    // $scope.onShowDetail = function(model, mode){
        //$scope.formode = 3;

        //$scope.mode='del';

        // console.log("[$scope] onShowDetail");
        // var shownum = '2';

        // console.log("[SHOW]");

        // //TRAP onBeforeEditMode
        // if ($scope.formode == 2) {
        //     console.log("--->[Active] onBeforeEditMode");
        //     $scope.formode = 0;
        //   }
        // else
        // {
        //   //---------------------
        //   //Preparation View Mode
        //   //---------------------
        //   console.log("--->[Active] onShowDetail");
        //   console.log("Data dari onShowDetail => ");

        //   $scope.crudState = shownum + $scope.formShowHandler();

        //   console.log("[VIEW] " + $scope.crudState);
        //   $scope.isEditState = false;

        // }
    // }




    //----------------------------------
    // Claim Gateway Entry Form
    //----------------------------------
    // $scope.isOverlayForm = false;

    // $scope.isEditState = false;

    // //----------------------------------
    // // CRUD state C = 1, R = 2, U = 3, D = 4
    // // CRUD state + Form Number = 111
    // //----------------------------------
    // $scope.crudState = '';


    // $scope.onOverlayMode = function(kodeClaim){
    //   console.log("[$scope] onOverlayMode");
    //   console.log(kodeClaim);
    //   $scope.mData.selectedRPP = kodeClaim;
    //   $scope.isOverlayForm = false;
    //   var newnum = '1';

    //   $scope.crudState = newnum + $scope.formShowHandler(kodeClaim);
    //   console.log("[OVERLAY] is " + $scope.isOverlayForm);
    //   console.log("[OVERLAY] " + $scope.crudState);

    // }

    // $scope.onSearchMaterial = function(){
    //    console.log("[$scope] onOverlayMode");
    //    $scope.isOverlayForm = false;
    //    var newnum = '1';

    //    $scope.crudState = newnum + $scope.formShowHandler();
    //    console.log("[OVERLAY] is " + $scope.isOverlayForm);
    //    console.log("[OVERLAY] " + $scope.crudState);

    // }



    // $scope.onSelectRows = function(rows) {
    //     console.log("onSelectRows=>", rows);

    //   }

    // //------------------------------------------------------------
    // $scope.formmode={mode:''};
    // $scope.doGeneratePass = function(){
    //     $scope.mUser.password = Math.random().toString(36).slice(-8);
    // }
    // $scope.onBeforeNewMode = function(){
    //     //show gateway page
    //     $scope.isOverlayForm = true;

    //     $scope.formode = 1;
    //     console.log("mode=>",$scope.formmode);
    //     $scope.newUserMode=true;
    //     $scope.editMode=false;
    //     //$scope.mode='new';
    // }
    // $scope.onBeforeEditMode = function(){
    //     $scope.formode = 2;
    //     //$scope.newUserMode=false;
    //     //console.log("mode=>",$scope.formmode);
    //     //$scope.editMode=true;

    //     console.log("[$scope] onBeforeEditMode");
    //     var editnum = '3'

    //     //pemilihan form edit
    //     console.log("Data dari onBeforeEditMode => ");
    //     $scope.isEditState = true;
    //     console.log("[EDIT] ");

    //     $scope.crudState = editnum + $scope.formShowHandler();

    //     console.log("[EDIT] " + $scope.crudState);

    // }
    // $scope.onBeforeDeleteMode = function(){
    //     $scope.formode = 3;
    //     console.log("mode=>",$scope.formmode);
    //     //$scope.mode='del';
    // }





    // // fungsi semi-generik untuk handle halaman tampil
    // $scope.formShowHandler = function(){
    //     $scope.formState = '';
    //     $scope.formState = '00';

    //     console.log($scope.formState);
    //     return $scope.formState;
    // }

    // // ======== tombol  simpan =========
    // $scope.simpan = function(){
    //   ngDialog.openConfirm ({
    //     template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-10"><p><i>Stock Opname ini membutuhkan Approval.</i></p></div></div><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">Stock Opname</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">D1/20160717001</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Stock Opname berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div><button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Batal</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">Kirim Permohonan</button> </div></div></div></div></form></div></div>',
    //     //templateUrl: '/app/parts/pomaintenance/alasan.html',
    //     plain: true,
    //     controller: 'InputHasilController',
    //    });
    // };
    // // ======== end tombol  simpan =========

    // // ======== tombol batal - Claim- alasan =========
    // $scope.batalPC = function(){
    //   ngDialog.openConfirm ({
    //     template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Pembatalan</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <div class="row"><div class="col-md-8"><label class="control-label requiredField" style="color:black;">Alasan<span class="asteriskField">*</span></label> <select class="form-control" style="widht: 100px><option value="Kesalahan Administrasi">Kesalahan Administrasi</option><option value="Kesalahan Pilih Transaksi">Kesalahan Pilih Transaksi</option><option value="Lainnya">Lainnya</option></select><br></div></div><div class="row"><div class="col-md-12"><label class="control-label" style="color:black;">Notes</label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div></div></div> <div class="form-group"><div class="row"><div class="col-md-12"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="okBatalPC()" style="color:white; background-color:#D53337">OK</button> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> </div></div></div></form></div></div>',
    //     plain: true,
    //     controller: 'InputHasilController',
    //    });
    // };

    // // dialog konfirmasi
    // $scope.okBatalPC = function(){
    //   ngDialog.openConfirm ({
    //     template:'\
    //                  <div align="center" class="ngdialog-buttons">\
    //                  <p><b>Konfirmasi</b></p>\
    //                  <p>Anda yakin akan membatalkan Stock Opname ini?</p>\
    //                  <div class="ngdialog-buttons" align="center">\
    //                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
    //                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()">Ya</button>\
    //                  </div>\
    //                  </div>',
    //     plain: true,
    //     controller: 'InputHasilController',
    //    });
    // };

    // // ======== end tombol batal - Claim =========



    // $scope.showMe = function(){
    //                confirm('Apakah Anda yakin menghapus data ini?');
    // };

    // //----------------------------------
    // // GRID FUNCTION HANDLER
    // //----------------------------------
    // // konfirmasi hapus
    // $scope.showMe = function(){
    //                confirm('Apakah Anda yakin menghapus data ini?');
    // };

    // // fungsi LocationId to LocationName
    // $scope.LokasiGudang = function(LocationId){
    //   var LocationName;
    //   switch(LocationId)
    //   {
    //     case 1:
    //       LocationName = "Lokasi Gudang Parts";
    //       break;
    //     case 2:
    //       LocationName = "Lokasi Gudang Parts 2";
    //       break;
    //   }
    //   return LocationName;

    // }

    // // fungsi SCCId to SCCName
    // $scope.SCCName = function(SCCId){
    //   var SccName;
    //   switch(SCCId)
    //   {
    //     case 1:
    //       SccName = "Overhaul Item";
    //       break;
    //     case 2:
    //       SccName = "Body Item";
    //       break;
    //     case 3:
    //       SccName = "Accesories Item";
    //       break;
    //     case 4:
    //       SccName = "CBU NON TAM";
    //       break;
    //     case 5:
    //       SccName = "Discontinue Item";
    //       break;
    //   }
    //   return SccName;
    // }

    // // fungsi ICCId to ICCName
    // $scope.ICCName = function(ICCId){
    //   var iccName;
    //   switch(ICCId)
    //   {
    //     case 1:
    //       iccName = "A - Very Fast Moving";
    //       break;
    //     case 2:
    //       iccName = "B - Fast Moving";
    //       break;
    //     case 3:
    //       iccName = "C - Medium Moving";
    //       break;
    //     case 4:
    //       iccName = "D - Slow Moving";
    //       break;
    //     case 5:
    //       iccName = "E - Very Slow Moving";
    //       break;
    //   }
    //   return iccName;
    // }

    // // fungsi StatusId to StatusName
    // $scope.StatusName = function(StockOpnameStatusId){
    //   var statusName;
    //   switch(StockOpnameStatusId)
    //   {
    //     case 1:
    //       statusName = "Open";
    //       break;
    //     case 2:
    //       statusName = "Completed";
    //       break;
    //   }
    //   return statusName;
    // }

    // //----------------------------------
    // // Dummy grid_input_hasil
    // //----------------------------------
    // $scope.grid_input_hasil = {
    //     enableSorting: true,
    //     enableRowSelection: true,
    //     multiSelect: true,
    //     enableSelectAll: true,
    //     columnDefs: [
    //       { name:'No Bin Tag', field: 'no_bin_tag'},
    //       { name:'No Material', field: 'no_material'},
    //       { name:'Nama Material', field: 'nama_material'},
    //       { displayName:'ICC', name:'ICC', field: 'icc'},
    //       { name:'Lokasi Rak', field: 'lokasi_rak'},
    //       { name:'Qty Fisik', field: 'qty_fisik' },
    //       { name:'Satuan', field: 'satuan'},
    //       { name:'Different Count', cellTemplate: '<center><input type="checkbox"></input></center>'},
    //       { displayName:'ReCount', name:'ReCount', field: 'recount'},
    //     ],
    //     data : [      {
    //                        "no_bin_tag": "001",
    //                        "no_material": "35677-33996",
    //                        "nama_material": "EXPANDER",
    //                        "icc":"A",
    //                        "lokasi_rak": "A01",
    //                        "qty_fisik": "3",
    //                        "satuan": "Pieces",
    //                        "recount": "0",
    //                    },
    //                    {
    //                        "no_bin_tag": "002",
    //                        "no_material": "35677-33996",
    //                        "nama_material": "EXPANDER",
    //                        "icc":"A",
    //                        "lokasi_rak": "A01",
    //                        "qty_fisik": "3",
    //                        "satuan": "Pieces",
    //                        "recount": "0",
    //                    }
    //                ]
    //   };

})

.filter('filterMaterialType', function () {
  var xtipe = {
    '1': 'Parts',
    '2': 'Bahan'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xtipe[input];
    }
  };
})


;
