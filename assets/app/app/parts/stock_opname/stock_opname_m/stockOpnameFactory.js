angular.module('app')
    .factory('StockOpname', function($http, CurrentUser, $filter) {
        var currentUser = CurrentUser.user;
        return {
            getStockOpnamePart: function(data) {
                return $http.get('/api/as/GetStockOpnamePart', {
                    params: {
                        PartTypeId: (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),
                        WarehouseId: (data.WarehouseId == null ? "n/a" : data.WarehouseId),
                        SCCId: (data.SCCId == null ? "n/a" : data.SCCId),
                        ICCId: (data.ICCId == null ? "n/a" : data.ICCId),
                        LocationTypeID: (data.LocationTypeID == null ? "n/a" : data.LocationTypeID)
                    }
                });

            },
            getStockOpnamePartWithPartsCode: function(data, partscode) {
                return $http.get('/api/as/GetStockOpnamePartWithPartsCode', {
                    params: {
                        PartTypeId: (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),
                        WarehouseId: (data.WarehouseId == null ? "n/a" : data.WarehouseId),
                        SCCId: (data.SCCId == null ? "n/a" : data.SCCId),
                        ICCId: (data.ICCId == null ? "n/a" : data.ICCId),
                        PartsCode: (partscode == null ? "n/a" : partscode),
                    }
                });

            },
            getStockOpnameItemByStockOpnameId: function(StockOpnameId) {
                return $http.get('/api/as/StockOpnameItem', { params: { StockOpnameId: StockOpnameId } });
            },
            getWarehouseById: function(id) {
                var res = $http.get('/api/as/Warehouse', { params: { id: id } });
                return res;
            },
            getWarehouse: function() {
                var res = $http.get('/api/as/Warehouse');
                return res;
            },
            getSCC: function() {
                var res = $http.get('/api/as/PartsClasses/GetPartsClassesFromType', { params: { ClassTypeId: 3 } });
                return res;
            },
            getICC: function() {
                var res = $http.get('/api/as/PartsClasses/GetPartsClassesFromType', { params: { ClassTypeId: 2 } });
                return res;
            },
            getPartType: function() {
                var res = $http.get('/api/as/PartsClasses', { params: { ClassTypeId: 1 } });
                return res;
            },
            getDataAll: function() {
                return $http.get('/api/as/GetStockOpnameAll');

            },
            getData: function(data) {
                console.log("[Factory]");
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
                console.log(data);
                var res = $http.get('/api/as/GetStockOpnameAll', {
                    params: {
                        LocationId: (data.LocationId == null ? "n/a" : data.LocationId),
                        SCCId: (data.SCCId == null ? "n/a" : data.SCCId),
                        ICCId: (data.ICCId == null ? "n/a" : data.ICCId),
                        StockOpnameStatusId: (data.StockOpnameStatusId == null ? "n/a" : data.StockOpnameStatusId),
                        StockOpnameNo: (data.StockOpnameNo == null ? "n/a" : data.StockOpnameNo),
                        StartDate: (startDate == null ? "n/a" : startDate),
                        EndDate: (endDate == null ? "n/a" : endDate),
                        Role:(data.RoleId == null ? "n/a" : data.RoleId)
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            create: function(data) {
                console.log(' tambah data=>', data);
                // var StockOpnameNo = '00000/SPG/1707-'+ (Math.floor((Math.random() * 100000) + 1));
                //var currentTimeStamp =
                return $http.post('/api/as/StockOpname', [{
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    StockOpnameNo: (data.StockOpnameNo == null ? 0 : data.StockOpnameNo),
                    OutletId: (data.OutletId == null ? 1 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 1 : data.WarehouseId),
                    DocDate: $filter('date')(new Date(), 'yyyy-MM-dd'),
                    SCCId: (data.SCCId == null ? 1 : data.SCCId),
                    ICCId: (data.ICCId == null ? 1 : data.ICCId),
                    // LocationId: (data.LocationId==null?1:data.LocationId),
                    LocationTypeID: (data.LocationTypeID == null ? 0 : data.LocationTypeID),
                    StockOpnameStatusId: (data.StockOpnameStatusId == null ? 1 : data.StockOpnameStatusId),
                    Notes: (data.Notes == null ? '-' : data.Notes)
                }]);
            },
            createItem: function(data) {
                console.log(' tambah data detail =>', data);
                return $http.post('/api/as/StockOpnameItem', data);
            },
            update: function(data) {
                data.CancelDate = new Date();
                // data.StockOpnameStatusId=5;
                data.CancelUserId = currentUser.UserId;
                console.log('rubah data=>', data);
                return $http.put('/api/as/StockOpname', [data]);
            },
            getPartsDetail: function(id) {
                console.log("getPartsDetail==>", id);
                return $http.get('/api/as/PartsUser/GetPartsDetailOutlet', { params: { PartsCode: id } });
            },
            GetWarehouseStockOpname: function(stid, mtid, oid) {
                var res = $http.get('/api/as/PartsUser/GetWarehouseStockOpname', {
                    params: {
                        ServiceTypeId: stid,
                        MaterialTypeId: mtid,
                        OutletId: oid
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            // sendNotif: function(data, recepient) {
            //     // console.log("model", IdSA);
            //     // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
            //     return $http.post('/api/as/SendNotificationForRole', [{
            //         Message: data,
            //         RecepientId: recepient,
            //         Param : 10
            //     }]);
            // },
            sendNotifADH: function(data, recepient, Param) {
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: "Request Approval untuk Pengajuan Stock Opname" + 
                        "<br>" + data.StockOpnameNo,
                    RecepientId: recepient,
                    Param: Param
                }]);
            },
            sendNotifACC: function(data, recepient, Param) {
            return $http.post('/api/as/SendNotificationForRole', [{
                Message: "Hasil Pengajuan Approval Stock Opname Anda Disetujui" + 
                    "<br>" + data.StockOpnameNo,
                RecepientId: recepient,
                Param: Param
            }]);
            },  
            sendNotifReject: function(data, recepient, Param) {
            return $http.post('/api/as/SendNotificationForRole', [{
                Message: "Hasil Pengajuan Approval Stock Opname Anda Ditolak" + 
                    "<br>" + data.StockOpnameNo,
                RecepientId: recepient,
                Param: Param
            }]);
            },
            GetWarehouseByMaterialTypeId: function(stid, mtid, oid) {
                var res = $http.get('/api/as/PartsUser/GetWarehouseByMaterialTypeId', {
                    params: {
                        ServiceTypeId: stid,
                        MaterialTypeId: mtid,
                        OutletId: oid
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDetailMaterialICCSCC: function(data) {
                return $http.get('/api/as/PartGlobal/PartDetailForStockOpname', {
                    params: {
                        WHId: data.WarehouseId,
                        SCC: data.SCC,
                        ICC: data.ICC,
                        PartsCode: data.partscode,
                        LocationTypeID: data.LocationTypeID
                    }
                });
            },
            getLastApproval: function(DataId) {
                console.log('DataId', DataId);
                var res=$http.get('/api/as/StockOpname/LastApproval', {params: {
                                                                  DataId : DataId,
                                                                } });
                console.log('hasil=>',res);
                return res;
            },
            getNeedApprKabeng: function(DataId) {
                console.log('DataId', DataId);
                var res=$http.get('/api/as/StockOpname/NeedApprKabeng', {params: {
                                                                  DataId : DataId,
                                                                } });
                console.log('hasil=>',res); 
                return res;
            },

            // cancel: function(data){
            //   console.log('rubah data=>', data);
            //   return $http.put('/api/as/StockReturnClaim', [{
            //                                       StockReturnClaimId: (data.StockReturnClaimId==null?0:data.StockReturnClaimId),
            //                                       StockReturnClaimNo: (data.StockReturnClaimNo==null?0:data.StockReturnClaimNo),
            //                                       StockReturnClaimNoTAM: (data.StockReturnClaimNoTAM==null?0:data.StockReturnClaimNoTAM),
            //                                       DocDate: (data.DocDate==null?0:data.DocDate),
            //                                       ShippingNo: (data.ShippingNo==null?0:data.ShippingNo),
            //                                       PurchaseOrderId: (data.PurchaseOrderId==null?0:data.PurchaseOrderId),
            //                                       InvoiceId: (data.InvoiceId==null?0:data.InvoiceId),
            //                                       CaseNo: (data.CaseNo==null?0:data.CaseNo),
            //                                       PartClaimTypeId: (data.PartClaimTypeId==null?0:data.PartClaimTypeId),
            //                                       Partsman: (data.Partsman==null?0:data.Partsman),
            //                                       Notes: (data.Notes==null?0:data.Notes),
            //                                       //Attachment: (data.Attachment==null?0:data.Attachment),
            //                                       StockReturnClaimStatusId: (data.StockReturnClaimStatusId==null?'5':'5')
            //                                     }]);
            // }
        }
    });
