angular.module('app')
    .controller('PartsStockOpnameController', function($scope, $http, InputHasil, CurrentUser, StockOpname, ngDialog, bsNotify, bsAlert, $timeout, $filter, PartsCurrentUser, PrintRpt, PartsGlobal) {


        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;

            $scope.getFilterComponent();
            $timeout(function() {
                console.log("Approve = ", $scope.checkRights(1));
                console.log("myRights => ", $scope.myRights);
                console.log("dev_parts 28 November 2017 ", document.lastModified);
                // $scope.GenerateDocNum();

                StockOpname.GetWarehouseStockOpname($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var gridData = res.data;
                        console.log("Warehouse", gridData);
                        $scope.mWarehouse = gridData;

                        // $scope.mData.WarehouseId = gridData[0].WarehouseId;

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            });
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.show_modalCancel = false;
        $scope.show_modalFilter = false;
        $scope.formApi = {};
        $scope.filter = {};
        $scope.filterMaterial = [];
        $scope.xRole = {
            selected: []
        };

        $scope.captionLabel = {};
        $scope.captionLabel.new = "Buat";
        $scope.pageMode = null;
        $scope.modalMode = "new";

        $scope.filterDetail = false;
        $scope.generateMaterial = false;

        // Initialization
        $scope.onKeyDownResult = "";
        $scope.onKeyUpResult = "";
        $scope.onKeyPressResult = "";
        $scope.gridHideActionColumn = true;

        // Utility functions
        var getKeyboardEventResult = function(keyEvent, keyEventDesc) {
            if (keyEvent.keyCode == 86 && keyEventDesc == "Key up") {
                console.log("Berhasiiiiiil");
                var x = $scope.filterMaterial[0];
                console.log(x);
                var k = x.split(' ');
                console.log(k);
                for (var i = 0; i < k.length; i++) {
                    $scope.filterMaterial[i] = k[i];
                }
            }
            console.log(keyEventDesc + " (keyCode: " + (window.event ? keyEvent.keyCode : keyEvent.which) + ")");
            return keyEventDesc + " (keyCode: " + (window.event ? keyEvent.keyCode : keyEvent.which) + ")";
        };

        // Event handlers
        $scope.onKeyDown = function($event) {
            $scope.onKeyDownResult = getKeyboardEventResult($event, "Key down");
            // $scope.getTheKeyboard($scope.onKeyDownResult, $scope.onKeyUpResult);
        };

        $scope.onKeyUp = function($event) {
            $scope.onKeyUpResult = getKeyboardEventResult($event, "Key up");
            // $scope.getTheKeyboard($scope.onKeyDownResult, $scope.onKeyUpResult);
        };

        $scope.onKeyPress = function($event) {
            $scope.onKeyPressResult = getKeyboardEventResult($event, "Key press");
        };

        // $scope.getTheKeyboard = function(x, y)
        // {
        //     console.log("Pertama " + x + " " + "Kedua " + y);
        //     if(x == )
        // }


        $scope.ResizeContent = function() {
            var tmpheight = $(window).height();
            $('#main').height('auto');
            $('#main').css('min-height', tmpheight);

            $('#layoutContainer_StockOpnameBaru').height('auto');
            $('#layoutContainer_StockOpnameBaru').css('min-height', tmpheight);

        }
        $scope.getFilterComponent = function() {
            $scope.mStockOpnameStatus = [{ Id: 1, Name: "Open" }, { Id: 2, Name: "In Progress" }, { Id: 3, Name: "Request Approval" }, { Id: 4, Name: "Completed" }, { Id: 5, Name: "Canceled" }];
            $scope.mPartType = [{ MaterialTypeId: 1, Description: "Parts" }, { MaterialTypeId: 2, Description: "Bahan" }];
            $scope.mTipeLocation=[{Id:1,Name:"Diluar Blocked Stock"},{Id:2,Name:"Blocked Stock"}];
            // $scope.mWarehouse=[{WarehouseId:0,WarehouseName:"Gudang 0"},{WarehouseId:1,WarehouseName:"Gudang 1"},{WarehouseId:2,WarehouseName:"Gudang 2"}];
            // StockOpname.getWarehouse().then(function(rs) {
            //     // console.log("getWarehouseController=>",rs);
            //     $scope.mWarehouse = rs.data.Result;
            // });
            StockOpname.getSCC().then(function(rs) {
                // console.log("getSCC=>",rs);
                $scope.mSCC = rs.data.Result;
            });
            StockOpname.getICC().then(function(rs) {
                // console.log("getICC=>",rs);
                $scope.mICC = rs.data.Result;
            });
            // StockOpname.getPartType().then(function(rs) {
            //     // console.log("getPartType=>",rs);
            //     $scope.mPartType = rs.data.Result;
            // });
        };

        $scope.checkRights = function(bit) {
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            var i = $scope.getRightX(8, 9);
            return i;
        }
        $scope.getRightServiceType = function() {
            var i = $scope.getRightX(10, 11);
            return i;
        }

        $scope.onDeleteFilter = function() {
            $scope.filter = {};
            $scope.mData = {};
            console.log($scope.user);
        }

        //-------------------------------TAMBAHAN UNTUK APPROVE--------------------


        $scope.onBulkApproveIH= function(data) {
            console.log("data : ", data);
            var mData = {};
            mData = data[0];
            var DataId = mData.StockOpnameId;
            StockOpname.getNeedApprKabeng(DataId).then(function(res) {
                console.log('res getNeedApprKabeng = ', res);
                var ApprovalStatus = res.data.Result["0"].ApprovalStatus;
                var NeedApprovedKabeng = res.data.Result["0"].NeedApprovedKabeng;
        
                if (mData.StockOpnameStatusId == 4) {
                    bsNotify.show(
                    {
                        title: "Stock Opname - Approved",
                        content: "Stock opname sudah disetujui",
                        type: 'danger'
                    });
                    return;
                    // bsAlert.warning("Stock Opname Sudah di Setujui", "");
                } else if (mData.StockOpnameStatusId == 5) {
                    bsNotify.show(
                    {
                        title: "Stock Opname - Canceled",
                        content: "Stock opname sudah dibatalkan",
                        type: 'danger'
                    });
                    return;
                    // bsAlert.warning("Stock Opname Sudah di Batalkan", "");
                } else if (mData.StockOpnameStatusId == 13) {
                    bsNotify.show(
                    {
                        title: "Stock Opname - Rejected",
                        content: "Stock opname sudah ditolak",
                        type: 'danger'
                    });
                    return;
                    // bsAlert.warning("Stock Opname Sudah di Tolak", "");
                } else if (mData.StockOpnameStatusId <= 2) {
                    bsNotify.show(
                    {
                        title: "In Progress",
                        content: "Approval Tidak Dapat Dilakukan, Status Stock Opname In Progress",
                        type: 'warning'
                    }
                    );
                } else if (NeedApprovedKabeng == 1) {
                    bsNotify.show(
                    {
                        title: "Need Approval",
                        content: "Need Approve Kepala Bengkel",
                        type: 'warning'
                    }
                    );
                } else if (ApprovalStatus == 1) {
                    bsNotify.show(
                    {
                        title: "Approved",
                        content: "Sudah di-Approve",
                        type: 'warning'
                    }
                    );
                } else if (ApprovalStatus == 99) {
                    bsNotify.show(
                    {
                        title: "Approval",
                        content: "Dokumen approval tidak ditemukan, silakan hubungi administrator !!",
                        type: 'warning'
                    }
                    );
                } else if (mData.StockOpnameStatusId == 3) {
                    $scope.ApproveIH(data);
                } 
                },
                function(err) {
                console.log("err=>", err);
                }
            );
        }
      
        $scope.ApproveIH = function(row){
            // console.log("Approve");
            console.log("row : ",row);
            $scope.ApproveData = row;
            ngDialog.openConfirm ({
                template:'\
                             <div align="center" class="ngdialog-buttons">\
                             <p><b>Konfirmasi</b></p>\
                             <p>Approve dokumen Stock Opname ini?</p>\
                             <div class="ngdialog-buttons" align="center">\
                               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                               <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                             </div>\
                             </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope:$scope
            });
        };

        $scope.yakinApprove = function(ApproveData){
            console.log('yakinApprove => ', ApproveData);
            console.log("Approve");
            console.log('Coba mau liat ', ApproveData);

            // ApproveData[0].CancelReasonDesc = 'APPROVAL';
            // StockOpname.sendNotif(ApproveData[0].CancelReasonDesc, 1021).then(
            //     function(res) {
            //       console.log('terkirim ke ADH')
            //     });
            if ($scope.user.RoleId==1128 || $scope.user.RoleId==1129){
                StockOpname.sendNotifADH(ApproveData[0], 1021, 10).then(
                    function(res) {
                        console.log('terkirim ke ADH')
                    }
                );
            } else {
                StockOpname.sendNotifACC(ApproveData[0], 1050, 8).then(  //untuk ngirim ke accounting
                    function(res) {
                        console.log('terkirim ke Accounting')
                    }
                );
            }

            PartsCurrentUser.actApproval({
                ProcessId: 8904, DataId: ApproveData[0].StockOpnameId, ApprovalStatus: 1
            }).then(function(res) {
                console.log('res PartsGlobal.actApproval = ', res);

                var DataId = ApproveData[0].StockOpnameId;
                StockOpname.getLastApproval(DataId).then(function(res) {
                    console.log('res getLastApproval = ', res);
                    var WFSeq = res.data.Result["0"].WorkFlowSequence;
                    var IsApproved = res.data.Result["0"].IsApproved;
                    console.log('WFSeq ',WFSeq,'IsApproved ',IsApproved);

                    if(WFSeq == 2 && IsApproved == 1) {

                        bsNotify.show(
                            {
                                // title: "Input Hasil",
                                // content: "Data berhasil di-approve",
                                title: "Approved",
                                content: "Stock Opname has been successfully approved.",
                                type: 'success'
                            }
                        );

                    } else {
                        console.log('Not Last Approval');

                        bsNotify.show(
                            {
                                title: "Approved",
                                content: "Stock Opname has been successfully approved. Next approval to ADH",
                                type: 'succes'
                            }
                        );

                    }

                },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
    
            },
                function(err) {
                    console.log("err=>", err);
                }
            );

            $scope.ngDialog.close();
            $scope.resetForm2();
        };

        $scope.onBulkRejectIH = function(row){
            console.log("row : ",row);
            $scope.ApproveData = row;
            ngDialog.openConfirm ({
                template:'\
                                <div align="center" class="ngdialog-buttons">\
                                <p><b>Konfirmasi</b></p>\
                                <p>Tolak dokumen Stock Opname ini?</p>\
                                <div class="ngdialog-buttons" align="center">\
                                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="RejectApprove(ApproveData)">Ya</button>\
                                </div>\
                                </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope:$scope
            });
        }

        $scope.RejectApprove = function(ApproveData){
            console.log('yakinApprove => ', ApproveData);
            console.log("Approve");
            PartsCurrentUser.actApproval({
                ProcessId: 8904, DataId: ApproveData[0].StockOpnameId, ApprovalStatus: 2
                });
            // StockOpname.sendNotif(data.CancelReasonDesc, 1021).then(
            //     function(res) {
            //         console.log('terkirim ke ADH')
            //     });
            StockOpname.sendNotifReject(ApproveData[0], 1050, 8).then(  //untuk ngirim ke accounting
                function(res) {
                    console.log('terkirim ke Accounting')
                }
            );
            console.log('Coba mau liat ', ApproveData);
            bsNotify.show(
                {
                    title: "Input Hasil",
                    content: "Data berhasil di tolak",
                    type: 'success'
                }
            );
            $scope.ngDialog.close();
            $scope.resetForm2();
        };



        //-------------------------------SAMPAI SINI UNTUK APPROVE--------------------
        $scope.onBulkFilter = function(filterMaterial) {
            // $scope.GenerateDocNum();
            $scope.grid_stock_opname_new = StockOpname.grid_stock_opname_new;
            console.log("Filter material ", filterMaterial);
            $scope.collection = [];

            for (var i = 0; i < filterMaterial.length; i++) {
                //StockOpname.getStockOpnamePartWithPartsCode(StockOpname.mData, filterMaterial[i]).then(function(rs){
                   $scope.namaMaterialSemua = '';
                   for(var i=0; i < 10; i++) {
                       console.log('ini semua material', $scope.filterMaterial[i])
                       if (($scope.filterMaterial[i] != null) && ($scope.filterMaterial[i] != undefined) && ($scope.filterMaterial[i] != '')){
                           $scope.namaMaterialSemua = $scope.namaMaterialSemua + ','+$scope.filterMaterial[i];
                       }
                   }
                   if (($scope.namaMaterialSemua.substring(0, 1)) == ','){
                       $scope.namaMaterialSemua = $scope.namaMaterialSemua.substring(1, $scope.namaMaterialSemua.length);
                   }
                //    var str1 = "'";
                //     $scope.namaMaterialSemua = str1.concat($scope.namaMaterialSemua,str1);
                    console.log('namamaterial semua nya nih', $scope.namaMaterialSemua);
                    console.log('StockOpname.mData', StockOpname.mData);
                    console.log('$scope.mData', $scope.mData);
                    if (StockOpname.mData.WarehouseId == null || StockOpname.mData.WarehouseId == undefined ){
                        bsNotify.show({
                            title: "Stock Opname",
                            content: "Harap Isi Lokasi Gudang",
                            type: 'danger'
                        });
                    }
                    if (StockOpname.mData.MaterialTypeId == null || StockOpname.mData.MaterialTypeId == undefined){
                        bsNotify.show({
                            title: "Stock Opname",
                            content: "Harap Isi Tipe Material",
                            type: 'danger'
                        });
                    }
                    if (StockOpname.mData.LocationTypeID == null || StockOpname.mData.LocationTypeID == undefined ){
                        bsNotify.show({
                            title: "Stock Opname",
                            content: "Harap Isi Tipe Lokasi Gudang",
                            type: 'danger'
                        });
                    }
                    else {
                 StockOpname.getDetailMaterialICCSCC({
                    partscode: $scope.namaMaterialSemua,
                    WarehouseId: StockOpname.mData.WarehouseId,
                    ICC: StockOpname.mData.ICCId,
                    SCC: StockOpname.mData.SCCId,
                    LocationTypeID: StockOpname.mData.LocationTypeID

                    //partscode: $scope.namaMaterialSemua ini sementara
                }).then(function(rs) {
                    $scope.ResizeContent();
                        var gridData = rs.data.Result;
                        
                        console.log("Iterasi " + i + " ");
                        console.log(gridData);
                        if ((typeof gridData[0] === undefined || gridData[0] == null)) {
                            bsNotify.show({
                                title: "Stock Opname",
                                content: "Material " + filterMaterial[i] + " tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            for (var i = 0; i < gridData.length; i++) {
                            gridData[i].QtyPhysical = null; //0;
                            if (gridData[i].LocationTypeID == 1){
                                gridData[i].QtySystem = gridData[i].QtyOnHand - gridData[i].QtyBlocked;
                            }
                            else{
                                gridData[i].QtySystem = gridData[i].QtyOnHand;
                            }

                            //gridData[0].QtyDifferent = gridData[0].QtySystem - gridData[0].QtyPhysical;
                            // gridData[0].QtyDifferent = ;
                            console.log('forestGum',$scope.grid_stock_opname_new.data.length);

                            gridData[i].DifferentCount = 0;
                            gridData[i].Recount = 0;
                            $scope.ngDialog.close();}

                            $scope.collection.push(gridData[i]);}
                            
                            
                            if ($scope.grid_stock_opname_new.data.length <= 0) {
                            //   var n = $scope.grid_stock_opname_new.data.length;
                            //   $scope.grid_stock_opname_new.data[n] = gridData[i];
                            for(var j in gridData){
                              var binTag = $scope.grid_stock_opname_new.data.length;

                              //Untuk Bintag
                              binTag += 1;

                              gridData[j].BinTag = '000' + binTag;
                              gridData[j].BinTag = gridData[j].BinTag.substr(gridData[j].BinTag.length - 4);

                                $scope.grid_stock_opname_new.data.push(gridData[j])
                            }
                            } else {
                                var ada_parts_sama = 0;
                                for (var i = 0; i < $scope.grid_stock_opname_new.data.length; i++) {
                                    for (var x=0; x<gridData.length; x++){
                                        if ($scope.grid_stock_opname_new.data[i].PartId==gridData[x].PartId) {
                                            ada_parts_sama++;
                                            bsNotify.show({
                                                title: "Stock Opname",
                                                content: "Material dengan Kode " + gridData[x].PartsCode + " Tidak boleh duplikat",
                                                type: 'danger'
                                            });
                                        }
                                    }
                                }

                                if (ada_parts_sama == 0){
                                    //for (var i = 0; i < $scope.grid_stock_opname_new.data.length; i++) {
                                        for (var x=0; x<gridData.length; x++){
                                            var binTag = $scope.grid_stock_opname_new.data.length;

                                            //Untuk Bintag
                                            binTag += 1;

                                            gridData[x].BinTag = '000' + binTag;
                                            gridData[x].BinTag = gridData[x].BinTag.substr(gridData[x].BinTag.length - 4);
                                            $scope.grid_stock_opname_new.data.push(gridData[x])
                                        }
                                    
                                   // }
                                }
                            
                              

                            console.log('idolakue',$scope.grid_stock_opname_new.data );
                            //  $scope.namaMaterialSemua = '';
                            // for(var i=0; i < 10; i++) {
                            //     console.log('ini semua material', $scope.filterMaterial[i])
                            //     if (($scope.filterMaterial[i] != null) && ($scope.filterMaterial[i] != undefined) && ($scope.filterMaterial[i] != '')){
                            //         $scope.namaMaterialSemua = $scope.namaMaterialSemua + ','+$scope.filterMaterial[i];
                            //     }
                            // }
                            // if (($scope.namaMaterialSemua.substring(0, 1)) == ','){
                            //     $scope.namaMaterialSemua = $scope.namaMaterialSemua.substring(1, $scope.namaMaterialSemua.length);
                            // }
                            // console.log('namamaterial semua nya nih', $scope.namaMaterialSemua);
                            $scope.ngDialog.close();
                        }

                    },
                    function(err) {
                        console.log("err=>", err);
                    });
                }
            }

            $timeout(function() {
                // .map($scope.grid_stock_opname_new.data,function(e){
                //     if(e.QtySystem==e.QtyPhysical){
                //         e.DifferentCount=0;
                //     }else{
                //         e.DifferentCount=1;
                //     }
                //     e.QtyDifferent = e.QtyPhysical-e.QtySystem;
                // });

                // $scope.grid_stock_opname_new.data.map($scope.grid_stock_opname_new.data, function(e){
                //         if(data.LocationTypeID==1){
                //             e.QtySystem=e.QtyOnHand;
                //         }else{
                //             e.QtySystem=e.QtyBlocked;
                //         }
                //         e.QtyPhysical=0;
                //         e.DifferentCount=1;
                //         e.QtyDifferent=0;
                //         e.AmountDifferent=0;
                //         e.Recount=0;
                //     });

                console.log("collection ", $scope.collection);
                // $scope.grid_stock_opname_new.data = $scope.collection;
                console.log("BulkFilter ", $scope.grid_stock_opname_new.data[0]);
            });
        }

        $scope.dotSeparateNumber = function(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
            }
            return val;

            // var i = parseFloat(amount);
            // if(isNaN(i)) { i = 0.00; }
            // var minus = '';
            // if(i < 0) { minus = '-'; }
            // i = Math.abs(i);
            // i = parseInt((i + .005) * 100);
            // i = i / 100;
            // s = new String(i);
            // if(s.indexOf('.') < 0) { s += '.00'; }
            // if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
            // s = minus + s;
            // return s;
        }

        $scope.selectWarehouse = function(MaterialTypeId) {
            console.log("MaterialTypeId ", MaterialTypeId);
            if (MaterialTypeId == 3) {
                StockOpname.GetWarehouseStockOpname($scope.mData.ServiceTypeId, MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var gridData = res.data;
                        console.log("Warehouse", gridData);
                        $scope.mWarehouse = gridData;

                        // $scope.mData.WarehouseId = gridData[0].WarehouseId;

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            } else {
                StockOpname.GetWarehouseByMaterialTypeId($scope.mData.ServiceTypeId, MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var gridData = res.data;
                        console.log("Warehouse", gridData);
                        $scope.mWarehouse = gridData;

                        // $scope.mData.WarehouseId = gridData[0].WarehouseId;

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

        }

        //----------------------------------
        // Get Data
        //----------------------------------
        // $scope.gridDataTree = [];

        //Cari Warehouseid
        console.log("$scope.user.RoleId", $scope.user.RoleId);
        $scope.filter.RoleId = $scope.user.RoleId;
        $scope.getData = function() {
            console.log("filter", $scope.filter);
            console.log("isinyaah", Object.keys($scope.filter));

            if (Object.keys($scope.filter).length == 0) {
                bsNotify.show({
                    title: "Stock Opname",
                    content: "Filter tidak boleh kosong",
                    type: 'warning'
                });
            } else if ((('LocationId' in $scope.filter) || ('SCCId' in $scope.filter) || ('ICCId' in $scope.filter) || ('StockOpnameStatusId' in $scope.filter)) && (!('startDate' in $scope.filter) && !('endDate' in $scope.filter))) {
                bsNotify.show({
                    title: "Stock Opname",
                    content: "Tanggal harus diisi",
                    type: 'warning'
                });
            } else {
                if (Object.keys($scope.filter).length == 0) {
                    StockOpname.getDataAll().then(function(rs) {
                            var gridData = rs.data.Result;
                            for (var i in gridData) {
                                gridData[i]["DocDate"] = setDate(gridData[i]["DocDate"]);
                                gridData[i]["CreatedDate"] = $scope.localeDate(gridData[i]["CreatedDate"]);
                            }
                            $scope.grid.data = gridData;
                            $scope.loading = false;
                        },
                        function(err) {
                            console.log("err=>", err);
                        });
                } else {
                    console.log("isi=>");
                    StockOpname.getData($scope.filter).then(function(res) {
                            console.log("Get Data=>",res.data.Result);
                            for (var i = 0; i < res.data.Result.length; i++) {
                                if(res.data.Result[i].LocationTypeID == 1){
                                    res.data.Result[i].TipeLocation = 'Diluar Blocked Stock';
                                }
                                else{
                                    res.data.Result[i].TipeLocation = 'Block Stock';
                                }
                            }

                            var gridData = res.data.Result;
                            for (var i in gridData) {
                                gridData[i]["DocDate"] = setDate(gridData[i]["DocDate"]);
                                gridData[i]["CreatedDate"] = $scope.localeDate(gridData[i]["CreatedDate"]);
                            }
                            $scope.grid.data = gridData;
                            $scope.loading = false;
                            var tmpRes = res.data.Result;
                            _.map(tmpRes,function(val){
                                if(val.MaterialTypeId == "1"){
                                    val.MaterialTypeIdDesc = "Parts"
                                }else{
                                    val.MaterialTypeIdDesc = "Bahan"
                                }
                                if (val.StockOpnameStatusId == 1){
                                  val.StockOpnameStatusIdDesc = "Open"
                                }else if (val.StockOpnameStatusId == 2){
                                  val.StockOpnameStatusIdDesc = "In Progress"
                                }else if (val.StockOpnameStatusId == 3){
                                  val.StockOpnameStatusIdDesc = "Request Approval"
                                }else if (val.StockOpnameStatusId == 4){
                                  val.StockOpnameStatusIdDesc = "Completed"
                                }else if (val.StockOpnameStatusId == 5){
                                  val.StockOpnameStatusIdDesc = "Canceled"
                                }
                            })
                            // console.log("getDataAll",gridData);
                        },
                        function(err) {
                            console.log("err=>", err);
                        });
                }
            }

        }

        // $scope.onSelectRows = function(rows) {
        //     console.log("onSelectRows=>", rows);

        // }


        // kedua scope di bawah harus bernilai 1 maka batal SO akan disabled
        $scope.disabledBatalSOstatus = 0;
        $scope.disabledBatalSOrecount = 0;


        $scope.BtnBatalSO = null;
        $scope.onBeforeEdit = function(model, mode) {
            $scope.hideCancelButton = true;
            console.log("model ==> ", model);
            if (model.StockOpnameStatusIdDesc == "Completed"){
                $scope.BtnBatalSO = true;
            } else {
                $scope.BtnBatalSO = false;
            }

            if (model.StockOpnameStatusIdDesc == "In Progress"){
                $scope.disabledBatalSOstatus = 1;
            }


            console.log('status btn', $scope.BtnBatalSO);
            // Get General Paramaneter
            $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
            $scope.mData.ServiceTypeId = $scope.getRightServiceType();
            console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
            console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);

            if ($scope.mData.MaterialTypeId == 0 || $scope.mData.ServiceTypeId == 0 || $scope.mData.MaterialTypeId == 3 || $scope.mData.ServiceTypeId == 3) {
                // alert('Material Type atau Service Type salah. Silakan hubungi administrator');
                console.log("Material Type atau Service Type salah. Silakan hubungi administrator");
                // $scope.goBack();
            }
            // else
            // {
            //    $scope.GenerateDocNum();
            // }


            // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
            //     var gridData = res.data;
            //     console.log("Warehouse", gridData);
            //     $scope.mWarehouse = gridData;

            //     $scope.mData.WarehouseId = gridData[0].WarehouseId;

            //     },
            //     function(err) {
            //     console.log("err=>", err);
            //     }
            // );
            $scope.mPartType = [{ MaterialTypeId: 1, Description: "Parts" }, { MaterialTypeId: 2, Description: "Bahan" }];

            $scope.filterDetail = true;
            if (mode == 'view') {
                console.log("onBeforeEdit ==>", model, mode);
                $scope.mData = model;
                $scope.pageMode = "show";
                $scope.getStockOpnameItemByStockOpnameId(model.StockOpnameId);

            } else if (mode == 'edit') {
                // $scope.mData = model;
                $scope.pageMode = "edit";
                console.log("onBeforeEdit ==>", model, mode);
                $scope.getStockOpnameItemByStockOpnameId(model.StockOpnameId);

            } else if (mode == 'new') {
                console.log("create new..");
                $scope.hideSaveButton = false;
                $scope.GenerateDocNum();
                $scope.mData = {};
                $scope.pageMode = "new";
                $scope.grid_stock_opname_new.data = [];
            }
        }

        $scope.getStockOpnameItemByStockOpnameId = function(StockOpnameId) {
            $scope.hideCancelButton = true;
            StockOpname.getStockOpnameItemByStockOpnameId(StockOpnameId).then(function(rs) {
                console.log("getStockOpnameItemByStockOpnameId", rs.data.Result,$scope.mData);

                for (var i=0; i < rs.data.Result.length; i++){
                    if (rs.data.Result[i].Recount > 0){
                        $scope.disabledBatalSOrecount = 1;
                    }
                }

                //$scope.itemDone = rs.data.Result.length;
                console.log("rs.data.Result.detail", rs.data.Result);
                for(var i=0; i<rs.data.Result.length; i++) {
                    if(rs.data.Result[i].QtyPhysical == null){
                        rs.data.Result[i].QtyDifferent = 0;
                    }
                    else{

                    }
                    rs.data.Result[i].CreatedDate = $scope.localeDate(rs.data.Result[i].CreatedDate);
                }
                for(var i=0; i<rs.data.Result.length; i++) {
                    if(rs.data.Result[i].QtyPhysical != rs.data.Result[i].QtySystem && rs.data.Result[i].QtyPhysical != null){
                      rs.data.Result[i].DifferentCount = 1;
                      rs.data.Result[i].AmountDifferent = Math.abs(rs.data.Result[i].QtyDifferent * rs.data.Result[i].StandardCost);
                      console.log("U");
                    } else if(rs.data.Result[i].QtyPhysical == rs.data.Result[i].QtySystem && rs.data.Result[i].QtyPhysical != null){
                      rs.data.Result[i].DifferentCount = 0;
                      rs.data.Result[i].AmountDifferent = Math.abs(rs.data.Result[i].QtyDifferent * rs.data.Result[i].StandardCost);
                      console.log("T");
                    } else if(rs.data.Result[i].QtyPhysical == null){
                      rs.data.Result[i].DifferentCount = 0;
                      rs.data.Result[i].AmountDifferent = Math.abs(rs.data.Result[i].QtyDifferent * rs.data.Result[i].StandardCost);
                      console.log("A");
                    }
                }
                $scope.itemDone = 0;
                // _.map(rs.data.Result, function(e) {
                //   if ($scope.mData.StockOpnameStatusId == 4) {
                //     if (e.QtySystem == e.QtyPhysical) {
                //         e.DifferentCount = 0;
                //     } else {
                //         e.DifferentCount = (e.QtyPhysical == null ? 0 : 1);
                //     }
                //     e.QtyDifferent = (e.QtyPhysical == null ? null : (e.QtyPhysical - e.QtySystem));
                //   }else {
                //     e.DifferentCount = 0;
                //     e.QtyDifferent = 0;
                //   }

                // });
                // _.map(rs.data.Result, function(e) {
                //     e.AmountDifferent = Math.abs(e.QtyDifferent * e.StandardCost);
                // });

                // for(var i=0; i<rs.data.Result.length; i++) {
                //     if(rs.data.Result[i].QtySystem == rs.data.Result[i].QtyPhysical){
                //         rs.data.Result[i].DifferentCount = 0
                //     }
                //     else{
                //         rs.data.Result[i].DifferentCount = (rs.data.Result[i].QtyPhysical == null ? null : (rs.data.Result[i].QtySystem - rs.data.Result[i].QtyPhysical));
                //     }
                // }

                $scope.BarangTersedia = 0;

                for(var i=0; i<rs.data.Result.length; i++) {
                if(rs.data.Result[i].QtyPhysical > 0 && rs.data.Result[i].QtyPhysical != null && rs.data.Result[i].QtyPhysical != undefined) {
                    $scope.BarangTersedia ++;
                }
                else {
                }

                }
                console.log ("barang",$scope.BarangTersedia);

                $scope.HargaAwal = 0;
                $scope.SelisihAwal = 0;
                $scope.SumHarga = 0;
                $scope.SumSelisih = 0;
                for(var i=0; i<rs.data.Result.length; i++) {
                    $scope.HargaAwal = rs.data.Result[i].QtySystem * rs.data.Result[i].StandardCost;
                    //$scope.SelisihAwal = (rs.data.Result[i].QtySystem - rs.data.Result[i].QtyPhysical )* rs.data.Result[i].StandardCost;
                    $scope.SelisihAwal = rs.data.Result[i].AmountDifferent;
                    $scope.SumHarga = $scope.SumHarga + $scope.HargaAwal;
                    $scope.SumSelisih = $scope.SumSelisih + $scope.SelisihAwal;
                }
                console.log("I smile", $scope.SelisihAwal);
                console.log("$scope.SumHarga",  $scope.SumHarga);
                console.log("$scope.SumSelisih",  $scope.SumSelisih);
                //$scope.TotalSelisih = 0;
                //$scope.TotalSelisih = $scope.SumHarga - $scope.SumSelisih;
                $scope.TotalSelisih = $scope.SumSelisih;
                $scope.mData.SumHarga = $scope.SumHarga;
                $scope.mData.TotalSelisih = $scope.TotalSelisih;
                $scope.itemDone = 0;
                $scope.itemTotal = rs.data.Result.length;
                $scope.mData.itemTotal = $scope.itemTotal;
                $scope.mData.itemDone = $scope.BarangTersedia;
                $scope.PersentaseItem = ($scope.mData.itemDone/ $scope.mData.itemTotal) * 100;
                $scope.PersentaseSelisih = ($scope.mData.TotalSelisih/$scope.mData.SumHarga) * 100;
                console.log("ini Total Presentasi", $scope.PersentaseSelisih);
                $scope.mData.PersentaseItem = $scope.PersentaseItem;
                $scope.mData.PersentaseSelisih = $scope.PersentaseSelisih;
                var binTag = 0;
                rs.data.Result.forEach(function(material) {
                    binTag += 1;
                    material.BinTag = '000' + binTag;
                    material.BinTag = material.BinTag.substr(material.BinTag.length - 4);
                });
                $scope.grid_stock_opname_item.data = rs.data.Result;
                $scope.mData.detail = $scope.grid_stock_opname_item.data;
                console.log("$scope.mData ==>", $scope.mData);
            });
        }

        $scope.onBeforeCancel = function() {
            $scope.filterDetail = false;
            $scope.generateMaterial = false;
            console.log("onBeforeCancel ==>");
        }

        $scope.searchMaterial = function() {
            console.log("searchMaterial ==> ", $scope.mData);
            StockOpname.mData = $scope.mData;
            StockOpname.grid_stock_opname_new = $scope.grid_stock_opname_new;

            $scope.generateMaterial = true;
            //$scope.show_modalFilter=true;
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/stock_opname/stock_opname_m/stockOpnameFilter.html\'\"></div>',
                //templateUrl: '/app/parts/pomaintenance/alasan.html',
                plain: true,
                controller: 'PartsStockOpnameController'
            });
        }

        $scope.doFilter = function(model) {
            console.log("doFilter ==>", model);
            $scope.generateMaterial = true;
            if (typeof model.MaterialNo === undefined || model.MaterialNo === "" || model.MaterialNo == null) {
                //filter auto
                console.log("null undefined kosong");
                $scope.getStockOpnamePart(model);

            } else if (!('MaterialNo' in model)) {
                console.log("No here");
                $scope.getStockOpnamePart(model);
            } else {
                StockOpname.getStockOpnamePartWithPartsCode(model, model.MaterialNo).then(function(rs) {
                        var gridData = rs.data.Result;
                        console.log('Coba',gridData[0]);
                        gridData[0].LocationTypeID = model.LocationTypeID;
                        console.log("Coba rizma",gridData[0]);
                        if ((typeof gridData[0] === undefined || gridData[0] == null)) {
                            bsNotify.show({
                                title: "Stock Opname",
                                content: "Material tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            if (rs.data.Result.length > 0) {
                                // _.map(rs.data.Result, function(e) {
                                //     if (gridData[0].LocationTypeID == 1) {
                                //         e.QtySystem = e.QtyOnHand  - e.QtyBlocked;
                                //     } else {
                                //         e.QtySystem = e.QtyOnHand;
                                //     }
                                //     e.QtyPhysical = 0;
                                //     e.DifferentCount = 1;
                                //     e.QtyDifferent = 0;
                                //     e.AmountDifferent = 0;
                                //     e.Recount = 0;
                                // });
                                for(var i=0; i<rs.data.Result.length; i++) {
                                    rs.data.Result[i].QtyPhysical = null;
                                    rs.data.Result[i].DifferentCount = 1;
                                    rs.data.Result[i].QtyDifferent = 0;
                                    rs.data.Result[i].AmountDifferent = 0;
                                    rs.data.Result[i].Recount = 0;
                                    rs.data.Result[i].LocationTypeID = model.LocationTypeID;
                                    if (rs.data.Result[i].LocationTypeID == 1) {
                                        rs.data.Result[i].QtySystem = rs.data.Result[i].QtyOnHand  - rs.data.Result[i].QtyBlocked;
                                    } else {
                                        rs.data.Result[i].QtySystem = rs.data.Result[i].QtyOnHand;
                                    }
                                    console.log("rs.data.Result[0].QtySystem", rs.data.Result[i].QtySystem)
                                    console.log("getStockOpnamePart asasas = ", rs.data.Result);
                                }

                                var binTag = 0;
                                rs.data.Result.forEach(function(material) {
                                    binTag += 1;
                                    material.BinTag = '000' + binTag;
                                    material.BinTag = material.BinTag.substr(material.BinTag.length - 4);
                                });
                                $scope.grid_stock_opname_new.data = rs.data.Result;
                                $scope.mData.detail = $scope.grid_stock_opname_new.data;
                                console.log("All ", $scope.grid_stock_opname_new.data[0]);
                            }


                        }

                    },
                    function(err) {
                        console.log("err=>", err);
                    });
            }

        }

        $scope.getStockOpnamePart = function(data) {
            // $scope.hideSaveButton = true; di comment karena permintaan bug_ticket 253-SIT
            StockOpname.getStockOpnamePart(data).then(function(rs) {
                console.log("getStockOpnamePart = ", rs.data.Result);
                $scope.itemDone = rs.data.Result.length;
                if (rs.data.Result.length > 0) {
                    _.map(rs.data.Result, function(e) {
                        if (data.LocationTypeID == 1) {
                            e.QtySystem = e.QtyOnHand  - e.QtyBlocked;
                        } else {
                            e.QtySystem = e.QtyOnHand;
                        }
                        e.QtyPhysical = 0;
                        e.DifferentCount = 1;
                        e.QtyDifferent = 0;
                        e.AmountDifferent = 0;
                        e.Recount = 0;
                    });

                    var binTag = 0;
                    rs.data.Result.forEach(function(material) {
                        binTag += 1;
                        material.BinTag = '000' + binTag;
                        material.BinTag = material.BinTag.substr(material.BinTag.length - 4);
                    });
                    console.log("getStockOpnamePart asasas = ", rs.data.Result);
                    $scope.grid_stock_opname_new.data = rs.data.Result;
                    $scope.mData.detail = $scope.grid_stock_opname_new.data;
                    console.log("All ", $scope.grid_stock_opname_new.data[0]);
                }
            });
        }

        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }
            
        $scope.onValidateSave = function(model) {
            model.detail = $scope.grid_stock_opname_new.data;
            if (!model.StockOpnameId) {
                console.log("Save Create => ", model);
                if (model.detail) {
                    $scope.doSaveStockOpname(model);
                } else {
                    bsNotify.show({
                        title: "StockOpname",
                        content: "Data tidak disimpan, periksa kembali",
                        type: 'danger'
                    });
                }

                // return true;
            } else {
                console.log("Save Edit => ", model);

                //Sebenarnya Ini Salah.
                // $scope.show_modalCancel = true;

                //Harusnya pake ini.
                // model.CreatedDate = $scope.localeDate(model.CreatedDate);
                StockOpname.update(model).then(function(resu) {
                    console.log("resu Edit => ", resu);
                    for (var i = 0; i < $scope.mData.length; i++) {
                        $scope.mData.detail[i].StockOpnameStatusId = $scope.mData.StockOpnameStatusId;
                        // $scope.mData.detail[i].CreatedDate = $scope.localeDate($scope.mData.detail[i].CreatedDate);
                    }
                    InputHasil.updateItem($scope.mData.detail).then(function(res) {
                        $scope.formApi.setMode('grid');
                        $scope.getData();
                    });

                });

                // $scope.doUpdateforCancel(model);
            }
        }

        $scope.onCancelDocument = function(mData) {

            //var recountCondition = (mData.StockOpnameStatusId == 2 && mData.detail[0].Recount > 0) ||(mData.QtySystem == mData.QtyPhysical || mData.StockOpnameStatusId > 2) ? true : false;
            var recountCondition = (mData.StockOpnameStatusId >= 2) ? true : false;

            if (recountCondition) { // berarti udah ga open lagi StockOpnameStatusId nya atau recountnya sudah bertambah ( karena kalau freeze masih bisa di batal kalau recountnya masih 0)
                bsNotify.show({
                    title: "Gagal Batal",
                    content: "Stock Opname sudah dalam progress",
                    type: 'danger'
                });
            } else
                $scope.show_modalCancel = true;
        }

        $scope.goBack = function() {
            $scope.formApi.setMode('grid');
        }

        // added by sss on 2017-12-19
        $scope.approveFreeze = function() {
            ngDialog.openConfirm({
                template: '\
                      <p>Are you sure you want to freeze this stock opname?</p>\
                      <div class="ngdialog-buttons">\
                          <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                          <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="Freeze();closeThisDialog(0)">Yes</button>\
                      </div>',
                plain: true,
                // controller: 'PartsStockOpnameController',
                scope: $scope
            });
        }
        $scope.trial = function() {
                console.log("$scope.mData trial : ", $scope.mData);
            }
            //

        $scope.Freeze = function() {
            $scope.mData.StockOpnameStatusId = 2;
            // $scope.mData.CreatedDate = $scope.localeDate($scope.mData.CreatedDate);
            StockOpname.update($scope.mData).then(function(rs) {
                    for (var i = 0; i < $scope.mData.detail.length; i++) {
                        $scope.mData.detail[i].StockOpnameStatusId = $scope.mData.StockOpnameStatusId;
                        // $scope.mData.detail[i].CreatedDate = $scope.localeDate($scope.mData.detail[i].CreatedDate);
                    }
                    InputHasil.updateItem($scope.mData.detail).then(function(res) {
                        $scope.resetForm();
                        bsNotify.show({
                            title: "Stock Opname",
                            content: "Freeze berhasil. Status menjadi In Progress",
                            type: 'success'
                        });
                    });
                },
                function(err) {
                    console.log("err=>", err);
                });
        }

        $scope.exportHasil = function() {

            //yap
            //operasi untuk menampilkan filter data yang hanya di munculkan di grid ---------------

            var fileName = "Stock Opname NO "+$scope.mData.StockOpnameNo;

            console.log('mData',$scope.mData);
            var grid_displayed_column_field = [];
            for (var i = 0; i < $scope.grid_stock_opname_item.columnDefs.length; i++) {
                if ($scope.grid_stock_opname_item.columnDefs[i].field)
                console.log('$scope.grid_stock_opname_item.columnDefs[i].field',$scope.grid_stock_opname_item.columnDefs[i].field);
                    grid_displayed_column_field.push($scope.grid_stock_opname_item.columnDefs[i].field);
            }

            var dataDisplay = [];
            var NoBinTag = 0;
            for (var i in $scope.grid_stock_opname_item.data) {
              NoBinTag=NoBinTag+1;
              console.log('$scope.grid_stock_opname_item.data[i]',$scope.grid_stock_opname_item.data[i]);
                var obj = {};
                $scope.grid_stock_opname_item.data[i].NoBinTag = NoBinTag;
                $scope.grid_stock_opname_item.data[i].NoStockOpname =$scope.mData.StockOpnameNo;
                $scope.grid_stock_opname_item.data[i].WarehouseName =$scope.mData.WarehouseName;
                for (var j = 0; j < grid_displayed_column_field.length; j++)
                    obj[grid_displayed_column_field[j]] = $scope.grid_stock_opname_item.data[i][grid_displayed_column_field[j]] == null ? '':$scope.grid_stock_opname_item.data[i][grid_displayed_column_field[j]];

                dataDisplay.push(obj);
            }
            //selesai operasi untuk menampilkan filter data yang hanya di munculkan di grid ---------------
            //yap

            // console.log($scope.grid_stock_opname_item.data);
            //console.log('data export',dataDisplay,$scope.grid_stock_opname_item.columnDefs);
            // $scope.JSONToCSVConvertor($scope.grid_stock_opname_item.data, "Convert", "Stock Opname");
            XLSXInterface.writeToXLSX(dataDisplay, fileName);
            //$scope.JSONToCSVConvertor(dataDisplay, "Convert", "Stock Opname");
        }

        $scope.JSONToCSVConvertor = function(JSONData, ReportTitle, ShowLabel) {
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';

            CSV += ReportTitle + '\r\n\n';

            if (ShowLabel) {
                var row = "";

                for (var index in arrData[0]) {

                    row += index + ',';
                }

                row = row.slice(0, -1);

                CSV += row + '\r\n';
            }


            for (var i = 0; i < arrData.length; i++) {
                var row = "";


                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);


                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }


            var fileName = $scope.mData.StockOpnameNo;
            // fileName += ReportTitle.replace(/ /g, "_");


            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            var link = document.createElement("a");
            link.href = uri;

            link.style = "visibility:hidden";
            link.download = "Stock Opname NO "+ fileName + ".csv";

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        // Cetakan
        $scope.cetakBinTag = function(StockOpnameId) {
            console.log('StockOpnameId =', StockOpnameId);
            //console.log('OutletId =', $scope.user.OrgId);
            var data = $scope.mData;
            $scope.printBinTag = 'as/PrintBinTag/' + StockOpnameId;
            $scope.cetakan($scope.printBinTag);
        };
        $scope.cetakBinTagList = function(StockOpnameId) {
            console.log('StockOpnameId =', StockOpnameId);
            //console.log('OutletId =', $scope.user.OrgId);
            var data = $scope.mData;
            $scope.printBinTagList = 'as/PrintBinTagList/' + StockOpnameId;
            $scope.cetakan($scope.printBinTagList);
        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };


        $scope.resetForm = function() {
            $scope.formApi.setMode('grid');
            $scope.mData = {};
            $scope.grid_stock_opname_new.data = {};
            $scope.grid_stock_opname_item.data = {};
            $scope.generateMaterial = false;
            $scope.getData();
        }

        $scope.resetForm2 = function() {    //without getdata
            $scope.formApi.setMode('grid');
            $scope.mData = {};
            $scope.grid_stock_opname_new.data = {};
            $scope.grid_stock_opname_item.data = {};
            $scope.generateMaterial = false;
        }

        $scope.doSaveStockOpname = function(model) {
            console.log("doSaveStockOpname", model);
            StockOpname.getWarehouseById(model.WarehouseId).then(function(res) {
                model.OutletId = res.data.OutletId;
                console.log("sebelum sevae", model);
                console.log("detail", $scope.grid_stock_opname_new.data);
                console.log("cek ini apa", $scope.grid_stock_opname_new.data[0].StockOpnameStatusId);
                var isFreeze = false;               
                for(var i in $scope.grid_stock_opname_new.data)
                {
                    console.log("cek ini apa", $scope.grid_stock_opname_new.data[i].StockOpnameStatusId);
                    if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId == 2 ){
                        console.log("mamsukdini?");
                        isFreeze = true;
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "error",
                            content: "Part Sedang Dalam Proses Freeze"
                          });
                    } 
                    else if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId == 1){
                        console.log("mamsukdini?");
                        isFreeze = true;
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "error",
                            content: "Part Sedang Dalam Proses Freeze"
                          });
                    }
                    // if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId != null || $scope.grid_stock_opname_new.data[i].StockOpnameStatusId != undefined){                    
                    //         if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId[0].StockOpnameStatusId != null || $scope.grid_stock_opname_new.data[i].StockOpnameStatusId[0].StockOpnameStatusId != undefined ){
                    //             if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId[0].StockOpnameStatusId == 2){
                    //                 console.log("mamsukdini?c");
                    //                 isFreeze = true;
                    //                 bsNotify.show({
                    //                     size: 'big',
                    //                     type: 'danger',
                    //                     title: "error",
                    //                     content: "Part Sedang Dalam Proses Freeze"
                    //                   });
                    //             }
                    //             else if ($scope.grid_stock_opname_new.data[i].StockOpnameStatusId[0].StockOpnameStatusId == 1){
                    //                 console.log("mamsukdini?a");
                    //                 isFreeze = true;
                    //                 bsNotify.show({
                    //                     size: 'big',
                    //                     type: 'danger',
                    //                     title: "error",
                    //                     content: "Part Sedang Dalam Proses Freeze"
                    //                   });
                    //             }
                    //     }               
                              
                    // }          
                    else{
                        isFreeze = false;
                        console.log("frezer?");
                    }
                }   
                if (isFreeze == false) {
                StockOpname.create(model).then(function(rs) {

                        console.log("ini detail stock",$scope.grid_stock_opname_new.data);
                            StockOpname.createItem($scope.grid_stock_opname_new.data).then(function(result) {
                                console.log("ini detail stock",$scope.grid_stock_opname_new.data);
                                if (Object.keys($scope.filter).length == 0){
                                    $scope.resetForm2();
                                } else {
                                    $scope.resetForm();
                                }
                                    bsNotify.show({
                                        title: "Stock Opname",
                                        content: "Data berhasil disimpan",
                                        type: 'success'
                                    });
                                },
                                function(err1) {
                                    console.log("err=>", err1);
                                });


                        for(var i = 0; i < $scope.grid_stock_opname_new.data.length; i++)
                        {
                        $scope.grid_stock_opname_new.data[i].StockOpnameId = rs.data;
                        $scope.grid_stock_opname_new.data[i].StockOpnameStatusId = 1;
                        if ($scope.grid_stock_opname_new.data[i].LocationTypeID == 1) {
                            $scope.grid_stock_opname_new.data[i].QtySystem = $scope.grid_stock_opname_new.data[i].QtyOnHand  - $scope.grid_stock_opname_new.data[i].QtyBlocked;
                          } else {
                            $scope.grid_stock_opname_new.data[i].QtySystem = $scope.grid_stock_opname_new.data[i].QtyOnHand;
                          }
                        }

                        var detail = $scope.grid_stock_opname_new.data;
                        // _.map(detail, function(e) {
                        //     e.StockOpnameId = rs.data;
                        //     e.StockOpnameStatusId = 1;

                            // detail[0].StockOpnameId = rs.data;
                            // detail[0].StockOpnameStatusId = 1;
                        //   if (detail[0].LocationTypeID == 1) {
                        //     detail[0].QtySystem = detail[0].QtyOnHand  - detail[0].QtyBlocked;
                        //   } else {
                        //     detail[0].QtySystem = detail[0].QtyOnHand;
                        //   }
                        // });

                        $scope.formApi.setMode('grid');
                        $scope.getData();
                        console.log("saveee", rs);
                    },
                    function(err) {
                        console.log("err=>", err);
                    });
                }      
                else{

                }          
            });
        }


        $scope.confirmationforCancel = function(model) {


            // Tambah validasi mandatory untuk alasan pembatalan
           if ((model.CancelReasonId == null) || (model.CancelReasonId == undefined) || (model.CancelReasonId == "") ){
             bsNotify.show({
               size: 'big',
               type: 'danger',
               title: "error",
               content: "Alasan Batal Belum Diisi"
             });
           } else {
              $scope.show_modalCancel=false;
            bsAlert.alert({
                title: "Konfirmasi",
                text: "Anda yakin akan membatalkan Stock Opname ini ?",
                type: "question",
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'Kembali',

            }, function() {
                model.StockOpnameStatusId = 5;
                console.log("konfirmasi", model);
                // model.CreatedDate = $scope.localeDate(model.CreatedDate);
                StockOpname.update(model).then(function(rs) {
                    for (var i = 0; i < $scope.mData.detail.length; i++) {
                        $scope.mData.detail[i].StockOpnameStatusId = $scope.mData.StockOpnameStatusId;
                        // $scope.mData.detail[i].CreatedDate = $scope.localeDate($scope.mData.detail[i].CreatedDate);
                    }
                    InputHasil.updateItem($scope.mData.detail).then(function(res) {
                        $scope.resetForm();
                        },
                        function(err) {
                            console.log("err=>", err);
                        });
                    });  

            }, function(dismiss) {

            });
          }
          };
            // console.log("$scope.StockOpnameData",$scope.StockOpnameData);
        // };

        $scope.GenerateDocNum = function() {

            PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SP ').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                            var DocNo = res.data;

                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "Stock Opname",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                $scope.mData.StockOpnameNo = DocNo[0];
                                // $scope.mData.DocDate = DocNo[1];
                                $scope.mData.DocDate = setDate(DocNo[1]);
                                console.log("Generating DocNo & DocDate ->");
                                console.log($scope.mData.StockOpnameNo);
                                console.log($scope.mData.DocDate);
                            }


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {

                }
            );
            // [END] Get Current User Warehouse
        }

        function setDate(dt) {
            var d = "";
            dt = new Date(dt);
            d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
            return d;
        }


        $scope.generateBinTagNo = function(row) {
            console.log(row);
        }



        //----------------------------------
        // Grid Setup
        //----------------------------------

        $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
        '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.StockOpnameStatusId > 1 || (row.entity.StockOpnameStatusId == 1 && grid.appScope.$parent.user.RoleId != 1134 && grid.appScope.$parent.user.RoleId != 1167)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '</div>';
  
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Id',
                    field: 'StockOpnameId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'Tipe Material',
                    field: 'MaterialTypeIdDesc',
                    width: '10%',
                    // cellFilter: 'filterMaterialType'
                },
                {
                    name: 'No Stock Opname',
                    field: 'StockOpnameNo',
                    width: '15%'
                },
                {
                    name: 'Tanggal Stock Opname',
                    field: 'DocDate',
                    cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '10%'
                },
                {
                    name: 'Lokasi Gudang',
                    field: 'WarehouseName',
                    width: '12%'
                },
                {
                    displayName: 'SCC',
                    name: 'SCC',
                    field: 'SCCName',
                    width: '10%'
                },
                {
                    displayName: 'ICC',
                    name: 'ICC',
                    field: 'ICCName',
                    width: '10%'
                },
                {
                    name: 'Tipe Lokasi Stock',
                    field: 'TipeLocation',
                    width: '15%'
                },
                {
                    name: 'Status',
                    field: 'StockOpnameStatusIdDesc',
                    width: '8%',
                },
                {
                    name: 'Action',
                    width: '8%',
                    pinnedRight: true,
                    cellTemplate: $scope.gridActionButtonTemplate
                }
            ]
        };

        $scope.grid_stock_opname_item = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            // selectedItems: console.log($scope.mySelections),
            enableSelectAll: false,
            columnDefs: [
                { name: 'No Bin Tag', field: 'BinTag', enableCellEdit: false, width: '7%' },
                { name: 'No Material', field: 'NoMaterial', enableCellEdit: false, width: '9%' },
                { name: 'Nama Material', field: 'NamaMaterial', enableCellEdit: false, width: '15%' },
                { name: 'ICC', field: 'ICCName', enableCellEdit: false, width: '8%' },
                { name: 'No StockOpname', field: 'NoStockOpname', enableCellEdit: false, visible:false },
                { name: 'Warehouse Name', field: 'WarehouseName', enableCellEdit: false, visible:false },
                { name: 'Lokasi Rak', field: 'ShelfName', enableCellEdit: false, width: '8%' },
                { name: 'Qty Sistem', field: 'QtySystem', enableCellEdit: false, width: '7%' },
                { name: 'Qty Fisik', field: 'QtyPhysical', enableCellEdit: false, width: '7%' },
                { name: 'Satuan', field: 'UomName', enableCellEdit: false, width: '6%' },
                { name: 'Different Count', field: 'DifferentCount', type: 'boolean', width: '8%', cellTemplate: '<center><input type="checkbox" ng-model="row.entity.DifferentCount" ng-true-value="1" ng-false-value="0" disabled/></center>', enableCellEdit: false },
                { name: 'Qty Selisih', field: 'QtyDifferent', enableCellEdit: false, width: '8%' },
                { name: 'Selisih Nilai', field: 'AmountDifferent', enableCellEdit: false, width: '8%' },
                { name: 'ReCount', field: 'Recount', enableCellEdit: false, width: '7%' },
                { name: 'Standard Cost', field: 'StandardCost', enableCellEdit: false, visible:false},
                { name: 'FranchiseId', field: 'FranchiseId', enableCellEdit: false, visible:false},
                { name: 'FanchiseName', field: 'FranchiseName', enableCellEdit: false, visible:false},
                { name: 'AmountBeforeStockOpname', field: 'AmountBeforeStockOpname', enableCellEdit: false, visible:false},
                { name: 'AmountAfterStockOpname', field: 'AmountAfterStockOpname', enableCellEdit: false, visible:false},
            ]
        };

        $scope.grid_stock_opname_new = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            // selectedItems: console.log($scope.mySelections),
            enableSelectAll: false,
            columnDefs: [
                { name: 'No Bin Tag', field: 'BinTag', enableCellEdit: false },
                { name: 'No Material', field: 'PartsCode', enableCellEdit: false },
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false },
                { displayName: 'ICC', name: 'ICC', field: 'ICCName', enableCellEdit: false },
                { name: 'Lokasi Rak', field: 'ShelfName', enableCellEdit: false },
                { name: 'Qty Sistem', field: 'QtySystem', enableCellEdit: false },
                { name: 'Qty Fisik', field: 'QtyPhysical', enableCellEdit: false },
                { name: 'Satuan', field: 'UomName', enableCellEdit: false },
                { name: 'Different Count', cellTemplate: '<center><input type="checkbox"></input></center>', enableCellEdit: false },
                { name: 'Qty Selisih', field: 'QtyDifferent', enableCellEdit: false },
                { name: 'Selisih Nilai', field: 'AmountDifferent', enableCellEdit: false },
                { displayName: 'ReCount', name: 'ReCount', field: 'Recount', enableCellEdit: false },
            ],
        };

        $scope.StatusName = function(StockOpnameStatusId) {
            var statusName;
            switch (StockOpnameStatusId) {
                case 1:
                    statusName = "Open";
                    break;
                case 2:
                    statusName = "In Progress";
                    break;
                case 3:
                    statusName = "Request Approval";
                    break;
                case 4:
                    statusName = "Completed";
                    break;
                case 5:
                    statusName = "Canceled";
                    break;
            }
            return statusName;
        }




        $scope.onBeforeNew = function() {
                //show gateway page
                // $scope.isOverlayForm = true;

                // $scope.formode = 1;
                console.warn("mode=>", $scope.formmode);
                // $scope.newUserMode=true;
                // $scope.editMode=false;
                //$scope.mode='new';
            }
            // $scope.onBeforeEdit = function(){
            //     //$scope.formode = 2;
            //     //$scope.newUserMode=false;
            //     //console.log("mode=>",$scope.formmode);
            //     //$scope.editMode=true;

        //     console.log("[$scope] onBeforeEditMode");
        //     var editnum = '3'

        //     //pemilihan form edit
        //     console.log("Data dari onBeforeEditMode => ");
        //     $scope.isEditState = true;
        //     console.log("[EDIT] ");

        //     $scope.crudState = editnum + $scope.formShowHandler();

        //     console.log("[EDIT] " + $scope.crudState);

        // }
        // $scope.onBeforeDelete = function(){
        //     $scope.formode = 3;
        //     console.log("mode=>",$scope.formmode);
        //     //$scope.mode='del';
        // }


        // $scope.onShowDetail = function(){
        //     //$scope.formode = 3;
        //     //console.log("mode=>",$scope.formmode);
        //     //$scope.mode='del';

        //     console.log("[$scope] onShowDetail");
        //     var shownum = '2';

        //     console.log("[SHOW]");

        //     //TRAP onBeforeEditMode
        //     if ($scope.isEditState) {
        //         console.log("--->[Active] onBeforeEditMode");
        //       }
        //     else
        //     {
        //       //---------------------
        //       //Preparation View Mode
        //       //---------------------
        //       console.log("--->[Active] onShowDetail");
        //       console.log("Data dari onShowDetail => ");

        //       $scope.crudState = shownum + $scope.formShowHandler();

        //       console.log("[VIEW] " + $scope.crudState);
        //       $scope.isEditState = false;

        //     }
        // }

        //------------------------------------------------------------
        // $scope.formmode = { mode: '' };
        // $scope.doGeneratePass = function() {
        //     $scope.mUser.password = Math.random().toString(36).slice(-8);
        // }

        //----------------------------------
        // Claim Gateway Entry Form
        //----------------------------------
        // $scope.isOverlayForm = false;
        // $scope.isEditState = false;

        //----------------------------------
        // CRUD state C = 1, R = 2, U = 3, D = 4
        // CRUD state + Form Number = 111
        //----------------------------------
        // $scope.crudState = '';

        // $scope.onSearchMaterial = function() {
        //     console.log("[$scope] onOverlayMode");
        //     $scope.isOverlayForm = false;
        //     var newnum = '1';

        //     $scope.crudState = newnum + $scope.formShowHandler();
        //     console.log("[OVERLAY] is " + $scope.isOverlayForm);
        //     console.log("[OVERLAY] " + $scope.crudState);

        // }


        // $scope.onOverlayMode = function(kodeClaim) {
        //     console.log("[$scope] onOverlayMode");
        //     console.log(kodeClaim);
        //     $scope.mData.selectedRPP = kodeClaim;
        //     $scope.isOverlayForm = false;
        //     var newnum = '1';

        //     $scope.crudState = newnum + $scope.formShowHandler(kodeClaim);
        //     console.log("[OVERLAY] is " + $scope.isOverlayForm);
        //     console.log("[OVERLAY] " + $scope.crudState);

        // }


        // // fungsi semi-generik untuk handle halaman tampil
        // $scope.formShowHandler = function() {
        //     $scope.formState = '';
        //     $scope.formState = '00';

        //     console.log($scope.formState);
        //     return $scope.formState;
        // }

        // $scope.onShowFilter = function() {
        //     ngDialog.openConfirm({
        //         template: '<div ng-include=\"\'app/parts/stock_opname/stock_opname_m/stockOpnameFilter.html\'\"></div>',
        //         //templateUrl: '/app/parts/pomaintenance/alasan.html',
        //         plain: true,
        //         controller: 'StockOpnameController'
        //     });
        // }

        // // ======== tombol  simpan =========
        // $scope.simpan = function() {
        //     ngDialog.openConfirm({
        //         template: '<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-10"><p><i>RPP ini membutuhkan Approval.</i></p></div></div><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">RPP</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">D1/20160717001</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item RPP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div><button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Batal</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">Kirim Permohonan</button> </div></div></div></div></form></div></div>',
        //         //templateUrl: '/app/parts/pomaintenance/alasan.html',
        //         plain: true,
        //         controller: 'StockOpnameController',
        //     });
        // };
        // // ======== end tombol  simpan =========

        // // ======== tombol batal - Claim- alasan =========
        // $scope.batalPC = function() {
        //     ngDialog.openConfirm({
        //         template: '<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Pembatalan</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <div class="row"><div class="col-md-8"><label class="control-label requiredField" style="color:black;">Alasan<span class="asteriskField">*</span></label> <select class="form-control" style="widht: 100px><option value="Kesalahan Administrasi">Kesalahan Administrasi</option><option value="Kesalahan Pilih Transaksi">Kesalahan Pilih Transaksi</option><option value="Lainnya">Lainnya</option></select><br></div></div><div class="row"><div class="col-md-12"><label class="control-label" style="color:black;">Notes</label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div></div></div> <div class="form-group"><div class="row"><div class="col-md-12"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="okBatalPC()" style="color:white; background-color:#D53337">OK</button> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> </div></div></div></form></div></div>',
        //         plain: true,
        //         controller: 'StockOpnameController',
        //     });
        // };

        // // dialog konfirmasi
        // $scope.okBatalPC = function() {
        //     ngDialog.openConfirm({
        //         template: '\
        //              <div align="center" class="ngdialog-buttons">\
        //              <p><b>Konfirmasi</b></p>\
        //              <p>Anda yakin akan membatalkan Stock Opname ini?</p>\
        //              <div class="ngdialog-buttons" align="center">\
        //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
        //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()">Ya</button>\
        //              </div>\
        //              </div>',
        //         plain: true,
        //         controller: 'StockOpnameController',
        //     });
        // };

        // // event button 'Filter'


        // // ======== end tombol batal - Claim =========

        // // Button Simpan StockOpname Baru
        // $scope.SimpanStockOpname = function(mData) {
        //     StockOpname.create(mData).then(function(res) {
        //             bsNotify.show({
        //                 title: "StockOpname",
        //                 content: "Data berhasil disimpan",
        //                 type: 'success'
        //             });
        //         },
        //         function(err) {
        //             console.log("err=>", err);
        //         });
        // }


        // //----------------------------------
        // // Grid Setup
        // //----------------------------------

        // $scope.grid = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [{
        //             name: 'Id',
        //             field: 'StockOpnameId',
        //             width: '7%',
        //             visible: false
        //         },
        //         {
        //             name: 'Tipe Material',
        //             field: 'tipe_material',
        //             width: '10%'
        //         },
        //         {
        //             name: 'No Stock Opname',
        //             field: 'StockOpnameNo',
        //             width: '10%'
        //         },
        //         {
        //             name: 'Tanggal Stock Opname',
        //             field: 'DocDate',
        //             width: '15%'
        //         },
        //         {
        //             name: 'Lokasi Gudang',
        //             field: 'WarehouseName',
        //             width: '15%'
        //         },
        //         // {
        //         //     name: 'Lokasi Gudang',
        //         //     width: '15%',
        //         //     cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.LokasiGudang(row.entity.LocationId)}}</div>',
        //         // },
        //         {
        //             displayName: 'SCC',
        //             name: 'SCC',
        //             field: 'SCCName',
        //             width: '15%'
        //         },
        //         // {
        //         //     displayName: 'SCC',
        //         //     name: 'SCC',
        //         //     width: '15%',
        //         //     cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.SCCName(row.entity.SCCId)}}</div>',
        //         // },
        //         {
        //             displayName: 'ICC',
        //             name: 'ICC',
        //             field: 'ICCName',
        //             width: '15%'
        //         },
        //         // {
        //         //     displayName: 'ICC',
        //         //     name: 'ICC',
        //         //     width: '15%',
        //         //     cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ICCName(row.entity.ICCId)}}</div>',
        //         // },
        //         {
        //             name: 'Status',
        //             field: 'StockOpnameStatusId',
        //             width: '10%'
        //         },
        //         // {
        //         //     name: 'Status',
        //         //     width: '10%',
        //         //     cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.StatusName(row.entity.StockOpnameStatusId)}}</div>',
        //         // }
        //     ]
        // };

        //----------------------------------
        // GRID FUNCTION HANDLER
        //----------------------------------
        // konfirmasi hapus
        // $scope.showMe = function() {
        //     confirm('Apakah Anda yakin menghapus data ini?');
        // };

        // fungsi LocationId to LocationName
        // $scope.LokasiGudang = function(LocationId) {
        //     var LocationName;
        //     switch (LocationId) {
        //         case 1:
        //             LocationName = "Lokasi Gudang Parts";
        //             break;
        //         case 2:
        //             LocationName = "Lokasi Gudang Parts 2";
        //             break;
        //     }
        //     return LocationName;

        // }

        // // fungsi SCCId to SCCName
        // $scope.SCCName = function(SCCId) {
        //     var SccName;
        //     switch (SCCId) {
        //         case 10:
        //             SccName = "Overhaul Item";
        //             break;
        //         case 11:
        //             SccName = "Body Item";
        //             break;
        //         case 12:
        //             SccName = "Accesories Item";
        //             break;
        //         case 13:
        //             SccName = "CBU NON TAM";
        //             break;
        //         case 14:
        //             SccName = "Discontinue Item";
        //             break;
        //     }
        //     return SccName;
        // }

        // // fungsi ICCId to ICCName
        // $scope.ICCName = function(ICCId) {
        //     var iccName;
        //     switch (ICCId) {
        //         case 3:
        //             iccName = "A - Very Fast Moving";
        //             break;
        //         case 4:
        //             iccName = "B - Fast Moving";
        //             break;
        //         case 5:
        //             iccName = "C - Medium Moving";
        //             break;
        //         case 6:
        //             iccName = "D - Slow Moving";
        //             break;
        //         case 7:
        //             iccName = "E - Very Slow Moving";
        //             break;
        //     }
        //     return iccName;
        // }

        // fungsi StatusId to StatusName
        // $scope.StatusName = function(StockOpnameStatusId) {
        //     var statusName;
        //     switch (StockOpnameStatusId) {
        //         case 1:
        //             statusName = "Open";
        //             break;
        //         case 2:
        //             statusName = "In Progress";
        //             break;
        //         case 3:
        //             statusName = "Request Approval";
        //             break;
        //         case 4:
        //             statusName = "Completed";
        //             break;
        //         case 5:
        //             statusName = "Canceled";
        //             break;
        //     }
        //     return statusName;
        // }



        //----------------------------------
        // Dummy grid_stock_opname_new
        //----------------------------------
        // $scope.grid_stock_opname_new = {
        //     enableSorting: true,
        //     enableRowSelection: false,
        //     multiSelect: false,
        //     selectedItems: console.log($scope.mySelections),
        //     enableSelectAll: false,
        //     columnDefs: [
        //         { name: 'No Bin Tag', field: 'no_bin_tag' },
        //         { name: 'No Material', field: 'PartsCode' },
        //         { name: 'Nama Material', field: 'PartsName' },
        //         { displayName: 'ICC', name: 'ICC', field: 'ICCName' },
        //         { name: 'Lokasi Rak', field: 'lokasi_rak' },
        //         { name: 'Qty Sistem', field: 'QtySystem' },
        //         { name: 'Qty Fisik', field: 'QtyPhysical' },
        //         { name: 'Satuan', field: 'satuan' },
        //         { name: 'Different Count', cellTemplate: '<center><input type="checkbox"></input></center>' },
        //         { name: 'Qty Selisih', field: 'QtyDifferent' },
        //         { name: 'Selisih Nilai', field: 'AmountDifferent' },
        //         { displayName: 'ReCount', name: 'ReCount', field: 'Recount' },
        //     ],
        //     // data: [{
        //     //         "no_bin_tag": "001",
        //     //         "no_material": "35677-33996",
        //     //         "nama_material": "EXPANDER",
        //     //         "icc": "A",
        //     //         "lokasi_rak": "A01",
        //     //         "qty_sistem": "3",
        //     //         "qty_fisik": "3",
        //     //         "satuan": "Pieces",
        //     //         "qty_selisih": "-1",
        //     //         "selisih_nilai": "0",
        //     //         "recount": "0",
        //     //     },
        //     //     {
        //     //         "no_bin_tag": "002",
        //     //         "no_material": "35677-33996",
        //     //         "nama_material": "EXPANDER",
        //     //         "icc": "A",
        //     //         "lokasi_rak": "A01",
        //     //         "qty_sistem": "3",
        //     //         "qty_fisik": "3",
        //     //         "satuan": "Pieces",
        //     //         "qty_selisih": "-1",
        //     //         "selisih_nilai": "0",
        //     //         "recount": "0",
        //     //     }
        //     // ]
        // };

        //----------------------------------
        // Dummy grid_parts_claim
        //----------------------------------
        // $scope.grid_rpp_lihat = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     selectedItems: console.log($scope.mySelections),
        //     enableSelectAll: true,
        //     columnDefs: [
        //         { name: 'No Material', field: 'no_material' },
        //         { name: 'Nama Material', field: 'nama_material' },
        //         { name: 'Qty Received', field: 'qty_received' },
        //         { name: 'Qty RPP', field: 'qty_rpp' },
        //         { name: 'Satuan', field: 'satuan' },
        //         { name: 'TAM Judgement', field: 'tam_judgement' },
        //         { name: 'Qty TAM Judgement', field: 'qty_tam_judgement' },

        //     ],
        //     data: [{
        //             "no_material": "35677-33996",
        //             "nama_material": "EXPANDER",
        //             "qty_received": "3",
        //             "qty_rpp": "3",
        //             "satuan": "Pieces",
        //             "tam_judgement": "B",
        //             "qty_tam_judgement": "3"
        //         },
        //         {
        //             "no_material": "35677-64438",
        //             "nama_material": "SHOCK BREAKER",
        //             "qty_received": "3",
        //             "tam_judgement": "B",
        //             "qty_tam_judgement": "3"
        //         }
        //     ]
        // };

    })
    .filter('filterMaterialType', function() {
        var xtipe = {
            '1': 'Parts',
            '2': 'Bahan'
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xtipe[input];
            }
        };
    });
