angular.module('app')
  .factory('PartsCategory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(classTypeId) {
        // return $http.get('/api/as/PartsClasses?ClassTypeId='+classTypeId);
        return $http.get('/api/as/PartsClasses/GetPartsClassesFromType?ClassTypeId='+classTypeId);
      },
      getCategoryType: function() {
        return $http.get('/api/as/PartsClassTypes');
      },
      create: function(obj) {
        obj.PartsClassId = 0;
        obj.ParentId = 0;
        return $http.post('/api/as/PartsClasses', [obj]);
      },
      update: function(obj){
        obj.ParentId = 0;
        return $http.put('/api/as/PartsClasses', [obj]);
      },
      delete: function(arrId) {
        return $http.delete('/api/as/PartsClasses', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },
      
      typeAHead: function(desc,classTypeId) {
        return $http.get('/api/as/PartsClasses/GetParentTypeAhead?description=' + desc + '&ClassTypeId='+classTypeId);
      },
      
      createCustom: function(obj){
        return $http.post('/api/as/PartsClasses/addParentChildPartClass', [obj]);
      },

      checkParent: function(desc, classTypeId){
        return $http.get('/api/as/PartsClasses/checkAvaibility?description=' + desc + '&classTypeId=' + classTypeId);
      },

      updateCustom: function(obj){
        return $http.put('/api/as/PartsClasses/updateParentChildPartClass', [obj]);
      }


    }
  });