angular.module('app')
    .controller('PartsCategoryController', function($scope, $http, CurrentUser, PartsCategory,translator2, $timeout, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPartsCategory = {}; //Model
    $scope.xPartsCategory = {};
    $scope.xPartsCategory.selected=[];
    $scope.classTypeData = [];
    $scope.filter = {ClassTypeId:null};

    $scope.enbElement = false;
    $scope.disEl = false;
    $scope.tmpDataGrid = [];
    $scope.veNamaKategori = 1;
    $scope.CreateOREdit = 0;
    $scope.DescParent={};
    $scope.DescParentEdit={};
    $scope.selectedtes = {};
    $scope.hasilcek;

    //----------------------------------
    // Get Data
    //----------------------------------
    PartsCategory.getCategoryType().then(
        function(res){
        for (var k in res.data.Result) {
          if (res.data.Result[k].ClassTypeId != 1 && res.data.Result[k].ClassTypeId != 4) {
            $scope.classTypeData.push(res.data.Result[k]);
          }
        }
            // $scope.classTypeData = res.data.Result;
            console.log('res.data.Result',res.data.Result);
            $scope.loading=false;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    $scope.getData = function() {
        if($scope.filter.ClassTypeId)
            PartsCategory.getData($scope.filter.ClassTypeId).then(
                function(res){
                    if (res.data.Result.length > 0) {
                        // for tree === 
                        // $scope.grid.data = translator2.arraySortforBSFormTree(res.data.Result, 'PartsClassId', "ParentId");
                        //      console.log($scope.grid.data);
                        // $scope.tmpDataGrid = angular.copy($scope.grid.data);
                        // =============
                        $scope.grid.data = res.data.Result;
                    }
                    // $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
    }

    // $scope.selDesc = '';
    $scope.onShowDetail = function(xrow, mode){
        // var sel = _.find($scope.classTypeData,function(o){
        //     return o.ClassTypeId == xrow.ClassTypeId;
        // });
        // $scope.selDesc = (sel==null||sel==undefined) ? '' : sel.Description;
        $scope.mPartsCategory = {};
        if (mode=='edit') {
            // $scope.mPartsCategory.ClassTypeId = 1;
            $scope.mPartsCategory = angular.copy(xrow);
            $scope.DescParentEdit={};
            // === commented 18 mei 2021
            $scope.CreateOREdit = 2;

            // console.log('edit mode..');
            // if(xrow.ParentId > 0){
            //     //berarti edit child
            //     var tes = $scope.tmpDataGrid.filter(function(el) {
            //         return el.PartsClassId == xrow.ParentId;
            //     });
            //     xrow.DescParent = tes[0].Description;
            //     $scope.mPartsCategory.DescParent = xrow.DescParent;
            //     $scope.mPartsCategory.Description = xrow.Description;
            //     $scope.veNamaKategori = 1;
            //     $scope.DescParentEdit.parentId = xrow.ParentId;
            //     $scope.DescParentEdit.childId = xrow.PartsClassId;
            //     $scope.DescParentEdit.ParentDesc = angular.copy($scope.mPartsCategory.DescParent);
            // } else {
            //     //berarti edit parent
            //     $scope.mPartsCategory.DescParent = xrow.Description;
            //     $scope.mPartsCategory.Description = '';
            //     $scope.veNamaKategori = 2;
            //     $scope.DescParentEdit.parentId = 0;
            //     $scope.DescParentEdit.PartsClassId = xrow.PartsClassId;
            //     console.log($scope.DescParentEdit);
            // }
            $scope.enbElement = false;
            $scope.disEl = true;

        } else if (mode == 'view') {
            // $scope.mPartsCategory.ClassTypeId =1;
            $scope.mPartsCategory.ClassTypeId =xrow.ClassTypeId;
            // === commented 18 mei 2021
            // $scope.veNamaKategori = 1;
            // if(xrow.ParentId > 0){
            //     var tes = $scope.tmpDataGrid.filter(function(el) {
            //         return el.PartsClassId == xrow.ParentId;
            //     });
            //     console.log("tesss",tes);
            //     xrow.DescParent = tes[0].Description;
            //     $scope.mPartsCategory.DescParent = xrow.DescParent;
            //     $scope.mPartsCategory.Description = xrow.Description;
            //     $scope.veNamaKategori = 1;
            // } else {
            //     $scope.mPartsCategory.DescParent = xrow.Description;
            //     $scope.mPartsCategory.Description = '';
            //     $scope.veNamaKategori = 2;
            // }
            $scope.enbElement = false;
            $scope.disEl = true;
            // prepareData();
        } else {
            $scope.disEl = false;
        }
        console.log('====>', $scope.mPartsCategory, xrow);
    };


    $scope.onBeforeNew = function(model, mode){
        $scope.veNamaKategori = 1;
        $scope.DescParent={};
        $scope.CreateOREdit = 1;
        $scope.disEl = false;
        // $scope.DescParent.Id = null;
        // $scope.DescParent.Parent = null;
        // $scope.DescParent.Child = null;
    }


    $scope.noResults = true;
    $scope.onNoResult = function() {
        console.log("on No Result");
        $scope.DescParent={};
    };

    $scope.onGotResult = function() {
        console.log("onGotResult=>");
    };

    $scope.getParent = function(key) {
        var ress = PartsCategory.typeAHead(key, $scope.filter.ClassTypeId).then(function(res) {
        console.log('res',res.data);

            return res.data;
        });
        console.log("ress", ress);
        return ress
    }

    $scope.onSelectParent = function($item, $model, $label) {
        console.log("onSelectParent=>", $item);
        // console.log("onSelectParent=>", $model);
        // console.log("onSelectParent=>", $label);
        $scope.DescParent = $item;
    };

    $scope.doCustomSave = function(mdl, mode){
        var arrayObj = {};
        PartsCategory.checkParent($scope.mPartsCategory.Description,$scope.mPartsCategory.ClassTypeId).then(function(res){
            if (res.data) {
                bsAlert.warning("Data gagal disimpan karena nama yang digunakan sama", "silahkan cek kembali");
            } else {
                arrayObj.parentId = 0;
                arrayObj.parentName = mdl.Description;
                arrayObj.childId = null;
                arrayObj.childName = null;
                arrayObj.ClassTypeId = mdl.ClassTypeId;
                arrayObj.Description = mdl.Description;
                console.log('edit parent only', arrayObj,mdl);
                if ($scope.CreateOREdit == 1) {
                    PartsCategory.create(arrayObj).then(function(res){
                        console.log(res);
                        $scope.getData();
                    });
                } else if ($scope.CreateOREdit == 2) {
                    arrayObj.PartsClassId = mdl.PartsClassId
                    arrayObj.parentId = 0;
                    arrayObj.parentName = mdl.Description;
                    arrayObj.childId = null;
                    arrayObj.childName = null;
                    arrayObj.classTypeId = mdl.ClassTypeId;
                    console.log('edit parent only', arrayObj);
                    PartsCategory.update(arrayObj).then(function(res){
                        console.log(res);
                        $scope.getData();
                    });
                }
            }
        });
        
    }

    // commented 18 mei 2021
    // $scope.doCustomSave = function(mdl,mode){

    //     var arrayObj = {};
    //     // console.log(mode);

    //     //if create data baru
    //     if ($scope.CreateOREdit == 1) {
    //         console.log('parent', $scope.DescParent);
    //         console.log('tes',$scope.mPartsCategory.DescParent,$scope.mPartsCategory);
    //         if ($scope.DescParent.Description == null ||$scope.DescParent.Description == '' || $scope.DescParent.Description == undefined){
    //         // if ($scope.DescParent){
    //             arrayObj.id = 0;
    //             arrayObj.parent = $scope.mPartsCategory.DescParent;
    //             arrayObj.child = $scope.mPartsCategory.Description;
    //             arrayObj.classTypeId = $scope.mPartsCategory.ClassTypeId;
    //             console.log('obj new parent', arrayObj);
    //         } else {
    //             arrayObj.id = $scope.DescParent.PartsClassId;
    //             arrayObj.parent = $scope.DescParent.Description;
    //             arrayObj.child = $scope.mPartsCategory.Description;
    //             arrayObj.classTypeId = $scope.mPartsCategory.ClassTypeId;
    //             console.log('obj existing parent', arrayObj);
    //         }
    //         PartsCategory.createCustom(arrayObj).then(function(res){
    //             console.log(res);
    //             $scope.getData();
    //         });


    //         //else update data
    //     } else if ($scope.CreateOREdit == 2) {
    //         console.log('Description parentedit', $scope.DescParentEdit);
    //         console.log('Description parent', $scope.DescParent);
    //         if ($scope.DescParentEdit.parentId > 0){
    //         //edit parent dan anak

    //         if ($scope.mPartsCategory.DescParent != $scope.DescParentEdit.ParentDesc){
    //             //pindah parent
    //             console.log('pindah');
    //             if (($scope.DescParent.PartsClassId == null) ||($scope.DescParent.PartsClassId == undefined)){
    //                 arrayObj.parentId = 0;
    //             } else {
    //                 arrayObj.parentId = $scope.DescParent.PartsClassId;
    //             }
    //         } else {
    //             //tidak pindah parent
    //             console.log('tidak pindah');
    //             arrayObj.parentId = $scope.DescParentEdit.parentId;
    //         }
    //             arrayObj.parentName = $scope.mPartsCategory.DescParent;
    //             arrayObj.childId = $scope.DescParentEdit.childId;
    //             arrayObj.childName = $scope.mPartsCategory.Description;
    //             arrayObj.classTypeId = 1;
    //             console.log('edit parent child', arrayObj);
    //             PartsCategory.updateCustom(arrayObj).then(function(res){
    //                 console.log(res);
    //                 $scope.getData();
    //             });
    //         } else {
    //         //edit parent aja
    //             //jalanin service cek
    //             var DescParentCopy = angular.copy($scope.mPartsCategory.DescParent);
    //             PartsCategory.checkParent($scope.mPartsCategory.DescParent,1).then(function(res){
    //                 $scope.hasilcek = res.data;
    //                 if ($scope.hasilcek) {
    //                     bsAlert.warning("Data gagal diupdate karena nama yang digunakan sama", "silahkan cek kembali");
    //                 } else {
    //                     arrayObj.parentId = $scope.DescParentEdit.PartsClassId;
    //                     arrayObj.parentName = DescParentCopy;
    //                     arrayObj.childId = null;
    //                     arrayObj.childName = null;
    //                     arrayObj.classTypeId = 1;
    //                     console.log('edit parent only', arrayObj);
    //                     console.log('masa sih',$scope.DescParentEdit);
    //                     PartsCategory.updateCustom(arrayObj).then(function(res){
    //                         console.log(res);
    //                         $scope.getData();
    //                     });
    //                 }
    //             });

    //         }

    //     }

    // }



    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'PartsClassId', field:'PartsClassId', visible:false },
            { name:'ParentId', field:'PartsClassId', visible:false },
            { name:'Nama Kategori', field: 'Description' },
        ]
    };

});
