angular.module('app')
    .controller('PartsInquiryController', function($scope, $http, CurrentUser, PartsInquiry, ngDialog, $timeout, bsNotify, bsAlert) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            //$scope.gridData=[];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.gridHideActionColumn = true;

        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.xData = {};
        $scope.xData.selected = [];
        $scope.filterMaterial = [];

        //$scope.xRole.selected=219;
        //----------------------------------
        // Get Data
        //----------------------------------
        // <<<<<<< HEAD
        //var gridData = [];
        $scope.alertResponse = function(item) {
            var res = item.ResponseMessage;
            res = res.substring(res.lastIndexOf('#') + 1);
            console.log('item', item);
            console.log('res', res);
            bsAlert.alert({
                    title: "Informasi",
                    text: res,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    // $scope.formApi.setMode('grid');
                    // $scope.goBack();
                },
                function() {

                }
            )
        }

        $scope.alertResponseTPOS = function(item) {
          //  var res = item.ResponseMessage;
            //res = res.substring(res.lastIndexOf('#') + 1);
          //  console.log('item', item);
          //  console.log('res', res);
            bsAlert.alert({
                    title: "Response TPOS",
                    text: 'No. Material '+item.partNumber +' Tidak Ditemukan di TPOS',
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    // $scope.formApi.setMode('grid');
                    // $scope.goBack();
                },
                function() {

                }
            )
        }

        $scope.getData = function(filterMaterial) {
            console.log("filterMaterial 0=>", angular.copy(filterMaterial));
            var newArr = [];
            _.map(filterMaterial, function(a) {
                // console.log('aa', a);
                console.log('aa.length', a.length);
                if (typeof a !== "undefined") {
                    if (a.length != 0) {
                        console.log("a 1=>", a);
                        var text = a.replace(/-/g, "");
                        newArr.push(text)
                    } else {
                        console.log('kalau kosong ke sini boiii', a.length);
                    }

                }
            });
            console.log("newArr 1=>", newArr);

            // console.warn(filterMaterial.length, $scope.user);
            $scope.grid.data = [];
            // for (var i = 0; i < filterMaterial.length; i++) {
            //     console.warn(filterMaterial[i]);

            PartsInquiry.getData($scope.user.OrgCode, newArr).then(function(rs) {
                    var gridData = rs.data;
                    // console.warn("Iterasi " + i + " ");
                    // console.warn('gridData', gridData);
                    console.log('gridData',gridData);


                    if (gridData.ResponseCode == 100) {
                        $scope.alertResponse(gridData);
                    }

                    // if ((typeof gridData[0] === undefined || gridData[0] == null)) {
                    //     bsNotify.show({
                    //         title: "Parts Inquiry",
                    //         content: "Material tidak ditemukan",
                    //         // content: "Material " + filterMaterial[i] + "tidak ditemukan",
                    //         type: 'danger'
                    //     });
                    // }
                    if (gridData[0].partName == ''){
                      console.log('masuk sini');
                      $scope.alertResponseTPOS(gridData[0])

                    } else {
                        // $scope.collection.push(gridData[0]);
                        // var n = $scope.grid.data.length;
                        // $scope.grid.data[n] = gridData[0];
                        $scope.grid.data = gridData;
                    }
                },
                function(err) {
                    console.log("err=>", err);
                });
            // }
            // $scope.data = [
            //     {
            //       "noPart": "09905-00012",
            //       "namaPart": "EXPANDER",
            //       "cdNew": "",
            //       "pnoNew": "",
            //       "cdOld": "",
            //       "pnoOld": "",
            //       "onHand": "Available",
            //       "hargaRetail": 264000,
            //       "measurementH": 50,
            //       "measurementW": 580,
            //       "measurementL": 780,
            //       "specialCD": "",
            //       "stopPenjualan": ""
            //     },
            //     {
            //       "noPart": "09905-39015",
            //       "namaPart": "BELT TIMING",
            //       "cdNew": "",
            //       "pnoNew": "",
            //       "cdOld": "",
            //       "pnoOld": "",
            //       "onHand": "Available",
            //       "hargaRetail": 100000,
            //       "measurementH": 90,
            //       "measurementW": 125,
            //       "measurementL": 350,
            //       "specialCD": "",
            //       "stopPenjualan": ""
            //     },
            //     {
            //       "noPart": "52119-BZ330",
            //       "namaPart": "COVER FR BUMPER",
            //       "cdNew": "",
            //       "pnoNew": "",
            //       "cdOld": "",
            //       "pnoOld": "",
            //       "onHand": "Not Available",
            //       "hargaRetail": 3000000,
            //       "measurementH": 55,
            //       "measurementW": 55,
            //       "measurementL": 75,
            //       "specialCD": "",
            //       "stopPenjualan": ""
            //     }
            // ];
            // $scope.grid.data = $scope.data;

            // $scope.toData = $scope.data;
            // =======
            //     var gridData = [];
            //     $scope.getData = function(mData) {
            //         console.log("mData getData = ", $scope.mData);
            //         if(
            //             ($scope.mData.PC1 == null || $scope.mData.PC1 === undefined) &&
            //             ($scope.mData.PC2 == null || $scope.mData.PC2 === undefined) &&
            //             ($scope.mData.PC3 == null || $scope.mData.PC3 === undefined) &&
            //             ($scope.mData.PC4 == null || $scope.mData.PC4 === undefined) &&
            //             ($scope.mData.PC5 == null || $scope.mData.PC5 === undefined) &&
            //             ($scope.mData.PC6 == null || $scope.mData.PC6 === undefined) &&
            //             ($scope.mData.PC7 == null || $scope.mData.PC7 === undefined) &&
            //             ($scope.mData.PC8 == null || $scope.mData.PC8 === undefined) &&
            //             ($scope.mData.PC9 == null || $scope.mData.PC9 === undefined) &&
            //             ($scope.mData.PC10 == null || $scope.mData.PC10 === undefined)
            //         ){
            //             bsNotify.show(
            //                 {
            //                     title: "Mandatory",
            //                     content: "Filter belum diisi.",
            //                     type: 'danger'
            //                 }
            //               );
            //         } else {
            //             PartsInquiry.getData(mData).then(function(res) { //start getdata
            //                 var gridData = res.data;
            //                 console.log("<controller getData> GridData => ", gridData);
            //                 $scope.grid.data = gridData;
            //                 $scope.loading = false;
            //             },
            //             function(err) {
            //                 console.log("err=>", err);
            //             }
            //             ); // end getdata
            //         } // end else

            //       }// end getData all

            //     // Hapus Filter
            //     $scope.onDeleteFilter = function(){
            //         //Pengkondisian supaya seolaholah kosong
            //         $scope.mData.PC1 = null;
            //         $scope.mData.PC2 = null;
            //         $scope.mData.PC3 = null;
            //         $scope.mData.PC4 = null;
            //         $scope.mData.PC5 = null;
            //         $scope.mData.PC6 = null;
            //         $scope.mData.PC7 = null;
            //         $scope.mData.PC8 = null;
            //         $scope.mData.PC9 = null;
            //         $scope.mData.PC10 = null;
            //     }
            //     // $scope.getData = function() {
            //     //     $scope.data = [
            //     //         {
            //     //           "noPart": "09905-00012",
            //     //           "namaPart": "EXPANDER",
            //     //           "cdNew": "",
            //     //           "pnoNew": "",
            //     //           "cdOld": "",
            //     //           "pnoOld": "",
            //     //           "onHand": "Available",
            //     //           "hargaRetail": 264000,
            //     //           "measurementH": 50,
            //     //           "measurementW": 580,
            //     //           "measurementL": 780,
            //     //           "specialCD": "",
            //     //           "stopPenjualan": ""
            //     //         },
            //     //         {
            //     //           "noPart": "09905-39015",
            //     //           "namaPart": "BELT TIMING",
            //     //           "cdNew": "",
            //     //           "pnoNew": "",
            //     //           "cdOld": "",
            //     //           "pnoOld": "",
            //     //           "onHand": "Available",
            //     //           "hargaRetail": 100000,
            //     //           "measurementH": 90,
            //     //           "measurementW": 125,
            //     //           "measurementL": 350,
            //     //           "specialCD": "",
            //     //           "stopPenjualan": ""
            //     //         },
            //     //         {
            //     //           "noPart": "52119-BZ330",
            //     //           "namaPart": "COVER FR BUMPER",
            //     //           "cdNew": "",
            //     //           "pnoNew": "",
            //     //           "cdOld": "",
            //     //           "pnoOld": "",
            //     //           "onHand": "Not Available",
            //     //           "hargaRetail": 3000000,
            //     //           "measurementH": 55,
            //     //           "measurementW": 55,
            //     //           "measurementL": 75,
            //     //           "specialCD": "",
            //     //           "stopPenjualan": ""
            //     //         }
            //     //     ];
            //     //     $scope.grid.data = $scope.data;
            // >>>>>>> ca2edb3cbdc16a8605f21c17bab34fc29bc25002

            //     $scope.toData = $scope.data;

            //     //console.log("pomaintenance=>",res.data.Result);

            //     $scope.loading = false;
        };

        //========= mode form
        //$scope.formmode = '';
        $scope.onBeforeNewMode = function() {
            console.log("mode new=>", mode);
        }

        $scope.onBeforeEditMode = function(row, mode) {
            console.log("row edit=>", row);
            console.log("mode edit=>", mode);

        }

        $scope.onShowDetail = function(row, mode) {
            console.log("mode view=>", mode);
            console.log("row view=>", row);

        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 5,

            columnDefs: [
                // <<<<<<< HEAD
                { name: 'No. Part', displayName: 'No. Part', field: 'partNumber', width: '10%', headerCellClass: "middle" },
                { name: 'Nama Part', displayName: 'Nama. Part', field: 'partName', width: '12%', headerCellClass: "middle" },
                // =======
                //             { name:'No. Part', displayName: 'No. Part', field: 'PartsCode', width: '10%', headerCellClass: "middle"},
                //             { name:'Nama Part', displayName: 'Nama. Part', field: 'PartsName', width: '12%', headerCellClass: "middle"},
                // >>>>>>> ca2edb3cbdc16a8605f21c17bab34fc29bc25002
                {
                    name: 'CD (New)',
                    displayName: 'CD (New)',
                    field: 'subtitutionCDNew',
                    width: 80,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:404%'><table class='ui-grid-header-table'><tbody><tr><td colspan='4'>Substitusi</td></tr></tbody></table></div>CD (New)</div>"
                },
                {
                    name: 'PNO (New)',
                    displayName: 'PNO (New)',
                    field: 'subtitutionPNONew',
                    width: 80,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>PNO (New)</div>"
                },
                {
                    name: 'CD (Old)',
                    displayName: 'CD (Old)',
                    field: 'subtitutionCDOld',
                    width: 80,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>CD (Old)</div>"
                },
                {
                    name: 'PNO (Old)',
                    displayName: 'PNO (Old)',
                    field: 'subtitutionPNOOld',
                    width: 80,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>PNO (Old)</div>"
                },
                { name: 'On Hand', displayName: 'On Hand', field: 'onHand', width: '8%',

                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (row.entity.onHand != 0 ) {
                     row.entity.onHand = "Tersedia";
                      } else {
                     row.entity.onHand = "Tidak Tersedia";  
                      }
                  }

                },
                // <<<<<<< HEAD
                //             { name:'Harga Retail', displayName: 'Harga Retail', field: 'RetailPrice', width: '8%', headerCellClass: "middle"},
                // =======
                { name: 'Harga Retail', displayName: 'Harga Retail', field: 'retailPrice', width: '10%', headerCellClass: "middle" },
                // >>>>>>> ca2edb3cbdc16a8605f21c17bab34fc29bc25002

                {
                    name: 'Measurement H',
                    displayName: 'H',
                    field: 'measurementH',
                    //width: '4%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:301%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Measurement</td></tr></tbody></table></div>H</div>"
                },
                {
                    name: 'Measurement W',
                    displayName: 'W',
                    field: 'measurementW',
                    //width: '4%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>W</div>"
                },
                {
                    name: 'Measurement L',
                    displayName: 'L',
                    field: 'measurementL',
                    //width: '4%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>L</div>"
                },
                { name: 'Special CD', displayName: 'Special CD', field: 'specialCD', width: '8%', headerCellClass: "middle" },
                { name: 'Stop Penjualan', displayName: 'Stop Penjualan', field: 'stopSale', width: '10%', headerCellClass: "middle" }
            ]
        };


    });
