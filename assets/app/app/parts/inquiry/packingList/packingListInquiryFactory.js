angular.module('app')
  .factory('PartsPackingListInquiry', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(data) {
      	var res=$http.get('/api/as/TPOSInquiry/PackingListInquiry', {params: {
          ShippingNo : (data.ShippingNo==null?"n/a":data.ShippingNo),
          OutletId : (data.OutletId==null?"n/a":data.OutletId)
    
        } });
        return res;
      }
    }
  });
