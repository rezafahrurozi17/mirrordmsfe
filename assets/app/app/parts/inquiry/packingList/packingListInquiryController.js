angular.module('app')
    .controller('PartsPackingListInquiryController', function($scope, $http, CurrentUser, PartsPackingListInquiry, ngDialog, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.gridHideActionColumn = true;
    
    $scope.user = CurrentUser.user();
    console.log ("$scope.user",$scope.user);
    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected = [];
    //$scope.xRole.selected=219; 
    // Alert

    $scope.alertResponse = function(item) {
      var res = item.ResponseMessage;
      res = res.substring(res.lastIndexOf('#') + 1);
      console.log('item', item);
      console.log('res', res);
      bsAlert.alert({
              title: "Informasi",
              text: res,
              type: "warning",
              showCancelButton: false
          },
          function() {
              // $scope.formApi.setMode('grid');
              // $scope.goBack();
          },
          function() {

            }
        )
      }


    //---------------------------------- 
    // Get Data
    //----------------------------------
    //var gridData = [];
    $scope.getData = function(mData) {
      console.log('Mdata = ', mData);
      mData.OutletId = $scope.user.OrgId;
      if(mData.ShippingNo == null || mData.ShippingNo === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "No. Shipping harus diisi",
              type: 'danger' 
          }
        );
      } else {
        // $scope.data = [
        //     {
        //       "noPO": "PO/20160715001",
        //       "noVendorInvoice": "06727797",
        //       "noCase": "H10115",
        //       "noPart": "09905-00012",
        //       "namaPart": "EXPANDER",
        //       "qty": 1
        //     },
        //     {
        //       "noPO": "PO/20160715002",
        //       "noVendorInvoice": "06727797",
        //       "noCase": "H10115",
        //       "noPart": "48520-09530",
        //       "namaPart": "ABSORBER A/S FR LH",
        //       "qty": 2
        //     },
        //     {
        //       "noPO": "PO/20160715003",
        //       "noVendorInvoice": "06727797",
        //       "noCase": "H10115",
        //       "noPart": "88310-02650",
        //       "namaPart": "COMPRESSOR A/S",
        //       "qty": 1
        //     }
        // ];
        
        $scope.mData.OutletCode = $scope.user.OrgCode;

        $scope.grid.data = $scope.data; 

        $scope.toData = $scope.data;
        PartsPackingListInquiry.getData($scope.mData).then(function(rs){
          var gridData = rs.data;
          console.log(gridData);
          //$scope.grid.data = gridData;

          if (gridData.ResponseCode == 100) {
            $scope.alertResponse(gridData);
          }

          if ((typeof gridData[0] === undefined || gridData[0] == null)) {
              bsNotify.show({
                  title: "Parts Inquiry",
                  content: "Material tidak ditemukan",
                  // content: "Material " + filterMaterial[i] + "tidak ditemukan",
                  type: 'danger'
              });
          } else {
              // $scope.collection.push(gridData[0]);
              // var n = $scope.grid.data.length;
              // $scope.grid.data[n] = gridData[0];
              $scope.grid.data = gridData;
          }
      },
      function(err) {
          console.log("err=>", err);
      });
        // }
      console.warn("selesaaai ", $scope.mData);

        // try
        // {
          // PartsPackingListInquiry.getData($scope.mData).then(function(rs)){
          //   var gridData = rs.data;
          //   console.log(gridData);
          //   $scope.grid.data = gridData;}
          // BE Version, Success
          // PartsPackingListInquiry.getData().then(function(res) {
          //     var loginData = res.data;
          //     console.log("TPOS Data = ", loginData);

          //   },
          //   function(err) {
          //     console.log("err=>", err);
          //   }
          // );

          // FE Version is still buggy
          // console.log("CORS Request");
          // var url = 'http://10.85.40.21/TPOSWS/api/v1/GoodsReceipts';
          // var xhr = $scope.createCORSRequest('GET', url);
          // if (!xhr) {
          //   throw new Error('CORS not supported');
          // }

          // // Response handlers.
          // xhr.onload = function() {
          //   var text = xhr.responseText;
          //   var title = getTitle(text);
          //   alert('Response from CORS request to ' + url + ': ' + title);
          // };

          // xhr.onerror = function() {
          //   alert('Woops, there was an error making the request.');
          // };

          // xhr.send();
      
        // }
        // catch(err)
        // {
        //   console.log("Controller", err);
        // }

        // $scope.loading = false;
      } // end else
    }

    //========= mode form
    //$scope.formmode = '';
    $scope.onBeforeNewMode = function(){
        console.log("mode new=>", mode);
    }

    $scope.onBeforeEditMode = function(row, mode){
        console.log("row edit=>", row);
        console.log("mode edit=>", mode);
        
    }

    $scope.onShowDetail = function(row, mode){
        console.log("mode view=>", mode);
        console.log("row view=>", row);
       
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.ShippingNo = null;           
  }

    // CORS Request XHR
    $scope.createCORSRequest = function(method, url) {
      var xhr = new XMLHttpRequest();
      if ("withCredentials" in xhr) {

        // Check if the XMLHttpRequest object has a "withCredentials" property.
        // "withCredentials" only exists on XMLHTTPRequest2 objects.
        xhr.open(method, url, true);

      } else if (typeof XDomainRequest != "undefined") {

        // Otherwise, check if XDomainRequest.
        // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
        xhr = new XDomainRequest();
        xhr.open(method, url);

      } else {

        // Otherwise, CORS is not supported by the browser.
        xhr = null;

      }
      return xhr;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],  
        paginationPageSize: 5,

        // columnDefs: [
        //     { name:'No. PO', displayName: 'No. PO', field: 'noPO', width: '15%'},
        //     { name:'No. Vendor Invoice', displayName: 'No. Vendor Invoice', field: 'noVendorInvoice', width: '15%'},
        //     { name:'No. Case', displayName: 'No. Case', field: 'noCase', width: '15%'},
        //     { name:'noPart', displayName: 'No. Part', field: 'noPart', width: '15%'},
        //     { name:'Nama Part', displayName: 'Nama. Part', field: 'namaPart', width: '25%'},
        //     { name:'qty', displayName: 'QTY', field: 'qty', width: '15%'}
           
        // ]
        columnDefs: [
          { name:'No. PO', displayName: 'No. PO', field: 'PurchaseOrderNo', width: '15%'},
          { name:'No. Vendor Invoice', displayName: 'No. Vendor Invoice', field: 'VendorInvoiceNo', width: '15%'},
          { name:'No. Case', displayName: 'No. Case', field: 'CaseNo', width: '15%'},
          { name:'noPart', displayName: 'No. Part', field: 'PartsCode', width: '15%'},
          { name:'Nama Part', displayName: 'Nama. Part', field: 'PartsName', width: '25%'},
          { name:'qty', displayName: 'QTY', field: 'ShipmentQty', width: '15%'}
         
      ]
    };


});
