angular.module('app')
    .factory('PartsClaimInquiry', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(data) {

                // return $http.get('/api/as/StockReturnClaim/GetClaimInquiry', {
                //     params: {
                //         OutletCode: (data.OutletCode == null ? "n/a" : data.OutletCode),
                //         claimDate: (data.claimDate == null ? "n/a" : data.claimDate),
                //         claimType: (data.claimType == null ? "n/a" : data.claimType),
                //         inquiryType: (data.inquiryType == null ? "n/a" : data.inquiryType),
                //         documentDMSNo: (data.documentDMSNo == null ? "n/a" : data.documentDMSNo)
                //     }
                // });
                return $http.get('/api/as/TPOSInquiry/GetClaimInquiry', {
                    params: {
                        OutletCode: (data.OutletCode == null ? "n/a" : data.OutletCode),
                        claimDate: (data.claimDate == null ? "n/a" : data.claimDate),
                        claimType: (data.claimType == null ? "n/a" : data.claimType),
                        inquiryType: (data.inquiryType == null ? "n/a" : data.inquiryType),
                        documentDMSNo: (data.documentDMSNo == null ? "n/a" : data.documentDMSNo)
                    }
                });
            },
            getDatadetail: function(data) {
                return $http.get('/api/as/TPOSInquiry/GetClaimInquirydetail', {
                    params: {
                        claimType: (data.claimType == null ? "n/a" : data.claimType),
                        tamRefNo: (data.tamRefNo == null ? "n/a" : data.tamRefNo),
                        partNo: (data.partNo == null ? "n/a" : data.partNo)
                    }
                });
            },
            getDatadetailclaim: function(data) {
                return $http.get('/api/as/TPOSInquiry/GetClaimInquirydetailclaim', {
                    params: {
                        dmsRefNo: (data.dmsRefNo == null ? "n/a" : data.dmsRefNo),
                        tamRefNo: (data.tamRefNo == null ? "n/a" : data.tamRefNo)                       
                    }
                });
            }
        }
    });