angular.module('app')
    .controller('PartsClaimInquiryController', function($scope, $http, CurrentUser, PartsClaimInquiry, ngDialog, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up

    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected = [];
    //$scope.xRole.selected=219; 
    //---------------------------------- 
    // Get Data
    //----------------------------------
    //var gridData = [];
    $scope.getData = function(mData) {
        if($scope.mData.TipeClaim == 1){
            $scope.mData.claimType = "";
            $scope.mData.claimDate = "";
            $scope.mData.inquiryType = 0;
            $scope.mData.documentDMSNo = "";
            $scope.mData.claimType = "PC";
            var tmpItem =  $scope.mData.TanggalClaim;
            var finalDate
            var yyyy = tmpItem.getFullYear().toString();
            var mm = (tmpItem.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItem.getDate().toString();
            finalDate = yyyy  + (mm[1] ? mm : "0" + mm[0]) +  (dd[1] ? dd : "0" + dd[0]);
            $scope.mData.claimDate = finalDate;
            $scope.mData.inquiryType = $scope.mData.TipeInquiry;
            $scope.mData.documentDMSNo = $scope.mData.NoDokumen;
        }
        else if($scope.mData.TipeClaim == 2){
            $scope.mData.claimType = "";
            $scope.mData.claimDate = "";
            $scope.mData.inquiryType = 0;
            $scope.mData.documentDMSNo = "";
            $scope.mData.claimType = "RPP"; 
            var tmpItem =  $scope.mData.TanggalClaim;
            var finalDate
            var yyyy = tmpItem.getFullYear().toString();
            var mm = (tmpItem.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItem.getDate().toString();
            finalDate = yyyy +  (mm[1] ? mm : "0" + mm[0]) +  (dd[1] ? dd : "0" + dd[0]);
            $scope.mData.claimDate = finalDate;
            $scope.mData.inquiryType = $scope.mData.TipeInquiry;
            $scope.mData.documentDMSNo = $scope.mData.NoDokumen;
        }
        console.log('mData = ', mData);

        if((mData.TanggalClaim == null || mData.TanggalClaim === undefined) &&
           (mData.TipeClaim == null || mData.TipeClaim === undefined) &&
           (mData.TipeInquiry == null || mData.TipeInquiry === undefined) && 
           (mData.NoDokumen == null || mData.NoDokumen === undefined || mData.NoDokumen == "")){
            bsNotify.show(
                {
                    title: "Mandatory",
                    content: "Isi inputan filter terlebih dahulu",
                    size: "small",
                    type: 'danger'
                }
            );
        } else
        if((mData.TipeClaim == null && mData.TipeInquiry == null) || 
           (mData.TipeClaim === undefined && mData.TipeInquiry === undefined) &&
           (mData.NoDokumen != undefined || mData.NoDokumen !== undefined)){
          
            $scope.mData.OutletCode = $scope.user.OrgCode;
            
            PartsClaimInquiry.getData($scope.mData).then(function(rs){
                if(rs.data.length > 0){
                    var gridData = rs.data;
                    console.log("coba liat",gridData);
                    $scope.grid.data = gridData;
                }else{
                    bsNotify.show({
                    size: 'big',
                    type: 'warning',
                    title: "No Data Available",
                  
                });
                    $scope.grid.data =[];
                }
            },
            function(err) {
                console.log("err=>", err);
            });
              // }
    
            //console.log("pomaintenance=>",res.data.Result);    
            // $scope.loading = false;
        } else {
            if(mData.TanggalClaim == null || mData.TanggalClaim === undefined){
                if((mData.TipeClaim != null || mData.TipeClaim !== undefined) || (mData.TipeInquiry != null || mData.TipeInquiry !== undefined)){
                    bsNotify.show(
                        {
                            title: "Mandatory",
                            content: "Tanggal Claim mandatory jika ada filter Tipe Claim / Tipe Inquiry",
                            size: "small",
                            type: 'danger'
                        }
                    );
                }
            } else {                            
            // $scope.data = [
            //     {
            //     "id": 1,
            //     "tipeClaim": "RPP",
            //     "kodeClaim": "A - Cancel Booking",
            //     "dealerRequest": "C - TAM Credit",
            //     "tanggalClaim": "17/Juli/2016",
            //     "deliveryDate": "12/Juli/2016",
            //     "tanggalGR": "12/Juli/2016",
            //     "qtyClaim": 1,
            //     "hargaNetDealer": 8030000,
            //     "totalHargaNetDealer": 8030000,
            //     "claimAmount": 8030000,
            //     "dealerNote": "Mohon diterima",
            //     "supervisorJudgeCode": "Approve",
            //     "supervisorJudgeDate": "17/Juli/2016",
            //     "TAMJudgement": "B",
            //     "tanggalPenilaianTAM": "18/Juli/2016",
            //     "tipeCustomer": "",
            //     "tipeMaterial": "",
            //     "noDokumenTAM": "PC001",
            //     "noDokumenDMS": "PC/20160717001",
            //     "noPO": "PO/20160715001",
            //     "noPart": "09905-00012",
            //     "noVendorInvoice": "06727797",
            //     "namaPart": "EXPANDER",
            //     "tanggalPenilaianSPV": "21/Juli/2016",
            //     "TAMJudgement": "R3",
            //     "tanggalPenilaianTAM": "22/Juli/2016",
            //     "leadTime": 5
            //     },
            //     {
            //     "id": 2,
            //     "tipeClaim": "RPP",
            //     "kodeClaim": "A - Cancel Booking",
            //     "dealerRequest": "C - TAM Credit",
            //     "tanggalClaim": "17/Juli/2016", 
            //     "deliveryDate": "12/Juli/2016",
            //     "tanggalGR": "12/Juli/2016",
            //     "qtyClaim": 1,
            //     "hargaNetDealer": 8030000,
            //     "totalHargaNetDealer": 8030000,
            //     "claimAmount": 8030000,
            //     "dealerNote": "Mohon diterima",
            //     "supervisorJudgeCode": "Approve",
            //     "supervisorJudgeDate": "17/Juli/2016",
            //     "TAMJudgement": "B",
            //     "tanggalPenilaianTAM": "18/Juli/2016",
            //     "tipeCustomer": "",
            //     "tipeMaterial": "",
            //     "noDokumenTAM": "PC/11001",
            //     "noDokumenDMS": "PC/20160717002",
            //     "noPO": "PO/20160715002",
            //     "noPart": "48520-09530",
            //     "noVendorInvoice": "06727796",
            //     "namaPart": "ABSORBER A/S FR LH",
            //     "tanggalPenilaianSPV": "21/Juli/2016",
            //     "TAMJudgement": "R2",
            //     "tanggalPenilaianTAM": "22/Juli/2016",
            //     "leadTime": 5
            //     },
            //     {
            //     "id": 3,
            //     "tipeClaim": "RPP",
            //     "kodeClaim": "A - Cancel Booking",
            //     "dealerRequest": "C - TAM Credit",
            //     "tanggalClaim": "17/Juli/2016", 
            //     "deliveryDate": "12/Juli/2016",
            //     "tanggalGR": "12/Juli/2016",
            //     "qtyClaim": 1,
            //     "hargaNetDealer": 8030000,
            //     "totalHargaNetDealer": 8030000,
            //     "claimAmount": 8030000,
            //     "dealerNote": "Mohon diterima",
            //     "supervisorJudgeCode": "Approve",
            //     "supervisorJudgeDate": "17/Juli/2016",
            //     "TAMJudgement": "B",
            //     "tanggalPenilaianTAM": "18/Juli/2016",
            //     "tipeCustomer": "",
            //     "tipeMaterial": "",
            //     "noDokumenTAM": "PC-1",
            //     "noDokumenDMS": "PC/20160717003",
            //     "noPO": "PO/20160715002",
            //     "noPart": "48520-09530",
            //     "noVendorInvoice": "06727795",
            //     "namaPart": "COMPRESSOR A/S",
            //     "tanggalPenilaianSPV": "21/Juli/2016",
            //     "TAMJudgement": "B",
            //     "tanggalPenilaianTAM": "22/Juli/2016",
            //     "leadTime": 5
            //     }
            // ];
            $scope.mData.OutletCode = $scope.user.OrgCode;
            // $scope.grid.data = $scope.data; 

            // $scope.toData = $scope.data;

            PartsClaimInquiry.getData($scope.mData).then(function(rs){
                if(rs.data.length > 0){
                    var gridData = rs.data;
                    console.log(gridData);
                    $scope.grid.data = gridData;
                }else{
                    bsNotify.show({
                    size: 'big',
                    type: 'warning',
                    title: "No Data Available",
                    // content: error.join('<br>'),
                    // number: error.length
                });
                    $scope.grid.data = [];
                }
            },
            function(err) {
                console.log("err=>", err);
            });
              // }

            //console.log("pomaintenance=>",res.data.Result);

           // $scope.loading = false;
            } // end else
        } // end else all
    }

    //========= mode form
    //$scope.formmode = '';
    $scope.onBeforeNewMode = function(){
        console.log("mode new=>", mode);
    }
    $scope.docustomCancel = function(mode){
        console.log("mode punya new=>", mode);
        $scope.grid.data = [];
    }

    $scope.onBeforeEditMode = function(row, mode){
        console.log("row edit=>", row);
        console.log("mode edit=>", mode);
        
    }

    $scope.onShowDetail = function(row, mode){
        console.log("mode view=>", mode);
        console.log("row view=>", row);
        console.log("row view=>", $scope.mData.claimType);
        if(mode !== 'new'){    
        var dataclaim = {claimType : $scope.mData.claimType, tamRefNo : row.tamRefNo, partNo : row.partNo}
            PartsClaimInquiry.getDatadetail(dataclaim).then(function(res) { 
                console.log("row ini res", res);  
            $scope.mData = res.data[0];
            var dataclaimdet = {dmsRefNo : row.dmsRefNo, tamRefNo : row.tamRefNo}
            PartsClaimInquiry.getDatadetailclaim(dataclaimdet).then(function(res) {   
                console.log("row ini res s", res);
            $scope.mData.detail = res.data[0];
            $scope.mData.kodeClaim = $scope.mData.detail.kodeClaim;
            $scope.mData.dealerRequest = $scope.mData.detail.dealerRequest;
            $scope.mData.tipeCustomer = $scope.mData.detail.noDokumenTAM;
            $scope.mData.tipeMaterial = $scope.mData.detail.noPart;
            $scope.mData.TotalpriceDealerNet = $scope.mData.detail.qtyClaim;
            });
        });
        }
        console.log("$scope.mData", $scope.mData);
       
       
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
        //Pengkondisian supaya seolaholah kosong
        $scope.mData.TanggalClaim = null;
        $scope.mData.TipeClaim = null;
        $scope.mData.TipeInquiry = null;
        $scope.mData.NoDokumen = null;        
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],  
        paginationPageSize: 5,

        columnDefs: [
            { name:'id', field:'id', width:'7%', visible:false },
            { name:'tanggal Claim', displayName: 'Tgl Claim', field: 'claimDate', width: '9%'}, 
            { name:'no. Dokumen TAM', displayName: 'No. Dokumen TAM', field: 'tamRefNo', width: '9%'},
            { name:'no. Dokumen DMS', displayName: 'No. Dokumen DMS', field: 'dmsRefNo', width: '10%'},
            { name:'no. PO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: '10%'},
            { name:'no. Vendor Invoice', displayName: 'No. Vendor Invoice', field: 'vendorInvoice', width: '8%'},
            { name:'no. PO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: '10%'},
            { name:'no. Part', displayName: 'No. Part', field: 'partNo', width: '9%'},
            { name:'nama Part', displayName: 'Nama. Part', field: 'partName', width: '10%'},
            { name:'tanggal Penilaian SPV', displayName: 'Tgl Penilaian SPV', field: 'spvJudgementDate', width: '8%'},
            { name:'TAM Judgement', displayName: 'TAM Judgement', field: 'tamJudgement', width: '8%'},
            { name:'tanggal Penilaian TAM', displayName: 'Tgl Penilaian TAM', field: 'tamJudgementDate', width: '10%'},
            { name:'lead Time', displayName: 'Lead Time', field: 'leadTime', width: '5%'}
        
        ]
    };


});
