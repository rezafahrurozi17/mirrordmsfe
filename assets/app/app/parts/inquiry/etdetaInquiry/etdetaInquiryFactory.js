angular.module('app')
  .factory('PartsETDETAInquiry', function($http, CurrentUser,$filter) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(data) {
        console.log("<factory> data => ", data);  
        
        var TanggalPO = $filter('date')(data.TanggalPO, 'yyyyMMdd');
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

        var res=$http.get('/api/as/TPOSInquiry/ETDandETA', {params: {
          outletCode          : (data.OutletCode==null?"n/a":data.OutletCode),
          purchaseOrderDate   : (TanggalPO==null?"n/a":TanggalPO),
          orderType           : (data.TipeOrder==null?"n/a":data.TipeOrder),
          purchaseOrderNumber : (data.PurchaseOrderNo==null?"n/a":data.PurchaseOrderNo),
          partNumber          : (data.PartsCode==null?"n/a":data.PartsCode),
          etdDateFrom         : (data.startDate==null?"n/a":startDate),
          etdDateTo           : (data.endDate==null?"n/a":endDate)
    
        } });
        return res;
      }
    }
  });