angular.module('app')
    .controller('PartsETDETAInquiryController', function($scope, $http, CurrentUser, PartsETDETAInquiry, ngDialog, bsNotify, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.gridHideActionColumn = true;

    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected = [];
    //$scope.xRole.selected=219; 

    // alert

    $scope.alertResponse = function(item) {
        var res = item.ResponseMessage;
        res = res.substring(res.lastIndexOf('#') + 1);
        console.log('item', item);
        console.log('res', res);
        bsAlert.alert({
                title: "Informasi",
                text: res,
                type: "warning",
                showCancelButton: false
            },
            function() {
                // $scope.formApi.setMode('grid');
                // $scope.goBack();
            },
            function() {

            }
        )
    }


    //---------------------------------- 
    // Get Data
    //----------------------------------
    //var gridData = [];
    $scope.getData = function(mData) {
        console.log('mData = ', mData);
        $scope.xData = angular.copy($scope.mData);

        if(mData.pilihan == null || mData.pilihan === undefined){
            bsNotify.show(
                {
                    title: "Mandatory",
                    content: "Pilih kategori filter, kemudian isi data filter",
                    type: 'danger'
                }
                );
        } else {
            // $scope.data = [
            //     {
            //       "noPO": "0667492",
            //       "tanggalPO": "15/Juli/2016",
            //       "noPart": "09905-00012",
            //       "tanggalBO": "-",
            //       "qtyBO": 0,
            //       "etd": "16/Juli/2016 11.00",
            //       "eta": "16/Juli/2016 11.00",
            //       "qtyIssue": 1,
            //       "etd_s": "16/Juli/2016 07.00",
            //       "eta_s": "16/Juli/2016 09.00",
            //       "qtyIssue_S": 1,
            //       "etd_t": "-",
            //       "eta_t": "-",
            //       "qtyIssue_T": 0,
            //       "satuan": "Pieces",
            //       "alasan": "Perubahan dari Supplier"
            //     },
            //     {
            //       "noPO": "0667492",
            //       "tanggalPO": "15/Juli/2016",
            //       "noPart": "09905-00012",
            //       "tanggalBO": "-",
            //       "qtyBO": 1,
            //       "etd": "18/Juli/2016 08.00",
            //       "eta": "1",
            //       "qtyIssue": '18/Juli/2016 10.00',
            //       "etd_s": "18/Juli/2016 12.00",
            //       "eta_s": 1,
            //       "qtyIssue_S": '-',
            //       "etd_t": "-",
            //       "eta_t": "-",
            //       "qtyIssue_T": '-',
            //       "satuan": "Pieces",
            //       "alasan": ""
            //     },
            //     {
            //       "noPO": "0667492",
            //       "tanggalPO": "15/Juli/2016",
            //       "noPart": "09905-00012",
            //       "tanggalBO": "16/Juli/2016 17.00",
            //       "qtyBO": 1,
            //       "etd": "-",
            //       "eta": "-",
            //       "qtyIssue": 0,
            //       "etd_s": "-",
            //       "eta_s": "-",
            //       "qtyIssue_S": 0,
            //       "etd_t": "-",
            //       "eta_t": "-",
            //       "qtyIssue_T": 0,
            //       "satuan": "Pieces",
            //       "alasan": ""
            //     }
            // ];

            if((mData.TipeOrder == null || mData.TipeOrder == undefined) && mData.pilihan == "order"){
                bsNotify.show(
                    {
                        title: "Mandatory",
                        content: "Pilih tipe order terlebih dahulu",
                        type: 'warning'
                    }
                  );
            } else {

                if(mData.pilihan == "order") {
                    $scope.xData.PartsCode = null;        
                    $scope.xData.startDate = null;             
                    $scope.xData.endDate = null;             
                } else if(mData.pilihan == "part") {
                    $scope.xData.TipeOrder = 0;
                    $scope.xData.PurchasOrderNo = null;
                    $scope.xData.TanggalPO = null;             
                    $scope.xData.startDate = null;             
                    $scope.xData.endDate = null;             
                } else if(mData.pilihan == "etd") {
                    $scope.xData.TipeOrder = 0;
                    $scope.xData.PurchasOrderNo = null;
                    $scope.xData.PartsCode = null;        
                    $scope.xData.TanggalPO = null;             
                }

                console.log("mData 2 ==> ",$scope.mData);
                $scope.grid.data = $scope.data; 

                $scope.xData.OutletCode = $scope.user.OrgCode;
                $scope.toData = $scope.data;

                //console.log("pomaintenance=>",res.data.Result);

                $scope.toData = $scope.data;
                PartsETDETAInquiry.getData($scope.xData).then(function(rs){
                    var gridData = rs.data;
                    console.log(gridData);
                    //$scope.grid.data = gridData;
                    if (gridData.ResponseCode == 100) {
                    $scope.alertResponse(gridData);
                    }

                    if ((typeof gridData[0] === undefined || gridData[0] == null)) {
                        bsNotify.show({
                            title: "Parts Inquiry",
                            content: "Material tidak ditemukan",
                            // content: "Material " + filterMaterial[i] + "tidak ditemukan",
                            type: 'danger'
                        });
                    } else {
                        // $scope.collection.push(gridData[0]);
                        // var n = $scope.grid.data.length;
                        // $scope.grid.data[n] = gridData[0];
                        $scope.grid.data = gridData;
                    }
                },
                function(err) {
                    console.log("err=>", err);
                });

                console.warn("selesaaai ", $scope.mData);
                //$scope.loading = false;
            }
        } // end else

    }

    //========= mode form
    //$scope.formmode = '';
    $scope.onBeforeNewMode = function(){
        console.log("mode new=>", mode);
    }

    $scope.onBeforeEditMode = function(row, mode){
        console.log("row edit=>", row);
        console.log("mode edit=>", mode);
        
    }

    $scope.onShowDetail = function(row, mode){
        console.log("mode view=>", mode);
        console.log("row view=>", row);
       
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
        //Pengkondisian supaya seolaholah kosong
        $scope.mData.pilihan = null;
        $scope.mData.TipeOrder = null;
        $scope.mData.PurchasOrderNo = null;
        $scope.mData.PartsCode = null;        
        $scope.mData.TanggalPO = null; 
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],  
        paginationPageSize: 5,

        columnDefs: [
            { name:'no. PO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: '9%', headerCellClass: 'middle'},
            { name:'tanggal PO', displayName: 'Tanggal PO', field: 'purchaseOrderDate', width: '10%', headerCellClass: 'middle'},
            { name:'no. Part', displayName: 'No. Part', field: 'partNumber', width: '10%', headerCellClass: 'middle'},
            { 
                name:'tanggal BO', 
                displayName: 'Tanggal BO', 
                field: 'boDate', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:162%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>BO</td></tr></tbody></table></div>Tanggal BO</div>"
            },
            { 
                name:'qty BO', 
                displayName: 'Qty BO', 
                field: 'boQuantity', 
                width: '8%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty BO</div>"
            },
            { 
                name:'etd Original', 
                displayName: 'ETD', 
                field: 'originalETD', 
                width: '12%',
                cellFilter: 'date:\'yyyy-MM-dd\'',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Original</td></tr></tbody></table></div>ETD</div>"
            },
            { 
                name:'eta Original', 
                displayName: 'ETA', 
                field: 'eta', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
            },
            { 
                name:'qty Issue Original', 
                displayName: 'Qty Issue', 
                field: 'originalIssueQuantity', 
                width: '12%', 
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Issue</div>"
            },
            { 
                name:'etd Sebelumnya', 
                displayName: 'ETD (s)', 
                field: 'prevETD', 
                width: '12%',
                cellFilter: 'date:\'yyyy-MM-dd\'',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Sebelumnya</td></tr></tbody></table></div>ETD</div>"
            },
            { 
                name:'eta Sebelumnya', 
                displayName: 'ETA (s)', 
                field: 'eta_s', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
            },
            { 
                name:'qty Issue Sebelumnya', 
                displayName: 'Qty Issue (s)', 
                field: 'prevIssueQuantity', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Issue</div>"
            },
            { 
                name:'ETD Terakhir', 
                displayName: 'ETD (T)', 
                field: 'latestETD',
                width: '12%',
                cellFilter: 'date:\'yyyy-MM-dd\'',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Terakhir</td></tr></tbody></table></div>ETD</div>"    
            },
            { 
                name:'ETA Terakhir', 
                displayName: 'ETA (T)', 
                field: 'eta_t', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
            },
            { 
                name:'Qty Issue Terakhir', 
                displayName: 'Qty Issue (T)', 
                field: 'latestIssueQuantity', 
                width: '12%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Issue</div>"
            },
            { name:'satuan', displayName: 'Satuan', field: 'satuan', width: '5%', headerCellClass: 'middle'},
            { name:'alasan', displayName: 'Alasan', field: 'alasan', width: '15%', headerCellClass: 'middle'}
        ]
    };


});
