angular.module('app')
    .controller('PartsOSInquiryController', function($scope, $http, CurrentUser, PartsOSInquiry, ngDialog, bsNotify, $timeout, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------

    $scope.gridHideActionColumn = true;

    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected = [];

    $scope.strToDate = function(dt) {
        console.log("Tanggal dt==>",dt);
        var tmpDt = dt.substring(0,4) + "-" + dt.substring(4,6) + "-" + dt.substring(6,8);
        console.log("tmpDt==>>",tmpDt);
        return new Date(tmpDt);
    }

    //$scope.sm_show2 = true;

    //$scope.xRole.selected=219;
    //----------------------------------
    // Get Data
    //----------------------------------
    //var gridData = [];
    $scope.getData = function(mData) {
        console.log(mData);
        //console.warn('mData', mData);
        if((mData.TipeOrder == null || mData.TipeOrder == undefined)){
            // if(mData.tanggalPO == null || mData.tanggalPO === undefined){
                bsNotify.show(
                    {
                        title: "Mandatory",
                        content: "Tipe PO belum diisi",
                        type: 'danger'
                    }
                  );
          
        } else if((mData.PartsCode == null || mData.PartsCode === undefined) && (mData.PurchaseOrderNo == null || mData.PurchaseOrderNo === undefined)){
           
                bsNotify.show(
                    {
                        title: "Mandatory",
                        content: "NO PO atau No Part belum diisi",
                        type: 'danger'
                    }
                  );
           
        }else if((mData.TipeOrder == null || mData.TipeOrder === undefined)&&(mData.PartsCode == null || mData.PartsCode === undefined) &&(mData.PurchaseOrderNo == null || mData.PurchaseOrderNo === undefined)){
            // if(mData.tanggalPO == null || mData.tanggalPO === undefined){
                bsNotify.show(
                    {
                        title: "Mandatory",
                        content: "Data belum diisi ",
                        type: 'danger'
                    }
                  );
            // }
            // else
            // {
            //   bsNotify.show(
            //         {
            //             title: "Lewat sisnbi",
            //             content: "Tanggal PO belum diisi",
            //             type: 'danger'
            //         }
            //       );
            // }
        }else {
        // $scope.data = [
        //     {
        //         "number": 1,
        //         "purchaseOrderNumber": "00044E10",
        //         "purchaseOrderDate": "20170524",
        //         "tamReference": "0004400",
        //         "partNumber": "0888083608     ",
        //         "processNumberPart": "0888083608     ",
        //         "purchaseOrderQuantity": 1,
        //         "processQuantity": 0,
        //         "reject": 1,
        //         "boPendingQuantity": 0,
        //         "boQuantity": 0,
        //         "issueQuantity": 0,
        //         "packQuantity": 0,
        //         "invoiceQuantity": 0,
        //         "cancelQuantity": 0,
        //         "denialQuantity": 1
        //       },
        //       {
        //         "number": 2,
        //         "purchaseOrderNumber": "00044E10",
        //         "purchaseOrderDate": "20170524",
        //         "tamReference": "0004400",
        //         "partNumber": "0888083609     ",
        //         "processNumberPart": "0888083609     ",
        //         "purchaseOrderQuantity": 1,
        //         "processQuantity": 0,
        //         "reject": 1,
        //         "boPendingQuantity": 0,
        //         "boQuantity": 0,
        //         "issueQuantity": 0,
        //         "packQuantity": 0,
        //         "invoiceQuantity": 0,
        //         "cancelQuantity": 0,
        //         "denialQuantity": 1
        //       },
        //       {
        //         "number": 3,
        //         "purchaseOrderNumber": "00044E10",
        //         "purchaseOrderDate": "20170524",
        //         "tamReference": "0004400",
        //         "partNumber": "0888680705     ",
        //         "processNumberPart": "0888680705     ",
        //         "purchaseOrderQuantity": 1,
        //         "processQuantity": 0,
        //         "reject": 1,
        //         "boPendingQuantity": 0,
        //         "boQuantity": 0,
        //         "issueQuantity": 0,
        //         "packQuantity": 0,
        //         "invoiceQuantity": 0,
        //         "cancelQuantity": 0,
        //         "denialQuantity": 1
        //       }
        // ];
        // $scope.grid.data = $scope.data;

        // $scope.toData = $scope.data;

        // //console.log("pomaintenance=>",res.data.Result);

        // $scope.loading = false;

        //=======Start Get Data =======
        if ($scope.mData.PartsCode != null){
            if ($scope.mData.PartsCode.toString().includes('-')){
                $scope.mData.PartsCode = $scope.mData.PartsCode.replace("-", "");        
            } else {
                $scope.mData.PartsCode = $scope.mData.PartsCode;        
    
            }        
        } else {
            $scope.mData.PartsCode = $scope.mData.PartsCode;        

        }
       

          $scope.mData.OutletCode = $scope.user.OrgCode;

          console.warn($scope.mData);
          console.warn("stateStatus ", PartsOSInquiry.stateStatus);

          // if(PartsOSInquiry.stateStatus == 1)
          // {
          //   PartsOSInquiry.getDataPending($scope.mData).then(function(rs){
          //       var gridData = rs.data;
          //       console.log(gridData);
          //       $scope.gridPendingDetail.data = gridData;
          //   },
          //   function(err) {
          //     console.log("err=>", err);
          //   });
          // }
          // else
          // {

        PartsOSInquiry.getData($scope.mData).then(function(rs){
            var gridData = rs.data;
            console.log("lihat ini apa",gridData);
            // gridData[0].purchaseOrderDate = new Date(gridData[0].purchaseOrderDate);
            // gridData[0].purchaseOrderNo = "20201228";
            // gridData[0].purchaseOrderDate = new Date("20201228");
            console.log("tanggal==>>",new Date("2020-12-28"));
            console.log("tanggal==>>",$scope.strToDate("20201227"));
            for (var i in gridData) {
                gridData[i].purchaseOrderDate = $scope.strToDate(gridData[i].purchaseOrderDate);
            }
            $scope.grid.data = gridData;   
            console.log("lihat ini apa lagi ",$scope.grid.data.length);     
            if ($scope.grid.data.length == null || $scope.grid.data.length == undefined || $scope.grid.data.length == 0) {
                bsAlert.alert({
                    title: "Response From TPOS Application",
                    text: "Data Tidak Ditemukan",
                    type: "warning"
                }
            );
            } else {
                
            }
        },
        function(err) {
            console.log("err=>", err);
        });
          // }
        console.warn("selesaaai ", $scope.mData);

        } // end else
    }

    //========= mode form
    //$scope.formmode = '';
    $scope.onBeforeNewMode = function(){
        console.log("mode new=>", mode);
    }

    $scope.onBeforeEditMode = function(row, mode){
        console.log("row edit=>", row);
        console.log("mode edit=>", mode);

    }

    $scope.onShowDetail = function(row, mode){
        console.log("mode view=>", mode);
        console.log("row view=>", row);

    }

    $scope.open = function() {
        $scope.showModal = true;
    };
    $scope.ok = function() {
        $scope.showModal = false;
    };

    $scope.cancel = function() {
        $scope.showModal = false;
    };

    $scope.pendingDetail = function(){
      PartsOSInquiry.getDataPending($scope.mData).then(function(rs){
                var gridData = rs.data;
                console.log(gridData);
                $scope.gridPendingDetail.data = gridData;
            },
            function(err) {
              console.log("err=>", err);
            });
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
        plain: true,
        controller: 'PartsOSInquiryController',
       }).then(function (value) {
                    // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                    alert(gridData);
                    if (PartsGlobal.cancelledLookupGrid) return 1;
                }, function (value) {
                  // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                  console.log("No Data fetched ");
                  if (PartsGlobal.cancelledLookupGrid) return 1;
                  // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                });;
    };

    $scope.boDetail = function(){
        $scope.mData.OutletCode = $scope.user.OrgCode;
      PartsOSInquiry.getDataPendingBO($scope.mData).then(function(rs){
                var gridData = rs.data;
                console.log("Grid BO",gridData);
                $scope.gridBODetail.data = gridData;
                ngDialog.openConfirm ({
                    template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/boDetail.html\'\"></div>',
                    plain: true,
                    // controller: 'PartsOSInquiryController',
                    scope:$scope
                   });
                console.log("Grid BO Detail", $scope.gridBODetail.data);
            },
            function(err) {
              console.log("err=>", err);
            });
    };

     $scope.issueToShip = function(){
        PartsOSInquiry.GetOrderStatusIssueToShip($scope.mData).then(function(rs){
                var gridData = rs.data;
                console.log("Grid Issue",gridData);
                $scope.gridIssueToShip.data = gridData;
                console.log("Grid Issue To Ship",$scope.gridIssueToShip.data);
            },
            function(err) {
              console.log("err=>", err);
            });
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/issueToShip.html\'\"></div>',
        plain: true,
        controller: 'PartsOSInquiryController',
       });
    };

    $scope.POSSInfo = function(){
        PartsOSInquiry.GetOrderStatusPOSSInfo($scope.mData).then(function(rs){
                var gridData = rs.data;
                console.log("Grid Poss",gridData);
                $scope.gridPOSSInfo.data = gridData;
                console.log("Grid POSS Info",gridData);
            },
            function(err) {
              console.log("err=>", err);
            });
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/POSSInfo.html\'\"></div>',
        plain: true,
        controller: 'PartsOSInquiryController',
       });
    };

    $scope.hello = function(){
        alert('OK');
        //console.log("");
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
        //Pengkondisian supaya seolaholah kosong
        $scope.mData.tanggalPO = null;
        $scope.mData.TipeOrder = null;
        $scope.mData.PurchaseOrderNo = null;
        $scope.mData.PartsCode = null;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
            { name:'No. Urut', displayName: 'No.', field: 'number', width: 40, headerCellClass: "middle"},
            { name:'No. PO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: 120, headerCellClass: "middle"},
            { name:'Tanggal PO', displayName: 'Tanggal PO', field: 'purchaseOrderDate', cellFilter: 'date:\'yyyy-MM-dd\'',width: 90, headerCellClass: "middle"},
            { name:'TAM Ref', displayName: 'TAM Ref.', field: 'tamReference', width: 75, headerCellClass: "middle"},
            { name:'No. Part', displayName: 'No. Part', field: 'partNumber', width: 120, headerCellClass: "middle"},
            { name:'Process No. Part', displayName: 'Process No. Part', field: 'processNumberPart', width: 120, headerCellClass: "middle"},
            { name:'qty PO', displayName: 'Qty PO', field: 'purchaseOrderQuantity', width: 70, headerCellClass: "middle"},
            { name:'process', displayName: 'Process', field: 'processQuantity', width: 70, headerCellClass: "middle"},
            { name:'reject', displayName: 'Reject', field: 'reject', width: 70, headerCellClass: "middle"},
            {
                name:'qty Pending',
                displayName: 'Qty Pending',
                field: 'boPendingQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:197%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Pending BO</td></tr></tbody></table></div>Qty Pending</div>"
            },
            {
                name:'qty BO',
                displayName: 'Qty BO',
                field: 'boQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty BO</div>"
            },
            {
                name:'qty Issue',
                displayName: 'Qty Issue',
                field: 'issueQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:500%'><table class='ui-grid-header-table'><tbody><tr><td colspan='5'>Issue To Shipping</td></tr></tbody></table></div>Qty Issue</div>"
            },
            {
                name:'qty Pack',
                displayName: 'Qty Pack',
                field: 'packQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Pack</div>"
            },
            {
                name:'qty Invoice',
                displayName: 'Qty Invoice',
                field: 'invoiceQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Invoice</div>"
            },
            {
                name:'qty Cancel',
                displayName: 'Qty Cancel',
                field: 'cancelQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Cancel</div>"
            },
            {
                name:'qty Denial',
                displayName: 'Qty Denial',
                field: 'denialQuantity',
                width: 80,
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Denial</div>"
            }
        ]
    };

    $scope.gridPendingDetail = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [5, 10, 20, 30, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 5,

        columnDefs: [
            { name:'noUrut', displayName: 'No.', field: 'number', width: '5%', headerCellClass: "middle"},
            { name:'noPO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: '15%', headerCellClass: "middle"},
            { name:'tanggalPO', displayName: 'Tanggal PO', field: 'purchaseOrderDate', width: '15%', headerCellClass: "middle"},
            { name:'noPart', displayName: 'No. Parts', field: 'partNumber', width: '15%', headerCellClass: "middle"},
            { name:'processNoPart', displayName: 'Process No. Part', field: 'processNumberPart', width: '20%', headerCellClass: "middle"},
            {
                name:'tanggalAlokasi',
                displayName: 'Tanggal Alokasi',
                field: 'allocationDate',
                width: '15%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:197%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Pending BO</td></tr></tbody></table></div>Tanggal Alokasi</div>"
            },
            {
                name:'qtyPending',
                displayName: 'Qty Pending',
                field: 'boPendingQuantity',
                width: '15%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Pending</div>"
            }
        ]

        // data:
    };

    $scope.gridBODetail = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [5, 10, 20, 30, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 5,

        columnDefs: [
            { name:'noUrut', displayName: 'No.', field: 'number', width: '5%', headerCellClass: "middle" },
            { name:'noPO', displayName: 'No. PO', field: 'purchaseOrderNumber', width: '17%', headerCellClass: "middle" },
            { name:'tanggalPO', displayName: 'Tanggal PO', field: 'purchaseOrderDate', width: '15%', headerCellClass: "middle" },
            { name:'noPart', displayName: 'No. Parts', field: 'partNumber', width: '15%', headerCellClass: "middle" },
            { name:'processNoPart', displayName: 'Process No. Part', field: 'processNumberPart', width: '20%', headerCellClass: "middle" },
            {
                name:'tanggalBO',
                displayName: 'Tanggal BO',
                field: 'boDate',
                width: '15%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:197%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>BO Detail</td></tr></tbody></table></div>Tanggal BO</div>"
            },
            {
                name:'qtyBO',
                displayName: 'Qty BO',
                field: 'boQuantity',
                width: '13%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty BO</div>"
            }
        ]
        // data: [
        //     {
        //       "number": 1,
        //       "purchaseOrderNumber": "PO/20160717003",
        //       "purchaseOrderDate": "31/Mei/2016",
        //       "partNumber": "PZ000-12345-12",
        //       "processNumberPart": "PZ000-35764-01",
        //       "tanggalBO": "3/Juni/2016",
        //       "boQuantity": 1,
        //     },
        //     {
        //       "number": 2,
        //       "purchaseOrderNumber": "PO/20160717003",
        //       "purchaseOrderDate": "31/Mei/2016",
        //       "partNumber": "PZ000-12345-12",
        //       "processNumberPart": "PZ000-35764-01",
        //       "tanggalBO": "3/Juni/2016",
        //       "boQuantity": 1,
        //     }
        // ]
    };

    $scope.gridIssueToShip = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [5, 10, 20, 30, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 5,

        columnDefs: [
            { name:'noUrut', displayName: 'No.', field: 'noUrut', width: '5%', headerCellClass: "middle"},
            { name:'noPO', displayName: 'No. PO', field: 'noPO', width: '15%', headerCellClass: "middle"},
            { name:'tanggalPO', displayName: 'Tanggal PO', field: 'tanggalPO', width: '15%', headerCellClass: "middle"},
            { name:'noPart', displayName: 'No. Parts', field: 'partNumber', width: '15%', headerCellClass: "middle"},
            { name:'processNoPart', displayName: 'Process No. Part', field: 'processNoPart', width: '20%', headerCellClass: "middle"},
            {
                name:'tanggalAlokasi',
                displayName: 'Tanggal Alokasi',
                field: 'tanggalAlokasi',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Issue</td></tr></tbody></table></div>Tanggal Alokasi</div>"
            },
            {
                name:'qtyAlokasi',
                displayName: 'Qty Alokasi',
                field: 'qtyAlokasi',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Alokasi</div>"
            },
            {
                name:'noCase',
                displayName: 'No. Case',
                field: 'noCase',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>No. Case</div>"
            },

            {
                name:'tanggalPack',
                displayName: 'Tanggal Pack',
                field: 'tanggalPack',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:200%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Pack</td></tr></tbody></table></div>Tanggal Pack</div>"
            },
            {
                name:'qtyPack',
                displayName: 'Qty Pack',
                field: 'qtyPack',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Pack</div>"
            },

            {
                name:'noVendorInvoice',
                displayName: 'No. Vendor Invoice',
                field: 'noVendorInvoice',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Invoice</td></tr></tbody></table></div>No. Vendor Invoice</div>"
            },
            {
                name:'tanggalInvoice',
                displayName: 'Tanggal Invoice',
                field: 'tanggalInvoice',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Tanggal Invoice</div>"
            },
            {
                name:'qtyInvoice',
                displayName: 'Qty Invoice',
                field: 'qtyInvoice',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty</div>"
            },

            {
                name:'qtyCancel',
                displayName: 'Qty Cancel',
                field: 'qtyCancel',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:200%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Cancel/Denial</td></tr></tbody></table></div>Qty Cancel</div>"
            },
            {
                name:'qtyDenial',
                displayName: 'Qty Denial',
                field: 'qtyDenial',
                width: '18%',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Denial</div>"
            }
        ]

        // data: [
        //     {
        //       "noUrut": 1,
        //       "noPO": "PO/20160717003",
        //       "tanggalPO": "31/Mei/2016",
        //       "noPart": "PZ000-12345-12",
        //       "processNoPart": "PZ000-35764-01",
        //       "tanggalAlokasi": "3/Juni/2016",
        //       "qtyAlokasi": 1,
        //       "noCase": 0,
        //       "tanggalPack": "",
        //       "qtyPack": 0,
        //       "noVendorInvoice": "0678867",
        //       "tanggalInvoice": "1/Juni/2016",
        //       "qtyInvoice": 12,
        //       "qtyCancel": 0,
        //       "qtyDenial": 0
        //     },
        //     {
        //       "noUrut": 2,
        //       "noPO": "PO/20160717003",
        //       "tanggalPO": "31/Mei/2016",
        //       "noPart": "PZ000-12345-12",
        //       "processNoPart": "PZ000-35764-01",
        //       "tanggalAlokasi": "3/Juni/2016",
        //       "qtyAlokasi": 1,
        //       "noCase": 0,
        //       "tanggalPack": "",
        //       "qtyPack": 0,
        //       "noVendorInvoice": "0678867",
        //       "tanggalInvoice": "1/Juni/2016",
        //       "qtyInvoice": 12,
        //       "qtyCancel": 0,
        //       "qtyDenial": 0
        //     }
        // ]
    };

    $scope.gridPOSSInfo = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [5, 10, 20, 30, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 5,

        columnDefs: [
            { name:'noUrut', displayName: 'No.', field: 'noUrut', width: '5%'},
            { name:'noPO', displayName: 'No. PO', field: 'noPO', width: '15%'},
            { name:'TAMRefNo', displayName: 'TAM Reff. No.', field: 'TAMRefNo', width: '15%'},
            { name:'tanggalPO', displayName: 'Tanggal PO', field: 'tanggalPO', width: '15%'},
            { name:'noPart', displayName: 'No. Parts', field: 'noPart', width: '15%'},
            { name:'processNoPart', displayName: 'Process No. Part', field: 'processNoPart', width: '20%'},
            { name:'qtyPO', displayName: 'Qty PO', field: 'qtyPO', width: '18%'},
            { name:'qtyProcess', displayName: 'Qty Process', field: 'qtyProcess', width: '18%'},
            { name:'kodeRA', displayName: 'Kode RA', field: 'kodeRA', width: '18%'},
            { name:'tanggalAlokasi', displayName: 'Tanggal Alokasi', field: 'tanggalAlokasi', width: '18%'},
            { name:'harga', displayName: 'Harga', field: 'harga', width: '18%'}
        ]

        // data: [
        //     {
        //       "noUrut": 1,
        //       "noPO": "PO/20160717003",
        //       "TAMRefNo": "E26677",
        //       "tanggalPO": "31/Mei/2016",
        //       "noPart": "PZ000-12345-12",
        //       "processNoPart": "PZ000-35764-01",
        //       "qtyPO": 7,
        //       "qtyProcess": 5,
        //       "kodeRA": "OD",
        //       "tanggalAlokasi": "3/Juni/2016",
        //       "harga": 100000
        //     },
        //     {
        //       "noUrut": 2,
        //       "noPO": "PO/20160717003",
        //       "TAMRefNo": "E26677",
        //       "tanggalPO": "31/Mei/2016",
        //       "noPart": "PZ000-12345-12",
        //       "processNoPart": "PZ000-35764-01",
        //       "qtyPO": 7,
        //       "qtyProcess": 5,
        //       "kodeRA": "OD",
        //       "tanggalAlokasi": "3/Juni/2016",
        //       "harga": 100000
        //     },
        // ]
    };



});
