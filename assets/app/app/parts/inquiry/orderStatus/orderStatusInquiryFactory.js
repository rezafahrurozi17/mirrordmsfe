angular.module('app')
  .factory('PartsOSInquiry', function($http, CurrentUser,$filter) {
    var currentUser = CurrentUser.user;
    var stateStatus = 0;
    return {
    	getData: function(data) {
	    	var startDate = $filter('date')(data.tanggalPO, 'yyyy-MM-dd');
	        return $http.get('/api/as/TPOSInquiry/GetGeneralOrderStatusInquiry', {params: {
	                                                            PartNo : (data.PartsCode==null?"":data.PartsCode),
	                                                            OutletCode : (data.OutletCode==null?"n/a":data.OutletCode),
	                                                            PONo : (data.PurchaseOrderNo==null?"":data.PurchaseOrderNo),
	                                                            TipeOrder : (data.TipeOrder==null?"n/a":data.TipeOrder),
	                                                            TanggalPO : (startDate==null?"":startDate)

	                                                          } });
      },
      getDataPending: function(data) {
	    	var startDate = $filter('date')(data.tanggalPO, 'yyyy-MM-dd');
	        return $http.get('/api/as/TPOSInquiry/GetOrderStatusPending', {params: {
	                                                            PartNo : (data.PartsCode==null?"":data.PartsCode),
	                                                            OutletCode : (data.OutletCode==null?"n/a":data.OutletCode),
	                                                            PONo : (data.PurchaseOrderNo==null?"":data.PurchaseOrderNo),
	                                                            TipeOrder : (data.TipeOrder==null?"n/a":data.TipeOrder),
	                                                            TanggalPO : (startDate==null?"":startDate)

	                                                          } });
      },
      getDataPendingBO: function(data) {
	    	var startDate = $filter('date')(data.tanggalPO, 'yyyy-MM-dd');
	        return $http.get('/api/as/TPOSInquiry/GetOrderStatusPendingBO', {params: {
	                                                            PartNo : (data.PartsCode==null?"":data.PartsCode),
	                                                            OutletCode : (data.OutletCode==null?"n/a":data.OutletCode),
	                                                            PONo : (data.PurchaseOrderNo==null?"":data.PurchaseOrderNo),
	                                                            TipeOrder : (data.TipeOrder==null?"n/a":data.TipeOrder),
	                                                            TanggalPO : (startDate==null?"":startDate)

	                                                          } });
      },
      GetOrderStatusIssueToShip: function(data) {
	    	var startDate = $filter('date')(data.tanggalPO, 'yyyy-MM-dd');
	        return $http.get('/api/as/TPOSInquiry/GetOrderStatusIssueToShip', {params: {
	                                                            PartNo : (data.PartsCode==null?"":data.PartsCode),
	                                                            OutletCode : (data.OutletCode==null?"n/a":data.OutletCode),
	                                                            PONo : (data.PurchaseOrderNo==null?"":data.PurchaseOrderNo),
	                                                            TipeOrder : (data.TipeOrder==null?"n/a":data.TipeOrder),
	                                                            TanggalPO : (startDate==null?"":startDate)

	                                                          } });
      },
      GetOrderStatusPOSSInfo: function(data) {
	    	var startDate = $filter('date')(data.tanggalPO, 'yyyy-MM-dd');
	        return $http.get('/api/as/TPOSInquiry/GetOrderStatusPOSSInfo', {params: {
	                                                            PartNo : (data.PartsCode==null?"":data.PartsCode),
	                                                            OutletCode : (data.OutletCode==null?"n/a":data.OutletCode),
	                                                            PONo : (data.PurchaseOrderNo==null?"":data.PurchaseOrderNo),
	                                                            TipeOrder : (data.TipeOrder==null?"n/a":data.TipeOrder),
	                                                            TanggalPO : (startDate==null?"":startDate)

	                                                          } });
      },
      setStatus : function(data){
        stateStatus = data;
      },
      getStatus : function(){
        return stateStatus;
      },
    }
  });