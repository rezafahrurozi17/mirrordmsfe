angular.module('app')
  .factory('ListToDo', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    this.MaterialTypeId = 0;
    this.ServiceTypeId = 0;
    // console.log(currentUser);
    return {
      getAllCount: function(data) {
        var res=$http.get('/api/as/ListToDo/GetAllData', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          MaterialTypeId: (data.MaterialTypeId==null?0:data.MaterialTypeId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate),
                                                          AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo)
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      GetWaitingPartsInfo: function(data) {
        var res=$http.get('/api/as/ListToDo/GetWaitingPartsInfo', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          MaterialTypeId: (data.MaterialTypeId==null?0:data.MaterialTypeId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate),
                                                          AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo)
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetWaitingPartsOrder: function(data) {
        var res=$http.get('/api/as/ListToDo/GetWaitingPartsOrder', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          MaterialTypeId: (data.MaterialTypeId==null?0:data.MaterialTypeId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate),
                                                          AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo)
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetWaitingGoodsIssue: function(data) {
        var res=$http.get('/api/as/ListToDo/GetWaitingGoodsIssue', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          MaterialTypeId: (data.MaterialTypeId==null?0:data.MaterialTypeId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate),
                                                          // AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo)
                                                          NoWo: (data.NoWo==null?"n/a":data.NoWo)
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetStatusRPP: function(data) {
        var res=$http.get('/api/as/ListToDo/GetStatusRPP', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate),
                                                          StockReturnRPPNo: (data.StockReturnRPPNo==null?"n/a":data.StockReturnRPPNo),
                                                          AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo),
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetStatusPartsClaim: function(data) { 
        console.log('20171219 - GetStatusPartsClaim data', data);
        var res=$http.get('/api/as/ListToDo/GetStatusPartsClaim', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          ServiceTypeId: (data.ServiceTypeId==null?0:data.ServiceTypeId),
                                                          PartsClaimNo: (data.StockReturnClaimNo==null?"n/a":data.StockReturnClaimNo),
                                                          LicensePlate: (data.LicensePlate==null?"n/a":data.LicensePlate)
                                                          //StockReturnClaimNo: (data.StockReturnClaimNo==null?"n/a":data.StockReturnClaimNo),
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetDeadStock: function(data) {
        var res=$http.get('/api/as/ListToDo/GetDeadStock', {params: {
                                                          OutletId: (data.OutletId==null?0:data.OutletId),
                                                          DeadStockNo: (data.DeadStockNo==null?"n/a":data.DeadStockNo),
                                                        } });
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getStatusWO: function(param) {
        var WoNo = param.replace(/\//g, '%20');
        var woNoFinal = angular.copy(WoNo);
        if (woNoFinal.includes("*")) {
            woNoFinal = woNoFinal.split('*');
            woNoFinal = woNoFinal[0];
        }
        var res = $http.get('/api/as/CheckIsGICancelWO/'+woNoFinal);
        return res;
      }
    }
  });
