angular.module('app')
  .controller('ListToDoController', function($scope, $http, $state, CurrentUser, PartsGlobal, ListToDo, MaterialRequest, PartsClaim, PartsDeadStockMaintain, ngDialog, $timeout, bsNotify, bsTab, Tabs, bsAlert) {

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };
    $scope.mFilter = {};
    var stopped;
    var pause = false;

    $scope.PartsInfo = 0;
    $scope.Order = 0;
    $scope.GoodsIssue = 0;
    $scope.Rpp = 0;
    $scope.PartsClaim = 0;
    $scope.DeadStock = 0;
    $scope.clickWaitingInfo = 0;
    $scope.clickWaitingOrder = 0;
    $scope.clickWaitingGoodIssue = 0;
    $scope.clickStatusRPP = 0;
    $scope.clickPartsCLaim = 0;
    $scope.clickPublishDataDeadStock = 0;
    $scope.CopyGrid1 = [];
    $scope.CopyGrid2 = [];
    $scope.CopyGrid3 = [];
    $scope.CopyGrid4 = [];
    $scope.CopyGrid5 = [];
    $scope.CopyGrid6 = [];



    

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = true;
      $scope.clock = "loading clock..."; // initialise the time variable
      $scope.tickInterval = 1000 //ms
      $scope.counter = 300;
      $scope.mFilter.Category = "0";
      $scope.mFilter.Kolom = "0";

      $scope.countdown();

      $timeout(function(){
        console.log("dev_parts 5 Desember 2017 " , document.lastModified, $scope.user);
        //$scope.mData.MaterialTypeId = $scope.getRightMaterialType();
        if ($scope.user.RoleId == 1122) {
          $scope.isPart = true;
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.OutletId = $scope.user.OrgId;
        }else if ($scope.user.RoleId == 1123) {
          $scope.isPart = true;
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.OutletId = $scope.user.OrgId;
        }else if($scope.user.RoleId == 1125){
          $scope.isPart = false;
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.OutletId == $scope.user.OrgId;

        }else if($scope.user.RoleId == 1124){
          $scope.isPart = false;
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.OutletId = $scope.user.OrgId;
        }
        // if($scope.mData.MaterialTypeId == 1)
        // {
        //   $scope.isPart = true;
        //   $scope.mData.OutletId = $scope.user.OrgId;
        // }
        // else if($scope.mData.MaterialTypeId == 2)
        // {
        //   $scope.isPart = false;
        //   $scope.mData.OutletId = $scope.user.OrgId;
        // }else{
        //   if ($scope.user.RoleId = 1122) {
        //     $scope.isPart = true;
        //     $scope.mData.MaterialTypeId = 1;
        //     $scope.mData.OutletId = $scope.user.OrgId;
        //   }if else ($scope.user.RoleId = 1123) {
        //     $scope.isPart = true;
        //     $scope.mData.MaterialTypeId = 1;
        //     $scope.mData.OutletId = $scope.user.OrgId;
        //   }if else($scope.user.RoleId = 1125){
        //     $scope.isPart = false;
        //     $scope.mData.MaterialTypeId = 2;
        //     $scope.mData.OutletId = $scope.user.OrgId;
        //
        //   }if else ($scope.user.RoleId = 1124){
        //     $scope.isPart = false;
        //     $scope.mData.MaterialTypeId = 2;
        //     $scope.mData.OutletId = $scope.user.OrgId;
        //   }
        // }
        $scope.mData.ServiceTypeId = $scope.getRightServiceType();
        console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
        console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);

        // ==== new get all data count ===
        ListToDo.getAllCount($scope.mData).then(function(res) {
          var dataCount = res.data;
            // console.log(gridData);
            $scope.PartsInfo = dataCount.TotalPartsInfo;
            $scope.Order = dataCount.TotalPartOrder;
            $scope.GoodsIssue = dataCount.TotalWaitingGoodIssue;
            $scope.Rpp = dataCount.TotalStatusRPP;
            $scope.PartsClaim = dataCount.TotalStatusPartsClaim;
            $scope.DeadStock = dataCount.TotalDeadStock;
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      });
    });

    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        //console.log("myRights => ", $scope.myRights);
        return res;
    }

    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 9);
      return i;
    }
    $scope.getRightServiceType= function(){
      // var i = $scope.getRightX(10, 11);
      // return i;
      var serviceTypeId = 0;
      switch($scope.user.RoleId)
      {
          case 1135:
          case 1125:
          case 1122:
              serviceTypeId = 1;
              break;
          case 1123:
          case 1124:
              serviceTypeId = 0;
              break;
      }
      return serviceTypeId;
    }

    $scope.countdown = function() {
      if (Tabs.isExist('app.parts_list_to_do')) {
        stopped = $timeout(function() {
          //console.log($scope.counter);
          if (!pause) {
            $scope.counter--;
            if($scope.counter == 0)
            {
              console.log("====> refresh data ...");
              //[START] refresh data

              $scope.getData();
              // [END] refresh data
              $scope.counter = 300;
            }
            $scope.countdown();
          }
        }, 1000);
      }
    };

    var tick = function () {
        $scope.clock = Date.now() // get the current time
        //onTimer();
        $timeout(tick, $scope.tickInterval); // reset the timer
    }

    // Start the timer
    $timeout(tick, $scope.tickInterval);

    // $scope.getData = function(mFilter)
    // {
    //   console.log('apa nih isi mfilter',$scope.mFilter);
    //   if($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null)
    //   {

    //     console.log($scope.mData);
    //     ListToDo.GetWaitingPartsInfo($scope.mData).then(function(res) {
    //         var gridData = res.data;
    //         // console.log(gridData);
    //         $scope.grid_menunggu_parts_info.data = gridData;
    //         $scope.CopyGrid1 = angular.copy(gridData);
    //         $scope.PartsInfo = $scope.grid_menunggu_parts_info.data.length;
    //         console.log("GRIDdata=>", gridData);
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );

    //     ListToDo.GetWaitingPartsOrder($scope.mData).then(function(res) {
    //         var gridData = res.data;
    //         console.log('getWAITING : ',gridData);
    //         $scope.irregularity = gridData.Irregularity;
    //         console.log('$scope.irregularity : ',$scope.irregularity);
    //         $scope.CopyGrid2 = angular.copy(gridData);
    //         $scope.grid_menunggu_order.data = gridData;
    //         $scope.Order = $scope.grid_menunggu_order.data.length;
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );

    //     ListToDo.GetWaitingGoodsIssue($scope.mData).then(function(res) {
    //         var gridData = res.data;
    //         // console.log(gridData);
    //         $scope.CopyGrid3 = angular.copy(gridData);
    //         $scope.grid_menunggu_goods_issue.data = gridData;
    //         $scope.GoodsIssue = $scope.grid_menunggu_goods_issue.data.length;
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );

    //     $scope.ToNameStatus = function(GoodsIssueStatusId) {
    //           var statusName;
    //           switch (GoodsIssueStatusId) {
    //               case 0:
    //                   statusName = "Completed";
    //                   break;
    //               case 1:
    //                   statusName = "Cancelled";
    //                   break;
    //           }
    //           return statusName;
    //       }

    //     ListToDo.GetStatusRPP($scope.mData).then(function(res) {
    //         var gridData = res.data;
    //         // console.log(gridData);
    //         $scope.CopyGrid4 = angular.copy(gridData);
    //         $scope.grid_status_rpp.data = gridData;
    //         $scope.Rpp = $scope.grid_status_rpp.data.length;
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );

    //     ListToDo.GetStatusPartsClaim($scope.mData).then(function(res) {
    //         var gridData = res.data;
    //         // console.log(gridData);
    //         $scope.CopyGrid5 = angular.copy(gridData);
    //         $scope.grid_status_parts_claim.data = gridData;
    //         $scope.PartsClaim = $scope.grid_status_parts_claim.data.length;
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );
    //     ListToDo.GetDeadStock($scope.mData).then(function(res) {  
    //         console.log ("$scope.mData om senang", res.data);       
    //         var gridData = res.data;
    //         // console.log(gridData);
    //         $scope.CopyGrid6 = angular.copy(gridData);
    //         $scope.grid_menunggu_publish_data_dead_stock.data = gridData;

    //         $scope.DeadStock = $scope.grid_menunggu_publish_data_dead_stock.data.length;
    //       },
    //       function(err) {
    //         console.log("err=>", err);
    //       }
    //     );


    //   }
    //   else
    //   {
    //     console.log("Filter ", mFilter);

    //     if ($scope.mFilter.Category == '1'){
    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid1.length; i++){

    //         if($scope.mFilter.Kolom == 'RequiredDate'){
    //           var tahun = $scope.CopyGrid1[i].RequiredDate.getFullYear();
    //           var bulan = $scope.CopyGrid1[i].RequiredDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid1[i].RequiredDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid1[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid1[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_parts_info.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid1[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid1[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid1[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid1[i][$scope.mFilter.Kolom] = $scope.CopyGrid1[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid1[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_menunggu_parts_info.data[i]);
    //             }

    //             $scope.CopyGrid1[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
    //           }
              

    //         }


    //       }
    //       // $scope.grid_menunggu_parts_info.data = gridData;
    //       console.log('CopyGrid1 1', $scope.CopyGrid1);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_parts_info);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_menunggu_parts_info.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'

    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }
    //     } else if ($scope.mFilter.Category == '2') {

    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid2.length; i++){

    //         if($scope.mFilter.Kolom == 'RequiredDate'){
    //           var tahun = $scope.CopyGrid2[i].RequiredDate.getFullYear();
    //           var bulan = $scope.CopyGrid2[i].RequiredDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid2[i].RequiredDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid2[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid2[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_order.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid2[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid2[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid2[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid2[i][$scope.mFilter.Kolom] = $scope.CopyGrid2[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid2[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_menunggu_order.data[i]);
    //             }

    //             $scope.CopyGrid2[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
    //           }

    //         }


    //       }
    //       // $scope.grid_menunggu_order.data = gridData;
    //       console.log('CopyGrid2 1', $scope.CopyGrid2);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_order);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_menunggu_order.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           // size: 'big',
    //           // type: 'danger',
    //           // title: "Data Tidak Ditemukan"
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'
    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }

    //     } else if ($scope.mFilter.Category == '3') {

    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid3.length; i++){

    //         if($scope.mFilter.Kolom == 'RequiredDate'){
    //           var tahun = $scope.CopyGrid3[i].RequiredDate.getFullYear();
    //           var bulan = $scope.CopyGrid3[i].RequiredDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid3[i].RequiredDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid3[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid3[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_goods_issue.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid3[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid3[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid3[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid3[i][$scope.mFilter.Kolom] = $scope.CopyGrid3[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid3[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_menunggu_goods_issue.data[i]);
    //             }

    //             $scope.CopyGrid3[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);

    //           }

    //         }


    //       }
    //       // $scope.grid_menunggu_goods_issue.data = gridData;
    //       console.log('CopyGrid3 1', $scope.CopyGrid3);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_goods_issue);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_menunggu_goods_issue.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           // size: 'big',
    //           // type: 'danger',
    //           // title: "Data Tidak Ditemukan"
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'
    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }
          

    //     } else if ($scope.mFilter.Category == '4') {

    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid4.length; i++){

    //         if($scope.mFilter.Kolom == 'ShippingDate'){
    //           var tahun = $scope.CopyGrid4[i].ShippingDate.getFullYear();
    //           var bulan = $scope.CopyGrid4[i].ShippingDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid4[i].ShippingDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid4[i].ShippingDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid4[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_status_rpp.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid4[i].ShippingDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid4[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid4[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid4[i][$scope.mFilter.Kolom] = $scope.CopyGrid4[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid4[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_status_rpp.data[i]);
    //             }
  
    //             $scope.CopyGrid4[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
    //           }
              

    //         }

    //       }
    //       // $scope.grid_status_rpp.data = gridData;
    //       console.log('CopyGrid4 1', $scope.CopyGrid4);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_status_rpp);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_status_rpp.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           // size: 'big',
    //           // type: 'danger',
    //           // title: "Data Tidak Ditemukan"
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'
    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }

    //     } else if ($scope.mFilter.Category == '5') {
    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid5.length; i++){

    //         if($scope.mFilter.Kolom == 'ShippingDate'){
    //           var tahun = $scope.CopyGrid5[i].ShippingDate.getFullYear();
    //           var bulan = $scope.CopyGrid5[i].ShippingDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid5[i].ShippingDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid5[i].ShippingDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid5[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_status_parts_claim.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid5[i].ShippingDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid5[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid5[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid5[i][$scope.mFilter.Kolom] = $scope.CopyGrid5[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid5[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_status_parts_claim.data[i]);
    //             }
  
    //             $scope.CopyGrid5[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
    //           }
              

    //         }


    //       }
    //       // $scope.grid_status_parts_claim.data = gridData;
    //       console.log('CopyGrid5 1', $scope.CopyGrid5);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_status_parts_claim);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_status_parts_claim.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           // size: 'big',
    //           // type: 'danger',
    //           // title: "Data Tidak Ditemukan"
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'
    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }

    //     } else if ($scope.mFilter.Category == '6') {
    //       var dataSearch = [];
    //       for (var i=0; i<$scope.CopyGrid6.length; i++){
    //         // FilingDate ValidFrom ValidTo
    //         if($scope.mFilter.Kolom == 'FilingDate'){
    //           var tahun = $scope.CopyGrid6[i].FilingDate.getFullYear();
    //           var bulan = $scope.CopyGrid6[i].FilingDate.getMonth() + 1;
    //           var tgl = $scope.CopyGrid6[i].FilingDate.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid6[i].FilingDate = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid6[i].FilingDate = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else if ($scope.mFilter.Kolom == 'ValidFrom') {
    //           var tahun = $scope.CopyGrid6[i].ValidFrom.getFullYear();
    //           var bulan = $scope.CopyGrid6[i].ValidFrom.getMonth() + 1;
    //           var tgl = $scope.CopyGrid6[i].ValidFrom.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid6[i].ValidFrom = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid6[i].ValidFrom = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else if ($scope.mFilter.Kolom == 'ValidTo') {
    //           var tahun = $scope.CopyGrid6[i].ValidTo.getFullYear();
    //           var bulan = $scope.CopyGrid6[i].ValidTo.getMonth() + 1;
    //           var tgl = $scope.CopyGrid6[i].ValidTo.getDate();
    //           if (bulan < 10){
    //             bulan = '0'+bulan;
    //           }
    //           if (tgl < 10){
    //             tgl = '0'+tgl;
    //           }
    //           $scope.CopyGrid6[i].ValidTo = tahun + '-' + bulan + '-' + tgl;

    //           if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true ){
    //             dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
    //           }

    //           tahun = parseInt(tahun);
    //           bulan = parseInt(bulan) - 1;
    //           tgl = parseInt(tgl);
    //           $scope.CopyGrid6[i].ValidTo = new Date(tahun, bulan, tgl, 00, 00, 00);

    //         } else {
    //           if ($scope.CopyGrid6[i][$scope.mFilter.Kolom]!=null) {
    //             var beforeUppercase = angular.copy($scope.CopyGrid6[i][$scope.mFilter.Kolom]);
    //             $scope.CopyGrid6[i][$scope.mFilter.Kolom] = $scope.CopyGrid6[i][$scope.mFilter.Kolom].toUpperCase();
    //             var tempKeyword = angular.copy($scope.mFilter.Keyword);
    //             tempKeyword = tempKeyword.toUpperCase();
    //             if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes(tempKeyword) == true ){
    //               dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
    //             }
  
    //             $scope.CopyGrid6[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
    //           }
              
            
    //         }


    //       }
    //       // $scope.grid_menunggu_publish_data_dead_stock.data = gridData;
    //       console.log('CopyGrid6 1', $scope.CopyGrid6);
    //       console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_publish_data_dead_stock);
    //       console.log('dataSearch 1', dataSearch);
    //       if (dataSearch.length > 0){
    //         $scope.grid_menunggu_publish_data_dead_stock.data = dataSearch;
    //       } else {
    //         bsNotify.show({
    //           // size: 'big',
    //           // type: 'danger',
    //           // title: "Data Tidak Ditemukan"
    //           title: "Data Tidak Ditemukan",
    //           content: " ",
    //           type: 'danger'
    //         });
    //         $scope.ngDialog.close();
    //         console.log("coba masuk ngga");
    //       }

    //     }


    //     // di bawah ini codingan lama
    //                           // // blok pengolahan data filter
    //                           // console.log("Filter ", mFilter);
    //                           // if(mFilter.Kolom == "1")
    //                           // {
    //                           //   $scope.mData.LicensePlate = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "2")
    //                           // {
    //                           //   $scope.mData.AppointmentNo = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "3")
    //                           // {
    //                           //   $scope.mData.NoWo = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "5")
    //                           // {
    //                           //   $scope.mData.StockReturnRPPNo = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "6")
    //                           // {
    //                           //   $scope.mData.StockReturnClaimNo = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "7")
    //                           // {
    //                           //   $scope.mData.DeadStockNo = mFilter.Keyword;
    //                           // }
    //                           // else if(mFilter.Kolom == "0")
    //                           // {
    //                           //   $scope.mData.DeadStockNo = mFilter.Keyword;
    //                           // }
    //                           // // blok pengolahan data filter
    //   }


    // }

    //List To Do New Version
    // $('#grid1').on('shown.bs.collapse', function() {
    //   // console.log("shown");
    //   $scope.clickWaitingInfo = 1;
    // }).on('show.bs.collapse', function() {
    //   // console.log("show");
    // }).on('hide.bs.collapse', function() {
    //   // console.log("hide");
    //   $scope.clickWaitingInfo = 0;
    // });
    $scope.getData = function(mFilter){
      // ==== new get all data count ===
      ListToDo.getAllCount($scope.mData).then(function(res) {
        var dataCount = res.data;
          // console.log(gridData);
          $scope.PartsInfo = dataCount.TotalPartsInfo;
          $scope.Order = dataCount.TotalPartOrder;
          $scope.GoodsIssue = dataCount.TotalWaitingGoodIssue;
          $scope.Rpp = dataCount.TotalStatusRPP;
          $scope.PartsClaim = dataCount.TotalStatusPartsClaim;
          $scope.DeadStock = dataCount.TotalDeadStock;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // =====
      if($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {
        if($scope.clickWaitingInfo == 1){
          ListToDo.GetWaitingPartsInfo($scope.mData).then(function(res) {
            var gridData = res.data;
            // console.log(gridData);
            $scope.grid_menunggu_parts_info.data = gridData;
            $scope.CopyGrid1 = angular.copy(gridData);
            $scope.PartsInfo = $scope.grid_menunggu_parts_info.data.length;
            // console.log("GRIDdata=>", gridData);
          },
          function(err) {
            console.log("err=>", err);
          }
          );
        }
        if($scope.clickWaitingOrder == 1){
          ListToDo.GetWaitingPartsOrder($scope.mData).then(function(res) {
            var gridData = res.data;
            console.log('getWAITING : ',gridData);
            $scope.irregularity = gridData.Irregularity;
            console.log('$scope.irregularity : ',$scope.irregularity);
            $scope.CopyGrid2 = angular.copy(gridData);
            $scope.grid_menunggu_order.data = gridData;
            $scope.Order = $scope.grid_menunggu_order.data.length;
          },
          function(err) {
            console.log("err=>", err);
                }
            );
        }
        if($scope.clickWaitingGoodIssue == 1){
          ListToDo.GetWaitingGoodsIssue($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            console.log('getWAITING : ',gridData);
            $scope.irregularity = gridData.Irregularity;
            console.log('$scope.irregularity : ',$scope.irregularity);
            $scope.CopyGrid3 = angular.copy(gridData);

            $scope.grid_menunggu_goods_issue.data = gridData;
            $scope.GoodsIssue = $scope.grid_menunggu_goods_issue.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
        }
        if($scope.clickStatusRPP == 1){
          ListToDo.GetStatusRPP($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid4 = angular.copy(gridData);
            $scope.grid_status_rpp.data = gridData;
            $scope.Rpp = $scope.grid_status_rpp.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
        }
        if($scope.clickPartsCLaim == 1){
          ListToDo.GetStatusPartsClaim($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid5 = angular.copy(gridData);
            $scope.grid_status_parts_claim.data = gridData;
            $scope.PartsClaim = $scope.grid_status_parts_claim.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
        }
        if($scope.clickPublishDataDeadStock == 1){
          ListToDo.GetDeadStock($scope.mData).then(function (res) {
            console.log("$scope.mData om senang", res.data);
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid6 = angular.copy(gridData);
            $scope.grid_menunggu_publish_data_dead_stock.data = gridData;
  
            $scope.DeadStock = $scope.grid_menunggu_publish_data_dead_stock.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        )
        }
      }else{
        if ($scope.mFilter.Category == '1' ) {
          if($scope.clickWaitingInfo == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid1.length; i++) {
  
            if ($scope.mFilter.Kolom == 'RequiredDate') {
              var tahun = $scope.CopyGrid1[i].RequiredDate.getFullYear();
              var bulan = $scope.CopyGrid1[i].RequiredDate.getMonth() + 1;
              var tgl = $scope.CopyGrid1[i].RequiredDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid1[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid1[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_parts_info.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid1[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid1[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid1[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid1[i][$scope.mFilter.Kolom] = $scope.CopyGrid1[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid1[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_menunggu_parts_info.data[i]);
                }
  
                $scope.CopyGrid1[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
              }
  
  
            }
  
  
          }
          // $scope.grid_menunggu_parts_info.data = gridData;
          console.log('CopyGrid1 1', $scope.CopyGrid1);
          console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_parts_info);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_menunggu_parts_info.data = dataSearch;
          } else {
            bsNotify.show({
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
  
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
        } else if ($scope.mFilter.Category == '2') {
          if($scope.clickWaitingOrder == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid2.length; i++) {
  
            if ($scope.mFilter.Kolom == 'RequiredDate') {
              var tahun = $scope.CopyGrid2[i].RequiredDate.getFullYear();
              var bulan = $scope.CopyGrid2[i].RequiredDate.getMonth() + 1;
              var tgl = $scope.CopyGrid2[i].RequiredDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid2[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid2[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_order.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid2[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid2[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid2[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid2[i][$scope.mFilter.Kolom] = $scope.CopyGrid2[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid2[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_menunggu_order.data[i]);
                }
  
                $scope.CopyGrid2[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
              }
  
            }
  
  
          }
          // $scope.grid_menunggu_order.data = gridData;
          console.log('CopyGrid2 1', $scope.CopyGrid2);
          console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_order);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_menunggu_order.data = dataSearch;
          } else {
            bsNotify.show({
              // size: 'big',
              // type: 'danger',
              // title: "Data Tidak Ditemukan"
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
  
        } else if ($scope.mFilter.Category == '3') {
          if($scope.clickWaitingGoodIssue == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid3.length; i++) {
  
            if ($scope.mFilter.Kolom == 'RequiredDate') {
              var tahun = $scope.CopyGrid3[i].RequiredDate.getFullYear();
              var bulan = $scope.CopyGrid3[i].RequiredDate.getMonth() + 1;
              var tgl = $scope.CopyGrid3[i].RequiredDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid3[i].RequiredDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid3[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_goods_issue.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid3[i].RequiredDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid3[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid3[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid3[i][$scope.mFilter.Kolom] = $scope.CopyGrid3[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid3[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_menunggu_goods_issue.data[i]);
                }
  
                $scope.CopyGrid3[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
  
              }
  
            }
  
  
          }
          // $scope.grid_menunggu_goods_issue.data = gridData;
          console.log('CopyGrid3 1', $scope.CopyGrid3);
          console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_goods_issue);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_menunggu_goods_issue.data = dataSearch;
          } else {
            bsNotify.show({
              // size: 'big',
              // type: 'danger',
              // title: "Data Tidak Ditemukan"
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
  
  
        } else if ($scope.mFilter.Category == '4') {
          if($scope.clickStatusRPP == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid4.length; i++) {
  
            if ($scope.mFilter.Kolom == 'ShippingDate') {
              var tahun = $scope.CopyGrid4[i].ShippingDate.getFullYear();
              var bulan = $scope.CopyGrid4[i].ShippingDate.getMonth() + 1;
              var tgl = $scope.CopyGrid4[i].ShippingDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid4[i].ShippingDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid4[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_status_rpp.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid4[i].ShippingDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid4[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid4[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid4[i][$scope.mFilter.Kolom] = $scope.CopyGrid4[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid4[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_status_rpp.data[i]);
                }
  
                $scope.CopyGrid4[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
              }
  
  
            }
  
          }
          // $scope.grid_status_rpp.data = gridData;
          console.log('CopyGrid4 1', $scope.CopyGrid4);
          console.log('grid_menunggu_parts_info 1', $scope.grid_status_rpp);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_status_rpp.data = dataSearch;
          } else {
            bsNotify.show({
              // size: 'big',
              // type: 'danger',
              // title: "Data Tidak Ditemukan"
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
  
        } else if ($scope.mFilter.Category == '5') {
          if($scope.clickPartsCLaim == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid5.length; i++) {
  
            if ($scope.mFilter.Kolom == 'ShippingDate') {
              var tahun = $scope.CopyGrid5[i].ShippingDate.getFullYear();
              var bulan = $scope.CopyGrid5[i].ShippingDate.getMonth() + 1;
              var tgl = $scope.CopyGrid5[i].ShippingDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid5[i].ShippingDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid5[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_status_parts_claim.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid5[i].ShippingDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid5[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid5[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid5[i][$scope.mFilter.Kolom] = $scope.CopyGrid5[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid5[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_status_parts_claim.data[i]);
                }
  
                $scope.CopyGrid5[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
              }
  
  
            }
  
  
          }
          // $scope.grid_status_parts_claim.data = gridData;
          console.log('CopyGrid5 1', $scope.CopyGrid5);
          console.log('grid_menunggu_parts_info 1', $scope.grid_status_parts_claim);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_status_parts_claim.data = dataSearch;
          } else {
            bsNotify.show({
              // size: 'big',
              // type: 'danger',
              // title: "Data Tidak Ditemukan"
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
  
        } else if ($scope.mFilter.Category == '6') {
          if($scope.clickPublishDataDeadStock == 0){
            bsAlert.warning("Untuk filter data pastikan section sudah muncul", "silahkan klik terlebih dahulu");
            return false
          }
          var dataSearch = [];
          for (var i = 0; i < $scope.CopyGrid6.length; i++) {
            // FilingDate ValidFrom ValidTo
            if ($scope.mFilter.Kolom == 'FilingDate') {
              var tahun = $scope.CopyGrid6[i].FilingDate.getFullYear();
              var bulan = $scope.CopyGrid6[i].FilingDate.getMonth() + 1;
              var tgl = $scope.CopyGrid6[i].FilingDate.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid6[i].FilingDate = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid6[i].FilingDate = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else if ($scope.mFilter.Kolom == 'ValidFrom') {
              var tahun = $scope.CopyGrid6[i].ValidFrom.getFullYear();
              var bulan = $scope.CopyGrid6[i].ValidFrom.getMonth() + 1;
              var tgl = $scope.CopyGrid6[i].ValidFrom.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid6[i].ValidFrom = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid6[i].ValidFrom = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else if ($scope.mFilter.Kolom == 'ValidTo') {
              var tahun = $scope.CopyGrid6[i].ValidTo.getFullYear();
              var bulan = $scope.CopyGrid6[i].ValidTo.getMonth() + 1;
              var tgl = $scope.CopyGrid6[i].ValidTo.getDate();
              if (bulan < 10) {
                bulan = '0' + bulan;
              }
              if (tgl < 10) {
                tgl = '0' + tgl;
              }
              $scope.CopyGrid6[i].ValidTo = tahun + '-' + bulan + '-' + tgl;
  
              if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes($scope.mFilter.Keyword) == true) {
                dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
              }
  
              tahun = parseInt(tahun);
              bulan = parseInt(bulan) - 1;
              tgl = parseInt(tgl);
              $scope.CopyGrid6[i].ValidTo = new Date(tahun, bulan, tgl, 00, 00, 00);
  
            } else {
              if ($scope.CopyGrid6[i][$scope.mFilter.Kolom] != null) {
                var beforeUppercase = angular.copy($scope.CopyGrid6[i][$scope.mFilter.Kolom]);
                $scope.CopyGrid6[i][$scope.mFilter.Kolom] = $scope.CopyGrid6[i][$scope.mFilter.Kolom].toUpperCase();
                var tempKeyword = angular.copy($scope.mFilter.Keyword);
                tempKeyword = tempKeyword.toUpperCase();
                if ($scope.CopyGrid6[i][$scope.mFilter.Kolom].includes(tempKeyword) == true) {
                  dataSearch.push($scope.grid_menunggu_publish_data_dead_stock.data[i]);
                }
  
                $scope.CopyGrid6[i][$scope.mFilter.Kolom] = angular.copy(beforeUppercase);
              }
  
  
            }
  
  
          }
          // $scope.grid_menunggu_publish_data_dead_stock.data = gridData;
          console.log('CopyGrid6 1', $scope.CopyGrid6);
          console.log('grid_menunggu_parts_info 1', $scope.grid_menunggu_publish_data_dead_stock);
          console.log('dataSearch 1', dataSearch);
          if (dataSearch.length > 0) {
            $scope.grid_menunggu_publish_data_dead_stock.data = dataSearch;
          } else {
            bsNotify.show({
              // size: 'big',
              // type: 'danger',
              // title: "Data Tidak Ditemukan"
              title: "Data Tidak Ditemukan",
              content: " ",
              type: 'danger'
            });
            $scope.ngDialog.close();
            console.log("coba masuk ngga");
          }
  
        }
      }
    }

    // =====
    $scope.getDataWaitingPartsInfo = function(mFilter) {
      console.log('apa nih isi mfilter',$scope.mFilter);
      $('#grid1').on('shown.bs.collapse', function() {
        // console.log("shown");
        $scope.clickWaitingInfo = 1;
        if($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null){
          console.log($scope.mData);
          ListToDo.GetWaitingPartsInfo($scope.mData).then(function(res) {
              var gridData = res.data;
              // console.log(gridData);
              $scope.grid_menunggu_parts_info.data = gridData;
              $scope.CopyGrid1 = angular.copy(gridData);
              $scope.PartsInfo = $scope.grid_menunggu_parts_info.data.length;
              // console.log("GRIDdata=>", gridData);
            },
            function(err) {
              console.log("err=>", err);
             }
          );
        }
      }).on('show.bs.collapse', function() {
        // console.log("show");
      }).on('hide.bs.collapse', function() {
        // console.log("hide");
        $scope.clickWaitingInfo = 0;
      });

      // if($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null){
      //   console.log($scope.mData);
      //   ListToDo.GetWaitingPartsInfo($scope.mData).then(function(res) {
      //       var gridData = res.data;
      //       // console.log(gridData);
      //       $scope.grid_menunggu_parts_info.data = gridData;
      //       $scope.CopyGrid1 = angular.copy(gridData);
      //       $scope.PartsInfo = $scope.grid_menunggu_parts_info.data.length;
      //       console.log("GRIDdata=>", gridData);
      //     },
      //     function(err) {
      //       console.log("err=>", err);
      //      }
      //   );
      // }
    }

    $scope.GetWaitingPartsOrder = function(mFilter){
        console.log('apa nih isi mfilter',$scope.mFilter);
        $('#grid2').on('shown.bs.collapse', function() {
          // console.log("shown");
          $scope.clickWaitingOrder = 1;
          if($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {  
            console.log($scope.mData);
            ListToDo.GetWaitingPartsOrder($scope.mData).then(function(res) {
              var gridData = res.data;
              console.log('getWAITING : ',gridData);
              $scope.irregularity = gridData.Irregularity;
              console.log('$scope.irregularity : ',$scope.irregularity);
              $scope.CopyGrid2 = angular.copy(gridData);
              $scope.grid_menunggu_order.data = gridData;
              $scope.Order = $scope.grid_menunggu_order.data.length;
            },
            function(err) {
              console.log("err=>", err);
                  }
              );
          }
        }).on('show.bs.collapse', function() {
          // console.log("show");
        }).on('hide.bs.collapse', function() {
          // console.log("hide");
          $scope.clickWaitingOrder = 0;
        });
      }
  $scope.GetWaitingGoodsIssue = function (mFilter) {
    console.log('apa nih isi mfilter', $scope.mFilter);
    $('#grid3').on('shown.bs.collapse', function() {
      // console.log("shown");
      $scope.clickWaitingGoodIssue = 1;
      if ($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {
        console.log($scope.mData);
        ListToDo.GetWaitingGoodsIssue($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            console.log('getWAITING : ',gridData);
            $scope.irregularity = gridData.Irregularity;
            console.log('$scope.irregularity : ',$scope.irregularity);
            $scope.CopyGrid3 = angular.copy(gridData);
            $scope.grid_menunggu_goods_issue.data = gridData;
            $scope.GoodsIssue = $scope.grid_menunggu_goods_issue.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
      }
    }).on('show.bs.collapse', function() {
      // console.log("show");
    }).on('hide.bs.collapse', function() {
      // console.log("hide");
      $scope.clickWaitingGoodIssue = 0;
    });
    $scope.ToNameStatus = function (GoodsIssueStatusId) {
      var statusName;
      switch (GoodsIssueStatusId) {
        case 0:
          statusName = "Completed";
          break;
        case 1:
          statusName = "Cancelled";
          break;
      }
      return statusName;
    }
  }

  $scope.GetStatusRPP = function (mFilter) {
    console.log('apa nih isi mfilter', $scope.mFilter);
    $('#grid4').on('shown.bs.collapse', function() {
      // console.log("shown");
      $scope.clickStatusRPP = 1;
      if ($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {
        console.log($scope.mData);
        ListToDo.GetStatusRPP($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid4 = angular.copy(gridData);
            $scope.grid_status_rpp.data = gridData;
            $scope.Rpp = $scope.grid_status_rpp.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
      }
    }).on('show.bs.collapse', function() {
      // console.log("show");
    }).on('hide.bs.collapse', function() {
      // console.log("hide");
      $scope.clickStatusRPP = 0;
    });
  }

  $scope.GetStatusPartsClaim = function (mFilter) {
    console.log('apa nih isi mfilter', $scope.mFilter);
    $('#grid5').on('shown.bs.collapse', function() {
      // console.log("shown");
      $scope.clickPartsCLaim = 1;
      if ($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {
        console.log($scope.mData);
        ListToDo.GetStatusPartsClaim($scope.mData).then(function (res) {
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid5 = angular.copy(gridData);
            $scope.grid_status_parts_claim.data = gridData;
            $scope.PartsClaim = $scope.grid_status_parts_claim.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        );
      }
    }).on('show.bs.collapse', function() {
      // console.log("show");
    }).on('hide.bs.collapse', function() {
      // console.log("hide");
      $scope.clickPartsCLaim = 0;
    })
  }

  $scope.GetDeadStock = function (mFilter) {
    console.log('apa nih isi mfilter', $scope.mFilter);
    $('#grid6').on('shown.bs.collapse', function() {
      // console.log("shown");
      $scope.clickPublishDataDeadStock = 1;
      if ($scope.mFilter.Keyword == '' || $scope.mFilter.Keyword == undefined || $scope.mFilter.Keyword == null) {
        console.log($scope.mData);
        ListToDo.GetDeadStock($scope.mData).then(function (res) {
            console.log("$scope.mData om senang", res.data);
            var gridData = res.data;
            // console.log(gridData);
            $scope.CopyGrid6 = angular.copy(gridData);
            $scope.grid_menunggu_publish_data_dead_stock.data = gridData;
  
            $scope.DeadStock = $scope.grid_menunggu_publish_data_dead_stock.data.length;
          },
          function (err) {
            console.log("err=>", err);
          }
        )
      }
    }).on('show.bs.collapse', function() {
      // console.log("show");
    }).on('hide.bs.collapse', function() {
      // console.log("hide");
      $scope.clickPublishDataDeadStock = 0;
    });
  }
    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mFilter.Category = null;
      $scope.mFilter.Kolom = null;
      $scope.mFilter.Keyword = null;

      $scope.mData.LicensePlate = null;
      $scope.mData.AppointmentNo = null;
      $scope.mData.StockReturnRPPNo = null;
      $scope.mData.StockReturnClaimNo = null;
      $scope.mData.DeadStockNo = null;

      $scope.getData();

    }


    $scope.changeCategory = function(dataCategory){
      $scope.mFilter.Kolom = null;
      $scope.mFilter.Keyword = null;
      console.log('value category selected', dataCategory);
      $scope.Kolom = [];

      if ($scope.mFilter.Category == '1') {
        if ($scope.CopyGrid1.length > 0){
          $scope.grid_menunggu_parts_info.data = angular.copy($scope.CopyGrid1);
        }
      } else if ($scope.mFilter.Category == '2') {
        if ($scope.CopyGrid2.length > 0){
          $scope.grid_menunggu_order.data = angular.copy($scope.CopyGrid2);
        }
      } else if ($scope.mFilter.Category == '3') {
        if ($scope.CopyGrid3.length > 0){
          $scope.grid_menunggu_goods_issue.data = angular.copy($scope.CopyGrid3);
        }
      } else if ($scope.mFilter.Category == '4') {
        if ($scope.CopyGrid4.length > 0){
          $scope.grid_status_rpp.data = angular.copy($scope.CopyGrid4);
        }
      } else if ($scope.mFilter.Category == '5') {
        if ($scope.CopyGrid5.length > 0){
          $scope.grid_status_parts_claim.data = angular.copy($scope.CopyGrid5);
        }
      } else if ($scope.mFilter.Category == '6') {
        if ($scope.CopyGrid6.length > 0){
          $scope.grid_menunggu_publish_data_dead_stock.data = angular.copy($scope.CopyGrid6);
        }
      }





      if(dataCategory == '1'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_menunggu_parts_info.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      } else if(dataCategory =='2'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_menunggu_order.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      } else if(dataCategory =='3'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_menunggu_goods_issue.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      } else if(dataCategory =='4'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_status_rpp.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      } else if(dataCategory =='5'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_status_parts_claim.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      } else if(dataCategory =='6'){
        var posisiAction = null;
        $scope.Kolom = angular.copy($scope.grid_menunggu_publish_data_dead_stock.columnDefs);
        for(var i=0; i<$scope.Kolom.length; i++){
          if($scope.Kolom[i].name == 'Action'){
            posisiAction = i;
          }
        }
        $scope.Kolom.splice(posisiAction,1);
        console.log('scope.Kolom', $scope.Kolom, posisiAction);

      }

    }

    $scope.changeKolom = function(dataKolom){
      $scope.mFilter.Keyword = null;
      console.log('ini kolom selected', dataKolom);
      console.log('ini Mfilter', $scope.mFilter);
      if ($scope.mFilter.Category == '1') {
        if ($scope.CopyGrid1.length > 0){
          $scope.grid_menunggu_parts_info.data = angular.copy($scope.CopyGrid1);
        }
      } else if ($scope.mFilter.Category == '2') {
        if ($scope.CopyGrid2.length > 0){
          $scope.grid_menunggu_order.data = angular.copy($scope.CopyGrid2);
        }
      } else if ($scope.mFilter.Category == '3') {
        if ($scope.CopyGrid3.length > 0){
          $scope.grid_menunggu_goods_issue.data = angular.copy($scope.CopyGrid3);
        }
      } else if ($scope.mFilter.Category == '4') {
        if ($scope.CopyGrid4.length > 0){
          $scope.grid_status_rpp.data = angular.copy($scope.CopyGrid4);
        }
      } else if ($scope.mFilter.Category == '5') {
        if ($scope.CopyGrid5.length > 0){
          $scope.grid_status_parts_claim.data = angular.copy($scope.CopyGrid5);
        }
      } else if ($scope.mFilter.Category == '6') {
        if ($scope.CopyGrid6.length > 0){
          $scope.grid_menunggu_publish_data_dead_stock.data = angular.copy($scope.CopyGrid6);
        }
      }

    }



    //go to new tab
    var linkSaveCb = function(mode, model) {
        console.log("mode", mode);
        console.log("mode", model);
    };
    var linkBackCb = function(mode, model) {
        //console.log("mode", mode);
        //console.log("mode", model);
        pause=false;
        $scope.onDeleteFilter();
        $scope.counter = 300;
        $scope.countdown();
        $timeout(function(){
          $('#layoutContainer_ListToDoForm').css('display','block');
        },100)
    };
    $scope.newTab = function(param,row){
        console.log("newTab=>");
        console.log("row : ",row);
        pause=true;
        // $scope.formApi.setMode('detail');
        if(param == 1)
        {
          // bsTab.add("app.parts_materialrequest");
          PartsGlobal.getWarehouse({ OutletId : $scope.user.OrgId, MaterialTypeId : row.MaterialTypeId, ServiceTypeId : row.ServiceTypeId}).then(function(res) {
              MaterialRequest.getData({"MaterialRequestStatusId":1,"WHId":res.data.Result[0].WarehouseId, "RefMRNo":row.RefNo,"ServiceTypeId":row.ServiceTypeId,"MaterialTypeId":row.MaterialTypeId}).then(function(res){
                  console.log("getData MReq ====> ",res.data.Result);
                  console.log("row MReq ====> ",row.RequestBy);
                  res.data.Result[0].RequestBy = row.RequestBy;
                  res.data.Result[0].isFromListTodo = 1;
                  var x;
                  switch(row.TransactionType)
                  {
                    case "Appointment":
                      x = "1";
                      break;
                    case "WO":
                      x = "2";
                      break;
                    case "Sales Order":
                      x = "3";
                      break;
                  }
                  var temp = {"linksref":"app.parts_materialrequest", "linkview":"parts_materialrequest@app"};
                  $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', res.data.Result[0], 'true', 'true', temp, {"fromOtherModule":true});
                },
                function(err) {
                  console.log("err=>", err);
                }
              );
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }
        else if(param == 2)
        {
          // bsTab.add("app.purchaseorderparts");
          var temp = {"linksref":"app.purchaseorderparts", "linkview":"purchaseorderparts@app"};
          $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"RefNo":row.RefNo, "TransactionType":row.TransactionType, "Irregular":row.Irregularity, "fromOtherModule":true, "moduleName":"ltd"});
        }
        else if(param == 3)
        {
          // bsTab.add("app.goodsissue");
          if(row.TransactionType == 'Work Order'){
            row.RefGITypeID = 1;
          }else if(row.TransactionType == 'Sales Order'){
            row.RefGITypeID = 2;
          }else if(row.TransactionType == 'Transfer Order'){
            row.RefGITypeID = 3;
          }
          if(row.RefGITypeID == 1){
            ListToDo.getStatusWO(row.RefNo).then(function(res){
              if(res.data === 666){
                bsAlert.warning('WO dengan nomor ' + row.RefNo +' sedang menunggu approval', 'silahkan cek kembali')
                return false
              }

              var temp = {"linksref":"app.goodsissue", "linkview":"goodsissue@app"};
              $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"RefGINo":row.RefNo, "TransactionType":row.TransactionType,"Irregular":row.Irregularity, "MaterialTypeId":row.MaterialTypeId, "ServiceTypeId":row.ServiceTypeId,"RefGITypeID":row.RefGITypeID, "fromOtherModule":true});
              $timeout(function(){
                $('#layoutContainer_ListToDoForm').css('display','none');
              },100);
            });
          }else{
              var temp = {"linksref":"app.goodsissue", "linkview":"goodsissue@app"};
              $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"RefGINo":row.RefNo, "TransactionType":row.TransactionType,"Irregular":row.Irregularity, "MaterialTypeId":row.MaterialTypeId, "ServiceTypeId":row.ServiceTypeId,"RefGITypeID":row.RefGITypeID, "fromOtherModule":true});
              $timeout(function(){
                $('#layoutContainer_ListToDoForm').css('display','none');
              },100);
          }
        }
        else if(param == 4)
        {
          // bsTab.add("app.parts_rpp");
          var temp = {"linksref":"app.parts_rpp", "linkview":"parts_rpp@app"};
          $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"AppointmentNo":row.AppointmentNo, "LicensePlate":row.LicensePlate, "fromOtherModule":true});
        }
        else if(param == 5)
        {
          // bsTab.add("app.parts_parts_claim");
          PartsClaim.getData({"StockReturnClaimNo":row.PartsClaimNo}).then(function(res){
            console.log("res PartsClaim ====> ",res.data.Result);
            var temp = {"linksref":"app.parts_parts_claim", "linkview":"parts_parts_claim@app"};
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', res.data.Result[0], 'true', 'true', temp, {"fromOtherModule":true});
          },
          function(res){
            console.log("err=>", err);
          });
        }
        else if(param == 6)
        {
          // bsTab.add("app.parts_deadstockmaintain");
          PartsGlobal.getWarehouse({ OutletId : $scope.user.OrgId, MaterialTypeId : row.MaterialTypeId, ServiceTypeId : row.ServiceTypeId}).then(function(res) {
            $scope.mData.WarehouseId = res.data.Result[0].WarehouseId;
          PartsDeadStockMaintain.getData({"OutletId":$scope.user.OrgId, "vDeadStockNo":row.DeadStockNo,"WarehouseId":$scope.mData.WarehouseId }).then(function(res) {        
              console.log("Apa=>", row.DeadStockNo);
              console.log("Mau cek mdata", $scope.mData);
              console.log("ini isinya", res.data.Result[0]);
            $scope.mData.DeadStockNo =  row.DeadStockNo;
            $scope.mData.OutletId = $scope.user.OrgId;            
            $scope.mData.DeadstockId = row.DeadstockId;
            var temp = {"linksref":"app.parts_deadstockmaintain", "linkview":"parts_deadstockmaintain@app"};
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', res.data.Result[0], 'true', 'true', temp, {"DeadstockId":$scope.mData.DeadstockId,"DeadStockNo": $scope.mData.DeadStockNo,"fromOtherModule":true});     
   
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        });
        }
    }


    //----------------------------------
    // Grid Setup
    //----------------------------------
    //----------------------------------
    // Dummy grid_menunggu_parts_info
    //----------------------------------
    $scope.grid_menunggu_parts_info = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'Countdown', field: 'Countdown'},
          { name:'Tanggal Dibutuhkan', field: 'RequiredDate', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Transaksi', field: 'TransactionType' },
          { name:'No Referensi', field: 'RefNo' },
          { name:'No Polisi', field: 'LicensePlate'},
          { name:'Request Dari', field: 'RequestBy'},
          { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.newTab(1,row.entity)"><u>Isi No Parts</u></a></center>', visible : true
          }
        ]
      };


    //----------------------------------
    // Dummy grid_menunggu_order
    //----------------------------------
    // var statTemp = '<center><a href="" ng-if="COL_FIELD != \'Butuh DP\'" ng-click="grid.appScope.newTab(2,row.entity)" ng-if="row.entity.FlagApproval == 1"><u>Buat Order</u></a></center>';
    var statTemp = '<center><a href="" ng-click="grid.appScope.newTab(2,row.entity)" ng-if="row.entity.FlagApproval == 1"><u>Buat Order</u></a></center>';
    $scope.grid_menunggu_order = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'Countdown', field: 'Countdown'},
          { name:'Tanggal Dibutuhkan', field: 'RequiredDate', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Transaksi', field: 'TransactionType' },
          { name:'No Referensi', field: 'RefNo', width: '15%' },
          { name:'No Polisi', field: 'LicensePlate', width: '8%'},
          { name:'Request Dari', field: 'RequestBy', width: '20%'},
          { name:'Irregularity', field: 'Irregularity'},
          { name:'Action', field: 'Irregularity', cellTemplate: statTemp
          },
          // { name:'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.newTab(2,row.entity)"><u>Buat Order</u></a></center>', visible : true
          // }
        ]
      };

    //----------------------------------
    // Dummy grid_menunggu_goods_issue
    //----------------------------------
    $scope.grid_menunggu_goods_issue = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'Tanggal Dibutuhkan', field: 'RequiredDate', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Referensi GI', field: 'TransactionType' },
          { name:'No Referensi', field: 'RefNo' },
          { name:'No Polisi', field: 'LicensePlate'},
          { name:'Irregularity', field: 'Irregularity'},
          { name:'Customer (TO)', field: 'Customer'},
          { name:'Bengkel (TO)', field: 'Outlet'},
          { name: 'Action', cellTemplate: '<center><a href ng-click="grid.appScope.newTab(3,row.entity)"><u>Buat GI</u></a></center>', visible : true
          }
        ]
      };

    //----------------------------------
    // Dummy grid_status_rpp
    //----------------------------------
    $scope.grid_status_rpp = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'Countdown', field: 'Countdown'},
          { name:'Tanggal Shipping', field: 'ShippingDate', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'No Appointment', field: 'AppointmentNo' },
          { name:'No Polisi', field: 'LicensePlate'},
          { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.newTab(4,row.entity)"><u>Buat RPP</u></a></center>', visible : true
          }
        ]
      };


    //----------------------------------
    // Dummy grid_status_parts_claim
    //----------------------------------
    $scope.grid_status_parts_claim = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'Countdown', field: 'Countdown'},
          { name:'Tanggal Shipping', field: 'ShippingDate', cellFilter: 'date:\'yyyy-MM-dd\'', },
          { name:'No Parts Claim', field: 'PartsClaimNo' },
          { name:'No Polisi', field: 'LicensePlate'},
          { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.newTab(5,row.entity)"><u>Kirim Parts Claim</u></a></center>', visible : true
          }
        ]
      };

    //----------------------------------
    // Dummy grid_menunggu_publish_data_dead_stock
    //----------------------------------
    $scope.grid_menunggu_publish_data_dead_stock = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        columnDefs: [
          { name:'No Dead Stock', field: 'DeadStockNo'},
          { name:'Tanggal Pengajuan DS', field: 'FilingDate', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Masa Berlaku Dari', field: 'ValidFrom', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Masa Berlaku Sampai', field: 'ValidTo', cellFilter: 'date:\'yyyy-MM-dd\''},
          { name:'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.newTab(6,row.entity)"><u>Lengkapi Data</u></a></center>', visible : true
          }
        ]
      };

});
