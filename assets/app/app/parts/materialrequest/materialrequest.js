angular.module('app')
    .factory('MaterialRequest', function($http, CurrentUser, $filter, PartsGlobal) {
        var currentUser = CurrentUser.user;
        var MRHeader = {};
        this.formApi = {};
        this.DataList = {};
        this.DataIndex = -1;
        return {
            setMRHeader: function(data) {
                for (var k in data) MRHeader[k] = data[k];
                //MRHeader['MRstatusName'] = C_MATERIAL.Status[MRHeader.GoodsReceiptStatusId].name;
            },
            getMRHeader: function() {
                return MRHeader;
            },
            getData: function(data) {
                console.log("Data Param... : ", data);
                if (typeof data == 'undefined') {
                    data = { RefMRTypeId: "" }
                }
                if (data.RefMRTypeId == undefined || data.RefMRTypeId == ''){
                    data.RefMRTypeId = {};
                }
                if (data.RefMRTypeId.id == undefined){
                    data.RefMRTypeId.id = null
                }
                if (data.MaterialRequestStatusId == undefined){
                    data.MaterialRequestStatusId = {};
                }else{
                    var tmpMatreq = angular.copy(data.MaterialRequestStatusId);
                    data.MaterialRequestStatusId = {};
                    data.MaterialRequestStatusId = tmpMatreq;
                }
                
                if (data.MaterialRequestStatusId.id == undefined){
                    data.MaterialRequestStatusId.id = null
                }
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
                var res = $http.get('/api/as/PartMR', {
                    params: {
                        WHId: data.WHId,
                        Reqid: "", //"",// null
                        RefMRT: (data.RefMRTypeId.id == null ? "" : data.RefMRTypeId.id),
                        NoRef: (data.RefMRNo == null ? "" : data.RefMRNo),
                        Nopol: (data.LicensePlate == null ? "" : data.LicensePlate),
                        MRStatus: (data.MaterialRequestStatusId.id == null ? "" : data.MaterialRequestStatusId.id),
                        Tgl1: (data.startDate == null ? "" : startDate),
                        Tgl2: (data.endDate == null ? "" : endDate),
                        isGr: (data.ServiceTypeId == null ? "n/a" : data.ServiceTypeId),
                        matTypeId: (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId)
                    }
                });
                console.log('res.=>', res);
                return res;
            },
            getDataDouble: function(data) {               
                var res = $http.get('/api/as/PartGlobal/IsPartInfo', {
                    params: {
                        Partid: (data.Partid == null ? "" : data.Partid),
                        MRNO: (data.MRNO == null ? "" : data.MRNO),
                        taskid: (data.taskid == null ? "" : data.taskid),
                        WHid: (data.WHid == null ? "" : data.WHid)                        
                    }
                });
                console.log('res.=>', res);
                return res;
            },
            getDataForApprove: function(data) {
                console.log("Data Param : ", data);
                if (typeof data == 'undefined') {
                    data = { RefMRTypeId: "" }
                }
                var res = $http.get('/api/as/PartMR', {
                    params: {
                        whid: -1,
                        reqid: "-1",
                        refmrt: (data.RefMRTypeId == null ? "" : data.RefMRTypeId.id),
                        noref: (data.RefMRNo == null ? "" : data.RefMRNo),
                        nopol: (data.LicensePlate == null ? "" : data.LicensePlate),
                        mrstatus: (data.MaterialRequestStatusId == null ? "" : data.MaterialRequestStatusId.id),
                        tgl1: (data.startDate == null ? "" : data.startDate),
                        tgl2: (data.endDate == null ? "" : data.endDate),
                        isGr: (data.ServiceTypeId == null ? "n/a" : data.ServiceTypeId),
                        matTypeId: (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId)
                    }
                });
                console.log('res.=>', res);
                return res;
            },
            getDataItem: function(data) {
                console.log("Data Param : ", data);
                if (typeof data == 'undefined') { data = { RefMRTypeId: "" } }
                var res = $http.get('/api/as/PartMRItem', {
                    params: {
                        reqid: data.MaterialRequestId, // null
                        refmrt: (data.RefMRTypeId == null ? "" : data.RefMRTypeId),
                        isGr: (data.ServiceTypeId == null ? "n/a" : data.ServiceTypeId),
                        matTypeId: (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),
                        mrstatus: (data.MaterialRequestStatusId == null ? "" : data.MaterialRequestStatusId)
                    }
                });
                console.log('res.=>', res);
                return res;
                //return this.parts_status;
            },
            // createData: function(data) {
            //   var MRData = this.getMRHeader();
            //   MRData['ListItems'] = data;
            //   // var res=$http.post('/api/as/PartMR', data);
            //   var res=$http.post('/api/as/PartMR', MRData);
            //   return res;
            // },
            createData: function(data, detail) {
                console.log(' tambah data=>', data);
                console.log(' detail data=>', detail);
                //var stockReturnClaimNo = Math.floor((Math.random() * 10000000) + 1);
                return $http.post('/api/as/PartMR', [{
                    //MaterialRequestId: (data.Id==null?0:data.Id),
                    MaterialRequestNo: (data.MaterialRequestNo == null ? "-999" : data.MaterialRequestNo),
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    DocDate: (data.DocDate == null ? "" : data.DocDate),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    RefMRTypeId: (data.RefMRTypeId == null ? 0 : data.RefMRTypeId),
                    RefMRNo: (data.RefMRNo == null ? "" : data.RefMRNo),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    RequestBy: (data.RequestBy == null ? "" : data.RequestBy),
                    MaterialRequestStatusId: (data.MaterialRequestStatusId == null ? 0 : data.MaterialRequestStatusId),
                    APart_MaterialRequestItem: detail
                }]);
            },
            updateData: function(data, detail) {
                console.log("data a :", data);
                data.DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                // PartsGlobal.RequestApprovalSent= true; ---
                data['APart_MaterialRequestItemTs'] = detail;
                var jData = [];
                jData.push(data);
                console.log("data b :", jData);
                return $http.put('/api/as/PartMR', jData);
                // return $http.put('/api/as/PartMR', [{id: "1", name: "satu"}, {id: "2", name: "Dua"}]);
            },
            //api/as/PartMR/HeaderState
            headerState: function(data) {
                console.log("Data Param For Get HeaderState : ", data);
                var res = $http.get('/api/as/PartMR/HeaderState', { params: { mrid: data.MaterialRequestId } });
                console.log('res. headerState =>', res);
                return res;
            },
            //api/as/PartMR/ActApproval  {int DataId, int ApprovalStatus, int ProcessId = 8010, string ListMRIds =null}
            //api/as/PartGlobal/MRActApproval
            ActApproval: function(data) {
                console.log("Param Data for Approve : ", data);
                // var res=$http.get('/api/as/PartMR/ActApproval', {params: {DataId : data.MaterialRequestId,
                var res = $http.get('/api/as/PartGlobal/MRActApproval', {
                    params: {
                        DataId: data.MaterialRequestId,
                        ApprovalStatus: data.ApprovalStatus,
                        ListMRIds: data.ListDataIds
                    }
                });
                console.log('res.=>', res);
                return res;
            },
            PrePicking: function(data) {
                console.log("Data Param PrePicking : ", data);
                if (typeof data == 'undefined') { data = { RefMRTypeId: "" } }
                var res = $http.get('/api/as/PartMR/PrePicking', { params: { mrtypeid: data.MaterialTypeId, mrid: data.MaterialRequestId } });
                console.log('res.=>', res);
                return res;
            },
            buatGI: function(data) {
                console.log("Data Param buatGI : ", data);
                if (typeof data == 'undefined') { data = { RefMRTypeId: "" } }
                var res = $http.get('/api/as/PartMR/BuatGI', { params: { mrtypeid: data.MaterialTypeId, mrid: data.MaterialRequestId } });
                console.log('res.=>', res);
                return res;
            },
            buatPO: function(data) {
                console.log("Data Param buatPO : ", data);
                if (typeof data == 'undefined') { data = { RefMRTypeId: "" } }
                // var res=$http.get('/api/as/PartMR/BuatPO?mrtypeid=0&mrid=0');
                var res = $http.get('/api/as/PartMR/BuatPO', {
                    params: {
                        mrtypeid: data.refmrtypeid,
                        mrid: data.mrid,
                        mritemids: data.itemids
                    }
                });
                console.log('res.=>', res);
                return res;
            }
        }
    });
