angular.module('app')
    .controller('MaterialRequestController', function($scope, $http, CurrentUser, MaterialRequest, $timeout, ngDialog, //parApproval
        PartsGlobal, PartsLookup, $state, $filter, bsAlert, bsTab, WO
    ) {
        //----------------------------------
        // Start-Up  gridHideActionColumn
        //----------------------------------
        // baru 2 maret 2017
        $scope.items = ['Appointment', 'WO', 'Sales Order'];
        $scope.selection = $scope.items[0];
        $scope.disableSelect = false;
        $scope.actionVisible = false;
        $scope.gridHideActionColumn= true;
        $scope.mySelections = [];
        $scope.ApprovalProcessId = 8010; // gunakan nilai ini sbg ParamId di PGlobal
        $scope.user = CurrentUser.user();
        $scope.formApi = {};


        //Form State
        $scope.isNewForm = false;
        $scope.isEditForm = false;
        $scope.isOverlayForm = false;
        $scope.checkRights = function(bit) {
            console.log("$scope.myRights : ", $scope.myRights);
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            console.log("myRights res=> ", res);
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }

        $scope.getRightMaterialType = function() {
            var i = $scope.getRightX(8, 9);
            return i;
        }

        $scope.getRightServiceType = function() {
            var i = $scope.getRightX(10, 11);
            return i;
        }


        // $scope.checkRights = function(bit) {
        //     console.log("Rights : ", $scope.myRights);
        //     var p = $scope.myRights & Math.pow(2, bit);
        //     var res = (p == Math.pow(2, bit));
        //     console.log("myRights => ", $scope.myRights);
        //     return res;
        // }
        // $scope.getRightX = function(a, b) {
        //     var i = 0;
        //     if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
        //     else if ($scope.checkRights(b)) i = 2;
        //     else if ($scope.checkRights(a)) i = 1;
        //     return i;
        // }
        // $scope.getRightMaterialType = function() {
        //     var i = $scope.getRightX(8, 9);
        //     // if (($scope.checkRights(10) && $scope.checkRights(11))) i = 3;
        //     // else if ($scope.checkRights(11)) i= 2;
        //     // else if ($scope.checkRights(10)) i= 1;
        //     return i;
        // }
        // $scope.getRightServiceType = function() {
        //     var i = $scope.getRightX(10, 11);
        //     return i;
        // }

        //Referensi Edit State
        $scope.isWO = false;
        $scope.isSO = false;
        $scope.isAp = false; //$scope.isTO = false;
        $scope.showBuatPO = false;
        $scope.showPrePicking = false;
        $scope.showBuatGI = false;
        $scope.MaterialType = PartsLookup.lkp_MaterialType();
        $scope.RefMRType = PartsLookup.lkp_RefMRType();
        $scope.MRHeader = MaterialRequest.getMRHeader();
        $scope.IsBahan = false;
        //$scope.isPR = false;

        $scope.ref = '';
        $scope.GrandTotal = 0;

        // $scope.$on('$viewContentLoaded', function() {
        //     $scope.loading=true; // terkait row selection, krn data static di false-kan dulu
        //     $scope.gridData=[];
        // });
        // added by sss on 2018-03-14
        var loadedContent = false;
        //
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $timeout(function() {
                    // added by sss on 2018-03-14
                    loadedContent = true;
                }, 2000)
                //     //
                //     var parts = $scope.checkRights(2);
                //     console.log("edit ?= ", parts);
                //     console.log('user=', $scope.user);
                //     console.log("Rights : ", $scope.myRights);
                //     $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                //     $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                //     console.log("M T : ", $scope.mData.MaterialTypeId);
                //     console.log("S T : ", $scope.mData.ServiceTypeId);
                //     console.log("PartsGlobal.myWarehouse", PartsGlobal.myWarehouse);
                //     // if (PartsGlobal.RequestApprovalSent) {
                //     //       console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
                //     //       $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus']= 'Request Approval';
                //     //       console.log("Current Header : ", $scope.grid.data[$scope.CurrRowIndex]);
                //     //   }; // gak jadi 21-11-2017
                //     if (PartsGlobal.myWarehouse = {}) {
                //         if ($scope.mData.MaterialTypeId > 0 && $scope.mData.ServiceTypeId > 0) {
                //             // PartsGlobal.getMyWarehouse({ //getWarehouse
                //             PartsGlobal.getWarehouse({
                //                 OutletId: $scope.user.OrgId,
                //                 MaterialTypeId: $scope.mData.MaterialTypeId,
                //                 ServiceTypeId: $scope.mData.ServiceTypeId
                //             }).then(function(res) {
                //                     var gridData = res.data.Result;
                //                     console.log("warehouse ", gridData[0]);
                //                     PartsGlobal.myWarehouses = gridData;
                //                     PartsGlobal.myWarehouse = gridData[0];
                //                     $scope.mData.WarehouseId = gridData[0].WarehouseId;
                //                     $scope.mData.WarehouseName = gridData[0].WarehouseName;
                //                 },
                //                 function(err) {
                //                     console.log("err=>", err);
                //                 }
                //             );
                //         }
                //     } else {
                //         $scope.mData.WarehouseId = PartsGlobal.myWarehouse.WarehouseId;
                //         $scope.mData.WarehouseName = PartsGlobal.myWarehouse.WarehouseName;
                //     }
                // });
                // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                // console.log("Mat Type Id : ", $scope.mData.MaterialTypeId);
                // console.log("Service Type Id : ", $scope.mData.ServiceTypeId);
                // var wh;
                // PartsGlobal.getMyWarehouse({
                //     OutletId : $scope.user.OrgId, MaterialTypeId : $scope.mData.MaterialTypeId,
                //     ServiceTypeId : $scope.mData.ServiceTypeId}).then(function(res) {
                //     var gridData = res.data;
                //     console.log("warehouse ", gridData[0]);
                //     $scope.mData.WarehouseId = gridData[0].WarehouseId;

            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );

            // var wh = PartsGlobal.getMyWarehouse({
            //     OutletId : $scope.user.OrgId, MaterialTypeId : $scope.mData.MaterialTypeId,
            //     ServiceTypeId : $scope.mData.ServiceTypeId});
            // console.log("MyWareHouse : ", wh);
            // if (wh = {}) {
            //     console.log("wh : Not Defined")
            // } else {
            //     console.log("wh :", wh);
            // }
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        // $scope.statusMR = [
        //   {id:'1', name:'Menunggu Parts Info'},
        //   {id:'2', name:'Menunggu DP'},
        //   {id:'3', name:'Menunggu Order'},
        //   {id:'4', name:'Menunggu Receiving'}, //notAnOption: false},
        //   {id:'5', name:'Menunggu Pre Picking'},
        //   {id:'6', name:'Partial Pre Picking'},
        //   {id:'7', name:'Completed Pre Picking'},
        //   {id:'8', name:'Completed Request'},
        //   {id:'9', name:'Cancelled'}, //, notAnOption: true}
        //   {id:'99', name:'Need Approval'}
        // ];

        $scope.resizeLayout = function() {
            console.log("Make sure this function called");
            $timeout(function() {
                var staticHeight =
                    $("#page-footer").outerHeight() +
                    $(".nav.nav-tabs").outerHeight() +
                    $(".ui.breadcrumb").outerHeight() + 85;
                var advsearch = $(".advsearch");
                var advsearchHeight = $(".advsearch").outerHeight();
                if (!advsearchHeight) advsearchHeight = 0;
                $("#layoutContainer_ListToDoForm").height($(window).height() - 5);
            }, 0);
        };
        $scope.statusMR = [
            { id: '1', name: 'Menunggu Parts Info' },
            { id: '2', name: 'Menunggu DP' },
            { id: '3', name: 'Menunggu Order' },
            { id: '4', name: 'Menunggu Receiving' }, //notAnOptionpre: false},
            { id: '5', name: 'Menunggu Pre Picking' },
            { id: '6', name: 'Partial Pre Picking' },
            { id: '7', name: 'Completed Pre Picking' },
            { id: '75', name: 'Menunggu Goods Issue' },
            { id: '80', name: 'Completed Request' },
            { id: '90', name: 'Cancelled' }, //, notAnOption: true}
            { id: '99', name: 'Need Approval' }
        ];
        $scope.filterStatusMR = function() {
            return function(item) {
                if (item.id < 90) {
                    return true;
                }
                return false;
            };
        };

        //*
        $scope.myStatusMR = ''; //$scope.statusMR[0];
        //<select ng-model="myStatusMR" ng-options="statusMR.name for status in statusMR"></select>
        $scope.user = CurrentUser.user();
        //$scope.mMaterialRequest = null; //Model

        $scope.mData = {};
        $scope.mData.isFromListTodo = 0;
        $scope.mDataFilter = {};
        $scope.callerstr = '';
        $scope.xMaterialRequest = { selected: [] };
        $scope.nopol = 'D1234'; //$scope.myStatusMR.name; //
        $scope.EditableItem = false; //for flag enable/disable edit item, not yet
        $scope.hapusFilter = function() {
            // $scope.mDataFilter.RefMRTypeId='';
            $scope.mData.RefMRTypeId = null;
            $scope.mData.MaterialRequestStatusId = null;
            $scope.mData.startDate = null;
            $scope.mData.endDate = null;
            $scope.mData.RefMRNo = null;
            $scope.mData.LicensePlate = null;
            // alert("Hapus filter");
        }
        $scope.setHeaderState = function() {
            MaterialRequest.headerState($scope.MRHeader).then(function(res) {
                var vHeaderState = res.data.Result;
                console.log("Result headerstate : ", vHeaderState);
                $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus'] = vHeaderState[0].MaterialRequestStatus;
            })
        }
        $scope.onBulkApproveMR = function(mData) {
            console.log("mData : ", mData);
            // mData[0].dataId = mData[0].MaterialRequestId;
            // alert("Approve donk");
            console.log("di approve ");
            var ListIds = ';';
            for (var i = 0; i < mData.length; i++) {
                ListIds += mData[i].MaterialRequestId + ';';
            }
            if (ListIds == ';') {
                ListIds = null
                alert("No selected data to Approve");
                return 0;
            };
            console.log("Data Id : ", ListIds);
            // PartsGlobal.actApproval({
            //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].MaterialRequestId, ApprovalStatus: 1
            // })
            //int DataId, int ApprovalStatus, int ProcessId = 8010, string ListMRIds
            MaterialRequest.ActApproval({
                MaterialRequestId: 0,
                ApprovalStatus: 1, //ProcessId : 8010,
                ListDataIds: ListIds
            })
        }
        $scope.onBulkRejectMR = function(mData) {
            // alert("Maaf ditolak");
            console.log("mData : ", mData);
            // mData[0].dataId = mData[0].MaterialRequestId;
            alert("Reject approval ");
            var ListIds = ';';
            for (var i = 0; i < mData.length; i++) {
                ListIds += mData[i].MaterialRequestId + ';';
            }
            if (ListIds == ';') {
                ListIds = null
                alert("No selected data to Reject");
                return 0;
            };
            console.log("Data Id : ", ListIds);
            // PartsGlobal.actApproval({
            //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].MaterialRequestId, ApprovalStatus: 1
            // })
            //int DataId, int ApprovalStatus, int ProcessId = 8010, string ListMRIds
            MaterialRequest.ActApproval({
                    MaterialRequestId: 0,
                    ApprovalStatus: 2, //ProcessId : 8010,
                    ListDataIds: ListIds
                })
                // PartsGlobal.actApproval({
                //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].MaterialRequestId, ApprovalStatus: 2
                // })
        }
        $scope.anyorder = function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].MaterialRequestStatusId == '3' || data[i].MaterialRequestStatusId == 3) {
                    return true;
                }
            }
            return false;
        }
        $scope.anyGI = function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].MaterialRequestStatusId == '75' || data[i].MaterialRequestStatusId == 75) {
                    return true;
                }
            }
            return false;
        }
        $scope.anyPP = function(data) { // any Pre Picking
            for (var i = 0; i < data.length; i++) {
                if (data[i].MaterialRequestStatusId == '5' || data[i].MaterialRequestStatusId == 5 || data[i].MaterialRequestStatusId == '6' || data[i].MaterialRequestStatusId == 6) {
                    return true;
                }
            }
            return false;
        }
        $scope.RequestApprovalPra = function(doNext) {
            var str0 = 'Material Request';
            PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                console.log("Updated : 22-12-2017 11:29");
                $scope.Approver = res.data.Result[0].Approvers;
                // var vProcUpdate = MaterialRequest.updateData ;
                // var vProcNext = $scope.RequestApprovalNext ;
                if ($scope.Approver == null) { $scope.Approver = 'Approver X (Jika ini yg tampil, maka master data (approver) belum diset )' }
                PartsGlobal.showApproval02({
                    subtitle: str0 + ' ini',
                    kegiatan: str0,
                    noreq: $scope.MRHeader.MaterialRequestNo, //'MR 1234 fac',
                    approver: $scope.Approver, // 'Factory Judit, Factory Tini',
                    pesan: 'Mohon untuk approve panambahan qty berikut ',
                    controller: 'MaterialRequestController',
                    ProcessId: $scope.ApprovalProcessId,
                    userid: 0,
                    dataId: $scope.MRHeader.MaterialRequestId,
                    headerData: $scope.MRHeader,
                    detailData: $scope.gridDetail.data,
                    procUpdateData: MaterialRequest.updateData, //($scope.MRHeader, $scope.gridDetail.data)
                    procNext: $scope.RequestApprovalNext
                        // procUpdateData : {MaterialRequest.updateData, $scope.RequestApprovalNext}
                        // procUpdateData : {vProcUpdate, vProcNext} // gak error tp gak berhasil keeksekusi
                });
            });
            // if (resReqAppr) {
            //   console.log("Current Header b inner : ", $scope.grid.data[$scope.CurrRowIndex]);
            //   doNext();
            // }
        }
        $scope.RequestApprovalNext = function() {
            console.log("PartsGlobal.RequestApprovalSent : ", PartsGlobal.RequestApprovalSent);
            if (PartsGlobal.RequestApprovalSent) {
                console.log("Current Header b4 b4: ", $scope.grid.data[$scope.CurrRowIndex]);
                $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus'] = 'Request Approval';
                console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
            }
        }
        $scope.RequestApproval = function() {
            // var str0 = 'Material Request';
            console.log("Current Header on before Request : ", $scope.grid.data[$scope.CurrRowIndex]);
            //   var ReqApprNext = function() {
            //     $timeout(function(){
            //     console.log("PartsGlobal.RequestApprovalSent Header : ", PartsGlobal.RequestApprovalSent);
            //     if (PartsGlobal.RequestApprovalSent) {
            //         console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
            //         $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus']= 'Request Approval';
            //         console.log("Current Header : ", $scope.grid.data[$scope.CurrRowIndex]);
            //       }
            //   });
            // }
            // if (PartsGlobal.showApproval({
            // PartsGlobal.showApproval({
            //   subtitle: str0 +' ini',
            //   kegiatan: str0,
            //   noreq: $scope.MRHeader.MaterialRequestNo, //'MR 1234 fac',
            //   approver: 'Factory Judit, Factory Tini',
            //   pesan: 'Tolong approve segera ',
            //   controller: 'MaterialRequestController',
            //   ProcessId: $scope.ApprovalProcessId,
            //   userid: 0,
            //   dataId: $scope.MRHeader.MaterialRequestId,
            //   headerData: $scope.MRHeader,
            //   detailData: $scope.gridDetail.data,
            //   procUpdateData : MaterialRequest.updateData //($scope.MRHeader, $scope.gridDetail.data)
            //   }); //{
            $scope.RequestApprovalPra(function() {
                if (PartsGlobal.RequestApprovalSent) {
                    console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
                    $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus'] = 'Request Approval';
                    console.log("Current Header : ", $scope.grid.data[$scope.CurrRowIndex]);
                }
            });
            //if (PartsGlobal.RequestApprovalSent) {
            // ReqApprNext();
            //}
            //   console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
            //   $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus']= 'Request Approval';
            //   console.log("Current Header : ", $scope.grid.data[$scope.CurrRowIndex]);
            // }
            // $scope.RequestApprovalNext();
            // OKNext($scope.RequestApprovalPra());
            console.log("Current Header on after Request : ", $scope.grid.data[$scope.CurrRowIndex]);
        }
        $scope.goBack = function() {
            MaterialRequest.formApi.setMode("grid");
        };
        $scope.alertAfterSave = function(item) {
            var noPO = item; //.ResponseMessage;
            noPO = noPO.substring(noPO.lastIndexOf('#') + 1);
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Purchase Order",
                    text: noPO,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    // $scope.formApi.setMode('grid');
                    $scope.goBack();
                },
                function() {

                }
            )
        }

        $scope.btnSimpan = function(callerid) {
            //var callerstr='';
            // console.log("caller : ", callerid);
            if (callerid == 10) { callerstr = 'Appointment' } else if (callerid == 20) { callerstr = 'WO' } else { callerstr = 'Sales Order' };
            // alert ('Simpan ' + callerstr);
            // var str0 = 'Material Request';
            var MRStatusPrev = 0;
            var idx = -1
            console.log("data item :", $scope.gridDetail.data);
            console.log("data ini", $scope.MRHeader);
            $scope.dataabdi = 0;
            for(var i in $scope.gridDetail.data){
                var dataque = {Partid : $scope.gridDetail.data[i].PartId, MRNO : $scope.MRHeader.MaterialRequestNo, taskid : $scope.gridDetail.data[i].TaskId, WHid : $scope.MRHeader.WarehouseId };
                MaterialRequest.getDataDouble(dataque).then(function(res) {
                    $scope.dataane = res.data.Result;
                    for(var i in $scope.dataane){
                        if ($scope.dataane[i].IsPartInfo > 0){
                            $scope.dataabdi ++;
                        }
                        else{
                            $scope.dataabdi = 0;
                        }
                    }
                    console.log("INI", $scope.dataabdi);
                });
            }

            // $scope.datadetail = $scope.gridDetail.data;
            // for(var i in  $scope.datadetail)
            // {
            //     for(var i in  $scope.gridDetail.data)
            // {
            //     if ($scope.datadetail[i] == $scope.gridDetail.data[i].PartNo){
            //         if ($scope.datadetail[i].TaskName == $scope.gridDetail.data[i].TaskName){
            //             bsNotify.show(
            //                 {
            //                   title: "DATA DUPLIKAT",
            //                   content: "CEK DATA ANDA KEMBALI",
            //                   type: 'denger'
            //                 }
            //                 );
            //                 return 0;

            //         }
            //         else{
            //         }

            //     }
            //     else {

            //     }
            // }
            // }

            $scope.needApproval = $scope.gridDetail.data.length > $scope.itemOldData.length;
            //$scope.cekdatadouble =
            // $scope.needApproval = $scope.needApproval && !$scope.IsBahan
            if (!$scope.needApproval && !$scope.IsBahan) {
                for (i = 0; i < $scope.itemOldData.length; i++) {
                    if ($scope.gridDetail.data[i].QtyRequest > $scope.itemOldData[i].QtyRequest) {
                        // $scope.grid['MaterialRequestStatus']= 'Request Approval';
                        // $scope.gridDetail.data[i]['MaterialRequestStatus']= 'Request Approval';
                        MRStatusPrev = $scope.gridDetail.data[i]['MaterialRequestStatusId'];
                        idx = i;
                        //$scope.gridDetail.data[i]['MaterialRequestStatusId']= 10;///
                        $scope.gridDetail.data[i]['ApprovalStatus'] = 0; ///  Jika Request Approval, tandai dgn ini
                        $scope.needApproval = true;
                        break;
                    }
                }
            }
            // $scope.needApproval= true;  ,,,
            if ($scope.needApproval) {
                // console.log("grid api : ", $scope.gridApi);
                console.log("Scope grid : ", $scope.grid);
                // $scope.grid['MaterialRequestStatus']= 'Request Approval';
                // $scope.columnsParts['MaterialRequestStatusId']= 10;
            }

            if ($scope.IsBahan) {
                MaterialRequest.updateData($scope.MRHeader, $scope.gridDetail.data).then(function(res) {
                        var insertResponse = res.data;
                        console.log("insertResponse Bahan => ", res.data);
                        if (insertResponse.ResponseCode == 23) {
                            PartsGlobal.showAlert({
                                title: "Material Request",
                                content: "Data berhasil disimpan",
                                type: 'success'
                            });
                            MaterialRequest.formApi.setMode("grid");
                        } else if (insertResponse.ResponseCode == 98) {
                            PartsGlobal.showAlert({
                                title: "Material Request",
                                content: insertResponse.ResponseMessage,
                                type: 'danger'
                            });
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            } else {
                if ($scope.needApproval) {
                    PartsGlobal.RequestApprovalSent = false;
                    PartsGlobal.setReqAppSentPG(false);
                    // if (PartsGlobal.showApproval({ // baris ini menggantikan kode di atas
                    $scope.MRHeader['MaterialRequestStatusId'] = 99; // status = Need Approval
                    // $scope.grid['MaterialRequestStatusId']= 99; -- 20-11-2017
                    // $scope.MRHeader['MaterialRequestStatus']= 'Request Approval',
                    // $scope.grid['MaterialRequestStatus']= 'Request Approval',
                    $scope.MRHeader['ApprovalStatus'] = -1;
                    // PartsGlobal.showApproval({  -- 20-11-2017
                    // var IsSentRequest = PartsGlobal.showApproval({
                    //   subtitle: str0 +' ini',
                    //   kegiatan: str0,
                    //   noreq: $scope.MRHeader.MaterialRequestNo, //'MR 1234 fac',
                    //   approver: 'Factory Judit, Factory Tini',
                    //   pesan: 'Tolong approve segera ',
                    //   controller: 'MaterialRequestController',
                    //   ProcessId: $scope.ApprovalProcessId,
                    //   userid: 0,
                    //   dataId: $scope.MRHeader.MaterialRequestId,
                    //   headerData: $scope.MRHeader,
                    //   detailData: $scope.gridDetail.data,
                    //   procUpdateData : MaterialRequest.updateData //($scope.MRHeader, $scope.gridDetail.data)
                    // });
                    $scope.RequestApproval();
                    // if (PartsGlobal.showApproval({
                    // // PartsGlobal.showApproval({
                    //   subtitle: str0 +' ini',
                    //   kegiatan: str0,
                    //   noreq: $scope.MRHeader.MaterialRequestNo, //'MR 1234 fac',
                    //   approver: 'Factory Judit, Factory Tini',
                    //   pesan: 'Tolong approve segera ',
                    //   controller: 'MaterialRequestController',
                    //   ProcessId: $scope.ApprovalProcessId,
                    //   userid: 0,
                    //   dataId: $scope.MRHeader.MaterialRequestId,
                    //   headerData: $scope.MRHeader,
                    //   detailData: $scope.gridDetail.data,
                    //   procUpdateData : MaterialRequest.updateData //($scope.MRHeader, $scope.gridDetail.data)
                    //   })) { //).then(function (res) {
                    //     console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
                    //     $scope.grid.data[$scope.CurrRowIndex]['MaterialRequestStatus']= 'Request Approval';
                    //     console.log("Current Header : ", $scope.grid.data[$scope.CurrRowIndex]);
                    // }
                    MaterialRequest.formApi.setMode("grid");
                } else {
                    $scope.dataane = $scope.gridDetail.data;
                    console.log("$scope.gridDetail.data",$scope.gridDetail.data[0]);
                    for (var i in $scope.gridDetail.data){
                        console.log("Masuk kaga", $scope.gridDetail.data[0].PartId);

                        // for(var i in  $scope.dataane ){
                        //    if ($scope.gridDetail.data[i].PartsId == $scope.dataane[i].PartsId){
                        //         PartsGlobal.showAlert({
                        //             title: "Material Request",
                        //             content: "Parts Double",
                        //             type: 'danger'
                        //         });
                        //         return 0;
                        //     }
                        // }
                        // Supaya Harus Ngisi No Material Dulu
                        if($scope.gridDetail.data[i].MaterialRequestStatusId == 1){
                            if ($scope.gridDetail.data[i].PartId == 0 || $scope.gridDetail.data[i].PartId == null){
                                PartsGlobal.showAlert({
                                    title: "Material Request",
                                    content: "Harap Isi No Material",
                                    type: 'danger'
                                });
                                 return 0;
                            }
                        }
                        else if($scope.gridDetail.data[i].MaterialRequestStatusId != 1){
                            if ($scope.gridDetail.data[i].PartId == 0 || $scope.gridDetail.data[i].PartId == null){
                                PartsGlobal.showAlert({
                                    title: "Material Request",
                                    content: "Harap Isi No Material",
                                    type: 'danger'
                                });
                                return 0;
                            }
                            else{

                            }

                        }
                        
                        else if ($scope.dataabdi > 0 ){
                            PartsGlobal.showAlert({
                                title: "Material Request",
                                content: "Material duplikat",
                                type: 'danger'
                            });
                            return 0;
                        }

                    } //DIPINDAH KE ATAS LOOPINGNYA KARENA JADI GANGGU UPDATE STOCK NYA

                    MaterialRequest.updateData($scope.MRHeader, $scope.gridDetail.data).then(function(res) {
                            var insertResponse = res.data;
                            console.log("insertResponse Part=> ", res.data);
                            if (insertResponse.ResponseCode == 23) {
                                PartsGlobal.showAlert({
                                    title: "Material Request",
                                    content: "Data berhasil disimpan",
                                    type: 'success'
                                });

                                var dataNotifParts = {};
                                dataNotifParts.Message = "(Request Input Nomor parts anda sudah dipenuhi ("+$scope.MRHeader.LicensePlate+" - "+$scope.MRHeader.RefMRNo+") )";
                                if ($scope.MRHeader.WarehouseId == 1016) {
                                    WO.sendNotif(dataNotifParts, 1028, 45).then(
                                        function(res) {
                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        });
                                }else {
                                    WO.sendNotif(dataNotifParts, 1029, 45).then(
                                        function(res) {
                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        });
                                }
                                $scope.setHeaderState();
                                MaterialRequest.formApi.setMode("grid");
                            } else if (insertResponse.ResponseCode == 98) {
                                PartsGlobal.showAlert({
                                    title: "Material Request",
                                    content: insertResponse.ResponseMessage,
                                    type: 'danger'
                                });
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
            }
            // MaterialRequest.updateData($scope.MRHeader, $scope.gridDetail.data);
            $scope.hapusFilter(); // 30-11-2017
            return 0;
        };
        // parApproval.subtitle = str0 +' ini';
        // parApproval.kegiatan = str0;
        // parApproval.noreq = 'MR 321';
        // parApproval.approver = 'Farhan, Jihan';
        // parApproval.pesan = 'Mohon di approve ya';
        /*
              PartsGlobal.setparApproval({
                    subtitle: str0 +' ini',
                    kegiatan: str0,
                    noreq: 'MR 234',
                    approver: 'Judit, Tini',
                    pesan: 'Mohon di approve segera'
                    // btnOK: 'OK '
                });
              // console.log("parApproval 2 : ", PartsGlobal.parApproval);
              ngDialog.openConfirm ({
                //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">MIP Maintenance</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Perhitungan MIP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></div></div></form></div></div>',
                template:'<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
                plain: true,
                controller: 'MaterialRequestController', //'MIPMaintenanceController',
               });
        */
        //btnPrePicking
        var linkSaveCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
        }
        var linkBackCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
        }
        var tmpPrepicking = {}
        $scope.btnPrePicking = function() {

            console.log('tmpPrepicking', tmpPrepicking);
            console.log("Edit");
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', tmpPrepicking, 'true', 'true');
        }

        // $scope.btnPrePicking = function() {
        //     //var callerstr='';
        // var itemselected = angular.copy($scope.mySelections); //[];
        // if (itemselected.length == 0) return 1;
        // console.log("caller : ", callerid);
        // if (callerid == 10) {
        //     callerstr = 'Appointment';
        // } else if (callerid == 20) { callerstr = 'WO' } else { callerstr = 'Sales Order' };
        // var dataHeader = MaterialRequest.getMRHeader();
        // MaterialRequest.PrePicking(dataHeader);
        // PartsGlobal.showAlert({
        //     title: "PrePicking ",
        //     content: "PrePicking done",
        //     type: 'info'
        // });
        // return 0;
        // };

        $scope.btnBuatPO = function() {
           //=================== Code Lama Sebelum Pindah UI ===================//
            // var itemselected = angular.copy($scope.mySelections); //[];
            // //var anyorder= false;
            // // if ($scope.mySelections.length==0) return 1;
            // if (itemselected.length == 0) {
            //     PartsGlobal.showAlert({
            //         title: "Buat PO ",
            //         content: "Tidak ada item yg dipilih, pilih dulu item yg akan dibuat PO-nya",
            //         type: 'danger'
            //     });
            //     return 1;
            // }
            // if (!$scope.anyorder(itemselected)) return 2;

            // console.log("caller : ", callerid);
            // if (callerid == 10) { callerstr = 'Appointment' } else if (callerid == 20) { callerstr = 'WO' } else { callerstr = 'Sales Order' };
            // // alert ('to Purchase Order from : ' + callerstr);
            // // MaterialRequest.buatPO(itemselected);
            // MaterialRequest.buatPO({
            //     refmrtypeid: $scope.MRHeader.RefMRTypeId, //...
            //     mrid: itemselected[0].MaterialRequestId,
            //     itemids: $scope.getSelectedItemIds(itemselected)
            // }).then(function(res) {
            //     // call alert after save here
            //     var create = res.data.Result[0].Results;
            //     console.log('res.data = ', create);
            //     $scope.alertAfterSave(create);
            // });

            // // $state.go('app.purchaseorderparts');
            // // $state.reload();
            // return 0;
            //=================== Code Lama Sebelum Pindah UI ===================//

            var dataHeader = MaterialRequest.getMRHeader();
            console.log("adiibbb",dataHeader)
            var TransactionType_x = angular.copy(dataHeader.TransactionType)
            if(TransactionType_x == 'Work Order'){
                TransactionType_x = 'Reception'
            }
            var temp = {"linksref":"app.purchaseorderparts", "linkview":"purchaseorderparts@app"};
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"RefNo":dataHeader.RefMRNo, "TransactionType":TransactionType_x, "Irregular":dataHeader.Irregularity, "fromOtherModule":true, "moduleName":"ltd"});
        };
        $scope.btnBuatGI = function(dataHeader) {

            //=================== Code Lama Sebelum Pindah UI ===================//
            // //var callerstr='';
            // console.log("caller : ", callerid);
            // if (callerid == 10) { callerstr = 'Appointment' } else if (callerid == 20) { callerstr = 'WO' } else { callerstr = 'Sales Order' };
            // alert('to Trx GI from : ' + callerstr);
            // var dataHeader = MaterialRequest.getMRHeader();
            // MaterialRequest.buatGI(dataHeader);
            // PartsGlobal.showAlert({
            //     title: "Buat GI ",
            //     content: "Generate GI done",
            //     type: 'info'
            // });
            // return 0;
            //=================== Code Lama Sebelum Pindah UI ===================//
            var dataHeader = MaterialRequest.getMRHeader();

            if(dataHeader.TransactionType == 'Work Order'){
                dataHeader.RefGITypeID = 1;
              }else if(dataHeader.TransactionType == 'Sales Order'){
                dataHeader.RefGITypeID = 2;
              }else if(dataHeader.TransactionType == 'Transfer Order'){
                dataHeader.RefGITypeID = 3;
              }
            
            console.log("Coba Adib",dataHeader)
            var temp = {"linksref":"app.goodsissue", "linkview":"goodsissue@app"};
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"RefGINo":dataHeader.RefMRNo, "TransactionType":dataHeader.TransactionType,"Irregular":dataHeader.Irregularity, "MaterialTypeId":dataHeader.MaterialTypeId, "ServiceTypeId":dataHeader.ServiceTypeId,"RefGITypeID":dataHeader.RefGITypeID, "fromOtherModule":true});
            $timeout(function(){
              $('#layoutContainer_ListToDoForm').css('display','none');
            },100);
        }; //

        //----------------------------------
        // Get Data
        //----------------------------------
        // $scope.dataNewItem = [{MaterialRequestId: MRHeader.MaterialRequestId, InvoiceId: "",
        //     PartId: "", QtyBase: "",
        //     QtyGR: "", UomGR: ""}];
        $scope.getSelectedItemIds = function(item) {
            var res = ';';
            for (var i = 0; i < item.length; i++) {
                res += item[i].MaterialRequestItemId + ';'; //.toString;
            }
            return res;
        };

        var gridData = [];
        $scope.getData = function() { // $scope.getData({MaterialRequestId : -1})
            // $scope.getData = function(mDataFilter) {
            console.log('currentUser >>', $scope.user);
            console.log(' $scope.getRightServiceType() >>', $scope.getRightServiceType());
            var getMaterialTypeId;
            var getServiceTypeId;

            // var getServiceTypeId = $scope.getRightServiceType();
            // if ($scope.user.RoleId == 1122) {
            //     getMaterialTypeId = 1;
            // } else if ($scope.user.RoleId == 1123) {
            //     getMaterialTypeId = 1;
            // } else if ($scope.user.RoleId == 1125) {
            //     getMaterialTypeId = 2;
            // } else if ($scope.user.RoleId == 1124) {
            //     getMaterialTypeId = 2;
            // }

            if ($scope.user.RoleId == 1122) {
                getMaterialTypeId = 1;
                getServiceTypeId = 1;
                console.log('1122', getMaterialTypeId, getServiceTypeId);
            } else if ($scope.user.RoleId == 1123) {
                getMaterialTypeId = 1;
                getServiceTypeId = 0;
                console.log('1123', getMaterialTypeId, getServiceTypeId);
            } else if ($scope.user.RoleId == 1125) {
                getMaterialTypeId = 2;
                getServiceTypeId = 1;
                console.log('1125', getMaterialTypeId, getServiceTypeId);
                // getServiceTypeId = 1;
                // getServiceTypeId = 0;
                // $scope.FunctionGetData2(mData);
            } else if ($scope.user.RoleId == 1124) {
                getMaterialTypeId = 2;
                getServiceTypeId = 0;
                console.log('1124', getMaterialTypeId, getServiceTypeId);
            }
            // $scope.mData.MaterialTypeId = $scope.mData.MaterialTypeId === undefined ? getMaterialTypeId : $scope.mData.MaterialTypeId;
            // $scope.mData.ServiceTypeId = $scope.mData.ServiceTypeId === undefined ? getServiceTypeId : $scope.mData.ServiceTypeId;
            $scope.mData.MaterialTypeId = getMaterialTypeId;
            $scope.mData.ServiceTypeId = getServiceTypeId;


            PartsGlobal.getWarehouse({
                OutletId: $scope.user.OrgId,
                MaterialTypeId: $scope.mData.MaterialTypeId,
                ServiceTypeId: $scope.mData.ServiceTypeId
            }).then(function(res) {
                    var gridData = res.data.Result;
                    if (gridData.length > 1) {
                        var patahHati = _.map(gridData, 'WarehouseId');
                        patahHati = patahHati.toString();
                        console.log("patahHati ", patahHati);
                        $scope.mData.WarehouseId = patahHati
                        // var GRBP = _.map(gridData, 'WarehouseName');
                        // console.log("GRBP ", GRBP);
                        // var Bahan = GRBP[0].search("Bahan");
                        // var Parts = GRBP[0].search("Parts");
                        // if (Bahan < 0){
                        //     $scope.mData.MaterialTypeId = 1;
                        // }
                        // else{
                        //     $scope.mData.MaterialTypeId = 2;
                        // }
                        //console.log("GRBP ", Bahan,Parts);
                        $scope.doGetData();
                    } else {
                        console.log("warehouse ", gridData[0]);
                        PartsGlobal.myWarehouses = gridData;
                        PartsGlobal.myWarehouse = gridData[0];
                        $scope.mData.WarehouseId = gridData[0].WarehouseId;
                        $scope.mData.WarehouseName = gridData[0].WarehouseName;

                        $scope.doGetData();
                    };


                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.doGetData = function() {
            console.log("=============== getdata ===============");
            $timeout(function() {
              $('#layoutContainer_ListToDoForm').height(0);
            }, 5000);
            console.log("$scope.mData.MaterialRequestId=>", $scope.mData.MaterialRequestId,$('#layoutContainer_ListToDoForm').height());
            if ($scope.mData.MaterialRequestId == -1) {
                // if (mDataFilter.MaterialRequestId == -1) {
                // if ($scope.mData.WarehouseId == undefined) {$scope.mData.WarehouseId = PartsGlobal.myWarehouse.WarehouseId}
                $scope.mData['WHId'] = $scope.mData.WarehouseId;
                MaterialRequest.getDataForApprove($scope.mData)
                    .then(
                        function(res) {
                            $('#layoutContainer_ListToDoForm').height(10);
                            gridData = [];
                            $scope.grid.data = res.data.Result;
                            console.log("Data MR =>", res.data.Result);
                            $scope.loading = false;
                            $scope.mData.MaterialRequestId = 0;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            } else {
                if ($scope.mData.isFromListTodo != 1) {
                    if ($scope.mData.RefMRTypeId == null && $scope.mData.MaterialRequestStatusId == null && $scope.mData.startDate == null && $scope.mData.endDate == null &&
                        ($scope.mData.RefMRNo == null || $scope.mData.RefMRNo == "") && ($scope.mData.LicensePlate == null || $scope.mData.LicensePlate == "")) {
                        // added by sss on 2018-03-14
                        if (loadedContent) {
                            PartsGlobal.showAlert({
                                title: "Material Request",
                                content: "Filter Tanggal MR tidak boleh kosong",
                                type: 'danger'
                            });
                        }
                        return 0;
                    }
                    if ($scope.mData.startDate == null && $scope.mData.endDate == null && ($scope.mData.MaterialRequestStatusId != null || $scope.mData.RefMRTypeId != null)) {
                        PartsGlobal.showAlert({
                            title: "Material Request",
                            content: "Filter Referensi MR dan Tanggal MR harus diisi bersamaan",
                            type: 'danger'
                        });
                        return 0;
                    }
                }

                if ($scope.mData.WarehouseId == undefined) {
                    $scope.mData.WarehouseId = PartsGlobal.myWarehouse.WarehouseId
                }
                $scope.mData['WHId'] = $scope.mData.WarehouseId;              
                $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                console.log("param getdata : ", $scope.mData.MaterialRequestStatusId);
                if ($scope.mData.MaterialRequestStatusId == undefined || $scope.mData.MaterialRequestStatusId == null){
                    $scope.mData.MaterialRequestStatusId = "";
                    $scope.mData.MaterialRequestStatusId.id = "";
                }
                if ($scope.mData.RefMRTypeId == undefined || $scope.mData.RefMRTypeId == null){
                    $scope.mData.RefMRTypeId = "";
                    $scope.mData.RefMRTypeId.id = "";
                }
                MaterialRequest.getData($scope.mData)
                    .then(
                        function(res) {
                           $('#layoutContainer_ListToDoForm').height(10);
                            gridData = [];
                            $scope.grid.data = res.data.Result;
                            console.log("role .=>", res.data.Result);
                            $scope.loading = false;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            };
        };

        $scope.setNewDataItem = function() {
            $scope.dataNewItem = [{
                MaterialRequestId: "",
                MaterialRequestItemId: 0,
                PartNo: "",
                UnitPrice: 0,
                QtyRequest: 0,
                QtyGI: 0,
                QtyReserved: 0,
                QtyGIReserved: 0,
                QtyPrepicking: 0,
                QtyOrder: 0,
                UomId: 0
            }];
        }
        $scope.addNewDataItem = function(caller) {
            $scope.setNewDataItem();
            $scope.dataNewItem[0].MaterialRequestId = $scope.MRHeader.MaterialRequestId;
            $scope.dataNewItem[0].OutletId = $scope.MRHeader.OutletId;
            $scope.dataNewItem[0].WarehouseId = $scope.MRHeader.WarehouseId;
            $scope.gridDetail.data.push($scope.dataNewItem[0]);
        }
        $scope.getDataItem = function(dataHeader, mode) {
            $scope.IniMaterialRequestNo = "";
            $scope.IniMaterialRequestNo = dataHeader.MaterialRequestNo;
            console.log("coba liat",$scope.IniMaterialRequestNo);
            $scope.setNewDataItem();
            $scope.dataNewItem[0].MaterialRequestId = dataHeader.MaterialRequestId;
            $scope.dataNewItem[0].OutletId = dataHeader.OutletId;
            $scope.dataNewItem[0].WarehouseId = dataHeader.WarehouseId;
            MaterialRequest.getDataItem(dataHeader).then(function(res) {
                    var gridData = res.data.Result;
                    var i, Total = 0;
                    console.log(gridData);
                    console.log("Testing Item dari : ", dataHeader);
                    $scope.itemOldData = angular.copy(gridData);
                    $scope.itemOldData[0].MaterialRequestNo = $scope.IniMaterialRequestNo;
                    console.log("old data", $scope.itemOldData);
                    if (res.length == 0) { //gridDetail
                        $scope.gridDetail.data = [];
                    } else {
                        // $scope.gridOptions.data = gridData;
                        $scope.gridDetail.data = gridData;
                        // if (mode=='edit') {$scope.gridDetail.data.push($scope.dataNewItem[0]);} didisable 06062017..
                        $scope.showBuatPO = $scope.anyorder(gridData);
                        $scope.showPrePicking = $scope.anyPP(gridData);
                        $scope.showBuatGI = $scope.anyGI(gridData);
                        for (i = 0; i < gridData.length; i++) {
                            Total += gridData[i].HargaTotal;
                        }
                        $scope.GrandTotal = Total;
                    }
                    console.log("GrandTotal : ", $scope.GrandTotal);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
        $scope.formmode = { mode: '' };
        $scope.onBeforeNewMode = function() {
            //show gateway page
            $scope.isOverlayForm = true;

            $scope.formode = 1;
            $scope.isNewForm = true;
            $scope.isEditForm = false;
            $scope.selection = null;
            MaterialRequest.formApi = $scope.formApi;

            //disable another views
            $scope.isWO = false;
            $scope.isSO = false;
            $scope.isAp = false;
            //$scope.isPR = false;

            console.log("mode=>", $scope.formmode);
            $scope.newUserMode = true;
            $scope.editMode = false;
        }
        $scope.onBeforeEditMode = function() {
            // console.log("Before Edit data => ", $scope.mData);
            $scope.formode = 2;
            $scope.gridOptions = {};
            $scope.gridOptions.columnDefs = [
                { name: 'id', field: 'Id', width: '7%', visible: false },
                { name: 'Pekerjaan', field: 'pekerjaan', minWidth: 150, width: "*" },
                { name: 'No Material', field: 'no_material', minWidth: 100, width: "*" },
                { name: 'Nama Material', field: 'nama_material', minWidth: 150, width: "*" },
                { name: 'Qty Free', field: 'qtyfree' }, // icc
                { name: 'Qty Request', field: 'qtyrequest' }, // icc
                { name: 'Qty Order', field: 'qtyorder' }, // icc
                { name: 'Qty Reserved', field: 'qtyreserved' }, // icc
                { name: 'Harga Satuan', field: 'hargasatuan' }, // icc
                { name: 'Satuan', field: 'satuan' }, // icc
                { name: 'Harga Total', field: 'hargatotal' }, // icc
                { name: 'Status', field: 'status' }, // icc
            ];
            MaterialRequest.formApi = $scope.formApi;
            //pemilihan form edit
            console.log("Data dari CRUD => " + $scope.mData.refMR); // ['Appointment', 'WO', 'Sales Order'];
            switch ($scope.mData.refMR) {
                case "WO":
                case "2":
                    $scope.isWO = true;
                    $scope.isSO = false;
                    $scope.isAp = false;
                    //$scope.isPR = false;
                    console.log("WO");
                    break;
                case "Sales Orders":
                case "3":
                    $scope.isWO = false;
                    $scope.isSO = true;
                    $scope.isAp = false;
                    //$scope.isPR = false;
                    console.log("Sales Orders");
                    break;
                case "Appointment":
                case "1":
                    $scope.isWO = false;
                    $scope.isSO = false;
                    $scope.isAp = true;
                    //$scope.isPR = false;
                    console.log("Appointment");
                    break;
            }

            console.log("Data dari CRUD => " + $scope.mData.refMR);

            //Pengecekan bahan, jika Tipe Material == Bahan, maka Action Button visible TRUE
            if ($scope.mData.VehicleName === "Bahan") {
                console.log("Ini HarusnYa hilang");
                $scope.gridOptions[$scope.gridOptions.length - 1].visible = true;
            }

            $scope.isNewForm = false;
            $scope.isEditForm = true;
            $scope.newUserMode = false;
            console.log("mode=>", $scope.formmode);
            $scope.editMode = true;
        }
        $scope.onBeforeDeleteMode = function() {
            $scope.formode = 3;
            $scope.isNewForm = false;
            $scope.isEditForm = false;
            console.log("mode=>", $scope.formmode);
            //$scope.mode='del';
        }
        $scope.onShowDetail = function(row, mode) { // ['Appointment', 'WO', 'Sales Order'];
            console.log("=============== onshowdetail ===============");
            $scope.formode = 3;
            $scope.isNewForm = false;
            $scope.isEditForm = mode == 'edit';
            $scope.isAp = row.RefMRTypeId == "1"; //row.refMR == 'Appointment';
            $scope.isWO = row.RefMRTypeId == "2"; //row.refMR == 'WO';
            $scope.isSO = row.RefMRTypeId == "3"; //row.refMR == 'Sales Order';
            $scope.showBuatPO = false;
            console.log("mode=>osd ", mode);
            MaterialRequest.formApi = $scope.formApi;
            MaterialRequest.setMRHeader(row);
            // var oldData = angular.copy($scope.gridDetail.data);
            // console.log("old data", oldData);
            console.log(row);
            $scope.getDataItem(row, mode);
            $scope.EditableItem = mode == 'edit';
            $scope.IsBahan = row.MaterialTypeId == "2";
            $scope.MRHeader = MaterialRequest.getMRHeader();
            if (row.MaterialTypeId == "2") {
                $scope.gridDetail.columnDefs = $scope.columnsBahan;
            } else {
                $scope.gridDetail.columnDefs = $scope.columnsParts;
            }
            // $scope.CurrRowIndex = $scope.grid.data.indexOf(row.entity) ;
            $scope.CurrRowIndex = $scope.grid.data.indexOf(row);
            console.log("Delete btn ", row, $scope.showDeleteItemBtn);
            console.log("index data : ", $scope.CurrRowIndex);
        }

        //----------------------------------
        // Select Option Referensi GI Setup
        //----------------------------------
        $scope.changedValue = function(item) {
            console.log(item);
            $scope.isOverlayForm = false;
            $scope.ref = item;
            $scope.disableSelect = true;
        }
        $scope.DeleteItem = function(row) {
            if (confirm("Yakin hapus data ?")) {
                var index = $scope.gridDetail.data.indexOf(row.entity);
                //row.entity.IdState = 'd';
                $scope.gridDetail.data.splice(index, 1);
                console.log("data delete :", row.entity);
            }
        }
        $scope.canEdit = function() {
                return $scope.EditableItem;
            }
            // $scope.SearchMaterial = function(NoMaterial){
        function checkDuplicateInObject(propertyName, inputArray) {
            var seenDuplicate = false,
                testObject = {};

            inputArray.map(function(item) {
                console.log("itemmmm",item);
                var itemPropertyName = item[propertyName];
                    if (itemPropertyName != null){
                    if (itemPropertyName in testObject) {
                    testObject[itemPropertyName].duplicate = true;
                    item.duplicate = true;
                    seenDuplicate = true;
                    }
                    else {
                    testObject[itemPropertyName] = item;
                    delete item.duplicate;
                    }
                }
            });

            return seenDuplicate;
            }
        $scope.SearchMaterial = function(data) {
            console.log("i never be her : ", $scope.gridDetail.data);
            var part = { pcode: data.PartNo };
            if ($scope.mData.WarehouseId != null) { part['WarehouseId'] = $scope.mData.WarehouseId };
            // PartsGlobal.getDetailMaterial(data.PartNo).then(function(res) {
            console.log("Part Param : ", part);
            $scope.cekdatadouble = $scope.gridDetail.data;
            var tst = checkDuplicateInObject('PartNo',$scope.cekdatadouble);
            console.log("tasdfasdfasdf =====>",tst);
            if (tst == false){
                console.log("Masuk? sini");
            }
            else if (tst == true){
                console.log("Masuk?");
                // PartsGlobal.showAlert({
                //     title: "Material Request",
                //     content: "No Material Tidak Boleh Sama",
                //     type: 'danger'

                // }); return 0;
            }

        //     PartsGlobal.getDetailMaterialOneWarehouse(part).then(function(res) {
        //         var gridData = res.data.Result;
        //         if (typeof gridData == 'undefined') return 0;
        //         if(gridData[0] == undefined || gridData[0] == null){
        //             PartsGlobal.showAlert({
        //                 title: "Material Request",
        //                 content: "Harap Isi No Material dengan Benar",
        //                 type: 'danger'
        //             });
        //             return 0;
        //         }
        //         else{
        //             gridData[0]['PartName'] = gridData[0]['PartsName'];
        //         }
        //         gridData[0]['PartNo'] = gridData[0]['PartsCode'];
        //         //harga diambil dari sumber MR(AJob_Parts_List Atau Sales Order Item)
        //         if (data.MaterialRequestStatusId == 1 || data.MaterialRequestStatusId == null) { // Khusus data yg "Menunggu Parts Info", harga ambil dari master
        //             gridData[0]['UnitPrice'] = gridData[0]['RetailPrice'];
        //             gridData[0]['HargaTotal'] = gridData[0]['RetailPrice'] * data.QtyRequest;
        //             console.log("20-12-2017 20:42  : UnitPrice from master ", gridData[0]['UnitPrice']);
        //         }
        //         console.log(gridData[0]);
        //         var index = $scope.gridDetail.data.indexOf(data);
        //         // $scope.gridDetail.data[$scope.gridDetail.data.length - 1] = gridData[0];
        //         // $scope.gridDetail.data[index] = gridData[0];
        //         for (var k in gridData[0]) $scope.gridDetail.data[index][k] = gridData[0][k];
        //     },
        //     function(err) {
        //         console.log("err=>", err);
        //         // confirm("Nomor Material Tidak Ditemukan.");
        //         confirm("Ada masalah, search Material gagal ");
        //     }
        // );
        //     }else{

            PartsGlobal.getDetailMaterial(part.pcode).then(function(res) {
                var partData = res.data.Result;
                if (typeof partData == 'undefined') return 0;

                if (partData[0].ShelfId == 0 || partData[0].ShelfId == ''){
                    PartsGlobal.showAlert({
                        title: "Material Request",
                        content: "Lokasi material belum ditentukan, set lokasi gudang dan rak terlebih dahulu",
                        type: 'danger'
                    });
                    return 0;
                }

                if(partData[0] == undefined || partData[0] == null){
                    PartsGlobal.showAlert({
                        title: "Material Request",
                        content: "Harap Isi No Material dengan Benar",
                        type: 'danger'
                    });
                    return 0;
                }
                else if (partData[0].WarehouseId != part.WarehouseId) {
                    var msgError = '';
                    if (partData[0].PartsClassId1 == 1) {
                        msgError = "PartsCode adalah tipe "+partData[0].MaterialType+", hanya bisa input tipe Bahan"
                    } else {
                        msgError = "PartsCode adalah tipe "+partData[0].MaterialType+", hanya bisa input tipe Parts"
                    }
                    PartsGlobal.showAlert({
                        title: "Material Request",
                        content: msgError,
                        type: 'danger'
                    });
                    return 0;
                }
                else {
                    PartsGlobal.getDetailMaterialOneWarehouse(part).then(function(res) {
                        var gridData = res.data.Result;
                        if (typeof gridData == 'undefined') return 0;
                        if(gridData[0] == undefined || gridData[0] == null){
                            PartsGlobal.showAlert({
                                title: "Material Request",
                                content: "Harap Isi No Material dengan Benar",
                                type: 'danger'
                            });
                            return 0;
                        }
                        else{
                            gridData[0]['PartName'] = gridData[0]['PartsName'];
                        }
                        gridData[0]['PartNo'] = gridData[0]['PartsCode'];
                        //harga diambil dari sumber MR(AJob_Parts_List Atau Sales Order Item)
                        if (data.MaterialRequestStatusId == 1 || data.MaterialRequestStatusId == 75 ||
                            data.MaterialRequestStatusId == 5  || data.MaterialRequestStatusId == null ) { // Khusus data yg "Menunggu Parts Info", harga ambil dari master
                            gridData[0]['UnitPrice'] = gridData[0]['RetailPrice'];
                            gridData[0]['HargaTotal'] = gridData[0]['RetailPrice'] * data.QtyRequest;
                            console.log("20-12-2017 20:42  : UnitPrice from master ", gridData[0]['UnitPrice']);

                        }

                        var index = $scope.gridDetail.data.indexOf(data);
                        // $scope.gridDetail.data[$scope.gridDetail.data.length - 1] = gridData[0];
                        // $scope.gridDetail.data[index] = gridData[0];
                        for (var k in gridData[0]) $scope.gridDetail.data[index][k] = gridData[0][k];
                        var Total = 0;
                        console.log('cek DG', $scope.gridDetail.data);
                        for (i = 0; i < $scope.gridDetail.data.length; i++) {
                        console.log('detail ke', i , $scope.gridDetail.data[i]);
                            Total += $scope.gridDetail.data[i].HargaTotal;
                            console.log('Total', Total);
                        }
                        $scope.GrandTotal = Total;
                        console.log("GrandTotal", $scope.GrandTotal);
                        console.log(gridData[0]);
                    },
                    function(err) {
                        console.log("err=>", err);
                        // confirm("Nomor Material Tidak Ditemukan.");
                        confirm("Ada masalah, search Material gagal ");
                    })
                
                }
            },
            function(err) {
                console.log("err=>", err);
                confirm("Ada masalah, search Material gagal ");
            }       
        );

            //}



        }
        $scope.dataDetail = [{
                "id": "10",
                "pekerjaan": "Ganti Oli",
                "no_material": "No Partsman",
                "nama_material": "Mat Partsman",
                "qtyfree": "10",
                "qtyrequest": "20",
                "qtyorder": "75",
                "qtyreserved": "50",
                "hargasatuan": "1500",
                "satuan": "Pieces",
                "hargatotal": "100",
                "status": "3"
            },
            {
                "id": "11",
                "pekerjaan": "Ganti baut",
                "no_material": "m63536",
                "nama_material": "Sparepart baut",
                "qtyfree": "2",
                "qtyrequest": "2",
                "qtyorder": "3",
                "qtyreserved": "1",
                "hargasatuan": "100",
                "satuan": "Pieces",
                "hargatotal": "2000",
                "status": "2"
            }
        ];
        //----------------------------------
        // Grid Setup grid.appScope.$parent.qtyReserved(row.entity)">{{row.entity.reserved}}
        //----------------------------------
        // '1': 'Menunggu Parts Info',
        // '2': 'Menunggu Parts Info',
        // '3': 'Menunggu Order',
        // '4': 'Menunggu Receiving',
        // '5': 'Menunggu Pre Picking',
        // '6': 'Partial Pre Picking',
        // '7': 'Completed Pre Picking',
        // '75': 'Menunggu Goods Issue',
        // '80': 'Completed Request',
        // '90': 'Cancelled',
        // '99': 'Need Approval'
        $scope.ToNameStatus = function(MaterialRequestStatusId) {
            var statusName;
            switch (MaterialRequestStatusId) {
                case 1:
                    statusName = "Menunggu Parts Info";
                    break;
                case 2:
                    statusName = "Menunggu DP";
                    break;
                case 3:
                    statusName = "Menunggu Order"; //"Open";
                    break;
                case 4:
                    statusName = "Menunggu Receiving"; //Partial";
                    break;
                case 5:
                    statusName = "Menunggu Pre Picking"; //Completed";
                    break;
                case 6:
                    statusName = "Partial Pre Picking"; //Completed";
                    break;
                case 7:
                    statusName = "Completed Pre Picking"; //Completed";
                    break;
                case 75:
                    statusName = "Menunggu Goods Issue"; //Completed";
                    break;
                case 80:
                    statusName = "Completed Request"; //Completed";
                    break;
                case 90:
                    statusName = "Cancelled"; //Completed";
                    break;
                case 99:
                    statusName = "Need Approval"; //Completed";
                    break;
                    // case 6:
                    //   statusName = "Cancelled";
                    //   break;
            }
            return statusName;
        }

        var celltmp = '<div> {{grid.appScope.$parent.statusMR[row.entity.MaterialRequestStatusId -1].name}} </div>';
        var celltmpMatType = '<div> {{grid.appScope.$parent.MaterialType[row.entity.MaterialTypeId-1].name}} </div>';
        var celltmpRefMRType = '<div> {{grid.appScope.$parent.RefMRType[row.entity.RefMRTypeId-1].name}} </div>';
        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
        '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '<a href="#" ng-show="row.entity.MaterialRequestStatusId == 75 || row.entity.MaterialRequestStatusId == 5 || row.entity.MaterialRequestStatusId == 3 || row.entity.MaterialRequestStatusId == 1" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '</div>';
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'MaterialRequestId', width: '7%', visible: false },
                { name: 'Tipe Material id', field: 'MaterialTypeId', cellTemplate: celltmpMatType, visible: false },
                { name: 'Tipe Material', field: 'MaterialType', width: "*" },
                { name: 'No Material Req', field: 'MaterialRequestNo' },
                { name: 'Referensi MR_', field: 'RefMRTypeId', cellTemplate: celltmpRefMRType, visible: false, displayName: 'Referensi MR' },
                { name: 'Referensi MR', field: 'RefMRType', displayName: 'Referensi MR' },
                { name: 'No. Referensi', field: 'RefMRNo' }, // icc
                { name: 'MR No', field: 'MaterialRequestNo', visible: false },
                { name: 'No. Polisi', field: 'LicensePlate' }, // icc
                { name: 'No. Rangka', field: 'NoRangka', visible: false },
                { name: 'Tahun Rakit', field: 'AssemblyYear', visible: false },
                { name: 'KatashikiCode', field: 'FullModel', visible: false },
                { name: 'Model', field: 'VModel', visible: false },
                { name: 'ModelName', field: 'VehicleModelName', visible: false },
                { name: 'KodeWarna', field: 'KodeWarna', visible: false },
                { name: 'Warna', field: 'Warna', visible: false },
                { name: 'Tanggal MR', field: 'DocDate', cellFilter: 'date:\'yyyy-MM-dd\'', displayName: 'Tanggal MR' }, // icc
                { name: 'Notes', field: 'Notes', visible: false },
                { name: 'RefMRId', field: 'RefMRId', visible: false },
                { name: 'StatusId', field: 'MaterialRequestStatusId', visible: false },
                { name: 'Status', field: 'MaterialRequestStatus', width: "*", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.MaterialRequestStatusId)}}</div>' },
                {
                    name: 'Action',
                    displayName: 'Action',
                    width: 180,
                    pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ],
            data: $scope.getData({ MaterialRequestId: -1 }) //[
                //         {
                //           "Id": 1,
                //           "MaterialTypeId": "1",
                //           "RefMRTypeId": "1",
                //           "RefMRNo" : "WO",
                //           "LicensePlate" : "1234567890",
                //           "tglMR" : "12345",
                //           "status" : "1"
                //         },
                //         {
                //           "Id": 2,
                //           "MaterialTypeId": "1",
                //           "RefMRTypeId": "2",
                //           "RefMRNo" : "WO",
                //           "LicensePlate" : "1234567890",
                //           "tglMR" : "12345",
                //           "status" : "2"
                //         },
                //         {
                //           "Id": 3,
                //           "MaterialTypeId": "1",
                //           "RefMRTypeId": "2",
                //           "RefMRNo" : "WO",
                //           "LicensePlate" : "1234567890",
                //           "tglMR" : "12345",
                //           "status" : "3"
                //         },
                //         {
                //           "Id": 4,
                //           "MaterialTypeId": "1",
                //           "RefMRTypeId": "3",
                //           "RefMRNo" : "WO",
                //           "LicensePlate" : "1234567890",
                //           "tglMR" : "12345",
                //           "status" : "4"
                //         },
                //         {
                //           "Id": 5,
                //           "MaterialTypeId": "1",
                //           "RefMRTypeId": "2",
                //           "RefMRNo" : "WO",
                //           "LicensePlate" : "1234567890",
                //           "tglMR" : "12345",
                //           "status" : "5"
                //         }
                //     ]
        }; // <input type="text" ng-model="mData.PartNo" ng-value="row.entity.PartNo">  {{row.entity.WareouseId}}
        // <div><div class="input-group"><input type="text" ng-value="row.entity.PartNo">\ ng-model="row.entity.PartNo"
        // <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartNo)">\
        // var cellAction ='<div style="padding-top: 5px;"><u><a href="#" ng-show="grid.appScope.isEditForm" ng-click="grid.appScope.DeleteItem(row)">Hapus</a></u></div>';
        // var cellAction ='<div style="padding-top: 5px;"><u><a href="#" ng-show="true" ng-click="grid.appScope.DeleteItem(row)">Hapus</a></u></div>';
        var cellAction = '<div style="padding-top: 5px;"><u><a href="#" ng-show="true" >Hapus</a></u></div>';
        // var cellPartNo = '<div><div class="input-group" ng-model="row.entity.PartNo"><input type="text" ng-value="row.entity.PartNo">\
        //                   <label class="input-group-btn"><span class="btn wbtn" ng-show="grid.appScope.isEditForm" ng-click="grid.appScope.SearchMaterial(row.entity)">\
        //                   <i class="fa fa-search fa-1"></i></span>\
        //                   </label></div></div>';

        var cellPartNo = '<div><div class="input-group">\
                      <span>{{row.entity.PartNo}}</span>\
                      <label class="input-group-btn">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>';

        $scope.columnsParts = [
            { name: 'id', field: 'MaterialRequestItemId', width: '7%', visible: false },
            { name: 'MR id', field: 'MaterialRequestId', width: '7%', visible: false }, //OutletId
            { name: 'Outlet id', field: 'OutletId', width: '7%', visible: false }, //WarehouseId
            { name: 'Warehouse id', field: 'WarehouseId', width: '7%', visible: false }, //
            { name: 'Id Task', field: 'TaskId', visible: false, width: '10%' },
            { name: 'Pekerjaan', field: 'TaskName', enableCellEdit: false, width: '10%' },
            { name: 'Part Id', field: 'PartId', width: '7%', visible: false },
            {
                name: 'No Material',
                field: 'PartNo',
                cellEditableCondition: $scope.canEdit,
                width: '10%',
                cellTemplate: cellPartNo
            },
            { name: 'Nama Material', field: 'PartName', enableCellEdit: true, width: '10%' },
            { name: 'Qty Free', field: 'QtyFree', enableCellEdit: false, width: '10%' },
            { name: 'Qty Request', field: 'QtyRequest', enableCellEdit: false, width: '10%' },
            { name: 'Qty Order', field: 'QtyOrder', enableCellEdit: false, width: '10%' },
            { name: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false, width: '10%' },
            // { name:'Qty Reserved',  field: 'QtyReserved', enableCellEdit: false, width:'10%' },
            {
                name: 'Harga Satuan',
                field: 'UnitPrice',
                enableCellEdit: false,
                width: '10%',
                cellFilter: 'number'
            },
            { name: 'Satuan', field: 'UomId', enableCellEdit: false, visible: false, width: '5%' },
            { name: 'Satuan', field: 'Satuan', enableCellEdit: false, width: '5%' },
            {
                name: 'Harga Total',
                field: 'HargaTotal',
                enableCellEdit: false,
                width: '5%',
                cellFilter: 'number'
            },
            { name: 'SourceRequestItemId', field: 'SourceRequestItemId', width: '7%', visible: false },
            {
                name: 'Status',
                field: 'MaterialRequestStatusId',
                enableCellEdit: false,
                width: '10%',
                cellFilter: 'filterStatusMatReq'
                    //cellTemplate: celltmp
            },
            // { name:'state', field: 'IdState', visible:false}
        ];
        $scope.columnsBahan = [
            { name: 'id', field: 'MaterialRequestItemId', width: '7%', visible: false },
            { name: 'MR id', field: 'MaterialRequestId', width: '7%', visible: false },
            { name: 'Outlet id', field: 'OutletId', width: '7%', visible: false }, //
            { name: 'Warehouse id', field: 'WarehouseId', width: '7%', visible: false }, //
            { name: 'Id Task', field: 'TaskId', visible: false, width: '10%' },
            { name: 'Pekerjaan', field: 'TaskName', enableCellEdit: false, width: '10%' },
            { name: 'Part id', field: 'PartId', width: '7%', visible: false },
            {
                name: 'No Material',
                field: 'PartNo',
                cellEditableCondition: $scope.canEdit,
                minWidth: 100,
                width: "*",
                cellTemplate: cellPartNo
            },
            { name: 'Nama Material', field: 'PartName', enableCellEdit: true, minWidth: 150, width: "*" },
            { name: 'Qty Free', field: 'QtyFree', enableCellEdit: false, width: '10%' }, //
            { name: 'Qty Request', field: 'QtyRequest', enableCellEdit: false, width: '10%' }, //
            { name: 'Qty Order', field: 'QtyOrder', enableCellEdit: false, width: '10%' }, //
            { name: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false, width: '10%' }, //
            {
                name: 'Harga Satuan',
                field: 'UnitPrice',
                enableCellEdit: false,
                width: '10%',
                cellFilter: 'number'
            },
            { name: 'Satuan', field: 'UomId', enableCellEdit: false, visible: false, width: '5%' },
            { name: 'Satuan', field: 'Satuan', enableCellEdit: false, width: '5%' },
            {
                name: 'Harga Total',
                field: 'HargaTotal',
                enableCellEdit: false,
                width: '5%',
                cellFilter: 'number'
            },
            { name: 'SourceRequestItemId', field: 'SourceRequestItemId', width: '7%', visible: false },
            {
                name: 'Status',
                field: 'MaterialRequestStatusId',
                enableCellEdit: false,
                width: '10%',
                cellFilter: 'filterStatusMatReq'
                    //cellTemplate: celltmp
            },
            {
                name: '_',
                displayName: 'Action',
                enableCellEdit: false,
                // visible : true,
                //cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.$parent.hello()">Hapus</a></u></div>'
                cellTemplate: '<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>'
            }
            // { name:'_',     //headerCellClass: "middle",
            // displayName:'Action', width: '5%', enableCellEdit: true, cellTemplate: cellAction}
            // { name:'state', field: 'IdState', visible:false}
        ];
        // var cellAction = '<div> {{grid.appScope.$parent.hapus(row)}} </div>';
        $scope.gridDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    rowEntity.HargaTotal = (rowEntity.QtyRequest * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                });
                gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                    $scope.mySelections = gridApi.selection.getSelectedRows();
                    console.log("Item(s) : ", $scope.mySelections); // ...
                    //build aray string
                    $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                    console.log("item ids : ", $scope.itemids);
                });
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: $scope.columnsParts
        };
        //$scope.gridDetail.data = $scope.dataDetail;
    }).filter('filterStatusMatReq', function() {
        var xtipe = {
            '1': 'Menunggu Parts Info',
            '2': 'Menunggu DP',
            '3': 'Menunggu Order',
            '4': 'Menunggu Receiving',
            '5': 'Menunggu Pre Picking',
            '6': 'Partial Pre Picking',
            '7': 'Completed Pre Picking',
            '75': 'Menunggu Goods Issue',
            '80': 'Completed Request',
            '90': 'Cancelled',
            '99': 'Need Approval'
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xtipe[input];
            }
        };
    });
